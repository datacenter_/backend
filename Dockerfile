FROM registry.cn-beijing.aliyuncs.com/bcbase/backend_base:1.7.0
COPY . /var/www/gamecenter
## init auth
RUN chmod -R 777 /var/www/gamecenter/public
RUN chmod -R 777 /var/www/gamecenter/runtime
COPY .dockerconfig/nginx_conf/gamecenter.conf /etc/nginx/conf.d/gamecenter.conf
EXPOSE 80
