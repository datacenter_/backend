<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class AbiosController extends CommandsBase
{
    /**
     * pandascore元数据全量
     */
    public function actionAllMetadata()
    {
        $allTask = [
            //dota元数据
            [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","",""),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosDotaItemList::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_DOTA2_ITEM),
                "params"=>[
                    "action"=>"/assets",
                    "params"=>[
                        "filter"=>"game.id=1",
                        "skip" =>0,
                        "take" =>50
                    ]
                ],

            ],
            //lol元数据
            [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","",""),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosLOLItemList::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_LOL_ITEM),
                "params"=>[
                    "action"=>"/assets",
                    "params"=>[
                        "filter"=>"game.id=2",
                        "skip" =>0,
                        "take" =>50
                    ]
                ],

            ],
            //csgo元数据
            [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","",""),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosCSWeaponsList::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_CSGO_WEAPON),
                "params"=>[
                    "action"=>"/assets",
                    "params"=>[
                        "filter"=>"game.id=5,category=weapon",
                        "skip" =>0,
                        "take" =>50
                    ]
                ],

            ],
            [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","",""),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosCSMapList::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_CSGO_MAP),
                "params"=>[
                    "action"=>"/maps",
                    "params"=>[
                        "filter"=>"game.id=5",
                        "skip" =>0,
                        "take" =>50
                    ]
                ],

            ],
        ];
        foreach ($allTask as $task) {
            TaskRunner::addTask($task, 5);
        }
    }
    /**
     * abios战队全量
     */
    public function actionAllTeam()  //战队全量
    {
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTeamList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/teams",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }

    /**
     * abios战队全量
     */
    public function actionAllHistoryPlayer()  //历史选手
    {
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTeamList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/teams",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }

    /**
     * abios选手全量
     */
    public function actionAllPlayer()  //选手全量
    {
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/players",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }
    /**
     * abios赛事
     */
    public function actionAllTournament()
    {
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournaments",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }

    /**
     * 比赛跟赛事增量
     */
    public function actionIncidents()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);  //抓取五分钟以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
//        $incidentTime ="2020-11-14T03:04:50Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_ABIOS, "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosIncidents::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,"incidents"),
            "params" => [
                "action" => "/incidents",
                "params" => [
                    "order" =>"id-desc",
                    "skip" =>0,
                    "take" =>50
                ],
                "since" =>$incidentTime,
            ],
        ];
        TaskRunner::addTask($item, 5);
    }

    /**
     *abios未到结束时间的赛事
     */
    public function actionTournamentNotHaveEndTime()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60*12);
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item1 = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_ABIOS, "", "", "",""),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,"incidents"),
            "params" => [
                "action" => "/tournaments",
                "params" => [
                    "filter"=>"end>".$incidentTime,
                    "skip" =>0,
                    "take" =>50
                ],
            ],
        ];
        TaskRunner::addTask($item1, 5);

        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_ABIOS, "", "", "",""),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,"incidents"),
            "params" => [
                "action" => "/tournaments",
                "params" => [
                    "filter"=>"end=null",
                    "skip" =>0,
                    "take" =>50
                ],
            ],
        ];
        TaskRunner::addTask($item, 5);
    }

    public function actionAllMatchUpcoming()
    {

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);
    }

  public function actionAllWhereMatch(){
      $endTimeData = date('Y-m-d H:i:s', strtotime('-9 hours'));
      $endTimeEx = explode(' ',$endTimeData);
      $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);

      //刚结束（结束时间1小时内））
      $grebEndTime=[
          "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
              "","","",""),
          "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
          "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
          "params"=>[
              "action"=>"/series",
              "params"=>[
                  "filter"=>"lifecycle=over,end>=$endTime",/**/
                  "skip" =>0,
                  "take" =>50
              ]
          ],

      ];
      TaskRunner::addTask($grebEndTime, 5);

      $live=[
          "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
              "","","",""),
          "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
          "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
          "params"=>[
              "action"=>"/series",
              "params"=>[
                  "filter"=>"lifecycle=live",/**/
                  "skip" =>0,
                  "take" =>50
              ]
          ],

      ];
      TaskRunner::addTask($live, 5);


      $upcoming=[
          "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
              "","","",""),
          "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
          "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
          "params"=>[
              "action"=>"/series",
              "params"=>[
                  "filter"=>"lifecycle=upcoming",/**/
                  "skip" =>0,
                  "take" =>50
              ]
          ],

      ];
      TaskRunner::addTask($upcoming, 5);
  }


    //比赛
    public function actionOverEndTimeData()
    {
        $endTimeData = date('Y-m-d H:i:s', strtotime('-32 hours'));
        $endTimeEx = explode(' ',$endTimeData);
        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH);

        $upcomingstart = date('Y-m-d H:i:s',strtotime('+7 day'));
        $upcomingstartEx = explode(' ',$upcomingstart);
        $upcomingStartTime = sprintf("%sT%sZ",$upcomingstartEx[0],$upcomingstartEx[1]);

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=over,end>=$endTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);

        $live=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=live",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($live, 5);

        $upcoming=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming,start<=$upcomingStartTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($upcoming, 5);
    }

    public function actionGetUpcomingAll()
    {
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH);
        $upcoming=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($upcoming, 5);

    }

    /**
     * 重刷status =5，run_type=5的数据(最近三个小时创建的数据或最近一小时开始执行的数据)
     */
    public function actionRunTaskInfoException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-1 hour"));
        $timeSub2 = date("Y-m-d H:i:s",strtotime("-1 hour"));
        $nowTime = date("Y-m-d H:i:s");
        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',5],
            ['=','run_type',5],
        ]);
//        $taskInfo->andWhere(['or',
//            ['and',['>=','created_time',$timeSub],
//                ['<=','created_time',$nowTime]
//            ],
//            ['and',['>=','begin_time',$timeSub2],
//                ['<=','begin_time',$nowTime]
//            ]
//        ]);
        $taskInfo->andWhere(['and',
                ['>=','created_time',$timeSub],
                ['<=','created_time',$nowTime]
            ]);
        $taskInfoArray = $taskInfo->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                $value->setAttribute('status',1);
                $value->save();
                TaskRunner::run($value['id']);
            }
        }
    }
}