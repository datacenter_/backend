<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\commands;

use app\modules\task\services\api\RenovateService;
use yii\console\Controller;
use yii\console\ExitCode;

class ApiController extends CommandsBase
{
    // 刷新正常数量Api
    public function actionGrabApi($refreshType=null)
    {
        $this->isRefreshSingapore($refreshType);
        $apiType = [
            'game',
            'organization',
            'lol_champion',
            'lol_item',
            'lol_summonerspell',
            'lol_rune',
            'dota_hero',
            'dota_item',
            'csgo_map',
            'csgo_weapon',
            'tournament',
        ];
        foreach ($apiType as $type){
            print("现在正在刷新：{$type}...")."\n\n";
            parent::debug("现在正在刷新：{$type}...")."\n\n";
            RenovateService::refurbishApi($type,null,null,false);
        }
        print("刷新完成")."\n";
        return ExitCode::OK;

    }
    // 刷新所有战队
    public function actionGrabTeams($refreshType=null)
    {
        $this->isRefreshSingapore($refreshType);
        print("现在正在刷新：teams...")."\n\n";
        RenovateService::refurbishApi('team',null,null,false);
        print("刷新完成")."\n";
        return ExitCode::OK;
    }
    // 刷新所有选手
    public function actionGrabPlayers($refreshType=null)
    {
        $this->isRefreshSingapore($refreshType);
        print("现在正在刷新：players...")."\n\n";
        RenovateService::refurbishApi('player',null,null,false);
        print("刷新完成")."\n";
        return ExitCode::OK;
    }
    // 刷新所有比赛
    public function actionGrabMatches($refreshType=null)
    {
        $this->isRefreshSingapore($refreshType);
        print("现在正在刷新：matches...")."\n\n";
        RenovateService::refurbishApi('match',null,null,false);
        print("刷新完成")."\n";
        return ExitCode::OK;
    }
    // 刷新所有对局
    public function actionGrabBattles($refreshType=null)
    {
        $this->isRefreshSingapore($refreshType);
        print("现在正在刷新：battles...")."\n\n";
        RenovateService::refurbishApi('battle',null,null,false);
        print("刷新完成")."\n";
        return ExitCode::OK;
    }

    /**
     * 刷新进行中的比赛
     * 刷新5e-csgo进行中的比赛
     * 刷新5e-csgo进行中的比赛（赛事）
     */
    public function actionMatchHotRefresh()
    {
        // 循环刷新进行中的比赛
        while (true){
            try{
                // 查询进行中的比赛
                RenovateService::getOngoingMatch();
            }catch (\Exception $e){
                print_r($e);
                parent::debug(null,$e,'command_error')."\n";
            }
            sleep(3);
        }
    }

    // 是否刷新新加坡
    public function isRefreshSingapore($refreshType=null)
    {
        if ($refreshType) {
            \Yii::$app->params['refresh_key'] = "Singapore";
        } else {
            \Yii::$app->params['refresh_key'] = null;
        }
        return ;
    }
}
