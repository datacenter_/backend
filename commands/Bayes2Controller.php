<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\ExitCode;

/**
 * bayes任务
 */
class Bayes2Controller extends CommandsBase
{
    /**
     *战队全量
     */
    public function actionAllTeam()  //战队全量
    {
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM,"","",""),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/title",
                "team"=>"allTeam",
                "params"=>[
                    "page"=>1
                ]
            ],
        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }

    /**
     * 选手全量
     */
    public function actionAllPlayer()  //选手全量
    {
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER,"",""),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/title",
                "player"=>"allPlayer",
                "params"=>[
                    "page"=>1
                ]
            ],

        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }
    /**
     * 赛事全量
     */
    public function actionAllTournament()
    {
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT);
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","","have_no_end"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTournamentList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-date_end",
                    "page"=>1
                ]
            ],
        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }
    /**
     * 比赛全量
     */
    public function actionAllMatch()
    {
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"",""),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ]
            ],
        ];
        TaskRunner::addTask($item, 5);
        return ExitCode::OK;
    }
    /**
     * 战队增量更新
     * 四十分钟执行一次
     */
    public function actionTeamIncidents()
    {
        $nowTime = time()-60*60*2; //取最近俩小时内更新的数据
        $item = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/title",
                "team"=>"allTeam",
                "incidents_time"=>$nowTime,
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
            ],
        ];
        TaskRunner::addTask($item, 5);
    }
    /**
     * 选手增量更新
     * 四十分钟执行一次
     */
    public function actionPlayerIncidents()
    {
        $nowTime = time()-60*60*2; //取最近俩小时内更新的数据
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/title",
                "player"=>"allPlayer",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($item, 5);
    }

    /**
     * 比赛增量
     * 一分钟执行一次
     */
    public function actionMatchIncidents()
    {
        $nowTime = time()-60*5; //取最近五分钟内更新的数据
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 5);
    }

    /**
     * 比赛大增量
     * 五十分钟执行一次
     */
    public function actionMatchLongIncidents()
    {
        $nowTime = time()-60*60*2; //取最近俩小时的数据
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"","","long_incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 5);
    }

    /**
     *取所有未开始的比赛
     *每两个小时执行一次
     */
    public function actionMatchUpcoming()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"","","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "state"=>"pending",
                    "page"=>1
                ],
            ],
        ];
        TaskRunner::addTask($playerInfo, 5);
    }

    /**
     * 赛事增量更新
     *
     */
    public function actionTournamentIncidents()
    {
        $nowTime = time()-60*60*2; //取最近俩小时内的更新
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($item, 5);
    }

    /**
     * 未开始的赛事
     * 每俩小时执行一次
     */
    public function actionTournamentUpcoming()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60);  //取未开始的赛事+已经开始一个小时的比赛
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($item, 5);
    }
    public function actionMatchUpcomingAndTeams() {

        $dateTime =  date('Y-m-d H:i:s',strtotime('-60 minutes'));
        $newatime = date('Y-m-d H:i:s',time());
        $aa = \app\modules\task\models\StandardDataMatch::find()->where(
            ['and',['>','scheduled_begin_at', $dateTime],['<','scheduled_begin_at',$newatime],['deleted' => 2],['status' => 1],['origin_id' => 10 ]]);
        $m  = $aa ->createCommand()->getRawSql();
        $data = $aa->asArray()->all();

        foreach ($data as $k => $v) {
            $matchInfo=[
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"","","upcoming_by_teams"),
                "type"=>\app\modules\task\services\grab\bayes2\BayesMatch::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
                "params"=>[
                    "action"=>"/match/".$v['rel_identity_id'],
                ],
            ];
            TaskRunner::addTask($matchInfo, 5);
        }
    }

    /**
     * 重刷取数据为空的任务（每小时执行一次）(不一定用)
     */
    public function actionRunTaskInfoException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-2 hour"));
        $nowTime = date("Y-m-d H:i:s");

        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',5],
            ['=','run_type',1],
        ]);
        $taskInfo->andWhere(['and',
            ['>=','created_time',$timeSub],
            ['<=','created_time',$nowTime]
        ]);
        $taskInfoArray = $taskInfo->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                $value->setAttribute('status',1);
                $value->setAttribute('error_info',null);
                $value->save();
                TaskRunner::run($value['id']);
            }
        }
    }
}