<?php


namespace app\commands;
use app\modules\common\services\CommonLogService;
use app\modules\task\services\Common;
use yii\console\ExitCode;

class CheckBayesStandardIsDelController extends CommandsBase
{
    public function actionMatch()
    {
        try {
            $match['match'] = Common::checkBayesStandardIsDel('match', 9);
            print_r($match);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

    public function actionTournament()
    {
        try {
            $tournament['tournament'] = Common::checkGrebIsDel('tournament', 3, 'all');
            print_r($tournament);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

    public function actionTeam()
    {
        try {
            $team['team'] = Common::checkGrebIsDel('team', 3, 'all');
            print_r($team);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }

    public function actionPlayer(){
        try {
            $player['player'] = Common::checkGrebIsDel('player', 3, 'all');
            print_r($player);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;

        }

    }
}