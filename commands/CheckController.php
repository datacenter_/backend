<?php


namespace app\commands;


use app\modules\common\services\CommonLogService;
use app\modules\common\services\Consts;
use app\modules\common\services\EsService;
use app\modules\match\models\ErrorNotice;
use app\modules\task\models\MatchBattle;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use PHPUnit\phpDocumentor\Reflection\Types\Null_;
use yii\console\Controller;
use yii\console\ExitCode;
use \app\modules\match\models\Match;

class CheckController extends CommandsBase
{
    public function actionCheckOnline()
    {
        // 连接数据库更新
//        $sql='select * from publish_config order by id desc limit 1';
//        $info=\Yii::$app->getDb()->createCommand($sql)->queryOne();
//        $this->debug(json_encode($info));
//        if($info['identity']==='production'){
//            return ExitCode::UNSPECIFIED_ERROR;
//        }
        // 数据库连接配置
        if(env('MASTER_DB_HOST')!='rm-3ns2997w29u8hgiro.mysql.rds.aliyuncs.com'){
            echo "主库地址错误，退出！！\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }

        if(env('MASTER_DB_DATABASE')!='dc_prd'){
            echo "主库名字错误，退出！！\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }
        return ExitCode::OK;
    }
}