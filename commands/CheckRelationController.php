<?php
namespace app\commands;
use app\modules\data\models\DataMetaDataNoRelation;
use app\modules\match\models\MatchLived;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;
use yii\console\Controller;

class CheckRelationController extends CommandsBase
{
    //元数据 LOL 装备
    public function actionItems($originId = 3)
    {
        $game_id = 2;
        $items_no_relation = [];
        $player_no_relation = [];
        $champion_no_relation = [];
        $data_type = 'no';
        $startTime = date('Y-m-d')." 00:00:00";
        $endTime = date('Y-m-d')." 23:59:59";
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $data = MatchLived::find()->select('id,match_data')
                ->where(['origin_id'=>$originId])
                ->andWhere(['type'=>$data_type])
                ->andWhere(['game_id'=>$game_id])
                ->andwhere(['>=','created_at',$startTime])
                ->andwhere(['<=','created_at',$endTime])
                ->orderBy('id ASC')
    //            ->createCommand()->getRawSql();
                ->asArray()
                ->all();
            if ($data){
                $items_list = [];
                $player_list = [];
                $champion_list = [];
                foreach($data as $key => $value){
                    $match_data = json_decode($value['match_data'],true);
                    if (isset($match_data['type']) && $match_data['type'] =='hello'){
                        continue;
                    }
                    $blue_players = $match_data['blue']['players'];
                    foreach ($blue_players as $key => $val){
                        $player_list[$val['id']] = $val['id'];
                        $blue_champion_id = $val['champion']['id'];
                        $champion_list[$blue_champion_id] = $blue_champion_id;
                        $blue_player_item = $val['items'];
                        foreach ($blue_player_item as $key_items => $val_items){
                            if ($val_items){
                                $blue_player_item_id = $val_items['id'];
                                $items_list[$blue_player_item_id] = $blue_player_item_id;
                            }
                        }
                    }
                    $red_players = $match_data['red']['players'];
                    foreach ($red_players as $key => $val){
                        $player_list[$val['id']] = $val['id'];
                        $red_champion_id = $val['champion']['id'];
                        $champion_list[$red_champion_id] = $red_champion_id;
                        $red_player_item = $val['items'];
                        foreach ($red_player_item as $key_items => $val_items){
                            if ($val_items){
                                $red_player_item_id = $val_items['id'];
                                $items_list[$red_player_item_id] = $red_player_item_id;
                            }
                        }
                    }
                }
                $items_list_res = $items_list;
                $player_list_res = $player_list;
                $champion_list_res = $champion_list;
                if ($items_list_res){
                    $items_Redis_Res = [];
                    //查询已经绑定的列表
                    $redis = new Redis();
                    $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
                    $redis->auth(env('REDIS_PASSWORD'));
                    $redis->select(env('REDIS_DATABASE'));
                    $redisNameRelation = "lol:items:relation:".$originId;
                    $items_Redis = $redis->hGetAll($redisNameRelation);
                    //循环redis中数据
                    foreach ($items_Redis as $key_r => $val_r){
                        if ($val_r){
                            $items_Redis_Res[] = $key_r;
                        }else{
                            $items_no_relation[$key_r] = $key_r;
                        }
                    }
                    //循环原始数据
                    foreach ($items_list_res as $key_rel => $val_rel){
                        if (!in_array($val_rel,$items_Redis_Res)){
                            $items_no_relation[$key_rel] = $val_rel;
                        }
                    }
                    if ($items_no_relation){
                        $DataMetaDataNoRelation = new DataMetaDataNoRelation();
                        foreach ($items_no_relation as $value){
                            $DataMetaDataNoRelationModel = clone $DataMetaDataNoRelation;
                            $insertData = [
                                'origin_id' => $originId,
                                'resource_type' => 'lol_item',
                                'rel_id' => (String)$value,
                                'status'=>1
                            ];
                            $DataMetaDataNoRelationModel->setAttributes($insertData);
                            if (!$DataMetaDataNoRelationModel->save()){
                                throw new BusinessException($DataMetaDataNoRelationModel->getErrors(), "保存失败");
                            }
                        }
                    }
                }
                //玩家
                if ($player_list_res){
                    //循环player 查看有没有  没绑定的玩家
                    foreach ($player_list_res as $key_player => $item_player){
                        $player_bd_info = HotBase::getMainIdByRelIdentityId('player', $item_player, '3');
                        if (!$player_bd_info){
                            $player_no_relation[$key_player] = $item_player;
                        }
                    }
                    if ($player_no_relation){
                        $DataMetaDataNoRelation = new DataMetaDataNoRelation();
                        foreach ($player_no_relation as $value){
                            $DataMetaDataNoRelationModel = clone $DataMetaDataNoRelation;
                            $insertData = [
                                'origin_id' => $originId,
                                'resource_type' => 'lol_player',
                                'rel_id' => (String)$value,
                                'status'=>1
                            ];
                            $DataMetaDataNoRelationModel->setAttributes($insertData);
                            if (!$DataMetaDataNoRelationModel->save()){
                                throw new BusinessException($DataMetaDataNoRelationModel->getErrors(), "保存失败");
                            }
                        }
                    }
                }
                //英雄
                if ($champion_list_res){
                    //循环player 查看有没有  没绑定的玩家
                    foreach ($champion_list_res as $key_champion => $item_champion){
                        $champion_bd_info = HotBase::getMainIdByRelIdentityId('lol_champion', $item_champion, '3');
                        if (!$champion_bd_info){
                            $champion_no_relation[$key_champion] = $item_champion;
                        }
                    }
                    if ($champion_no_relation){
                        $DataMetaDataNoRelation = new DataMetaDataNoRelation();
                        foreach ($champion_no_relation as $value){
                            $DataMetaDataNoRelationModel = clone $DataMetaDataNoRelation;
                            $insertData = [
                                'origin_id' => $originId,
                                'resource_type' => 'lol_champion',
                                'rel_id' => (String)$value,
                                'status'=>1
                            ];
                            $DataMetaDataNoRelationModel->setAttributes($insertData);
                            if (!$DataMetaDataNoRelationModel->save()){
                                throw new BusinessException($DataMetaDataNoRelationModel->getErrors(), "保存失败");
                            }
                        }
                    }
                }
            }
        }
        return '执行成功';
    }

}