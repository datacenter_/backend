<?php


namespace app\commands;
use app\modules\common\services\CommonLogService;
use app\modules\task\services\Common;
use yii\console\ExitCode;

class CheckStandardIsDelController extends CommandsBase
{
    public function actionMatch(){
        try {
            $match['match'] = Common::checkGrebIsDel('match', 3, 'all');
            if (!empty($match['match'])) {
                parent::debug($match);
            }
            print_r($match);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

    public function actionTeam()
    {
        try {
            $team['team'] = Common::checkGrebIsDel('team', 3, 'all');
            if (!empty($team['team'])) {
                parent::debug($team);
            }
            print_r($team);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }

    public function actionPlayer(){
        try {
            $player['player'] = Common::checkGrebIsDel('player', 3, 'all');
            if (!empty( $player['player'])) {
                parent::debug($player);
            }
            print_r($player);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;

        }

    }

    public function actionTournament()
    {
        try {
            $tournament['tournament'] = Common::checkGrebIsDel('tournament', 3, 'all');
            if (!empty($tournament['tournament'])) {
                parent::debug($tournament);
            }
            print_r($tournament);
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

    public function actionMetadata(){
        try {
            $data = [];
            $data['lol_summoner_spell'] = Common::checkGrebIsDel('lol_summoner_spell',3,'all');
            $data['lol_champion'] = Common::checkGrebIsDel('lol_champion',3,'all');
            $data['lol_item'] = Common::checkGrebIsDel('lol_item',3,'all');
            $data['csgo_map'] = Common::checkGrebIsDel('csgo_map',3,'all');

            $data['csgo_weapon'] = Common::checkGrebIsDel('csgo_weapon',3,'all');
            $data['dota2_item'] = Common::checkGrebIsDel('dota2_item',3,'all');
            $data['dota2_ability'] = Common::checkGrebIsDel('dota2_ability',3,'all');
            $data['dota2_hero'] = Common::checkGrebIsDel('dota2_hero',3,'all');
            parent::debug($data);
            print_r($data);
            return ExitCode::OK;

        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }
}