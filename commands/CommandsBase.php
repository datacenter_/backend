<?php


namespace app\commands;


use yii\console\Controller;

class CommandsBase extends Controller
{
    public $uriStr;
    public $pjtName;
    public function beforeAction($action)
    {
        $this->uriStr = str_replace("/", '.', $action->getUniqueId());
        $this->pjtName = "command_" . $this->uriStr;
        if (class_exists('\Gclibs\Log\Log')) {
            \Gclibs\Log\Log::init($this->pjtName, dirname(__DIR__) . DIRECTORY_SEPARATOR . "runtime/logs");
        }
        // 判断是否存在方法
        \Yii::$app->params['log_begin_at'] = msectime();

        $result = [];
        $result['state'] = 'start to perform';
        $commandLine = $action->uniqueId;
        $code = null;
        $msg = null;

        $beginAt = \Yii::$app->params['log_begin_at'] ?? msectime();
        $endAt = msectime();
        self::addLog([
            'time' => date("Y-m-d H:i:s"),
            'project' => $this->pjtName,
            'code' => $code,
            'error_level' => $code == 1 ? 1 : 0,
            'path' => $this->uriStr,
            'run_time' => $endAt - $beginAt,
            'res_size' => round(strlen(json_encode($result)) / 1024, 3),
            'user_id' => 0,
            'user_type' => 0,
            'ext_info' => [
                'begin_at' => $beginAt,
                'end_at' => $endAt,
                'command_line' => $commandLine,
                'data' => $result,
            ],
            'exception_info' => $code == 0 ? null : [
                'code' => $code,
                'message' => $msg,
                'data' => $result,
                "err"=>null
            ],
        ]);
        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $commandLine = $action->uniqueId;
        $code = $result['code'] ?? null;
        $msg = $result['msg'] ?? null;
        $beginAt = \Yii::$app->params['log_begin_at'] ?? msectime();
        $endAt = msectime();
        self::addLog([
            'time' => date("Y-m-d H:i:s"),
            'project' => $this->pjtName,
            'code' => $code,
            'error_level' => $code == 1 ? 1 : 0,
            'path' => $this->uriStr,
            'run_time' => $endAt - $beginAt,
            'res_size' => round(strlen(json_encode($result)) / 1024, 3),
            'user_id' => 0,
            'user_type' => 0,
            'ext_info' => [
                'begin_at' => $beginAt,
                'end_at' => $endAt,
                'command_line' => $commandLine,
                'data' => "End to perform",
            ],
            'exception_info' => $code == 0 ? null : [
                'code' => $code,
                'message' => $msg,
                'data' => $result,
                "err"=>"command_error"
            ],
        ]);
        return parent::afterAction($action, $result);

    }

    private static function addLog($info, $printOut = true)
    {
        if ($printOut) {
            print_r(json_encode($info)."\n");
        }
        if (class_exists('\Gclibs\Log\Log')) {
            try {
                \Gclibs\Log\Log::outputLog($info);
            } catch (\Throwable $e) {
//                throw $e;
            }
        }
    }

    //$e 只有当打印错误时候使用第二参数并且第三个参数传 command_error，其他时候使用第一个参数
//
    public function debug($info,$e = null,$err = null)
    {
        self::addLog([
            'path' => $this->uriStr,
            'ext_info' => $info,
            'project' => $this->pjtName,
            "exception_info"=> $e,
            "err"=>$err

        ]);
    }

}