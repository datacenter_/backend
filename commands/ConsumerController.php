<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\task\services\TaskMQConsumer;
use app\modules\task\services\TaskRedisRunner;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\tasks\feijing\FeiJingBase;
use app\modules\task\services\tasks\feijing\Start;
use app\modules\task\services\tasks\lanjing\LanJingBase;
use app\modules\task\services\tasks\TaskTypes;
use MQ\Model\Message;
use WBY\MQ\SDK\MQClientFactory;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ConsumerController extends CommandsBase
{
    public function actionRunner()
    {
        // 跑任务
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'business_task';
        $groupId = 'GID_business';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 3, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }
    //执行addMqPriority 队列
    public function actionPriorityRunner()
    {
        // 跑任务
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'priority_list';
        $groupId = 'GID_business';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 3, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }
    //执行添加国外队列
    public function actionAbroad()
    {
        // 跑任务
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'abroad';
        $groupId = 'GID_business';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 3, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }

    public function actionRefreshRel()
    {
        // 跑任务
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'rest_catch_task';
        $groupId = 'GID_business';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 1, 0);
        /** @var Message[] $message */
        $subscriber->daemon();
    }

    public function actionRefreshUpdateConfig()
    {

    }

    public function actionRedisRunnerOrder($runModel='test'){
//        $LOCK_key='LOCK_redisQueue_order';
        //限制本次队列只有一个进程在操作
//        self::lockLimit($redis,$LOCK_key,60*30);
        while (true){
            $redis = self::getRedis();
            $key='redisQueue_order_'.$runModel;
            //获取当前队列长度
            $length = $redis->llen($key);
            if($length){
                print_r("新循环开始$length\n");
                parent::debug("新循环开始$length\n");
            }
            for($i=0;$i<$length;$i++){
                try{
                    $record = $redis->rpop($key);
                    print_r($i);
                    print_r('------');
                    if(!empty($record)){
                        //处理业务逻辑
                        $record_decode=json_decode($record,true);
                        @print_r($record_decode['id']);
                        TaskRunner::run($record_decode['id']);
                        print_r("执行完毕\n");
                        continue;
                    }
                }catch(\Throwable $e){
                    print_r("出错了！！");
                    print_r($e);
                    parent::debug([],$e,'command_error');

                    continue;
                }
            }
            continue;
        }
//        $redis->del($key); //业务逻辑处理完毕，解锁
//        $redis->close();
    }
    public static function lockLimit($redis,$key,$expire = 60){
        if(!$key) {
            return false;
        }
        do {
            if($acquired = ($redis->setnx($key, time()))) { // 如果redis不存在，则成功
                $redis->expire($key, $expire);
                break;
            }
            usleep($expire);
        } while (true);
        return true;
    }
    public static function getRedis()
    {
        $redis=\Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        return $redis;
    }

}