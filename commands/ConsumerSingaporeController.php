<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\task\services\TaskMQConsumer;
use MQ\Model\Message;
use WBY\MQ\SDK\MQClientFactory;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ConsumerSingaporeController extends CommandsBase
{
    /**
     * api队列（新加坡）
     */
    public function actionRunner()
    {
        \Yii::$app->params['refresh_key'] = "Singapore";
        // 跑任务
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'data_api';
        $groupId = 'GID_singapore';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 3, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }
}
