<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\task\services\grab\abios\PlayerList;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\feijing\LOLPropList;
use app\modules\task\services\grab\feijing\LOLTeamList;
use app\modules\task\services\grab\pandascore\DotaPlayerList;
use app\modules\task\services\grab\pandascore\DotaTeamList;
use app\modules\task\services\grab\pandascore\LOLHeroList;
use app\modules\task\services\grab\pandascore\LOLPlayerList;
use app\modules\task\services\grab\pandascore\LOLRuneList;
use app\modules\task\services\grab\pandascore\LOLSkillList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\realTime\abios\AbiosLolGet;
use app\modules\task\services\realTime\RealTimeRecordService;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\tasks\feijing\FeiJingBase;
use app\modules\task\services\tasks\feijing\Start;
use app\modules\task\services\tasks\lanjing\LanJingBase;
use app\modules\task\services\tasks\TaskTypes;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ConversionController extends CommandsBase
{
    public function actionFirst()
    {
        \app\modules\task\services\logformat\Csgo5e::getListAndDo();
        return ExitCode::OK;
    }
}