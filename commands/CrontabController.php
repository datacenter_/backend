<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\commands\crontab\TealPlayerRefresh;
use app\modules\common\services\Consts;
use app\modules\task\services\crontabTask\AutoCreateMatch;
use app\modules\task\services\crontabTask\RefreshImage;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class CrontabController
 * @package app\commands
 */
class CrontabController extends CommandsBase
{
    /**
     * @return int
     * @throws \yii\db\Exception
     * 计划每分钟执行一次，刷新未结束的赛事，自动创建match
     */
    public function actionAutoCreateMatch($runModel='test')
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_CRONTAB,
                "", "", "", $runModel,""),
            "type" => AutoCreateMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,"",Consts::RESOURCE_TYPE_MATCH),
            "params" => [],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }

    public function actionTeamPlayerRefresh($runModel="test",$gameId="all")
    {
        $service=new TealPlayerRefresh();
        $service->run(['rum_model'=>$runModel,'game_id'=>$gameId]);
    }

    public function actionRefreshImage($table,$col,$limit=null)
    {
        if(!$limit){
            $limit=1;
        }
        print_r(sprintf("table:%s,col:%s,limit:%s",$table,$col,$limit));
        RefreshImage::refresh($table,$col,$limit);
    }
}