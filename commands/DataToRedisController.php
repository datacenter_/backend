<?php
namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\data\models\StandardDataMetadata;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Map;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\task\services\StandardDataService;
use yii\console\Controller;
use yii\console\ExitCode;

class DataToRedisController extends CommandsBase
{
    /**
     * dota2 talent
     */
    public function actionDota2Talent() {
        $gameId = 3;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $talentNameList = "dota2:talent:list";
        $externalIdRedisName = 'dota2:talent:external_id_relation_list';
        $externalNameRedisName = 'dota2:talent:external_name_relation_list';
        $Dota2Talent = MetadataDota2Talent::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($Dota2Talent as $key => $value){
            $dota_talent_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($talentNameList,$value['id'],$dota_talent_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,8];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "dota2:talent:relation:".$originId;
            $redisNameList = "dota2:talent:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('dota2_talent',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($master_id){
                    //查找主表数据插入到redis
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "dota2:talent:list:unknown";
        $UnknownInfo = MetadataDota2Talent::find()->where(['talent'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    /**
     * dota2 ability
     */
    public function actionDota2Ability() {
        $gameId = 3;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $abilityNameList = "dota2:ability:list";
        $externalIdRedisName = 'dota2:ability:external_id_relation_list';
        $externalNameRedisName = 'dota2:ability:external_name_relation_list';
        $Dota2Ability = MetadataDota2Ability::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($Dota2Ability as $key => $value){
            $dota_ability_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($abilityNameList,$value['id'],$dota_ability_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,8];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "dota2:ability:relation:".$originId;
            $redisNameList = "dota2:ability:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('dota2_ability',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($master_id){
                    //查找主表数据插入到redis
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "dota2:ability:list:unknown";
        $UnknownInfo = MetadataDota2Ability::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    /**
     * dota2道具
     */
    public function actionDota2Items() {
        $gameId = 3;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $itemsNameList = "dota2:items:list";
        $externalIdRedisName = 'dota2:items:external_id_relation_list';
        $externalNameRedisName = 'dota2:items:external_name_relation_list';
        $Dota2Item = MetadataDota2Item::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($Dota2Item as $key => $value){
            $dota_items_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($itemsNameList,$value['id'],$dota_items_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,8];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "dota2:items:relation:".$originId;
            $redisNameList = "dota2:items:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('dota2_item',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($master_id){
                    //查找主表数据插入到redis
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "dota2:items:list:unknown";
        $UnknownInfo = MetadataDota2Item::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    /**
     * dota2英雄
     */
    public function actionDota2Hero() {
        $gameId = 3;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $championsNameList = "dota2:hero:list";
        $externalIdRedisName = 'dota2:hero:external_id_relation_list';
        $externalNameRedisName = 'dota2:hero:external_name_relation_list';
        $LolChampion = MetadataDota2Hero::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolChampion as $key => $value){
            $champions_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($championsNameList,$value['id'],$champions_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,8];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "dota2:hero:relation:".$originId;
            $redisNameList = "dota2:hero:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('dota2_hero',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($master_id){
                    //查找主表数据插入到redis
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "dota2:hero:list:unknown";
        $UnknownInfo = MetadataDota2Hero::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    //添加道具
    public function actionLolItems($originId = 3){
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $itemsNameList = "lol:items:list";
        $externalIdRedisName = 'lol:items:external_id_relation_list';
        $externalNameRedisName = 'lol:items:external_name_relation_list';
        $LolItem = MetadataLolItem::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolItem as $key => $value){
            $item_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($itemsNameList,$value['id'],$item_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:items:relation:".$originId;
            $redisNameList = "lol:items:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_item',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolItem::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "lol:items:list:unknown";
        $UnknownInfo = MetadataLolItem::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    //英雄
    public function actionLolChampions(){
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $championsNameList = "lol:champions:list";
        $externalIdRedisName = 'lol:champions:external_id_relation_list';
        $externalNameRedisName = 'lol:champions:external_name_relation_list';
        $LolChampion = MetadataLolChampion::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolChampion as $key => $value){
            $champions_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($championsNameList,$value['id'],$champions_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:champions:relation:".$originId;
            $redisNameIdRelation = "lol:champions:name_relation_id:".$originId;
            $redisNameList = "lol:champions:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_champion',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                //插入名称和ID的关联关系
                if ($val == 6){
                    $redis->hset($redisNameIdRelation,$item['external_name'],$master_id);
                }
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolChampion::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "lol:champions:list:unknown";
        $UnknownInfo = MetadataLolChampion::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    //元数据 LOL 召唤师技能
    public function actionLolSummonerSpell()
    {
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $summoner_spellNameList = "lol:summoner_spell:list";
        $externalIdRedisName = 'lol:summoner_spell:external_id_relation_list';
        $externalNameRedisName = 'lol:summoner_spell:external_name_relation_list';
        $LolAbility = MetadataLolSummonerSpell::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolAbility as $key => $value){
            $summoner_spell_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($summoner_spellNameList,$value['id'],$summoner_spell_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:summoner_spell:relation:".$originId;
            $redisNameIdRelation = "lol:summoner_spell:name_relation_id:".$originId;
            $redisNameList = "lol:summoner_spell:list:".$originId;
            //查询所有
            //插入名称和ID的关联关系
            if ($originId == 6){
                $MetadataLolSummonerSpellList =  MetadataLolSummonerSpell::find()->where(['deleted'=>2])->asArray()->all();
                foreach ($MetadataLolSummonerSpellList as $value){
                    //获取Unknown
                    if ($value['name'] == 'Unknown'){
                        $redis->set('lol:summoner_spell:list:unknown',@json_encode($value,320));
                    }else{
                        if (!empty($value['external_name'])){
                            $redis->hset($redisNameIdRelation,$value['external_name'],$value['id']);
                            $redis->hset($redisNameList,$value['id'],@json_encode($value,320));
                        }
                    }
                }
            }
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_summoner_spell',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                if ($originId != 6){
                    if ($master_id){
                        //查找主表数据插入到redis
                        //$itemRes = MetadataLolSummonerSpell::find()->where(['id'=>$master_id])->asArray()->one();
                        $itemRes = $master_id_relation_list[$master_id];
                        if ($itemRes){
                            $redis->hset($redisNameList,$master_id,@json_encode($itemRes,320));
                        }
                    }
                }
            }
        }
        //获取Unknown
        $unknownRedisNameList = "lol:summoner_spell:list:unknown";
        $UnknownInfo = MetadataLolSummonerSpell::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($unknownRedisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    //元数据 LOL 召唤师技能
    public function actionLolRune()
    {
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $runeNameList = "lol:rune:list";
        $externalIdRedisName = 'lol:rune:external_id_relation_list';
        $externalNameRedisName = 'lol:rune:external_name_relation_list';
        $LolRune = MetadataLolRune::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolRune as $key => $value){
            $rune_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($runeNameList,$value['id'],$rune_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:rune:relation:".$originId;
            //$redisNameIdRelation = "lol:rune:name_relation_id:".$originId;
            $redisNameList = "lol:rune:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_rune',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                //插入名称和ID的关联关系
//                if ($val == 6){
//                    $redis->hset($redisNameIdRelation,$item['external_name'],$master_id);
//                }
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolRune::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "lol:rune:list:unknown";
        $UnknownInfo = MetadataLolRune::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    //元数据 LOL Ability技能
    public function actionLolAbility()
    {
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $abilityNameList = "lol:ability:list";
        $externalIdRedisName = 'lol:ability:external_id_relation_list';
        $championAbilityHotkeyRelation = "lol:ability:champion_ability_hotkey_relation_list";
        $externalNameRedisName = 'lol:ability:external_name_relation_list';
        $LolAbility = MetadataLolAbility::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolAbility as $key => $value){
            $ability_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            if (!empty($value['champion_id']) && !empty($value['hotkey'])){
                $redis->hset($championAbilityHotkeyRelation,$value['champion_id'].'_'.$value['hotkey'],$value['id']);
            }
            $redis->hset($abilityNameList,$value['id'],$ability_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:ability:relation:".$originId;
            $redisChampionAbilityRelation = "lol:ability:champion_ability_relation_id:".$originId;
            $redisNameList = "lol:ability:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_ability',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                //插入名称和ID的关联关系
//                if ($val == 6){
//                    $redis->hset($redisNameIdRelation,$item['external_name'],$master_id);
//                }
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolAbility::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes && $itemRes['deleted'] == 2){
                        $redis->hset($redisChampionAbilityRelation,$itemRes['champion_id'].'_'.$itemRes['hotkey'],$itemRes['id']);
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameListUnknownInfo = "lol:ability:list:unknown";
        $UnknownInfo = MetadataLolAbility::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameListUnknownInfo,@json_encode($UnknownInfo,320));
        }
        return ExitCode::OK;
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getAllDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','std.name','std.external_name','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        //$sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }
    //刷新游戏地图信息到redis
    public function actionGameMaps(){
        $redisName = Consts::GAME_MAPS.":";
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        $game_ids = [1,2,3];
        foreach ($game_ids as $game_id){
            if ($game_id == 1){
                $maps = MetadataCsgoMap::find()->where(['deleted'=>2])->asArray()->all();
                foreach ($maps as $key => $item){
                    $redis->hset($redisName.'csgo',$item['id'],@json_encode($item,320));
                }
            }
            if ($game_id == 2){
                $maps = MetadataLolMap::find()->where(['deleted'=>2])->asArray()->all();
                foreach ($maps as $key => $item){
                    $redis->hset($redisName.'lol',$item['id'],@json_encode($item,320));
                }
            }
            if ($game_id == 3){
                $maps = MetadataDota2Map::find()->where(['deleted'=>2])->asArray()->all();
                foreach ($maps as $key => $item){
                    $redis->hset($redisName.'dota',$item['id'],@json_encode($item,320));
                }
            }
        }
        return ExitCode::OK;
    }
    //刷新游戏基础数据信息到redis
    public function actionIngameGoal(){
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        $redisName = "enum:ingame_goal";
        //获取道具列表
        $res = \app\modules\common\models\EnumIngameGoal::find()->asArray()->all();
        foreach ($res as $key => $item){
            $master_id = $item['id'];
            print_r("正在刷EnumIngameGoal，ID为：".$master_id);
            //插入关系列表
            $redis->hset($redisName,$master_id,@json_encode($item,320));
        }
        return ExitCode::OK;
    }
}