<?php


namespace app\commands;


use app\commands\check\EsApiCheck;
use app\modules\common\services\CommonLogService;
use app\modules\common\services\Consts;
use app\modules\common\services\EsService;
use app\modules\match\models\ErrorNotice;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchBattle;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use PHPUnit\phpDocumentor\Reflection\Types\Null_;
use yii\console\Controller;
use yii\console\ExitCode;
use \app\modules\match\models\Match;

class DingController extends CommandsBase
{
    public function actionPush()
    {
        try {
            $type = env("CONFIG_TYPE") ?? null;

            $lastDate = date("Y-m-d H:i:s", strtotime("-15 minute"));
            $taskInfo = TaskInfo::find()->where(['>=', 'begin_time', $lastDate])->andWhere(['status' => 4]);
            $datas = $taskInfo->asArray()->all();
            if (empty($datas)) {
                echo "没事数据\n";
            }
            foreach ($datas as $key => $val) {
                $newData = ['id' => $val['id'], 'run_type' => $val['run_type'], 'batch_id' => $val['batch_id'],
                    'error_info' => $val['error_info'], 'debug_info' => $val['debug_info']];
                $info = json_encode($newData,320);

                $params = [
                    'type' => 'task_info',
                    'tag' => $type .' '. $val['tag'],
                    'info' => $info,
                ];

                $infoJson = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params);
                var_export($infoJson);
            }

            //检测有没有创建多个battlePush信息或者有没有创建
            \app\commands\check\MatchSocketCheck::checkBattlePush();


            print_r("\n检测ESapi输出...start\n");
            //检测ESapi输出
            $team = EsApiCheck::team();
            print_r($team."\n");
            $player = EsApiCheck::player();
            print_r($player."\n");
            $match = EsApiCheck::match();
            print_r($match."\n");
            $tournaments = EsApiCheck::tournaments();
            print_r($tournaments."\n");
            print_r('检测ESapi输出...end');


            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }

    public  function actionDiffEsByMatch() {
        try {
            $type = env("CONFIG_TYPE") ?? null;
            if ($type != 'hk_Online') {
                return ExitCode::OK;
            }
            $lock = true;
            $matchData = Match::find()->select(['id'])->orderBy('id asc')->asArray()->all();
            $matchCount = count($matchData);
            print_r("比赛一共{$matchCount}条");
            $matchEs = $this->getEsMatchAll($matchCount);
            if ($matchCount == $matchEs['count']) {
                parent::debug(['没有差异值']);
                $lock = false;
            }
            if ($matchEs && $matchData && $lock) {

                $matchDataIds = array_column($matchData, 'id', 'id');
                $matchEsIds = array_column($matchEs['list'], 'match_id', 'match_id');
                print_r("ES比赛一共{$matchEs['count']}条");

                $diffArr = array_diff($matchDataIds, $matchEsIds);
                $diffJson = json_encode(array_values($diffArr));
                echo $diffJson;
                parent::debug($diffJson);
                $params = [
                    'type' => 'EsMatch',
                    'tag' =>  $type . ' EsMatch缺少',
                    'info' => $diffJson,
                ];
                if (!empty($diffArr)) {
                    $data = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params);
                    echo $data ."\n";
                }
            }


            print_r("\n监控高于ES比赛没有结束的数据_start\n");
            //监控高于ES比赛没有结束的数据
            $battle_start_at = date("Y-m-d H:i:s", strtotime("-24 hours"));
            $battle_end_at = date("Y-m-d H:i:s", strtotime("-12 hours"));
            $paramEs['range[begin_at]'] = $battle_start_at . ',' . $battle_end_at;
            $paramEs['token'] = env("ES_MATCHE_ONGOING_HONGKONG_TOKEN");
            $dataX = EsApiCheck::requestGet(env("ES_MATCHE_ONGOING_HONGKONG"), [], $paramEs);
            print_r("开始访问结果".$dataX);

            $dataXData = json_decode($dataX, true);

            if (!empty($dataXData['data']['list'])) {
                $matchIdX = array_column($dataXData['data']['list'], 'match_id');
                $JsonX = json_encode($matchIdX);

                $paramX = [
                    'type' => 'EsErrorMatchHK',
                    'tag' => $type.'_香港Es' . '异常进行中比赛',
                    'info' => $JsonX,
                ];
                $dataX = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $paramX);
                print_r($type.$dataX."香港Es异常进行中比赛\n");
            } elseif (!isset($dataXData['data']['list']) && $dataXData['code'] != 200) {
                $dataXData = json_encode($dataXData);
                print_r($type."香港Es' . '返回错误.$dataXData\n");

                $errorparamX = [
                    'type' => 'EsErrorMatchHK',
                    'tag' => $type.'_香港Es' . '返回错误',
                    'info' => $dataXData,
                ];
                $dataX = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $errorparamX);

            }


            $paramEsSingapore['range[begin_at]'] = $battle_start_at . ',' . $battle_end_at;
            $paramEsSingapore['token'] = env("ES_MATCHE_ONGOING_SINGAPORE_TOKEN");
            $errorXi = EsApiCheck::requestGet(env("ES_MATCHE_ONGOING_SINGAPORE"), [], $paramEsSingapore);
            print_r("开始访问结果".$errorXi);

            $errorSingapore = json_decode($errorXi, true);

            if (!empty($errorSingapore['data']['list'])) {

                $matchIdXi = array_column($errorSingapore['data']['list'], 'match_id');
                $JsonXi = json_encode($matchIdXi);
                print_r($type."新加坡Es异常进行中比赛.$JsonXi\n");

                $paramXi = [
                    'type' => 'EsErrorMatchSgp',
                    'tag' => $type.'新加坡Es异常进行中比赛',
                    'info' => $JsonXi,
                ];
                $dataXi = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $paramXi);
                echo $dataXi . "\n";
            } elseif (!isset($errorSingapore['data']['list']) && $errorSingapore['code'] != 200) {
                $errorSingapore = json_encode($errorSingapore);
                print_r($type."新加坡Es返回错误:$errorSingapore\n");

                $errorSin = [
                    'type' => 'EsErrorMatchSgp',
                    'tag' => $type.'新加坡Es返回错误',
                    'info' => $errorSingapore,
                ];
                $dataX = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $errorSin);
            }


            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }



    public function getEsMatchAll($matchCount) {
        $index_info = 'resource_match_list';
        $conditions = [];
        $order = array('field' => 'match_id', 'sort' => 'asc');
        $offset  =  0;
        $columns = [];
        $EsService = EsService::initConnect();
        $res     = $EsService->getIndexList($index_info, $offset, $matchCount,$conditions,$order,$columns);
        return $res;
    }

    public  function getEsOngoingMatch($Singapore = null) {
        $battle_begin_at = date("Y-m-d H:i:s", strtotime("-12 hours"));

        $index_info = 'resource_match_list';
        $conditions = [];
        $conditions = [
            ["field" => 'status', "type" => '=', "value" => 'ongoing'],
            ['field'=>'begin_at',"type"=>'<=' , 'value' => $battle_begin_at]
        ];
        $order = array('field' => 'match_id', 'sort' => 'asc');
        $offset  =  0;
        $columns = [];
        if ($Singapore) {
            \Yii::$app->params['refresh_key'] = "Singapore";

        }
        $EsService = EsService::initConnect();
        $res     = $EsService->getIndexList($index_info, $offset, 1000,$conditions,$order,$columns);
        return $res;
    }

    public  function actionCountErrorMatchStatus(){
        try {
            $type = env("CONFIG_TYPE") ?? null;

            $battle_begin_at = date("Y-m-d H:i:s", strtotime("-12 hours"));
            $battle = MatchBattle::find()->alias('b')
                ->leftJoin('`match` as m','b.`match` = m.id')
                ->leftJoin('data_resource_update_config as c',"c.resource_id = m.id and c.resource_type = 'match' and c.tag_type = 'score_data' ")
                ->select([
                    "b.`match`",'b.id','b.game','b.status','c.origin_id'])
                ->where(
                    [
                        'and',
                        [
                            "or",
                            [
                                'and',
                                ["b.status" => "2"],
                                ["<=", "b.begin_at", $battle_begin_at]
                            ],
//                        [
//                        'and',
//                        ["b.end_at"=>null],
//                        [
//                            "b.begin_at"=>null,
//                            "b.status"=>"3"
//                        ]
//                        ]
                        ],
                        ['b.deleted' => 2],
                        ['m.deleted' => 2]
                    ]
                );
//        $battlesal = $battle->createCommand()->getRawSql();
         $battleData = $battle->asArray()->all();

         //有问题的battleId
            $errBattleId = array_column($battleData,'id');

            foreach ($battleData as $bkey => $bval) {
                $battleErrorNotice = ErrorNotice::find()->where(['battle_id' => $bval['id']])->one();
                if (empty($battleErrorNotice)) {
                    $battleErrorNotice = new ErrorNotice();

                }

                $bdata['resource_id'] = $bval['match'];
                $bdata['resource_type'] = Consts::RESOURCE_TYPE_MATCH;
                $bdata['status'] = "1";
                $bdata['battle_id'] = $bval['id'];
                $bdata['error_info'] = "battle当前状态{$bval['status']}是异常请处理";
                $bdata['game_id'] = $bval['game'];
                $bdata['origin_id'] = (int)$bval['origin_id'];
                $bdata['modified_at'] =  date("Y-m-d H:i:s", time());
                $battleErrorNotice->setAttributes($bdata);
                if (!$battleErrorNotice->save()) {
                    $error_info = [
                        "msg" => "保存记录错误比赛低下buttle状态失败",
                        "file" => json_encode($battleErrorNotice->getErrors()),
                        "code" => ExitCode::UNSPECIFIED_ERROR,
                    ];
                    return $error_info;
                }
            }

            //把正常的状态改为2


            $begin_at = date("Y-m-d H:i:s", strtotime("-12 hours"));

            $match = Match::find()->alias('m')
                ->select(['m.id as m_id', 'm.game', 'm.deleted', "t.*"])
                ->leftJoin('match_real_time_info as t', "t.id = m.id")
                ->where(
                    [
                        'and',
                        [
                            "or",
                            [
                                'and',
                                ["t.status" => "2"],
                                ["<=", "t.begin_at", $begin_at]

                            ],
//                    [
//                        'and',
//                        ['not',["t.end_at"=>null]],
//                        ['not',["t.begin_at"=>null]],
//                        ['!=',"t.status","3"]
//                    ],
//                    [
//                        'and',
//                        ["t.end_at"=>null],
//                        [
//                            "t.begin_at"=>null,
//                            "t.status"=>"3"
//                        ]
//                    ]

                        ],

                        ['m.deleted' => 2]
                    ]
                );

//        $sal = $match->createCommand()->getRawSql();
            $matchData = $match->asArray()->all();

            //有问题的matchId
            $errMatchId = array_column($matchData,'m_id');

            foreach ($matchData as $key => $val) {
                $matchErrorNotice = ErrorNotice::find()->where(
                    ["and",
                        ['resource_id' => $val['m_id']],
                        ['battle_id' => null]
                    ]
                )->one();
                if (empty($matchErrorNotice)) {
                    $matchErrorNotice = new ErrorNotice();

                }

                $data['resource_id'] = $val['m_id'];
                $data['resource_type'] = Consts::RESOURCE_TYPE_MATCH;
                $data['status'] = "1";
                $data['error_info'] = "match当前状态{$val['status']}是异常请处理";
                $data['game_id'] = $val['game'];
                $data['modified_at'] =  date("Y-m-d H:i:s", time());

                $matchErrorNotice->setAttributes($data);
                if (!$matchErrorNotice->save()) {
                    $error_info = [
                        "msg" => "保存记录错误比赛状态失败",
                        "file" => json_encode($matchErrorNotice->getErrors()),
                        "code" => ExitCode::UNSPECIFIED_ERROR,
                    ];
                    return $error_info;

                }
            }


            //把已处理的改为2
            $allNotice =  ErrorNotice::find()->where([])->asArray()->all();

            foreach ($allNotice as $aKey => $aVey) {
                if (!empty($aVey['battle_id'])) {
                    if (!in_array($aVey['battle_id'],$errBattleId)){
                        ErrorNotice::updateAll(['status'=>'2'],['battle_id'=>$aVey['battle_id']]);
                       continue;
                    }
                }else{
                    if (!in_array($aVey['resource_id'],$errMatchId)){
                        ErrorNotice::updateAll(['status'=>'2'],['resource_id'=>$aVey['resource_id']]);
                        continue;
                    }
                }

            }




            $battleErrorNotice = ErrorNotice::find()->where(
                [
                    "and",
                    ['not', ['battle_id' => null]],
                    ["status" => 1],
                    [">","modified_at",$begin_at]
                ]
            )->count();

            if (!empty($battleErrorNotice)) {
                echo "有异常的进行中battle";
                $params = [
                    'type' => 'ErrorNotice',
                    'tag' => $type . " battle_ongoing",
                    'info' => json_encode("异常的进行中battle一共有{$battleErrorNotice}场", JSON_UNESCAPED_UNICODE),
                ];
                $battle_ding = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params);
                var_export($battle_ding) . "\n";
            }

            $matchErrorNotice = ErrorNotice::find()->where(
                [
                    "and",
                    ['battle_id' => null],
                    ["status" => 1],
                    [">","modified_at",$begin_at]

                ]
            )->count();
            if (!empty($matchErrorNotice)) {
                echo "有异常的进行中match";
                $paramss = [
                    'type' => 'ErrorNotice',
                    'tag' => $type . " match_ongoing",
                    'info' => json_encode("异常的进行中match一共有{$matchErrorNotice}场", JSON_UNESCAPED_UNICODE),
                ];
                $match_ding = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $paramss);
                var_export($match_ding) . "\n";
            }
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }


    }

    public function actionReuse(){
        try {
            $type = env("CONFIG_TYPE") ?? null;

            $lastDate = date("Y-m-d H:i:s", strtotime("-1 day"));
            $minuteDate = date("Y-m-d H:i:s", strtotime("-10 minute"));

            $taskInfo = TaskInfo::find()->select('id,tag')->where([
                "and",
                ['>=', 'created_time', $lastDate],
                ["<=", 'created_time', $minuteDate],
            ])->andWhere(['status' => 1]);
            $datas = $taskInfo->asArray()->all();
            if (empty($datas)) {
                echo "没事数据\n";
            }

            if (!empty($datas)) {
                $newData = array_column($datas,"id",'');
                $info = json_encode($newData);

                $params = [
                    'type' => 'task_info_reuse',
                    'tag' =>$type. " task_info没有执行",
                    'info' => $info,
                ];
                $infoJson = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params);
                var_export($infoJson);

                foreach ($datas as $key => $val) {
                    if(strpos($val['tag'], 'main_increment.') !== false){
                        TaskRunner::run($val['id']);
                    }
                }

            }

            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }


    public function actionShua() {
        for ($i = 0;$i<30000;$i++) {
            $matchData = Match::find()->select(['id','team_1_id','team_2_id'])
                ->where(['team_version'=>null])->limit(500)->orderBy('id asc')->asArray()->all();

            if (empty($matchData)) {
                break;
            }
            foreach ($matchData as $key => $val) {
                $team_version = MatchService::setTeamVersion($val['id'],$val['team_1_id'],$val['team_2_id']);
                $params['team_version'] = $team_version;
                print_r("处理{$val['id']} team_version 变为 {$team_version}\n");
                Match::updateAll($params,['id'=>$val['id']]);
            }
        }

    }
}