<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class HltvController extends CommandsBase
{
    /**
     * hltv战队全量
     */
    public function actionAllTeam()  //战队全量
    {
        $item = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvTeam::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TEAM),
            "params" => [
                'type' => "team",
                'offset' => "0",
                'limit' => "50",
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    /**
     * hltv选手全量
     */
    public function actionAllPlayer()  //选手全量
    {
        $item = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvPlayer::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_PLAYER),
            "params" => [
                'type' => "player",
                'offset' => "0",
                'limit' => "50",
            ],
        ];
        TaskRunner::addTask($item, 1);
    }


    public function actionAllTournament()
    {
        $nowTime = date('Y-m-d');
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvTournament::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params" => [
                'type' => "tournament",
                'offset' => "0",
                'limit' => "50",
                'where' => "where scheduled_begin_at > '$nowTime' or scheduled_end_at > '$nowTime' "
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);

    }

    /**
     * 抓取hltv比赛
     */
    public function actionAllMatch()
    {
//        $nowTime = date('Y-m-d H:i:s');
        $nowTime = time();
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "50",
//                'where' => "where scheduled_begin_at > '$nowTime' "
                'where' => "where match_time > '$nowTime' "
            ],
        ];
        TaskRunner::addTask($info, 1);

    }
}