<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\common\services\Consts;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class HltvMatchIncidentsController extends CommandsBase
{
    /**
     * Hello Test
     * @return int
     */
    public function actionRefresh()
    {
        $dateSub = date("Y-m-d H:i:s",time()-5*60);
        $dateNow = date("Y-m-d H:i:s");
        $startTask = [
                "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_HLTV, "", "", "","incidents"),
                "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
                "params" => [
                    'type' => "match",
                    'offset' => "0",
                    'limit' => "20",
//                    'where' => "WHERE update_at> DATE_SUB(NOW(),INTERVAL 2 MINUTE)", //抓1分钟内的数据
                    'where' => "WHERE update_at> '".$dateSub."' AND update_at<= '".$dateNow."'", //抓两分钟内的数据
                ]
        ];
        TaskRunner::addTask($startTask, 1);
        return ExitCode::OK;
    }
}