<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\data\models\DbStream;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class HltvOriginIncidentsController extends CommandsBase
{
    /**
     * Hello Test
     * @return int
     */
    public function actionRefresh()
    {
        //
        $startTask = [
            [
                "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\hltv\HltvTeam::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TEAM),
                "params" => [
                    'type' => "team",
                    'offset' => "0",
                    'limit' => "50",
                    'where' => "WHERE update_at> DATE_SUB(NOW(),INTERVAL 3 HOUR)", //抓三个小时内的数据
                ],
            ],
            [
                "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\hltv\HltvPlayer::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_PLAYER),
                "params" => [
                    'type' => "player",
                    'offset' => "0",
                    'limit' => "50",
                    'where' => "WHERE modified_at> DATE_SUB(NOW(),INTERVAL 3 HOUR)", //抓三个小时内的
                ],
            ],
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
        //赛事任务处理
        $tournaments = DbStream::getDb()->createCommand(
            "select distinct id as tournament_id from tournament_all where update_at > DATE_SUB(NOW(),INTERVAL 3 HOUR)"
        )->queryAll();
        $tournamentsIds = array_column($tournaments,'tournament_id');
        $prizeDistributions = DbStream::getDb()->createCommand(
            "select distinct tournament_id from prize_distribution where update_at > DATE_SUB(NOW(),INTERVAL 3 HOUR)"
        )->queryAll();
        $pIds = array_column($prizeDistributions,'tournament_id');

        $teamSnapshots = DbStream::getDb()->createCommand(
            "select distinct tournament_id from team_snapshot where update_at > DATE_SUB(NOW(),INTERVAL 3 HOUR)"
        )->queryAll();
        $tIds = array_column($teamSnapshots,'tournament_id');

        $ids = array_unique(array_merge($tournamentsIds,$pIds,$tIds));
        $idsString = implode(",",$ids);
        if($idsString){
            $taskArray =  [
                "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\hltv\HltvTournament::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TOURNAMENT),
                "params" => [
                    'type' => "tournament",
                    'offset' => "0",
                    'limit' => "50",
                    'where' => "WHERE id IN($idsString)", //抓3小时内
                ],
            ];
            TaskRunner::addTask($taskArray, 1);
        }
        return ExitCode::OK;
    }
}
