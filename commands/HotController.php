<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\data\models\ReceiveData5eplay;
use app\modules\data\models\ReceiveDataFunspark;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\services\operation\OperationRefreshHotInfo;
use app\modules\task\services\QueueServer;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HotController extends CommandsBase
{
    public function actionRefresh()
    {
        return OperationRefreshHotInfo::refreshList();
    }
    public function actionGenerator()
    {
        $lg=new LogGenerator();
        $lg->addInfo();
    }

    public function actionGeneratorS()
    {
        $lg=new LogGenerator();
        $lg->addInfo('funspark','2020-06-25 16:58:49','2020-06-25 17:52:29','127.0.0.2');
    }

    public function actionReInit()
    {
        // 修改状态为未处理
        ReceiveData5eplay::updateAll(['is_handle'=>2],['>','id','0']);
        \app\modules\task\services\logformat\Csgo5e::getListAndDo();
        return ExitCode::OK;
    }

    public function actionReInitsp()
    {
        // 修改状态为未处理
        ReceiveDataFunspark::updateAll(['is_handle'=>2],['>','id','0']);
        \app\modules\task\services\logformat\CsgoFunspark::getListAndDo();
        return ExitCode::OK;
    }

    public function actionReInitUcc()
    {
        ReceiveDataFunspark::updateAll(['is_handle'=>2],['>','id','0']);
        CsgoLogUCC::getListAndDo();
    }
    public function actionReInitE()
    {
        // 修改状态为未处理
        ReceiveDataFunspark::updateAll(['is_handle'=>2],['>','id','0']);
        \app\modules\task\services\logformat\CsgoFunspark::getListAndDo();
        return ExitCode::OK;
    }

    // 5ezhiwaide,rest
    public function actionRefreshForLogInfo()
    {
        // 循环log刷新表，循环插入更新事件
        while (true){
            try{
                OperationRefreshHotInfo::refreshListForLog();
            }catch (\Exception $e){
                print_r($e);
                parent::debug(null,$e,'command_error');
            }
            sleep(1);
        }

    }

    // zhipao5e  refresh_for_log_info_five
    public function actionRefreshForLogInfoFive()
    {
        // 循环log刷新表，循环插入更新事件
        while (true){
            echo 111;
            try{
                OperationRefreshHotInfo::refreshListForLogWs();
            }catch (\Exception $e){
                print_r($e);
                parent::debug(null,$e,'command_error');
            }
            sleep(10);
        }

    }

    public function actionWsapi()
    {
        $list=\app\modules\match\models\MatchLived::find()->where(['game_id'=>1,'match_id'=>567926])->orderBy('socket_time')->limit(500000)->asArray()->all();
        foreach($list as $k=>$v){
            $params=$v;
            $params['match_data']=json_decode($params['match_data'],true);
            print_r($params);
            parent::debug($params);
            $taskInfo=[
                'params'=>json_encode($params)
            ];
            $info = \app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi::run([],$taskInfo);

        }
    }
}