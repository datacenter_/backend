<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class IncidentsController extends CommandsBase
{
    /**
     * Hello Test
     * @return int
     */
    public function actionRefresh()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);  //抓取五分钟以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,"","incidents"),
            "params" => [
                "action" => "/incidents",
                "params" => [
                    "since" =>$incidentTime,
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }
}