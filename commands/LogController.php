<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\data\models\DbSocket;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchLivedDatasOffset;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\kafka\KafkaCustomer;
use app\modules\task\services\kafka\KafkaProducer;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\match\models\MatchLived;
use app\modules\task\services\hot\hltv\HltvMatchWs;
use app\rest\exceptions\BusinessException;
use Redis;
use RdKafka;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class LogController extends CommandsBase
{

    public $redis;
    public $offset;
    /**
     * @param int $offset
     * @param int $limit
     * @param $speed
     * @param string $ip
     * @param int $id
     * @return int
     */
    public function actionEvent($offset = 0, $limit = 1000, $speed, $ip = 'test092710', $start_id = 0,$end_id=0,$incr_id=0)
    {
        $start_time = time();
        $ord = ReceiveData5eplayFormatEvents::find();
        $ord->andWhere(['ip_address' => $ip]);
        if ($start_id > 0) {
            $ord->andWhere(['>=', 'id', $start_id]);
        }
        if ($end_id > 0) {
            $ord->andWhere(['<=', 'id', $end_id]);
        }
        if ($incr_id > 0) {
            $ord->andWhere(['=', 'id', $incr_id]);
        }
        $list= $ord->asArray()->all();
//        echo $ord->createCommand()->getRawSql();exit;
//        $list         = $ord->asArray()->offset($offset)->limit($limit)->all();
//        print_r($list);exit;
        $j            = 0;
        $offset_speed = 0;
        $count        = count($list);
//        for ($i = 0; $i <= 99999999; $i++) {
//            $filter_list = array_slice($list, $offset_speed, $speed);
//            if ($offset_speed >= $count) {
//                $end_time   = time();
//                $spend_time = date('i:s', $end_time - $start_time);
//                exit('complete! spend time ' . $spend_time);
//            }
//            $offset_speed = $i * $speed + $speed;
//            print_r($filter_list);exit;
            foreach ($list as $k => $v) {
                $v['event_id'] = $v['id'];
                $cc['params']  = json_encode($v,320);
                $res           = FiveHotCsgoMatchWs::run('', $cc);
                $j++;
                $info = json_decode($v['info'], true);
                print_r($j . '-------' . $info['log_time'] . '----------' . $v['id'] . '---------' . $v['event_type'] . PHP_EOL);
                parent::debug($j . '-------' . $info['log_time'] . '----------' . $v['id'] . '---------' . $v['event_type'] . PHP_EOL);
//                print_r( $v );
//                sleep(1);
            }
            sleep(1);
//        }
        return ExitCode::OK;
    }

    public function actionTest(){
        $start_time = time();
        $id = 226028;
        $id = 20288;
        $id = 191481;
        $id = 327019;//tiger
//        $id = 277892;//dev

        $taskInfo = TaskInfo::find()->where(['id'=>$id])->asArray()->one();

        $res=\app\modules\task\services\hot\five\FiveHotCsgoMatchWsToApi::run('',$taskInfo);
        $end_time   = time();
        $spend_time = date('i:s', $end_time - $start_time);
        print_r($spend_time);exit;
//        $this->assertEquals(
//            1,
//            1
//        );
    }

    /**
     * 通过时间段和ip回放日志
     * 注意:命令行中的字符串参数有空格时需要加双引号
     * @param string $type
     * @param string $originStartTime
     * @param string $originEndTime
     * @param string $areaAddress
     * @param string $virtualIpAddress
     * @param int $speed
     * @param string $url http://admin.gc.comingtrue.cn/v1/receive/receive/debug 开发 http://39.102.46.132/v1/receive/receive/debug  线上
     * @return int
     * @example php yii log/add-info funspark "2020-08-19 13:37:42" "2020-08-19 13:37:55" "127.0.0.2" "127.0.0.3" 10
     */
    public function actionAddInfo($type = '5eplay', $startId = 0,
                                  $endId = 0, $beforeIpAddress = '',
                                  $afterIpAddress = '', $url = "http://api.gc.veryapi.com/v1/receive/receive/debug")
    {
        $lg = new LogGenerator();
        $lg->addInfo($type, $startId, $endId, $beforeIpAddress, $afterIpAddress, $url);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionAddFileInfo($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug",$speed=1,$total=100)
    {
        $lg = new LogGenerator();
        $lg->addFileInfo($areaAddress, $file, $url,$speed,$total);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @param int $log_type 1 5e 2 funspark
     * @param int $speed
     * @param int $total
     * @return int
     */
    public function actionAddFileInfoFast($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug", $log_type=1)
    {
        $lg = new LogGenerator();
        $lg->addFileInfoFast($areaAddress, $file, $url,$log_type);
        return ExitCode::OK;//退出代码
    }

    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionLogGeneratorTest($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug")
    {
        $lg = new LogGenerator();
        $lg->addInfo($areaAddress, $file, $url);
        return ExitCode::OK;//退出代码
    }
    /**
     * @param string $areaAddress 虚拟地址(不能和其他ip重复)
     * @param string $file 读取的文件地址
     * @param string $url 推送到的ip地址
     * @return int
     */
    public function actionWorkerman()
    {
//        echo '13';exit;
//        $http_worker = new Worker("websocket://0.0.0.0:2345");
//        // 启动4个进程对外提供服务
//        $http_worker->count = 4;
//
//        // 接收到浏览器发送的数据时回复hello world给浏览器
//        $http_worker->onMessage = function($connection, $data)
//        {
//            // 向浏览器发送hello world
//            $connection->send('hello world');
//        };
//
//        // 运行worker
//       $res = Worker::runAll();
//       print_r($res);
//        return ExitCode::OK;//退出代码
    }

    public function actionHltvRefresh()
    {
//        echo 11;exit;
        $testEventId = 1111162;//
        $aa          = MatchLived::find()->where(['in', 'id', $testEventId])->asArray()->all();
        foreach ($aa as $k => $v) {
            $newList[$k]['id'] = $v['id'];
            $newList[$k]['type'] = $v['type'];
            $newList[$k]['socket_time'] = $v['socket_time'];
            if ($v['type'] == 'no'){
                $newList['match_data'] =json_decode($v['match_data'],true) ;
            }else{
                $newList[key(json_decode($v['match_data'],true)['log'][0])] =current(json_decode($v['match_data'],true)['log'][0]) ;
            }
            $tag['ext2_type'] = 'refresh';
            $v['match_id'] =3063;
            $dd['params']  = json_encode($v);
            $res           = HltvMatchWs::run($tag, $dd);
            print_r($res);
            parent::debug($res);
        }


    }
    public function actionProducer($length)
    {
        if ($length>10000){
            echo '请不要输入大于10000的长度';
        }
        if (!$length){
            echo '请不要输入空值';
        }
        $sql = 'select * from match_data where id= 205630';
        $db_websoket = DbSocket::getDb()->createCommand($sql);
        $dbres = $db_websoket->queryAll();
        $db = $dbres[0]['match_data'];
        $events_data = $db;
        $event_data_ws  = [
            'match_data' => $events_data,
            'type'       => 'events',
            'game_id'    => 1,
            'match_id'   => 1
        ];


        $rk = new \RdKafka\Producer();
        $rk->setLogLevel(LOG_DEBUG);
        //$rk->addBrokers(KafkaProducer::BROKERS_INTERNAL);
        $rk->addBrokers(env('KAFKA_BROKERS_INTERNAL'));
        $topic       = $rk->newTopic(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'));
        $start_time = time();
        for ($i = 0; $i < $length; $i++) {
            $event_data_ws['lg_id']=$i;
            $arr['data'] = $event_data_ws;
            $data        = json_encode($arr, 320);
            $res         = $topic->produce(RD_KAFKA_PARTITION_UA, 0, $data);
            $rk->flush(-1);
        }
        $end_time   = time();
        $spend_time = date('i:s', $end_time - $start_time);
        echo 'TEST 数量 :' . $length.PHP_EOL;
        echo 'complete! spend time :' . $spend_time.PHP_EOL;
    }

    /**
     * kafka消费socket数据(单通道)
     * 多通道
     * ori_pandascore_lol_01 ori_pandascore_lol_02 ori_pandascore_lol_03
     * ori_pandascore_csgo_01 ori_pandascore_csgo_02 ori_pandascore_csgo_03
     * ori_hltv_csgo_01 ori_hltv_csgo_02 ori_hltv_csgo_03
     * ori_bayes_csgo_01 ori_bayes_csgo_02 ori_bayes_csgo_03
     * ori_bayes_lol_01 ori_bayes_lol_02 ori_bayes_lol_03
     * ori_bayes_dota2_01 ori_bayes_dota2_02 ori_bayes_dota2_03
     * ori_orange_lol_01 ori_orange_lol_02 ori_orange_lol_03
     * @throws BusinessException
     */
    public function actionCustomer()
    {
        $groupId = env('KAFKA_GROUP_ID');//'consumer-group3';
        $topic = env('KAFKA_TOPIC_TEST');
        //$broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreLol1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_lol_01';
        //$broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }



    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreLol2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_lol_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }


    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreLol3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_lol_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }



    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreCsgo1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_csgo_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreCsgo2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_csgo_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreCsgo3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_csgo_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreDota1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_dota_01';
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreDota2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_dota_02';
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionPandascoreDota3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_pandascore_dota_03';
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionHltvCsgo1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_hltv_csgo_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionHltvCsgo2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_hltv_csgo_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionHltvCsgo3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_hltv_csgo_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesCsgo1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_csgo_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesCsgo2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_csgo_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesCsgo3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_csgo_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesLol1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_lol_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesLol2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_lol_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesLol3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_lol_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesDota1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_dota2_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesDota2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_dota2_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionBayesDota3()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_bayes_dota2_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionOrangeLol1()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_orange_lol_01';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionOrangeLol2()
    {
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_orange_lol_02';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }
    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionOrangeLol3()
    {
//        KafkaCustomer::test1();
        $groupId = env('KAFKA_GROUP_ID');
        $topic = 'ori_orange_lol_03';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_INTERNAL');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    /**
     * kafka消费socket数据(单通道)
     * @throws BusinessException
     */
    public function actionCustomer1()
    {
        KafkaCustomer::test1();
//        $groupId = env('KAFKA_GROUP_ID');
//        $topic = 'sparkstreamingsql_test';
//        $broken = KafkaProducer::BROKERS_INTERNAL;
//        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

}