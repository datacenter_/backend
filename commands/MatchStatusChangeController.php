<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\common\models\EnumApiSpeed;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\match\models\Match;
use app\modules\match\models\MatchComingSocketOrder;
use app\modules\match\models\MatchErrorManyTeam;
use app\modules\match\models\MatchSocketList;
use app\modules\match\services\MatchService;
use app\modules\task\models\BayesCoverages;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\task\models\StandardDataMatch;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class MatchStatusChangeController extends CommandsBase
{
    /**
     * 刷新是否有视频直播
     */
    public function actionMatchHaveVideo()
    {
        try {
        $matchStreamList = MatchRealTimeInfo::find()->alias('match_real')
            ->select(["match_real.id as match_id","count(*) as video_count"])
            ->innerJoin('match_stream_list as msl','match_real.id = msl.match_id')
            ->where(['or',
                    ['=','match_real.status',1],
                    ['=','match_real.status',2],
                ])->groupBy('msl.match_id')->asArray()->all();
        foreach ($matchStreamList as $value){
            MatchService::setMatchRealData($value['match_id'], ['is_streams_supported'=> 1,'video_count'=>(int)$value['video_count']], Consts::USER_TYPE_ROBOT, 0, 0, 0, 0);
        }
        return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }

    /**
     * 比赛是否有详情跟实时数据，api速率
     */
    public function actionMatchHaveDetail()
    {
        try {
            $matchStreamList = MatchRealTimeInfo::find()
                ->alias('mr')->select(['m.id'])
                ->leftJoin('match as m', 'mr.id=m.id')
                ->andWhere(['=','m.deleted' ,2])
                ->andWhere(['or',
                    ['=', 'mr.status', 1],
                    ['=', 'mr.status', 2],
                ])->asArray()->all();
        foreach ($matchStreamList as $value){
            $info= DataResourceUpdateConfig::find()->where(['resource_id'=>$value['id']])->andWhere(['resource_type'=>'match'])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
            $origin_id = $info['origin_id'];
            $update_type = $info['update_type'];
            if(isset($matchRealModel)){
                $matchRealModel=null;
            }
            //log类型的数据
            if($origin_id == 5){
                if($update_type == 3){
                    $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                    if($matchRealModel){
                        $realArray = [
                            'is_battle_detailed'=>1,
                            'is_pbpdata_supported'=>1
                        ];
//                        $matchRealModel->setAttribute('is_battle_detailed',1);
//                        $matchRealModel->setAttribute('is_pbpdata_supported',1);
//                        $matchRealModel->save();
                        MatchService::setMatchRealData($matchRealModel['id'],$realArray,Consts::USER_TYPE_ROBOT,0);
                        if(isset($matchRealModel) && $matchRealModel['is_pbpdata_supported'] == 2){
                            $realArray1 = [
                                'data_level'=>'unsupported'
                            ];
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray1,Consts::USER_TYPE_ROBOT,0);
//                            $matchRealModel->setAttribute('data_level','unsupported');
//                            $matchRealModel->save();
                        }elseif(isset($matchRealModel) && $matchRealModel['is_pbpdata_supported'] == 1){
                            $enumApiSpeed = EnumApiSpeed::find()->where(['origin_id'=>$info['origin_id']])->andWhere(['game_id'=>$info['game_id']])->asArray()->one();
                            if($enumApiSpeed){
                                if($matchRealModel['data_speed'] != 0){
                                    $realArray2 = [
                                        'data_level'=> $enumApiSpeed['data_level']
                                    ];
//                                    $matchRealModel->setAttribute('data_level',$enumApiSpeed['data_level']);
                                }else{
                                    $realArray2 = [
                                        'data_level'=> $enumApiSpeed['data_level'],
                                        'data_speed'=> $enumApiSpeed['data_speed']
                                    ];
//                                    $matchRealModel->setAttribute('data_level',$enumApiSpeed['data_level']);
//                                    $matchRealModel->setAttribute('data_speed',$enumApiSpeed['data_speed']);
                                }
                                MatchService::setMatchRealData($matchRealModel['id'],$realArray2,Consts::USER_TYPE_ROBOT,0);
//                                $matchRealModel->save();
                            }
                        }
                    }
                }
            }else{  //非log，有绑定关系的数据
                $result = \app\modules\task\models\StandardDataMatch::find()->alias('std')->select(["std.id","std.rel_identity_id","std.is_battle_detailed","std.is_pbpdata_supported"])
                    ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
                    ->where(['dsmr.resource_type'=>\app\modules\common\services\Consts::RESOURCE_TYPE_MATCH])
                    ->andWhere(['dsmr.master_id'=>$value['id']])
                    ->andWhere(['std.deleted'=>2])
                    ->andWhere(['std.origin_id'=>$origin_id])->asArray()->one();
                if($result && $update_type == 3){
                    $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                    if($result['is_battle_detailed'] == 1){
//                        $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                        if($matchRealModel){
                            $realArray =[
                                'is_battle_detailed'=>1
                            ];
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray,Consts::USER_TYPE_ROBOT,0);
                        }
                    }
                    if($result['is_battle_detailed'] == 2){
//                        $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                        if($matchRealModel) {
                            $realArray1 =[
                                'is_battle_detailed'=>2
                            ];
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray1,Consts::USER_TYPE_ROBOT,0);
                        }
                    }
                    if($result['is_pbpdata_supported'] == 1){
//                        $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                        if($matchRealModel) {
                            $realArray2 =[
                                'is_pbpdata_supported'=>1
                            ];
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray2,Consts::USER_TYPE_ROBOT,0);
                        }
                        $auto_add_sub_type = null;
                        //查询录入方式
                        if ($origin_id == 9 || $origin_id == 10){
                            $BayesCoveragesInfo = BayesCoverages::find()->select('types')->where(['rel_identity_id'=> $result['rel_identity_id']])->andWhere(['provider'=>'bayes'])->asArray()->one();
                            if (@$BayesCoveragesInfo['types']){
                                if (strstr($BayesCoveragesInfo['types'],'live_data_riot')){
                                    $auto_add_sub_type = 'live_data_riot';
                                }else{
                                    $auto_add_sub_type_list = explode(',',$BayesCoveragesInfo['types']);
                                    $auto_add_sub_type = $auto_add_sub_type_list[0];
                                }
                            }
                        }
                        //更新为 HotDataRunningMatch auto_add_type 为 socket
                        $match_hot_running_Model = HotDataRunningMatch::find()->where(["match_id"=>$value['id']])->one();
                        if($match_hot_running_Model) {
                            $match_hot_running_Model->setAttributes([
                                'auto_add_type' => 'socket',
                                'auto_add_sub_type' => $auto_add_sub_type
                            ]);
                            $match_hot_running_Model->save();
                        }
                    }
                    if($result['is_pbpdata_supported'] == 2){
                        $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                        if($matchRealModel) {
                            $realArray3 =[
                                'is_pbpdata_supported'=>2
                            ];
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray3,Consts::USER_TYPE_ROBOT,0);
                        }
                        //更新为 HotDataRunningMatch auto_add_type 为 socket
                        $match_hot_running_Model = HotDataRunningMatch::find()->where(["match_id"=>$value['id']])->one();
                        if($match_hot_running_Model) {
                            $match_hot_running_Model->setAttributes(['auto_add_type' => 'restapi']);
                            $match_hot_running_Model->save();
                        }
                    }
                    //api速率
                    if(isset($matchRealModel) && $matchRealModel['is_pbpdata_supported'] == 2){
                        $realArray4 =[
                            'data_level'=>'unsupported'
                        ];
                        MatchService::setMatchRealData($matchRealModel['id'],$realArray4,Consts::USER_TYPE_ROBOT,0);
                    }elseif(isset($matchRealModel) && $matchRealModel['is_pbpdata_supported'] == 1){
                        $enumApiSpeed = EnumApiSpeed::find()->where(['origin_id'=>$info['origin_id']])->andWhere(['game_id'=>$info['game_id']])->asArray()->one();
                        if($enumApiSpeed){
                            if($matchRealModel['data_speed'] != 0){
                                $realArray5 =[
                                    'data_level'=>$enumApiSpeed['data_level']
                                ];
                            }else{
                                $realArray5 =[
                                    'data_level'=>$enumApiSpeed['data_level'],
                                    'data_speed' =>$enumApiSpeed['data_speed']
                                ];
                            }
                            MatchService::setMatchRealData($matchRealModel['id'],$realArray5,Consts::USER_TYPE_ROBOT,0);
                        }
                    }
                }elseif(!$result){ //没有绑定关系
                    $matchRealModel = MatchRealTimeInfo::find()->where(['id'=>$value['id']])->one();
                    if($matchRealModel) {
                        $realArray =[
                            'is_battle_detailed'=>2,
                            'is_pbpdata_supported' =>2,
                            'data_level' =>'manual',

                        ];
                        MatchService::setMatchRealData($matchRealModel['id'],$realArray,Consts::USER_TYPE_ROBOT,0);
                    }
                }
            }
        }
        return ExitCode::OK;

        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }

    /**
     * 有socket的比赛存入match_coming_scoket表
     */
    public function actionMatchSocket()
    {
        try {
        $dateTimeStart =  date('Y-m-d H:i:s',strtotime('+ 1month'));
        $dateTimeEnd =  date('Y-m-d H:i:s',strtotime('- 1day'));
        $transInfo = StandardDataMatch::find()->where(['and',
            ['=','is_pbpdata_supported',1],
            ['>=','scheduled_begin_at',$dateTimeEnd],
            ['<=','scheduled_begin_at',$dateTimeStart],
        ])->asArray()->all();
        foreach ($transInfo as $value){
            $info['begin_at'] = $value['scheduled_begin_at'];  //计划开始时间
            $info['end_at'] = $value['end_at'];
            $info['status'] = $value['status'];
            $standardInfo = MatchSocketList::find()->where([
                'origin_id' => $value['origin_id'],
                'match_id' => $value['rel_identity_id'],
                'game_id' => $value['game_id'],
            ])->one();
            if($value['origin_id'] == 4){
                $number_of_games = $value['number_of_games'];
                if( $number_of_games < 3){
                    $number_of_games =3;
                }
                for ($i=1;$i<=$number_of_games;$i++){
                    $order = MatchComingSocketOrder::find()->where(['and',
                        ['=','match_id',$value['rel_identity_id']],
                        ['=','order', $i],
                    ])->one();
                    if(!$order){
                        $order = new MatchComingSocketOrder();
                        $order->setAttributes([
                            'match_id'=>$value['rel_identity_id'],
                            'order'=>$i,
                        ]);
                        $order->save();
                    }
                }
            }
            if (!$standardInfo) {
                $standardInfo = new MatchSocketList();
                $standardInfo->setAttributes(
                    [
                        'origin_id' => $value['origin_id'],
                        'match_id' => $value['rel_identity_id'],
                        'game_id' => $value['game_id'],
                    ]
                );
            }else{
                if (@$value['deleted'] == 1){
                    $standardInfo->delete();
                    continue;
                }
                $standardInfo->setAttributes($info);
            }
            if (!$standardInfo->save()) {
                throw new BusinessException($standardInfo->getErrors(), '保存失败');
            }
        }
        return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }


    public static function actionMatchByErrorTeam() {
        try {
            $sql = "select mc.id as match_id
     ,count(1) as ct
     ,count(distinct(sdta_rel.master_id)) as d_team_1_id_ct
     ,count(distinct(sdtb_rel.master_id)) as d_team_2_id_ct
     ,count(distinct(greatest(sdtb_rel.master_id, sdta_rel.master_id))) as d_team_max_id_ct
     ,count(distinct(least(sdtb_rel.master_id, sdta_rel.master_id))) as d_team_min_id_ct
     ,group_concat(sdta_rel.master_id) as gc_team_1_id
     ,group_concat(sdtb_rel.master_id) as gc_team_b_id
     ,smc.origin_id
     ,smc.rel_identity_id
     ,sdta_rel.master_id as team_1_id
     ,sdtb_rel.master_id as team_2_id
from `match` mc
         left join data_standard_master_relation rel
                   on mc.id = rel.master_id and rel.resource_type = 'match'
         left join standard_data_match smc on smc.id = rel.standard_id
         left join match_real_time_info mrti on mc.id = mrti.id
         left join standard_data_team sdta
                   on smc.team_1_id = sdta.rel_identity_id and smc.origin_id = sdta.origin_id
         left join data_standard_master_relation sdta_rel
                   on sdta.id=sdta_rel.standard_id and sdta_rel.resource_type='team'
         left join standard_data_team sdtb
                   on smc.team_2_id = sdtb.rel_identity_id and smc.origin_id = sdtb.origin_id
         left join data_standard_master_relation sdtb_rel
                   on sdtb.id=sdtb_rel.standard_id and sdtb_rel.resource_type='team'
where mrti.status in (1,2,4,5)
  and smc.id is not null
  and mc.deleted = 2 
group by mc.id
having ct>1
   and (d_team_max_id_ct>1 or d_team_min_id_ct>1);";
            $connection = \Yii::$app->db;
            $command = $connection->createCommand($sql);
            $data = $command->queryAll();
            if (!empty($data)) {
                $matchIds = array_column($data, 'match_id', 'match_id');
                $dataKey = array_column($data, null, 'match_id');
                $matchCore = Match::find()->alias("m")
                    ->select(['m.id', 'm.scheduled_begin_at', 't.status', 'g.image as game_image','m.team_1_id','m.team_2_id'])
                    ->leftJoin('match_real_time_info as t', "t.id=m.id")
                    ->leftJoin('enum_game as g', "g.id=m.game")
                    ->where(["m.id" => $matchIds])->asArray()->all();
                $newDatas = [];
                $diffRecoveryByError = [];




                foreach ($matchCore as $key => $val) {
//
                    $relData = DataStandardMasterRelation::find()->where(['resource_type'=>'match','master_id'=>$val['id']])->asArray()->all();
                    if (!empty($relData)) {
                        $stanIds = array_column($relData,'standard_id');
                        $stanData = StandardDataMatch::find()->select(['id','rel_identity_id','origin_id','team_1_id','team_2_id'])->where(['id'=>$stanIds])->asArray()->all();
                       $newteam = [];
                        $origin_rel_identity_id = [];
                        foreach ($stanData as $stanK => $stanV) {
                            $team1Id = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$stanV['team_1_id'],$stanV['origin_id']);
                            $team2Id = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$stanV['team_2_id'],$stanV['origin_id']);

                            $newteam[$stanV['origin_id']]['team_1_id'] = $team1Id;
                            $newteam[$stanV['origin_id']]['team_2_id'] = $team2Id;
                            $origin_rel_identity_id[$stanV['origin_id']."_".$team1Id] = $stanV['team_1_id'];
                            $origin_rel_identity_id[$stanV['origin_id']."_".$team2Id] = $stanV['team_2_id'];
                       }
//                        $newteamId = [];
//                        foreach ($newteam as $teamK=> $teamV) {
//                            $newteamId[$teamK]['team_1_rel_identity_id'] = $teamV['team_1_id'];
//                            $newteamId[$teamK]['team_2_rel_identity_id'] = $teamV['team_2_id'];
//                            $newteamId[$teamK]['team_1_id'] = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$teamV['team_1_id'],$teamK);
//                            $newteamId[$teamK]['team_2_id'] = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$teamV['team_2_id'],$teamK);
//
//                        }

                    }

                    $temsdatas = [];
                    foreach ($newteam as $teamIdsK => $teamIdsV) {
                        $newTeams = array_values($teamIdsV);
                        $temsdata = \app\modules\org\models\Team::find()->alias('t')
                            ->select(['t.id as tems_id', 't.name as tems_name', 't.image as tems_image','enum_origin.image as origin_image','enum_origin.id as origin_id'])
                            ->leftJoin('enum_origin',"enum_origin.id = {$teamIdsK}")
                            ->where(['t.id' => $newTeams])
                            ->asArray()->all();
                        foreach ($temsdata as $temsdataK => $temsdataV) {
                            $temsdatas[$temsdataV['origin_id'].'_'.$temsdataV['tems_id']] = $temsdataV;
                        }

                    }

                    foreach ($temsdatas as $temsdatasK => &$temsdatasV) {
                        $temsdatasV['rel_identity_id'] = $origin_rel_identity_id[$temsdatasK];
                    }





//                    foreach ($temsdata as $keyTeam => $valTeam) {
                        $newData['match_id'] = $val['id'];
                        $newData['scheduled_begin_at'] = $val['scheduled_begin_at'];
                        $newData['status'] = $val['status'];
                        $newData['game_image'] = $val['game_image'];
                        $newData['tems_info'] = json_encode(array_values($temsdatas),JSON_UNESCAPED_UNICODE);
                        $newDatas[] = $newData;
//                        $diffRecoveryByError[$val['id']][] = $valTeam['id'];

                    }
//                }

                //1、把错误数据存到错误表里面
                foreach ($newDatas as $keys => $valS) {
                    $errorMatch = MatchErrorManyTeam::find()->where(['match_id' => $valS['match_id']])->one();
                    $old_allData = [];
                    if ($errorMatch){
                        $old_allData = $errorMatch->toArray();

                    }

                    $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
                    if (!$errorMatch) {
                        $errorMatch = new MatchErrorManyTeam();
                        $operationType = OperationLogService::OPERATION_TYPE_ADD;
                    }
                    $valS['data_status'] = 1;
                    $errorMatch->setAttributes($valS);
                    if (!$errorMatch->save()) {
                        throw new BusinessException($errorMatch->getErrors(), '保存错误');
                    }

                    $new_allData = $errorMatch->toArray();

                    $filter = array_keys($new_allData);
                    $filter = array_diff($filter, ["created_at", "modified_at"]);
                    $diffInfo = Common::getDiffInfo($old_allData, $new_allData, $filter);
                    if ($diffInfo['changed']) {
                        OperationLogService::addLog($operationType,
                            Consts::RESOURCE_TYPE_ERROR_MANY_TEAM,
                            $new_allData['id'],
                            ["diff" => $diffInfo["diff"], "new" => $new_allData],
                            0,
                            Consts::RESOURCE_TYPE_ERROR_MANY_TEAM,
                            null,
                            0,
                            Consts::USER_TYPE_ROBOT,
                            OperationLogService::getLoginUserId()
                        );
                    }
                }

                //2、把错误表里数据全取出来
                //3、错误表里某条数据不再这次获取的错误数据里面 就证明这条数据已修复
                $allData = MatchErrorManyTeam::find()->where([])->asArray()->all();
                $allDataId = array_column($allData,'match_id');
                $diff = array_diff($allDataId,$matchIds);
//                $oldids = [];
//                foreach ($allData as $keyDaty => $valDaty) {
//                    if (!in_array($valDaty['tems_id'], $diffRecoveryByError[$valDaty['match_id']])) {
//                        $oldids[] = $valDaty['id'];
//                    }
//
//                }

                //4.修改data_status为2（已修复）
                if (!empty($diff)) {
                    $paramData['data_status'] = 2;
                    MatchErrorManyTeam::updateAll($paramData, ['match_id' => $diff]);

                }


            }

            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }



}
