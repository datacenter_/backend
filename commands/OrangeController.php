<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * 玩家lol任务
 */
class OrangeController extends CommandsBase
{
    /**
     * 抓取玩家所有数据
     */
    public function actionAllData()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_ORANGE, "", "", ""),
            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params" => [
                "action" => "/lol/event/list",
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }

    /**
     * 抓取todo的数据
     */
    public function actionTodo()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","todo"),
            "type" => \app\modules\task\services\grab\orange\OrangeSchedule::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                "action" => "/lol/schedule/todo",
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
}