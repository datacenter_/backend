<?php


namespace app\commands;


use app\modules\common\services\CommonLogService;
use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\grab\riotgames\RiotAbility;
use app\modules\task\services\grab\riotgames\RiotChampions;
use app\modules\task\services\grab\riotgames\RiotItems;
use app\modules\task\services\grab\riotgames\RiotRunesReforged;
use app\modules\task\services\grab\riotgames\RiotSummoner;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

class OriginGrabController extends CommandsBase
{
    /**
     * 更新拳头lol文件
     * @return int
     */
    public function actionGrab()
    {
        try {
            echo "更新元数据拳头";
            $this->actionRiotTask();
            echo "更新元数据威慑";
            $this->actionValveTask();
            echo "更新元数据Ps";
            $this->actionPsTask();
            echo "执行完成";
            return ExitCode::OK;

        } catch (\Exception $e) {
            CommonLogService::setException($e,__METHOD__,'error');

        }

    }

    //拳头
    public function actionRiotTask()
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", "", '');
        $taskInfo = [
            //拳头
            [ //技能
                "type" => RiotAbility::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_ABILITY),
                "params" => [
                    'action' => 'ability',
                    'params' => [
                    ],

                ],
            ],
            [ //英雄
                "type" => RiotChampions::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_CHAMPION),
                "params" => [
                    'action' => 'champion',
                    'params' => [
                    ],

                ],
            ],
            [ //道具
                "type" => RiotItems::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_ITEM),
                "params" => [
                    'action' => 'item',
                    'params' => [
                    ],

                ],
            ],
            //符文
            [
                "type" => RiotRunesReforged::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_RUNE),
                "params" => [
                    'action' => '',
                    'params' => [
                    ],

                ],
            ],
            //召唤师技能
            [
                "type" => RiotSummoner::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL),
                "params" => [
                    'action' => 'summoner',
                    'params' => [
                    ],
                ],
            ],

        ];

        foreach ($taskInfo as $val) {
            TaskRunner::addTask($val, 1);
        }

    }

    //威慑
    public function actionValveTask()
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", "", '');

        $taskInfo = [
            [
                "tag" => $tag,
                "type" => \app\modules\task\services\grab\valve\ValveDotaHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/IEconDOTA2_570/GetHeroes/v1",
                    "params" => [
                        "language" => "zh,en",
                    ]
                ],
            ],

            [
                "tag" => $tag,
                "type" => \app\modules\task\services\grab\valve\ValueDotaItemList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/IEconDOTA2_570/GetGameItems/v1",
                    "params" => [
                        "language" => "zh,en",
//                    'id' => 1,
                    ]
                ],
            ],


        ];

        foreach ($taskInfo as $val) {
            TaskRunner::addTask($val, 1);
        }


    }

    public function actionPsTask() {

        $startTask = [
            //lol召唤师技能
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/lol/spells",
                    "params" => [
                        "page" => 1,
                        'per_page' => 20
                    ]
                ],
            ],
//            //lol英雄列表
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/lol/champions",
                    "params" => [
                        "page" => 1,
                        'per_page' => 20
                    ]
                ],
            ],
            //lol道具
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/lol/items",
                    "params" => [
                        "page" => 1,
                        'per_page' => 20
                    ]
                ],
            ],
            //csgo地图
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSMapsList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/csgo/maps",  //地图csgo/maps
                    "params" => [
                        "page" => 1,
                        'per_page' => 10
                    ]
                ],
            ],
////            //csgo武器
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/csgo/weapons",  //地图csgo/maps
                    "params" => [
                        "page" => 1,
                        'per_page' => 10
                    ]
                ],
            ],
//            //dota道具
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/items",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota技能天赋
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/abilities",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota英雄
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/heroes",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }

}