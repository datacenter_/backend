<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class PandascoreController extends CommandsBase
{
    /**
     * pandascore元数据全量
     */
    public function actionAllMetadata()
    {
        $allTask = [
            //lol召唤师技能
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL),
                "params" => [
                    "action" => "/lol/spells",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            //lol英雄列表
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_LOL_CHAMPION),
                "params" => [
                    "action" => "/lol/versions/all/champions",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            //lol道具
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_LOL_ITEM),
                "params" => [
                    "action" => "/lol/versions/all/items",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            //csgo地图
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSMapsList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_CSGO_MAP),
                "params" => [
                    "action" => "/csgo/maps",  //地图csgo/maps
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            //csgo武器
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_CSGO_WEAPON),
                "params" => [
                    "action" => "/csgo/weapons",  //地图csgo/maps
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
//            //dota道具
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_DOTA2_ITEM),
                "params" => [
                    "action" => "/dota2/items",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota技能天赋
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE),
                "params" => [
                    "action" => "/dota2/abilities",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota英雄
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_DOTA2_HERO),
                "params" => [
                    "action" => "/dota2/heroes",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
        ];
        foreach ($allTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }
    /**
     * pandascore战队全量
     */
    public function actionAllTeam()  //战队全量
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTeamList::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_TEAM),
            "params" => [
                "action" => "/teams",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    /**
     * pandascore选手全量
     */
    public function actionAllPlayer()  //选手全量
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascorePlayerList::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_PLAYER),
            "params" => [
                "action" => "/players",
                "params" => [
                    "page" => '1',
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    /**
     * pandascore赛事（进行中，即将开始）
     */
    public function actionAllTournament()
    {
        $taskAll = [
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_PANDASCORE, "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_TOURNAMENT),
                "params" => [
                    "action" => "/series/running",
                    "params" => [
                        "page" => '1',
                    ]
                ],
            ],
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_PANDASCORE, "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_TOURNAMENT),
                "params" => [
                    "action" => "/series/upcoming",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
        ];
        foreach ($taskAll as $item) {
            TaskRunner::addTask($item, 1);
        }
    }

    /**
     * @return int
     * pandascore upcoming接口
     */
    public function actionUpcoming()  //俩小时跑一次
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", "","upcoming"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                "action" => "/matches/upcoming",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }
    /**
     * @return int
     * pandascore incidents接口
     */
    public function actionIncidents()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);  //抓取五分钟以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                "action" => "/incidents",
                "params" => [
                    "since" =>$incidentTime,
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     * 保证数据准确的incidents
     */
    public function actionLongIncidents()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60*12);  //抓取半天内的数据
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_PANDASCORE, "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_MATCH,"twelveHour"),
            "params" => [
                "action" => "/incidents",
                "params" => [
                    "since" =>$incidentTime,
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }
}