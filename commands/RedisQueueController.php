<?php


namespace app\commands;


use app\commands\redisQueue\RedisQueueConsumer;
use yii\console\Controller;

class RedisQueueController extends CommandsBase
{
    private function getConfig()
    {
        $redisConfig = [
            'host' => env("REDIS_HOST"),
            'database' => 1+env("REDIS_DATABASE"),
            'port' => env("REDIS_PORT")??6379,
            'auth' => env("REDIS_PASSWORD")
        ];
        return $redisConfig;
    }

    public function actionManager()
    {
        $redisConfig = $this->getConfig();
        $manager = new \Gclibs\Redisqueue\QueueManager($redisConfig);
        $tpl = "php " . dirname(__DIR__) . "/yii redis-queue/runner %s  >> " . dirname(__DIR__) . "/runtime/logs/out.log &";
        $manager->managerRunner($tpl);
    }

    public function actionRunner($key)
    {
        $redisConfig = $this->getConfig();
        $queue = new \Gclibs\Redisqueue\QueueManager($redisConfig);
        $queue->setConsumer(new RedisQueueConsumer());
        $queue->consumerRunner($key);
    }

    public function actionAdd()
    {
        $redisConfig = $this->getConfig();
        $queue = new \Gclibs\Redisqueue\QueueManager($redisConfig);
        $queue->addQueue('hello',['iam'=>"gebilaowang"]);
        $queue->addQueue('hello',['iam'=>"gebilaowang"]);
        $queue->addQueue('hello',['iam'=>"gebilaowang"]);
        $queue->addQueue('hello',['iam'=>"gebilaowang"]);
    }
}