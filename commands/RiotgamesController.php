<?php


namespace app\commands;
use app\modules\common\services\CommonLogService;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\match\services\StreamService;
use app\modules\metadata\services\LolRiotgamesService;
use app\modules\metadata\services\LolService;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;

class RiotgamesController extends CommandsBase
{
    /**
     * 更新拳头lol文件
     */
    public function actionRiotgames(){

        try {
            $service = new LolRiotgamesService();
            $service->run([]);
            return ExitCode::OK;

        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;

        }

    }

}