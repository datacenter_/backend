<?php


namespace app\commands;


use app\modules\common\services\RedisLock;
use yii\console\Controller;
use yii\console\ExitCode;

class RunnerBase extends CommandsBase
{
    public $lockKey;

    public function __destruct()
    {
        $this->unLockAll();
    }

    public function setLock($key)
    {
        if (!RedisLock::addLock($key)) {
            // 执行失败，有进程正在执行
//            throw new \Exception("执行失败，有进程正在执行");
            $error_info = [
                "msg" => '执行失败，有进程正在执行',
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        } else {
            $this->lockKey[] = $key;
        }
    }

    public function unLockAll()
    {
        if ($this->lockKey) {
            foreach ($this->lockKey as $lockKey){
                RedisLock::releaseLock($lockKey);
            }
            $this->lockKey = [];
        }
    }
}