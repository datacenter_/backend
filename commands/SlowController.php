<?php


namespace app\commands;

use app\modules\common\services\CommonLogService;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\match\services\StreamService;
use app\modules\metadata\services\LolRiotgamesService;
use app\modules\metadata\services\LolService;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;


class SlowController extends CommandsBase
{
    /**
     * 抓取视频网站数据
     * 抓取视频源数据添加入库
     */
    public function actionGrabOrigin()
    {
        try {
            StreamService::grabOrigin();
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

    /**
     * 10分钟跑一次
     * 抓取hltv视频源
     */
    public function actionGrabHltvOrigin()
    {
        try {
            StreamService::grabHltvOrigin();
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }

//    /**
//     * 抓取hltvd队伍
//     * @return int
//     */
//    public function actionGrabTeam(){
//        BindingTeamService::grabTeam();
//        return ExitCode::OK;
//
//    }
    /**
     * 更新添加视频列表数据
     */
    public function actionUpSaveUrl()
    {
        try {
            StreamService::upSaveUrl();
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }


    }

    /**
     * 10分钟执行一次
     * 用于数据再带绑定页面 后来根据比赛id刷到已添加
     */
    public function actionOnceAgainUp()
    {
        try {
            StreamService::onceAgainUp();
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }

    }
//    /**
//     *
//     * @return int
//     */
//    public function actionUpVideoUrl(){
//        StreamService::upVideoUrl();
//        return ExitCode::OK;
//
//    }


}