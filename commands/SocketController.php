<?php
namespace app\commands;

use yii\console\ExitCode;

class SocketController extends CommandsBase
{
    /**
     * 刷新socket 最后一条时间
     */
    public function actionChangeTime()
    {
        try {
            $redis = new \Redis();
            $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
            $redis->auth(env('REDIS_PASSWORD'));
            $redis->select(env('REDIS_DATABASE'));
            $nowDay = date('Y-m-d');
            $res = $redis->hGetAll('match_last_socket_time:'.$nowDay);
            if ($res){
                $socket_res = [];
                foreach ($res as $key => $item){
                    $match_socket_info = explode('_',$key);
                    $match_id = $match_socket_info[0];
                    $match_server_id = $match_socket_info[1];
                    $socket_res[$match_id][$match_server_id] = $item;
                }
                //$socket_res 有数据再刷
                if ($socket_res){
                    foreach ($socket_res as $key1 => $item1){
                        $match_s1_time = $item1['s1'];
                        $match_s2_time = $item1['s2'];
                        $match_socket_sql = null;
                        if (!empty($match_s1_time) && !empty($match_s2_time)){
                            $match_socket_sql = "update match_coming_socket set websocket_time1 = '".$match_s1_time."' , websocket_time2 = '".$match_s2_time ."' where match_id = '".$key1."'";
                        }elseif (!empty($match_s1_time) && empty($match_s2_time)){
                            $match_socket_sql = "update match_coming_socket set websocket_time1 = '".$match_s1_time."' where match_id = '".$key1."'";
                        }elseif (empty($match_s1_time) && !empty($match_s2_time)){
                            $match_socket_sql = "update match_coming_socket set websocket_time2 = '".$match_s2_time ."' where match_id = '".$key1."'";
                        }else{
                            continue;
                        }
                        if ($match_socket_sql){
                            print_r('正在刷Rel比赛ID'.$key1);
                            \Yii::$app->db->createCommand($match_socket_sql)->execute();
                        }
                    }
                }
            }
            return ExitCode::OK;
        } catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
}
