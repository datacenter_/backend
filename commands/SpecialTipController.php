<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\data\services\DataTodoService;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * bayes任务
 */
class SpecialTipController extends CommandsBase
{
    /**
     * match_battle_player_dota2表steam_id变化触发的主表任务(赵恒)
     */
    public function actionBattlePlayerDota2SteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerDota2::find()->select(['player_id','steam_id'])->distinct()->where(['not',['steam_id'=>null]])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\org\models\Player::find()->alias('p')->select(['p.id','p.steam_id','p.game_id','dsmr.standard_id as standard_id','std.origin_id'])
                ->innerJoin('data_standard_master_relation as dsmr','p.id = dsmr.master_id')
                ->innerJoin('standard_data_player as std','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['p.id'=>$itemPlayer['player_id']])->asArray()->one();
            if($result) {
                if ($result['steam_id'] != $itemPlayer['steam_id']) {
                    $oldInfo = [
                        'id' => $result['id'],
                        'steam_id' => $result['steam_id']
                    ];
                    $newInfo = [
                        'id' => $itemPlayer['player_id'],
                        'steam_id' => $itemPlayer['steam_id']
                    ];
                    $diffInfo = Common::getDiffInfo($oldInfo, $newInfo);
                    $diff = $diffInfo["diff"];
                    DataTodoService::addToDo(Consts::RESOURCE_TYPE_PLAYER, $result['id'], Consts::TAG_TYPE_CORE_DATA, $diff, $result['standard_id'], null, 2);
                }
            }
        }
    }
    /**
     * match_battle_player_csgo表steam_id变化触发的主表任务(赵恒)
     */
    public function actionBattlePlayerCsgoSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerCsgo::find()->select(['player_id','steam_id'])->distinct()->where(['not',['steam_id'=>null]])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\org\models\Player::find()->alias('p')->select(['p.id','p.steam_id','p.game_id','dsmr.standard_id as standard_id','std.origin_id'])
                ->innerJoin('data_standard_master_relation as dsmr','p.id = dsmr.master_id')
                ->innerJoin('standard_data_player as std','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['p.id'=>$itemPlayer['player_id']])->asArray()->one();
            if($result) {
                if ($result['steam_id'] != $itemPlayer['steam_id']) {
                    $oldInfo = [
                        'id' => $result['id'],
                        'steam_id' => $result['steam_id']
                    ];
                    $newInfo = [
                        'id' => $itemPlayer['player_id'],
                        'steam_id' => $itemPlayer['steam_id']
                    ];
                    $diffInfo = Common::getDiffInfo($oldInfo, $newInfo);
                    $diff = $diffInfo["diff"];
                    DataTodoService::addToDo(Consts::RESOURCE_TYPE_PLAYER, $result['id'], Consts::TAG_TYPE_CORE_DATA, $diff, $result['standard_id'], null, 2);
                }
            }
        }
    }
}