<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\task\services\grab\abios\PlayerList;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\feijing\LOLPropList;
use app\modules\task\services\grab\feijing\LOLTeamList;
use app\modules\task\services\grab\pandascore\DotaPlayerList;
use app\modules\task\services\grab\pandascore\DotaTeamList;
use app\modules\task\services\grab\pandascore\LOLHeroList;
use app\modules\task\services\grab\pandascore\LOLPlayerList;
use app\modules\task\services\grab\pandascore\LOLRuneList;
use app\modules\task\services\grab\pandascore\LOLSkillList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\realTime\abios\AbiosLolGet;
use app\modules\task\services\realTime\RealTimeRecordService;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\tasks\feijing\FeiJingBase;
use app\modules\task\services\tasks\feijing\Start;
use app\modules\task\services\tasks\lanjing\LanJingBase;
use app\modules\task\services\tasks\TaskTypes;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StartController extends CommandsBase
{
    public function actionFirst()
    {
        \app\modules\task\services\grab\pandascore\Start::run();
        return ExitCode::OK;
    }

    public function actionStart()
    {
        \app\modules\task\services\grab\pandascore\Start::run();
//        $startList = [
//            // abios的playerlist
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PlayerList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/players",
//                    "params" => [
//                        "page" => 1,
//                        'with[]' => 'game',
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => TeamList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/teams",
//                    "params" => [
//                        "page" => 1,
//                        'with' => ['game'],
//                    ]
//                ],
//            ],
//            // pandarscore
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLPlayerList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/players",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => DotaPlayerList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/dota2/players",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLTeamList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/teams",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => DotaTeamList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/dota2/teams",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//
//            //
//        ];
////        foreach ($startList as $item) {
////            TaskRunner::addTask($item, 1);
////        }
//
//        $metadata = [
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLHeroList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/champions",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLPropList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/items",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLRuneList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/runes",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 0,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => LOLSkillList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/spells",
//                    "params" => [
//                        "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                        "page" => 0,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//        ];
//
//        foreach ($metadata as $item) {
//            TaskRunner::addTask($item, 1);
//        }
        return ExitCode::OK;
    }

    public function actionSocket()
    {
        \app\modules\task\services\realTime\abios\AbiosSocketCsgo::run('series');
    }

    public function actionGets()
    {
        // 获取进行中列表，看数据变化情况
        $listInfo = AbiosLolGet::getLiveMatchList();
        $logInfo = RealTimeRecordService::setBattleLog(2, 2, 'lol_list', 1, date("YmdHis"), $listInfo);

        $matchInfo230968 = AbiosLolGet::getMatchInfo(230968);
        $logInfo = RealTimeRecordService::setBattleLog(2, 2, 'lol_detail', 230968, date("YmdHis"), $matchInfo230968);

        $matchInfo231425 = AbiosLolGet::getMatchInfo(231425);
        $logInfo = RealTimeRecordService::setBattleLog(2, 2, 'lol_detail', 230968, date("YmdHis"), $matchInfo231425);

        $matchInfo231426 = AbiosLolGet::getMatchInfo(231426);
        $logInfo = RealTimeRecordService::setBattleLog(2, 2, 'lol_detail', 230968, date("YmdHis"), $matchInfo231426);
    }

    /**
     * 弃用的刷新推荐绑定
     */
    public function actionRefresh()
    {
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshTeam();
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMetadata();
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMatch();
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshTournament();
    }

    /**
     * 已绑定，且比赛状态为未开始和进行中的
     * 由于参赛战队存在变更的可能性，所以推荐绑定标红的机制增加一部分已绑定情况也要刷新
     */
    public function actionRefreshBdMatch(){
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshBdMatch();
    }

    /**
     * 刷新推荐绑定
     */
    public function actionRefreshPlayer()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function actionRefreshTeam()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshTeam();
        }catch (\Exception $e) {
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function actionRefreshMetadata()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMetadata();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function actionRefreshMatch()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMatch();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function actionRefreshTournament()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshTournament();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
}