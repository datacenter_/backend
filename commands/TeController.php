<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\task\services\TaskMQConsumer;
use app\modules\task\services\TaskRedisRunner;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\tasks\feijing\FeiJingBase;
use app\modules\task\services\tasks\feijing\Start;
use app\modules\task\services\tasks\lanjing\LanJingBase;
use app\modules\task\services\tasks\TaskTypes;
use MQ\Model\Message;
use WBY\MQ\SDK\MQClientFactory;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TeController extends CommandsBase
{
    public function actionRunner()
    {
        TaskRunner::run(471746);
    }

}