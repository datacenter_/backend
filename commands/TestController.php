<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\models\HelperImageToAli;
use app\modules\common\models\UploadImageLog;
use app\modules\common\services\ImageConversionHelper;
use app\modules\file\services\UploadService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\tournament\models\Tournament;
use app\rest\exceptions\BusinessException;
use yii\console\ExitCode;
use yii\data\Pagination;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class TestController extends CommandsBase
{
    public function actionTeam()  //战队全量
    {
        $masterDatas = Team::find()->where(['and',
            ['<>','image',''],
            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
        ])->all(); //team
        static $i = 0;
        foreach ($masterDatas as $value){
            $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
            if($image['ali_url'] && !$image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($image['ali_url']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $image->setAttributes($insertData);
                $image->save();
            }elseif($image['hk_url']){
                $aliUrl = $image['hk_url'];
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
            }else{
                $aliUrl= self::mvImageToAli($value['image']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
            }
            $value->setAttributes([
                'image'=>$aliUrl
            ]);
            $value->save();
        }
        print_r($i);
    }
    public function actionPlayer()  //选手全量
    {
        $masterDatas = Player::find()->where(['and',
            ['<>','image',''],
            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
        ])->all();
        static $i = 0;
        foreach ($masterDatas as $value){
            $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
            if($image['ali_url'] && !$image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($image['ali_url']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $image->setAttributes($insertData);
                $image->save();
            }elseif($image['hk_url']){
                $aliUrl = $image['hk_url'];
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
            }else{
                $aliUrl= self::mvImageToAli($value['image']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $value['id'].PHP_EOL;
            }
            $value->setAttributes([
                'image'=>$aliUrl
            ]);
            $value->save();
        }
        print_r($i);
    }
    public function actionTournament()  //选手全量
    {
        $masterDatas = Tournament::find()->where(['and',
            ['<>','image','']
        ])->all();
        static $i = 0;
        foreach ($masterDatas as $value){
            $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
            if($image['ali_url'] && !$image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($image['ali_url']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $image->setAttributes($insertData);
                $image->save();
            }elseif($image['hk_url']){
                $aliUrl = $image['hk_url'];
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
            }else{
                $aliUrl= self::mvImageToAli($value['image']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
            }
            $value->setScenario('bing_tournament');
            $value->setAttributes([
                'image'=>$aliUrl
            ]);
            $value->save();
        }
        print_r($i);
    }
    public static function mvImageToAli($url)
    {
        $localPath = \app\modules\common\services\ImageConversionHelper::downfile($url);
        $fileName = basename($localPath);

        $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
        if($ext == "jpg" || $ext == "jpeg"){
            $imageResource = imagecreatefromjpeg($localPath);
            $postion = strrpos($localPath,'.',0);
            $localPath = substr($localPath,0,$postion).'.png';
            $res = imagepng($imageResource,$localPath);
            if($res){
                $ext = "png";
            }else{
                throw new BusinessException($res->getErrors(), '转存png失败');
            }
        }
//        $dst = "elmt/".substr(time(),0,5).'.'.$ext;
        $dst = "els/".base64_encode(date("ymd")).'/'.base64_encode(date("His")).rand(100,999).'.'.$ext;
        $aliOriginUrl = UploadService::upload($dst, $localPath);
        $aliUrl = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
        return $aliUrl;
    }
    public function actionRefreshsn() //战队快照
    {
        for ($i =1;$i<=30000;$i++){
            $q = \app\modules\org\models\TeamSnapshot::find()->where(['and',
                ['<>','image',''],
                ['not like','image','img.elementsdata.cn']
            ]);
            $count = $q->count();
            $page = (empty($i) ? 1 : $i) - 1;
            $pageSize = 500;
            $pages = new Pagination([
                'totalCount' => $count,
                'pageSize' => $pageSize,  // 分页默认条数是20条
                'page' => $page,
            ]);
            $masterDatas = $q->offset($pages->offset)->limit($pages->limit)
                ->asArray()->all();
            if($masterDatas){
                foreach ($masterDatas as $value){
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                            "", "", "", ""),
                        "batch_id" => QueueServer::setBatchId("opc","","team_snapshot","team_snapshot"),
                        "type" =>"team_snapshot",
                        "params" =>$value
                    ];
                    TaskRunner::addTask($item,1);
                }
            }else{
                break;
            }
        }
        return 1;
    }
    public function actionPlayerSnap() //快照
    {
        for ($i =1;$i<=30000;$i++){
            $q = \app\modules\org\models\PlayerSnapshot::find()->where(['and',
                ['<>','image',''],
                ['not like','image','img.elementsdata.cn']
            ]);
            $count = $q->count();
            $page = (empty($i) ? 1 : $i) - 1;
            $pageSize = 500;
            $pages = new Pagination([
                'totalCount' => $count,
                'pageSize' => $pageSize,  // 分页默认条数是20条
                'page' => $page,
            ]);
            $masterDatas = $q->offset($pages->offset)->limit($pages->limit)
                ->asArray()->all();
            if($masterDatas){
                foreach ($masterDatas as $value){
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                            "", "", "", ""),
                        "batch_id" => QueueServer::setBatchId("opc","","player_snapshot","team_snapshot"),
                        "type" =>"player_snapshot",
                        "params" =>$value
                    ];
                    TaskRunner::addTask($item,1);
                }
            }else{
                break;
            }
        }
        return 1;
    }

    /**
     * 测试下载功能
     * @throws BusinessException
     */
    public function actionDownImage()
    {
        $datas = UploadImageLog::find()->limit(1)->orderBy("created_time desc")->asArray()->all();
        foreach ($datas as $data) {
            $url = $data['origin_url'];
            $localPath = self::downfile($url);
            if (!$localPath) {
                $uploadImageLog = new UploadImageLog();
                $uploadImageLog->setAttributes(
                    ['origin_url' => $url],
                    ['info' => '下载失败']
                );
                $uploadImageLog->save();
                throw new BusinessException([], '测试的下载失败');
            }
            $fileName = basename($localPath);

            $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
            if ($ext == "jpg" || $ext == "jpeg") {
                $imageResource = imagecreatefromjpeg($localPath);
                if (!$imageResource) {
                    $imageResource = imagecreatefrompng($localPath);
                }
                if (!$imageResource) {
                    $uploadImageLog = new UploadImageLog();
                    $uploadImageLog->setAttributes(
                        ['origin_url' => $url],
                        ['info' => '测试的图片转换失败']
                    );
                    $uploadImageLog->save();
                    throw new BusinessException([], '测试的图片转换失败');
                }
                $postion = strrpos($localPath, '.', 0);
                $localPath = substr($localPath, 0, $postion) . '.png';
                $res = imagepng($imageResource, $localPath);
                if ($res) {
                    $ext = "png";
                } else {
                    throw new BusinessException([], '测试的转存png失败');
                }
            }
        }
    }
    public static function downfile($httpUrl)
    {
        $url=$httpUrl;
//        $url=implode("/",$urlEncodeArray);
        for($i =0;$i<3;$i++) {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $file = file_get_contents($url, false, stream_context_create($arrContextOptions));
            if($file){
                break;
            }else{
                continue;
            }
        }
        if(!$file){
            $uploadImageLog = new UploadImageLog();
            $uploadImageLog->setAttributes([
                'origin_url'=>$url,
                'info'=>"测试下载资源失败"
            ]);
            $uploadImageLog->save();
            throw new BusinessException([], '测试下载资源失败');
        }
//        $fileArr=explode('.',$httpUrl);
//        $fileExt=end($fileArr);
        $filename=basename($url);
        $pic_local_path = \Yii::$app->getBasePath() . '/public/assets/cache/'.md5(time().substr(md5(uniqid(mt_rand(), true)),1,6));
        $pic_local = $pic_local_path . '/' . $filename;

        if (!file_exists($pic_local_path)) {
//            print_r($pic_local_path);
//            exit;
            $info=mkdir($pic_local_path, 0777,true);
            @chmod($pic_local_path, 0777);
        }

        file_put_contents($pic_local, $file);
        print_r($pic_local);
        return $pic_local;
    }

}