<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\services\logformat\LogGenerator;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class UpComingController extends CommandsBase
{
    /**
     * Hello Test
     * @return int
     */
    public function actionRefresh()  //俩小时跑一次
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh","upcoming"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                "action" => "/matches/upcoming",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }
}