<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class ValveController extends CommandsBase
{
    /**
     * valve元数据全量
     */
    public function actionAllMetadata()
    {
        $allTask = [
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\valve\ValveDotaHeroList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_VALVE,Consts::METADATA_TYPE_DOTA2_HERO),
                "params" => [
                    "action" => "/IEconDOTA2_570/GetHeroes/v1",
                    "params" => [
                        "language"=>"zh,en",
                    ]
                ],
            ],
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\valve\ValueDotaItemList::class,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_VALVE,Consts::METADATA_TYPE_LOL_ITEM),
                "params" => [
                    "action" => "/IEconDOTA2_570/GetGameItems/v1",
                    "params" => [
                        "language"=>"zh,en",
//                    'id' => 1,
                    ]
                ],
            ],
        ];
        foreach ($allTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }


}