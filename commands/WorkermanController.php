<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use app\modules\org\models\Player;
use yii\console\Controller;
use yii\console\ExitCode;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use Workerman\Worker;;
use Workerman\Timer;
use yii\helpers\Console;




/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author WangJinLong
 * @version 1.0
 */
class WorkermanController extends CommandsBase
{
    public $send;
    public $daemon;
    public $gracefully;
    public $match_id;

    // 这里不需要设置，会读取配置文件中的配置
    public $config = [];


    public function options($actionID)
    {
        return ['send', 'daemon', 'gracefully','match_id'];
    }

    public function optionAliases()
    {
        return [
            's' => 'send',
            'd' => 'daemon',
            'g' => 'gracefully',
            'm' => 'match_id',
        ];
    }

    public function actionIndex()
    {

        if ('start' == $this->send) {
            try {
                $this->start($this->daemon);
            } catch (\Exception $e) {
                $this->stderr($e->getMessage() . "\n", Console::FG_RED);
            }
        } else if ('stop' == $this->send) {
            $this->stop();
        } else if ('restart' == $this->send) {
            $this->restart();
        } else if ('reload' == $this->send) {
            $this->reload();
        } else if ('status' == $this->send) {
            $this->status();
        } else if ('connections' == $this->send) {
            $this->connections();
        }
    }

    public function initWorker()
    {
        $ip = isset($this->config['ip']) ? $this->config['ip'] : $this->ip;
        $port = isset($this->config['port']) ? $this->config['port'] : $this->port;
        $worker = new Worker("websocket://{$ip}:{$port}");

        // 1 processes
        $worker->count = 1;

        // Emitted when new connection come
        $worker->onConnect = function ($connection) {
            echo "New connection\n";
//            $redis = new Redis();
//            $wsWorker->redis = $redis;
        };

//        $redis = new Redis();
//        $redis->connect('192.168.4.21',6379);
//        $x = new Player();
        $worker->onWorkerStart = function($worker){
            $match_id = $this->match_id;
//            global $redis;
//            $redis = \Yii::$app->redis;
////                $events=$redis->lrange('ws:508:history:events',0,1);
//            $frames=$redis->lrange('ws:508:history:frames',0,1);
//            $frames=$redis->get('c');
//            print_r($frames);exit;
//            $redis = new Redis();
//            $player = Player::find()->asArray()->one();
            if (!$match_id){
                return false;
            }
            Timer::add(2, function()use($worker,$match_id){

                $redis = \Yii::$app->redis;
//                $events=$redis->lrange('ws:508:history:events',0,1);
                $frames=$redis->lrange('ws:'.$match_id.':history:frames',0,1);
                $events=$redis->lrange('ws:'.$match_id.':history:events',0,1);
                if (empty($frames)||empty($events)){
                    return false;
                }
                $res['frames'] = json_decode($frames[0],true);
                $res['events'] = json_decode($events[0],true);
                $res_json = json_encode($res);
                foreach($worker->connections as $connection) {


//                    $connection->send('3');
//                    $connection->send(date('H:i:s'));
//                    $connection->send(json_encode($player));
                    $connection->send($res_json);
                }
            });
//            $match_id = $this->match_id;
//            global $redis;
//            $redis = \Yii::$app->redis;
////                $events=$redis->lrange('ws:508:history:events',0,1);
//            $frames=$redis->lrange('ws:508:history:frames',0,1);
//            $frames=$redis->get('c');
//            print_r($frames);exit;
//            $redis = new Redis();
//            $player = Player::find()->asArray()->one();
//            Timer::add(1, function()use($wsWorker,$match_id){
//                $redis = \Yii::$app->redis;
////                $events=$redis->lrange('ws:508:history:events',0,1);
//                $frames=$redis->lrange('ws:'.$match_id.':history:frames',0,1);
//                $events=$redis->lrange('ws:'.$match_id.':history:events',0,1);
//
//                $data['frames'] = json_decode($frames[0],true);
//                $data['events'] = json_decode($events[0],true);
//                $data_json = json_encode($data);
//                foreach($wsWorker->connections as $connection) {
//
//
////                    $connection->send('3');
////                    $connection->send(date('H:i:s'));
////                    $connection->send(json_encode($player));
////                    $connection->send($data_json);
//                }
//            });
        };

        // Emitted when data received
        $worker->onMessage = function ($connection, $data)use($worker) {
//            global $worker;
//            // 判断当前客户端是否已经验证,即是否设置了uid
//            if(!isset($connection->uid))
//            {
//                // 没验证的话把第一个包当做uid（这里为了方便演示，没做真正的验证）
//                $connection->uid = $data;
//                /* 保存uid到connection的映射，这样可以方便的通过uid查找connection，
//                 * 实现针对特定uid推送数据
//                 */
//                $worker->uidConnections[$connection->uid] = $connection;
//                return $connection->send('login success, your uid is ' . $connection->uid);
//            }
//            // 其它逻辑，针对某个uid发送 或者 全局广播
//            // 假设消息格式为 uid:message 时是对 uid 发送 message
//            // uid 为 all 时是全局广播
//            list($recv_uid, $message) = explode(':', $data);
//            // 全局广播
//            if($recv_uid == 'all')
//            {
//                broadcast($message);
//            }
//            // 给特定uid发送
//            else
//            {
//                sendMessageByUid($recv_uid, $message);
//            }

            // Send hello $data
//            $connection->send('dddd hello ' . $data);
//            $data;

        };
        // 当有客户端连接断开时
        $worker->onClose = function($connection)
        {
            global $worker;
            if(isset($connection->uid))
            {
                echo "Connection closed\n";
                // 连接断开时删除映射
                unset($worker->uidConnections[$connection->uid]);
            }
        };

// 向所有验证的用户推送数据
        function broadcast($message)
        {
            global $worker;
            foreach($worker->uidConnections as $connection)
            {
                $connection->send($message);
            }
        }

// 针对uid推送数据
        function sendMessageByUid($uid, $message)
        {
            global $worker;
            if(isset($worker->uidConnections[$uid]))
            {
                $connection = $worker->uidConnections[$uid];
                $connection->send($message);
            }
        }


    }

    /**
     * workman websocket start
     */
    public function start()
    {

        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;

        $argv[0] = $argv[1];
        $argv[1] = 'start';
        if ($this->daemon) {
            $argv[2] = '-d';
        }

        // Run worker
        Worker::runAll();
    }

    /**
     * workman websocket restart
     */
    public function restart()
    {
        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'restart';
        if ($this->daemon) {
            $argv[2] = '-d';
        }

        if ($this->gracefully) {
            $argv[2] = '-g';
        }

        // Run worker
        Worker::runAll();
    }

    /**
     * workman websocket stop
     */
    public function stop()
    {
        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'stop';
        if ($this->gracefully) {
            $argv[2] = '-g';
        }

        // Run worker
        Worker::runAll();
    }

    /**
     * workman websocket reload
     */
    public function reload()
    {
        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'reload';
        if ($this->gracefully) {
            $argv[2] = '-g';
        }

        // Run worker
        Worker::runAll();
    }

    /**
     * workman websocket status
     */
    public function status()
    {
        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'status';
        if ($this->daemon) {
            $argv[2] = '-d';
        }

        // Run worker
        Worker::runAll();
    }

    /**
     * workman websocket connections
     */
    public function connections()
    {
        $this->initWorker();
        // 重置参数以匹配Worker
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'connections';

        // Run worker
        Worker::runAll();
    }
}