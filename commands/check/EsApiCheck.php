<?php


namespace app\commands\check;


use app\modules\match\models\Battle;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\GetApi\BattleService;
use app\modules\task\services\api\GetApi\MatchService;
use app\modules\task\services\api\GetApi\PlayerService;
use app\modules\task\services\api\GetApi\TeamService;
use app\modules\task\services\api\GetApi\TournamentService;
use app\modules\task\services\Common;
use app\modules\tournament\models\Tournament;

class EsApiCheck
{
    public static function team() {
        $onlineType = env("CONFIG_TYPE") ?? null;
        if ($onlineType != 'hk_Online'){
            return [];
        }

        $team = Team::find()->select([])->where([])->orderBy('`modified_at` DESC')->limit(5)->asArray()->all();
        $newsuccess = [];
        foreach ($team as $key => $val) {
            $mqTeam = TeamService::getTeamDetail($val['id']);

            $paramEs['team_id'] = $val['id'];
            $paramEs['token'] = env("ES_TOKEN");
            $esTeamHK = self::requestGet(env("ES_HK_TEAM"),[],$paramEs);
            $esTeamData = json_decode($esTeamHK, true);
            $success = self::checkParams($mqTeam,$esTeamData,$val['id'],'香港','team');

            $paramSgp['team_id'] = $val['id'];
            $paramSgp['token'] = env("ES_TOKEN");
            $esTeamSgp = self::requestGet(env("ES_SGP_TEAM"),[],$paramSgp);

            $esTeamDataSgp = json_decode($esTeamSgp, true);

            $successSgp = self::checkParams($mqTeam,$esTeamDataSgp,$val['id'],'新加坡','team');
            if ($successSgp) {
                $newsuccess[] = $successSgp;

            }


            if ($success) {
                $newsuccess[] = $success;

            }
        }

        $num = count($newsuccess);

        if ($newsuccess && $num == 10) {

            $message1 = implode(',', $newsuccess);
            $info = json_encode($message1, 320);

            $params1 = [
                'type' => 'CheckEsApi战队',
                'tag' => $onlineType . ' ' . 'CheckEsApi战队输出',
                'info' => $info,
            ];

            Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params1);
        } else {
            echo "\n战队没有错误数据\n";
        }

    }

    public static function player()
    {
        $onlineType = env("CONFIG_TYPE") ?? null;

        if ($onlineType != 'hk_Online'){
            return [];
        }
        $player = Player::find()->select([])->orderBy('`modified_at` DESC')->limit(5)->asArray()->all();
        $newsuccess = [];
        foreach ($player as $key => $val) {
            $mqPlayer = PlayerService::getPlayerDetail($val['id']);
            $paramEsHk['player_id'] = $val['id'];
            $paramEsHk['token'] = env("ES_TOKEN");
            $esPlayerHK = self::requestGet(env("ES_HK_PLAYER"),[],$paramEsHk);

            $esPlayerData = json_decode($esPlayerHK, true);
            $success = self::checkParams($mqPlayer, $esPlayerData, $val['id'], '香港', 'player');

            $paramEsSgp['player_id'] = $val['id'];
            $paramEsSgp['token'] = env("ES_TOKEN");
            $esPlayerSgp = self::requestGet(env("ES_SGP_PLAYER"),[],$paramEsSgp);

            $esPlayerSgpData = json_decode($esPlayerSgp, true);
            $successSgp = self::checkParams($mqPlayer, $esPlayerSgpData, $val['id'], '新加坡', 'player');
            if ($successSgp) {
                $newsuccess[] = $successSgp;

            }
            if ($success) {
                $newsuccess[] = $success;

            }

        }

        $num = count($newsuccess);

        if ($newsuccess && $num == 10) {

            $message1 = implode(',', $newsuccess);
            $info = json_encode($message1, 320);

            $params1 = [
                'type' => 'CheckEsApi选手',
                'tag' => $onlineType . ' ' . 'CheckEsApi选手输出',
                'info' => $info,
            ];

            Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params1);
        } else {
            echo "\n选手没有错误数据\n";
        }

    }


    public static function checkParams($mqTeam, $esTeamData, $teamId, $type = null, $origin = null)
    {
        if (empty($mqTeam[0])) {
            return $origin . '_' . $teamId . "_ 数据库输出没有数据";
        }
        if ($type == '新加坡') {
            \Yii::$app->params['refresh_key'] = "Singapore";
            $mqTeam[0] = ApiBase::convertNull($mqTeam[0]);
        }
        if ($esTeamData['code'] != '200' && $esTeamData['message'] != 'successful operation'){
            return json_encode($esTeamData,320)."{$type}EsApi_{$origin}_{$teamId}_链接失败";
        }

        if ($mqTeam[0] && empty($esTeamData['data'])) {
            return $origin . '_' . $teamId . "_ {$type}EsApi_输出没有数据";
        }

        if ($origin == 'tournament') {

            if (!empty($mqTeam[0]['teams'])) {
                $mqTeamS = Common::sort_for_arrays($mqTeam[0]['teams'],"team_id");
                $mqTeam[0]['teams'] = $mqTeamS;
            }

            if (!empty($esTeamData['data']['teams'])) {
                $EsTeamS = Common::sort_for_arrays($esTeamData['data']['teams'],"team_id");
                $esTeamData['data']['teams'] = $EsTeamS;
            }


        }




        if ($origin == 'match') {
            unset($mqTeam[0]['delay']);
            unset($mqTeam[0]['tournament']['is_private']);
            unset($mqTeam[0]['tournament']['is_private_delay']);
            unset($mqTeam[0]['tournament']['private_delay_seconds']);
            unset($mqTeam[0]['tournament']['public_delay']);
            unset($mqTeam[0]['tournament']['public_data_devel']);

        }
        if (isset($mqTeam[0]['deleted'])) {
            unset($mqTeam[0]['deleted']);
        }
        $mqData = self::dg($mqTeam[0], $type, $origin);
        $esData = self::dg($esTeamData['data']);

        $maTeamJson = json_encode($mqData, 320);
        $esTeamJson = json_encode($esData, 320);


        $maTeamMd5 = md5($maTeamJson);
        $esTeamMd5 = md5($esTeamJson);

        if ($maTeamMd5 != $esTeamMd5) {
            if ($origin == 'match'){
                $diff = Common::getDiffInfo($mqData, $esData);
                $errInfo = json_encode($diff['diff']);
                return $origin . "比赛状态_{$mqData['status']}id_" . $teamId . "_{$type}EsApi与数据库输出数据不相等diff:".$errInfo;
            }
            return $origin . 'id_' . $teamId . "_{$type}EsApi与数据库输出数据不相等";
        }

        return null;
    }


    public static function dg($array, $type = null, $origin = null, &$res = [])
    {
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                $res[$key] = self::dg($item, $type, $origin);

            } else {
                if ($key == 'modified_at') {
                    unset($key);
                } else {
                    if ($origin == 'match') {
                        if (isset($array['pbpdata_type']) && $key == 'url' && strpos($item, '/pbpdata/') !== false) {
                            if ($type == '香港') {
                                $item = 'wss://live.elementsdata.cn/v1' . $item;
                            }
                            if ($type == '新加坡') {
                                $item = 'wss://live.elements-data.com/v1' . $item;
                            }
                        }
                    }
                    $res[$key] = (string)$item;
                }
            }
        }
        return $res;
    }

    public static function match()
    {
        $onlineType = env("CONFIG_TYPE") ?? null;

        if ($onlineType != 'hk_Online'){
            return [];
        }

        $match = Match::find()->select([])->where(['id'=>61440])->orderBy('`modified_at` DESC')->limit(5)->asArray()->all();
        $newsuccess = [];
        foreach ($match as $key => $val) {
            $mqMatch = MatchService::getMatchDetail($val['id'], '');
            $mqMatch = $mqMatch['list'];
            $paramEsHk['match_id'] = $val['id'];
            $paramEsHk['token'] = env("ES_TOKEN");
            $esMatchHK = self::requestGet(env("ES_HK_MATCH"),[],$paramEsHk);
            $esMatchData = json_decode($esMatchHK, true);
            $success = self::checkParams($mqMatch, $esMatchData, $val['id'], '香港', 'match');


            $paramEsSgp['match_id'] = $val['id'];
            $paramEsSgp['token'] = env("ES_TOKEN");
            $esMatchSgp = self::requestGet(env("ES_SGP_MATCH"),[],$paramEsSgp);
            $esMatchSgpData = json_decode($esMatchSgp, true);
            $successSgp = self::checkParams($mqMatch, $esMatchSgpData, $val['id'], '新加坡', 'match');
            if ($successSgp) {
                $newsuccess[] = $successSgp;
            }
            if ($success) {
                $newsuccess[] = $success;

            }

        }
        $num = count($newsuccess);

        if ($newsuccess && $num == 10) {

            $message1 = implode(',', $newsuccess);
            $info = json_encode($message1, 320);

            $params1 = [
                'type' => 'CheckEsApi比赛',
                'tag' => $onlineType . ' ' . 'CheckEsApiMatch输出',
                'info' => $info,
            ];

            $su = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params1);
            return $su;
        } else {
            echo "\n比赛没有错误数据\n";
        }

    }


    public static function tournaments()
    {
        $onlineType = env("CONFIG_TYPE") ?? null;

        if ($onlineType != 'hk_Online'){
            return [];
        }
        $tournament = Tournament::find()->select([])->where([])->orderBy('`modified_at` DESC')->limit(5)->asArray()->all();
        $newsuccess = [];
        foreach ($tournament as $key => $val) {
            $mqtournament = TournamentService::getTournamentDetail($val['id']);
            $paramEsHk['tournament_id'] = $val['id'];
            $paramEsHk['token'] = env("ES_TOKEN");
            $esTournamentHK = self::requestGet(env("ES_HK_TOURNAMENTS"),[],$paramEsHk);

            $esTournamentData = json_decode($esTournamentHK, true);
            $success = self::checkParams($mqtournament, $esTournamentData, $val['id'], '香港', 'tournament');

            $paramEsSgp['tournament_id'] = $val['id'];
            $paramEsSgp['token'] = env("ES_TOKEN");
            $esTournamentSgp = self::requestGet(env("ES_SGP_TOURNAMENTS"),[],$paramEsSgp);
            $esTournamentSgpData = json_decode($esTournamentSgp, true);
            $successSgp = self::checkParams($mqtournament, $esTournamentSgpData, $val['id'], '新加坡', 'tournament');
            if ($successSgp) {
                $newsuccess[] = $successSgp;
            }
            if ($success) {
                $newsuccess[] = $success;
            }

        }

        $num = count($newsuccess);

        if ($newsuccess && $num == 10) {

            $message1 = implode(',', $newsuccess);
            $info = json_encode($message1, 320);

            $params1 = [
                'type' => 'CheckEsApi赛事',
                'tag' => $onlineType . ' ' . 'CheckEsApi赛事输出',
                'info' => $info,
            ];

            $su = Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params1);
            return $su;
        } else {
            echo "\n赛事没有错误数据\n";
        }
    }



    public static function requestGet($getUrl,$header,$params) {
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $urlWithQuery = $getUrl . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $url = env("ES_AGENCY_URL");
        $ch = curl_init();
            $info = [
                'url' => $urlWithQuery,
                'method' => 'get',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}