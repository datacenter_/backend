<?php
/**
 *
 */
namespace app\commands\check;
use app\modules\match\models\Battle;
use app\modules\match\models\MatchBattle;
use app\modules\task\models\MatchPushLog;
use app\modules\task\services\Common;
use yii\db\Query;

class MatchSocketCheck
{
    public static function checkBattlePush()
    {
        // 最近一个小时相关match
        $list=(new Query())->select(['match'])->from('match_battle')
            ->where(['>','created_at',date("Y-m-d H:i:s",time()-3600)])
            ->groupBy('match')
            ->all();
        foreach ($list as $match){
            self::checkMatchBattlePush($match['match']);
        }
    }

    public static function checkMatchBattlePush($matchId)
    {
        $type = env("CONFIG_TYPE") ?? null;
        self::addLog('开始检查match:'.$matchId);
        // 获取match下的battle和对应battle的创建时间
        $battles=MatchBattle::find()->where(['match'=>$matchId])->asArray()->all();
        $battleIds=array_column($battles,'id');
        // 获取对应battle的livelog数据
        $pushLog=MatchPushLog::find()->where(['in','battle_id',$battleIds])->asArray()->all();
        $pushClassify= [];
        foreach($pushLog as $val){
            $pushClassify[$val['battle_id']][$val['event_type']][]=$val;
        }
        foreach($battles as $key=>$val){
            $battleId=$val['id'];
            self::addLog('开始检查battle：'.$battleId);
            if(!isset($pushClassify[$battleId])){

                self::addLog('没有找到对应的push信息（no all）');
                $message1 = "没有找到matchId:{$val['match']} 对应的battleId:{$battleId} push信息（no all）";
                $info1 = json_encode($message1,JSON_UNESCAPED_UNICODE);
                $params1 = [
                    'type' => 'IsBattlePush',
                    'tag' => $type .' '. 'IsBattlePush',
                    'info' => $info1,
                ];

                 Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params1);
                continue;
            }
            if(!isset($pushClassify[$battleId]['battle_creation'])){
                self::addLog('没有找到matchId对应的battleId创建Push信息');
                //$val['match']下面的$battleId创建
                $message2 = "没有找到matchId:{$val['match']} 对应的battleId:{$battleId} 创建Push信息";
                $info2 = json_encode($message2,JSON_UNESCAPED_UNICODE);
                $params2 = [
                    'type' => 'IsBattlePush',
                    'tag' => $type .' '. 'IsBattlePush',
                    'info' => $info2,
                ];
                Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params2);

                continue;
            }
            if(count($pushClassify[$battleId]['battle_creation'])>1){

                //有可能push_log表有个battle创建了又删除了然后又创建了，然后就是match_battle有一条而Push_log就有两条
                $battle_dea = $pushClassify[$battleId]['battle_deletion'];
                if (!empty($battle_dea)) {
                    $del_count = count($battle_dea) + 1;
                    if (count($pushClassify[$battleId]['battle_creation']) == $del_count){
                        continue;
                    }
                }

                self::addLog('创建多个battlePush信息');
                $message3 = "matchId:{$val['match']} 对应的battleId:{$battleId} 创建多个battlePush信息";
                $info3 = json_encode($message3,JSON_UNESCAPED_UNICODE);
                $params3 = [
                    'type' => 'IsBattlePush',
                    'tag' => $type .' '. 'IsBattlePush',
                    'info' => $info3,
                ];
                Common::requestPost(env('PUSH_MESSAGE_DING'), [], $params3);
                continue;
            }
            self::addLog('正常');
        }
    }

    public static function addLog($info)
    {
        $ec=$info;
        if(is_array($info)){
            $ec=json_encode($info);
        }
        print_r($ec."\n");
    }

}
