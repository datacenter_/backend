<?php
namespace app\commands\crontab;

class CrontabBase
{
    protected $runModel;
    protected $name;
    public function log($info)
    {
        print_r("\n".$info."\n");
        \Yii::info($info,$this->name);
    }
}