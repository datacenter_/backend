<?php
/**
 *
 */

namespace app\commands\redisQueue;


use app\modules\task\services\TaskRunner;
use Gclibs\Redisqueue\ConsumerInterface;

class RedisQueueConsumer implements ConsumerInterface
{
    public static function run($key, $info)
    {
        // $info 就是body
        $record_decode=json_decode($info,true);
        @print_r($record_decode['id']);
        TaskRunner::run($record_decode['id']);
    }
}