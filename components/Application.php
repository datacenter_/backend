<?php
namespace app\components;

/**
 * Class Application
 * @package app\components
 * @property \app\components\Response $response
 * @property \app\rest\Serializer $serializer
 * @property \yii\redis\Connection $redis
 * @property \app\components\SimpleTokenManager $tokenManager
 */
class Application extends \yii\web\Application
{
    /**
     * @return \yii\redis\Connection
     */
    public function getRedis()
    {
        return $this->get('redis');
    }
}