<?php
namespace app\components;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * 认证用户ActiveRecord基类
 * Class IdentityActiveRecord
 * @package app\components
 * @property int $id
 */
abstract class IdentityActiveRecord extends ActiveRecord implements IdentityInterface
{
    /**
     * @var string 存储时的命名空间
     */
    public static $namespaceForStorage;

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param string $token
     * @param null $type
     * @return IdentityActiveRecord|null|IdentityInterface
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return SimpleTokenManager::getIdentityByToken($token, self::getNamespaceForTokenStorage());
    }

    /**
     * @param string $token
     * @return array|mixed|null
     */
    public function getPermissionsByAccessToken($token)
    {
        $permissions = SimpleTokenManager::getPermissionsByToken($token, self::getNamespaceForTokenStorage());
        if (empty($permissions)) {
            $permissions = $this->getPermissions();
            SimpleTokenManager::savePermissionByToken($token, self::getNamespaceForTokenStorage());
        }

        return $permissions;
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
    }

    public function validateAuthKey($authKey)
    {
    }

    public function getPrettyId()
    {
        return $this->id;
    }

    /**
     * 存储时的命名空间,目前以认证类的basename为命名空间
     * @return string
     */
    public static function getNamespaceForTokenStorage()
    {
        if (static::$namespaceForStorage == null) {
            static::$namespaceForStorage = basename(str_replace('\\', '/', static::class));
        }

        return static::$namespaceForStorage;
    }

    /**
     * 获取权限
     * @return array
     */
    public function getPermissions()
    {
        return [];
    }

}