<?php
namespace app\components;

use yii\base\Model;

/**
 * Class PaginationForm
 * @package app\components
 */
class PaginationForm extends Model
{
    /**
     * @var int 页码
     */
    public $page;

    /**
     * @var int 页大小
     */
    public $pageSize;

    public function rules()
    {
        return [
            [['page', 'pageSize'], 'trim'],
            [['page', 'pageSize'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            ['page', 'default', 'value' => 1],
            ['pageSize', 'default', 'value' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'page' => '页码',
            'pageSize' => '页大小',
        ];
    }

    public function getPage()
    {
        return $this->page ? ($this->page - 1) : 0;
    }
}