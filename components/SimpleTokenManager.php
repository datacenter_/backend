<?php
namespace app\components;

use yii\base\Component;

/**
 * Class SimpleTokenManager
 * @package app\components
 */
class SimpleTokenManager extends Component
{
    const TOKEN_REDIS_KEY_PREFIX = "%s:token:%s:"; //redis token key
    const TOKEN_LIVE_TIME = 36000;       //Token有效期
    const TOKEN_AUTO_DELAY_TIME = 3600;     //60分钟活跃自动延时

    const TOKEN_PERMISSION_REDIS_KEY_PREFIX = "%s:token:permission:%s:"; //redis权限key
    const TOKEN_PERMISSION_LIVE_TIME = 300; //权限缓存时长 秒

    /**
     * @param string $token
     * @param string|null $namespace
     * @return string
     */
    protected static function buildTokenKey($token, $namespace = null)
    {
        if (empty($namespace)) {
            $namespace = 'user';
        }

        return sprintf(self::TOKEN_REDIS_KEY_PREFIX, $namespace, $token);
    }

    /**
     * @param string $token
     * @param string|null $namespace
     * @return string
     */
    protected static function buildTokenPermissionKey($token, $namespace = null)
    {
        if (empty($namespace)) {
            $namespace = 'user';
        }

        return sprintf(self::TOKEN_PERMISSION_REDIS_KEY_PREFIX, $namespace, $token);
    }

    /**
     * @param \app\components\IdentityActiveRecord $user 认证用户
     * @param int|null $expiresAt 过期时间 UNIX时间戳
     * @param string|null $namespace 标识空间
     * @return string
     */
    public static function generateToken($user, $namespace = null, $expiresAt = null)
    {
        $randStr = \Yii::$app->security->generateRandomString(48);
        $token = \Yii::$app->security->maskToken($randStr);
        $key = static::buildTokenKey($token, $namespace);

        \Yii::$app->getRedis()->multi();
        \Yii::$app->getRedis()->set($key, serialize($user));
        if (!empty($expiresAt)) {
            \Yii::$app->getRedis()->expireat($key, $expiresAt);
        } else {
            \Yii::$app->getRedis()->expire($key, static::getTokenLiveTime());
        }
        \Yii::$app->getRedis()->exec();
        \Yii::info('用户' . $user->getPrettyId() . "登录成功");

        return $token;
    }

    /**
     * @param string$token
     * @param \app\components\IdentityActiveRecord $user
     * @param string $namespace
     * @return bool
     */
    public static function removeToken($token, $namespace = null)
    {
        $key = static::buildTokenKey($token, $namespace);
        \Yii::$app->getRedis()->del($key);

        return true;
    }

    /**
     * @param string $namespace
     * @param string $token
     * @return IdentityActiveRecord
     */
    public static function getIdentityByToken($token, $namespace = null)
    {
        $key = static::buildTokenKey($token, $namespace);
        if (\Yii::$app->getRedis()->exists($key)) {
            $ttl = \Yii::$app->getRedis()->ttl($key);
            if (intval($ttl) <= self::TOKEN_AUTO_DELAY_TIME) {
                \Yii::$app->getRedis()->expire($key, self::TOKEN_LIVE_TIME);
                \Yii::info($token . "延期成功");
            }

            return unserialize(\Yii::$app->getRedis()->get($key));
        }
    }

    /**
     * Token默认有效期
     * @return int
     */
    private static function getTokenLiveTime(): int
    {
        return env("token_live_time") ? intval(env("token_live_time")) : self::TOKEN_LIVE_TIME;
    }

    /**
     * 保存权限
     * @param string $token
     * @param null|string $namespace
     * @param array $permissions
     * @return bool
     */
    public static function savePermissionByToken($token, $namespace = null, array $permissions = [])
    {
        $key = static::buildTokenPermissionKey($token, $namespace);

        \Yii::$app->getRedis()->multi();
        \Yii::$app->getRedis()->set($key, serialize($permissions));
        \Yii::$app->getRedis()->expire($key, static::TOKEN_PERMISSION_LIVE_TIME);
        \Yii::$app->getRedis()->exec();

        return true;
    }

    /**
     * 获取权限
     * @param string $token
     * @param null|string $namespace
     * @return mixed|null
     */
    public static function getPermissionsByToken($token, $namespace = null)
    {
        $key = static::buildTokenPermissionKey($token, $namespace);
        if (\Yii::$app->getRedis()->exists($key)) {
            return unserialize(\Yii::$app->getRedis()->get($key));
        }

        return null;
    }

}