<?php
$db = require __DIR__ . '/db.php';
$db_websoket = require __DIR__ . '/db_websoket.php';
$db_stream = require __DIR__ . '/db_stream.php';
$db_task = require __DIR__ . '/db_task.php';
$db_gy = require __DIR__ . '/db_gy.php';
$db_match_shuai = require __DIR__ . '/db_match_shuai.php';

$config = [
    "id" => env("APP_NAME"),
    'homeUrl' => '/rest',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
//    'on beforeRequest' => function () { //服务端自己处理跨域问题
//        if (\Yii::$app->getRequest()->isOptions) {
//            \Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', ['*']);
//            \Yii::$app->getResponse()->getHeaders()->set('Access-Control-Request-Headers', ['*']);
//            \Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', ['*']);
//            \Yii::$app->getResponse()->getHeaders()->set('Access-Control-Request-Method', ['*']);
//            \Yii::$app->getResponse()->setStatusCode(200)->send();
//            \Yii::$app->end();
//        }
//    },
    'timeZone' => 'PRC',
    'language' => 'zh-CN',
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'app\rest\JsonParser',
            ],
            'cookieValidationKey' => 'OJ8jhQcE0prGgaQhh-7LjFsdf88c7YY2KhJl75Ix',
        ],
        'response' => [
            'class' => 'app\components\Response',
            'targetFormat' => [
                'json'
            ],
        ],
        'tokenManager' => [
            'class' => \app\components\SimpleTokenManager::class
        ],
        'cache' => [
            'class' => yii\caching\FileCache::class,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => env('REDIS_HOST'),
            'database' => env('REDIS_DATABASE'), // 默认使用0号库(不建议修改)
            'password' => env('REDIS_PASSWORD'), // 无密码填写null
            'port' => env('REDIS_PORT')
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'MyFile' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => YII_DEBUG ? [] : ['trace', 'profile', 'error', 'warning'],
                    'exportInterval' => 1,
                    'fileMode' => 777,
                    'logFile' => (env('LOG_FILE_PATH') ?? '@runtime') . '/' . date('Y-m-d') . '.log',
                    'maxFileSize' => 1024 * 20,  //设置文件大小，以k为单位
                    'rotateByCopy' => false,
                    'maxLogFiles' => 100,  //同个文件名最大数量
                ]
            ],
        ],
        'db' => $db,
        'db_websoket' => $db_websoket,
        'db_stream' => $db_stream,
        'db_task' => $db_task,
        'db_gy' => $db_gy,
        'db_match_shuai' => $db_match_shuai,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'GET ' => 'guest/default',
            ]
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/i18n',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app.exception' => 'exception.php'
                    ],
                ],
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/i18n',
                    'fileMap' => [
                        'yii' => 'yii.php'
                    ],
                ]
            ],
        ],
        'user' => [
            'identityClass' => app\modules\admin\models\Admin::class,
        ],
    ],

    'modules' => [
        'v1' => [
            'class' => \app\modules\v1\Module::class,
        ]
    ]
];

if (YII_DEBUG) {
    $config = yii\helpers\ArrayHelper::merge(
        $config,
        require __DIR__ . '/debug.php'
    );
}

$config = yii\helpers\ArrayHelper::merge(
    $config,
    require __DIR__ . '/modules.php'
);

return $config;


