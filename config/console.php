<?php
error_reporting(0);
$db = require __DIR__ . '/db.php';
$db_websoket = require __DIR__ . '/db_websoket.php';
$db_stream = require __DIR__ . '/db_stream.php';
$db_gy = require __DIR__ . '/db_gy.php';
$db_match_shuai = require __DIR__ . '/db_match_shuai.php';


$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'controllerMap' => [
        'workerman' => [
            'class' => 'app\commands\WorkermanController',
            'config' => [
                'ip' => '0.0.0.0',
                'port' => '8345',
                'daemonize' => true,
            ],
        ],
    ],
    'timeZone' => 'PRC',
    'language' => 'zh-CN',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => env('REDIS_HOST'),
            'database' => env('REDIS_DATABASE'), // 默认使用0号库(不建议修改)
            'password' => env('REDIS_PASSWORD'), // 无密码填写null
            'port' => env('REDIS_PORT')
        ],
        // 新加坡redis
        'redis_spe' => [
            'class' => 'yii\redis\Connection',
            'hostname' => env('REDIS_HOST_SPE'),
            'database' => env('REDIS_DATABASE_SPE'), // 默认使用0号库(不建议修改)
            'password' => env('REDIS_PASSWORD_SPE'), // 无密码填写null
            'port' => env('REDIS_PORT_SPE')
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'db_websoket' => $db_websoket,
        'db_stream' => $db_stream,
        'db_gy' => $db_gy,
        'db_match_shuai' => $db_match_shuai,
        'user' => [
            'class' => \yii\web\User::class,
            'identityClass' => app\modules\admin\models\Admin::class,
        ],
    ],
    'modules' => [
        'v1' => [
            'class' => \app\modules\v1\Module::class,
        ]
    ]
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
