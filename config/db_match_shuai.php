<?php
return [
    'class' => \yii\db\Connection::class,
    'dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env('SHUAI_DB_HOST'), env('SHUAI_DB_PORT'), env('SHUAI_DB_DATABASE')),
    'username' => env('SHUAI_DB_USERNAME'),
    'password' => env('SHUAI_DB_PASSWORD'),
    'charset' => env('SHUAI_DB_CHARSET'),
];