<?php

//爬虫数据源
return [
    'class' => \yii\db\Connection::class,
    'dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env('TASK_DB_HOST'), env('TASK_DB_PORT'),env('TASK_DB_DATABASE')),
    'username' => env('TASK_DB_USERNAME'),
    'password' => env('TASK_DB_PASSWORD'),
    'charset' => env('TASK_DB_CHARSET'),
];
