<?php

// configuration adjustments for 'dev' environment

$config = [];

$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
];

$config['bootstrap'][] = 'gii';
$config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    'allowedIPs' => ['127.0.0.1', '::1', '172.17.0.1'],
//    'generators' => [
//        'controller' => [
//            'class' => 'app\templates\controller\Generator',
//            'templates' => [
//                'default' => '@app/templates/controller/default',
//            ]
//        ],
//        'module' => [
//            'class' => 'app\templates\module\Generator',
//            'templates' => [
//                'default' => '@app/templates/module/default',
//            ]
//        ],
//    ],
];

return $config;