<?php
namespace app\constants;

/**
 * 用户行为类型
 * Class UserBehaviorType
 * @package app\modules\log\constants
 */
class UserBehaviorType
{
    /**
     * 用户登录类型
     */
    const USER_BEHAVIOR_TYPE_LOGIN = 1;

    /**
     * 注册
     */
    const USER_BEHAVIOR_TYPE_REGISTER = 2;

    /**
     * 添加KOL
     */
    const USER_BEHAVIOR_TYPE_ADD_KOL = 3;

    /**
     * 修改KOL
     */
    const USER_BEHAVIOR_TYPE_UPDATE_KOL = 4;

    /**
     * 添加Schedule
     */
    const USER_BEHAVIOR_TYPE_ADD_SCHEDULE = 5;

    /**
     * 修改Schedule
     */
    const USER_BEHAVIOR_TYPE_UPDATE_SCHEDULE = 6;

    /**
     * 删除Schedule
     */
    const USER_BEHAVIOR_TYPE_DELETE_SCHEDULE = 7;

    /**
     * 删除Schedule,
     */
    const USER_BEHAVIOR_TYPE_DELETE_ACCOUNT_SCHEDULE = 17;

    /**
     * 添加客户
     */
    const USER_BEHAVIOR_TYPE_ADD_CUSTOMER = 8;

    /**
     * 修改个人资料
     */
    const USER_BEHAVIOR_TYPE_UPDATE_BASE_INFO = 9;


    public static $userBehaviorTypeLabels = [
        self::USER_BEHAVIOR_TYPE_LOGIN => '登录',
        self::USER_BEHAVIOR_TYPE_REGISTER => '注册',
        self::USER_BEHAVIOR_TYPE_ADD_KOL => '添加账号',
        self::USER_BEHAVIOR_TYPE_UPDATE_KOL => '修改账号',
        self::USER_BEHAVIOR_TYPE_ADD_SCHEDULE => '添加排期',
        self::USER_BEHAVIOR_TYPE_UPDATE_SCHEDULE => '修改排期',
        self::USER_BEHAVIOR_TYPE_DELETE_SCHEDULE => '删除排期',
        self::USER_BEHAVIOR_TYPE_DELETE_ACCOUNT_SCHEDULE => '移除账号排期',
        self::USER_BEHAVIOR_TYPE_ADD_CUSTOMER => '添加客户',
        self::USER_BEHAVIOR_TYPE_UPDATE_BASE_INFO => '修改个人资料'
    ];

}