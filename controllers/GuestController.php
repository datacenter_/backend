<?php
namespace app\controllers;

/**
 * 不需要身份认证的controller
 * Class GuestController
 * @package app\controllers
 * @property \yii\web\user user
 */
class GuestController extends RestController
{
    /**
     * 默认路由
     * @return array
     */
    public function actionDefault()
    {
        return ['Welcome, I\'m ' . \Yii::$app->id . '!'];
    }

    /**
     * 请求方法为OPTIONS默认成功
     */
    public function actionOptions()
    {
        return  ['GET', 'POST'];
    }
}