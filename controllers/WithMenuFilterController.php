<?php
namespace app\controllers;

use app\filters\MenuAuthFilter;

/**
 * 带菜单权限验证控制器
 * Class WithMenuAuthController
 * @package app\controllers
 */
abstract class WithMenuFilterController extends WithTokenAuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['menuAuthFilter'] = [
            'class' => MenuAuthFilter::class,
        ];

        return $behaviors;
    }

    /**
     * 定义菜单key 用于权限验证
     * @return string
     */
    abstract static function getMenukey(): String;
}