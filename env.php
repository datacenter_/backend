<?php
/**
 * Setup application environment
 */
if (strlen(env("RUNTIME_ENVIROMENT"))) {
    $env = ".env." . env("RUNTIME_ENVIROMENT");
} else {
    $env = ".env.dev";
}
$dotenv = \Dotenv\Dotenv::create(__DIR__, $env ?: null);
$dotenv->load();