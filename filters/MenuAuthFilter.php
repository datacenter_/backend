<?php
namespace app\filters;

use app\controllers\WithMenuFilterController;
use app\rest\exceptions\NoOperationPermissionException;
use yii\base\ActionFilter;

/**
 * 菜单过滤器
 * Class MenuAuthFilter
 * @package app\filters
 */
class MenuAuthFilter extends ActionFilter
{
    public function beforeAction($action)
    {
        /** @var WithMenuFilterController $owner */
        $owner = $this->owner;
        $accessToken = \Yii::$app->getRequest()->getHeaders()->get(TokenAuthMethod::$tokenKey);
        $menus = $owner->getIdentity()->getPermissionsByAccessToken($accessToken);

        if (empty($menus) || !is_array($menus) || !in_array($owner::getMenukey(), $menus)) {
            throw new NoOperationPermissionException();
        }

        return parent::beforeAction($action);
    }
}