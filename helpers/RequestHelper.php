<?php
namespace app\helpers;

class RequestHelper
{
    /**
     * 从Reqeust获取参数, 优先级header > query > post > body
     * @param string $targetParam
     * @return array|mixed|null|string
     */
    public static function getParam($targetParam)
    {
        $request = \Yii::$app->getRequest();

        if (!is_string($targetParam) || strlen($targetParam) <= 0) {
            return null;
        }

        $param = $request->getHeaders()->get($targetParam) ?? $request->getQueryParam($targetParam);
        if ($param === null) {
            if ($request->getIsPost()) {
                $param = $request->post($targetParam);
            } else {
                $param = $request->getBodyParam($targetParam);
            }
        }

        return $param;
    }
}