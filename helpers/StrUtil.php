<?php
namespace app\helpers;

/**
 * 字符串工具类
 * Class StringUtils
 * @package app\helpers
 */
class StrUtil
{
    /**
     * 是否空字符串
     * @param String $str
     * @param string $encoding
     * @return bool
     */
    public static function isEmpty(String $str, $encoding = "UTF8"): bool
    {
        if (is_null($str)) {
            return true;
        }

        if (mb_strlen($str, $encoding) == 0) {
            return true;
        }

        if (mb_strlen(str_replace(["\0", "\t", "\n", "\r", "\x0B", " "], '', $str), $encoding) == 0) {
            return true;
        }

        return false;
    }

    /**
     * 是否非空白字符串
     * @param String $str
     * @param string $encoding
     * @return bool
     */
    public static function notEmpty(String $str, $encoding = "UTF8"): bool
    {
        return !self::isEmpty($str, $encoding);
    }
}