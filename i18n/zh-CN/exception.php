<?php
use app\rest\Code;

return [
    Code::$exceptionDefaultMessages[Code::BUSINESS_EXCEPTION] => '业务操作异常,请稍后重试',
    Code::$exceptionDefaultMessages[Code::UNAUTHORIZATION_EXCEPTION] => '对不起,您没有该操作权限',
    Code::$exceptionDefaultMessages[Code::AUTHENTICATION_FAILED_EXCEPTION] => '用户名密码错误',
    Code::$exceptionDefaultMessages[Code::NO_LOGGED_IN_EXCEPTION] => '对不起，您还没有登录',
    Code::$exceptionDefaultMessages[Code::REQUEST_PARAMS_UNVALIDATED_EXCEPTION] => '参数不合法',
    Code::$exceptionDefaultMessages[Code::NO_OPERATION_PERMISSION_EXCEPTION] => '对不起,您没有操作权限',
    Code::$exceptionDefaultMessages[Code::RUNTIME_EXCEPTION] => '操作失败，请稍后重试',
    Code::$exceptionDefaultMessages[Code::ENTITY_NOT_EXIST_EXCEPTION] => '没有找到该数据',
    Code::$exceptionDefaultMessages[Code::REMOTE_CALL_EXCEPTION] => '远程调用异常',
    'default exception message' => '操作失败',
];