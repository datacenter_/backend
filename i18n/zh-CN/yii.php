<?php

$messages = [
    '{attribute} cannot be blank.' => '{attribute}不能为空',
    '{attribute} {value} has already been taken.' => '{attribute}{value}已存在',
    'The combination {values} of {attributes} has already been taken.' => '{attributes}已存在',
    '{attribute} "{value}" has already been taken.' => '{attribute}{value}已经被占用了。',
    'The format of {attribute} is invalid.' => '{attribute}的格式无效'
];

$i18n = \Yii::$app->language;
return array_merge(
    require \Yii::getAlias('@yii') . "/messages/{$i18n}/yii.php",
    $messages
);