<?php

namespace app\modules\account\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\account\models\ClientAccount;
use app\modules\account\services\AccountService;
use yii\web\Controller;

/**
 * Default controller for the `account` module
 */
class ClientAccountController extends WithTokenAuthController
{
    public function actionAdd()
    {
        $infos=\Yii::$app->request->post();
        return AccountService::addClient($infos);
    }

    public function actionDetail()
    {
        $id=\Yii::$app->request->get("id");
        return AccountService::getInfoById($id);
    }

    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return AccountService::clientList($params);
    }

    public function actionEdit()
    {
        $infos=\Yii::$app->request->post();
        return AccountService::edit($infos);
    }

    public function actionRePassword()
    {
        $id=\Yii::$app->request->post("id");
        $secretKey=\Yii::$app->request->post("secret_key");
        return AccountService::rePassword($id,$secretKey);
    }
}
