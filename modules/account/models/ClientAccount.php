<?php

namespace app\modules\account\models;

use Yii;

/**
 * This is the model class for table "client_account".
 *
 * @property int $id
 * @property string|null $client_id 客户端id
 * @property string|null $secret_key 密码
 * @property string|null $created_time 创建时间
 * @property string|null $modify_at 修改时间
 * @property string|null $description 描述
 */
class ClientAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'modify_at'], 'safe'],
            [['description'], 'string'],
            [['client_id'], 'string', 'max' => 100],
            [['secret_key'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => '客户端id',
            'secret_key' => '密码',
            'created_time' => '创建时间',
            'modify_at' => '修改时间',
            'description' => '描述',
        ];
    }
}
