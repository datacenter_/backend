<?php
/**
 *
 */

namespace app\modules\account\services;


use app\modules\account\models\ClientAccount;

class AccountService
{
    public static function addClient($params)
    {
        $addInfo=[
            'client_id'=>$params['client_id'],
            'secret_key'=>\Yii::$app->getSecurity()->generatePasswordHash($params['secret_key']),
        ];
        $clientInfo=new ClientAccount();
        $clientInfo->setAttributes($addInfo);
        $clientInfo->save();
        return $clientInfo->id;
    }

    public static function clientList($params)
    {
        $page=(isset($params['page'])&&$params['page'])?$params['page']:1;
        $perPage=(isset($params['per_page'])&&$params['per_page'])?$params['per_page']:10;
        $count=ClientAccount::find()->count();
        $list=ClientAccount::find()->limit($perPage)->offset($perPage*($page-1))->all();
        return [
            'total'=>$count,
            'list'=>$list
        ];
    }

    public static function rePassword($id,$secretKey)
    {
        $clientInfo=ClientAccount::find()->where(['id'=>$id])->one();
        $clientInfo->setAttribute('secret_key',\Yii::$app->getSecurity()->generatePasswordHash($secretKey));
        $clientInfo->save();
        return $clientInfo;
    }

    public static function edit($params)
    {
        $clientInfo=ClientAccount::find()->where(['id'=>$params['id']])->one();
        $clientInfo->setAttributes($params);
        return $clientInfo->save();
    }

    public static function getInfoById($id)
    {
        return ClientAccount::find()->where(['id'=>$id])->one();
    }

}