<?php
namespace app\modules\admin\constants;

/**
 * Class Common
 * @package app\modules\admin
 */
class Common
{
    /**
     * 超级管理员
     */
    const ADMIN_TYPE_SUPER = 1;

    /**
     * 普通管理员
     */
    const ADMIN_TYPE_GENERAL = 2;
}