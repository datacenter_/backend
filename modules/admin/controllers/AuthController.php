<?php
namespace app\modules\admin\controllers;

use app\controllers\GuestController;
use app\modules\admin\forms\LoginForm;
use app\modules\admin\services\AuthService;
use app\modules\common\services\ImageConversionHelper;

use app\commands\HotController;

/**
 * Class AuthController
 * @package app\modules\admin\controllers
 */
class AuthController extends GuestController
{
    /**
     * 登录
     * @return array
     * @throws \app\rest\exceptions\AuthenticationFailedException
     */
    public function actionLogin()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return AuthService::login($attributes);
    }

    public function actionDown()
    {
        $url=$this->pGet('url');
        $info=ImageConversionHelper::downfile($url);
        return $info;
    }

    public function actionHots()
    {
        return HotController::actionRefresh();
    }
}