<?php
namespace app\modules\admin\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\admin\services\AuthService;
use app\modules\admin\services\CustomerService;

/**
 * Class AdminManagerController
 * @package app\modules\admin\controllers
 */
class UserController extends WithTokenAuthController
{
    /**
     * 获取详情
     */
    public function actionGetAuthInfo()
    {
        return $this->getIdentity();
    }

    /**
     * 退出登陆
     */
    public function actionLogout()
    {
        $token = $this->getIentityToken();

        return AuthService::logout($token);
    }

    /**
     * 获取首页信息
     */
    public function actionGetData()
    {
        return CustomerService::getData();
    }
}