<?php
namespace app\modules\admin\forms;

use app\modules\admin\models\Admin;
use app\modules\admin\models\Resource;
use app\validators\ChineseOrLetterValidator;
use app\validators\MobilePhoneValidator;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\modules\admin\forms
 */
class RegisterForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $cell_phone;
    /**
     * @var integer
     */
    public $id;

    /**
     * @var int[]
     */
    public $permission_list;

    public function rules()
    {
        return [
            [['username', 'password', 'name', 'cell_phone'], 'trim','on'=>'create'],
            [['username', 'password', 'name', 'cell_phone', 'permission_list'], 'required','on'=>'create'],
            [['username', 'password'], 'string', 'max' => 100, 'min' => 1,'on'=>'create'],
            ['name', ChineseOrLetterValidator::class, 'max' => 22,'on'=>'create'],
            ['cell_phone', MobilePhoneValidator::class,'on'=>'create'],
            [['cell_phone', 'username'], 'unique', 'targetClass' => \app\modules\Admin\models\Admin::class,'on'=>'create'],
            ['permission_list', 'in', 'allowArray' => true, 'range' => Resource::getAllEnableIdList(), 'message' => '选择的管理员权限不存在','on'=>'create'],

            [['username', 'name', 'cell_phone','id'], 'trim', 'on' => 'update'],
            [['username', 'name', 'cell_phone', 'permission_list','id'], 'required','on' => 'update'],
            [['username'], 'string', 'max' => 100, 'min' => 1,'on' => 'update'],
            [['id'], 'integer','on' => 'update'],
            ['name', ChineseOrLetterValidator::class, 'max' => 22,'on' => 'update'],
            [['cell_phone'], 'phoneValidation','on' => 'update'],
            ['permission_list', 'in', 'allowArray' => true, 'range' => Resource::getAllEnableIdList(), 'message' => '选择的管理员权限不存在' ,'on' => 'update'],
        ];
    }

    public function phoneValidation($attribute,$params)
    {
        $admin = Admin::find()->where(['id' => $this->id])->asArray()->one();
        if ($admin['cell_phone'] == $this->$attribute) {
            return true;
        }else{
            $phone = Admin::find()->where(['cell_phone' => $this->$attribute])->one();
            if ($phone && !empty($phone)) {
                $this->addError($attribute, "手机号已经有人注册了");
                return true;
            }
        }
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = [
            'username',
            'password',
            'name',
            'permission_list',
            'cell_phone'
        ];
        $scenarios['update'] = [
            'username',
            'name',
            'permission_list',
            'cell_phone',
            'id'
        ];
        return $scenarios;
    }
    public function attributeLabels()
    {
        return [
            'username' => '管理员登录名',
            'password' => '管理员密码',
            'name' => '管理员姓名',
            'cell_phone' => '管理员电话',
            'permission_list' => '管理员权限'
        ];
    }
}