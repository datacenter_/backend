<?php
namespace app\modules\admin\models;

use app\components\IdentityActiveRecord;
use app\constants\Contract;
use app\modules\admin\constants\Common;

/**
 * Class Admin
 * @package app\modules\admin\models
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $cell_phone
 * @property string $name
 * @property int $role_type
 * @property \DateTime $created_at
 * @property \DateTime $modified_at
 * @property int $is_enable
 * @property string $pretty_id
 * @property string[] $menus  权限数组
 */
class Admin extends IdentityActiveRecord
{
    const PRETTY_KEY = "GL";

    public static function tableName()
    {
        return '{{admin}}';
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['pretty_id'] = 'prettyId';

        return $fields;
    }

    public function attributeLabels()
    {
        return [
            'username' => '管理员登录名',
            'password' => '管理员密码',
            'cell_phone' => '管理员电话',
            'name' => '管理员姓名',
        ];
    }

    public function extraFields()
    {
        $extraFields = parent::extraFields();
        $extraFields['permissions'] = 'permissions';

        return $extraFields;
    }

    public function getPermissions()
    {
        $menus = [];
        if ($this->role_type == Common::ADMIN_TYPE_SUPER) {
            $result = Resource::find()->select(['identification'])->where(['is_enable' => Contract::COMMON_DB_YES])->asArray()->all();
        } else {
            $result = static::hasMany(Resource::class, ['id' => 'resource_id'])->select(['identification'])
                ->where(['is_enable' => Contract::COMMON_DB_YES])
                ->viaTable(Permission::tableName(), ['admin_id' => 'id'])->asArray()->all();
        }

        if (!empty($result)) {
            foreach ($result as $row) {
                $menus[] = $row['identification'];
            }
        }

        return $menus;
    }

    public function getPrettyId()
    {
        return static::PRETTY_KEY . $this->id;
    }

    public static function getByUsername($username)
    {
        return static::findOne(['username' => $username, 'is_enable' => Contract::COMMON_DB_YES]);
    }

}