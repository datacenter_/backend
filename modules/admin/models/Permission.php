<?php
namespace app\modules\admin\models;

use yii\db\ActiveRecord;

/**
 * Class AdminPermission
 * @package app\modules\admin\models
 * @property int $admin_id
 * @property int $resource_id
 */
class Permission extends ActiveRecord
{
    public static function tableName()
    {
        return '{{admin_permission}}';
    }

}