<?php
namespace app\modules\admin\models;

use app\constants\Contract;
use yii\db\ActiveRecord;

/**
 * Class Resource
 * @package app\modules\admin\models
 * @property int $int
 * @property string $name
 * @property string $identification
 * @property int $is_enable
 * @property \DateTime $created_at
 * @property \DateTime $modified_at
 */
class Resource extends ActiveRecord
{
    public static function tableName()
    {
        return '{{admin_resource}}';
    }

    /**
     * 获取有效数据的ID List
     * @return array
     */
    public static function getAllEnableIdList()
    {
        $result = static::findAll(['is_enable' => Contract::COMMON_DB_YES]);
        if (!empty($result)) {
            return array_column($result, 'id');
        }

        return [];
    }

}