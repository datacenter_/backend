<?php
namespace app\modules\admin\services;

use app\components\SimpleTokenManager;
use app\constants\Contract;
use app\modules\admin\constants\Common;
use app\modules\admin\forms\LoginForm;
use app\modules\admin\forms\RegisterForm;
use app\modules\admin\models\Admin;
use app\modules\admin\models\Permission;
use app\modules\admin\models\Resource;
use app\rest\exceptions\AuthenticationFailedException;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\data\Pagination;

/**
 * Class AuthService
 * @package app\modules\admin\services
 */
class AuthService
{
    /**
     * 登录
     * @param array $attributes
     * @return array
     * @throws AuthenticationFailedException
     */
    public static function login(array $attributes = [])
    {
        $form = new LoginForm();
        $form->setAttributes($attributes);

        if ($form->validate()) {
            $identity = Admin::getByUsername($form->username);
            if (!empty($identity) && $identity->is_enable == Contract::COMMON_DB_YES
                && \Yii::$app->getSecurity()->validatePassword($form->password, $identity->password)) {
                unset($identity['password']);
                $data = [
                    'user' => $identity,
                    'token' => \Yii::$app->tokenManager::generateToken($identity, Admin::getNamespaceForTokenStorage()),
                    'menus' => $identity->permissions
                ];

                \Yii::$app->tokenManager::savePermissionByToken($data['token'], Admin::getNamespaceForTokenStorage(), $identity->permissions);

                return $data;
            }
        }

        throw new AuthenticationFailedException();
    }

    /**
     * 修改密码
     */
    public static function rePassword($attributes)
    {
        $admin = Admin::find()->where(['id' => $attributes['id']])->one();
        $admin->password = \Yii::$app->getSecurity()->generatePasswordHash($attributes['new_password']);

        if ($admin->save() > 0) {
            return ['success' => true, 'msg' => '修改成功'];
        }
    }
    /**
     * 注册/添加管理员账号
     * @param array $attributes
     * @return mixed
     * @throws RequestParamsUnvalidatedException
     */
    public static function register(array $attributes = [])
    {
        $form = new RegisterForm();


        if (isset($attributes['id']) && !empty($attributes['id'])) {
            $form->setScenario('update');
            // 删除以前得权限
            Permission::deleteAll(['admin_id' => $attributes['id']]);
        }else{
            $form->setScenario('create');
        }
        $form->setAttributes($attributes);

        if ($form->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if (isset($attributes['id']) && !empty($attributes['id'])) {
                    $admin =  Admin::find()->where(['id' => $attributes['id']])->one();
                } else {
                    $admin = Admin::instance(true);
                    $admin->password = \Yii::$app->getSecurity()->generatePasswordHash($form->password);
                    $admin->is_enable = Contract::COMMON_DB_YES;
                }
                $admin->role_type = isset($attributes['role_type'])?$attributes['role_type']:3;
                $admin->username = $attributes['username'];
                $admin->cell_phone = $form->cell_phone;
                $admin->name = $form->name;
                $admin->save();

                if (!empty($form->permission_list)) {
                    foreach ($form->permission_list as $resourceId) {
                        $adminPermission = Permission::instance(true);
                        $adminPermission->admin_id = $admin->getId();
                        $adminPermission->resource_id = $resourceId;
                        $adminPermission->save(true);
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                \Yii::warning($e->getMessage());

                throw $e;
            }
            return $admin->prettyId;
        } else {
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }

    }

    public static function logout($token)
    {
        return SimpleTokenManager::removeToken($token,Admin::getNamespaceForTokenStorage());
    }

    public static function adminList($params)
    {
        $admin = Admin::find()->select('id,username,cell_phone,name,role_type,created_at,modified_at')
            ->alias('a')
            ->where(['is_enable' => Contract::COMMON_DB_YES]);

        if (isset($params['name']) && !empty($params['name'])) {
            $admin->andWhere(['like', 'name', $params['name']]);
        }

        if (isset($params['username']) && !empty($params['username'])) {
            $admin->andWhere(['like', 'username', $params['username']]);
        }

        if (isset($params['cell_phone']) && !empty($params['cell_phone'])) {
            $admin->andWhere(['cell_phone' => $params['cell_phone']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $admin->andWhere(['>=','created_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $admin->andWhere(['<','created_at', $end]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $admin->andWhere(['id' => $params['id']]);
        }


        $pages = new Pagination([
            'totalCount' => $admin->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $admin->orderBy('id desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        // 获取权限
        $adminIds = array_column($model,'id');
        $permission = Permission::find()->alias('p')
            ->select('p.admin_id,r.name as name,r.id as r_id')
            ->leftJoin('admin_resource as r','p.resource_id = r.id')
            ->where(['in', 'p.admin_id', $adminIds])
            ->andWhere(['r.is_enable' => Contract::COMMON_DB_YES])
            ->asArray()
            ->all();
        $resource = [];
        foreach ($permission as $auth) {
            $resource[$auth['admin_id']]['key'][] = $auth['r_id'];
            $resource[$auth['admin_id']]['name'][] = $auth['name'];
        }
        unset($permission);

        foreach ($model as &$v) {

            $v['permission'] = $resource[$v['id']] ?? [];
            $v['role_name'] = $v['role_type'] == 1 ? '超级管理员' : '普通管理员';
        }
        return ['list' => $model, 'total' => $admin->count()];
    }

    public static function getPermission()
    {
        return Resource::findAll(['is_enable' => Contract::COMMON_DB_YES]);
    }

    public static function delete($post)
    {
        $admin = Admin::findOne($post['id']);
        if (!empty($admin) && $admin) {
            $admin->is_enable = Contract::COMMON_DB_NO;
            if ($admin->save() > 0) {
                return ['success' => true, 'msg' => '成功'];
            }
        }

        throw new BusinessException(['message' => $admin], '删除管理员失败');
    }
}