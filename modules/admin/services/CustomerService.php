<?php

namespace app\modules\admin\services;


use app\constants\Contract;
use app\modules\common\models\EnumGame;
use app\modules\customer\models\Customer;
use app\modules\data\models\DataTodo;
use app\modules\data\models\StandardDataMetadata;
use app\modules\kol\models\Schedule;
use app\modules\match\models\Match;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\standardIncrement\StandardTeam;
use app\modules\tournament\models\Tournament;
use function GuzzleHttp\Psr7\str;
use yii\data\Pagination;

/**
 * Class UserSearchService
 * @package app\modules\admin\services
 */
class CustomerService
{
    /**
     * @param $params
     */
    public static function getCustomerList($params)
    {
        $customer = Customer::find()->alias('c')
            ->select('c.modified_at,ub.nickname,c.id as c_id,c.name,c.created_at,c.entry_at,u.cell_phone')
            ->leftJoin('kt_user as u','c.user_id = u.id')
            ->leftJoin('kt_user_base_info as ub','ub.user_id = u.id');
            $customer->where(['c.is_enable' => Contract::COMMON_DB_YES]);
        if (isset($params['name']) && !empty($params['name'])) {
            $customer->andWhere(['like', 'c.name', $params['name']]);
        }

        if (isset($params['c_id']) && !empty($params['c_id'])) {
            $customer->andWhere(['c.id' => $params['c_id']]);
        }
        if (isset($params['cell_phone']) && !empty($params['cell_phone'])) {
            $customer->andWhere(['u.cell_phone' => $params['cell_phone']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $customer->andWhere(['>=', 'c.created_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $customer->andWhere(['<', 'c.created_at', $end]);
        }

        $pages = new Pagination([
            'totalCount' => $customer->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $customer->orderBy('c.id desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        return ['list' => $model, 'total' => $customer->count()];
    }

    public static function getTaskByCustomer($cId)
    {
        $schedule = Customer::find()->alias('c')
            ->select('s.schedule_date,s.schedule_start_time,s.schedule_end_time,k.name,sb.ad_instruction')
            ->leftJoin('kt_kol_schedule_business as sb', 'sb.customer_id = c.id')
            ->leftJoin('kt_kol_schedule as s','s.id = sb.kol_schedule_id')
            ->leftJoin('kt_user_kol as k','k.id = s.kol_id')
            ->where(['sb.customer_id' => $cId])
            ->orderBy('s.schedule_date desc')
            ->asArray()
            ->all();

        return $schedule;
    }
    public function getMatch($start,$end,$game = false)
    {
        if ($game) {
            $arr = [];
            $game =  EnumGame::find()->asArray()->all();

            foreach ($game as $value)
            {
                $arr[$value['id']] = 0;
            }

            $match =  Match::find()
                ->innerJoin('enum_game','match.game = enum_game.id')
                ->where(['>=','scheduled_begin_at',$start])
                ->andWhere(['<=','scheduled_begin_at',$end])
                ->asArray()->all();
            if (empty($match))
            {
               return $arr;
            }
            foreach ($match as $val)
            {
                $arr[$val['game']] += 1;
            }

            return $arr;
        }
        return Match::find()->where(['>=','scheduled_begin_at',$start])
            ->andWhere(['<=','scheduled_begin_at',$end])->count();
    }
    public function getTournament($start,$end)
    {
        return Tournament::find()->where(['>=','scheduled_begin_at',$start])
            ->andWhere(['<=','scheduled_end_at',$end])->count();
    }
    public static function getData()
    {
        // 获取昨天
        $yStart = date("Y-m-d 00:00:00",strtotime("-1 day"));
        $ywEnd = date("Y-m-d 23:59:59",strtotime("-1 day"));
        $info['match_yesterday'] = self::getMatch($yStart,$ywEnd);
        $info['tournament_yesterday'] = self::getTournament($yStart,$ywEnd);
        $info['game_yesterday'] = self::getMatch($yStart,$ywEnd,true);

        // 获取今天
        $tStart = date("Y-m-d H:i:s",mktime(0, 0 , 0));
        $twEnd = date("Y-m-d H:i:s",mktime(23,59,59));
        $info['match_today'] = self::getMatch($tStart,$twEnd);
        $info['tournament_today'] = self::getTournament($tStart,$twEnd);
        $info['game_today'] = self::getMatch($tStart,$twEnd,true);

        // 获取明天
        $minStart = date("Y-m-d 00:00:00",strtotime("+1 day"));
        $minwEnd = date("Y-m-d 23:59:59",strtotime("+1 day"));
        $info['match_tomorrow'] = self::getMatch($minStart,$minwEnd);
        $info['tournament_tomorrow'] = self::getTournament($minStart,$minwEnd);
        $info['game_tomorrow'] = self::getMatch($minStart,$minwEnd,true);

        // 获取上周
        $wStart = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-date('w')+1-7,date('Y')));
        $wEnd = date('Y-m-d H:i:s',mktime(23,59,59,date('m'),date('d')-date('w')+7-7,date('Y')));
        $info['match_week'] = self::getMatch($wStart,$wEnd);
        $info['tournament_week'] = self::getTournament($wStart,$wEnd);
        // 获取本月
        $mStart =  date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y")));
        $mEnd =  date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y")));
        $info['match_moth'] = self::getMatch($mStart,$mEnd);
        $info['tournament_moth'] = self:: getTournament($mStart,$mEnd);

        // 获取待处理绑定的
        $team = StandardDataTeam::find()->where(['in','deal_status',[2,3]])->andWhere(['in','game_id',[1,2,3]])->asArray()->limit(20)->all();
        $player = StandardDataPlayer::find()->where(['in','deal_status',[2,3]])->andWhere(['in','game_id',[1,2,3]])->asArray()->limit(20)->all();
        $metaItems = StandardDataMetadata::find()->select('id')->where(['in','deal_status',[1,2,3]])->andWhere(['metadata_type'=>'lol_item'])->asArray()->limit(20)->all();
        $num = count($team) + count($player) + count($metaItems);
        $info['bind']['team'] = $team;
        $info['bind']['player'] = $player;
        $info['bind']['meta_items'] = $metaItems;
        $info['bind']['num'] = $num;
        // 获取待处理更新的
        $todoList = DataTodo::find()->where(['status' => 1])->asArray()->limit(20)->all();
        $todoCount = count($todoList);
        $info['bind_update']['list'] = $todoList;
        $info['bind_update']['num'] = $todoCount;

        return $info;
    }
}