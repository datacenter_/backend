<?php

namespace app\modules\admin\services;

use app\constants\Contract;
use app\constants\Platform;
use app\modules\kol\models\Schedule;
use app\modules\mini\models\User;
use function GuzzleHttp\Psr7\str;
use yii\data\Pagination;

/**
 * Class UserSearchService
 * @package app\modules\admin\services
 */
class ScheduleService
{
    /**
     * @param $params
     */
    public static function search($params, $info = false)
    {
        if ($info) {
            $field = 's.is_deleted,s.schedule_date,s.schedule_start_time,s.schedule_end_time,s.id as task_id,sb.ad_instruction,c.name,
            k.name as kol_name,k.platform_identification,k.platform_id,u.last_login_time,ub.mcn_name,
            u.id as uid,ub.nickname,u.cell_phone,u.created_at as u_created_at,c.id as c_id';
        } else {
            $field = 's.is_deleted,s.schedule_date,s.schedule_start_time,s.schedule_end_time,s.id as task_id,sb.ad_instruction,c.name,k.name as kol_name,k.platform_identification,k.platform_id,u.last_login_time,ub.mcn_name';
        }
        $scheduleModel = Schedule::find()->alias('s')
            ->select($field)
            ->leftJoin('kt_user_kol as k', 's.kol_id = k.id')
            ->leftJoin('kt_user as u', 'u.id = k.user_id')
            ->leftJoin('kt_user_base_info as ub', 'ub.user_id = u.id')
            ->leftJoin('kt_kol_schedule_business as sb', 's.id = sb.kol_schedule_id')
            ->leftJoin('kt_user_customer as c', 'sb.customer_id = c.id');

        if (isset($params['mcn_name']) && !empty($params['mcn_name'])) {
            $scheduleModel->andWhere(['like', 'ub.mcn_name' ,$params['mcn_name']]);
        }
        if (isset($params['account_name']) && !empty($params['account_name'])) {
            $scheduleModel->andWhere(['like', 'k.name', $params['account_name']]);
        }
        if (isset($params['cell_phone']) && !empty($params['cell_phone'])) {
            $scheduleModel->andWhere(['u.cell_phone' => $params['cell_phone']]);
        }

        if (isset($params['platform_id']) && !empty($params['platform_id'])) {
            $scheduleModel->andWhere(['k.platform_id' => $params['platform_id']]);
        }
        if (isset($params['account_id']) && !empty($params['account_id'])) {
            $scheduleModel->andWhere(['like', 'k.platform_identification', $params['account_id']]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $scheduleModel->andWhere(['s.id' => $params['id']]);
        }

        if (isset($params['task_id']) && !empty($params['task_id'])) {
            $scheduleModel->andWhere(['like', 's.id', $params['task_id']]);
        }

        if (isset($params['task_info']) && !empty($params['task_info'])) {
            $scheduleModel->andWhere(['like', 'sb.ad_instruction', $params['task_info']]);
        }
        if (isset($params['status']) && !empty($params['status'])) {
            $now = date('Y-m-d');
            // 启用
            if ($params['status'] == 1) {
                $scheduleModel->andWhere(['>=','s.schedule_date',$now]);
                $scheduleModel->andWhere(['!=','s.is_deleted',Contract::COMMON_DB_YES]);
            }
            // 过期
            if ($params['status'] == 2) {
                $scheduleModel->andWhere(['<', 's.schedule_date', $now]);
                $scheduleModel->andWhere(['!=','s.is_deleted',Contract::COMMON_DB_YES]);
            }
            // 已删除
            if ($params['status'] == 3) {
                $scheduleModel->andWhere(['s.is_deleted' => Contract::COMMON_DB_YES]);
            }
        }

        $pages = new Pagination([
            'totalCount' => $scheduleModel->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $scheduleModel->orderBy('s.id desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        foreach ($model as &$v) {
            $v['schedule_status_name'] = self::getFormatByStatus($v['schedule_date'],$v['is_deleted']);
            $v['platform_name'] = Platform::$platformLabels[$v['platform_id']] ?? '';
        }
        return ['list' => $model, 'total' => $scheduleModel->count(), 'sql' => $scheduleModel->createCommand()->getRawSql()];
    }

    public static function getScheduleInfoByKol($params)
    {
        $sIds = explode(',', $params['s_ids']);

        $schedule = Schedule::find()->alias('s')
            ->select(['s.is_deleted', 's.schedule_date', 'c.name', 's.id as s_id', 's.notify_status', 'sb.ad_instruction', 's.schedule_start_time', 's.schedule_end_time'])
            ->leftJoin('kt_kol_schedule_business as sb', 's.id = sb.kol_schedule_id and sb.is_enable = 1')
            ->leftJoin('kt_user_customer as c', 'sb.customer_id = c.id and c.is_enable = 1')
            ->where(['in', 's.id', $sIds])
            ->asArray()
            ->all();

        foreach ($schedule as &$v) {
            $v['schedule_status_name'] = self::getFormatByStatus($v['schedule_date'], $v['is_deleted']);
            $v['notify_status_name'] = $v['notify_status'] == 1 ? '是' : '否';
        }

        return $schedule;
    }

    public static function getFormatByStatus($date, $isDelete)
    {
        $now = strtotime(date('Y-m-d'));
        $start = strtotime($date);

        $statusName = '';
        if ($start >= $now) {
            $statusName = '占用';
        } else {
            $statusName = '过期';
        }

        if ($isDelete == Contract::COMMON_DB_YES) {
            $statusName = '已删除';
        }
        return $statusName;
    }

    public static function dataFormat($info)
    {
        $data = [];
        foreach ($info['list'] as $v) {
            $data[$v['task_id']]['schedule_start_time'] = $v['schedule_start_time'];
            $data[$v['task_id']]['schedule_end_time'] = $v['schedule_end_time'];
            $data[$v['task_id']]['schedule_date'] = $v['schedule_date'];
            $data[$v['task_id']]['kol_name'] = $v['kol_name'];
            $data[$v['task_id']]['platform_identification'] = $v['platform_identification'];
            $data[$v['task_id']]['last_login_time'] = $v['last_login_time'];
            $data[$v['task_id']]['mcn_name'] = $v['mcn_name'];
            $data[$v['task_id']]['nickname'] = $v['nickname'];
            $data[$v['task_id']]['cell_phone'] = $v['cell_phone'];
            $data[$v['task_id']]['u_created_at'] = $v['u_created_at'];
            $data[$v['task_id']]['uid'] = $v['uid'];
            $data[$v['task_id']]['task_id'] = $v['task_id'];
            $data[$v['task_id']]['schedule_status_name'] = $v['schedule_status_name'];
            $data[$v['task_id']]['platform_name'] = $v['platform_name'];
            $data[$v['task_id']]['ad'][$v['c_id']]['ad_instruction'] = $v['ad_instruction'];
            $data[$v['task_id']]['ad'][$v['c_id']]['name'] = $v['name'];
        }

        $data = array_shift($data);
        $data['ad'] = array_values($data['ad']);
        return $data;
    }
}