<?php
namespace app\modules\admin\services;

use app\components\PaginationForm;
use app\constants\UserBehaviorType;
use app\modules\log\services\UserBehaviorService;
use app\rest\exceptions\RequestParamsUnvalidatedException;

/**
 * Class UserLogService
 * @package app\modules\admin\services
 */
class UserLogService
{
    /**
     * 用户日志
     * @param array $attributes
     * @return \app\components\Pagination
     * @throws RequestParamsUnvalidatedException
     */
    public static function userBehaviorList(array $attributes = [])
    {
        if (empty($attributes['userId'])) {
            throw new RequestParamsUnvalidatedException([], '请选择要查看的用户');
        }

        $attributes['type'] = [
            UserBehaviorType::USER_BEHAVIOR_TYPE_LOGIN,
            UserBehaviorType::USER_BEHAVIOR_TYPE_REGISTER,
            UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_BASE_INFO
        ];

        return self::getLog($attributes);
    }

    /**
     * 排期日志
     * @param array $attributes
     * @return \app\components\Pagination
     * @throws RequestParamsUnvalidatedException
     */
    public static function scheduleBehaviorList(array $attributes = [])
    {
        if (empty($attributes['scheduleId'])) {
            throw new RequestParamsUnvalidatedException([], '请选择要查看的排期');
        }

        $attributes['type'] = [
            UserBehaviorType::USER_BEHAVIOR_TYPE_ADD_SCHEDULE,
            UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_SCHEDULE,
            UserBehaviorType::USER_BEHAVIOR_TYPE_DELETE_SCHEDULE
        ];

        return self::getLog($attributes);
    }

    /**
     * Kol维度排期日志
     * @param array $attributes
     * @return \app\components\Pagination
     * @throws RequestParamsUnvalidatedException
     */
    public static function kolScheduleBehaviorList(array $attributes = [])
    {
        if (empty($attributes['kolId'])) {
            throw new RequestParamsUnvalidatedException([], '请选择要查看的排期');
        }

        $attributes['type'] = [
            UserBehaviorType::USER_BEHAVIOR_TYPE_ADD_SCHEDULE,
            UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_SCHEDULE,
            UserBehaviorType::USER_BEHAVIOR_TYPE_DELETE_SCHEDULE
        ];

        return self::getLog($attributes);
    }

    /**
     * @param $attributes
     * @return \app\components\Pagination
     */
    private static function getLog($attributes)
    {
        $startTime = new \DateTime();
        $startTime = $startTime->modify('-30 days')->format('Y-m-d 00:00:00');
        $attributes['startTime'] = $startTime;

        $result = [];
        $pagination = UserBehaviorService::page($attributes);
        if ($pagination->notEmpty()) {
            foreach ($pagination->getList() as $behavior) {
                $result[] = $behavior->toArray(['operatedResult' => 'content', 'operatedAt' => 'created_at', 'operationName' => 'type_name']);
            }
        }
        $pagination->list = $result;

        return $pagination;
    }
}