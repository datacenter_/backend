<?php

namespace app\modules\admin\services;

use app\constants\Contract;
use app\modules\customer\models\Customer;
use app\constants\Platform;
use app\modules\kol\models\Kol;
use app\modules\kol\services\KolService;
use app\modules\mini\models\User;
use yii\data\Pagination;

/**
 * Class UserSearchService
 * @package app\modules\admin\services
 */
class UserSearchService
{
    /**
     * @param $params
     */
    public static function search($params)
    {
        $userModel = User::find()->alias('u')
            ->select('ub.nickname,u.*,u.id as u_id,ub.mcn_name,COUNT(k.id) as account_num')
            ->leftJoin('kt_user_base_info as ub', 'u.id = ub.user_id')
            ->leftJoin('kt_user_kol as k', 'k.user_id = u.id and k.is_deleted = -1');

        $userModel->where(['u.is_enable' => Contract::COMMON_DB_YES]);

        if (isset($params['mcn_name']) && !empty($params['mcn_name'])) {
            $userModel->andWhere(['like','ub.mcn_name', $params['mcn_name']]);
        }
        if (isset($params['nickname']) && !empty($params['nickname'])) {
            $userModel->andWhere(['like', 'ub.nickname', $params['nickname']]);
        }

        if (isset($params['cell_phone']) && !empty($params['cell_phone'])) {
            $userModel->andWhere(['u.cell_phone' => $params['cell_phone']]);
        }

        if (isset($params['created_begin']) && !empty($params['created_begin'])) {
            $userModel->andWhere(['>=', 'u.created_at', $params['created_begin']]);
        }
        if (isset($params['created_end']) && !empty($params['created_end'])) {
            $end = date('Y-m-d', strtotime('+1 day',strtotime($params['created_end'])));
            $userModel->andWhere(['<', 'u.created_at', $end]);
        }

        if (isset($params['last_date_begin']) && !empty($params['last_date_begin'])) {
            $userModel->andWhere(['>=', 'u.last_login_time', $params['last_date_begin']]);
        }
        if (isset($params['last_date_end']) && !empty($params['last_date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day',strtotime($params['last_date_end'])));
            $userModel->andWhere(['<', 'u.last_login_time', $end]);
        }

        if (isset($params['u_id']) && !empty($params['u_id'])) {
            $userModel->andWhere(['like','u.id', $params['u_id']]);
        }
        $userModel->groupBy('u.id')->orderBy('u.id desc');
        $count = $userModel->count();

        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $userModel->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        return ['list' => $model, 'total' => $count];
    }

    public static function getUserInfo($userId)
    {
        // 获取用户详情
        $userInfo = User::find()
            ->alias('u')
            ->select('ub.nickname,u.id,ub.mcn_name,u.cell_phone,u.last_login_time,u.created_at')
            ->where(['u.id' => $userId])
            ->leftJoin('kt_user_base_info as ub', 'u.id = ub.user_id')
            ->asArray()
            ->one();

        return $userInfo;
    }

    public static function getCustomerInfo($userId)
    {
        // 获取用户详情
        $CustomerInfo = Customer::find()
            ->alias('c')
            ->select('c.id,c.created_at,c.name')
            ->where(['c.user_id' => $userId])
            ->asArray()
            ->all();

        return $CustomerInfo;
    }

    public static function getTodayScheduleByKol($userId)
    {

        $todayStart = $todayEnd = date('Y-m-d');

        $today = Kol::getLatelyForTime($userId, $todayStart, $todayEnd, ['s.id as s_id','k.id as kol_id', 'k.name', 'k.platform_identification', 'k.platform_id']);
        return $today;
    }

    public static function getWeekScheduleByKol($userId)
    {
        $week = self::getWeekMyActionAndEnd();
        $startWeek = $week['week_start'];
        $endWeek = $week['week_end'];

        $week = Kol::getLatelyForTime($userId, $startWeek, $endWeek, ['s.id as s_id','k.id as kol_id', 'k.name', 'k.platform_identification', 'k.platform_id']);
        return $week;
    }

    public static function getMonthScheduleByKol($userId)
    {
        $startMonth = date("Y-m-01");
        $endMonth = date('Y-m-t', time());

        $month = Kol::getLatelyForTime($userId, $startMonth, $endMonth, ['s.id as s_id','k.id as kol_id', 'k.name', 'k.platform_identification', 'k.platform_id']);
        return $month;
    }
    /**
     * 获取账号信息
     * @param $userId
     */
    public static function getAccountInfo($userId)
    {
        $accountTotal = Kol::find()->where(['user_id' => $userId, 'is_deleted' => Contract::COMMON_DB_NO])->count();

        // 获取账号和排期信息 今日
        $today = self::getTodayScheduleByKol($userId);
        list($todayInfo, $todayTotal) = self::dataFormat($today);
        unset($today);

        // 获取账号信息 本周
        $week = self::getWeekScheduleByKol($userId);
        list($weekInfo, $weekTotal) = self::dataFormat($week);
        unset($week);

        // 获取账号信息 本月
        $month = self::getMonthScheduleByKol($userId);
        list($monthInfo, $monthTotal) = self::dataFormat($month);
        unset($month);

        return [
            'today_info' => $todayInfo,
            'week_info' => $weekInfo,
            'month_info' => $monthInfo,
            'today_total' => $todayTotal,
            'week_total' => $weekTotal,
            'month_total' => $monthTotal,
            'account_total' => $accountTotal
        ];
    }

    public static function dataFormat($data)
    {
        $info = [];
        foreach ($data as &$item) {
            if (isset($info[$item['kol_id']])) {
                $info[$item['kol_id']]['num'] += 1;
                $info[$item['kol_id']]['schedule_ids'] .= ',' . $item['s_id'];
            } else {

                $info[$item['kol_id']]['name'] = $item['name'];
                $info[$item['kol_id']]['id'] = $item['kol_id'];
                $info[$item['kol_id']]['platform_identification'] = $item['platform_identification'];
                $info[$item['kol_id']]['platform_name'] = Platform::$platformLabels[$item['platform_id']] ?? '';
                $info[$item['kol_id']]['num'] = 1;
                $info[$item['kol_id']]['schedule_ids'] = $item['s_id'];
            }
        }
        $total = count($info);
        unset($data);

        return [array_values($info), $total];
    }

    public static function getWeekMyActionAndEnd($time = '', $first = 1)
    {
        //当前日期
        if (!$time) $time = time();
        $sdefaultDate = date("Y-m-d", $time);
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $w = date('w', strtotime($sdefaultDate));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d', strtotime("$sdefaultDate -" . ($w ? $w - $first : 6) . ' days'));
        //本周结束日期
        $week_end = date('Y-m-d', strtotime("$week_start +6 days"));
        return array("week_start" => $week_start, "week_end" => $week_end);
    }
}