<?php

namespace app\modules\common;

/**
 * common module definition class
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\common\controllers';

    public function init()
    {
        parent::init();
    }
}
