<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'v1/common',
                    'rules' => [
                        'GET,POST platforms' => 'currency/get-platform'
                    ]
                ]
            ]
        ],
    ],
];

