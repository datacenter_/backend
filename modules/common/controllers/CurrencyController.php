<?php
namespace app\modules\common\controllers;

use app\controllers\GuestController;
use app\modules\common\models\CommonOperation;
use app\modules\common\Module;
use app\constants\Platform;
use app\modules\common\services\OperationLogService;

/**
 * Class LoginController
 * @package app\modules\admin\controllers
 * @property \app\modules\admin\Module $module
 */
class CurrencyController extends GuestController
{
    public function actionGetPlatform()
    {
        return Platform::$platformLabels;
    }

    public function actionOpt()
    {
        OperationLogService::add(
            OperationLogService::RESOURCE_TYPE_CLAN,
            1,
            '{}',
            1,
            OperationLogService::USER_TYPE_ADMIN,
            "1",
            "hello"
        );
    }

    public function actionCount()
    {
        return OperationLogService::operationCounts(OperationLogService::RESOURCE_TYPE_CLAN,[1]);
    }

    public function actionList()
    {
        return OperationLogService::operationLogById(OperationLogService::RESOURCE_TYPE_CLAN,1);
    }
}