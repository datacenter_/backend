<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "common_operation".
 *
 * @property int $id
 * @property int|null $user_type 操作人类型1,管理员，2机器人
 * @property int|null $user_id 操作人id
 * @property string|null $resource_type 资源
 * @property int|null $resource_id 资源id
 * @property string|null $tag 附加标签
 * @property int|null $basis_id 操作依据id，如果是依据操作提醒表
 * @property string $created_time 时间
 * @property string|null $info 操作详情
 */
class CommonOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'common_operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'user_id', 'resource_id', 'basis_id'], 'integer'],
            [['created_time'], 'safe'],
            [['info'], 'string'],
            [['resource_type', 'tag'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => '操作人类型1,管理员，2机器人',
            'user_id' => '操作人id',
            'resource_type' => '资源',
            'resource_id' => '资源id',
            'tag' => '附加标签',
            'basis_id' => '操作依据id，如果是依据操作提醒表',
            'created_time' => '时间',
            'info' => '操作详情',
        ];
    }
}
