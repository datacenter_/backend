<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_game".
 *
 * @property int $id
 * @property string|null $name 游戏名称
 * @property string|null $e_name 英文名
 * @property string|null $e_short 英文简称
 */
class EnumApiSpeed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_api_speed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id','data_speed'], 'integer'],
            [['data_level'], 'string'],
        ];
    }
}
