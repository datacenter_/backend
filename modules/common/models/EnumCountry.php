<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_country".
 *
 * @property int $id
 * @property string|null $name 中文名称
 * @property string|null $e_name 英文名称
 * @property string|null $two 2位缩写
 * @property string|null $three 3位缩写
 * @property string|null $code 国家代码
 * @property int|null $type 类别标志1国家2地区
 * @property string|null $hltv hltv别名
 * @property string|null $pandascore
 * @property int|null $created_time 添加时间
 * @property string|null $image 图片
 */
class EnumCountry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_time'], 'integer'],
            [['name', 'e_name', 'three'], 'string', 'max' => 200],
            [['two'], 'string', 'max' => 10],
            [['code', 'hltv', 'pandascore','abios'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'e_name' => 'E Name',
            'two' => 'Two',
            'three' => 'Three',
            'code' => 'Code',
            'type' => 'Type',
            'hltv' => 'Hltv',
            'pandascore' => 'Pandascore',
            'created_time' => 'Created Time',
            'image' => 'Image',
        ];
    }

    public static function getCountry($key = '')
    {
        if (!empty($key)) {
            $info = self::find()->select('id,name,type')
                ->where(['name' => $key])
                ->orderBy('e_name asc')
                ->asArray()->one();
        }
        $info = self::find()->select('id,name,e_name,type')->orderBy('e_name asc')->asArray()->all();
        $data1 = [];
        $data2 = [];
        foreach ($info as $value) {
            if ($value['type'] == 1) {
                $data1[] = $value;
            } else {
                $data2[] = $value;
            }
        }
        return array_merge($data1, $data2);
    }

    public static function getNationalityByKey($keyType, $key)
    {
        return self::find()->where([$keyType => $key])->asArray()->one();
    }
}
