<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_first_events".
 *
 * @property int $id
 * @property int|null $game_id
 * @property string|null $first_event
 */
class EnumFirstEvents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_first_events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id'], 'integer'],
            [['first_event'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'first_event' => 'First Event',
        ];
    }
}
