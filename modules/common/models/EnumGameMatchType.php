<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_game_match_type".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_ext
 * @property string|null $detail
 */
class EnumGameMatchType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_game_match_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_ext', 'detail'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_ext' => 'Name Ext',
            'detail' => 'Detail',
        ];
    }
}
