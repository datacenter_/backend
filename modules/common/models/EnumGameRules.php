<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_game_rules".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_ext
 * @property int|null $detail
 */
class EnumGameRules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_game_rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detail'], 'integer'],
            [['name', 'name_ext'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_ext' => 'Name Ext',
            'detail' => 'Detail',
        ];
    }
}
