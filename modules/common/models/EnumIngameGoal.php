<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_country".
 *
 * @property int $id
 * @property string|null $name 中文名称
 * @property string|null $e_name 英文名称
 * @property string|null $two 2位缩写
 * @property string|null $three 3位缩写
 * @property string|null $code 国家代码
 * @property int|null $type 类别标志1国家2地区
 * @property string|null $pandascore
 * @property int|null $created_time 添加时间
 */
class EnumIngameGoal extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'enum_ingame_goal';
    }

    public function rules()
    {
        return [
            [['game_id'], 'integer'],
            [['ingame_obj_id', 'game_name_cn', 'ingame_obj_type','sub_type','lane','ingame_obj_name','ingame_obj_name_cn','pandascore_name','bayes_name'], 'string'],
        ];
    }

//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'name' => '中文名称',
//            'e_name' => '英文名称',
//            'two' => '2位缩写',
//            'three' => '3位缩写',
//            'code' => '国家代码',
//            'type' => '类别标志1国家2地区',
//            'pandascore' => 'Pandascore',
//            'created_time' => '添加时间',
//        ];
//    }
}
