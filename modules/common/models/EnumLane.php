<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_lane".
 *
 * @property int $id
 * @property int|null $game_id 游戏id
 * @property string|null $name 分路名称
 * @property string|null $c_name 分路名称英文
 * @property string|null $pandascore_lan_name 分路ID
 */
class EnumLane extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_lane';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id'], 'integer'],
            [['name', 'c_name', 'pandascore_lan_name','abios_lan_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'name' => 'Name',
            'c_name' => 'C Name',
            'pandascore_lan_name' => 'Pandascore Lan Name',
            'abios_lan_name' => 'Abios Lan Name',
        ];
    }
}
