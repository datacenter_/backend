<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_game".
 *
 * @property int $id
 * @property string|null $name 游戏名称
 * @property string|null $e_name 英文名
 * @property string|null $e_short 英文简称
 */
class EnumPosition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function getPosition($key = '',$gameId)
    {
        if (!empty($key) && !empty($gameId)) {
            return self::find()->select('id,name,game_id')
                ->where(['name' => $key])
                ->andWhere(['game_id' => $gameId])
                ->asArray()->one();
        }
        return [];
    }
}
