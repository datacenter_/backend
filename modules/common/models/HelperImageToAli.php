<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "helper_image_to_ali".
 *
 * @property int $id
 * @property string|null $origin_url 源url
 * @property string|null $identity_key 标识，排重
 * @property string|null $ali_url 转存到ali的url
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 */
class HelperImageToAli extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper_image_to_ali';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'modified_time'], 'safe'],
            [['origin_url', 'ali_url','hk_url'], 'string', 'max' => 255],
            [['identity_key'], 'string', 'max' => 100],
            [['origin_url'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_url' => '源url',
            'identity_key' => '标识，排重',
            'ali_url' => '转存到ali的url',
            'created_time' => '创建时间',
            'modified_time' => '修改时间',
        ];
    }
}
