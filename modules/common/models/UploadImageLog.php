<?php

namespace app\modules\common\models;

use Yii;

class UploadImageLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upload_image_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'modified_time'], 'safe'],
            [['origin_url','info'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_url' => '源url',
            'info' => '信息',
            'created_time' => '创建时间',
            'modified_time' => '修改时间',
        ];
    }
}
