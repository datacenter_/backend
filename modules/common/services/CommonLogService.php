<?php
/**
 *
 */

namespace app\modules\common\services;


use App\Library\Context\Dimension\DimExecution;
use app\modules\common\models\CommonLog;

class CommonLogService
{
    const MAX_DEPTH = 50;
    const LOG_ARGS = 2;     // 0: 不记录函数参数；  1：记录所有函数参数； 2：仅记录顶部栈帧的函数参数。

    protected $traceMode = null;
    protected $entrySep = null;
    const SPLIT_MARK = ' ';
    public static function log($type, $info, $sourceType = "", $sourceId = 0)
    {

    }

    public static function record($type, $info, $sourceType = "", $sourceId = 0)
    {
        $log = new CommonLog();
        $log->setAttributes(
            ["type" => $type,
                "info" => is_string($info) ? $info : json_encode($info),
                'source_type' => $sourceType,
                'source_id' => $sourceId]
        );
        $log->save();
    }

    public static function recordException($exception, $sourceType = "", $sourceId = 0)
    {
        if ($exception instanceof \Exception) {
            $info = self::formatContent($exception->getTrace(), $exception->getMessage());
            $type = get_class($exception);
            self::record($type, $info);
        }

    }

    protected static function formatContent(array $stack, $message)
    {
        $frameList = array();
        $logArgs = 1;
        foreach ($stack as $frame) {
            $frameList[] = self::formatFrame($frame, $logArgs);
            $logArgs = true;
        }
        return $message . ':' . self::SPLIT_MARK
            . implode(self::SPLIT_MARK, $frameList) . self::SPLIT_MARK;
    }

    protected static function formatFrame($frame, $logArgs = false)
    {
        $fileLine = empty($frame['file']) ? '{native-code}' : "{$frame['file']}:{$frame['line']}";
        $type = empty($frame['type']) ? '' : $frame['type'];
        $call = $type ? @"{$frame['class']}{$type}{$frame['function']}" : $frame['function'];
        $argStr = '';
        if ($logArgs) {
            if (!empty($frame['args'])) {
                $argStr = json_encode($frame['args']);
                $argStr = substr($argStr, 1, strlen($argStr) - 2);
            } else {
                $argStr = '<no-args>';
            }
        }
        return "{$fileLine} {$call}({$argStr})";
    }

    public static function fwriteFile($fileName,$info) {

        $crontabPath = \Yii::$app->basePath ."/runtime/logs/crontab/" ;
        $file = "{$crontabPath}{$fileName}.log";

        if (!file_exists($crontabPath)) {
            mkdir($crontabPath, 0777,true);
            @chmod($crontabPath, 0777);
        }


        if (file_exists($file)) {
            $myfile = fopen($file, "a");
        }else{
            $myfile = fopen($file, "w");
        }

        fwrite($myfile, $info);
        fclose($myfile);
    }

    /**
     * @param $e
     * @param $tag
     * @param $type 'levels' => 'error',
     */
    public static function setException($e,$tag,$type){

        if ($type == 'error') {

            $errs = [
                'tag' => $tag,
                'type' => $type,
                'error_info' => [
                    "time" => date("Y-m-d H:i:s"),
                    "msg" => $e->getMessage(),
                    "file" => $e->getFile(),
                    "line" => $e->getLine(),
                    "code" => $e->getCode(),
                ],
            ];
        }else{
            $errs = [
                'tag' => $tag,
                'type' => $type,
                'info' => $e,
                "time" => date("Y-m-d H:i:s"),
            ];
        }

        $errorInfo = json_encode($errs, JSON_UNESCAPED_UNICODE) . "\r";

        $fileName = "crontab_" . date("Ymd");
        CommonLogService::fwriteFile($fileName, $errorInfo);
    }

}