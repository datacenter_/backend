<?php
/**
 *
 */

namespace app\modules\common\services;


class Consts
{
    const ORIGIN_FEIJING = "feijing";
    const ORIGIN_ABIOS = "abios";
    const ORIGIN_PANDASCORE = "pandascore";
    const ORIGIN_ORANGE = "orange";
    const ORIGIN_FIVE = "five";
    const ORIGIN_RIOTGAMES = "riotgames";
    const ORIGIN_HLTV = "hltv";
    const ORIGIN_VALVE ="valve";
    const ORIGIN_BAYES = "bayes";
    const ORIGIN_BAYES2 = "bayesplus";
    const ORIGIN_RADAR_PURPLE = "radarpurple";
    const USER_TYPE_ADMIN = 1;//管理员:手动
    const USER_TYPE_ROBOT = 2;//机器人:自动

    // 绑定类型
    const RESOURCE_TYPE_BING_TEAM = 'bing_team';
    const RESOURCE_TYPE_BING_PLAYER = 'bing_player';
    const RESOURCE_TYPE_BING_TOURNAMENT = 'bing_tournament';
    const RESOURCE_TYPE_BING_MATCH = 'bing_match';
    const RESOURCE_TYPE_BING_LOL_CHAMPION = 'bind_lol_champion';
    const RESOURCE_TYPE_BING_LOL_ITEM = 'bind_lol_item';
    const RESOURCE_TYPE_BIND_DOTA2_HERO = 'bind_dota2_hero';
    const RESOURCE_TYPE_BIND_DOTA2_ABILITY = 'bind_dota2_ability';
    const RESOURCE_TYPE_BIND_DOTA2_ITEM = 'bind_dota2_item';
    const RESOURCE_TYPE_BIND_LOL_ABILITY = 'bind_lol_ability';

    // 资源类型
    const RESOURCE_TYPE_CLAN = "clan";
    const RESOURCE_TYPE_TEAM = "team";
    const RESOURCE_TYPE_SERIES = "series";
    const RESOURCE_TYPE_PLAYER = "player";
    const RESOURCE_TYPE_TOURNAMENT = "tournament";
    const RESOURCE_TYPE_MATCH = "match";
    const RESOURCE_TYPE_SON_TOURNAMENT = "son_tournament"; //子赛事
    const RESOURCE_TYPE_GRANDSON_TOURNAMENT = "grandson_tournament"; //孙赛事
    const RESOURCE_TYPE_METADATA = "metadata";
    const RESOURCE_WEBSOCKET_MATCH_DATA = "websocket_match_data";  //比赛实时数据
    const RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI = "websocket_match_data_api";  //比赛实时数据
    // 系统资源
    const RESOURCE_TYPE_SYS_CONFIG_UPDATE = "sys_update_config_default";
    const RESOURCE_TYPE_SYS_DATA_RESOURCE_UPDATE_CONFIG = "sys_data_resource_update_config";
    const RESOURCE_TYPE_SYS_DATA_RESOURCE_TO_DO = "data_resource_to_do";
    const RESOURCE_TYPE_ERROR_MANY_TEAM = "error_many_team";

    // 如果需要，添加加match的类型子类

    // 数据类型
    const TAG_TYPE_CORE_DATA = "core_data";
    const TAG_TYPE_BASE_DATA = "base_data";
    const TAG_TYPE_REAL_TIME = "real_time";
    const TAG_TYPE_TEAM_PLAYER = "team_player";
    const TAG_TYPE_ATTEND_TEAM = "attend_team";  //参赛战队
    const TAG_TYPE_CREATE_DATA = "create_data"; // 比赛创建
    const TAG_TYPE_BINDING_DATA = "binding_data"; // 比赛创建
    const TAG_TYPE_UPDATE_DATA = "update_data"; // 数据更新
    const TAG_TYPE_SCORE_DATA = "score_data";   // 比赛更新

    const TAG_TYPE_TOURNAMENT_TEAM_RELATION = 'tournament_team_relation'; //参赛条件
    const TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE= "tournament_price_distribute";
    // 绑定状态
    const BINGING_STATUS_WISH_ADD = '1';
    const BINGING_STATUS_WISH_BINDING = '2';
    const BINGING_STATUS_WISH_BINDING_MORE = '3';
    const BINGING_STATUS_WISH_DONE = '4';

    // 修改类型
    const UPDATE_TYPE_AUTO = '3';
    const UPDATE_TYPE_MANUAL = '2';
    const UPDATE_TYPE_NOTHING = '1';

    const TODO_DEAL_STATUS_PADDING = 1;
    const TODO_DEAL_STATUS_DOING = 2;
    const TODO_DEAL_STATUS_DONE = 3;
    const TODO_DEAL_STATUS_NOTHING = 4;

    const METADATA_TYPE_KOG_CHAMPION = 'kog_champion';  //王者荣耀-英雄
    const METADATA_TYPE_KOG_ABILITY = 'kog_ability';  //王者荣耀-技能
    const METADATA_TYPE_KOG_ITEM = 'kog_item';  //王者荣耀-道具
    const METADATA_TYPE_KOG_RUNE = 'kog_rune';  //王者荣耀-符文
    const METADATA_TYPE_KOG_SUMMONER_SPELL = 'kog_summoner_spell';  //王者荣耀-召唤师技能


    const METADATA_TYPE_LOL_ABILITY = 'lol_ability';  //技能
    const METADATA_TYPE_LOL_CHAMPION = 'lol_champion';  //英雄
    const METADATA_TYPE_LOL_ITEM = 'lol_item';  //道具
    const METADATA_TYPE_LOL_RUNE = 'lol_rune'; //符文
    const METADATA_TYPE_LOL_SUMMONER_SPELL = 'lol_summoner_spell';  //召唤师技能

    const METADATA_TYPE_DOTA2_HERO = 'dota2_hero';  //英雄
    const METADATA_TYPE_DOTA2_ITEM = 'dota2_item'; //道具
    const METADATA_TYPE_DOTA2_ABILITY = 'dota2_ability'; //技能
    const METADATA_TYPE_DOTA2_TALENT = 'dota2_talent';  //天赋
    const METADATA_TYPE_CSGO_MAP = 'csgo_map';  //cs地图
    const METADATA_TYPE_CSGO_WEAPON ='csgo_weapon'; //cs武器
    // 分组 阶段
    const TOURNAMENT_GROUP = 'group';
    const TOURNAMENT_STAGE = 'stage';

    // 码表类型
    const ENUM_COUNTRY = 'enum_country';
    const ENUM_GAME = 'enum_game';
    const ENUM_GAME_MATCH_TYPE = 'enum_game_match_type';
    const ENUM_POSITION = 'enum_position';
    const ENUM_GAME_RULES = 'enum_game_rules';
    const ENUM_MATCH_STATE = 'enum_match_state';
    const ENUM_CSGO_WEAPON = 'enum_csgo_weapon';

    //match
    const MARCH_BATTLE_INFO = "match_battle_info";
    const MARCH_BATTLE_EVENTS = "match_battle_events";
    const MARCH_BATTLE_IS_FORFEIT = "match_battle_is_forfeit";
    const MARCH_BATTLE_REDIS_EXPIRE_TIME = 432000;

    //battle
    const RESOURCE_TYPE_BATTLE = "battle";

    //任务执行类型
    const  EXECUTE_TYPE_CTB = "ctb";  //自动
    const  EXECUTE_TYPE_OPT = "opt";  //手工

    //redis
    const MATCH_ALL_INFO = 'match_all_info';
    const MATCH_REAL_TIME_INFO = 'match_real_time_info';
    const MATCH_RELATION_ORIGIN_MATCH = 'match_relation_origin_match';
    const GAME_MAPS = 'maps';
    //LOL player_kill 事件
    const EVENT_PLAYER_KILL = 50;
    const EVENT_UNKNOWN = 51;


    //match日志类型
    const  MATCH_CREATION = 'match_creation';
    const  MATCH_UPDATE = 'match_update';
    const  MATCH_DELETION = 'match_deletion';
    const  MATCH_RECOVERY = 'match_recovery';
    const  MATCH_FIXTURE_CHANGE = 'match_fixture_change';
    const  MATCH_FIXTURE_TEAMCHANGE = 'match_fixture_teamchange';

    //match日志类型
    const  BATTLE_CREATION = 'battle_creation';
    const  BATTLE_UPDATE = 'battle_update';
    const  BATTLE_DELETION = 'battle_deletion';
    const  BATTLE_RECOVERY = 'battle_recovery';

}