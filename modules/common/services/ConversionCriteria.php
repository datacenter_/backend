<?php


namespace app\modules\common\services;


class ConversionCriteria
{
    // 北京时间转化为标准时间
    public static function DataTimeConversionCriteria($dataTime)
    {
        if(!$dataTime){
            return null;
        }
        if(strstr($dataTime,'.')){
            $dataTime = explode('.', $dataTime)[0];
        }
        $strtotimeDataTime = strtotime($dataTime);
        return date('Y-m-d\TH:i:s\Z', $strtotimeDataTime - 8*60*60);
    }

    // normal 和 remove
    public static function normalOrRemoveString($state)
    {
        switch ($state) {
            case 1:
                $statusValue = "normal";
                break;
            default:
                $statusValue = "remove";
                break;
        }
        return $statusValue;
    }

    // 检测true false null
    public static function issetTrueFalseType($value) {
        switch ($value){
            case 1:
                $statusValue = true;
                break;
            case 2:
                $statusValue = false;
                break;
            default:
                $statusValue = null;
                break;
        }
        return $statusValue;
    }

    // 选手resize
    public static function playerResizeConversion($playerImage) {
        return ImageConversionHelper::showMfitSizeConversion($playerImage,160,0) ?? null;
    }

    // 检测json格式
    public static function issetJsonField($data,$issetField = []) {
        $res = json_decode($data, true);
        $error = json_last_error();
        if (!empty($error)) {
            return [];
        }
        $dataArray = [];
        foreach ($res as $k=>$val){
//            foreach ($issetField as $v){
//                if(!isset($val[$v])){
//                    return [];
//                }
//            }
            $dataArray[] = $val;
        }

        return $dataArray;
    }
}
