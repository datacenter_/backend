<?php
/**
 *
 */

namespace app\modules\common\services;

use Elasticsearch\ClientBuilder;

class EsService
{
    private $client;
    private static $_instance;
    private function __construct()
    {
        if (\Yii::$app->params['refresh_key'] == "Singapore") {
            $setHosts = [
                'host'   => env('ES_HOST_SPE'),
                'port'   => env('ES_PORT_SPE'),
                'scheme' => env('ES_SCHEME_SPE'),
                'user'   => env('ES_USER_SPE'),
                'pass'   => env('ES_PASS_SPE')
            ];
        } else {
            $setHosts = [
                'host'   => env('ES_HOST'),
                'port'   => env('ES_PORT'),
                'scheme' => env('ES_SCHEME'),
                'user'   => env('ES_USER'),
                'pass'   => env('ES_PASS')
            ];
        }
        $this->client = ClientBuilder::create()
            ->setHosts([$setHosts])
            ->setConnectionPool('\Elasticsearch\ConnectionPool\SimpleConnectionPool')
            ->setRetries(10)
            ->build();
    }

    // 私有属性的克隆方法 防止被克隆
    public function __clone(){
        trigger_error('Clone is not allowed !');
    }

    public static function initConnect()
    {
        if(! (self::$_instance instanceof self) )
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function init()
    {
        return self::initConnect()->getClent();
    }

    public function getClent()
    {
        return $this->client;
    }

    // 检测索引是否存在
    public function issetEsIndex($indexName){
        $indexParam = [
            'index' => $indexName
        ];
        return $this->init()->indices()->exists($indexParam);
    }

    // 创建索引类型
    public function createEsIndex($indexName,$indexStructure){
        // 检测索引是否存在
        $index = $this->issetEsIndex($indexName);
        if(!$index) {
            $this->init()->indices()->create($indexStructure);
        }
    }

    // 添加文档数据
    public function createIndexAdd($indexName,$apiId,$esInfo){
        $indexParams = [
            'index' => $indexName,
            'id' => $apiId,
            'body' => $esInfo,
            'client' => [
                'timeout' => 10,
                'connect_timeout' => 10
            ]
        ];
        $this->init()->index($indexParams);
    }

    // 批量添加
    public function createIndexAddAll($indexName,$esInfos){
        $indexParams = [
            'index' => $indexName,
            'body' => $esInfos,
            'client' => [
                'timeout' => 10,
                'connect_timeout' => 10
            ]
        ];
        $this->init()->bulk($indexParams);
    }

    // 修改文档数据
    public function updateIndexAdd($indexName,$apiId,$esInfo){
        $indexParams = [
            'index' => $indexName,
            'id' => $apiId,
            'body' => $esInfo
        ];
        $this->init()->update($indexParams);
    }

    // 删除索引
    public function deleteIndex($indexName)
    {
        // 检测索引是否存在
        $index = $this->issetEsIndex($indexName);
        if($index){
            $indexParam = [
                'index' => $indexName
            ];
            $this->init()->indices()->delete($indexParam);
        }
        return true;
    }

    // 删除索引文档数据
    public function deleteFile($indexName,$apiId)
    {
        try {
            $fileInfo = $this->getFile($indexName,$apiId);
            if(!empty($fileInfo)) {
                $indexParam = [
                    'index' => $indexName,
                    'id' => $apiId
                ];
                return $this->init()->delete($indexParam);
            }else{
                return;
            }
        } catch (\Exception $e) {
            return;
        }
    }

    // 获取某条数据
    public function getFile($indexName,$apiId)
    {
        try {
            $params = [
                'index' => $indexName,
                'id' => (int)$apiId
            ];
            $info = $this->init()->get($params);
            return $info['_source'];
        } catch (\Exception $e) {
            return null;
        }
    }

    // 搜索返回一条数据
    public function searchOne($indexName,$field,$apiId)
    {
        $params['index'] = $indexName;
        $params['body']['query']['match'][$field] = (int)$apiId;
        $info = $this->init()->search($params);
        return $info['hits']['hits'][0]['_source'];
    }

    // es检索数据
    public function getIndexList($index_info,$offset=0,$limit=35,$conditions=[],$order=[],$columns=[])
    {
        //match 模糊匹配多个
        //term是代表完全匹配  比如数字，日期，布尔值或 not_analyzed 的字符串
        //bool联合查询: must,should,must_not
        $params = [
            "track_total_hits" => true, // 让10000条以后可以显示
            '_source' => $columns,
            'from'    => $offset,
            'size'    => $limit,
        ];
        // 排序
        if(isset($order['field'])&&isset($order['sort'])){
            $params['sort'] = [
                $order['field'] => [
                    'order' => $order['sort'],
                ],
            ];
        }else{
            if(!empty($order)){
                $params['sort'] = $order;
            }
        }
        // 筛选
        if (!empty($conditions)) {
            foreach ($conditions as $v) {
                switch ($v['type']) {
                    case 'between':
                        $params['query']['bool']['must'][]['range'][$v['field']]['gte'] = $v['value'][0];
                        $params['query']['bool']['must'][]['range'][$v['field']]['lte'] = $v['value'][1];
                        break;
                    case '>':
                        $params['query']['bool']['filter'][]['range'][$v['field']]['gt'] = $v['value'];
                        break;
                    case '<':
                        $params['query']['bool']['must'][]['range'][$v['field']]['lt'] = $v['value'];
                        break;
                    case '>=':
                        $params['query']['bool']['must'][]['range'][$v['field']]['gte'] = $v['value'];
                        break;
                    case '<=':
                        $params['query']['bool']['must'][]['range'][$v['field']]['lte'] = $v['value'];
                        break;
                    case '=':
                        $params['query']['bool']['must'][]['match'][$v['field']] = $v['value'];
                        break;
                    case '==':
                        // 查询多个值
                        $params['query']['bool']['must'][]['terms'][$v['field']] = $v['value'];
                        break;
                    case '===':
                        // 查询子数组的值
                        $params['query']['bool']['must'][]['term'][$v['field']]['value'] = $v['value'];
                        break;
                    case '!=':
                        $params['query']['bool']['must_not'][]['term'][$v['field']] = $v['value'];
                        break;
                    case 'in':
                        if (!empty($v['value']) && is_array($v['value'])) {
                            foreach ($v['value'] as $m => $n) {
                                $params['query']['bool']['must'][] = array(
                                    'term' => array(
                                        $v['field'] => $n,
                                    ),
                                );
                            }
                        }
                        break;
                    case 'not in':
                        if (!empty($v['value']) && is_array($v['value'])) {
                            foreach ($v['value'] as $m => $n) {
                                $params['query']['bool']['must_not'][] = array(
                                    'term' => array(
                                        $v['field'] => $n,
                                    ),
                                );
                            }
                        }
                        break;
                    case 'like':
                        $params['query']['bool']['must'][]['wildcard'][$v['field']] = "*".$v['value']."*";
                        break;
                    default:
                        return false;
                        break;
                }
            }
        }
        $searchParams = [
            'index'  => $index_info,
            'body' => $params,
            'client' => [
                'timeout'         => 10,
                'connect_timeout' => 10
            ]
        ];
        $searchResponse = $this->init()->search($searchParams);
        $reData['list'] = [];
        if (!empty($searchResponse['hits']['hits'])) {
            foreach ($searchResponse['hits']['hits'] as $k => $v) {
                $reData['list'][$k] = $v['_source'];
            }
        }
        $reData['count'] = !empty($searchResponse['hits']['total']['value']) ? $searchResponse['hits']['total']['value'] : 0;
        return $reData;
    }
}
