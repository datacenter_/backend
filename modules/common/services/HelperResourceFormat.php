<?php

namespace app\modules\common\services;

use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\models\EnumGameMatchType;
use app\modules\common\models\EnumGameRules;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\metadata\models\EnumCsgoWeapon;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\org\models\TeamIntroduction;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\common\models\EnumPosition;
use app\modules\org\models\Clan;
use app\modules\common\models\EnumMatchState;
use app\rest\exceptions\BusinessException;
use function PHPUnit\Framework\isEmpty;

class HelperResourceFormat
{
    public static function validateAndFormat($input, $resourceType, $tag = 'default', $gameId, $withOldKey = true)
    {
        $config = self::getConfigByType($resourceType, $tag, $gameId);

        $checkInfo = self::runConfig($input, $config);

        $output = $checkInfo['output'];
        $check = $checkInfo['check'];
        if ($withOldKey) {
            $output = array_merge($input, $output);
        }

        $unset = $checkInfo['unset'];
        foreach ($unset as $keyName) {
            unset($output[$keyName]);
        }
        // 根据resource_type,和tag获取到配置，根据配置去检查或者格式化数组
        return [
            'output' => $output,
            "check" => $check,
            'validate' => $config,
            'unsets' => $unset
        ];
    }

    /**
     * @param $input
     * @param $resourceType
     * @param string $tag
     * @param $gameId
     * @param bool $withOldKey
     * @return mixed
     * 这里不再返回验证结果，直接返回处理之后的diffinfo
     */
    public static function validateAndFormatDiffInfo($input, $resourceType, $tag = 'default', $gameId, $withOldKey = true)
    {
        // 这里对diff信息进行降维，
        $lowArray = [];
        foreach ($input as $key => $val) {
            $lowArray[$key] = $val['after'];
        }
        //调用validate
        $checkInfo = self::validateAndFormat($lowArray, $resourceType, $tag, $gameId, $withOldKey);
        // 是否抛出错误
        if (count($checkInfo['check'])) {
            throw new BusinessException([], implode(',', $checkInfo['check']));
        }
        //然后升维
        $returnInfo = $input;
        foreach ($returnInfo as $key => &$val) {
            if (array_key_exists($key,$checkInfo['output'])) {
                $val['after'] = $checkInfo['output'][$key];
            }
        }
        foreach ($checkInfo['unsets'] as $keyName) {
            unset($returnInfo[$keyName]);
        }
        return $returnInfo;
    }

    public static function runConfig($input, $config)
    {
        $output = [];
        $check = [];
        $unset = [];
        foreach ($config as $key => $val) {
            if (array_key_exists($key,$input) && is_callable($val)) {
                $checkInfo = $val($input);
                $output[$key] = $checkInfo['output'];
                if ($checkInfo['check']) {
                    $check[$key] = $checkInfo['check'];
                }
                if (isset($checkInfo['unset']) && $checkInfo['unset']) {
                    $unset[] = $key;
                }
                continue;
            }
            if (is_string($val)) {
                $output[$key] = $input[$val];
                continue;
            }

        }
        return [
            'output' => $output,
            'check' => $check,
            'unset' => $unset,
            'validate' => count($check) == 0
        ];
    }

    /**
     * 获取对应类型的对应政策
     * $resourceType        类型
     * $tag                 标签
     */
    public static function getConfigByType($resourceType, $tag = "default", $gameId)
    {

        // 配置
        $configMap = [

            //俱乐部
            Consts::RESOURCE_TYPE_CLAN => [
                //数据源新增
                'add' => [
                    //名称
                    'name' => function ($params) {
                        $nameInfo = self::checkName($params['name'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $nameInfo['after'],
                            'check' => $nameInfo['error_msg'],
                            'unset' => !$nameInfo['is_right'],
                            'error_msg' => $nameInfo['error_msg'],
                        ];
                    },
                    //国家地区
                    'country' => function ($params) {
                        if(self::checkIsStringNull($params['country'])){
                            return self::formatCheckResult($params['country'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['country'])){
                            return self::formatCheckResult($params['country'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['country'],Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($params['country']);
                        }else{
                            return self::formatCheckResult($params['country'],false,true);
                        }
                    },
                    //全称
                    //简称
                    //别名:逗号分隔,增量更新
                    'alias' => function ($params) {
                        $aliasInfo = self::checkAlias($params['alias'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $aliasInfo['after'],
                            'check' => $aliasInfo['error_msg'],
                            'unset' => !$aliasInfo['is_right'],
                            'error_msg' => $aliasInfo['error_msg'],
                        ];
                    },
                    //slug
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    //图标(存服务器)
                    'image' => function ($params) {
                        $imageInfo = self::checkImage($params['image'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $imageInfo['after'],
                            'check' => $imageInfo['error_msg'],
                            'unset' => !$imageInfo['is_right'],
                            'error_msg' => $imageInfo['error_msg'],
                        ];
                    },
                    //简介(英文)
                    //简介(中文)
                    //下属战队
                    // 是否删除
                    'deleted' => function ($params) {
                        $deletedInfo = self::checkDeleted($params['deleted'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $deletedInfo['after'],
                            'check' => $deletedInfo['error_msg'],
                            'unset' => !$deletedInfo['is_right'],
                            'error_msg' => $deletedInfo['error_msg'],
                        ];
                    },
                ],
                //数据源更新
                'change' => [
                    //名称
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    //国家地区
                    'country' => function ($params) {
                        if(self::checkIsStringNull($params['country'])){
                            return self::formatCheckResult($params['country'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['country'])){
                            return self::formatCheckResult($params['country'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['country'],Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($params['country']);
                        }else{
                            return self::formatCheckResult($params['country'],false,true);
                        }
                    },
                    //全称
                    //简称
                    //别名:逗号分隔,增量更新,TODO
                    'alias' => function ($params) {
                        $aliasInfo = self::checkAlias($params['alias'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_CHANGE);
                        return [
                            'output' => $aliasInfo['after'],
                            'check' => $aliasInfo['error_msg'],
                            'unset' => !$aliasInfo['is_right'],
                            'error_msg' => $aliasInfo['error_msg'],
                        ];
                    },
                    //slug
                    'slug' => function ($params) {
                        $slugInfo = self::checkSlug($params['slug'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_CHANGE);
                        return [
                            'output' => $slugInfo['after'],
                            'check' => $slugInfo['error_msg'],
                            'unset' => !$slugInfo['is_right'],
                            'error_msg' => $slugInfo['error_msg'],
                        ];
                    },
                    //图标(存服务器)
                    'image' => function ($params) {
                        $imageInfo = self::checkImage($params['image'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_CHANGE);
                        return [
                            'output' => $imageInfo['after'],
                            'check' => $imageInfo['error_msg'],
                            'unset' => !$imageInfo['is_right'],
                            'error_msg' => $imageInfo['error_msg'],
                        ];
                    },
                    //简介(英文)
                    //简介(中文)
                    //下属战队
                    // 是否删除
                    'deleted' => function ($params) {
                        $deletedInfo = self::checkDeleted($params['deleted'], Consts::RESOURCE_TYPE_CLAN, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $deletedInfo['after'],
                            'check' => $deletedInfo['error_msg'],
                            'unset' => !$deletedInfo['is_right'],
                            'error_msg' => $deletedInfo['error_msg'],
                        ];
                    },
                ],
            ],
            //选手
            Consts::RESOURCE_TYPE_PLAYER => [
                //数据源新增
                'add' => [
                    //选手名称
                    'nick_name' => function ($params) {
                        if(self::checkIsStringNull($params['nick_name'])){
                            return self::formatCheckResult($params['nick_name'],false,true);
                        }else{
                            return self::formatCheckResult($params['nick_name'],false,false);
                        }
                    },
                    //名称
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            $params['name'] = null;
                            return self::formatCheckResult($params['name'],false,false);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    // 选手国籍
                    'nationality' => function ($params) {
                        $v=$params['nationality'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    // 姓名(英文)
                    // 姓名(中文)
                    // 选手角色
                    'role' => function ($params) use ($gameId) {
                        $v=$params['role'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_POSITION,$gameId)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'team' => function ($params) {
                          return self::formatCheckResult($params['team'],false,true);
                    },
                    // Slug
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    //图标(存服务器)
                    'image' => function ($params) {
                        $imageInfo = self::checkImage($params['image'], Consts::RESOURCE_TYPE_PLAYER, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $imageInfo['after'],
                            'check' => $imageInfo['error_msg'],
                            'unset' => !$imageInfo['is_right'],
                            'error_msg' => $imageInfo['error_msg'],
                        ];
                    },
                    'birthday' => function ($params) {
                        if(self::checkIsStringNull($params['birthday'])){
                            $params['birthday'] = null;
                            return self::formatCheckResult($params['birthday'],false,false);
                        }
                        if(self::checkIsNumberZero($params['birthday'])){
                            return self::formatCheckResult($params['birthday'],false,true);
                        }
                        if(date('Y-m-d', strtotime($params['birthday'])) != "1970-01-01"){
                            return self::formatCheckResult($params['birthday'],false,false);
                        }else{
                            return self::formatCheckResult($params['birthday'],false,true);
                        }
                    },
                    // 英文简介
                    'introduction' => function ($params) {
                        if(self::checkIsStringNull($params['introduction'])){
                            $params['introduction'] = null;
                            return self::formatCheckResult($params['introduction']);
                        }else{
                            return self::formatCheckResult($params['introduction']);
                        }
                    },
                    'introduction_cn' => function ($params) {
                        if(self::checkIsStringNull($params['introduction_cn'])){
                            $params['introduction_cn'] = null;
                            return self::formatCheckResult($params['introduction_cn']);
                        }else{
                            return self::formatCheckResult($params['introduction_cn']);
                        }
                    },
                    // 是否删除
                    'deleted' => function ($params) {
                        if(self::checkIsStringNull($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        if(self::checkIsNumberZero($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        else{
                            return self::formatCheckResult($params['deleted']);
                        }
                    },
                ],
                //数据源更新
                'change' => [
                    //昵称
                    'nick_name' => function ($params) {
                        if(self::checkIsStringNull($params['nick_name'])){
                            return self::formatCheckResult($params['nick_name'],false,true);
                        }else{
                            return self::formatCheckResult($params['nick_name'],false,false);
                        }
                    },
                    //名称
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            $params['name'] = null;
                            return self::formatCheckResult($params['name'],false,false);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },

                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    // 选手国籍
                    'nationality' => function ($params) {
                        $v=$params['nationality'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 选手角色
                    'role' => function ($params) use ($gameId) {
                        $v=$params['role'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_POSITION,$gameId)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // Slug
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'team' => function ($params) {
                        return self::formatCheckResult($params['team'],false,true);
                    },
                    //图标(存服务器)
                    'image' => function ($params) {
                        $isBlank = true;
                        $isRight = true;
                        $after = $params['image'];
                        if (isset($params['image']) && empty($after)) {
                            $isBlank = false;
                            $isRight = true;
                        } else {
                            $isBlank = true;
                            $isRight = true;
                        }
                        return [
                            'output' => $after,
                            'check' => false,//是否报错
                            'unset' => !$isRight,
                            'error_msg' => '',
                        ];
                    },
                    // 生日
                    'birthday' => function ($params) {
                        if(self::checkIsStringNull($params['birthday'])){
                            $params['birthday'] = null;
                            return self::formatCheckResult($params['birthday'],false,false);
                        }
                        if(self::checkIsNumberZero($params['birthday'])){
                            return self::formatCheckResult($params['birthday'],false,true);
                        }
                        if(date('Y-m-d', strtotime($params['birthday'])) != "1970-01-01"){
                            return self::formatCheckResult($params['birthday'],false,false);
                        }else{
                            return self::formatCheckResult($params['birthday'],false,true);
                        }
                    },
                    // 简介(英文)(标准表中无该字段)
                    'introduction' => function ($params) {
                        if(self::checkIsStringNull($params['introduction'])){
                            $params['introduction'] = null;
                            return self::formatCheckResult($params['introduction']);
                        }else{
                            return self::formatCheckResult($params['introduction']);
                        }
                    },
                    'introduction_cn' => function ($params) {
                        if(self::checkIsStringNull($params['introduction_cn'])){
                            $params['introduction_cn'] = null;
                            return self::formatCheckResult($params['introduction_cn']);
                        }else{
                            return self::formatCheckResult($params['introduction_cn']);
                        }
                    },
                    // 是否删除
                    'deleted' => function ($params) {
                        if(self::checkIsStringNull($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        if(self::checkIsNumberZero($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        else{
                            return self::formatCheckResult($params['deleted']);
                        }
                    }
                ],
            ],
            // 战队
            Consts::RESOURCE_TYPE_TEAM => [
                'add' => [
                    // 名称
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    // 俱乐部ID
                    'clan_id' => function ($params) {
                        $clanIdInfo = self::checkClanId($params['clan_id'], Consts::RESOURCE_TYPE_TEAM, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $clanIdInfo['after'],
                            'check' => $clanIdInfo['error_msg'],
                            'unset' => !$clanIdInfo['is_right'],
                            'error_msg' => $clanIdInfo['error_msg'],
                        ];
                    },
                    // 国家地区
                    'country' => function ($params) {
                        $v=$params['country'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛区
                    'region' => function ($params) {
                        $v=$params['region'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 简称
                    // 别名TODO:增量更新
                    // 图标TODO:存到服务器
                    'logo' => function ($params) {
                        $logoInfo = self::checkImage($params['logo'], Consts::RESOURCE_TYPE_TEAM, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $logoInfo['after'],
                            'check' => $logoInfo['error_msg'],
                            'unset' => !$logoInfo['is_right'],
                            'error_msg' => $logoInfo['error_msg'],
                        ];
                    },
                    // 世界排名(标准表中无该字段)
                    'world_ranking' => function ($params) {
                        if(self::checkIsStringNull($params['world_ranking'])){
                            $params['world_ranking'] = null;
                            return self::formatCheckResult($params['world_ranking'],false);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['world_ranking'])){
                            $params['world_ranking'] = (string)self::getStringToNumber($params['world_ranking']);
                            return self::formatCheckResult($params['world_ranking']);
                        }else{
                            return self::formatCheckResult($params['world_ranking'],false,true);
                        }
                    },
                    // 选手平均年龄
                    'average_player_age' => function ($params) {
                        if(self::checkIsStringNull($params['average_player_age'])){
                            $params['average_player_age'] = null;
                            return self::formatCheckResult($params['average_player_age'],false,false);
                        }
                        if(self::checkIsNumberZero($params['average_player_age'])){
                            return  self::formatCheckResult($params['average_player_age'],false,true);
                        }
                        if(self::checkIsAverageNum($params['average_player_age'])){
                            return self::formatCheckResult($params['average_player_age']);
                        } else{
                            return self::formatCheckResult($params['average_player_age'],false,true);
                        }
                    },
                    'total_earnings' => function ($params) {
                        if(self::checkIsStringNull($params['total_earnings'])){
                            $params['total_earnings'] = null;
                            return self::formatCheckResult($params['total_earnings']);
                        }
                        if(self::checkIsNumberZero($params['total_earnings'])){
                            return self::formatCheckResult($params['total_earnings'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_earnings'])){
                            $params['total_earnings'] = (string)self::getStringToNumber($params['total_earnings']);
                            return self::formatCheckResult($params['total_earnings']);
                        }else{
                            return self::formatCheckResult($params['total_earnings'],false,true);
                        }
                    },
                    // 赛区(标准表中无该字段)
                    // 历史选手,TODO:应用原方法
                    // 简介(英文)(标准表中无该字段)
                    // 简介(中文)(标准表中无该字段)
                    // 战队选手,TODO:应用原方法
                    // 选手状态(标准表中无该字段)
                    // 是否删除
                    'deleted' => function ($params) {
                        $deletedInfo = self::checkDeleted($params['deleted'], Consts::RESOURCE_TYPE_TEAM, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $deletedInfo['after'],
                            'check' => $deletedInfo['error_msg'],
                            'unset' => !$deletedInfo['is_right'],
                            'error_msg' => $deletedInfo['error_msg'],
                        ];
                    }
                ],
                'change' => [
                    // 名称
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    // 俱乐部ID
                    'clan_id' => function ($params) {
                        return self::formatCheckResult($params['clan_id'],false,true);
                    },
                    // 国家地区
                    'country' => function ($params) {
                        $v=$params['country'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 全称 没验证 全都通过
                    'full_name' => function ($params) {
                        if(self::checkIsStringNull($params['full_name'])){
                            $params['full_name'] = null;
                            return self::formatCheckResult($params['full_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['full_name'],false,false);
                        }
                    },
                    // 简称 没验证 全都通过
                    'short_name' => function ($params) {
                        if(self::checkIsStringNull($params['short_name'])){
                            $params['short_name'] = null;
                            return self::formatCheckResult($params['short_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['short_name'],false,false);
                        }
                    },
                    // 别名TODO:增量更新 直接unset
                    'alias' => function ($params) {
                        return self::formatCheckResult($params['alias'],false,true);
                    },
                    // Slug  直接unset
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    // 图标TODO:存到服务器 没验证 直接通过
                    'logo' => function ($params) {
                        $isBlank = true;
                        $isRight = true;
                        $after = $params['logo'];
                        if (isset($params['logo']) && !empty($after)) {
                            $isBlank = false;
                            $isRight = true;
                        } else {
                            $isBlank = true;
                            $isRight = true;
                        }
                        return [
                            'output' => $after,
                            'check' => false,//是否报错
                            'unset' => !$isRight,
                            'error_msg' => '',
                        ];
                    },
                    // 赛区
                    'region' => function ($params) {
                        $v=$params['region'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 世界排名(标准表中无该字段)
                    'world_ranking' => function ($params) {
                        if(self::checkIsStringNull($params['world_ranking'])){
                            $params['world_ranking'] = null;
                            return self::formatCheckResult($params['world_ranking'],false);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['world_ranking'])){
                            $params['world_ranking'] = (string)self::getStringToNumber($params['world_ranking']);
                            return self::formatCheckResult($params['world_ranking']);
                        }else{
                            return self::formatCheckResult($params['world_ranking'],false,true);
                        }
                    },
                    // 总奖金
                    'total_earnings' => function ($params) {
                        if(self::checkIsStringNull($params['total_earnings'])){
                            $params['total_earnings'] = null;
                            return self::formatCheckResult($params['total_earnings']);
                        }
                        if(self::checkIsNumberZero($params['total_earnings'])){
                            return self::formatCheckResult($params['total_earnings'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_earnings'])){
                            $params['total_earnings'] = (string)self::getStringToNumber($params['total_earnings']);
                            return self::formatCheckResult($params['total_earnings']);
                        }else{
                            return self::formatCheckResult($params['total_earnings'],false,true);
                        }
                    },
                    // 选手平均年龄(标准表中无该字段)
                    'average_player_age' => function ($params) {
                        if(self::checkIsStringNull($params['average_player_age'])){
                            $params['average_player_age'] = null;
                            return self::formatCheckResult($params['average_player_age'],false,false);
                        }
                        if(self::checkIsNumberZero($params['average_player_age'])){
                            return  self::formatCheckResult($params['average_player_age'],false,true);
                        }
                        if(self::checkIsAverageNum($params['average_player_age'])){
                            return self::formatCheckResult($params['average_player_age']);
                        } else{
                            return self::formatCheckResult($params['average_player_age'],false,true);
                        }
                    },
                    'history_players' => function ($params) {
                        if(self::checkIsStringNull($params['history_players'])){
                            $params['history_players'] = null;
                            return self::formatCheckResult($params['history_players']);
                        }else{
                            return self::formatCheckResult($params['history_players']);
                        }
                    },
                    // 简介(英文)(标准表中无该字段)
                    'introduction' => function ($params) {
                        if(self::checkIsStringNull($params['introduction'])){
                            $params['introduction'] = null;
                            return self::formatCheckResult($params['introduction']);
                        }else{
                            return self::formatCheckResult($params['introduction']);
                        }
                    },
                    'introduction_cn' => function ($params) {
                        if(self::checkIsStringNull($params['introduction_cn'])){
                            $params['introduction_cn'] = null;
                            return self::formatCheckResult($params['introduction_cn']);
                        }else{
                            return self::formatCheckResult($params['introduction_cn']);
                        }
                    },
                    // 战队选手,TODO:应用原方法
                    'players' => function ($params) {
                        if(self::checkIsStringNull($params['players'])){
                            $params['players'] = null;
                            return self::formatCheckResult($params['players']);
                        }else{
                            return self::formatCheckResult($params['players']);
                        }
                    },
                    // 选手状态(标准表中无该字段)
                    // 是否删除
                    'deleted' => function ($params) {
                        if(self::checkIsStringNull($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        if(self::checkIsNumberZero($params['deleted'])){
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                        else{
                            return self::formatCheckResult($params['deleted']);
                        }
                    }
                ],
            ],
            //赛事
            Consts::RESOURCE_TYPE_TOURNAMENT => [
                // 数据源新增
                'add' => [
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        return self::formatCheckResult($params['name_cn'],false,false);
                    },
                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    'status' => function ($params) {
                        $v=$params['status'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = 1;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        elseif(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        elseif(self::checkIsIdForEnum($v,Consts::ENUM_MATCH_STATE)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult(1,false,true);
                        }
                    },
                    // 计划开始日期
                    'scheduled_begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_begin_at'])){
                            $params['scheduled_begin_at'] = null;
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['scheduled_begin_at'])){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                    },
                    // 计划结束时间
                    'scheduled_end_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_end_at'])){
                            $params['scheduled_end_at'] = null;
                            return self::formatCheckResult($params['scheduled_end_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['scheduled_end_at'])){
                            return self::formatCheckResult($params['scheduled_end_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_end_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_end_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_end_at'],false,true);
                        }
                    },
                    'short_name' => function ($params) {
                        $result = self::checkGetContent($params['short_name']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'short_name_cn' => function ($params) {
                        $result = self::checkGetContent($params['short_name']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'introduction' => function ($params) {
                        $result = self::checkGetContent($params['introduction']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'introduction_cn' => function ($params) {
                        $result = self::checkGetContent($params['introduction_cn']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'format' => function($params) {
                        $result = self::checkGetContent($params['format']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'format_cn' => function($params) {
                        $result = self::checkGetContent($params['format_cn']);
                        return self::formatCheckResult($result,false,false);
                    },
                    // slug
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    // 队伍数量(标准表中无该字段)
                    'number_of_teams' => function ($params) {
                        if(self::checkIsStringNull($params['number_of_teams'])){
                            $params['number_of_teams'] = null;
                            return self::formatCheckResult($params['number_of_teams'],false,false);
                        }
                        elseif(self::getStringToNumber($params['number_of_teams'])){
                            $params['number_of_teams'] = null;
                            return self::formatCheckResult(self::getStringToNumber($params['number_of_teams']),false,false);
                        }
                        elseif(self::checkIsPositiveInteger($params['number_of_teams'])){
                            return self::formatCheckResult($params['number_of_teams'],false,false);
                        }else{
                            return self::formatCheckResult($params['number_of_teams'],false,true);
                        }
                    },
                    // 赛事级别(标准表中无该字段)
                    'tier' => function ($params) {
                        return self::formatCheckResult($params['tier'],false,false);
                    },
                    // 赛区(标准表中无该字段)
                    // 国家地区
                    'country' => function ($params) {
                        $v=$params['country'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛区
                    'region' => function ($params) {
                        $v=$params['region'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    //举办地(英文)无限制
                    //举办地(中文)无限制
                    //主办方(英文)无限制
                    //主办方(中文)无限制
                    //游戏版本 无限制
                    //奖金 无限制
                    //积分 无限制
                    //名额 无限制(未完成)
                    'map_pool' => function($params){
                        if(self::checkIsStringNull($params['map_pool'])){
                            return self::formatCheckResult($params['map_pool'],false);
                        }else{
                            return self::formatCheckResult($params['map_pool']);
                        }
                    },
                    'teams' => function ($params) {
                        $v=$params['teams'];
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        if($v){
                            $a = json_decode($v);
                            if(!is_array($a)){
                                return self::formatCheckResult($v,false,true);
                            }else{
                                return self::formatCheckResult($v,false,false);
                            }
                        }
                    },
                    'teams_condition' => function($params){
                         if(self::checkIsStringNull($params['teams_condition'])){
                             return self::formatCheckResult($params['teams_condition'],false,false);
                         }
                         elseif(self::checkIsNumberZero($params['teams_condition'])){
                             $params['teams_condition'] = null;
                             return self::formatCheckResult($params['teams_condition'],false,false);
                         }
                         elseif($params['teams_condition']){
                            $a = json_decode($params['teams_condition']);
                            if(!is_array($a)){
                                return self::formatCheckResult($params['teams_condition'],false,true);
                            }else{
                                return self::formatCheckResult($params['teams_condition'],false,false);
                            }
                         }
                    },
                    // 是否删除
                    'deleted' => function ($params) {
                        $deletedInfo = self::checkDeleted($params['deleted'], Consts::RESOURCE_TYPE_TEAM, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $deletedInfo['after'],
                            'check' => @$deletedInfo['error_msg'],
                            'unset' => !$deletedInfo['is_right'],
                            'error_msg' => @$deletedInfo['error_msg'],
                        ];
                    }
                ],
                // 数据源更新
                'change' => [
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        return self::formatCheckResult($params['name_cn'],false,false);
                    },
                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['game_id'])){
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            return self::formatCheckResult($params['game_id'],false,true);
                        }
                    },
                    // 状态
                    'status' => function ($params) {
                        $v=$params['status'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果是0 则过滤掉，
                        elseif(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        elseif(self::checkIsIdForEnum($v,Consts::ENUM_MATCH_STATE)){
                            return self::formatCheckResult($v);
                        }
                        else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // type
                    'type' => function ($params) {
                        // 如果是空，则set，
                        if(self::checkIsStringNull($params['type'])){
                            return self::formatCheckResult($params['type'],false,true);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['type'])){
                            return self::formatCheckResult($params['type'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsPositiveInteger($params['type'])){
                            return self::formatCheckResult($params['type']);
                        }else{
                            return self::formatCheckResult($params['type'],false,true);
                        }
                    },
                    // 计划开始日期
                    'scheduled_begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_begin_at'])){
                            $params['scheduled_begin_at'] = null;
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['scheduled_begin_at'])){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                    },
                    // 计划结束时间
                    'scheduled_end_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_end_at'])){
                            $params['scheduled_end_at'] = null;
                            return self::formatCheckResult($params['scheduled_end_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['scheduled_end_at'])){
                            return self::formatCheckResult($params['scheduled_end_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_end_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_end_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_end_at'],false,true);
                        }
                    },
                    'short_name' => function ($params) {
                        $result = self::checkGetContent($params['short_name']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'short_name_cn' => function ($params) {
                        $result = self::checkGetContent($params['short_name']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'introduction' => function ($params) {
                        $result = self::checkGetContent($params['introduction']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'introduction_cn' => function ($params) {
                        $result = self::checkGetContent($params['introduction_cn']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'format' => function($params) {
                        $result = self::checkGetContent($params['format']);
                        return self::formatCheckResult($result,false,false);
                    },
                    'format_cn' => function($params) {
                        $result = self::checkGetContent($params['format_cn']);
                        return self::formatCheckResult($result,false,false);
                    },
                    // slug
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    // 队伍数量(标准表中无该字段)
                    'number_of_teams' => function ($params) {
                        if(self::checkIsStringNull($params['number_of_teams'])){
                            $params['number_of_teams'] = null;
                            return self::formatCheckResult($params['number_of_teams'],false,false);
                        }
                        elseif(self::checkIsNumberZero($params['number_of_teams'])){
                            $params['number_of_teams'] = null;
                            return self::formatCheckResult($params['number_of_teams'],false,false);
                        }
                        elseif(self::getStringToNumber($params['number_of_teams'])){
                            return self::formatCheckResult(self::getStringToNumber($params['number_of_teams']),false,false);
                        }
                        else{
                            return self::formatCheckResult($params['number_of_teams'],false,true);
                        }
                    },
                    // 赛事级别(标准表中无该字段)
                    'tier' => function ($params) {
                        return self::formatCheckResult($params['tier'],false,false);
                    },
                    // 赛区(标准表中无该字段)
                    // 国家地区
                    'country' => function ($params) {
                        $v=$params['country'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛区
                    'region' => function ($params) {
                        $v=$params['region'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_COUNTRY)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    //举办地(英文)无限制
                    'map_pool' => function($params){
                        if(self::checkIsStringNull($params['map_pool'])){
                            return self::formatCheckResult($params['map_pool'],false);
                        }else{
                            return self::formatCheckResult($params['map_pool']);
                        }
                    },

                    // 是否删除
                    'deleted' => function ($params) {
                        $deletedInfo = self::checkDeleted($params['deleted'], Consts::RESOURCE_TYPE_TEAM, QueueServer::QUEUE_TYPE_ADD);
                        return [
                            'output' => $deletedInfo['after'],
                            'check' => $deletedInfo['error_msg'],
                            'unset' => !$deletedInfo['is_right'],
                            'error_msg' => $deletedInfo['error_msg'],
                        ];
                    }
                ],
            ],
            // 比赛
            Consts::RESOURCE_TYPE_MATCH => [
                'add' => [
                    'has_incident_report' => function ($params) {
                        $drawBool = self::checkStatusBool($params['has_incident_report']);
                        if($drawBool){
                            return self::formatCheckResult($params['has_incident_report']);
                        }else{
                            return self::formatCheckResult($params['has_incident_report'],false,true);
                        }
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    //502bug编号
                    //是否有对局详情数据
                    'is_battle_detailed' => function ($params) {
                        return self::formatCheckResult($params['is_battle_detailed'],false,true);
                    },
                    //是否支持视频直播
                    'is_streams_supported' => function ($params) {
                        return self::formatCheckResult($params['is_streams_supported'],false,true);
                    },

                    // 是否支持PBP数据
                    'is_pbpdata_supported' => function ($params) {
                        return self::formatCheckResult($params['is_pbpdata_supported'],false,true);
                    },
                    'game' => function ($params) {
                        return self::formatCheckResult($params['game'],false,true);
                    },
                    'status' => function ($params) {
                        $v=$params['status'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = 1;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_MATCH_STATE)){
                            return self::formatCheckResult($v);
                        }else{
                            $v = 1;
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // name 暂时没用
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            $params['name'] = null;
                            return self::formatCheckResult($params['name']);
                        }else{
                            return self::formatCheckResult($params['name']);
                        }
                    },
                    // 描述 暂时没用
                    'relation' => function ($params) {
                        if(self::checkIsStringNull($params['relation'])){
                            $params['relation'] = null;
                            return self::formatCheckResult($params['relation']);
                        }else{
                            return self::formatCheckResult($params['relation']);
                        }
                    },

                    // 游戏项目
                    'game_id' => function ($params) {
                        if(self::checkIsStringNull($params['game_id'])){
                            throw new BusinessException([], "游戏ID不允许为空");
//                            return self::formatCheckResult($params['game_id'],true,false,'游戏ID不允许为空！');
                        }
                        //如果是0 则过滤掉
                        if(self::checkIsNumberZero($params['game_id'])){
                            throw new BusinessException([], "游戏ID不允许为空");
//                            return self::formatCheckResult($params['game_id'],true,false,'游戏ID不允许为空！');
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['game_id'],Consts::ENUM_GAME)){
                            return self::formatCheckResult($params['game_id']);
                        }else{
                            throw new BusinessException([], "游戏ID不允许为空");
//                            return self::formatCheckResult($params['game_id'],true,false,'游戏ID不允许为空！');
                        }
                    },
//                    'game' => function ($params) {
//                        if(self::checkIsStringNull($params['game'])){
//                            return self::formatCheckResult($params['game'],true,false,'游戏ID不允许为空！');
//                        }
//                        //如果是0 则过滤掉
//                        if(self::checkIsNumberZero($params['game'])){
//                            return self::formatCheckResult($params['game'],true,false,'游戏ID不允许为空！');
//                        }
//                        //如果不存在，则过滤掉
//                        if(self::checkIsIdForEnum($params['game'],Consts::ENUM_GAME)){
//                            return self::formatCheckResult($params['game']);
//                        }else{
//                            return self::formatCheckResult($params['game'],true,false,'游戏ID不允许为空！');
//                        }
//                    },
                    // 比赛规则 数据源没给的话，根据游戏项目取默认值
                    'game_rules' => function ($params) use ($gameId){
                        // 取默认值
                        if(self::checkIsStringNull($params['game_rules'])){
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_game_rule');
                            $params['game_rules'] = $ruleId;
                            return self::formatCheckResult($params['game_rules'],false);
                        }
                        //如果是0 取默认值
                        if(self::checkIsNumberZero($params['game_rules'])){
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_game_rule');
                            $params['game_rules'] = $ruleId;
                            return self::formatCheckResult($params['game_rules'],false);
                        }
                        //如果不存在，取默认值
                        if(self::checkIsIdForEnum($params['game_rules'],Consts::ENUM_GAME_RULES)){
                            return self::formatCheckResult($params['game_rules']);
                        }else{
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_game_rule');
                            $params['game_rules'] = $ruleId;
                            return self::formatCheckResult($params['game_rules'],false);
                        }
                    },
                    // 比赛类型 数据源没给的话，根据游戏项目取默认值
                    'match_type' => function ($params) use ($gameId){
                        if(self::checkIsStringNull($params['match_type'])){
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_match_type');
                            $params['match_type'] = $ruleId;
                            return self::formatCheckResult($params['match_type'],false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['match_type'])){
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_match_type');
                            $params['match_type'] = $ruleId;
                            return self::formatCheckResult($params['match_type'],false);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['match_type'],Consts::ENUM_GAME_MATCH_TYPE)){
                            return self::formatCheckResult($params['match_type']);
                        }else{
                            $ruleId = self::defultGameRulesIdByRuleId($gameId,'default_match_type');
                            $params['match_type'] = $ruleId;
                            return self::formatCheckResult($params['match_type'],false);
                        }
                    },
                    // 赛制局数
                    'number_of_games' => function ($params) {
                        if(self::checkIsStringNull($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                        if(self::checkIsNumberZero($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                        if(self::checkIsTeamScoureInteger($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,false);
                        }else{
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                    },
                    // 所属赛事
                    'tournament_id' => function ($params) {
                        //如果是 0 '' null 不允许创建
                        if(self::checkIsStringNull($params['tournament_id']) || self::checkIsNumberZero($params['tournament_id'])){
                            throw new BusinessException([], "所属赛事ID不允许为空");
//                            return self::formatCheckResult($params['tournament_id'],true,false,'所属赛事ID不允许为空！');
                        }else{
                            $params['tournament_id'] = HotBase::getMainIdByRelIdentityId('tournament',$params['tournament_id'],$params['origin_id'],$params['game_id']);
                            return self::formatCheckResult($params['tournament_id']);
                        }
                    },
                    // 所属分組 todo
                    'group_id' => function ($params) {
                        return self::formatCheckResult($params['group_id'],false,true);
                    },
//                    'group_id' => function ($params) {
//                        //如果是 0 '' null
//                        if(self::checkIsStringNull($params['group_id']) || self::checkIsNumberZero($params['group_id'])){
//                            $params['group_id'] = null;
//                            return self::formatCheckResult($params['group_id']);
//                        }else{
//                            $params['group_id'] = HotBase::getMainIdByRelIdentityId('group',$params['group_id'],$params['origin_id'],$params['game_id']);
//                            return self::formatCheckResult($params['group_id']);
//                        }
//                    },
                    // 计划开始时间 除非比赛状态是已推迟，否则数据源没给的话，不允许创建
//                    'scheduled_begin_at' => function ($params) {
//                        //如果是 0 '' null 不允许创建
//                        if(self::checkIsStringNull($params['scheduled_begin_at']) || self::checkIsNumberZero($params['scheduled_begin_at'])){
//                            throw new BusinessException([], "计划开始时间不允许为空");
////                            return self::formatCheckResult($params['scheduled_begin_at'],true,false,'计划开始时间不允许为空！');
//                        }else{
//                            return self::formatCheckResult($params['scheduled_begin_at']);
//                        }
//                    },
                    // 计划开始时间 除非比赛状态是已推迟，否则数据源没给的话，不允许创建
                    'scheduled_begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_begin_at'])){
                            $params['scheduled_begin_at'] = null;
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['scheduled_begin_at'])){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                    },
                    // 原始计划开始时间
                    'original_scheduled_begin_at' => function ($params) {
                        //如果是 0 '' null 如果数据源没给，则根据创建比赛时的计划开始时间生成
                        if(self::checkIsStringNull($params['original_scheduled_begin_at']) ||
                            self::checkIsNumberZero($params['original_scheduled_begin_at'])){
                            $params['original_scheduled_begin_at'] = $params['scheduled_begin_at'];
                            return self::formatCheckResult($params['original_scheduled_begin_at']);
                        }else{
                            if(date('Y-m-d H:i:s', strtotime($params['original_scheduled_begin_at'])) != "1970-01-01 08:00:00"){
                                return self::formatCheckResult($params['original_scheduled_begin_at'],false,false);
                            }else{
                                $params['original_scheduled_begin_at'] = $params['scheduled_begin_at'];
                                return self::formatCheckResult($params['original_scheduled_begin_at'],false,true);
                            }
                        }
                    },
                    // 主队
                    'team_1_id' => function ($params) {
                        // 如果是 0 '' null
                        if(self::checkIsStringNull($params['team_1_id']) || self::checkIsNumberZero($params['team_1_id'])){
                            $params['team_1_id'] = -1;
                            return self::formatCheckResult($params['team_1_id']);
                        }else{
                            $teamOneArr = StandardDataTeam::find()->where(['rel_identity_id'=>$params['team_1_id'],'game_id'=>$params['game_id']])->asArray()->one();
                            if (!empty($teamOneArr)) {
                                $team1Id = HotBase::getMainIdByRelIdentityId('team',$params['team_1_id'],$params['origin_id'],$params['game_id']);
                                if($team1Id){
                                    $params['team_1_id'] = $team1Id;
                                }else{
                                    throw new BusinessException([], "请先绑定主队！");

                                }
                            }else{
                                $params['team_1_id'] = -1;

                            }

                            return self::formatCheckResult($params['team_1_id']);
                        }
                    },
                    // 客队
                    'team_2_id' => function ($params) {
                        // 如果是 0 '' null
                        if(self::checkIsStringNull($params['team_2_id']) || self::checkIsNumberZero($params['team_2_id'])){
                            $params['team_2_id'] = -1;
                            return self::formatCheckResult($params['team_2_id']);
                        }else{
                            $teamTwoArr = StandardDataTeam::find()->where(['rel_identity_id'=>$params['team_2_id'],'game_id'=>$params['game_id']])->asArray()->one();
                            if (!empty($teamTwoArr)) {
                                $team2Id = HotBase::getMainIdByRelIdentityId('team',$params['team_2_id'],$params['origin_id'],$params['game_id']);
                                if($team2Id){
                                    $params['team_2_id'] = $team2Id;
                                }else{
                                    throw new BusinessException([], "请先绑定客队！");

                                }
                            }else{
                                $params['team_2_id'] = -1;

                            }
                            return self::formatCheckResult($params['team_2_id']);
                        }
                    },
                    // slug自动生成
                    // 比赛地点（英文）
                    // 比赛地点（中文）
                    // 轮次
                    // 轮次名称（英文）
                    // 轮次名称（中文）
                    // 描述（英文） 小于1000字，多余部分截断
                    'description' => function ($params) {
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    // 对阵表位置 unset
                    'bracket_pos' => function ($params) {
                        return self::formatCheckResult($params['bracket_pos'],false,true);
                    },
                    // 地图BP 未做
                    // 地图排序 未做
                    // 实际开始时间
                    'begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['begin_at'])){
                            $params['begin_at'] = null;
                            return self::formatCheckResult($params['begin_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['begin_at'])){
                            return self::formatCheckResult($params['begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['begin_at'],false,true);
                        }
                    },
                    // 实际结束时间
                    'end_at' => function ($params) {
                        if(self::checkIsStringNull($params['end_at'])){
                            $params['end_at'] = null;
                            return self::formatCheckResult($params['end_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['end_at'])){
                            return self::formatCheckResult($params['end_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['end_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['end_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['end_at'],false,true);
                        }
                    },
                    // 比赛状态
                    // 主队可客队比分 （null,'' unset掉） 最后存取 0 或者 正整数
                    'team_1_score' => function ($params) {
                        if(self::checkIsStringNull($params['team_1_score'])){
                            $params['team_1_score'] = 0;
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }
                        if(self::checkIsNumberZero($params['team_1_score'])){
                            $params['team_1_score'] = 0;
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }
                        if(self::checkIsTeamScoureInteger($params['team_1_score'])){
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }else{
                            return self::formatCheckResult($params['team_1_score'],false,true);
                        }
                    },
                    'team_2_score' => function ($params) {
                        if(self::checkIsStringNull($params['team_2_score'])){
                            $params['team_2_score'] = 0;
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }
                        if(self::checkIsNumberZero($params['team_2_score'])){
                            $params['team_2_score'] = 0;
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }
                        if(self::checkIsTeamScoureInteger($params['team_2_score'])){
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }else{
                            return self::formatCheckResult($params['team_2_score'],false,true);
                        }
                    },
                    // 获胜方 转成 1或者2
                    'winner' => function ($params) {
//                        if(self::checkIsStringNull($params['winner']) || self::checkIsNumberZero($params['winner'])){
//                            return self::formatCheckResult($params['winner'],false,true);
//                        }else{
//                            $winnerTeamId = HotBase::getMainIdByRelIdentityId('team',$params['winner'],$params['origin_id'],$params['game_id']);
//                            if($winnerTeamId==$params['team_1_id']){
//                                $params['winner'] = 1;
//                            }elseif ($winnerTeamId==$params['team_2_id']){
//                                $params['winner'] = 2;
//                            }else{
//                                return self::formatCheckResult($params['winner'],false,true);
//                            }
//                        }
                        if(self::checkIsStringNull($params['winner'])){
                            $params['winner'] = null;
                            return self::formatCheckResult($params['winner'],false,false);
                        }
                        if(self::checkIsNumberZero($params['winner'])){
                            return self::formatCheckResult($params['winner'],false,true);
                        }
                        if(self::checkIsPositiveInteger($params['winner'])){
                            return self::formatCheckResult($params['winner'],false,false);
                        }else{
                            return self::formatCheckResult($params['winner'],false,true);
                        }
                    },
                    // 平局 存1或者2
                    'draw' => function ($params) {
                        $drawBool = self::checkStatusBool($params['draw']);
                        if($drawBool){
                            return self::formatCheckResult($params['draw']);
                        }else{
                            return self::formatCheckResult($params['draw'],false,true);
                        }
                    },
                    // 默认领先战队 存1或者2或者null
                    'default_advantage' => function ($params) {
//                        if(self::checkIsStringNull($params['default_advantage']) || self::checkIsNumberZero($params['default_advantage'])){
//                            return self::formatCheckResult($params['default_advantage'],false,true);
//                        }else{
//                            $teamId = HotBase::getMainIdByRelIdentityId('team',$params['default_advantage'],$params['origin_id'],$params['game_id']);
//                            if($teamId==$params['team_1_id']){
//                                $params['default_advantage'] = 1;
//                            }elseif ($teamId==$params['team_2_id']){
//                                $params['default_advantage'] = 2;
//                            }else{
//                                return self::formatCheckResult($params['default_advantage'],false,true);
//                            }
//                        }
                        if(self::checkIsStringNull($params['default_advantage'])){
                            $params['default_advantage'] = null;
                            return self::formatCheckResult($params['default_advantage'],false,false);
                        }
                        if(self::checkIsNumberZero($params['default_advantage'])){
                            return self::formatCheckResult($params['default_advantage'],false,true);
                        }
                        if(self::checkIsPositiveInteger($params['default_advantage'])){
                            return self::formatCheckResult($params['default_advantage'],false,false);
                        }else{
                            return self::formatCheckResult($params['default_advantage'],false,true);
                        }
                    },
                    // 是否弃权 存1或者2
                    'forfeit' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['forfeit']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['forfeit']);
                        }else{
                            return self::formatCheckResult($params['forfeit'],false,true);
                        }
                    },
                    'battle_forfeit' => function ($params) {
                        if(self::checkIsStringNull($params['battle_forfeit'])){
                            $params['battle_forfeit'] = null;
                            return self::formatCheckResult($params['battle_forfeit']);
                        }else{

                            $mapInfoArr = json_decode($params['battle_forfeit'],true);
                            if (!is_array($mapInfoArr)) {
                                return self::formatCheckResult($params['battle_forfeit'],false,true);
                            }

                            $error = json_last_error();
                            if (!empty($error)) {
                                return self::formatCheckResult($params['battle_forfeit'],false,true);
                            }
                            return self::formatCheckResult($params['battle_forfeit']);
                        }
                    },
                    // 是否有对局详情数据 1或者2
//                    'is_battle_detailed' => function ($params) {
//                        $statusBool = self::checkStatusBool($params['is_battle_detailed']);
//                        if($statusBool){
//                            return self::formatCheckResult($params['is_battle_detailed']);
//                        }else{
//                            return self::formatCheckResult($params['is_battle_detailed'],false,true);
//                        }
//                    },
                    // 是否支持PBP数据 1或者2
//                    'is_pbpdata_supported' => function ($params) {
//                        $forfeitBool = self::checkStatusBool($params['is_pbpdata_supported']);
//                        if($forfeitBool){
//                            return self::formatCheckResult($params['is_pbpdata_supported']);
//                        }else{
//                            return self::formatCheckResult($params['is_pbpdata_supported'],false,true);
//                        }
//                    },
                    // 是否支持视频直播 1或者2
//                    'is_streams_supported' => function ($params) {
//                        $forfeitBool = self::checkStatusBool($params['is_streams_supported']);
//                        if($forfeitBool){
//                            return self::formatCheckResult($params['is_streams_supported']);
//                        }else{
//                            return self::formatCheckResult($params['is_streams_supported'],false,true);
//                        }
//                    },
                    // 是否删除
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                    // ban-pick
                    'map_info' => function ($params) {
                        if($params['map_info'] == '[]'){
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        if(self::checkIsStringNull($params['map_info'])){
                            $params['map_info'] = null;

                            return self::formatCheckResult($params['map_info'],false,false);
                        }
                        if(self::checkIsNumberZero($params['map_info'])){
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        $mapInfoArr = json_decode($params['map_info'],true);
                        if (!is_array($mapInfoArr)) {
                            return self::formatCheckResult($params['map_info'],false,true);
                        }

                        $error = json_last_error();
                        if (!empty($error)) {
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        if(!is_null($mapInfoArr)){
                            foreach ($mapInfoArr as $kk => $vv) {
                                $mapId = MetadataCsgoMap::find()->select(['id'])->where(['name'=>$vv['map']])->asArray()->one();
                                $newArr[]['map'] = $mapId['id'];
                            }
                            $maps = json_encode($newArr);

                            return self::formatCheckResult($maps,false,false);
                        }else{
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                    },

                    'map_ban_pick' => function ($params) {
                        if($params['map_ban_pick'] == '[]'){
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                        if(self::checkIsStringNull($params['map_ban_pick'])){
                            $params['map_ban_pick'] = null;
                            return self::formatCheckResult($params['map_ban_pick'],false,false);
                        }
                        if(self::checkIsNumberZero($params['map_ban_pick'])){
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                        $mapNam = json_decode($params['map_ban_pick'],true);

                        if (!is_array($mapNam)) {
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }

                        $error = json_last_error();
                        if (!empty($error)) {
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }

                        if(!is_null($mapNam)){
                            foreach ($mapNam as $kkk => $vvv) {
                                $vvv['map'] = str_replace(" ", "", $vvv['map']);
                                $mapId = MetadataCsgoMap::find()->where(['name'=>$vvv['map'],'deleted' => 2])->asArray()->one();

                                $newArr[$kkk]['team'] = $vvv['team'];
                                $newArr[$kkk]['type'] = $vvv['type'];
                                $newArr[$kkk]['map'] = $mapId['id'];
                            }
                            $mapBan = json_encode($newArr);
                            return self::formatCheckResult($mapBan,false,false);
                        }else{
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                    },

                ],
                'change' => [
                    //502bug编号
                    //是否有对局详情数据
                    'is_battle_detailed' => function ($params) {
                        return self::formatCheckResult($params['is_battle_detailed'],false,true);
                    },
                    //是否支持视频直播
                    'is_streams_supported' => function ($params) {
                        return self::formatCheckResult($params['is_streams_supported'],false,true);
                    },

                    // 是否支持PBP数据
                    'is_pbpdata_supported' => function ($params) {
                        return self::formatCheckResult($params['is_pbpdata_supported'],false,true);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'game' => function ($params) {
                        return self::formatCheckResult($params['game'],false,true);
                    },
                    // 状态
                    'status' => function ($params) {
                        $v=$params['status'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($v,Consts::ENUM_MATCH_STATE)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // name 暂时没用
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            $params['name'] = null;
                            return self::formatCheckResult($params['name']);
                        }else{
                            return self::formatCheckResult($params['name']);
                        }
                    },
                    // 描述 暂时没用
                    'relation' => function ($params) {
                        if(self::checkIsStringNull($params['relation'])){
                            $params['relation'] = null;
                            return self::formatCheckResult($params['relation']);
                        }else{
                            return self::formatCheckResult($params['relation']);
                        }
                    },
                    // 游戏项目 unset
                    'game_id' => function ($params) {
                        return self::formatCheckResult($params['game_id'],false,true);
                    },
                    // 比赛规则 unset
                    'game_rules' => function ($params) {
                        return self::formatCheckResult($params['game_rules'],false,true);
                    },
                    // 比赛类型
                    'match_type' => function ($params) {
                        if(self::checkIsStringNull($params['match_type'])){
                            $params['match_type'] = null;
                            return self::formatCheckResult($params['match_type'],false,true);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($params['match_type'])){
                            $params['match_type'] = null;
                            return self::formatCheckResult($params['match_type'],false,true);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkIsIdForEnum($params['match_type'],Consts::ENUM_GAME_MATCH_TYPE)){
                            return self::formatCheckResult($params['match_type'],false);
                        }else{
                            $params['match_type'] = null;
                            return self::formatCheckResult($params['match_type'],false,true);
                        }
                    },
                    // 赛制局数
                    'number_of_games' => function ($params) {
                        if(self::checkIsStringNull($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                        if(self::checkIsNumberZero($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                        if(self::checkIsTeamScoureInteger($params['number_of_games'])){
                            return self::formatCheckResult($params['number_of_games'],false,false);
                        }else{
                            return self::formatCheckResult($params['number_of_games'],false,true);
                        }
                    },
                    // 所属赛事 直接unset
                    'tournament_id' => function ($params) {
                        return self::formatCheckResult($params['tournament_id'],false,true);
                    },
                    // 所属分組 现在直接unset （将来有逻辑）
                    'group_id' => function ($params) {
                        return self::formatCheckResult($params['group_id'],false,true);
                    },
                    // 计划开始日期 可更改
                    'scheduled_begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['scheduled_begin_at'])){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                        if(self::checkIsNumberZero($params['scheduled_begin_at'])){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['scheduled_begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['scheduled_begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['scheduled_begin_at'],false,true);
                        }
                    },
                    // 原始计划开始时间 直接unset
                    'original_scheduled_begin_at' => function ($params) {
                        return self::formatCheckResult($params['original_scheduled_begin_at'],false,true);
                    },
                    // 实际开始时间
                    'begin_at' => function ($params) {
                        if(self::checkIsStringNull($params['begin_at'])){
                            $params['begin_at'] = null;
                            return self::formatCheckResult($params['begin_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['begin_at'])){
                            return self::formatCheckResult($params['begin_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['begin_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['begin_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['begin_at'],false,true);
                        }
                    },
                    // 实际结束时间
                    'end_at' => function ($params) {
                        if(self::checkIsStringNull($params['end_at'])){
                            $params['end_at'] = null;
                            return self::formatCheckResult($params['end_at'],false,false);
                        }
                        if(self::checkIsNumberZero($params['end_at'])){
                            return self::formatCheckResult($params['end_at'],false,true);
                        }
                        if(date('Y-m-d H:i:s', strtotime($params['end_at'])) != "1970-01-01 08:00:00"){
                            return self::formatCheckResult($params['end_at'],false,false);
                        }else{
                            return self::formatCheckResult($params['end_at'],false,true);
                        }
                    },
//                    // 是否调整时间表  待完善
                    'is_rescheduled' => function ($params) {
                        $drawBool = self::checkStatusBool($params['is_rescheduled']);
                        if($drawBool){
                            return self::formatCheckResult($params['is_rescheduled']);
                        }else{
                            return self::formatCheckResult($params['is_rescheduled'],false,true);
                        }
                    },
//                    // 主队
//                    'team_1_id' => function ($params) {
//                        // 如果是 0 '' null
//                        if(self::checkIsStringNull($params['team_1_id']) || self::checkIsNumberZero($params['team_1_id'])){
//                            $params['team_1_id'] = -1;
//                            return self::formatCheckResult($params['team_1_id']);
//                        }else{
//                            // 查询战队
//                            $team1Id = HotBase::getMainIdByRelIdentityId('team',$params['team_1_id'],$params['origin_id']);
//                            if($team1Id){
//                                $params['team_1_id'] = $team1Id;
//                            }else{
//                                return self::formatCheckResult($params['team_1_id'],false,true);
//                            }
//                            return self::formatCheckResult($params['team_1_id']);
//                        }
//                    },
//                    // 客队
//                    'team_2_id' => function ($params) {
//                        // 如果是 0 '' null
//                        if (self::checkIsStringNull($params['team_2_id'])){
//                            $params['team_2_id'] = -1;
//                            return self::formatCheckResult($params['team_2_id']);
//                        } elseif (self::checkIsNumberZero($params['team_2_id'])){
//                            $params['team_2_id'] = -1;
//                            return self::formatCheckResult($params['team_2_id']);
//                        } else {
//                            // 查询战队
//                            $team2Id = HotBase::getMainIdByRelIdentityId('team',$params['team_2_id'],3);
//                            if($team2Id){
//                                $params['team_2_id'] = $team2Id;
//                            }else{
//                                return self::formatCheckResult($params['team_2_id'],false,true);
//                            }
//                            return self::formatCheckResult($params['team_2_id']);
//                        }
//                    },
                    // slug自动生成 调用自己方法
                    // 比赛地点（英文）  比赛地点（中文） 轮次（可能存个正整数）   轮次名称（英/中 文） 来啥存啥
                    // 描述（英文） 描述（中文） 有现成方法（找jianqi要）
                    // 对阵表位置 unset
                    'bracket_pos' => function ($params) {
                        return self::formatCheckResult($params['bracket_pos'],false,true);
                    },
                    // 地图BP(转成自己格式存)
                    // 比赛地图(转成自己格式存)
                    // 实际开始时间  实际结束时间  （判斷是否為时间）然后直接存
                    // 比赛状态 判断是否为合法ID（不存在直接unset） 有码表
                    // 实时数据 （待讨论）
                    // 主队可客队比分 （null,'' unset掉） 最后存取 0 或者 正整数
                    // 主队比分
                    'team_1_score' => function ($params) {
                        if(self::checkIsStringNull($params['team_1_score'])){
                            $params['team_1_score'] = 0;
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }
                        if(self::checkIsNumberZero($params['team_1_score'])){
                            $params['team_1_score'] = 0;
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }
                        if(self::checkIsTeamScoureInteger($params['team_1_score'])){
                            return self::formatCheckResult($params['team_1_score'],false,false);
                        }else{
                            return self::formatCheckResult($params['team_1_score'],false,true);
                        }
                    },
                    'team_2_score' => function ($params) {
                        if(self::checkIsStringNull($params['team_2_score'])){
                            $params['team_2_score'] = 0;
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }
                        if(self::checkIsNumberZero($params['team_2_score'])){
                            $params['team_2_score'] = 0;
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }
                        if(self::checkIsTeamScoureInteger($params['team_2_score'])){
                            return self::formatCheckResult($params['team_2_score'],false,false);
                        }else{
                            return self::formatCheckResult($params['team_2_score'],false,true);
                        }
                    },
//                    'team_1_score' => function ($params) {
//                        if ($params['status'] == 1){
//                            return self::formatCheckResult($params['team_1_score'],false,true);
//                        }else{
//                            return self::formatCheckResult($params['team_1_score']);
//                        }
//                    },
                    // 客队比分
//                    'team_2_score' => function ($params) {
//                        if ($params['status'] == 1){
//                            return self::formatCheckResult($params['team_2_score'],false,true);
//                        }else{
//                            return self::formatCheckResult($params['team_2_score']);
//                        }
//                    },
                    // 获胜方 转成 1或者2
//                    'winner' => function ($params) {
//                            $teamId = HotBase::getMainIdByRelIdentityId('team',$params['winner'],$params['origin_id']);
//                            $params['winner'] = $teamId;
//                            return self::formatCheckResult($params['winner']);
//                    },
                    // 平局 存1或者2
                    'draw' => function ($params) {
                        $drawBool = self::checkStatusBool($params['draw']);
                        if($drawBool){
                            return self::formatCheckResult($params['draw']);
                        }else{
                            return self::formatCheckResult($params['draw'],false,true);
                        }
                    },
                    'has_incident_report' => function ($params) {
                        $drawBool = self::checkStatusBool($params['has_incident_report']);
                        if($drawBool){
                            return self::formatCheckResult($params['has_incident_report']);
                        }else{
                            return self::formatCheckResult($params['has_incident_report'],false,true);
                        }
                    },
                    // 默认领先战队 存1或者2或者null
                    // 是否弃权 存1或者2
                    'forfeit' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['forfeit']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['forfeit']);
                        }else{
                            return self::formatCheckResult($params['forfeit'],false,true);
                        }
                    },

                    'battle_forfeit' => function ($params) {
                        if(self::checkIsStringNull($params['battle_forfeit'])){
                            $params['battle_forfeit'] = null;
                            return self::formatCheckResult($params['battle_forfeit']);
                        }else{

                            $mapInfoArr = json_decode($params['battle_forfeit'],true);
                            if (!is_array($mapInfoArr)) {
                                return self::formatCheckResult($params['battle_forfeit'],false,true);
                            }

                            $error = json_last_error();
                            if (!empty($error)) {
                                return self::formatCheckResult($params['battle_forfeit'],false,true);
                            }
                            return self::formatCheckResult($params['battle_forfeit']);
                        }
                    },
                    // 是否有对局详情数据 1或者2
//                    'is_battle_detailed' => function ($params) {
//                        $forfeitBool = self::checkStatusBool($params['is_battle_detailed']);
//                        if($forfeitBool){
//                            return self::formatCheckResult($params['is_battle_detailed']);
//                        }else{
//                            return self::formatCheckResult($params['is_battle_detailed'],false,true);
//                        }
//                    },
                    // 默认领先战队 存1或者2或者null
                    'default_advantage' => function ($params) {
                        if(self::checkIsStringNull($params['default_advantage'])){
                            $params['default_advantage'] = null;
                            return self::formatCheckResult($params['default_advantage'],false,false);
                        }
                        if(self::checkIsNumberZero($params['default_advantage'])){
                            return self::formatCheckResult($params['default_advantage'],false,true);
                        }
                        if(self::checkIsPositiveInteger($params['default_advantage'])){
                            return self::formatCheckResult($params['default_advantage'],false,false);
                        }else{
                            return self::formatCheckResult($params['default_advantage'],false,true);
                        }
                    },
                    // winner存1或者2或者null
                    'winner' => function ($params) {
                        if(self::checkIsStringNull($params['winner'])){
                            $params['winner'] = null;
                            return self::formatCheckResult($params['winner'],false,false);
                        }
                        if(self::checkIsNumberZero($params['winner'])){
                            return self::formatCheckResult($params['winner'],false,true);
                        }
                        if(self::checkIsPositiveInteger($params['winner'])){
                            return self::formatCheckResult($params['winner'],false,false);
                        }else{
                            return self::formatCheckResult($params['winner'],false,true);
                        }
                    },
                    'game_version' =>function($params){
                        return self::formatCheckResult($params['game_version'],false,false);
                    },
                    // 外面转
                    // 是否支持PBP数据 1或者2
//                    'is_pbpdata_supported' => function ($params) {
//                        $forfeitBool = self::checkStatusBool($params['is_pbpdata_supported']);
//                        if($forfeitBool){
//                            return self::formatCheckResult($params['is_pbpdata_supported']);
//                        }else{
//                            return self::formatCheckResult($params['is_pbpdata_supported'],false,true);
//                        }
//                    },
                    // 是否支持视频直播 1或者2
//                    'is_streams_supported' => function ($params) {
//                        $forfeitBool = self::checkStatusBool($params['is_streams_supported']);
//                        if($forfeitBool){
//                            return self::formatCheckResult($params['is_streams_supported']);
//                        }else{
//                            return self::formatCheckResult($params['is_streams_supported'],false,true);
//                        }
//                    },
                    // 是否删除
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                    // ban-pick
                    'map_ban_pick' => function ($params) {
                        if($params['map_ban_pick'] == '[]'){
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                        if(self::checkIsStringNull($params['map_ban_pick'])){
                            $params['map_ban_pick'] = null;
                            return self::formatCheckResult($params['map_ban_pick'],false,false);
                        }
                        if(self::checkIsNumberZero($params['map_ban_pick'])){
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                        $mapNam = json_decode($params['map_ban_pick'],true);

                        if (!is_array($mapNam)) {
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }

                        $error = json_last_error();
                        if (!empty($error)) {
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }

                        if(!is_null($mapNam)){
                            foreach ($mapNam as $kkk => $vvv) {
                                $vvv['map'] = str_replace(" ", "", $vvv['map']);
                                $mapId = MetadataCsgoMap::find()->where(['name'=>$vvv['map'],'deleted' => 2])->asArray()->one();

                                $newArr[$kkk]['team'] = $vvv['team'];
                                $newArr[$kkk]['type'] = $vvv['type'];
                                $newArr[$kkk]['map'] = $mapId['id'];
                            }
                            $mapBan = json_encode($newArr);
                            return self::formatCheckResult($mapBan,false,false);
                        }else{
                            return self::formatCheckResult($params['map_ban_pick'],false,true);
                        }
                    },
                    'description_cn' => function ($params) {
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'description' => function ($params) {
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },

                    // ban-pick
                    'map_info' => function ($params) {
                        if($params['map_info'] == '[]'){
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        if(self::checkIsStringNull($params['map_info'])){
                            $params['map_info'] = null;
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        if(self::checkIsNumberZero($params['map_info'])){
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                        $mapInfoArr = json_decode($params['map_info'],true);
                        if (!is_array($mapInfoArr)) {
                            return self::formatCheckResult($params['map_info'],false,true);
                        }

                        $error = json_last_error();
                        if (!empty($error)) {
                            return self::formatCheckResult($params['map_info'],false,true);
                        }

                        if(!is_null($mapInfoArr)){
                            foreach ($mapInfoArr as $kk => $vv) {
                                $mapId = MetadataCsgoMap::find()->select(['id'])->where(['name'=>$vv['map']])->asArray()->one();
                                $newArr[]['map'] = $mapId['id'];
                            }
                            $maps = json_encode($newArr);

                            return self::formatCheckResult($maps,false,false);
                        }else{
                            return self::formatCheckResult($params['map_info'],false,true);
                        }
                    }
                ],
            ],
            //lol英雄
            Consts::METADATA_TYPE_LOL_CHAMPION => [
                "add" => [
                    // 状态
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'title' => function ($params) {
                        return self::formatCheckResult($params['title'],false,false);
                    },
                    'title_cn' => function ($params) {
                        return self::formatCheckResult($params['title_cn'],false,false);
                    },
                    'primary_role' => function ($params) {
                        return self::formatCheckResult($params['primary_role'],false,false);
                    },
                    'primary_role_cn' => function ($params) {
                        return self::formatCheckResult($params['primary_role_cn'],false,false);
                    },
                    'secondary_role' => function ($params) {
                        return self::formatCheckResult($params['secondary_role'],false,false);
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },


                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },

                ],
                "change" => [
//                    // 状态
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'title' => function ($params) {
                        return self::formatCheckResult($params['title'],false,false);
                    },
                    'title_cn' => function ($params) {
                        return self::formatCheckResult($params['title_cn'],false,false);
                    },
                    'primary_role' => function ($params) {
                        return self::formatCheckResult($params['primary_role'],false,false);
                    },
                    'primary_role_cn' => function ($params) {
                        return self::formatCheckResult($params['primary_role_cn'],false,false);
                    },
                    'secondary_role' => function ($params) {
                        return self::formatCheckResult($params['secondary_role'],false,false);
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
//                    'image' => function ($params) {
//
//                      if (self::checkIsStringDataNull($params['image'])){
//                          return self::formatCheckResult($params['image'],false,true);
//                      }else{
//                          return self::formatCheckResult($params['image'],false,false);
//                      }
//                    },
//                    'small_image' => function ($params) {
//
//                        if (self::checkIsStringDataNull($params['small_image'])){
//                            return self::formatCheckResult($params['small_image'],false,true);
//                        }else{
//                            return self::formatCheckResult($params['small_image'],false,false);
//                        }
//                    },

                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                     // 是否删除
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ]

            ],
            //lol技能
            Consts::METADATA_TYPE_LOL_ABILITY =>[
                "add" => [
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'champion_id' => function ($params) {
                        if(self::checkIsStringNull($params['champion_id'])){
                            $params['champion_id'] = null;
                            return self::formatCheckResult($params['champion_id'],false,false);
                        }
                        $checkBinding = self::checkBindingMetadata($params['champion_id'],Consts::METADATA_TYPE_LOL_CHAMPION);

                        if($checkBinding){
                            $params['champion_id'] = $checkBinding;
                            return self::formatCheckResult($params['champion_id']);
                        }else{
                            return self::formatCheckResult($params['champion_id'],false,true);
                        }

                    },
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'title' => function ($params) {
                        return self::formatCheckResult($params['title'],false,false);
                    },
                    'title_cn' => function ($params) {
                        return self::formatCheckResult($params['title_cn'],false,false);
                    },
                    'primary_role' => function ($params) {
                        return self::formatCheckResult($params['primary_role'],false,false);
                    },
                    'primary_role_cn' => function ($params) {
                        return self::formatCheckResult($params['primary_role_cn'],false,false);
                    },
                    'secondary_role' => function ($params) {
                        return self::formatCheckResult($params['secondary_role'],false,false);
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'hotkey' => function ($params) {
                        if(self::checkIsStringNull($params['hotkey'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['hotkey'],false,false);
                        }
                        $result = self::checkStringToLetter($params['hotkey']);

                        if ($result) {
                            $params['hotkey'] = $result;
                            return self::formatCheckResult($params['hotkey'],false,false);

                        }else {
                            return self::formatCheckResult($params['hotkey'],false,true);
                        }

                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },

                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                ],
                "change" => [
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'champion_id' => function ($params) {
                    if(self::checkIsStringNull($params['champion_id'])){
                            $params['champion_id'] = null;
                            return self::formatCheckResult($params['champion_id'],false,false);
                        }
                        $checkBinding = self::checkBindingMetadata($params['champion_id'],Consts::METADATA_TYPE_LOL_CHAMPION);

                        if($checkBinding){
                            $params['champion_id'] = $checkBinding;
                            return self::formatCheckResult($params['champion_id']);
                        }else{
                            return self::formatCheckResult($params['champion_id'],false,true);
                        }

                    },
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'title' => function ($params) {
                        return self::formatCheckResult($params['title'],false,false);
                    },
                    'title_cn' => function ($params) {
                        return self::formatCheckResult($params['title_cn'],false,false);
                    },
                    'primary_role' => function ($params) {
                        return self::formatCheckResult($params['primary_role'],false,false);
                    },
                    'primary_role_cn' => function ($params) {
                        return self::formatCheckResult($params['primary_role_cn'],false,false);
                    },
                    'secondary_role' => function ($params) {
                        return self::formatCheckResult($params['secondary_role'],false,false);
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'hotkey' => function ($params) {
                        if(self::checkIsStringNull($params['hotkey'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['hotkey'],false,false);
                        }
                        $result = self::checkStringToLetter($params['hotkey']);

                        if ($result) {
                            $params['hotkey'] = $result;
                            return self::formatCheckResult($params['hotkey'],false,false);

                        }else {
                            return self::formatCheckResult($params['hotkey'],false,true);
                        }

                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    // 是否删除
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ]
            ],
            //lol道具
            Consts::METADATA_TYPE_LOL_ITEM => [
                "add" => [
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    // 赛事名称(英文)
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost'])){
                            $params['total_cost'] = (int) self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },
                    'is_trinket' => function ($params) {
                        $v=$params['is_trinket'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }


                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'is_purchasable' => function ($params) {
                        $v=$params['is_purchasable'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },

                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
                "change" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                      //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost'])){
                            $params['total_cost'] = (int) self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'is_trinket' => function ($params) {
                        $v=$params['is_trinket'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                   },
                    'is_purchasable' => function ($params) {
                        $v=$params['is_purchasable'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                         return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],

            ],
            ///lol召唤师技能
            Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => [
                "add" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },

                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
                "change"=> [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                    ]
            ],
            //lol符文
            Consts::METADATA_TYPE_LOL_RUNE => [
                "add" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'path_name' => function ($params) {
                        if(self::checkIsStringNull($params['path_name'])){
                            $params['path_name'] = null;
                            return self::formatCheckResult($params['path_name']);
                        }else{
                            return self::formatCheckResult($params['path_name']);
                        }
                    },
                    'path_name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['path_name_cn'])){
                            $params['path_name_cn'] = null;
                            return self::formatCheckResult($params['path_name_cn']);
                        }else{
                            return self::formatCheckResult($params['path_name_cn']);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },

                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
                "change" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'path_name' => function ($params) {
                        if(self::checkIsStringNull($params['path_name'])){
                            $params['path_name'] = null;
                            return self::formatCheckResult($params['path_name']);
                        }else{
                            return self::formatCheckResult($params['path_name']);
                        }
                    },
                    'path_name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['path_name_cn'])){
                            $params['path_name_cn'] = null;
                            return self::formatCheckResult($params['path_name_cn']);
                        }else{
                            return self::formatCheckResult($params['path_name_cn']);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
            ],
            //daota 英雄
            Consts::METADATA_TYPE_DOTA2_HERO => [
                "add" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'title' => function ($params) {
                        if(self::checkIsStringNull($params['title'])){
                            $params['title'] = null;
                            return self::formatCheckResult($params['title']);
                        }else{
                            return self::formatCheckResult($params['title']);
                        }
                    },
                    'title_cn' => function ($params) {
                        if(self::checkIsStringNull($params['title_cn'])){
                            $params['title_cn'] = null;
                            return self::formatCheckResult($params['title_cn']);
                        }else{
                            return self::formatCheckResult($params['title_cn']);
                        }
                    },
                    'talents' => function ($params) {
                        if(self::checkIsStringNull($params['talents'])){
                            $newKeyId = [
                                ['id'=>10 , 'left' => '','right'=>''],
                                ['id'=>15 , 'left' => '','right'=>''],
                                ['id'=>20 , 'left' => '','right'=>''],
                                ['id'=>25 , 'left' => '','right'=>'']
                            ];
                            $params['talents'] = json_encode($newKeyId);

                            return self::formatCheckResult($params['talents'],false,false);
                        }
                        $talent = self::checkDaoTadota2HeroTalent($params['talents']);
                        if ($talent){
                            return self::formatCheckResult($talent);
                        }else{
                            return self::formatCheckResult($params['talents'],false,true);
                        }
                    },
                    'primary_attr' => function ($params) {
                        if(self::checkIsStringNull($params['primary_attr'])){
                            $params['primary_attr'] = null;
                            return self::formatCheckResult($params['primary_attr']);
                        }else{
                            return self::formatCheckResult($params['primary_attr']);
                        }
                    },
                    'primary_attr_cn' => function ($params) {
                        if(self::checkIsStringNull($params['primary_attr_cn'])){
                            $params['primary_attr'] = null;
                            return self::formatCheckResult($params['primary_attr_cn']);
                        }else{
                            return self::formatCheckResult($params['primary_attr_cn']);
                        }
                    },
                    'roles' => function ($params) {
                        if(self::checkIsStringNull($params['roles'])){
                            $params['roles'] = null;
                            return self::formatCheckResult($params['roles']);
                        }else{
                            return self::formatCheckResult($params['roles']);
                        }
                    },
                    'roles_cn' => function ($params) {
                        if(self::checkIsStringNull($params['roles_cn'])){
                            $params['roles_cn'] = null;
                            return self::formatCheckResult($params['roles_cn']);
                        }else{
                            return self::formatCheckResult($params['roles_cn']);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
                "change" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    // 状态
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'title' => function ($params) {
                        if(self::checkIsStringNull($params['title'])){
                            $params['title'] = null;
                            return self::formatCheckResult($params['title']);
                        }else{
                            return self::formatCheckResult($params['title']);
                        }
                    },
                    'title_cn' => function ($params) {
                        if(self::checkIsStringNull($params['title_cn'])){
                            $params['title_cn'] = null;
                            return self::formatCheckResult($params['title_cn']);
                        }else{
                            return self::formatCheckResult($params['title_cn']);
                        }
                    },
                    'talents' => function ($params) {
                        if(self::checkIsStringNull($params['talents'])){
                            return self::formatCheckResult($params['talents'],false,true);
                        }
                        $talent = self::checkDaoTadota2HeroTalent($params['talents']);
                        if ($talent){
                            return self::formatCheckResult($talent);
                        }else{
                            return self::formatCheckResult($params['talents'],false,true);
                        }
                    },
                    'primary_attr' => function ($params) {
                        if(self::checkIsStringNull($params['primary_attr'])){
                            $params['primary_attr'] = null;
                            return self::formatCheckResult($params['primary_attr']);
                        }else{
                            return self::formatCheckResult($params['primary_attr']);
                        }
                    },
                    'primary_attr_cn' => function ($params) {
                        if(self::checkIsStringNull($params['primary_attr_cn'])){
                            $params['primary_attr'] = null;
                            return self::formatCheckResult($params['primary_attr_cn']);
                        }else{
                            return self::formatCheckResult($params['primary_attr_cn']);
                        }
                    },
                    'roles' => function ($params) {
                        if(self::checkIsStringNull($params['roles'])){
                            $params['roles'] = null;
                            return self::formatCheckResult($params['roles']);
                        }else{
                            return self::formatCheckResult($params['roles']);
                        }
                    },
                    'roles_cn' => function ($params) {
                        if(self::checkIsStringNull($params['roles_cn'])){
                            $params['roles_cn'] = null;
                            return self::formatCheckResult($params['roles_cn']);
                        }else{
                            return self::formatCheckResult($params['roles_cn']);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ]
            ],
            //daita 技能
            Consts::METADATA_TYPE_DOTA2_ABILITY => [
                "add" => [
                    'hero_id' => function ($params) {
                        if(self::checkIsStringNull($params['hero_id'])){
                            $params['hero_id'] = null;
                            return self::formatCheckResult($params['hero_id'],false,false);
                        }
                        $checkBinding = self::checkBindingMetadata($params['hero_id'],Consts::METADATA_TYPE_DOTA2_HERO);
                        if($checkBinding){
                            $params['hero_id'] = $checkBinding;
                            return self::formatCheckResult($params['hero_id']);
                        }else{
                            return self::formatCheckResult($params['hero_id'],false,true);
                        }
                    },
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'hotkey' => function ($params) {
                        if(self::checkIsStringNull($params['hotkey'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['hotkey'],false,false);
                        }
                        $result = self::checkStringToLetter($params['hotkey']);

                        if ($result) {
                            $params['hotkey'] = $result;
                            return self::formatCheckResult($params['hotkey'],false,false);

                        }else {
                            return self::formatCheckResult($params['hotkey'],false,true);
                        }

                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },

                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                ],
                "change" => [
                    'hero_id' => function ($params) {
                        if(self::checkIsStringNull($params['hero_id'])){
                            $params['hero_id'] = null;
                            return self::formatCheckResult($params['hero_id'],false,false);
                        }
                        $checkBinding = self::checkBindingMetadata($params['hero_id'],Consts::METADATA_TYPE_DOTA2_HERO);

                        if($checkBinding){
                            $params['hero_id'] = $checkBinding;
                            return self::formatCheckResult($params['hero_id']);
                        }else{
                            return self::formatCheckResult($params['hero_id'],false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },

                    'hotkey' => function ($params) {
                        if(self::checkIsStringNull($params['hotkey'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['hotkey'],false,false);
                        }
                        $result = self::checkStringToLetter($params['hotkey']);

                        if ($result) {
                            $params['hotkey'] = $result;
                            return self::formatCheckResult($params['hotkey'],false,false);

                        }else {
                            return self::formatCheckResult($params['hotkey'],false,true);
                        }

                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },

                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                ],
            ],
            //data 道具
            Consts::METADATA_TYPE_DOTA2_ITEM => [
                "add" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost'])){
                            $params['total_cost'] = (int) self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },

                    'is_trinket' => function ($params) {
                        $v=$params['is_trinket'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'is_recipe' => function ($params) {
                        $v=$params['is_recipe'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    'is_secret_shop' => function ($params) {
                        $v=$params['is_secret_shop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },


                    'is_home_shop' => function ($params) {
                        $v=$params['is_home_shop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    'is_neutral_drop' => function ($params) {
                        $v=$params['is_neutral_drop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ],
                "change" =>[
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    // 赛事名称(中文)
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }else{
                            return self::formatCheckResult($params['name_cn'],false,false);
                        }
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost'])){
                            $params['total_cost'] = (int) self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },

                    'is_trinket' => function ($params) {
                        $v=$params['is_trinket'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'is_recipe' => function ($params) {
                        $v=$params['is_recipe'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    'is_secret_shop' => function ($params) {
                        $v=$params['is_secret_shop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },


                    'is_home_shop' => function ($params) {
                        $v=$params['is_home_shop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    'is_neutral_drop' => function ($params) {
                        $v=$params['is_neutral_drop'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = null;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果是0 则过滤掉，
                        if(self::checkIsNumberZero($v)){
                            return self::formatCheckResult($v,false,true);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'description' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description'] = mb_substr($params['description'],0,999,'utf-8');
                        return self::formatCheckResult($params['description']);
                    },
                    // 描述（中文） 小于1000字，多余部分截断
                    'description_cn' => function ($params) {
                        if(self::checkIsStringNull($params['description'])){
                            $params['description'] = null;
                            return self::formatCheckResult($params['description'],false,false);
                        }
                        $params['description_cn'] = mb_substr($params['description_cn'],0,999,'utf-8');
                        return self::formatCheckResult($params['description_cn']);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                ],
            ],
            //daota 天赋
            Consts::METADATA_TYPE_DOTA2_TALENT =>[
              "add" => [
                  'hero_id' => function ($params) {
                      if(self::checkIsStringNull($params['hero_id'])){
                          $params['hero_id'] = null;
                          return self::formatCheckResult($params['hero_id'],false,false);
                      }
                      $checkBinding = self::checkBindingMetadata($params['hero_id'],Consts::METADATA_TYPE_DOTA2_HERO);

                      if($checkBinding){
                          $params['hero_id'] = $checkBinding;
                          return self::formatCheckResult($params['hero_id']);
                      }else{
                          return self::formatCheckResult($params['hero_id'],false,true);
                      }
                  },
                  'external_id' => function ($params) {
                      if(self::checkIsStringNull($params['external_id'])){
                          $params['external_id'] = null;
                          return self::formatCheckResult($params['external_id'],false,false);
                      }else{
                          return self::formatCheckResult($params['external_id'],false,false);
                      }
                  },
                  'external_name' => function ($params) {
                      if(self::checkIsStringNull($params['external_name'])){
                          return self::formatCheckResult($params['external_name'],false,true);
                      }else{
                          return self::formatCheckResult($params['external_name'],false,false);
                      }
                  },
                  'flag' => function ($params) {
                      return self::formatCheckResult($params['flag'],false,true);
                  },

                  "talent_cn" => function($params) {
                      if(self::checkIsStringNull($params['talent_cn'])){
                          $params['talent_cn'] = null;
                          return self::formatCheckResult($params['talent_cn']);
                      }else{
                          return self::formatCheckResult($params['talent_cn']);
                      }
                  },

                  "talent" => function($params) {
                      if(self::checkIsStringNull($params['talent'])){
                          $params['talent'] = null;
                          return self::formatCheckResult($params['talent']);
                      }else{
                          return self::formatCheckResult($params['talent']);
                      }
                  },

                  'deleted_at' => function ($params) {
                      return self::formatCheckResult($params['deleted_at'],false,true);
                  },
                  'deleted' => function ($params) {
                      $forfeitBool = self::checkStatusBool($params['deleted']);
                      if($forfeitBool){
                          return self::formatCheckResult($params['deleted']);
                      }else{
                          return self::formatCheckResult($params['deleted'],false,true);
                      }
                  },
                  'state' => function ($params) {
                      $v=$params['state'];
                      //如果不存在，则过滤掉
                      if(self::checkStatusBool($v)){
                          return self::formatCheckResult($v);
                      }else{
                          return self::formatCheckResult($v,false,true);
                      }
                  },

                  'left_right' => function ($params) {
                      return self::formatCheckResult($params['left_right'],false);
                  },
                  'level' => function ($params) {
                      return self::formatCheckResult($params['level'],false);
                  },
                  'slug' => function ($params) {
                      return self::formatCheckResult($params['slug'],false,true);
                  },
              ],
              "change" => [
                  'hero_id' => function ($params) {
                      if(self::checkIsStringNull($params['hero_id'])){
                          $params['hero_id'] = null;
                          return self::formatCheckResult($params['hero_id'],false,false);
                      }
                      $checkBinding = self::checkBindingMetadata($params['hero_id'],Consts::METADATA_TYPE_DOTA2_HERO);

                      if($checkBinding){
                          $params['hero_id'] = $checkBinding;
                          return self::formatCheckResult($params['hero_id']);
                      }else{
                          return self::formatCheckResult($params['hero_id'],false,true);
                      }
                  },
                  'external_id' => function ($params) {
                      if(self::checkIsStringNull($params['external_id'])){
                          $params['external_id'] = null;
                          return self::formatCheckResult($params['external_id'],false,false);
                      }else{
                          return self::formatCheckResult($params['external_id'],false,false);
                      }
                  },
                  'external_name' => function ($params) {
                      if(self::checkIsStringNull($params['external_name'])){
                          return self::formatCheckResult($params['external_name'],false,true);
                      }else{
                          return self::formatCheckResult($params['external_name'],false,false);
                      }
                  },

                  'flag' => function ($params) {
                      return self::formatCheckResult($params['flag'],false,true);
                  },
                  "talent_cn" => function($params) {
                      if(self::checkIsStringNull($params['talent_cn'])){
                          $params['talent_cn'] = null;
                          return self::formatCheckResult($params['talent_cn']);
                      }else{
                          return self::formatCheckResult($params['talent_cn']);
                      }
                  },

                    "talent" => function($params) {
                      if(self::checkIsStringNull($params['talent'])){
                          $params['talent'] = null;
                          return self::formatCheckResult($params['talent']);
                      }else{
                          return self::formatCheckResult($params['talent']);
                      }
                  },
                  'deleted_at' => function ($params) {
                      return self::formatCheckResult($params['deleted_at'],false,true);
                  },
                  'deleted' => function ($params) {
                      $forfeitBool = self::checkStatusBool($params['deleted']);
                      if($forfeitBool){
                          return self::formatCheckResult($params['deleted']);
                      }else{
                          return self::formatCheckResult($params['deleted'],false,true);
                      }
                  },
                  'state' => function ($params) {
                      $v=$params['state'];
                      if(self::checkStatusBool($v)){
                          return self::formatCheckResult($v);
                      }else{
                          return self::formatCheckResult($v,false,true);
                      }
                  },
                  'left_right' => function ($params) {
                      if(self::checkIsStringNull($params['left_right'])){
                          $params['level'] = null;
                          return self::formatCheckResult($params['left_right']);
                      }
                      if (self::checkIsPositiveInteger($params['left_right'])){
                          return self::formatCheckResult($params['left_right'],false,false);
                      }else {
                          return self::formatCheckResult($params['left_right'],false,true);

                      }
                  },

                  'level' => function ($params) {
                      if(self::checkIsStringNull($params['level'])){
                          $params['level'] = null;
                          return self::formatCheckResult($params['level']);
                      }
                      if (self::checkIsPositiveInteger($params['level'])){
                          return self::formatCheckResult($params['level'],false,false);
                      }else {
                          return self::formatCheckResult($params['level'],false,true);

                      }
                  },


                  'slug' => function ($params) {
                      return self::formatCheckResult($params['slug'],false,true);
                  },

              ],

            ],
            //CSGO 地图
            Consts::METADATA_TYPE_CSGO_MAP => [
                "add" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn']);
                        }else{
                            return self::formatCheckResult($params['name_cn']);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },

                    'short_name' => function ($params) {
                        if(self::checkIsStringNull($params['short_name'])){
                            $params['short_name'] = null;
                            return self::formatCheckResult($params['short_name']);
                        }else{
                            return self::formatCheckResult($params['short_name']);
                        }
                    },
                    'map_type_cn' => function ($params) {
                        return self::formatCheckResult($params['map_type_cn'],false,true);
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'is_default' => function ($params) {
                        $v=$params['is_default'];
                        //如果不存在，则过滤掉
                        if(self::checkIsStringNull($v)){
                            $v = '2';
                            return self::formatCheckResult($v);
                        }

                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult(2,false,false);
                        }
                        },
                    'map_type' => function ($params) {
                        $v=$params['map_type'];
                        //如果不存在，则过滤掉
                        if(self::checkIsStringNull($v)){
                            $v = '1';
                            return self::formatCheckResult($v);
                        }
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{

                            return self::formatCheckResult("1",false,false);
                        }
                    },

                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                ],
                "change" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    'name_cn' => function ($params) {
                        if(self::checkIsStringNull($params['name_cn'])){
                            $params['name_cn'] = null;
                            return self::formatCheckResult($params['name_cn']);
                        }else{
                            return self::formatCheckResult($params['name_cn']);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },
                    'map_type_cn' => function ($params) {
                        return self::formatCheckResult($params['map_type_cn'],false,true);
                    },
                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'short_name' => function ($params) {
                        if(self::checkIsStringNull($params['short_name'])){
                            $params['short_name'] = null;
                            return self::formatCheckResult($params['short_name']);
                        }else{
                            return self::formatCheckResult($params['short_name']);
                        }
                    },

                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },

                    'is_default' => function ($params) {
                        $v=$params['is_default'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'map_type' => function ($params) {
                        $v=$params['map_type'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },


                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },

                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                ]
            ],
            //CSGO武器
            Consts::METADATA_TYPE_CSGO_WEAPON => [
                "add" => [
                    "cost" => function ($params) {
                        if(self::checkIsStringNull($params['cost'])){
                            $params['cost'] = null;
                            return self::formatCheckResult($params['cost']);
                        }
                        if(self::getStringToNumber($params['cost']) || self::getStringToNumber($params['cost']) == 0){
                            $params['cost'] = (int)self::getStringToNumber($params['cost']);
                            return self::formatCheckResult($params['cost']);
                        }else{
                            return self::formatCheckResult($params['cost'],false,true);
                        }

                    },

                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost']) || self::getStringToNumber($params['total_cost']) == 0){
                            $params['total_cost'] = (int)self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },
                    'kill_award' => function ($params) {
                        if(self::checkIsStringNull($params['kill_award'])){
                            $params['kill_award'] = null;
                            return self::formatCheckResult($params['kill_award']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['kill_award']) || self::getStringToNumber($params['kill_award']) == 0){
                            $params['kill_award'] = (int)self::getStringToNumber($params['kill_award']);
                            return self::formatCheckResult($params['kill_award']);
                        }else{
                            return self::formatCheckResult($params['kill_award'],false,true);
                        }
                    },

                    'ammunition' => function ($params) {
                        if(self::checkIsStringNull($params['ammunition'])){
                            $params['ammunition'] = null;
                            return self::formatCheckResult($params['ammunition']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['ammunition']) || self::getStringToNumber($params['ammunition']) == 0){
                            $params['ammunition'] =  (int)self::getStringToNumber($params['ammunition']);
                            return self::formatCheckResult($params['ammunition']);
                        }else{
                            return self::formatCheckResult($params['ammunition'],false,true);
                        }
                    },


                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    'kind' => function ($params) {
                        if(self::checkIsStringNull($params['kind'])){
                            return self::formatCheckResult($params['kind'],false,true);
                        }

                        if (self::checkIsIdForEnum($params['kind'],Consts::ENUM_CSGO_WEAPON)){

                            return self::formatCheckResult($params['kind'],false,false);

                        }else{
                            return self::formatCheckResult($params['kind'],false,true);

                        }
                    },
                    'state' => function ($params) {
                        $v=$params['state'];
                        // 如果是空，则set，
                        if(self::checkIsStringNull($v)){
                            $v = 1;
                            return self::formatCheckResult($v,false,false);
                        }
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    'capacity' => function ($params) {
                        if(self::checkIsStringNull($params['capacity'])){
                            $params['capacity'] = null;
                            return self::formatCheckResult($params['capacity']);
                        }
                        //如果不存在，则过滤掉

                        if(self::getStringToNumber($params['capacity']) || self::getStringToNumber($params['capacity']) == 0){
                            $params['capacity'] =  (int)self::getStringToNumber($params['capacity']);
                            return self::formatCheckResult($params['capacity']);
                        }else{
                            return self::formatCheckResult($params['capacity'],false,true);
                        }
                    },

                    'reload_time' => function ($params) {
                        return self::formatCheckResult($params['reload_time'],false,false);
                    },

                    'movement_speed' => function ($params) {
                        return self::formatCheckResult($params['movement_speed'],false,false);
                    },
                    'is_ct_available' => function ($params) {
                        if(self::checkIsStringNull($params['is_ct_available'])){
                            $params['is_ct_available'] = null;
                            return self::formatCheckResult($params['is_ct_available']);
                        }
                        if (self::checkStatusBool($params['is_ct_available'])){
                            return self::formatCheckResult($params['is_ct_available'],false,false);
                        }else {
                            return self::formatCheckResult($params['is_ct_available'],false,true);

                        }
                    },

                    'is_t_available' => function ($params) {
                        if(self::checkIsStringNull($params['is_t_available'])){
                            $params['is_t_available'] = null;
                            return self::formatCheckResult($params['is_t_available']);
                        }

                        if (self::checkStatusBool($params['is_t_available'])){
                            return self::formatCheckResult($params['is_t_available'],false,false);
                        }else {
                            return self::formatCheckResult($params['is_t_available'],false,true);

                        }
                    },

                    'firing_mode' => function ($params) {
                        return self::formatCheckResult($params['firing_mode'],false,false);
                    },
                    'data_source' => function ($params) {
                        return self::formatCheckResult($params['data_source'],false,false);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },

                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },
                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
//                    'image' => function ($params) {
//                        if (self::checkIsStringDataNull($params['image'])){
//                            return self::formatCheckResult($params['image'],false,true);
//                        }else{
//                            return self::formatCheckResult($params['image'],false,false);
//                        }
//                    },
                ],
                "change" => [
                    'name' => function ($params) {
                        if(self::checkIsStringNull($params['name'])){
                            return self::formatCheckResult($params['name'],false,true);
                        }else{
                            return self::formatCheckResult($params['name'],false,false);
                        }
                    },
                    'kind' => function ($params) {
                        if(self::checkIsStringNull($params['kind'])){
                            return self::formatCheckResult($params['kind'],false,true);
                        }

                        if (self::checkIsIdForEnum($params['kind'],Consts::ENUM_CSGO_WEAPON)){

                            return self::formatCheckResult($params['kind'],false,false);

                        }else{
                            return self::formatCheckResult($params['kind'],false,true);

                        }
                   },

                    'state' => function ($params) {
                        $v=$params['state'];
                        //如果不存在，则过滤掉
                        if(self::checkStatusBool($v)){
                            return self::formatCheckResult($v);
                        }else{
                            return self::formatCheckResult($v,false,true);
                        }
                    },
                    "cost" => function ($params) {
                        if(self::checkIsStringNull($params['cost'])){
                            $params['cost'] = null;
                            return self::formatCheckResult($params['cost']);
                        }
                        if(self::getStringToNumber($params['cost']) || self::getStringToNumber($params['cost']) == 0){
                            $params['cost'] = (int)self::getStringToNumber($params['cost']);
                            return self::formatCheckResult($params['cost']);
                        }else{
                            return self::formatCheckResult($params['cost'],false,true);
                        }

                    },

                    'total_cost' => function ($params) {
                        if(self::checkIsStringNull($params['total_cost'])){
                            $params['total_cost'] = null;
                            return self::formatCheckResult($params['total_cost']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['total_cost']) || self::getStringToNumber($params['total_cost']) == 0){
                            $params['total_cost'] = (int)self::getStringToNumber($params['total_cost']);
                            return self::formatCheckResult($params['total_cost']);
                        }else{
                            return self::formatCheckResult($params['total_cost'],false,true);
                        }
                    },
                    'kill_award' => function ($params) {
                        if(self::checkIsStringNull($params['kill_award'])){
                            $params['kill_award'] = null;
                            return self::formatCheckResult($params['kill_award']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['kill_award']) || self::getStringToNumber($params['kill_award']) == 0){
                            $params['kill_award'] = (int)self::getStringToNumber($params['kill_award']);
                            return self::formatCheckResult($params['kill_award']);
                        }else{
                            return self::formatCheckResult($params['kill_award'],false,true);
                        }
                    },

                    'ammunition' => function ($params) {
                        if(self::checkIsStringNull($params['ammunition'])){
                            $params['ammunition'] = null;
                            return self::formatCheckResult($params['ammunition']);
                        }
                        //如果不存在，则过滤掉
                        if(self::getStringToNumber($params['ammunition']) || self::getStringToNumber($params['ammunition']) == 0){
                            $params['ammunition'] =  (int)self::getStringToNumber($params['ammunition']);
                            return self::formatCheckResult($params['ammunition']);
                        }else{
                            return self::formatCheckResult($params['ammunition'],false,true);
                        }
                    },

                    'capacity' => function ($params) {
                        if(self::checkIsStringNull($params['capacity'])){
                            $params['capacity'] = null;
                            return self::formatCheckResult($params['capacity']);
                        }
                        //如果不存在，则过滤掉

                        if(self::getStringToNumber($params['capacity']) || self::getStringToNumber($params['capacity']) == 0){
                            $params['capacity'] =  (int)self::getStringToNumber($params['capacity']);
                            return self::formatCheckResult($params['capacity']);
                        }else{
                            return self::formatCheckResult($params['capacity'],false,true);
                        }
                    },

                    'reload_time' => function ($params) {
                        return self::formatCheckResult($params['reload_time'],false,false);
                    },
                    'movement_speed' => function ($params) {
                        return self::formatCheckResult($params['movement_speed'],false,false);
                    },
                    'is_ct_available' => function ($params) {
                        if(self::checkIsStringNull($params['is_ct_available'])){
                            $params['is_ct_available'] = null;
                            return self::formatCheckResult($params['is_ct_available']);
                        }
                        if (self::checkStatusBool($params['is_ct_available'])){
                            return self::formatCheckResult($params['is_ct_available'],false,false);
                        }else {
                            return self::formatCheckResult($params['is_ct_available'],false,true);

                        }
                    },

                    'is_t_available' => function ($params) {
                        if(self::checkIsStringNull($params['is_t_available'])){
                            $params['is_t_available'] = null;
                            return self::formatCheckResult($params['is_t_available']);
                        }

                        if (self::checkStatusBool($params['is_t_available'])){
                            return self::formatCheckResult($params['is_t_available'],false,false);
                        }else {
                            return self::formatCheckResult($params['is_t_available'],false,true);

                        }
                    },

                    'firing_mode' => function ($params) {
                        return self::formatCheckResult($params['firing_mode'],false,false);
                    },
                    'data_source' => function ($params) {
                        return self::formatCheckResult($params['data_source'],false,false);
                    },
                    'deleted_at' => function ($params) {
                        return self::formatCheckResult($params['deleted_at'],false,true);
                    },
                    'deleted' => function ($params) {
                        $forfeitBool = self::checkStatusBool($params['deleted']);
                        if($forfeitBool){
                            return self::formatCheckResult($params['deleted']);
                        }else{
                            return self::formatCheckResult($params['deleted'],false,true);
                        }
                    },
                    'external_id' => function ($params) {
                        if(self::checkIsStringNull($params['external_id'])){
                            $params['external_id'] = null;
                            return self::formatCheckResult($params['external_id'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_id'],false,false);
                        }
                    },
                    'external_name' => function ($params) {
                        if(self::checkIsStringNull($params['external_name'])){
                            $params['external_name'] = null;
                            return self::formatCheckResult($params['external_name'],false,false);
                        }else{
                            return self::formatCheckResult($params['external_name'],false,false);
                        }
                    },

                    'flag' => function ($params) {
                        return self::formatCheckResult($params['flag'],false,true);
                    },

                    'slug' => function ($params) {
                        return self::formatCheckResult($params['slug'],false,true);
                    },
//                    'image' => function ($params) {
//                        if (self::checkIsStringDataNull($params['image'])){
//                            return self::formatCheckResult($params['image'],false,true);
//                        }else{
//                            return self::formatCheckResult($params['image'],false,false);
//                        }
//                    },
                ]
            ]


        ];
        if (isset($configMap[$resourceType]) && isset($configMap[$resourceType][$tag])) {
            return $configMap[$resourceType][$tag];
        } else {
            return [];
        }
    }

    public static function checkBindingMetadata($relIdentityId,$metadata_type){
        $std = \app\modules\data\models\StandardDataMetadata::find()
            ->select(["id","rel_identity_id"])
            ->where(["metadata_type"=>$metadata_type,"deal_status"=>4,'rel_identity_id'=>$relIdentityId])->asArray()->one();
        if (empty($std)) {
            return false;
        }
        $rel = DataStandardMasterRelation::find()
            ->select(['master_id','standard_id'])
            ->where(['resource_type'=>$metadata_type,'standard_id'=>$std['id']])
            ->asArray()->one();
        if (empty($rel)) {
            return false;
        }
        return $rel['master_id'];
    }


    /**
     * 验证时间信息(是否是年月日)
     * $timeInfo        时间信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkTimeInfo($timeInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $timeInfo;//当前值
        $after = $before;//验证后的值
        if ($before !== "") {
            if (date('Y-m-d', strtotime($before) == $before)) {
                $isRight = true;//是否正确
                $isBlack = false;//是否为空
                $after = $before;//处理之后的值
                $error_msg = "";//错误信息 
            } else {
                $isRight = false;//是否正确
                $isBlack = true;//是否为空
                $after = "";//处理之后的值
                $error_msg = "";//错误信息                 
            }
        } else {
            $isRight = true;//是否正确
            $isBlack = true;//是否为空
            $after = "";//处理之后的值
            $error_msg = "";//错误信息             
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证赛事状态
     * $statusInfo      状态信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkStatusInfo($statusInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $statusInfo;//当前值
        $after = $before;//验证后的值
        if ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;//是否正确
            $isBlack = true;//是否为空
            $after = "";//处理之后的值
            $error_msg = "";//错误信息   
        }
        if ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $after = self::checkPositiveInteger($after);
            if (!$after) {
                $isRight = false;//是否正确
                $isBlack = true;//是否为空
                $after = "";//处理之后的值
                $error_msg = "状态错误!";//错误信息     
            } else {
                $after = EnumMatchState::find('id')->where(['id' => $after])->one();
                if (!$after) {
                    $isRight = false;//是否正确
                    $isBlack = true;//是否为空
                    $after = "";//处理之后的值
                    $error_msg = "状态错误!";//错误信息                     
                } else {
                    $isRight = true;//是否正确
                    $isBlack = false;//是否为空
                    $after = $before;//处理之后的值
                    $error_msg = "";//错误信息                     
                }
            }
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }


    /**
     * 验证俱乐部ID
     * $clanIdInfo      俱乐部ID信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkClanId($clanIdInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $clanIdInfo;//当前值
        $after = $before;//验证后的值
        if ($resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;//是否正确
            $isBlack = true;//是否为空
            $after = "";//处理之后的值
            $error_msg = "";//错误信息            
        }
        // 战队数据源更新为空
        if ($clanIdInfo == "" && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;//是否正确
            $isBlack = true;//是否为空
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        // 战队数据源更新不为空
        if ($clanIdInfo && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $clanIdInfo = self::checkPositiveInteger($clanIdInfo);
            if ($clanIdInfo) {
                $clanIdInfo = Clan::find('id')->where(['id' => $clanIdInfo])->one();
                if ($clanIdInfo) {
                    $isRight = true;//是否正确
                    $isBlack = false;//是否为空
                    $after = $after;//处理之后的值
                    $error_msg = "";//错误信息
                } else {
                    $isRight = false;//是否正确
                    $isBlack = true;//是否为空
                    $after = "";//处理之后的值
                    $error_msg = "";//错误信息
                }
            }
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }
    /**
     * 验证游戏ID
     * $gameIdInfo      游戏ID信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkGameId($gameIdInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $gameIdInfo;//当前值
        $after = $before;//验证后的值
        $after = self::checkPositiveInteger($gameIdInfo);
        //游戏ID为正整数时在enum_game表中是否存在
        if ($after != "") {
            $after = EnumGame::find('id')->where(['id' => $after])->one();
            if (!$after) {
                $isRight = false;//是否正确
                $isBlack = true;//是否为空
                $after = "";//处理之后的值
                $error_msg = "";//错误信息
            } else {
                $isRight = true;//是否正确
                $isBlack = false;//是否为空
                $after = $before;//处理之后的值
                $error_msg = "";//错误信息                
            }
        }
        // 选手数据源新增
        if (!$after && $resourceType == Consts::RESOURCE_TYPE_PLAYER && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "游戏ID错误!";//错误信息
        }
        // 选手数据源更新
        if ($after == Consts::RESOURCE_TYPE_PLAYER && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        // 战队数据源新增
        if (!$after && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "游戏ID错误!";//错误信息
        }
        // 战队数据源更新
        if ($resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        // 赛事数据源新增
        if (!$after && $resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "游戏项目错误!";//错误信息            
        }
        // 赛事数据源更新
        if ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "!";//错误信息 
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证删除状态
     * $deletedInfo     是否删除信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkDeleted($deletedInfo, $resourceType='', $tag='')
    {
        $isBlack = true;//是否为空:true.是,false.否
        if (in_array($deletedInfo, [1, 2])) {
            $isRight = true;
        } else {
            $isRight = false;
        }
        $before = $deletedInfo;
        $after = $before;
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
        ];
    }

    public static function checkStatusBool($status,$isTrue = false)
    {
        if (in_array($status, [1, 2])) {
            $isRight = true;
        } else {
            $isRight = false;
        }
       return $isRight;
    }

    /**
     * 验证图片
     * $imageInfo       图片信息
     * $resourceType    类
     * $tag             子类
     * TODO:服务器地址
     */
    public static function checkImage($imageInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $imageInfo;//当前值
        $after = $before;//验证后的值
        if ($imageInfo) {
            $isRight = true;
            $isBlack = false;
            $after = $after;//处理之后的值TODO::服务器地址拼接
            $error_msg = "";//错误信息
        }
        return [
            'before' => $before,
            'after' => $imageInfo,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证Slug
     * $slugInfo        Slug信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkSlug($slugInfo, $resourceType, $tag)
    {
        $before = $slugInfo;//当前值
        // 处理
        $isRight = false;
        $isBlack = true;
        $after = "";//处理之后的值
        $error_msg = "";//错误信息
        // 战队数据源新增
        if (!$after && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = true;
            $isBlack = false;
            $after = $before;//处理之后的值
            $error_msg = "";//错误信息
        }
        if (!$after && $resourceType == Consts::RESOURCE_TYPE_PLAYER && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = true;
            $isBlack = false;
            $after = $before;//处理之后的值
            $error_msg = "";//错误信息
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证别名
     * $aliasInfo       别名信息
     * $resourceType    类
     * $tag             子类
     * TODO:增量更新
     */
    public static function checkAlias($aliasInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $aliasInfo;//当前值
        $after = $before;//验证后的值
        //俱乐部数据源新增
        if (!$aliasInfo && $resourceType == Consts::RESOURCE_TYPE_CLAN && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = true;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        //俱乐部数据源更新
        if (!$aliasInfo && $resourceType == Consts::RESOURCE_TYPE_CLAN && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        //战队数据源更新
        if ($resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = true;
            $isBlack = true;
            $after = $aliasInfo;//处理之后的值
            $error_msg = "";//错误信息
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证名称
     * $name            名称信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkName($nameInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $nameInfo;//当前值
        $after = $before;//验证后的值
        $error_msg = '';
        //俱乐部,选手数据源新增
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_CLAN && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "俱乐部名称不能为空!";//错误信息
        }
        //俱乐部,选手数据源更新
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_CLAN && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息
        }
        // 选手,数据源新增
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_PLAYER && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "选手昵称不能为空!";//错误信息            
        }
        // 选手,数据源更新
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_PLAYER && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息 
        }
        // 战队,数据源新增
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "战队名称不能为空!";//错误信息            
        }
        // 战队,数据源更新
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_TEAM && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息 
        }
        // 赛事,数据源新增
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_ADD) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "赛事名称(英文)不能为空!";//错误信息            
        }
        // 赛事,数据源更新
        if (!$nameInfo && $resourceType == Consts::RESOURCE_TYPE_TOURNAMENT && $tag == QueueServer::QUEUE_TYPE_CHANGE) {
            $isRight = false;
            $isBlack = true;
            $after = "";//处理之后的值
            $error_msg = "";//错误信息            
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     * 验证国家
     * $name            国家信息
     * $resourceType    类
     * $tag             子类
     */
    public static function checkCountry($countryInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $countryInfo;//当前值
        $after = $before;//验证后的值
        $error_msg = "";
        if ($after !== "") {
            $verify = self::checkPositiveInteger($before);
            $countryID = self::checkIsIdForEnum($verify,ENUM_COUNTRY);
            if ($countryID) {
                $isRight = true;//是否正确
                $isBlack = false;//是否为空
                $after = $verify;
                $error_msg = "";//错误信息
            } else {
                $isRight = false;//是否正确
                $isBlack = false;//是否为空
                $after = "";//处理之后的值
                $error_msg = "";//错误信息
            }
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }
    /**
     * @param $nameInfo
     * @param $resourceType
     * @param $tag
     * @return array(判断选手平均年龄)
     */
    public static function checkplayersAge($nameInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = $nameInfo;//当前值
        $after = $before;//验证后的值
        $error_msg = "";
        if ($after != '') {
            $after = StringHelper::checkStringToNumber($before);
            if (!$after) {
                $isRight = false;//是否正确
                $isBlack = true;//是否为空
                $after = "";//处理之后的值
                $error_msg = "";//错误信息
            }
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    public static function checkHistoryPlayers($nameInfo, $resourceType, $tag)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = true;//是否为空:true.是,false.否
        $before = json_decode($nameInfo);//当前值
        $after = $before;//验证后的值
        $error_msg = "";
        if ($after != '') {
            foreach ($after as $item) {
                $num = StringHelper::checkString($item);
                $transItem = str_replace(',', '', $num[0]);
            }
            if (!$after) {
                $isRight = false;//是否正确
                $isBlack = true;//是否为空
                $after = "";//处理之后的值
                $error_msg = "";//错误信息
            }
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }

    /**
     *验证是不是字符串类型
     * @param $string
     * @return array
     */
    public static function checkIsString($string)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = false;//是否为空:true.是,false.否
        $before = $string;//当前值
        $after = $before;//验证后的值
        $error_msg = "";
        $data = is_string($after);
        if (!$data) {
            $isRight = false;//是否正确
            $isBlack = true;//是否为空
            $after = '';
            $error_msg = "";//错误信息
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }


    /**
     * 是否为数字数字（平均年龄）：正则匹配（如果有小数点保留小数点和小数点后的数字）
     * @param $num
     * @return int|string
     */
    public static function checkIsAverageNum($num)
    {
        if(is_numeric($num) && $num > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断是否是数字
     * @param $num
     * @return array
     */
    public static function checkIsNum($num)
    {
        $isRight = true;//是否正确:true.正确,false.错误
        $isBlack = false;//是否为空:true.是,false.否
        $before = $num;//当前值
        $after = $before;//验证后的值
        $error_msg = "";
        $data = is_numeric($after);
        if (!$data) {
            $isRight = false;//是否正确
            $isBlack = true;//是否为空
            $after = '';
            $error_msg = "";//错误信息
        }
        return [
            'before' => $before,
            'after' => $after,
            'is_right' => $isRight,
            'is_black' => $isBlack,
            'error_msg' => $error_msg,
        ];
    }


    /**
     *长文本（各种简介）：多于1000字的部分裁剪掉
     * @param $data
     * @return string
     */
    public static function checkGetContent($data)
    {
        $strlen = mb_strlen($data);
        if ($strlen > 1000) {
            $data = mb_substr($data, 0, 1000);
        }
        return $data;
    }

    /**
     * $type = "Y-m-d"
     * $type = "Y-m-d H:i:s"
     * @param $date
     * @param $type
     * @return int
     */
    public static function checkTime($date, $type)
    {
        if (is_int($date) || is_numeric($date)) {
            if ($date <= 2147454847) { // 2038-01-19 03:14:07
                $datas = date($type, $date);
            }
        } else {
            $datas = date($type, strtotime($date));
        }
        $preg = '/^([12]\d\d\d)-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[0-1]) ([0-1]\d|2[0-4]):([0-5]\d)(:[0-5]\d)|([12]\d\d\d)-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[0-1])?$/';
        $data = preg_match($preg, $datas);
        if ($data) {
            return $datas;
        }
    }

    /**
     * 验证是否为正整数
     * $value       需要验证的参数
     */
    public static function checkPositiveInteger($value)
    {
        if (!is_numeric($value) || strpos($value, ".") || $value < 1) {
            return "";
        }
        return $value;
    }

    /**
     * @param $value
     * 是否为整数数
     */
    public static function checkIsPositiveInteger($value)
    {
        if (!is_numeric($value) || strpos($value, ".") || $value < 1) {
            return false;
        }else{
            return true;
        }
    }
    /**
     * @param $value
     * 是否为整数数
     */
    public static function checkIsTeamScoureInteger($value)
    {
        if (!is_numeric($value) || strpos($value, ".")) {
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $v
     * @return bool
     * 验证是否是字符串空或者null
     */
    public static function checkIsStringNull($v)
    {
//        if (empty($v) && ($v !== 0 || $v !== '0')) {
//            return true;
//        }
//        return false;
        if($v === null || $v === ''){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $v
     * @return bool
     * 验证是否是数字0
     */
    public static function checkIsNumberZero($v)
    {
        if ($v === 0 || $v === '0') {
            return true;
        }
        return false;
    }

    /**
     * @param $v
     * @return bool
     */
    public static function checkIsStringStringNull($v)
    {
        if ($v === 'null' || $v === 'Null') {
            return true;
        }
        return false;
    }

    /**
     * @param $v
     * @return bool
     */
    public static function checkIsStringDataNull($v)
    {
        if (isset($v) && empty($v)) {
            return true;
        }
        return false;
    }


    /**
     * @param $v
     * @param $enumType
     * @return bool
     * 是否为id,传入对应的值和码表类型
     */
    public static function checkIsIdForEnum($v, $enumType, $gameId = '')
    {
        if (!is_numeric($v)) {
            return false;
        }
        //$defaultGameRuleId = EnumGameRules::find()->select('id')->where(['id' => $value])->one()['id'];
        $isHave = false;
        switch ($enumType) {
            case Consts::ENUM_COUNTRY:
                $isHave = EnumCountry::find()->where(['id' => $v])->one();
                break;
            case Consts::ENUM_GAME:
                $isHave = EnumGame::find()->where(['id' => $v])->one();
                break;
            case Consts::ENUM_GAME_MATCH_TYPE:
                $isHave = EnumGameMatchType::find()->where(['id' => $v])->one();
                break;
            case Consts::ENUM_POSITION:
                $isHave = EnumPosition::find()->where(['id' => $v, 'game_id' => $gameId])->one();
                break;
            case Consts::ENUM_GAME_RULES:
                $isHave = EnumGameRules::find()->where(['id' => $v])->one();
                break;
            case Consts::ENUM_MATCH_STATE:
                $isHave = EnumMatchState::find()->where(['id' => $v])->one();
                break;
            case Consts::METADATA_TYPE_LOL_ABILITY:
                $isHave = MetadataLolChampion::find()->where(['id' => $v])->one();
                break;
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
                $isHave = MetadataDota2Hero::find()->where(['id' => $v])->one();
                break;
            case Consts::ENUM_CSGO_WEAPON:
                $isHave = EnumCsgoWeapon::find()->where(['id' => $v])->one();
                break;
            default:
                return false;
        }
        if ($isHave) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $string
     * @return int
     * 是否为正整数（世界排名、总奖金、赛制局数）：正则匹配（去掉特殊字符，如果有小数点保留整数）
     */
    public static function getStringToNumber($string)
    {
        $patterns = "/[\d\.,]+/";
        preg_match_all($patterns,$string,$arr);
        $result = implode('',$arr[0]);
        $res = str_replace(",","",$result);
        if($res){
            return intval($res);
        }
    }

    //从字符串提取字母
    public static function checkStringToLetter($string){
        $patterns = "/[a-zA-Z]|[\d]+/";
        preg_match_all($patterns,$string,$arr);
        if (empty($arr[0])){
            return false;
        }
        $result = implode('',$arr[0]);
        return $result;
    }
    //从字符串提取数字
    public static function checkNum($string){
        $patterns = "/[\d]+/";
        preg_match_all($patterns,$string,$arr);
        if (empty($arr[0])){
            return false;
        }
        $result = implode('',$arr[0]);
        return $result;
    }

    public static function formatCheckResult($outPut,$sendError=false,$unset=false,$msg="")
    {
        return [
            'output' => $outPut,
            'check' => $sendError,
            'unset' => $unset,
            'error_msg' => $msg,
        ];
    }

    public static function checkDaoTadota2HeroTalent($talent){

//        $a = '[{"id":10,"left":"34","right":"16"},{"id":15,"left":"54","right":"1"},{"id":20,"left":"7","right":"32"},{"id":25,"left":"33","right":"43"}]';
        $talents = json_decode($talent,true);
        if (!is_array($talents)) {
            return false;
        }
        $error = json_last_error();
        if (!empty($error)) {
            return false;
        }
        $newarr = [];
        foreach ($talents as $key => $val) {
            if (!in_array($val['id'],[10,15,20,25])){
                unset($val);
                $isKey = [ 0 => 10, 1 => 15 , 2 => 20 , 3 => 25];
                if ($isKey[$key]) {
                    $newarr[$key] = ['id'=>$isKey[$key],'left' => '','right'=>''];
                }
                continue;
            };

            if (!empty($talents[$key])){
                $new = array_slice($talents[$key],-2);
            }
            $newarr[$key]['id'] = $val['id'];
            foreach ($new as $k => $v){
                $std = \app\modules\data\models\StandardDataMetadata::find()
                    ->select(["id","rel_identity_id"])
                    ->where(["metadata_type"=>"dota2_talent","deal_status"=>4,'rel_identity_id'=>$v])->asArray()->one();
                if ($std){
                    $rel = DataStandardMasterRelation::find()
                        ->select(['master_id','standard_id'])
                        ->where(['resource_type'=>'dota2_talent','standard_id'=>$std['id']])
                        ->asArray()->one();
                    if (!empty($rel)) {
                        $newarr[$key][$k] = $rel['master_id'];
                    }else{
                        $newarr[$key][$k] = '';
                    }
                }else{
                    $newarr[$key][$k] = '';

                }

            }

        }
        $newJson = json_encode(array_values($newarr));
        return  $newJson;
    }

    public static function defultGameRulesIdByRuleId($gameId,$field='')
    {

        return EnumGame::find()->select($field)->where(['id' => $gameId])->one()[$field];
    }
}