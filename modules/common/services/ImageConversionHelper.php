<?php
/**
 *
 */

namespace app\modules\common\services;


use app\modules\common\models\EnumGame;
use app\modules\common\models\HelperImageToAli;
use app\modules\common\models\UploadImageLog;
use app\modules\file\services\UploadService;
use app\modules\task\services\Common;
use app\rest\exceptions\BusinessException;
use Swlib\Util\Helper;

class ImageConversionHelper
{
    const RESIZE_TYPE_EQUIVALENCY = 'equivalency';
    const RESIZE_TYPE_FIXED = 'fixed';
    const RESIZE_TYPE_MFIT = 'mfit';

    // 阿里的图片可以自定义大小，这个用来加resize后缀
    public static function showSizeConversion_copy($url, $resizeConfig, $type = "ali")
    {
        $url=self::purifyImage($url);
        // svg 格式不能转
        $urlDataInfo = (pathinfo($url));
        if(!isset($urlDataInfo['extension'])) return;
        if(!isset($resizeConfig['width'])){
            return $url.'?x-oss-process=image/resize,h_'.$resizeConfig['height'];
        }
        $ext = $urlDataInfo['extension'];
        if ($ext == 'svg') {
            return $url;
        }
        if(!strpos($url,'aliyuncs')){
            return $url;
        }
        $extStr = '';
        $resizeType = $resizeConfig['resize_type'];
        switch ($resizeType) {
            case self::RESIZE_TYPE_EQUIVALENCY:
                $maxLength = $resizeConfig['max'];
                $extStr = sprintf('resize,l_%s', $maxLength);
                break;
            case self::RESIZE_TYPE_FIXED:
                $width = $resizeConfig['width'];
                $height = $resizeConfig['height'];
                $extStr = sprintf('resize,m_fixed,h_%s,w_%s', $height, $width);
                break;
            case self::RESIZE_TYPE_MFIT:
                $width = $resizeConfig['width'];
                $height = $resizeConfig['height'];
                $extStr = sprintf('resize,m_mfit,h_%s,w_%s', $height, $width);
                break;
            default:
                break;
        }
        return $url . '?x-oss-process=image/' . $extStr;
    }
    public static function showSizeConversion($url, $resizeConfig, $type = "ali")
    {
        $url=self::purifyImage($url);
        // svg 格式不能转
        $urlDataInfo = (pathinfo($url));
        $ext = isset($urlDataInfo['extension'])?$urlDataInfo['extension']:"";
        if ($ext == 'svg') {
            return $url;
        }
        if(!strpos($url,'aliyuncs') && !strpos($url,'elementsdata') && !strpos($url,'elements-data')){
            return $url;
        }
        $extStr = '';
        $resizeType = $resizeConfig['resize_type'];
        switch ($resizeType) {
            case self::RESIZE_TYPE_EQUIVALENCY:
                $maxLength = $resizeConfig['max'];
                $extStr = sprintf('resize,l_%s', $maxLength);
                break;
            case self::RESIZE_TYPE_FIXED:
                $width = $resizeConfig['width'];
                $height = $resizeConfig['height'];
                $extStr = sprintf('resize,m_fixed,h_%s,w_%s', $height, $width);
                break;
            case self::RESIZE_TYPE_MFIT:
                $width = $resizeConfig['width'];
                $height = $resizeConfig['height'];
                $extStr = sprintf('resize,m_mfit,h_%s,w_%s', $height, $width);
                break;
            default:
                break;
        }
        return $url . '?x-oss-process=image/' . $extStr;
    }

    public static function showFixedSizeConversion($url, $height, $width, $type = "ali")
    {
        $resizeConfig = [
            'resize_type' => self::RESIZE_TYPE_FIXED,
            'width' => $width,
            'height' => $height,
        ];
        return self::showSizeConversion($url, $resizeConfig, $type);
    }
    public static function showMfitSizeConversion($url, $height, $width, $type = "ali")
    {
        $resizeConfig = [
            'resize_type' => self::RESIZE_TYPE_MFIT,
            'width' => $width,
            'height' => $height,
        ];
        return self::showSizeConversion($url, $resizeConfig, $type);
    }

    public static function purifyImage($url, $type='ali')
    {
        $urlArr=explode('?',$url);
        return $urlArr[0];
    }

    public static function mvImageToAli($url)
    {
        // 检查是否已经存过，如果没有，则上传到阿里，如果存在，就调用已经存在的url
        $info = HelperImageToAli::find()->where(['origin_url' => $url])->one();
        if ($info) {
            //hltv默认图
            if($info['origin_url'] == "https://img.elementsdata.cn/hltv_img/csgo.png" || $info['origin_url'] == "https://img.elementsdata.cn/game_logo_default/csgo.png"){
                $enumGame = EnumGame::find()->select('simple_image')->where(['slug'=>'csgo'])->one();
                if($enumGame){
                   return $enumGame['simple_image'];
                }
            }
        } else {
            //hltv默认图
            if($url == "https://img.elementsdata.cn/hltv_img/csgo.png" || $url == "https://img.elementsdata.cn/game_logo_default/csgo.png"){
                $enumGame = EnumGame::find()->select('simple_image')->where(['slug'=>'csgo'])->one();
                if($enumGame){
                    return $enumGame['simple_image'];
                }
            }
            // 下载url
//            $localInfo = self::getImage($url);
//            $localPath=$localInfo['save_path'];

            if(strpos($url,"https://admin.esportsdirectory.info") !== false){  //bayes下载
                $localPath = \app\modules\common\services\ImageConversionHelper::downBayesFile($url);
                if(!$localPath){
                    $uploadImageLog = new UploadImageLog();
                    $uploadImageLog->setAttributes([
                        'origin_url'=>$url,
                        'info'=>"下载资源失败"
                    ]);
                    $uploadImageLog->save();
                    throw new BusinessException([], '下载失败');
                }
            }
            else{
                $localPath = self::downfile($url);
                if(!$localPath){
                    $uploadImageLog = new UploadImageLog();
                    $uploadImageLog->setAttributes([
                        'origin_url'=>$url,
                        'info'=>"下载资源失败"
                    ]);
                    $uploadImageLog->save();
                    throw new BusinessException([], '下载失败');
                }
            }
            $fileName = basename($localPath);

            $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
            if($ext == "jpg" || $ext == "jpeg"){
                $imageResource = imagecreatefromjpeg($localPath);
                if(!$imageResource){
                    $imageResource = imagecreatefrompng($localPath);
                }
                if(!$imageResource){
                    $uploadImageLog = new UploadImageLog();
                    $uploadImageLog->setAttributes([
                        'origin_url'=>$url,
                        'info'=>"下载资源失败"
                    ]);
                    $uploadImageLog->save();
                    throw new BusinessException([], '图片转换失败');
                }
                $postion = strrpos($localPath,'.',0);
                $localPath = substr($localPath,0,$postion).'.png';
                $res = imagepng($imageResource,$localPath);
                if($res){
                    $ext = "png";
                }else{
                    throw new BusinessException($info->getErrors(), '转存png失败');
                }
            }
            $info = new HelperImageToAli();
            $dst = "els/".base64_encode(date("ymd")).'/'.substr(md5(uniqid(mt_rand(), true)),1,12).'.'.$ext;
            $aliOriginUrl = UploadService::upload($dst, $localPath);
            if(!$aliOriginUrl){
                throw new BusinessException($info->getErrors(), '上传阿里云osss失败');
            }
            $aliUrl = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
            $is = strstr($aliUrl, 'http');
            if($is){
                $aliUrl = str_replace("http","https",$aliUrl);
            }
            $attributeData = [
                'origin_url' => $url,
                'ali_url' => $aliUrl,
                'hk_url' => $aliUrl,
                'identity_key' => md5($url)
            ];
            $info->setAttributes($attributeData);
//            $info->setAttributes([
//                'origin_url' => $url,
//                'ali_url' => $aliUrl,
//                'identity_key' => md5($url)
//            ]);
            if (!$info->save()) {
                throw new BusinessException($info->getErrors(), '转存失败');
            }
        }

        return $info['ali_url'];
    }

    public static function showHeightConversion($url, $height, $type = "ali")
    {
        if(isset($url)){
            $resizeConfig = [
                'resize_type' => self::RESIZE_TYPE_FIXED,
                'height' => $height,
            ];
            return self::showSizeConversion($url, $resizeConfig, $type);
        }
    }

    //把图片下载到本地的目录，返回本地目录
    public static function imgDownLocal($img_url)
    {
        $path = '';             //默认路径
        $cacheDir = \Yii::$app->basePath . '/downloadTmp';      //图片本地目录地址
        if (!is_dir($cacheDir)) {                         //如果目录不存在
            mkdir($cacheDir, 0777, true);                 //创建目录
        }

        $cmd = "wget --no-check-certificate -P {$cacheDir} {$img_url}";
        exec($cmd, $output, $returnValue);
        print_r($output);
        if ($returnValue == 0) {
            $path = $cacheDir . '/' . basename($img_url);
        }
        return $path;
    }

    public static function getImage($url, $save_dir = '', $filename = '', $type = 1)
    {
//        if(strpos($url,'pandascore.co')){
//            $url='http://47.75.63.34/reget.php?url='.$url;
//        }
        $save_dir=\Yii::$app->basePath . '/downloadTmp/'.md5(time());
        $filename=basename($url);
        if (trim($url) == '') {
            return array('file_name' => '', 'save_path' => '', 'error' => 1);
        }
        if (trim($save_dir) == '') {
            $save_dir = './';
        }
        if (trim($filename) == '') {//保存文件名
            $ext = strrchr($url, '.');
            if ($ext != '.gif' && $ext != '.jpg') {
                return array('file_name' => '', 'save_path' => '', 'error' => 3);
            }
            $filename = time() . $ext;
        }
        if (0 !== strrpos($save_dir, '/')) {
            $save_dir .= '/';
        }
//创建保存目录
        if (!file_exists($save_dir) && !mkdir($save_dir, 0777, true)) {
            return array('file_name' => '', 'save_path' => '', 'error' => 5);
        }
//获取远程文件所采用的方法
        if ($type) {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $img = curl_exec($ch);
            curl_close($ch);
        } else {
            ob_start();
            readfile($url);
            $img = ob_get_contents();
            ob_end_clean();
        }
//$size=strlen($img);
//文件大小
        print_r($save_dir . $filename);
        $fp2 = @fopen($save_dir . $filename, 'a');
        fwrite($fp2, $img);
        fclose($fp2);
        unset($img, $url);
        return array('file_name' => $filename, 'save_path' => $save_dir . $filename, 'error' => 0);
    }

    public static function downfile($httpUrl)
    {
        $url=$httpUrl;
//        $url=implode("/",$urlEncodeArray);
        for($i =0;$i<3;$i++) {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $file = file_get_contents($url, false, stream_context_create($arrContextOptions));
            if($file){
                break;
            }else{
                continue;
            }
        }
        if(!$file){
            $uploadImageLog = new UploadImageLog();
            $uploadImageLog->setAttributes([
                'origin_url'=>$url,
                'info'=>"下载资源失败"
            ]);
            $uploadImageLog->save();
            throw new BusinessException([], '下载资源失败');
        }
//        $fileArr=explode('.',$httpUrl);
//        $fileExt=end($fileArr);
        $filename=basename($url);
        $pic_local_path = \Yii::$app->getBasePath() . '/public/assets/cache/'.md5(time().substr(md5(uniqid(mt_rand(), true)),1,6));
        $pic_local = $pic_local_path . '/' . $filename;

        if (!file_exists($pic_local_path)) {
//            print_r($pic_local_path);
//            exit;
            $info=mkdir($pic_local_path, 0777,true);
            @chmod($pic_local_path, 0777);
        }

        file_put_contents($pic_local, $file);
        return $pic_local;
    }

    /**
     * @param $url
     * @return string
     * @throws BusinessException
     * @throws \app\modules\task\services\exceptions\TaskRestException
     * 获取下载bayes图片
     */
    public static function downBayesFile($url)
    {
//        $accessToken = \app\modules\task\services\grab\bayes\BayesBase::getAccessToken();
        $accessToken = \app\modules\task\services\grab\bayes2\BayesBase::getAccessToken();
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $imageStream = Common::requestGet($url,$header,"");
        if(!$imageStream){
            $uploadImageLog = new UploadImageLog();
            $uploadImageLog->setAttributes([
                'origin_url'=>$url,
                'info'=>"下载资源失败"
            ]);
            $uploadImageLog->save();
            throw new BusinessException([], '转存图片失败');
        }
        $im = imagecreatefromstring($imageStream);
        $type = getimagesizefromstring($imageStream);
        if($im && $type['mime']){
            $suffix = explode("/",$type['mime']);
            $filename = basename($url);
            $pic_local_path = \Yii::$app->getBasePath() . '/public/assets/cache/'.md5(time());
            $pic_local = $pic_local_path . '/' . $filename.'.'.end($suffix);
            if (!file_exists($pic_local_path)) {
//            print_r($pic_local_path);
//            exit;
                $info=mkdir($pic_local_path, 0777,true);
                @chmod($pic_local_path, 0777);
            }

            file_put_contents($pic_local, $imageStream);
            return $pic_local;
        }else{
            $uploadImageLog = new UploadImageLog();
            $uploadImageLog->setAttributes(
                ['origin_url'=>$url],
                ['info'=>'bayes转存图片失败']
            );
            $uploadImageLog->save();
            throw new BusinessException([], '转存图片失败');
        }
    }
}
