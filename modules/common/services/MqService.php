<?php
/**
 *
 */

namespace app\modules\common\services;

use WBY\MQ\SDK\MQClientFactory;
use WBY\MQ\SDK\RocketMQ\Publisher;

class MqService
{
    const TOPIC_GAME_CENTER_BUSINESS = 'gamecenter';

    public static function publish($topic, $tag, $body)
    {
        $publisher = new Publisher(self::getProducer($topic), $tag);

        $messages = [json_encode($body)];

        $startDeliverTime = time() + 1;

        /** @var TopicMessage[] $messages */
        $messages = $publisher->produce($messages, $startDeliverTime);

        return $messages[0]->getMessageId();
    }


    /**
     * @param $topic
     * @return \MQ\MQProducer
     */
    public static function getProducer($topic)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);
        $instanceId = env('ONS_instanceId');

        return $client->getProducer($instanceId, $topic);
    }

    /**
     * @param $topic
     * @param $groupId
     * @param $tags
     * @return \MQ\MQConsumer
     */
    public static function getConsume($topic, $groupId, $tags)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);
        $instanceId = env('ONS_instanceId');

        return $client->getConsumer($instanceId, $topic, $groupId, implode("||", $tags));
    }

    public static function publishBusiness($type,$id)
    {
        self::publish(self::TOPIC_GAME_CENTER_BUSINESS, $type, ['event_id' => $id]);
    }


}
