<?php
/**
 *
 */

namespace app\modules\common\services;


use app\modules\common\models\CommonOperation;
use app\modules\common\models\Operation;
use app\modules\common\models\OperationLog;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class OperationLogService
{
    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_ROBOT = 2;

    const OPERATION_TYPE_ADD = 'add';
    const OPERATION_TYPE_UPDATE = 'update';
    const OPERATION_TYPE_DELETE = 'delete';

    // 这个值 用来划分批次
    private $versionKey = null;
    private static $instance = null;
    //构造方法私有化，防止外部创建实例
    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    public static function addLog($operationType,$resourceType, $resourceId, $info,$resourceExtId=0, $tag = "", $versionKeyExt = null, $basisId = "", $userType = self::USER_TYPE_ADMIN, $userId = 0,$ext2Type="")
    {
        // 添加主表增量
        $opt = new OperationLog();
        $opt->setAttributes([
            'type' => $operationType,
            'version_key' => self::getVersionKey(),
            'key_ext' => $versionKeyExt,
            'user_type' => $userType,
            'user_id' => $userId ? $userId : self::getLoginUserId(),
            'resource_type' => $resourceType,
            'resource_id' => $resourceId,
            'resource_ext_id' => $resourceExtId,
            'tag' => $tag,
            'basis_id' => $basisId,
            'info' => json_encode($info),
        ]);
        if ($opt->save()) {
            TaskService::addTaskMainIncrement($resourceType,$operationType,$info,$tag,$ext2Type);
            return true;
        } else {
            throw new BusinessException($opt->getErrors(), "添加log失败");
        }
    }

    public static function operationLogById($resourceType, $resourceId)
    {
        return OperationLog::find()->andWhere(['resource_id' => $resourceId, 'resource_type' => $resourceType])->orderBy("id desc")->all();
    }

    public static function operationCounts($resourceType, $ids)
    {
        $countMap = [];
        $list = OperationLog::find()->select(["count(distinct(version_key)) as count", 'resource_id'])->andWhere(['in', 'resource_id', $ids])->andWhere(['resource_type' => $resourceType])->groupBy("resource_id")->asArray()->all();
        foreach ($list as $k => $v) {
            $countMap[$v["resource_id"]] = [
                'count'=>$v["count"],
                'resource_id'=>$v["resource_id"],
                'resource_type'=>$resourceType
            ];
        }
        return $countMap;
    }

    public static function getOperationLog($params)
    {
        $whereConfig=[
            "resource_type"=>[
                'type'=>'=',
                'key'=>'log.resource_type'
            ],
            "resource_id"=>[
                'type'=>'=',
                'key'=>'log.resource_id'
            ],
        ];
        $whereArray=DbHelper::getWhere($whereConfig,$params);
        $where=array_merge(['and'],$whereArray);
        $page=(isset($params['page'])&&$params['page'])?$params['page']:1;
        $perPage=(isset($params['per_page'])&&$params['per_page'])?$params['per_page']:10;

        $q=(new Query())
            ->select(['log.*',"admin.id as admin_id","if(log.user_type=1,admin.name,'robot') as admin_name"])
            ->from('operation_log as log')
            ->leftJoin('admin', 'log.user_id=admin.id and log.user_type=1')
            ->where($where);
        $count=$q->count();
//        $sql=\Yii::$app->getDb()->createCommand($q)->getRawSql();
        $list=$q->orderBy("id desc")->limit($perPage)->offset($perPage*($page-1))
            ->all();

        foreach($list as $key=>$val){
            $tmp=json_decode($val['info'],true);
            if($val["type"] == OperationLogService::OPERATION_TYPE_UPDATE || $val['type'] == QueueServer::QUEUE_TYPE_CHANGE || $val["type"] == 'refresh_match'){
                $detail=$tmp["diff"];
            }else{
                $detail=$tmp["new"];
            }
            $list[$key]["detail"]=$detail;
        }
        return [
            'total'=>$count,
            'list'=>$list
        ];
    }

    public static function getLoginUserId()
    {
        try {
            $id = \Yii::$app->getUser()->getId();
            return $id;
        } catch (\Exception $e){
            return 0;
        }

    }

    public static function getVersionKey()
    {
        return self::getInstance()->getVersion();
    }

    public static function resetVersionKey()
    {
        return self::getInstance()->setVersion();
    }

    public function setVersion()
    {
        // 用来区分操作，可能一次操作修改了多个记录，这里标识出来
        $this->versionKey = md5(uniqid(md5(microtime(true)), true));
    }

    public function getVersion()
    {
        if (!$this->versionKey) {
            $this->setVersion();
        }
        return $this->versionKey;
    }
}