<?php


namespace app\modules\common\services;


class RedisService
{
    private static $_instance;
    const REDIS_QUEUE_PRE='GC_QUEUE:';
    public static function getInstance($db = 0)
    {
        try{
            if (isset(self::$_instance[$db]) && self::$_instance[$db]->Ping() == 'Pong') {
                return self::$_instance[$db];
            }
        } catch (\Exception $e) {
        }
        self::$_instance[$db] = self::_connect($db);
        return self::$_instance[$db];
    }

    private static function _connect($db)
    {
        $redis = new \Redis();
        $host=env('REDIS_HOST');
        $port=env('REDIS_PORT')??6379;
        $password=env('REDIS_PASSWORD')??null;
        $redis->connect($host, $port);
        if($password){
            $redis->auth($password);
        }
        $redis->select($db);
        return $redis;
    }

    public static function addToQueue($key,$val)
    {

    }
}