<?php


namespace app\modules\common\services;


class RestHelper
{
    public static function getRealIp($default=null)
    {
        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            return $_SERVER['HTTP_X_REAL_IP'];
        } elseif ($_SERVER['REMOTE_ADDR']) {
            return $_SERVER['REMOTE_ADDR'];
        }else{
            return $default;
        }
    }
}