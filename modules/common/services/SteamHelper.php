<?php

namespace app\modules\common\services;

class SteamHelper
{
    /**
     * player的steam_id转化
     * $SteamID         steam_id
     */
    public static function playerConversionSteamId($SteamID)
    {
        // $SteamID = 'STEAM_1:0:82392660';//76561198125051048
        // $SteamID = 'STEAM_1:1:111366757';//76561198182999243
        // $SteamID = 'STEAM_1:0:73487156';//76561198107240040
        // $SteamID = 'STEAM_1:1:432312920';//76561198824891569
        if($SteamID == "BOT"){
            $steamId = 111;
            $steamId = (string)$steamId;
            return $steamId;
        }
        $log = explode(':',$SteamID);
        if(@$log[1] === "0"){
            $steamId = $log[2] * 2;
            $steamId = $steamId + 1197960265728;
            $steamId = "7656".$steamId;
            $steamId = (string)$steamId;
            return $steamId;
        }
        if(@$log[1] === "1"){
            $steamId = $log[2].".5";
            $steamId = $steamId * 2;
            $steamId = $steamId + 1197960265728;
            $steamId = "7656".$steamId;
            $steamId = (string)$steamId;
            return $steamId;
        }
        return "";
    }
}