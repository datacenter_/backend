<?php


namespace app\modules\common\services;


class StringHelper
{
    public static function checkString($string)
    {
        $patterns = "/[\d\.,]+/";
        preg_match_all($patterns,$string,$arr);
        return $arr[0];
    }
    public static function checkStringToNumber($string)
    {
        $patterns = "/[\d\.,]+/";
        preg_match_all($patterns,$string,$arr);
        $result = implode('',$arr[0]);
        return str_replace(",","",$result);
    }
}