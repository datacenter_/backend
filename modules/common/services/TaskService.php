<?php

namespace app\modules\common\services;

use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;


/**
 * Class AuthService
 * @package app\modules\common\services
 */
class TaskService
{
    public static function addTask($resourceType,$changeType,$resourceDiff,$extType)
    {
        //
    }

    /**
     * @param $resourceType
     * @param $changeType
     * @param $resourceDiff
     * @param string $extType
     * @param int $runType
     * @throws \yii\db\Exception
     * 添加到队列，$runType==1是队列消费，3是同步执行
     */
    public static function addTaskMainIncrement($resourceType,$changeType,$resourceDiff,$extType="",$ext2Type="",$runType=1)
    {
        try{
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_MAIN_INCREMENT,
                '',
                $resourceType,
                $changeType,
                $extType,
                $ext2Type);
            $queueInfos= [
                "tag" => $tag,
                "params" => $resourceDiff,
            ];
            // 添加到队列
            TaskRunner::addTask($queueInfos,$runType,time()+1);
        }catch (\Exception $e){
            // 插入错误列表
            CommonLogService::recordException($e);
        }
    }

    public static function addTaskRefreshUpdateConfig($resourceType,$operationInfo,$extType="",$runType=1)
    {
        // 添加到队列，执行批量刷新账号的更新配置
        try{
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_OPERATE_CONFIG_REFRESH,
                '',
                $resourceType,
                QueueServer::QUEUE_TYPE_ADD,
                $extType);
            $queueInfos= [
                "tag" => $tag,
                "params" => $operationInfo,
            ];
            // 添加到队列
            return TaskRunner::addTask($queueInfos,$runType);
        }catch (\Exception $e){
            // 插入错误列表
            CommonLogService::recordException($e);
        }
    }

}