<?php
/**
 *
 */

namespace app\modules\common\services;


class TimeHelper
{
    public static function msectime()
    {
        $timeInfo = explode(' ', microtime());
        $msec = $timeInfo[0];
        $sec = $timeInfo[1];
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }
}