<?php

namespace app\modules\common\services;

class WinnerHelper
{
    /**
     * csgo获胜判断
     * $scorefirst          第一队分数
     * $scoresecond         第二队分数
     * $battleCount         battle总数
     * $matchType           比赛类型
     * $matchRules          比赛规则
     */
    public static function csgoWinnerInfo($scorefirst,$scoresecond,$battleCount,$matchType=0,$matchRules=0)
    {
        $winner = $battleCount / 2;//获胜的分数点
        $isFinish = 2;//是否结束
        $winnerTeam = "";//获胜队伍
        $isDraw = 2;//是否平局
        if($scorefirst > $winner && $scoresecond < $winner){
            $winnerTeam = 1;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scoresecond > $winner && $scorefirst < $winner){
            $winnerTeam = 2;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scorefirst == $winner && $scoresecond == $winner){
            $winnerTeam = "";
            $isFinish = 1;
            $isDraw = 1;
        }
        $info = [
            'is_finish' => $isFinish,
            'winner_team' => $winnerTeam,
            'is_draw' => $isDraw,
        ];
        return $info;
    }
    /**
     * lol获胜判断
     * $scorefirst          第一队分数
     * $scoresecond         第二队分数
     * $battleCount         battle总数
     * $matchType           比赛类型
     * $matchRules          比赛规则
     */
    public static function lolWinnerInfo($scorefirst,$scoresecond,$matchRulesNum=0)
    {
        $winner = $matchRulesNum / 2;//获胜的分数点
        $isFinish = 2;//是否结束
        $winnerTeam = "";//获胜队伍
        $isDraw = 2;//是否平局
        if($scorefirst > $winner && $scoresecond < $winner){
            $winnerTeam = 1;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scoresecond > $winner && $scorefirst < $winner){
            $winnerTeam = 2;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scorefirst == $winner && $scoresecond == $winner){
            $winnerTeam = "";
            $isFinish = 1;
            $isDraw = 1;
        }
        $info = [
            'is_finish' => $isFinish,
            'winner_team' => $winnerTeam,
            'is_draw' => $isDraw,
        ];
        return $info;
    }
    /**
     * DOTA获胜判断
     * $scorefirst          第一队分数
     * $scoresecond         第二队分数
     * $battleCount         battle总数
     * $matchType           比赛类型
     * $matchRules          比赛规则
     */
    public static function DotaWinnerInfo($scorefirst,$scoresecond,$matchRulesNum=0)
    {
        $winner = $matchRulesNum / 2;//获胜的分数点
        $isFinish = 2;//是否结束
        $winnerTeam = "";//获胜队伍
        $isDraw = 2;//是否平局
        if($scorefirst > $winner && $scoresecond < $winner){
            $winnerTeam = 1;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scoresecond > $winner && $scorefirst < $winner){
            $winnerTeam = 2;
            $isFinish = 1;
            $isDraw = 2;
        }
        if($scorefirst == $winner && $scoresecond == $winner){
            $winnerTeam = "";
            $isFinish = 1;
            $isDraw = 1;
        }
        $info = [
            'is_finish' => $isFinish,
            'winner_team' => $winnerTeam,
            'is_draw' => $isDraw,
        ];
        return $info;
    }
}