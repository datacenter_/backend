<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\data\services\DataBindingService;

class BindingController extends WithTokenAuthController
{
    public function actionTeamList()
    {
        $params=\Yii::$app->request->get();
        return BindingTeamService::getList($params);
    }

    /**
     * 首页待处理绑定
     * @return mixed
     */
    public function actionToDoBinding()
    {
        return DataBindingService::getToDoBindingList();
    }

    /**
     * 首页数据源绑定概况
     * @return array
     */
    public function actionBindingStatistics()
    {
        return DataBindingService::BindingStatisticsByOrigin();
    }

    //绑定设置开始时间
    public function actionBindingStartTime(){
        $bd_type = $this->pPost('bd_type');
        $start_time = $this->pPost('start_time');
        $origin_ids = $this->pPost('origin_ids');
        return DataBindingService::setBindingStartTime($bd_type,$start_time,$origin_ids);
    }
}