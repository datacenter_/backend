<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMatchService;

class BindingMatchController extends WithTokenAuthController
{
    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return BindingMatchService::getList($params);
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        return BindingMatchService::getDetail($id);
    }

    public function actionBinding()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        return BindingMatchService::setBinding($standardId,$masterId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionAdd()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        return BindingMatchService::addAndBinding($standardId,Consts::USER_TYPE_ADMIN,$userId);
    }

    /**
     * @return bool
     * 刷新单条数据
     */
    public function actionRefreshStand()
    {
        $params=\Yii::$app->request->post();
        $relIdentityId = $params['rel_identity_id'];
        $resourceType = $params['resource_type'];
        $originId = $params['origin_id'];
        $gameId = $params['game_id'];
        return BindingMatchService::getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId,"refresh");
    }
    /**
     *重置standrad表，刷新数据
     */
    public function actionResetStand()
    {
        $params=\Yii::$app->request->post();
        $relIdentityId = $params['rel_identity_id'];
        $resourceType = $params['resource_type'];
        $originId = $params['origin_id'];
        $gameId = $params['game_id'];
        BindingMatchService::resetStandard($relIdentityId,$resourceType,$originId,$gameId);
    }
    /**
     * 通过identifiers的value值重抓bayes数据
     */
    public function actionRefreshByeasByIdentifier()
    {
        $params=\Yii::$app->request->post();
        $identifierValue = $params['value'];
        $action = "/identifiers/lookup";
        $param = [
          "value"=>$identifierValue
        ];
        $lookupJsonData = \app\modules\task\services\grab\bayes2\BayesBase::getCurlByParams($action,$param);
        if($lookupJsonData){
            return $lookupJsonData;
        }
    }

}