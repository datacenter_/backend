<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMetadataService;

class BindingMetadataController extends WithTokenAuthController
{
    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return BindingMetadataService::getList($params);
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        $metadataType=$this->pGet('metadata_type');
        return BindingMetadataService::getDetail($id,$metadataType);
    }

    public function actionBinding()
    {
//        $userId=$this->getIdentity()->getId();
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        $metadataType=$this->pPost('metadata_type');
        return BindingMetadataService::setBinding($metadataType,$standardId,$masterId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionAdd()
    {
//        $userId=$this->getIdentity()->getId();
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        $metadataType=$this->pPost('metadata_type');
        return BindingMetadataService::addAndBinding($metadataType, $standardId,Consts::USER_TYPE_ADMIN,$userId);
    }

}