<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\data\services\binding\BindingTeamService;

class BindingPlayerController extends WithTokenAuthController
{
    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return BindingPlayerService::getList($params);
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        return BindingPlayerService::getDetail($id);
    }

    public function actionBinding()
    {
        $userId=$this->getIdentity()->getId();
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        return BindingPlayerService::setBinding($standardId,$masterId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionAdd()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        return BindingPlayerService::addAndBinding($standardId,Consts::USER_TYPE_ADMIN,$userId);
    }
}