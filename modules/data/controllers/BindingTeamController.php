<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingTeamService;

class BindingTeamController extends WithTokenAuthController
{
    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return BindingTeamService::getList($params);
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        return BindingTeamService::getDetail($id);
    }

    public function actionBinding()
    {
        $userId=$this->getIdentity()->getId();
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        $originId=$this->pPost('origin_id');
        return BindingTeamService::setBinding($standardId,$masterId,Consts::USER_TYPE_ADMIN,$userId,0,true,$originId);
    }

    public function actionAdd()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        return BindingTeamService::addAndBinding($standardId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionGrabTeam(){
        return BindingTeamService::grabTeam();

    }

}