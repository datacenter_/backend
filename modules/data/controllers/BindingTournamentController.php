<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\data\services\binding\BindingTournamentService;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

class BindingTournamentController extends WithTokenAuthController
{

    public function actionTreeList()
    {
        $params=\Yii::$app->request->get();
        return BindingTournamentService::getTreeList($params);
    }

    public function actionList()
    {
        $params=\Yii::$app->request->get();
        return BindingTournamentService::getList($params);
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        return BindingTournamentService::getDetail($id);
    }

    public function actionBinding()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        return BindingTournamentService::setBinding($standardId,$masterId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionAdd()
    {
        $userId=$this->user->id;
        $standardId=$this->pPost('standard_id');
        return BindingTournamentService::addAndBinding($standardId,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionBindSon()
    {
        $standardId=$this->pPost('standard_id');
        $masterId=$this->pPost('master_id');
        $tournamentId=$this->pPost('standard_tournament_id');
        $elsTournamentId=$this->pPost('els_tournament_id');
        $type=$this->pPost('type');
        return BindingTournamentService::setGroupOrStage($standardId,$masterId,$tournamentId,$type,$elsTournamentId);
    }

    public function actionCloseBind()
    {
        $masterId=$this->pPost('master_id');
        return BindingTournamentService::CloseGroupOrStage($masterId);
    }
}