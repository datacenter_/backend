<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\UpdateConfigService;
use app\rest\exceptions\BusinessException;

class ConfigController extends WithTokenAuthController
{
    public function actionList()
    {
        $params=$this->pGet();
        return UpdateConfigService::getDefaultConfigList($params);
    }

    public function actionDetail()
    {
        $gameId=$this->pGet('game_id');
        $resourceType=$this->pGet('resource_type');
        return UpdateConfigService::getDefaultConfigDetail($gameId,$resourceType);
    }

    public function actionEdit()
    {
        $gameId=$this->pPost('game_id');
        $config=$this->pPost('config');
//        $userId=$this->getIdentity()->getId();
        $userId=$this->user->id;
        return UpdateConfigService::setDefaultInfo($gameId,$config,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionBatch()
    {
        $gameId=$this->pPost('game_id');
        $resourceType=$this->pPost('resource_type');
        $config=$this->pPost('config');
        $userId=$this->getIdentity()->getId();
        return UpdateConfigService::addBatchUpdateTask($gameId,$resourceType,$config,Consts::USER_TYPE_ADMIN,$userId);
    }

    public function actionResourceConfigDetail()
    {
        //
        $resourceId=$this->pGet('resource_id');
        $resourceType=$this->pGet('resource_type');
        return UpdateConfigService::getResourceUpdateConfigDetail($resourceId, $resourceType);
    }

    public function actionResourceConfigEdit()
    {
        $resourceId=$this->pPost('resource_id');
        $resourceType=$this->pPost('resource_type');
        $config=$this->pPost('config');
        if(isset($config["config"])){
            $config=$config["config"];
        }
        return UpdateConfigService::updateResourceUpdateConfig($resourceId, $config);
    }
    //赛事页面中 单条赛事的比赛设置调用  只是刷新不做设置
    public function actionSetMatchResourceConfig(){
        $tournamentId=$this->pPost('tournament_id');//比赛ID
        $scheduleOriginId=$this->pPost('schedule_origin_id');//赛程数据源
        $scheduleUpdateType=$this->pPost('schedule_update_type');//赛程更新方式
        $scoreOriginId=$this->pPost('score_origin_id');//实时比分数据源
        $scoreUpdateType=$this->pPost('score_update_type');//实时比分更新方式
        if (empty($tournamentId) || empty($scheduleOriginId) || empty($scheduleUpdateType) || empty($scoreOriginId) || empty($scoreUpdateType)){
            throw new BusinessException([],'有参数为空，不能提交');
        }
        //获取赛事下所有比赛
        return UpdateConfigService::setMatchResourceConfig($tournamentId,$scheduleOriginId,$scheduleUpdateType,$scoreOriginId,$scoreUpdateType);
    }
}