<?php
namespace app\modules\data\controllers;
use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\DataBindingService;
use app\modules\metadata\services\MetadataService;
use app\modules\org\services\TeamService;
use app\modules\task\models\EnumOrigin;
use app\modules\task\services\Common;
use app\modules\task\services\StandardDataService;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;


class DataController extends WithTokenAuthController
//class DataController extends RestController
{
    public function actionSetConfig()
    {
        $attributes = \Yii::$app->getRequest()->post();

        $match = new DataConfig();
        $match->setAttributes($attributes);
        if ($match->validate()) {
            return DataConfig::add($attributes, $this->user->id);
        } else {
            throw new BusinessException($match->getErrors(), '参数信息错误');
        }
    }

    public function actionGetConfigList()
    {
        $id = \Yii::$app->getRequest()->get('id');

        $dataConfig = DataConfig::find();
        if ($id && !empty($id)) {
            $dataConfig->andWhere(['id' => $id]);
        }
        $dataConfig=$dataConfig->orderBy('id desc');
        $totalCount=$dataConfig->count();
        $page = \Yii::$app->request->get('page',1) - 1;
        $pageSize = \Yii::$app->request->get('per_page',20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $dataConfig->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        return ['list' => $model, 'total' => $totalCount];
    }

    // 获取绑定信息
    public function actionGetBingInfo()
    {
        $id = \Yii::$app->getRequest()->get('master_id');
        $type = \Yii::$app->getRequest()->get('type');

        $tableName = '';
        switch ($type)
        {
            case 'player' : $tableName = 'standard_data_player';break;
            case 'group' : $tableName = 'standard_data_group';break;
            case 'match' : $tableName = 'standard_data_match';break;
            case 'team' : $tableName = 'standard_data_team';break;
            case 'tournament' : $tableName = 'standard_data_tournament';break;
            case 'stage' : $tableName = 'standard_data_stage';break;
            default:$tableName = 'standard_data_metadata';break;
        }
        $bingInfo = DataStandardMasterRelation::find()->alias('dmr')
                    ->select('b.id,b.origin_id,enum_origin.name,enum_origin.desc,dmr.master_id,b.rel_identity_id as standard_id')
                    ->where(['dmr.resource_type' => $type,'dmr.master_id' => $id])
                    ->leftJoin($tableName.' as b','b.id = dmr.standard_id')
                    ->leftJoin('enum_origin','b.origin_id = enum_origin.id');
                    $info = $bingInfo->asArray()->all();
        return $info;
    }

    // 设置绑定信息
    public function actionSetBingInfo()
    {
        $masterID = \Yii::$app->getRequest()->post('master_id');
        $standardId = \Yii::$app->getRequest()->post('standard_id');
        $game_id = \Yii::$app->getRequest()->post('game_id');
        $type = \Yii::$app->getRequest()->post('type');

        $className = MetadataService::getObjByType($type);
        $obj = $className::find()->where(['id'=>$standardId])->asArray()->one();
        if (empty($obj)) {
            throw new BusinessException([],'没有数据源Id:'.$standardId);
        }
        $sGameId = $obj->toArray()['game'] ?? $obj->toArray()['game_id'];
        if ($game_id != $sGameId)
        {
            throw new BusinessException([],'不是相同游戏');
        }
        $logOldTeam = $className::find()->where(['id'=>$standardId])->asArray()->one();
        $info = DataStandardMasterRelation::find()
            ->alias('dmr')
            ->leftJoin('standard_data_tournament','standard_data_tournament.id = dmr.standard_id')
            ->where(
                [
                    'dmr.resource_type' => $type,
                    'dmr.master_id' => $masterID,
                    'standard_data_tournament.origin_id' =>$logOldTeam->toArray()['origin_id']
                ])->one();
        if (!empty($info) && $info)
        {
            throw new BusinessException([],'已经绑定过了 id:'.$masterID);
        }


        // 更新绑定关系
        $operationBindingInfo=[
            'resource_type'=>$type,
            'standard_id'=>$standardId,
            'master_id'=>$masterID
        ];
        $op = DataStandardMasterRelation::find()->where(
            ['resource_type' => $type, 'standard_id' => $standardId])->one();
        if (empty($op) || !$op)
        {
            $op = new DataStandardMasterRelation();
        }
        $op->setAttributes($operationBindingInfo);
        $op->save();
        // 更新操作状态
        $stand=$className::find()->where(['id'=>$standardId])->one();
        $stand->setAttribute('deal_status',Consts::BINGING_STATUS_WISH_DONE);
        $stand->save();
        // 删除预期绑定
        $delSql='delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
        $cmd=\Yii::$app->getDb()
            ->createCommand($delSql)
            ->bindParam(':standard_id',$standardId)
            ->bindParam(':resource_type',$operationBindingInfo['resource_type'])
            ->execute();

            $diffInfo = Common::getDiffInfo($logOldTeam, $stand->toArray(), []);
            if ($diffInfo['changed']) {
                OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                    'bing_'.$type,
                    $standardId,
                    ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                    0,
                    Consts::TAG_TYPE_CORE_DATA,
                    null,
                    0,
                    Consts::USER_TYPE_ADMIN,
                    $this->getIdentity()->getId()
                );
            }
        return ['success' => '成功','msg' => true];
    }

    // 解绑
    public function actionCloseBing()
    {
        $standardId = \Yii::$app->getRequest()->post('standard_id');
        $type = \Yii::$app->getRequest()->post('type');
        return DataBindingService::cancelBinding($standardId, $type);
    }
}