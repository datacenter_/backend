<?php
namespace app\modules\data\controllers;


use app\controllers\RestController;
use app\modules\task\services\Common;

class ImageController extends RestController
{
    //获取bayes图片
    public function actionGetBayesImage()
    {
        $params=$this->pGet();
        $url = $params['image_url'];
        $origin_id = $params['origin_id'];
        if ($origin_id == 9){
            $accessToken = \app\modules\task\services\grab\bayes\BayesBase::getAccessToken();
        }else{
            $accessToken = \app\modules\task\services\grab\bayes2\BayesBase::getAccessToken();
        }
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $imageStream = Common::requestGet($url,$header,"");
        $im = imagecreatefromstring($imageStream);
        $type = getimagesizefromstring($imageStream);
        $mime = $type['mime'];
        if($im != false){
            if(isset($type['mime']) && $type['mime']){
                header('Content-Type:'.$mime);
                echo $imageStream;
            }else{
                echo '错误的图片流';
            }
        }
    }
}