<?php
/**
 *
 */

namespace app\modules\data\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\DebugDataTodoService;
use app\modules\match\services\PbpService;
use app\modules\task\services\Common;

class ToDoController extends WithTokenAuthController
{
    public function actionList()
    {
        $params = $this->pGet();
        return DataTodoService::getList($params);
    }

    public function actionDetail()
    {
        $id = $this->pGet('id');
        return DataTodoService::getDetail($id);
    }

    /**
     * 数据更新列表修改处理状态     By      王傲渊
     * $id          ID(data_todo->ID)
     * $sign        处理状态:1.已处理,2.未处理
     */
    public function actionDeal()
    {
        $id = $this->pPost('id');
        $sign = $this->pPost('sign');
        if($sign == 1){
            return DataTodoService::setDealStatus($id, Consts::TODO_DEAL_STATUS_DONE, Consts::USER_TYPE_ADMIN, $this->user->id);
        }
        if($sign == 2){
            return DataTodoService::setDealStatus($id, Consts::TODO_DEAL_STATUS_PADDING, Consts::USER_TYPE_ADMIN, $this->user->id);
        }
        if($sign != 1 && $sign != 2){
            return [];
        }
    }

    public function actionDebugAdd()
    {
        // 这个方法用来测试更新，配置更新源，
        $params=$this->pPost();
        return DebugDataTodoService::debugAddToDo($params);
    }

    public function actionDebugInfo()
    {
        // 获取debug信息
        return DebugDataTodoService::debugInfoList();
    }
    //获取bayes图片
    public function actionGetBayesImage()
    {
        $params=$this->pGet();
        $url = $params['image_url'];
        $accessToken = \app\modules\task\services\grab\bayes\BayesBase::getAccessToken();
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $imageStream = Common::requestGet($url,$header,"");
        $im = imagecreatefromstring($imageStream);
        $type = getimagesizefromstring($imageStream);
        $mime = $type['mime'];
        if($im != false){
            if(isset($type['mime']) && $type['mime']){
                header('Content-Type:'.$mime);
                echo $imageStream;
            }else{
                echo '错误的图片流';
            }
        }
    }
}