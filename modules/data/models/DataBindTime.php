<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_bind_time".
 *
 * @property int $id
 * @property string|null $bd_type 绑定的类型
 * @property string|null $bd_start_time 绑定开始时间
 * @property string|null $bd_origin_ids
 * @property string|null $created_at 创建时间
 * @property string|null $modified_at 修改时间
 */
class DataBindTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_bind_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bd_start_time', 'created_at', 'modified_at'], 'safe'],
            [['bd_type', 'bd_origin_ids'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bd_type' => 'Bd Type',
            'bd_start_time' => 'Bd Start Time',
            'bd_origin_ids' => 'Bd Origin Ids',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
