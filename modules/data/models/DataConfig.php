<?php

namespace app\modules\data\models;

use app\rest\exceptions\BusinessException;
use Yii;

class DataConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['core_data_config','core_data_mode','base_data_config','base_data_model',
                'team_player_config','team_player_model','type','competition_team_config',
                'competition_team_model','create_match_config','create_match_model','match_bind',
                'match_update_config','match_update_model','real_score_config','real_score_model',
                'cuser','tournament_type','game_type'
                ], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            if ($attribute['type'] ==3 ){

                self::deleteAll(['type' => 3]);

                $tournament = json_decode($attribute['tournament'], true);
                $son_tournament = json_decode($attribute['son_tournament'], true);
                $stage_tournament = json_decode($attribute['stage_tournament'], true);
                $group_tournament = json_decode($attribute['group_tournament'], true);
                $tournamentModel = new self();
                $tournamentModel->setAttributes($tournament);
                $tournamentModel->setAttribute('tournament_type',1);
                $tournamentModel->setAttribute('type',$attribute['type']);
                $tournamentModel->save();

                $sonTournament = new self();
                $sonTournament->setAttributes($son_tournament);
                $sonTournament->setAttribute('tournament_type',2);
                $sonTournament->setAttribute('type',$attribute['type']);;
                $sonTournament->save();

                $stageTournament = new self();
                $stageTournament->setAttributes($stage_tournament);
                $stageTournament->setAttribute('tournament_type',3);
                $stageTournament->setAttribute('type',$attribute['type']);;
                $stageTournament->save();

                $groupTournament = new self();
                $groupTournament->setAttributes($group_tournament);
                $groupTournament->setAttribute('tournament_type',4);
                $groupTournament->setAttribute('type',$attribute['type']);;
                $groupTournament->save();

                return ['msg' =>'设置成功','tournament_config_id' => $tournamentModel->id,
                    'son_tournament_id' => $sonTournament->id,'stage_tournament' => $stageTournament->id,
                    'group_tournament' => $groupTournament->id
                ];

            }else{
                $dataConfig = self::find()->where(['id' => $attribute['id']])->one();
                $dataConfig->setAttributes($attribute);
                if ($dataConfig->save() > 0) {
                    return ['msg' =>'设置成功','data_config_id' => $dataConfig->id];
                }else{
                    throw new BusinessException($dataConfig->getErrors(),'设置失败');
                }
            }
        } else {

            if ($attribute['type'] == 3) {
                $tournament = json_decode($attribute['tournament'], true);
                $son_tournament = json_decode($attribute['son_tournament'], true);
                $stage_tournament = json_decode($attribute['stage_tournament'], true);
                $group_tournament = json_decode($attribute['group_tournament'], true);
                $tournamentModel = new self();
                $tournamentModel->setAttributes($tournament);
                $tournamentModel->setAttribute('tournament_type',1);
                $tournamentModel->setAttribute('type',$attribute['type']);
                $tournamentModel->save();

                $sonTournament = new self();
                $sonTournament->setAttributes($son_tournament);
                $sonTournament->setAttribute('tournament_type',2);
                $sonTournament->setAttribute('type',$attribute['type']);;
                $sonTournament->save();

                $stageTournament = new self();
                $stageTournament->setAttributes($stage_tournament);
                $stageTournament->setAttribute('tournament_type',3);
                $stageTournament->setAttribute('type',$attribute['type']);;
                $stageTournament->save();

                $groupTournament = new self();
                $groupTournament->setAttributes($group_tournament);
                $groupTournament->setAttribute('tournament_type',4);
                $groupTournament->setAttribute('type',$attribute['type']);;
                $groupTournament->save();

                return ['msg' =>'设置成功','tournament_config_id' => $tournamentModel->id,
                    'son_tournament_id' => $sonTournament->id,'stage_tournament' => $stageTournament->id,
                    'group_tournament' => $groupTournament->id
                    ];
            }else{
             $dataConfig = new self();
             $dataConfig->setAttributes($attribute);
             $dataConfig->setAttribute('cuser', $userId);
             if ($dataConfig->save() > 0) {
                 return ['msg' =>'设置成功','data_config_id' => $dataConfig->id];
             }else{
                 throw new BusinessException($dataConfig->getErrors(),'设置失败');
             }
            }
        }
    }
}
