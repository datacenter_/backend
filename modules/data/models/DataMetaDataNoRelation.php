<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_metadata_no_relation".
 *
 * @property int $id
 * @property string|null $resource_type
 * @property string|null $rel_id
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class DataMetaDataNoRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_metadata_no_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id','status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['resource_type'], 'string', 'max' => 255],
            [['rel_id'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'resource_type' => 'Resource Type',
            'rel_id' => 'Rel ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
