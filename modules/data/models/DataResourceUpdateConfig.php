<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_resource_update_config".
 *
 * @property int $id
 * @property int|null $game_id 游戏id
 * @property string|null $resource_type 资源类型
 * @property int|null $resource_id 资源id
 * @property string|null $tag_type 资源子类
 * @property string|null $slug slug
 * @property int|null $origin_id 默认源
 * @property int|null $update_type 默认更新类型(operation_update_config_default)
 * @property string|null $created_time 创建时间
 * @property string|null $modify_at 修改时间
 */
class DataResourceUpdateConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_resource_update_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'resource_id', 'origin_id', 'update_type'], 'integer'],
            [['created_time', 'modify_at'], 'safe'],
            [['resource_type', 'tag_type', 'slug'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => '游戏id',
            'resource_type' => '资源类型',
            'resource_id' => '资源id',
            'tag_type' => '资源子类',
            'slug' => 'slug',
            'origin_id' => '默认源',
            'update_type' => '默认更新类型(operation_update_config_default)',
            'created_time' => '创建时间',
            'modify_at' => '修改时间',
        ];
    }
}
