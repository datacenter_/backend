<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_resource_update_config_operation".
 *
 * @property int $id
 * @property int|null $game_id 游戏id
 * @property string|null $resource_type 资源类型大类型
 * @property string|null $config_info 配置
 * @property int|null $operator 操作人
 * @property string|null $created_time 创建时间
 * @property string|null $modified_at 修改时间
 * @property int|null $deal_status 处理状态1待处理2处理中3处理成功4失败
 * @property int|null $task_info_id 关联任务id
 */
class DataResourceUpdateConfigOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_resource_update_config_operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'operator', 'deal_status', 'task_info_id'], 'integer'],
            [['config_info'], 'string'],
            [['created_time', 'modified_at'], 'safe'],
            [['resource_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => '游戏id',
            'resource_type' => '资源类型大类型',
            'config_info' => '配置',
            'operator' => '操作人',
            'created_time' => '创建时间',
            'modified_at' => '修改时间',
            'deal_status' => '处理状态1待处理2处理中3处理成功4失败',
            'task_info_id' => '关联任务id',
        ];
    }
}
