<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_standard_master_relation".
 *
 * @property int $id
 * @property string|null $resource_type 资源类型
 * @property int|null $standard_id 标准库id
 * @property int|null $master_id 主库id
 * @property string|null $created_time 创建时间
 * @property string|null $modify_at 修改时间
 */
class DataStandardMasterRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_standard_master_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['standard_id', 'master_id'], 'integer'],
            [['created_time', 'modify_at'], 'safe'],
            [['resource_type'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resource_type' => '资源类型',
            'standard_id' => '标准库id',
            'master_id' => '主库id',
            'created_time' => '创建时间',
            'modify_at' => '修改时间',
        ];
    }
}
