<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_standard_master_relation".

 */
class DataStandardTournamentGroupStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_tournament_group_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['standard_id', 'master_id','standard_tournament_id','els_tournament_id'], 'integer'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }
}
