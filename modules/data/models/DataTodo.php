<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_todo".
 *
 * @property int $id
 * @property string|null $resource_type
 * @property int|null $resource_id
 * @property string|null $tag_type
 * @property int|null $standard_id 标准id（冗余）
 * @property int|null $task_id 来源id
 * @property string|null $change_info 变动记录
 * @property int|null $status 处理状态1待处理2处理中3已完成
 * @property string|null $deal_time 处理时间
 * @property int|null $update_type 2手动3自动
 * @property int|null $game_id （冗余）
 * @property string|null $resource_name （冗余）
 * @property string|null $resource_logo （冗余）
 * @property string|null $created_time 创建时间
 * @property string|null $modify_at 修改时间
 */
class DataTodo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_todo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resource_id', 'standard_id', 'task_id', 'status', 'update_type', 'game_id'], 'integer'],
            [['change_info'], 'string'],
            [['deal_time', 'created_time', 'modify_at'], 'safe'],
            [['resource_type', 'tag_type', 'resource_name', 'resource_logo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resource_type' => 'Resource Type',
            'resource_id' => 'Resource ID',
            'tag_type' => 'Tag Type',
            'standard_id' => '标准id（冗余）',
            'task_id' => '来源id',
            'change_info' => '变动记录',
            'status' => '处理状态1待处理2处理中3已完成',
            'deal_time' => '处理时间',
            'update_type' => '2手动3自动',
            'game_id' => '（冗余）',
            'resource_name' => '（冗余）',
            'resource_logo' => '（冗余）',
            'created_time' => '创建时间',
            'modify_at' => '修改时间',
        ];
    }
}
