<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "data_update_config_default".
 *
 * @property int $id
 * @property int|null $game_id 游戏id
 * @property string|null $resource_type 资源类型
 * @property string|null $tag_type 资源子类
 * @property string|null $slug slug(这里冗余，方便使用)
 * @property int|null $origin_id 默认源
 * @property int|null $update_type 默认更新类型(operation_update_config_default)
 */
class DataUpdateConfigDefault extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_update_config_default';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'origin_id', 'update_type'], 'integer'],
            [['resource_type', 'tag_type', 'slug'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => '游戏id',
            'resource_type' => '资源类型',
            'tag_type' => '资源子类',
            'slug' => 'slug(这里冗余，方便使用)',
            'origin_id' => '默认源',
            'update_type' => '默认更新类型(operation_update_config_default)',
        ];
    }
}
