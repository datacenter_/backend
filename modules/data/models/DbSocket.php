<?php

namespace app\modules\data\models;

use app\rest\exceptions\BusinessException;
use Yii;

class DbSocket extends \yii\db\ActiveRecord
{
    public static function getDb() {
        return Yii::$app->db_websoket;
    }
}
