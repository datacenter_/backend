<?php

namespace app\modules\data\models;

use app\rest\exceptions\BusinessException;
use Yii;

class DbStream extends \yii\db\ActiveRecord
{
    public static function getDb() {
        $db =  Yii::$app->get('db_stream');
        // 这里重连一下 防止超时
        $db->close();
        $db->open();
        return $db;
    }
}
