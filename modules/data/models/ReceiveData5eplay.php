<?php

namespace app\modules\data\models;

use app\rest\exceptions\BusinessException;
use Yii;

class ReceiveData5eplay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receive_data_5eplay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'log','ip_address','created_at','modified_at'
            ], 'string'],
        ];
    }
}
