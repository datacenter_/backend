<?php

namespace app\modules\data\models;

use app\rest\exceptions\BusinessException;
use Yii;

class ReceiveDataFunspark extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receive_data_funspark';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'log','ip_address','created_at','modified_at'
            ], 'string'],
        ];
    }
}
