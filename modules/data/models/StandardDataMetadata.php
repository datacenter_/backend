<?php

namespace app\modules\data\models;

use Yii;

/**
 * This is the model class for table "standard_data_metadata".
 *
 * @property int $id
 * @property string|null $metadata_type
 * @property string|null $rel_identity_id
 * @property int|null $origin_id
 * @property int|null $game_id
 * @property int|null $belong
 * @property string|null $name
 * @property string|null $name_cn
 * @property string|null $map_type_cn
 * @property string|null $map_type
 * @property string|null $thumbnail
 * @property string|null $rectangle_image
 * @property string|null $square_image
 * @property int|null $is_default 是否为默认地图
 * @property string|null $short_name
 * @property string|null $hotkey
 * @property string|null $image
 * @property string|null $small_image
 * @property string|null $description
 * @property string|null $description_cn
 * @property string|null $slug 删除标记，2表示删除
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property int|null $state
 * @property string|null $title
 * @property string|null $title_cn
 * @property string|null $primary_role
 * @property string|null $primary_role_cn
 * @property string|null $secondary_role
 * @property string|null $secondary_role_cn
 * @property string|null $external_id
 * @property string|null $external_name
 * @property string|null $total_cost
 * @property int|null $is_trinket
 * @property int|null $is_purchasable
 * @property string|null $path_name
 * @property string|null $path_name_cn
 * @property string|null $ammunition 弹夹容量（CSGO）
 * @property string|null $capacity 后备弹量（CSGO）
 * @property string|null $cost 费用（CSGO）
 * @property string|null $kill_award 击杀汇报(CSGO)
 * @property string|null $kind 类别（taser是隶属于我们的gear分类下） enum_csgo_weapon
 * @property int|null $is_recipe dota2专用
 * @property int|null $is_secret_shop dota2专用
 * @property int|null $is_home_shop dota2专用
 * @property int|null $is_neutral_drop dota专用
 * @property int|null $deal_status 处理状态
 * @property string|null $primary_attr
 * @property string|null $primary_attr_cn
 * @property string|null $roles_cn
 * @property string|null $roles
 * @property string|null $data_source CSGO武器  当前数据源
 * @property string|null $firing_mode CSGO武器  设计模式
 * @property int|null $is_t_available CSGO武器  T是否可有
 * @property int|null $is_ct_available CSGO武器  CT是否可有
 * @property string|null $reload_time CSGO武器 装填时间
 * @property string|null $movement_speed CSGO武器   移动速度
 * @property int|null $left_right CSGO武器  
 * @property int|null $level CSGO武器  
 * @property string|null $talent_cn DAOTA  天赋
 * @property string|null $talent DAOTA  天赋
 * @property string|null $talents json 天赋
 * @property int|null $champion_id 英雄id
 * @property int|null $hero_id DaoTa英雄id
 * @property int|null $deleted 1删除，2正常
 * @property string|null $deleted_at deleted_at
 */
class StandardDataMetadata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_metadata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'belong', 'is_default', 'state', 'is_trinket', 'is_purchasable', 'is_recipe', 'is_secret_shop', 'is_home_shop', 'is_neutral_drop', 'deal_status', 'is_t_available', 'is_ct_available', 'left_right', 'level', 'champion_id', 'hero_id', 'deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['metadata_type', 'rel_identity_id', 'name', 'name_cn', 'map_type_cn', 'map_type', 'thumbnail', 'rectangle_image', 'square_image', 'short_name', 'hotkey', 'image', 'small_image', 'title', 'title_cn', 'primary_role', 'primary_role_cn', 'secondary_role', 'secondary_role_cn', 'external_id', 'external_name', 'path_name', 'path_name_cn', 'ammunition', 'capacity', 'cost', 'kill_award', 'kind', 'primary_attr', 'primary_attr_cn', 'roles_cn', 'roles', 'data_source', 'firing_mode', 'reload_time', 'movement_speed', 'talent_cn', 'talent', 'talents'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 225],
            [['total_cost'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'metadata_type' => 'Metadata Type',
            'rel_identity_id' => 'Rel Identity ID',
            'origin_id' => 'Origin ID',
            'game_id' => 'Game ID',
            'belong' => 'Belong',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'map_type_cn' => 'Map Type Cn',
            'map_type' => 'Map Type',
            'thumbnail' => 'Thumbnail',
            'rectangle_image' => 'Rectangle Image',
            'square_image' => 'Square Image',
            'is_default' => 'Is Default',
            'short_name' => 'Short Name',
            'hotkey' => 'Hotkey',
            'image' => 'Image',
            'small_image' => 'Small Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'slug' => 'Slug',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'state' => 'State',
            'title' => 'Title',
            'title_cn' => 'Title Cn',
            'primary_role' => 'Primary Role',
            'primary_role_cn' => 'Primary Role Cn',
            'secondary_role' => 'Secondary Role',
            'secondary_role_cn' => 'Secondary Role Cn',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
            'total_cost' => 'Total Cost',
            'is_trinket' => 'Is Trinket',
            'is_purchasable' => 'Is Purchasable',
            'path_name' => 'Path Name',
            'path_name_cn' => 'Path Name Cn',
            'ammunition' => 'Ammunition',
            'capacity' => 'Capacity',
            'cost' => 'Cost',
            'kill_award' => 'Kill Award',
            'kind' => 'Kind',
            'is_recipe' => 'Is Recipe',
            'is_secret_shop' => 'Is Secret Shop',
            'is_home_shop' => 'Is Home Shop',
            'is_neutral_drop' => 'Is Neutral Drop',
            'deal_status' => 'Deal Status',
            'primary_attr' => 'Primary Attr',
            'primary_attr_cn' => 'Primary Attr Cn',
            'roles_cn' => 'Roles Cn',
            'roles' => 'Roles',
            'data_source' => 'Data Source',
            'firing_mode' => 'Firing Mode',
            'is_t_available' => 'Is T Available',
            'is_ct_available' => 'Is Ct Available',
            'reload_time' => 'Reload Time',
            'movement_speed' => 'Movement Speed',
            'left_right' => 'Left Right',
            'level' => 'Level',
            'talent_cn' => 'Talent Cn',
            'talent' => 'Talent',
            'talents' => 'Talents',
            'champion_id' => 'Champion ID',
            'hero_id' => 'Hero ID',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
        ];
    }
}
