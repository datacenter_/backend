<?php
/**
 *
 */

namespace app\modules\data\services;


use app\modules\common\services\Consts;
use app\modules\data\models\DataBindTime;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\task\models\EnumOrigin;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\Common;
use app\modules\task\services\StandardDataService;
use app\rest\exceptions\BusinessException;

class DataBindingService
{
    /**
     * @param array $standardId
     * @param array $type
     * @return array
     * @throws BusinessException
     * @throws \yii\base\Exception
     */
    public static function cancelBinding($standardId, $type): array
    {
        $info = DataStandardMasterRelation::find()->where(['standard_id' => $standardId])->asArray()->one();
        if (empty($info) && !$info) {
            throw new  BusinessException([], '没有这项信息');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            DataStandardMasterRelation::deleteAll(['standard_id' => $standardId]);
            // 更改状态
            $className = StandardDataService::getStandardActiveTable($type);
            $obj = $className::find()->where(['id' => $standardId])->one();
            $obj->setAttribute('deal_status', 1);
            if (!$obj->save()) {
                throw new BusinessException($obj->getErrors(), '保存失败');
            }
            $transaction->commit();
            return ['success' => '成功', 'msg' => true];
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * 首页待绑定提示列表
     */
    public static function getToDoBindingList()
    {
        //战队
        $standDataTeam = StandardDataTeam::find()->select(['id','game_id','rel_identity_id'])->where(['<>','deal_status',4])->orderBy('modified_at desc')->limit(10);
        $standDataTeamCount = $standDataTeam->count();
        $standDataTeamData = $standDataTeam->asArray()->all();
        foreach ($standDataTeamData as $teamKey => $teamData){
            $standDataTeamData[$teamKey]['type'] = "team";
        }
        $data['list']['team'] = $standDataTeamData;
        $count = $standDataTeamCount;
        //选手
        $standDataPlayer = StandardDataPlayer::find()->select(['id','game_id','rel_identity_id'])->where(['<>','deal_status',4])->orderBy('modified_at desc')->limit(10);
        $standDataPlayerCount = $standDataPlayer->count();
        $standDataPlayerData = $standDataPlayer->asArray()->all();
        foreach ($standDataPlayerData as $playerKey => $playerData){
            $standDataPlayerData[$playerKey]['type'] = "player";
        }
        $data['list']['player'] = $standDataPlayerData;
        $count = $standDataPlayerCount+$count;
        //元数据
        $metadataTypeList = BindingMetadataService::getMetadataTypeList();
        foreach ($metadataTypeList as $typeValue){
            $standDataMeta = StandardDataMetadata::find()->select(['id','rel_identity_id'])->where(['and',
                ['=','metadata_type',$typeValue],
                ['<>','deal_status',4]
            ])->orderBy('modified_at desc')->limit(10);
            $standDataMetaCount = $standDataMeta->count();
            $standDataMetaData = $standDataMeta->asArray()->all();
            foreach ($standDataMetaData as $metaDataKey=>$metaDataValue){
                $standDataMetaData[$metaDataKey]['type'] = $typeValue;
            }
            $data['list'][$typeValue] = $standDataMetaData;
            $count = $standDataMetaCount+$count;
        }
        //赛事
        $standDataTournament = StandardDataTournament::find()->select(['id','game_id','rel_identity_id'])->where(['and',
            ['=','type',1],
            ['<>','deal_status',4]
        ])->orderBy('modified_at desc')->limit(10);
        $standDataTournamentCount = $standDataTournament->count();
        $standDataTournamentData = $standDataTournament->asArray()->all();
        foreach ($standDataTournamentData as $keyTournament => $valueTournament){
            $standDataTournamentData[$keyTournament]['type'] = "tournament";
        }
        $data['list']['tournament'] = $standDataTournamentData;
        $count = $standDataTournamentCount+$count;
        //比赛
        $standDataMatch = StandardDataMatch::find()->select(['id','game_id','rel_identity_id'])->where(['<>','deal_status',4])->orderBy('modified_at desc')->limit(10);
        $standDataMatchCount = $standDataMatch->count();
        $standDataMatchData = $standDataMatch->asArray()->all();
        foreach ($standDataMatchData as $keyMatch => $valueMatch){
            $standDataMatchData[$keyMatch]['type'] = "match";
        }
        $data['list']['match'] = $standDataMatchData;

        $data["list"] = array_merge($data['list']);
//        $data["list"] = array_merge($standDataTeamData,$standDataPlayerData,$standDataTournamentData,$standDataMatchData);
        $count = $standDataMatchCount+$count;
        //统计
        //待绑定统计
        $enumOriginList = EnumOrigin::find()->select(['id','name','desc'])->where(['flag'=>1])->andWhere(['in','id',[2,3,4,7,10]])->asArray()->all();
        $coutData = [];
        //查询战队
        foreach ($enumOriginList as $enumOriginValue){
            $notRelation = 0;
            $teamCount = StandardDataTeam::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationTeamCount = StandardDataTeam::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'team'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $coutData[$enumOriginValue['name']]['team']['total'] = $teamCount['sum'];
            $coutData[$enumOriginValue['name']]['team']['relation'] = $relationTeamCount['sum'];
            $coutData[$enumOriginValue['name']]['team']['not_relation'] = $teamCount['sum'] - $relationTeamCount['sum'];
            $notRelation = $teamCount['sum'] - $relationTeamCount['sum'];
            //按数据源查询选手
            $playerCount = StandardDataPlayer::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationPlayerCount = StandardDataPlayer::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'player'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $coutData[$enumOriginValue['name']]['player']['total'] = $playerCount['sum'];
            $coutData[$enumOriginValue['name']]['player']['relation'] = $relationPlayerCount['sum'];
            $coutData[$enumOriginValue['name']]['player']['not_relation'] = $playerCount['sum'] - $relationPlayerCount['sum'];
            $notRelation = $notRelation + ($playerCount['sum'] - $relationPlayerCount['sum']);

            //按数据源查询赛事
            $tournamentCount = StandardDataTournament::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
                ['=','type',1],
            ])->asArray()->one();

            $relationTournamentCount = StandardDataTournament::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'tournament'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                    ['std.type'=>1]
                ])->asArray()->one();
            $coutData[$enumOriginValue['name']]['tournament']['total'] = $tournamentCount['sum'];
            $coutData[$enumOriginValue['name']]['tournament']['relation'] = $relationTournamentCount['sum'];
            $coutData[$enumOriginValue['name']]['tournament']['not_relation'] = $tournamentCount['sum'] - $relationTournamentCount['sum'];
            $notRelation = $notRelation + ($tournamentCount['sum'] - $relationTournamentCount['sum']);
            //按数据源查询比赛
            $matchCount = StandardDataMatch::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
            ])->asArray()->one();

            $relationMatchCount = StandardDataMatch::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'match'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                ])->asArray()->one();
            $coutData[$enumOriginValue['name']]['match']['total'] = $matchCount['sum'];
            $coutData[$enumOriginValue['name']]['match']['relation'] = $relationMatchCount['sum'];
            $coutData[$enumOriginValue['name']]['match']['not_relation'] = $matchCount['sum'] - $relationMatchCount['sum'];
            $notRelation = $notRelation + ($matchCount['sum'] - $relationMatchCount['sum']);
            //元数据统计
            $metadataTypeList = BindingMetadataService::getMetadataTypeList();
            foreach ($metadataTypeList as $typeValue){
                $metadataCount = StandardDataMetadata::find()->select(['count(*) as sum'])->where(['and',
                    ['=','origin_id',$enumOriginValue['id']],
                    ['=','metadata_type',$typeValue],
                ])->asArray()->one();
                $relationMetadataCount = StandardDataMetadata::find()->alias('std')->select(['count(*) as sum'])
                    ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='.'"'.$typeValue.'"')
                    ->where(['and',
                        ['std.origin_id'=>$enumOriginValue['id']],
                    ])->asArray()->one();
                $coutData[$enumOriginValue['name']][$typeValue]['total'] = $metadataCount['sum'];
                $coutData[$enumOriginValue['name']][$typeValue]['relation'] = $relationMetadataCount['sum'];
                $coutData[$enumOriginValue['name']][$typeValue]['not_relation'] = $metadataCount['sum'] - $relationMetadataCount['sum'];
                $notRelation = $notRelation + ($metadataCount['sum'] - $relationMetadataCount['sum']);
            }
            $totalNotRelation[] = $notRelation;
        }
        $totalNotRelations = 0;
        foreach ($totalNotRelation as $relationValue){
            $totalNotRelations = $totalNotRelations + $relationValue;
        }
        $data['not_relation_total'] = $totalNotRelations;
        $data['count'] = $coutData;
        return $data;
    }

    public static function BindingStatisticsByOri()
    {
        $enumOriginList = EnumOrigin::find()->select(['id','name','desc'])->where(['flag'=>1])->andWhere(['in','id',[2,3,4,7,10]])->asArray()->all();
        $data = [];
        //按数据源查询战队
        foreach ($enumOriginList as $enumOriginValue){
            $teamCount = StandardDataTeam::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationTeamCount = StandardDataTeam::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'team'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $data[$enumOriginValue['name']]['team']['total'] = $teamCount['sum'];
            $data[$enumOriginValue['name']]['team']['relation'] = $relationTeamCount['sum'];
            $data[$enumOriginValue['name']]['team']['not_relation'] = $teamCount['sum'] - $relationTeamCount['sum'];

            //按数据源查询选手
            $playerCount = StandardDataPlayer::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationPlayerCount = StandardDataPlayer::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'player'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $data[$enumOriginValue['name']]['player']['total'] = $playerCount['sum'];
            $data[$enumOriginValue['name']]['player']['relation'] = $relationPlayerCount['sum'];
            $data[$enumOriginValue['name']]['player']['not_relation'] = $playerCount['sum'] - $relationPlayerCount['sum'];

            //按数据源查询赛事
            $tournamentCount = StandardDataTournament::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
                ['=','type',1],
            ])->asArray()->one();

            $relationTournamentCount = StandardDataTournament::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'tournament'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                    ['std.type'=>1]
                ])->asArray()->one();
            $data[$enumOriginValue['name']]['tournament']['total'] = $tournamentCount['sum'];
            $data[$enumOriginValue['name']]['tournament']['relation'] = $relationTournamentCount['sum'];
            $data[$enumOriginValue['name']]['tournament']['not_relation'] = $tournamentCount['sum'] - $relationTournamentCount['sum'];
            //按数据源查询比赛
            $matchCount = StandardDataMatch::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
            ])->asArray()->one();

            $relationMatchCount = StandardDataMatch::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'match'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                ])->asArray()->one();
            $data[$enumOriginValue['name']]['match']['total'] = $matchCount['sum'];
            $data[$enumOriginValue['name']]['match']['relation'] = $relationMatchCount['sum'];
            $data[$enumOriginValue['name']]['match']['not_relation'] = $matchCount['sum'] - $relationMatchCount['sum'];
//            //查询元数据
//            $metadataTypeList = BindingMetadataService::getMetadataTypeList();
//            foreach ($metadataTypeList as $typeValue){
//                $metadataCount = StandardDataMetadata::find()->select(['count(*) as sum'])->where(['and',
//                    ['=','origin_id',$enumOriginValue['id']],
//                    ['=','metadata_type',$typeValue],
//                ])->asArray()->one();
//                $relationMetadataCount = StandardDataTournament::find()->alias('std')->select(['count(*) as sum'])
//                    ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='.'"'.$typeValue.'"')
//                    ->where(['and',
//                        ['std.origin_id'=>$enumOriginValue['id']],
//                        ['std.type'=>1]
//                    ])->asArray()->one();
//                $data[$enumOriginValue['name']][$typeValue]['total'] = $metadataCount['sum'];
//                $data[$enumOriginValue['name']][$typeValue]['relation'] = $relationMetadataCount['sum'];
//                $data[$enumOriginValue['name']][$typeValue]['not_relation'] = $metadataCount['sum'] - $relationMetadataCount['sum'];
//            }
        }
        return $data;
    }
    public static function BindingStatisticsByOrigin()
    {
        $enumOriginList = EnumOrigin::find()->select(['id','name','desc'])->where(['flag'=>1])->andWhere(['in','id',[2,3,4,7,10]])->asArray()->all();
        $data = [];
        //查询战队
        foreach ($enumOriginList as $enumOriginValue){
            $teamCount = StandardDataTeam::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationTeamCount = StandardDataTeam::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'team'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $data['team'][$enumOriginValue['name']]['total'] = $teamCount['sum'];
            $data['team'][$enumOriginValue['name']]['relation'] = $relationTeamCount['sum'];
            $data['team'][$enumOriginValue['name']]['not_relation'] = $teamCount['sum'] - $relationTeamCount['sum'];

            //按数据源查询选手
            $playerCount = StandardDataPlayer::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']]
            ])->asArray()->one();

            $relationPlayerCount = StandardDataPlayer::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'player'")
                ->where(['std.origin_id'=>$enumOriginValue['id']])->asArray()->one();
            $data['player'][$enumOriginValue['name']]['total'] = $playerCount['sum'];
            $data['player'][$enumOriginValue['name']]['relation'] = $relationPlayerCount['sum'];
            $data['player'][$enumOriginValue['name']]['not_relation'] = $playerCount['sum'] - $relationPlayerCount['sum'];

            //按数据源查询赛事
            $tournamentCount = StandardDataTournament::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
                ['=','type',1],
            ])->asArray()->one();

            $relationTournamentCount = StandardDataTournament::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'tournament'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                    ['std.type'=>1]
                ])->asArray()->one();
            $data['tournament'][$enumOriginValue['name']]['total'] = $tournamentCount['sum'];
            $data['tournament'][$enumOriginValue['name']]['relation'] = $relationTournamentCount['sum'];
            $data['tournament'][$enumOriginValue['name']]['not_relation'] = $tournamentCount['sum'] - $relationTournamentCount['sum'];
            //按数据源查询比赛
            $matchCount = StandardDataMatch::find()->select(['count(*) as sum'])->where(['and',
                ['=','origin_id',$enumOriginValue['id']],
            ])->asArray()->one();

            $relationMatchCount = StandardDataMatch::find()->alias('std')->select(['count(*) as sum'])
                ->innerJoin('data_standard_master_relation as dsmr','dsmr.standard_id = std.id and dsmr.resource_type ='."'match'")
                ->where(['and',
                    ['std.origin_id'=>$enumOriginValue['id']],
                ])->asArray()->one();
            $data['match'][$enumOriginValue['name']]['total'] = $matchCount['sum'];
            $data['match'][$enumOriginValue['name']]['relation'] = $relationMatchCount['sum'];
            $data['match'][$enumOriginValue['name']]['not_relation'] = $matchCount['sum'] - $relationMatchCount['sum'];
        }
        return $data;
    }


    //绑定设置开始时间
    public static function setBindingStartTime($bd_type,$start_time,$origin_ids){

        if (!empty($bd_type) && !empty($start_time)){
            $DataBindTime = DataBindTime::find()->where(['bd_type' => $bd_type])->one();
            if ($DataBindTime){
                $updateData = [
                    'bd_start_time' => $start_time,
                    'bd_origin_ids' => $origin_ids
                ];
                $DataBindTime->setAttributes($updateData);
                if (!$DataBindTime->save()){
                    throw new BusinessException($DataBindTime->getErrors(), '保存失败!');
                }
            }else{
                $DataBindTime = new DataBindTime();
                $insertData = [
                    'bd_type' => $bd_type,
                    'bd_start_time' => $start_time,
                    'bd_origin_ids' => $origin_ids
                ];
                $DataBindTime->setAttributes($insertData);
                if (!$DataBindTime->save()){
                    throw new BusinessException($DataBindTime->getErrors(), '保存失败!');
                }
            }
        }
        return ['更新成功！'];
    }
}