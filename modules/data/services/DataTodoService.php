<?php
/**
 *
 */

namespace app\modules\data\services;


use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataTodo;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\Player;
use app\modules\org\services\PlayerService;
use app\modules\org\services\TeamService;
use app\modules\task\services\Common;
use app\modules\task\services\MasterDataService;
use app\rest\exceptions\BusinessException;
use app\modules\match\services\MatchService;
use app\modules\tournament\services\TournamentService;

class DataTodoService
{
    public static function addTodo($resourceType, $majorId, $tagType, $tagValue, $standardId, $taskId, $updateType,$gameId=null)
    {
        // 添加任务的时候，将资源信息冗余进列表，方便查看，检查，列表等
        $resourceInfo = self::getResourceInfo($resourceType, $majorId);
        $info = [
            'resource_type' => $resourceType,
            'resource_id' => $majorId,
            'tag_type' => $tagType,
            'change_info' => json_encode($tagValue),
            'standard_id' => (int)$standardId,
            'task_id' => (int)$taskId,
            'update_type' => $updateType,
            'status' => Consts::TODO_DEAL_STATUS_PADDING,
            'game_id' => $resourceInfo['game_id'] ? $resourceInfo['game_id'] : $gameId,
            'resource_name' => $resourceInfo['resource_name'],
            'resource_logo' => $resourceInfo['resource_logo'],
        ];
        $todo = new DataTodo();
        $todo->setAttributes($info);
        if (!$todo->save()) {
            throw new BusinessException($todo->getErrors(), "添加todo失败");
        }
        $diffInfo = Common::getDiffInfo([], $todo->toArray());
        if ($diffInfo["changed"]) {
            OperationLogService::addLog($diffInfo['change_type'],
                Consts::RESOURCE_TYPE_SYS_DATA_RESOURCE_TO_DO,
                $todo['id'],
                $diffInfo,
                0,
                null,
                null,
                "",
                0,
                0
            );
        }
        if ($updateType == Consts::UPDATE_TYPE_AUTO) {
//            $transaction = \Yii::$app->db->beginTransaction();
//            try {
                self::dealWithTodo($todo->id, Consts::USER_TYPE_ROBOT, 0);
//                $transaction->commit();
//            }catch (\Exception $e) {
//                $transaction->rollBack();
//                throw $e;
//            }
        }
        if ($updateType == Consts::UPDATE_TYPE_NOTHING) {
            return self::setDealStatus($todo->id, Consts::TODO_DEAL_STATUS_NOTHING);
        }
        return $todo->toArray();
    }

    /**
     * @param $id
     * @param $userType
     * @param $userId
     * @return array|bool|object|string
     * @throws BusinessException
     * 自动处理，如果标为自动的，这里做对应的自动更新
     */
    public static function dealWithTodo($id, $userType, $userId)
    {
        $info = DataTodo::find()->where(['id' => $id])->one();
        if (!$info) {
            throw new BusinessException([], '此id不存在');
        }
        // 获取变动类型，变动情况，更新对应资源
        $resourceType = $info['resource_type'];
        $tagType = $info['tag_type'];
        $resourceId = $info['resource_id'];
        //删除log
        $todoInfo = json_decode($info['change_info'], true);
        foreach ($todoInfo as $key => $val) {
            $infoAttribute[$key] = $val['after'];
        }
        if(isset($infoAttribute['deleted']) && $infoAttribute['deleted'] == 1){
            Common::deletedMaster($info['resource_id'],$resourceType,"");
        }
        if (isset($infoAttribute['deleted']) && $infoAttribute['deleted'] == 2){
            Common::recoverMaster($info['resource_id'],$resourceType,"");
        }
        //事务
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            switch ($resourceType) {
                case Consts::RESOURCE_TYPE_PLAYER:
                    self::updatePlayer($resourceId, $tagType, json_decode($info['change_info'], true), $info['id'], $userType, $userId);
                    break;
                case Consts::RESOURCE_TYPE_TEAM:
                    self::updateTeam($resourceId, $tagType, json_decode($info['change_info'], true), $info['id'], $userType, $userId);
                    break;
                case Consts::RESOURCE_TYPE_MATCH:
                    self::updateMatch($resourceId, $tagType, json_decode($info['change_info'], true), $info['id'], $userType, $userId);
                    break;
                case Consts::RESOURCE_TYPE_TOURNAMENT:
                    self::updateTournament($resourceId, $tagType, json_decode($info['change_info'], true), $info['id'], $userType, $userId);
                    break;
                case Consts::METADATA_TYPE_LOL_ITEM:
                case Consts::METADATA_TYPE_LOL_CHAMPION:
                case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                case Consts::METADATA_TYPE_LOL_RUNE:
                case Consts::METADATA_TYPE_LOL_ABILITY:
                //dota2元数据
                case Consts::METADATA_TYPE_DOTA2_HERO:
                case Consts::METADATA_TYPE_DOTA2_ITEM:
                case Consts::METADATA_TYPE_DOTA2_ABILITY:
                case Consts::METADATA_TYPE_DOTA2_TALENT:
                //csgo
                case Consts::METADATA_TYPE_CSGO_MAP:
                case Consts::METADATA_TYPE_CSGO_WEAPON:
                    self::updateMetaData($resourceId, $tagType, json_decode($info['change_info'], true), $info['id'], $userType, $userId,$resourceType, $info['game_id']);
                    break;
                //todo 添加各种资源的更新
                default:
                    throw new BusinessException($info->toArray(), '不能处理此类消息');
                    break;
            }
            $taskInfoDeal =  self::setDealStatus($id, Consts::TODO_DEAL_STATUS_DONE);
            $transaction->commit();
            return $taskInfoDeal;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function setDealStatus($id, $status, $userType = 0, $userId = 0)
    {
        $info = DataTodo::find()->where(['id' => $id])->one();
        if (empty($info)) {
            return false;
        }
        $old = $info->toArray();
        $info->setAttribute('status', $status);
        $info->setAttribute('deal_time', date("Y-m-d H:i:s"));
        $info->save();
        $new = $info->toArray();
        $diffInfo = Common::getDiffInfo($old, $new);
        if ($diffInfo["changed"]) {
            OperationLogService::addLog($diffInfo['change_type'],
                Consts::RESOURCE_TYPE_SYS_DATA_RESOURCE_TO_DO,
                $id,
                $diffInfo,
                0,
                null,
                null,
                "",
                $userType,
                $userId
            );
        }
        return $info->toArray();
    }

    public static function getList($params)
    {
        $whereConfig = [
            "resource_type" => function ($params) {//增加其他游戏的
                if ($params['resource_type'] =='metadata'){
                    if ($params['game_id'] == 0 ) {
                        return ['or',
                            ['like', 'data_todo.resource_type', 'csgo'],
                            ['like', 'data_todo.resource_type', 'lol'],
                            ['like', 'data_todo.resource_type', 'dota2'],
                        ];
                    }
                    if ($params['game_id'] == 1 ) {
                        return [
                            'and', ['like', 'data_todo.resource_type', 'csgo'],
                        ];
                    }
                    if ($params['game_id'] == 2) {
                        return [
                            'and', ['like', 'data_todo.resource_type', 'lol'],
                        ];
                    }
                    if ($params['game_id'] == 3) {
                        return [
                            'and', ['like', 'data_todo.resource_type', 'dota2'],
                        ];
                    }
                }else{
                    if ($params['resource_type']) {
                        return [
                            'and',
                            ['=', 'data_todo.resource_type', $params['resource_type']],
                        ];
                    }
                }
            },
            "resource_id" => function ($params) {
                if ($params['id']) {
                    return [
                        'or', ["=", 'data_todo.resource_id', $params['id']],
                    ];
                }
            },
            'status' => [
                'type' => '=',
                'key' => 'data_todo.status'
            ],
            'game_id' => [
                'type' => '=',
                'key' => 'data_todo.game_id'
            ],
            "resource_name" => function ($params) {
                if ($params['resource_name']) {
                    return [
                        'or', ['like', 'data_todo.resource_name', $params['resource_name']],
                    ];
                }
            }
        ];
        $whereArray = DbHelper::getWhere($whereConfig, $params);
        $where = array_merge(['and'], $whereArray);
        $page = (isset($params['page']) && $params['page']) ? $params['page'] : 1;
        $perPage = (isset($params['per_page']) && $params['per_page']) ? $params['per_page'] : 10;
        $q = DataTodo::find();
        $q->where($where);
        // 时间
        if (isset($params['date_begin']) && $params['date_begin'] == $params['date_end']) {
            $q->andwhere(['like', 'data_todo.created_time', $params['date_begin']]);
        }
        if (isset($params['date_begin']) && $params['date_begin'] != $params['date_end']) {
            $q->andwhere(['>=', 'data_todo.created_time', $params['date_begin']]);
            $q->andwhere(['<=', 'data_todo.created_time', $params['date_end']]);
        }
        $count = $q->count();
        $sql = $q->createCommand()->getRawSql();
        $list = $q->asArray()->orderBy('id desc')->limit($perPage)->offset($perPage * ($page - 1))->all();
        $resourceIds = array_column($list, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_BING_MATCH, $resourceIds);
        foreach ($list as $key => &$val) {
            // 获取操作记录
            $val['operation'] = $operationInfo[$val['id']];
            $val['resource_logo'] = ImageConversionHelper::showFixedSizeConversion($val['resource_logo'], 66, 66, 'ali');
        }
        return [
            'count' => $count,
            'list' => $list
        ];
    }

    // 未完成
    public static function getDetail($id)
    {
        $info = DataTodo::find()->where(['id' => $id])->one();
        $info['change_info'] = json_decode($info['change_info'], true);
        return $info;
    }

    private static function getResourceInfo($resourceType, $resourceId)
    {
        $resourceInfo = [
            'resource_type' => $resourceType,
            'resource_id' => $resourceId,
            'resource_name' => '',
            'resource_logo' => '',
            'game_id' => '',
        ];
        switch ($resourceType) {
            case Consts::RESOURCE_TYPE_PLAYER:
                $info = PlayerService::getPlayer($resourceId);
                $resourceInfo['resource_name'] = $info['core_data']['nick_name'];
                $resourceInfo['resource_logo'] = $info['core_data']['image'];
                $resourceInfo['game_id'] = $info['core_data']['game_id'];
                break;
            case Consts::RESOURCE_TYPE_TEAM:
                $info = TeamService::getTeam($resourceId);
                $resourceInfo['resource_name'] = $info['core_data']['name'];
                $resourceInfo['resource_logo'] = $info['core_data']['image'];
                $resourceInfo['game_id'] = $info['core_data']['game'];
                break;
            case Consts::RESOURCE_TYPE_TOURNAMENT:
                $info = TournamentService::getTournament($resourceId);
                $resourceInfo['resource_name'] = $info['core_data']['name'];
                $resourceInfo['resource_logo'] = $info['core_data']['image'];
                $resourceInfo['game_id'] = $info['core_data']['game'];
                break;
            case Consts::RESOURCE_TYPE_MATCH:
                $info = MatchService::getMatch($resourceId);
                $resourceInfo['resource_name'] = '';
                $resourceInfo['resource_logo'] = '';
                $resourceInfo['game_id'] = $info['core_data']['game'];
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:
            case Consts::METADATA_TYPE_LOL_ABILITY:
            case Consts::METADATA_TYPE_LOL_RUNE:
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
            case Consts::METADATA_TYPE_LOL_CHAMPION:
            case Consts::METADATA_TYPE_DOTA2_HERO:
            case Consts::METADATA_TYPE_DOTA2_ITEM:
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
            case Consts::METADATA_TYPE_DOTA2_TALENT:
            case Consts::METADATA_TYPE_CSGO_MAP:
            case Consts::METADATA_TYPE_CSGO_WEAPON:
                $info = MetadataService::getDetail($resourceType,$resourceId);
                $resourceInfo['resource_name'] = $info['name'];
                $resourceInfo['resource_logo'] = $info['image'];
                break;
            //todo 补充各种获取详情
        }
        return $resourceInfo;
    }

    private static function updatePlayer($majorId, $tagType, $diffInfo, $todoId, $userType, $userId)
    {
        $info = [];
        foreach ($diffInfo as $key => $val) {
            $info[$key] = $val['after'];
        }
        if ($tagType == Consts::TAG_TYPE_CORE_DATA) {
            $info['id'] = $majorId;
            PlayerService::setPlayerCoreData($info, $userType, $userId, $todoId);
        }
        if ($tagType == Consts::TAG_TYPE_BASE_DATA) {
            $info['id'] = $majorId;
            PlayerService::setPlayerBaseData($majorId, $info, $userType, $userId, $todoId);
        }
    }

    /**
     * 修改team主表  By  王傲渊
     * $majorId         资源ID,队伍ID(数值)
     * $tagType         资源子类(字符串)
     * $diffInfo        变化信息(数组)
     * $todoId          data_todo表ID(数值)
     * $userType        人员类别:1.管理员:手动,2.机器人:自动(数值)
     * $userId          用户ID:自动时ID为零(数值)
     */
    private static function updateTeam($majorId, $tagType, $diffInfo, $todoId, $userType, $userId)
    {
        //变化信息
        $info = [];
        foreach ($diffInfo as $key => $value) {
            //变化后的信息
            $info[$key] = $value['after'];
        }
        $info['id'] = $majorId;//资源ID
        //数据类型
        //team表
        if ($tagType === Consts::TAG_TYPE_CORE_DATA) {
            TeamService::setTeamCoreData($info, $userType, $userId, $todoId, $majorId);
        }
        //team_introduction表
        if ($tagType === Consts::TAG_TYPE_BASE_DATA) {
            TeamService::setTeamBaseData($majorId, $info, $userType, $userId, $todoId);
        }
        //team_player_relation表
        if ($tagType === Consts::TAG_TYPE_TEAM_PLAYER) {
            TeamService::setTeamPlayers($majorId, $diffInfo['player_id']['after'], $userType, $userId, $todoId);
        }
    }

    /**
     * 修改match主表  By  王傲渊
     * $majorId         资源ID,队伍ID(数值)
     * $tagType         资源子类(字符串)
     * $diffInfo        变化信息(数组)
     * $todoId          data_todo表ID(数值)
     * $userType        人员类别:1.管理员:手动,2.机器人:自动(数值)
     * $userId          用户ID:自动时ID为零(数值)
     */
    private static function updateMatch($majorId, $tagType, $diffInfo, $todoId, $userType, $userId)
    {
        //变化信息
        $info = [];
        foreach ($diffInfo as $key => $value) {
            //变化后的信息
            $info[$key] = $value['after'];
        }
        $info['id'] = $majorId;//资源ID
        //数据类型
        //match表
        if ($tagType === Consts::TAG_TYPE_CORE_DATA) {
            MatchService::setMatchCoreData($info, $userType, $userId, $todoId, $majorId);
        }
        //match_base表
        if ($tagType === Consts::TAG_TYPE_BASE_DATA) {
            MatchService::setMatchBaseData($majorId, $info, $userType, $userId, $todoId);
        }
        //real_time表
        if ($tagType === Consts::TAG_TYPE_REAL_TIME) {
            MatchService::setMatchRealData($majorId, $info, $userType, $userId, $todoId);
        }
    }

    /**
     * 修改tournament主表  By  王傲渊
     * $majorId         资源ID(数值)
     * $tagType         资源子类(字符串)
     * $diffInfo        变化信息(数组)
     * $todoId          data_todo表ID(数值)
     * $userType        人员类别:1.管理员:手动,2.机器人:自动(数值)
     * $userId          用户ID:自动时ID为零(数值)
     */
    private static function updateTournament($majorId, $tagType, $diffInfo, $todoId, $userType, $userId)
    {
        //变化信息
        $info = [];
        foreach ($diffInfo as $key => $value) {
            //变化后的信息
            $info[$key] = $value['after'];
        }
        $info['id'] = $majorId;//资源ID
        //数据类型
        //tournament表
        if ($tagType === Consts::TAG_TYPE_CORE_DATA ||  $tagType === Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION) {
            TournamentService::setTournamentCoreData($info, $userType, $userId, $todoId, $majorId);
        }
        //tournament_base表
        if ($tagType === Consts::TAG_TYPE_BASE_DATA) {
            TournamentService::setTournamentBaseData($majorId, $info, $userType, $userId, $todoId);
        }
        //tournament_price_distribute
        if ($tagType === Consts::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE) {
            TournamentService::setTournamentPriceDistribute($majorId, $info['prize_distribution'], $userType, $userId, $basisId = 0, $originId = 0);
        }
        if ($tagType === Consts::TAG_TYPE_ATTEND_TEAM) {
            $team_relation = json_decode($info['team_relation'],true);
//            $team_relation = array_column($a,'id');
            TournamentService::setTeamRelation($majorId, $team_relation, $userType, $userId, $basisId = 0, $originId = 0);
        }
    }

    private static function updateMetaData($majorId, $tagType, $diffInfo, $todoId, $userType, $userId,$resourceType,$gameId)
    {
        //变化信息
        $info = [];
        foreach ($diffInfo as $key => $value) {
            //变化后的信息
            $info[$key] = $value['after'];
        }
        $info['id'] = $majorId;//资源ID
        if ($tagType === Consts::TAG_TYPE_CORE_DATA) {
            MetadataService::editInfo($resourceType,$info, $userType, $userId, $gameId,$todoId,$majorId);
        }
        if ($tagType === Consts::TAG_TYPE_BASE_DATA) {
            MetadataService::editInfo($resourceType,$info, $userType, $userId, $gameId,$todoId,$majorId);
        }
    }
}