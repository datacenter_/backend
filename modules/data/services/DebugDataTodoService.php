<?php
/**
 *
 */

namespace app\modules\data\services;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\HelperResourceFormat;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataTodo;
use app\modules\org\services\PlayerService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\StandardDataService;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use app\modules\org\models\Clan;

class DebugDataTodoService
{
    /**
     * 测试添加DeBug
     * $params          前端传入假数据(数组)
     */
    public static function debugAddToDo($params)
    {
        $originId = isset($params['origin_id'])?$params['origin_id']:"";//数据源ID
        $originType = isset($params['origin_type'])?$params['origin_type']:"";
        if((!$originType)&&$originId){
            $originList=EnumService::getEnum('origin');//数据源列表
            $originInfo=$originList[$originId];//数据源信息
            $originType = $originInfo['name'];//数据源名称
        }
        $resourceType = $params['resource_type'];//数据源类型
        $standardId = $params['standard_id'];//标准表对应ID
        $changeInfo = $params['change_info'];//变化信息
        // 添加一条对应资源的变动记录
        // 获取对应关系表
        $standardActiveTableClass = StandardDataService::getStandardActiveTable($resourceType);
        $standardInfoObj = $standardActiveTableClass::find()->where(['id' => $standardId])->one();
        if (!$standardInfoObj) {
            throw new BusinessException([], '没有这条记录');
        }
        $before = $standardInfoObj->toArray();
        $updateFilter=['id','origin_id','game_id','rel_identity_id','created_at','modified_at'];
        //赛事开始时间校验
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            if (!empty($changeInfo['scheduled_begin_at'])){
                if(date('Y-m-d H:i:s',strtotime($changeInfo['scheduled_begin_at'])) == "1970-01-01 08:00:00"){
                    unset($changeInfo['scheduled_begin_at']);
                }
            }
            if (!empty($changeInfo['scheduled_end_at'])){
                if(date('Y-m-d H:i:s', strtotime($changeInfo['scheduled_end_at'])) == "1970-01-01 08:00:00"){
                    unset($changeInfo['scheduled_end_at']);
                }
            }
        }
        if($resourceType == Consts::RESOURCE_TYPE_MATCH){
            $preg = '/^([12]\d\d\d)-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[0-1]) ([0-1]\d|2[0-4]):([0-5]\d)(:[0-5]\d)?$/';
            if (!empty($changeInfo['original_scheduled_begin_at'])){
                if(!preg_match($preg,$changeInfo['original_scheduled_begin_at'])){
                    unset($changeInfo['original_scheduled_begin_at']);
                }
            }
            if (!empty($changeInfo['scheduled_begin_at'])){
                if(!preg_match($preg,$changeInfo['scheduled_begin_at'])){
                    unset($changeInfo['scheduled_begin_at']);
                }
            }
            if (!empty($changeInfo['begin_at'])){
                if(!preg_match($preg,$changeInfo['begin_at'])){
                    unset($changeInfo['begin_at']);
                }
            }
            if (!empty($changeInfo['end_at'])){
                if(!preg_match($preg,$changeInfo['end_at'])){
                    unset($changeInfo['end_at']);
                }
            }
        }
        $updateInfo=$changeInfo;
        foreach($updateFilter as $unsetKey){
//            if(isset($updateInfo[$unsetKey])){
                unset($updateInfo[$unsetKey]);
//            }
        }
//        $updateInfo['deleted']=(int)$updateInfo['deleted']?(int)$updateInfo['deleted']:2;
        $standardInfoObj->setAttributes($updateInfo);
        if (!$standardInfoObj->save()) {
            throw new BusinessException($standardInfoObj->getErrors(), '没有这条记录');
        }
        $after = $standardInfoObj->toArray();
        $diffInfo = Common::getDiffInfo($before, $after);
        if(!$diffInfo['changed']){
            throw new BusinessException([],'数据没有发生变化');
        }
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originType,
            $resourceType,
            $diffInfo["change_type"],
            "");
        $changeBody = $diffInfo;
        $item = [
            "tag" => $tag,
            "params" => $changeBody,
        ];
        $taskInfo=TaskRunner::addTask($item, 3);
        // 添加debug数据

        return $taskInfo;
    }

    public function debugInfoList()
    {

    }
}