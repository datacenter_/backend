<?php
/**
 *
 */

namespace app\modules\data\services;

use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataResourceUpdateConfigOperation;
use app\modules\data\models\DataUpdateConfigDefault;
use app\modules\match\models\Match;
use app\modules\match\models\SortingLog;
use app\modules\match\services\MatchService;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\TaskRunner;
use app\modules\tournament\services\TournamentService;
use app\rest\exceptions\BusinessException;
use Redis;

class UpdateConfigService
{
    const RESOURCE_TAG_CONFIG=[
        Consts::RESOURCE_TYPE_TEAM=>[
            Consts::TAG_TYPE_CORE_DATA,
            Consts::TAG_TYPE_BASE_DATA,
            Consts::TAG_TYPE_TEAM_PLAYER,
        ],
        Consts::RESOURCE_TYPE_PLAYER=>[
            Consts::TAG_TYPE_CORE_DATA,
            Consts::TAG_TYPE_BASE_DATA,
        ],
        Consts::RESOURCE_TYPE_TOURNAMENT=>[
            Consts::TAG_TYPE_CORE_DATA,
            Consts::TAG_TYPE_BASE_DATA,
            Consts::TAG_TYPE_ATTEND_TEAM,
        ],
        Consts::RESOURCE_TYPE_MATCH=>[
            Consts::TAG_TYPE_CREATE_DATA,
            Consts::TAG_TYPE_BINDING_DATA,
            Consts::TAG_TYPE_UPDATE_DATA,
            Consts::TAG_TYPE_SCORE_DATA,
        ],
        //lol技能
        Consts::METADATA_TYPE_LOL_ABILITY =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //lol英雄
        Consts::METADATA_TYPE_LOL_CHAMPION =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //lol道具
        Consts::METADATA_TYPE_LOL_ITEM =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //lol符文
        Consts::METADATA_TYPE_LOL_RUNE =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //lol召唤师技能
        Consts::METADATA_TYPE_LOL_SUMMONER_SPELL =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //csgo地图
        Consts::METADATA_TYPE_CSGO_MAP =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //csgo武器
        Consts::METADATA_TYPE_CSGO_WEAPON =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //dota2英雄
        Consts::METADATA_TYPE_DOTA2_HERO =>[
            Consts::TAG_TYPE_CORE_DATA,
            Consts::TAG_TYPE_BASE_DATA,
        ],
        //dota2道具
        Consts::METADATA_TYPE_DOTA2_ITEM =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //dota2技能
        Consts::METADATA_TYPE_DOTA2_ABILITY =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
        //dota2天赋
        Consts::METADATA_TYPE_DOTA2_TALENT =>[
            Consts::TAG_TYPE_CORE_DATA,
        ],
    ];
    public static function initDefaultConfig()
    {
        $games=EnumService::getEnum('games');
        foreach ($games as $game){
            foreach(self::RESOURCE_TAG_CONFIG as $resource=>$tagInfos){
                $tmpInfo=[];
                foreach($tagInfos as $tag){
                    $tmpInfo[$resource.".".$tag]=[
                        'origin_id'=>1,
                        'update_type'=>1,
                    ];
                }
                self::setDefaultInfo($game['id'],$tmpInfo);
            }
        }

    }

    public static function setDefaultInfo($gameId,$configInfo,$userType=OperationLogService::USER_TYPE_ADMIN,$userId=1)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            $operationType=OperationLogService::OPERATION_TYPE_ADD;
            $oldInfo=self::getValuesBySlugs($gameId,array_keys($configInfo));
            foreach($configInfo as $key=>$val){
                $resourceTagInfo=self::getResourceTagByKeyString($key);
                if(isset(self::RESOURCE_TAG_CONFIG[$resourceTagInfo['resource']]) &&
                    in_array($resourceTagInfo['tag'],self::RESOURCE_TAG_CONFIG[$resourceTagInfo['resource']])){
                    // 更新数据
                    $configWhere=['game_id'=>$gameId,
                        'resource_type'=>$resourceTagInfo['resource'],
                        'tag_type'=>$resourceTagInfo['tag'],
                        'slug'=>$key];
                    $info=DataUpdateConfigDefault::find()
                        ->andWhere($configWhere)->one();
                    $configData=[
                        'origin_id'=>$val['origin_id'],
                        'update_type'=>$val['update_type'],
                    ];
                    $old=[];
                    if($info){
                        $operationType=OperationLogService::OPERATION_TYPE_UPDATE;
                        $old=$info->toArray();
                        $info->setAttributes($configData);
                    }else{
                        $info=new DataUpdateConfigDefault();
                        $info->setAttributes(array_merge($configWhere,$configData));
                    }
                    if(!$info->save()){
                        throw new BusinessException($info->getErrors(),'保存失败for:'.$key);
                    }
                    $new = $info->toArray();
                    $diffInfoTmp=Common::getDiffInfo($old,$new,array_keys($configData));
                    if($diffInfoTmp['changed']){
                        $diffInfos[]=[
                            'slug'=>$key,
                            'new'=>$new,
                            'old'=>$old,
                            'diff'=>$diffInfoTmp
                        ];
                    }
                }
            }
            $newInfo=self::getValuesBySlugs($gameId,array_keys($configInfo));
            $diffInfo=Common::getDiffInfo($oldInfo,$newInfo,null,true);
            if($diffInfo["changed"]){
                OperationLogService::addLog($operationType,Consts::RESOURCE_TYPE_SYS_CONFIG_UPDATE,
                    $gameId,['new'=>$newInfo,'diff'=>$diffInfo['diff']],
                    0,
                    null,
                    null,
                    "",
                    $userType,
                    $userId);
            }
            // 添加changelog
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }

    }

    public static function addBatchUpdateTask($gameId,$resourceType,$configInfo,$userType=OperationLogService::USER_TYPE_ADMIN,$userId=1)
    {
        // 添加操作
        $operation=new DataResourceUpdateConfigOperation();
        $operation->setAttributes([
            'game_id'=>$gameId,
            'resource_type'=>$resourceType,
            'config_info'=>json_encode($configInfo),
            'operator'=>$userId,
        ]);
        if(!$operation->save()){
            throw new BusinessException($operation->getErrors(),"批量修改添加任务失败");
        }
        // 添加任务，然后去执行
        $taskInfo=TaskService::addTaskRefreshUpdateConfig($resourceType,$operation->toArray());
        // 更新operation的rel
        $operation->setAttribute('task_info_id',$taskInfo['id']);
        $operation->save();
    }

    private static function getValuesBySlugs($gameId,$slugs)
    {
        $list=[];
        $info= DataUpdateConfigDefault::find()->where(['game_id'=>$gameId])->andWhere(['in','slug',$slugs])->asArray()->all();
        foreach($info as $key=>$val){
            $list[$val['slug']]=[
                'origin_id'=>$val['origin_id'],
                'update_type'=>$val['update_type'],
            ];
        }
        return $list;
    }

    private static function getResourceTagByKeyString($str)
    {
        $infoArr=explode(".",$str);
        $resource=$infoArr[0];
        $tag=$infoArr[1];
        return [
            'resource'=>$resource,
            'tag'=>$tag
        ];
    }

    public static function getDefaultConfigList()
    {
        // 获取所有数据做组装
        $games=EnumService::getEnum('games');
        $info=DataUpdateConfigDefault::find()->asArray()->all();
        $configInfo=[];
        foreach($info as $key=>$val){
            $configInfo[$val['game_id']]['config'][$val['resource_type'].'.'.$val['tag_type']]=$val;
        }
        // 获取资源id,根据资源id获取统计信息
        $resourceIds=array_keys($configInfo);
        $operationInfo=OperationLogService::operationCounts(Consts::RESOURCE_TYPE_SYS_CONFIG_UPDATE,$resourceIds);
        // 数据组装
        foreach ($configInfo as $key=>$val){
            $configInfo[$key]['game']=$games[$key];
            $configInfo[$key]['operation']=$operationInfo[$key];
        }
        return array_values($configInfo);
    }

    public static function getDefaultConfigDetail($gameId,$resourceType)
    {
        $games=EnumService::getEnum('games');
        $info=DataUpdateConfigDefault::find()
            ->where(['game_id'=>$gameId,'resource_type'=>$resourceType])->asArray()->all();
        $configInfo=[];
        foreach($info as $key=>$val){
            $configInfo['config'][$val['resource_type'].'.'.$val['tag_type']]=$val;
        }
        // 数据组装
        $configInfo['game']=$games[$gameId];
        return $configInfo;
    }

    public static function initResourceUpdateConfig($resourceId, $resourceType, $gameId)
    {
        // 获取默认配置
        $defaultConfig=self::getDefaultConfigDetail($gameId,$resourceType);
        // 自动初始化
        self::updateResourceUpdateConfig($resourceId,$defaultConfig["config"],Consts::USER_TYPE_ROBOT,1);
    }

    public static function getResourceUpdateConfig($resourceId,$resourceType)
    {
        // 获取配置
        $info= DataResourceUpdateConfig::find()->where(['resource_id'=>$resourceId])->andWhere(['resource_type'=>$resourceType])->asArray()->all();
        $tagInfo=Common::array_columns($info,['origin_id','update_type'],'tag_type');
        return $tagInfo;
    }

    public static function getResourceUpdateConfigDetail($resourceId,$resourceType)
    {
        // 获取配置
        $info= DataResourceUpdateConfig::find()->where(['resource_id'=>$resourceId])->andWhere(['resource_type'=>$resourceType])->asArray()->all();
        $configInfo=[];
        $gameId=0;
        foreach($info as $key=>$val){
            $gameId=$val['game_id'];
            $configInfo['config'][$val['resource_type'].'.'.$val['tag_type']]=$val;
        }
//        $tagInfo=Common::array_columns($info,['origin_id','update_type'],'tag_type');
//        return $tagInfo;
        $games=EnumService::getEnum('games');
        // 数据组装
        $configInfo['game']=$games[$gameId];
        return $configInfo;
    }

    public static function updateResourceUpdateConfig($resourceId, $configInfo,$userType=OperationLogService::USER_TYPE_ADMIN,$userId=1)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            $operationType=OperationLogService::OPERATION_TYPE_ADD;
            $oldInfo=self::getResourceValuesBySlugs($resourceId,array_keys($configInfo));
            foreach($configInfo as $key=>$val){
                $resourceTagInfo=self::getResourceTagByKeyString($key);
                if(isset(self::RESOURCE_TAG_CONFIG[$resourceTagInfo['resource']]) &&
                    in_array($resourceTagInfo['tag'],self::RESOURCE_TAG_CONFIG[$resourceTagInfo['resource']])){
                    // 更新数据
                    $configWhere=['resource_id'=>$resourceId,
                        'resource_type'=>$resourceTagInfo['resource'],
                        'tag_type'=>$resourceTagInfo['tag'],
                        'slug'=>$key];

                    $info=DataResourceUpdateConfig::find()
                        ->andWhere($configWhere)->one();
                    if ($resourceTagInfo['tag']=='score_data'){
                        $rel_match_id = HotBase::getRelIdByMasterId('match',$val['resource_id'],$val['origin_id']);
                        if ($val['origin_id'] == 9  || $val['origin_id'] == 10){
                            $rel_match_id =  HotBase::getPerIdByRelMatchId($rel_match_id,$val['origin_id']);
                        }
                        $redis = new Redis();
                        $redis->connect(env('REDIS_HOST'),env('REDIS_PORT'));
                        $redis->auth(env('REDIS_PASSWORD'));
                        $redis->select(env('REDIS_DATABASE'));
                        $redisKey='kafka:autoStatus:' . $info['origin_id'] . ':' . $rel_match_id;
                        $redis->del($redisKey);
                    }
                    $configData=[
                        'origin_id'=>$val['origin_id'],
                        'update_type'=>$val['update_type'],
                    ];
                    // 初始化的时候 带着game_id,这里做冗余了，后续更新的时候 有的地方不带game_id
                    // 刷新配置的时候 用到这里面的game_id了，所以尽可能保证逻辑
                    if(isset($val["game_id"]) && $val["game_id"]){
                        $configData['game_id']=$val["game_id"];
                    }

                    if($info){
                        $operationType=OperationLogService::OPERATION_TYPE_UPDATE;
                        $info->setAttributes($configData);
                    }else{
                        $info=new DataResourceUpdateConfig();
                        $info->setAttributes(array_merge($configWhere,$configData));
                    }
                    if(!$info->save()){
                        throw new BusinessException($info->getErrors(),'保存失败for:'.$key);
                    }
//                    //判断要不要更新hot数据
//                    if ($resourceTagInfo['resource'] == 'match' && $resourceTagInfo['tag'] == 'score_data'){
//                        if ($val['update_type'] == 3){
//                            $auto_status = 1;
//                        }else{
//                            $auto_status = 2;
//                        }
//                        $match_data_RealTimeInfo = [
//                            'auto_status'=>$auto_status,
//                            'update_origin_id'=>$val['origin_id']
//                        ];
//                        MatchService::setRealTimeInfo($resourceId, $match_data_RealTimeInfo, 'auto');
//                    }
                }
            }
            $newInfo=self::getResourceValuesBySlugs($resourceId,array_keys($configInfo));
            $diffInfo=Common::getDiffInfo($oldInfo,$newInfo,null,true);
            if($diffInfo["changed"]){
                OperationLogService::addLog($operationType,
                    Consts::RESOURCE_TYPE_SYS_DATA_RESOURCE_UPDATE_CONFIG,
                    $resourceId,
                    ['new'=>$newInfo,'diff'=>$diffInfo['diff']],
                    0,
                    null,
                    null,
                    "",
                    $userType,
                    $userId
                    );
            }
            // 添加changelog
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }

    }
    public static function updateResourceUpdateConfigByResourceAndType($resourceId, $resource_type,$tag_type,$origin_id,$update_type)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            // 更新数据
            $configWhere=[
                'resource_id'=>$resourceId,
                'resource_type'=>$resource_type,
                'tag_type'=>$tag_type
            ];
            $info=DataResourceUpdateConfig::find()
                ->andWhere($configWhere)->one();
            $configData=[
                'origin_id'=>$origin_id,
                'update_type'=>$update_type,
            ];
            if($info){
                $info->setAttributes($configData);
            }else{
                throw new BusinessException($info->getErrors(),'此比赛无配置信息');
            }
            if(!$info->save()){
                throw new BusinessException($info->getErrors(),'此比赛配置信息更新失败');
            }
            // 添加changelog
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }
    }

    private static function getResourceValuesBySlugs($resourceId,$slugs)
    {
        $list=[];
        $info= DataResourceUpdateConfig::find()->where(['resource_id'=>$resourceId])->andWhere(['in','slug',$slugs])->asArray()->all();
        foreach($info as $key=>$val){
            $list[$val['slug']]=[
                'origin_id'=>$val['origin_id'],
                'update_type'=>$val['update_type'],
            ];
        }
        return $list;
    }
    //赛事页面中 单条赛事的比赛设置调用  只是刷新不做设置
    public static function setMatchResourceConfig($tournamentId,$scheduleOriginId,$scheduleUpdateType,$scoreOriginId,$scoreUpdateType){
        //获取赛事下所有比赛
        $matchList = TournamentService::getMatchListByTournamentId($tournamentId);
        if ($matchList){
            foreach ($matchList as $key => $item){
                $matchId = $item['id'];
                //赛程
                self::updateResourceUpdateConfigByResourceAndType($matchId, 'match','update_data',$scheduleOriginId,$scheduleUpdateType);
                //实时比分
                self::updateResourceUpdateConfigByResourceAndType($matchId, 'match','score_data',$scoreOriginId,$scoreUpdateType);
            }
        }
        return true;
    }
    public static function setAutoInfo($matchId,$autoStatus,$autoOriginId){
        if($autoStatus==1){
            if($autoOriginId==5){
                // 5e的赛事绑定状态不在关联关系，在sorting_log
                $sortingLog=SortingLog::find()->where(['match_id'=>$matchId])->one();
                if(!$sortingLog){
                    throw new BusinessException([],'没有对应数据源的绑定信息'.$autoOriginId);
                }
                self::addToHotListFive($matchId,$sortingLog);
            }else{
                $standardInfo=MatchService::getRelStandardInfo($matchId,$autoOriginId);
                if(!$standardInfo){
                    //新建HOT表
                    $MatchInfo = Match::find()->select(['game','scheduled_begin_at'])->where(['id'=>$matchId])->asArray()->one();
                    //没有绑定信息 新增
                    $info = [
                        'auto_origin_id'=>$autoOriginId,
                        'auto_status'=>1,
                        'auto_add_type'=>'restapi',
                        'game_id' => @$MatchInfo["game"],
                        'match_start_time' => @$MatchInfo["scheduled_begin_at"],
                    ];
                    MatchService::addHotAutoListByCreateMatch($matchId,$info);
                    //throw new BusinessException([],'没有对应数据源的绑定信息'.$autoOriginId);
                }else{
                    $info = [
                        'auto_add_type'=>'restapi'
                    ];
                    MatchService::addHotAutoList($matchId,$standardInfo,$info);
                }
            }
        }else{
            //关闭
            MatchService::setAutoStop($matchId,$autoOriginId);
//            if($autoOriginId==5){
//                MatchService::setAutoStop($matchId,$autoOriginId);
//            }elseif($autoOriginId == 3){
//                //关闭
//                MatchService::setAutoStop($matchId,$autoOriginId);
//            }else {
//                $standardInfo = MatchService::getRelStandardInfo($matchId, $autoOriginId);
//                if ($standardInfo) {
//                    self::setStop($standardInfo["id"]);
//                }
//            }
        }
    }
    private static function addToHotListFive($matchId,$sortingLog)
    {
        $hotInfo=HotDataRunningMatch::find()
            ->where(['match_id'=>$matchId])
            ->andWhere(['origin_id'=>5])
            ->one();
        if($hotInfo){
            $hotInfo->setAttributes([
                'auto_status'=>1,
                'from_event_id'=>0,
                'to_event_id'=>0
            ]);
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存失败');
            }
        }else{
            $hot=new HotDataRunningMatch();
            $hot->setAttributes([
                'match_id'=>$matchId,
                'standard_id'=>$sortingLog['id'],
                'origin_id'=>5,
                'game_id'=>1,
                'rel_identity_id'=>'',
                'match_start_time'=>'',
                'auto_status'=>1
            ]);
            if(!$hot->save()){
                throw new BusinessException($hot->getErrors(),'保存失败');
            }
        }
    }
}