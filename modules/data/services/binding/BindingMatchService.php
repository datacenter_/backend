<?php
/**
 *
 */

namespace app\modules\data\services\binding;


use app\modules\common\models\EnumGameRules;
use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\match\services\MatchService;
use app\modules\operation\models\OperationStandardMasterRelation;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\models\TaskDataHotInfo;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\RefreshResource;
use app\modules\task\services\StandardDataService;
use app\modules\task\services\UpdateConfig;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\services\TournamentService;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class BindingMatchService
{
//    drop view if exists view_relation_expect_match;
//    CREATE VIEW `view_relation_expect_match`
//    AS
//    SELECT `pdsmre`.`standard_id`                                                                               AS `standard_id`,
//    group_concat(`pdsmre`.`master_id` separator ',')                                                     AS `master_id_all`,
//    concat(group_concat(ifnull(`mpeb`.`name`,'') separator ','), ',',
//    group_concat(ifnull(`mpeb`.`name_cn`,'') separator ','), ',',
//    group_concat(ifnull(`mpeb`.`short_name`,'') separator ','), ',',
//    group_concat(ifnull(`mpeb`.`short_name_cn`,'') separator ',')
//    )                                                                                                     AS `tournament_name_all`,
//    concat(group_concat(ifnull(`team1`.`name`,'') separator ','), ',',
//    group_concat(ifnull(`team2`.`name`,'')  separator ','))                                                 AS `team_name_all`,
//    `edc`.`origin_id`                                                                                    AS `origin_id`,
//    if(`mpe`.`id`, ifnull(`ori`.`desc`, '人工'), '')                                                       AS `change_type`
//    FROM `data_standard_master_relation_expect` `pdsmre`
//    left join `match` `mpe` on (`mpe`.`id` = `pdsmre`.`master_id`)
//    left join `data_change_config` `edc`
//    on (`edc`.`id` = `pdsmre`.`master_id`) and (`edc`.`resource_type` = 'match')
//    left join `enum_origin` `ori` on `edc`.`origin_id` = `ori`.`id`
//    left join `tournament` `mpeb` on (`mpeb`.`id` = `mpe`.`tournament_id`)
//    left join `team` `team1` on (`mpe`.`team_1_id` = `team1`.`id`)
//    left join `team` `team2` on (`mpe`.`team_2_id` = `team2`.`id`)
//
//    where (`pdsmre`.`resource_type` = 'match')
//    group by `pdsmre`.`standard_id`;
    //
    public static function getList($params)
    {
        if (!empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $params['date_end'] = $end;
        }

        if (!empty($params['scheduled_end_at'])) {
            $scheduledEndAt= date('Y-m-d H:i:s', strtotime('+1 day', strtotime($params['scheduled_end_at']))-1);
            $params['scheduled_end_at'] = $scheduledEndAt;
        }

        $q=StandardDataMatch::find()->alias('sm');
            $q->select(['sm.*','t.name as tournament_name','t.name_cn as tournament_name_cn','dcc.origin_id as dcc_origin_id','if(els_m.id,ifnull(ori.desc,"人工"),pdsmre.change_type) as change_type'
            ,'sm.id as sm_id','steam1.id as steam1_id','steam2.id as steam2_id','els_m.id as els_m_id','els_m.game_rules as els_game_rules',
                'els_m.match_type as els_match_type','els_m.number_of_games as els_number_of_games',
                'els_m.scheduled_begin_at as els_scheduled_begin_at','team1.name as els_team_1_name','team2.name as els_team_2_name',
                'st.id as st_id','t.id as tid','t.name as els_tournament_name','st.name as standard_tournament_name',
                'steam1.name as team_1_name','steam2.name as team_2_name','dcc.origin_id as dcc_origin_id'
            ]);
            // 关联比赛
            $q->leftJoin('data_standard_master_relation as smrtm','smrtm.standard_id = sm.id and smrtm.resource_type ='."'match'");
            $q->leftJoin('match as els_m','smrtm.master_id = els_m.id');
            $q->leftJoin('team as team1','els_m.team_1_id = team1.id');
            $q->leftJoin('team as team2','els_m.team_2_id = team2.id');
            $q->leftJoin('tournament as t','els_m.tournament_id = t.id');

            $q->leftJoin('standard_data_tournament as st','st.origin_id = sm.origin_id and st.rel_identity_id = sm.tournament_id and st.game_id = sm.game_id and st.type = "1"');
            // 绑战队关联
            $q->leftJoin('standard_data_team as steam1','steam1.rel_identity_id = sm.team_1_id and steam1.origin_id = sm.origin_id');
            $q->leftJoin('standard_data_team as steam2','steam2.rel_identity_id = sm.team_2_id and steam2.origin_id = sm.origin_id');
            $q->leftJoin('data_change_config as dcc','dcc.resource_id = smrtm.master_id and dcc.resource_type = "match"');
            $q->leftJoin('enum_origin as ori','ori.id = dcc.origin_id');
            // 绑赛事关联
            $q->leftJoin('view_relation_expect_match as pdsmre', 'pdsmre.standard_id = sm.id');



        $whereConfig=[
            "origin_id"=>[
                'type'=>'=',
                'key'=>'sm.origin_id'
            ],
            "game_id"=>[
                'type'=>'=',
                'key'=>'sm.game_id'
            ],
            "deal_status"=>[
                'type'=>'=',
                'key'=>'sm.deal_status'
            ],
            "status"=>[
                'type'=>"=",
                'key'=>'sm.status'
            ],
            "rel_identity_id" => function ($params) {
                if (isset($params['rel_identity_id']) && $params['rel_identity_id']) {
                    return ["=", 'sm.rel_identity_id', $params['rel_identity_id']];
                }
            },
            "scheduled_begin_at"=>[
                'type'=>">=",
                'key'=>'sm.scheduled_begin_at'
            ],
            "scheduled_end_at"=>[
                'type'=>"<=",
                'key'=>'sm.scheduled_begin_at'
            ],
            "date_begin"=>[
                'type'=>">=",
                'key'=>'sm.modified_at'
            ],
            "date_end"=> [
                'type'=>"<",
                'key'=>'sm.modified_at'
            ],
        ];
        $whereArray=DbHelper::getWhere($whereConfig,$params);
        $where=array_merge(['and'],$whereArray);

        $q->where($where);

        // 赛事名称
        if (isset($params['tournament_name']) && !empty($params['tournament_name']))
        {
            $q->andWhere(['or',
                ['like','st.name',$params['tournament_name']],
                ['like','t.name',$params['tournament_name']],
                ['like','pdsmre.tournament_name_all', $params['tournament_name']]
            ]);
        }
        // 战队名称
        if (isset($params['team_name']) && !empty($params['team_name']))
        {
            $q->andWhere(['or',
                ['steam1.name' => $params['team_name']],
                ['steam2.name' => $params['team_name']],
                ['team1.name' => $params['team_name']],
                ['team2.name' => $params['team_name']],
                ['like','pdsmre.team_name_all', $params['team_name']]
            ]);
        }

        if (isset($params['status']) && !empty($params['status']))  //比赛状态搜索
        {
            $q->andWhere(['=','sm.status',$params['status']]);
        }

        if (isset($params['els_id']) && !empty($params['els_id']))
        {
            $q->andWhere(['or',
                ['els_m.id' => $params['els_id']],
                ['pdsmre.master_id_all' => $params['els_id']]
            ]);
        }
        //创建方式筛选
        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != '人工' && $params['change_type'] != '全部'){
            $q->andWhere(['or',
                ['pdsmre.change_type' => $params['change_type']],
                ['ori.desc' => $params['change_type']]
            ]);
        }
        if($params['change_type'] == '人工'){
            $q->andWhere(['or',
                ['=','pdsmre.change_type','人工'],
//                ['is','ori.desc' ,null]
            ]);
        }
        $total = $q->count('"1"');
//        $total=1000;
//        return $total;
        $page=(isset($params['page'])&&$params['page'])?$params['page']:1;
        $perPage=(isset($params['per_page'])&&$params['per_page'])?$params['per_page']:10;

        $q->orderBy('sm.scheduled_begin_at desc,sm.deal_status asc,sm.modified_at desc')->limit($perPage)
            ->offset($perPage*($page-1));
        $sql=$q->createCommand()->getRawSql();
            $list=$q->asArray()->all();
        $games = array_column(EnumService::getGameLogo(),'logo','id');
        $resourceIds=array_column($list,'id');
        $operationInfo=OperationLogService::operationCounts(Consts::RESOURCE_TYPE_BING_MATCH,$resourceIds);

        foreach ($list as &$val)
        {
            $val['operation']=@$operationInfo[$val['id']];
            $val['game_logo']=@$games[$val['game_id']];
            $val['game_rules']=self::getGameRulesNamesbyId($val['game_rules']);
            $val["binding_info"]=self::getBindingInfo($val['id']);
//            $val["country_name"]=$country[$val['nationality']] ?? '';
//            $val["els_country_name"]=$country[$val['els_player_country']] ?? '';
            $bindingExpectInfo = self::getBindingExceptInfo($val['id']);
            if ($bindingExpectInfo){
                foreach ($bindingExpectInfo as $k => $v){
                    $tournament_id_els =  $v['tournament_id'];
                    $bindingExpectInfo[$k]['tournament_name'] = Tournament::find()->select(['name'])->where(['id'=>$tournament_id_els])->asArray()->one()['name'];
                    $team_1_id_els = $v['team_1_id'];
                    $team_2_id_els = $v['team_2_id'];
                    //获取队伍的数据源信息
                    $team_1_info_els = Team::find()->select(['name'])->where(['id'=>$team_1_id_els])->asArray()->one()['name'];
                    $team_2_info_els = Team::find()->select(['name'])->where(['id'=>$team_2_id_els])->asArray()->one()['name'];
                    $bindingExpectInfo[$k]['team_1_name'] = $team_1_info_els;
                    $bindingExpectInfo[$k]['team_2_name'] = $team_2_info_els;
                }
            }
            $val["binding_expect_info"]=$bindingExpectInfo;
            $val['game_logo'] = ImageConversionHelper::showFixedSizeConversion($val['game_logo'],66,66,'ali');
        }
        return [
            'total'=>$total,
            'list'=>$list
        ];
    }

    public static function getBindingInfo($standardId)
    {
        $q=(new Query())
            ->select('match.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('match','rel.master_id=match.id')
            ->andWhere(['standard_id'=>$standardId,'resource_type'=>Consts::RESOURCE_TYPE_MATCH]);
        return $q->one();
    }

    public static function getGameRulesNamesbyId($id) {
        if (empty($id)){
            return '';
        }
        $names = EnumGameRules::find()->where(['id'=>$id])->asArray()->one()['name'];
        return $names;

    }

    public static function getBindingExceptInfo($standardId)
    {
        $q=(new Query())
            ->select('match.*')
            ->from('data_standard_master_relation_expect as rel')
            ->leftJoin('match','rel.master_id=match.id')
            ->andWhere(['standard_id'=>$standardId,'resource_type'=>Consts::RESOURCE_TYPE_MATCH])
            ->limit(10);
        $player = $q->all();

        return $player;
    }
    public static function getDetail($standardId)
    {
        $info=StandardDataMatch::find()->where(['id'=>$standardId])->asArray()->one();
        if ($info){
            $origin_id = $info['origin_id'];
            $team_1_id = $info['team_1_id'];
            $team_2_id = $info['team_2_id'];
            //获取队伍的数据源信息
            $team_1_info = StandardDataTeam::find()->where(['rel_identity_id'=>$team_1_id])->andWhere(['origin_id'=>$origin_id])->asArray()->one();
            $team_2_info = StandardDataTeam::find()->where(['rel_identity_id'=>$team_2_id])->andWhere(['origin_id'=>$origin_id])->asArray()->one();
            $info['team_1_info'] = $team_1_info;
            $info['team_2_info'] = $team_2_info;
            //查询所属赛事
            $tournament_id = $info['tournament_id'];
            $tournament_info = StandardDataTournament::find()->where(['rel_identity_id'=>$tournament_id])->andWhere(['origin_id'=>$origin_id])->asArray()->one();
            $info['tournament_info'] = $tournament_info;
        }
        $bindingInfo = self::getBindingInfo($standardId);
        $bindingExpectInfo = self::getBindingExceptInfo($standardId);
        if ($bindingExpectInfo){
            foreach ($bindingExpectInfo as $key => $val){
                $tournament_id_els =  $val['tournament_id'];
                $bindingExpectInfo[$key]['tournament_info'] = Tournament::find()->where(['id'=>$tournament_id_els])->asArray()->one();
                $team_1_id_els = $val['team_1_id'];
                $team_2_id_els = $val['team_2_id'];
                //获取队伍的数据源信息
                $team_1_info_els = Team::find()->where(['id'=>$team_1_id_els])->asArray()->one();
                $team_2_info_els = Team::find()->where(['id'=>$team_2_id_els])->asArray()->one();
                $bindingExpectInfo[$key]['team_1_info_els'] = $team_1_info_els;
                $bindingExpectInfo[$key]['team_2_info_els'] = $team_2_info_els;
            }
        }
        $data['base']=$info;
        $data["binding_info"]=$bindingInfo;
        $data["binding_expect_info"]=$bindingExpectInfo;
        return $data;
    }

    public static function setBinding($standardId,$masterId,$userType=Consts::USER_TYPE_ADMIN,$userId=0,$basisId=0,$log=true)
    {
        $match = Match::find()->where(['id'=>$masterId])->asArray()->one();
        if (empty($match)) {
            throw new BusinessException([],'没有查询到比赛Id:'.$masterId);
        }
        $logOldmatch = StandardDataMatch::find()->where(['id'=>$standardId])->asArray()->one();
        // 已经绑定得不能绑定
        $info = DataStandardMasterRelation::find()
            ->alias('dmr')
            ->select('standard_data_match.*')
            ->leftJoin('standard_data_match','standard_data_match.id = dmr.standard_id')
            ->where(
                [
                    'dmr.resource_type' => Consts::RESOURCE_TYPE_MATCH,
                    'dmr.master_id' => $masterId,
                    'standard_data_match.origin_id' =>$logOldmatch['origin_id']
                ])->asArray()->one();
        if (!empty($info) && $info)
        {
            throw new BusinessException([],"已经绑定过了 id:".$info['rel_identity_id']);
        }

        if ($match['game'] != $logOldmatch['game_id'])
        {
            throw new BusinessException([],'游戏项目不同');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 更新绑定关系
            $operationBindingInfo=[
                'resource_type'=>Consts::RESOURCE_TYPE_MATCH,
                'standard_id'=>$standardId,
                'master_id'=>$masterId
            ];
            $op = DataStandardMasterRelation::find()->where(
                ['resource_type' => Consts::RESOURCE_TYPE_MATCH, 'standard_id' => $standardId])->one();
            if (empty($op) || !$op)
            {
                $op = new DataStandardMasterRelation();
            }
            $op->setAttributes($operationBindingInfo);
            $op->save();
            // 更新操作状态
            $stand=StandardDataMatch::find()->where(['id'=>$standardId])->one();
            $stand->setAttribute('deal_status',Consts::BINGING_STATUS_WISH_DONE);
            $stand->save();
            // 删除预期绑定
            $delSql='delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
            $cmd=\Yii::$app->getDb()
                ->createCommand($delSql)
                ->bindParam(':standard_id',$standardId)
                ->bindParam(':resource_type',$resourceType)
                ->execute();


            $diffInfo = Common::getDiffInfo($logOldmatch, $stand->toArray(), []);
            if ($diffInfo['changed']) {
                OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                    Consts::RESOURCE_TYPE_BING_MATCH,
                    $standardId,
                    ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                    0,
                    Consts::TAG_TYPE_BINDING_DATA,
                    null,
                    $basisId,
                    $userType,
                    $userId
                );
            }
            //判断如果是自动
            $match_UpdateConfig = UpdateConfigService::getResourceUpdateConfig($match["id"],'match');
            //获取默认比赛 配置的 抓取状态 是否是 自动
            if (isset($match_UpdateConfig['score_data']['update_type']) && $match_UpdateConfig['score_data']['update_type'] == 3){
                $auto_origin_id = $match_UpdateConfig['score_data']['origin_id'];
                if ($auto_origin_id == $logOldmatch['origin_id']){
                    //更新hot表
                    $upData = [
                        'standard_id'=>$standardId,
                        'rel_identity_id'=>$logOldmatch['rel_identity_id'],
                    ];
                    MatchService::updateHotDataRunningMatchInfo($masterId,$upData);
                }
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function addAndBinding($id,$userType=Consts::USER_TYPE_ADMIN,$userId=0)
    {
        // standard的player信息
        $infoFromStandard=StandardDataMatch::find()->where(['id'=>$id])->asArray()->one();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 账号属性分类
            $tagShowInfo=UpdateConfig::toTagInfoShow(Consts::RESOURCE_TYPE_MATCH,$infoFromStandard);

            // 账号保存
            $matchInfo=MatchService::setMatch($tagShowInfo,$userType,$userId,$id,$infoFromStandard['origin_id']);
            // 添加绑定状态
            $matchId=$matchInfo["match_id"];
            self::setBinding($id,$matchId,$userType,$userId,0,false);
            $team1Id=$matchInfo["core_data"]["team_1_id"];
            $team2Id=$matchInfo["core_data"]["team_2_id"];
            $tournamentId=$matchInfo["core_data"]["tournament_id"];

            $ids=[];
            if($team1Id){
                $ids[]=$team1Id;
            }
            if($team2Id){
                $ids[]=$team2Id;
            }
//            TournamentService::updateTeamSnapshot(implode(',',$ids),2,$tournamentId,'tournament');
            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
//        //单图弃权
//        if ($infoFromStandard['battle_forfeit'] && isset($infoFromStandard['battle_forfeit'])) {
////                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
//            $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//            if (!empty($infoFromStandard['battle_forfeit'])) {
//                $BaorderArr = json_decode($infoFromStandard['battle_forfeit'], true);
//                foreach ($BaorderArr as $BkK => $Bvv) {
//                    if ($Bvv['rel_team_id'] == $infoFromStandard['team_1_id']) {
//                        $battleWinnerid = $infoFromStandard['team_2_id'];
//                    }
//                    if ($Bvv['rel_team_id'] == $infoFromStandard['team_2_id']) {
//                        $battleWinnerid = $infoFromStandard['team_1_id'];
//                    }
//                    $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $battleWinnerid, $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//
//                    $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//                    if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
//                        if ($Bvv['battle_order'] == 1 && !empty($infoFromStandard['default_advantage'])) {
//                            continue;
//                        }
//                        if (empty($bWinnerid)) {
//                            continue;
//                        }
//
//                        $battleType = 'forfeit';//'advantage' 或者 forfeit
//                        $dealType = 'create';//create 或者 delete
////                            $originId = 3;//数据源ID
////                            $matchId = 789;//比赛ID
//                        $battleOrder = $Bvv['battle_order'];// 具体orderID 或者 传空 是新建
//                        $battleId = '';//如果传值就按照battleID 不按照battle order
//                        $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
//                        $winnerId = $bWinnerid;//4317  1
//                        $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
//                        $rel_battleId = 1;//数据源对局ID
//                        HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//                    }
//                }
//            }
//        }
//        //默认领先
//        if ($infoFromStandard['default_advantage'] && isset($infoFromStandard['default_advantage'])) {
//            $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//            $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $infoFromStandard['default_advantage'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//
//            //判断更新配置 是不是自动更新
//            $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//            if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
//                $battleType = 'advantage';//'advantage' 或者 forfeit
//                $dealType = 'create';//create 或者 delete
////                    $originId = 3;//数据源ID
////                    $matchId = 789;//比赛ID
//                $battleOrder = '1';// 具体orderID 或者 传空 是新建
//                $battleId = '';//如果传值就按照battleID 不按照battle order
//                $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
//                $winnerId = $bWinnerid;//4317  1
//                $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
//                $rel_battleId = 1;//数据源对局ID
//                HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//            }
//        }
//
//        if ($infoFromStandard['is_forfeit'] && isset($infoFromStandard['is_forfeit'])) {
//            $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//            $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $infoFromStandard['winner'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
//
//            //判断更新配置 是不是自动更新
//            $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//            if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
//                $battleType = 'forfeit';//'advantage' 或者 forfeit
//                $dealType = 'create';//create 或者 delete
////                    $originId = 3;//数据源ID
////                    $matchId = 789;//比赛ID
//                $battleOrder = '1';// 具体orderID 或者 传空 是新建
//                $battleId = '';//如果传值就按照battleID 不按照battle order
//                $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
//                $winnerId = $bWinnerid;//4317  1
//                $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
//                $rel_battleId = 1;//数据源对局ID
//                HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//            }
//        }

    }

    /**
     * @param $relIdentityId(比赛id)
     * @param $resourceType(资源类型)
     * @param $originId(来源id)
     * @param $gameId(游戏id)
     * @param $reason(刷新方式)
     */
    public static function getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId,$reason="")
    {
        $hotLog = TaskDataHotInfo::find()->where([
            'origin_id' => $originId,
            'resource_type' => $resourceType,
            'game_id' => $gameId,
            'identity_id' => $relIdentityId,
        ])->one();
        if($hotLog){
            $hotLog->setAttribute('json_info',null);
            $hotLog->save();
//           $hotLog->delete();
        }

        $result = RefreshResource::refreshByResourceId($relIdentityId, $resourceType, $originId, $gameId, $extType = "refresh");
        $response = json_decode($result['response'],true);
        if($originId == 7){
            if($response['code'] == 200 || $response['code'] == 500){
                return $response['msg'];
            }else{
                throw new BusinessException($response,$response['msg']);
            }
        }else{
            if($response){
                return $response;
            }else{
                throw new BusinessException($response,"没有该数据项,请检查参数");
            }
        }
    }
    public static function resetStandard($relIdentityId,$resourceType,$originId,$gameId)
    {
        $standardDataActiveTable = StandardDataService::getStandardActiveTable($resourceType);
        if($resourceType == Consts::METADATA_TYPE_LOL_CHAMPION || $resourceType == Consts::METADATA_TYPE_LOL_ITEM
            || $resourceType == Consts::METADATA_TYPE_LOL_SUMMONER_SPELL || $resourceType == Consts::METADATA_TYPE_LOL_RUNE
            ||$resourceType == Consts::METADATA_TYPE_LOL_ABILITY || $resourceType == Consts::METADATA_TYPE_DOTA2_TALENT
            ||$resourceType == Consts::METADATA_TYPE_DOTA2_ABILITY || $resourceType == Consts::METADATA_TYPE_DOTA2_ITEM
            ||$resourceType == Consts::METADATA_TYPE_DOTA2_HERO || $resourceType == Consts::METADATA_TYPE_CSGO_WEAPON
            ||$resourceType == Consts::METADATA_TYPE_CSGO_MAP){
            $standardInfo = $standardDataActiveTable::find()->where([
                'origin_id' => $originId,
                'rel_identity_id' => $relIdentityId,
                'game_id' => $gameId,
                'metadata_type' => $resourceType,
            ])->one();
            if(!$standardInfo){
                throw new BusinessException([],'未查询到该数据，请检查参数');
            }
            //stand metadata表
            $metaDataAttribute = [
                'belong'=> null,
                'name'=> null,
                'name_cn'=> null,
                'map_type_cn'=> null,
                'map_type'=> null,
                'thumbnail'=> null,
                'rectangle_image'=> null,
                'square_image'=> null,
                'is_default'=> null,
                'short_name'=> null,
                'hotkey'=> null,
                'image'=> null,
                'small_image'=> null,
                'description'=> null,
                'description_cn'=> null,
                'slug'=> null,
                'state'=> 1,
                'title'=> null,
                'title_cn'=> null,
                'primary_role'=> null,
                'primary_role_cn'=> null,
                'secondary_role'=> null,
                'secondary_role_cn'=> null,
                'external_id'=> null,
                'external_name'=> null,
                'total_cost'=> null,
                'is_trinket'=> null,
                'is_purchasable'=> null,
                'path_name'=> null,
                'path_name_cn'=> null,
                'ammunition'=> null,
                'capacity'=> null,
                'cost'=> null,
                'kill_award'=> null,
                'kind'=> null,
                'is_recipe'=> null,
                'is_secret_shop'=> null,
                'is_home_shop'=> null,
                'is_neutral_drop'=> null,
                'primary_attr'=> null,
                'primary_attr_cn'=> null,
                'roles_cn'=> null,
                'roles'=> null,
                'data_source'=> null,
                'firing_mode'=> null,
                'is_t_available'=> null,
                'is_ct_available'=> null,
                'reload_time'=> null,
                'movement_speed'=> null,
                'left_right'=> null,
                'level'=> null,
                'talent_cn'=> null,
                'talent'=> null,
                'talents'=> null,
                'champion_id'=> null,
                'hero_id'=> null,
                'deleted'=> 2,
                'deleted_at'=> null,
            ];
            $standardInfo->setAttributes($metaDataAttribute);
            $standardInfo->save();
        }elseif ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            $standardInfo = $standardDataActiveTable::find()->where([
                'origin_id' => $originId,
                'rel_identity_id' => $relIdentityId,
                'game_id' => $gameId,
                'type' => 1,
            ])->one();

        }else {
            $standardInfo = $standardDataActiveTable::find()->where([
                'origin_id' => $originId,
                'rel_identity_id' => $relIdentityId,
                'game_id' => $gameId,
            ])->one();
            if (!$standardInfo) {
                throw new BusinessException([], '未查询到该数据，请检查参数'); //批量处理删除这个
            }
        }
        if($resourceType == Consts::RESOURCE_TYPE_TEAM){
            //stand team表
            $teamAttribute = [
                'clan_id'=> null,
                'country'=> null,
                'name'=> null,
                'short_name'=> null,
                'full_name'=> null,
                'alias'=> null,
                'logo'=> null,
                'cuser'=> null,
                'players'=> null,
                'history_players'=> null,
                'slug'=> null,
                'region'=> null,
                'world_ranking'=> null,
                'ago_30'=> null,
                'total_earnings'=> null,
                'average_player_age'=> null,
                'create_team_date'=> null,
                'close_team_date'=> null,
                'introduction'=> null,
                'introduction_cn'=> null,
                'clan_name'=> null,
                'clan_full_name'=> null,
                'clan_short_name'=> null,
                'clan_slug'=> null,
                'clan_alias'=> null,
                'clan_image'=> null,
                'clan_cuser'=> null,
                'seo_url'=> null,
                'deleted'=> 2,
                'deleted_at'=> null,
            ];
            $standardInfo->setAttributes($teamAttribute);
            $standardInfo->save();
        }
        if($resourceType == Consts::RESOURCE_TYPE_PLAYER){
            //stand player表
            $playAttribute = [
                'nick_name'=> null,
                'birthday'=> null,
                'nationality'=> null,
                'name'=> null,
                'name_cn'=> null,
                'image'=> null,
                'position_id'=> null,
                'introduction'=> null,
                'introduction_cn'=> null,
                'deleted'=> 2,
                'cuser'=> null,
                'slug'=> null,
                'role'=> null,
                'team'=> null,
                'status_id'=> null,
                'steam_id'=> null,
                'deleted_at'=> null,
            ];
            $standardInfo->setAttributes($playAttribute);
            $standardInfo->save();
        }
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            //stand tournament表
            $tournamentAttribute = [
                'rel_league_id'=> null,
                'name'=> null,
                'name_cn'=> null,
                'slug'=> null,
                'status'=> null,
                'scheduled_begin_at'=> null,
                'scheduled_end_at'=> null,
                'short_name'=> null,
                'short_name_cn'=> null,
                'number_of_teams'=> null,
                'tier'=> null,
                'son_sort'=> null,
                'address'=> null,
                'address_cn'=> null,
                'country'=> null,
                'region'=> null,
                'organizer'=> null,
                'organizer_cn'=> null,
                'version'=> null,
                'introduction'=> null,
                'introduction_cn'=> null,
                'prize_bonus'=> null,
                'prize_points'=> null,
                'prize_seed'=> null,
                'map_pool'=> null,
                'format'=> null,
                'format_cn'=> null,
                'teams_condition'=> null,
                'teams'=> null,
                'image'=> null,
                'parent_id'=> null,
                'type'=> 1,
                'prize_distribution'=> null,
                'deleted'=> 2,
                'deleted_at'=> null,
            ];
            $standardInfo->setAttributes($tournamentAttribute);
            $standardInfo->save();
        }
        if($resourceType == Consts::RESOURCE_TYPE_MATCH){
            //stand match表
            $matchAttribute = [
                'tournament_id'=> null,
                'match_rule_id'=> null,
                'son_tournament_id'=> null,
                'stage_id'=> null,
                'group_id'=> null,
                'team_1_id'=> null,
                'team_2_id'=> null,
                'team_1_score'=> null,
                'team_2_score'=> null,
                'winner'=> null,
                'game_rules'=> null,
                'match_type'=> null,
                'number_of_games'=> null,
                'original_scheduled_begin_at'=> null,
                'scheduled_begin_at'=> null,
                'slug'=> null,
                'is_rescheduled'=> null,
                'relation'=> null,
                'name'=> null,
                'draw'=> null,
                'forfeit'=> null,
                'end_at'=> null,
                'begin_at'=> null,
                'status'=> null,
                'deleted'=> 2,
                'cuser'=> null,
                'is_battle_detailed'=> null,
                'is_pbpdata_supported'=> null,
                'live_url'=> null,
                'embed_url'=> null,
                'official_stream_url'=> null,
                'default_advantage'=> null,
                'description'=> null,
                'location'=> null,
                'location_cn'=> null,
                'round_order'=> null,
                'round_name'=> null,
                'round_name_cn'=> null,
                'bracket_pos'=> null,
                'description_cn'=> null,
                'auto_status'=> null,
                'map_info'=> null,
                'map_ban_pick'=> null,
                'map_vetoes'=> null,
                'is_streams_supported'=> 2,
                'unbound_identity_data'=> null,
                'missing_field_for_binding'=> null,
                'battle_forfeit'=> null,
                'has_incident_report'=> 2,
                'deleted_at'=> null,
            ];
            $standardInfo->setAttributes($matchAttribute);
            $standardInfo->save();
        }
        return self::getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId,"refresh");
    }
}