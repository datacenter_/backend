<?php
/**
 *
 */

namespace app\modules\data\services\binding;


use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DataStandardMasterRelationExpect;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\UpdateConfigService;
use app\modules\metadata\services\MetadataService;
use app\modules\operation\models\OperationStandardMasterRelation;
use app\modules\org\services\PlayerService;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use Swoft\Validator\Annotation\Mapping\Enum;
use yii\db\ActiveRecord;
use yii\db\Query;

class BindingMetadataService
{
    public static function getList($params)
    {
        $metadataType = $params['metadata_type'] ? $params['metadata_type'] : Consts::METADATA_TYPE_LOL_CHAMPION;
        $masterTableName = 'metadata_'.$metadataType;
        $q = StandardDataMetadata::find()
            ->select(['m.*',
                'dcc.origin_id as dcc_origin_id','if(pdsmr.id,ifnull(ori.desc,"人工"),if(pdsmr2.id,ifnull(ori.desc,"人工"),null)) as change_type',

                'if(rel_lol_cha.name_cn,rel_lol_cha.name_cn,rel_lol_cha_exp.name_cn) as champion_name_cn',
//                'rel_lol_cha_exp.name_cn as exp_champion_name_cn',
                'if(rel_lol_cha.title_cn,rel_lol_cha.title_cn,rel_lol_cha_exp.title_cn) as champion_title_cn',
//                'rel_lol_cha_exp.title_cn as exp_champion_title_cn',
                'if(rel_dota2_cha.name_cn,rel_dota2_cha.name_cn,rel_dota2_cha_exp.name_cn) as hero_name_cn',
//                'rel_dota2_cha_exp.name_cn as exp_hero_name_cn',
                'if(rel_dota2_cha.title_cn,rel_dota2_cha.title_cn,rel_dota2_cha_exp.title_cn) as hero_title_cn',
//                'rel_dota2_cha_exp.title_cn as exp_hero_title_cn',
                'if(m.deal_status=4,'.$masterTableName.'.state,typesa.state) as show_state',
                $masterTableName.'.state as master_state',
                $masterTableName.'.id as master_id',
                'typesa.state as exp_state','typesa.id as exp_id',
            ])
            ->alias('m')
            ->leftJoin('data_standard_master_relation as pdsmr','pdsmr.standard_id = m.id and pdsmr.resource_type = m.metadata_type')
            ->leftJoin("$masterTableName",$masterTableName.".id = pdsmr.master_id")
            ->leftJoin('data_change_config as dcc',"dcc.resource_id = pdsmr.master_id and dcc.resource_type = '$metadataType'")
            ->leftJoin('data_standard_master_relation_expect as pdsmr2','pdsmr2.standard_id = m.id and pdsmr2.resource_type = m.metadata_type')
            ->leftJoin("$masterTableName as typesa","typesa.id = pdsmr2.master_id")
            ->leftJoin('data_change_config as dcc_expect',"dcc_expect.resource_id = pdsmr2.master_id and dcc_expect.resource_type = '$metadataType'")
            ->leftJoin("enum_origin as ori_expect","dcc_expect.origin_id = ori_expect.id")
            ->leftJoin("metadata_lol_champion rel_lol_cha",$masterTableName=="metadata_lol_ability"?"rel_lol_cha.id=$masterTableName.champion_id":0)
            ->leftJoin("metadata_lol_champion rel_lol_cha_exp",$masterTableName=="metadata_lol_ability"?"rel_lol_cha_exp.id=typesa.champion_id":0)
            ->leftJoin("metadata_dota2_hero rel_dota2_cha",$masterTableName=="metadata_dota2_ability"?"rel_dota2_cha.id=$masterTableName.hero_id":0)
            ->leftJoin("metadata_dota2_hero rel_dota2_cha_exp",$masterTableName=="metadata_dota2_ability"?"rel_dota2_cha_exp.id=typesa.hero_id":0)
            ->leftJoin("enum_origin as ori","dcc.origin_id = ori.id");
        $whereConfig=[
            "origin"=>[
                'type'=>'=',
                'key'=>'m.origin_id'
            ],
            "deal_status"=>[
                'type'=>'=',
                'key'=>'m.deal_status'
            ],
//            "state"=>[
//                'type'=>"=",
//                'key'=>"state"
//            ],
//            "external_id"=>[
//                'type'=>"=",
//                'key'=>'m.external_id'
//            ],
//            "id"=>[
//                'type'=>"=",
//                'key'=>'pdsmr.master_id'
//            ],
            "rel_identity_id"=>[
                'type'=>"=",
                'key'=>'m.rel_identity_id'
            ],
//            "date_begin"=>[
//                'type'=>">=",
//                'key'=>'m.modified_at'
//            ],
//            "date_end"=> [
//                'type'=>"<=",
//                'key'=>'m.modified_at'
//            ]
        ];

        $whereArray = DbHelper::getWhere($whereConfig, $params);
        $where = array_merge(['and'], $whereArray);
        $q->where($where);
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $q->andWhere(['>=', 'm.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $q->andWhere(['<', 'm.modified_at', $end]);
        }
//        $q->andWhere(['pdsmr.resource_type' => $metadataType]);
        $q->andWhere(['m.metadata_type' => $metadataType]);
        if (isset($params['title']) && !empty($params['title'])) {
            if ($metadataType == Consts::METADATA_TYPE_DOTA2_TALENT)
            {
                $q->andWhere(['or',
//                    ['like','m.title',$params['title']],
//                    ['like','m.title_cn',$params['title']],
                    ['like',"$masterTableName.talent",$params['title']],
                    ['like',"$masterTableName.talent_cn",$params['title']],
                    ['like',"typesa.talent",$params['title']],
                    ['like',"typesa.talent_cn",$params['title']],
                    ['like',"m.name",$params['title']],
                    ['like',"m.name_cn",$params['title']],
                ]);
            }else{
                $q->andWhere(['or',
                    ['like','m.title',$params['title']],
                    ['like','m.title_cn',$params['title']],
                    ['like',"$masterTableName.title",$params['title']],
                    ['like',"$masterTableName.title_cn",$params['title']],
                    ['like',"typesa.title",$params['title']],
                    ['like',"typesa.title_cn",$params['title']],
                ]);
            }

        }
        if (isset($params['name']) && !empty($params['name'])) {
            if ($metadataType == Consts::METADATA_TYPE_CSGO_WEAPON){
                $q->andWhere(['or',
                    ['like','m.name',$params['name']],
                    ['like','m.name_cn',$params['name']],
                    ['like',"$masterTableName.name",$params['name']],
                    ['like','typesa.name',$params['name']],
                ]);
            }else{
                $q->andWhere(['or',
                    ['like','m.name',$params['name']],
                    ['like','m.name_cn',$params['name']],
                    ['like',"$masterTableName.name",$params['name']],
                    ['like',"$masterTableName.name_cn",$params['name']],
                    ['like','typesa.name',$params['name']],
                    ['like','typesa.name_cn',$params['name']],
                ]);
            }
        }
        if(isset($params['external_id']) && !empty($params['external_id'])) {
//            if ($metadataType == (Consts::METADATA_TYPE_LOL_CHAMPION || Consts::METADATA_TYPE_LOL_ITEM || Consts::METADATA_TYPE_LOL_RUNE)) {
                $q->andWhere(['or',
                    ['=', "$masterTableName.external_id", $params['external_id']],
                    ['=', 'm.external_id', $params['external_id']],
                    ['=', 'typesa.external_id', $params['external_id']],
                ]);
//            } else {
//                $q->andWhere(['m.external_id' => $params['external_id']]);
//            }
        }

        // slug
        if (isset($params['slug']) && !empty($params['slug'])) {
            $q->andWhere(['or',
                ['like','m.slug',$params['slug']],
                ['like',"$masterTableName.slug",$params['slug']],
                ['like',"typesa.slug",$params['slug']],
            ]);
        }


        if (isset($params['state']) && !empty($params['state'])) {
            $q->andWhere(['or',
                ['=',"$masterTableName.state",$params['state']],
                ['=',"typesa.state",$params['state']],
            ]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $q->andWhere(['or',
                ['=','pdsmr.master_id',$params['id']],
                ['=',"pdsmr2.master_id",$params['id']]
            ]);
        }
        if (isset($params['external_name']) && !empty($params['external_name'])) {
            $q->andWhere(['or',
                ['like',"m.external_name",$params['external_name']],
                ['like',"$masterTableName.external_name",$params['external_name']],
                ['like',"typesa.external_name",$params['external_name']]
            ]);
        }

        // 查询技能所属英雄的lol
        if ($params['metadata_type'] == Consts::METADATA_TYPE_LOL_ABILITY && isset($params['champion_title']) && !empty($params['champion_title'])) {
            $q->andWhere(['or',
                ['=','rel_lol_cha.title',$params['champion_title']],
                ['=','rel_lol_cha.title_cn',$params['champion_title']],
                ['=','rel_lol_cha_exp.title',$params['champion_title']],
                ['=','rel_lol_cha_exp.title_cn',$params['champion_title']],
            ]);
        }

        if ($params['metadata_type'] == Consts::METADATA_TYPE_LOL_ABILITY &&isset($params['champion_name']) && !empty($params['champion_name'])) {
            $q->andWhere(['or',
                ['=','rel_lol_cha.name',$params['champion_name']],
                ['=','rel_lol_cha.name_cn',$params['champion_name']],
                ['=','rel_lol_cha_exp.name',$params['champion_name']],
                ['=','rel_lol_cha_exp.name_cn',$params['champion_name']],
            ]);
        }

        // 查询技能所属英雄的dota2
        if (isset($params['hero_title']) && !empty($params['hero_title'])) {
            $q->andWhere(['or',
                ['like',"rel_dota2_cha.title_cn",$params['hero_title']],
                ['like',"rel_dota2_cha_exp.title_cn",$params['hero_title']],
                ['like',"rel_dota2_cha.title",$params['hero_title']],
                ['like',"rel_dota2_cha_exp.title",$params['hero_title']],
            ]);
        }

        if (isset($params['hero_name']) && !empty($params['hero_name'])) {
            $q->andWhere(['or',
                ['like',"rel_dota2_cha.name_cn",$params['hero_name']],
                ['like',"rel_dota2_cha_exp.name_cn",$params['hero_name']],
                ['like',"rel_dota2_cha.name",$params['hero_name']],
                ['like',"rel_dota2_cha_exp.name",$params['hero_name']],
            ]);
        }
        //创建方式筛选
//        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != '人工' && $params['change_type'] != '全部'){
//            $q->andWhere(
//                ['change_type' => $params['change_type']]
//            );
//        }
//        if($params['change_type'] == '人工'){
//            $q->andWhere(['and',
//                ['is','ori.desc',null],
//                ['>',$masterTableName.'.id',0]
//            ]);
//        }
        $q->groupBy("m.id");
        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != '全部'){
            $q->having(['change_type'=>$params['change_type']]);
        }

        $sql=$q->createCommand()->getRawSql();
//        print_r($sql);
        $total = $q->count();
        $page = (isset($params['page']) && $params['page']) ? $params['page'] : 1;
        $perPage = (isset($params['per_page']) && $params['per_page']) ? $params['per_page'] : 10;

        $list = $q->limit($perPage)->offset($perPage * ($page - 1))->orderBy('m.deal_status asc,m.modified_at desc')->asArray()->all();
        $games = array_column(EnumService::getGameLogo(),'logo','id');
        $resourceIds=array_column($list,'id');
        $BingMetadataType = 'bind_' . $metadataType;
        $operationInfo=OperationLogService::operationCounts($BingMetadataType,$resourceIds);
        foreach ($list as $key => &$val) {

            $val["binding_info"] = self::getBindingInfo($val['id'], $metadataType);
            $val["binding_expect_info"] = self::getBindingExceptInfo($val['id'], $metadataType);
            $val['operation']=$operationInfo[$val['id']];
            $val['game_logo']=$games[$val['game_id']] ?? '';
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'],66,66,'ali');

            if ($metadataType == "lol_champion" ) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {
                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['image'],54,54,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['image'],54,54,'ali');
                }

            }

            if ($metadataType == Consts::METADATA_TYPE_DOTA2_HERO) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {
                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['image'],46,76,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['image'],46,76,'ali');
                }

            }

            if ($metadataType ==  Consts::METADATA_TYPE_LOL_ABILITY || $metadataType ==  Consts::METADATA_TYPE_LOL_ITEM
                || $metadataType ==  Consts::METADATA_TYPE_LOL_RUNE || $metadataType ==  Consts::METADATA_TYPE_LOL_SUMMONER_SPELL
                    || $metadataType ==  Consts::METADATA_TYPE_DOTA2_ABILITY

            ) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {
                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['image'],32,32,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['image'],32,32,'ali');
                }
            }

            if ($metadataType ==  Consts::METADATA_TYPE_DOTA2_ITEM) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {
                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['image'],28,38,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['image'],28,38,'ali');
                }
            }

            if ($metadataType == Consts::METADATA_TYPE_CSGO_MAP) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {
                        $val['binding_expect_info'][$lolCEkey]['square_image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['square_image'],42,84,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['square_image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['square_image'],42,84,'ali');
                }

            }

            if ($metadataType == Consts::METADATA_TYPE_CSGO_WEAPON) {
                if (!empty($val['binding_expect_info'])) {
                    foreach ($val['binding_expect_info'] as $lolCEkey => $lolCEval) {

//                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showFixedSizeConversion($lolCEval['image'],28,0,'ali');
                        $val['binding_expect_info'][$lolCEkey]['image']= ImageConversionHelper::showMfitSizeConversion($lolCEval['image'],28,0,'ali');
                    }
                }
                if (!empty($val['binding_info'])) {
                    $val['binding_info']['image'] = ImageConversionHelper::showMfitSizeConversion($val['binding_info']['image'],28,0,'ali');
                }

            }




//            if (empty($val['dcc_origin_id']))
//            {
//                $val['change_type'] = '人工';
//            }else{
//                switch ($val['dcc_origin_id'])
//                {
//                    case 1 : $val['change_type'] = '飞鲸';break;
//                    case 2 : $val['change_type'] = 'ABIOS';break;
//                    case 3 : $val['change_type'] = 'pandascore';break;
//                }
//            }
        }
        return [
            'total' => $total,
            'list' => $list
        ];
    }

    public static function getBindingInfo($standardId, $metadataType)
    {
        // 这里做整合了，获取详情时，先获取绑定的ids，然后去对应的资源获取列表
        $bindings = DataStandardMasterRelation::find()->where(['resource_type' => $metadataType, 'standard_id' => $standardId])->one();
        $className = MetadataService::getObjByType($metadataType);
        $classObj = new $className;
        if ($classObj instanceof ActiveRecord) {
            return $classObj::find()->where(['id' => $bindings['master_id']])->asArray()->one();
        }
    }
    public static function getMasterInfo($metadataType,$masterId)
    {
        $className = MetadataService::getObjByType($metadataType);
        return $className::find()->where(['id' => $masterId])->asArray()->one();
    }
    public static function getBindingExceptInfo($standardId, $metadataType)
    {
        $bindings = DataStandardMasterRelationExpect::find()->where(['resource_type' => $metadataType, 'standard_id' => $standardId])
            ->limit(10)->asArray()->all();
        $ids = array_column($bindings, 'master_id');
        $className = MetadataService::METADATA_TYPE_TO_CLASS[$metadataType];
        $classObj = new $className;
        if ($classObj instanceof ActiveRecord) {
            return $classObj::find()->where(['in', 'id', $ids])->asArray()->all();
        }
    }

    public static function getDetail($standardId, $metadataType)
    {
        $info = StandardDataMetadata::find()->where(['id' => $standardId])->asArray()->one();
        $bindingInfo = self::getBindingInfo($standardId, $metadataType);
        $bindingExpectInfo = self::getBindingExceptInfo($standardId, $metadataType);
        $val['base'] = $info;
        $val["binding_info"] = $bindingInfo;
        $val["binding_expect_info"] = $bindingExpectInfo;
        return $val;
    }

    public static function setBinding($resourceType, $standardId, $masterId, $userType = Consts::USER_TYPE_ADMIN, $userId = 0,$basisId = 0,$log = true)
    {
        $metaDataInfo = self::getMasterInfo($resourceType,$masterId);
        if (empty($metaDataInfo)) {
            throw new BusinessException([],'没有查询到Id:'.$masterId);
        }
        $logOldData = StandardDataMetadata::find()->where(['id'=>$standardId])->asArray()->one();
//        $info = DataStandardMasterRelation::find()
//            ->alias('dmr')
//            ->select('standard_data_metadata.*')
//            ->leftJoin('standard_data_metadata','standard_data_metadata.id = dmr.standard_id')
//            ->where(
//                [
//                    'dmr.resource_type' => $resourceType,
//                    'dmr.master_id' => $masterId,
//                    'standard_data_metadata.origin_id' =>$logOldData['origin_id']
//                ])->asArray()->one();
//        if (!empty($info) && $info)
//        {
//            throw new BusinessException([],'已经绑定过了 id:'.$info['rel_identity_id']);
//        }
        if ($resourceType != $logOldData['metadata_type'])
        {
            throw new BusinessException([],'游戏项目不同');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 更新绑定关系
            $operationBindingInfo = [
                'resource_type' => $resourceType,
                'standard_id' => $standardId,
                'master_id' => $masterId,
            ];
            $op = DataStandardMasterRelation::find()->where(
                ['resource_type' => $resourceType, 'standard_id' => $standardId])->one();
            if (empty($op) || !$op)
            {
                $op = new DataStandardMasterRelation();
            }
            $op->setAttributes($operationBindingInfo);
            $op->save();
            // 更新操作状态
            $stand = StandardDataMetadata::find()->where(['id' => $standardId])->one();
            $stand->setAttribute('deal_status', Consts::BINGING_STATUS_WISH_DONE);
            $stand->save();
            // 删除预期绑定
            $delSql = 'delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
            $cmd = \Yii::$app->getDb()
                ->createCommand($delSql)
                ->bindParam(':standard_id', $standardId)
                ->bindParam(':resource_type', $resourceType)
                ->execute();

            $resourceType = 'bind_' . $resourceType;

            $diffInfo = Common::getDiffInfo($logOldData, $stand->toArray(), []);
            if ($diffInfo['changed']) {
                OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                    $resourceType,
                    $standardId,
                    ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                    0,
                    Consts::TAG_TYPE_CORE_DATA,
                    null,
                    $basisId,
                    $userType,
                    $userId
                );
            }
            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    public static function addAndBinding($metadataType, $id, $userType = Consts::USER_TYPE_ADMIN, $userId = 0)
    {
        // standard中的信息
        $infoFromStandard = StandardDataMetadata::find()->where(['id' => $id, 'metadata_type' => $metadataType])->asArray()->one();
        if($infoFromStandard['image']){
            $infoFromStandard['image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['image']);
        }
        if($infoFromStandard['small_image']){
            $infoFromStandard['small_image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['small_image']);
        }
        if(!empty($infoFromStandard['square_image'])){
            $infoFromStandard['square_image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['square_image']);
        }
        if(!empty($infoFromStandard['rectangle_image'])){
            $infoFromStandard['rectangle_image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['rectangle_image']);
        }
        if(!empty($infoFromStandard['thumbnail'])){
            $infoFromStandard['thumbnail'] = ImageConversionHelper::mvImageToAli($infoFromStandard['thumbnail']);
        }

        // 账号保存
        $addInfo=$infoFromStandard;
        unset($addInfo['id']);
        if(isset($addInfo['created_time'])){
            unset($addInfo['created_time']);
        }
        if(isset($addInfo['modified_at'])){
            unset($addInfo['modified_at']);
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {

            // 账号属性分类  1、验证字段属性 进行赋值  2、得到表的字段tag进行分块  3、再把不同的字段存储不同的tag里面
            $tagShowInfo = UpdateConfig::toTagInfoShow($metadataType, $addInfo);

            $info = [];
            if ($metadataType == Consts::METADATA_TYPE_DOTA2_HERO){
                if ($tagShowInfo['core_data']) {
                    $info = $tagShowInfo['core_data'];
                }

                if ($tagShowInfo['base_data']) {
                    $info_base = $tagShowInfo['base_data'];
                    $info = array_merge($info,$info_base);
                }

            }else{
                $info = $tagShowInfo['core_data'];
            }
            $metadataInfo =MetadataService::editInfo($metadataType,$info,$userType,$userId,$infoFromStandard['game_id'],$id,$infoFromStandard['origin_id']);
            // 添加绑定状态

            $metadataId = $metadataInfo["id"];
            self::setBinding($metadataType,$id, $metadataId, $userType, $userId,0,false);

            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * 获取所有元数据类型
     * @return array
     */
    public static function getMetadataTypeList()
    {
        return [
            Consts::METADATA_TYPE_LOL_ABILITY,
            Consts::METADATA_TYPE_LOL_CHAMPION,
            Consts::METADATA_TYPE_LOL_ITEM,
            Consts::METADATA_TYPE_LOL_RUNE,
            Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,
            Consts::METADATA_TYPE_DOTA2_HERO,
            Consts::METADATA_TYPE_DOTA2_ITEM,
            Consts::METADATA_TYPE_DOTA2_ABILITY,
            Consts::METADATA_TYPE_DOTA2_TALENT,
            Consts::METADATA_TYPE_CSGO_MAP,
            Consts::METADATA_TYPE_CSGO_WEAPON,
        ];
    }
}