<?php
/**
 *
 */

namespace app\modules\data\services\binding;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\operation\models\OperationStandardMasterRelation;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\models\abios\TeamPlayerRelation;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class BindingPlayerService
{
    public static function getList($params)
    {
        if (!empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $params['date_end'] = $end;
        }
        $q = StandardDataPlayer::find()
            ->select(['p.*','mp.player_country as els_player_country','dcc.origin_id as dcc_origin_id','if(mp.id,ifnull(ori.desc,"人工"),pdsmre.change_type) as change_type'])
            ->alias('p')
            ->leftJoin('data_standard_master_relation as pdsmr', 'pdsmr.standard_id = p.id and pdsmr.resource_type = "player"')
            ->leftJoin('view_relation_expect_player as pdsmre', 'pdsmre.standard_id = p.id')
            ->leftJoin('player as mp', 'mp.id = pdsmr.master_id')
            ->leftJoin('data_change_config as dcc', 'dcc.resource_id = pdsmr.master_id and dcc.resource_type = "player"')
            ->leftJoin('enum_origin as ori', 'dcc.origin_id = ori.id');
        if (isset($params['rel_rel_identity_id']) && $params['rel_rel_identity_id']) {
            $q->leftJoin('bayes_identifiers as bi','bi.rel_identity_id = p.rel_identity_id and bi.resource_type = "player" and bi.origin_id = '.$params['origin_id']);
        }
        $whereConfig = [
            "origin_id" => [
                'type' => '=',
                'key' => 'p.origin_id'
            ],
            "game_id" => [
                'type' => '=',
                'key' => 'p.game_id'
            ],
            "deal_status" => [
                'type' => '=',
                'key' => 'p.deal_status'
            ],
            "player_country" => function ($params) {
                if (isset($params['player_country'])&&$params['player_country']) {
                    return [
                        'or',
                        ["=", 'mp.player_country', $params['player_country']],
                        ["like", 'pdsmre.player_country_all', $params['player_country']],
                    ];
                }
            },
            "id" => function ($params) {
                if (isset($params['id'])&&$params['id']) {
                    return [
                        'or',
                        ["=", 'pdsmr.master_id', $params['id']],
                        ["like", 'pdsmre.master_id_all', $params['id']],
                    ];
                }
            },
            "rel_identity_id" => [
                'type' => "=",
                'key' => 'p.rel_identity_id'
            ],
            "rel_rel_identity_id" => function ($params) {
                if (isset($params['rel_rel_identity_id']) && $params['rel_rel_identity_id']) {
                    return ["=", 'bi.value', $params['rel_rel_identity_id']];
                }
            },
            "date_begin" => [
                'type' => ">=",
                'key' => 'p.modified_at'
            ],
            "date_end" => [
                'type' => "<",
                'key' => 'p.modified_at'
            ]
        ];
        $whereArray = DbHelper::getWhere($whereConfig, $params);
        $where = array_merge(['and'], $whereArray);
        $q->where($where);
//        $q->andWhere(['pdsmr.resource_type' => 'player']);
        if (!empty($params['nick_name']) && isset($params['nick_name'])) {
            $q->andWhere(['or',
                ['like', 'mp.nick_name', $params['nick_name']],
                ['like', 'pdsmre.nick_name_all', $params['nick_name']],
                ['like', 'p.nick_name', $params['nick_name']],
            ]);
        }
        //创建方式筛选
        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != '人工' && $params['change_type'] != '全部'){
            $q->andWhere(['or',
                ['pdsmre.change_type' => $params['change_type']],
                ['ori.desc' => $params['change_type']]
            ]);
        }
        if($params['change_type'] == '人工'){
            $q->andWhere(['or',
                ['=','pdsmre.change_type','人工'],
//                ['is','ori.desc' ,null]
            ]);
        }
        $total = $q->count();
        $page = (isset($params['page']) && $params['page']) ? $params['page'] : 1;
        $perPage = (isset($params['per_page']) && $params['per_page']) ? $params['per_page'] : 10;
        $q->orderBy('p.deal_status asc,p.modified_at desc')->limit($perPage)->offset($perPage * ($page - 1));
        //$sql=$q->createCommand()->getRawSql();
        $list = $q->asArray()->all();
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');

        $resourceIds = array_column($list, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_BING_PLAYER, $resourceIds);
        foreach ($list as $key => &$val) {
            $val["binding_info"] = self::getTeamBindingInfo($val['id']);
            $val["country_name"] = $country[$val['nationality']] ?? '';
            $val["els_country_name"] = $country[$val['els_player_country']] ?? '';
            $val["binding_expect_info"] = self::getTeamBindingExceptInfo($val['id']);
            $val['operation'] = isset($operationInfo[$val['id']]) ? $operationInfo[$val['id']] : '';
            $val['game_logo'] = $games[$val['game_id']] ?? '';
            if($val['origin_id'] == 9 || $val['origin_id'] == 10){
                $hostUrl = \Yii::$app->request->hostInfo;
                $val['image'] = $hostUrl."/v1/data/image/get-bayes-image?origin_id=".$val['origin_id']."&image_url=".$val['image'];
            }else{
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 66, 66, 'ali');
            }
        }
        return [
            'total' => $total,
            'list' => $list
        ];
    }

    public static function getTeamBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('player.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('player', 'rel.master_id=player.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_PLAYER]);
        $player = $q->one();
        if($player['image']){
            $player['image'] = ConversionCriteria::playerResizeConversion($player['image']);
        }
        return $player;
    }

    public static function getTeamBindingExceptInfo($standardId)
    {
        $q = (new Query())
            ->select(['player.*','player_base.birthday','player_base.introduction','player_base.introduction_cn'])
            ->from('data_standard_master_relation_expect as rel')
            ->leftJoin('player', 'rel.master_id=player.id')
            ->leftJoin('player_base','player_base.player_id = player.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_PLAYER])
            ->limit(10);
        $player = $q->all();

        foreach ($player as &$value) {
            $value['teams'] = self::getStandardTeamPlayerInfo($value['id']);
            $value['image'] = ConversionCriteria::playerResizeConversion($value['image']);
//            $value['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'], 66, 66, 'ali');
        }

        return $player;
    }

    public static function getStandardTeamPlayerInfo($playerId)
    {
        // 从主表team里面查找下面得队员
        $teamPlayer = TeamPlayerRelation::find()->where(['player_id' => $playerId])->asArray()->all();
        $teamIds = array_column($teamPlayer, 'team_id');

        $team = Team::find()->where(['in', 'id', $teamIds])->asArray()->all();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');
        foreach ($team as &$value) {
            $value["country_name"] = $country[$value['country']] ?? '';
            $value['game_logo'] = $games[$value['game']] ?? '';
        }

        return $team;
    }

    public static function getDetail($standardId)
    {
        $info = StandardDataPlayer::find()->where(['id' => $standardId])->asArray()->one();
        $bindingInfo = self::getTeamBindingInfo($standardId);
        $bindingExpectInfo = self::getTeamBindingExceptInfo($standardId);
        $bindingExpectTeam = '';
        if($info['team']){
            $bindingExpectTeam = StandardDataTeam::find()->select('name,short_name,logo')->where(['rel_identity_id'=>$info['team']])
                ->andWhere(['origin_id'=>$info['origin_id']])->asArray()->one();
        }
        $val['base'] = $info;
        if($bindingExpectTeam){
            $val['base']['binding_expect_team'] = $bindingExpectTeam;
        }
        $val["binding_info"] = $bindingInfo;
        $val["binding_expect_info"] = $bindingExpectInfo;
        $playerIds = array_column($val["binding_expect_info"], 'id');
        // 获取战队

        $teamIds = \app\modules\org\models\TeamPlayerRelation::find()
            ->alias('tpr')
            ->select('t.*,tpr.player_id as player_id')
            ->leftJoin('team as t', 'tpr.team_id = t.id')
            ->where(['in', 'tpr.player_id', $playerIds])
            ->asArray()->all();
        foreach ($val["binding_expect_info"] as &$value) {
            $value['team_name'] = '';
            foreach ($teamIds as $v) {
                if ($value['id'] == $v['player_id']) {
                    $value['team_name'] = $v['name'];
                    $value['team_id'] = $v['id'];
                }
            }
        }
        return $val;
    }

    public static function setBinding($standardId, $masterId, $userType = Consts::USER_TYPE_ADMIN, $userId = 0, $basisId = 0, $log = true)
    {
        $Player = Player::find()->where(['id' => $masterId])->asArray()->one();
        if (empty($Player)) {
            throw new BusinessException([], '不存在该绑定id:' . $masterId);
        }
        $logOldPlayer = StandardDataPlayer::find()->where(['id' => $standardId])->asArray()->one();
//        $info = DataStandardMasterRelation::find()
//            ->alias('dmr')
//            ->select('standard_data_player.*')
//            ->leftJoin('standard_data_player', 'standard_data_player.id = dmr.standard_id')
//            ->where(
//                [
//                    'dmr.resource_type' => Consts::RESOURCE_TYPE_PLAYER,
//                    'dmr.master_id' => $masterId,
//                    'standard_data_player.origin_id' => $logOldPlayer['origin_id']
//                ])->asArray()->one();
//        if (!empty($info) && $info) {
//            throw new BusinessException([], '已经绑定过了 id:' . $info['rel_identity_id']);
//        }

        if ($Player['game_id'] != $logOldPlayer['game_id']) {
            throw new BusinessException([], '游戏不相同');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 更新绑定关系
            $operationBindingInfo = [
                'resource_type' => Consts::RESOURCE_TYPE_PLAYER,
                'standard_id' => $standardId,
                'master_id' => $masterId
            ];
            $op = DataStandardMasterRelation::find()->where(
                ['resource_type' => Consts::RESOURCE_TYPE_PLAYER, 'standard_id' => $standardId])->one();
            if (empty($op) || !$op) {
                $op = new DataStandardMasterRelation();
            }
            $op->setAttributes($operationBindingInfo);
            $op->save();
            // 更新操作状态
            $stand = StandardDataPlayer::find()->where(['id' => $standardId])->one();
            $stand->setAttribute('deal_status', Consts::BINGING_STATUS_WISH_DONE);
            $stand->save();
            // 删除预期绑定
            $delSql = 'delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
            $cmd = \Yii::$app->getDb()
                ->createCommand($delSql)
                ->bindParam(':standard_id', $standardId)
                ->bindParam(':resource_type', $resourceType)
                ->execute();

            $diffInfo = Common::getDiffInfo($logOldPlayer, $stand->toArray(), []);
            if ($diffInfo['changed']) {
                OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                    Consts::RESOURCE_TYPE_BING_PLAYER,
                    $standardId,
                    ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                    0,
                    Consts::TAG_TYPE_CORE_DATA,
                    null,
                    $basisId,
                    $userType,
                    $userId
                );
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function addAndBinding($id, $userType = Consts::USER_TYPE_ADMIN, $userId = 0)
    {
        // standard的player信息
        $infoFromStandard = StandardDataPlayer::find()->where(['id' => $id])->asArray()->one();

        $infoFromStandard['player_country'] = $infoFromStandard['nationality'];

        if($infoFromStandard['image'] == "https://static.hltv.org/images/playerprofile/blankplayer.svg"){  //hltv选手默认图片
            $infoFromStandard['image'] ='';
        }
        if($infoFromStandard['image']){
            $infoFromStandard['image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['image']);
        }
        // 生成slug
        if(empty($infoFromStandard['slug'])){
            $infoFromStandard['slug'] = Common::makeConstantMatching($infoFromStandard['nick_name']);
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 账号属性分类  1、验证字段属性 进行赋值  2、得到表的字段tag进行分块  3、再把不同的字段存储不同的tag里面
            $tagShowInfo = UpdateConfig::toTagInfoShow(Consts::RESOURCE_TYPE_PLAYER, $infoFromStandard);

            // 账号保存
            $playerInfo = PlayerService::setPlayer($tagShowInfo, $userType, $userId, $id, $infoFromStandard['origin_id']);

            // 添加绑定状态
            $playerId = $playerInfo["player_id"];
            self::setBinding($id, $playerId, $userType, $userId, 0, false);

            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    private static function getRoleMapping($roleString)
    {
        $map = [
            'top' => 1,
            'Top' => 1,
            'Jungle' => 2,
            'jun' => 2,
            'Support' => 5,
            'sup' => 5,
            'adc' => 4,
            'ADC' => 4,
            'mid' => 3,
            'MID' => 3,
        ];
        if (isset($map[$roleString])) {
            return $map[$roleString];
        }
    }

    public static function getPlayersByStandardPlayerRelIdentityIds($relIdentityIds, $originId, $gameId)
    {
        if(!$relIdentityIds){
            $relIdentityIds = [];
        }
        $q = StandardDataPlayer::find()
            ->alias('standard_player')
            ->select(['player.*','standard_player.rel_identity_id as rel_identity_id'])
            ->leftJoin('data_standard_master_relation rel', 'standard_player.id=rel.standard_id and rel.resource_type="player"')
            ->leftJoin('player', 'player.id=rel.master_id')
            ->andWhere(['in', 'standard_player.rel_identity_id', $relIdentityIds])
            ->andWhere(['=', 'standard_player.origin_id', $originId])
            ->andWhere(['>','player.id','0'])
            ->andWhere(['=','player.game_id',$gameId]);
        $player=$q->asArray()->all();
        if(!$player){
            $player = [];
        }
        $info=array_column($player,null,'rel_identity_id');
        return $info;
    }
}