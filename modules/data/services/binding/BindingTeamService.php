<?php
/**
 *
 */

namespace app\modules\data\services\binding;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DbStream;
use app\modules\operation\models\OperationStandardMasterRelation;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataRelTeamPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class BindingTeamService
{
    public static function getList($params)
    {
        if (!empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $params['date_end'] = $end;
        }
        $q = StandardDataTeam::find()
            ->select(['t.*','mt.country as els_country','dcc.origin_id as dcc_origin_id','if(mt.id,ifnull(ori.desc,"人工"),pdsmre.change_type) as change_type'])
            ->alias('t')
            ->leftJoin('data_standard_master_relation as pdsmr', 'pdsmr.standard_id = t.id AND (`pdsmr`.`resource_type` = \'team\')')
            ->leftJoin('team as mt', 'pdsmr.master_id = mt.id')
            ->leftJoin('data_change_config as dcc', 'dcc.resource_id = pdsmr.master_id and dcc.resource_type = "team"')
            ->leftJoin('view_relation_expect_team as pdsmre', 'pdsmre.standard_id = t.id')
            ->leftJoin('enum_origin as ori', 'dcc.origin_id = ori.id');
            if (isset($params['rel_rel_identity_id']) && $params['rel_rel_identity_id']) {
                $q->leftJoin('bayes_identifiers as bi', 'bi.rel_identity_id = t.rel_identity_id and bi.resource_type = "team" and bi.origin_id = '.$params['origin_id'] );
            }

        $whereConfig = [
            "origin_id" => [
                'type' => '=',
                'key' => 't.origin_id'
            ],
            "game_id" => [
                'type' => '=',
                'key' => 't.game_id'
            ],
            "deal_status" => [
                'type' => '=',
                'key' => 't.deal_status'
            ],

            "country" => function ($params) {
                if (isset($params["country"]) && $params["country"]) {
                    return ['or',
                        [
                            "=",
                            'mt.country',
                            $params["country"],
                        ],
                        [
                            "like",
                            'pdsmre.country_all',
                            $params["country"],
                        ]
                    ];
                }
            },

            "id" => function ($params) {
                if (isset($params['id']) && $params["id"]) {
                    return [
                        'or',
                        [
                            "=",
                            'pdsmr.master_id',
                            $params['id'],
                        ],
                        [
                            "like",
                            'pdsmre.master_id_all',
                            $params['id'],
                        ],
                    ];
                }
            },
            "rel_identity_id" => function ($params) {
                if (isset($params['rel_team_id']) && $params['rel_team_id']) {
                    return ["=", 't.rel_identity_id', $params['rel_team_id']];
                }
            },
            "rel_rel_identity_id" => function ($params) {
                if (isset($params['rel_rel_identity_id']) && $params['rel_rel_identity_id']) {
                    return ["=", 'bi.value', $params['rel_rel_identity_id']];
                }
            },
            "date_begin" => [
                'type' => ">=",
                'key' => 't.modified_at'
            ],
            "date_end" => [
                'type' => "<",
                'key' => 't.modified_at'
            ],
            "team_name" => function ($params) {
                if (isset($params['team_name']) && $params['team_name']) {
                    return ['or',
                        ['like', 'mt.name', $params['team_name']],
                        ['like', 'mt.full_name', $params['team_name']],
                        ['like', 'mt.short_name', $params['team_name']],
                        ['like', 'mt.alias', $params['team_name']],
                        ['like', 't.full_name', $params['team_name']],
                        ['like', 't.name', $params['team_name']],
                        ['like', 't.short_name', $params['team_name']],
                        ['like', 't.alias', $params['team_name']],
                        ['like', 'pdsmre.name_all', $params['team_name']],
                        ['like', 'pdsmre.full_name_all', $params['team_name']],
                        ['like', 'pdsmre.short_name_all', $params['team_name']],
                        ['like', 'pdsmre.alias_all', $params['team_name']],
                    ];
                }
            }
        ];
        $whereArray = DbHelper::getWhere($whereConfig, $params);
        $where = array_merge(['and'], $whereArray);
        $q->where($where);
        //创建方式筛选
        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != "人工" && $params['change_type'] != "全部"){
            $q->andWhere(['or',
                ['pdsmre.change_type' => $params['change_type']],
                ['ori.desc' => $params['change_type']]
            ]);
        }
        if($params['change_type'] == '人工'){
            $q->andWhere(['or',
                ['=','pdsmre.change_type','人工'],
//                ['is','ori.desc' ,null]
            ]);
        }
//        if (isset($params['team_name']) && $params['team_name'])
//        {
//            $q->andWhere(['or',
//                ['like','mt.name',$params['team_name']],
//                ['like','mt.full_name',$params['team_name']],
//                ['like','mt.short_name',$params['team_name']],
//                ['like','mt.alias',$params['team_name']],
//                ['like','t.team_name',$params['team_name']],
//                ['like','t.name',$params['team_name']],
//                ['like','t.short_name',$params['team_name']],
//                ['like','t.alias',$params['team_name']],
//            ]);
//        }
        //$sql=$q->createCommand()->getRawSql();
        $total = $q->count();
        $page = (isset($params['page']) && $params['page']) ? $params['page'] : 1;
        $perPage = (isset($params['per_page']) && $params['per_page']) ? $params['per_page'] : 10;
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');

        $list = $q->orderBy('t.deal_status asc,t.modified_at desc')->limit($perPage)->offset($perPage * ($page - 1))->asArray()->all();

        $resourceIds = array_column($list, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_BING_TEAM, $resourceIds);

        foreach ($list as $key => &$val) {
            $val["binding_info"] = self::getTeamBindingInfo($val['id']);
            $val["els_country_name"] = $country[$val['els_country']] ?? '';
            $val["country_name"] = $country[$val['country']] ?? '';
            $val['game_logo'] = $games[$val['game_id']] ?? '';
            $val["binding_expect_info"] = self::getTeamBindingExceptInfo($val['id']);
            if(isset($val["binding_expect_info"])){
                foreach ($val["binding_expect_info"] as $key =>$value){
                    $val['binding_expect_info'][$key]['image'] = ImageConversionHelper::showFixedSizeConversion($value["image"], 66, 66,'ali');
                }
            }
            if($val['origin_id'] == 9 || $val['origin_id'] == 10){
                $hostUrl = \Yii::$app->request->hostInfo;
                $val['logo'] = $hostUrl."/v1/data/image/get-bayes-image?origin_id=".$val['origin_id']."&image_url=".$val['logo'];
            }
            $val["binding_expect_info"]["image"] = ImageConversionHelper::showFixedSizeConversion($val["binding_expect_info"]["image"], 66, 66,'ali');
            $val['operation'] = @$operationInfo[$val['id']];
            $val['binding_info']['image'] = ImageConversionHelper::showFixedSizeConversion($val['binding_info']['image'], 66, 66, 'ali');
//            if (empty($val['dcc_origin_id'])) {
//                $val['change_type'] = '人工';
//            } else {
//                switch ($val['dcc_origin_id']) {
//                    case 1 :
//                        $val['change_type'] = '飞鲸';
//                        break;
//                    case 2 :
//                        $val['change_type'] = 'ABIOS';
//                        break;
//                    case 3 :
//                        $val['change_type'] = 'pandascore';
//                        break;
//                }
//            }
        }
        return [
            'total' => $total,
            'list' => $list
        ];
    }

    public static function getTeamBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('t.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('team as t', 'rel.master_id=t.id')
            ->andWhere(['rel.standard_id' => $standardId, 'rel.resource_type' => Consts::RESOURCE_TYPE_TEAM]);
        return $q->one();

    }

    public static function getTeamBindingExceptInfo($standardId)
    {
        $q = (new Query())
            ->select('t.*')
            ->from('data_standard_master_relation_expect as rel')
            ->leftJoin('team as t', 'rel.master_id=t.id')
            ->andWhere(['rel.standard_id' => $standardId, 'rel.resource_type' => Consts::RESOURCE_TYPE_TEAM])
            ->limit(10);
        $team = $q->all();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');
        foreach ($team as &$value) {
            $value["country_name"] = $country[$value['country']] ?? '';
            $value['game_logo'] = $games[$value['game']] ?? '';
            $value['team'] = TeamService::getTeam($value['id']);
        }
        return $team;
    }

    public static function getDetail($standardId)
    {
        $info = StandardDataTeam::find()->where(['id' => $standardId])->asArray()->one();
        $playerIds = json_decode($info['players'], true);
        $playersStandard = [];
        if ($playerIds) {
            $playersStandard =
                StandardDataPlayer::find()->
                where(['and',
                    ['in', 'rel_identity_id', $playerIds],
                    ['=', 'origin_id', $info['origin_id']],
                    ['=', 'game_id', $info['game_id']],
                ])
                    ->asArray()->all();
        }
        $info['players'] = $playersStandard;
        $bindingInfo = self::getTeamBindingInfo($standardId);
        $bindingExpectInfo = self::getTeamBindingExceptInfo($standardId);

        foreach ($bindingExpectInfo as &$val) {
            $val['players'] = self::getTeamPlayerInfo($val['id']);
        }

        if ($bindingInfo) {
            $bindingInfo['players'] = self::getTeamPlayerInfo($val['id']);
        }

        $data['base'] = $info;
        $data["binding_info"] = $bindingInfo;
        $data["binding_expect_info"] = $bindingExpectInfo;
        return $data;
    }

    public static function setBinding($standardId, $masterId, $userType = Consts::USER_TYPE_ADMIN, $userId = 0, $basisId = 0, $log = true, $originId = false)
    {
        $Team = Team::find()->where(['id' => $masterId])->asArray()->one();
        if (empty($Team)) {
            throw new BusinessException([], '没有查询到战队Id:' . $masterId);
        }

        $logOldTeam = StandardDataTeam::find()->where(['id' => $standardId])->asArray()->one();

//        $info = DataStandardMasterRelation::find()
//            ->alias('dmr')
//            ->select('standard_data_team.*')
//            ->leftJoin('standard_data_team', 'standard_data_team.id = dmr.standard_id')
//            ->where(
//                [
//                    'dmr.resource_type' => Consts::RESOURCE_TYPE_TEAM,
//                    'dmr.master_id' => $masterId,
//                    'standard_data_team.origin_id' => $logOldTeam['origin_id']
//                ])->asArray()->one();
//        if (!empty($info) && $info) {
//            throw new BusinessException([], '已经绑定过了 id:' . $info['rel_identity_id']);
//        }
        if ($Team['game'] != $logOldTeam['game_id']) {
            throw new BusinessException([], '游戏不相同');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 更新绑定关系
            $operationBindingInfo = [
                'resource_type' => Consts::RESOURCE_TYPE_TEAM,
                'standard_id' => $standardId,
                'master_id' => $masterId
            ];
            $op = DataStandardMasterRelation::find()->where(
                ['resource_type' => Consts::RESOURCE_TYPE_TEAM, 'standard_id' => $standardId])->one();
            if (empty($op) || !$op) {
                $op = new DataStandardMasterRelation();
            }
            $op->setAttributes($operationBindingInfo);
            $op->save();
            // 更新操作状态
            $stand = StandardDataTeam::find()->where(['id' => $standardId])->one();
            $stand->setAttribute('deal_status', Consts::BINGING_STATUS_WISH_DONE);
            $stand->save();
            // 删除预期绑定
            $delSql = 'delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
            $cmd = \Yii::$app->getDb()
                ->createCommand($delSql)
                ->bindParam(':standard_id', $standardId)
                ->bindParam(':resource_type', $operationBindingInfo['resource_type'])
                ->execute();


            $diffInfo = Common::getDiffInfo($logOldTeam, $stand->toArray(), []);
            if ($diffInfo['changed']) {
                OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                    Consts::RESOURCE_TYPE_BING_TEAM,
                    $standardId,
                    ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                    0,
                    Consts::TAG_TYPE_CORE_DATA,
                    null,
                    $basisId,
                    $userType,
                    $userId
                );
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function getStandardTeamPlayers($teamInfo)
    {
        $teamIdentityId = $teamInfo['rel_identity_id'];
        $originId = $teamInfo["origin_id"];
        $gameId = $teamInfo["game_id"];
        $q = StandardDataRelTeamPlayer::find()
            ->select("std_player.*")
            ->leftJoin(['std_player' => 'standard_data_player'], "standard_data_rel_team_player.player_identity_id=std_player.rel_identity_id")
            ->where(['and',
                ['=', 'standard_data_rel_team_player.team_identity_id', $teamIdentityId],
                ['=', 'standard_data_rel_team_player.origin_id', $originId],
                ['=', 'standard_data_rel_team_player.game_id', $gameId]
            ]);
        $playerInfo = $q->asArray()->all();
        return $playerInfo;
    }

    public function getTeamPlayerInfo($teamId)
    {
        // 从主表team里面查找下面得队员
        $teamPlayer = TeamPlayerRelation::find()->where(['team_id' => $teamId])->asArray()->all();
        $playerIds = array_column($teamPlayer, 'player_id');

        $player = Player::find()->where(['in', 'id', $playerIds])->asArray()->all();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');
        foreach ($player as &$value) {
            $value["country_name"] = $country[$value['player_country']] ?? '';
            $value['game_logo'] = $games[$value['game_id']] ?? '';
        }

        return $player;
    }

    public function getTeamPlayer($teamId)
    {
        $q = (new Query())
            ->select('t.*,rel.player_id as player_id')
            ->from('team_player_relation as rel')
            ->leftJoin('team as t', 'rel.team_id=t.id')
            ->andWhere(['in', 'rel.team_id', array_column($teamId, 'id')]);
        return $q->all();
    }

    public static function addAndBinding($id, $userType = Consts::USER_TYPE_ADMIN, $userId = 0)
    {
        // standard的player信息
        $infoFromStandard = StandardDataTeam::find()->where(['id' => $id])->one()->toArray();
        $infoFromStandard['game'] = $infoFromStandard['game_id'];
        if($infoFromStandard['logo']){
            $infoFromStandard['logo'] = ImageConversionHelper::mvImageToAli($infoFromStandard['logo']);
        }
        $infoFromStandard['organization'] = $infoFromStandard['clan_id'];
        // 生成slug
        if(empty($infoFromStandard['slug'])){
            $infoFromStandard['slug'] = Common::makeConstantMatching($infoFromStandard['name']);
        }
//        $infoFromStandard['full_name'] = $infoFromStandard['name'];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 账号属性分类
            $tagShowInfo = UpdateConfig::toTagInfoShow(Consts::RESOURCE_TYPE_TEAM, $infoFromStandard);
            // 账号保存
            $teamInfo = TeamService::setTeam($tagShowInfo, $userType, $userId, $id, $infoFromStandard['origin_id']);
            // 添加绑定状态
            $teamId = $teamInfo["team_id"];
            self::setBinding($id, $teamId, $userType, $userId, 0, false);

            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function grabTeam()
    {
        $teamCount = DbStream::getDb()->createCommand(
            "select count(*) as count from team_all"
        )->queryOne()['count'];
        if (empty($teamCount)) return [];

        $pageSize = 500;

        $totalPage = ceil($teamCount / $pageSize);
        $j = 0;
        for ($i = 0; $i <= $totalPage; $i++) {
            $j++;
            $offset = ($j - 1) * $pageSize;

            $teamData = DbStream::getDb()->createCommand("select
 * from team_all  limit  {$offset},{$pageSize} ")->queryAll();

            foreach ($teamData as $key => $val) {
                $teamInfo = null;
                $teamInfo = StandardDataTeam::find()->where(['rel_identity_id' => $val['team_id'],'origin_id' => 8])->one();
                if (!$teamInfo) {
                    $teamInfo = new StandardDataTeam();
                }
                $param['origin_id'] = 8;
                $param['rel_identity_id'] = $val['team_id'];
                $param['game_id'] = 1;
                $param['country'] = self::getCountryId($val['country']);
                $param['name'] = $val['name'];
                $param['short_name'] = $val['name'];
                $param['full_name'] = $val['name'];
                $param['logo'] = $val['image'];
                $param['players'] = "[" . implode(',', explode('+', $val['player_now'])) . "]" ?: "[]";
                $param['history_players'] = "[" . implode(',', explode('+', $val['player_old'])) . "]" ?: "[]";
                $teamInfo->setAttributes($param);

                if (!$teamInfo->save()) {
                    continue;
                }
            };
        }
        echo "执行完成";
    }

    public static function getCountryId($country)
    {
        if (empty($country)) return '';
        $countryId = EnumCountry::find()->select("id")->where(['hltv' => $country])->asArray()->one()['id'];
        return $countryId ?: '';
    }

}