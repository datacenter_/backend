<?php
/**
 *
 */

namespace app\modules\data\services\binding;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DataStandardTournamentGroupStage;
use app\modules\metadata\services\MetadataService;
use app\modules\operation\models\OperationStandardMasterRelation;
use app\modules\org\models\Player;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\models\TeamSnapshot;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataGroup;
use app\modules\task\models\StandardDataStage;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\Common;
use app\modules\task\services\standardIncrement\StandardTournament;
use app\modules\task\services\UpdateConfig;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentTeamRelation;
use app\modules\tournament\services\TournamentService;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class BindingTournamentService
{
    public static function getList($params)
    {
        if (!empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $params['date_end'] = $end;
        }
//        if (!empty($params['scheduled_end_at'])) {
//            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));
//            $params['scheduled_end_at'] = $end;
//        }

        $q=StandardDataTournament::find()
            ->select(['t.*','mtb.country as els_country','dcc.origin_id as dcc_origin_id','if(mt.id,ifnull(ori.desc,"人工"),pdsmre.change_type) as change_type'])
            ->alias('t')
            ->leftJoin('data_standard_master_relation as pdsmr','pdsmr.standard_id = t.id and pdsmr.resource_type = \'tournament\'')
            ->leftJoin('tournament as mt','pdsmr.master_id = mt.id')
            ->leftJoin('tournament_base as mtb','mtb.tournament_id = mt.id')
            ->leftJoin('data_change_config as dcc','dcc.resource_id = pdsmr.master_id and dcc.resource_type = "tournament"')
            ->leftJoin('view_relation_expect_tournament as pdsmre', 'pdsmre.standard_id = t.id')
            ->leftJoin('enum_origin as ori', 'dcc.origin_id = ori.id');


        $whereConfig=[
            "origin"=>[
                'type'=>'=',
                'key'=>'t.origin_id'
            ],
            "games"=>[
                'type'=>'=',
                'key'=>'t.game_id'
            ],
            "status"=>[
                'type'=>'=',
                'key'=>'t.deal_status'
            ],
//            "scheduled_begin_at"=>[
//                'type'=>">=",
//                'key'=>'t.scheduled_begin_at'
//            ],
//            "scheduled_end_at"=>[
//                'type'=>"<",
//                'key'=>'t.scheduled_begin_at'
//            ],
            "country" => function ($params) {
                if (isset($params["country"]) && $params["country"]) {
                    return ['or',
                        [
                            "=",
                            'mtb.country',
                            $params["country"],
                        ],
                        [
                            "like",
                            'pdsmre.country_all',
                            $params["country"],
                        ]
                    ];
                }
            },
            "id" => function ($params) {
                if (isset($params['id']) && $params["id"]) {
                    return [
                        'or',
                        [
                            "=",
                            'pdsmr.master_id',
                            $params['id'],
                        ],
                        [
                            "like",
                            'pdsmre.master_id_all',
                            $params['id'],
                        ],
                    ];
                }
            },
            "rel_identity_id" => function ($params) {
                if (isset($params['rel_identity_id']) && $params['rel_identity_id']) {
                    return ["=", 't.rel_identity_id', $params['rel_identity_id']];
                }
            },

            "date_begin"=>[
                'type'=>">=",
                'key'=>'t.modified_at'
            ],
            "date_end"=>
                [
                    'type'=>"<",
                    'key'=>'t.modified_at'
                ]
        ];
        $whereArray=DbHelper::getWhere($whereConfig,$params);

        $where=array_merge(['and'],$whereArray);
        $q->where($where);
        $q->andWhere(['t.type'=>1]);

        if (isset($params['scheduled_begin_at_1']) && !empty($params['scheduled_begin_at_1']) &&
            isset($params['scheduled_begin_at_2']) && !empty($params['scheduled_begin_at_2'])) {
            $scheduled_begin_at_2_end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_begin_at_2'])));

            $q->andWhere(
                [
                    'or',
                    [
                        'and',
                        ['>=', 't.scheduled_begin_at', $params['scheduled_begin_at_1']],
                        ['<', 't.scheduled_begin_at', $scheduled_begin_at_2_end]
                    ],
                    [
                        'and',
                        ['>=', 'mt.scheduled_begin_at', $params['scheduled_begin_at_1']],
                        ['<', 'mt.scheduled_begin_at', $scheduled_begin_at_2_end],
                        "mt.id is not null"
                    ]

                ]
               );
        }

        if (isset($params['scheduled_end_at_1']) && !empty($params['scheduled_end_at_1']) &&
            isset($params['scheduled_end_at_2']) && !empty($params['scheduled_end_at_2'])) {
            $scheduled_end_at_2_end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at_2'])));
            $q->andWhere(
                [
                    'or',
                    [
                        'and',
                        ['>=', 't.scheduled_end_at', $params['scheduled_end_at_1']],
                        ['<', 't.scheduled_end_at', $scheduled_end_at_2_end]
                    ],
                    [
                        'and',
                        ['>=', 'mt.scheduled_end_at', $params['scheduled_end_at_1']],
                        ['<', 'mt.scheduled_end_at', $scheduled_end_at_2_end],
                        "mt.id is not null"
                    ]

                ]
            );
        }


        if (isset($params['scheduled_begin_at']) && !empty($params['scheduled_begin_at']) &&
                isset($params['scheduled_end_at']) && !empty($params['scheduled_end_at']) && $params['is_null'] == 'false'
            ) {
            $scheduled_end_at = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));

            $q->andWhere(
                [
                    'or',
                    [
                        'and',
                        [
                            'or',
                            ['<', 't.scheduled_begin_at', $scheduled_end_at],
                            ['>', 't.scheduled_end_at', $params['scheduled_begin_at']],


                        ],
                        't.scheduled_end_at is not null',
                        't.scheduled_begin_at is not null'
                    ],
                    [
                        'and',
                        [
                            'or',
                            ['<', 'mt.scheduled_begin_at', $scheduled_end_at],
                            ['>', 'mt.scheduled_end_at', $params['scheduled_begin_at']],


                        ],
                        'mt.scheduled_end_at is not null',
                        'mt.scheduled_begin_at is not null',
                    ],
                ]

            );
        }

        if ($params['is_null'] == 'true') {
            $scheduled_end_at = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));

            $q->andWhere([
                'and',
                [
                    'or',
                    ['<', 't.scheduled_begin_at', $scheduled_end_at],
                    ['>', 't.scheduled_end_at', $params['scheduled_begin_at']],
                    ['<', 'mt.scheduled_begin_at', $scheduled_end_at],
                    ['>', 'mt.scheduled_end_at', $params['scheduled_begin_at']],

                ],
                [
                    'or',
                    't.scheduled_begin_at is null',
                    't.scheduled_end_at is null',
                    'mt.scheduled_begin_at is null',
                    'mt.scheduled_end_at is null',
                ]

            ]);
        }

        $q->andWhere(['or',
            ['t.parent_id'=>null],
            ['t.parent_id'=>0]
        ]);  //只显示根目录的
//        $q->andWhere(['pdsmr.resource_type' => 'tournament']);
        if (isset($params['name']) && $params['name'])
        {
            $q->andWhere(['or',
                ['like','t.name',$params['name']],
                ['like','t.name_cn',$params['name']],
                ['like','t.short_name',$params['name']],
                ['like','t.short_name_cn',$params['name']],
                ['like','mt.name',$params['name']],
                ['like','mt.name_cn',$params['name']],
                ['like','mt.short_name',$params['name']],
                ['like','mt.short_name_cn',$params['name']],
                ['like','pdsmre.name_all', $params['name']],
                ['like','pdsmre.name_cn_all', $params['name']],
                ['like','pdsmre.short_name_all', $params['name']],
                ['like','pdsmre.short_name_cn_all', $params['name']],
            ]);
        }
        //创建方式筛选
        if(isset($params['change_type']) && !empty($params['change_type']) && $params['change_type'] != '人工' && $params['change_type'] != '全部'){
            $q->andWhere(['or',
                ['pdsmre.change_type' => $params['change_type']],
                ['ori.desc' => $params['change_type']]
            ]);
        }
        if($params['change_type'] == '人工'){
            $q->andWhere(['or',
                ['=','pdsmre.change_type','人工'],
//                ['is','ori.desc' ,null]
            ]);
        }
        $total=$q->count();
//        $m = $q->createCommand()->getRawSql();

//        print_r($m);die;

        $page=(isset($params['page'])&&$params['page'])?$params['page']:1;
        $perPage=(isset($params['per_page'])&&$params['per_page'])?$params['per_page']:10;
        $country = array_column(EnumCountry::getCountry(),'name','id');
        $list=$q->orderBy('t.deal_status asc,t.modified_at desc')->limit($perPage)->offset($perPage*($page-1))->asArray()->all();
        $games = array_column(EnumService::getGameLogo(),'logo','id');

        $resourceIds=array_column($list,'id');
        $operationInfo=OperationLogService::operationCounts(Consts::RESOURCE_TYPE_BING_TOURNAMENT,$resourceIds);

        foreach ($list as $key=>&$val){
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 66, 66, 'ali');
            $val["binding_info"]=self::getTournamentBindingInfo($val['id']);
            $val["country_name"]=$country[$val['country']] ?? '';
            $val["els_country_name"]=$country[$val['els_country']] ?? '';
            $val["binding_expect_info"]=self::getTournamentBindingExceptInfo($val['id']);
            $val['operation']=$operationInfo[$val['id']] ?? '';
            $val['game_logo']=$games[$val['game_id']] ?? '';
//            if (empty($val['dcc_origin_id']))
//            {
//                $val['change_type'] = '人工';
//            }else{
//                switch ($val['dcc_origin_id'])
//                {
//                    case 1 : $val['change_type'] = '飞鲸';break;
//                    case 2 : $val['change_type'] = 'ABIOS';break;
//                    case 3 : $val['change_type'] = 'pandascore';break;
//                }
//            }
        }
        return [
            'total'=>$total,
            'list'=>$list
        ];
    }

    public static function getTournamentBindingInfo($standardId)
    {
        $q=(new Query())
            ->select('tournament.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('tournament','rel.master_id=tournament.id')
            ->andWhere(['standard_id'=>$standardId,'resource_type'=>Consts::RESOURCE_TYPE_TOURNAMENT])->one();
        if($q){
            $q['image'] = ImageConversionHelper::showFixedSizeConversion($q['image'], 66, 66, 'ali');
        }
        return $q;
    }

    public static function getTournamentBindingExceptInfo($standardId)
    {
        $q=(new Query())
            ->select(['tournament.*','tournament_base.country'])
            ->from('data_standard_master_relation_expect as rel')
            ->leftJoin('tournament','rel.master_id=tournament.id')
            ->leftJoin('tournament_base','tournament_base.tournament_id=rel.master_id')
            ->andWhere(['standard_id'=>$standardId,'resource_type'=>Consts::RESOURCE_TYPE_TOURNAMENT])
            ->all();
        if($q){
            foreach ($q as &$value){
                $value['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'], 66, 66, 'ali');
            }
        }
        return $q;
    }

    public static function getDetail($standardId)
    {
        $info=StandardDataTournament::find()->where(['id'=>$standardId])->asArray()->one();
        $teamsIds = json_decode($info['teams_condition'], true);
        $teamsIdsStandard = [];
        if ($teamsIds) {
            $teamsIds = array_column($teamsIds,'team_id');
            $teamsIdsStandard =
                StandardDataTeam::find()->
                where(['and',
                    ['in', 'rel_identity_id', $teamsIds],
                    ['=', 'origin_id', $info['origin_id']],
                    ['=', 'game_id', $info['game_id']],
                ])
                    ->asArray()->all();
        }
        $info['teams_condition'] = $teamsIdsStandard;
        $bindingInfo=self::getTournamentBindingInfo($standardId);
        $bindingExpectInfo=self::getTournamentBindingExceptInfo($standardId);
        //获取主表赛事下teams
        foreach ($bindingExpectInfo as $key => &$val) {
            $bindingExpectInfo[$key]['teams_list'] = self::getTeamsInfoByMainTournamentId($val['id']);
        }
        $data['base']=$info;
        $data["binding_info"]=$bindingInfo;
        $data["binding_expect_info"]=$bindingExpectInfo;
        return $data;
    }

    public static function setBinding($standardId,$masterId,$userType=Consts::USER_TYPE_ADMIN,$userId=0,$basisId = 0,$log=true)
    {
        $Team = Tournament::find()->where(['id'=>$masterId])->asArray()->one();
        if (empty($Team)) {
            throw new BusinessException([],'没有查询到赛事Id:'.$masterId);
        }
        $logOldInfo = StandardDataTournament::find()->where(['id'=>$standardId])->asArray()->one();
//        $info = DataStandardMasterRelation::find()
//            ->alias('dmr')
//            ->select('standard_data_tournament.*')
//            ->leftJoin('standard_data_tournament','standard_data_tournament.id = dmr.standard_id')
//            ->where(
//                [
//                    'dmr.resource_type' => Consts::RESOURCE_TYPE_TOURNAMENT,
//                    'dmr.master_id' => $masterId,
//                    'standard_data_tournament.origin_id' =>$logOldInfo['origin_id']
//                ])->asArray()->one();
//        if (!empty($info) && $info)
//        {
//            throw new BusinessException([],'已经绑定过了 id:'.$info['rel_identity_id']);
//        }
        if ($Team['game'] != $logOldInfo['game_id'])
        {
            throw new BusinessException([],'游戏不相同');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 更新绑定关系
            $operationBindingInfo=[
                'resource_type'=>Consts::RESOURCE_TYPE_TOURNAMENT,
                'standard_id'=>$standardId,
                'master_id'=>$masterId
            ];
            $op = DataStandardMasterRelation::find()->where(
                ['resource_type' => Consts::RESOURCE_TYPE_TOURNAMENT, 'standard_id' => $standardId])->one();
            if (empty($op) || !$op)
            {
                $op = new DataStandardMasterRelation();
            }
            $op->setAttributes($operationBindingInfo);
            $op->save();
            // 更新操作状态
            $stand=StandardDataTournament::find()->where(['id'=>$standardId])->one();
            $stand->setAttribute('deal_status',Consts::BINGING_STATUS_WISH_DONE);
            $stand->save();
            // 删除预期绑定
            $delSql='delete from data_standard_master_relation_expect where standard_id=:standard_id and resource_type=:resource_type';
            $cmd=\Yii::$app->getDb()
                ->createCommand($delSql)
                ->bindParam(':standard_id',$standardId)
                ->bindParam(':resource_type',$operationBindingInfo['resource_type'])
                ->execute();

                $diffInfo = Common::getDiffInfo($logOldInfo, $stand->toArray(), []);
                if ($diffInfo['changed']) {
                    OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                        Consts::RESOURCE_TYPE_BING_TOURNAMENT,
                        $standardId,
                        ["diff" => $diffInfo["diff"], "new" => $stand->toArray()],
                        0,
                        Consts::TAG_TYPE_CORE_DATA,
                        null,
                        $basisId,
                        $userType,
                        $userId
                    );
                }
            $transaction->commit();
            return true;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function getStandardTeamPlayerInfo($teamId)
    {
        // 从主表team里面查找下面得队员
        $teamPlayer = TeamPlayerRelation::find()->where(['team_id' => $teamId])->asArray()->all();
        $playerIds = array_column($teamPlayer,'player_id');

        return Player::find()->where(['in','id',$playerIds])->asArray()->all();
    }

    public function getTeamPlayer($teamId)
    {
        $q=(new Query())
            ->select('team.*')
            ->from('team_player_relation as rel')
            ->leftJoin('player','rel.player_id=player.id')
            ->andWhere(['rel.team_id'=>$teamId]);
        return $q->all();
    }

    public static function addAndBinding($id,$userType=Consts::USER_TYPE_ADMIN,$userId=0)
    {
        // standard的信息
        $infoFromStandard=StandardDataTournament::find()->where(['id'=>$id])->asArray()->one();
        if($infoFromStandard['image']){
            $infoFromStandard['image'] = ImageConversionHelper::mvImageToAli($infoFromStandard['image']);
        }
        $infoFromStandard['game'] = (string)$infoFromStandard['game_id'];
        $infoFromStandard['location'] = $infoFromStandard['address'];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 账号属性分类
            $tagShowInfo=UpdateConfig::toTagInfoShow(Consts::RESOURCE_TYPE_TOURNAMENT,$infoFromStandard);
            // 账号保存
            $tournamentInfo=TournamentService::setTournament($tagShowInfo,$userType,$userId,$id,$infoFromStandard['origin_id']);
            // 添加绑定状态
            $tournamentId=$tournamentInfo["id"];
            self::setBinding($id,$tournamentId,$userType,$userId,0,false);

            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function getTreeList($params)
    {
        $tournament_id = $params['rel_league_id'];
        $originId = $params['origin_id'];
        // 获取已绑下子集
        $bingInfo = DataStandardTournamentGroupStage::find()->where(['standard_tournament_id' => $tournament_id])->asArray()->all();

        // 获取赛事得顶级
        $tournamentInfo = StandardDataTournament::find()->alias('t')
            ->select('id,name,rel_league_id as rel_identity_id')
            ->where(['t.origin_id' => $originId,'t.rel_league_id' => $tournament_id])
            ->asArray()->one();
        $tournamentInfo['type'] = 1;
        // 获取赛事阶段
        $tournamentInfo['son'] = StandardDataStage::find()->alias('s')
            ->select('id,name,rel_identity_id,tournament_id')
            ->where(['s.origin_id' => $originId,'s.tournament_id' => $tournament_id])
            ->asArray()->all();

        // 获取赛事分组
        $stageIds = array_column($tournamentInfo['son'],'rel_identity_id');
        $group = StandardDataGroup::find()
            ->select('id,name,rel_identity_id,tournament_id,stage_id')
            ->where(['origin_id' => $originId])
            ->andWhere(['in','stage_id',$stageIds])
            ->asArray()->all();
        $groupIDs = array_column($group,'stage_id');
        array_walk($group,function (&$val,$key) use($bingInfo){
            $val['is_bind'] = false;
            if (TournamentService::deep_in_array($val['rel_identity_id'],$bingInfo,'master_id'))
            {

                $val['is_bind'] = true;
            }
            return $val['type'] = 4;
        });
        foreach ($tournamentInfo['son'] as &$val)
        {
            if (!empty($val)) {
                $val['is_bind'] = false;
                if (TournamentService::deep_in_array($val['rel_identity_id'],$bingInfo,'master_id'))
                {

                    $val['is_bind'] = d;
                }
                $val['type'] = 3;
            }
            $key = array_search($val['rel_identity_id'],$groupIDs);
            if (isset($group[$key])) {
                $val['son'][] = $group[$key];
            }

        }

        return $tournamentInfo;
    }

    public static function setGroupOrStage($standardId,$masterId,$tournamentId,$type,$elsTournamentId)
    {
        $class = MetadataService::getObjByType($type);

        $info = $class::find()->where(['rel_identity_id' => $standardId,'tournament_id' => $tournamentId])->asArray()->one();
        if (empty($info) || !$info)
        {
            throw new BusinessException([],'没有查询该数据');
        }

        $relation = new DataStandardTournamentGroupStage();
        $relation->setAttributes(['type' => $type,'standard_tournament_id' => $tournamentId,
            'standard_id' => $standardId
        ,'master_id' => $masterId,
            'els_tournament_id' => $elsTournamentId
        ]);
        $relation->save();

        return $relation->toArray();
    }

    public static function CloseGroupOrStage($masterId)
    {
        $info = DataStandardTournamentGroupStage::find()->where(['master_id' => $masterId])->one();
        if (!$info)
        {
            throw new BusinessException($info,'没有查询该数据');
        }

        return DataStandardTournamentGroupStage::deleteAll(['master_id' => $masterId]);
    }

    public function getTeamsInfoByMainTournamentId($tournamentId)
    {
        // 从主表team里面查找下面得队员
        $tournament_Teams = TournamentTeamRelation::find()->where(['tournament_id' => $tournamentId])->asArray()->all();
        $teamsIds = array_column($tournament_Teams, 'team_id');
        $teams = TeamSnapshot::find()->where(['in', 'team_id', $teamsIds])->asArray()->all();
        return $teams;
    }

}