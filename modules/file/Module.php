<?php

namespace app\modules\file;

/**
 * file module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\file\controllers';

}
