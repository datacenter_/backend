<?php

namespace app\modules\file\controllers;

use app\controllers\RestController;
use app\modules\file\services\UploadService;

/**
 * Default controller for the `file` module
 */
class FileController extends RestController
{
    public function actionUpload()
    {
        $scr = $_FILES['file']['tmp_name'];
        $ext = substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.') + 1); // 上传文件后缀
        $dst = "els/".base64_encode(date("ymd")).'/'.base64_encode(date("His")).rand(100,999).'.'.$ext;
        $aliOriginUrl = UploadService::upload($dst, $scr);
        $url = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
        $is = strstr($url, 'http');
        if($is){
            $url = str_replace("http","https",$url);
        }

//        $dst = 'type_logo/' . md5(time()) . '/'  . md5($_FILES['file']['name']).'.'.$ext;
//        $url = UploadService::upload($dst,$scr);
        return ["url"=>$url];
    }
}
