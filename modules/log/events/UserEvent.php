<?php
namespace app\modules\log\events;

use app\components\IdentityActiveRecord;
use app\modules\customer\models\Customer;
use app\modules\kol\models\Kol;
use app\modules\kol\models\Schedule;
use app\modules\kol\models\ScheduleBusiness;
use app\modules\log\handlers\UserBehaviorHandler;
use app\modules\mini\models\User;
use app\modules\mini\models\UserBaseInfo;
use yii\base\Event;

/**
 * 封装event类，主要为了避免事件逻辑侵入业务代码过多、以及定义事件传参等
 * Class UserEvent
 * @package app\modules\log\events
 */
class UserEvent extends Event
{
    /**
     * 登陆
     */
    const EVENT_AFTER_USER_LOGIN = 'afterUserLogin';
    /**
     * 注册
     */
    const EVENT_AFTER_USER_REGISTER = 'afterUserRegister';

    /**
     * 添加KOL
     */
    const EVENT_AFTER_USER_ADD_KOL = 'afterUserAddKol';

    /**
     * 修改KOL
     */
    const EVENT_AFTER_USER_UPDATE_KOL = 'afterUserUpdateKol';

    /**
     * 添加排期
     */
    const EVENT_AFTER_USER_ADD_SCHEDULE = 'afterUserAddSchedule';

    /**
     * 修改排期
     */
    const EVENT_AFTER_USER_UPDATE_SCHEDULE = 'afterUserUpdateSchedule';

    /**
     * 删除排期
     */
    const EVENT_AFTER_USER_DELETE_ACCOUNT_SCHEDULE = 'afterUserDeleteAccountSchedule';

    /**
     * 修改用户资料
     */
    const EVENT_AFTER_USER_UPDATE_BASE_INFO = 'afterUserUpdateBaseInfo';

    /**
     * @var IdentityActiveRecord
     */
    public $identity;

    /**
     * @var Kol
     */
    public $kol;

    /**
     * @var Schedule
     */
    public $schedule;

    /**
     * 添加的客户
     * @var ScheduleBusiness[]
     */
    public $addBusinesses = [];

    /**
     * 修改的客户
     * @var ScheduleBusiness[]
     */
    public $updateBusinesses = [];

    /**
     * 删除的客户
     * @var ScheduleBusiness[]
     */
    public $deletedBusinesses = [];

    /**
     * @var UserBaseInfo
     */
    public $userBaseInfo;

    /**
     * 订阅一个事件
     * @param string $eventName
     */
    public static function subscribe(string $eventName)
    {
        parent::on(static::class, $eventName, [UserBehaviorHandler::class, 'on' . ucfirst($eventName)]);
    }

    /**
     * 发布事件:用户登录
     * @param IdentityActiveRecord $identity
     */
    public static function publishAfterUserLogin(IdentityActiveRecord $identity)
    {
        $event = new self();
        $event->identity = $identity;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_LOGIN, $event);
        } catch (\Throwable $e) {
            //非关键流程,记录异常log后期排查,不要影响正常业务流程
            \Yii::error('Event->' . self::EVENT_AFTER_USER_LOGIN . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:用户登录
     * @param IdentityActiveRecord $identity
     */
    public static function publishAfterUserRegister(IdentityActiveRecord $identity)
    {
        $event = new self();
        $event->identity = $identity;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_REGISTER, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_REGISTER . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:添加KOL
     * @param kol $kol
     */
    public static function publishAfterUserAddKol(Kol $kol)
    {
        $event = new self();
        $event->kol = $kol;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_ADD_KOL, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_ADD_KOL . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:修改KOL
     * @param kol $kol
     */
    public static function publishAfterUserUpdateKol(Kol $kol)
    {
        $event = new self();
        $event->kol = $kol;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_UPDATE_KOL, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_UPDATE_KOL . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:添加Schedule
     * @param Schedule $schedule
     */
    public static function publishAfterUserAddSchedule(Schedule $schedule)
    {
        $event = new self();
        $event->schedule = $schedule;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_ADD_SCHEDULE, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_ADD_SCHEDULE . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:修改Schedule
     * @param Schedule $schedule
     * @param ScheduleBusiness[] $addBusinesses 添加的客户
     * @param ScheduleBusiness[] $updateBusinesses 修改的客户
     * @param ScheduleBusiness[] $deleteBusiness 删除的客户
     */
    public static function publishAfterUserUpdateSchedule(Schedule $schedule, array $addBusinesses = [], array $updateBusinesses = [], array $deleteBusiness = [])
    {
        $event = new self();
        $event->schedule = $schedule;
        $event->addBusinesses = $addBusinesses;
        $event->updateBusinesses = $updateBusinesses;
        $event->deletedBusinesses = $deleteBusiness;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_UPDATE_SCHEDULE, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_UPDATE_SCHEDULE . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:删除Schedule
     * @param Schedule $schedule
     */
    public static function publishAfterUserDeleteAccountSchedule(Schedule $schedule)
    {
        $event = new self();
        $event->schedule = $schedule;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_DELETE_ACCOUNT_SCHEDULE, $event);
        } catch (\Throwable $e) {
            var_dump($e);die;
            \Yii::error('Event->' . self::EVENT_AFTER_USER_DELETE_ACCOUNT_SCHEDULE . ' triggered failed, message->' . $e->getMessage());
        }
    }

    /**
     * 发布事件:修改资料
     * @param UserBaseInfo $userBaseInfo
     */
    public static function publishAfterUserUpdateBaseInfo(UserBaseInfo $userBaseInfo)
    {
        $event = new self();
        $event->userBaseInfo = $userBaseInfo;

        try {
            parent::trigger(static::class, self::EVENT_AFTER_USER_UPDATE_BASE_INFO, $event);
        } catch (\Throwable $e) {
            \Yii::error('Event->' . self::EVENT_AFTER_USER_UPDATE_BASE_INFO . ' triggered failed, message->' . $e->getMessage());
        }
    }

}