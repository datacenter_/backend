<?php
namespace app\modules\log\forms;

use app\components\PaginationForm;
use yii\base\ArrayHelper;

/**
 * Class UserBehaviorListForm
 * @package app\modules\log\forms
 */
class UserBehaviorListForm extends PaginationForm
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $kolId;

    /**
     * @var 排期ID
     */
    public $scheduleId;

    /**
     * @var int[]
     */
    public $type;

    /**
     * 开始时间
     * ```
     * date('Y-m-d H:i:s')
     * ```
     * @var \DateTime
     */
    public $startTime;

    /**
     * 结束时间
     * ```
     * date('Y-m-d H:i:s')
     * ```
     * @var \DateTime
     */
    public $endTime;

    /**
     * @return array
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userId' => '用户',
            'kolId' => 'KOL',
            'scheduleId' => '排期',
            'type' => '操作记录类型',
            'startTime' => '开始时间',
            'endTime' => '结束时间',
        ]);
    }
}