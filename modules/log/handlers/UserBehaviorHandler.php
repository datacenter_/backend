<?php
namespace app\modules\log\handlers;

use app\modules\log\models\UserBehavior;
use app\constants\UserBehaviorType;
use app\modules\log\events\UserEvent;
use app\modules\log\Module;
use yii\base\Event;

/**
 * 用户行为事件处理
 * Class UserBehaviorHandler
 * @package app\modules\log\handlers
 */
class UserBehaviorHandler
{
    /**
     * 用户登录
     * @param UserEvent $event
     */
    public static function onAfterUserLogin(UserEvent $event)
    {
        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->identity->getId(),
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_LOGIN,
            'owner_id' => 0,
            'kol_id' => 0,
            'event_serialization' => serialize($event),
            'content' => ''
        ];
        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 用户注册
     * @param UserEvent $event
     */
    public static function onAfterUserRegister(UserEvent $event)
    {
        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->identity->getId(),
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_REGISTER,
            'owner_id' => 0,
            'kol_id' => 0,
            'event_serialization' => serialize($event),
            'content' => ''
        ];
        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 用户添加KOL
     * @param UserEvent $event
     */
    public static function onAfterUserAddKol(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->kol->user_id);

        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->kol->user_id,
            'kol_id' => $event->kol->id,
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_ADD_KOL,
            'owner_id' => $latestLoginRecord->id,
            'event_serialization' => serialize($event),
            'content' => sprintf('账号名称:%s;平台:%s', $event->kol->name, $event->kol->platformName)
        ];

        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 用户修改KOL
     * @param UserEvent $event
     */
    public static function onAfterUserUpdateKol(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->kol->user_id);

        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->kol->user_id,
            'kol_id' => $event->kol->id,
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_KOL,
            'owner_id' => $latestLoginRecord->id,
            'event_serialization' => serialize($event),
            'content' => sprintf('账号名称:%s;平台:%s', $event->kol->name, $event->kol->platformName)
        ];

        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 用户添加排期
     * @param UserEvent $event
     */
    public static function onAfterUserAddSchedule(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->schedule->kol->user_id);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!empty($event->schedule->businesses)) {
                foreach ($event->schedule->businesses as $business) {
                    $userBehavior = UserBehavior::instance(true);
                    $attributes = [
                        'user_id' => $event->schedule->kol->user_id,
                        'kol_id' => $event->schedule->kol->id,
                        'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_ADD_SCHEDULE,
                        'owner_id' => $latestLoginRecord->id,
                        'schedule_id' => $event->schedule->id,
                        'event_serialization' => serialize($event),
                        'content' => sprintf('账号名称:%s;客户名称:%s;投放时间:%s;广告内容:%s', $event->schedule->kol->name,
                            $business->customer->name,
                            $event->schedule->formattedScheduleTime,
                            $business->ad_instruction)
                    ];

                    $userBehavior->setAttributes($attributes, false);
                    $userBehavior->save();
                }
                $transaction->commit();
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * 用户修改排期时影响到添加、修改、删除排期
     * @param UserEvent $event
     */
    public static function onAfterUserUpdateSchedule(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->schedule->kol->user_id);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!empty($event->addBusinesses)) {
                foreach ($event->addBusinesses as $business) {
                    $userBehavior = UserBehavior::instance(true);
                    $attributes = [
                        'user_id' => $event->schedule->kol->user_id,
                        'kol_id' => $event->schedule->kol->id,
                        'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_ADD_SCHEDULE,
                        'owner_id' => $latestLoginRecord->id,
                        'schedule_id' => $event->schedule->id,
                        'event_serialization' => serialize($event),
                        'content' => sprintf('账号名称:%s;客户名称:%s;投放时间:%s;广告内容:%s', $event->schedule->kol->name,
                            $business->customer->name,
                            $event->schedule->formattedScheduleTime,
                            $business->ad_instruction)
                    ];

                    $userBehavior->setAttributes($attributes, false);
                    $userBehavior->save();
                }
            }

            if (!empty($event->updateBusinesses)) {
                foreach ($event->updateBusinesses as $business) {
                    $userBehavior = UserBehavior::instance(true);
                    $attributes = [
                        'user_id' => $event->schedule->kol->user_id,
                        'kol_id' => $event->schedule->kol->id,
                        'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_SCHEDULE,
                        'owner_id' => $latestLoginRecord->id,
                        'schedule_id' => $event->schedule->id,
                        'event_serialization' => serialize($event),
                        'content' => sprintf('账号名称:%s;客户名称:%s;投放时间:%s;广告内容:%s', $event->schedule->kol->name,
                            $business->customer->name,
                            $event->schedule->formattedScheduleTime,
                            $business->ad_instruction)
                    ];

                    $userBehavior->setAttributes($attributes, false);
                    $userBehavior->save();
                }
            }

            if (!empty($event->deletedBusinesses)) {
                foreach ($event->deletedBusinesses as $business) {
                    $userBehavior = UserBehavior::instance(true);
                    $attributes = [
                        'user_id' => $event->schedule->kol->user_id,
                        'kol_id' => $event->schedule->kol->id,
                        'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_DELETE_SCHEDULE,
                        'owner_id' => $latestLoginRecord->id,
                        'schedule_id' => $event->schedule->id,
                        'event_serialization' => serialize($event),
                        'content' => sprintf('账号名称:%s;客户名称:%s;投放时间:%s;广告内容:%s 记录删除', $event->schedule->kol->name,
                            $business->customer->name,
                            $event->schedule->formattedScheduleTime,
                            $business->ad_instruction)
                    ];

                    $userBehavior->setAttributes($attributes, false);
                    $userBehavior->save();
                }
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * 用户删除账号的一次排期
     * @param UserEvent $event
     */
    public static function onAfterUserDeleteAccountSchedule(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->schedule->kol->user_id);

        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->schedule->kol->user_id,
            'kol_id' => $event->schedule->kol->id,
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_DELETE_ACCOUNT_SCHEDULE,
            'owner_id' => $latestLoginRecord->id,
            'schedule_id' => $event->schedule->id,
            'event_serialization' => serialize($event),
            'content' => sprintf('账号名称:%s;投放时间:%s',
                $event->schedule->kol->name,
                $event->schedule->formattedScheduleTime),
        ];

        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 修改用户资料
     * @param UserEvent $event
     */
    public static function onAfterUserUpdateBaseInfo(UserEvent $event)
    {
        $latestLoginRecord = self::getLatestLoginOrRegisterRecordByUserId($event->userBaseInfo->user_id);

        $userBehavior = UserBehavior::instance();
        $attributes = [
            'user_id' => $event->userBaseInfo->user_id,
            'kol_id' => 0,
            'type' => UserBehaviorType::USER_BEHAVIOR_TYPE_UPDATE_BASE_INFO,
            'owner_id' => $latestLoginRecord->id,
            'event_serialization' => serialize($event),
            'content' => sprintf('头像:<a href="%s" target="_blank">%s;机构名称:%s</a>',
                $event->userBaseInfo->avatar,
                $event->userBaseInfo->avatar,
                $event->userBaseInfo->mcn_name),
        ];

        $userBehavior->setAttributes($attributes, false);
        $userBehavior->save();
    }

    /**
     * 最近一次登录/注册记录，作为owner_id
     * @param $userId
     * @return UserBehavior
     */
    private static function getLatestLoginOrRegisterRecordByUserId($userId)
    {
        return UserBehavior::getLatestOneByTypes($userId, [UserBehaviorType::USER_BEHAVIOR_TYPE_LOGIN, UserBehaviorType::USER_BEHAVIOR_TYPE_REGISTER]);
    }


}