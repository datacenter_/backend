<?php
namespace app\modules\log\models;

use app\constants\UserBehaviorType;
use yii\db\ActiveRecord;

/**
 * Class UserBehavior
 * @package app\modules\event\models
 * @property int $id
 * @property int $user_id
 * @property int $kol_id
 * @property int $owner_id 最近一次登录的日志ID
 * @property int $type
 * @property string $type_name
 * @property int $schedule_id
 * @property string $content
 * @property string $event_serialization
 * @property \DateTime $created_at
 * @property \DateTime $modified_at
 *
 * @property string $operationName
 */
class UserBehavior extends ActiveRecord
{
    public static function tableName()
    {
        return '{{kt_user_behavior_log}}';
    }

    public function init(){
        $this->on(static::EVENT_BEFORE_INSERT, function () {
            $this->type_name = $this->getOperationName();
        });
    }

    public function extraFields()
    {
        return [
            'operationName' => 'operationName',
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'KOL ID',
            'user_id' => '用户ID',
            'kol_id' => 'KOL ID',
            'owner_id' => '最近一次登录',
            'type' => '类型ID',
            'type_name' => '操作动作',
            'schedule_id' => '排期ID',
            'content' => '操作结果',
            'event_serialization' => '事件对象',
            'created_at' => '操作时间',
            'modified_at' => '修改时间',
        ];
    }

    public function getOperationName()
    {
        return static::getTypeName($this->type);
    }

    public static function getTypeName($type)
    {
        return UserBehaviorType::$userBehaviorTypeLabels[$type] ?? '未定义';
    }

    /**
     * 获取最近一条记录
     * @param int $userId
     * @param int[] $types
     * @return UserBehavior
     */
    public static function getLatestOneByTypes($userId, $types)
    {
        $types = (array)$types;
        $condition = [
            'type' => $types,
            'user_id' => $userId
        ];

        $orders = [
            'created_at' => SORT_DESC,
            'id' => SORT_DESC
        ];

        return static::find()->orderBy($orders)->onCondition($condition)->one();
    }
}