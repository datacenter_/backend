<?php
namespace app\modules\match\controllers;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Battle;
use app\modules\match\models\CurrencyInput;
use app\modules\match\models\Match;
use app\modules\match\models\MatchData;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\LolHero;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;

/**
 *
 */

class AutoController extends WithTokenAuthController
{
    public function actionAddTask()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return Match::add($attributes, $this->user->id);
    }

    public function actionChangeStatus()
    {

    }
}