<?php

namespace app\modules\match\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\CommonLogService;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Battle;
use app\modules\match\models\CurrencyInput;
use app\modules\match\models\Match;
use app\modules\match\models\MatchData;
use app\modules\match\services\BattleService;
use app\modules\match\services\BattleServiceBase;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\LolHero;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;

/**
 *
 */
class BattleController extends WithTokenAuthController
{
    public function actionAdd()
    {
        $gameType = $this->pPost('game_type');
        $dataType = $this->pPost('data_type');
        $data = $this->pPost('data');
        $data['order'] = $this->pPost('order');
        $matchId = $this->pPost('match_id');
        return BattleService::addBattleInfo($matchId, $gameType, $data, $dataType, Consts::USER_TYPE_ADMIN, $this->user->id);
    }

    public function actionEdit()
    {
        $gameType = $this->pPost('game_type');
        $dataType = $this->pPost('data_type');
        $data = $this->pPost('data');
        $battleId = $this->pPost('battle_id');
        return BattleService::setBattleInfo($battleId, $gameType, $data, $dataType, Consts::USER_TYPE_ADMIN, $this->user->id);
    }

    public function actionDetail()
    {
        $battleId = $this->pGet('id');
        $gameType = $this->pGet('game_type');
        return BattleService::getBattleInfo($battleId, $gameType);
    }

    public function actionList()
    {
        $matchId = $this->pGet('match_id');
        $gameType = $this->pGet('game_type');
        return BattleService::getBattleListByMatchId($matchId, $gameType);

    }

    public function actionDel()
    {
        $battleId = $this->pPost('battle_id');
        $gameType = $this->pPost('game_type');
        return BattleService::deleteBattle($gameType, $battleId);
    }

    public function actionRecovery()
    {
        $battleId = $this->pPost('battle_id');
        $gameType = $this->pPost('game_type');
        return BattleService::recoveryBattle($battleId,$gameType,Consts::USER_TYPE_ADMIN, $this->user->id);
    }

    //用于刷ESBattle
    public function actionRefreshBattle()
    {
        $userType = Consts::USER_TYPE_ADMIN;
        $adminId = $this->user->id;
        $params = $this->pPost();
        $logNewBattle['match_id'] = $params['match_id'];
        $logNewBattle['battle_id'] = $params['battle_id'];
        $diffInfo = [
            'diff' => [
                "Refresh" => 'Battle'
            ]
        ];

        try{
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_MAIN_INCREMENT,
                '',
                Consts::RESOURCE_TYPE_MATCH,
                'update',
                Consts::TAG_TYPE_CORE_DATA);
            $queueInfos= [
                "tag" => $tag,
                "params" => ["diff" => $diffInfo["diff"], "new" => $logNewBattle],
            ];
            // 添加到队列
            TaskRunner::addTask($queueInfos,1);
        }catch (\Exception $e){
            // 插入错误列表
            CommonLogService::recordException($e);
        }

    }
}