<?php

namespace app\modules\match\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Battle;
use app\modules\match\models\CurrencyInput;
use app\modules\match\models\Match;
use app\modules\match\models\MatchData;
use app\modules\match\models\SortingLog;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\LolHero;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use Redis;

/**
 *
 */
class Csgo5eplayMatchController extends WithTokenAuthController
{
    public function actionUpdatePrefectLogo()
    {
        $match_id     = $this->pPost('match_id');
        $prefect_logo = $this->pPost('prefect_logo');
        $flagArr      = [1, 2];
        if (!$match_id || !$prefect_logo || !in_array($prefect_logo, $flagArr)) {
            //return ['status' => 'failed', 'match_id' => $match_id, 'message' => 'match_id or prefect_logo is empty'];
            throw new BusinessException([], "match_id or prefect_logo is empty！");
        } else {
            $sortingLog = SortingLog::find()->where(['match_id' => $match_id])->one();
//            print_r($sortingLog);exit;
            if ($sortingLog) {
                $sortingLog->setAttribute('prefect_logo', $prefect_logo);
                if (!$sortingLog->team_1_log_name || !$sortingLog->team_2_log_name || !$sortingLog->service_ip) {
                    //return ['status' => 'failed', 'match_id' => $match_id, 'message' => '主队名称或者客队名称或ip地址未填写请完善'];
                    throw new BusinessException([], "主队名称或者客队名称或ip地址未填写请完善！");
                }
                $res = $sortingLog->save();
                if ($res) {
                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '修改成功'];
                } else {
                    //return ['status' => 'failed', 'match_id' => $match_id, 'message' => '修改失败'];
                    throw new BusinessException([], "修改失败！");
                }
            } else {
                //return ['status' => 'failed', 'match_id' => $match_id, 'message' => 'match not exist'];
                throw new BusinessException([], "比赛不存在！");
            }
        }

    }

    public function actionDoRefresh()
    {
        $match_id     = $this->pPost('match_id');
        $prefect_logo = $this->pPost('prefect_logo');
        $ip_arr = $this->pPost('ip_arr');
        if (!$match_id || $prefect_logo != 1 || !$ip_arr) {
            //return ['status' => 'failed', 'match_id' => $match_id, 'message' => 'match_id  prefect_logo is empty'];
            throw new BusinessException([], "match_id or prefect_logo is empty！");
        } else {
            $sortingLog = SortingLog::find()->where(['match_id' => $match_id])->one();
            if ($sortingLog) {
                foreach ($ip_arr as $k=>$v){
                    if (empty($v['service_ip'])){
                        unset($ip_arr[$k]);
                    }else{
                        $ip_arr[$k]['service_ip'] = trim($v['service_ip']);
                    }
                }
                //save
                $sortingLog->setAttribute('prefect_logo', $prefect_logo);
                $sortingLog->setAttribute('service_ip', json_encode($ip_arr));
                $res = $sortingLog->save();
                //ip,matchId
                $service_ip = json_decode($sortingLog->service_ip,true);
                $ip = array_column($service_ip,'service_ip');
//                $ip      = explode(';', $sortingLog->service_ip);
                $matchId = $sortingLog->getAttribute('match_id');
                if (!$matchId || empty($service_ip)) {
                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '启动失败'];
                } else {
                    foreach ($service_ip as $k1 => $v1) {
                        $search_ip = trim($v1['service_ip']);
                    }
                    $taskInfo  = TaskInfo::find()->andWhere(['tag'=>'event_ws.5eplay....refresh'])->andWhere(['and',['<', 'status', 3],['redis_server'=>$search_ip]])->andWhere(['>=','created_time',date("Y-m-d H:i:s", strtotime("-1 minute"))])->asArray()->one();
                    if (empty($taskInfo)) {
                        $match = Match::find()->andWhere(['id'=>$matchId])->asArray()->one();
                        $number = @$match['number_of_games']?@$match['number_of_games']:3;
                        $scheduled_begin_at = $match['scheduled_begin_at'];
                        $start_time = date('Y-m-d H:i:s', strtotime($scheduled_begin_at) - 1800);
                        $end_time   = date('Y-m-d H:i:s', strtotime($scheduled_begin_at) + 5400 * $number);
                        $where = ['or'];
                        foreach ($service_ip as $k => $v) {
                            $refresh_start_time = $v['refresh_start_time'] ? $v['refresh_start_time'] : $start_time;
                            $refresh_end_time   = $v['refresh_end_time'] ? $v['refresh_end_time'] : $end_time;
//                            $vArr = explode(';',$v['service_ip']);
                            $last_ip = trim($v['service_ip']);
                            $where[]           = [
                                'and',
                                ['in', 'ip_address', $v['service_ip']],
                                ['>=', 'created_at', $refresh_start_time],
                                ['<=', 'created_at', $refresh_end_time]
                            ];
                        }
                        $info = [
                            'tag'          => 'event_ws.5eplay....refresh',
                            'params'       => [
                                'where'          => $where,
                                'del_redis_keys' => 'ws:' . $matchId . ':',
                                'match_id'       => $matchId,
                            ],
                            'redis_server' => 'order_csgolog_'.$last_ip,
                        ];
                        \app\modules\task\services\TaskRunner::addTask($info, 9002);
                    }else{
                        return ['status' => 'success', 'match_id' => $match_id, 'message' => '已经有一个正在重刷中的程序,请等待'];
                    }

                }
                if ($res) {
                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '启动成功'];
                } else {
                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '启动失败'];
                }
            } else {
                //return ['status' => 'failed', 'match_id' => $match_id, 'message' => 'match not exist'];
                throw new BusinessException([], "match not exist！");
            }
        }
    }
}