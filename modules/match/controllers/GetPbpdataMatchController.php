<?php


namespace app\modules\match\controllers;


use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\match\models\Match;
use app\modules\match\services\PbpService;

class GetPbpdataMatchController extends RestController
{
    public function actionLiveMatch()
    {
        return PbpService::getUpComing();
    }

}
