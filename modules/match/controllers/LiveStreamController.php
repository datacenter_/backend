<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\match\models\Match;
use app\modules\match\models\MatchStreamList;
use app\modules\match\services\MatchService;
use app\modules\match\services\StreamService;
use app\modules\task\models\StandardDataMatchVideoList;
use app\rest\exceptions\BusinessException;

/**
 *
 */
class LiveStreamController extends WithTokenAuthController
{
    /**
     * 获取绑定比赛的数据源
     */
    public function actionList()
    {
        $params=$this->pGet();
        if(isset($params['match_id']) && $params['match_id']){
            return MatchService::getLiveSteamList($params);
        }
    }
    public function actionAdd()
    {
        $video_id= $this->pPost('video_id');
        $match_id= $this->pPost('match_id');
        if(isset($video_id) && isset($match_id) && $video_id && $match_id){
            return MatchService::addLiveSteam($video_id,$match_id);
        }
    }

    public function actionGetMatchLiveVideoUrl(){
        $endTime = date("Y-m-d H:i:s",time()+60*15);
        $nowTime = date("Y-m-d H:i:s",time());

        $match = Match::find()->alias('m')
            ->select("m.id as match_id")
            ->leftJoin('match_real_time_info as t','t.id = m.id')
            ->where(['and',
            ['>=','scheduled_begin_at',$nowTime],
            ['<=','scheduled_begin_at',$endTime]
        ])->orWhere(['status' =>2])->asArray()->all();
        if (empty($match)) return [];
        $newMach  = [];

        $lowk = 0;
        foreach ($match as $key=>$val) {
            $newData = MatchService::getLiveSteamListUrl($val);
            foreach ($newData as $k => $v) {
                $newMach[$lowk]['video_id'] = $v['video_id'];
                $newMach[$lowk]['origin_id'] = $v['origin_id'] ?: '';
                $newMach[$lowk]['live_url'] = $v['live_url'] ?: '';
                $lowk++;
            }
            $getListUrl = StreamService::getListUrl($val);
            foreach ($getListUrl as $j => $j) {
                $newMach[$lowk]['video_id'] = $j['video_id'];
                $newMach[$lowk]['origin_id'] = $j['origin_id'] ?: '';
                $newMach[$lowk]['live_url'] = $j['live_url'] ?: '';
                $lowk++;
            }

        }
        return $newMach;

    }
}