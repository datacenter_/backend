<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\common\services\Consts;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchLivedData;
use app\modules\match\services\BattleServiceBase;
use app\modules\match\services\MatchService;
use app\modules\match\services\MatchsocketService;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;

/**
 *
 */
class LivedController extends RestController
{
    //插入数据
    public function actionAdd2()
    {
        $data = file_get_contents("php://input");
        $data1 = json_decode($data,true);
        $origin_id = $data1['origin_id'];
        $game_id = $data1['game_id'];
        $rel_match_id = $data1['match_id'];
        $round = $data1['round'];
        $match_data = $data1['match_data'];
        $type = $data1['type'];
        $socket_time = $data1['socket_time'];
        $task = $data1['task'];

//        $origin_id = $this->pPost('origin_id');
//        $game_id = $this->pPost('game_id');
//        $rel_match_id= $this->pPost('match_id');
//        $round= $this->pPost('round');
//        $match_data = $this->pPost('match_data');
//        $type = $this->pPost('type');
//        $socket_time = $this->pPost('socket_time');//入库时间
//        $task = $this->pPost('task');//入是否添加并处理
        if (empty($origin_id) || empty($game_id)){
            throw new BusinessException([], '参数不能为空');
        }
        if (empty($socket_time)){
            $socket_time = date('Y-m-d H:i:s');
        }
        if($origin_id == 2 && $game_id == 3){//Abios dota 需要根据battleID 获取原始比赛ID
            //查询原始比赛ID
            $match_data_array = @json_decode($match_data,true);
            if ($match_data_array){
                $rel_battle_id = $match_data_array['payload']['match']['id'];
                $standardMatchInfo = StandardDataMatch::find()->where(['like','battlesIds',$rel_battle_id])->asArray()->one();
                $rel_match_id = @$standardMatchInfo['rel_identity_id'];
            }
        }
        if ($origin_id == 4 && $match_data == 'ping'){
            return true;
        }
        $insertData = [
            'origin_id' => $origin_id,
            'game_id' => $game_id,
            'match_id' => (String)$rel_match_id,
            'round' => (String)$round,
            'match_data' => $match_data,
            'type' => $type,
            'socket_time' => $socket_time
        ];
        $MatchLivedModel = new MatchLived();
        $MatchLivedModel->setAttributes($insertData);
        if ($MatchLivedModel->save()){
            //查看比赛的录入状态 true或者是false
            $auto_status = MatchService::getMatchConfigAndHotData($origin_id,$game_id,$rel_match_id,$socket_time);
            $socket_id = $MatchLivedModel->id;
            if ($task == 1 && in_array($game_id,[1,2,3]) && $auto_status){
                //添加到队列
                $insertData['socket_id'] = $socket_id;
                $newInsertData = @$insertData;
                $newInsertData['match_data']= @json_decode($match_data,true);
                //TODO 如果是CSGO 还要看怎么做
                $api_server = null;
                $socket_server = null;
                $taskType = 4;
                if ($game_id == 2){
                    $taskType = '9002';
                    if ($origin_id == 4){//玩家的是ws to api
                        $socket_server = 'order_lol_'.$rel_match_id;
                    }else{//现在是有3 ps
                        $api_server = 'order_lol_'.$rel_match_id;
                        $socket_server = 'order_lol_socket_'.$rel_match_id;
                    }
                }
                if ($game_id == 1){
                    if ($origin_id==7){
                        $socket_server = 'order_hltvcsgo_'.$rel_match_id;
                    }else{
                        //origin_id 3
                        $api_server = 'order_csgo_'.$rel_match_id;
                        $socket_server = 'order_csgo_'.$rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($api_server){
                    //设置队列
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $game_id),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                        "batch_id" => date("YmdHis"),
                        "params" => $newInsertData,
                        "redis_server"=>$api_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }
                if($socket_server){
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $game_id),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $newInsertData,
                        "redis_server"=>$socket_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }

            }
        }else{
            throw new BusinessException($MatchLivedModel->getErrors(), '插入数据库错误');
        }
        return true;
    }

    //暂时无用
    public function actionAdd()
    {
        $origin_id = $this->pPost('origin_id');
        $game_id = $this->pPost('game_id');
        $rel_match_id= $this->pPost('match_id');
        $round= $this->pPost('round');
        $match_data = $this->pPost('match_data');
        $type = $this->pPost('type');
        $socket_time = $this->pPost('socket_time');//入库时间
        $task = $this->pPost('task');//入是否添加并处理
        if (empty($origin_id) || empty($game_id)){
            throw new BusinessException([], '参数不能为空');
        }
        if (empty($socket_time)){
            $socket_time = date('Y-m-d H:i:s');
        }
        if($origin_id == 2 && $game_id == 3){//Abios dota 需要根据battleID 获取原始比赛ID
            //查询原始比赛ID
            $match_data_array = @json_decode($match_data,true);
            if ($match_data_array){
                $rel_battle_id = $match_data_array['payload']['match']['id'];
                $standardMatchInfo = StandardDataMatch::find()->where(['like','battlesIds',$rel_battle_id])->asArray()->one();
                $rel_match_id = @$standardMatchInfo['rel_identity_id'];
            }
        }
        if ($origin_id == 4 && $match_data == 'ping'){
            return true;
        }
        $insertData = [
            'origin_id' => $origin_id,
            'game_id' => $game_id,
            'match_id' => (String)$rel_match_id,
            'round' => (String)$round,
            'match_data' => $match_data,
            'type' => $type,
            'socket_time' => $socket_time
        ];
        $MatchLivedModel = new MatchLived();
        $MatchLivedModel->setAttributes($insertData);
        if ($MatchLivedModel->save()){
            //查看比赛的录入状态 true或者是false
            $auto_status = MatchService::getMatchConfigAndHotData($origin_id,$game_id,$rel_match_id,$socket_time);
            $socket_id = $MatchLivedModel->id;
            if ($task == 1 && in_array($game_id,[1,2,3]) && $auto_status){
                //添加到队列
                $insertData['socket_id'] = $socket_id;
                $newInsertData = @$insertData;
                $newInsertData['match_data']= json_decode($match_data,true);
                //TODO 如果是CSGO 还要看怎么做
                $api_server = null;
                $socket_server = null;
                $taskType = 4;
                if ($game_id == 2){
                    $taskType = '9002';
                    if ($origin_id == 4){//玩家的是ws to api
                        $socket_server = 'order_lol_'.$rel_match_id;
                    }else{//现在是有3 ps
                        $api_server = 'order_lol_'.$rel_match_id;
                        $socket_server = 'order_lol_socket_'.$rel_match_id;
                    }
                }
                if ($game_id == 1){
                    if ($origin_id==7){
                        $socket_server = 'order_hltvcsgo_'.$rel_match_id;
                    }else{
                        //origin_id 3
                        $api_server = 'order_csgo_'.$rel_match_id;
                        $socket_server = 'order_csgo_'.$rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($api_server){
                    //设置队列
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $game_id),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                        "batch_id" => date("YmdHis"),
                        "params" => $newInsertData,
                        "redis_server"=>$api_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }
                if($socket_server){
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $game_id),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $newInsertData,
                        "redis_server"=>$socket_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }

            }
        }else{
            throw new BusinessException($MatchLivedModel->getErrors(), '插入数据库错误');
        }
        return true;
    }
    //直接测试用
    public function actionAddJson()
    {
        $origin_id = $this->pPost('origin_id');
        $game_id = $this->pPost('game_id');
        $rel_match_id= $this->pPost('match_id');
        $round= $this->pPost('round');
        $match_data = $this->pPost('match_data');
        $type = $this->pPost('type');
        $socket_time = $this->pPost('socket_time');//入库时间
        $task = $this->pPost('task');//入是否添加并处理
        if (empty($origin_id) || empty($game_id)){
            throw new BusinessException([], '参数不能为空');
        }
        if (empty($socket_time)){
            $socket_time = date('Y-m-d H:i:s');
        }
        $insertData = [
            'origin_id' => $origin_id,
            'game_id' => $game_id,
            'match_id' => (String)$rel_match_id,
            'round' => (String)$round,
            'match_data' => $match_data,
            'type' => $type,
            'socket_time' => $socket_time
        ];
        $MatchLivedModel = new MatchLived();
        $MatchLivedModel->setAttributes($insertData);
        if ($MatchLivedModel->save()){
            if($task == 1){
                //查看比赛的录入状态 true或者是false
                $socket_id = $MatchLivedModel->id;
                //添加到队列
                $insertData['socket_id'] = $socket_id;
                $newInsertData = @$insertData;
                $newInsertData['match_data']= json_decode($match_data,true);
                $socket_server = 'order_'.$game_id.'_'.$rel_match_id;
                $item = [
                    "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                        $origin_id, QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA, "add", $game_id),
                    "type" => QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                    "batch_id" => date("YmdHis"),
                    "params" => $newInsertData,
                    "redis_server"=>$socket_server
                ];
                TaskRunner::addTask($item, 9002);
            }
        }else{
            throw new BusinessException($MatchLivedModel->getErrors(), '插入数据库错误');
        }
        return true;
    }
}