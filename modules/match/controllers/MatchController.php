<?php

namespace app\modules\match\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\models\OperationLog;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Battle;
use app\modules\match\models\CurrencyInput;
use app\modules\match\models\Match;
use app\modules\match\models\MatchData;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\LolHero;
use app\modules\org\models\Team;
use app\modules\org\services\PlayerService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;

/**
 *
 */
class MatchController extends WithTokenAuthController
{
    public function actionAdd()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return Match::add($attributes, $this->user->id);
    }

    public function actionEdit()
    {
        $attributes=$this->pPost();
        $basisId=$this->pPost('todo_id');

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::RESOURCE_TYPE_MATCH,Consts::TAG_TYPE_CORE_DATA);
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::RESOURCE_TYPE_MATCH,Consts::TAG_TYPE_CORE_DATA);
        }

        return MatchService::setMatch($attributes,Consts::USER_TYPE_ADMIN,$this->user->id,$basisId);
    }

    public function actionDetail()
    {
        $id = $this->pGet('id');
        return MatchService::getMatch($id);
    }

    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();

        return MatchService::getMatchList($params);
    }

    public function actionDel()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('status');
        if(isset($deleted) && $deleted == 1){
            $data = PlayerService::checkBing('match', $id);
            if (!$data) {
                throw new BusinessException([], '绑定有数据源的比赛不能删除！');
            }
            return Common::deletedMaster($id,Consts::RESOURCE_TYPE_MATCH,Consts::TAG_TYPE_CORE_DATA);
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::RESOURCE_TYPE_MATCH,Consts::TAG_TYPE_CORE_DATA);
        }
//        if ($deleted == Match::NO) {
//            $match = Match::find()->where(['id' => $id])->one();
//            $logOldInfo = $match->toArray();
//            $match->deleted = $deleted;
//            $match->deleted_at = null;
//            if ($match->update()) {
//                $logNewInfo = $match->toArray();
//                $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
//                if($diffInfo['changed']){
//                    OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                        Consts::RESOURCE_TYPE_MATCH,
//                        $id,
//                        ["diff" => $diffInfo['diff'], "new" => $logNewInfo],
//                        0,
//                        Consts::TAG_TYPE_BASE_DATA,
//                        null,
//                        0,
//                        Consts::USER_TYPE_ADMIN,
//                        $this->user->id
//                    );
//                }
//                return ['id' => $id, 'msg' => '恢复成功'];
//            }
//        }
//        //TODO 绑定有数据源的赛事不能删除
//        $data = PlayerService::checkBing('match', $id);
//        if (!$data) {
//            throw new BusinessException([], '绑定有数据源的比赛不能删除！');
//        }
//        $match = Match::find()->where(['id' => $id])->one();
//        $logOldInfo = $match->toArray();
//        $match->deleted = $deleted;
//        $match->deleted_at = date("Y-m-d H:i:s");
//        if ($match->update()) {
//            $logNewInfo = $match->toArray();
//            $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
//            if($diffInfo['changed']){
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                    Consts::RESOURCE_TYPE_MATCH,
//                    $id,
//                    ["diff" => $diffInfo['diff'], "new" => $logNewInfo],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//            }
//            return ['id' => $id, 'msg' => '删除成功'];
//        }
//        throw new BusinessException($match->getErrors(), '删除失败');

    }

    // 通用录入
    public function actionCreateInput()
    {
        $attributes = \Yii::$app->getRequest()->post();

        $match = new CurrencyInput();
        $match->setAttributes($attributes);
        if ($match->validate()) {
            return CurrencyInput::add($attributes);
        } else {
            throw new BusinessException($match->getErrors(), '参数信息错误');
        }
    }

    // lol录入
    public function actionGameInput()
    {
        $attributes = \Yii::$app->getRequest()->post();

        $match = new MatchData();
        $match->setAttributes($attributes);
        if ($match->validate()) {
            return MatchData::add($attributes);
        } else {
            throw new BusinessException($match->getErrors(), '参数信息错误');
        }
    }

    public function actionGetTournament()
    {
        $search_type = \Yii::$app->getRequest()->get('search_type','');
        $search_value = \Yii::$app->getRequest()->get('search_value','');
        $game_id = \Yii::$app->getRequest()->get('game_id');

        $tournament = Tournament::find()
            ->where(['deleted' => 2])
            ->andWhere(['type' => 1]);
        if (!empty($game_id) && $game_id) {
            $tournament->andWhere(['game' => $game_id]);
        }
        if($search_type == '') {
            $tournament->andWhere(['or',
                ['=', 'status', 1],
                ['=', 'status', 2]
            ])->andFilterWhere(['!=', 'status', 3]);
            if($search_value){
                $tournament->andWhere(['like', 'name', trim($search_value)]);
            }
        }
        if(!empty($search_type) && $search_type == "name"){
            $tournament->andWhere(['like', 'name', trim($search_value)]);
        }
        if (!empty($search_type) && $search_type == "id") {
            $tournament->andFilterWhere(['=', 'id', trim($search_value)]);
        }
        $tournament = $tournament->orderBy('scheduled_begin_at desc');
        $a =$tournament->createCommand()->getRawSql();
        $totalCount = $tournament->count();
//        $page = \Yii::$app->request->get('page', 1) - 1;
//        $pageSize = \Yii::$app->request->get('per_page', 20);
//
//        $pages = new Pagination([
//            'totalCount' => $totalCount,
//            'pageSize' => $pageSize,  // 分页默认条数是20条
//            'page' => $page,
//        ]);
        $model = $tournament->asArray()->all();

        return ['list' => $model, 'total' => $totalCount];
    }
    public function actionSearchTournament()
    {

        $searchType = \Yii::$app->getRequest()->get('search_type','id');
        $searchValue = \Yii::$app->getRequest()->get('search_value','');
        $game_id = \Yii::$app->getRequest()->get('game_id');

        $tournament = Tournament::find()
            ->where(['deleted' => 2])
            ->andWhere(['type' => 1]);
        if (!empty($searchType) && $searchType == "id"){
            $tournament->andFilterWhere(['=', $searchType, $searchValue]);
        }
        if (!empty($searchType) && $searchType) {
            $tournament->andFilterWhere(['like', $searchType, $searchValue]);
        }
        if (!empty($game_id) && $game_id) {
            $tournament->andWhere(['game' => $game_id]);
        }
        $tournament = $tournament->orderBy('scheduled_begin_at desc');
        $totalCount = $tournament->count();
        $model = $tournament->asArray()->all();

        return ['list' => $model, 'total' => $totalCount];
    }


    public function actionGetSonTournament()
    {
        $id = \Yii::$app->getRequest()->get('tournament_id');

        $sonTournament = TournamentRelationTree::find()
            ->where(['tournament_id' => $id])
            ->andWhere(['type' => 2])
            ->asArray()->all();
        $sonTreeIds = array_column($sonTournament, 'id');

        $tournament = Tournament::find()
            ->where(['deleted' => 2])
            ->andWhere(['type' => 2])
            ->andWhere(['in', 'tree_id', $sonTreeIds]);

        $tournament = $tournament->orderBy('id desc');
        $totalCount = $tournament->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $tournament->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }


    public function actionGetStage()
    {
        $id = \Yii::$app->getRequest()->get('tournament_id');

        $stage = TournamentRelationTree::find()
            ->where(['tournament_id' => $id])
            ->andWhere(['type' => 3])
            ->asArray()->all();
        $stage = array_column($stage, 'id');

        $stageObject = TournamentStage::find()
            ->where(['deleted' => 2])
            ->andWhere(['in', 'tree_id', $stage]);

        $stageObject = $stageObject->orderBy('id desc');
        $totalCount = $stageObject->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $stageObject->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public function actionGetGroup()
    {
        $id = \Yii::$app->getRequest()->get('tournament_id');

        $group = TournamentRelationTree::find()
            ->where(['tournament_id' => $id])
            ->andWhere(['type' => 4])
            ->asArray()->all();
        $group = array_column($group, 'id');

        $groupObject = TournamentGroup::find()
            ->where(['deleted' => 2])
            ->andWhere(['in', 'tree_id', $group]);

        $groupObject = $groupObject->orderBy('id desc');
        $totalCount = $groupObject->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $groupObject->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public function actionSearchTeam()
    {
        $groupId = \Yii::$app->getRequest()->get('group_id');

        $group = GroupTeamRelation::find()
            ->where(['group_id' => $groupId])
            ->asArray()->all();
        $teamIds = array_column($group, 'team_id');

        $teamObject = Team::find()
            ->Where(['in', 'id', $teamIds]);

        $teamObject = $teamObject->orderBy('name asc');
        $totalCount = $teamObject->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $teamObject->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public function actionGetInput()
    {
        $matchId = \Yii::$app->getRequest()->get('match_id');

        $inputInfo = CurrencyInput::find()
            ->alias('ci')
            ->select('ci.*,ms.*')
            ->leftJoin('match_status as ms', 'ms.match_id = ci.match_id')
            ->where(['ci.match_id' => $matchId])->asArray()->one();

        $battleInfo = Battle::find()
            ->select('battle.*,md.*,md.id as match_data_id')
            ->leftJoin('match_data as md', 'battle.id = md.battle_id')
            ->where(['battle.match_id' => $matchId])
            ->orderBy('battle.num asc')
            ->asArray()
            ->all();


        return ['input_info' => $inputInfo, 'battle_info' => $battleInfo];
    }

    public function actionGetMatch()
    {
        $matchId = \Yii::$app->getRequest()->get('match_id');

        $matchInfo = Match::find()->alias('m')
            ->select('m.*,mb.*,mb.id as match_base_id')
            ->leftJoin('match_base as mb', 'm.id = mb.match_id')
            ->where(['m.id' => $matchId])
            ->asArray()->all();
        $matchInfo['data_config'] = UpdateConfigService::getResourceUpdateConfigDetail($matchId, 'match');
        return $matchInfo;
    }

    public function actionGetHeroList()
    {
        $type = \Yii::$app->getRequest()->get('type');
        $banIds = \Yii::$app->getRequest()->get('ban_ids');

        // 1-lol 2-dota
        if ($type == 1) {
            $object = LolHero::find();

        } else {
            $object = DotaHero::find();
        }
        if (!empty($banIds) && $banIds) {
            $object->where(['not in', 'id', explode(',', $banIds)]);
        }

        $object->andWhere(['deleted' => 2]);
        $object = $object->orderBy('hero_name asc');
        $totalCount = $object->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $object->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public function actionRealtimeDetail()
    {
        $matchId = $this->pGet('match_id');
        return MatchService::getRealTimeInfo($matchId);
    }

    public function actionRealtimeSet()
    {
        $matchId = $this->pPost('match_id');
        $dataType = $this->pPost('data_type');
        $data = $this->pPost('data');
        $basisId=$this->pPost('todo_id');

        return MatchService::setRealTimeInfo($matchId, $data, $dataType,$basisId, Consts::USER_TYPE_ADMIN, $this->user->id);
    }

    /**
     * 获取比赛队员和战队相关信息
     * $sortingLogId        sorting_log表主键ID
     */
    public static function actionGetInfo()
    {
        $sortingLogId = \Yii::$app->getRequest()->get('sorting_log_id');
        return $info = MatchService::getLogMatchInfo($sortingLogId);
    }

    public function actionRefreshMatch(){
        $match_id = $this->pPost('match_id');
        $gameType = $this->pPost('game_type');
        $origin_id = $this->pPost('origin_id');
        $refresh_type = $this->pPost('refresh_type');
        $refresh_sub_type = $this->pPost('auto_add_sub_type');
        $operate_type = $this->pPost('operate_type',null);
        $result = "没有执行成功";



        if ($origin_id == 5) {
            $result = MatchService::refreshMatchInfoOnCsgoLog($match_id, $origin_id,$operate_type);
            return $result;
        }
        if ($origin_id == 7) {
            if ($refresh_type == 'socket'){
                $result = MatchService::refreshMatchInfoOnCsgoHltv($operate_type,$match_id, $origin_id,$gameType);
                return $result;
            }
            if ($refresh_type == 'restapi'){
                $result = MatchService::refreshMatchInfo($match_id, $origin_id, $gameType);
                return $result;
            }
        }
        if ($origin_id == 3){
            if ($refresh_type == 'restapi'){
                $result = MatchService::refreshMatchInfo($match_id,$origin_id,$gameType);
            }
            if ($refresh_type == 'socket'){
                $result = MatchService::refreshMatchInfoBySocketBtn($refresh_type,$operate_type,$match_id,$origin_id,$gameType);
            }
        }
        if ($origin_id == 2){
            if ($refresh_type == 'restapi'){
                $result = MatchService::refreshMatchInfo($match_id,$origin_id,$gameType);
            }
            if ($refresh_type == 'socket'){
                $result = [];//MatchService::refreshMatchInfoBySocketBtn($refresh_type,$operate_type,$match_id,$origin_id,$gameType);
            }
        }
        if ($origin_id == 4){
            if ($refresh_type == 'restapi'){
                $result = MatchService::refreshMatchInfo($match_id,$origin_id,$gameType);
            }
            if ($refresh_type == 'socket'){
                $result = MatchService::refreshMatchInfoBySocketBtn($refresh_type,$operate_type,$match_id,$origin_id,$gameType);
            }
        }
        if ($origin_id == 9 || $origin_id == 10){
//            if ($refresh_type == 'restapi'){
//                $result = MatchService::refreshMatchInfo($match_id,$origin_id,$gameType);
//            }
            if ($refresh_type == 'socket'){
                $result = MatchService::refreshMatchInfoBySocketBtn($refresh_type,$operate_type,$match_id,$origin_id,$gameType,$refresh_sub_type);
            }
        }
        $Operation_info =[
            "diff" => [
                'operate_type' => $operate_type,
                'match_id' => $match_id,
                'origin_id' => $origin_id,
                'gameType' => $gameType,
                'refresh_type' => $refresh_type,
                'refresh_sub_type' => $refresh_sub_type
            ]
        ];
        $version_key = OperationLogService::getVersionKey();
        $opt = new OperationLog();
        $opt->setAttributes([
            'type' => 'refresh_match',
            'version_key' => $version_key,
            'key_ext' => null,
            'user_type' => Consts::USER_TYPE_ADMIN,
            'user_id' => $this->user->id,
            'resource_type' => Consts::RESOURCE_TYPE_MATCH,
            'resource_id' => $match_id,
            'resource_ext_id' => 0,
            'tag' => null,
            'basis_id' => 0,
            'info' => @json_encode($Operation_info)
        ]);
        $opt->save();
        return $result;
    }
    public function actionEditDataSpeed()
    {
        $matchId = $this->pPost('match_id');
        $dataLevel = $this->pPost('data_level');
        $dataSpeed = $this->pPost('data_speed');
        return $info = MatchService::editDataSpeed($matchId,$dataLevel,$dataSpeed);
    }
    /**
     * 统计最近七天比赛场次
     */
    public function actionGetMatchCountByTime()
    {
        return MatchService::getMatchCountByTime();
    }
    public function actionGetMatchCountLately()
    {
        return MatchService::getMatchCountLately();
    }

    /**
     * 实时数据量
     */
    public function actionRealTimeData()
    {
        $type = $this->pGet('type',null);
        return MatchService::getRealTimeData($type);
    }

    public function actionMatchManyTeam()
    {
        return MatchService::getManyTeam();
    }

    public function actionEditErrorTeamByMatch()
    {
        $prams=$this->pPost();
        return MatchService::matchErrorTeamByEdit($prams,$this->user->id);
    }

}
