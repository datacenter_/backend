<?php

namespace app\modules\match\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\match\services\RoundService;

/**
 * GaoYu
 * 2020-09-27
 */
class RoundController extends WithTokenAuthController
{
    // 回合创建添加
    public function actionRoundAdd()
    {
        $battleId = \Yii::$app->getRequest()->post('battle_id');
        return RoundService::getRoundCreate($battleId);
    }
    // 回合删除
    public function actionRoundDel()
    {
        $roundId = \Yii::$app->getRequest()->post('round_id');
        $battleId = \Yii::$app->getRequest()->post('battle_id');
        return RoundService::getRoundDelete($battleId,$roundId);
    }
}