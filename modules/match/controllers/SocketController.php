<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\match\services\BattleServiceBase;
use app\modules\match\services\MatchsocketService;

/**
 * Class SocketController
 * @package app\modules\match\controllers
 */
class SocketController extends RestController
{
    /**
     * @return mixed
     * upcoming比赛列表
     */
    public function actionApilist()
    {
        $params=$this->pGet();
        return MatchsocketService::pageApilist($params);
    }
    public function actionApiget()
    {
        $params=$this->pPost();
        return MatchsocketService::apiget($params);
    }
    public function actionApiedit()
    {
        $params=$this->pPost();
        return MatchsocketService::apiEdit($params);
    }
    public function actionApiKillPidList(){
        $params=$this->pPost();
        return MatchsocketService::apiKillPidList($params);
    }
    public function actionApiConnectList(){
        $params=$this->pPost();
        return MatchsocketService::apiConnectList($params);
    }
    /**
     * @return mixed
     * upcoming比赛列表
     */
    public function actionList()
    {
        $params=$this->pGet();
        return MatchsocketService::pageList($params);
    }
    public function actionEdit()
    {
        $params = $this->pPost();
        return MatchsocketService::edit($params);
    }

    public function actionDetail()
    {
        $prams=$this->pGet();
        return MatchsocketService::detail($prams);
    }

    public function actionDel(){
        $prams=$this->pPost();
        return MatchsocketService::del($prams);
    }
    //获取lived 所有信息
    public function actionReceiveList()
    {
        $rel_match_id = $this->pGet('rel_match_id');
        $origin_id = $this->pGet('origin_id');
        $search_data = $this->pGet('search_data');
        $search_id = $this->pGet('search_id');
        return MatchsocketService::receiveList($origin_id,$rel_match_id,$search_data,$search_id);
    }
    //删除 match_lived_datas
    public function actionLivedDel(){
        $lived_id = $this->pPost('lived_id');
        return MatchsocketService::livedDel($lived_id);
    }
    //恢复 match_lived_datas
    public function actionLivedResume(){
        $lived_id = $this->pPost('lived_id');
        return MatchsocketService::livedResume($lived_id);
    }

    //重新 获取比赛的 socket   所有信息
    public function actionRegraspSocket()
    {
        $match_id = $this->pPost('match_id');
        $origin_id = $this->pPost('origin_id');
        $game_id = $this->pPost('game_id');
        return MatchsocketService::regraspSocket($origin_id,$match_id,$game_id);
    }

}
