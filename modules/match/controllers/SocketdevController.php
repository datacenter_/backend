<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\match\services\BattleServiceBase;
use app\modules\match\services\MatchsocketService;
use app\modules\match\services\MatchsocketServiceDev;

/**
 * Class SocketController
 * @package app\modules\match\controllers
 */
class SocketdevController extends RestController
{
    /**
     * @return mixed
     * upcoming比赛列表
     */
    public function actionApilist()
    {
        $params=$this->pGet();
        return MatchsocketServiceDev::pageApilist($params);
    }
    public function actionApiget()
    {
        $params=$this->pPost();
        return MatchsocketServiceDev::apiget($params);
    }
    public function actionApiedit()
    {
        $params=$this->pPost();
        return MatchsocketServiceDev::apiEdit($params);
    }
    public function actionApiKillPidList(){
        $params=$this->pPost();
        return MatchsocketServiceDev::apiKillPidList($params);
    }
    /**
     * @return mixed
     * upcoming比赛列表
     */
    public function actionList()
    {
        $params=$this->pGet();
        return MatchsocketServiceDev::pageList($params);
    }
    public function actionEdit()
    {
        $params = $this->pPost();
        return MatchsocketServiceDev::edit($params);
    }

    public function actionDetail()
    {
        $prams=$this->pGet();
        return MatchsocketServiceDev::detail($prams);
    }

    public function actionDel(){
        $prams=$this->pPost();
        return MatchsocketServiceDev::del($prams);
    }
}