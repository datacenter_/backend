<?php

namespace app\modules\match\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\match\services\SortlogService;

/**
 *
 */
class SortlogController extends WithTokenAuthController
{
    // 通过赛事或分组 获取比赛数据（分组优先级高）
    public function actionSortMatchList()
    {
        $params = \Yii::$app->getRequest()->get();
        return SortlogService::getMatchList($params);
    }
    // 日志分拣列表
    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();
        return SortlogService::getSortingLogList($params);
    }
    // 日志分拣添加
    public function actionSortAdd()
    {
        $params = \Yii::$app->getRequest()->post();
        return SortlogService::getSortingLogAdd($params);
    }
    // 日志分拣添加
    public function actionSortSelect()
    {
        $params = \Yii::$app->getRequest()->get();
        return SortlogService::getSortingLogSelect($params);
    }
    // 日志分拣修改
    public function actionSortEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return SortlogService::getSortingLogEdit($params);
    }
    // 日志分拣删除/恢复
    public function actionSortDelete()
    {
        $logId = \Yii::$app->getRequest()->post('log_id');
        $flag = \Yii::$app->getRequest()->post('flag');
        return SortlogService::getSortingLogDelete($logId,$flag);
    }
}