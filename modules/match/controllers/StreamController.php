<?php


namespace app\modules\match\controllers;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\data\models\DbSocket;
use app\modules\data\models\DbStream;
use app\modules\match\services\MatchService;
use app\modules\match\services\SortlogService;
use app\modules\match\services\StreamService;
use yii\db\Query;

class StreamController extends WithTokenAuthController
{
    public function actionEdit()
    {
        $params = $this->pPost();
        return StreamService::edit($params,Consts::USER_TYPE_ADMIN,$this->user->id);
//        return SortlogService::getSortingLogAdd($params);
    }

    public function actionDetail()
    {
        $prams=$this->pGet();
        return StreamService::detail($prams,Consts::USER_TYPE_ADMIN,$this->user->id);
    }

    public function actionList()
    {
        $params=$this->pGet();
        return StreamService::pageList($params,Consts::USER_TYPE_ADMIN,$this->user->id);
    }
    public function actionDel(){
        $prams=$this->pPost();
        return StreamService::del($prams);
    }


    public function actionGrabHltvOrigin(){

        return StreamService::grabHltvOrigin();
    }
    public function actionGrabOrigin(){

        return StreamService::grabOrigin();
    }


    public  function actionUpSaveUrl()
    {

        return StreamService::upSaveUrl();

    }

    /**
     * 获取视频来源
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetEnumVideoOrigin() {
        return StreamService::enumVideoOrigin();
    }

    public function actionGetStreamPlatform() {
        return StreamService::streamPlatform();

    }



}