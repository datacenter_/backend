<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\match\services\VideoOriginService;

/**
 *
 */
class VideoOriginController extends WithTokenAuthController
{
    // 通过赛事或分组 获取比赛数据（分组优先级高）
    public function actionVideoMatchList()
    {
        $params = \Yii::$app->getRequest()->get();
        return VideoOriginService::getMatchList($params);
    }
    // 视频比赛列表
    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();
        return VideoOriginService::getVideoOriginList($params);
    }
    // 视频比赛添加
    public function actionAdd()
    {
        $params = \Yii::$app->getRequest()->post();
        return VideoOriginService::getVideoOriginAdd($params);
    }
    // 日志分拣添加
    public function actionSelect()
    {
        $params = \Yii::$app->getRequest()->get();
        return VideoOriginService::getVideoSelect($params);
    }
    // 日志分拣修改
    public function actionEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return VideoOriginService::getVideoOriginEdit($params);
    }
    // 日志分拣删除/恢复
    public function actionDelete()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        return VideoOriginService::getVideoDelete($id,$deleted);
    }
}