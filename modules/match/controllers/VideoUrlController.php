<?php


namespace app\modules\match\controllers;


use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\common\models\EnumGame;
use app\modules\match\models\Match;
use app\modules\match\services\MatchService;
use app\modules\match\services\StreamService;

class VideoUrlController extends RestController
{
    /**
     * 用于给达雷的需要更新的url跳过token验证
     * @return array
     */
    public function actionGetMatchLiveVideoUrl(){
        $endTime = date("Y-m-d H:i:s",time()+60*15);
        $nowTime = date("Y-m-d H:i:s",time());

        $match = Match::find()->alias('m')
            ->select("m.id as match_id")
            ->leftJoin('match_real_time_info as t','t.id = m.id')
            ->where(['and',
                ['>=','scheduled_begin_at',$nowTime],
                ['<=','scheduled_begin_at',$endTime]
            ])->orWhere(['status' =>2])->asArray()->all();
        if (empty($match)) return [];
        $newMach  = [];
        $lowk = 0;

        foreach ($match as $key=>$val) {
            $getListUrl = StreamService::getListUrl($val);
            foreach ($getListUrl as $k => $j) {
                $newMach[$lowk]['video_id'] = $j['video_id'];
                $newMach[$lowk]['match_id'] = $j['match_id'];
                $newMach[$lowk]['origin_id'] = $j['origin_id'] ?: '';
                $newMach[$lowk]['live_url'] = $j['live_url'] ?: '';
                $newMach[$lowk]['gameName'] = $this->getGamebymatchId($j['match_id']);
//                $newMach[$lowk]['gameName']  = 'https://www.twitch.tv/tmcsgo';
                $url = strstr($newMach[$lowk]['live_url'],'https://www.twitch.tv/');
                if ($url) {
                    $newMach[$lowk]['live_url'] = $j['live_url']."--".$newMach[$lowk]['gameName'];
                }
                $lowk++;
            }
        }
        return $newMach;

    }

    public function getGamebymatchId($id){
        $gameId = Match::find()->select('game')->where(['id'=>$id])->asArray()->one();
        $gameName = EnumGame::find()->select('name')->where(['id'=>$gameId])->asArray()->one();
        return $gameName['name'];
    }

}