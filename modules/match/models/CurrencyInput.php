<?php

namespace app\modules\match\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class CurrencyInput extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency_input';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id','video_id','is_image_text','data_source_id','is_real','main_team_grade','out_team_grade'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function add($attribute)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $match = self::find()->where(['id' => $attribute['id']])->one();
            $match->setAttributes($attribute);
            if ($match->save() > 0 ) {

                // 录入状态
                $status = MatchStatus::find()->where(['match_id' => $attribute['match_id']])->one();
                $status->setAttributes($attribute);
                $status->save();

                if (isset($attribute['battle']) && !empty($attribute['battle'])) {
                    $battle = json_decode($attribute['battle'],true);

                    Battle::deleteAll(['match_id' => $attribute['match_id']]);
                    $model = new Battle();
                    foreach($battle as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('status_type',$val['status_type']);
                        $_model->setAttribute('confrontation_begin_date',$val['confrontation_begin_date']);
                        $_model->setAttribute('match_id', $attribute['match_id']);
                        $_model->setAttribute('num', $attribute['id']);
                        $_model->save();
                    }
                }

                return ['input_id' => $attribute['id'], 'msg' => '成功'];
            }else{
                throw new BusinessException($match->getErrors(),'创建失败');
            }
        } else {
            $match = new self();
            $match->setAttributes($attribute);
            if ($match->save() > 0 ) {

                // 录入状态
                $status = new MatchStatus();
                $status->setAttributes($attribute);
                $status->save();

                if (isset($attribute['battle']) && !empty($attribute['battle'])) {
                    $battle = json_decode($attribute['battle'],true);

                    $model = new Battle();
                    foreach($battle as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('status_type',$val['status_type']);
                        $_model->setAttribute('confrontation_begin_date',$val['confrontation_begin_date']);
                        $_model->setAttribute('match_id', $attribute['match_id']);
                        $_model->setAttribute('num', $attribute['id']);
                        $_model->save();
                    }
                }
                return ['input_id' => $match->id, 'msg' => '成功'];
                } else{
                throw new BusinessException($match->getErrors(),'创建失败');
            }

        }
    }
}
