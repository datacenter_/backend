<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "enum_stream_platform".
 *
 * @property int $id 平台id
 * @property string $name 平台名称
 * @property string|null $image
 * @property string|null $url
 */
class EnumStreamPlatform extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_stream_platform';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['image', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'url' => 'Url',
        ];
    }
}
