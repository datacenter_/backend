<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "enum_video_origin".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $desc
 * @property int|null $flag 1可有  2不可用
 */
class EnumVideoOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_video_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flag'], 'integer'],
            [['name', 'desc'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'flag' => 'Flag',
        ];
    }
}
