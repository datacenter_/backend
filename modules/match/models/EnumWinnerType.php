<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "enum_winner_type".
 *
 * @property int $id
 * @property string|null $winner_type
 * @property string|null $winner_type_cn
 * @property string|null $image
 */
class EnumWinnerType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_winner_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['winner_type', 'winner_type_cn'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'winner_type' => 'Winner Type',
            'winner_type_cn' => 'Winner Type Cn',
            'image' => 'Image',
        ];
    }
}
