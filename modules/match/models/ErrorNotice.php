<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "error_notice".
 *
 * @property int $id
 * @property int|null $resource_id
 * @property string|null $resource_type
 * @property int|null $battle_id
 * @property string|null $status 1-待处理
 * @property string|null $error_info
 * @property int|null $game_id
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class ErrorNotice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'error_notice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resource_id', 'battle_id', 'game_id','origin_id'], 'integer'],
            [['error_info'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['resource_type', 'status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resource_id' => 'Resource ID',
            'resource_type' => 'Resource Type',
            'battle_id' => 'Battle ID',
            'status' => 'Status',
            'error_info' => 'Error Info',
            'game_id' => 'Game ID',
            'origin_id' => 'Game ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
