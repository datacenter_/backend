<?php

namespace app\modules\match\models;

use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\match\services\MatchService;
use app\modules\task\services\Common;
use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class Match extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game','tournament_id','team_1_id','team_2_id', 'is_rescheduled','slug'
            ], 'required'],
            [['game','tournament_id','team_1_id','team_2_id','stage_id','group_id','son_tournament_id','deleted','cuser'
            ,'is_rescheduled','game_rules','match_rule_id','match_type','number_of_games'
            ], 'integer'],
            [['created_at', 'modified_at', 'deleted_at','slug','original_scheduled_begin_at','scheduled_begin_at','relation','team_version','scheduled_begin_at'
            ], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }

    public static function add($attribute, $userId)
    {
        // 修改 添加记录
        $logOld = [];
        $logNew = [];
        $operationType = '';
        if (isset($attribute['id']) && $attribute['id']) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $logOld =MatchService::getMergeMatch($attribute['id']);
            $match = self::find()->where(['id' => $attribute['id']])->one();
            $match->setAttributes($attribute);
            if ($match->save() > 0 ) {
                $matchBase = MatchBase::find()->where(['match_id' => $attribute['id']])->one();
                if (!$matchBase) {
                    $matchBase = new self();
                }
                $matchBase->setAttributes($attribute);
                $matchBase->save();
            }else{
                throw new BusinessException($match->getErrors(),'创建失败');
            }
        } else {
            $operationType = OperationLogService::OPERATION_TYPE_ADD;
            $match = new self();
            $match->setAttributes($attribute);
            $match->setAttribute('cuser',$userId);
            if ($match->save() > 0 ) {
                $matchBase = new MatchBase();
                $matchBase->setAttributes($attribute);
                $matchBase->setAttribute('match_id', $match->id);
                $matchBase->save();
                } else{
                throw new BusinessException($match->getErrors(),'创建失败');
            }

        }


        $logNew =MatchService::getMergeMatch($match->id);
        // 添加修改log
        $diffInfo = Common::getDiffInfo($logOld, $logNew,[],true);
        if ($diffInfo["changed"]) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_MATCH,
                $match->id,
                ["diff" => $diffInfo["diff"], "new" => $logNew],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                $userId
            );
        }
        return ['match_id' => $match->id, 'msg' => '成功'];
    }
}
