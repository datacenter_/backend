<?php

namespace app\modules\match\models;

use Yii;

class MatchBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id'], 'integer'],
            [['location', 'location_cn','round_order','round_name','round_name_cn','bracket_pos','description',
            'description_cn','game_version',
            ], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
