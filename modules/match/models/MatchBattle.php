<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle".
 *
 * @property int $id
 * @property int|null $game 游戏
 * @property int|null $match 比赛	
 * @property int|null $order 排序
 * @property string|null $begin_at 实际开始时间
 * @property string|null $end_at 实际结束时间
 * @property int|null $status 状态
 * @property int|null $is_draw 是否平局
 * @property int|null $is_forfeit 是否弃权
 * @property int|null $is_default_advantage 是否是默认领先获胜
 * @property int|null $map 比赛地图
 * @property string|null $duration 对局时长
 * @property int|null $winner 获胜战队
 * @property int|null $is_battle_detailed 是否有对局详情数据
 * @property int|null $flag 1正常2删除
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game', 'match', 'order', 'status', 'is_draw', 'is_forfeit', 'is_default_advantage', 'map', 'winner', 'is_battle_detailed', 'flag','deleted'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['begin_at', 'end_at'], 'string', 'max' => 60],
            [['duration'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'begin_at' => 'Begin At',
            'end_at' => 'End At',
            'status' => 'Status',
            'is_draw' => 'Is Draw',
            'is_forfeit' => 'Is Forfeit',
            'is_default_advantage' => 'Is Default Advantage',
            'map' => 'Map',
            'duration' => 'Duration',
            'winner' => 'Winner',
            'is_battle_detailed' => 'Is Battle Detailed',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted Time',
        ];
    }
}
