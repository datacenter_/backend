<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_banpick".
 *
 * @property int $id
 * @property int|null $battle_id 对局ID
 * @property int|null $team teamID
 * @property int|null $type ban:1,pick:2
 * @property int|null $order 排序
 * @property int|null $hero 英雄ID
 */
class MatchBattleBanpick extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_banpick';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'team', 'type', 'order', 'hero'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'team' => 'Team',
            'type' => 'Type',
            'order' => 'Order',
            'hero' => 'Hero',
            'created_at' => 'Created At',
        ];
    }
}
