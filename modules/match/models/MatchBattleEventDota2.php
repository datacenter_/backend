<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_event_dota2".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property int|null $event_id 事件ID
 * @property int|null $p_tid 战队编号
 * @property string|null $event_type 事件类型
 * @property int|null $ingame_timestamp 游戏内时间戳
 * @property int|null $position 地点
 * @property int|null $killer killer
 * @property string|null $killer_player_name killer名字
 * @property string|null $killer_player_rel_name killer原始名字
 * @property int|null $killer_champion_id 击杀英雄
 * @property string|null $killer_faction 击杀阵营
 * @property int|null $killer_ingame_obj_type 击杀者游戏内对象类型	
 * @property int|null $killer_sub_type 击杀者子类型	
 * @property int|null $victim 受害者
 * @property string|null $victim_player_name 受害者名字
 * @property string|null $victim_player_rel_name 受害者原始名字
 * @property int|null $victim_champion_id 受害者英雄
 * @property string|null $victim_faction 受害者阵营
 * @property int|null $victim_ingame_obj_type 受害者游戏内对象类型
 * @property int|null $victim_sub_type 受害者子类型
 * @property string|null $assist 助攻
 * @property int|null $assist_ingame_obj_type 助攻者游戏内对象类型	
 * @property int|null $assist_sub_type 助攻者子类型
 * @property int|null $is_first_event 是否为第一次特殊事件
 * @property int|null $first_event_type 第一次特殊事件类型
 * @property string|null $created_at 创建日期
 */
class MatchBattleEventDota2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_event_dota2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_steal','item_id','battle_id', 'game', 'match', 'order', 'event_id', 'p_tid', 'ingame_timestamp', 'killer', 'killer_champion_id', 'killer_ingame_obj_type', 'killer_sub_type', 'victim', 'victim_champion_id', 'victim_ingame_obj_type', 'victim_sub_type', 'assist_ingame_obj_type', 'assist_sub_type', 'is_first_event', 'first_event_type'], 'integer'],
            [['assist','killer_steam_id','victim_steam_id','victim_position','killer_position','rune_name','special_ability_name','ward_type','coverages'], 'string'],
            [['created_at'], 'safe'],
            [['event_type'], 'string', 'max' => 100],
            [['killer_player_name', 'killer_player_rel_name', 'victim_player_name', 'victim_player_rel_name'], 'string', 'max' => 255],
            [['position','killer_faction', 'victim_faction'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'event_id' => 'Event ID',
            'p_tid' => 'P Tid',
            'event_type' => 'Event Type',
            'ingame_timestamp' => 'Ingame Timestamp',
            'position' => 'Position',
            'killer' => 'Killer',
            'killer_player_name' => 'Killer Player Name',
            'killer_player_rel_name' => 'Killer Player Rel Name',
            'killer_champion_id' => 'Killer Champion ID',
            'killer_faction' => 'Killer Faction',
            'killer_ingame_obj_type' => 'Killer Ingame Obj Type',
            'killer_sub_type' => 'Killer Sub Type',
            'victim' => 'Victim',
            'victim_player_name' => 'Victim Player Name',
            'victim_player_rel_name' => 'Victim Player Rel Name',
            'victim_champion_id' => 'Victim Champion ID',
            'victim_faction' => 'Victim Faction',
            'victim_ingame_obj_type' => 'Victim Ingame Obj Type',
            'victim_sub_type' => 'Victim Sub Type',
            'assist' => 'Assist',
            'assist_ingame_obj_type' => 'Assist Ingame Obj Type',
            'assist_sub_type' => 'Assist Sub Type',
            'is_first_event' => 'Is First Event',
            'first_event_type' => 'First Event Type',
            'created_at' => 'Created At',
        ];
    }
}
