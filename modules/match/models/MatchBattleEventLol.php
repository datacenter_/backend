<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_event_lol".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property int|null $p_tid 战队编号
 * @property int|null $event_type 事件类型
 * @property int|null $ingame_timestamp 游戏内时间戳
 * @property int|null $position 地点
 * @property int|null $killer killer
 * @property int|null $killer_ingame_obj_type 击杀者游戏内对象类型	
 * @property int|null $killer_sub_type 击杀者子类型	
 * @property int|null $victim 受害者
 * @property int|null $victim_ingame_obj_type 受害者游戏内对象类型
 * @property int|null $victim_sub_type 受害者子类型
 * @property int|null $assist 助攻
 * @property int|null $assist_ingame_obj_type 助攻者游戏内对象类型	
 * @property int|null $assist_sub_type 助攻者子类型
 * @property int|null $is_first_event 是否为第一次特殊事件
 * @property int|null $first_event_type 第一次特殊事件类型
 * @property string|null $created_at 创建日期
 */
class MatchBattleEventLol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_event_lol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'game', 'match', 'order','event_id', 'p_tid', 'ingame_timestamp', 'killer', 'killer_champion_id','killer_ingame_obj_type', 'killer_sub_type', 'victim', 'victim_champion_id','victim_ingame_obj_type', 'victim_sub_type', 'assist_ingame_obj_type', 'assist_sub_type', 'is_first_event', 'first_event_type'], 'integer'],
            [['created_at'], 'safe'],
            [['assist','event_type','killer_faction','victim_faction','killer_player_name','killer_player_rel_name','victim_player_name','victim_player_rel_name','position','restore_to','spawn_time','other_info'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => '对局唯一ID标识',
            'game' => '游戏项目',
            'match' => '所属比赛',
            'order' => '排序',
            'p_tid' => '战队编号',
            'event_type' => '事件类型',
            'ingame_timestamp' => '游戏内时间戳',
            'position' => '地点',
            'killer' => 'killer',
            'killer_ingame_obj_type' => '击杀者游戏内对象类型	',
            'killer_sub_type' => '击杀者子类型	',
            'victim' => '受害者',
            'victim_ingame_obj_type' => '受害者游戏内对象类型',
            'victim_sub_type' => '受害者子类型',
            'assist' => '助攻',
            'assist_ingame_obj_type' => '助攻者游戏内对象类型	',
            'assist_sub_type' => '助攻者子类型',
            'is_first_event' => '是否为第一次特殊事件',
            'first_event_type' => '第一次特殊事件类型',
            'restore_to' => '回档到何时',
            'spawn_time' => '刷新时间',
            'other_info' => '其他信息',
            'created_at' => '创建日期',
        ];
    }
}
