<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_ext_csgo".
 *
 * @property int $id
 * @property int|null $is_confirmed 是否为官方完整数据
 * @property int|null $is_finished 是否已结束
 * @property int|null $is_pause 是否暂停
 * @property int|null $is_live 是否比赛正式数据
 * @property int|null $current_round 当前回合
 * @property int|null $is_bomb_planted 炸弹状态
 * @property string|null $win_round_1_side 第1回合获胜阵营
 * @property int|null $win_round_1_team 第1回合获胜战队
 * @property string|null $win_round_1_detail 特殊事件详情
 * @property string|null $win_round_16_side 第16回合获胜阵营
 * @property int|null $win_round_16_team 第16回合获胜战队
 * @property string|null $win_round_16_detail 特殊事件详情
 * @property string|null $first_to_5_rounds_wins_side 先获胜5回合阵营
 * @property int|null $first_to_5_rounds_wins_team 先获胜5回合战队
 * @property string|null $first_to_5_rounds_wins_detail 特殊事件详情
 * @property string|null $live_rounds 有效回合ID jason
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattleExtCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_ext_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [[
                'id', 'is_confirmed', 'is_finished', 'is_pause', 'is_live', 'current_round', 'is_bomb_planted', 'win_round_1_team', 'win_round_16_team', 'first_to_5_rounds_wins_team',
                'is_freeze_time',
                'in_round_timestamp',
                'round_time',
                'time_since_plant'
            ], 'integer'],
            [['win_round_1_detail', 'win_round_16_detail', 'first_to_5_rounds_wins_detail', 'live_rounds'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['win_round_1_side', 'win_round_16_side', 'first_to_5_rounds_wins_side'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_confirmed' => 'Is Confirmed',
            'is_finished' => 'Is Finished',
            'is_pause' => 'Is Pause',
            'is_live' => 'Is Live',
            'current_round' => 'Current Round',
            'is_bomb_planted' => 'Is Bomb Planted',
            'win_round_1_side' => 'Win Round 1 Side',
            'win_round_1_team' => 'Win Round 1 Team',
            'win_round_1_detail' => 'Win Round 1 Detail',
            'win_round_16_side' => 'Win Round 16 Side',
            'win_round_16_team' => 'Win Round 16 Team',
            'win_round_16_detail' => 'Win Round 16 Detail',
            'first_to_5_rounds_wins_side' => 'First To 5 Rounds Wins Side',
            'first_to_5_rounds_wins_team' => 'First To 5 Rounds Wins Team',
            'first_to_5_rounds_wins_detail' => 'First To 5 Rounds Wins Detail',
            'live_rounds' => 'Live Rounds',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
