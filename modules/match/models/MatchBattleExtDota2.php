<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_ext_dota2".
 *
 * @property int $id
 * @property int|null $is_confirmed 是否为官方完整数据
 * @property int|null $is_finished 是否已结束
 * @property string|null $ban_pick 英雄BP,1表示ban，2表示pick
 * @property int|null $first_blood_p_tid 一血战队编号
 * @property int|null $first_blood_time 一血时间
 * @property int|null $first_to_5_kills_p_tid 率先获得五个击杀战队编号
 * @property int|null $first_to_5_kills_time 5杀时间
 * @property int|null $first_to_10_kills_p_tid 率先获得十个击杀战队编号
 * @property int|null $first_to_10_kills_time 首十时间
 * @property int|null $first_tower_p_tid 首塔战队编号
 * @property int|null $first_tower_time 首塔时间
 * @property int|null $first_barracks_p_tid 首兵营战队编号
 * @property int|null $first_barracks_time 首兵营时间
 * @property int|null $first_roshan_p_tid 首肉山战队编号
 * @property int|null $first_roshan_time 首肉山时间
 * @property string|null $net_worth_diff_timeline 经济差时间线
 * @property string|null $experience_diff_timeline 经验差时间线
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建时间
 */
class MatchBattleExtDota2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_ext_dota2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'is_confirmed', 'is_finished', 'first_blood_p_tid', 'first_blood_time', 'first_to_5_kills_p_tid', 'first_to_5_kills_time', 'first_to_10_kills_p_tid', 'first_to_10_kills_time', 'first_tower_p_tid', 'first_tower_time', 'first_barracks_p_tid', 'first_barracks_time', 'first_roshan_p_tid', 'first_roshan_time',
                'is_pause',
                'is_live',

            ], 'integer'],
            [['ban_pick', 'net_worth_diff_timeline', 'experience_diff_timeline',
                'time_of_day',
                'elites_status',
                'first_blood_details',
                'first_to_5_kills_details',
                'first_to_10_kills',
                'first_tower',
                'first_barracks',
                'first_roshan',
                ], 'string'],
            [['modified_at', 'created_at'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_confirmed' => 'Is Confirmed',
            'is_finished' => 'Is Finished',
            'ban_pick' => 'Ban Pick',
            'first_blood_p_tid' => 'First Blood P Tid',
            'first_blood_time' => 'First Blood Time',
            'first_to_5_kills_p_tid' => 'First To 5 Kills P Tid',
            'first_to_5_kills_time' => 'First To 5 Kills Time',
            'first_to_10_kills_p_tid' => 'First To 10 Kills P Tid',
            'first_to_10_kills_time' => 'First To 10 Kills Time',
            'first_tower_p_tid' => 'First Tower P Tid',
            'first_tower_time' => 'First Tower Time',
            'first_barracks_p_tid' => 'First Barracks P Tid',
            'first_barracks_time' => 'First Barracks Time',
            'first_roshan_p_tid' => 'First Roshan P Tid',
            'first_roshan_time' => 'First Roshan Time',
            'net_worth_diff_timeline' => 'Net Worth Diff Timeline',
            'experience_diff_timeline' => 'Experience Diff Timeline',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
        ];
    }
}
