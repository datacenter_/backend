<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_ext_lol".
 *
 * @property int $id
 * @property int|null $is_confirmed 是否为官方完整数据
 * @property int|null $is_pause 是否暂停
 * @property int|null $is_finished 是否已结束
 * @property int|null $is_live 是否比赛正式数据
 * @property string|null $ban_pick 英雄BP,1表示ban，2表示pick
 * @property int|null $first_blood_p_tid 一血战队编号
 * @property int|null $first_blood_time 一血时间
 * @property int|null $first_to_5_kills_p_tid 率先获得五个击杀战队编号
 * @property int|null $first_to_5_kills_time 5杀时间
 * @property int|null $first_to_10_kills_p_tid 率先获得十个击杀战队编号
 * @property int|null $first_to_10_kills_time 首十时间
 * @property int|null $first_turret_p_tid 首塔战队编号
 * @property int|null $first_turret_time 首塔时间
 * @property int|null $first_inhibitor_p_tid 首水晶战队编号
 * @property int|null $first_inhibitor_time 首水晶时间
 * @property int|null $first_rift_herald_p_tid 首峡谷先锋战队编号
 * @property int|null $first_rift_herald_time 首峡谷先锋时间
 * @property int|null $first_dragon_p_tid 首元素巨龙战队编号
 * @property int|null $first_dragon_time 首元素巨龙时间
 * @property int|null $first_baron_nashor_p_tid 首纳什男爵战队编号
 * @property int|null $first_baron_nashor_time 首纳什男爵时间
 * @property int|null $first_elder_dragon_p_tid 首远古巨龙战队编号
 * @property int|null $first_elder_dragon_time 首远古巨龙时间
 * @property string|null $gold_diff_timeline 经济差时间线
 * @property string|null $experience_diff_timeline 经验差时间线
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建时间
 * @property string|null $faction
 * @property string|null $first_blood_detail
 * @property string|null $first_to_5_detail
 * @property string|null $first_to_10_detail
 * @property string|null $first_turret_detail
 * @property string|null $first_inhibitor_detail
 * @property string|null $first_rift_herald_detail
 * @property string|null $first_dragon_detail
 * @property string|null $first_baron_nashor_detail
 * @property string|null $first_elder_dragon_detail
 */
class MatchBattleExtLol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_ext_lol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_confirmed', 'is_pause', 'is_finished', 'is_live', 'first_blood_p_tid', 'first_blood_time', 'first_to_5_kills_p_tid', 'first_to_5_kills_time', 'first_to_10_kills_p_tid', 'first_to_10_kills_time', 'first_turret_p_tid', 'first_turret_time', 'first_inhibitor_p_tid', 'first_inhibitor_time', 'first_rift_herald_p_tid', 'first_rift_herald_time', 'first_dragon_p_tid', 'first_dragon_time', 'first_baron_nashor_p_tid', 'first_baron_nashor_time', 'first_elder_dragon_p_tid', 'first_elder_dragon_time'], 'integer'],
            [['ban_pick', 'gold_diff_timeline', 'experience_diff_timeline', 'first_blood_detail', 'first_to_5_detail', 'first_to_10_detail', 'first_turret_detail', 'first_inhibitor_detail', 'first_rift_herald_detail', 'first_dragon_detail', 'first_baron_nashor_detail', 'first_elder_dragon_detail','elites_status'], 'string'],
            [['modified_at', 'created_at'], 'safe'],
            [['faction'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_confirmed' => 'Is Confirmed',
            'is_pause' => 'Is Pause',
            'is_finished' => 'Is Finished',
            'is_live' => 'Is Live',
            'ban_pick' => 'Ban Pick',
            'first_blood_p_tid' => 'First Blood P Tid',
            'first_blood_time' => 'First Blood Time',
            'first_to_5_kills_p_tid' => 'First To 5 Kills P Tid',
            'first_to_5_kills_time' => 'First To 5 Kills Time',
            'first_to_10_kills_p_tid' => 'First To 10 Kills P Tid',
            'first_to_10_kills_time' => 'First To 10 Kills Time',
            'first_turret_p_tid' => 'First Turret P Tid',
            'first_turret_time' => 'First Turret Time',
            'first_inhibitor_p_tid' => 'First Inhibitor P Tid',
            'first_inhibitor_time' => 'First Inhibitor Time',
            'first_rift_herald_p_tid' => 'First Rift Herald P Tid',
            'first_rift_herald_time' => 'First Rift Herald Time',
            'first_dragon_p_tid' => 'First Dragon P Tid',
            'first_dragon_time' => 'First Dragon Time',
            'first_baron_nashor_p_tid' => 'First Baron Nashor P Tid',
            'first_baron_nashor_time' => 'First Baron Nashor Time',
            'first_elder_dragon_p_tid' => 'First Elder Dragon P Tid',
            'first_elder_dragon_time' => 'First Elder Dragon Time',
            'gold_diff_timeline' => 'Gold Diff Timeline',
            'experience_diff_timeline' => 'Experience Diff Timeline',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'faction' => 'Faction',
            'first_blood_detail' => 'First Blood Detail',
            'first_to_5_detail' => 'First To 5 Detail',
            'first_to_10_detail' => 'First To 10 Detail',
            'first_turret_detail' => 'First Turret Detail',
            'first_inhibitor_detail' => 'First Inhibitor Detail',
            'first_rift_herald_detail' => 'First Rift Herald Detail',
            'first_dragon_detail' => 'First Dragon Detail',
            'first_baron_nashor_detail' => 'First Baron Nashor Detail',
            'first_elder_dragon_detail' => 'First Elder Dragon Detail',
        ];
    }
}
