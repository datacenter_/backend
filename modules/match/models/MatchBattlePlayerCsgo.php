<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_player_csgo".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property int|null $team_order 存1或2主队客队
 * @property int|null $is_pause 是否暂停
 * @property int|null $is_live 是否比赛正式数据
 * @property int|null $team_id 战队ID
 * @property int|null $player_id 选手ID
 * @property string|null $nick_name 选手昵称
 * @property int|null $kills 击杀
 * @property int|null $headshot_kills 爆头击杀
 * @property int|null $deaths 死亡
 * @property int|null $k_d_diff 击杀死亡差
 * @property int|null $assists 助攻
 * @property int|null $flash_assists 闪光弹助攻
 * @property string|null $adr 平均每局伤害
 * @property int|null $first_kills 首先击杀
 * @property int|null $first_deaths 首先死亡
 * @property string|null $first_kills_diff 首杀差
 * @property int|null $multi_kills 多杀
 * @property int|null $1_v_n_clutches 1VN残局获胜
 * @property int|null $knife_kill 刀杀
 * @property int|null $ace_kill ACE
 * @property string|null $kast KAST
 * @property string|null $rating Rating 2.0
 * @property int|null $primary_weapon 主武器ID
 * @property int|null $secondary_weapon 副武器ID
 * @property int|null $knive 刀ID
 * @property int|null $grenades 投掷物ID
 * @property int|null $kevlar 防弹衣ID
 * @property int|null $helmet 头盔ID
 * @property int|null $defusekit 拆弹器ID
 * @property int|null $C4 C4炸弹ID
 * @property int|null $hp 血量
 * @property int|null $is_alive 存活状态
 * @property int|null $money 金钱
 * @property string|null $coordinate 坐标
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattlePlayerCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_player_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'battle_id', 'game', 'match', 'order', 'team_order', 'is_pause', 'is_live', 'team_id', 'player_id', 'kills', 'headshot_kills', 'deaths', 'k_d_diff', 'assists', 'flash_assists', 'first_kills', 'first_deaths', 'multi_kills', '1_v_n_clutches', 'knife_kill', 'ace_kill', 'primary_weapon', 'secondary_weapon', 'knive',  'kevlar', 'helmet', 'defusekit', 'C4', 'hp', 'is_alive', 'money',
                    'damage',
                    'team_damage',
                    'damage_taken',
                    'hegrenade_damage_taken',
                    'inferno_damage_taken',
                    'planted_bomb',
                    'defused_bomb',
                    'chicken_kills',
                    'team_kills',
                    'one_kills',
                    'two_kills',
                    'three_kills',
                    'four_kills',
                    'five_kills',
                    'one_on_one_clutches',
                    'one_on_two_clutches',
                    'one_on_three_clutches',
                    'one_on_four_clutches',
                    'one_on_five_clutches',
                    'hit_generic',
                    'hit_head',
                    'hit_chest',
                    'hit_stomach',
                    'hit_left_arm',
                    'hit_right_arm',
                    'hit_left_leg',
                    'hit_right_leg',
                    'awp_kills',
                    'knife_kills',
                    'taser_kills',
                    'shotgun_kills',
                    'has_kevlar',
                    'has_helmet',
                    'has_defusekit',
                    'has_bomb',
                    'equipment_value',
                    'ping',
                ], 'integer'],
            [[ 'blind_enemy_time','blind_teammate_time','steam_id',
                'side',
                'weapon',
                'position',
                'blinded_time',
                'grenades',
                ], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['nick_name', 'adr', 'first_kills_diff', 'kast', 'rating', 'coordinate'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'team_order' => 'Team Order',
            'is_pause' => 'Is Pause',
            'is_live' => 'Is Live',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'nick_name' => 'Nick Name',
            'kills' => 'Kills',
            'headshot_kills' => 'Headshot Kills',
            'deaths' => 'Deaths',
            'k_d_diff' => 'K D Diff',
            'assists' => 'Assists',
            'flash_assists' => 'Flash Assists',
            'adr' => 'Adr',
            'first_kills' => 'First Kills',
            'first_deaths' => 'First Deaths',
            'first_kills_diff' => 'First Kills Diff',
            'multi_kills' => 'Multi Kills',
            '1_v_n_clutches' => '1 V N Clutches',
            'knife_kill' => 'Knife Kill',
            'ace_kill' => 'Ace Kill',
            'kast' => 'Kast',
            'rating' => 'Rating',
            'primary_weapon' => 'Primary Weapon',
            'secondary_weapon' => 'Secondary Weapon',
            'knive' => 'Knive',
            'grenades' => 'Grenades',
            'kevlar' => 'Kevlar',
            'helmet' => 'Helmet',
            'defusekit' => 'Defusekit',
            'C4' => 'C4',
            'hp' => 'Hp',
            'is_alive' => 'Is Alive',
            'money' => 'Money',
            'coordinate' => 'Coordinate',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
