<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_player_dota2".
 *
 * @property int $id
 * @property int|null $order 排序
 * @property string|null $rel_identity_id 原始选手id
 * @property string|null $rel_team_id 数据源team_id
 * @property int|null $team_order 此order存1或2表示主队客队
 * @property int|null $seed 选手排序
 * @property int|null $battle_id
 * @property int|null $game 游戏
 * @property int|null $match 比赛
 * @property int|null $team_id 战队ID
 * @property string|null $faction 阵营
 * @property int|null $role 角色
 * @property int|null $lane 分路
 * @property int|null $player_id 选手id,咱家的id
 * @property string|null $nick_name 选手昵称
 * @property string|null $rel_nick_name 数据源选手昵称
 * @property int|null $hero 英雄
 * @property int|null $level 等级
 * @property int|null $alive 存活状态
 * @property int|null $ultimate_cd 大招状态
 * @property string|null $coordinate 坐标
 * @property int|null $kills 击杀
 * @property int|null $double_kill 双杀
 * @property int|null $triple_kill 三杀
 * @property int|null $ultra_kill 四杀
 * @property int|null $rampage 五杀
 * @property int|null $largest_multi_kill 最大多杀
 * @property int|null $largest_killing_spree 最大连杀
 * @property int|null $deaths 死亡
 * @property int|null $assists 助攻
 * @property string|null $kda KDA
 * @property string|null $participation 参团率
 * @property int|null $last_hits 补刀得分
 * @property int|null $lane_creep_kills 小兵击击杀数
 * @property int|null $neutral_creep_kills 野怪击杀总数
 * @property int|null $neutral_minion_team_jungle_kills 本方野怪击杀数
 * @property int|null $neutral_minion_enemy_jungle_kills 敌方野怪击杀数
 * @property string|null $lhpm 分均补刀
 * @property int|null $denies 反补
 * @property int|null $net_worth 财产总和
 * @property int|null $gold_earned 金币获取
 * @property int|null $gold_spent 已花费金币
 * @property int|null $gold_remaining 剩余金币
 * @property string|null $gpm 分均金钱
 * @property string|null $gold_earned_percent 经济占比
 * @property int|null $experience 总经验
 * @property string|null $xpm 分均经验
 * @property int|null $damage_to_heroes 对英雄伤害
 * @property string|null $dpm_to_heroes 对英雄分均伤害
 * @property string|null $damage_percent_to_heroes 对英雄伤害占比
 * @property int|null $damage_taken 承受伤害
 * @property string|null $dtpm 分均承受伤害
 * @property string|null $damage_taken_percent 承伤占比
 * @property int|null $damage_to_heroes_hp_removal 对英雄生命移除
 * @property int|null $damage_to_heroes_magical 对英雄魔法伤害
 * @property int|null $damage_to_heroes_physical 对英雄物理伤害
 * @property int|null $damage_by_hero_pure_dmg 纯粹伤害
 * @property int|null $damage_by_mobs_hp_removal 召唤物生命移除
 * @property int|null $damage_by_mobs_magical_dmg 召唤物魔法伤害
 * @property int|null $damage_by_mobs_physical_dmg 召唤物物理伤害
 * @property int|null $damage_by_mobs_pure_dmg 召唤物纯粹伤害
 * @property int|null $damage_to_towers 对防御塔伤害
 * @property int|null $damage_from_heroes_hp_removal 来自英雄的生命移除
 * @property int|null $damage_from_heroes_magical_dmg 来自英雄的魔法伤害
 * @property int|null $damage_from_heroes_physical_dmg 来自英雄的物理伤害
 * @property int|null $damage_from_heroes_pure_dmg 来自英雄的纯粹伤害
 * @property int|null $damage_from_mobs_hp_removal 来自召唤物的生命移除
 * @property int|null $damage_from_mobs_magical_dmg 来自召唤物魔法伤害
 * @property int|null $damage_from_mobs_physical_dmg 来自召唤物物理伤害
 * @property int|null $damage_from_mobs_pure_dmg 来自召唤物纯粹伤害
 * @property string|null $damage_conversion_rate 伤害转化率
 * @property int|null $total_heal 总治疗量
 * @property int|null $total_crowd_control_time 总控制时长
 * @property int|null $wards_purchased 买眼
 * @property int|null $wards_placed 插眼
 * @property int|null $wards_kills 排眼
 * @property int|null $observer_wards_purchased 购买侦查守卫
 * @property int|null $observer_wards_placed 放置侦查守卫
 * @property int|null $observer_wards_kills 摧毁侦查守卫
 * @property int|null $sentry_wards_purchased 购买岗哨守卫
 * @property int|null $sentry_wards_placed 放置岗哨守卫
 * @property int|null $sentry_wards_kills 摧毁岗哨守卫
 * @property int|null $camps_stacked 堆野数
 * @property int|null $runes_detail_double_damage_runes 双倍伤害神符
 * @property int|null $runes_detail_haste_runes 极速神符
 * @property int|null $runes_detail_illusion_runes 幻象神符
 * @property int|null $runes_detail_invisibility_runes 隐身神符
 * @property int|null $runes_detail_regeneration_runes 恢复神符
 * @property int|null $runes_detail_bounty_runes 赏金神符
 * @property int|null $runes_detail_arcane_runes 奥术神符
 * @property string|null $items 装备
 * @property string|null $summoner_spells 召唤师技能
 * @property string|null $abilities_timeline 技能升级时间线
 * @property string|null $items_timeline 道具升级时间线
 * @property string|null $modified_at 更新时间
 * @property string|null $created_at 创建时间
 * @property string|null $deleted_at 删除日期
 */
class MatchBattlePlayerDota2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_player_dota2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'team_order', 'seed', 'battle_id', 'game', 'match', 'team_id', 'role', 'lane', 'player_id', 'hero', 'level', 'is_alive', 'ultimate_cd', 'kills', 'double_kill', 'triple_kill', 'ultra_kill', 'rampage', 'largest_multi_kill', 'largest_killing_spree', 'deaths', 'assists', 'last_hits', 'lane_creep_kills', 'neutral_creep_kills', 'neutral_minion_team_jungle_kills', 'neutral_minion_enemy_jungle_kills', 'denies', 'net_worth', 'gold_earned', 'gold_spent', 'gold_remaining', 'experience', 'damage_to_heroes', 'damage_taken', 'damage_to_heroes_hp_removal', 'damage_to_heroes_magical', 'damage_to_heroes_physical', 'damage_to_heroes_pure', 'damage_by_mobs_hp_removal', 'damage_by_mobs_magical_dmg', 'damage_by_mobs_physical_dmg', 'damage_by_mobs_pure_dmg', 'damage_to_towers', 'damage_from_heroes_hp_removal', 'damage_from_heroes_magical_dmg', 'damage_from_heroes_physical_dmg', 'damage_from_heroes_pure_dmg', 'damage_from_mobs_hp_removal', 'damage_from_mobs_magical_dmg', 'damage_from_mobs_physical_dmg', 'damage_from_mobs_pure_dmg', 'total_heal', 'total_crowd_control_time', 'wards_purchased', 'wards_placed', 'wards_kills', 'observer_wards_purchased', 'observer_wards_placed', 'observer_wards_kills', 'sentry_wards_purchased', 'sentry_wards_placed', 'sentry_wards_kills', 'camps_stacked', 'runes_detail_double_damage_runes', 'runes_detail_haste_runes', 'runes_detail_illusion_runes', 'runes_detail_invisibility_runes', 'runes_detail_regeneration_runes', 'runes_detail_bounty_runes', 'runes_detail_arcane_runes',
                'is_visible',
                'respawntimer',
                'buyback_cooldown',
                'buyback_cost',
                'health',
                'health_max',
                'tower_kills',
                'barrack_kills',
                'melee_barrack_kills',
                'ranged_barrack_kills',
                'roshan_kills',
                'gold_reliable',
                'gold_unreliable',
                'gold_herokill',
                'gold_creepkill',
                'total_runes_pickedup',
                'bounty_runes_pickedup',
                'double_damage_runes_pickedup',
                'illusion_runes_pickedup',
                'invisibility_runes_pickedup',
                'regeneration_runes_pickedup',
                'arcane_runes_pickedup',
                'smoke_purchased',
                'smoke_used',
                'dust_purchased',
                'dust_used',
            ], 'integer'],
            [['items', 'summoner_spells', 'abilities_timeline', 'items_timeline',
                'net_worth_percent',
                'damage_taken_hp_removal',
                'damage_taken_magical',
                'damage_taken_pure',
                'damage_to_buildings',
                'damage_to_objectives',
                'steam_id',
                'talents'
            ], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['rel_identity_id', 'rel_team_id', 'faction', 'nick_name', 'rel_nick_name'], 'string', 'max' => 255],
            [['coordinate'], 'string', 'max' => 200],
            [['kda', 'participation', 'lhpm', 'gpm', 'gold_earned_percent', 'dpm_to_heroes', 'damage_percent_to_heroes', 'dtpm', 'damage_taken_percent', 'damage_conversion_rate'], 'string', 'max' => 20],
            [['xpm'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'rel_identity_id' => 'Rel Identity ID',
            'rel_team_id' => 'Rel Team ID',
            'team_order' => 'Team Order',
            'seed' => 'Seed',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'team_id' => 'Team ID',
            'faction' => 'Faction',
            'role' => 'Role',
            'lane' => 'Lane',
            'player_id' => 'Player ID',
            'nick_name' => 'Nick Name',
            'rel_nick_name' => 'Rel Nick Name',
            'hero' => 'Hero',
            'level' => 'Level',
            'is_alive' => 'Alive',
            'ultimate_cd' => 'Ultimate Cd',
            'coordinate' => 'Coordinate',
            'kills' => 'Kills',
            'double_kill' => 'Double Kill',
            'triple_kill' => 'Triple Kill',
            'ultra_kill' => 'Quadra Kill',
            'rampage' => 'Penta Kill',
            'largest_multi_kill' => 'Largest Multi Kill',
            'largest_killing_spree' => 'Largest Killing Spree',
            'deaths' => 'Deaths',
            'assists' => 'Assists',
            'kda' => 'Kda',
            'participation' => 'Participation',
            'last_hits' => 'Last Hits',
            'lane_creep_kills' => 'Lane Creep Kills',
            'neutral_creep_kills' => 'Neutral Creep Kills',
            'neutral_minion_team_jungle_kills' => 'Neutral Minion Team Jungle Kills',
            'neutral_minion_enemy_jungle_kills' => 'Neutral Minion Enemy Jungle Kills',
            'lhpm' => 'Lhpm',
            'denies' => 'Denies',
            'net_worth' => 'Net Worth',
            'gold_earned' => 'Gold Earned',
            'gold_spent' => 'Gold Spent',
            'gold_remaining' => 'Gold Remaining',
            'gpm' => 'Gpm',
            'gold_earned_percent' => 'Gold Earned Percent',
            'experience' => 'Experience',
            'xpm' => 'Xpm',
            'damage_to_heroes' => 'Damage To Heroes',
            'dpm_to_heroes' => 'Dpm To Heroes',
            'damage_percent_to_heroes' => 'Damage Percent To Heroes',
            'damage_taken' => 'Damage Taken',
            'dtpm' => 'Dtpm',
            'damage_taken_percent' => 'Damage Taken Percent',
            'damage_to_heroes_hp_removal' => 'Damage By Hero Hp Removal',
            'damage_to_heroes_magical' => 'Damage By Hero Magical Dmg',
            'damage_to_heroes_physical' => 'Damage By Hero Physical Dmg',
            'damage_to_heroes_pure' => 'Damage By Hero Pure Dmg',
            'damage_by_mobs_hp_removal' => 'Damage By Mobs Hp Removal',
            'damage_by_mobs_magical_dmg' => 'Damage By Mobs Magical Dmg',
            'damage_by_mobs_physical_dmg' => 'Damage By Mobs Physical Dmg',
            'damage_by_mobs_pure_dmg' => 'Damage By Mobs Pure Dmg',
            'damage_to_towers' => 'Damage To Towers',
            'damage_from_heroes_hp_removal' => 'Damage From Heroes Hp Removal',
            'damage_from_heroes_magical_dmg' => 'Damage From Heroes Magical Dmg',
            'damage_from_heroes_physical_dmg' => 'Damage From Heroes Physical Dmg',
            'damage_from_heroes_pure_dmg' => 'Damage From Heroes Pure Dmg',
            'damage_from_mobs_hp_removal' => 'Damage From Mobs Hp Removal',
            'damage_from_mobs_magical_dmg' => 'Damage From Mobs Magical Dmg',
            'damage_from_mobs_physical_dmg' => 'Damage From Mobs Physical Dmg',
            'damage_from_mobs_pure_dmg' => 'Damage From Mobs Pure Dmg',
            'damage_conversion_rate' => 'Damage Conversion Rate',
            'total_heal' => 'Total Heal',
            'total_crowd_control_time' => 'Total Crowd Control Time',
            'wards_purchased' => 'Wards Purchased',
            'wards_placed' => 'Wards Placed',
            'wards_kills' => 'Wards Kills',
            'observer_wards_purchased' => 'Observer Wards Purchased',
            'observer_wards_placed' => 'Observer Wards Placed',
            'observer_wards_kills' => 'Observer Wards Kills',
            'sentry_wards_purchased' => 'Sentry Wards Purchased',
            'sentry_wards_placed' => 'Sentry Wards Placed',
            'sentry_wards_kills' => 'Sentry Wards Kills',
            'camps_stacked' => 'Camps Stacked',
            'runes_detail_double_damage_runes' => 'Runes Detail Double Damage Runes',
            'runes_detail_haste_runes' => 'Runes Detail Haste Runes',
            'runes_detail_illusion_runes' => 'Runes Detail Illusion Runes',
            'runes_detail_invisibility_runes' => 'Runes Detail Invisibility Runes',
            'runes_detail_regeneration_runes' => 'Runes Detail Regeneration Runes',
            'runes_detail_bounty_runes' => 'Runes Detail Bounty Runes',
            'runes_detail_arcane_runes' => 'Runes Detail Arcane Runes',
            'items' => 'Items',
            'summoner_spells' => 'Summoner Spells',
            'abilities_timeline' => 'Abilities Timeline',
            'items_timeline' => 'Items Timeline',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
