<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_player_lol".
 *
 * @property int $id
 * @property int|null $order 排序
 * @property string|null $rel_identity_id 原始选手id
 * @property string|null $rel_team_id 数据源team_id
 * @property int|null $team_order 此order存1或2表示主队客队
 * @property int|null $seed 选手排序
 * @property int|null $battle_id
 * @property int|null $game 游戏
 * @property int|null $match 比赛
 * @property int|null $team_id 战队ID
 * @property string|null $faction 阵营
 * @property int|null $role 角色
 * @property int|null $lane 分路
 * @property int|null $player_id 选手id,咱家的id
 * @property string|null $nick_name 选手昵称
 * @property string|null $rel_nick_name 数据源选手昵称
 * @property int|null $champion 英雄
 * @property int|null $level 等级
 * @property int|null $alive 存活状态
 * @property string|null $ultimate_cd 大招状态
 * @property string|null $coordinate 坐标
 * @property int|null $kills 击杀
 * @property int|null $double_kill 双杀
 * @property int|null $triple_kill 三杀
 * @property int|null $quadra_kill 四杀
 * @property int|null $penta_kill 五杀
 * @property int|null $largest_multi_kill 最大多杀
 * @property int|null $largest_killing_spree 最大连杀
 * @property int|null $deaths 死亡
 * @property int|null $assists 助攻
 * @property string|null $kda KDA
 * @property string|null $participation 参团率
 * @property int|null $cs 补刀得分
 * @property string|null $minion_kills 小兵击击杀数
 * @property string|null $total_neutral_minion_kills 野怪击杀总数
 * @property string|null $neutral_minion_team_jungle_kills 本方野怪击杀数
 * @property string|null $neutral_minion_enemy_jungle_kills 敌方野怪击杀数
 * @property string|null $cspm 分均补刀
 * @property int|null $gold_earned 金币获取
 * @property int|null $gold_spent 已花费金币
 * @property int|null $gold_remaining 剩余金币
 * @property string|null $gpm 分均金钱
 * @property string|null $gold_earned_percent 经济占比
 * @property int|null $experience 总经验
 * @property string|null $xpm 分均经验
 * @property string|null $damage_to_champions 对英雄伤害
 * @property string|null $damage_to_champions_physical 对英雄物理伤害
 * @property string|null $damage_to_champions_magic 对英雄魔法伤害
 * @property string|null $damage_to_champions_true 对英雄真实伤害
 * @property string|null $dpm_to_champions 对英雄分均伤害
 * @property string|null $damage_percent_to_champions 对英雄伤害占比
 * @property string|null $damage_to_towers 对防御塔伤害
 * @property string|null $damage_taken 承受伤害
 * @property string|null $damage_taken_physical 承受物理伤害
 * @property string|null $damage_taken_magic 承受魔法伤害
 * @property string|null $damage_taken_true 承受真实伤害
 * @property string|null $dtpm 分均承受伤害
 * @property string|null $damage_taken_percent 承伤占比
 * @property string|null $damage_conversion_rate 伤害转化率
 * @property string|null $total_heal 总治疗量
 * @property string|null $total_crowd_control_time 总控制时长
 * @property int|null $wards_purchased 买眼
 * @property int|null $wards_placed 插眼
 * @property int|null $wards_kills 排眼
 * @property int|null $warding_totems_purchased 购买监视图腾
 * @property int|null $warding_totems_placed 放置监视图通
 * @property int|null $warding_totems_kills 摧毁监控图腾
 * @property int|null $control_wards_purchased 购买控制守卫
 * @property int|null $control_wards_placed 放置控制守卫
 * @property int|null $control_wards_kills 摧毁控制守卫
 * @property string|null $total_damage 总伤害
 * @property string|null $total_damage_physical 总物理伤害
 * @property string|null $total_damage_magic 总魔法伤害
 * @property string|null $total_damage_true 总真实伤害
 * @property string|null $items 装备
 * @property string|null $summoner_spells 召唤师技能
 * @property string|null $runes 符文
 * @property string|null $abilities_timeline 技能升级时间线
 * @property string|null $items_timeline 道具升级时间线
 * @property string|null $modified_at 更新时间
 * @property string|null $created_at 创建时间
 * @property string|null $deleted_at 删除日期
 * @property string|null $hp 血量
 * @property string|null $keystone 基石符文
 * @property string|null $respawntimer 复活时长
 * @property int|null $health 当前血量
 * @property int|null $health_max 总血量
 * @property string|null $damage_selfmitigated 伤害减免
 * @property string|null $damage_shielded_on_teammates 给队友套盾减伤
 * @property string|null $damage_to_buildings 对建筑物伤害
 * @property string|null $damage_to_objectives 对争夺目标伤害
 * @property string|null $total_crowd_control_time_others 对其他目标控制时长
 * @property string|null $vision_score 视野得分
 */
class MatchBattlePlayerLol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_player_lol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'team_order', 'seed', 'battle_id', 'game', 'match', 'team_id', 'role', 'lane', 'player_id', 'champion', 'level', 'alive', 'kills', 'double_kill', 'triple_kill', 'quadra_kill', 'penta_kill', 'largest_multi_kill', 'largest_killing_spree', 'deaths', 'assists', 'cs', 'gold_earned', 'gold_spent', 'gold_remaining', 'experience', 'wards_purchased', 'wards_placed', 'wards_kills', 'warding_totems_purchased', 'warding_totems_placed', 'warding_totems_kills', 'control_wards_purchased', 'control_wards_placed', 'control_wards_kills', 'health', 'health_max','turret_kills','inhibitor_kills','rift_herald_kills','dragon_kills','baron_nashor_kills'], 'integer'],
            [['items', 'summoner_spells', 'runes', 'abilities_timeline', 'items_timeline'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['rel_identity_id', 'rel_team_id', 'faction', 'nick_name', 'rel_nick_name', 'ultimate_cd', 'coordinate', 'kda', 'participation', 'minion_kills', 'total_neutral_minion_kills', 'neutral_minion_team_jungle_kills', 'neutral_minion_enemy_jungle_kills', 'cspm', 'gpm', 'gold_earned_percent', 'xpm', 'damage_to_champions', 'damage_to_champions_physical', 'damage_to_champions_magic', 'damage_to_champions_true', 'dpm_to_champions', 'damage_percent_to_champions', 'damage_to_towers', 'damage_taken', 'damage_taken_physical', 'damage_taken_magic', 'damage_taken_true', 'dtpm', 'damage_taken_percent', 'damage_conversion_rate', 'total_heal', 'total_crowd_control_time', 'total_damage', 'total_damage_physical', 'total_damage_magic', 'total_damage_true', 'hp', 'keystone', 'respawntimer', 'damage_selfmitigated', 'damage_shielded_on_teammates', 'damage_to_buildings', 'damage_to_objectives', 'total_crowd_control_time_others', 'vision_score'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'rel_identity_id' => 'Rel Identity ID',
            'rel_team_id' => 'Rel Team ID',
            'team_order' => 'Team Order',
            'seed' => 'Seed',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'team_id' => 'Team ID',
            'faction' => 'Faction',
            'role' => 'Role',
            'lane' => 'Lane',
            'player_id' => 'Player ID',
            'nick_name' => 'Nick Name',
            'rel_nick_name' => 'Rel Nick Name',
            'champion' => 'Champion',
            'level' => 'Level',
            'alive' => 'Alive',
            'ultimate_cd' => 'Ultimate Cd',
            'coordinate' => 'Coordinate',
            'kills' => 'Kills',
            'double_kill' => 'Double Kill',
            'triple_kill' => 'Triple Kill',
            'quadra_kill' => 'Quadra Kill',
            'penta_kill' => 'Penta Kill',
            'largest_multi_kill' => 'Largest Multi Kill',
            'largest_killing_spree' => 'Largest Killing Spree',
            'deaths' => 'Deaths',
            'assists' => 'Assists',
            'kda' => 'Kda',
            'participation' => 'Participation',
            'cs' => 'Cs',
            'minion_kills' => 'Minion Kills',
            'total_neutral_minion_kills' => 'Total Neutral Minion Kills',
            'neutral_minion_team_jungle_kills' => 'Neutral Minion Team Jungle Kills',
            'neutral_minion_enemy_jungle_kills' => 'Neutral Minion Enemy Jungle Kills',
            'cspm' => 'Cspm',
            'gold_earned' => 'Gold Earned',
            'gold_spent' => 'Gold Spent',
            'gold_remaining' => 'Gold Remaining',
            'gpm' => 'Gpm',
            'gold_earned_percent' => 'Gold Earned Percent',
            'experience' => 'Experience',
            'xpm' => 'Xpm',
            'damage_to_champions' => 'Damage To Champions',
            'damage_to_champions_physical' => 'Damage To Champions Physical',
            'damage_to_champions_magic' => 'Damage To Champions Magic',
            'damage_to_champions_true' => 'Damage To Champions True',
            'dpm_to_champions' => 'Dpm To Champions',
            'damage_percent_to_champions' => 'Damage Percent To Champions',
            'damage_to_towers' => 'Damage To Towers',
            'damage_taken' => 'Damage Taken',
            'damage_taken_physical' => 'Damage Taken Physical',
            'damage_taken_magic' => 'Damage Taken Magic',
            'damage_taken_true' => 'Damage Taken True',
            'dtpm' => 'Dtpm',
            'damage_taken_percent' => 'Damage Taken Percent',
            'damage_conversion_rate' => 'Damage Conversion Rate',
            'total_heal' => 'Total Heal',
            'total_crowd_control_time' => 'Total Crowd Control Time',
            'wards_purchased' => 'Wards Purchased',
            'wards_placed' => 'Wards Placed',
            'wards_kills' => 'Wards Kills',
            'warding_totems_purchased' => 'Warding Totems Purchased',
            'warding_totems_placed' => 'Warding Totems Placed',
            'warding_totems_kills' => 'Warding Totems Kills',
            'control_wards_purchased' => 'Control Wards Purchased',
            'control_wards_placed' => 'Control Wards Placed',
            'control_wards_kills' => 'Control Wards Kills',
            'total_damage' => 'Total Damage',
            'total_damage_physical' => 'Total Damage Physical',
            'total_damage_magic' => 'Total Damage Magic',
            'total_damage_true' => 'Total Damage True',
            'items' => 'Items',
            'summoner_spells' => 'Summoner Spells',
            'runes' => 'Runes',
            'abilities_timeline' => 'Abilities Timeline',
            'items_timeline' => 'Items Timeline',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'hp' => 'Hp',
            'keystone' => 'Keystone',
            'respawntimer' => 'Respawntimer',
            'health' => 'Health',
            'health_max' => 'Health Max',
            'damage_selfmitigated' => 'Damage Selfmitigated',
            'damage_shielded_on_teammates' => 'Damage Shielded On Teammates',
            'damage_to_buildings' => 'Damage To Buildings',
            'damage_to_objectives' => 'Damage To Objectives',
            'total_crowd_control_time_others' => 'Total Crowd Control Time Others',
            'vision_score' => 'Vision Score',
            'turret_kills' => 'turret_kills',
            'inhibitor_kills' => 'inhibitor_kills',
            'rift_herald_kills' => 'rift_herald_kills',
            'dragon_kills' => 'dragon_kills',
            'baron_nashor_kills' => 'baron_nashor_kills',
        ];
    }
}
