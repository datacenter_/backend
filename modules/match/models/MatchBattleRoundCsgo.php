<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_round_csgo".
 *
 * @property int $id
 * @property int|null $battle_id 对局id
 * @property int|null $round_ordinal 回合序号
 * @property int|null $winner_team_id 获胜id
 * @property int|null $winner_end_type 获胜方式
 * @property string|null $created_at
 */
class MatchBattleRoundCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_round_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'round_ordinal', 'winner_team_id', 'winner_end_type'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'round_ordinal' => 'Round Ordinal',
            'winner_team_id' => 'Winner Team ID',
            'winner_end_type' => 'Winner End Type',
            'created_at' => 'Created At',
        ];
    }
}
