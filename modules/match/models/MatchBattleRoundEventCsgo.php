<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_round_event_csgo".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property string|null $event_type 事件类型
 * @property int|null $team_id 回合触发特殊事件后的战队ID
 * @property int|null $round_ordinal 回合序号
 * @property string|null $in_round_timestamp 回合内时间戳
 * @property string|null $round_time 回合内时间
 * @property int|null $is_bomb_planted 炸弹是否已安放
 * @property string|null $time_since_plant 安放炸弹后时间
 * @property int|null $killer 击杀者ID
 * @property string|null $killer_nick_name 击杀者选手昵称 优先使用数据源给的昵称
 * @property string|null $killer_side 击杀者阵营
 * @property string|null $killer_position 击杀者位置
 * @property int|null $victim 受害者ID
 * @property string|null $victim_nick_name 受害者选手昵称 优先使用数据源给的昵称
 * @property string|null $victim_side 受害者阵营
 * @property string|null $victim_position 受害者位置
 * @property int|null $assist 助攻
 * @property string|null $assist_nick_name 助攻选手昵称 优先使用数据源给的昵称
 * @property string|null $assist_side 助攻阵营
 * @property int|null $flashassist 闪光弹助攻player_id
 * @property string|null $flashassistt_nick_name 闪光弹助攻选手昵称
 * @property string|null $flashassist_side 闪光弹助攻阵营
 * @property int|null $weapon 武器道具ID
 * @property int|null $damage 伤害
 * @property string|null $hit_group 击杀命中部位
 * @property int|null $is_headshot 是否为爆头击杀
 * @property int|null $round_end_type round_end- 回合获胜方式 - 有码表
 * @property string|null $winner_side round_end- 获胜方阵营
 * @property int|null $ct_score round_end- CT比分
 * @property int|null $t_score round_end- T比分
 * @property int|null $bomb_planted_player_id 炸弹安放- 选手ID
 * @property string|null $bomb_planted_nick_name 炸弹安放- 选手昵称
 * @property string|null $bomb_planted_side 炸弹安放- 阵营
 * @property string|null $bomb_planted_position 炸弹安放- 位置
 * @property int|null $bomb_defused_player_id 炸弹拆除- 选手ID
 * @property string|null $bomb_defused_nick_name 炸弹拆除- 选手昵称
 * @property string|null $bomb_defused_side 炸弹拆除- 阵营
 * @property string|null $bomb_defused_position 炸弹拆除- 位置
 * @property string|null $bomb_exploded_time 炸弹爆炸- 时间戳
 * @property string|null $created_at 创建日期
 */
class MatchBattleRoundEventCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_round_event_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['survived_players_ct','survived_players_t','battle_id', 'game', 'match', 'order', 'team_id', 'round_ordinal', 'is_bomb_planted', 'killer', 'victim', 'assist', 'flashassist', 'weapon', 'damage', 'is_headshot', 'round_end_type', 'ct_score', 't_score', 'bomb_planted_player_id', 'bomb_defused_player_id','suicide_player_id','event_id'], 'integer'],
            [['created_at'], 'safe'],
            [['event_type', 'killer_nick_name', 'killer_side', 'killer_position', 'victim_nick_name', 'victim_side', 'victim_position', 'assist_nick_name', 'assist_side', 'flashassistt_nick_name', 'flashassist_side', 'hit_group'], 'string', 'max' => 255],
            [['special_description','in_round_timestamp', 'round_time', 'time_since_plant', 'bomb_planted_nick_name', 'bomb_planted_position', 'bomb_defused_nick_name', 'bomb_defused_position','suicide_nick_name','suicide_side','suicide_position','weapon_name'], 'string', 'max' => 100],
            [['winner_side', 'bomb_planted_side', 'bomb_defused_side'], 'string', 'max' => 30],
            [['bomb_exploded_time','killer_steam_id','victim_steam_id','assist_steam_id','flashassist_steam_id','steam_id'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'event_type' => 'Event Type',
            'team_id' => 'Team ID',
            'round_ordinal' => 'Round Ordinal',
            'in_round_timestamp' => 'In Round Timestamp',
            'round_time' => 'Round Time',
            'is_bomb_planted' => 'Is Bomb Planted',
            'time_since_plant' => 'Time Since Plant',
            'killer' => 'Killer',
            'killer_nick_name' => 'Killer Nick Name',
            'killer_side' => 'Killer Side',
            'killer_position' => 'Killer Position',
            'victim' => 'Victim',
            'victim_nick_name' => 'Victim Nick Name',
            'victim_side' => 'Victim Side',
            'victim_position' => 'Victim Position',
            'assist' => 'Assist',
            'assist_nick_name' => 'Assist Nick Name',
            'assist_side' => 'Assist Side',
            'flashassist' => 'Flashassist',
            'flashassistt_nick_name' => 'Flashassistt Nick Name',
            'flashassist_side' => 'Flashassist Side',
            'weapon' => 'Weapon',
            'damage' => 'Damage',
            'hit_group' => 'Hit Group',
            'is_headshot' => 'Is Headshot',
            'round_end_type' => 'Round End Type',
            'winner_side' => 'Winner Side',
            'ct_score' => 'Ct Score',
            't_score' => 'T Score',
            'bomb_planted_player_id' => 'Bomb Planted Player ID',
            'bomb_planted_nick_name' => 'Bomb Planted Nick Name',
            'bomb_planted_side' => 'Bomb Planted Side',
            'bomb_planted_position' => 'Bomb Planted Position',
            'bomb_defused_player_id' => 'Bomb Defused Player ID',
            'bomb_defused_nick_name' => 'Bomb Defused Nick Name',
            'bomb_defused_side' => 'Bomb Defused Side',
            'bomb_defused_position' => 'Bomb Defused Position',
            'bomb_exploded_time' => 'Bomb Exploded Time',
            'created_at' => 'Created At',
        ];
    }
}
