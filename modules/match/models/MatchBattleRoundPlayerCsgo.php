<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_round_player_csgo".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property int|null $player_order 主队客队 1或者2
 * @property int|null $round_ordinal 回合序号
 * @property string|null $side 阵营
 * @property int|null $team_id 战队ID
 * @property int|null $player_id 选手ID
 * @property string|null $nick_name 选手昵称 优先使用数据源给的昵称
 * @property int|null $kills 击杀
 * @property int|null $headshot_kills 爆头击杀
 * @property int|null $is_died 是否死亡
 * @property int|null $assists 助攻
 * @property int|null $flash_assists 闪光弹助攻
 * @property int|null $damage 伤害
 * @property int|null $is_first_kill 是否首先击杀
 * @property int|null $is_first_death 是否首先死亡
 * @property int|null $is_multi_kill 是否多杀
 * @property int|null $is_1_v_n_clutche 是否1VN残局获胜
 * @property int|null $is_knife_kill 是否刀杀
 * @property int|null $is_ace_kill 是否ACE
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattleRoundPlayerCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_round_player_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'game', 'match', 'order', 'player_order', 'round_ordinal', 'team_id', 'player_id', 'kills', 'headshot_kills', 'is_died', 'assists', 'flash_assists', 'damage', 'is_first_kill', 'is_first_death', 'is_multi_kill', 'is_1_v_n_clutche', 'is_knife_kill', 'is_ace_kill',
                'team_damage',
                'damage_taken',
                'hegrenade_damage_taken',
                'inferno_damage_taken',
                'is_planted_bomb',
                'is_defused_bomb',
                'chicken_kills',
                'team_kills',
                'one_kills',
                'two_kills',
                'three_kills',
                'four_kills',
                'five_kills',
                'is_one_on_one_clutch',
                'is_one_on_two_clutch',
                'is_one_on_three_clutch',
                'is_one_on_four_clutch',
                'is_one_on_five_clutch',
                'hit_generic',
                'hit_head',
                'hit_chest',
                'hit_stomach',
                'hit_left_arm',
                'hit_right_arm',
                'hit_left_leg',
                'hit_right_leg',
                'awp_kills',
                'knife_kills',
                'taser_kills',
                'shotgun_kills'],
                'integer'],
            [[ 'blind_enemy_time','blind_teammate_time','steam_id'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['side', 'nick_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'player_order' => 'Player Order',
            'round_ordinal' => 'Round Ordinal',
            'side' => 'Side',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'nick_name' => 'Nick Name',
            'kills' => 'Kills',
            'headshot_kills' => 'Headshot Kills',
            'is_died' => 'Is Died',
            'assists' => 'Assists',
            'flash_assists' => 'Flash Assists',
            'damage' => 'Damage',
            'is_first_kill' => 'Is First Kill',
            'is_first_death' => 'Is First Death',
            'is_multi_kill' => 'Is Multi Kill',
            'is_1_v_n_clutche' => 'Is 1 V N Clutche',
            'is_knife_kill' => 'Is Knife Kill',
            'is_ace_kill' => 'Is Ace Kill',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
