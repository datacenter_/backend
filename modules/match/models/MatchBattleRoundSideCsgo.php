<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_round_side_csgo".
 *
 * @property int $id
 * @property int|null $battle_id 对局唯一ID标识
 * @property int|null $game 游戏项目
 * @property int|null $match 所属比赛
 * @property int|null $order 排序
 * @property int|null $side_order 主队 客队1或者2
 * @property int|null $round_ordinal 回合序号
 * @property string|null $side 阵营
 * @property int|null $team_id 战队ID
 * @property int|null $survived_players 存活人数
 * @property int|null $kills 击杀
 * @property int|null $headshot_kills 爆头击杀
 * @property int|null $is_winner 是否为获胜方
 * @property string|null $round_end_type 回合结束方式
 * @property int|null $is_opening_kill_side 是否首杀
 * @property int|null $is_opening_kill_headshot 首杀是否爆头
 * @property int|null $is_bomb_planted 是否安放了炸弹
 * @property int|null $is_knife_kill 是否有刀杀
 * @property int|null $is_ace_kill 是否有ACE
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattleRoundSideCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_round_side_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'game', 'match', 'order', 'side_order', 'round_ordinal', 'team_id', 'survived_players', 'kills', 'headshot_kills', 'is_winner', 'is_opening_kill_side', 'is_opening_kill_headshot', 'is_bomb_planted', 'is_knife_kill', 'is_ace_kill',
            'deaths',
            'assists',
            'flash_assists',
            'is_first_kill',
            'is_first_death',
            'damage',
            'team_damage',
            'damage_taken',
            'hegrenade_damage_taken',
            'inferno_damage_taken',
            'is_planted_bomb',
            'is_defused_bomb',
            'chicken_kills',
            'team_kills',
            'multi_kills',
            'one_kills',
            'two_kills',
            'three_kills',
            'four_kills',
            'five_kills',
            'is_one_on_x_clutch',
            'is_one_on_one_clutch',
            'is_one_on_two_clutch',
            'is_one_on_three_clutch',
            'is_one_on_four_clutch',
            'is_one_on_five_clutch',
            'hit_generic',
            'hit_head',
            'hit_chest',
            'hit_stomach',
            'hit_left_arm',
            'hit_right_arm',
            'hit_left_leg',
            'hit_right_leg',
            'awp_kills',
            'knife_kills',
            'taser_kills',
            'shotgun_kills'
            ], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['side', 'round_end_type',
            'blind_enemy_time',
            'blind_teammate_time',
            ], 'string', 'max' => 255],
            [['opening_kill_details',
                'bomb_planted_details',
                'knife_kill_details',
                'taser_kill_details',
                'ace_kill_details',
                'team_kill_details',
            ], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'battle_id' => 'Battle ID',
            'game' => 'Game',
            'match' => 'Match',
            'order' => 'Order',
            'side_order' => 'Side Order',
            'round_ordinal' => 'Round Ordinal',
            'side' => 'Side',
            'team_id' => 'Team ID',
            'survived_players' => 'Survived Players',
            'kills' => 'Kills',
            'headshot_kills' => 'Headshot Kills',
            'is_winner' => 'Is Winner',
            'round_end_type' => 'Round End Type',
            'is_opening_kill_side' => 'Is Opening Kill Side',
            'is_opening_kill_headshot' => 'Is Opening Kill Headshot',
            'is_bomb_planted' => 'Is Bomb Planted',
            'is_knife_kill' => 'Is Knife Kill',
            'is_ace_kill' => 'Is Ace Kill',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
