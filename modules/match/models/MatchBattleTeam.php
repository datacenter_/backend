<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_team".
 *
 * @property int $id
 * @property int|null $order 排序
 * @property int|null $battle_id 对局id
 * @property int|null $score 比分
 * @property int|null $identity_id 标示
 * @property int|null $team_id 对应的teamid
 */
class MatchBattleTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'battle_id', 'score', 'team_id'], 'integer'],
            [['identity_id','rel_identity_id','name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => '排序',
            'battle_id' => '对局id',
            'score' => '比分',
            'identity_id' => '标示',
            'team_id' => '对应的teamid',
        ];
    }
}
