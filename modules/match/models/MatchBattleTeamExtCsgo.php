<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_team_ext_csgo".
 *
 * @property int $id 这个等于battle_team的主键id
 * @property string|null $starting_faction_side 开局阵营
 * @property int|null $1st_half_score 上半场比分
 * @property int|null $2nd_half_score 下半场比分
 * @property int|null $ot_score 加时比分
 */
class MatchBattleTeamExtCsgo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_team_ext_csgo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', '1st_half_score', '2nd_half_score', 'ot_score',
                'kills',
                'headshot_kills',
                'deaths',
                'kd_diff',
                'assists',
                'flash_assists',
                'first_kills',
                'first_deaths',
                'first_kills_diff',
                'damage',
                'team_damage',
                'damage_taken',
                'hegrenade_damage_taken',
                'inferno_damage_taken',
                'planted_bomb',
                'defused_bomb',
                'chicken_kills',
                'team_kills',
                'one_kills',
                'two_kills',
                'three_kills',
                'four_kills',
                'five_kills',
                'one_on_one_clutches',
                'one_on_two_clutches',
                'one_on_three_clutches',
                'one_on_four_clutches',
                'one_on_five_clutches',
                'hit_generic',
                'hit_head',
                'hit_chest',
                'hit_stomach',
                'hit_left_arm',
                'hit_right_arm',
                'hit_left_leg',
                'hit_right_leg',
                'awp_kills',
                'knife_kills',
                'taser_kills',
                'shotgun_kills',
                'multi_kills',
                'one_on_x_clutches',
            ], 'integer'],
            [['starting_faction_side',
                'adr',
                'kast',
                'rating',
                'blind_enemy_time',
                'blind_teammate_time'
                ], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'starting_faction_side' => 'Starting Faction Side',
            '1st_half_score' => '1st Half Score',
            '2nd_half_score' => '2nd Half Score',
            'ot_score' => 'Ot Score',
        ];
    }
}
