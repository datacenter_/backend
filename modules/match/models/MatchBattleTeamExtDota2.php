<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_team_ext_dota2".
 *
 * @property int $id
 * @property string|null $faction 阵营（radiant、dire）
 * @property int|null $kills 击杀
 * @property int|null $net_worth 财产
 * @property int|null $gold_earned 金币获取
 * @property int|null $experience 经验
 * @property int|null $tower_kills 摧毁防御塔
 * @property int|null $melee_barrack_kills 摧毁近战兵营
 * @property int|null $ranged_barrack_kills 摧毁远程兵营
 * @property int|null $roshan_kills 击杀肉山
 * @property string|null $building_status_towers 防御塔状态
 * @property string|null $building_status_barracks 兵营状态
 * @property string|null $building_status_ancient 遗迹
 */
class MatchBattleTeamExtDota2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_team_ext_dota2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'kills', 'net_worth', 'gold_earned', 'experience', 'tower_kills', 'melee_barrack_kills', 'ranged_barrack_kills', 'roshan_kills',
                'deaths',
                'assists',
                'net_worth_diff',
                'experience_diff',
                'barrack_kills',
                'total_runes_pickedup',
                'bounty_runes_pickedup',
                'double_damage_runes_pickedup',
                'haste_runes_pickedup',
                'illusion_runes_pickedup',
                'invisibility_runes_pickedup',
                'regeneration_runes_pickedup',
                'arcane_runes_pickedup',
                'smoke_purchased',
                'smoke_used',
                'dust_purchased',
                'dust_used',
                'observer_wards_purchased',
                'observer_wards_placed',
                'observer_wards_kills',
                'sentry_wards_purchased',
                'sentry_wards_placed',
                'sentry_wards_kills',
                ], 'integer'],
            [['building_status','building_status_towers', 'building_status_barracks',
                'building_status_ancient',
                ], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['faction'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'faction' => 'Faction',
            'kills' => 'Kills',
            'net_worth' => 'Net Worth',
            'gold_earned' => 'Gold Earned',
            'experience' => 'Experience',
            'tower_kills' => 'Tower Kills',
            'melee_barrack_kills' => 'Melee Barrack Kills',
            'ranged_barrack_kills' => 'Ranged Barrack Kills',
            'roshan_kills' => 'Roshan Kills',
            'building_status_towers' => 'Building Status Towers',
            'building_status_barracks' => 'Building Status Barracks',
            'building_status_ancient' => 'Building Status Ancient',
        ];
    }
}
