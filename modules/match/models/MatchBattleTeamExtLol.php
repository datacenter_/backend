<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_battle_team_ext_lol".
 *
 * @property int $id
 * @property string|null $faction 阵营
 * @property int|null $kills 击杀
 * @property int|null $deaths 死亡
 * @property int|null $assists 助攻
 * @property int|null $gold 经济
 * @property int|null $gold_diff 经济差
 * @property int|null $experience 经验
 * @property int|null $experience_diff 经验差
 * @property int|null $turret_kills 摧毁防御塔
 * @property int|null $inhibitor_kills 摧毁水晶
 * @property int|null $rift_herald_kills 击杀峡谷先锋
 * @property int|null $dragon_kills 击杀元素巨龙
 * @property string|null $dragon_kills_detail 击杀元素巨龙详情
 * @property int|null $baron_nashor_kills 击杀纳什男爵
 * @property string|null $building_status 建筑物状态
 */
class MatchBattleTeamExtLol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_team_ext_lol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kills', 'deaths', 'assists', 'wards_purchased' ,'wards_placed','wards_kills', 'gold', 'gold_diff', 'experience', 'experience_diff', 'turret_kills', 'inhibitor_kills', 'rift_herald_kills', 'dragon_kills', 'baron_nashor_kills'], 'integer'],
            [['dragon_kills_detail', 'building_status'], 'string'],
            [['faction'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'faction' => 'Faction',
            'kills' => 'Kills',
            'deaths' => 'Deaths',
            'assists' => 'Assists',
            'wards_purchased' => 'Wards_purchased',
            'wards_placed' => 'Wards_placed',
            'wards_kills' => 'Wards_kills',
            'gold' => 'Gold',
            'gold_diff' => 'Gold Diff',
            'experience' => 'Experience',
            'experience_diff' => 'Experience Diff',
            'turret_kills' => 'Turret Kills',
            'inhibitor_kills' => 'Inhibitor Kills',
            'rift_herald_kills' => 'Rift Herald Kills',
            'dragon_kills' => 'Dragon Kills',
            'dragon_kills_detail' => 'Dragon Kills Detail',
            'baron_nashor_kills' => 'Baron Nashor Kills',
            'building_status' => 'Building Status',
        ];
    }
}
