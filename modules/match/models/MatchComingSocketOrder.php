<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_coming_socket_order".
 *
 * @property int $id
 * @property string|null $match_id
 * @property int|null $order
 * @property string|null $begin_at
 * @property string|null $fpid
 * @property string|null $spid
 * @property int|null $status
 * @property int|null $connection_status
 * @property string|null $connection_error
 * @property int|null $server_status
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class MatchComingSocketOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_coming_socket_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'status', 'connection_status', 'server_status','status_refresh'], 'integer'],
            [['begin_at', 'created_at', 'modified_at'], 'safe'],
            [['connection_error'], 'string'],
            [['match_id', 'fpid', 'spid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'order' => 'Order',
            'begin_at' => 'Begin At',
            'fpid' => 'Fpid',
            'spid' => 'Spid',
            'status' => 'Status',
            'connection_status' => 'Connection Status',
            'connection_error' => 'Connection Error',
            'server_status' => 'Server Status',
            'status_refresh' => 'Server Refresh',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
