<?php

namespace app\modules\match\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class MatchData extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id','camp','first_blood','five_kills','ten_kills','first_blood_tower',
                'first_canyon_pioneer','first_small_dragons','first_big_dragons','first_crystal'
            ,'type','battle_id'
            ], 'integer'],

            [['ban','pick','kill','economics','push_tower','small_dragons',
                'baron','crystal','match_duration','match_begin_date','experience'
                ,'destroy_near_barrack','destroy_far_barrack','other_destroy_far_barrack','other_destroy_near_barrack',
                'other_economics','other_kill','other_push_tower','other_small_dragons','other_baron',
                'other_crystal'
            ], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function add($attribute)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $currencyInput = CurrencyInput::find()->where(['match_id' => $attribute['match_id']])->one();
            $currencyInput->setAttributes($attribute);
            if ($currencyInput->save() > 0) {

                // 录入状态
                $status = MatchStatus::find()->where(['match_id' => $attribute['match_id']])->one();
                if (!empty($status) && $status)
                {
                    $status->setAttributes($attribute);
                    $status->save();
                }

                if (isset($attribute['battle']) && !empty($attribute['battle'])) {
                    $battle = json_decode($attribute['battle'], true);

                    Battle::deleteAll(['match_id' => $attribute['match_id']]);
                    self::deleteAll(['match_id' => $attribute['match_id']]);
                    $model = new Battle();
                    $match = new self();

                    foreach ($battle as $key => $val) {
                        $_model = clone $model;
                        $_model->setAttribute('status_type', $val['status_type']);
                        $_model->setAttribute('confrontation_begin_date', $val['confrontation_begin_date']);
                        $_model->setAttribute('match_id', $attribute['match_id']);
                        $_model->setAttribute('num', $val['id']);
                        $_model->save();

                        $matchInfo = clone $match;
                        $matchInfo->setAttributes($val);
                        $matchInfo->setAttribute('battle_id', $_model->id);
                        $matchInfo->setAttribute('match_id', $attribute['match_id']);
                        $matchInfo->save();
                    }
                }
                return ['input_id' => $attribute['match_id'], 'msg' => '成功'];
            }
            } else {
                $currencyInput = new CurrencyInput();
                $currencyInput->setAttributes($attribute);
                if ($currencyInput->save() > 0) {

                    // 录入状态
                    $status = new MatchStatus();
                    $status->setAttributes($attribute);
                    $status->save();

                    if (isset($attribute['battle']) && !empty($attribute['battle'])) {
                        $battle = json_decode($attribute['battle'], true);

                        $match = new self();
                        $model = new Battle();

                        foreach ($battle as $key => $val) {
                            $_model = clone $model;
                            $_model->setAttribute('status_type', $val['status_type']);
                            $_model->setAttribute('confrontation_begin_date', $val['confrontation_begin_date']);
                            $_model->setAttribute('num', $val['id']);
                            $_model->setAttribute('match_id', $attribute['match_id']);
                            $_model->save();

                            $matchInfo = clone $match;
                            $matchInfo->setAttributes($val);
                            $matchInfo->setAttribute('battle_id', $_model->id);
                            $matchInfo->setAttribute('match_id', $attribute['match_id']);
                            $matchInfo->save();
                        }

                    }
                    return ['msg' => '成功'];
                } else {
                    throw new BusinessException($currencyInput->getErrors(), '创建失败');
                }

            }
        }
}
