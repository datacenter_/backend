<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_error_many_team".
 *
 * @property int $id
 * @property int|null $match_id match_Id
 * @property string|null $scheduled_begin_at 计划开始比赛时间
 * @property int|null $status 比赛状态  1-未开始，2-进行中，3-已结束，4-已推迟，5已取消
 * @property string|null $game_image 游戏图像
 * @property string|null $tems_info 战队详情
 * @property int|null $data_status 数据状态：有错误（1）、已修复（2）
 * @property int|null $operation_status 操作状态：待处理（1）、已处理（2）
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property string|null $deleted_at
 */
class MatchErrorManyTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_error_many_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id', 'status', 'data_status', 'operation_status'], 'integer'],
            [['scheduled_begin_at', 'created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['tems_info'], 'string'],
            [['game_image'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'scheduled_begin_at' => 'Scheduled Begin At',
            'status' => 'Status',
            'game_image' => 'Game Image',
            'tems_info' => 'Tems Info',
            'data_status' => 'Data Status',
            'operation_status' => 'Operation Status',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
