<?php

namespace app\modules\match\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $origin_id 游戏源ID
 * @property int $game_id 游戏ID
 * @property string $match_data 数据的json信息
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class MatchLived extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_lived_datas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['origin_id','game_id','deleted'], 'integer'],
            [['origin_id','game_id'], 'integer'],
            [['match_data','type','match_id','round'], 'string'],
            //[['socket_time','deleted_at'], 'safe']
            [['socket_time'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function add($attribute)
    {
        $matchLivedData = new self();
        $matchLivedData->setAttributes($attribute);
        if ($matchLivedData->save() > 0 ) {
            return ['msg' => '创建成功'];
        } else{
            throw new BusinessException($matchLivedData->getErrors(),'创建失败');
        }
    }
}
