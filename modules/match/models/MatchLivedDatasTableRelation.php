<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_lived_datas_table_relation".
 *
 * @property int $id
 * @property int $origin_id
 * @property int $game_id
 * @property string $rel_match_id
 * @property string $table_name
 * @property string $md5_str
 * @property string|null $create_at
 * @property string|null $updated_at
 */
class MatchLivedDatasTableRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_lived_datas_table_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'rel_match_id', 'table_name', 'md5_str'], 'required'],
            [['origin_id', 'game_id'], 'integer'],
            [['create_at', 'updated_at'], 'safe'],
            [['rel_match_id', 'table_name', 'md5_str'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'game_id' => 'Game ID',
            'rel_match_id' => 'Rel Match ID',
            'table_name' => 'Table Name',
            'md5_str' => 'Md5 Str',
            'create_at' => 'Create At',
            'updated_at' => 'Updated At',
        ];
    }
}
