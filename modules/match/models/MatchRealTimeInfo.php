<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_real_time_info".
 *
 * @property int $id match_id
 * @property int|null $auto_origin_id 自动数据源id
 * @property int|null $auto_status 自动同步状态
 * @property string|null $map_ban_pick 地图阵容
 * @property string|null $map_info 地图信息
 * @property string|null $begin_at 实际开始时间
 * @property string|null $end_at 实际结束时间
 * @property int|null $status 比赛状态
 * @property int|null $is_draw 是否平局
 * @property int|null $is_forfeit 是否弃权
 * @property int|null $default_advantage 默认领先战队
 * @property int|null $map_vetoes
 * @property int|null $team_1_score 主队
 * @property int|null $team_2_score 客队
 * @property int|null $winner 获胜战队
 * @property int|null $is_battle_detailed 是否有对局详情
 * @property int|null $is_pbpdata_supported 是否提供实时数据
 * @property int|null $is_streams_supported 是否提供视频直播源
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 */
class MatchRealTimeInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_real_time_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'auto_origin_id', 'auto_status', 'status', 'is_draw', 'is_forfeit', 'default_advantage', 'map_vetoes', 'team_1_score', 'team_2_score', 'winner', 'is_battle_detailed', 'is_pbpdata_supported', 'is_streams_supported','data_speed','video_count'], 'integer'],
            [['map_ban_pick', 'map_info','data_level'], 'string'],
            [['created_time', 'modified_time'], 'safe'],
            [['begin_at', 'end_at'], 'string', 'max' => 60],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auto_origin_id' => 'Auto Origin ID',
            'auto_status' => 'Auto Status',
            'map_ban_pick' => 'Map Ban Pick',
            'map_info' => 'Map Info',
            'begin_at' => 'Begin At',
            'end_at' => 'End At',
            'status' => 'Status',
            'is_draw' => 'Is Draw',
            'is_forfeit' => 'Is Forfeit',
            'default_advantage' => 'Default Advantage',
            'map_vetoes' => 'Map Vetoes',
            'team_1_score' => 'Team 1 Score',
            'team_2_score' => 'Team 2 Score',
            'winner' => 'Winner',
            'is_battle_detailed' => 'Is Battle Detailed',
            'is_pbpdata_supported' => 'Is Pbpdata Supported',
            'is_streams_supported' => 'Is Streams Supported',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
