<?php

namespace app\modules\match\models;

use Yii;

class MatchSocketList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_coming_socket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'is_over1','is_over2','is_over3','flag','fpid1','fpid2','fpid3','connection_status1','connection_status2','connection_status3' ,'status','status1', 'status2', 'status3','status1_refresh','status2_refresh','status3_refresh'], 'integer'],
            [['begin_at', 'end_at','websocket_time1','websocket_time2'], 'safe'],
            [['spid1','spid2','spid3','connection_error1','connection_error2','connection_error3' ,'match_id'], 'string'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源ID',
            'game_id' => '游戏ID',
            'match_id' => '比赛ID',
            'begin_at' => '比赛开始时间',
            'end_at' => '比赛结束时间',
            'is_over' => '是否杀了进程',
            'status1' => '服务器1',
            'status2' => '服务器2',
            'status3' => '服务器3',
        ];
    }
}
