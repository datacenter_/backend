<?php

namespace app\modules\match\models;

use Yii;

class MatchStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id','match_status','auto_input_status'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
