<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_stream_list".
 *
 * @property int $id
 * @property string|null $streamer 主播名称
 * @property string|null $platform_name 直播平台名称
 * @property int|null $platform_id 直播平台id
 * @property int|null $video_id 接口方直播id
 * @property int|null $country 国籍ID
 * @property string|null $title 标题
 * @property string $live_url 直播地址 存url
 * @property string|null $live_url_identity 视频唯一  url+matchId
 * @property string|null $live_embed_url 嵌入代码
 * @property int|null $viewers 观看人数
 * @property int|null $is_official 是否为官方直播源
 * @property int $match_id
 * @property string|null $preview 预览图
 * @property int|null $flag 1正常2删除
 * @property int|null $origin_id 数据源id
 * @property int|null $video_origin_id 视频爬虫来源的id
 * @property string|null $modified_at 更新时间
 * @property string|null $deleted_at 删除时间
 * @property string|null $created_at 创建时间
 */
class MatchStreamList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_stream_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['platform_id', 'video_id', 'country', 'viewers', 'is_official', 'match_id', 'flag', 'origin_id', 'video_origin_id'], 'integer'],
            [['live_url', 'match_id'], 'required'],
            [['modified_at', 'deleted_at', 'created_at'], 'safe'],
            [['streamer', 'platform_name', 'title'], 'string', 'max' => 200],
            [['live_url', 'live_embed_url', 'preview'], 'string', 'max' => 1000],
            [['live_url_identity'], 'string', 'max' => 100],
            [['live_url_identity', 'match_id'], 'unique', 'targetAttribute' => ['live_url_identity', 'match_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'streamer' => 'Streamer',
            'platform_name' => 'Platform Name',
            'platform_id' => 'Platform ID',
            'video_id' => 'Video ID',
            'country' => 'Country',
            'title' => 'Title',
            'live_url' => 'Live Url',
            'live_url_identity' => 'Live Url Identity',
            'live_embed_url' => 'Live Embed Url',
            'viewers' => 'Viewers',
            'is_official' => 'Is Official',
            'match_id' => 'Match ID',
            'preview' => 'Preview',
            'flag' => 'Flag',
            'origin_id' => 'Origin ID',
            'video_origin_id' => 'Video Origin ID',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
            'created_at' => 'Created At',
        ];
    }
}
