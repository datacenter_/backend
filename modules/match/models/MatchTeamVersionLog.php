<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "match_team_version_log".
 *
 * @property int $id
 * @property string|null $team_md5
 * @property string|null $teamids 两个战队id以逗号连接
 * @property string|null $team_version 战队变更版本号
 * @property int|null $match_id
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class MatchTeamVersionLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_team_version_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['team_md5'], 'string', 'max' => 50],
            [['teamids'], 'string', 'max' => 255],
            [['team_version'], 'string', 'max' => 100],
            [['team_md5', 'match_id', 'team_version'], 'unique', 'targetAttribute' => ['team_md5', 'match_id', 'team_version']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_md5' => 'Team Md5',
            'teamids' => 'Teamids',
            'team_version' => 'Team Version',
            'match_id' => 'Match ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
