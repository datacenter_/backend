<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "socket_game_configure".
 *
 * @property int $id
 * @property int $game_id 游戏ID
 * @property int|null $inhibitor_reborn_time 水晶重生倒计时
 * @property int|null $rift_herald_first_time 峡谷先锋首次刷新
 * @property int|null $rift_herald_reborn_time 峡谷先锋重生倒计时
 * @property int|null $rift_herald_quit_time 峡谷先锋退场时间
 * @property int|null $rift_herald_max_num 峡谷先锋最多数量
 * @property int|null $dragon_first_time 小龙首次刷新
 * @property int|null $dragon_reborn_time 小龙重生倒计时
 * @property int|null $dragon_quit_num 小龙退场单队数量
 * @property int|null $baron_nashor_first_time 大龙首次刷新
 * @property int|null $baron_nashor_reborn_time 大龙重生倒计时
 * @property int|null $elder_dragon_first_time 远古龙首次刷新时长
 * @property int|null $elder_dragon_reborn_time 远古龙重生倒计时
 * @property int|null $mp_maxrounds mp_maxrounds
 * @property int|null $mp_overtime_enable mp_overtime_enable
 * @property int|null $mp_overtime_maxrounds mp_overtime_maxrounds
 * @property int|null $mp_roundtime_defuse mp_roundtime_defuse
 * @property int|null $mp_freezetime mp_freezetime
 * @property int|null $mp_c4timer mp_c4timer
 */
class SocketGameConfigure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'socket_game_configure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id'], 'required'],
            [['game_id', 'inhibitor_reborn_time', 'rift_herald_first_time', 'rift_herald_reborn_time', 'rift_herald_quit_time', 'rift_herald_max_num', 'dragon_first_time', 'dragon_reborn_time', 'dragon_quit_num', 'baron_nashor_first_time', 'baron_nashor_reborn_time', 'elder_dragon_first_time', 'elder_dragon_reborn_time', 'mp_maxrounds', 'mp_overtime_enable', 'mp_overtime_maxrounds', 'mp_roundtime_defuse', 'mp_freezetime', 'mp_c4timer'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'inhibitor_reborn_time' => 'Inhibitor Reborn Time',
            'rift_herald_first_time' => 'Rift Herald First Time',
            'rift_herald_reborn_time' => 'Rift Herald Reborn Time',
            'rift_herald_quit_time' => 'Rift Herald Quit Time',
            'rift_herald_max_num' => 'Rift Herald Max Num',
            'dragon_first_time' => 'Dragon First Time',
            'dragon_reborn_time' => 'Dragon Reborn Time',
            'dragon_quit_num' => 'Dragon Quit Num',
            'baron_nashor_first_time' => 'Baron Nashor First Time',
            'baron_nashor_reborn_time' => 'Baron Nashor Reborn Time',
            'elder_dragon_first_time' => 'Elder Dragon First Time',
            'elder_dragon_reborn_time' => 'Elder Dragon Reborn Time',
            'mp_maxrounds' => 'Mp Maxrounds',
            'mp_overtime_enable' => 'Mp Overtime Enable',
            'mp_overtime_maxrounds' => 'Mp Overtime Maxrounds',
            'mp_roundtime_defuse' => 'Mp Roundtime Defuse',
            'mp_freezetime' => 'Mp Freezetime',
            'mp_c4timer' => 'Mp C4timer',
        ];
    }
}
