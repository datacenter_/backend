<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "sorting_log".
 *
 * @property int $id
 * @property int|null $match_id 比赛ID
 * @property int|null $log_type 日志类别
 * @property string|null $team_1_log_name 主队日志名称
 * @property string|null $team_2_log_name 客队日志名称
 * @property string|null $service_ip 服务器IP
 * @property string|null $sleeps 延迟(秒)
 * @property int|null $flag 是否删除
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 * @property string|null $deleted_time 删除时间
 */
class SortingLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sorting_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id', 'log_type', 'flag','timezone','prefect_logo'], 'integer'],
            [['created_time', 'modified_time', 'deleted_time','refresh_time','last_refresh_time'], 'safe'],
            [['team_1_log_name', 'team_2_log_name', 'service_ip', 'sleeps'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'log_type' => 'Log Type',
            'team_1_log_name' => 'Team 1 Log Name',
            'team_2_log_name' => 'Team 2 Log Name',
            'service_ip' => 'Service Ip',
            'sleeps' => 'Sleeps',
            'flag' => 'Flag',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'deleted_time' => 'Deleted Time',
            'prefect_logo' => 'Prefect Logo',
            'refresh_time' => 'Refresh Time',
        ];
    }
    public function getMatch(){
        return $this->hasOne(match::className(),['id'=>'match_id']);
    }
}
