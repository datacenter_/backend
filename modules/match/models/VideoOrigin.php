<?php

namespace app\modules\match\models;

use Yii;

/**
 * This is the model class for table "sorting_log".
 *
 * @property int $id
 * @property int|null $match_id 比赛ID
 * @property int|null $log_type 日志类别
 * @property string|null $team_1_log_name 主队日志名称
 * @property string|null $team_2_log_name 客队日志名称
 * @property string|null $service_ip 服务器IP
 * @property string|null $sleeps 延迟(秒)
 * @property int|null $flag 是否删除
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 * @property string|null $deleted_time 删除时间
 */
class VideoOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id','deleted'], 'integer'],
            [['created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['origin_video_url'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'match_id',
            'origin_video_url' => 'origin_video_url',
            'deleted' => 'deleted',
            'created_at' => 'created_at',
            'modified_at' => 'modified_at',
            'deleted_at' => 'deleted_at',
        ];
    }
}
