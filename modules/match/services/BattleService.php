<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services;

use app\modules\match\services\battle\BattleInterface;
use app\modules\match\services\battle\CommonBattleService;
use app\modules\match\services\battle\CsgoBattleService;
use app\modules\match\services\battle\DotaBattleService;
use app\modules\match\services\battle\LolBattleService;

class  BattleService
{
    const GAME_TYPE_LOL = 'lol';
    const GAME_TYPE_DOTA = 'dota';
    const GAME_TYPE_COMMON = 'common';
    const GAME_TYPE_CSGO = 'csgo';

    const DATA_TYPE_BASE = 'base'; // match_battle
    const DATA_TYPE_BAN_PICK = 'ban_pick'; // match_battle_ext..
    const DATA_TYPE_DETAILS = 'details'; // match_battle_ext..
    const DATA_TYPE_STATICS = 'statics'; // match_battle_team ..
    const DATA_TYPE_PLAYERS = 'players'; // match_battle_player_ext...
    const DATA_TYPE_ROUND_PLAYERS = 'round_players'; // match_battle_player_ext...
    const DATA_TYPE_EVENTS = 'events'; // match_battle_events_...
    const DATA_TYPE_FIRST_EVENTS = 'first_events'; // match_battle_events_...
    const DATA_TYPE_ROUNDS = 'rounds'; // match_battle_rounds...


    public static function getBattleInfo($battleId, $gameType, $dataType = "", $adminType = 1, $adminId = 0)
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::getBattleInfo($battleId, $dataType, $adminType, $adminId);
    }

    public static function setBattleInfo($battleId, $gameType, $data, $dataType, $adminType = 1, $adminId = 0)
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::setBattleInfo($battleId, $data, $dataType, $adminType, $adminId);
    }

    public static function addBattleInfo($matchId, $gameType, $data, $dataType, $adminType = 1, $adminId = 0)
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::addBattleInfo($matchId, $data, $dataType, $adminType, $adminId);
    }

    public static function getBattleListByMatchId($matchId, $gameType, $adminType = 1, $adminId = 0)
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::getBattleListByMatchId($matchId, $adminType, $adminId);
    }
    public static function deleteBattle($gameType, $battleId, $adminType = 1, $adminId = 0,$delType = 'special',$isSendMsg='')
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::deleteBattle($battleId, $adminType, $adminId,$delType,$isSendMsg);

    }
    public static function recoveryBattle( $battleId, $gameType,$adminType = 1, $adminId = 0)
    {
        $service = self::getBattleServiceByGameType($gameType);
        return $service::recoveryBattle($battleId,$gameType, $adminType, $adminId);
    }

    /**
     * @param $gameType
     * @return BattleInterface
     */
    public static function getBattleServiceByGameType($gameType)
    {
        $serviceMap = [
            self::GAME_TYPE_DOTA => DotaBattleService::class,
            self::GAME_TYPE_LOL => LolBattleService::class,
            self::GAME_TYPE_COMMON => CommonBattleService::class,
            self::GAME_TYPE_CSGO => CsgoBattleService::class,
        ];
        if (isset($serviceMap[$gameType])) {
            return $serviceMap[$gameType];
        }
    }

}