<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services;


use app\modules\common\models\EnumGame;
use app\modules\common\models\OperationLog;
use app\modules\common\services\Consts;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\EnumService;
use app\modules\common\services\EsService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchErrorManyTeam;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchLivedData;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\models\MatchStreamList;
use app\modules\match\models\MatchTeamVersionLog;
use app\modules\match\models\SortingLog;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataDota2Map;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\models\TeamSnapshot;
use app\modules\org\services\TeamService;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataMatchVideoList;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\api\ApiBase;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\operation\OperationRefreshHotInfo;
use app\modules\task\services\StandardDataService;
use app\modules\task\services\wsdata\pandascore\PandascoreLolApi;
use app\modules\task\services\wsdata\pandascore\PandascoreLolws;
use app\modules\tournament\models\TournamentTeamRelation;
use app\modules\tournament\services\TournamentService;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use app\modules\task\services\logformat\Csgo5e;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\common\services\SteamHelper;
use app\modules\task\services\logformat\CsgoFunspark;
use app\modules\task\services\logformat\CsgoLogFormatBase;
use yii\db\Exception;

class MatchService
{
    const MATCH_INFO_TYPE_AUTO='auto';
    CONST MATCH_INFO_TYPE_BAN_PICK='ban_pick';
    CONST MATCH_INFO_TYPE_MAP='map';
    const MATCH_INFO_TYPE_DETAIL='detail';
    const MATCH_INFO_TYPE_DETAIL_BY_SOCKET_FRAMES='detail_by_scoket_frames';
    const MATCH_INFO_TYPE_DETAIL_BY_ORDER_SOCKET_FRAMES='detail_by_order_scoket_frames';

    private $playerItemsList;

    public static function getMatchList($params)
    {
        $match = Match::find()->alias('m')
            ->select(['m.*',
                'ifnull(mi.status,1) as status',
//                'hdrm.origin_id as auto_origin_id',
//                'hdrm.auto_status',
//                'hdrm.auto_add_type',
//                'sdm.is_battle_detailed',
//                'sdm.is_pbpdata_supported',
                't.name',
                't.name_cn',
                'g.logo',
                'm.id as match_id',
                'tm.name as main_team_name',
                'tmo.name as other_team_name',
                'tm.image as main_logo',
                'tmo.image as other_logo',
                'tg.name as group_name',
                'mi.team_1_score',
                'mi.team_2_score',
            ])
            ->leftJoin('tournament as t', 't.id = m.tournament_id')
            ->leftJoin('team as tm', 'tm.id = m.team_1_id')
            ->leftJoin('team as tmo', 'tmo.id = m.team_2_id')
            ->leftJoin('match_real_time_info as mi', 'mi.id = m.id')
            ->leftJoin('tournament_group as tg', 'tg.id = m.group_id')
            ->leftJoin('enum_game as g', 'm.game = g.id');
//            ->leftJoin('hot_data_running_match as hdrm','hdrm.match_id = m.id');
            //->leftJoin('data_standard_master_relation as rel','rel.master_id = m.id and rel.resource_type="match" ')
            //->leftJoin('standard_data_match as sdm', 'sdm.id = rel.standard_id');

        if (isset($params['name']) && !empty($params['name'])) {
            $match->andfilterwhere(['like', 't.name', $params['name']]);
        }
        if (isset($params['group_name']) && !empty($params['group_name'])) {
            $match->andfilterwhere(['like', 'tg.name', $params['group_name']]);
        }
        if (isset($params['team_name']) && !empty($params['team_name'])) {
            $match->andfilterwhere(['or',
                ['like', 'tm.name', $params['team_name']],
                ['like', 'tmo.name', $params['team_name']],
                ['like', 'tm.full_name', $params['team_name']],
                ['like', 'tmo.full_name', $params['team_name']],
                ['like', 'tm.short_name', $params['team_name']],
                ['like', 'tmo.short_name', $params['team_name']],
                ['like', 'tm.alias', $params['team_name']],
                ['like', 'tmo.alias', $params['team_name']],
                ]
            );
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $match->andWhere(['m.id' => $params['id']]);
        }

        if (isset($params['status']) && !empty($params['status'])) {
            $match->andWhere(['mi.status' => $params['status']]);
        }

        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $match->andWhere(['g.id' => $params['game_id']]);
        }

        if (isset($params['match_type']) && !empty($params['match_type'])) {  //比赛类型筛选
//            $match->andWhere(['like','m.match_type',$params['match_type']]);
            $match->andWhere(['m.match_type' => $params['match_type']]);
        }
        if (isset($params['game_rules']) && !empty($params['game_rules'])) {  //比赛规则
            $match->andWhere(['m.game_rules' => $params['game_rules']]);
        }

        if (isset($params['scheduled_begin_at']) && !empty($params['scheduled_begin_at'])) {
            $match->andWhere(['>=', 'm.scheduled_begin_at', $params['scheduled_begin_at']]);
        }
        if (isset($params['scheduled_end_at']) && !empty($params['scheduled_end_at'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));
            $match->andWhere(['<', 'm.scheduled_begin_at', $end]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $match->andWhere(['>=', 'm.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $match->andWhere(['<', 'm.modified_at', $end]);
        }
        //and hdrm.origin_id = sdm.origin_id
//        $match->andWhere('hdrm.auto_status = 1');

        $match = $match->orderBy('m.scheduled_begin_at desc,m.modified_at desc');
        $totalCount = $match->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
//        $sql = $match->createCommand()->getRawSql();
        $model = $match->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        $resourceIds = array_column($model, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_MATCH, $resourceIds);

        foreach ($model as &$val) {
            $rel_identity_id = '';
            $is_battle_detailed = 2;
            $is_pbpdata_supported = 2;
            $matchId = $val['id'];
            $matchAutoInfo = HotDataRunningMatch::find()->select(['auto_status','auto_add_type'])->where(['match_id'=>$val['id']])->asArray()->one();
            //初始化数组
            $val['auto_status'] = @$matchAutoInfo['auto_status'];
            $val['auto_add_type'] = @$matchAutoInfo['auto_add_type'];

            $updateInfo = DataResourceUpdateConfig::find()->select(['origin_id','update_type'])->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchId])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
            $updateInfo_origin_id = @$updateInfo['origin_id'];
            $val['auto_origin_id'] = $updateInfo_origin_id;
            $val['update_type'] = @$updateInfo['update_type'];
            //如果是日志那么就从SortingLog取值
            if ($updateInfo_origin_id == 5){
                $SortingLogInfo = SortingLog::find()->where(['match_id'=>$matchId])->asArray()->one();
                $rel_identity_id = @$SortingLogInfo['id'];
            }else{
                //所有的relation
                $relationInfos = DataStandardMasterRelation::find()->where(['resource_type'=>'match'])->andWhere(['master_id'=>$val['id']])->asArray()->all();
                if ($relationInfos){
                    foreach ($relationInfos as $item){
                        $standard_id = $item['standard_id'];
                        //查询指定的standard match 数据
                        $standardDataMatchInfo = StandardDataMatch::find()->where(['id'=>$standard_id])->asArray()->one();
                        if (@$standardDataMatchInfo['origin_id'] == $updateInfo_origin_id && @$standardDataMatchInfo['origin_id']){
                            $rel_identity_id = @$standardDataMatchInfo['rel_identity_id'];
                            $is_battle_detailed = @$standardDataMatchInfo['is_battle_detailed'];
                            $is_pbpdata_supported = @$standardDataMatchInfo['is_pbpdata_supported'];
                        }
                    }
                }
            }
            $val['rel_identity_id'] = $rel_identity_id;
            $val['is_battle_detailed'] = $is_battle_detailed;
            $val['is_pbpdata_supported'] = $is_pbpdata_supported;
            //查询当前比赛自动录入的方式

            $val['operation'] = $operationInfo[$val['id']];
            $val['logo'] = ImageConversionHelper::showFixedSizeConversion($val['logo'],66,66,'ali');
            $val['main_logo'] = ImageConversionHelper::showFixedSizeConversion($val['main_logo'],66,66,'ali');
            $val['other_logo'] = ImageConversionHelper::showFixedSizeConversion($val['other_logo'],66,66,'ali');
            $val['update_config'] = UpdateConfigService::getResourceUpdateConfigDetail($val['id'], Consts::RESOURCE_TYPE_MATCH);
            //$val['rel_identity_id'] = self::getRelIdentityIdAndMainId('match',$val['id']);
            $val['binding_info'] = TeamService::actionGetBingInfo($val['id'],'match');
            $val['stream_url_info'] = self::getStreamUrlInfo($val['id']);

        }
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getMergeMatch($matchId)
    {
        $core = Match::find()->where(['id' => $matchId])->asArray()->one();
        $base = MatchBase::find()->where(['match_id' => $matchId])->asArray()->one();

        return array_merge($core, $base?$base:[]);
    }

    public static function setMatch($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        $matchId = isset($attribute['id']) ? $attribute['id'] : null;
        $coreData = $attribute[Consts::TAG_TYPE_CORE_DATA];
        $realData = $attribute[Consts::TAG_TYPE_REAL_TIME];
        $baseData = $attribute[Consts::TAG_TYPE_BASE_DATA];
        // 设置match数据
        if ($coreData) {
            if ($matchId && !isset($coreData["id"])) {
                $coreData["id"] = $matchId;
            }
            $match = self::setMatchCoreData($coreData, $userId, $userType, $basisId, $originId);
            $matchId = $match['id'] ?? null;
            $gameId = $match['game'] ?? null;
            if($matchId){
                $reamTime = self::setMatchRealData($matchId, $realData, $userType, $userId, $basisId, $originId, $gameId);
            }
        }
        // 设置比赛基础数据
        if ($baseData) {
            $matchBase = self::setMatchBaseData($matchId, $baseData, $userType, $userId, $basisId);
        }
        // 设置操作状态
        if ($basisId) {
            DataTodoService::setDealStatus($basisId, Consts::TODO_DEAL_STATUS_DONE);
        }
        return self::getMatch($matchId);
    }
    public static function setMatchUpdateTime($playerId,$updateTime)
    {
        return Match::updateAll(['modified_at'=>$updateTime],['id'=>$playerId]);
    }

    // 获取match有关数据 格式化输出
    public static function getMatch($matchId)
    {
        $matchCore = Match::find()->where(["id" => $matchId])->asArray()->one();
        $matchBase = MatchBase::find()->where(['match_id' => $matchId])->asArray()->one();
        $realData = MatchRealTimeInfo::find()->where(['id' => $matchId])->asArray()->one();
        return [
            'match_id' => $matchId,
            Consts::TAG_TYPE_CORE_DATA => $matchCore,
            Consts::TAG_TYPE_BASE_DATA => $matchBase,
            Consts::TAG_TYPE_REAL_TIME => $realData,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($matchId, Consts::RESOURCE_TYPE_MATCH),
        ];
    }
    // match_real_time表数据塞入
    public static function setMatchRealData($matchId, $attribute, $userType, $userId, $basisId = 0, $originId = 0, $gameId=0)
    {
        if(!$gameId){
            $matchData = Match::find()->select(['game','number_of_games'])->where(['id'=>$matchId])->asArray()->one();
            $gameId = @$matchData['game'];
        }
        $match_real_time_info = MatchRealTimeInfo::find()->where(['id' => $matchId])->one();
        $operationType = OperationLogService::OPERATION_TYPE_UPDATE;

        if(!$match_real_time_info){
            //这里不知道什么时候会走到， 如果有走到，把map_info 字段添加地图 参考setMatchCoreData里面
            $match_real_time_info = new MatchRealTimeInfo();
            $attribute['id'] = $matchId;
            $operationType = OperationLogService::OPERATION_TYPE_ADD;

        }
        $logOldMatchBase = $match_real_time_info ->toArray();

        // 比赛比分 主队与客队 字段限制：必须为null（比赛开始前）或0（比赛开始后）或正整数  有待考虑


        // 是否有对局详情数据  1或者2
//        if(array_key_exists('is_battle_detailed',$attribute) && $attribute['is_battle_detailed']==1){
//            if(!$originId){
//                $match_UpdateConfig = UpdateConfigService::getResourceUpdateConfig($gameId,'match');
//                $originId = $match_UpdateConfig['update_data']['origin_id'];
//            }
//            $configInfo = DataResourceUpdateConfig::find()->select('id')
//                ->where([
//                    'resource_type' => 'match', 'game_id' => $gameId, 'origin_id'=>$originId,
//                    'resource_id'=>$matchId, 'update_type'=>3, 'tag_type'=>'score_data'
//                ])
//                ->asArray()->one();
//            if($configInfo){
//                $attribute['is_battle_detailed'] = 1;
//            }else{
//                $attribute['is_battle_detailed'] = 2;
////                //设置hot_data_running_match如果比赛结束了且比赛没有对局详情的，把hot_data_running_match 表的比赛状态也修改下
////                if($attribute['status'] == 3){
////                    $hotInfo=HotDataRunningMatch::find()
////                        ->where(['match_id'=>$matchId])
////                        ->one();
////                    $hotInfo->setAttribute('status',3);
////                    $hotInfo->save();
////                }
//            }
//
//        }else{
//            $attribute['is_battle_detailed'] = 2;
//        }
        // 是否支持PBP数据 1或者2
//        if(array_key_exists('is_pbpdata_supported',$attribute) && $attribute['is_pbpdata_supported']==1){
//            $configInfo = DataResourceUpdateConfig::find()->select('id')
//                ->where([
//                    'resource_type' => 'match', 'game_id' => $gameId, 'origin_id'=>$originId,
//                    'resource_id'=>$matchId, 'update_type'=>3, 'tag_type'=>'score_data'
//                ])
//                ->asArray()->one();
//            if($configInfo){
//                $attribute['is_pbpdata_supported'] = 1;
//            }else{
//                $attribute['is_pbpdata_supported'] = 2;
//            }
//        }else{
//            $attribute['is_pbpdata_supported'] = 2;
//        }
        // 是否支持视频直播 1或者2
//        if(array_key_exists('is_streams_supported',$attribute)){
//            $configInfo = MatchStreamList::find()->select('id')
//                ->where(['match_id'=>$matchId])
//                ->asArray()->one();
//            if(!$configInfo){
//                $attribute['is_streams_supported'] = 2;
//            }
//        }
        // 计划开始时间在status等于4的情况可以为空
        if(array_key_exists('scheduled_begin_at',$attribute) && $match_real_time_info){

            //设置hot_data_running_match的计划开始时间
            $hotInfo=HotDataRunningMatch::find()
                ->where(['match_id'=>$matchId])
                ->one();
            $hotInfo->setAttribute('match_start_time',$attribute['scheduled_begin_at']);
            $hotInfo->save();

            if(array_key_exists('status',$attribute)){
                if($attribute['status'] == 4){
                    if(empty($attribute['scheduled_begin_at'])){
                        $attribute['scheduled_begin_at'] = null;
                    }
                }else{
                    if(empty($attribute['scheduled_begin_at'])){
                        unset($attribute['scheduled_begin_at']);
                    }
                }
            }else{
                if($match_real_time_info->status == 4){
                    if(empty($attribute['scheduled_begin_at'])){
                        $attribute['scheduled_begin_at'] = null;
                    }
                }else{
                    if(empty($attribute['scheduled_begin_at'])){
                        unset($attribute['scheduled_begin_at']);
                    }
                }
            }
        }

        $match_real_time_info->setAttributes($attribute);
        // 这里加判断，如果比赛状态是进行中或者已结束，比分中的null改成0
        $matchInfo = $match_real_time_info->toArray();
        if (in_array($matchInfo['status'], [2, 3])) {
            if ($matchInfo['team_1_score'] === null) {
                $match_real_time_info->setAttribute('team_1_score', 0);
            }
            if ($matchInfo['team_2_score'] === null) {
                $match_real_time_info->setAttribute('team_2_score', 0);
            }
        }
        if(!$match_real_time_info->save()){
            throw new BusinessException($match_real_time_info->getErrors(),'match_real_time_info保存失败');
        };
        $logNewMatchBase = $match_real_time_info->toArray();
        //

//        if($match_real_time_info && $match_real_time_info->status == 2){
//            if(array_key_exists('team_1_soure',$attribute)){
//                $team_1_soure = $attribute['team_1_soure'];
//                $soure['team_1_soure'] = $team_1_soure;
//            }
//            if(array_key_exists('team_2_soure',$attribute)){
//                $team_2_soure = $attribute['team_2_soure'];
//                $soure['team_2_soure'] = $team_2_soure;
//            }
//            $match_real_time_info->setAttributes($soure);
//            if(!$match_real_time_info->save()){
//                throw new BusinessException($match_real_time_info->getErrors(),'比赛比分保存失败');
//            }
//        }
        if(!empty($attribute)){
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setMatchUpdateTime($matchId,$time);
        }



        // 记录log
        $filter = array_keys($logNewMatchBase);
        $filter = array_diff($filter, ["created_time", "modified_time"]);
        $diffInfo = Common::getDiffInfo($logOldMatchBase, $logNewMatchBase, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_MATCH,
                $matchId,
                ["diff" => $diffInfo["diff"], "new" => $logNewMatchBase],
                0,
                Consts::TAG_TYPE_REAL_TIME,
                null,
                $basisId,
                $userType,
                $userId
            );
        }

        return ;
    }
    public static function getTeamIdByOrder($matchId,$winnerId){
        $matchInfo = Match::find()->select(['team_1_id','team_2_id'])->where(['id'=>$matchId])->asArray()->one();
        if($matchInfo['team_1_id']==$winnerId && $matchInfo['team_1_id']){
            $winnerOrder = 1;
        }elseif ($matchInfo['team_2_id']==$winnerId && $matchInfo['team_2_id']){
            $winnerOrder = 2;
        }else{
            $winnerOrder = null;
        }
        return $winnerOrder;
    }
    public static function setMatchCoreData($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        //删除字段
        if(isset($attribute['deleted']) && $attribute['deleted'] == 0){
            $attribute['deleted'] = 2;
        }
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            $attribute['deleted_at'] = date("Y-m-d H:i:s");
        }
        if(isset($attribute['deleted']) && $attribute['deleted'] == 2){
            $attribute['deleted_at'] = null;
        }
        if(array_key_exists('deleted',$attribute) && $attribute['deleted'] == 0){
            $attribute['deleted'] = 2;
        }
        // 这里判断必填项
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldMatch = [];
        if (isset($attribute['id']) && $attribute['id']) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $match = Match::find()->where(['id' => $attribute['id']])->one();
            // 是否调整时间表
            if(array_key_exists('scheduled_begin_at',$attribute)){
                if($attribute['scheduled_begin_at']){
                    if($attribute['scheduled_begin_at'] == $match->original_scheduled_begin_at){
                        $attribute['is_rescheduled'] = 2;
                    }else{
                        $attribute['is_rescheduled'] = 1;
                    }
                }
            }
            if (!$match) {
                throw new BusinessException($attribute, "不存在这个match");
            }
            $logOldMatch = $match->toArray();
            $match->setAttributes($attribute);
            if (!$match->save()) {
                throw new BusinessException($match->getErrors(), "设置core_data失败");
            }
            if ($match->id && $match->team_1_id && $match->team_2_id ) {

                $teamVersion = self::setTeamVersion($match->id,$match->team_1_id,$match->team_2_id);
                $match->setAttributes(['team_version' => $teamVersion]);
                if (!$match->save()) {
                    throw new BusinessException($match->getErrors(), "设置team_version失败");
                }
            }

            if(!$attribute['slug']){
                // 判断是否slug重新生成
                if(isset($attribute['team_1_id']) || isset($attribute['team_2_id']) || isset($attribute['scheduled_begin_at'])) {
                    $gameId = $match->game;
                    $team_1_id = $match->team_1_id;
                    $team_2_id = $match->team_2_id;
                    $scheduled_begin_at = $match->scheduled_begin_at ? $match->scheduled_begin_at:'';
                    $newSlug = self::matchTeamSlugGenerate($team_1_id, $team_2_id, $scheduled_begin_at,$gameId);
                    $match->setAttributes(['slug' => $newSlug]);
                    if (!$match->save()) {
                        throw new BusinessException($match->getErrors(), "设置match_slug失败");
                    }
                }
            }
        } else {
            $attribute['cuser'] = $userId;
            $match = new Match();
            $match->setAttributes($attribute);
            if (!$match->save()) {
                throw new BusinessException($match->getErrors(), "设置core_data失败");
            }

            if ($match->id && $match->team_1_id && $match->team_2_id ) {
                $teamVersion = self::setTeamVersion($match->id,$match->team_1_id,$match->team_2_id);
                $match->setAttributes(['team_version' => $teamVersion]);
                if (!$match->save()) {
                    throw new BusinessException($match->getErrors(), "设置team_version失败");
                }
            }

            $match_real_time_info = new MatchRealTimeInfo();
            $match_real_array['id'] = $match['id'];
            $match_real_array['status'] = 1;
            //比赛类型是best_of 才会设置 默认地图
            if ($attribute['match_type'] == 1){
                $map_info = self::getMapLists($attribute['game'],'maps','normal',$attribute['number_of_games']);
                if($map_info){
                    $match_real_array['map_info'] = $map_info;
                }
            }
            $match_real_time_info->setAttributes($match_real_array);
            if(!$match_real_time_info->save()){
                throw new BusinessException($match_real_time_info->getErrors(),'match_real_time_info保存失败');
            }

            // 创建关系
            if ($basisId != 0) {
                \Yii::$app->db->createCommand()->batchInsert('data_change_config', ['basis_id', 'resource_type', 'resource_id', 'origin_id'],
                    [[$basisId, 'match', $match->id, $originId]])->execute();
            }
            //抓取配置信息
            UpdateConfigService::initResourceUpdateConfig($match["id"], Consts::RESOURCE_TYPE_MATCH, $attribute["game"]);
            //判断如果是自动那就插入到hotdatarunningmatch
            $match_UpdateConfig = UpdateConfigService::getResourceUpdateConfig($match["id"],'match');
            //获取默认比赛 配置
            if (isset($match_UpdateConfig['score_data']['origin_id']) && $match_UpdateConfig['score_data']['origin_id']){
                $auto_origin_id = $match_UpdateConfig['score_data']['origin_id'];
                $match_data_RealTimeInfo = [
                    'auto_origin_id'=>$auto_origin_id,
                    'game_id' => $attribute["game"],
                    'match_start_time' => $attribute["scheduled_begin_at"],
                ];
                MatchService::setRealTimeInfo($match["id"], $match_data_RealTimeInfo, self::MATCH_INFO_TYPE_AUTO);
            }
        }
        // todo
        if($match->team_1_id && $match->team_2_id){
            $teamids = $match->team_1_id.','.$match->team_2_id;
            self::addTournamentTeam($match->team_1_id,$match->tournament_id);
            self::addTournamentTeam($match->team_2_id,$match->tournament_id);
            TournamentService::updateTeamSnapshot($teamids,2,$match->tournament_id,'tournament');
        }
        $logNewMatch = $match->toArray();
        // 记录log
        $filter = array_keys($logNewMatch);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($logOldMatch, $logNewMatch, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_MATCH,
                $logNewMatch['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewMatch],
                0,
                Consts::TAG_TYPE_CORE_DATA,
                null,
                $basisId,
                $userType,
                $userId,
                ''
            );
        }
        return $match;
    }

    public static function addTournamentTeam($teamId,$tournamentId){
        $model = TournamentTeamRelation::find()->where(['tournament_id' => $tournamentId, 'team_id' => $teamId])->one();
        if (!$model) {
            $model = new TournamentTeamRelation();
            $model->setAttribute('tournament_id', $tournamentId);
        }
        if($teamId != -1){
            $relationInfo['team_id'] = $teamId;
            $relationInfo['type'] = 1;
            $relationInfo['tournament_id'] = $tournamentId;
            $model->setAttributes($relationInfo);
            if (!$model->save()) {
                throw new BusinessException($model->getErrors(), '保存关联失败');
            }
        }
        return;
    }

    // slug生成
    public static function matchTeamSlugGenerate($teamOneId,$teamTwoId,$dateTime,$gameId){
        if($teamOneId==-1){
            $teamOneName = 'TBD';
        }else{
            $teamOneName = self::teamIdGetTeamName($teamOneId,$gameId) ?? 'TBD';
        }
        if($teamTwoId==-1){
            $teamTwoName = 'TBD';
        }else{
            $teamTwoName = self::teamIdGetTeamName($teamTwoId,$gameId) ?? 'TBD';
        }
        // 提取计划开始时间
        $scheduledDate = date('Y-m-d', strtotime(date($dateTime)));
        $scheduledDate = str_replace("-","_",$scheduledDate);
        return $teamOneName."_"."vs"."_".$teamTwoName."_".$scheduledDate;
    }
    // team简称
    public static function teamIdGetTeamName($teamId,$gameId){
        $team = Team::find()->select('name')->where(['id'=>$teamId,'game'=>$gameId])->asArray()->one();
        return Common::makeConstantMatching($team['name']);
    }

    public static function setMatchBaseData($matchId, $attribute, $userType, $userId, $basisId = 0)
    {
        // 这里不用id，以免产生冲突
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldTeamBase = [];
        $updateData = $attribute;
        unset($updateData['id']);
        $updateData['match_id'] = $matchId;
        $matchBase = MatchBase::find()->where(['match_id' => $matchId])->one();
        $logOldMatchBase= [];
        if ($matchBase) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $logOldMatchBase = $matchBase->toArray();
            $matchBase->setAttributes($attribute);

            //修改就主表变更时间
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setMatchUpdateTime($matchId,$time);
        } else {
            $matchBase = new MatchBase();
            $matchBase->setAttributes($updateData);
        }
        if (!$matchBase->save()) {
            throw new BusinessException($matchBase->getErrors(), "设置base_data失败");
        }
        $logNewMatchBase = $matchBase->toArray();

        // 记录log
        $filter = array_keys($logNewMatchBase);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($logOldMatchBase, $logNewMatchBase, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_MATCH,
                $matchId,
                ["diff" => $diffInfo["diff"], "new" => $logNewMatchBase],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $matchBase;
    }

    public static function setRealTimeInfo($matchId,$data,$dataType,$basisId = null , $adminType=1,$adminId=0)
    {
        switch($dataType){
            case self::MATCH_INFO_TYPE_AUTO:
                self::setHotAutoInfo($matchId,$data);
//                self::setAutoInfo($matchId,$data);
                break;
            case self::MATCH_INFO_TYPE_BAN_PICK:
                self::setBanPickInfo($matchId,$data);
                break;
            case self::MATCH_INFO_TYPE_MAP:
                self::setMapInfo($matchId,$data);
                break;
            case self::MATCH_INFO_TYPE_DETAIL:
                unset($data['map_ban_pick']);
//                unset($data['map_info']);
                self::setDetail($matchId,$data,$basisId, $adminType,$adminId);
                break;
            case self::MATCH_INFO_TYPE_DETAIL_BY_SOCKET_FRAMES:
                self::setDetailBySocketFrams($matchId,$data);
                break;
            case self::MATCH_INFO_TYPE_DETAIL_BY_ORDER_SOCKET_FRAMES:
                self::setDetailByOrderSocketFrams($matchId,$data);
                break;
        }
        if ($basisId) {
            DataTodoService::setDealStatus($basisId, Consts::TODO_DEAL_STATUS_DONE);
        }
        return self::getRealTimeInfo($matchId);
    }

    public static function getDetail($matchId)
    {
        $m=self::getRealTimeInfoModelById($matchId);
        return $m->toArray();
    }

    public static function setDetail($matchId,$data,$basisId = null , $userType=1,$userId=0)
    {
        if ($data['team_1_score'] === null || $data['team_1_score'] === '0') {
            $data['team_1_score'] = 0;
        }
        if ($data['team_2_score'] === null || $data['team_2_score'] === '0') {
            $data['team_2_score'] = 0;
        }
        if ($matchId){
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
        }else{
            $operationType = OperationLogService::OPERATION_TYPE_ADD;

        }

        $m=self::getRealTimeInfoModelById($matchId);
        $logOldMatch = $m->toArray();
        $m->setAttributes($data);
        if(!$m->save()){
            throw new BusinessException($m->getErrors(),'保存失败');
        }

        $time = date("Y-m-d H:i:s",time());
        $upTime = self::setMatchUpdateTime($matchId,$time);

        $logNewMatch = $m->toArray();
        // 记录log
        $filter = array_keys($logNewMatch);
        $filter = array_diff($filter, ["created_time", "modified_time"]);
        $diffInfo = Common::getDiffInfo($logOldMatch, $logNewMatch, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_MATCH,
                $logNewMatch['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewMatch],
                0,
                Consts::TAG_TYPE_REAL_TIME,
                null,
                $basisId,
                $userType,
                $userId
            );
        }




    }
    //frames 结束了 就不更新了
    public static function setDetailBySocketFrams($matchId,$data){
        $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $m=self::getRealTimeInfoModelById($matchId);
            if (isset($m->status) && $m->status == 3){}else{
                $logOldMatch = $m->toArray();
                $m->setAttributes($data);
                if(!$m->save()){
                    throw new BusinessException($m->getErrors(),'Frames保存比赛数据失败！');
                }
                $logNewMatch = $m->toArray();
                // 记录log
                $filter = array_keys($logNewMatch);
                $filter = array_diff($filter, ["created_time", "modified_time"]);
                $diffInfo = Common::getDiffInfo($logOldMatch, $logNewMatch, $filter);
                if ($diffInfo['changed']) {
                    OperationLogService::addLog($operationType,
                        Consts::RESOURCE_TYPE_MATCH,
                        $logNewMatch['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNewMatch],
                        0,
                        Consts::TAG_TYPE_REAL_TIME,
                        null,
                        null,
                        1,
                        0
                    );
                }
            }
            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw new BusinessException($e->getMessage(), "比赛结束了，不更新数据了！");
        }
    }
    //frames 结束了 就不更新了 Order 顺序
    public static function setDetailByOrderSocketFrams($matchId,$data){
        $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $m=self::getRealTimeInfoModelById($matchId);
            $logOldMatch = $m->toArray();
            $m->setAttributes($data);
            if(!$m->save()){
                throw new BusinessException($m->getErrors(),'Frames保存比赛数据失败！');
            }
            $logNewMatch = $m->toArray();
            // 记录log
            $filter = array_keys($logNewMatch);
            $filter = array_diff($filter, ["created_time", "modified_time"]);
            $diffInfo = Common::getDiffInfo($logOldMatch, $logNewMatch, $filter);
            if ($diffInfo['changed']) {
                OperationLogService::addLog($operationType,
                    Consts::RESOURCE_TYPE_MATCH,
                    $logNewMatch['id'],
                    ["diff" => $diffInfo["diff"], "new" => $logNewMatch],
                    0,
                    Consts::TAG_TYPE_REAL_TIME,
                    null,
                    null,
                    1,
                    0
                );
            }

            $transaction->commit();
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw new BusinessException($e->getMessage(), "比赛结束了，不更新数据了！");
        }
    }
    public static function getRealTimeInfo($matchId)
    {
        //比赛信息
        $match=MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
        //hot表信息
        $hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId])->orderBy('auto_status asc , origin_id asc')->asArray()->one();
        //$auto_add_sub_type
        $auto_add_sub_type_list = [];
        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchId])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
        //优先判断数据源是不是一样
        if ($updateInfo['origin_id'] == 9 || $updateInfo['origin_id'] == 10){
            //查询他的 录入方式列表
            $where =['and',
                ['=','std.origin_id',$updateInfo['origin_id']],
                ['=','rel.master_id',$matchId],
            ];
            $auto_add_sub_type_res = StandardDataMatch:: find()->alias('std')
                ->select(['bc.types'])
                ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type= "match" ')
                ->leftJoin('bayes_coverages as bc' , 'std.rel_identity_id = bc.rel_identity_id')
                ->where($where)->asArray()->one();
            if ($auto_add_sub_type_res){
                $auto_add_sub_type_list = explode(',',$auto_add_sub_type_res['types']);
            }
        }
        $info=[
            'match_id'=>$matchId,
            self::MATCH_INFO_TYPE_AUTO=>['auto_origin_id'=>$hotInfo['origin_id'],'auto_status'=>$hotInfo['auto_status'],'auto_add_type'=>$hotInfo['auto_add_type'],'auto_add_sub_type'=>$hotInfo['auto_add_sub_type'],'auto_add_sub_type_list' => $auto_add_sub_type_list],
            self::MATCH_INFO_TYPE_BAN_PICK=>json_decode($match['map_ban_pick'],true),
            self::MATCH_INFO_TYPE_MAP=>json_decode($match['map_info'],true),
            self::MATCH_INFO_TYPE_DETAIL=>self::getDetail($matchId),
        ];
        return $info;
    }

    public static function getRelStandardInfo($matchId,$originId)
    {
        $q=StandardDataMatch::find()->alias('sm')
            ->leftJoin('data_standard_master_relation rel','sm.id=rel.standard_id and rel.resource_type="match"')
            ->where(['and',['=','sm.origin_id',$originId],['=','rel.master_id',$matchId]]);
        return $q->one();
    }

    private static function addToHotListFive($matchId,$sortingLog)
    {
        $hotInfo=HotDataRunningMatch::find()
            ->where(['match_id'=>$matchId])
            ->andWhere(['origin_id'=>5])
            ->one();
        if($hotInfo){
            $hotInfo->setAttributes([
                'auto_status'=>1,
                'from_event_id'=>0,
                'to_event_id'=>0
            ]);
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存失败');
            }
        }else{
            $hot=new HotDataRunningMatch();
            $hot->setAttributes([
                'match_id'=>$matchId,
                'standard_id'=>$sortingLog['id'],
                'origin_id'=>5,
                'game_id'=>1,
                'rel_identity_id'=>'',
                'match_start_time'=>'',
                'auto_status'=>1
            ]);
            if(!$hot->save()){
                throw new BusinessException($hot->getErrors(),'保存失败');
            }
        }
    }

    private static function addToHotList($matchId,$standardInfo)
    {

        $hotInfo=HotDataRunningMatch::find()->where(['standard_id'=>$standardInfo['id']])->one();
        if($hotInfo){
            $hotInfo->setAttribute('auto_status',1);
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存失败');
            }
        }else{
            $hot=new HotDataRunningMatch();
            $hot->setAttributes([
                'match_id'=>$matchId,
                'standard_id'=>$standardInfo['id'],
                'origin_id'=>$standardInfo['origin_id'],
                'game_id'=>$standardInfo['game_id'],
                'rel_identity_id'=>$standardInfo['rel_identity_id'],
                'match_start_time'=>$standardInfo["scheduled_begin_at"],
                'auto_status'=>1,
            ]);
            if(!$hot->save()){
                throw new BusinessException($hot->getErrors(),'保存失败');
            }
        }
    }
    public static function addHotAutoListByCreateMatch($matchId,$info){
        $origin_id = $info['auto_origin_id'];
        $hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId])->one();
        if($hotInfo){
            //还不知道会不会走这里
            if (isset($info['auto_status']) && $info['auto_status']){
                $hotInfo->setAttribute('auto_status',$info['auto_status']);
            }
            if (isset($info['auto_add_type']) && $info['auto_add_type']){
                $hotInfo->setAttribute('auto_add_type',$info['auto_add_type']);
            }
            if (isset($info['auto_add_sub_type']) && $info['auto_add_sub_type']){
                $hotInfo->setAttribute('auto_add_sub_type',$info['auto_add_sub_type']);
            }
            $hotInfo->setAttribute('origin_id',$origin_id);
            $hotInfo->save();
        }else{
            //新建比赛
            $insertData = [
                'match_id'=>$matchId,
                'origin_id'=>$origin_id,
                'game_id'=>@$info['game_id'],
                'auto_add_type'=>'restapi',
                'auto_status'=>1,
                'match_start_time'=>@$info["match_start_time"],
                'match_status'=>1
            ];
            $hot=new HotDataRunningMatch();
            $hot->setAttributes($insertData);
            if(!$hot->save()){
                throw new BusinessException($hot->getErrors(),'保存HOTDATA失败');
            }
        }
    }
    public static function addHotAutoList($matchId,$standardInfo,$info)
    {
        $hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId])->one();
        if(!$hotInfo) {
            $hotInfo = new HotDataRunningMatch();
        }
        $insertData = [
            'match_id'=>$matchId,
            'standard_id'=>$standardInfo['id'],
            'origin_id'=>$info['auto_origin_id'],
            'game_id'=>$standardInfo['game_id'],
            'rel_identity_id'=>$standardInfo['rel_identity_id'],
            'match_start_time'=>$standardInfo["scheduled_begin_at"]
        ];
        if (isset($info['auto_status']) && $info['auto_status']){
            $insertData['auto_status'] = $info['auto_status'];
        }
        if (isset($info['auto_add_type']) && $info['auto_add_type']){
            $insertData['auto_add_type'] = $info['auto_add_type'];
        }
        if (isset($info['auto_add_sub_type']) && $info['auto_add_sub_type']){
            $insertData['auto_add_sub_type'] = $info['auto_add_sub_type'];
        }
        $hotInfo->setAttributes($insertData);
        if(!$hotInfo->save()){
            throw new BusinessException($hotInfo->getErrors(),'保存失败');
        }
    }

    public static function setStop($standardId)
    {
        $hotInfo=HotDataRunningMatch::find()->where(['standard_id'=>$standardId])->one();
        if($hotInfo){
            $hotInfo->setAttribute('auto_status',2);
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存失败');
            }
        }
    }
    //setAutoStop
    public static function setAutoStop($matchId,$autoOriginId)
    {
        //$hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId,'origin_id' =>$autoOriginId])->one();
        $hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId])->one();
        if($hotInfo){
            $hotInfo->setAttribute('auto_status',2);
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存失败');
            }
        }
    }
    public static function setAutoInfo($matchId,$info)
    {
        // 检查对应数据源是否有绑定关系
        $autoStatus=$info['auto_status'];
        $autoOriginId=$info['auto_origin_id'];

        if($autoStatus==1){
            if($autoOriginId==5){
                // 5e的赛事绑定状态不在关联关系，在sorting_log
                $sortingLog=SortingLog::find()->where(['match_id'=>$matchId])->one();
                if(!$sortingLog){
                    throw new BusinessException([],'没有对应数据源的绑定信息');
                }
                self::addToHotListFive($matchId,$sortingLog);
            }else{
                // 这里检查绑定状态，如果有则添加到刷新队列，如果没有，就提示失败
                $standardInfo=self::getRelStandardInfo($matchId,$autoOriginId);
                if(!$standardInfo){
                    throw new BusinessException([],'没有对应数据源的绑定信息');
                }
                self::addToHotList($matchId,$standardInfo);
            }
        }else{
            if($autoOriginId==5){
                self::setAutoStop($matchId,$autoOriginId);
            }else {
                $standardInfo = self::getRelStandardInfo($matchId, $autoOriginId);
                if ($standardInfo) {
                    self::setStop($standardInfo["id"]);
                }
            }
        }
        $m=self::getRealTimeInfoModelById($matchId);
        $m->setAttributes($info);
        if(!$m->save()){
            throw new BusinessException($m->getErrors(),'保存失败');
        }
    }

    public static function setHotAutoInfo($matchId,$info)
    {
        //根绝MatchID 获取当前录入的数据源
        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchId])->andWhere(['tag_type'=>'update_data'])->asArray()->one();
        //优先判断数据源是不是一样
        if ($updateInfo['origin_id']){
            $autoOriginId=$updateInfo['origin_id'];
            $info['auto_origin_id'] = $updateInfo['origin_id'];
        }else{
            return false;
        }
        //如果设置了 auto_add_type
        if($autoOriginId==5){
            // 5e的赛事绑定状态不在关联关系，在sorting_log
            $sortingLog=SortingLog::find()->where(['match_id'=>$matchId])->one();
            if(!$sortingLog){
                throw new BusinessException([],'没有对应数据源的绑定信息'.$autoOriginId);
            }
            self::addToHotListFive($matchId,$sortingLog);
        }else{
            //查找绑定状态
            $standardInfo=self::getRelStandardInfo($matchId,$autoOriginId);
            if(!$standardInfo){
                self::addHotAutoListByCreateMatch($matchId,$info);
            }else{
                self::addHotAutoList($matchId,$standardInfo,$info);
            }
        }
        return true;
    }
    //更新MatchRealTimeInfo数据
    public static function updateMatchRealTimeInfo($matchId,$data){
        $m = MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
        $m->setAttributes($data);
        if(!$m->save()){
            throw new BusinessException($m->getErrors(),'保存MatchRealTimeInfo失败');
        }
    }
    //更新hot表信息
    public static function updateHotDataRunningMatchInfo($matchId,$info){
        $hotInfo=HotDataRunningMatch::find()->where(['match_id'=>$matchId])->one();
        if($hotInfo){
            foreach ($info as $key => $item){
                $hotInfo->setAttribute($key,$item);
            }
            if(!$hotInfo->save()){
                throw new BusinessException($hotInfo->getErrors(),'保存HotDataRunningMatchInfo失败');
            }
        }
        return true;
    }

    public static function setBanPickInfo($matchId,$info)
    {
        $enInfo = json_encode($info);
        if ($enInfo == '[{"team":"","type":"","map":""}]') {
            $newInfoJson = null;
        }else{
            $newInfoJson = $enInfo;
        }

        $m=self::getRealTimeInfoModelById($matchId);
        $m->setAttribute('map_ban_pick',$newInfoJson);
        if(!$m->save()){
            throw new BusinessException($m->getErrors(),'保存失败');
        }
    }

    public static function setMapInfo($matchId,$info)
    {
        $m=self::getRealTimeInfoModelById($matchId);
        $m->setAttribute('map_info',json_encode($info));
        if(!$m->save()){
            throw new BusinessException($m->getErrors(),'保存失败');
        }
    }

    public static function getRealTimeInfoModelById($matchId)
    {
        $model=MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
        if(!$model){
            $model=new MatchRealTimeInfo();
            $model->setAttribute('id',$matchId);
        }
        return $model;
    }

    /**
     * 获取日志分拣比赛详情
     * $sortingLogId        sorting_log表主键ID
     */
    public static function getLogMatchInfo($sortingLogId)
    {
        $logData = SortingLog::find()
            ->where(['id' => $sortingLogId])
            ->select('match_id,log_type,service_ip')
            ->one();
        if(isset($logData['match_id']) && !empty($logData['match_id'])){
            $matchId = $logData['match_id'];
        }else{
            return [];
        }
        $map = [];
        if(isset($logData['log_type']) && !empty($logData['log_type'])){
            //暂时注释掉这个条件，这个值可能是错误的
//            $map = ['=','log_type',$logData['log_type']];
        }
        $matchData = Match::find()->where(['id' => $matchId])->select('scheduled_begin_at,number_of_games')->one();
        $numberOfGames=$matchData['number_of_games'];
        if(isset($matchData['scheduled_begin_at']) && !empty($matchData['scheduled_begin_at'])){
            $planStartTime = $matchData['scheduled_begin_at'];
            $beginTime = date("Y-m-d H:i:s",strtotime("-30 minute",strtotime($planStartTime)));
            $endTime = date("Y-m-d H:i:s",strtotime("+$numberOfGames hour",strtotime($planStartTime)));//查询结束时间节点
        }else{
            return [];
        }
        $eventMap = ['or',
            ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT],
            ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T],
            ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_LEFT_BUY_ZONE_WITH],
        ];
        $service_ip = json_decode($logData['service_ip'], true);
        $refresh_start_time = $beginTime;
        $refresh_end_time = $endTime;
        foreach ($service_ip as $k => $v) {
            $refresh_start_time = $v['refresh_start_time'] ? $v['refresh_start_time'] : $beginTime;
            $refresh_end_time   = $v['refresh_end_time'] ? $v['refresh_end_time'] : $endTime;
        }


        $timeMap = ['and',
            ['>=','created_at',$refresh_start_time],
            ['<=','created_at',$refresh_end_time],
        ];
        $q = ReceiveData5eplayFormatEvents::find()
            ->where($eventMap)
            ->andWhere($timeMap)
            ->andWhere($map)
            ->select('ip_address,created_at')
            ->groupBy('ip_address');
        $sql=$q->createCommand()->getRawSql();
        $ipData=$q->asArray()->all();
        $steamHelper = new SteamHelper();
        $info = [];
        $result = [];
        foreach($ipData as $ipKey => $ipValue){
            $teamEventMap = ['or',
                ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT],
                ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T],
                ['=','event_type',CsgoLogFormatBase::EVENT_TYPE_LEFT_BUY_ZONE_WITH],
            ];
            $teamNameData = ReceiveData5eplayFormatEvents::find()
                ->where($teamEventMap)
                ->andWhere(['ip_address' => $ipValue['ip_address']])
                ->andWhere($timeMap)
                ->andWhere($map)
                ->select('info,created_at')
                ->asArray()
                ->all();
            $ipTeamsTmp=[];
            $elsPlayerTmp=[];
            $elsTeamTmp=[];
            $ipPlayersTmp=[];
            $log_time = '';
            $event_time = '';
            foreach($teamNameData as $teamKey => $teamValue){
                $eventInfo = json_decode($teamValue['info'],true);
                if($teamKey == 0){
                    if(isset($eventInfo['log_time']) && !empty($eventInfo['log_time'])){
                        $log_time=self::changeTime($eventInfo['log_time']);
                        $event_time=$teamValue['created_at'];
                    }
                }
                if(isset($eventInfo['log_team_name']) && !empty($eventInfo['log_team_name'])){
                    $ipTeamsTmp[$eventInfo['log_team_name']]=$eventInfo['log_team_name'];
                }
                if(isset($eventInfo['playerName']) && isset($eventInfo['playID'])){
                    if($eventInfo['playID']=="BOT"){
                        continue;
                    }
                    $steamIdChange = $steamHelper->playerConversionSteamId($eventInfo['playID']);
                    $elsPlayer = Player::find()->where(['like','steam_id',$steamIdChange])->asArray()->one();
                    if($elsPlayer){
                        $player = [
                            'player_nickname' => $eventInfo['playerName'],
                            'steam_id' => $eventInfo['playID'],
                            'steamID64' => $steamIdChange,
                            'els_player_id' => $elsPlayer['id'],
                            'els_player_nick_name' =>$elsPlayer['nick_name'],
                        ];
                        $teamItem = TeamPlayerRelation::find()->alias('tr')
                            ->select(['team.name','team.id'])
                            ->leftJoin("team","team.id = tr.team_id")
                            ->where(['tr.player_id'=>$elsPlayer['id']])->asArray()->one();
                        if($teamItem){
                            $player['team_id'] = $teamItem['id'];
                            $elsTeamTmp[$teamItem['name']][$eventInfo['playID']]=$player;
                        }else{
                            $player = [
                                'player_nickname' => $eventInfo['playerName'],
                                'steam_id' => $eventInfo['playID'],
                                'steamID64' => $steamIdChange,
                                'els_player_id' => $elsPlayer['id'],
                                'els_player_nick_name' =>$elsPlayer['nick_name'],
                                'team_id' => '',
                            ];
                            $ipPlayersTmp[$eventInfo['playID']]=$player;
                        }
                    }else{
                        $player = [
                            'player_nickname' => $eventInfo['playerName'],
                            'steam_id' => $eventInfo['playID'],
                            'steamID64' => $steamIdChange,
                            'els_player_id' => '',
                            'els_player_nick_name' =>'',
                            'team_id' => '',
                        ];
                        $ipPlayersTmp[$eventInfo['playID']]=$player;
                    }
                }
                $elsTeamTmpArray = [];
                if(isset($elsTeamTmp) && count($elsTeamTmp)>0){
                    foreach ($elsTeamTmp as $key=>$value){
                        $elsTeamTmpArray[$key] = array_values($value);
                    }
                }
                $result[$ipValue['ip_address']] =[
                    "created_at" => $ipValue['created_at'],
                    "ip"=>$ipValue['ip_address'],
                    "teams"=>$ipTeamsTmp,
                    "players"=>array_values($ipPlayersTmp),
//                    "els_players"=>array_values($elsPlayerTmp),
                    "log_time"=>$log_time,
                    "event_time"=>$event_time,
                    "els_teams"=>$elsTeamTmpArray,
                ];
            }
        }
        $info['result'] = $result;
        $info['match_id'] = $matchId;
        $info['begin_time'] = $beginTime;
        $info['end_time'] = $endTime;
        $info['ip_data'] = $ipData;
        return $info;
    }

    /**
     * @param $matchId
     * @return array|\yii\db\ActiveRecord[]
     * (返回推荐数据源接口直播url)
     */
    public static function getLiveSteamList($params)
    {
        $relation = DataStandardMasterRelation::find()->alias('rel')
            ->select(
                ['video.id as video_id','enum_origin.desc as origin_name','v.name as video_origin_name','v.id as video_origin_id',
                    'video.streamer',
                'video.title','video.room_num','country.name as country','video.platform_name','video.platform_id',
                'video.preview','video.match_rel_id','video.official_stream_url','video.live_url',
                'video.live_embed_url','std.id', 'video.viewers', 'video.origin_id','video.modified_time as modified_time',
                    'video.game_id'
                ])//3836
            ->leftJoin('standard_data_match as std','std.id = rel.standard_id')
            ->leftJoin('standard_data_match_video_list as video','video.match_rel_id = std.rel_identity_id')
            ->leftJoin('match','rel.master_id = match.id')
            ->leftJoin('enum_origin','enum_origin.id = video.origin_id')
            ->leftJoin('enum_video_origin as v', 'v.id = video.video_origin_id')
            ->leftJoin('enum_country as country', 'country.id = video.country')
            ->where(['rel.resource_type' => 'match','match.id'=> $params['match_id']]);
//            ->andWhere(['<>','video.id',null]);

        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $info = $relation->asArray()->all();

        $teamName = Match::find()->alias('m')
            ->select(['t.full_name','t.short_name','t.name','t.alias','t2.full_name as t_full_name','t2.short_name as t_short_name','t2.name as t_name','t2.alias as t_alias','m.game'])
            ->leftJoin('team as t','t.id = m.team_1_id')
            ->leftJoin('team as t2','t2.id = m.team_2_id')
            ->where(['m.id'=>$params['match_id']])
            ->asArray()->one();
        $matchVideo = StandardDataMatchVideoList::find()->alias('video')
            ->select(['video.id as video_id','enum_origin.desc as origin_name',
                'video.streamer','video.title','video.room_num','country.name as country','video.platform_name',
                    'video.platform_id','video.preview','video.viewers', 'video.match_rel_id','video.official_stream_url',
                    'video.live_url','video.live_embed_url', 'video.origin_id','v.id as video_origin_id',
                'v.name as video_origin_name','video.modified_time as modified_time','video.game_id'
                ])
             ->leftJoin('enum_origin','enum_origin.id = video.origin_id')
             ->leftJoin('enum_video_origin as v', 'v.id = video.video_origin_id')
            ->leftJoin('enum_country as country', 'country.id = video.country')
            ->andFilterWhere(['or',
                ['like','title',$teamName['full_name']],
                ['like','title',$teamName['short_name']],
                ['like','title',$teamName['name']],
                ['like','title',$teamName['alias']],
                ['like','title',$teamName['t_full_name']],
                ['like','title',$teamName['t_short_name']],
                ['like','title',$teamName['t_name']],
                ['like','title',$teamName['t_alias']]
                ])
            ->andWhere(['video.game_id'=>$teamName['game']])
            ->asArray()->all();
        $data = array_merge($info,$matchVideo);
        $dataDesc = self::my_array_sort($data,'viewers',SORT_DESC);

        $matchStreamList = MatchStreamList::find()->select(['id','video_id','live_url'])
            ->where(['and',
//                ['>','video_id','0'],
                ['=','match_id',$params['match_id']]
            ])->asArray()->all();

        $newUrl = [];
        foreach ($matchStreamList as $videokey => $videoval) {
            $newUrl[] = md5($videoval['live_url']);

        }

//        $addIds=array_column($matchStreamList,'video_id');
        $newArr = [];
        foreach ($dataDesc as $key=>&$val){
            $val['origin_name'] = $val['video_origin_name'] ?: $val['origin_name'];
            $val['is_add']=in_array(md5($val['live_url']),$newUrl);
            $newArr[$val['video_id']] = $val;
        }

//        $dataDesc = array_column($dataDesc,null,'video_id');

//        var_dump($newArr);die;

        $count = count($newArr);//总条数
        $start=$page*$pageSize;//偏移量，当前页-1乘以每页显示条数
        $newData = array_slice($newArr,$start,$pageSize,true );
        return ['list' => array_values($newData), 'total' => $count];
    }
    public static function addLiveSteam($videoId,$matchId)
    {
        $videoInfo = StandardDataMatchVideoList::find()->where(['id'=>$videoId])->one();
        $streamInfo=new MatchStreamList();
        $matchStream = MatchStreamList::find()
            ->where(['live_url' => $videoInfo['live_url']])
            ->andWhere(['match_id'=> $matchId])
            ->one();

        if($matchStream){
            throw new BusinessException($matchStream->getErrors(),'该视频源已添加，请勿重复添加');
        }
        $platformName = self::getInfoFromUrl($videoInfo['live_url'])['platform_name'];

        $data= [
            'origin_id' => $videoInfo['origin_id'] ?: '',
            'video_origin_id' => $videoInfo['video_origin_id'] ?: '',
            'streamer' => $videoInfo['streamer'],
            'title' => $videoInfo['title'],
            'country' => $videoInfo['country'],
            'viewers' => $videoInfo['viewers'],
            'match_id' => $matchId,
            'video_id' => $videoInfo['id'],
            'is_official' => 2,
            'live_url_identity' => md5($videoInfo['live_url']),
            'live_url' => $videoInfo['live_url'],
            'live_embed_url' => $videoInfo['live_embed_url'],
            'room_id' =>  self::getInfoFromUrl($videoInfo['live_url'])['room_id'],
            'platform_name' => $platformName,
            'platform_id' =>  (int)StreamService::getPlatformIdbyName($platformName)['id'] ? (int)StreamService::getPlatformIdbyName($platformName)['id'] : null,

        ];

        $streamInfo->setAttributes($data);
        if(!$streamInfo->save()){
            throw new BusinessException($streamInfo->getErrors(),'保存直播记录失败');
        }
    }

    /**
     * 根据直播地址获取平台
     * @param $url
     * @return array
     */
    public static function getInfoFromUrl($url)
    {
        $mp=[
            [
                'reg'=>'/^https\:\/\/www\.huya\.com\/(.*)$/',
                'type'=>'Huya'
            ],
            [
                'reg'=>'/^https\:\/\/www\.huomao\.com\/(.*)$/',
                'type'=>'Huomao'
            ],
            [
                'reg'=>'/^https\:\/\/www\.twitch\.tv\/(.*)$/',
                'type'=>'Twitch'
            ]
        ];
        $info=[
            'room_id'=>"",
            'platform_name'=>'',
            'url'=>$url,
        ];
        foreach($mp as $r){
            $mt=preg_match_all($r['reg'],$url,$matchAll);
            if($mt){
                $info['platform_name']=$r['type'];
                $info['room_id']=@$matchAll[1][0];
                break;
            }
        }
        return $info;
    }

    //二维数组排序方法
    //$arrays 是数组
    //$sort_key 排序的KEY
    //$sort_order 排序方式
    //$sort_type 排序类型
    public static function my_array_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
        return $arrays;
    }


    public static function getLiveSteamListUrl($params)
    {
        $relation = DataStandardMasterRelation::find()->alias('rel')
            ->select(['video.id as video_id','enum_origin.desc as origin_name','video.streamer',
                'video.title','video.room_num','video.country','video.platform_name','video.platform_id',
                'video.preview','video.match_rel_id','video.official_stream_url','video.live_url',
                'video.live_embed_url','std.id', 'video.viewers', 'video.origin_id'])//3836
            ->leftJoin('standard_data_match as std','std.id = rel.standard_id')
            ->leftJoin('standard_data_match_video_list as video','video.match_rel_id = std.rel_identity_id')
            ->leftJoin('match','rel.master_id = match.id')
            ->leftJoin('enum_origin','enum_origin.id = video.origin_id')
            ->where(['rel.resource_type' => 'match','match.id'=> $params['match_id']]);

        $info = $relation->asArray()->all();
        $teamName = Match::find()
            ->select(['t.full_name','t.short_name','t.name','t2.full_name as t_full_name','t2.short_name as t_short_name','t2.name as t_name'])
            ->where(['m.id'=>$params['match_id']])->alias('m')
            ->leftJoin('team as t','t.id = m.team_1_id')
            ->leftJoin('team as t2','t2.id = m.team_2_id')
            ->asArray()->one();
        $matchVideo = StandardDataMatchVideoList::find()->alias('video')
            ->select(['video.id as video_id','enum_origin.desc as origin_name',
                'video.streamer','video.title','video.room_num','video.country','video.platform_name',
                'video.platform_id','video.preview','video.viewers', 'video.match_rel_id','video.official_stream_url',
                'video.live_url','video.live_embed_url', 'video.origin_id'
            ])
            ->leftJoin('enum_origin','enum_origin.id = video.origin_id')
            ->andFilterWhere(['or',
                ['like','title',$teamName['full_name']],
                ['like','title',$teamName['short_name']],
                ['like','title',$teamName['name']],
                ['like','title',$teamName['t_full_name']],
                ['like','title',$teamName['t_short_name']],
                ['like','title',$teamName['t_name']]
            ])->asArray()->all();

        $data = array_merge($info,$matchVideo);
        $dataDesc = self::my_array_sort($data,'viewers',SORT_DESC);

        $matchStreamList = MatchStreamList::find()->select(['id','video_id'])
            ->where(['and',
                ['>','video_id','0'],
                ['=','match_id',$params['match_id']]
            ])->asArray()->all();

        $addIds=array_column($matchStreamList,'video_id');

        foreach ($dataDesc as $key=>&$val){
            $val['is_add']=in_array($val['video_id'],$addIds);
        }
        return $dataDesc;
    }
    //查看是否开启了自动录入
    public static function getMatchConfigAndHotData($origin_id,$game_id,$rel_match_id,$socket_time)
    {
        $auto_status = false;
        if ($origin_id == 9 || $origin_id == 10){
            $rel_match_id =  HotBase::getRelMatchIdByPerId($rel_match_id,$origin_id);
        }
        //查看standard 比赛表对应的数据
        $match_id = HotBase::getMainIdByRelIdentityId('match',$rel_match_id,$origin_id);
        if ($match_id){
            $updateInfo = DataResourceUpdateConfig::find()->select(['origin_id','update_type'])->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$match_id])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
            $updateInfo_origin_id = @$updateInfo['origin_id'];
            //外面的录入状态  3是自动
            $updateInfo_update_type = @$updateInfo['update_type'];
            //首先判断数据源是否一样
            if ($updateInfo_origin_id == $origin_id && $updateInfo_update_type == 3){
                //把hot表 auto_status 和 auto_add_type
                $HotDataRunningMatchRes = HotDataRunningMatch::find()->where(['match_id' => $match_id])->one();
                //判断是不是自动录入
                $auto_status = @$HotDataRunningMatchRes['auto_status'];
                $auto_add_type = @$HotDataRunningMatchRes['auto_add_type'];
                if ($auto_status == 1){
                    $auto_status = true;
                    if ($auto_add_type == 'restapi'){
                        //设置
                        $HotDataRunningMatchRes->setAttributes([
                            'deal_status' => 1,
                            'auto_add_type' => 'socket'
                        ]);
                        $HotDataRunningMatchRes->save();
                    }
                }
            }
        }
        return $auto_status;
    }
    //查看是否开启了自动录入
    public static function getMatchConfigAndHotDataByLog($origin_id,$game_id,$rel_match_id,$socket_time)
    {
        $auto_status = 2;
        if ($origin_id == 9 || $origin_id == 10) {
            $rel_match_id =  HotBase::getRelMatchIdByPerId($rel_match_id,$origin_id);
        }
        //查看standard 比赛表对应的数据
        $match_id = HotBase::getMainIdByRelIdentityId('match',$rel_match_id,$origin_id);
        if ($match_id){
            $updateInfo = DataResourceUpdateConfig::find()->select(['origin_id','update_type'])->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$match_id])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
            $updateInfo_origin_id = @$updateInfo['origin_id'];
            //外面的录入状态  3是自动
            $updateInfo_update_type = @$updateInfo['update_type'];
            //首先判断数据源是否一样
            if ($updateInfo_origin_id == $origin_id && $updateInfo_update_type == 3){
                //把hot表 auto_status 和 auto_add_type
                $HotDataRunningMatchRes = HotDataRunningMatch::find()->where(['match_id' => $match_id])->one();
                //判断是不是自动录入
                $auto_status = @$HotDataRunningMatchRes['auto_status'];
                $auto_add_type = @$HotDataRunningMatchRes['auto_add_type'];
                if ($auto_status == 1){
                    $auto_status = 1;
                    if ($auto_add_type == 'restapi'){
                        //设置
                        $HotDataRunningMatchRes->setAttributes([
                            'deal_status' => 1,
                            'auto_add_type' => 'socket'
                        ]);
                        $HotDataRunningMatchRes->save();
                    }
                }else{
                    $auto_status =2;
                }
            }
        }
        return $auto_status;
    }

    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据identityId和originId获取对应的主表id，有可能是空
     */
    public static function getMainIdAndStandardIdByRelIdentityId($resourceType,$relIdentityId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',(int)$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            $resData = [
                'master_id' => $info['master_id'],
                'standard_id' => $info['id'],
            ];
            return $resData;
        }
    }

    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据identityId和originId获取对应的主表id，有可能是空
     */
    public static function getRelIdentityIdAndMainId($resourceType,$masterId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','m.id',(int)$masterId],
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->leftJoin('match as m','m.id = rel.master_id')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info['rel_identity_id'];
        }else{
            return null;
        }
    }
    //重刷rest api
    public static function refreshMatchInfo($match_id,$origin_id,$gameType){
        //获取match的所有battle
        $battleList = MatchBattle::find()->select('id')->where(['match'=>$match_id])->asArray()->all();
        if (count($battleList)){
            foreach ($battleList as $key =>$item){
                $battleId = $item['id'];
                BattleService::deleteBattle($gameType, $battleId);
            }
        }
        //删除完成 添加一条任务
        $result = OperationRefreshHotInfo::refreshMatchInfoByMatchId($match_id,$origin_id,'rest_api',$gameType,$match_id);
        return $result;
    }
    public static function refreshMatchInfoOnCsgoLog($match_id,$origin_id,$operate_type){
        if ($origin_id == 5) {
            $sortingLog = SortingLog::find()->where(['match_id' => $match_id])->asArray()->one();
            if (!empty($sortingLog)) {
                $service_ip = json_decode($sortingLog['service_ip'], true);
                if (empty($service_ip)){
                    return false;
                }
                foreach ($service_ip as $k1 => $v1) {
                    $search_ip = $v1['service_ip'];
                }
                $taskInfo   = TaskInfo::find()->andWhere(['tag' => 'event_ws.5eplay....refresh'])->andWhere(['and',['<', 'status', 3],['redis_server'=>$search_ip]])->andWhere(['>=', 'created_time', date("Y-m-d H:i:s", strtotime("-1 minute"))])->asArray()->one();
                if (empty($taskInfo)) {
                    $update_data = [
                        'team_1_score' => 0,
                        'team_2_score' => 0,
                        'winner' => null
                    ];
                    if ($operate_type != 'xiufu'){
                        $update_data['status'] = 2;
                        $update_data['end_at'] = null;
                    }
                    //更新 match_real_time_info
                    $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$match_id])->one();
                    if ($MatchRealTimeInfo){
                        $MatchRealTimeInfo->setAttributes($update_data);
                        $MatchRealTimeInfo->save();
                    }

                    $match              = Match::find()->andWhere(['id' => $match_id])->asArray()->one();
                    $number             = @$match['number_of_games'] ? @$match['number_of_games'] : 3;
                    $scheduled_begin_at = $match['scheduled_begin_at'];
                    $start_time         = date('Y-m-d H:i:s', strtotime($scheduled_begin_at) - 1800);
                    $end_time           = date('Y-m-d H:i:s', strtotime($scheduled_begin_at) + 5400 * $number);
                    $where = ['or'];
                    foreach ($service_ip as $k => $v) {
                        $refresh_start_time = $v['refresh_start_time'] ? $v['refresh_start_time'] : $start_time;
                        $refresh_end_time   = $v['refresh_end_time'] ? $v['refresh_end_time'] : $end_time;
//                        $vArr = explode(';',$v['service_ip']);
                        $last_ip = $v['service_ip'];
                        $where[]           = [
                            'and',
                            ['in', 'ip_address', $v['service_ip']],
                            ['>=', 'created_at', $refresh_start_time],
                            ['<=', 'created_at', $refresh_end_time]
                        ];
                    }
                    $info = [
                        'tag'          => 'event_ws.5eplay....refresh',
                        'params'       => [
                            'where'          => $where,
                            'del_redis_keys' => 'ws:' . $match_id . ':',
                            'match_id'       => $match_id,
                        ],
                        'redis_server' => 'order_csgolog_'.$last_ip,
                    ];
                    \app\modules\task\services\TaskRunner::addTask($info, 9002);
                    return true;
                } else {
                    throw new BusinessException([], "已经有一个正在重刷中的程序,请等待");
//                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '已经有一个正在重刷中的程序,请等待'];
                }
            } else {
                return false;
            }

        }else{
            return false;
        }

    }

    public static function refreshMatchInfoOnCsgoHltv($operate_type,$match_id,$origin_id,$gameType,$type='all'){
        if ($origin_id == 7) {
            $rel_match_id_res = HotBase::getMetadataByMasterId('match',$match_id,$origin_id);
            $rel_match_id = $rel_match_id_res['rel_identity_id'];
            $taskInfo   = TaskInfo::find()->andWhere(['tag'=>'websocket_match_data.7.websocket_match_data.add.1.refresh'])->andWhere(['redis_server' => 'order_hltvcsgo_'.$rel_match_id])->andWhere(['<', 'status', 3])->andWhere(['>=', 'created_time', date("Y-m-d H:i:s", strtotime("-1 minute"))])->asArray()->one();
            if (empty($taskInfo)) {
                $update_data = [
                    'team_1_score' => 0,
                    'team_2_score' => 0,
                    'winner' => null
                ];
                if ($operate_type != 'xiufu'){
                    $update_data['status'] = 2;
                    $update_data['end_at'] = null;
                }
                //更新 match_real_time_info
                $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$match_id])->one();
                if ($MatchRealTimeInfo){
                    $MatchRealTimeInfo->setAttributes($update_data);
                    $MatchRealTimeInfo->save();
                }

                $gameType ='hltvcsgo';
                if (!empty($rel_match_id_res)) {
                    //重刷socket结束刷一次hot_refresh
                    $params       = [
                        'rel_match_id' => $rel_match_id,
                        'match_id'     => $match_id,
                        'del_redis_keys' => 'hltv_ws:' . $match_id . ':',
                        'origin_id'    => $origin_id,
                        'event_type'   => $type,
                        'operate_type' => $operate_type,
                    ];
                    //删除完成 添加一条任务
                    $taskRes = OperationRefreshHotInfo::refreshMatchInfoBySocket($origin_id,$params, $gameType);
                    return $taskRes;
                }
                return '没有绑定信息';

            } else {
                throw new BusinessException([], "已经有一个正在重刷中的程序,请等待");
//                return ['status' => 'success', 'match_id' => $match_id, 'message' => '已经有一个正在重刷中的程序,请等待'];
            }
        }else{
            return false;
        }

    }
    //重刷socket
    public static function refreshMatchInfoBySocketBtn($refresh_type,$operate_type,$match_id,$origin_id,$gameType,$refresh_sub_type=null){
        $type='all';
        //查询当前比赛的所有socket数据
        $rel_match_id_res = HotBase::getMetadataByMasterId('match',$match_id,$origin_id);
        if ($rel_match_id_res){
            $rel_match_id = @$rel_match_id_res['rel_identity_id'];
            //防止用户连点 重刷
            if ($origin_id == 2 || $origin_id == 3 || $origin_id == 4 || $origin_id == 9 || $origin_id == 10){
                $taskInfo   = TaskInfo::find()->where(['tag' => 'refresh_match_socket.....refresh'])->andWhere(['redis_server' => 'order_lol_'.$rel_match_id])->andWhere(['>=', 'created_time', date("Y-m-d H:i:s", strtotime("-5 minute"))])->asArray()->one();
                if ($taskInfo) {
                    throw new BusinessException([], "已经有一个正在重刷中的程序,请等待");
//                    return ['status' => 'success', 'match_id' => $match_id, 'message' => '已经有一个正在重刷中的程序,请等待'];
                }
            }


//          重抓：
//              Socket：
//                  比赛相关数据先初始化，Battle删除，然后正常录入
//              Rest：
//                  比赛相关数据不初始化，Battle删除，然后正常录入
//          修复：
//              Socket：
//                  比赛相关数据不初始化，Battle删除，比赛跑完统一Set覆盖
//              Rest：
//                  比赛相关数据不初始化，Battle删除，比赛跑完统一Set覆盖
            //做判断  先做玩家的lol
            if ($origin_id == 4 && $gameType == 'lol' || $origin_id == 3 && $gameType == 'lol' || $origin_id == 9 && $gameType == 'lol' || $origin_id == 10 && $gameType == 'lol'){
                if ($operate_type != 'xiufu' && $refresh_type == 'socket'){
                    $update_data = [
                        'status' => 2,
                        'end_at' => null,
                        'team_1_score' => 0,
                        'team_2_score' => 0,
                        'winner' => null
                    ];
                    //更新 match_real_time_info
                    $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$match_id])->one();
                    if ($MatchRealTimeInfo){
                        $MatchRealTimeInfo->setAttributes($update_data);
                        $MatchRealTimeInfo->save();
                    }
                }
            }else{
                $update_data = [
                    'team_1_score' => 0,
                    'team_2_score' => 0,
                    'winner' => null
                ];
                if ($operate_type != 'xiufu'){
                    $update_data['status'] = 2;
                    $update_data['end_at'] = null;
                }
                //更新 match_real_time_info
                $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$match_id])->one();
                if ($MatchRealTimeInfo){
                    $MatchRealTimeInfo->setAttributes($update_data);
                    $MatchRealTimeInfo->save();
                }
            }
            //获取match的所有battle
            $battleList = MatchBattle::find()->select('id')->where(['match'=>$match_id])->asArray()->all();
            if (count($battleList)){
                foreach ($battleList as $key =>$item){
                    $battleId = $item['id'];
                    BattleService::deleteBattle($gameType, $battleId);
                }
            }
            MatchBattleRelation::deleteAll(['match_id' =>$match_id]);
            //redis初始化
            $Redis = \Yii::$app->redis;
            $Redis->select(env('REDIS_DATABASE'));
            //新的结构删除
            $match_all_info_list = $Redis->keys('match_all_info:'.$match_id."*");
            if ($match_all_info_list) {
                foreach ($match_all_info_list as $item_mai) {
                    $Redis->del($item_mai);
                }
            }
            //删除关联关系
            $redis_match_lol_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":lol:".$origin_id;
            $Redis->hdel($redis_match_lol_relation,$rel_match_id);
            //比赛层的
            $redis_name_match = 'match_battle_info:' . $match_id;
            $Redis->del($redis_name_match);
            //玩家的item time line
            $Redis->del("match_lol:players:items_timeline:".$match_id);
            //api的
            $redis_name = 'match_battle_event_info_api:' . $match_id;
            $redis_names_list = $Redis->keys($redis_name . "*");
            if ($redis_names_list) {
                foreach ($redis_names_list as $key => $item) {
                    $Redis->del($item);
                }
            }
            //事件的记录redis
            $redis_name_match_battle_events_api = Consts::MARCH_BATTLE_EVENTS . '_api:' . $match_id;
            $redis_name_match_battle_events_api_list = $Redis->keys($redis_name_match_battle_events_api . "*");
            if ($redis_name_match_battle_events_api_list) {
                foreach ($redis_name_match_battle_events_api_list as $key => $item) {
                    $Redis->del($item);
                }
            }
            $redis_name_match_battle_events_ws = Consts::MARCH_BATTLE_EVENTS . '_ws:' . $match_id;
            $redis_name_match_battle_events_ws_list = $Redis->keys($redis_name_match_battle_events_ws . "*");
            if ($redis_name_match_battle_events_ws_list) {
                foreach ($redis_name_match_battle_events_ws_list as $key => $item) {
                    $Redis->del($item);
                }
            }
            //WS的
            $redis_name_ws = 'match_battle_event_info:' . $match_id;
            $redis_names_list_ws = $Redis->keys($redis_name_ws . "*");
            if ($redis_names_list_ws) {
                foreach ($redis_names_list_ws as $key => $item) {
                    $Redis->del($item);
                }
            }
            //删除比赛对局的玩家道具时间线
            $redisNameMatchLolPlayersItems = "match_lol:players:items_timeline:" . $match_id;
            $redis_names_list_playersItems = $Redis->keys($redisNameMatchLolPlayersItems . "*");
            if ($redis_names_list_playersItems) {
                foreach ($redis_names_list_playersItems as $key => $item) {
                    $Redis->del($item);
                }
            }

            if($gameType == 'csgo' && $origin_id==3){
                //组合数据
                $params = [
                    'rel_match_id' => $rel_match_id,
                    'match_id' => $rel_match_id,
                    'origin_id' => $origin_id,
                    'event_type' => $type,
                    'refresh_sub_type' => $refresh_sub_type,
                    'operate_type' => $operate_type,
                    'db_match_id' => $match_id,

                ];
            }else {
                //组合数据
                $params = [
                    'rel_match_id' => $rel_match_id,
                    'match_id' => $match_id,
                    'origin_id' => $origin_id,
                    'event_type' => $type,
                    'refresh_sub_type' => $refresh_sub_type,
                    'operate_type' => $operate_type
                ];
            }

            //删除完成 添加一条任务
            $result = OperationRefreshHotInfo::refreshMatchInfoBySocket($origin_id,$params,$gameType);
        }else{
            $result = '没有绑定信息';
        }
        return $result;
    }
    //获取当前比赛的所有socket数据
    public static function getMatchLivedDatas($origin_id,$rel_match_id,$type='all'){
        $map = [
            'origin_id' => $origin_id,
            'match_id' => $rel_match_id,
//            'game_id' => $game_id,
//            'type' => 'events',
        ];
        if ($type != 'all'){
            $map['type'] = $type;
        }
        $data = MatchLived::find()
            ->where($map)
//            ->andwhere(['<','id','797771'])
//            ->andwhere(['>=','id','797645'])
//            ->andwhere(['=','id','797950'])
            ->select('id,match_data,type,round,match_id,game_id,origin_id,socket_time,created_at')
            ->orderBy('id asc')
            ->asArray()
            ->all();
        return $data;
    }

    /**
     * @param $time
     * @return false|string
     * 转换时间格式
     */
    public static function changeTime($time)
    {
        $time = substr($time, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }
    //获取比赛的默认地图
    public static function getMapLists($gameId,$response_type='map_id',$request_type='normal',$number_of_games = 0){
        //默认领先和单图弃权的默认图
        if ($request_type == 'special'){
            $where = ['external_name'=> 'default'];
        }else{
            $where = ['is_default'=>1];
        }
        //设置默认地图
        $map_id = 1;
        switch ($gameId) {
            case 1:
                $maps_list = MetadataCsgoMap::find()->where($where)->asArray()->one();
                if ($maps_list){
                    $map_id = @$maps_list['id'];
                }
                break;
            case 2:
                $maps_list = MetadataLolMap::find()->where($where)->asArray()->one();
                if ($maps_list){
                    $map_id = @$maps_list['id'];
                }
                break;
            case 3:
                $maps_list = MetadataDota2Map::find()->where($where)->asArray()->one();
                if ($maps_list){
                    $map_id = @$maps_list['id'];
                }
                break;
            default:
                $map_id = "common_game_no_map";
                break;
        }
        if($map_id == "common_game_no_map"){
            return null;
        }
        //看看是要什么形式的返回
        if ($response_type == 'map_id'){
            return $map_id;
        }else{
            if ($number_of_games > 1){
                //根据 对局数生成对应的地图数量
                $maps_array = [];
                for ($i=0;$i<$number_of_games;$i++){
                    $maps_array[] = ['map'=>$map_id];
                }
                return json_encode($maps_array,320);
            }else{
                $maps_array = [['map'=>$map_id]];
                return json_encode($maps_array,320);
            }
        }
    }
    public static function editDataSpeed($matchId,$dataLevel,$dataSpeed)
    {
        if (isset($matchId)) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            // 更新操作
            $realTimeInfo = MatchRealTimeInfo::find()->where(["id" => $matchId])->one();
            $logOldMatchBase = $realTimeInfo ->toArray();
            $realTimeInfo->setAttributes(['data_level'=>$dataLevel,
                                          'data_speed'=>$dataSpeed]);
            if (!$realTimeInfo->save()) {
                throw new BusinessException($realTimeInfo->getErrors(), "操作失败,请检查提交或联系管理员");
            }
            $logNewMatchBase = $realTimeInfo->toArray();
            $filter = array_keys($logNewMatchBase);
            $filter = array_diff($filter, ["created_time", "modified_time"]);
            $diffInfo = Common::getDiffInfo($logOldMatchBase, $logNewMatchBase, $filter);
            if ($diffInfo['changed'] && isset($diffInfo['diff']['data_level'])) {
                OperationLogService::addLog($operationType,
                    Consts::RESOURCE_TYPE_MATCH,
                    $matchId,
                    ["diff" => $diffInfo["diff"], "new" => $logNewMatchBase],
                    0,
                    Consts::TAG_TYPE_REAL_TIME,
                    null,
                    0,
                    Consts::USER_TYPE_ADMIN,
                    OperationLogService::getLoginUserId()
                );
            }


        } else {
            throw new BusinessException([], "操作失败,缺少matchId");
        }
    }

//    /**
//     * 获取日志分拣比赛详情
//     * $sortingLogId        sorting_log表主键ID
//     */
//    public static function getLogMatchInfo($sortingLogId)
//    {
//        $logData = SortingLog::find()->where(['id' => $sortingLogId])->select('match_id,log_type')->one();
//        $matchId = $logData['match_id'];//比赛ID
//        if(!$matchId){
//            return [];
//        }
//        $matchData = Match::find()->where(['id' => $matchId])->select('scheduled_begin_at')->one();
//        $scheduledBeginAt = $matchData['scheduled_begin_at'];//计划开始时间
//        if(!$scheduledBeginAt){
//            $beginTime = "";
//            $endTime = "";
//            return [];
//        }else{
//            $beginTime = date("Y-m-d H:i:s",strtotime("-30 minute",strtotime($scheduledBeginAt)));
//            $endTime = date("Y-m-d H:i:s",strtotime("+1 hour",strtotime($scheduledBeginAt)));//查询结束时间节点
//        }
//        switch($logData['log_type']){
//            // 5eplay数据源
//            case 1:
//                $map = ['or',
//                    ['=','event_type',Csgo5e::EVENT_TYPE_TEAM_PLAYING_CT],
//                    ['=','event_type',Csgo5e::EVENT_TYPE_TEAM_PLAYING_T],
//                    ['=','event_type',Csgo5e::TYPE_LEFT_BUY_ZONE_WITH],
//                ];
//            break;
//            // funspark数据源
//            case 2:
//                $map = ['or',
//                    ['=','event_type',CsgoFunspark::EVENT_TYPE_TEAM_PLAYING_CT],
//                    ['=','event_type',CsgoFunspark::EVENT_TYPE_TEAM_PLAYING_T],
//                    ['=','event_type',CsgoFunspark::TYPE_LEFT_BUY_ZONE_WITH],
//                ];
//            break;
//            default:
//            break;
//        }
//        $timeMap = ['and',
//            ['>=','created_at',$beginTime],
//            ['<=','created_at',$endTime],
//        ];
//        $ipData = ReceiveData5eplayFormatEvents::find()->where($map)->andWhere($timeMap)->select('ip_address')->asArray()->all();
//        $ipAddress = [];
//        $steamHelper = new SteamHelper();
//        foreach($ipData as $key => $value){
//            $ipAddress[] = [
//                'ip_address' => $value['ip_address'],
//            ];
//        }
//        $ipAddress = array_unique($ipAddress,SORT_REGULAR);
//        $ipAddress = array_values($ipAddress);
//        foreach($ipAddress as $ipTmpKey => $ipTmpValue){
//            $createdAt = ReceiveData5eplayFormatEvents::find()->where($map)->andWhere($timeMap)->andWhere(['ip_address' => $ipTmpValue])->select('created_at')->one();
//            $info['created_at'][] = [
//                'created_at' => $createdAt['created_at'],
//            ];
//        }
//        $teamName = [];
//        $players = [];
//        foreach($ipAddress as $ipKey => $ipValue){
//            $ipMap = ['ip_address' => $ipValue['ip_address']];
//            $eventData = ReceiveData5eplayFormatEvents::find()->where($map)->andWhere($timeMap)->andWhere($ipMap)->select('info')->asArray()->all();
//            foreach($eventData as $eventKey => $eventValue){
//                $eventInfo = json_decode($eventValue['info'],true);
//                if(isset($eventInfo['log_team_name']) && !empty($eventInfo['log_team_name'])){
//                    $teamName[] = [
//                        'team_name' => $eventInfo['log_team_name'],
//                    ];
//                }
//                if(isset($eventInfo['playerName']) && !empty($eventInfo['playerName'])){
//                    $steamIdChange = $steamHelper->playerConversionSteamId($eventInfo['playID']);
//                    $players[] = [
//                        'player_nickname' => $eventInfo['playerName'],
//                        'steam_id' => $eventInfo['playID'],
//                        'steamID64' => $steamIdChange,
//                    ];
//                    foreach($players as $playerKey => $playerValue){
//                        if($playerValue['steam_id'] == "BOT"){
//                            unset($players[$playerKey]);
//                        }
//                    }
//                }
//            }
//            $teamName = array_unique($teamName,SORT_REGULAR);
//            $teamName = array_values($teamName);
//            $players = array_unique($players,SORT_REGULAR);
//            $players = array_values($players);
//            $info[] = [
//                'ip_address' => $ipValue['ip_address'],
//                'team_name' => $teamName,
//                'players' => $players,
//            ];
//        }
//        $info['match_id'] = $matchId;
//        $info['begin_time'] = $beginTime;
//        $info['end_time'] = $endTime;
//        return $info;
//    }

    public static function getStreamUrlInfo($matchId) {
        $q = MatchStreamList::find()->alias('m');
        $q->leftJoin('enum_stream_platform as p', 'p.id = m.platform_id');
        $q->select(["count(m.platform_id) as countNum","m.platform_name","m.platform_id","p.image"]);
        $q->where(['m.match_id' => $matchId, 'm.flag' => 1]);
        $q->groupBy(['m.platform_id']);
        $data =$q->asArray()->all();
        if (!empty($data)) {
            return $data;
        }
        return [];
    }

    public static function  matchLog($matchId,$event){

        $matchCore = Match::find()->alias("m")
            ->select(["m.*","t.name as t_name","r.name as r_name",
                'tt.id as tt_tournament_id',
                'tt.son_match_sort as tt_son_match_sort',
                'tt.name as tt_name',
                'tt.name_cn as tt_name_cn',
                'tt_status.c_name as tt_status',
                'tt.scheduled_begin_at as tt_scheduled_begin_at',
                'tt.scheduled_end_at as tt_scheduled_end_at',
                'tt.short_name as tt_short_name',
                'tt.short_name_cn as tt_short_name_cn',
                'tt.slug as tt_slug',
                'tt.image as tt_image',
                'tt.modified_at as tt_modified_at',
                'tt.created_at as tt_created_at',
                'tt.deleted_at as tt_deleted_at',

            ])
            ->leftJoin('enum_game_match_type as t',"t.id=m.match_type")
            ->leftJoin('enum_game_rules as r',"r.id = m.game_rules")
            ->leftjoin('tournament as tt','tt.id = m.tournament_id')
            ->leftJoin('enum_match_state as tt_status','tt_status.id = tt.status')
            ->where(["m.id" => $matchId])->asArray()->one();
        if (empty($matchCore)) {
            $mssage = "查不到比赛Id:{$matchId}-{$event}";
            $url = env('PUSH_MESSAGE_DING');
            $params = [
                'type' =>'matchBack_shuai',
                'tag' =>'matchBack',
                'info' =>$mssage,
            ];
            Common::requestPost($url,[],$params);
            return false;
        }

        //一周前不推送
        if ($matchCore['scheduled_begin_at']) {
            $aWeekAgo = date("Y-m-d H:i:s",strtotime( "-1 week"));
            if ($matchCore['scheduled_begin_at'] < $aWeekAgo) {
                return false;
            }
        }

        $getCompeTingTeams = self::getCompeTingTeams($matchCore['team_1_id'],$matchCore['team_2_id'],$matchCore['tournament_id'],$matchCore['group_id']);
        $realData = MatchRealTimeInfo::find()->alias('t')
            ->select(['t.*',"s.c_name"])
            ->leftJoin('enum_match_state as s',"s.id = t.status")
            ->where(['t.id' => $matchId])->asArray()->one();

        $getScores = self::getScores($realData['team_1_score'],$realData['team_2_score'],$matchCore['team_1_id'],$matchCore['team_2_id']);
        $game = self::getGame($matchCore['game']);
        $dataInfo['event_type'] = $event;
        $dataInfo['match_id'] = (int)$matchId;
        if (!empty($game['game_id'])){
            $dataInfo['game']['game_id'] = $game['game_id'];
            $dataInfo['game']['name'] = $game['name'];
            $dataInfo['game']['name_cn'] = $game['name'];
            $dataInfo['game']['default_game_rules'] = $game['default_game_rules'];

            $dataInfo['game']['default_match_type'] = $game['default_match_type'];
            $dataInfo['game']['full_name'] = $game['full_name'];
            $dataInfo['game']['short_name'] = $game['short_name'];
            $dataInfo['game']['slug'] = $game['slug'];
            $dataInfo['game']['image'] = $game['image'];

        }else{
            $dataInfo['game'] = null;
        }

        $dataInfo['game_rules'] = $matchCore['r_name'];
        $dataInfo['match_type'] = $matchCore['t_name'];
        $dataInfo['number_of_games'] = $matchCore['number_of_games'];
        if ($matchCore['tt_tournament_id']) {
            $dataInfo['tournament']['tournament_id'] = $matchCore['tt_tournament_id'];
            $dataInfo['tournament']['name'] = $matchCore['tt_name'];
            $dataInfo['tournament']['name_cn'] = $matchCore['tt_name_cn'];
            if (!empty($game['game_id'])){
                $game['modified_at'] =  ConversionCriteria::DataTimeConversionCriteria($game['modified_at']);
                $game['created_at'] =  ConversionCriteria::DataTimeConversionCriteria($game['created_at']);
                $game['deleted_at'] =  ConversionCriteria::DataTimeConversionCriteria($game['deleted_at']);
                $dataInfo['tournament']['game'] = $game;

            }else{
                $dataInfo['tournament']['game'] = null;
            };

            $dataInfo['tournament']['status'] = $matchCore['tt_status'];
            $dataInfo['tournament']['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['tt_scheduled_begin_at']);
            $dataInfo['tournament']['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['tt_scheduled_end_at']);
            $dataInfo['tournament']['short_name'] = $matchCore['tt_short_name'];
            $dataInfo['tournament']['short_name_cn'] = $matchCore['tt_short_name_cn'];
            $dataInfo['tournament']['slug'] = $matchCore['tt_slug'];
            $dataInfo['tournament']['image'] = $matchCore['tt_image'];
            $dataInfo['tournament']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['tt_modified_at']);
            $dataInfo['tournament']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['tt_created_at']);
            $dataInfo['tournament']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['tt_deleted_at']);
        }else{
            $dataInfo['tournament'] = null;
        }

        $dataInfo['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['scheduled_begin_at']);
        $dataInfo['original_scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['original_scheduled_begin_at']);
        $dataInfo['is_rescheduled'] = ConversionCriteria::issetTrueFalseType($matchCore['is_rescheduled']);
        $dataInfo['teams'] = $getCompeTingTeams;
        $dataInfo['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($realData['begin_at']);
        $dataInfo['end_at'] = ConversionCriteria::DataTimeConversionCriteria($realData['end_at']);
        $dataInfo['status'] = $realData['c_name'];
        $dataInfo['is_draw'] = ConversionCriteria::issetTrueFalseType($realData['is_draw']);
        $dataInfo['is_forfeit'] = ConversionCriteria::issetTrueFalseType($realData['is_forfeit']);

//        $dataInfo['default_advantage']['team_id'] = $realData['is_forfeit'];
        if ($realData['default_advantage'] == '1'){
            $dataInfo['default_advantage']['team_id'] = $matchCore['team_1_id'];
            $dataInfo['default_advantage']['opponent_order'] = 1;
        }elseif ($realData['default_advantage'] == '2'){
            $dataInfo['default_advantage']['team_id'] = $matchCore['team_2_id'];
            $dataInfo['default_advantage']['opponent_order'] = 2;
        }else{
            $dataInfo['default_advantage'] = null;

        }
        $dataInfo['scores'] = $getScores;
        if ($realData['winner'] == '1'){
            $dataInfo['winner']['team_id'] = $matchCore['team_1_id'];
            $dataInfo['winner']['opponent_order'] = 1;
        }elseif ($realData['winner'] == '2'){
            $dataInfo['winner']['team_id'] = $matchCore['team_2_id'];
            $dataInfo['winner']['opponent_order'] = 2;
        }else{
            $dataInfo['winner'] = null;
        }
        $dataInfo['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($realData['is_battle_detailed']);
        $getBattle = self::getBattle($matchId,$matchCore['game'],$matchCore['tournament_id'],$matchCore['group_id']);
        $dataInfo['battles'] = $getBattle;
        $getPbpData = self::getPbpdata($realData['is_pbpdata_supported'],$matchCore['scheduled_begin_at'],$realData['data_level'],$matchId);
        $dataInfo['pbpdata'] = $getPbpData['pbpdata'];
        $dataInfo['streams']['is_supported'] = ConversionCriteria::issetTrueFalseType($realData['is_streams_supported']);
        $dataInfo['streams']['streams'] =self::getStreamsData($matchCore['id']);
        $dataInfo['team_version'] = $matchCore['team_version'] ?: null;
        $dataInfo['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['modified_at']);
        $dataInfo['created_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['created_at']);
        $dataInfo['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($matchCore['deleted_at']);
        $values = ApiBase::convertNull($dataInfo);
        return $values;
    }

    private static function getStreamsData($matchId)
    {
        $field = [
            'stream.id','stream.streamer','stream.platform_id','stream.title','stream.is_official','stream.viewers','stream.preview','stream.live_url','stream.live_embed_url',
            'country.id as country_id','country.e_name as name','country.name as name_cn','country.two as short_name','country.image as image',
            'platform.name as platform_name','platform.image as platform_image'
        ];
        $streamData = MatchStreamList::find()->alias('stream')->select($field)
            ->leftjoin('enum_country as country','country.id = stream.country')
            ->leftjoin('enum_stream_platform as platform','platform.id = stream.platform_id')
            ->where(['stream.match_id'=>$matchId,'stream.flag'=>1])->asArray()->all();
        $streams = [];
        foreach ($streamData as $k=>$v){
            $stream['stream_id'] = $v['id'];
            $stream['streamer'] = $v['streamer'];
            $stream['platform']['platform_id'] = $v['platform_id'];
            $stream['platform']['name'] = $v['platform_name'];
            $stream['platform']['image'] = $v['platform_image'];
            $stream['title'] = $v['title'];
            $stream['is_official'] = ConversionCriteria::issetTrueFalseType($v['is_official']);
            if($v['country_id']) {
                $stream['country']['id'] = $v['country_id'];
                $stream['country']['name'] = $v['name'];
                $stream['country']['name_cn'] = $v['name_cn'];
                $stream['country']['short_name'] = $v['short_name'];
                $stream['country']['image'] = $v['image'];
            }else{
                $stream['country'] = null;
            }
            $stream['viewers'] = $v['viewers'];
            $stream['preview'] = $v['preview'];
            $stream['url'] = $v['live_url'];
            $stream['embed_url'] = $v['live_embed_url'];
            $streams[] = $stream;
        }
        return $streams;
    }

    public static function getPbpdata($is_pbpdata_supported,$match_scheduled_begin_at,$data_level,$match_id){
        $PBP_DATA_URL = "ws://39.105.105.57:9997";
        $is_supported = ConversionCriteria::issetTrueFalseType($is_pbpdata_supported);
        if($is_supported) {
            if($match_scheduled_begin_at) {
                $match_scheduled_begin_at = date('Y-m-d H:i:s', strtotime( '-15 Minute', strtotime($match_scheduled_begin_at)));
                $opens_at = ConversionCriteria::DataTimeConversionCriteria($match_scheduled_begin_at);
            }else{
                $opens_at = null;
            }
            $tmp['pbpdata'] = [
                'is_supported' => $is_supported,
                'quality' => $data_level, // 实时数据质量
                'opens_at' => $opens_at,
                'endpoints' => [
                    [
                        'pbpdata_type' => 'frames',
                        'url' => $PBP_DATA_URL."/pbpdata/{$match_id}/frames",
                    ],
                    [
                        'pbpdata_type' => 'events',
                        'url' => $PBP_DATA_URL."/pbpdata/{$match_id}/events",
                    ],
                ],
            ];
        }else{
            $tmp['pbpdata'] = [
                'is_supported' => $is_supported,
                'quality' => $data_level,
                'opens_at' => null,
                'endpoints' => [],
            ];
        }

        return $tmp;

    }
    public static function getBattle($matchId,$gameId,$tournamentId,$groupId,$type = 'match',$battleId = null){
        if ($type == 'battle') {
            $where = ['battle.id' => $battleId];
        }else{
            $where = ['match' => $matchId,'deleted'=>2];
        }
        $battles = MatchBattle::find()->alias('battle')->where($where)
            ->leftJoin('enum_match_state as battle_status', 'battle_status.id = battle.status')
            ->select( [
                'battle.id as battle_id',
                'battle.game as game',
                'battle.order as order',
                'battle.order as teams',
                'battle.begin_at as begin_at',
                'battle.end_at as end_at',
                'battle_status.c_name as status',
//            'battle.status as status',
                'battle.is_draw as is_draw',
                'battle.is_forfeit as is_forfeit',
                'battle.is_default_advantage as is_default_advantage',
                'battle.duration as duration',
                'battle.winner as scores',
                'battle.winner as winner',
                'battle.is_battle_detailed as is_battle_detailed',
                'battle.modified_at as modified_at',
                'battle.created_at as created_at',
                'battle.deleted_at as deleted_at',
            ])->asArray()->all();
        $matchBattles = [];

        foreach ($battles as $k=>$battle){
            $game = self::getGame($gameId);
            unset($game['modified_at']);
            unset($game['created_at']);
            unset($game['deleted_at']);
            $getBattleTeamsData = self::getBattleTeamsData($battle['battle_id'],$tournamentId,$groupId);
            $getBattleScoresData = self::getBattleScoresData($battle['battle_id']);

            $battleCre = $battle;
            $battleCre['game'] = $game;
            $battleCre['teams'] = $getBattleTeamsData;
            $battleCre['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['begin_at']);
            $battleCre['end_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['end_at']);
            // battle状态
            $battleCre['status'] = $battle['status'];
            $battleCre['is_draw'] = ConversionCriteria::issetTrueFalseType($battle['is_draw']);
            // 是否弃权
            $battleCre['is_forfeit'] = ConversionCriteria::issetTrueFalseType($battle['is_forfeit']);
            $battleCre['is_default_advantage'] = ConversionCriteria::issetTrueFalseType($battle['is_default_advantage']);
            $battleCre['scores'] = $getBattleScoresData;
            $battleCre['winner'] = self::getBattleWinnerData($battle['battle_id'],$battle['winner']);
            $battleCre['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($battle['is_battle_detailed']);
            $battleCre['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['modified_at']);
            $battleCre['created_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['created_at']);
            $battleCre['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['deleted_at']);
            $matchBattles[] = $battleCre;
        }

        return $matchBattles;
    }

    // battle下获胜战队
    private static function getBattleWinnerData($battleId,$winner)
    {
        $battleWinner = MatchBattleTeam::find()->select(['team_id','order'])->where(['battle_id'=>$battleId,'order'=>$winner])->asArray()->one();
        if(empty($battleWinner)) return null;

        $team['team_id'] = $battleWinner['team_id'];
        $team['opponent_order'] = $battleWinner['order'];
        return $team;
    }

    // battle下比分
    private static function getBattleScoresData($battleId)
    {
        $battleScores = MatchBattleTeam::find()->select(['team_id','order','score'])->where(['battle_id'=>$battleId])->asArray()->all();
        $teams = [];
        foreach ($battleScores as $k=>$val) {
            $teams[$k]['team_id'] = $val['team_id'];
//            $teams[$k]['team_snapshot'] = self::getTeamSnapshotData($val['team_id'],$tournamentId,$groupId);
            $teams[$k]['opponent_order'] = $val['order'];
            $teams[$k]['score'] = $val['score'];
        }
        return $teams;
    }

    public static function getBattleTeamsData($battleId,$tournamentId,$groupId){
        $battleTeam = MatchBattleTeam::find()->select(['team_id','order'])->where(['battle_id'=>$battleId])->asArray()->all();
        $teams = [];
        foreach ($battleTeam as $k=>$v){
            if (!is_numeric($v['team_id'])){
                continue;
            }
            $team['team_id'] = $v['team_id'];
            $team['team_snapshot'] = self::getTeamSnapshot($v['team_id'],$tournamentId,$groupId);
            $team['opponent_order'] = $v['order'];
            $teams[] = $team;
        }
        return $teams;
    }

    public static function getScores($score1,$score2,$team1,$team2){
        $data = [];
        if ($team1){
            $score['team_id'] = $team1;
            $score['opponent_order'] = 1;
            $score['score'] = $score1;
            $data[] = $score;
        }
        if ($team2) {
            $scores['team_id'] = $team2;
            $scores['opponent_order'] = 2;
            $scores['score'] = $score2;
            $data[] = $scores;
        }
        return $data;

    }

    public static function getCompeTingTeams($teamid1,$teamid2,$tournamentId,$groupId){
        $data = [];
        if (is_numeric($teamid1)){
            $team['team_id'] = $teamid1;
            $team['team_snapshot'] = self::getTeamSnapshot($teamid1,$tournamentId,$groupId);
            $team['opponent_order'] = 1;
            $data[] =$team;
        }
        if (is_numeric($teamid2)){
            $team2['team_id'] = $teamid2;
            $team2['team_snapshot'] = self::getTeamSnapshot($teamid2,$tournamentId,$groupId);
            $team2['opponent_order'] = 2;
            $data[] =$team2;
        }
        return $data;

    }

    public static function getTeamSnapshot($teamId, $tournamentId, $groupId){

        if (!$teamId) {
            return null;
        }
        if ($groupId) {
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $groupId, 'team.type' => 'group'];
        } else if (empty($groupId) && $tournamentId) {
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $tournamentId, 'team.type' => 'tournament'];
        } else {
            return null;
        }

        $snapData = TeamSnapshot::find()->alias('team')->select([
            'team.name as name',
            'country.id as country_id',
            'country.e_name as country_name',
            'country.name as country_name_cn',
            'country.two as country_short_name',
            'country.image as country_image',
//            'team.country as country',
            'team.full_name as full_name',
            'team.short_name as short_name',
            'team.slug as slug',
            'team.image as image',
            'team_i.world_ranking as world_ranking',
        ])
            ->leftJoin('team_introduction_snapshot as team_i', 'team.id = team_i.team_id')
            ->leftJoin('enum_country as country', 'team.country = country.id')
            ->where($where)->asArray()->one();
        if (!empty($snapData)) {
            $snap['name'] = $snapData['name'];
            if ($snapData['country_id']) {
                $snap['country']['id'] = $snapData['country_id'];
                $snap['country']['name'] = $snapData['country_name'];
                $snap['country']['name_cn'] = $snapData['country_name_cn'];
                $snap['country']['short_name'] = $snapData['country_short_name'];
                $snap['country']['image'] = $snapData['country_image'];
            } else {
                $snap['country'] = null;
            }
            $snap['full_name'] = $snapData['full_name'];
            $snap['short_name'] = $snapData['short_name'];
            $snap['slug'] = $snapData['slug'];
            $snap['image'] = ImageConversionHelper::showFixedSizeConversion($snapData['image'], 200, 200);
            $snap['world_ranking'] = $snapData['world_ranking'];
        } else {
            return null;
        }

        return $snap;


    }

    public static function getUTCTime($times){
        if (empty($times)){
            return null;
        }
        $time = date('Y-m-d\TH:i:s\Z', strtotime($times)-3600*8);
        return $time;
    }

    public static function getGame($gameId){
        if (empty($gameId)) {
            return null;
        }
        $game = EnumGame::find()->alias("game")
//            ->select(["g.*","t.name as t_name","r.name as r_name"])
            ->select(['game.id as game_id',
                'game.e_name as name',
                'game.name as name_cn',
                'r.name as default_game_rules',
                't.name as default_match_type',
                'game.full_name as full_name',
                'game.short_name as short_name',
                'game.slug as slug',
                'game.image as image',
                'game.image as image',
                'game.modified_at as modified_at',
                'game.created_at as created_at',
                'game.deleted_at as deleted_at',

            ])
            ->leftJoin('enum_game_match_type as t',"t.id=game.default_match_type")
            ->leftJoin('enum_game_rules as r',"r.id = game.default_game_rule")
            ->where(['game.id'=>$gameId])->asArray()->one();
        return $game ?: null;
    }

    /**
     *最近一周的比赛数量
     * @return array
     */
    public static function getMatchCountByTime()
    {
        $gameListData = EnumGame::find()->select(['id','e_short'])->asArray()->all();
        $queryTime = date('Y-m-d',strtotime('-3day'));
        $countTime = $queryTime;
        $matchCount = [];
        for ($i =0;$i<7;$i++){
            $matchCount ['count_time'][] = $countTime;
            $countTime = date('Y-m-d', strtotime($countTime.'+1 day'));
        }
        foreach ($gameListData as $key=>$value){
            $sumCount = [];
            for($i=0;$i<7;$i++){
                $matchDay = Match::find()->select(['count(*) as sum'])
                    ->where(['and',
                        ['>=','scheduled_begin_at',$queryTime],
                        ['<','scheduled_begin_at',date('Y-m-d', strtotime($queryTime.'+1 day'))],
                        ['=','game',$value['id']],
                        ['=','deleted',2]
                    ])->asArray()->one();
                if($matchDay['sum'] == 0){
                    $sumCount[] ="";
                }else{
                    $sumCount[] = $matchDay['sum'];
                }
                $queryTime = date('Y-m-d', strtotime($queryTime.'+1 day'));
                if($i >= 6){
                    $j = 0;
                    foreach($sumCount as $sumVal){
                        if($sumVal == ""){
                            $j++;
                        }
                        if($j >= 7){
                            unset($sumCount);
                        }
                    }
                    $queryTime = date('Y-m-d',strtotime('-3day'));
                }
            }
            if(count($sumCount)>0){
                $matchCount['count'][][$value['e_short']] = $sumCount;
            }
        }
        return $matchCount;
    }
    public static function getMatchCountLately()
    {
        $matchCount = [];
        //昨天的统计
        $yesterdayTime = date('Y-m-d',strtotime('-1day'));
        $yesterdayTimeData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$yesterdayTime],
                ['<','scheduled_begin_at',date('Y-m-d', strtotime($yesterdayTime.'+1 day'))],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['yesterday'] = $yesterdayTimeData['sum'];
        //今天的统计
        $todayTime = date('Y-m-d');
        $todayTimeDate = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$todayTime],
                ['<','scheduled_begin_at',date('Y-m-d', strtotime($todayTime.'+1 day'))],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['today'] = $todayTimeDate['sum'];
        //明天比赛场数的统计
        $tomorrowTime = date('Y-m-d',strtotime('+1day'));
        $tomorrowTimeDate = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$tomorrowTime],
                ['<','scheduled_begin_at',date('Y-m-d', strtotime($tomorrowTime.'+1 day'))],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['tomorrow'] = $tomorrowTimeDate['sum'];
        //进行中的统计
        $matchOngoingDate = Match::find()->alias('m')->select(['count(*) as sum'])
            ->leftJoin('match_real_time_info as real','m.id = real.id')
            ->where(['and',
                ['=','real.status',2],
                ['=','m.deleted',2]
            ])->asArray()->one();
        $matchCount['ongoing'] = $matchOngoingDate['sum'];
        //上周
        $lastWeekStartTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-7,date("Y")));
        $lastWeekEndTime = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")-date("w")+7-7,date("Y")));
        $lastWeekDate = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$lastWeekStartTime],
                ['<=','scheduled_begin_at',$lastWeekEndTime],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['last_week'] = $lastWeekDate['sum'];
        //本周
        $thisWeekStartTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y")));
        $thisWeekEndTime = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y")));
        $thisWeekData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$thisWeekStartTime],
                ['<=','scheduled_begin_at',$thisWeekEndTime],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['this_week'] = $thisWeekData['sum'];
        //下周
        $nextWeekStartTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+8,date("Y")));
        $nextWeekEndTime = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")-date("w")+14,date("Y")));
        $nextWeekData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$nextWeekStartTime],
                ['<=','scheduled_begin_at',$nextWeekEndTime],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['next_week'] = $nextWeekData['sum'];
        //上月
        $lastMonthStartTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-1,1,date("Y")));
        $lastMonthEndTime = date("Y-m-d H:i:s",mktime(23,59,59,date("m") ,0,date("Y")));
        $lastMonthData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$lastMonthStartTime],
                ['<=','scheduled_begin_at',$lastMonthEndTime],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['last_month'] = $lastMonthData['sum'];
        //本月
        $thisMonthStartTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y")));
        $thisMonthEndTime = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y")));
        $thisMonthData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$thisMonthStartTime],
                ['<=','scheduled_begin_at',$thisMonthEndTime],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['this_month'] = $thisMonthData['sum'];
        //下月
        $lastDate = Common::getNextMonthDays(date("Y-m-d H:i:s"));
        $nextMonthStartTime = $lastDate[0];
        $nextMonthEndTime = $lastDate[1];
        $nextMonthData = Match::find()->select(['count(*) as sum'])
            ->where(['and',
                ['>=','scheduled_begin_at',$nextMonthStartTime],
                ['<','scheduled_begin_at',date('Y-m-d', strtotime($nextMonthEndTime.'+1 day'))],
                ['=','deleted',2]
            ])->asArray()->one();
        $matchCount['next_month'] = $nextMonthData['sum'];
        return $matchCount;
    }



    /**
     * 实时数据量
     */
    public static function getRealTimeData($type)
    {
        $index = EsService::initConnect()->issetEsIndex('stats-socket-in-out-cnt');
        if (empty($index)){
            return [];
        }
        $resData = EsService::initConnect()->getIndexList(
            'stats-socket-in-out-cnt',
            0, 60,[],
            ['field' => 'dt', 'sort' => 'desc']
        );
        if($type == "total") {
            $dataSource = [];
            if (!empty($resData['list'])) {
                $realTimeInfo = $resData['list'];
                $ctime_str = [];
                foreach ($realTimeInfo as $k => $data) {
                    $dateTime = date("H:i:s", strtotime($data['dt']) - 8 * 60 * 60);
                    $ctime_str[] = $dateTime;
                    $dataSource[$k]['dt'] = $dateTime;
                    $dataSource[$k]['ori'] = $data['ori'];
                    $dataSource[$k]['socket'] = $data['socket'];
                }
                array_multisort($ctime_str, SORT_ASC, $dataSource);
            }
            return $dataSource;
        }else {
            $dataSource = [
                'bayesplus' => [
                    [
                        'game_name' => 'csgo',
                        'ori_csgo' => [],
                        'src_csgo' => [],
                    ],
                    [
                        'game_name' => 'lol',
                        'ori_lol' => [],
                        'src_lol' => [],
                    ],
                    [
                        'game_name' => 'dota2',
                        'ori_dota2' => [],
                        'src_dota2' => [],
                    ],
                ],
                'hltv' => [
                    [
                        'game_name' => 'csgo',
                        'ori_csgo' => [],
                        'src_csgo' => [],
                    ],
                    [
                        'game_name' => 'lol',
                        'ori_lol' => [],
                        'src_lol' => [],
                    ],
                    [
                        'game_name' => 'dota2',
                        'ori_dota2' => [],
                        'src_dota2' => []
                    ],
                ],
                'pandascore' => [
                    [
                        'game_name' => 'csgo',
                        'ori_csgo' => [],
                        'src_csgo' => [],
                    ],
                    [
                        'game_name' => 'lol',
                        'ori_lol' => [],
                        'src_lol' => [],
                    ],
                    [
                        'game_name' => 'dota2',
                        'ori_dota2' => [],
                        'src_dota2' => []
                    ],
                ],
                'orange' => [
                    [
                        'game_name' => 'csgo',
                        'ori_csgo' => [],
                        'src_csgo' => [],
                    ],
                    [
                        'game_name' => 'lol',
                        'ori_lol' => [],
                        'src_lol' => [],
                    ],
                    [
                        'game_name' => 'dota2',
                        'ori_dota2' => [],
                        'src_dota2' => []
                    ],
                ],
                'date' => []
            ];
            $ctime_str = [];
            if (!empty($resData['list'])) {
                $realTimeInfo = $resData['list'];
                foreach ($realTimeInfo as $key => $data) {
                    $dateTime = date("H:i:s", strtotime($data['dt']) - 8 * 60 * 60);
                    $ctime_str[] = $dateTime;
                    $realTimeInfo[$key]['dt'] = $dateTime;
                }
                array_multisort($ctime_str, SORT_ASC, $realTimeInfo);
                foreach ($realTimeInfo as $key => $data) {
                    $dataSource['date'][] = $data['dt'];
                    // 输入/输出 - bayesplus
                    if (isset($data['ori_bayes_csgo'])) {
                        $dataSource['bayesplus'][0]['ori_csgo'][] = $data['ori_bayes_csgo'];
                        $dataSource['bayesplus'][0]['src_csgo'][] = $data['socket'];
                    } else {
                        $dataSource['bayesplus'][0]['ori_csgo'][] = 0;
                        $dataSource['bayesplus'][0]['src_csgo'][] = $data['socket'];
                    }
                    if (isset($data['ori_bayes_lol'])) {
                        $dataSource['bayesplus'][1]['ori_lol'][] = $data['ori_bayes_lol'];
                        $dataSource['bayesplus'][1]['src_lol'][] = $data['socket'];
                    } else {
                        $dataSource['bayesplus'][1]['ori_lol'][] = 0;
                        $dataSource['bayesplus'][1]['src_lol'][] = $data['socket'];
                    }
                    if (isset($data['ori_bayes_dota2'])) {
                        $dataSource['bayesplus'][2]['ori_dota2'][] = $data['ori_bayes_dota2'];
                        $dataSource['bayesplus'][2]['src_dota2'][] = $data['socket'];
                    } else {
                        $dataSource['bayesplus'][2]['ori_dota2'][] = 0;
                        $dataSource['bayesplus'][2]['src_dota2'][] = $data['socket'];
                    }
                    // 输入/输出 - hltv
                    if (isset($data['ori_hltv_csgo'])) {
                        $dataSource['hltv'][0]['ori_csgo'][] = $data['ori_hltv_csgo'];
                        $dataSource['hltv'][0]['src_csgo'][] = $data['socket'];
                    } else {
                        $dataSource['hltv'][0]['ori_csgo'][] = 0;
                        $dataSource['hltv'][0]['src_csgo'][] = $data['socket'];
                    }
                    if (isset($data['ori_hltv_lol'])) {
                        $dataSource['hltv'][1]['ori_lol'][] = $data['ori_hltv_lol'];
                        $dataSource['hltv'][1]['src_lol'][] = $data['socket'];
                    } else {
                        $dataSource['hltv'][1]['ori_lol'][] = 0;
                        $dataSource['hltv'][1]['src_lol'][] = $data['socket'];
                    }
                    if (isset($data['ori_hltv_dota2'])) {
                        $dataSource['hltv'][2]['ori_dota2'][] = $data['ori_hltv_dota2'];
                        $dataSource['hltv'][2]['src_dota2'][] = $data['socket'];
                    } else {
                        $dataSource['hltv'][2]['ori_dota2'][] = 0;
                        $dataSource['hltv'][2]['src_dota2'][] = $data['socket'];
                    }
                    // 输入/输出 - pandascore
                    if (isset($data['ori_pandascore_csgo'])) {
                        $dataSource['pandascore'][0]['ori_csgo'][] = $data['ori_pandascore_csgo'];
                        $dataSource['pandascore'][0]['src_csgo'][] = $data['socket'];
                    } else {
                        $dataSource['pandascore'][0]['ori_csgo'][] = 0;
                        $dataSource['pandascore'][0]['src_csgo'][] = $data['socket'];
                    }
                    if (isset($data['ori_pandascore_lol'])) {
                        $dataSource['pandascore'][1]['ori_lol'][] = $data['ori_pandascore_lol'];
                        $dataSource['pandascore'][1]['src_lol'][] = $data['socket'];
                    } else {
                        $dataSource['pandascore'][1]['ori_lol'][] = 0;
                        $dataSource['pandascore'][1]['src_lol'][] = $data['socket'];
                    }
                    if (isset($data['ori_pandascore_dota2'])) {
                        $dataSource['pandascore'][2]['ori_dota2'][] = $data['ori_pandascore_dota2'];
                        $dataSource['pandascore'][2]['src_dota2'][] = $data['socket'];
                    } else {
                        $dataSource['pandascore'][2]['ori_dota2'][] = 0;
                        $dataSource['pandascore'][2]['src_dota2'][] = $data['socket'];
                    }
                    // 输入/输出 - orange
                    if (isset($data['ori_orange_csgo'])) {
                        $dataSource['orange'][0]['ori_csgo'][] = $data['ori_orange_csgo'];
                        $dataSource['orange'][0]['src_csgo'][] = $data['socket'];
                    } else {
                        $dataSource['orange'][0]['ori_csgo'][] = 0;
                        $dataSource['orange'][0]['src_csgo'][] = $data['socket'];
                    }
                    if (isset($data['ori_orange_lol'])) {
                        $dataSource['orange'][1]['ori_lol'][] = $data['ori_orange_lol'];
                        $dataSource['orange'][1]['src_lol'][] = $data['socket'];
                    } else {
                        $dataSource['orange'][1]['ori_lol'][] = 0;
                        $dataSource['orange'][1]['src_lol'][] = $data['socket'];
                    }
                    if (isset($data['ori_orange_dota2'])) {
                        $dataSource['orange'][2]['ori_dota2'][] = $data['ori_orange_dota2'];
                        $dataSource['orange'][2]['src_dota2'][] = $data['socket'];
                    } else {
                        $dataSource['orange'][2]['ori_dota2'][] = 0;
                        $dataSource['orange'][2]['src_dota2'][] = $data['socket'];
                    }
                }
            }

            return $dataSource;
        }
    }

    public static function getManyTeam() {
        $allData = MatchErrorManyTeam::find()->where(['data_status'=>1]);//->orderBy('scheduled_begin_at asc')->asArray()->all();

        $allData = $allData->orderBy('scheduled_begin_at asc');
        $totalCount = $allData->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
//        $sql = $match->createCommand()->getRawSql();
        $model = $allData->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $matchId = array_column($model,'match_id');
        $matchTeam = Match::find()->alias('m')
            ->select(['m.id as id','t.id as team_1_id','ts.id as team_2_id','t.name as team_1_name','ts.name as team_2_name','t.image as team_1_image','ts.image as team_2_image'])
            ->leftJoin('team as t','t.id = m.team_1_id')
            ->leftJoin('team as ts','ts.id = m.team_2_id')
            ->where(['m.id'=>$matchId])->asArray()->all();
        $matchTeamById = array_column($matchTeam,null,'id');
        $resourceIds = array_column($model, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_ERROR_MANY_TEAM, $resourceIds);
        if (!empty($model)) {
            foreach ($model as $key => &$val) {
                $val['team_master'] = $matchTeamById[$val['match_id']];
                $val['operationInfo'] = $operationInfo[$val['id']];
            }
            //
            return ['list' => $model, 'total' => $totalCount];
        }
        return ['list' => [], 'total' => 0];
    }

    public static function matchErrorTeamByEdit($prams,$userId) {
        $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
        $allData = MatchErrorManyTeam::find()->where(['id'=>$prams['id']])->one();
        $old_allData = $allData ->toArray();

        if (empty($allData)) {
            throw new BusinessException([], '没有这条数据');
        }
        $data['operation_status'] = (int)$prams['operation_status'];
        $allData->setAttributes($data);
        if (!$allData->save()) {
            throw new BusinessException($allData->getErrors(), '保存失败');
        }
        $new_allData = $allData->toArray();

        $filter = array_keys($new_allData);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($old_allData, $new_allData, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_ERROR_MANY_TEAM,
                $prams['id'],
                ["diff" => $diffInfo["diff"], "new" => $new_allData],
                0,
                Consts::RESOURCE_TYPE_ERROR_MANY_TEAM,
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                $userId
            );
        }

        return $allData->toArray();
    }

    public static function setTeamVersion($match_id,$team_1_id,$team_2_id) {
        $teamids = $team_1_id.','.$team_2_id;
        $team_md5 = md5($teamids);

        $dataMatch = MatchTeamVersionLog::find()->where(
            ['match_id'=>$match_id])->orderBy('team_version desc')->one();
        // 如果存在，并且一样，则不新增直接返回
        if($dataMatch && $dataMatch['team_md5']==$team_md5){
            return $dataMatch['team_version'];
        }
        // 如果不一样，或者不存在，则新增
        $vteam_version_num = $dataMatch?$dataMatch['team_version'] + 1:1;
        $newData = new MatchTeamVersionLog();
        $param['team_md5'] = $team_md5;
        $param['teamids'] = $teamids;
        $param['team_version'] =  (string)$vteam_version_num;
        $param['match_id'] = $match_id;
        $newData->setAttributes($param);
        if (!$newData->save()) {
            throw new BusinessException($newData->getErrors(), 'team_version日志记录失败');
        }
        $newData = $newData->toArray();
        return $newData['team_version'];

    }
}
