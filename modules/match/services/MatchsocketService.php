<?php


namespace app\modules\match\services;

use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchComingSocketOrder;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchSocketList;
use app\modules\match\models\SocketGameConfigure;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use function PHPUnit\Framework\throwException;


class MatchsocketService
{
    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     * 列表
     */
    public static function pageList($params)
    {
        $q=MatchSocketList::find()->alias('msl')
            ->select(['msl.*','eg.logo','sdm.name as rel_match_title','eo.name as origin_name','tone.name as t1_name','ttwo.name as t2_name'])
            ->leftJoin('enum_game as eg','eg.id = msl.game_id')
            ->leftJoin('standard_data_match as sdm' , 'sdm.rel_identity_id = msl.match_id')
            ->leftJoin('enum_origin as eo' , 'eo.id = msl.origin_id')
            ->leftJoin('standard_data_team as tone', 'tone.rel_identity_id = sdm.team_1_id')
            ->leftJoin('standard_data_team as ttwo', 'ttwo.rel_identity_id = sdm.team_2_id');
        $where = [];
        $origin_id=$params['origin_id'];
        $game_id=$params['game_id'];
        $match_id=$params['match_id'];
        $rel_match_title=$params['rel_match_title'];


//        if($params['date_begin']){  //开始时间
//            $q->andWhere(['>','msl.begin_at',$params['date_begin']]);
//        }
        if ($origin_id){
            $where = ['msl.origin_id'=>$origin_id];
        }
        if ($game_id){
            $where = ['msl.game_id'=>$game_id];
        }
        if ($match_id){
            $where = ['msl.match_id'=>$match_id];
        }
        if ($rel_match_title){
            $where = ['like','sdm.name',$rel_match_title];
        }
        if (isset($params['status']) && !empty($params['status']))  //比赛状态搜索
        {
            $where = ['=','msl.status',$params['status']];
        }
        $q->where($where);
        if (!empty($params['date_end']) && $params['date_begin']) { //结束时间
            $params['date_end'] = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $q->andWhere(['<','msl.begin_at',$params['date_end']])->andWhere(['>=','msl.begin_at',$params['date_begin']]);
        }
        $q->groupBy('msl.match_id');
        $totalCount = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);

        $q->orderBy('msl.begin_at desc')->offset($pages->offset)->limit($pages->limit);
        //$sql = $q->createCommand()->getRawSql();
        $socketList = $q->asArray()->all();
        return ['list' => $socketList, 'total' => $totalCount];
    }

    /**
     * @param $params
     * @return array
     * @throws BusinessException
     * 编辑
     */
    public static function edit($params)
    {
        if(isset($params['id'])){
            $socketInfo=MatchSocketList::find()->where(['id'=>$params['id']])->one();
        }
        if(!$socketInfo){
            $socketInfo=new MatchSocketList();
        }
        $socketInfo->setAttributes($params);

        if(!$socketInfo->save()){
            throw new BusinessException($socketInfo->getErrors(),'保存比赛Socket记录失败');
        }
        return $socketInfo->toArray();
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord|null
     * @throws BusinessException
     * 详情
     */
    public static function detail($params)
    {
        if(!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $socketInfo=MatchSocketList::find()->where(['id'=>$params['id']])->asArray()->one();
        return $socketInfo;
    }

    /**
     * @param $params
     * @return array
     * @throws BusinessException
     *删除
     */
    public static function del($params)
    {
        if(!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $socketInfo=MatchSocketList::find()->where(['id'=>$params['id']])->one();
        if($socketInfo){
            $socketInfo->setAttributes(['flag'=>2]);
            if(!$socketInfo->save()){
                throw new BusinessException($socketInfo->getErrors(),'保存失败');
            }
        }
        return $socketInfo->toArray();
    }
    //API列表
    public static function pageApilist($params)
    {
        $dateTimeStart =  date('Y-m-d');
        $dateTimeEnd =  date('Y-m-d',strtotime('+ 1day'));
        $q=MatchSocketList::find();
        $q->where(['flag'=>1]);
        $q->andWhere(['<=','status',2]);
        $origin_id=$params['origin_id'];
        $game_id=$params['game_id'];
        if ($origin_id){
            $q->andWhere(['origin_id'=>$origin_id]);
        }
        if ($game_id){
            $q->andWhere(['game_id'=>$game_id]);
        }
        $q->andWhere(['>=','begin_at',$dateTimeStart]);
        $q->andWhere(['<','begin_at',$dateTimeEnd]);
        $totalCount = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $q->orderBy('begin_at asc')->offset($pages->offset)->limit($pages->limit);
        $socketList = $q->asArray()->all();
        if ($totalCount > 0){
            foreach ($socketList as $key => $item){
                $socketList[$key]['start_connection'] = strtotime($item['begin_at']) - 840;
                $socketList[$key]['end_connection'] = strtotime($item['begin_at']) + 1800;
                if ($origin_id == 4){
                    $allMatchBattleOrders = MatchComingSocketOrder::find()->where(['match_id'=>$item['match_id']])->asArray()->all();
                    foreach ($allMatchBattleOrders as $order_key => $order_val){
                        $match_status = $item['status'];
                        //进行中 用外面表时间  进行中用里面表时间  如果为空 还是用外面表时间
                        if ($match_status == 1){
                            $order_begin_at = $item['begin_at'];
                        }else{
                            $order_begin_at = $order_val['begin_at'];
                            if (empty($order_begin_at)){
                                $order_begin_at = $item['begin_at'];
                            }
                        }
                        $allMatchBattleOrders[$order_key]['order_start_connection'] = strtotime($order_begin_at) - 840;
                        $allMatchBattleOrders[$order_key]['order_end_connection'] = strtotime($order_begin_at) + 1800;
                    }
                    $socketList[$key]['battle_order_list'] = $allMatchBattleOrders;
                }
            }
        }
        return ['list' => $socketList, 'total' => $totalCount];
    }
    //API 更新
    public static function apiEdit($params)
    {
        $origin_id = $match_id =null;
        if(isset($params['id'])){
            $socketInfo=MatchSocketList::find()->where(['id'=>$params['id']])->one();
            $origin_id = @$socketInfo['origin_id'];
            $match_id = @$socketInfo['match_id'];
        }
        if(!$socketInfo){
            return '无此条ID内容';
        }
        unset($params['id']);
        if ($params['spid1']){
            $params['spid1'] = @$socketInfo['spid1'].'_'.$params['spid1'];
        }
        if ($params['spid2']){
            $params['spid2'] = @$socketInfo['spid2'].'_'.$params['spid2'];
        }
        if ($params['spid3']){
            $params['spid3'] = @$socketInfo['spid3'].'_'.$params['spid3'];
        }
        if (isset($params['connection_error1']) && $params['connection_error1']){
            $params['connection_error1'] = @$socketInfo['connection_error1'].'_'.$params['connection_error1'];
        }
        if (isset($params['connection_error2']) && $params['connection_error2']){
            $params['connection_error2'] = @$socketInfo['connection_error2'].'_'.$params['connection_error2'];
        }
        if (isset($params['connection_error3']) && $params['connection_error3']){
            $params['connection_error3'] = @$socketInfo['connection_error3'].'_'.$params['connection_error3'];
        }
        if (!empty($params)){
            $socketInfo->setAttributes($params);
            if(!$socketInfo->save()){
                return '保存比赛Socket记录失败';
            }
        }
        //玩家 特殊
        if ( $origin_id == 4){
            $MatchComingSocketOrder = MatchComingSocketOrder::find()->where(['match_id'=>$match_id])->andWhere(['order'=>$params['order']])->one();
            if (!$MatchComingSocketOrder){
                return '无origin_id：'.$origin_id.'，match_id：'.$match_id.'，order：'.$params['order'].'的内容';
            }
            $params_MatchComingSocketOrder = [];
            //获取要修改的值
            if (isset($params['order_begin_at'])){
                $params_MatchComingSocketOrder['begin_at'] = $params['order_begin_at'];
            }
            if (isset($params['order_status'])){
                $params_MatchComingSocketOrder['status'] = $params['order_status'];
            }
            if (isset($params['order_fpid'])){
                $params_MatchComingSocketOrder['fpid'] = $params['order_fpid'];
            }
            if (isset($params['order_spid'])){
                if (empty($params['order_spid'])){
                    $params_MatchComingSocketOrder['spid'] = null;
                }else{
                    $params_MatchComingSocketOrder['spid'] = @$MatchComingSocketOrder['spid'].'_'.$params['order_spid'];
                }
            }
            if (isset($params['order_connection_status'])){
                $params_MatchComingSocketOrder['connection_status'] = (Int)$params['order_connection_status'];
            }
            if (isset($params['order_server_status'])){
                $params_MatchComingSocketOrder['server_status'] = (Int)$params['order_server_status'];
            }
            if (isset($params['status_refresh'])){
                $params_MatchComingSocketOrder['status_refresh'] = (Int)$params['status_refresh'];
            }
            if (isset($params['order_connection_error'])){
                $params_MatchComingSocketOrder['connection_error'] = @$MatchComingSocketOrder['connection_error'].'_'.$params['order_connection_error'];
            }
            if (!empty($params_MatchComingSocketOrder)){
                $MatchComingSocketOrder->setAttributes($params_MatchComingSocketOrder);
                if(!$MatchComingSocketOrder->save()){
                    return '保存MatchComingSocketOrder记录失败match_id：'.$match_id.'，order：'.$params['order'];
                }
            }
        }
        return 'success';
    }
    //API get
    public static function apiget($params)
    {
        $socketInfo = [];
        $origin_id = $match_id =null;
        if(isset($params['id'])){
            $socketInfo=MatchSocketList::find()->where(['id'=>$params['id']])->asArray()->one();
            $origin_id = @$socketInfo['origin_id'];
            $match_id = @$socketInfo['match_id'];
        }
        if(!$socketInfo){
            return '无此条ID内容';
        }
        //玩家 特殊
        if ($origin_id == 4){
            $MatchComingSocketOrder = MatchComingSocketOrder::find()->where(['match_id' => $match_id])->andWhere(
                ['order' => $params['order']]
            )->asArray()->one();
            if (!$MatchComingSocketOrder) {
                return '无origin_id：'.$origin_id.'，match_id：'.$match_id.'，order：'.$params['order'].'的内容';
            }
            $order_connection_status = $MatchComingSocketOrder['connection_status'];
            if ($order_connection_status == 1 || $order_connection_status ==2){
                return 'no';
            }
        }else{
            $old_connection_status1 = $socketInfo['connection_status1'];
            $old_connection_status2 = $socketInfo['connection_status2'];
            $old_connection_status3 = $socketInfo['connection_status3'];
            $server_id = $params['server_id'];
            if ($server_id == 1){
                //1请求中，2是连接中
                if ($old_connection_status1 == 1 || $old_connection_status1 ==2){
                    return 'no';
                }
            }
            if ($server_id == 2){
                //1请求中，2是连接中
                if ($old_connection_status2 == 1 || $old_connection_status2 ==2){
                    return 'no';
                }
            }
            if ($server_id == 3){
                //1请求中，2是连接中
                if ($old_connection_status3 == 1 || $old_connection_status3 ==2){
                    return 'no';
                }
            }
        }
        return 'ok';
    }
    //API 更新
    public static function apiKillPidList($params)
    {
        $dateTimeStart =  date('Y-m-d H:i:s',strtotime('+ 1day'));
        $dateTimeEnd =  date('Y-m-d H:i:s',strtotime('- 1day'));
        $q=MatchSocketList::find();
        $q->where(['flag'=>1]);
        if (isset($params['origin_id']) && $params['origin_id']){
            $q->andWhere(['origin_id'=>$params['origin_id']]);
        }
        $q->andWhere(['>=','begin_at',$dateTimeEnd]);
        $q->andWhere(['<','begin_at',$dateTimeStart]);
        $q->orderBy('status desc');
        // = $q->createCommand()->getRawSql();
        $socketList = $q->asArray()->all();
        foreach ($socketList as &$item){
            if (!empty($item['end_at'])){
                $item['end_at'] = date('Y-m-d H:i:s',strtotime($item['end_at']) + 1800);
            }
            if (@$item['origin_id'] == 4){
                $allMatchBattleOrders = MatchComingSocketOrder::find()->where(['match_id'=>$item['match_id']])->asArray()->all();
                $item['battle_order_list'] = $allMatchBattleOrders;
            }
        }
        return ['list' => $socketList];
    }

    public static function apiConnectList($params){
        $origin_id = (Int) $params['origin_id'];
        $connect_id = (Int) $params['connect_id'];
        $list = [];
        if(!empty($origin_id)){
            if ($origin_id == 4){
                $list = MatchComingSocketOrder::find()->alias('mcso')
                    ->select(['mcso.*','mcs.id as match_primary_id','mcs.match_id as match_match_id','mcs.begin_at as match_begin_at',
                        'mcs.end_at as match_end_at', 'mcs.status as match_status',
                        'mcs.websocket_time1 as match_websocket_time1',
                        'mcs.websocket_time2 as match_websocket_time2'])
                    ->leftJoin('match_coming_socket as mcs','mcs.match_id = mcso.match_id')
                    ->where(['mcs.origin_id' => 4])->where(['mcso.connection_status' => 2])->asArray()->all();
            }else{
                if ($connect_id == 1){
                    $list =  MatchSocketList::find()->where(['origin_id'=>$origin_id])->andWhere(['connection_status1' => 2])->asArray()->all();
                }
                if ($connect_id == 2){
                    $list =  MatchSocketList::find()->where(['origin_id'=>$origin_id])->andWhere(['connection_status2' => 2])->asArray()->all();
                }
            }
        }

        return $list;
    }

    //获取接收的列表
    public static function receiveList($origin_id,$rel_match_id,$search_data = null, $search_id = null){
        if (empty($origin_id) || empty($rel_match_id)){
            return '参数不能为空';
        }
        if ($origin_id == 9||$origin_id == 10){
            $rel_identity_id_Info =  StandardDataMatch::find()->select('perid')->where(['origin_id' => $origin_id])->andWhere(['rel_identity_id' => $rel_match_id])->asArray()->one();
            if (!$rel_identity_id_Info){
                return 'not have rel_identity_id';
            }
            $rel_match_id = $rel_identity_id_Info['perid'];
        }
        $MatchLived = MatchLived::find()->where(['match_id' => $rel_match_id])->andWhere(['origin_id' => $origin_id]);
        if (!empty($search_id)){
            $MatchLived->andWhere(['id'=>$search_id]);
        }
        if (!empty($search_data)){
            $MatchLived->andWhere(['like','match_data',$search_data]);
        }
        $MatchLived->orderBy('id desc');
        $sql = $MatchLived->createCommand()->getRawSql();
        $totalCount = $MatchLived->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $MatchLived->offset($pages->offset)->limit($pages->limit);
        //$sql = $MatchLived->createCommand()->getRawSql();
        $MatchLivedList = $MatchLived->asArray()->all();
        return ['list' => $MatchLivedList, 'total' => $totalCount];
    }

    //livedDel 删除
    public static function livedDel($lived_id){
        if (!empty($lived_id)){
            $livedInfo = MatchLived::find()->where(['id' => $lived_id])->one();
            $livedInfo->delete();
//            $data = [
//                'deleted' => 1,
//                'deleted_at' => date('Y-m-d H:i:s')
//            ];
//            $livedInfo->setAttributes($data);
//            $livedInfo->save();
        }
        return true;
    }
    public static function livedResume($lived_id){
        if (!empty($lived_id)){
            $livedInfo = MatchLived::find()->where(['id' => $lived_id])->one();
            $data = [
                'deleted' => 2,
                'deleted_at' => null
            ];
            $livedInfo->setAttributes($data);
            $livedInfo->save();
        }
        return true;
    }

    //重抓
    public static function regraspSocket($origin_id,$match_id,$game_id){
        $taskRes = [];
        $matchInfo = Match::find()->where(['id'=>$match_id])->asArray()->one();
        if ($matchInfo){
            $rel_match_id = HotBase::getRelIdByMasterId('match',$match_id,4);
            if ($rel_match_id){
                //删除原有的Socket 数据
                //MatchLived::deleteAll(['match_id' => $rel_match_id,'origin_id'=>$origin_id]);
                //循环获取
                //$number_of_games = $matchInfo['number_of_games'];
                //组合数据
                $params = [
                    'match_id' => $match_id,
                    'origin_id' => $origin_id,
                    'game_id' => $game_id
                ];
                if ($origin_id == 4){
//                    for ($i=1;$i<=$number_of_games;$i++){
//                        self::regraspSocketByOrange($origin_id,$game_id,$rel_match_id,-1000,$i);
//                    }
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            999,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            999,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$origin_id}_{$match_id}_{$game_id}"
                    ];
                    //插入taskinfo
                    $taskRes = TaskRunner::addTask($task, 9002);
                }else{
                    throw new BusinessException([],'暂无处理该数据源');
                }
            }
        }
        return $taskRes;
    }
    //Orange
    private static function regraspSocketByOrange($origin_id,$game_id,$rel_match_id,$start_time,$match_order){
        $socket_time = date("Y-m-d H:i:s");
        $action='/lol/schedule/matchlog';
        $params=[
            'schedule_id'=> $rel_match_id,
            'match_order'=> $match_order,
            'start_time' =>$start_time
        ];
        $infoJson=OrangeBase::curlGet($action,'GET',$params);
        $info=@json_decode($infoJson,true);
        $res = @$info['data']['list'];
        if ($res){
            foreach ($res as $val){
                $match_data = json_encode($val,320);
                $insertData = [
                    'origin_id' => (Int)$origin_id,
                    'game_id' => (Int)$game_id,
                    'match_id' => (String)$rel_match_id,
                    'round' => (String)$match_order,
                    'match_data' => $match_data,
                    'type' => 'frames',
                    'socket_time' => $socket_time
                ];
                $MatchLivedModel = new MatchLived();
                $MatchLivedModel->setAttributes($insertData);
                $MatchLivedModel->save();
            }
            $event_time = end($res)['event_time'];
            $start_time = $event_time + 1;
            self::regraspSocketByOrange($origin_id,$game_id,$rel_match_id,$start_time,$match_order);
        }else{
            return;
        }
    }

    //socket录入服务器配置 列表
    public static function setSocketGameConfigure($game_id,$attributes){
        if (empty($game_id)){
            throw new BusinessException([], '游戏ID不能为空！');
        }
        $SocketGameConfigure = SocketGameConfigure::find()->where(['game_id'=>$game_id])->one();
        if ($SocketGameConfigure){
            $SocketGameConfigure->setAttributes($attributes);
            if (!$SocketGameConfigure->save()){
                throw new BusinessException($SocketGameConfigure->getErrors(), 'game_id：'.$game_id.'保存失败！');
            }
            $redis = new \Redis();
            $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
            $redis->auth(env('REDIS_PASSWORD'));
            $redis->select(env('REDIS_DATABASE'));
            //redis name
            $redis_Name_Str = 'enum:socket_';
            $socketGameConfigure = SocketGameConfigure::find()->where(['game_id'=>$game_id])->asArray()->one();
            if ($socketGameConfigure){
                if ($socketGameConfigure['game_id'] == 1){
                    $csgo_socket_configure = [
                        'mp_maxrounds' => $socketGameConfigure['mp_maxrounds'],
                        'mp_overtime_enable' => $socketGameConfigure['mp_overtime_enable'],
                        'mp_overtime_maxrounds' => $socketGameConfigure['mp_overtime_maxrounds'],
                        'mp_roundtime_defuse' => $socketGameConfigure['mp_roundtime_defuse'],
                        'mp_freezetime' => $socketGameConfigure['mp_freezetime'],
                        'mp_c4timer' => $socketGameConfigure['mp_c4timer']
                    ];
                    $redis->hMSet($redis_Name_Str.'csgo',$csgo_socket_configure);
                }
                if ($socketGameConfigure['game_id'] == 2){
                    $lol_socket_configure = [
                        'inhibitor_reborn_time' => $socketGameConfigure['inhibitor_reborn_time'],
                        'rift_herald_first_time' => $socketGameConfigure['rift_herald_first_time'],
                        'rift_herald_reborn_time' => $socketGameConfigure['rift_herald_reborn_time'],
                        'rift_herald_quit_time' => $socketGameConfigure['rift_herald_quit_time'],
                        'rift_herald_max_num' => $socketGameConfigure['rift_herald_max_num'],
                        'dragon_first_time' => $socketGameConfigure['dragon_first_time'],
                        'dragon_reborn_time' => $socketGameConfigure['dragon_reborn_time'],
                        'dragon_quit_num' => $socketGameConfigure['dragon_quit_num'],
                        'baron_nashor_first_time' => $socketGameConfigure['baron_nashor_first_time'],
                        'baron_nashor_reborn_time' => $socketGameConfigure['baron_nashor_reborn_time'],
                        'elder_dragon_first_time' => $socketGameConfigure['elder_dragon_first_time'],
                        'elder_dragon_reborn_time' => $socketGameConfigure['elder_dragon_reborn_time']
                    ];
                    $redis->hMSet($redis_Name_Str.'lol',$lol_socket_configure);
                }
            }
        }
        return ['msg' => '保存成功'];
    }
}
