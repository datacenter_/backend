<?php


namespace app\modules\match\services;

use app\modules\match\models\MatchSocketListDev;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;


class MatchsocketServiceDev
{
    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     * 列表
     */
    public static function pageList($params)
    {
        $q=MatchSocketListDev::find()->alias('msl')
            ->select(['msl.*','eg.logo','sdm.name as rel_match_title','eo.name as origin_name','tone.name as t1_name','ttwo.name as t2_name'])
            ->leftJoin('enum_game as eg','eg.id = msl.game_id')
            ->leftJoin('standard_data_match as sdm' , 'sdm.rel_identity_id = msl.match_id')
            ->leftJoin('enum_origin as eo' , 'eo.id = msl.origin_id')
            ->leftJoin('standard_data_team as tone', 'tone.rel_identity_id = sdm.team_1_id')
            ->leftJoin('standard_data_team as ttwo', 'ttwo.rel_identity_id = sdm.team_2_id');
        $where = [];
        $origin_id=$params['origin_id'];
        $game_id=$params['game_id'];
        $match_id=$params['match_id'];
        $rel_match_title=$params['rel_match_title'];


//        if($params['date_begin']){  //开始时间
//            $q->andWhere(['>','msl.begin_at',$params['date_begin']]);
//        }
        if ($origin_id){
            $where = ['msl.origin_id'=>$origin_id];
        }
        if ($game_id){
            $where = ['msl.game_id'=>$game_id];
        }
        if ($match_id){
            $where = ['msl.match_id'=>$match_id];
        }
        if ($rel_match_title){
            $where = ['like','sdm.name',$rel_match_title];
        }
        if (isset($params['status']) && !empty($params['status']))  //比赛状态搜索
        {
            $where = ['=','msl.status',$params['status']];
        }
        $q->where($where);
        if (!empty($params['date_end']) && $params['date_begin']) { //结束时间
            $params['date_end'] = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $q->andWhere(['<','msl.begin_at',$params['date_end']])->andWhere(['>=','msl.begin_at',$params['date_begin']]);
        }
        $totalCount = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);

        $q->orderBy('msl.begin_at desc')->offset($pages->offset)->limit($pages->limit);
        //$sql = $q->createCommand()->getRawSql();
        $socketList = $q->asArray()->all();
        return ['list' => $socketList, 'total' => $totalCount];
    }

    /**
     * @param $params
     * @return array
     * @throws BusinessException
     * 编辑
     */
    public static function edit($params)
    {
        if(isset($params['id'])){
            $socketInfo=MatchSocketListDev::find()->where(['id'=>$params['id']])->one();
        }
        if(!$socketInfo){
            $socketInfo=new MatchSocketListDev();
        }
        $socketInfo->setAttributes($params);

        if(!$socketInfo->save()){
            throw new BusinessException($socketInfo->getErrors(),'保存比赛Socket记录失败');
        }
        return $socketInfo->toArray();
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord|null
     * @throws BusinessException
     * 详情
     */
    public static function detail($params)
    {
        if(!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $socketInfo=MatchSocketListDev::find()->where(['id'=>$params['id']])->asArray()->one();
        return $socketInfo;
    }

    /**
     * @param $params
     * @return array
     * @throws BusinessException
     *删除
     */
    public static function del($params)
    {
        if(!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $socketInfo=MatchSocketListDev::find()->where(['id'=>$params['id']])->one();
        if($socketInfo){
            $socketInfo->setAttributes(['flag'=>2]);
            if(!$socketInfo->save()){
                throw new BusinessException($socketInfo->getErrors(),'保存失败');
            }
        }
        return $socketInfo->toArray();
    }
    //API列表
    public static function pageApilist($params)
    {
        $dateTimeStart =  date('Y-m-d');
        $dateTimeEnd =  date('Y-m-d',strtotime('+ 1day'));
        $q=MatchSocketListDev::find();
        $q->where(['flag'=>1]);
        $q->andWhere(['<=','status',2]);
        $origin_id=$params['origin_id'];
        $game_id=$params['game_id'];
        if ($origin_id){
            $q->andWhere(['origin_id'=>$origin_id]);
        }
        if ($game_id){
            $q->andWhere(['game_id'=>$game_id]);
        }
        $q->andWhere(['>=','begin_at',$dateTimeStart]);
        $q->andWhere(['<','begin_at',$dateTimeEnd]);
        $totalCount = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $q->orderBy('begin_at asc')->offset($pages->offset)->limit($pages->limit);
        $socketList = $q->asArray()->all();
        if ($totalCount > 0){
            foreach ($socketList as $key => $item){
                $socketList[$key]['start_connection'] = strtotime($item['begin_at']) - 840;
                $socketList[$key]['end_connection'] = strtotime($item['begin_at']) + 1800;
            }
        }
        return ['list' => $socketList, 'total' => $totalCount];
    }
    //API 更新
    public static function apiEdit($params)
    {
        if(isset($params['id'])){
            $socketInfo=MatchSocketListDev::find()->where(['id'=>$params['id']])->one();
        }
        if(!$socketInfo){
            return '无此条ID内容';
        }
        if ($params['spid1']){
            $params['spid1'] = @$socketInfo['spid1'].'_'.$params['spid1'];
        }
        if ($params['spid2']){
            $params['spid2'] = @$socketInfo['spid2'].'_'.$params['spid2'];
        }
        if ($params['spid3']){
            $params['spid3'] = @$socketInfo['spid3'].'_'.$params['spid3'];
        }
        if (isset($params['connection_error1']) && $params['connection_error1']){
            $params['connection_error1'] = @$socketInfo['connection_error1'].'_'.$params['connection_error1'];
        }
        if (isset($params['connection_error2']) && $params['connection_error2']){
            $params['connection_error2'] = @$socketInfo['connection_error2'].'_'.$params['connection_error2'];
        }
        if (isset($params['connection_error3']) && $params['connection_error3']){
            $params['connection_error3'] = @$socketInfo['connection_error3'].'_'.$params['connection_error3'];
        }
        $socketInfo->setAttributes($params);
        if(!$socketInfo->save()){
            return '保存比赛Socket记录失败';
        }
        return 'success';
    }
    //API get
    public static function apiget($params)
    {
        $socketInfo = [];
        if(isset($params['id'])){
            $socketInfo=MatchSocketListDev::find()->where(['id'=>$params['id']])->asArray()->one();
        }
        if(!$socketInfo){
            return '无此条ID内容';
        }
        $old_connection_status1 = $socketInfo['connection_status1'];
        $old_connection_status2 = $socketInfo['connection_status2'];
        $old_connection_status3 = $socketInfo['connection_status3'];
        $server_id = $params['server_id'];
        if ($server_id == 1){
            //1请求中，2是连接中
            if ($old_connection_status1 == 1 || $old_connection_status1 ==2){
                return 'no';
            }
        }
        if ($server_id == 2){
            //1请求中，2是连接中
            if ($old_connection_status2 == 1 || $old_connection_status2 ==2){
                return 'no';
            }
        }
        if ($server_id == 3){
            //1请求中，2是连接中
            if ($old_connection_status3 == 1 || $old_connection_status3 ==2){
                return 'no';
            }
        }
        return 'ok';
    }
    //API 更新
    public static function apiKillPidList($params)
    {
        $dateTimeStart =  date('Y-m-d H:i:s',strtotime('+ 1day'));
        $dateTimeEnd =  date('Y-m-d H:i:s',strtotime('- 1day'));
        $q=MatchSocketListDev::find();
        $q->where(['flag'=>1]);
        if (isset($params['origin_id']) && $params['origin_id']){
            $q->andWhere(['origin_id'=>$params['origin_id']]);
        }
        $q->andWhere(['>=','begin_at',$dateTimeEnd]);
        $q->andWhere(['<','begin_at',$dateTimeStart]);
        $q->orderBy('status desc');
        // = $q->createCommand()->getRawSql();
        $socketList = $q->asArray()->all();
        foreach ($socketList as &$item){
            if (!empty($item['end_at'])){
                $item['end_at'] = date('Y-m-d H:i:s',strtotime($item['end_at']) + 1800);
            }
        }
        return ['list' => $socketList];
    }
}