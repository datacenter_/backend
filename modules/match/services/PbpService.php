<?php
/**
 *
 */

namespace app\modules\match\services;


use app\modules\match\models\Match;

class PbpService
{
    public static function getUpComing()
    {
        $nowDate = date("Y-m-d H:i:s");
        $endDate = date("Y-m-d H:i:s", strtotime("-20 minute"));
        $startDate = date("Y-m-d H:i:s", strtotime("+20 minute"));

        $data = Match::find()->alias('m')
            ->select(['m.id as match_id'])
            ->leftJoin('match_real_time_info as r','r.id = m.id')
            ->where(
                [
                    'and',
                    ['=','r.is_pbpdata_supported',1],
                    [
                        'or',
                        ['=', "r.status", 2],
                        [
                            'and',
                            ['>=','r.end_at',$endDate],
                            ['=','r.status',3],
                        ],
                        [
                            'and',
                            ['<=','m.scheduled_begin_at',$startDate],
                            ['=','r.status',1],
                        ]
                    ]
                ]
            );
//            ->andWhere(['and',
//                ['=', "r.status", 2],
//                ['r.is_pbpdata_supported'=>1]
//            ])
//            ->orWhere(['and',
//                ['>=','r.end_at',$endDate],
//                ['<=','m.scheduled_begin_at',$startDate],
//                ['r.is_pbpdata_supported'=>1]
//            ]);
        $q = $data->createCommand()->getRawSql();
        $datas = $data->asArray()->all();
        return $datas;
    }
}
