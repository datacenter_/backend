<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/09/27
 * Time: 06:13
 */

namespace app\modules\match\services;

use app\modules\match\models\Match;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\rest\exceptions\BusinessException;
use app\modules\common\services\DbHelper;
use yii\data\Pagination;

class RoundService
{
    public static function getRoundDelete($battleId,$roundId)
    {
        if(!$roundId || !$battleId) throw new BusinessException([], "缺少字段参数！");
        // 找出最大的round_ordinal
        $roundMaxOrdinal = MatchBattleRoundCsgo::find()->where(['battle_id'=>$battleId])->max('`round_ordinal`');
        $roundOrdinal = MatchBattleRoundCsgo::find()->where(['id'=>$roundId])->max('`round_ordinal`');
        if($roundMaxOrdinal){
            if($roundOrdinal == $roundMaxOrdinal){
                $roundModel = MatchBattleRoundCsgo::find()->where(['id' => $roundId,'battle_id' => $battleId])->one();;
            }else{
                throw new BusinessException([], "此Round_Id不是现在最大的回合Id，请删除最新的Round！");
            }
        }else{
            throw new BusinessException([], "此battle下没有有效Round！");
        }
        if(!$roundModel->delete()){
            throw new BusinessException($roundModel->getErrors(),'删除失败');
        }
        // 操作有效回合序号
        $matchBattleExt = MatchBattleExtCsgo::find()->where(['id' => $battleId])->one();
        if($matchBattleExt){
            $roundLivedJson = $matchBattleExt->live_rounds;
            if($roundLivedJson && $roundLivedJson!='[]'){
                $roundLivedArray = json_decode($roundLivedJson,true);
                $currentRoundLives = array_merge(array_diff($roundLivedArray, array($roundOrdinal)));
                // array_unique
                $currentRoundLivesJson = array_unique($currentRoundLives);
                $matchBattleExt->setAttributes(['live_rounds'=>json_encode($currentRoundLivesJson)]);
                $matchBattleExt->save();
            }
        }
        return true;
    }
    public static function getRoundCreate($battleId)
    {
        if(!$battleId) throw new BusinessException([], "缺少字段参数！");
        $roundModel = new MatchBattleRoundCsgo();
        // 找出最大的round_ordinal
        $roundMaxOrdinal = MatchBattleRoundCsgo::find()->where(['battle_id'=>$battleId])->max('`round_ordinal`');
        $currentRoundMaxOrdinal = $roundMaxOrdinal ? $roundMaxOrdinal+1:1;
        $roundModel->setAttributes(['battle_id'=>$battleId,'round_ordinal'=>$currentRoundMaxOrdinal]);
        if (!$roundModel->save()) {
            throw new BusinessException($roundModel->getErrors(), "创建回合失败！");
        }
        // 操作有效回合序号
        $matchBattleExt = MatchBattleExtCsgo::find()->where(['id' => $battleId])->one();
        if(!$matchBattleExt){
            $matchBattleExt = new MatchBattleExtCsgo;
            $matchBattleExtArray = [
                'id' => $battleId,
                'live_rounds' => json_encode(array($currentRoundMaxOrdinal))
            ];
            $matchBattleExt->setAttributes($matchBattleExtArray);
            $matchBattleExt->save();
        }else {
            $roundLivedJson = $matchBattleExt->live_rounds;
            if ($roundLivedJson && $roundLivedJson != '[]') {
                $roundLivedArray = json_decode($roundLivedJson, true);
                array_push($roundLivedArray, $currentRoundMaxOrdinal);
                // array_unique
                $currentRoundLives = array_unique($roundLivedArray);
                $matchBattleExtArray = [
                    'live_rounds' => json_encode($currentRoundLives)
                ];
            } else {
                $matchBattleExtArray = [
                    'live_rounds' => json_encode(array($currentRoundMaxOrdinal))
                ];
            }
            $matchBattleExt->setAttributes($matchBattleExtArray);
            $matchBattleExt->save();
        }
        return $roundModel->toArray();
    }
}