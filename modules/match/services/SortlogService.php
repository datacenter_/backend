<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/7/29
 * Time: 22:13
 */

namespace app\modules\match\services;

use app\modules\match\models\Match;
use app\modules\match\models\SortingLog;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\org\models\Team;
use app\rest\exceptions\BusinessException;
use app\modules\common\services\DbHelper;
use yii\data\Pagination;

class SortlogService
{
    public static function getSortingLogDelete($logId,$flag)
    {
        if(!$logId || !$flag) throw new BusinessException([], "缺少字段参数！");
        $sortLog = SortingLog::find()->where(['id'=>$logId])->one();
        $sortLog->setAttributes([
            'flag'=>$flag,
        ]);
        if (!$sortLog->save()) {
            throw new BusinessException($sortLog->getErrors(), '删除失败');
        }
        return $sortLog->toArray();
    }
    public static function getSortingLogEdit($params)
    {
        $logId = $params['log_id'];
        $sortLog = SortingLog::find()->where(['id'=>$logId])->one();
        if (!empty($params['ip_arr'])){
            foreach ($params['ip_arr'] as $k=>$v){
                if (is_array($v)&&$v['service_ip']){
                    $params['ip_arr'][$k]['service_ip'] =trim($v['service_ip']);
                }
            }
        }
        $params['service_ip'] = json_encode($params['ip_arr']);//改成可以接收不同时间段不同ip
        $sortLog->setAttributes($params);
        if (!$sortLog->save()) {
            throw new BusinessException($sortLog->getErrors(), '修改失败');
        }
        return $sortLog->toArray();
    }
    public static function getSortingLogSelect($params)
    {
        $logId = $params['log_id'];
        if(!$logId) throw new BusinessException([], "未发现日志ID！");
        return SortingLog::find()->where(['id'=>$logId])->one();
    }
    public static function getSortingLogAdd($params)
    {
        $sortLogData = SortingLog::find()->where(['log_type'=>$params['log_type'],'match_id'=>$params['match_id']])->one();
        if($sortLogData){
            throw new BusinessException([], "该比赛下日志类型已经创建，请去编辑页面！");
        }
        $sortLog = new SortingLog();
        $sortLog->setAttributes($params);
        if (!$sortLog->save()) {
            throw new BusinessException($sortLog->getErrors(), "添加日志分拣失败！");
        }
        return $sortLog->toArray();
    }
    public static function getMatchList($params)
    {
        if(isset($params['tournament_id']) && !empty($params['tournament_id'])){
            $where = ['tournament_id'=>$params['tournament_id']];
        }
        if(isset($params['group_id']) && !empty($params['group_id'])){
            $where = ['group_id'=>$params['group_id']];
        }
        if(empty($where)){
            return [];
        }
        return Match::find()->alias('m')->select([
                'm.id','m.team_1_id','m.team_2_id','m.scheduled_begin_at',
                'team.name as main_team_name',
                'team2.name as other_team_name'
            ])
            ->leftJoin("team as team", "m.team_1_id = team.id")
            ->leftJoin("team as team2", "m.team_2_id = team2.id")
            ->orderBy('m.scheduled_begin_at desc')
            ->where($where)->asArray()->all();
    }
    public static function getSortingLogList($params)
    {
        $whereConfig = [
            "game_id" => [ 'type' => "=", 'key' => 'm.game'],
            "id" => [ 'type' => "=", 'key' => 'm.id'],
            "status" => [ 'type' => "=", 'key' => 'm_real.status'],
            "game_rules" => [ 'type' => "=", 'key' => 'm.game_rules'],
            "match_type" => [ 'type' => "=", 'key' => 'm.match_type']
        ];
        $where = DbHelper::getWhere($whereConfig, $params);
        $q = SortingLog::find()->alias('log')->where(array_merge(['and'], $where));
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $q->andWhere(['>=', "m.modified_at", $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $q->andWhere(['<', "m.modified_at", $end]);
        }
        if (isset($params['scheduled_begin_at']) && !empty($params['scheduled_begin_at'])) {
            $q->andWhere(['>=', "m.scheduled_begin_at", $params['scheduled_begin_at']]);
        }
        if (isset($params['scheduled_end_at']) && !empty($params['scheduled_end_at'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));
            $q->andWhere(['<', "m.scheduled_begin_at", $end]);
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $q->andWhere(['like',"tour.name",$params['name']]);
        }
        if (isset($params['group_name']) && !empty($params['group_name'])) {
            $q->andWhere(['like',"group.name",$params['group_name']]);
        }
        if (isset($params['team_name']) && !empty($params['team_name'])) {
            $q->andWhere(['or', ['like', "team.name", $params['team_name']], ['like', "team2.name", $params['team_name']]]);
        }
             $q->select([
                 'log.id','log.match_id','log.log_type','log.team_1_log_name','log.team_2_log_name','log.service_ip','log.sleeps','log.timezone','log.flag',
                 'm.number_of_games','m.scheduled_begin_at','m.team_1_id','m.team_2_id','m.created_at','m.modified_at',
                 'game.image as logo',
                 'm_real.status as status',
                 'tour.name as name','tour.name_cn name_cn',
                 'group.name as group_name','group.name_cn as group_name_cn',
                 'rules.name as game_rules','type.name as match_type',
                 'team.name as main_team_name','team.image as main_logo',
                 'team2.name as other_team_name','team2.image as other_logo'
             ])
//             ->joinWith('match as m')
             ->leftJoin("match as m", "log.match_id = m.id")
             ->leftJoin("match_real_time_info as m_real", "m.id = m_real.id")
             ->leftJoin("enum_game as game", "m.game = game.id")
             ->leftJoin("tournament as tour", "m.tournament_id = tour.id")
             ->leftJoin("tournament_group as group", "m.group_id = group.id")
             ->leftJoin("enum_game_rules as rules", "m.game_rules = rules.id")
             ->leftJoin("enum_game_match_type as type", "m.match_type = type.id")
             ->leftJoin("team as team", "m.team_1_id = team.id")
             ->leftJoin("team as team2", "m.team_2_id = team2.id");
        $q->orderBy('m.scheduled_begin_at desc');
        $count = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $list, 'total' => $count];
    }
}