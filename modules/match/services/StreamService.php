<?php


namespace app\modules\match\services;

use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DbStream;
use app\modules\match\models\EnumStreamPlatform;
use app\modules\match\models\EnumVideoOrigin;
use app\modules\match\models\Match;
use app\modules\match\models\MatchStreamList;
use app\modules\match\models\SortingLog;
use app\modules\task\models\EnumOrigin;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataMatchVideoList;
use app\modules\task\models\StandardDataMatchVideoListRelation;
use app\modules\task\services\Common;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\org\models\Team;
use app\rest\exceptions\BusinessException;
use app\modules\common\services\DbHelper;
use PHPUnit\phpDocumentor\Reflection\Types\Null_;
use yii\data\Pagination;
use yii\validators\ValidationAsset;


class StreamService
{

    public static function edit($params, $userType, $userId = 0)
    {
        $streamInfo = null;
        if (isset($params['id'])) {
            $streamInfo = MatchStreamList::find()->where(['id'=>$params['id']])->one();
            $live_embed_url = MatchStreamList::find()
                ->andWhere(['<>', 'id', $params['id']])
//                ->andWhere(['<>', 'video_origin_id', $streamInfo['video_origin_id']])
                ->andwhere(['match_id'=> $params['match_id']])
                ->andwhere(['live_url'=> $params['live_url']])
                ->one();
            if ($live_embed_url) {
                throw new BusinessException($live_embed_url->getErrors(), '直播间地址不能重复添加');
            }
        } else {
            $id_add_live_embed_url = MatchStreamList::find()
                ->where(['live_url' => $params['live_url']])
                ->andWhere(['match_id' => $params['match_id']])
                ->one();
              $params['video_origin_id'] = 7; //手动添加啊；‘
            if ($id_add_live_embed_url) {
                throw new BusinessException($id_add_live_embed_url->getErrors(), '直播间地址不能重复添加');
            }

        }
        if (!$streamInfo) {
            $streamInfo = new MatchStreamList();
        }
         if ($params['live_url']) {
             $params['live_url_identity'] = md5($params['live_url']);
         }

        $streamInfo->setAttributes($params);
        if (!$streamInfo->save()) {
            throw new BusinessException($streamInfo->getErrors(), '保存直播记录失败');
        }
        return $streamInfo->toArray();
    }

    public static function detail($params, $userType, $userId = 0)
    {
        if (!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $streamInfo = MatchStreamList::find()->where(['id' => $params['id']])->asArray()->one();
        return ['list' => $streamInfo];


    }

    public static function del($params)
    {
        if (!$params['id']) throw new BusinessException([], "缺少字段参数！");
        $streamInfo = MatchStreamList::find()->where(['id' => $params['id']])->one();
        if (!$streamInfo->delete()) {
            throw new BusinessException($streamInfo->getErrors(), '删除失败');
        }

        return $streamInfo->toArray();
    }

    /**
     * 已绑定或者已添加的视频
     * @param $params
     * @param $userType
     * @param int $userId
     * @return array
     */
    public static function pageList($params, $userType, $userId = 0)
    {
        $q = MatchStreamList::find()->alias('m');
        $q->select(["m.*", "v.name as v_name", 'v.id as v_id', "o.name as o_name", "o.id as i_id"]);
        $q->leftJoin('enum_video_origin as v', 'v.id = m.video_origin_id');
        $q->leftJoin('enum_origin as o', 'o.id =  m.origin_id');
        $matchId = $params['matchId'];
        $q->where(['m.match_id' => $matchId, 'm.flag' => 1]);

        $totalCount = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);

        $list = $q->offset($pages->offset)->limit($pages->limit)->orderBy('m.viewers desc')->asArray()->all();


        foreach ($list as $key => &$val) {
            $val['origin_name'] = $val['v_name'] ?: $val['o_name'];
        }
        return ['list' => $list, 'total' => $totalCount];
    }

    /**
     * 给达磊专用的接口
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListUrl($params)
    {
        $q = MatchStreamList::find();
//        $q = new MatchStreamList();
        $matchId = $params['match_id'];
        $q->where(['match_id' => $matchId, 'flag' => 1]);;

        $list = $q->asArray()->all();

        $originIdList = array_filter(array_column($list, "origin_id", 'id'));
        if (!empty($originIdList)) {
            $originInfo = EnumOrigin::find()->select(['name', 'id'])->where(['id' => $originIdList])->asArray()->all();
            $originId = array_column($originInfo, 'name', 'id');
        }
        foreach ($list as $key => &$val) {
            $val['origin_name'] = $originId[$val['origin_id']] ?? '';
        }
        return $list;
    }

    public static function getGameByDaleiGame($game){
        $daleiGame = [
            "CS:GO" =>1,
            "csgo" =>1,
            "lol" =>2,
            "FIFA" =>19,
            "使命召唤：现代战争" =>10,
            "刀塔2" =>3,
            "dota2" =>3,
            "反恐精英：全球攻势" =>1,
            "堡垒之夜" =>18,
            "守望先锋" =>5,
            "无畏契约" =>12,
            "星际争霸Ⅱ&星际争霸" =>6,
            "炉石传说" =>7,
            "王者荣耀" =>4,
            "绝地求生" =>8,
            "英雄联盟" =>2,
            "风暴英雄" =>15,
            "魔兽世界" =>13,
        ];
        return $daleiGame[$game];
    }


    /**
     * 抓取视频网站数据
     * @return array
     * @throws \yii\db\Exception
     */
    public static function grabOrigin()
    {
        self::log("开始执行");
        //待添加，定期清除更新时间过老的视频
        $delData = date("Y-m-d H:i:s", time()-60*60*24);
        $delVideoId = StandardDataMatchVideoList::find()->select('id')->where(['<','modified_time',$delData])->asArray()->all();
        if (!empty($delVideoId)){
          foreach ($delVideoId as $kKey => $vval ){
              StandardDataMatchVideoListRelation::deleteAll(['video_id' => $vval]);
             }
            StandardDataMatchVideoList::deleteAll(['in','id',$delVideoId]);
         }


        $before = date("Y-m-d H:i:s", time()-3600);
        $nowDate = date("Y-m-d H:i:s",time()+480);
        $sqlCount="select count(*) as count from streams where `update_at` >= '{$before}' and  `update_at` <= '{$nowDate}' ";
        self::log("sqlCount as:".$sqlCount);
        $streamOrigin = DbStream::getDb()->createCommand(
            $sqlCount
            )
            ->queryOne()['count'];

        if (empty($streamOrigin)){
            self::log("没有数据，执行完成");
            return [];
        }
        $pageSize = 500;
        $totalPage = ceil($streamOrigin / $pageSize);
        $j = 0;
        for ($i = 0; $i < $totalPage; $i++) {
            $j++;
            $offset = ($j - 1) * $pageSize;
            $sqlList="select * from streams where `update_at` >= '{$before}'  and  `update_at` <= '{$nowDate}'  limit  {$offset},{$pageSize} ";
            self::log("sqlList as:".$sqlList);
            $data = DbStream::getDb()->createCommand(
                $sqlList
            )->queryAll();

            if (empty($data)) {
                self::log("没有数据了，跳出");
                break;
            }
            foreach ($data as $key => $val) {

                $streamInfo = StandardDataMatchVideoList::find()->where(['live_url' => $val['url']])->one();
                if (!$streamInfo){
                    $streamInfo = StandardDataMatchVideoList::find()->where(['streams_origin_id' => $val['id'], 'video_origin_id' => 9])->one();
                    if (!$streamInfo) {
                        $streamInfo = new StandardDataMatchVideoList();
                    }
                }
                $param['video_origin_id'] = 9;
                $param['game_id'] = (int)self::getGameByDaleiGame($val['gamename']);
                if (!empty($val['origin_id'])){
                    $param['origin_id'] = (int)$val['origin_id'];
                }
                $param['streamer'] = $val['streamer'];
                $param['platform_name'] = $val['platform'];
                $param['platform_id'] = (int)self::getPlatformIdbyName($param['platform_name'])['id'] ? (int)self::getPlatformIdbyName($param['platform_name'])['id'] : null;
                $param['title'] = $val['title'];
                $param['country'] = !empty($val['languages']) ? self::getcountrBylanguages($val['languages']) : null;
                $param['preview'] = $val['preview'];
                $param['live_url'] = $val['url'];
                $param['live_url_identity'] = md5($val['url']);
                if (empty($val['embed_url'])) {
                    $urlInfo = self::getInfoFromUrl($val['url']);
                    $param['live_embed_url'] = self::completionUrl($urlInfo['platform_name'],$urlInfo['room_id']);
                }
                if (empty($param['platform_name'])){
                    $param['platform_name'] = self::getInfoFromUrl($val['url'])['platform_name'];
                }
                if ($param['platform_name'] == "Twitch") {
                    $embedUrlPreg = preg_split('/&/', $val['embed_url'], -1, PREG_SPLIT_NO_EMPTY);
                    $param['live_embed_url'] = isset($embedUrlPreg[0]) ? $embedUrlPreg[0] : '';
                }else {
                    $param['live_embed_url'] = $val['embed_url'];
                }

                if (empty($val['url'])) {
                    $param['live_url'] = StreamService::transformationUrl($param['live_embed_url']);
                }
                $param['streams_origin_id'] = $val['id'];
                $val['viewers'] = str_replace(",","",$val['viewers']);
                if (!is_numeric($val['viewers']) && !empty($val['viewers'])) {
                    $back = mb_substr($val['viewers'], -1, 1);
                    if($back === ''){
                        $param['viewers'] = 0;
                    }else{
                        $front = strstr($val['viewers'], $back, true);
                        $param['viewers'] = self::num($back, $front);
                    }
                }else{
                    $param['viewers'] = (int)$val['viewers'];
                }

                if ($param['platform_name']=='Douyu' || $param['platform_id'] == '4') {
                    continue;
                }
                if (strpos($param['live_url'],'www.douyu.com') !== false || strpos($param['live_url'],'www.douyu.com') === 0) {
                    continue;
                }
                if (empty($param['platform_name'])){
                    continue;
                }

                $streamInfo->setAttributes($param);
                if (!$streamInfo->save()) {
//                    $a = $streamInfo->getErrors();
//                    var_dump($a);
//                    throw new BusinessException($streamInfo->getErrors(), "{$val['id']}");
                    continue;

                }

            }
        }
        self::log("执行完成");
    }

    public static function log($info)
    {
        print_r(sprintf("%s|%s\n",date("Y-m-dH:i:s"),$info));
    }

    public static function num($back, $front)
    {
        if($front){
            $num =0;
        }
        switch ($back) {
            case "万":
                $num = $front * 10000;
                break;
            case "W":
                $num = $front * 10000;
                break;
            case "k":
                $num = $front * 1000;
                break;
            case "千":
                $num = $front * 1000;
                break;
            default:
                $num = 0;
        }
        return (int)$num;

    }


    public static function grabHltvOrigin()
    {

        //   大于等于当前时间加12小时，小于当前时间  ands tatus = live   没10分钟跑一次
        //   大于等于当前时间，小于当前时间加12小时  ands tatus = live   没10分钟跑一次
        //
        $date = strtotime(date("Y-m-d"));
        $twelveHours = strtotime(date("Y-m-d H:i:s",time()+3600*12));
//        $date = "1599580770";
        $count = "select count(*) as count from match_all where `match_time` <= {$twelveHours} and `match_time` >= {$date} or `status` = 'LIVE'";
        $matchCount = DbStream::getDb()->createCommand($count)->queryOne()['count'];
        if (empty($matchCount)){
            return [];
        }
        $pageSize = 500;
        $totalPage = ceil($matchCount / $pageSize);
        $j = 0;
        for ($i = 0; $i < $totalPage; $i++) {
            $j++;
            $offset = ($j - 1) * $pageSize;
            $sql = "select
`l`.`streamer`,`l`.`id`,`l`.`embed_url`,`l`.`viewers`,`l`.`url`,`l`.`country`,`m`.`matche_id`
from `match_all` as `m`
left join `live_video` as `l` on l.matche_id = m.matche_id
where `match_time` >= {$date}  and `match_time` <= {$twelveHours} or `status` = 'LIVE' limit  {$offset},{$pageSize}";
            $hltvData = DbStream::getDb()->createCommand($sql)->queryAll();
            if (empty($hltvData)) {
                return '';
            }
            foreach ($hltvData as $key => $val) {
                // 如果存在，更新，获取video_id
// 如果不存在，则添加到待添加，并获取video_id
// (video_id,origin_id,rel_match_id)然后添加关联表(添加关联关系的时候，判断此关联关系是否已添加，如果添加不做操作)

              $success = Common::addStandardDataMatchVideo($val['url'],$val['embed_url'],false,7, 1, $val['matche_id'],Consts::ORIGIN_HLTV,$val);
                if (!$success) {
                    continue;
                }


//                $murl = md5($val['url']);
//                $where = ['live_url_identity' => $murl];
//                $streamInfo = StandardDataMatchVideoList::find()->where($where)->one();
//                if (!$streamInfo) {
//                    $streamInfo = new StandardDataMatchVideoList();
//                    $param['video_origin_id'] = 8;
//                }
//
//                $param['game_id'] = 1;
//                $param['viewers'] = $val['viewers'];
//                $param['streamer'] = $val['streamer'];
//                $param['country'] = (int)self::getcountryByName($val['country'])['id'];
//                $param['platform_name'] = self::getInfoFromUrl($val['url'])['platform_name'];
//                $param['platform_id'] = (int)self::getPlatformIdbyName($param['platform_name'])['id'] ? (int)self::getPlatformIdbyName($param['platform_name'])['id'] : null;
//                $param['match_rel_id'] = $val['matche_id'];
//                $param['live_url'] = $val['url'];
//                $param['live_url_identity'] = $murl;
//                if (empty($val['embed_url'])) {
//                    $urlInfo = self::getInfoFromUrl($val['url']);
//                    $param['live_embed_url'] = self::completionUrl($urlInfo['platform_name'],$urlInfo['room_id']);
//                }
//                if ($param['platform_name'] == "Twitch") {
//                    $embedUrlPreg = preg_split('/&/', $val['embed_url'], -1, PREG_SPLIT_NO_EMPTY);
//                    $param['live_embed_url'] = isset($embedUrlPreg[0]) ? $embedUrlPreg[0] : '';
//                }else {
//                    $param['live_embed_url'] = $val['embed_url'];
//                }
//                if (empty($param['live_url'])) {
//                    $param['live_url'] = StreamService::transformationUrl($param['live_embed_url']);
//                }
//                if (empty($val['embed_url']) && empty($val['url'])) {
//                    continue;
//                }
//                $param['streams_origin_id'] = "hltv-" . $val['id'];
//                $streamInfo->setAttributes($param);
//                // 调试错误的时候用的
//                if (!$streamInfo->save()) {
//                    //                    throw new BusinessException($streamInfo->getErrors(),"戴天添加失败");
//                    continue;
//                }
//                $videoId = $streamInfo['id'];
//
//                //(video_id,origin_id,rel_match_id)然后添加关联表(添加关联关系的时候，判断此关联关系是否已添加，如果添加不做操作)
//                $where = ['video_id' => $videoId, 'origin_id' => 7, 'match_rel_id' => $val['matche_id']];
//                $relation=StandardDataMatchVideoListRelation::find()->where($where)->one();
//                if(!$relation){
//                    $relation = new StandardDataMatchVideoListRelation();
//                }
//                $relation->setAttributes($where);
//                if (!$relation->save()) {
////                    $a = $relation->getErrors();
//                    continue;
//                }
//
//
//
//                //检查比赛关联关系：如果原始比赛id和主表比赛id有关联关系，
//                //则检查主表这场比赛是否存在这个视频url，
//                //如果没有，则添加条视频，如果有，结束
//
//                //todo 这里有多条
//                    $standardDataMatch= StandardDataMatch::find()->select(['id'])
//                        ->where(['rel_identity_id'=>$val['matche_id']])->asArray()->one();
//                $dataStandard = DataStandardMasterRelation::find()->select(['master_id'])
//                    ->where(['standard_id'=>$standardDataMatch['id'],'resource_type'=>'match'])->asArray()->one();
//                $match = Match::find()->where(['id'=>$dataStandard['master_id']])->asArray()->one();
//                if (empty($standardDataMatch) || empty($dataStandard) || empty($match)) {
////                      throw new BusinessException($streamInfo->getErrors(),"");
//                                    continue;
//                }
//
//                $MatchStreamList = MatchStreamList::find()->where(['live_url' => $val['url'],'match_id'=>$match['id']])->one();
//                if (empty($MatchStreamList)){
//
//
////                    $MatchStreamList = new MatchStreamList();
//                    $stream_list['streamer'] = $streamInfo['streamer'];
//                    $stream_list['viewers'] = $streamInfo['viewers'];
//                    $stream_list['match_id'] = $match['id'];
//                    $stream_list['video_origin_id'] = $streamInfo['video_origin_id'];
//                    $stream_list['live_url'] = $streamInfo['live_url'];
//                    $stream_list['live_embed_url'] = $streamInfo['live_embed_url'];
//                    $stream_list['country'] = $streamInfo['country'];
//                    $stream_list['platform_name'] = $streamInfo['platform_name'];
//                    $stream_list['platform_id'] = $streamInfo['platform_id'];
//                    $stream_list['video_id'] = $videoId;
//                    self::matchStreamListAdd($stream_list,$match['id']);
//
////                    $MatchStreamList->setAttributes($stream_list);
////                    if (!$MatchStreamList->save()) {
////                        continue;
//////                    throw new BusinessException($streamInfo->getErrors());
////                    }
//                }


            }

        }
        echo "执行完成";
    }


    public static function upSaveUrl()
    {
//        $getUrlStream = MatchStreamList::find()->select(['id', 'live_url'])->where(['flag' => 1]);
        $getUrlStream =  MatchStreamList::find()->alias('matchS')
            ->select(['matchS.id','matchS.match_id','matchS.live_url','re.status'])
            ->leftJoin('match_real_time_info as re','re.id = matchS.match_id')
            ->where(['flag' => 1,'re.status'=>1])->orWhere(['re.status'=>2]);


        $getUrlStreamData = $getUrlStream->asArray()->all();
        if (empty($getUrlStreamData)){
            return "没数据了1";
        }
        $url = array_filter(array_column($getUrlStreamData, 'live_url', 'id'));

        $standardDatadata = StandardDataMatchVideoList::find()->select(
            ['streamer', 'id','platform_name', 'platform_id', 'country', 'preview', 'live_embed_url', 'live_url', 'viewers', 'title','origin_id','video_origin_id'])
            ->where(['live_url' => $url]);
        $count = $standardDatadata->count();
        $pageSize = 500;
        $totalPage = ceil($count / $pageSize);

        $j = 0;
        for ($i = 0; $i < $totalPage; $i++) {
            $j++;
            $offset = ($j - 1) * $pageSize;
            $endData = $standardDatadata->offset($offset)->limit($pageSize)->asArray()->all();

            if (empty($endData)) {
                return "没数据了";
            }
            foreach ($endData as $key => $val) {

//                $streamInfo = MatchStreamList::find()->where(['live_url' => $val['live_url']])->one();
//                if (!$streamInfo) {
//                    $streamInfo = new MatchStreamList();
//
////                    $param['video_origin_id'] = (int)$val['video_origin_id'];
////                    if (!empty($val['origin_id'])) {
////                        $param['origin_id'] = (int)$val['origin_id'];
////                    }
//                }

                $param['streamer'] = $val['streamer'];
                $param['platform_name'] = $val['platform_name'];
                $param['title'] = $val['title'];
                $param['viewers'] = $val['viewers'];
                $param['platform_id'] = $val['platform_id'];
                $param['country'] = $val['country'];
                $param['preview'] = $val['preview'];
                $param['live_url'] = $val['live_url'];
                $param['live_embed_url'] = $val['live_embed_url'];
                $param['video_id'] = (int)$val['id'];
                $param = array_filter($param);
//                $streamInfo->setAttributes($param);

                if ($param['platform_name']=='Douyu' || $param['platform_id'] == '4') {
                    continue;
                }
                if (strpos($param['live_url'],'www.douyu.com') !== false || strpos($param['live_url'],'www.douyu.com') === 0) {
                    continue;
                }
                if (empty($param['platform_name'])){
                    continue;
                }

                MatchStreamList::updateAll($param,['live_url'=>$val['live_url']]);


//                if (!$streamInfo->save()) {
//                    throw new BusinessException($streamInfo->getErrors(), "{$val['id']}");
//                    continue;
//
//                }

            }
        }
        echo "执行完成";
    }



//    public static function upVideoUrl()
//    {
//
//        $teamName = Match::find()->alias('m')
//            ->select(['m.id as match_id','t.full_name','t.short_name','t.name','t2.full_name as t_full_name','t2.short_name as t_short_name','t2.name as t_name'])
//            ->where(['mi.status'=>2])
//            ->alias('m')
//            ->leftJoin('team as t','t.id = m.team_1_id')
//            ->leftJoin('team as t2','t2.id = m.team_2_id')
//            ->leftJoin('match_real_time_info as mi', 'mi.id = m.id')
//            ->asArray()->all();
//        foreach ($teamName as $key => $val) {
//
//            $matchVideo = StandardDataMatchVideoList::find()->alias('video')
//                ->select(['video.id as video_id','enum_origin.desc as origin_name',
//                    'video.streamer','video.title','video.room_num','video.country','video.platform_name',
//                    'video.platform_id','video.preview','video.viewers', 'video.match_rel_id','video.official_stream_url',
//                    'video.live_url','video.live_embed_url', 'video.origin_id','v.id as video_origin_id',
//                    'v.name as video_origin_name'
//                ])
//                ->leftJoin('enum_origin','enum_origin.id = video.origin_id')
//                ->leftJoin('enum_video_origin as v', 'v.id = video.video_origin_id')
//                ->andFilterWhere(['or',
//                    ['like','title',$val['full_name']],
//                    ['like','title',$val['short_name']],
//                    ['like','title',$val['name']],
//                    ['like','title',$val['t_full_name']],
//                    ['like','title',$val['t_short_name']],
//                    ['like','title',$val['t_name']]
//                ])->asArray()->all();
//            foreach ($matchVideo as $k => $v) {
//                $streamInfo=null;
//                $streamInfo = MatchStreamList::find()->where(['video_id'=>$v['video_id']])
//                    ->andWhere(['video_origin_id' => $v['video_origin_id']])
//                    ->andWhere(['origin_id' => $v['origin_id']])
//                    ->one();
//                if(!$streamInfo){
//                    $streamInfo = new MatchStreamList();
//
//                }
//                $data= [
//                    'origin_id' => $v['origin_id'] ?: '',
//                    'video_origin_id' => $v['video_origin_id'] ?: '',
//                    'streamer' => $v['streamer'],
//                    'title' => $v['title'],
//                    'country' => $v['country'],
//                    'viewers' => $v['viewers'],
//                    'match_id' => $val['match_id'],
//                    'video_id' => $v['video_id'],
//                    'live_url' => $v['live_url'],
//                    'live_embed_url' => $v['live_embed_url'],
//                    'room_id' =>  MatchService::getInfoFromURl($v['live_url'])['room_id'],
//                    'platform_name' => MatchService::getInfoFromUrl($v['live_url'])['platform_name'],
//                ];
//
//                $streamInfo->setAttributes($data);
//                if(!$streamInfo->save()){
//                    continue;
//
//                }
//            }
//
//
//        }
//
//    }

    /**
     * 通过平台名称获取id
     * @param $name
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getPlatformIdbyName($name){
        if (empty($name)) {
            return null;
        }
        $data = EnumStreamPlatform::find()->where(['name'=>$name])->asArray()->one();
        if (!empty($data)) {
            return $data;
        }
    }


    /**
     * 通过国家名称获取id
     * @param $name
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getcountryByName($name){
        $data['id'] = null;
        if (empty($name)) {
            return null;
        }
        $data = EnumCountry::find()->where(['hltv'=>$name])->asArray()->one();
        if (!empty($data)) {
            return $data;
        }
        return $data;
    }

    /**
     * 通过国家名称获取id
     * @param $name
     * @return array|int|\yii\db\ActiveRecord|null
     */
    public static function getcountrBylanguages($name){
        if (empty($name)) {
            return null;
        }
        $data = EnumCountry::find()->where(['languages'=>$name])->asArray()->one();
        if (!empty($data)) {
            return (int)$data['id'];
        }
    }
    /**
     * 根据直播地址获取平台
     * @param $url
     * @return array
     */
    public static function getInfoFromUrl($url)
    {
        $mp=[
            [
                'reg'=>'/^https\:\/\/www\.huya\.com\/(.*)$/',
                'type'=>'Huya'
            ],
            [
                'reg'=>'/^https\:\/\/www\.huomao\.com\/(.*)$/',
                'type'=>'Huomao'
            ],
            [
                'reg'=>'/^https\:\/\/www\.twitch\.tv\/(.*)$/',
                'type'=>'Twitch'
            ],
            [
                'reg'=>'/^http\:\/\/www\.twitch\.tv\/(.*)$/',
                'type'=>'Twitch'
            ],
            [
                'reg'=>'/^https\:\/\/live\.bilibili\.\com\/(.*)$/',
                'type'=>'BiliBili'
            ],
            [
                'reg'=>'/^https\:\/\/www\.douyu\.com\/(.*)$/',
                'type'=>'Douyu'
            ],
            [
                'reg'=>'/^https:\/\/www\.youtube\.com\/watch\?v\=(.*)$/',
                'type'=>'YouTube'
            ],
            [
                'reg'=>'/^http\:\/\/www\.afreecatv\.com\/(.*)$/',
                'type'=>'AfreecaTV'
            ],
            [
                'reg'=>'/^twitch\.tv\/(.*)$/',
                'type'=>'Twitch'
            ],
        ];
        $info=[
            'room_id'=>"",
            'platform_name'=>'',
            'url'=>$url,
        ];
        foreach($mp as $r){
            $mt=preg_match_all($r['reg'],$url,$matchAll);
            if($mt){
                $info['platform_name']=$r['type'];
                $info['room_id']=@$matchAll[1][0];
                break;
            }
        }
        return $info;
    }

    /**
     * 由直播平台加房间号转化嵌入代码
     * @param $type
     * @param $param
     * @return string
     */
    public static function completionUrl($type,$param)
    {
        $embed_url = '';
        switch ($type) {
            case "Huya":
                $embed_url = "http://liveshare.huya.com/iframe/".$param;
                break;
            case "Huomao":
                $embed_url = "https://www.huomao.com/outplayer/index/".$param;
                break;
            case "Bilibili":
                $embed_url = "https://s1.hdslb.com/bfs/static/blive/live-assets/player/flash/pageplayer-latest.swf?room_id=".$param."&cid=".$param."state=LIVE";
                break;
//            case "Douyu":
//                $embed_url = "https://www.douyu.com/".$param;
//                break;
            case "Twitch":
                $embed_url = "https://player.twitch.tv/?channel=".$param;
                break;
            case "YouTube":
                $embed_url = "https://www.youtube.com/embed/".$param;
                break;
            default:
                $embed_url = null;

        }
        return $embed_url;
    }

    public static function transformationUrl($url){
        if($url){
            if(strstr($url,'&state=LIVE')){
                $url = str_replace('d=','/',$url);
                $str = basename($url);

                // 最后的值
                $numStr = substr($str,0,strpos($str, '&'));
                $zUrl = 'https://live.bilibili.com/'.$numStr;
            }else{
                $arr = parse_url($url);
                if(isset($arr['path'])){
                    if(strstr($arr['path'],'=')){
                        $numStr = str_replace('=','/',$arr['path']);
                    }else{
                        $numStr = $arr['path'];
                    }
                }
                if(isset($arr['query'])){
                    if(strstr($arr['query'],'=')){
                        $numStr = str_replace('=','/',$arr['query']);
                    }
                }
                // 最后的值
                $numStrValue = basename($numStr);
                // 后面所有内容
                $endContent = substr($arr['host'],strpos($arr['host'],'.'));
                $zUrl = 'https://www'.$endContent.'/'.$numStrValue;
            }
        }else{
            $zUrl = '';
        }
        return $zUrl;
    }


    /**
     * @param null $liveUrl
     * @param null $getPlatformName
     * @param null $getEmbedUrl
     * @return array|false
     */
    public static function getLiveUrlInfo($liveUrl = null,$getPlatformName = null,$getEmbedUrl = null){
        if (empty($liveUrl) && empty($getEmbedUrl)){
            return false;
        }

        if (empty($liveUrl) && $getEmbedUrl) {
            $liveUrl = StreamService::transformationUrl($getEmbedUrl);
        }

        $platformInfo = StreamService::getInfoFromUrl($liveUrl);

        if (empty($getPlatformName)){
            $platformdata = StreamService::getPlatformIdbyName($platformInfo['platform_name']);
            $platformInfo['platformId'] = (int)$platformdata['id'] ?? null;
        }else{
            $platformdata = StreamService::getPlatformIdbyName($getPlatformName);
            $platformInfo['platformId'] = (int)$platformdata['id'] ?? null;
        }


        if (empty($getEmbedUrl)) {
            $liveEmbedUrl = StreamService::completionUrl($platformInfo['platform_name'], $platformInfo['room_id']);
            $platformInfo['liveEmbedUrl'] = $liveEmbedUrl;
        }else{
            $platformInfo['liveEmbedUrl'] = $getEmbedUrl;

        }

        return $platformInfo;
    }



    public static function onceAgainUp() {
        $endTime = date("Y-m-d H:i:s",time()+3600*12);
        $nowTime = date("Y-m-d H:i:s",time());
        //获取比赛的mathId
        $match = Match::find()->alias('m')
            ->select("m.id as match_id")
            ->leftJoin('match_real_time_info as t','t.id = m.id')
            ->where(['and',
                ['>=','scheduled_begin_at',$nowTime],
                ['<=','scheduled_begin_at',$endTime]
            ])->orWhere(['status' =>2])->asArray()->all();

        if (empty($match)){
             return;
        }

        $matchIds = array_column($match, 'match_id','match_id');
        $relation = DataStandardMasterRelation::find()->alias('rel')
            ->select([
                'rel.master_id','rel.standard_id',"std.rel_identity_id"
            ])
            ->leftJoin('standard_data_match as std','std.id = rel.standard_id')
            ->leftJoin('match','rel.master_id = match.id')
            ->where(['rel.master_id' =>$matchIds])
            ->andWhere(['rel.resource_type'=>'match'])
            ->asArray()->all();
        $rel_identity_ids = array_column($relation,'rel_identity_id','rel_identity_id');
        $matchidsByRel = array_column($relation,'master_id','rel_identity_id');
//
//        $isStandard_video = StandardDataMatchVideoList::find()
//            ->where(['match_rel_id'=>$rel_identity_ids])
//            ->asArray()->all();

        $relationVideoList = StandardDataMatchVideoListRelation::find()->alias('video_r')
            ->select(['video.*',
                'video_r.video_id as r_video_id',
                'video_r.origin_id as r_origin_id',
                'video_r.match_rel_id as r_match_rel_id',
            ])
            ->leftJoin('standard_data_match_video_list as video','video.id = video_r.video_id')
            ->where(['video_r.match_rel_id' =>$rel_identity_ids])
            ->asArray()->all();

        if (empty($relationVideoList)) return [];
        foreach ($relationVideoList as $key => $val) {
            if(isset($matchidsByRel[$val['match_rel_id']])) {
                $match_rel_id = $matchidsByRel[$val['match_rel_id']];
            }else{
                continue;
            }


            $param['streamer'] = $val['streamer'];
            $param['platform_name'] = $val['platform_name'];
            $param['title'] = $val['title'];
            $param['viewers'] = $val['viewers'];
            $param['platform_id'] = (int)$val['platform_id'];
            $param['country'] = $val['country'];
            $param['match_id'] = (int)$matchidsByRel[$val['match_rel_id']];
            $param['preview'] = $val['preview'];
            $param['live_url'] = $val['live_url'];
            $param['live_embed_url'] = $val['live_embed_url'];
            $param['video_id'] = (int)$val['id'];

            if ($param['platform_name']=='Douyu' || $param['platform_id'] == '4') {
                continue;
            }
            if (strpos($param['live_url'],'www.douyu.com') !== false || strpos($param['live_url'],'www.douyu.com') === 0) {
                continue;
            }
            if (empty($param['platform_name'])){
                continue;
            }

            self::matchStreamListAdd($param,$match_rel_id,'select',null,$val['origin_id'],$val['video_origin_id']);


//            $streamInfo = MatchStreamList::find(),
//                ->where(['live_url' => $val['live_url'],'match_id'=>$match_rel_id])->one();
//
//            if (!$streamInfo) {
//                $streamInfo = new MatchStreamList();
//                $param['video_origin_id'] = (int)$val['video_origin_id'];
//                if (!empty($val['origin_id'])) {
//                    $param['origin_id'] = (int)$val['origin_id'];
//                }
//            }
//            $param['streamer'] = $val['streamer'];
//            $param['platform_name'] = $val['platform_name'];
//            $param['title'] = $val['title'];
//            $param['viewers'] = $val['viewers'];
//            $param['platform_id'] = (int)$val['platform_id'];
//            $param['country'] = $val['country'];
//            $param['match_id'] = (int)$matchidsByRel[$val['match_rel_id']];
//            $param['preview'] = $val['preview'];
//            $param['live_url'] = $val['live_url'];
//            $param['live_embed_url'] = $val['live_embed_url'];
//            $param['video_id'] = (int)$val['id'];
//            $streamInfo->setAttributes($param);
//            if (!$streamInfo->save()) {
////                throw new BusinessException($streamInfo->getErrors());
//                continue;
//
//            }
        }
        echo "执行完成";

    }

    public static function enumVideoOrigin() {
        $data =EnumVideoOrigin::find()->asArray()->all();
        return $data;
    }

    public static function streamPlatform() {
        $data = EnumStreamPlatform::find()->asArray()->all();
        return $data;
    }

    /**
     * 向已添加表添加数据
     * @param $stream_list
     * @param $match_id
     * @param $newOrigin_id
     * @throws BusinessException
     */
    public static function matchStreamListAdd($stream_list,$match_id,$type='',$MatchStreamList=null,$origin_id='',$video_origin_id=null){

        if ($type == 'select'){
            $MatchStreamList = MatchStreamList::find()
                ->where(['live_url' => $stream_list['live_url'],'match_id'=>$match_id])->one();
        }
        if (empty($MatchStreamList)) {
            $MatchStreamList = new MatchStreamList();
            if (!empty($video_origin_id)){
                $stream_list['video_origin_id'] = (int)$video_origin_id;

            }
            if (!empty($origin_id)) {
                $stream_list['origin_id'] = (int)$origin_id;
            }

        }

        $stream_list['live_url_identity'] = md5($stream_list['live_url']);
        $MatchStreamList->setAttributes($stream_list);
        if (!$MatchStreamList->save()) {
           $errorMsg =  json_encode($MatchStreamList->getErrors(),JSON_UNESCAPED_UNICODE);
            throw new BusinessException($MatchStreamList->getErrors(), "保存视频流失败{$errorMsg}");

        }
    }


}