<?php
/**
 *
 */

namespace app\modules\match\services\battle;


interface BattleInterface
{
    public static function getBattleInfo($battleId,$dataType='',$adminType=1,$adminId=0);
    public static function setBattleInfo($battleId,$data,$dataType='',$adminType=1,$adminId=0);
    public static function addBattleInfo($matchId,$data,$dataType='',$adminType=1,$adminId=0);
    public static function getBattleListByMatchId($matchId,$adminType=1,$adminId=0);
    public static function deleteBattle($battleId,$adminType=1,$adminId=0,$delType = 'special',$isSendMsg='');
    public static function recoveryBattle($battleId,$dataType='',$adminType=1,$adminId=0);
}