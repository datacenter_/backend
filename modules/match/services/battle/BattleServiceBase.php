<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services\battle;


use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleEventDota2;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattlePlayerDota2;
use app\modules\match\models\MatchBattleTeam;

use app\modules\match\models\MatchBattleTeamExtDota2;
use app\modules\match\models\MatchBattleTeamExtLol;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchBattlePlayerLol;

use app\modules\match\models\MatchBattleTeamExtCsgo;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;

use app\modules\match\services\MatchService;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

class BattleServiceBase
{
    const GAME_STATUS_CSGO = 1;
    const GAME_STATUS_LOL = 2;
    const GAME_STATUS_DOTA2 = 3;
    public static function deleteBattle($battleId,$adminType=1,$adminId=0,$delType = 'special',$isSendMsg='')
    {
        // 删除battle
        $battle = MatchBattle::find()->where(['id' => $battleId])->one();
        $logOldbattle = $battle->toArray();
        if ($delType=='special'){
            //判断是不是默认领先和单图弃权
            if ($battle->is_default_advantage == 1){
                return true;
            }
            if ($battle->is_forfeit == 1){
                return true;
            }
        }
        $match = Match::find()->where(['id' => $battle['match']])->one();
        // 游戏ID
        $gameId = $match['game'];
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // 删除CSGO battle_ext
                $MatchBattleExtCsgo = MatchBattleExtCsgo::findOne(['id' => $battleId]);
                if($MatchBattleExtCsgo){
                    $MatchBattleExtCsgo->setAttributes([
                        'is_confirmed'=>null,
                        'is_finished'=>null,
                        'is_pause'=>null,
                        'is_live'=>null,
                        'current_round'=>null,
                        'is_bomb_planted'=>null,
                        'win_round_1_side'=>null,
                        'win_round_1_team'=>null,
                        'win_round_1_detail'=>null,
                        'win_round_16_side'=>null,
                        'win_round_16_team'=>null,
                        'win_round_16_detail'=>null,
                        'first_to_5_rounds_wins_side'=>null,
                        'first_to_5_rounds_wins_team'=>null,
                        'first_to_5_rounds_wins_detail'=>null,
                        'live_rounds'=>null,
                    ]);
                    $MatchBattleExtCsgo->save();
                }
                // 删除CSGO battlePlayers
                MatchBattlePlayerCsgo::deleteAll(['battle_id' => $battleId]);
                // 删除CSGO round
                MatchBattleRoundCsgo::deleteAll(['battle_id' => $battleId]);
                // 删除CSGO round_event
                MatchBattleRoundEventCsgo::deleteAll(['battle_id' => $battleId]);
                // 删除CSGO round_players
                MatchBattleRoundPlayerCsgo::deleteAll(['battle_id' => $battleId]);
                // 删除CSGO round_side
                MatchBattleRoundSideCsgo::deleteAll(['battle_id' => $battleId]);
                break;
            case self::GAME_STATUS_LOL:
                //删除LOL event
                MatchBattleEventLol::deleteAll(['battle_id' => $battleId]);
                // 删除LOL battle_ext
                $MatchBattleExtLol = MatchBattleExtLol::findOne(['id' => $battleId]);
                if($MatchBattleExtLol){
                    $MatchBattleExtLol->setAttributes([
                        'is_confirmed'=>null,
                        'is_pause'=>null,
                        'is_live'=>null,
                        'is_finished'=>null,
                        'ban_pick'=>null,
                        'first_blood_p_tid'=>null,
                        'first_blood_time'=>null,
                        'first_to_5_kills_p_tid'=>null,
                        'first_to_5_kills_time'=>null,
                        'first_to_10_kills_p_tid'=>null,
                        'first_to_10_kills_time'=>null,
                        'first_turret_p_tid'=>null,
                        'first_turret_time'=>null,
                        'first_inhibitor_p_tid'=>null,
                        'first_inhibitor_time'=>null,
                        'first_rift_herald_p_tid'=>null,
                        'first_rift_herald_time'=>null,
                        'first_dragon_p_tid'=>null,
                        'first_dragon_time'=>null,
                        'first_baron_nashor_p_tid'=>null,
                        'first_baron_nashor_time'=>null,
                        'first_elder_dragon_p_tid'=>null,
                        'first_elder_dragon_time'=>null,
                        'gold_diff_timeline'=>null,
                        'experience_diff_timeline'=>null,
                        'faction'=>null,
                        'first_blood_detail'=>null,
                        'first_to_5_detail'=>null,
                        'first_to_10_detail'=>null,
                        'first_turret_detail'=>null,
                        'first_inhibitor_detail'=>null,
                        'first_rift_herald_detail'=>null,
                        'first_dragon_detail'=>null,
                        'first_baron_nashor_detail'=>null,
                        'first_elder_dragon_detail'=>null,
                        'elites_status'=>null
                    ]);
                    $MatchBattleExtLol->save();
                }
                // 删除LOL battlePlayers
                MatchBattlePlayerLol::deleteAll(['battle_id' => $battleId]);
                break;
            case self::GAME_STATUS_DOTA2:
                //删除LOL event
                MatchBattleEventDota2::deleteAll(['battle_id' => $battleId]);
                // 删除DOTA2 battle_ext
                $MatchBattleExtDota2 = MatchBattleExtDota2::findOne(['id' => $battleId]);
                if($MatchBattleExtDota2){
                    $MatchBattleExtDota2->setAttributes([
                        'is_confirmed'=>null,
                        'is_pause'=>null,
                        'is_live'=>null,
                        'is_finished'=>null,
                        'ban_pick'=>null,
                        'first_blood_p_tid'=>null,
                        'first_blood_time'=>null,
                        'first_to_5_kills_p_tid'=>null,
                        'first_to_5_kills_time'=>null,
                        'first_to_10_kills_p_tid'=>null,
                        'first_to_10_kills_time'=>null,
                        'first_tower_p_tid'=>null,
                        'first_tower_time'=>null,
                        'first_barracks_p_tid'=>null,
                        'first_barracks_time'=>null,
                        'first_roshan_p_tid'=>null,
                        'first_roshan_time'=>null,
                        'net_worth_diff_timeline'=>null,
                        'experience_diff_timeline'=>null,
                        'time_of_day'=>null,
                        'first_blood_details'=>null,
                        'first_to_5_kills_details'=>null,
                        'first_to_10_kills'=>null,
                        'first_tower'=>null,
                        'first_barracks'=>null,
                        'first_roshan'=>null,
                        'elites_status'=>null
                    ]);
                    $MatchBattleExtDota2->save();
                }
                // 删除DOTA2battlePlayers
                MatchBattlePlayerDota2::deleteAll(['battle_id' => $battleId]);
                break;
            default:
                break;
        }
        // 查询team信息
        $battleTeam = MatchBattleTeam::find()->where(['battle_id' => $battleId])->asArray()->all();
        foreach ($battleTeam as $k=>$val){
            //更新team比分
            MatchBattleTeam::updateAll(['score'=>null,'identity_id' => null,'rel_identity_id' => null,'team_id'=>null],['id'=>$val['id']]);
            // lol删除teamExt
            if($gameId == self::GAME_STATUS_LOL) {
                $MatchBattleTeamExtLol = MatchBattleTeamExtLol::findOne(['id' => $val['id']]);
                if($MatchBattleTeamExtLol) $MatchBattleTeamExtLol->delete();
            }
            // csgo删除teamExt
            if($gameId == self::GAME_STATUS_CSGO){
                $MatchBattleTeamExtCsgo = MatchBattleTeamExtCsgo::findOne(['id' => $val['id']]);
                if($MatchBattleTeamExtCsgo) $MatchBattleTeamExtCsgo->delete();
            }
            // dota2 删除teamExt
            if($gameId == self::GAME_STATUS_DOTA2){
                $MatchBattleTeamExtDota2 = MatchBattleTeamExtDota2::findOne(['id' => $val['id']]);
                if($MatchBattleTeamExtDota2) $MatchBattleTeamExtDota2->delete();
            }
        }
        // 删除公用team 因为LOL 不能删除
//        MatchBattleTeam::deleteAll(['battle_id' => $battleId]);
        //删除battle的关联关系表
        MatchBattleRelation::deleteAll(['match_id' => $battle['match'],'order' => $battle['order']]);
//        if(!$battle->delete()){
//            throw new BusinessException($battle->getErrors(),'删除失败');
//        }
        //清空battle
        $battlesInfo = [
            'begin_at'=>null,
            'end_at'=>null,
            'status'=>null,
            'is_draw'=>null,
            'is_forfeit'=>null,
            'is_default_advantage'=>null,
            'map'=>null,
            'duration'=>null,
            'winner'=>null,
            'is_battle_detailed'=>null,
            'modified_at'=>null,
            'deleted_at'=>date("Y-m-d H:i:s"),
            'deleted'=>1
        ];
        $battle->setAttributes($battlesInfo);
        if(!$battle->save()){
            throw new BusinessException($battle->getErrors(),'删除失败');
        }
        if(empty($isSendMsg)){
            //加taskINfo renwu
            $logNewbattle = $battle->toArray();
            $filter = array_keys($logNewbattle);
            $filter = array_diff($filter, ["created_at", "modified_at"]);
            $diffInfo = Common::getDiffInfo($logOldbattle, $logNewbattle, $filter);
            if ($diffInfo['changed']) {
                    OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
                        Consts::RESOURCE_TYPE_BATTLE,
                        $logNewbattle['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNewbattle],
                        0,
                        '',
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        OperationLogService::getLoginUserId()
                    );
            }
        }

        return true;
    }

    public static function setBaseInfo($battleId, $data, $dataType, $adminType = 1, $adminId = 0)
    {
        $baseInfo = $data;
//        $id = $baseInfo['id'];
        if(isset($baseInfo['duration'])) $baseInfo['duration']=strval($baseInfo['duration']);
        $info = MatchBattle::find()->where(['id' => $battleId])->one();
//        $match_id = $info['match'];
        $logOldBattle = $info->toArray();
        if(!$info){
            throw new BusinessException([],'没有找到对应的对局信息');
        }
        $info->setAttributes($baseInfo);
        if (!$info->save()) {
//            throw new BusinessException($info->getFirstErrors(), "保存失败");
            throw new BusinessException($info->getErrors(), "保存失败");
        }
        $logNewBattle = $info->toArray();

        // 记录log
        //对局内容变更：按钮触发
        $filter = array_keys($logNewBattle);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($logOldBattle, $logNewBattle, $filter);

        if(isset($data['team_1_score'])){
            $battle = MatchBattleTeam::find()->where(['battle_id'=>$battleId,'order'=>1])->one();
            $oldteam_1_score = $battle->toArray();
            $battle->setAttribute('score',$data['team_1_score']);
            if(!$battle->save()){
                throw new BusinessException($battle->getErrors(),'保存score1失败');
            }
            $newteam_1_score = $battle->toArray();
            $team_1_score_filter = array_keys($newteam_1_score);
            $team_1_filter = array_diff($team_1_score_filter, ["created_at", "modified_at"]);
            $team_1_score_diffInfo = Common::getDiffInfo($oldteam_1_score, $newteam_1_score, $team_1_filter);

        }
        if(isset($data['team_2_score'])){
            $battle = MatchBattleTeam::find()->where(['battle_id'=>$battleId,'order'=>2])->one();

            $oldteam_2_score = $battle->toArray();
            $battle->setAttribute('score',$data['team_2_score']);
            if(!$battle->save()){
                throw new BusinessException($battle->getErrors(),'保存score2失败');
            }
            $newteam_2_score = $battle->toArray();
            $team_2_score_filter = array_keys($newteam_2_score);
            $team_2_filter = array_diff($team_2_score_filter, ["created_at", "modified_at"]);
            $team_2_score_diffInfo = Common::getDiffInfo($oldteam_2_score, $newteam_2_score, $team_2_filter);

        }
        //单图弃权
        if ($data['is_forfeit'] == 1){
            $battle_id_num = -1;
            //第一步需不需要更新大比分
            //第二部修改redis
            $redis_name = Consts::MARCH_BATTLE_IS_FORFEIT.':'.$data['match'].":".$data['id'];
            $Redis = \Yii::$app->get('redis');
            $Redis->select(env('REDIS_DATABASE'));
            $Redis->set($redis_name,1);
            $Redis->expire($redis_name,Consts::MARCH_BATTLE_REDIS_EXPIRE_TIME);//5天有效期
            //查询有没有当前的battle ID ，没有就加一个占位
            $order_id = $data['order'];
            $match_id = $data['match'];
            $redis_match_battles = Consts::MARCH_BATTLE_INFO.':'.$match_id;
            $match_battles = $Redis->hvals($redis_match_battles);
            if ($match_battles){
                foreach ($match_battles as $key => $item){
                    $battle_res_explode = explode('-',$item);
                    $battle_id_val = $battle_res_explode[0];
                    if ($battleId == $battle_id_val){
                        $battle_id_num = $key;
                    }
                }
            }
            if ($battle_id_num >= 0){
                $battle_list_keys = $Redis->hkeys($redis_match_battles);
                $battle_num_val = $battle_list_keys[$battle_id_num];
                if ($battle_num_val){
                    $Redis->hdel($redis_match_battles,$battle_num_val);
                }
                $Redis->hset($redis_match_battles,'forfeit_'.$order_id,$battleId.'-'.$order_id);
            }else{
                $Redis->hset($redis_match_battles,'forfeit_'.$order_id,$battleId.'-'.$order_id);
            }
            $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$match_id])->andWhere(['order'=>$order_id])->one();
            if (!$matchBattleRelation){
                //首先插入关系表
                $matchBattleRelation = new MatchBattleRelation();
                $matchBattleRelation->setAttributes([
                    'match_id' => (int)$match_id,
                    'order' => $order_id,
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                $matchBattleRelation->save();
            }else{
                $matchBattleRelation->setAttributes([
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                $matchBattleRelation->save();
            }
        }
        //默认领先
        if ($data['is_default_advantage'] == 1){
            $battle_id_num = -1;
            $order_id = $data['order'];
            $match_id = $data['match'];
            $Redis = \Yii::$app->get('redis');
            $Redis->select(env('REDIS_DATABASE'));
            $redis_match_battles = Consts::MARCH_BATTLE_INFO.':'.$match_id;
            $match_battles = $Redis->hvals($redis_match_battles);
            if ($match_battles){
                foreach ($match_battles as $key => $item){
                    $battle_res_explode = explode('-',$item);
                    $battle_id_val = $battle_res_explode[0];
                    if ($battleId == $battle_id_val){
                        $battle_id_num = $key;
                    }
                }
            }
            if ($battle_id_num >= 0){
                $battle_list_keys = $Redis->hkeys($redis_match_battles);
                $battle_num_val = $battle_list_keys[$battle_id_num];
                if ($battle_num_val){
                    $Redis->hdel($redis_match_battles,$battle_num_val);
                }
                $Redis->hset($redis_match_battles,'advantage_'.$order_id,$battleId.'-'.$order_id);
            }else{
                $Redis->hset($redis_match_battles,'advantage_'.$order_id,$battleId.'-'.$order_id);
            }
            $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$match_id])->andWhere(['order'=>1])->one();
            if (!$matchBattleRelation){
                //首先插入关系表
                $matchBattleRelation = new MatchBattleRelation();
                $matchBattleRelation->setAttributes([
                    'match_id' => (int)$match_id,
                    'order' => 1,
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                if (!$matchBattleRelation->save()) {
                    throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
                }
            }else{
                $matchBattleRelation->setAttributes([
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                $matchBattleRelation->save();
            }
        }

        if ($diffInfo['changed']) {
            $fil = ['begin_at','end_at','status','winner','is_draw','is_default_advantage','is_forfeit','is_battle_detailed','deleted'];
            $key = array_keys($diffInfo['diff']);
            $intersectVal = array_intersect($fil,$key);
        }

        if (!empty($intersectVal) || isset($team_1_score_diffInfo['diff']['score']) || isset($team_2_score_diffInfo['diff']['score'])) {
            $operationType = QueueServer::QUEUE_TYPE_UPDATE;

            if (isset($diffInfo['diff']['deleted'])
                && $diffInfo['diff']['deleted']['before'] == '1'
                && $diffInfo['diff']['deleted']['after'] == '2') {
                $operationType = QueueServer::CHANGE_TYPE_RECOVERY;
            }else if (isset($diffInfo['diff']['deleted'])
                && $diffInfo['diff']['deleted']['before'] == '2'
                && $diffInfo['diff']['deleted']['after'] == '1'){
                $operationType = QueueServer::CHANGE_TYPE_DELETE;
            }


            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_BATTLE,
                $logNewBattle['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewBattle],
                0,
                'battle_status',
                null,
                0,
                $adminType,
                $adminId
            );
        }

        return;
    }

    public static function getBaseInfo($battleId, $dataType, $adminType = 1, $adminId = 0)
    {
        $baseInfo = MatchBattle::find()->where(['id' => $battleId])->asArray()->one();
        return $baseInfo;
    }

    public static function addBattleInfo($matchId, $data, $dataType='', $adminType = 1, $adminId = 0)
    {
        // 每个battle有对局双方，从match获取
        $matchInfo=MatchService::getMergeMatch($matchId);
        $teamId1=$matchInfo['team_1_id'];
        $teamId2=$matchInfo['team_2_id'];
        // 初始化对战战队
        $baseInfo = new MatchBattle();
        $baseInfo->setAttribute('match',$matchId);
        $baseInfo->setAttribute('order',$data['order']);
        $baseInfo->setAttribute('game',(int)$matchInfo['game']);
        if($data){
            $baseInfo->setAttributes($data);
        }
        if (!$baseInfo->save()) {
            throw new BusinessException($baseInfo->getErrors(), "添加battle失败");
        }
        // 初始化主队，客队
        $mt1=new MatchBattleTeam();
        $mt1->setAttributes(
            [
                'battle_id'=>$baseInfo->id,
                'order'=>1,
                'team_id'=>$teamId1,
            ]
        );
        if (!$mt1->save()) {
            throw new BusinessException($mt1->getErrors(), "添加battle失败");
        }
        $teamExtId1 = $mt1->id;

        $mt2=new MatchBattleTeam();
        $mt2->setAttributes(
            [
                'battle_id'=>$baseInfo->id,
                'order'=>2,
                'team_id'=>$teamId2,
            ]
        );
        if (!$mt2->save()) {
            throw new BusinessException($mt2->getErrors(), "添加battle失败");
        }
        $teamExtId2 = $mt2->id;
        //判断是哪个游戏
        if ($matchInfo['game'] == 2 && isset($matchInfo['game'])){
            $mbtel1 = new MatchBattleTeamExtLol();
            $mbtel1->setAttribute('id',$teamExtId1);
            $mbtel1->save();
            $mbtel2 = new MatchBattleTeamExtLol();
            $mbtel2->setAttribute('id',$teamExtId2);
            $mbtel2->save();
        }
        if ($matchInfo['game'] == 3 && isset($matchInfo['game'])){
            $mbtel1 = new MatchBattleTeamExtDota2();
            $mbtel1->setAttribute('id',$teamExtId1);
            $mbtel1->save();
            $mbtel2 = new MatchBattleTeamExtDota2();
            $mbtel2->setAttribute('id',$teamExtId2);
            $mbtel2->save();
        }
        $logOldbattle = [];
        $logNewbattle = $baseInfo->toArray();
        $filter = array_keys($logNewbattle);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($logOldbattle, $logNewbattle, $filter);
        if ($diffInfo['changed']) {

            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$logNewbattle['match']]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            OperationLogService::addLog(QueueServer::QUEUE_TYPE_ADD,
                Consts::RESOURCE_TYPE_BATTLE,
                $logNewbattle['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewbattle],
                0,
                '',
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                OperationLogService::getLoginUserId()
            );
        }

        return $baseInfo->toArray();
    }

    public static function setBattleTeam($battleId,$teamList)
    {
        // 设置对局的team，编号1主队2客队
        foreach($teamList as $team){
            $t=new MatchBattleTeam();
            $t->setAttributes($team);
            if(!$t->save()){
                throw new BusinessException($t->getErrors(),'设置队伍失败');
            }
        }
    }

    public static function getBattleIdsByMatchId($matchId)
    {
        //,'flag'=>1
        $battleIds = MatchBattle::find()->select('id')->where(['match' => $matchId])->orderBy('order')->asArray()->all();
        $ids = array_column($battleIds, 'id');
        return $ids;
    }

    /**
     * Ws接口中通过matchId和order获取battleId
     * @param $matchId
     * @return array
     */
    public static function getBattleByMatchIdOnWs($matchId,$order)
    {
        return MatchBattle::find()->where(['match' => $matchId,'order'=>$order])->orderBy('order')->asArray()->one();
    }
    //恢复battle
    public static function recoveryBattle($battleId, $dataType='', $adminType = 1, $adminId = 0)
    {
        $baseInfo = MatchBattle::find()->where(['id' => $battleId])->one();
        $logOldbattle = $baseInfo->toArray();
        if ($baseInfo){
            $map = [
                'deleted'=>2,
                'deleted_at'=>null,
                'status'=>1,
                'is_draw'=>2,
                'is_forfeit'=>2,
                'is_default_advantage'=>2,
                'is_battle_detailed'=>2,
            ];
            $baseInfo->setAttributes($map);
            $baseInfo->save();
        }

        $logNewbattle = $baseInfo->toArray();
        $filter = array_keys($logNewbattle);
        $filter = array_diff($filter, ["created_at", "modified_at"]);
        $diffInfo = Common::getDiffInfo($logOldbattle, $logNewbattle, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
                Consts::RESOURCE_TYPE_BATTLE,
                $logNewbattle['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewbattle],
                0,
                '',
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                OperationLogService::getLoginUserId()
            );
        }
        return $baseInfo;
    }
}