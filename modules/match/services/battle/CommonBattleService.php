<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services\battle;


use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtLol;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\rest\exceptions\BusinessException;

class CommonBattleService extends BattleServiceBase implements BattleInterface
{
    public static function getBattleInfo($battleId, $dataType = '', $adminType = 1, $adminId = 0)
    {
        return [
            'base' => self::getBaseInfo($battleId, $dataType, $adminType, $adminId),
            BattleService::DATA_TYPE_STATICS => self::getStatics($battleId)
        ];
    }

    public static function setBattleInfo($battleId, $data, $dataType = '', $adminType = 1, $adminId = 0)
    {
        switch ($dataType) {
            case BattleService::DATA_TYPE_BASE:
                self::setBaseInfo($battleId, $data, $dataType, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_STATICS:
                self::setStatics($battleId, $data, $adminType, $adminId);
                break;
        }
    }

    public static function getBattleListByMatchId($matchId, $adminType = 1, $adminId = 0)
    {
        $battleIds = self::getBattleIdsByMatchId($matchId);
        $info = [];
        foreach ($battleIds as $id) {
            $info[] = self::getBattleInfo($id);
        }
        return $info;
    }

    public static function addBattleInfo($matchId, $data, $dataType = "", $adminType = 1, $adminId = 0)
    {
        $baseInfo = parent::addBattleInfo($matchId, $data, $dataType, $adminType, $adminId);
        // 初始化扩展表
        $battleId = $baseInfo["id"];
        $MatchBattle = MatchBattle::find()->where(['id'=>$battleId])->one();
        //获取地图
        $map_id = MatchService::getMapLists($MatchBattle['game']);
        $matchBattleDefault = [
            'status' => 1,//对局状态：未开始
            'game' => $MatchBattle['game'],//游戏ID
            'is_default_advantage' => 2,//默认领先获胜：否
            'is_forfeit' => 2,//是否弃权：否
            'is_battle_detailed' => 2,//是否有对局详情数据：否
            'is_draw' => 2,//是否平局：否
            'map'=> $map_id ? (int)$map_id : null //比赛地图：读取地图码表的默认地图
        ];
        $MatchBattle->setAttributes($matchBattleDefault);
        if (!$MatchBattle->save()) {
            throw new BusinessException($MatchBattle->getErrors(), '保存battle默认数据失败');
        }
        return self::getBattleInfo($battleId);
    }
    public static function setStatics($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置统计数据,统计数据在战队信息中
        // 如果没有id，只有order，我们根据order获取id
        foreach ($data as $key => $val) {
            if (isset($val['id']) && $val['id']) { //编辑
                $matchBattleTeam = MatchBattleTeam::find()->where(['id' => $val['id']])->one();
                if($matchBattleTeam){
                    $matchBattleTeam->setAttributes($val);
                }else{
                    throw new BusinessException($data, '参数错误，找不到对应的对象');
                }
            }elseif (!isset($val['id']) && (isset($val['order']) && $val['order'])) {
                $matchBattleTeam = MatchBattleTeam::find()->where([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
                    'rel_identity_id' => @$val['rel_identity_id']
                ])->one();
                if (!$matchBattleTeam) {
                    $matchBattleTeam = new MatchBattleTeam();
                }
                $matchBattleTeam->setAttributes([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
                    'rel_identity_id' => (string)$val['rel_identity_id'],
                    'score' => $val['score'],
                    'team_id' => $val['team_id']
                ]);
            }else{
                throw new BusinessException($data, '参数错误，找不到对应的对象');
            }
            if (!$matchBattleTeam->save()) {
                throw new BusinessException($matchBattleTeam->getErrors(), '保存失败');
            }
        }
    }
    public static function getStatics($battleId)
    {
        $q = MatchBattleTeam::find()->where(['battle_id' => $battleId]);
        $list = $q->asArray()->all();
        return $list;
    }

}