<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services\battle;

use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeamExtCsgo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Player;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

class CsgoBattleService extends BattleServiceBase implements BattleInterface
{
    public static function getBattleInfo($battleId, $dataType = '', $adminType = 1, $adminId = 0)
    {
        return [
            BattleService::DATA_TYPE_BASE => self::getBaseInfo($battleId, $dataType, $adminType, $adminId),
//            BattleService::DATA_TYPE_BAN_PICK => self::getBanPick($battleId),
            BattleService::DATA_TYPE_DETAILS => self::getDetails($battleId),
            BattleService::DATA_TYPE_STATICS => self::getStatics($battleId),
            BattleService::DATA_TYPE_ROUNDS => self::getRounds($battleId),
        ];
    }

    public static function setBattleInfo($battleId, $data, $dataType = '', $adminType = 1, $adminId = 0)
    {
        switch ($dataType) {
            case BattleService::DATA_TYPE_BASE:
                self::setBaseInfo($battleId, $data, $dataType, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_ROUND_PLAYERS:
                self::setRoundPlayers($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_DETAILS:
                self::setDetails($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_STATICS:
                self::setStatics($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_PLAYERS:
                self::setPlayers($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_ROUNDS:
                self::setRounds($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_EVENTS:
                self::setEvents($battleId, $data, $adminType, $adminId);
        }
    }
    public static function setRounds($battleId, $data, $adminType = 1, $adminId = 0)
    {
        $matchId = MatchBattle::find()->select(['match'])->where(['id' => $battleId])->one();
        foreach ($data as $key => $val) {
            $MatchBattleRoundCsgo = MatchBattleRoundCsgo::find()->where(['battle_id' => $battleId, 'round_ordinal' => $val['round_ordinal']])->one();
//            $Round = [
//                'battle_id' => $battleId,
//                'round_ordinal' => $val['round_ordinal'],
//                'winner_team_id' => $val['winner_team_id'],
//                'winner_end_type' => $val['winner_end_type'],
//            ];
            $Round = $val;
            $Round['battle_id'] = $battleId; // 这样支持部分更新的效果
            if (!$MatchBattleRoundCsgo) {
                // 加入round表
                $MatchBattleRoundCsgo = new MatchBattleRoundCsgo();
            }
            $MatchBattleRoundCsgo->setAttributes($Round);
            $MatchBattleRoundCsgo->save();
            if (isset($val['side']) && !empty($val['side'])) {
                foreach ($val['side'] as $k => $v) {
                    if (!empty($v['side_order'])&&!empty($v['round_ordinal'])){
                        $MatchBattleRoundSideCsgo = MatchBattleRoundSideCsgo::find()->where(['battle_id' => $battleId, 'round_ordinal' => $v['round_ordinal'], 'side_order' => $v['side_order']])->one();
                        $v['battle_id'] = $battleId;
                        $v['match'] = $matchId['match'];
                        if (!$MatchBattleRoundSideCsgo) {
                            $MatchBattleRoundSideCsgo = new MatchBattleRoundSideCsgo();
                        }
                        $MatchBattleRoundSideCsgo->setAttributes($v);
                        if (!$MatchBattleRoundSideCsgo->save()){
                            throw new BusinessException($MatchBattleRoundSideCsgo->getErrors(), '保存roundSide失败');
                        }
                    }

                }
            }
            if (!$MatchBattleRoundCsgo->save()) {
                throw new BusinessException($MatchBattleRoundCsgo->getErrors(), '保存失败');
            }
        }
    }
    // round下选手数据入库
    public static function setRoundPlayers($battleId, $data, $adminType = 1, $adminId = 0)
    {
        $matchId = MatchBattle::find()->select(['match'])->where(['id' => $battleId])->one();
        foreach ($data as $k=>$player){
            $player['battle_id'] = $battleId;
            $player['match'] = $matchId['match'];
            $where = [
                'battle_id' => $battleId,
                'round_ordinal' => $player['round_ordinal'],
                'order' => $player['order'],
//                'player_order' => $player['player_order'],
            ];
            $MatchBattleRoundPlayer = MatchBattleRoundPlayerCsgo::find()->where($where)->one();
            if(!$MatchBattleRoundPlayer){
                $MatchBattleRoundPlayer = new MatchBattleRoundPlayerCsgo();
            }
            $MatchBattleRoundPlayer->setAttributes($player);
            $MatchBattleRoundPlayer->save();
            if (!$MatchBattleRoundPlayer->save()) {
                throw new BusinessException($MatchBattleRoundPlayer->getErrors(), '保存失败');
            }
        }
    }

    public static function getRounds($battleId)
    {
        $rounds=MatchBattleRoundCsgo::find()->where(['battle_id'=>$battleId])->orderBy('round_ordinal')->asArray()->all();
        return $rounds;
    }

    public static function getDetails($battleId)
    {
        $matchBattleExt = MatchBattleExtCsgo::find()->where(['id' => $battleId])->asArray()->one();

        $matchBattleTeam = MatchBattleTeam::find()->select(['team_id'])
            ->where(['and',
                ['=', 'battle_id', $matchBattleExt['id']],
                ['=', 'order', $matchBattleExt['win_round_1_team']],
            ])->one();
        $matchBattleExt['win_round_1_team'] = $matchBattleTeam['team_id'];

        $matchBattleTeam2 = MatchBattleTeam::find()->select(['team_id'])
            ->where(['and',
                ['=', 'battle_id', $matchBattleExt['id']],
                ['=', 'order', $matchBattleExt['win_round_16_team']],
            ])->one();
        $matchBattleExt['win_round_16_team'] = $matchBattleTeam2['team_id'];

        $matchBattleTeam3 = MatchBattleTeam::find()->select(['team_id'])
            ->where(['and',
                ['=', 'battle_id', $matchBattleExt['id']],
                ['=', 'order', $matchBattleExt['first_to_5_rounds_wins_team']],
            ])->one();
        $matchBattleExt['first_to_5_rounds_wins_team'] = $matchBattleTeam3['team_id'];

        return $matchBattleExt;
    }
    public static function getStatics($battleId)
    {
        $q = MatchBattleTeam::find()
            ->alias('battle_team')
            ->select('ext_csgo.*,battle_team.*')
            ->leftJoin('match_battle_team_ext_csgo as ext_csgo', 'battle_team.id=ext_csgo.id')
            ->where(['battle_team.battle_id' => $battleId])->orderBy('battle_team.order asc');
//        $sql=$q->createCommand()->getRawSql();
        $list = $q->asArray()->all();
        return $list;
    }

    public static function setDetails($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置详情
        $battleExtCsgo = MatchBattleExtCsgo::find()->where(['id' => $battleId])->one();
        if (!$battleExtCsgo) {
            $battleExtCsgo = new MatchBattleExtCsgo();
            $data['id'] = $battleId;
        }
        $battleExtCsgo->setAttributes($data);
        if (!$battleExtCsgo->save()) {
            throw new BusinessException($battleExtCsgo->getErrors(), '保存失败');
        }
    }

    public static function setStatics($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置统计数据,统计数据在战队信息中
        // 如果没有id，只有order，我们根据order获取id
        foreach ($data as $key => $val) {
            $matchBattleTeamId = 0;
            if (isset($val['id']) && $val['id']) {
                $matchBattleTeam = MatchBattleTeam::find()->where(['id' => $val['id']])->one();
            }
            if (!isset($val['id']) && isset($val['order']) && $val['order']) {


                $transaction = \Yii::$app->db->beginTransaction('REPEATABLE READ');
                try {
                    //事务
                    $matchBattleTeam = MatchBattleTeam::find()->where([
                        'battle_id' => $battleId,
                        'order' => $val['order'],
                    ])->one();
                    if (!$matchBattleTeam) {
                        $matchBattleTeam = new MatchBattleTeam();
                        // 这里判断必填项
                    }
                    $operationType = OperationLogService::OPERATION_TYPE_UPDATE;

                    $oldResult= $matchBattleTeam->toArray();
                    $matchBattleTeam->setAttributes([
                        'battle_id' => $battleId,
                        'order' => $val['order'],
                        'score' => isset($val['score']) ? $val['score'] : null,
                        'identity_id' => isset($val['identity_id']) ? $val['identity_id'] : "",
                        'rel_identity_id' => isset($val['rel_identity_id']) ? strval($val['rel_identity_id']) : "",
                        'team_id' => isset($val['team_id']) ? $val['team_id'] : null,
                        'name' => isset($val['name']) ? $val['name'] : "",
                    ]);
                    $teamRes = $matchBattleTeam->save();
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw new BusinessException($e->getMessage(), "csgoTeam保存错误,事务回滚");
                }
                if (!$teamRes) {
                    throw new BusinessException($data, '参数错误，找不到对应的对象');
                }
                $nowResultTeam = $matchBattleTeam->toArray();
                $matchBattle = MatchBattle::find()->where(['id'=>$battleId])->asArray()->one();
                $nowResult['match'] = $matchBattle['match'];
                $nowResult['id'] = $battleId;
                // 记录log
                $diffInfo = Common::getDiffInfo($oldResult, $nowResultTeam );
                if ($diffInfo['changed']) {
                    OperationLogService::addLog($operationType,
                        Consts::RESOURCE_TYPE_BATTLE,
                        $nowResult['id'],
                        ["diff" => $diffInfo["diff"], "new" => $nowResult],
                        0,
                        'battle_status',
                        null,
                        0,
                        Consts::USER_TYPE_ROBOT,
                        0
                    );
                }

            }
            $matchBattleTeamId = $matchBattleTeam->id;
            $info = MatchBattleTeamExtCsgo::find()->where(['id' => $matchBattleTeamId])->one();
            if (!$info) {
                $info = new MatchBattleTeamExtCsgo();
                $info->setAttribute('id',$matchBattleTeamId);
            }
            $val['id'] = $matchBattleTeamId;
            $info->setAttributes($val);
            if (!$info->save()) {
                $errorMsg =  json_encode($info->getErrors(),320);
                $dataJson = json_encode($val,320);
                $firstErrors = $c=$info->getFirstErrors();
                $firstErrorsJson = json_encode($firstErrors,320);
                throw new BusinessException($info->getErrors().$firstErrorsJson.$dataJson, "保存失败--{$errorMsg}");
            }
        }
    }

    public static function getBattleListByMatchId($matchId, $adminType = 1, $adminId = 0)
    {
        $battleIds = self::getBattleIdsByMatchId($matchId);
        $info = [];
        foreach ($battleIds as $id) {
            $info[] = self::getBattleInfo($id);
        }
        return $info;
    }

    public static function addBattleInfo($matchId, $data, $dataType = "", $adminType = 1, $adminId = 0)
    {
        $baseInfo = parent::addBattleInfo($matchId, $data, $dataType, $adminType, $adminId);
        // 初始化扩展表
        $battleId = $baseInfo["id"];
        $ext = new MatchBattleExtCsgo();
        $ext->setAttribute('id', $battleId);
        if (!$ext->save()) {
            throw new BusinessException($ext->getErrors(), '保存失败');
        }
        return self::getBattleInfo($battleId);
    }

    public static function setPlayers($battleId, $data, $adminType = 1, $adminId = 0)
    {
        if (empty($battleId)){
            throw new BusinessException([], "保存选手时需要battleId");
        }
        $matchId = MatchBattle::find()->select(['match'])->where(['id' => $battleId])->one();
        $playerList = MatchBattlePlayerCsgo::find()->where(['battle_id'=>$battleId])->asArray()->all();
        $oldPlayerListIdKeys = array_column($playerList,'id');
        $newPlayerListIdKeys = [];
        $diffPlayerListIdKeys = [];
        // 根据battleId，order
        foreach ($data as $player) {
            if (isset($player['id']) && $player['id']) {
                $playerModel = MatchBattlePlayerCsgo::find()->where(['id' => $player['id']])->one();
            }

            //battle_id order
            if (!isset($player['id']) && isset($player['order']) && $player['order']) {
                //事务
                $transaction = \Yii::$app->db->beginTransaction( 'REPEATABLE READ');
                try {
                    if ($player['steam_id']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'steam_id' => $player['steam_id']])->one();
                    }elseif ($player['nick_name']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'nick_name' => $player['nick_name'],
                        ])->one();
                    }elseif ($player['player_id']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'player_id' => $player['player_id'],
                        ])->one();
                    }else{
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'order' => $player['order'],
                        ])->one();
                    }

                    $player['match'] = $matchId['match'];
                    $player['battle_id'] = $battleId;
                    if (isset($player['adr'])) $player['adr'] = strval($player['adr']);
                    if (isset($player['first_kills_diff'])) $player['first_kills_diff'] = strval($player['first_kills_diff']);
                    if (isset($player['kast'])) $player['kast'] = strval($player['kast']);
                    if (isset($player['rating'])) $player['rating'] = strval($player['rating']);
                    if (!$playerModel) {
                        $playerModel = new MatchBattlePlayerCsgo();
//                    $playerModel->setAttribute('battle_id',$battleId);
                    }
                    $playerModel->setAttributes($player);
                    $res = $playerModel->save();
                    $newPlayerListIdKeys[] = $playerModel->id;
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw new BusinessException($e->getMessage(), "csgo选手保存错误,事务回滚");
                }
                if (!$res) {
                    throw new BusinessException($playerModel->getErrors(), "保存选手失败");
                }

            }
        }
        $diffPlayerListIdKeys = array_diff($oldPlayerListIdKeys,$newPlayerListIdKeys);
        if (!empty($diffPlayerListIdKeys)&&!empty($newPlayerListIdKeys)){
            foreach ($diffPlayerListIdKeys as $k=>$v){
                $diffPlayer = MatchBattlePlayerCsgo::find()->where(['id'=>$v,'battle_id'=>$battleId])->one();
                $diffPlayer->delete();
            }
        }
    }

    public static function setEvents($battleId, $data, $adminType = 1, $adminId = 0)
    {
        foreach ($data as $key => $event) {
            if (array_key_exists('event_id',$event)&&$event['event_id']){
                $eventModel = MatchBattleRoundEventCsgo::find()->where([
                    'battle_id' => $battleId,
                    'event_id' => $event['event_id'],
                ])->one();
            }else{
                $eventModel = MatchBattleRoundEventCsgo::find()->where([
                    'battle_id' => $battleId,
                    'order' => $event['order'],
                    'round_ordinal' => $event['round_ordinal'],
                ])->one();
            }

            if (!$eventModel) {
                $eventModel = new MatchBattleRoundEventCsgo();
                $eventModel->setAttributes([
                    'battle_id' => $battleId,
                    'order' => $event['order'],
                    'round_ordinal' => $event['round_ordinal'],
                ]);
            }
            if(isset($event['time_since_plant'])){
                $event['time_since_plant'] = (string)$event['time_since_plant'];
            }
            if(isset($event['in_round_timestamp'])){
                $event['in_round_timestamp'] = (string)$event['in_round_timestamp'];
            }
            if(isset($event['round_time'])){
                $event['round_time'] = (string)$event['round_time'];
            }
            if(isset($event['round_ordinal'])){
                $event['round_ordinal'] = (int)$event['round_ordinal'];
            }
            $eventModel->setAttributes($event);
            if (!$eventModel->save()) {
                throw new BusinessException($eventModel->getErrors(), "保存event事件失败");
            }
        }
    }
}