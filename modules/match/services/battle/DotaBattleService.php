<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services\battle;


use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleBanpick;
use app\modules\match\models\MatchBattleEventDota2;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattlePlayerDota2;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtDota2;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\rest\exceptions\BusinessException;

class DotaBattleService extends BattleServiceBase implements BattleInterface
{
    public static function getBattleInfo($battleId, $dataType = '', $adminType = 1, $adminId = 0)
    {
        return [
            BattleService::DATA_TYPE_BASE => self::getBaseInfo($battleId, $dataType, $adminType, $adminId),
            BattleService::DATA_TYPE_BAN_PICK => self::getBanPick($battleId),
            BattleService::DATA_TYPE_DETAILS => self::getDetails($battleId),
            BattleService::DATA_TYPE_STATICS => self::getStatics($battleId),
        ];
    }

    public static function setBattleInfo($battleId, $data, $dataType = '', $adminType = 1, $adminId = 0)
    {
        switch ($dataType) {
            case BattleService::DATA_TYPE_BASE:
                self::setBaseInfo($battleId, $data, $dataType, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_BAN_PICK:
                self::setBanPick($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_DETAILS:
                self::setDetails($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_FIRST_EVENTS:
                self::setDetailsEvents($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_STATICS:
                self::setStatics($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_PLAYERS:
                self::setPlayers($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_EVENTS:
                self::setEvents($battleId, $data, $adminType, $adminId);
                break;
        }
    }

    public static function setBanPick($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置阵容
        $battleExtDota2 = MatchBattleExtDota2::find()->where(['id' => $battleId])->one();
        if (!$battleExtDota2) {
            $battleExtDota2 = new MatchBattleExtDota2();
            $battleExtDota2->setAttribute('id', $battleId);
        }
        $battleExtDota2->setAttribute('ban_pick', json_encode($data));
        if (!$battleExtDota2->save()) {
            throw new BusinessException($battleExtDota2->getErrors(), '保存失败');
        }
        //保存到另外一个ban pick表
        MatchBattleBanpick::deleteAll(['battle_id' => $battleId]);
        $MatchBattleBanPick = new MatchBattleBanpick();
        foreach ($data as $key => &$val){
            $MatchBattleBanPickModel = clone $MatchBattleBanPick;
            if (!isset($val['order'])){
                $val['order'] = null;
            }
            $val['battle_id']= $battleId;
            $MatchBattleBanPickModel->setAttributes($val);
            $MatchBattleBanPickModel->save();
//            if (!$MatchBattleBanPickModel->save()) {
//                throw new BusinessException($MatchBattleBanPickModel->getErrors(), '保存失败');
//            }
        }
    }

    public static function getBanPick($battleId)
    {
        $battleExtDota2 = MatchBattleExtDota2::find()->where(['id' => $battleId])->one();
        if ($battleExtDota2) {
            return $battleExtDota2['ban_pick'];
        }
    }

    public static function getDetails($battleId)
    {
        return MatchBattleExtDota2::find()->where(['id' => $battleId])->asArray()->one();
    }

    public static function getStatics($battleId)
    {
        $q = MatchBattleTeam::find()
            ->alias('battle_team')
            ->select('ext_dota2.*,battle_team.*')
            ->leftJoin('match_battle_team_ext_dota2 as ext_dota2', 'battle_team.id=ext_dota2.id')
            ->where(['battle_team.battle_id' => $battleId]);
//        $sql=$q->createCommand()->getRawSql();
        $list = $q->asArray()->all();
        return $list;
    }
    //设置详情
    public static function setDetails($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置详情
        $battleExtDota2 = MatchBattleExtDota2::find()->where(['id' => $battleId])->one();
        if (!$battleExtDota2) {
            $battleExtDota2 = new MatchBattleExtDota2();
            $battleExtDota2->setAttribute('id', $battleId);
        }
        if( $data['ban_pick']){
            $data['ban_pick'] = json_encode($data['ban_pick']);
        }
        $battleExtDota2->setAttributes($data);
        if (!$battleExtDota2->save()) {

            $dataJson = json_encode($data,320);
            $firstErrors = $c=$battleExtDota2->getFirstErrors();
            $firstErrorsJson = json_encode($firstErrors,320);
            throw new BusinessException($battleExtDota2->getErrors().$firstErrorsJson.$dataJson, '保存失败');
        }
    }

    public static function setStatics($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置统计数据,统计数据在战队信息中
        // 如果没有id，只有order，我们根据order获取id
        foreach ($data as $key => $val) {
            $matchBattleTeamId = 0;
            if (isset($val['id']) && $val['id']) {
                $matchBattleTeam = MatchBattleTeam::find()->where(['id' => $val['id']])->one();
            }
            if (!isset($val['id']) && (isset($val['order']) && $val['order'])) {
                $matchBattleTeam = MatchBattleTeam::find()->where([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
//                    'rel_identity_id' => @$val['rel_identity_id']
                ])->one();
                if (!$matchBattleTeam) {
                    $matchBattleTeam = new MatchBattleTeam();
                }
                $matchBattleTeam->setAttributes([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
                    'rel_identity_id' => (string)$val['rel_identity_id'],
                    'score' => (int)$val['score'],
                    'team_id' => (int)$val['team_id']
                ]);
                $matchBattleTeam->save();
            }

            if (!$matchBattleTeam) {
                throw new BusinessException($data, '参数错误，找不到对应的对象');
            }
            $matchBattleTeam->setAttributes($data);
            $matchBattleTeam->save();

            $matchBattleTeamId = $matchBattleTeam->id;

            $info = MatchBattleTeamExtDota2::find()->where(['id' => $matchBattleTeamId])->one();
            if (!$info) {
                $info = new MatchBattleTeamExtDota2();
                $info->setAttribute('id',$matchBattleTeamId);
            }
            $val['id'] = $matchBattleTeamId;
            $info->setAttributes($val);
            if (!$info->save()) {
                throw new BusinessException($info->getErrors(), '保存失败');
            }
        }
    }

    public static function getBattleListByMatchId($matchId, $adminType = 1, $adminId = 0)
    {
        $battleIds = self::getBattleIdsByMatchId($matchId);
        $info = [];
        foreach ($battleIds as $id) {
            $info[] = self::getBattleInfo($id);
        }
        foreach ($info as $key=>$battle){
            $info[$key]['ban_pick'] =json_decode($battle['ban_pick'],true);
        }
        return $info;
    }

    public static function addBattleInfo($matchId, $data, $dataType = "", $adminType = 1, $adminId = 0)
    {
        $baseInfo = parent::addBattleInfo($matchId, $data, $dataType, $adminType, $adminId);
        // 初始化扩展表
        $battleId = $baseInfo["id"];
        $MatchBattle = MatchBattle::find()->where(['id'=>$battleId])->one();
        //获取地图
        $map_id = MatchService::getMapLists(3);
        $matchBattleDefault = [
            'status' => 1,//对局状态：未开始
            'game' => 3,//游戏ID
            'is_default_advantage' => 2,//默认领先获胜：否
            'is_forfeit' => 2,//是否弃权：否
            'is_battle_detailed' => 2,//是否有对局详情数据：否
            'is_draw' => 2,//是否平局：否
            'map'=>(Int)$map_id//比赛地图：读取地图码表的默认地图
        ];
        $MatchBattle->setAttributes($matchBattleDefault);
        if (!$MatchBattle->save()) {
            throw new BusinessException($MatchBattle->getErrors(), '保存battle默认数据失败');
        }
        $ext = new MatchBattleExtDota2();
        $ext->setAttribute('id', $battleId);
        $ext->setAttribute('is_confirmed', 2);
        if (!$ext->save()) {
            throw new BusinessException($ext->getErrors(), '保存失败');
        }
        return self::getBattleInfo($battleId);
    }

    public static function setPlayers($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 根据battleId，order
        foreach ($data as $player) {

            if (isset($player['id']) && $player['id']) {
                $playerModel = MatchBattlePlayerDota2::find()->where(['id' => $player['id']])->one();
            }
            //事务
            $transaction = \Yii::$app->db->beginTransaction( 'REPEATABLE READ');
            try {
                if (!isset($player['id']) && isset($player['rel_identity_id']) && $player['rel_identity_id']) {
                    $playerModel = MatchBattlePlayerDota2::find()->where([
                        'battle_id' => $battleId,
                        //'order' => $player['order'],//因为restapi和socket不一致 也不能使用player_id 因为可能还没有绑定
                        //'team_order' => $player['team_order'],
                        'rel_identity_id' => $player['rel_identity_id'],
                    ])->one();

                }
                if (!$playerModel) {
                    $playerModel = new MatchBattlePlayerDota2();
                    $player['battle_id'] =$battleId;
                }
                if(isset($player['rel_team_id'])){
                    $player['rel_team_id']=(string)$player['rel_team_id'];
                }
                if(isset($player['dtpm'])){
                    $player['dtpm']=(string)$player['dtpm'];
                }
                if(isset($player['rel_team_id'])){
                    $player['rel_team_id']=(string)$player['rel_team_id'];
                }
                if(isset($player['damage_conversion_rate'])){
                    $player['damage_conversion_rate']=(string)$player['damage_conversion_rate'];
                }
                if(isset($player['participation'])){
                    $player['participation']=(string)$player['participation'];
                }
                if(isset($player['gold_earned_percent'])){
                    $player['gold_earned_percent']=(string)$player['gold_earned_percent'];
                }
                if(isset($player['dpm_to_champions'])){
                    $player['dpm_to_champions']=(string)$player['dpm_to_champions'];
                }
                if(isset($player['kda'])){
                    $player['kda']=(string)$player['kda'];
                }
                if(isset($player['gpm'])){
                    $player['gpm']=(string)$player['gpm'];
                }
                if(isset($player['cspm'])){
                    $player['cspm']=(string)$player['cspm'];
                }
                if(isset($player['lhpm'])){
                    $player['lhpm']=(string)$player['lhpm'];
                }
                if(isset($player['xpm'])){
                    $player['xpm']=(string)$player['xpm'];
                }
                if(isset($player['damage_percent_to_champions'])){
                    $player['damage_percent_to_champions']=(string)$player['damage_percent_to_champions'];
                }
                $playerModel->setAttributes($player);
                $playerSaveRes = $playerModel->save();
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new BusinessException($e->getMessage(), "dota2选手保存错误,事务回滚");
            }
            if (!$playerSaveRes) {
                throw new BusinessException($playerModel->getErrors(), "保存选手失败");
            }
        }
    }

    public static function getTeamOrderByTeamFaction($battle_id,$team_faction)
    {
        $MatchBattleTeamModel = MatchBattleTeam::find()->alias('mbt')
            ->select('mbt.id,mbt.order')
            ->leftJoin('match_battle_team_ext_dota2 as mbtel' ,'mbt.id = mbtel.id')
            ->where(['mbt.battle_id'=>$battle_id])
            ->andWhere(['mbtel.faction'=>$team_faction]);
//        $MatchBattleTeamModel->createCommand()->getRawSql();
        $info = $MatchBattleTeamModel->asArray()->one();
        if ($info){
            return $info['order'];
        }else{
            throw new BusinessException($MatchBattleTeamModel->getErrors(), '无当前队伍信息');
        }

    }
    public static function setEvents($battleId, $data, $adminType = 1, $adminId = 0)
    {
        foreach ($data as $event) {
            $eventModel = MatchBattleEventDota2::find()->where([
                'battle_id' => $battleId,
                'event_id' => $event['event_id'],
            ])->one();
            if (!$eventModel) {
                $eventModel = new MatchBattleEventDota2();
            }
            if (!$eventModel) {
                throw new BusinessException($data, '找不到对应的操作资源');
            }
            $event['battle_id'] = $battleId;
            $eventModel->setAttributes($event);
            if (!$eventModel->save()) {
                throw new BusinessException($eventModel->getErrors(), "保存事件失败");
            }

        }
    }
}