<?php
/**
 * Created by PhpStorm.
 * User: cheny
 * Date: 2020/3/23
 * Time: 17:13
 */

namespace app\modules\match\services\battle;

use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleBanpick;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchBattlePlayerLol;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtLol;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\org\models\Player;
use app\rest\exceptions\BusinessException;

class LolBattleService extends BattleServiceBase implements BattleInterface
{
    public static function getBattleInfo($battleId, $dataType = '', $adminType = 1, $adminId = 0)
    {
        return [
            BattleService::DATA_TYPE_BASE => self::getBaseInfo($battleId, $dataType, $adminType, $adminId),
            BattleService::DATA_TYPE_BAN_PICK => self::getBanPick($battleId),
            BattleService::DATA_TYPE_DETAILS => self::getDetails($battleId),
            BattleService::DATA_TYPE_STATICS => self::getStatics($battleId),
        ];
    }

    public static function setBattleInfo($battleId, $data, $dataType = '', $adminType = 1, $adminId = 0)
    {
        switch ($dataType) {
            case BattleService::DATA_TYPE_BASE:
                self::setBaseInfo($battleId, $data, $dataType, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_BAN_PICK:
                self::setBanPick($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_DETAILS:
                self::setDetails($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_FIRST_EVENTS:
                self::setDetailsEvents($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_STATICS:
                self::setStatics($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_PLAYERS:
                self::setPlayers($battleId, $data, $adminType, $adminId);
                break;
            case BattleService::DATA_TYPE_EVENTS:
                self::setEvents($battleId, $data, $adminType, $adminId);
                break;
        }
    }

    public static function setBanPick($battleId, $data, $adminType = 1, $adminId = 0)
    {
        //设置阵容
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if (!$battleExtLol) {
            $battleExtLol = new MatchBattleExtLol();
            $battleExtLol->setAttribute('id', $battleId);
        }
        $battleExtLol->setAttribute('ban_pick', json_encode($data,320));
        if (!$battleExtLol->save()) {
            throw new BusinessException($battleExtLol->getErrors(), '保存失败');
        }
        //保存到另外一个ban pick表
        MatchBattleBanpick::deleteAll(['battle_id' => $battleId]);
        $MatchBattleBanPick = new MatchBattleBanpick();
        foreach ($data as $key => &$val){
            $MatchBattleBanPickModel = clone $MatchBattleBanPick;
            if (!isset($val['order'])){
                $val['order'] = null;
            }
            $val['battle_id']= $battleId;
            $MatchBattleBanPickModel->setAttributes($val);
            $MatchBattleBanPickModel->save();
//            if (!$MatchBattleBanPickModel->save()) {
//                throw new BusinessException($MatchBattleBanPickModel->getErrors(), '保存失败');
//            }
        }
    }

    public static function getBanPick($battleId)
    {
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if ($battleExtLol) {
            return $battleExtLol['ban_pick'];
        }
    }

    public static function getDetails($battleId)
    {
        return MatchBattleExtLol::find()->where(['id' => $battleId])->asArray()->one();
    }

    public static function getStatics($battleId)
    {
        $q = MatchBattleTeam::find()
            ->alias('battle_team')
            ->select('ext_lol.*,battle_team.*')
            ->leftJoin('match_battle_team_ext_lol as ext_lol', 'battle_team.id=ext_lol.id')
            ->where(['battle_team.battle_id' => $battleId]);
//        $sql=$q->createCommand()->getRawSql();
        $list = $q->asArray()->all();
        return $list;
    }
    //设置详情的特殊事件setDetailsEvents
    public static function setDetailsEvents($battleId, $data, $adminType = 1, $adminId = 0)
    {
        if (count($data) > 0){
            // 设置详情，一血等
            $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
            if (!$battleExtLol) {
                $battleExtLol = new MatchBattleExtLol();
                $battleExtLol->setAttribute('id', $battleId);
            }
            $team_faction = [];
            //首先查询team的order
            $killer_team = self::getTeamOrderByTeamFaction($battleId,'blue');
            if ($killer_team == 1){
                $team_faction['blue'] = 1;
                $team_faction['red'] = 2;
            }else{
                $team_faction['blue'] = 2;
                $team_faction['red'] = 1;
            }
            foreach ($data as $item){
                $item[$item['first_event_type_key']] = $team_faction[$item['first_event_faction']];
                $battleExtLol->setAttributes($item);
            }
            if (!$battleExtLol->save()) {
                throw new BusinessException($battleExtLol->getErrors(), '保存对局特殊事件失败');
            }
        }else{
            return ;
        }
    }
    //设置详情
    public static function setDetails($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置详情，一血等
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if (!$battleExtLol) {
            $battleExtLol = new MatchBattleExtLol();
            $battleExtLol->setAttribute('id', $battleId);
        }
        if(isset($data['ban_pick']) && $data['ban_pick']){
            $data['ban_pick'] = @json_encode($data['ban_pick'],320);
        }
        $battleExtLol->setAttributes($data);
        if (!$battleExtLol->save()) {
            throw new BusinessException($battleExtLol->getErrors(), '保存失败');
        }
    }

    public static function setStatics($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 设置统计数据,统计数据在战队信息中
        // 如果没有id，只有order，我们根据order获取id
        foreach ($data as $key => $val) {
            $matchBattleTeamId = 0;
            if (isset($val['id']) && $val['id']) {
                $matchBattleTeam = MatchBattleTeam::find()->where(['id' => $val['id']])->one();
            }
            if (!isset($val['id']) && (isset($val['order']) && $val['order'])) {
                $matchBattleTeam = MatchBattleTeam::find()->where([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
                    //'rel_identity_id' => @$val['rel_identity_id']//之前赵恒添加，现在去掉，不知道有什么影响没
                ])->one();
                if (!$matchBattleTeam) {
                    $matchBattleTeam = new MatchBattleTeam();
                }
                $matchBattleTeam->setAttributes([
                    'battle_id' => $battleId,
                    'order' => $val['order'],
                    'rel_identity_id' => (string)$val['rel_identity_id'],
                    'score' => $val['score'],
                    'team_id' => $val['team_id']
                ]);
                $matchBattleTeam->save();
            }

            if (!$matchBattleTeam) {
                throw new BusinessException($data, '参数错误，找不到对应的对象');
            }
            $matchBattleTeam->setAttributes($data);
            $matchBattleTeam->save();

            $matchBattleTeamId = $matchBattleTeam->id;

            $info = MatchBattleTeamExtLol::find()->where(['id' => $matchBattleTeamId])->one();
            if (!$info) {
                $info = new MatchBattleTeamExtLol();
                $info->setAttribute('id',$matchBattleTeamId);
            }
            if (isset($val['dragon_kills_detail']) && is_array($val['dragon_kills_detail'])) {
                $val['dragon_kills_detail'] = json_encode($val['dragon_kills_detail']);
            }
            if (isset($val['gold']) && $val['gold']) {
                $val['gold'] = (string)$val['gold'];
            }
            $val['id'] = $matchBattleTeamId;
            $info->setAttributes($val);
            if (!$info->save()) {
                throw new BusinessException($info->getErrors(), '保存失败');
            }
        }
    }

    public static function getBattleListByMatchId($matchId, $adminType = 1, $adminId = 0)
    {
        $battleIds = self::getBattleIdsByMatchId($matchId);
        $info = [];
        foreach ($battleIds as $id) {
            $info[] = self::getBattleInfo($id);
        }
        foreach ($info as $key=>$battle){
            $info[$key]['ban_pick'] =json_decode($battle['ban_pick'],true);
        }
        return $info;
    }

    public static function addBattleInfo($matchId, $data, $dataType = "", $adminType = 1, $adminId = 0)
    {
        $baseInfo = parent::addBattleInfo($matchId, $data, $dataType, $adminType, $adminId);
        // 初始化扩展表
        $battleId = $baseInfo["id"];
        $MatchBattle = MatchBattle::find()->where(['id'=>$battleId])->one();
        //获取地图
        $map_id = MatchService::getMapLists(2);
        $matchBattleDefault = [
            'status' => 1,//对局状态：未开始
            'game' => 2,//游戏ID
            'is_default_advantage' => 2,//默认领先获胜：否
            'is_forfeit' => 2,//是否弃权：否
            'is_battle_detailed' => 2,//是否有对局详情数据：否
            'is_draw' => 2,//是否平局：否
            'map'=>(Int)$map_id//比赛地图：读取地图码表的默认地图
        ];
        $MatchBattle->setAttributes($matchBattleDefault);
        if (!$MatchBattle->save()) {
            throw new BusinessException($MatchBattle->getErrors(), '保存battle默认数据失败');
        }
        $ext = new MatchBattleExtLol();
        $ext->setAttribute('id', $battleId);
        $ext->setAttribute('is_confirmed', 2);
        if (!$ext->save()) {
            throw new BusinessException($ext->getErrors(), '保存失败');
        }
        return self::getBattleInfo($battleId);
    }

    public static function setPlayers($battleId, $data, $adminType = 1, $adminId = 0)
    {
        // 根据battleId，order
        foreach ($data as $player) {
            //事务
            $transaction = \Yii::$app->db->beginTransaction( 'REPEATABLE READ');
            try {
                if (isset($player['id']) && $player['id']) {
                    $playerModel = MatchBattlePlayerLol::find()->where(['id' => $player['id']])->one();
                }

                if (!isset($player['id']) && isset($player['rel_identity_id']) && $player['rel_identity_id']) {
                    $playerModel = MatchBattlePlayerLol::find()->where([
                        'battle_id' => $battleId,
                        //'order' => $player['order'],//因为restapi和socket不一致 也不能使用player_id 因为可能还没有绑定
                        'team_order' => $player['team_order'],
                        'rel_identity_id' => $player['rel_identity_id'],
                    ])->one();

                }
                if (!$playerModel) {
                    $playerModel = new MatchBattlePlayerLol();
                    $player['battle_id']= $battleId;
                }

                if(isset($player['rel_team_id'])){
                    $player['rel_team_id']=(string)$player['rel_team_id'];
                }
                if(isset($player['dtpm'])){
                    $player['dtpm']=(string)$player['dtpm'];
                }
                if(isset($player['rel_team_id'])){
                    $player['rel_team_id']=(string)$player['rel_team_id'];
                }
                if(isset($player['damage_conversion_rate'])){
                    $player['damage_conversion_rate']=(string)$player['damage_conversion_rate'];
                }
                if(isset($player['participation'])){
                    $player['participation']=(string)$player['participation'];
                }
                if(isset($player['gold_earned_percent'])){
                    $player['gold_earned_percent']=(string)$player['gold_earned_percent'];
                }
                if(isset($player['dpm_to_champions'])){
                    $player['dpm_to_champions']=(string)$player['dpm_to_champions'];
                }
                if(isset($player['damage_taken_percent'])){
                    $player['damage_taken_percent']=(string)$player['damage_taken_percent'];
                }
                if(isset($player['kda'])){
                    $player['kda']=(string)$player['kda'];
                }
                if(isset($player['gpm'])){
                    $player['gpm']=(string)$player['gpm'];
                }
                if(isset($player['cspm'])){
                    $player['cspm']=(string)$player['cspm'];
                }
                if(isset($player['damage_percent_to_champions'])){
                    $player['damage_percent_to_champions']=(string)$player['damage_percent_to_champions'];
                }
                $playerModel->setAttributes($player);
                $playerSaveRes = $playerModel->save();

                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new BusinessException($e->getMessage(), "lol选手保存错误,事务回滚");
            }
            if (!$playerSaveRes) {
                throw new BusinessException($playerModel->getErrors(), "保存选手失败");
            }
        }
    }

    public static function setEvents($battleId, $data, $adminType = 1, $adminId = 0)
    {
        foreach ($data as $event) {
            $eventModel = MatchBattleEventLol::find()->where([
                'battle_id' => $battleId,
                'order' => $event['order'],
            ])->one();
            if (!$eventModel) {
                $eventModel = new MatchBattleEventLol();
            }
            if (!$eventModel) {
                throw new BusinessException($data, '找不到对应的操作资源');
            }
            //判断类型
//            if ($event['killer_faction']){
//                $event['killer_faction'] = self::getTeamOrderByTeamFaction($battleId,$event['killer_faction']);
//            }
//            if ($event['victim_faction']){
//                $event['victim_faction'] = self::getTeamOrderByTeamFaction($battleId,$event['victim_faction']);
//            }
            $event['battle_id'] = $battleId;
            $eventModel->setAttributes($event);
            if (!$eventModel->save()) {
                throw new BusinessException($eventModel->getErrors(), "保存事件失败");
            }

        }
    }
    //删除事件
    public static function delEvents($battleId, $data, $adminType = 1, $adminId = 0)
    {
        foreach ($data as $event) {
            $eventModel = MatchBattleEventLol::find()->where([
//                'battle_id' => $battleId,
                'id' => $event['id'],
            ])->one();
            if (!$eventModel) {
                throw new BusinessException($data, '找不到对应的操作资源');
            }
            if (!$eventModel->delete()) {
                throw new BusinessException($eventModel->getErrors(), "保存事件失败");
            }

        }
    }
    public static function getTeamOrderByTeamFaction($battle_id,$team_faction)
    {
        $MatchBattleTeamModel = MatchBattleTeam::find()->alias('mbt')
            ->select('mbt.id,mbt.order')
            ->leftJoin('match_battle_team_ext_lol as mbtel' ,'mbt.id = mbtel.id')
            ->where(['mbt.battle_id'=>$battle_id])
            ->andWhere(['mbtel.faction'=>$team_faction]);
//        $MatchBattleTeamModel->createCommand()->getRawSql();
        $info = $MatchBattleTeamModel->asArray()->one();
        if ($info){
            return $info['order'];
        }else{
            throw new BusinessException([], '无当前队伍信息');
        }

    }
}