<?php

namespace app\modules\metadata\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\services\MetadataService;
use app\modules\task\services\Common;

/**
 *
 */
class CsgoController extends WithTokenAuthController
{
    // 地图增加/修改
    public function actionEditMap()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_CSGO_MAP,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_CSGO_MAP,"");
        }

        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_CSGO_MAP) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_CSGO_MAP) : 0;
        return MetadataService::editInfo(Consts::METADATA_TYPE_CSGO_MAP, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }
    // 地图列表
    public function actionMapList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_CSGO_MAP, $params);
    }
    // 地图数据
    public function actionMapData()
    {
        $maps = MetadataCsgoMap::find()->select('id,name,name_cn')->where(['and',
            ['=','deleted',2],
            ['=','flag',1],
        ])->asArray()->all();
        return $maps;
    }
    // 获取一条地图数据
    public function actionMapInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getDetail(Consts::METADATA_TYPE_CSGO_MAP, $id);
    }
    // 地图删除
    public function actionDelMap()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_CSGO_MAP,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_CSGO_MAP,"");
        }
//        if ($deleted == 2) {
//            return MetadataService::unDel(Consts::METADATA_TYPE_CSGO_MAP, $id, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        } elseif ($deleted == 1) {
//            return MetadataService::del(Consts::METADATA_TYPE_CSGO_MAP, $id, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }
    // 武器增加/修改
    public function actionEditWeapon()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_CSGO_WEAPON) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_CSGO_WEAPON) : 0;
        return MetadataService::editInfo(Consts::METADATA_TYPE_CSGO_WEAPON, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }
    //武器列表
    public function actionWeaponList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_CSGO_WEAPON, $params);
    }
    //获取一条武器数据
    public function actionWeaponInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getDetail(Consts::METADATA_TYPE_CSGO_WEAPON, $id);
    }
    // 删除武器
    public function actionDelWeapon()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_CSGO_WEAPON,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_CSGO_WEAPON,"");
        }

//        if ($deleted == 2) {
//            return MetadataService::unDel(Consts::METADATA_TYPE_CSGO_WEAPON, $id, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        } elseif ($deleted == 1) {
//            return MetadataService::del(Consts::METADATA_TYPE_CSGO_WEAPON, $id, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }
}