<?php
namespace app\modules\metadata\controllers;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\metadata\services\DotaService;
use app\modules\metadata\models\DodaProp;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\services\MetadataService;
use app\modules\task\services\Common;
use app\rest\exceptions\BusinessException;
use app\modules\metadata\models\MetadataDota2Map;

/**
 *
 */

class DotaController extends WithTokenAuthController
{
    //获取详情(dota2天赋，道具)
    public function actionGetInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');
        $metadataType = \Yii::$app->getRequest()->get('metadata-type');
        return MetadataService::getDetail($metadataType,$id);
    }
    public function actionAddHero()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_HERO) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_HERO) : 0;
        $basisId=$this->pPost('todo_id');
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_HERO,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_HERO,"");
        }
        return MetadataService::editInfo(Consts::METADATA_TYPE_DOTA2_HERO, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }
    public function actionDelHero()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_DOTA2_HERO,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_DOTA2_HERO,"");
        }
//        if($deleted== 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_DOTA2_HERO, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif ($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_DOTA2_HERO, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }

    }
    public function actionHeroList()
    {
        $params = \Yii::$app->getRequest()->get();
        return  MetadataService::getList(Consts::METADATA_TYPE_DOTA2_HERO, $params);
    }
    // 添加英雄时 获取选择的天赋
    public function actionChooseTalent()
    {
        return  MetadataService::getTalent();
    }

    public function actionHeroInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getDota2HeroDetail($id);
    }

    public function actionAddProp()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_ITEM) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_ITEM) : 0;
        $basisId=$this->pPost('todo_id');
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_ITEM,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_ITEM,"");
        }
        return MetadataService::editInfo(Consts::METADATA_TYPE_DOTA2_ITEM, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionPropList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_DOTA2_ITEM, $params);

    }

    public function actionDelProp()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_DOTA2_ITEM,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_DOTA2_ITEM,"");
        }

//        if($deleted == 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_DOTA2_ITEM, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif ($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_DOTA2_ITEM, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }


    public function actionAddAbility()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_ABILITY) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_ABILITY) : 0;
        $basisId=$this->pPost('todo_id');
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_ABILITY,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_ABILITY,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_DOTA2_ABILITY, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionAbilityList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_DOTA2_ABILITY, $params);

    }
    //dota2详情
    public function actionAbilityInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getDotaAbilityDetail($id);
    }

    public function actionDelAbility()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_DOTA2_ABILITY,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_DOTA2_ABILITY,"");
        }

//        if($deleted==2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_DOTA2_ABILITY, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif ($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_DOTA2_ABILITY, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    public function actionAddTalent()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_TALENT) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_DOTA2_TALENT) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_TALENT,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_DOTA2_TALENT,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_DOTA2_TALENT, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionTalentList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_DOTA2_TALENT, $params);

    }

    public function actionDelTalent()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_DOTA2_TALENT,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_DOTA2_TALENT,"");
        }
//        if($deleted==2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_DOTA2_TALENT, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif ($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_DOTA2_TALENT, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    public function actionGetHeroList()
    {
        return MetadataService::getHeroList(2);
    }
    // 地图数据
    public function actionMapData()
    {
        $maps = MetadataDota2Map::find()->select('id,name,name_cn')->where(['and',
            ['=','deleted',2],
            ['=','flag',1],
        ])->asArray()->all();
        return $maps;
    }
}