<?php


namespace app\modules\metadata\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\metadata\services\MetadataService;
use app\modules\task\services\Common;

class KogController extends WithTokenAuthController
{

    //英雄列表
    public function actionHeroList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_KOG_CHAMPION, $params);
    }

    //添加英雄
    public function actionAddHero()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_CHAMPION) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_CHAMPION) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_KOG_CHAMPION,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_KOG_CHAMPION,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_KOG_CHAMPION, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    //英雄详情
    public function actionHeroInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolHeroDetail($id);
    }



    /**
     * 删除 恢复
     * @return array|bool
     * @throws \app\rest\exceptions\BusinessException
     * @throws \yii\base\Exception
     */
    public function actionDelHero()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_KOG_CHAMPION,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_KOG_CHAMPION,"");
        }

    }

    public function actionAbilityList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_KOG_ABILITY, $params);

    }

    // 英雄技能
    public function actionAddAbility()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_ABILITY) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_ABILITY) : 0;
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_KOG_ABILITY,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_KOG_ABILITY,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_KOG_ABILITY, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionDelAbility()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_KOG_ABILITY,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_KOG_ABILITY,"");
        }
    }

    /**
     * (技能详情)
     */
    public function actionAbilityInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getKogAbilityDetail($id);
    }

    //道具添加
    public function actionAddProp()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_ITEM) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_ITEM) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_KOG_ITEM,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_KOG_ITEM,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_KOG_ITEM, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    /**
     * (道具详情)
     */
    public function actionPropInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getKogPropDetail($id);
    }

    public function actionPropList()
    {
        $params = \Yii::$app->getRequest()->get();

        return MetadataService::getList(Consts::METADATA_TYPE_KOG_ITEM, $params);
    }

    public function actionDelProp()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_KOG_ITEM,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_KOG_ITEM,"");
        }

    }

    // 召唤师技能
    public function actionAddSkill()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_SUMMONER_SPELL) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_SUMMONER_SPELL) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_KOG_SUMMONER_SPELL,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_KOG_SUMMONER_SPELL,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_KOG_SUMMONER_SPELL, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    public function actionSkillList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_KOG_SUMMONER_SPELL, $params);

    }

    /**
     * 召唤师技能详情
     */
    public function actionSkillInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getKogSkillDetail($id);

    }

    public function actionDelSkill()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_KOG_SUMMONER_SPELL,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_KOG_SUMMONER_SPELL,"");
        }

    }


    // 符文
    public function actionAddRune()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_RUNE) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_KOG_RUNE) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_KOG_RUNE,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_KOG_RUNE,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_KOG_RUNE, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionRuneList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_KOG_RUNE, $params);

    }

    /**
     * 符文详情
     */
    public function actionRuneInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getKogRuneDetail($id);

    }

    public function actionDelRune()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_KOG_RUNE,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_KOG_RUNE,"");
        }
    }


}