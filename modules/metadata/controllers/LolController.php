<?php

namespace app\modules\metadata\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\metadata\services\MetadataService;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\task\services\Common;

/**
 *
 */
class LolController extends WithTokenAuthController
{
    public function actionAddHero()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_CHAMPION) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_CHAMPION) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_LOL_CHAMPION,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_LOL_CHAMPION,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_LOL_CHAMPION, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    public function actionHeroList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_LOL_CHAMPION, $params);
    }

    public function actionHeroInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolHeroDetail($id);
    }

    public function actionDelHero()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_LOL_CHAMPION,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_LOL_CHAMPION,"");
        }

//        if($deleted == 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_LOL_CHAMPION, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_LOL_CHAMPION, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    public function actionAddProp()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_ITEM) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_ITEM) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_LOL_ITEM,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_LOL_ITEM,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_LOL_ITEM, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    public function actionPropList()
    {
        $params = \Yii::$app->getRequest()->get();

        return MetadataService::getList(Consts::METADATA_TYPE_LOL_ITEM, $params);
    }

    /**
     * (道具详情)
     */
    public function actionPropInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolPropDetail($id);
    }

    public function actionDelProp()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_LOL_ITEM,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_LOL_ITEM,"");
        }

//        if($deleted == 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_LOL_ITEM, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_LOL_ITEM, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    // 召唤师技能
    public function actionAddSkill()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);
    }

    public function actionSkillList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, $params);

    }

    /**
     * 召唤师技能详情
     */
    public function actionSkillInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolSkillDetail($id);

    }

    public function actionDelSkill()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,"");
        }

//        if($deleted == 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    // 符文
    public function actionAddRune()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_RUNE) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_RUNE) : 0;

        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_LOL_RUNE,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_LOL_RUNE,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_LOL_RUNE, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }

    public function actionRuneList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_LOL_RUNE, $params);

    }
    /**
     * 符文详情
     */
    public function actionRuneInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolRuneDetail($id);

    }

    public function actionDelRune()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_LOL_RUNE,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_LOL_RUNE,"");
        }

//        if($deleted == 2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_LOL_RUNE, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_LOL_RUNE, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    // 英雄技能
    public function actionAddAbility()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $basisId=$this->pPost('todo_id');
        $gameId = Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_ABILITY) ?  Common::getGameIdByMetaDataType(Consts::METADATA_TYPE_LOL_ABILITY) : 0;
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::METADATA_TYPE_LOL_ABILITY,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::METADATA_TYPE_LOL_ABILITY,"");
        }

        return MetadataService::editInfo(Consts::METADATA_TYPE_LOL_ABILITY, $attributes, Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId(),$gameId,$basisId);

    }


    public function actionAbilityList()
    {
        $params = \Yii::$app->getRequest()->get();
        return MetadataService::getList(Consts::METADATA_TYPE_LOL_ABILITY, $params);

    }

    /**
     * (技能详情)
     */
    public function actionAbilityInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return MetadataService::getLolAbilityDetail($id);
    }

    public function actionDelAbility()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('deleted');
        if(isset($deleted) && $deleted == 1){
            return Common::deletedMaster($id,Consts::METADATA_TYPE_LOL_ABILITY,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::METADATA_TYPE_LOL_ABILITY,"");
        }

//        if($deleted==2){
//            return MetadataService::unDel(Consts::METADATA_TYPE_LOL_ABILITY, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }elseif($deleted == 1){
//            return MetadataService::del(Consts::METADATA_TYPE_LOL_ABILITY, $id,Consts::USER_TYPE_ADMIN, \Yii::$app->getUser()->getId());
//        }
    }

    public function actionGetHeroList()
    {
        return MetadataService::getHeroList(1);
    }
    // 地图数据
    public function actionMapData()
    {
        return MetadataLolMap::find()->select('id,name,name_cn')->where(['and',
            ['=','deleted',2],
            ['=','flag',1],
        ])->asArray()->all();
    }
}