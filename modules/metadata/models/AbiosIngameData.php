<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "abios_ingame_data".
 *
 * @property int $id
 * @property int|null $game_id
 * @property int|null $rel_id
 * @property string|null $rel_name
 * @property string|null $category
 */
class AbiosIngameData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'abios_ingame_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'rel_id', 'ingame_id'], 'integer'],
            [['rel_name', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'rel_id' => 'Rel ID',
            'ingame_id' => 'Ingame ID',
            'rel_name' => 'Rel Name',
            'category' => 'Category',
        ];
    }
}
