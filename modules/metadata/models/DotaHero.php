<?php

namespace app\modules\metadata\models;

use app\rest\exceptions\BusinessException;
use Yii;


class DotaHero extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dota_hero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hero_name','status','official_name','icon','role_type','attack_type','main_attribute'], 'required'],
            [['cuser', 'deleted', 'status'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['hero_name','hero_name_cn','official_name','main_attribute','attack_type','role_type',
                'role_type_cn','leg_num'], 'string', 'max' => 70],
            [['icon'], 'string', 'max' => 300]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'hero_name' => '英雄名称',
            'status' => '状态',
            'official_name' => '官方内部名称',
            'icon' => '英雄logo',
            'role_type' => '角色类型',
            'attack_type' => '攻击类型',
            'main_attribute' => '主属性',
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $hero = self::find()->where(['id' => $attribute['id']])->one();
            $hero->setAttributes($attribute);
            if ($hero->save() > 0 ) {
                // 天赋树
                if (isset($attribute['hero_talent_config']) && !empty($attribute['hero_talent_config'])) {
                    $heroTalentConfig = json_decode($attribute['hero_talent_config'],true);
                    // 先删后加
                    DotaTalent::deleteAll(['dota_hero_id' => $attribute['id']]);

                    $model = new DotaTalent();
                    foreach($heroTalentConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('left_talent_name',$val['left_talent_name']);
                        $_model->setAttribute('left_talent_name_cn',$val['left_talent_name_cn']);
                        $_model->setAttribute('right_talent_name',$val['right_talent_name']);
                        $_model->setAttribute('right_talent_name_cn',$val['right_talent_name_cn']);
                        $_model->setAttribute('type',$val['type']);
                        $_model->setAttribute('dota_hero_id',$attribute['id']);
                        $_model->save();
                    }
                }
                if (isset($attribute['hero_skill_config']) && !empty($attribute['hero_skill_config'])) {
                    $heroSkillConfig = json_decode($attribute['hero_skill_config'],true);
                    // 先删后加
                    HeroSkill::deleteAll(['hero_id' => $attribute['id']]);

                    $model = new HeroSkill();
                    foreach($heroSkillConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('skill_name',$val['skill_name']);
                        $_model->setAttribute('skill_name_cn',$val['skill_name_cn']);
                        $_model->setAttribute('default_key',$val['default_key']);
                        $_model->setAttribute('logo',$val['logo']);
                        $_model->setAttribute('introduction',$val['introduction']);
                        $_model->setAttribute('introduction_cn',$val['introduction_cn']);
                        $_model->setAttribute('type',2);
                        $_model->setAttribute('hero_id',$attribute['id']);
                        $_model->save();
                    }
                    return ['hero_id' => $attribute['id'], 'msg' => '成功'];
                }

                return ['hero_id' => $attribute['id'], 'msg' => '成功'];
            }
        } else {
            $hero = new self();
            $hero->setAttributes($attribute);
            $hero->setAttribute('cuser',$userId);
            if ($hero->save() > 0 ) {
                $heroId = $hero->id;
                // 天赋树
                if (isset($attribute['hero_talent_config']) && !empty($attribute['hero_talent_config'])) {
                    $heroTalentConfig = json_decode($attribute['hero_talent_config'],true);

                    $model = new DotaTalent();
                    foreach($heroTalentConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('left_talent_name',$val['left_talent_name']);
                        $_model->setAttribute('left_talent_name_cn',$val['left_talent_name_cn']);
                        $_model->setAttribute('right_talent_name',$val['right_talent_name']);
                        $_model->setAttribute('right_talent_name_cn',$val['right_talent_name_cn']);
                        $_model->setAttribute('type',$val['type']);
                        $_model->setAttribute('dota_hero_id',$heroId);
                        $_model->save();
                    }
                }
                //关系
                if (isset($attribute['hero_skill_config']) && !empty($attribute['hero_skill_config'])) {
                    $heroSkillConfig = json_decode($attribute['hero_skill_config'],true);

                    $model = new HeroSkill();
                    foreach($heroSkillConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('skill_name',$val['skill_name']);
                        $_model->setAttribute('skill_name_cn',$val['skill_name_cn']);
                        $_model->setAttribute('default_key',$val['default_key']);
                        $_model->setAttribute('logo',$val['logo']);
                        $_model->setAttribute('introduction',$val['introduction']);
                        $_model->setAttribute('introduction_cn',$val['introduction_cn']);
                        $_model->setAttribute('type',2);
                        $_model->setAttribute('hero_id',$heroId);
                        $_model->save();
                    }
                    return ['hero_id' => $heroId, 'msg' => '成功'];
                }

                return ['hero_id' => $heroId, 'msg' => '成功'];
            }else{
                throw new BusinessException($hero->getErrors(),'创建失败');
            }
        }
    }
}
