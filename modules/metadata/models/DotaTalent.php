<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class DotaTalent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dota_hero_talent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dota_hero_id','type'], 'integer'],
            [['left_talent_name', 'left_talent_name_cn','right_talent_name','right_talent_name_cn'], 'string','max'=>100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
