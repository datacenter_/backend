<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "enum_csgo_weapon".
 *
 * @property int $id
 * @property string|null $kind 英文
 * @property string|null $kind_cn 中文
 * @property string|null $panda_score
 */
class EnumCsgoWeapon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_csgo_weapon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kind', 'kind_cn', 'panda_score','abios'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kind' => 'Kind',
            'kind_cn' => 'Kind Cn',
            'panda_score' => 'Panda Score',
            'abios' => 'abios Score',
        ];
    }
}
