<?php

namespace app\modules\metadata\models;

use app\rest\exceptions\BusinessException;
use Yii;


class LolHero extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lol_hero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hero_name','hero_status','hero_title','hero_logo'], 'required'],
            [['cuser', 'deleted', 'hero_status'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['hero_name','hero_name_cn','hero_title','hero_title_cn'], 'string', 'max' => 50],
            [['hero_logo'], 'string', 'max' => 300]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'hero_name' => '英雄名称',
            'hero_status' => '状态',
            'hero_title' => '英雄名称',
            'hero_logo' => '英雄logo',
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $hero = self::find()->where(['id' => $attribute['id']])->one();
            $hero->setAttributes($attribute);
            if ($hero->save() > 0 ) {

                if (isset($attribute['hero_skill_config']) && !empty($attribute['hero_skill_config'])) {
                    $heroSkillConfig = json_decode($attribute['hero_skill_config'],true);
                    // 先删后加
                    HeroSkill::deleteAll(['hero_id' => $attribute['id']]);

                    $model = new HeroSkill();
                    foreach($heroSkillConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('skill_name',$val['skill_name']);
                        $_model->setAttribute('skill_name_cn',$val['skill_name_cn']);
                        $_model->setAttribute('default_key',$val['default_key']);
                        $_model->setAttribute('logo',$val['logo']);
                        $_model->setAttribute('introduction',$val['introduction']);
                        $_model->setAttribute('introduction_cn',$val['introduction_cn']);
                        $_model->setAttribute('type',1);
                        $_model->setAttribute('hero_id',$attribute['id']);
                        $_model->save();
                    }
                    return ['hero_id' => $attribute['id'], 'msg' => '成功'];
                }

                return ['hero_id' => $attribute['id'], 'msg' => '成功'];
            }
        } else {
            $hero = new self();
            $hero->setAttributes($attribute);
            $hero->setAttribute('cuser',$userId);
            if ($hero->save() > 0 ) {
                $heroId = $hero->id;

                //关系
                if (isset($attribute['hero_skill_config']) && !empty($attribute['hero_skill_config'])) {
                    $heroSkillConfig = json_decode($attribute['hero_skill_config'],true);

                    $model = new HeroSkill();
                    foreach($heroSkillConfig as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('skill_name',$val['skill_name']);
                        $_model->setAttribute('skill_name_cn',$val['skill_name_cn']);
                        $_model->setAttribute('default_key',$val['default_key']);
                        $_model->setAttribute('logo',$val['logo']);
                        $_model->setAttribute('introduction',$val['introduction']);
                        $_model->setAttribute('introduction_cn',$val['introduction_cn']);
                        $_model->setAttribute('type',1);
                        $_model->setAttribute('hero_id',$heroId);
                        $_model->save();
                    }
                    return ['hero_id' => $heroId, 'msg' => '成功'];
                }

                return ['hero_id' => $heroId, 'msg' => '成功'];
            }else{
                throw new BusinessException($hero->getErrors(),'创建失败');
            }
        }
    }
}
