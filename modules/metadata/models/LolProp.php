<?php

namespace app\modules\metadata\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class LolProp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lol_prop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prop_name','prop_status','icon'], 'required'],
            [['cuser', 'deleted', 'prop_status'], 'integer'],
            [['prop_name_cn', 'official_id','prop_money'], 'string','max'=>100],
            [['introduction', 'introduction_cn'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prop_name' => '道具名称',
            'prop_status' => '状态',
            'icon' => '头像',
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $prop = self::find()->where(['id' => $attribute['id']])->one();
            $prop->setAttributes($attribute);
            if ($prop->save() > 0 ) {
                return ['prop_id' => $attribute['id'], 'msg' => '成功'];
            }else{
                throw new BusinessException($prop->getErrors(),'创建失败');
            }
        } else {
            $prop = new self();
            $prop->setAttributes($attribute);
            $prop->setAttribute('cuser',$userId);
            if ($prop->save() > 0 ) {
                    return ['prop_id' => $prop->id, 'msg' => '成功'];
                } else{
                throw new BusinessException($prop->getErrors(),'创建失败');
            }

        }
    }
}
