<?php

namespace app\modules\metadata\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class LolRune extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lol_rune';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rune_name','rune_status','icon','type_name'], 'required'],
            [['cuser', 'deleted', 'rune_status'], 'integer'],
            [['type_name', 'type_name_cn','rune_name_cn','rune_name'], 'string','max'=>70],
            [['icon'], 'string','max'=>300],
            [['introduction', 'introduction_cn'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'rune_name' => '符文名称',
            'type_name' => '分类名称',
            'rune_status' => '状态',
            'icon' => '头像',
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $rune = self::find()->where(['id' => $attribute['id']])->one();
            $rune->setAttributes($attribute);
            if ($rune->save() > 0 ) {
                return ['rune_id' => $attribute['id'], 'msg' => '成功'];
            }else{
                throw new BusinessException($rune->getErrors(),'创建失败');
            }
        } else {
            $rune = new self();
            $rune->setAttributes($attribute);
            $rune->setAttribute('cuser',$userId);
            if ($rune->save() > 0 ) {
                    return ['rune_id' => $rune->id, 'msg' => '成功'];
                } else{
                throw new BusinessException($rune->getErrors(),'创建失败');
            }

        }
    }
}
