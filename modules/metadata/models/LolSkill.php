<?php

namespace app\modules\metadata\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class LolSkill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lol_skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['skill_name','skill_status','icon'], 'required'],
            [['cuser', 'deleted', 'skill_status'], 'integer'],
            [['skill_name', 'skill_name_cn'], 'string','max'=>100],
            [['icon'], 'string','max'=>300],
            [['skill_content_cn', 'skill_content'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'skill_name' => '技能名称',
            'skill_status' => '状态',
            'icon' => '头像',
        ];
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $skill = self::find()->where(['id' => $attribute['id']])->one();
            $skill->setAttributes($attribute);
            if ($skill->save() > 0 ) {
                return ['skill_id' => $attribute['id'], 'msg' => '成功'];
            }else{
                throw new BusinessException($skill->getErrors(),'创建失败');
            }
        } else {
            $skill = new self();
            $skill->setAttributes($attribute);
            $skill->setAttribute('cuser',$userId);
            if ($skill->save() > 0 ) {
                    return ['skill_id' => $skill->id, 'msg' => '成功'];
                } else{
                throw new BusinessException($skill->getErrors(),'创建失败');
            }

        }
    }
}
