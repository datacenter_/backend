<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_csgo_weapon".
 *
 * @property int $id
 * @property string|null $name 名称
 * @property string|null $kind 类别
 * @property int|null $state 状态
 * @property int|null $cost 费用
 * @property int|null $kill_award 击杀回报
 * @property string|null $movement_speed 移动速度
 * @property int|null $ammunition 弹夹容量
 * @property int|null $capacity 后备弹量	
 * @property string|null $reload_time 装填时间
 * @property int|null $is_ct_available CT是否可用
 * @property int|null $is_t_available T是否可用
 * @property string|null $firing_mode 射击模式
 * @property string|null $slug Slug
 * @property string|null $image 图标	
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 * @property int|null $deleted 1删除，2正常
 * @property string|null $data_source 当前数据源
 * @property int|null $flag
 * @property string|null $external_id
 * @property string|null $external_name
 */
class MetadataCsgoWeapon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_csgo_weapon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'cost', 'kill_award', 'ammunition', 'capacity', 'is_ct_available', 'is_t_available', 'deleted', 'flag'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'movement_speed', 'reload_time', 'firing_mode', 'slug', 'image', 'data_source', 'external_id', 'external_name'], 'string', 'max' => 255],
            [['kind'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'kind' => 'Kind',
            'state' => 'State',
            'cost' => 'Cost',
            'kill_award' => 'Kill Award',
            'movement_speed' => 'Movement Speed',
            'ammunition' => 'Ammunition',
            'capacity' => 'Capacity',
            'reload_time' => 'Reload Time',
            'is_ct_available' => 'Is Ct Available',
            'is_t_available' => 'Is T Available',
            'firing_mode' => 'Firing Mode',
            'slug' => 'Slug',
            'image' => 'Image',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted',
            'data_source' => 'Data Source',
            'flag' => 'Flag',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
        ];
    }
}
