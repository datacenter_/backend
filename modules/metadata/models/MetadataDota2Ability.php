<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_dota2_ability".
 *
 * @property int $id
 * @property int|null $hero_id
 * @property string|null $name
 * @property string|null $name_cn
 * @property string|null $hotkey
 * @property string|null $slug
 * @property string|null $image
 * @property string|null $description
 * @property string|null $description_cn
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $external_id
 * @property string|null $external_name
 */
class MetadataDota2Ability extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_dota2_ability';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hero_id', 'flag','state','deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'hotkey', 'slug', 'image', 'external_id', 'external_name','ability_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hero_id' => 'Hero ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'hotkey' => 'Hotkey',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
            'ability_code' => 'ABILITY CODE',
            'deleted' => 'deleted',
        ];
    }
}
