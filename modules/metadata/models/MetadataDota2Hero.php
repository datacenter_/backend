<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_dota2_hero".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_cn
 * @property int|null $state
 * @property string|null $external_id
 * @property string|null $external_name
 * @property string|null $title
 * @property string|null $title_cn
 * @property string|null $primary_attr
 * @property string|null $primary_attr_cn
 * @property string|null $roles
 * @property string|null $roles_cn
 * @property string|null $slug
 * @property string|null $image
 * @property string|null $small_image
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $talents
 */
class MetadataDota2Hero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_dota2_hero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'flag','deleted'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['talents'], 'string'],
            [['name', 'name_cn', 'external_id', 'external_name', 'title', 'title_cn', 'primary_attr', 'primary_attr_cn', 'roles', 'roles_cn', 'slug', 'image', 'small_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
            'title' => 'Title',
            'title_cn' => 'Title Cn',
            'primary_attr' => 'Primary Attr',
            'primary_attr_cn' => 'Primary Attr Cn',
            'roles' => 'Roles',
            'roles_cn' => 'Roles Cn',
            'slug' => 'Slug',
            'image' => 'Image',
            'small_image' => 'Small Image',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'talents' => 'Talents',
            'deleted' => 'deleted',
        ];
    }
}
