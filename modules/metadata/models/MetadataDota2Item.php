<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_dota2_item".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_cn
 * @property int|null $state
 * @property string|null $external_id
 * @property string|null $external_name
 * @property int|null $total_cost
 * @property int|null $is_recipe 1或2，空
 * @property int|null $is_secret_shop
 * @property int|null $is_home_shop
 * @property int|null $is_neutral_drop
 * @property string|null $slug
 * @property string|null $image
 * @property string|null $description
 * @property string|null $description_cn
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property int|null $deleted 1删除，2正常
 */
class MetadataDota2Item extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_dota2_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'total_cost', 'is_recipe', 'is_secret_shop', 'is_home_shop', 'is_neutral_drop', 'flag', 'deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'external_id', 'external_name', 'slug', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
            'total_cost' => 'Total Cost',
            'is_recipe' => 'Is Recipe',
            'is_secret_shop' => 'Is Secret Shop',
            'is_home_shop' => 'Is Home Shop',
            'is_neutral_drop' => 'Is Neutral Drop',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted',
        ];
    }
}
