<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_dota2_talent".
 *
 * @property int $id
 * @property int|null $hero_id
 * @property int|null $level
 * @property int|null $left_right
 * @property string|null $talent
 * @property string|null $talent_cn
 * @property string|null $slug
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $external_id
 * @property string|null $external_name
 */
class MetadataDota2Talent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_dota2_talent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hero_id', 'level', 'left_right', 'flag','state','deleted'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['talent', 'talent_cn', 'slug', 'external_id', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hero_id' => 'Hero ID',
            'level' => 'Level',
            'left_right' => 'Left Right',
            'talent' => 'Talent',
            'talent_cn' => 'Talent Cn',
            'slug' => 'Slug',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
        ];
    }
}
