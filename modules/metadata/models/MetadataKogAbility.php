<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_kog_ability".
 *
 * @property int $id
 * @property int|null $champion_id 英雄id
 * @property string|null $name 名称
 * @property string|null $name_cn 名称中文
 * @property string|null $hotkey 默认快捷键
 * @property int|null $state
 * @property string|null $slug slug
 * @property string|null $image 图标
 * @property string|null $description 描述
 * @property string|null $description_cn 描述中文
 * @property int|null $flag
 * @property int|null $deleted 1删除，2正常
 * @property string|null $modified_at 修改时间
 * @property string|null $created_at 创建时间
 * @property string|null $deleted_at 删除时间
 * @property string|null $external_id
 * @property string|null $external_name
 */
class MetadataKogAbility extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_kog_ability';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['champion_id', 'state', 'flag', 'deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'hotkey', 'slug', 'image', 'external_id', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'champion_id' => 'Champion ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'hotkey' => 'Hotkey',
            'state' => 'State',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'deleted' => 'Deleted',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
        ];
    }
}
