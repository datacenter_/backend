<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_kog_champion".
 *
 * @property int $id
 * @property string|null $name 名称
 * @property string|null $name_cn 中文名称
 * @property int|null $state 状态
 * @property string|null $title 称号
 * @property string|null $title_cn 称号中文
 * @property string|null $primary_role 主要角色类型
 * @property string|null $primary_role_cn
 * @property string|null $secondary_role
 * @property string|null $secondary_role_cn
 * @property string|null $slug
 * @property string|null $image
 * @property string|null $small_image
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property int|null $deleted 1删除，2正常
 * @property string|null $external_id 官方id
 * @property string|null $external_name
 */
class MetadataKogChampion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_kog_champion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'flag', 'deleted'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'title', 'title_cn', 'primary_role', 'primary_role_cn', 'secondary_role', 'secondary_role_cn', 'slug', 'image', 'small_image', 'external_id', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'title' => 'Title',
            'title_cn' => 'Title Cn',
            'primary_role' => 'Primary Role',
            'primary_role_cn' => 'Primary Role Cn',
            'secondary_role' => 'Secondary Role',
            'secondary_role_cn' => 'Secondary Role Cn',
            'slug' => 'Slug',
            'image' => 'Image',
            'small_image' => 'Small Image',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
        ];
    }
}
