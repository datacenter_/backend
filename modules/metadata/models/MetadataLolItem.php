<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_lol_item".
 *
 * @property int $id
 * @property string|null $name 名称
 * @property string|null $name_cn 名称中文
 * @property int|null $state 状态
 * @property string|null $external_id 官方id
 * @property int|null $total_cost 总费用
 * @property int|null $is_trinket 是否是饰品
 * @property int|null $is_purchasable 是否可直接购买
 * @property string|null $slug
 * @property string|null $image 图标
 * @property string|null $description 描述
 * @property string|null $description_cn 描述
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property int|null $deleted 1删除2正常
 * @property string|null $external_name
 */
class MetadataLolItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_lol_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'total_cost', 'is_trinket', 'is_purchasable', 'flag', 'deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'external_id', 'slug', 'image', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'external_id' => 'External ID',
            'total_cost' => 'Total Cost',
            'is_trinket' => 'Is Trinket',
            'is_purchasable' => 'Is Purchasable',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted',
            'external_name' => 'External Name',
        ];
    }
}
