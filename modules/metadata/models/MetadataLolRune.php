<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_lol_rune".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_cn
 * @property int|null $state
 * @property string|null $external_id
 * @property string|null $path_name
 * @property string|null $path_name_cn
 * @property string|null $slug
 * @property string|null $image
 * @property string|null $description
 * @property string|null $description_cn
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $external_name
 */
class MetadataLolRune extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_lol_rune';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'flag','deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'external_id', 'path_name', 'path_name_cn', 'slug', 'image', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'external_id' => 'External ID',
            'path_name' => 'Path Name',
            'path_name_cn' => 'Path Name Cn',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'external_name' => 'External Name',
        ];
    }
}
