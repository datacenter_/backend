<?php

namespace app\modules\metadata\models;

use Yii;

/**
 * This is the model class for table "metadata_lol_summoner_spell".
 *
 * @property int $id
 * @property string|null $name 名字
 * @property string|null $name_cn 中文名字
 * @property int|null $state 状态
 * @property string|null $slug slug
 * @property string|null $image 图标
 * @property string|null $description 描述
 * @property string|null $description_cn 描述
 * @property int|null $flag
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $external_id
 * @property string|null $external_name
 */
class MetadataLolSummonerSpell extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_lol_summoner_spell';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'flag','deleted'], 'integer'],
            [['description', 'description_cn'], 'string'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'slug', 'image', 'external_id', 'external_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'description_cn' => 'Description Cn',
            'flag' => 'Flag',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'external_id' => 'External ID',
            'external_name' => 'External Name',
            'deleted' => 'deleted',
        ];
    }
}
