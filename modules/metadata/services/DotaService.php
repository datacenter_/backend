<?php

namespace app\modules\metadata\services;


use app\modules\metadata\models\DodaProp;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\DotaTalent;
use app\modules\metadata\models\HeroSkill;
use yii\data\Pagination;

class DotaService
{
    public static function getPropList($params)
    {
        $dotaProp = DodaProp::find()->alias('d');

        if (isset($params['prop_status']) && !empty($params['prop_status'])) {
            $dotaProp->andWhere(['d.prop_status' => $params['prop_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $dotaProp->andWhere(['d.id' => $params['id']]);
        }
        if (isset($params['prop_name']) && !empty($params['prop_name'])) {
            $dotaProp->andfilterwhere(['like', 'd.prop_name', $params['prop_name']]);
        }
        if (isset($params['official_name']) && !empty($params['official_name'])) {
            $dotaProp->andfilterwhere(['like', 'd.official_name', $params['official_name']]);
        }
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $dotaProp->andWhere(['>=', 'd.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $dotaProp->andWhere(['<', 'd.modified_at', $end]);
        }

        $dotaProp=$dotaProp->orderBy('d.id desc');
        $totalCount=$dotaProp->count();
        $page = \Yii::$app->request->get('page',1) - 1;
        $pageSize = \Yii::$app->request->get('per_page',20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $dotaProp->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getHeroList($params)
    {
        $dotaHero = DotaHero::find()->alias('d');

        if (isset($params['official_name']) && !empty($params['official_name'])) {
            $dotaHero->andfilterwhere(['like', 'd.official_name', $params['official_name']]);
        }

        if (isset($params['hero_name']) && !empty($params['hero_name'])) {
            $dotaHero->andfilterwhere(['like', 'd.hero_name', $params['hero_name']]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $dotaHero->andWhere(['d.id' => $params['id']]);
        }

        if (isset($params['status']) && !empty($params['status'])) {
            $dotaHero->andWhere(['d.status' => $params['status']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $dotaHero->andWhere(['>=', 'd.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $dotaHero->andWhere(['<', 'd.modified_at', $end]);
        }

        $dotaHero=$dotaHero->orderBy('d.id desc');
        $totalCount=$dotaHero->count();
        $page = \Yii::$app->request->get('page',1) - 1;
        $pageSize = \Yii::$app->request->get('per_page',20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $dotaHero->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getHeroInfo($id)
    {
        $dotaHeroInfo = DotaHero::find()->where(['id' => $id])->asArray()->one();
        $dotaHeroInfo['hero_talent_info'] = DotaTalent::find()->where(['dota_hero_id' => $id])->asArray()->all();
        $dotaHeroInfo['hero_skill_info'] = HeroSkill::find()->where(['hero_id' => $id,'type'=>2])->asArray()->all();

        return $dotaHeroInfo;
    }
}