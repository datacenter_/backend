<?php

namespace app\modules\metadata\services;


use app\commands\crontab\CrontabBase;
use app\commands\crontab\CrontabInterface;
use app\modules\common\services\Consts;
use app\modules\file\services\UploadService;
use app\modules\task\services\grab\riotgames\RiotAbility;
use app\modules\task\services\grab\riotgames\RiotChampions;
use app\modules\task\services\grab\riotgames\RiotgamesBase;
use app\modules\task\services\grab\riotgames\RiotItems;
use app\modules\task\services\grab\riotgames\RiotRunesReforged;
use app\modules\task\services\grab\riotgames\RiotSummoner;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class LolRiotgamesService extends CrontabBase implements CrontabInterface
{
    private $version;
    public function run($args)
    {
        //下载
        $fileUrl = $this->downLoadLatestFile();
        $this->log("文件路径：".$fileUrl);
        //解压文件，把需要文件放到对应的地方
        $info = $this->dealWithTagFile($fileUrl);
        $this->addTaseAndRun();
    }

    public function uploadImagesImages($fromPath, $toPath)
    {
        // 读取目录下的所有图片文件
        $images = scandir($fromPath);
        // 挨个上传到对应的oss
        foreach ($images as $imgName) {
            if ($imgName === '.' || $imgName === '..') {
                continue;
            }
            $imageUri = $fromPath . "/" . $imgName;
            $ossUri = $toPath . "/" . $imgName;
            UploadService::uploadTo($imageUri, $ossUri);
        }
    }
    /**
     * @param $fileUrl
     * 解压文件，把需要文件放到对应的地方
     */
    public function dealWithTagFile($newfname)
    {
        $this->log("开始处理文件解压：".$newfname);
        $dirName=$this->unTag($newfname, dirname($newfname));

        $unzipFilePath = pathinfo($newfname, PATHINFO_DIRNAME) . '/' . pathinfo($newfname, PATHINFO_FILENAME);

        $base = \Yii::$app->basePath;
        $riotgamesFilePath = $base . "/data/";
        $v = $this->getLatestVersion();
        if (empty($v)) {
            throw new \Exception("获取版本号失败");
        }

        $championFullPath = $unzipFilePath . "/" . $v . "/data/en_US/championFull.json";
        $newchampionFullPath = $riotgamesFilePath . "championFull.json";
        $championFullzh_CNPath = $unzipFilePath . "/" . $v . "/data/zh_CN/championFull.json";
        $newchampionFullzh_CNPath = $riotgamesFilePath . "championFullzh_CN.json";
        $runesReforgedPath = $unzipFilePath . "/" . $v . "/data/en_US/runesReforged.json";
        $newRunesReforgedPath = $riotgamesFilePath . "runesReforged.json";
        $runesReforgedzh_CNPath = $unzipFilePath . "/" . $v . "/data/zh_CN/runesReforged.json";
        $newRunesReforgedzh_CNPath = $riotgamesFilePath . "runesReforgedzh_CN.json";
        // 复制文件
        $this->log("开始复制文件：".$riotgamesFilePath);
        if (!copy($championFullPath, $newchampionFullPath)) {
            throw new \Exception("复制拳头英语技能文件错误");
        }
        if (!copy($championFullzh_CNPath, $newchampionFullzh_CNPath)) {
            throw new \Exception("复制拳头中文技能文件错误");
        }
        if (!copy($runesReforgedPath, $newRunesReforgedPath)) {
            throw new \Exception("复制拳头英语符文文件错误");
        }

        if (!copy($runesReforgedzh_CNPath, $newRunesReforgedzh_CNPath)) {
            throw new \Exception("复制拳头中文符文文件错误");
        }
        //curl文件上传到该路径,当作接口请求
        $curlFilePath = "riotgames/curl";
        //上传文件到阿里云
        UploadService::uploadTo($newchampionFullPath, $curlFilePath."/championFull.json");
        UploadService::uploadTo($newchampionFullzh_CNPath, $curlFilePath."/championFullzh_CN.json");
        UploadService::uploadTo($newRunesReforgedPath, $curlFilePath."/runesReforged.json");
        UploadService::uploadTo($newRunesReforgedzh_CNPath, $curlFilePath."/runesReforgedzh_CN.json");

        // 上传图片
        //道具
        $itemImgFromPath = $unzipFilePath . "/" . $v . "/img/item";
        $itemToPath = "riotgames/item";
        //英雄
        $championImgFromPath = $unzipFilePath . "/" . $v . "/img/champion";
        $championToPath = "riotgames/champion";

        //技能召唤师技能
        $spellionImgFromPath = $unzipFilePath . "/" . $v . "/img/spell";
        $spellionToPath = "riotgames/spell";


        //符文图片  lol_rune
        $runeToPath = "riotgames/rune";
        $runeJson = file_get_contents($newRunesReforgedPath,'r');
        $runearr = json_decode($runeJson,true);


        foreach ($runearr as $key => $item) {
            foreach ($item['slots'] as $slots_item) {
                foreach ($slots_item['runes'] as $rItems){
//                    $rItems['path_name'] = $item['key'];
//                    $infoMerge[$rItems['id']]=$rItems;
//                    $runeImgFromPath = $unzipFilePath . "/img/perk-images/Styles/".$item['key']."/".$rItems['key'];
                    $runeImgFromPath = $unzipFilePath . "/img/" .$rItems['icon'];

                    $this->log("开始上传符文图片：".$runeImgFromPath);
                    $ossUri = $runeToPath ."/" . basename($runeImgFromPath);
                    UploadService::uploadTo($runeImgFromPath, $ossUri);
//                    $this->uploadImagesImages($runeImgFromPath, $runeToPath);;
                }
            }
        }

//        if (!file_exists($pic_local_path)) {
////            print_r($pic_local_path);
////            exit;
//            $info=mkdir($pic_local_path, 0777,true);
//            @chmod($pic_local_path, 0777);
//        }

        $this->log("开始上传图片：".$newfname);
        $this->uploadImagesImages($itemImgFromPath, $itemToPath);
        $this->uploadImagesImages($championImgFromPath, $championToPath);
        $this->uploadImagesImages($spellionImgFromPath, $spellionToPath);
        //删除文件和压缩文件
        $this->log("开始删除压缩后的文件：".$unzipFilePath);
        $this->delFile($unzipFilePath);
        $this->log("开始删除压缩文件：".$newfname);
        unlink($newfname);
    }

    /**
     * 删除文件夹以及文件内文件
     * @param $path
     * @return bool
     */
    public function delFile($path)
    {
        //先删除目录下的文件：
        $dh = opendir($path);
        while ($file = readdir($dh)) {
            if ($file != "." && $file != "..") {
                $fullpath = $path . "/" . $file;
                if (!is_dir($fullpath)) {
                    unlink($fullpath);
                } else {
                    $this->delFile($fullpath);
                }
            }
        }
        closedir($dh);
        //删除当前文件夹：
        if (rmdir($path)) {
            return true;
        } else {
            return false;
        }
    }

    public function getLatestVersion()
    {
        if(!$this->version){
            $this->version=RiotgamesBase::curlNewVersion();
        }
        return RiotgamesBase::curlNewVersion();
    }

    public function getFileUrlByVersion($version)
    {
//        return sprintf("https://ddragon.leagueoflegends.com/cdn/dragontail-%s.zip", $version);
        return sprintf("https://ddragon.leagueoflegends.com/cdn/dragontail-%s.tgz", $version);
    }

    /**
     * @return false|resource
     * 获取最新的压缩包，下载下来，返回路径
     */
    public function downLoadLatestFile()
    {
        $this->log("开始下载文件");
        ini_set("memory_limit", "512M");
        $v = $this->getLatestVersion();
        if (empty($v)) {
            throw new \Exception("获取版本号失败1");

        }
        $url = $this->getFileUrlByVersion($v);
        $base = \Yii::$app->basePath;
        $cacheBase = $base . '/runtime/cache/';
        $file = basename($url);
        $newfname = $cacheBase . $file;
        return $this->downLoadFileByWget($url, $cacheBase);
//        return $this->downLoadFileByFopen($url, $newfname);
    }

    /**
     *
     * @param string $filePath 压缩包所在地址 【绝对文件地址】d:/test/123.zip
     * @param string $path 解压路径 【绝对文件目录路径】d:/test
     * @return bool
     */
    public function unzip($filePath, $path)
    {
        if (empty($path) || empty($filePath)) {
            return false;
        }
        $zip = new \ZipArchive();
        if ($zip->open($filePath) === true) {
            $zip->extractTo($path);
            $zip->close();
            return true;
        } else {
            return false;
        }
    }

    public function unTag($filePatch, $path)
    {
        $toPath = $path . '/' . pathinfo($filePatch, PATHINFO_FILENAME);
        if (!is_dir($toPath)) {
            mkdir($toPath);
        }
        $cmd = sprintf("tar -zxvf %s -C %s", $filePatch, $toPath);
        exec($cmd, $re);
        return $toPath;
    }

    /**
     * @param string $url
     * @param string $newfname
     * @return string
     */
    public function downLoadFileByFopen(string $url, string $newfname): string
    {
        $header = get_headers($url, 1);
        $size = $header['Content-Length'];

        $fp = fopen($url, 'rb');
        if ($fp === false) exit('文件不存在或打开失败');
        set_time_limit(0);
        if ($fp) {
            $newf = fopen($newfname, "wb"); // 远在文件文件
            while (!feof($fp)) {
                fwrite($newf, fread($fp, $size), $size); // 没有写完就继续
            }
            fclose($fp);

        }
        return $newfname;
    }


    public function downLoadFileByWget(string $url, string $cachePath): string
    {
        $cmd = sprintf("wget -P %s %s ", $cachePath, $url);
        exec($cmd, $re);
        return $cachePath . basename($url);
    }

    public function addTaseAndRun(): void
    {
        $this->log("添加任务并执行：");
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", "", '');
        $taskInfo = [
            [ //技能
                "type" => RiotAbility::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_ABILITY),
                "params" => [
                    'action' => 'ability',
                    'params' => [
                    ],

                ],
            ],
            [ //英雄
                "type" => RiotChampions::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_CHAMPION),
                "params" => [
                    'action' => 'champion',
                    'params' => [
                    ],

                ],
            ],
            [ //道具
                "type" => RiotItems::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_ITEM),
                "params" => [
                    'action' => 'item',
                    'params' => [
                    ],

                ],
            ],
            //符文
            [
                "type" => RiotRunesReforged::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_RUNE),
                "params" => [
                    'action' => '',
                    'params' => [
                    ],

                ],
            ],
            //召唤师技能
            [
                "type" => RiotSummoner::class,
                "tag" => $tag,
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RIOTGAMES,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL),
                "params" => [
                    'action' => 'summoner',
                    'params' => [
                    ],
                ],
            ],

        ];

        foreach ($taskInfo as $val) {
            TaskRunner::addTask($val, 1);
        }
    }
}