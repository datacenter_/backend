<?php

namespace app\modules\metadata\services;


use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\metadata\models\DodaProp;
use app\modules\metadata\models\DotaHero;
use app\modules\metadata\models\DotaTalent;
use app\modules\metadata\models\HeroSkill;
use app\modules\metadata\models\LolHero;
use app\modules\metadata\models\LolProp;
use app\modules\metadata\models\LolRune;
use app\modules\metadata\models\LolSkill;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\task\services\Common;
use app\modules\task\services\grab\riotgames\RiotgamesBase;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\db\Exception;

class LolService
{
    const METADATA_TYPE_TO_CLASS = [
        Consts::METADATA_TYPE_LOL_ABILITY => MetadataLolAbility::class,
        Consts::METADATA_TYPE_LOL_CHAMPION => MetadataLolChampion::class,
        Consts::METADATA_TYPE_LOL_ITEM => MetadataLolItem::class,
        Consts::METADATA_TYPE_LOL_RUNE => MetadataLolRune::class,
        Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => MetadataLolSummonerSpell::class,
    ];

    public static function getPropList($params)
    {
        $LolProp = LolProp::find()->alias('p');

        if (isset($params['prop_status']) && !empty($params['prop_status'])) {
            $LolProp->andWhere(['d.prop_status' => $params['prop_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $LolProp->andWhere(['p.id' => $params['id']]);
        }
        if (isset($params['prop_name']) && !empty($params['prop_name'])) {
            $LolProp->andfilterwhere(['like', 'p.prop_name', $params['prop_name']]);
        }
        if (isset($params['official_id']) && !empty($params['official_id'])) {
            $LolProp->andWhere(['p.official_id' => $params['official_id']]);
        }
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $LolProp->andWhere(['>=', 'p.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $LolProp->andWhere(['<', 'p.modified_at', $end]);
        }

        $LolProp = $LolProp->orderBy('p.id desc');
        $totalCount = $LolProp->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $LolProp->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getHeroList($params)
    {
        $LolHero = LolHero::find()->alias('h');

        if (isset($params['hero_title']) && !empty($params['hero_title'])) {
            $LolHero->andfilterwhere(['like', 'h.hero_title', $params['hero_title']]);
        }

        if (isset($params['hero_name']) && !empty($params['hero_name'])) {
            $LolHero->andfilterwhere(['like', 'h.hero_name', $params['hero_name']]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $LolHero->andWhere(['h.id' => $params['id']]);
        }

        if (isset($params['hero_status']) && !empty($params['hero_status'])) {
            $LolHero->andWhere(['h.hero_status' => $params['hero_status']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $LolHero->andWhere(['>=', 'h.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $LolHero->andWhere(['<', 'h.modified_at', $end]);
        }

        $LolHero = $LolHero->orderBy('h.id desc');
        $totalCount = $LolHero->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $LolHero->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getHeroInfo($id)
    {
        $lolHeroInfo = LolHero::find()->where(['id' => $id])->asArray()->one();
        $lolHeroInfo['hero_skill_info'] = HeroSkill::find()->where(['hero_id' => $id, 'type' => 1])->asArray()->all();

        return $lolHeroInfo;
    }

    public static function getRuneList($params)
    {
        $lolRune = LolRune::find()->alias('r');

        if (isset($params['rune_status']) && !empty($params['rune_status'])) {
            $lolRune->andWhere(['r.rune_status' => $params['rune_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $lolRune->andWhere(['r.id' => $params['id']]);
        }
        if (isset($params['rune_name']) && !empty($params['rune_name'])) {
            $lolRune->andfilterwhere(['like', 'r.rune_name', $params['rune_name']]);
        }
        if (isset($params['type_name']) && !empty($params['type_name'])) {
            $lolRune->andfilterwhere(['like', 'r.type_name', $params['type_name']]);
        }
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $lolRune->andWhere(['>=', 'r.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $lolRune->andWhere(['<', 'r.modified_at', $end]);
        }

        $lolRune = $lolRune->orderBy('r.id desc');
        $totalCount = $lolRune->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $lolRune->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getSkillList($params)
    {
        $LolSkill = LolSkill::find()->alias('s');

        if (isset($params['skill_status']) && !empty($params['skill_status'])) {
            $LolSkill->andWhere(['s.skill_status' => $params['skill_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $LolSkill->andWhere(['s.id' => $params['id']]);
        }
        if (isset($params['skill_name']) && !empty($params['skill_name'])) {
            $LolSkill->andfilterwhere(['like', 's.skill_name', $params['skill_name']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $LolSkill->andWhere(['>=', 's.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $LolSkill->andWhere(['<', 's.modified_at', $end]);
        }

        $LolSkill = $LolSkill->orderBy('s.id desc');
        $totalCount = $LolSkill->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $LolSkill->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function editInfo($metadataType, $info, $userType, $userId, $basisId)
    {
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldInfo = [];

        $class = self::METADATA_TYPE_TO_CLASS[$metadataType];
        $operationClass = "";
        if ($class) {
            $operationClass = new $class;
        }
        if ($operationClass instanceof ActiveRecord) {

            // 插入或者更新操作
            if ($info["id"]) {
                // 更新操作
                $originInfo = $operationClass::find()->where(["id" => $info["id"]])->one();
                if (!$originInfo) {
                    throw new Exception("不存在对应的操作数据:" . $metadataType . $info["id"]);
                }
                $logOldInfo = $originInfo->toArray();
                $originInfo->setAttributes($info);
            } else {
                // 插入操作
                $originInfo = $operationClass;
                $originInfo->setAttributes($info);
            }
            if (!$originInfo->save()) {
                throw new BusinessException($operationClass->getErrors(), "操作失败,请检查提交或联系管理员");
            }

            $logNewInfo = $originInfo->toArray();

            $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
            if ($diffInfo['changed']) {
                OperationLogService::addLog($operationType,
                    Consts::RESOURCE_TYPE_PLAYER,
                    $logNewInfo['id'],
                    ["diff" => $diffInfo["diff"], "new" => $logNewInfo],
                    0,
                    $metadataType,
                    null,
                    $basisId,
                    $userType,
                    $userId
                );
            }
        } else {
            throw new Exception("不存在对应的操作类:" . $metadataType);
        }
    }

    public static function delInfo($metadataType, $metadataId, $userType, $userId, $basisId)
    {
        // 判断是否绑定，这里没有删除绑定

    }


}