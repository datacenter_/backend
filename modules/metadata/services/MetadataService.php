<?php

namespace app\modules\metadata\services;


use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\metadata\models\HeroSkill;
use app\modules\metadata\models\LolHero;
use app\modules\metadata\models\LolProp;
use app\modules\metadata\models\LolRune;
use app\modules\metadata\models\LolSkill;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\models\MetadataKogAbility;
use app\modules\metadata\models\MetadataKogChampion;
use app\modules\metadata\models\MetadataKogItem;
use app\modules\metadata\models\MetadataKogRune;
use app\modules\metadata\models\MetadataKogSummonerSpell;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataGroup;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataStage;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\db\Exception;

class MetadataService
{
    const METADATA_TYPE_TO_CLASS = [
        Consts::METADATA_TYPE_KOG_CHAMPION => MetadataKogChampion::class,
        Consts::METADATA_TYPE_KOG_ABILITY => MetadataKogAbility::class,
        Consts::METADATA_TYPE_KOG_ITEM => MetadataKogItem::class,
        Consts::METADATA_TYPE_KOG_RUNE => MetadataKogRune::class,
        Consts::METADATA_TYPE_KOG_SUMMONER_SPELL => MetadataKogSummonerSpell::class,

        Consts::METADATA_TYPE_LOL_ABILITY => MetadataLolAbility::class,
        Consts::METADATA_TYPE_LOL_CHAMPION => MetadataLolChampion::class,
        Consts::METADATA_TYPE_LOL_ITEM => MetadataLolItem::class,
        Consts::METADATA_TYPE_LOL_RUNE => MetadataLolRune::class,
        Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => MetadataLolSummonerSpell::class,
        Consts::METADATA_TYPE_DOTA2_ABILITY => MetadataDota2Ability::class,
        Consts::METADATA_TYPE_DOTA2_HERO => MetadataDota2Hero::class,
        Consts::METADATA_TYPE_DOTA2_ITEM => MetadataDota2Item::class,
        Consts::METADATA_TYPE_DOTA2_TALENT => MetadataDota2Talent::class,
        Consts::METADATA_TYPE_CSGO_MAP => MetadataCsgoMap::class,
        Consts::METADATA_TYPE_CSGO_WEAPON => MetadataCsgoWeapon::class,
        Consts::TOURNAMENT_GROUP => StandardDataGroup::class,
        Consts::TOURNAMENT_STAGE => StandardDataStage::class,
        Consts::RESOURCE_TYPE_TEAM => StandardDataTeam::class,
        Consts::RESOURCE_TYPE_MATCH => StandardDataMatch::class,
        Consts::RESOURCE_TYPE_PLAYER => StandardDataPlayer::class,
        Consts::RESOURCE_TYPE_TOURNAMENT => StandardDataTournament::class,
    ];

    public static function getPropList($params)
    {
        $LolProp = LolProp::find()->alias('p');

        if (isset($params['prop_status']) && !empty($params['prop_status'])) {
            $LolProp->andWhere(['d.prop_status' => $params['prop_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $LolProp->andWhere(['p.id' => $params['id']]);
        }
        if (isset($params['prop_name']) && !empty($params['prop_name'])) {
            $LolProp->andfilterwhere(['like', 'p.prop_name', $params['prop_name']]);
        }
        if (isset($params['official_id']) && !empty($params['official_id'])) {
            $LolProp->andWhere(['p.official_id' => $params['official_id']]);
        }
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $LolProp->andWhere(['>=', 'p.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $LolProp->andWhere(['<', 'p.modified_at', $end]);
        }

        $LolProp = $LolProp->orderBy('p.id desc');
        $totalCount = $LolProp->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $LolProp->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }
    public static function getLolPropDetail($Id)
    {
        $prop = MetadataLolItem::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'prop' => $prop,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_LOL_ITEM)
        ];
    }

    public static function getHeroList($type)
    {
        if ($type == 1) {
            return MetadataLolChampion::find()
                ->where(['and',
                    ['=','deleted',2],
                    ['=','flag',1],
                ])->orderBy('name asc')
                ->asArray()
                ->all();
        } else {
            return MetadataDota2Hero::find()
                ->where(['and',
                    ['=','deleted',2],
                    ['=','flag',1],
                ])->orderBy('name asc')
                ->asArray()
                ->all();
        }
    }

    public static function getTalent()
    {
        return MetadataDota2Talent::find()->where(['flag' => 1])->asArray()->all();
    }

    public static function getHeroInfo($id)
    {
        $lolHeroInfo = LolHero::find()->where(['id' => $id])->asArray()->one();
        $lolHeroInfo['hero_skill_info'] = HeroSkill::find()->where(['hero_id' => $id, 'type' => 1])->asArray()->all();

        return $lolHeroInfo;
    }

    public static function getRuneList($params)
    {
        $lolRune = LolRune::find()->alias('r');

        if (isset($params['rune_status']) && !empty($params['rune_status'])) {
            $lolRune->andWhere(['r.rune_status' => $params['rune_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $lolRune->andWhere(['r.id' => $params['id']]);
        }
        if (isset($params['rune_name']) && !empty($params['rune_name'])) {
            $lolRune->andfilterwhere(['like', 'r.rune_name', $params['rune_name']]);
        }
        if (isset($params['type_name']) && !empty($params['type_name'])) {
            $lolRune->andfilterwhere(['like', 'r.type_name', $params['type_name']]);
        }
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $lolRune->andWhere(['>=', 'r.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $lolRune->andWhere(['<', 'r.modified_at', $end]);
        }

        $lolRune = $lolRune->orderBy('r.id desc');
        $totalCount = $lolRune->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $lolRune->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function getSkillList($params)
    {
        $LolSkill = LolSkill::find()->alias('s');

        if (isset($params['skill_status']) && !empty($params['skill_status'])) {
            $LolSkill->andWhere(['s.skill_status' => $params['skill_status']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $LolSkill->andWhere(['s.id' => $params['id']]);
        }
        if (isset($params['skill_name']) && !empty($params['skill_name'])) {
            $LolSkill->andfilterwhere(['like', 's.skill_name', $params['skill_name']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $LolSkill->andWhere(['>=', 's.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $LolSkill->andWhere(['<', 's.modified_at', $end]);
        }

        $LolSkill = $LolSkill->orderBy('s.id desc');
        $totalCount = $LolSkill->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $LolSkill->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        return ['list' => $model, 'total' => $totalCount];
    }

    public static function editInfo($metadataType, $info, $userType, $userId, $gameId = null,$basisId = 0 ,$originId = 0)
    {
        $logOldInfo = [];
        $operationClass = self::getObjByType($metadataType);
        // 插入或者更新操作
        if (isset($info["id"])) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            // 更新操作
            $originInfo = $operationClass::find()->where(["id" => $info["id"]])->one();
            if (!$originInfo) {
                throw new Exception("不存在对应的操作数据:" . $metadataType . $info["id"]);
            }
            $logOldInfo = $originInfo->toArray();
            $originInfo->setAttributes($info);
            if (!$originInfo->save()) {
//            print_r($originInfo->getErrors());
                throw new BusinessException($originInfo->getErrors(), "操作失败,请检查提交或联系管理员");
            }
        } else {
            $operationType = OperationLogService::OPERATION_TYPE_ADD;
            // 插入操作
            $originInfo = clone $operationClass;
            if (!isset($info['flag'])) {
                $info['flag'] = 1; // 默认未删除
            }
            $originInfo->setAttributes($info);
            if (!$originInfo->save()) {
//            print_r($originInfo->getErrors());
                throw new BusinessException($originInfo->getErrors(), "操作失败,请检查提交或联系管理员");
            }

            // 创建关系
            if ($basisId != 0) {
                \Yii::$app->db->createCommand()->batchInsert('data_change_config', ['basis_id', 'resource_type', 'resource_id', 'origin_id'],
                    [[$basisId, $metadataType, $originInfo->id, $originId]])->execute();
            }
            //新建的时候初始化默认配置
            if($gameId){
                UpdateConfigService::initResourceUpdateConfig($originInfo["id"],$metadataType,$gameId);
            }
        }
        if ($basisId) {
            DataTodoService::setDealStatus($basisId, Consts::TODO_DEAL_STATUS_DONE);
        }

        $logNewInfo = $originInfo->toArray();

        $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                $metadataType,
                $logNewInfo['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewInfo],
                0,
                $metadataType,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $logNewInfo;
    }

    public static function del($metadataType, $metadataId, $userType, $userId, $basisId = 0)
    {
        // 判断是否绑定，这里没有删除绑定
        $bindings = self::getBindingInfo($metadataType, $metadataId);
        if ($bindings && count($bindings)) {
            throw new BusinessException($bindings, "还有关联关系，请检查");
        }
        self::editInfo($metadataType, ['id' => $metadataId, 'deleted' => 1, 'deleted_at' => date('Y-m-d H:i:s')], $userType, $userId, null, $basisId);
        OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
            $metadataType,
            $metadataId,
            ["diff" => [], "new" => []],
            0,
            Consts::TAG_TYPE_BASE_DATA,
            null,
            0,
            Consts::USER_TYPE_ADMIN,
            $userId
        );
    }

    public static function unDel($metadataType, $metadataId, $userType, $userId, $basisId = 0)
    {
        OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
            $metadataType,
            $metadataId,
            ["diff" => [], "new" => []],
            0,
            Consts::TAG_TYPE_BASE_DATA,
            null,
            0,
            Consts::USER_TYPE_ADMIN,
            $userId
        );
        return self::editInfo($metadataType, ['id' => $metadataId, 'deleted' => 2, 'deleted_at' => null], $userType, $userId,null, $basisId);
    }

    public static function getBindingInfo($metadataType, $metadataId)
    {
        // 反向查关联关系
        $rels = DataStandardMasterRelation::find()->where(['resource_type' => $metadataType, 'master_id' => $metadataId])->asArray()->all();
        $ids = array_column($rels, 'standard_id');
        $standards = StandardDataMetadata::find()->where(['in', 'id', $ids])->asArray()->all();
        return $standards;
    }

    public static function getList($metadataType, $params)
    {
        $tableName = 'metadata_' . $metadataType;
//        if (!empty($params['date_end'])) {
//            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
//            $params['date_end'] = $end;
//        }
        // 生成where条件
        $whereConfig = [

            "state" => [
                'type' => '=',
                'key' => $tableName . '.state'
            ],
            "external_id" => [
                'type' => "=",
                'key' => $tableName . '.external_id'
            ],
//            "external_name" => [
//                'type' => "like",
//                'key' => $tableName . '.external_name'
//            ],
//            "date_begin" => [
//                'type' => ">=",
//                'key' => $tableName . '.modified_at'
//            ],
//            "date_end" => [
//                'type' => "<",
//                'key' => $tableName . '.modified_at'
//            ],

            "id" => [ 'type' => "=", 'key' => $tableName . '.id'],
            "map_type" => [ 'type' => "=", 'key' => $tableName . '.map_type'],

            "kind" => [ 'type' => "=", 'key' => $tableName . '.kind']
        ];
        $where = DbHelper::getWhere($whereConfig, $params);
        $operationClass = self::getObjByType($metadataType);
        $q = $operationClass::find()->where(array_merge(['and'], $where));
        // 时间筛选
        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $q->andWhere(['>=', "$tableName.modified_at", $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $q->andWhere(['<', "$tableName.modified_at", $end]);
        }

        if ($metadataType == Consts::METADATA_TYPE_LOL_ABILITY) {
            $q->select("$tableName.*,lc.name_cn as hero_name,lc.title_cn as hero_title");
            $q->leftJoin("metadata_lol_champion as lc", "lc.id = $tableName.champion_id");
            $heroName = 'lc';
        }

        if ($metadataType == Consts::METADATA_TYPE_KOG_ABILITY) {
            $q->select("$tableName.*,lc.name_cn as hero_name,lc.title_cn as hero_title");
            $q->leftJoin("metadata_kog_champion as lc", "lc.id = $tableName.champion_id");
            $heroName = 'lc';
        }

        if ($metadataType == Consts::METADATA_TYPE_DOTA2_TALENT) {
            $q->select("$tableName.*,dh.name_cn as hero_name,dh.title_cn as hero_title");
            $q->leftJoin("metadata_dota2_hero as dh", "dh.id = $tableName.hero_id");
        }
        if ($metadataType == Consts::METADATA_TYPE_DOTA2_ABILITY) {
            $q->select("$tableName.*,dh.name_cn as hero_name,dh.title_cn as hero_title");
            $q->leftJoin("metadata_dota2_hero as dh", "dh.id = $tableName.hero_id");
            $heroName = 'dh';
        }
        if (isset($params['title']) && !empty($params['title'])) {
            $q->andWhere(['or', ['like', "$tableName.title", $params['title']], ['like', "$tableName.title_cn", $params['title']]]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $q->andWhere(["$tableName.id" => $params['id']]);
        }

        if (isset($params['talent']) && !empty($params['talent'])) {
            $q->andWhere(['or', ['like', "$tableName.talent", $params['talent']], ['like', "$tableName.talent_cn", $params['talent']]]);
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $q->andWhere(['or', ['like', "$tableName.name", $params['name']], ['like', "$tableName.name_cn", $params['name']]]);
        }
        if (isset($params['hero_title']) && !empty($params['hero_title'])) {
            $q->andWhere(['or', ['like', "$heroName.title", $params['hero_title']], ['like', "$heroName.title_cn", $params['hero_title']]]);
        }

        if (isset($params['hero_name']) && !empty($params['hero_name'])) {
            $q->andWhere(['or', ['like', "$heroName.name", $params['hero_name']], ['like', "$heroName.name_cn", $params['hero_name']]]);
        }
        if (isset($params['path_name']) && !empty($params['path_name'])) {
            $q->andWhere(['or', ['like', "$tableName.path_name", $params['path_name']], ['like', "$tableName.path_name_cn", $params['path_name']]]);
        }

        if (isset($params['map_name']) && !empty($params['map_name'])) {
            $q->andWhere(['or', ['like', "$tableName.name", $params['map_name']], ['like', "$tableName.name_cn", $params['map_name']]]);
        }
        if (isset($params['weapon_name']) && !empty($params['weapon_name'])) {
            $q->andWhere(['like', "$tableName.name", $params['weapon_name']]);
        }

        if (isset($params['external_name']) && !empty($params['external_name'])) {
            $q->andWhere(['like',"$tableName.external_name",$params['external_name']]);
        }

        if (isset($params['slug']) && !empty($params['slug'])) {
            $q->andWhere(['like',"$tableName.slug",$params['slug']]);
        }

        $count = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);

        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->orderBy("$tableName.modified_at desc")->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        $m = $q->createCommand()->getSql();
        $resourceIds = array_column($list, 'id');
        $operationInfo = OperationLogService::operationCounts($metadataType, $resourceIds);
        foreach ($list as &$val) {
            $val['operation'] = $operationInfo[$val['id']];
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 66, 66, 'ali');

            if ($metadataType == 'csgo_map') {
                $val['square_image'] = ImageConversionHelper::showFixedSizeConversion($val['square_image'], 42,84,  'ali');
            }
            if ($metadataType == 'csgo_weapon') {
//                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 28, 0, 'ali');
                $val['image']  = ImageConversionHelper::showMfitSizeConversion($val['image'],28,0);
            }
            if ($metadataType == 'dota2_hero') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 46,76,  'ali');
            }
            if ($metadataType == 'dota2_ability') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 32, 32, 'ali');
            }

            if ($metadataType == 'dota2_item') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 28,38,  'ali');
            }

            if ($metadataType == 'lol_champion') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 54, 54, 'ali');
            }
            if ($metadataType == 'lol_ability') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 32, 32, 'ali');
            }

            if ($metadataType == 'lol_item') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 32, 32, 'ali');
            }
            if ($metadataType == 'lol_summoner_spell') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 32, 32, 'ali');
            }

            if ($metadataType == 'lol_rune') {
                $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 32, 32, 'ali');
            }

            $val['binding_info'] = TeamService::actionGetBingInfo($val['id'],$metadataType);
            $val['update_config'] = UpdateConfigService::getResourceUpdateConfigDetail($val['id'], $metadataType);

        }
        return ['list' => $list, 'total' => $count];

    }

    public static function getDetail($metadataType, $metadataId)
    {
        $mdObj = self::getObjByType($metadataType);
        $mdobjData = $mdObj::find()->where(['id' => $metadataId])->asArray()->one();
        $data_config = UpdateConfigService::getResourceUpdateConfigDetail($metadataId,$metadataType);
        $mdobjData['data_config'] = $data_config;
        return  $mdobjData;
    }

    public static function getLolHeroDetail($heroId)
    {
        $champion = MetadataLolChampion::find()->where(['id' => $heroId])->one();
        $ability = MetadataLolAbility::find()->where(['champion_id' => $heroId,'flag' => 1])->asArray()->all();
        return [
            'champion' => $champion,
            'ability' => $ability,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($heroId,Consts::METADATA_TYPE_LOL_CHAMPION)
        ];
    }
    public static function getLolAbilityDetail($Id)
    {
        $ability = MetadataLolAbility::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'ability' => $ability,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_LOL_ABILITY)
        ];
    }

    public static function getLolSkillDetail($Id)
    {
        $skill = MetadataLolSummonerSpell::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'skill' => $skill,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_LOL_SUMMONER_SPELL)
        ];
    }

    public static function getLolRuneDetail($Id)
    {
        $rune = MetadataLolRune::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'rune' => $rune,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_LOL_RUNE)
        ];
    }

    public static function getDota2HeroDetail($heroId)
    {
        $champion = MetadataDota2Hero::find()->where(['id' => $heroId])->one();
        $talent = MetadataDota2Talent::find()->where(['hero_id' => $heroId,'flag' => 1])->asArray()->all();
        $ability = MetadataDota2Ability::find()->where(['hero_id' => $heroId,'flag' => 1])->asArray()->all();
        $t = [];
        foreach ($talent as $value) {
            $t[$value['level']][] = $value;
        }
        return [
            'champion' => $champion,
            'ability' => $ability,
            'talent' => $t,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($heroId,Consts::METADATA_TYPE_DOTA2_HERO)
        ];
    }
    //dota2技能管理详情
    public static function getDotaAbilityDetail($id)
    {
        $data = MetadataDota2Ability::find()->where(['id' => $id,'flag' => 1])->one();
        return [
            'ability' => $data
        ];
    }

    public static function getKogHeroDetail($heroId)
    {
        $champion = MetadataKogChampion::find()->where(['id' => $heroId])->one();
        $ability = MetadataKogAbility::find()->where(['champion_id' => $heroId,'flag' => 1])->asArray()->all();
        return [
            'champion' => $champion,
            'ability' => $ability,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($heroId,Consts::METADATA_TYPE_KOG_CHAMPION)
        ];
    }

    public static function getKogAbilityDetail($Id)
    {
        $ability = MetadataKogAbility::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'ability' => $ability,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_KOG_ABILITY)
        ];
    }

    public static function getKogPropDetail($Id)
    {
        $prop = MetadataKogItem::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'prop' => $prop,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_KOG_ITEM)
        ];
    }

    public static function getKogSkillDetail($Id)
    {
        $skill = MetadataKogSummonerSpell::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'skill' => $skill,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_KOG_SUMMONER_SPELL)
        ];
    }

    public static function getKogRuneDetail($Id)
    {
        $rune = MetadataKogRune::find()->where(['id' => $Id,'flag' => 1])->one();
        return [
            'rune' => $rune,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($Id,Consts::METADATA_TYPE_KOG_RUNE)
        ];
    }

    /**
     * @param $metadataType
     * @return ActiveRecord
     * @throws Exception
     */
    public static function getObjByType($metadataType)
    {
        if(!isset(self::METADATA_TYPE_TO_CLASS[$metadataType])){
            throw new Exception('不存在对应的metadata操作类'.$metadataType);
        }
        $class = self::METADATA_TYPE_TO_CLASS[$metadataType];
        $operationClass = "";
        if ($class) {
            $operationClass = new $class;
        }
        if ($operationClass instanceof ActiveRecord) {
            return $operationClass;
        }
        throw new Exception("不存在对应的操作类:" . $metadataType);
    }
}