<?php
/**
 *
 */

namespace app\modules\operation\controllers;


use app\controllers\RestController;
use app\modules\common\services\OperationLogService;

class LogController extends RestController
{
    public function actionList()
    {
        $params=$this->pGet();
        return OperationLogService::getOperationLog($params);
    }
}