<?php
namespace app\modules\org\controllers;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use app\modules\org\services\ClanService;
use app\modules\common\models\EnumCountry;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

/**
 *
 */

class ClanController extends WithTokenAuthController
{
    public function actionAdd()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return ClanService::add($attributes,Consts::USER_TYPE_ADMIN,$this->user->id);
    }

    public function actionDel()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $status = \Yii::$app->getRequest()->post('status');
        if(isset($status) && $status == 1){
            $team = Team::find()->where(['organization' => $id])->one();
            if ($team && !empty($team)) {
                throw new BusinessException([],'绑定有战队的俱乐部不能删除!');
            }
            return Common::deletedMaster($id,Consts::RESOURCE_TYPE_CLAN,"");
        }
        if (isset($status) && $status == Clan::NO){
            return Common::recoverMaster($id,Consts::RESOURCE_TYPE_CLAN,"");
        }

//        if ($status == Clan::NO) {
//            $clan = Clan::find()->where(['id' => $id])->one();
//            $old['base'] = [
//                'id'=>$id,
//                'deleted'=>json_encode($clan['deleted']),
//            ];
//            $clan->deleted = $status;
//            $clan->deleted_at = null;
//            if ($clan->update()) {
//                $new['base'] =[
//                    'id'=>$id,
//                    'deleted'=>json_encode($clan['deleted']),
//                ];
//                $diffInfo = Common::getDiffInfo($old,$new);
//                if($diffInfo['changed']){
//                    OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
//                        Consts::RESOURCE_TYPE_CLAN,
//                        $id,
//                        ["diff" => $diffInfo["diff"], "new" => $new],
//                        0,
//                        Consts::TAG_TYPE_BASE_DATA,
//                        null,
//                        0,
//                        Consts::USER_TYPE_ADMIN,
//                        $this->user->id
//                    );
//
//                    return ['id' => $id, 'msg' => '恢复成功'];
//                }
//            }
//        }
//        // TODO 绑定有战队的俱乐部不能删除
//        $team = Team::find()->where(['organization' => $id])->one();
//        if ($team && !empty($team)) {
//            throw new BusinessException([],'绑定有战队的俱乐部不能删除!');
//        }
//
//        $clan = Clan::find()->where(['id' => $id])->one();
//        $old['base'] = [
//            'id'=>$id,
//            'deleted'=>json_encode($clan['deleted']),
//        ];
//        $clan->deleted = $status;
//        $clan->deleted_at = date('Y-m-d H:i:s');
//        if ($clan->update()) {
//            $new['base'] = [
//                'id'=>$id,
//                'deleted'=>json_encode($clan['deleted']),
//            ];
//            $diffInfo = Common::getDiffInfo($old,$new);
//            if($diffInfo['changed']){
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                    Consts::RESOURCE_TYPE_CLAN,
//                    $id,
//                    ["diff" => $diffInfo["diff"], "new" => $new],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//                return ['id' => $id, 'msg' => '删除成功'];
//            }
//        }
//        throw new BusinessException($clan->getErrors(),'删除失败');
    }


    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();

        return ClanService::getClanList($params);
    }

    public function actionGetCountry()
    {
       return EnumCountry::getCountry();
    }

    public function actionGetTeams()
    {
        $id = \Yii::$app->getRequest()->post('clan_id');

        $team = Team::find()->select('id as team_id,organization,game,country,image,name')->where(['organization' => $id])->asArray()->all();

        $country = array_column(EnumCountry::getCountry(),'name','id');
        $gameLogo = array_column(EnumService::getGameLogo(),'logo','id');

        foreach ($team as &$value) {
            $value['country_name'] = $country[$value['country']] ?? '';
            $value['game_image'] = $gameLogo[$value['game']] ?? '';
            $value['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'], 66, 66, 'ali');
        }

        return $team;
    }

    public function actionGetClanList()
    {
        return Clan::find()->select('id,name,full_name,short_name,alias')->where(['deleted' => Clan::NO])->orderBy('name asc')->all();
    }
    public function actionGetInfoById()
    {
        $prams=$this->pPost();
//        $data = Clan::find()->where(['id'=>$prams['id']])->asArray()->one();
        $data = Clan::find()->alias('c') ->select('c.*,ci.introduction as introduction,ci.introduction_cn as introduction_cn')->leftJoin('clan_introduction as ci', 'ci.clan_id = c.id')
            ->where(['c.id'=>$prams['id']])->asArray()->one();
        return $data;
    }
}