<?php
namespace app\modules\org\controllers;
use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\models\Clan;
use app\modules\org\models\Player;
use app\modules\org\models\PlayerTeamConfig;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\services\PlayerService;
use app\modules\org\services\TeamService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

/**
 *
 */

class PlayerController extends WithTokenAuthController
{
    public function actionAdd()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $clan = new Player();
        $clan->setAttributes($attributes);
        if ($clan->validate()) {
            return PlayerService::setPlayer($attributes,$this->user->id);
        } else {
            throw new BusinessException($clan->getErrors(),'参数信息错误');
        }
    }

    public function actionList()
    {
        //由之前的get传参改为post传参  因为url参数有特殊符号会丢失或者转移，导致无法搜索到，bug编号251
        $params = \Yii::$app->getRequest()->get();
//        $params = \Yii::$app->getRequest()->post();

        return PlayerService::getPlayerList($params);
    }

    public function actionInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        $info = Player::find()->alias('p')
            ->select('p.*,pb.*')
            ->leftJoin('player_base as pb', 'pb.player_id = p.id')
            ->where(['p.id' => $id])->asArray()->one();
        $info['data_config'] = UpdateConfigService::getResourceUpdateConfigDetail($id,'player');
        $info['player_config'] = PlayerTeamConfig::find()->where(['player_id' => $id])->asArray()->all();
        return $info;
    }

    public function actionDetail()
    {
        $id=$this->pGet('id');
        return PlayerService::getPlayer($id);
    }

    public function actionEdit()
    {
        $basisId=$this->pPost('todo_id');
        $params=$this->pPost();
        if(isset($params['deleted']) && $params['deleted'] == 1){
             Common::deletedMaster($params['id'],Consts::RESOURCE_TYPE_PLAYER,"");
        }
        if (isset($params['deleted']) && $params['deleted'] == 2){
            Common::recoverMaster($params['id'],Consts::RESOURCE_TYPE_PLAYER,"");
        }
        return PlayerService::setPlayer($params,Consts::USER_TYPE_ADMIN,\Yii::$app->getUser()->getId(),$basisId);
    }

    public function actionDel()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $status = \Yii::$app->getRequest()->post('status');
        if(isset($status) && $status == 1){
            //TODO 绑定有数据源的选手不能删除，绑定有战队的选手不能删除
            $data = PlayerService::checkBing('player',$id);
            if (!$data) {
                throw new BusinessException([],'绑定有数据源的选手不能删除！');
            }
            $team = TeamPlayerRelation::find()->select('team_id')->where(['player_id' => $id])->asArray()->one();
            if (!empty($team['team_id'])) {
                throw new BusinessException([],'已绑定战队不能删除！');
            }
            return Common::deletedMaster($id,Consts::RESOURCE_TYPE_PLAYER,"");
        }

        if (isset($status) && $status == 2){
            return Common::recoverMaster($id,Consts::RESOURCE_TYPE_PLAYER,"");
        }

//        if ($status == Clan::NO) {
//            $clan = Player::find()->where(['id' => $id])->one();
//            $old = [
//                'id'=>$id,
//                'deleted'=>json_encode($clan['deleted']),
//            ];
//            $clan->deleted = $status;
//            $clan->deleted_at = null;
//
//            if ($clan->update()) {
//                $new =[
//                    'id'=>$id,
//                    'deleted'=>json_encode($clan['deleted']),
//                ];
//
//                $diffInfo = Common::getDiffInfo($old,$new);
//
//                $teamByPlayerInfo = TeamPlayerRelation::find()
//                    ->select("team_player_relation.team_id as team_id")
//                    ->leftJoin('team', 'team.id=team_player_relation.team_id')
//                    ->where(["player_id" => $id])->asArray()->all();
//                if (!empty($teamByPlayerInfo)) {
//                    $teamByPlayerIds = json_encode(array_column($teamByPlayerInfo,'team_id'));
//                }else{
//                    $teamByPlayerIds = null;
//                }
//                $new['team_by_playerIds'] =  $teamByPlayerIds;
//
//                if($diffInfo['changed']) {
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
//                    Consts::RESOURCE_TYPE_PLAYER,
//                    $id,
//                    ["diff" => $diffInfo["diff"], "new" => $new],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//                return ['id' => $id, 'msg' => '恢复成功'];
//            }
//        }
//        }
//        //TODO 绑定有数据源的选手不能删除，绑定有战队的选手不能删除
//        $data = PlayerService::checkBing('player',$id);
//        if (!$data) {
//            throw new BusinessException([],'绑定有数据源的选手不能删除！');
//        }
//        $team = TeamPlayerRelation::find()->select('team_id')->where(['player_id' => $id])->asArray()->one();
//        if (!empty($team['team_id'])) {
//            throw new BusinessException([],'已绑定战队不能删除！');
//        }
//        $clan = Player::find()->where(['id' => $id])->one();
//        $old = [
//            'id'=>$id,
//            'deleted'=>json_encode($clan['deleted']),
//        ];
//        if(isset($clan['steam_id']) && $clan['steam_id']){
//            $clan['steam_id'] = (string)$clan['steam_id'];
//        }
//        $clan->deleted = $status;
//        $clan->deleted_at = date('Y-m-d H:i:s');
//        if ($clan->update()) {
//            $new = [
//                'id'=>$id,
//                'deleted'=>json_encode($clan['deleted']),
//            ];
//            $diffInfo = Common::getDiffInfo($old,$new);
//            if($diffInfo['changed']){
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                    Consts::RESOURCE_TYPE_PLAYER,
//                    $id,
//                    ["diff" => $diffInfo["diff"], "new" => $new],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//                return ['id' => $id, 'msg' => '删除成功'];
//        }
//        throw new BusinessException($clan->getErrors(),'删除失败');
//    }
    }

    // 搜索选手

    public function actionSearchPlayer()
    {
        $name = \Yii::$app->getRequest()->get('keyword');
        $gameId = \Yii::$app->getRequest()->get('game_id');

        return PlayerService::search($name,$gameId);
    }
    public function actionCheckPlayer() {
        $prams = $this->pGet();
        return PlayerService::checkPlayer($prams);

    }
}