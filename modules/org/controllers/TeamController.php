<?php

namespace app\modules\org\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\services\PlayerService;
use app\modules\org\services\TeamService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\TournamentGroup;
use app\rest\exceptions\BusinessException;
use app\modules\tournament\models\TournamentTeamRelation;
/**
 *
 */
class TeamController extends WithTokenAuthController
{
    public function actionEdit()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $baseId=$this->pPost('todo_id');
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::RESOURCE_TYPE_TEAM,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::RESOURCE_TYPE_TEAM,"");
        }

        return TeamService::setTeam($attributes, Consts::USER_TYPE_ADMIN, $this->user->id, $baseId);
    }

    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();
        return TeamService::getTeamList($params);
    }

    public function actionDetail()
    {
        $teamId = $this->pGet('id');
        return TeamService::getTeam($teamId);
    }

    public function actionInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        $info = Team::find()->alias('t')
            ->select('t.*,ti.*')
            ->leftJoin('team_introduction as ti', 'ti.team_id = t.id')
            ->where(['t.id' => $id])->asArray()->one();
        $info['player'] = TeamPlayerRelation::find()
            ->alias('tr')
            ->select(['tr.*','p.*','country.name as country_name','country.e_name as country_e_name'])
            ->leftJoin('player as p', 'p.id = tr.player_id')
            ->leftJoin('enum_country as country', 'country.id = p.player_country')
            ->where(['tr.team_id' => $id])->asArray()->all();
        $info['data_config'] = UpdateConfigService::getResourceUpdateConfigDetail($id, 'team');
        $info['image'] = ImageConversionHelper::showFixedSizeConversion($info['image'], 66, 66);
        return $info;
    }

    public function actionDel()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $status = \Yii::$app->getRequest()->post('status');

        if(isset($status) && $status == 1){
            //TODO 绑定有数据源的战队不能删除，绑定有俱乐部和选手的战队不能删除
            $data = PlayerService::checkBing('team', $id);
            if (!$data) {
                throw new BusinessException([], '绑定有数据源的战队不能删除！');
            }
            $team = Team::find()->select('organization')->where(['id' => $id])->asArray()->one();
            if (!empty($team['organization'])) {
                throw new BusinessException([], '已绑定俱乐部不能删除！');
            }

            $tpr = TeamPlayerRelation::find()->where(['team_id' => $id])->all();
            if ($tpr && !empty($tpr)) {
                throw new BusinessException([], '已有选手的战队不能删除！');
            }
            $is_tournament = TournamentTeamRelation::find()->where(['team_id' => $id])->all();
            if($is_tournament && !empty($is_tournament)){
                throw new BusinessException([],'已有赛事的战队不能删除');
            }
            $is_match = Match::find()->where(['or',
                ["=", "team_1_id", $id],
                ["=", "team_2_id", $id],
            ])->all();
            if($is_match && !empty($is_match)){
                throw  new BusinessException([],'已有比赛的战队不能删除');
            }
            return Common::deletedMaster($id,Consts::RESOURCE_TYPE_TEAM,"");
        }

        if (isset($status) && $status == 2){
            return Common::recoverMaster($id,Consts::RESOURCE_TYPE_TEAM,"");
        }

        //战队下增量更新（选手） $logNewTeam 加一个当前选手字段
//        $teamPlayer = TeamPlayerRelation::find()
//            ->alias('tr')
//            ->select(['tr.*','p.*','country.name as country_name','country.e_name as country_e_name'])
//            ->leftJoin('player as p', 'p.id = tr.player_id')
//            ->leftJoin('enum_country as country', 'country.id = p.player_country')
//            ->where(['tr.team_id' => $id])->asArray()->all();
//        if (!empty($teamPlayer)){
//            $playeids = json_encode(array_column($teamPlayer,'player_id'));
//        }else{
//            $playeids = "[]";
//        }
//
//
//        if ($status == Clan::NO) {
//            $clan = Team::find()->where(['id' => $id])->one();
//            $old = [
//                'id'=>$id,
//                'info'=>json_encode($clan['deleted']),
//            ];
//            $clan->deleted = $status;
//            $clan->deleted_at = null;
//
//            if ($clan->update()) {
//                $new =[
//                    'id'=>$id,
//                    'info'=>json_encode($clan['deleted']),
//                ];
//                $diffInfo = Common::getDiffInfo($old,$new);
//                $new['playes'] = $playeids;
//                if($diffInfo['changed']) {
//                    OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
//                        Consts::RESOURCE_TYPE_TEAM,
//                        $id,
//                        ["diff" => $diffInfo["diff"], "new" => $new],
//                        0,
//                        Consts::TAG_TYPE_BASE_DATA,
//                        null,
//                        0,
//                        Consts::USER_TYPE_ADMIN,
//                        $this->user->id
//                    );
//                    return ['id' => $id, 'msg' => '恢复成功'];
//                }
//            }
//        }
//        //TODO 绑定有数据源的战队不能删除，绑定有俱乐部和选手的战队不能删除
//        $data = PlayerService::checkBing('team', $id);
//        if (!$data) {
//            throw new BusinessException([], '绑定有数据源的战队不能删除！');
//        }
//        $team = Team::find()->select('organization')->where(['id' => $id])->asArray()->one();
//        if (!empty($team['organization'])) {
//            throw new BusinessException([], '已绑定俱乐部不能删除！');
//        }
//
//        $tpr = TeamPlayerRelation::find()->where(['team_id' => $id])->all();
//        if ($tpr && !empty($tpr)) {
//            throw new BusinessException([], '已有选手的战队不能删除！');
//        }
//        $is_tournament = TournamentTeamRelation::find()->where(['team_id' => $id])->all();
//        if($is_tournament && !empty($is_tournament)){
//            throw new BusinessException([],'已有赛事的战队不能删除');
//        }
//        $is_match = Match::find()->where(['or',
//              ["=", "team_1_id", $id],
//              ["=", "team_2_id", $id],
//            ])->all();
//        if($is_match && !empty($is_match)){
//            throw  new BusinessException([],'已有比赛的战队不能删除');
//        }
//        $clan = Team::find()->where(['id' => $id])->one();
//        $old = [
//            'id'=>$id,
//            'deleted'=>json_encode($clan['deleted']),
//        ];
//        $clan->deleted = $status;
//        $clan->deleted_at = date('Y-m-d H:i:s');
//        if ($clan->update()) {
//            $new = [
//                'id'=>$id,
//                'deleted'=>json_encode($clan['deleted']),
//            ];
//            $diffInfo = Common::getDiffInfo($old,$new);
//            if($diffInfo['changed']){
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                    Consts::RESOURCE_TYPE_TEAM,
//                    $id,
//                    ["diff" => $diffInfo["diff"], "new" => $new],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//                return ['id' => $id, 'msg' => '删除成功'];
//            }
//        }
//        throw new BusinessException($clan->getErrors(), '删除失败');
    }

    // 搜索战队

    public function actionSearchTeam()
    {
        $name = \Yii::$app->getRequest()->get('keyword');
        $gameId = \Yii::$app->getRequest()->get('game_id');

        return TeamService::search($name, $gameId);
    }

    //获取战队快照
    public function actionGetTeamsInfo(){
        $teamIds = $this->pGet('team_ids');
        $game_id = $this->pGet('game_id');
        return TeamService::getTeamsInfo($teamIds,$game_id);
    }
}