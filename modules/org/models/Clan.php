<?php

namespace app\modules\org\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan".
 *
 * @property int $id 主键
 * @property string $clan_id 俱乐部唯一标识
 * @property string $name 英文名称
 * @property string $full_name 英文全称
 * @property string $short_name 英文简称
 * @property string $alias 别名
 * @property int $logo_id LOGO主键ID
 * @property int $introduction_id 介绍信息主键ID
 * @property int $country 国家ID
 * @property int $deleted 删除标识 1:删除  0:未删除
 * @property string $seo_url SEO URL
 * @property string $created_at 创建时间
 * @property string $modified_at 修改时间
 */
class Clan extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'name'], 'required'],
            [['introduction_id', 'country', 'deleted'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['short_name'], 'string', 'max' => 200],
            [['name', 'full_name','slug'], 'string', 'max' => 100],
            [['alias', 'seo_url', 'image'], 'string', 'max' => 300]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'name' => '俱乐部名称',
            'full_name' => '英文全称',
            'short_name' => '英文简称',
            'alias' => '别名',
            'image' => 'LOGO',
            'introduction_id' => '介绍信息主键ID',
            'country' => '国家ID',
            'deleted' => '删除标识 1:删除  0:未删除',
            'seo_url' => 'SEO URL',
            'created_at' => '创建时间',
            'modified_at' => '修改时间',
        ];
    }
}
