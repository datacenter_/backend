<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "clan_hot_order".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部ID
 * @property int $order 排序
 * @property int $deleted 删除标识 1:删除 0:未删除
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 * @property string|null $deleted_at 删除时间
 */
class ClanHotOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clan_hot_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clan_id', 'order'], 'required'],
            [['clan_id', 'order', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'clan_id' => '俱乐部ID',
            'order' => '排序',
            'deleted' => '删除标识 1:删除 0:未删除',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
            'deleted_at' => '删除时间',
        ];
    }
}
