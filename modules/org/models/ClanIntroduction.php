<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class ClanIntroduction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clan_introduction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clan_id'], 'integer'],
            [['introduction', 'introduction_cn'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'clan_id' => '俱乐部表主键ID',
            'introduction' => '英文介绍',
            'introduction_cn' => '中文介绍',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
        ];
    }
}
