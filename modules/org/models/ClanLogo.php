<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "club_logo".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部ID
 * @property string $logo 图片地址
 * @property int $img_ver 版本
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class ClanLogo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'club_logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clan_id'], 'required'],
            [['clan_id', 'img_ver'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['logo'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'clan_id' => '俱乐部ID',
            'logo' => '图片地址',
            'img_ver' => '版本',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
        ];
    }
}
