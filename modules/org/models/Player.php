<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string|null $nick_name 选手名称
 * @property int $game_id 游戏id
 * @property int|null $player_country 选手国家
 * @property string|null $name 真实姓名英文
 * @property string|null $name_cn 真实姓名中文
 * @property string|null $image
 * @property string|null $slug
 * @property int|null $role 角色位置
 * @property int $deleted 是否删除:1.删除,2.未删除
 * @property int|null $cuser
 * @property string $created_at
 * @property string|null $modified_at
 * @property string|null $deleted_at
 * @property string|null $steam_id
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nick_name','game_id'], 'required'],
            [['game_id', 'player_country', 'role', 'deleted', 'cuser'], 'integer'],
            [['created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn'], 'string'],
            [['image'], 'string', 'max' => 300],
            [['slug', 'steam_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick_name' => 'Nick Name',
            'game_id' => 'Game ID',
            'player_country' => 'Player Country',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'image' => 'Image',
            'slug' => 'Slug',
            'role' => 'Role',
            'deleted' => 'Deleted',
            'cuser' => 'Cuser',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
            'steam_id' => 'Steam ID',
        ];
    }
}
