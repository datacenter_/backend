<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "club_logo".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部ID
 * @property string $logo 图片地址
 * @property int $img_ver 版本
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class PlayerBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['player_id'], 'integer'],
            [['steam_id', 'birthday', 'introduction', 'introduction_cn'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
