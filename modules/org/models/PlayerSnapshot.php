<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string|null $nick_name 选手名称
 * @property int $game_id 游戏id
 * @property int|null $player_country 选手国家
 * @property string|null $name 真实姓名英文
 * @property string|null $name_cn 真实姓名 英文
 * @property string|null $logo
 * @property int|null $deleted 0-恢复 1-删除
 * @property int|null $cuser
 * @property string $created_at
 * @property string|null $modified_at
 */
class PlayerSnapshot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id'], 'required'],
            [['game_id', 'player_country', 'deleted', 'cuser','role','player_id','relation_id'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['nick_name'], 'string', 'max' => 200],
            [['name', 'name_cn','slug'], 'string', 'max' => 200],
            [['image'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick_name' => '选手名称',
            'game_id' => '游戏id',
            'player_country' => '选手国家',
            'name' => '真实姓名英文',
            'name_cn' => '真实姓名 英文',
            'logo' => 'Logo',
            'deleted' => '0-恢复 1-删除',
            'cuser' => 'Cuser',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'birthday' => '生日',
        ];
    }
}
