<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "player_team_config".
 *
 * @property int $id
 * @property int|null $team_id
 * @property int|null $position_id
 * @property int|null $status_id
 * @property int|null $player_id
 */
class PlayerTeamConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player_team_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'position_id', 'status_id', 'player_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'position_id' => 'Position ID',
            'status_id' => 'Status ID',
            'player_id' => 'Player ID',
        ];
    }
}
