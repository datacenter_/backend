<?php

namespace app\modules\org\models;

use app\modules\common\models\OperationLog;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\services\TeamService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan".
 *
 * @property int $id 主键
 * @property string $clan_id 俱乐部唯一标识
 * @property string $name 英文名称
 * @property string $full_name 英文全称
 * @property string $short_name 英文简称
 * @property string $alias 别名
 * @property int $logo_id LOGO主键ID
 * @property int $introduction_id 介绍信息主键ID
 * @property int $country 国家ID
 * @property int $deleted 删除标识 1:删除  0:未删除
 * @property string $seo_url SEO URL
 * @property string $created_at 创建时间
 * @property string $modified_at 修改时间
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['name','game','image'], 'required'],
            [['game'], 'required'],
            [['organization', 'game', 'country', 'cuser','deleted'], 'integer'],
            [['full_name', 'name', 'short_name','slug','alias'], 'string', 'max' => 180],
            [['modified_at','deleted_at'], 'string'],
            [['image'], 'string', 'max' => 300]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }


}