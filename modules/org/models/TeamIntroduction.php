<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "team_introduction".
 *
 * @property int $id
 * @property int|null $team_id
 * @property string|null $steam_id steamid 
 * @property int|null $region 所属赛区
 * @property string|null $world_ranking 世界排名
 * @property string|null $ago_30 世界前30周
 * @property string|null $total_earnings 获得奖金
 * @property string|null $average_player_age 平均年龄
 * @property string|null $history_players 历史选手
 * @property string|null $create_team_date 战队创建日期
 * @property string|null $close_team_date 战队解散日期
 * @property string|null $introduction 英文
 * @property string|null $introduction_cn 中文
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class TeamIntroduction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_introduction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'region'], 'integer'],
            [['create_team_date', 'close_team_date', 'created_at', 'modified_at'], 'safe'],
            [['introduction', 'introduction_cn'], 'string'],
            [['steam_id'], 'string', 'max' => 100],
            [['world_ranking', 'ago_30', 'total_earnings', 'average_player_age'], 'string', 'max' => 50],
            [['history_players'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'steam_id' => 'Steam ID',
            'region' => 'Region',
            'world_ranking' => 'World Ranking',
            'ago_30' => 'Ago 30',
            'total_earnings' => 'Total Earnings',
            'average_player_age' => 'Average Player Age',
            'history_players' => 'History Players',
            'create_team_date' => 'Create Team Date',
            'close_team_date' => 'Close Team Date',
            'introduction' => 'Introduction',
            'introduction_cn' => 'Introduction Cn',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
