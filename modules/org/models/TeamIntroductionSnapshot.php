<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class TeamIntroductionSnapshot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_introduction_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region','team_id'], 'integer'],
            [['introduction', 'introduction_cn','steam_id','create_team_date','close_team_date'], 'string'],

            [['world_ranking','ago_30','total_earnings','average_player_age'], 'string', 'max' => 50],
            [['steam_id'], 'string', 'max' => 100],
            [['history_players'], 'string', 'max' => 300]
        ];
    }

}
