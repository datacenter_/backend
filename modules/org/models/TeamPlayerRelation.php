<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "team_player_relation".
 *
 * @property int $id
 * @property int $team_id 战队
 * @property int $player_id 选手
 * @property int|null $status_id 选手状态
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class TeamPlayerRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_player_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'player_id'], 'required'],
            [['team_id', 'player_id', 'status_id'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
