<?php

namespace app\modules\org\models;

use Yii;

/**
 * This is the model class for table "team_player_relation".
 *
 * @property int $id
 * @property int $team_id 战队
 * @property int $player_id 选手
 * @property int|null $status_id 选手状态
 */
class TeamPlayerRelationSnasphot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_player_relation_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'player_id'], 'required'],
            [['team_id', 'player_id', 'status_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => '战队',
            'player_id' => '选手',
            'status_id' => '选手状态',
        ];
    }
}
