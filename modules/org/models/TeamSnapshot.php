<?php

namespace app\modules\org\models;

use app\modules\common\models\OperationLog;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\services\TeamService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan".
 *
 * @property int $id 主键
 * @property string $clan_id 俱乐部唯一标识
 * @property string $name 英文名称
 * @property string $full_name 英文全称
 * @property string $short_name 英文简称
 * @property string $alias 别名
 * @property int $logo_id LOGO主键ID
 * @property int $introduction_id 介绍信息主键ID
 * @property int $country 国家ID
 * @property int $deleted 删除标识 1:删除  0:未删除
 * @property string $seo_url SEO URL
 * @property string $created_at 创建时间
 * @property string $modified_at 修改时间
 */
class TeamSnapshot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','game','image'], 'required'],
            [['organization', 'game', 'country', 'cuser','deleted','team_id','relation_id'], 'integer'],
            [['full_name', 'name', 'short_name','slug','alias'], 'string', 'max' => 100],
            [['modified_at','type'], 'string'],
            [['image'], 'string', 'max' => 300]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }

    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $attribute['modified_at'] = date('Y-m-d H:i:s');
            $team = Team::find()->where(['id' => $attribute['id']])->one();
            $oldTeamInfo = $team->toArray();
            $team->setAttributes($attribute);
            if ($team->save() > 0 ) {
                $teamIntroduction = TeamIntroduction::find()->where(['team_id' => $attribute['id']])->one();
                $oldTeamIntroduction = $teamIntroduction->toArray();
                if (!$teamIntroduction) {
                    $teamIntroduction = new TeamIntroduction();
                }
                $teamIntroduction->setAttributes($attribute);
                if ($teamIntroduction->save() <= 0) {
                    throw new BusinessException($teamIntroduction->getErrors(),'基础信息添加失败');
                }
                $oldTr = TeamPlayerRelation::find()->where(['team_id' => $attribute['id']])->asArray()->all();
                // 进入选手关系

                $playerIds = explode(',',$attribute['player_ids']);
                // 先删后加
                TeamPlayerRelation::deleteAll(['team_id' => $attribute['id']]);

                $model = new TeamPlayerRelation();
                foreach($playerIds as $playerId)
                {
                    $_model = clone $model;
                    $_model->setAttribute('player_id',$playerId);
                    $_model->setAttribute('team_id',$attribute['id']);
                    $_model->save();
                }
                $newTr = TeamPlayerRelation::find()->where(['team_id' => $attribute['id']])->asArray()->all();
                $Introduction = $teamIntroduction->toArray();
                $teamInfo = $team->toArray();

                $logNew = array_merge($Introduction,$teamInfo);
                $logOld = array_merge($oldTeamIntroduction,$oldTeamInfo);
                $logNew['relation'] = $newTr;
                $logOld['relation'] = $oldTr;
                // 添加修改log
                $diffInfo = Common::getDiffInfo($logOld, $logNew,[],true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(QueueServer::QUEUE_TYPE_CHANGE,
                        Consts::RESOURCE_TYPE_TEAM,
                        $attribute['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }
                return ['team_id' => $attribute['id'], 'msg' => '成功'];
            }
        }
        else {
            $team = new Team();
            $team->setAttributes($attribute);
            $team->setAttribute('cuser',$userId);
            if ($team->save() > 0 ) {
                $tId = $team->id;
                $teamIntroduction = new TeamIntroduction();
                $teamIntroduction->setAttribute('team_id',$tId);
                $teamIntroduction->setAttributes($attribute);
                if ($teamIntroduction->save() <= 0) {
                    throw new BusinessException($teamIntroduction->getErrors(),'基础信息添加失败');
                }

                $playerIds = explode(',',$attribute['player_ids']);
                $model = new TeamPlayerRelation();
                foreach($playerIds as $playerId)
                {
                    $_model = clone $model;
                    $_model->setAttribute('player_id',$playerId);
                    $_model->setAttribute('team_id',$tId);
                    $_model->save();
                }

                $Introduction = $teamIntroduction->toArray();
                $teamInfo = $team->toArray();
                $logNew = array_merge($Introduction,$teamInfo);
                // 添加修改log
                $diffInfo = Common::getDiffInfo([], $logNew,[],true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(QueueServer::QUEUE_TYPE_ADD,
                        Consts::RESOURCE_TYPE_TEAM,
                        $logNew['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }

                UpdateConfigService::initResourceUpdateConfig($logNew["id"],Consts::RESOURCE_TYPE_TEAM,$attribute["game"]);

                return ['team_id' => $tId, 'msg' => '成功'];
            }else{
                throw new BusinessException($team->getErrors(),'创建失败');
            }
        }
    }
}