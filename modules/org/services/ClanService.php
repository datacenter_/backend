<?php

namespace app\modules\org\services;


use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\org\models\ClanIntroduction;
use app\modules\task\services\Common;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use app\modules\common\models\EnumCountry;
use app\modules\common\services\EnumService;

class ClanService
{
    public static function getClanList($params)
    {
        $clan = Clan::find()->alias('c')
            ->select('c.*,count(t.organization) as team_num,ci.introduction,ci.introduction_cn,t.game')
            ->leftJoin('clan_introduction as ci', 'ci.clan_id = c.id')
            ->leftJoin('team as t','c.id = t.organization');
        if (isset($params['country']) && !empty($params['country'])) {
            $clan->andWhere(['c.country' => $params['country']]);
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $clan->andWhere(['or',
                ['like','c.name',$params['name']],
                ['like','c.full_name',$params['name']],
                ['like','c.short_name',$params['name']],
                ['like','c.alias',$params['name']]
            ]);
        }

        if (isset($params['id']) && !empty($params['id'])) {
            $clan->andWhere(['c.id' => $params['id']]);
        }

        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $clan->andWhere(['t.game' => $params['game_id']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $clan->andWhere(['>=', 'c.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $clan->andWhere(['<', 'c.modified_at', $end]);
        }
        $clan=$clan->orderBy('c.modified_at desc')->groupBy('c.id');
        $totalCount=$clan->count();
        $page = \Yii::$app->request->get('page',1) - 1;
        $pageSize = \Yii::$app->request->get('per_page',20);

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $model = $clan->offset($pages->offset)->limit($pages->limit)->asArray()->all();
//        $model = $clan->offset($pages->offset)->limit($pages->limit)->createCommand()->getRawSql();
//        var_dump($model);die('aa');
        $country = array_column(EnumCountry::getCountry(),'name','id');
        $gameLogo = array_column(EnumService::getGameLogo(),'logo','id');
        $clanIds = array_column($model,'id');
        $team = Team::find()->select('game,organization')->where(['in','organization',$clanIds])->asArray()->all();
        $game = [];
        foreach ($team as $key => $val) {
            $game[$val['organization']][$val['game']] = $gameLogo[$val['game']];
        }

        $resourceIds=array_column($model,'id');
        $operationInfo=OperationLogService::operationCounts(Consts::RESOURCE_TYPE_CLAN,$resourceIds);

        foreach ($model as &$val) {
            $val['country_name'] = $country[$val['country']] ?? '';
            $val['game_logo'] = $game[$val['id']] ?? [];
            $val['operation']=$operationInfo[$val['id']];
            $val['image']=ImageConversionHelper::showFixedSizeConversion($val['image'],66,66);
//            $val['game_logo']=ImageConversionHelper::showFixedSizeConversion($val['game_logo'],66,66);
        }
        return ['list' => $model, 'total' => $totalCount];
    }

    private static function getClanInfo($id)
    {
        $clan=Clan::find()->where(["id"=>$id])->asArray()->one();
        $clanIntroduction=ClanIntroduction::find()->where(['clan_id'=>$id])->asArray()->one();
        return [
            'base'=>$clan,
            'introduction'=>$clanIntroduction
        ];
    }
    public static function add($attribute, $userType,$userId,$basisId = 0)
    {
        if(isset($attribute['deleted']) && $attribute['deleted'] == 1){
            Common::deletedMaster($attribute['id'],Consts::RESOURCE_TYPE_CLAN,"");
        }
        if (isset($attribute['deleted']) && $attribute['deleted'] == 2){
            Common::recoverMaster($attribute['id'],Consts::RESOURCE_TYPE_CLAN,"");
        }
        // 修改
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldInfo = [];
        $id = '';
        if(isset($attribute['image']) && $attribute['image']){
            $attribute['image'] = ImageConversionHelper::purifyImage($attribute['image'],'ali');
        }
        if (isset($attribute['id']) && $attribute['id']) {
            $id = $attribute['id'];
            $operationType=OperationLogService::OPERATION_TYPE_UPDATE;
            $logOldInfo=self::getClanInfo($attribute['id']);
            $clanIn = ClanIntroduction::find()->where(['clan_id' => $attribute['id']])->one();
            if(!$clanIn){
                $clanIn = new ClanIntroduction();
                $clanIn->setAttribute('clan_id',$attribute['id']);
            }
            //中文简介
            $clanIn->introduction = $attribute['introduction'];
            //英文简介
            $clanIn->introduction_cn = $attribute['introduction_cn'];
            $clanIn->save();
            unset($attribute['introduction'], $attribute['introduction_cn']);
            $clan = Clan::find()->where(['id' => $attribute['id']])->one();
            $attribute['modified_at'] = date("Y-m-d H:i:s",time());
            $clan->setAttributes($attribute);

        } else {
            if (!empty($attribute['introduction']) || !empty($attribute['introduction_cn'])) {
                $clanIn = new ClanIntroduction();
                $clanIn->introduction = $attribute['introduction'];
                $clanIn->introduction_cn = $attribute['introduction_cn'];
                $clanIn->save();
                $introductionId = $clanIn->id;
            }
            unset($attribute['introduction'], $attribute['introduction_cn']);
            $clan = new Clan();
            $clan->setAttributes($attribute);
            $clan->setAttribute('cuser', $userId);
        }

        if (isset($introductionId)) {
            $clan->setAttribute('introduction_id', $introductionId);
        }

        if ($clan->save() > 0) {
            $id = $clan->id;
            if (isset($clanIn)) {
                $clanIn->clan_id = $clan->id;
                $clanIn->save();
            }
        } else {
            throw new BusinessException($clan->getErrors(), '操作失败');
        }
        $logNewInfo = self::getClanInfo($id);

        $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);

        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_CLAN,
                $id,
                ["diff" => $diffInfo["diff"], "new" => array_merge($logNewInfo,$logOldInfo)],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }

        return ['clan_id' => $clan->id, 'msg' => '操作成功'];
    }
}