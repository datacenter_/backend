<?php

namespace app\modules\org\services;


use app\modules\common\services\Consts;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\models\PlayerBase;
use app\modules\org\models\PlayerTeamConfig;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;
use app\modules\org\models\Clan;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\services\EnumService;
use app\modules\common\models\EnumPosition;

class PlayerService
{
    public static function getPlayerList($params)
    {
        $player = Player::find()->alias('p')
            ->select('p.*,t.name,t.id as team_id')
            ->leftJoin('team_player_relation as tpr','tpr.player_id = p.id')
            ->leftJoin('team as t', 't.id = tpr.team_id');

        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $player->andWhere(['p.game_id' => $params['game_id']]);
        }
        if (isset($params['nick_name']) && !empty($params['nick_name'])) {
            $player->andfilterwhere(['like', 'p.nick_name', $params['nick_name']]);
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $player->andWhere(['or',
                ['like','t.name',$params['name']],
                ['like','t.full_name',$params['name']],
                ['like','t.short_name',$params['name']],
                ['like','t.alias',$params['name']]
            ]);
        }
        if (isset($params['player_country']) && !empty($params['player_country'])) {
            $player->andWhere(['p.player_country' => $params['player_country']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $player->andWhere(['p.id' => $params['id']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $player->andWhere(['>=', 'p.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $player->andWhere(['<', 'p.modified_at', $end]);
        }
        // steam_id    新加字段,意义未知
        if(isset($params['steam_id']) && !empty($params['steam_id'])){
            $player->andWhere(['p.steam_id' => $params['steam_id']]);
        }

        $pages = new Pagination([
            'totalCount' => $player->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $player->orderBy('p.modified_at desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        //$sql=$player->orderBy('p.modified_at desc')->offset($pages->offset)->limit($pages->limit)->createCommand()->getRawSql();
        $country = array_column(EnumCountry::getCountry(),'name','id');
        $games = array_column(EnumService::getGameLogo(),'logo','id');

        // 操作记录
        $resourceIds=array_column($model,'id');
        $operationInfo=OperationLogService::operationCounts(Consts::RESOURCE_TYPE_PLAYER,$resourceIds);
        // 选手多个战队
        $teamIDs = TeamPlayerRelation::find()
            ->alias('tr')
            ->select('tr.id,tr.player_id,t.name,t.id as team_id')
            ->leftJoin('team as t','t.id = tr.team_id')
            ->where(['in','tr.player_id',$resourceIds])->asArray()->all();
        $playerTeam = [];
        foreach ($teamIDs as $key=>&$val)
        {
            $playerTeam[$val['player_id']][] = $val;
            $val['image'] =  ConversionCriteria::playerResizeConversion($val['image']);
//            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'],66,66,'ali');
        }
        foreach ($model as &$val) {
            $val['player_country'] = $country[$val['player_country']] ?? '';
            $val['game_logo'] = $games[$val['game_id']] ?? '';
            $val['operation']=$operationInfo[$val['id']];
            $val['team_info'] = $playerTeam[$val['id']];
            $val['update_config'] = UpdateConfigService::getResourceUpdateConfigDetail($val['id'], Consts::RESOURCE_TYPE_PLAYER);
            $val['image'] =  ConversionCriteria::playerResizeConversion($val['image']);
//            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'],66,66,'ali');
            $val['binding_info'] = TeamService::actionGetBingInfo($val['id'],'player');

        }
        return ['list' => $model, 'total' => $player->count()];
    }


    public static function search($name,$gameId)
    {
        $search = Player::find()->select('id,nick_name,image,player_country');

        if ($name && !empty($name)) {
            $search->andfilterwhere(['like', 'nick_name', $name]);
        }

        if ($gameId && !empty($gameId)) {
            $search->andfilterwhere(['game_id' => $gameId]);
        }
        $search->andWhere(['deleted' => 2]);
        $pages = new Pagination([
            'totalCount' => $search->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $search->orderBy('nick_name asc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        $country = array_column(EnumCountry::getCountry(),'name','id');
        foreach ($model as &$val) {
            $val['country_name'] = $country[$val['player_country']] ?? '';
        }
        return ['list' => $model, 'total' => $search->count()];
    }

    public static function setPlayerUpdateTime($playerId,$updateTime)
    {
        return Player::updateAll(['modified_at'=>$updateTime],['id'=>$playerId]);
    }

    public static function setTeamUpdateTime($teamId,$updateTime)
    {
        return Team::updateAll(['modified_at'=>$updateTime],['id'=>$teamId]);
    }
    /**
     * @param $attribute
     * @param $userType
     * @param $userId
     * @param int $basisId
     * @throws BusinessException
     * 管理页面修改player用
     */
    public static function setPlayer($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        // 游戏ID
        if(isset($attribute['core_data']['game_id']) && $attribute['core_data']['game_id'] !== ""){
            if(!is_numeric($attribute['core_data']['game_id']) || strpos($attribute['core_data']['game_id'],".") || $attribute['core_data']['game_id'] < 1){
                throw new BusinessException([], '游戏ID必须为正整数!');
            }
            $gameId = EnumGame::find('id')->where(['id'=>$attribute['core_data']['game_id']])->one();
            if(!$gameId){
                throw new BusinessException([], '该游戏ID不存在!');
            }
        }
        // role选手位置ID为正整数
        if(isset($attribute['core_data']['role']) && $attribute['core_data']['role'] !== ""){
            if(!is_numeric($attribute['core_data']['role']) || strpos($attribute['core_data']['role'],".") || $attribute['core_data']['role'] < 1){
                unset($attribute['core_data']['role']);
            }
            $role = EnumPosition::find('id')->where(['id'=>$attribute['core_data']['role'],'game_id'=>$attribute['core_data']['game_id']])->one();
            if(!$role){
                unset($attribute['core_data']['role']);
            }
        }
        // player_country选手国籍
        if(isset($attribute['core_data']['player_country']) && $attribute['core_data']['player_country'] !== ""){
            if(!is_numeric($attribute['core_data']['player_country']) || strpos($attribute['core_data']['player_country'],".") || $attribute['core_data']['player_country'] <= 0){
                unset($attribute['core_data']['player_country']);
            }
            $role = EnumCountry::find('id')->where(['id'=>$attribute['core_data']['player_country']])->one();
            if(!$role){
                unset($attribute['core_data']['player_country']);
            }
        }
        // 选手昵称为必选项
        if(empty($attribute['core_data']['nick_name'])){
            unset($attribute['core_data']['nick_name']);
        }
        $playerId=isset($attribute['player_id'])?$attribute['player_id']:null;
        $coreData=$attribute[Consts::TAG_TYPE_CORE_DATA];
        $baseData=$attribute[Consts::TAG_TYPE_BASE_DATA];
        $teamPlayerRelation=isset($attribute['team_player_relation'])?$attribute['team_player_relation']:null;

        if($coreData){
            if($playerId && !isset($coreData["id"])){
                $coreData["id"]=$playerId;
            }
            $player = self::setPlayerCoreData($coreData, $userType, $userId, $basisId,$originId);
            $playerId=$player['id'];
        }
        if($baseData){
            $playerBase = self::setPlayerBaseData($playerId, $baseData, $userType, $userId, $basisId);
        }
        if ($teamPlayerRelation) {
            self::setPlayerTeamConfig($playerId, $teamPlayerRelation, $userType, $userId, $basisId);
        }
        // 设置操作状态
        if($basisId){
            DataTodoService::setDealStatus($basisId,Consts::TODO_DEAL_STATUS_DONE);
        }
        return self::getPlayer($playerId);
    }

    public static function getPlayer($playerId)
    {
        $playerCore = Player::find()->where(["id" => $playerId])->asArray()->one();
        $playerBase = PlayerBase::find()->where(['player_id' => $playerId])->asArray()->one();
        $configList = TeamPlayerRelation::find()
            ->select("team_player_relation.*,team.name")
            ->leftJoin('team', 'team.id=team_player_relation.team_id')
            ->where(["player_id" => $playerId])->asArray()->all();
        return [
            'player_id'=>$playerId,
            Consts::TAG_TYPE_CORE_DATA => $playerCore,
            Consts::TAG_TYPE_BASE_DATA => $playerBase,
            'team_player_relation' => $configList,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($playerId,'player')

        ];
    }

    public static function setPlayerTeamConfig($playerId, $configList, $userType, $userId, $basisId = 0)
    {
        // 添加或者修改战队关联关系
        // 读取当前关系
        $playerOldInfo = TeamPlayerRelation::find()->select('player_id,team_id,status_id')->where(["player_id" => $playerId])->orderBy("team_id")->asArray()->all();
        $logOld = [
            'id'=>$playerId,
            'player_info' => json_encode($playerOldInfo)
        ];
        foreach ($configList as $key => $val) {
            $whereArray = [
                'player_id' => $playerId,
                'team_id' => $val['team_id']
            ];
            $dataArray = [
                'status_id' => $val['status_id']
            ];
            $config = TeamPlayerRelation::find()->where($whereArray)->one();
            if ($config) {
                $config->setAttributes($dataArray);
            } else {
                $config = new TeamPlayerRelation();
                $config->setAttributes(array_merge($whereArray, $dataArray));
            }
            $config->save();
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setTeamUpdateTime($val['team_id'],$time);
        }
        // 读取修改后关系
        $playerInfo = TeamPlayerRelation::find()->select('player_id,team_id,status_id')->where(["player_id" => $playerId])->orderBy("team_id")->asArray()->all();
        $logNew = [
            'id'=>$playerId,
            'player_info' => json_encode($playerInfo),
            'team_by_playerIds' =>json_encode(array_column($playerInfo,'team_id'))
        ];
        // 添加修改log
        $diffInfo = Common::getDiffInfo($logOld, $logNew,[],true);
        if ($diffInfo["changed"]) {
            OperationLogService::addLog($diffInfo['change_type'],
                Consts::RESOURCE_TYPE_PLAYER,
                $playerId,
                ["diff" => $diffInfo["diff"], "new" => $logNew],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
    }

    public static function setPlayerCoreData($attribute, $userType, $userId, $basisId = 0,$originId=0)
    {
        // 这里判断必填项
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldPlayer = [];
        if (isset($attribute['id']) && $attribute['id']) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $player = Player::find()->where(['id' => $attribute['id']])->one();
            if (!$player) {
                throw new BusinessException($attribute, "不存在这个player");
            }
            $logOldPlayer = $player->toArray();
            $player->setAttributes($attribute);
            if(!$player->save()){
                throw new BusinessException($player->getErrors(),"设置core_data失败");
            }
        } else {
            $player = new Player();
            if(!$attribute['game_id']){
                throw new BusinessException($attribute,"添加选手时，game_id必填");
            }
            $player->setAttributes($attribute);
            $player->setAttribute('cuser', $userId);
            if(!$player->save()){
                $playerErrors = $player->getErrors();
                $error = '';
                if(count($playerErrors)>0){
                    foreach ($playerErrors as $key => $value){
                        $error .= $value[0];
                    }
                }
                throw new BusinessException($player->getErrors(),$error);
            }
            // 创建关系
            if ($basisId !=0 )
            {
                \Yii::$app->db->createCommand()->batchInsert('data_change_config', ['basis_id', 'resource_type', 'resource_id','origin_id'],
                    [[$basisId, 'player', $player->id,$originId]])->execute();
            }
            UpdateConfigService::initResourceUpdateConfig($player["id"],Consts::RESOURCE_TYPE_PLAYER,$attribute["game_id"]);
        }
        $logNewPlayer = $player->toArray();
        // 记录log
        $filter = array_keys($logNewPlayer);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldPlayer, $logNewPlayer, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_PLAYER,
                $logNewPlayer['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewPlayer],
                0,
                Consts::TAG_TYPE_CORE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $player;
    }

    public static function setPlayerBaseData($playerId, $attribute, $userType, $userId, $basisId = 0)
    {
        // 这里不用id，以免产生冲突
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldPlayerBase = [];
        $updateData = $attribute;
        unset($updateData['id']);
        $updateData['player_id'] = $playerId;
        $playerBase = PlayerBase::find()->where(['player_id' => $playerId])->one();
        if ($playerBase) {
            $operationType=OperationLogService::OPERATION_TYPE_UPDATE;
            $logOldPlayerBase = $playerBase->toArray();
            $playerBase->setAttributes($attribute);
            //修改就主表变更时间
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setPlayerUpdateTime($playerId,$time);
        } else {
            $playerBase = new PlayerBase();
            $playerBase->setAttributes($updateData);
        }
        if(!$playerBase->save()){
            throw new BusinessException($playerBase->getErrors(),"设置base_data失败");
        }
        $logNewPlayerBase = $playerBase->toArray();

        // 记录log
        $filter = array_keys($logNewPlayerBase);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldPlayerBase, $logNewPlayerBase, $filter);

        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_PLAYER,
                $playerId,
                ["diff" => $diffInfo["diff"], "new" => $logNewPlayerBase],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $playerBase;
    }

    public static function checkBing($type,$id)
    {
        $data = DataStandardMasterRelation::find()->where(['resource_type' => $type,'master_id' =>$id])->asArray()->all();

    return $data ? false : true;
    }

    public static function checkPlayer($param) {

        $where = [];
        if (!empty($param['game_id'])) {
            $where['game_id'] = $param['game_id'];
        }

        if (!empty($param['origin_id'])) {
            $where['origin_id'] = $param['origin_id'];
        }
        $where['deal_status']= 4;
        $where['deleted']= 2;

        $stanTeam = StandardDataTeam::find()->alias('sta')
            ->select(['sta.id as standard_id','sta.origin_id','sta.players as sta_players','sta.history_players as sta_history_players','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel' ,"sta.id = rel.standard_id and rel.resource_type = 'team'")
            ->where($where);
        $stanTeam->andWhere([
            'or',
            [
                'and',
                'sta.players is not null',
                ['<>','sta.players',"[]"]
            ],
            [
                'and',
                'sta.history_players is not null',
                ['<>','sta.history_players',"[]"]
            ]
        ]);
        $q = $stanTeam->createCommand()->getRawSql();
        $stanTeam = $stanTeam->asArray()->all();

        $master_id = array_column($stanTeam,'master_id');
        //历史选手
        $teamHistoryPlayers = TeamIntroduction::find()->select(['team_id','history_players'])->where(['team_id'=>$master_id])->asArray()->all();
        $teamHistoryPlayersByKey = array_column($teamHistoryPlayers,null,'team_id');

        //当前选手
        $nowPlayers = TeamPlayerRelation::find()->alias('tr')->select(['team_id','player_id'])->where(['tr.team_id' => $master_id])->asArray()->all();
        $nowPlayersByKey = [];
        foreach ($nowPlayers as $nowPlayersK => $nowPlayersV) {
            $nowPlayersByKey[$nowPlayersV['team_id']][] = $nowPlayersV;
        }
        $NowplayersDiffNum = [];
        $oldPlayersDiffNum = [];

        foreach ($stanTeam as $stanTeamK => $stanTeamV) {

            //判断当前选手
//            if (!empty($stanTeamV['sta_players']) && $stanTeamV['sta_players'] != '[]' ){

                $stanNowplayers = json_decode($stanTeamV['sta_players'],true);
                $stanNowplayersNum = count($stanNowplayers);
                $nowPlayersByKeyNum = count($nowPlayersByKey[$stanTeamV['master_id']]);

                if ($stanNowplayersNum != $nowPlayersByKeyNum) {
                    $NowplayersDiffNum[$stanTeamV['origin_id']][] = $stanTeamV['master_id'];
                    continue;
                }
//            }


            //判断历史选手
//            if (!empty($stanTeamV['sta_history_players']) && $stanTeamV['sta_history_players'] != '[]') {
                $stanOldlayers = json_decode($stanTeamV['sta_history_players'],true);
                $stanOldlayersNum = count($stanOldlayers);
                $teamHistoryPlayersMaster = $teamHistoryPlayersByKey[$stanTeamV['master_id']];
                $teamHistoryPlayersMasterNum= count(json_decode($teamHistoryPlayersMaster['history_players'],true));
                if ($stanOldlayersNum != $teamHistoryPlayersMasterNum) {
                    $NowplayersDiffNum[$stanTeamV['origin_id']][] = $stanTeamV['master_id'];
                }
//            }

        }
        return $NowplayersDiffNum;

    }
}