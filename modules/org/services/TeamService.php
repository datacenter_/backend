<?php

namespace app\modules\org\services;


use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\data\controllers\DataController;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use app\modules\common\models\EnumCountry;
use app\modules\common\services\EnumService;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use yii\data\Pagination;

class TeamService
{
    public static function getTeamList($params)
    {
        $clan = Team::find()->alias('t')
            ->select('t.*,c.name as clan_name,c.id as clan_id,c.country as clan_country')
            ->leftJoin('clan as c', 't.organization = c.id');
        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $clan->andWhere(['t.game' => $params['game_id']]);
        }
        if (isset($params['clan_name']) && !empty($params['clan_name'])) {
            $clan->andWhere(['or',
                ['like', 'c.name', $params['clan_name']],
                ['like', 'c.full_name', $params['clan_name']],
                ['like', 'c.short_name', $params['clan_name']],
                ['like', 'c.alias', $params['clan_name']]
            ]);
        }
        if (isset($params['team_name']) && !empty($params['team_name'])) {
            $clan->andWhere(['or',
                ['like', 't.name', $params['team_name']],
                ['like', 't.full_name', $params['team_name']],
                ['like', 't.short_name', $params['team_name']],
                ['like', 't.alias', $params['team_name']]
            ]);
        }
        if (isset($params['country']) && !empty($params['country'])) {
            $clan->andWhere(['t.country' => $params['country']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $clan->andWhere(['t.id' => $params['id']]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $clan->andWhere(['>=', 't.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $clan->andWhere(['<', 't.modified_at', $end]);
        }

        $pages = new Pagination([
            'totalCount' => $clan->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $clan->orderBy('t.modified_at desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');

        $resourceIds = array_column($model, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_TEAM, $resourceIds);

        foreach ($model as &$val) {
            $val['country_name'] = $country[$val['country']] ?? '';
            $val['game_logo'] = $games[$val['game']] ?? '';
            $val['operation'] = $operationInfo[$val['id']];
            $val['update_config'] = UpdateConfigService::getResourceUpdateConfigDetail($val['id'], Consts::RESOURCE_TYPE_TEAM);
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 66, 66, 'ali');
            $val['binding_info'] = self::actionGetBingInfo($val['id'],'team');
        }
        return ['list' => $model, 'total' => $clan->count()];
    }
    // 获取绑定信息方法
    public static function actionGetBingInfo($id,$type)
    {
        $tableName = '';
        switch ($type)
        {
            case 'player' : $tableName = 'standard_data_player';break;
            case 'group' : $tableName = 'standard_data_group';break;
            case 'match' : $tableName = 'standard_data_match';break;
            case 'team' : $tableName = 'standard_data_team';break;
            case 'tournament' : $tableName = 'standard_data_tournament';break;
            case 'stage' : $tableName = 'standard_data_stage';break;
            default:$tableName = 'standard_data_metadata';break;
        }
        $bingInfo = DataStandardMasterRelation::find()->alias('dmr')
            ->select('b.id,b.origin_id,enum_origin.name,enum_origin.desc,dmr.master_id,b.rel_identity_id as standard_id,enum_origin.image as enum_origin_image')
            ->where(['dmr.resource_type' => $type,'dmr.master_id' => $id])
            ->leftJoin($tableName.' as b','b.id = dmr.standard_id')
            ->leftJoin('enum_origin','b.origin_id = enum_origin.id');
        $info = $bingInfo->asArray()->all();
        return $info;
    }

    public static function search($name, $gameId = false)
    {
        $search = Team::find()->select('id,name,image,country');

        if ($name && !empty($name)) {
            $search->andWhere(['or',
                ['like', 'name', $name],
                ['like', 'full_name', $name],
                ['like', 'short_name', $name],
                ['like', 'alias', $name]
            ]);
        }
        if ($gameId && !empty($gameId)) {
            $search->andfilterwhere(['game' => $gameId]);
        }

        $search->andWhere(['deleted' => 2]);
        $pages = new Pagination([
            'totalCount' => $search->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $search->orderBy('name asc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        foreach ($model as &$val) {
            $val['country_name'] = $country[$val['country']] ?? '';
        }

        return ['list' => $model, 'total' => $search->count()];
    }


    public static function add($attribute, $userId)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $attribute['modified_at'] = date('Y-m-d H:i:s');
            $team = Team::find()->where(['id' => $attribute['id']])->one();
            $oldTeamInfo = $team->toArray();
            $team->setAttributes($attribute);
            if ($team->save() > 0) {
                $teamIntroduction = TeamIntroduction::find()->where(['team_id' => $attribute['id']])->one();
                $oldTeamIntroduction = [];
                if (!$teamIntroduction) {
                    $attribute['team_id'] = $attribute['id'];
                    $teamIntroduction = new TeamIntroduction();
                } else {
                    $attribute['team_id'] = $attribute['id'];
                    $oldTeamIntroduction = $teamIntroduction->toArray();
                }
                $teamIntroduction->setAttributes($attribute);
                if ($teamIntroduction->save() <= 0) {
                    throw new BusinessException($teamIntroduction->getErrors(), '基础信息添加失败');
                }
                $oldTr = TeamPlayerRelation::find()->where(['team_id' => $attribute['id']])->asArray()->all();
                // 进入选手关系

                $playerIds = explode(',', $attribute['player_ids']);
                // 先删后加
                TeamPlayerRelation::deleteAll(['team_id' => $attribute['id']]);

                $model = new TeamPlayerRelation();
                foreach ($playerIds as $playerId) {
                    $_model = clone $model;
                    $_model->setAttribute('player_id', $playerId);
                    $_model->setAttribute('team_id', $attribute['id']);
                    $_model->save();
                }
                $newTr = TeamPlayerRelation::find()->where(['team_id' => $attribute['id']])->asArray()->all();
                $Introduction = $teamIntroduction->toArray();
                $teamInfo = $team->toArray();

                $logNew = array_merge($Introduction, $teamInfo);
                $logOld = array_merge($oldTeamIntroduction, $oldTeamInfo);
                $logNew['relation'] = $newTr;
                $logOld['relation'] = $oldTr;
                // 添加修改log
                $diffInfo = Common::getDiffInfo($logOld, $logNew, array_diff(array_keys($logNew), ['modified_at']), true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(QueueServer::QUEUE_TYPE_CHANGE,
                        Consts::RESOURCE_TYPE_TEAM,
                        $attribute['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }
                return ['team_id' => $attribute['id'], 'msg' => '成功'];
            }
        } else {
            $team = new Team();
            $team->setAttributes($attribute);
            $team->setAttribute('cuser', $userId);
            if ($team->save() > 0) {
                $tId = $team->id;
                $teamIntroduction = new TeamIntroduction();
                $teamIntroduction->setAttribute('team_id', $tId);
                $teamIntroduction->setAttributes($attribute);
                if ($teamIntroduction->save() <= 0) {
                    throw new BusinessException($teamIntroduction->getErrors(), '基础信息添加失败');
                }

                $playerIds = explode(',', $attribute['player_ids']);
                $model = new TeamPlayerRelation();
                foreach ($playerIds as $playerId) {
                    $_model = clone $model;
                    $_model->setAttribute('player_id', $playerId);
                    $_model->setAttribute('team_id', $tId);
                    $_model->save();
                }

                $Introduction = $teamIntroduction->toArray();
                $teamInfo = $team->toArray();
                $logNew = array_merge($Introduction, $teamInfo);
                // 添加修改log
                $diffInfo = Common::getDiffInfo([], $logNew, [], true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(QueueServer::QUEUE_TYPE_ADD,
                        Consts::RESOURCE_TYPE_TEAM,
                        $logNew['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }

                UpdateConfigService::initResourceUpdateConfig($logNew["id"], Consts::RESOURCE_TYPE_TEAM, $attribute["game"]);

                return ['team_id' => $tId, 'msg' => '成功'];
            } else {
                throw new BusinessException($team->getErrors(), '创建失败');
            }
        }
    }

//    public static function getTeam($teamId)
//    {
//        return Team::find()
//            ->select("team_introduction.*,team.*")
//            ->leftJoin('team_introduction', 'team.id=team_introduction.team_id')
//            ->where(["team.id" => $teamId])->asArray()->all();
//    }


    /**
     * @param $attribute
     * @param $userType
     * @param $userId
     * @param int $basisId
     * @throws BusinessException
     * 管理页面修改player用
     */
    public static function setTeam($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        // 添加战队管理->选手添加页面容错:添加时传值为players,编辑时传值为team_player   By  王傲渊
        if (isset($attribute['players'])) {
            $attribute['team_player'] = $attribute['players'];
            unset($attribute['players']);
        }
        // 国家地区
        if(isset($attribute['core_data']['country']) && $attribute['core_data']['country'] !== ""){
            if(!is_numeric($attribute['core_data']['country']) || strpos($attribute['core_data']['country'],".") || $attribute['core_data']['country'] < 1){
                $attribute['core_data']['country'] = "";
            }

            if(isset($attribute['core_data']['country']) && $attribute['core_data']['country'] !== ""){
                $role = EnumCountry::find('id')->where(['id'=>$attribute['core_data']['country']])->one();

                if(!$role){
                    $attribute['core_data']['country'] = "";
                }
            }
        }
//        var_dump(json_decode($attribute,JSON_UNESCAPED_UNICODE));die;
        // 俱乐部
        if(isset($attribute['core_data']['organization']) && $attribute['core_data']['organization'] !== ""){
            if(!is_numeric($attribute['core_data']['organization']) || strpos($attribute['core_data']['organization'],".") || $attribute['core_data']['organization'] < 1){
                $attribute['core_data']['organization'] = "";
            }
            if(isset($attribute['core_data']['organization']) && $attribute['core_data']['organization'] !== ""){
                $role = Clan::find('id')->where(['id'=>$attribute['core_data']['organization']])->one();
                if(!$role){
                    $attribute['core_data']['organization'] = "";
                }
            }
        }

        if (empty($attribute['core_data']['name'])) {
            throw new BusinessException($attribute, "添加战队时，名称必填");
        }

        if (empty($attribute['core_data']['game'])) {
            throw new BusinessException($attribute, "添加战队时，游戏项目必填");
        }
        if (isset($attribute['core_data']['image']) && empty($attribute['core_data']['image'])) {
            $image = EnumGame::find()->select('simple_image')->where(['id'=>$attribute['core_data']['game']])->asArray()->one();

            $attribute['core_data']['image'] = $image['simple_image'] ?? '';
        }

        $teamId = isset($attribute['id']) ? $attribute['id'] : null;
        $coreData = $attribute[Consts::TAG_TYPE_CORE_DATA];
        $baseData = $attribute[Consts::TAG_TYPE_BASE_DATA];
        $teamPlayers = $attribute[Consts::TAG_TYPE_TEAM_PLAYER];
        if ($coreData) {
            if ($teamId && !isset($coreData["id"])) {
                $coreData["id"] = $teamId;
            }
            $team = self::setTeamCoreData($coreData, $userId, $userType, $basisId, $originId);
            $teamId = $team['id'];
        }
        if ($baseData) {
            $teamBase = self::setTeamBaseData($teamId, $baseData, $userType, $userId, $basisId);
        }

        if (isset($teamPlayers)) {
            $newPlayers = self::setTeamPlayers($teamId, $teamPlayers, $userType, $userId, $basisId);
        }
        // 设置操作状态
        if ($basisId) {
            DataTodoService::setDealStatus($basisId, Consts::TODO_DEAL_STATUS_DONE);
        }
        return self::getTeam($teamId);
    }

    public static function setTeamUpdateTime($teamId,$updateTime)
    {
        return Team::updateAll(['modified_at'=>$updateTime],['id'=>$teamId]);
    }
    public static function getTeam($teamId)
    {
        $playerCore = Team::find()->where(["id" => $teamId])->asArray()->one();
        $playerBase = TeamIntroduction::find()->where(['team_id' => $teamId])->asArray()->one();
        $configList = TeamPlayerRelation::find()
            ->alias('tr')
            ->select(['tr.*','p.*','country.name as country_name','country.e_name as country_e_name'])
            ->leftJoin('player as p', 'p.id = tr.player_id')
            ->leftJoin('enum_country as country', 'country.id = p.player_country')
            ->where(['tr.team_id' => $teamId])->asArray()->all();
        foreach($configList as $key => &$value){
            $value['id'] = $value['player_id'];
        }
        return [
            'team_id' => $teamId,
            Consts::TAG_TYPE_CORE_DATA => $playerCore,
            Consts::TAG_TYPE_BASE_DATA => $playerBase,
            Consts::TAG_TYPE_TEAM_PLAYER => $configList,
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($teamId, Consts::RESOURCE_TYPE_TEAM),
        ];
    }

    public static function setTeamCoreData($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        // 这里判断必填项
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldTeam = [];
        if (isset($attribute['id']) && $attribute['id']) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $team = Team::find()->where(['id' => $attribute['id']])->one();
            if (!$team) {
                throw new BusinessException($attribute, "不存在这个player");
            }
            $logOldTeam = $team->toArray();
            $team->setAttributes($attribute);
            if (!$team->save()) {
                throw new BusinessException($team->getErrors(), "设置core_data失败");
            }
        } else {
            $attribute['team_name'] = $attribute['name'];
            $attribute['cuser'] = $userId;
            if (!$attribute['game']) {
                throw new BusinessException($attribute, "添加战队时，game_id必填");
            }
            $team = new Team();
            $team->setAttributes($attribute);
            if (!$team->save()) {
                throw new BusinessException($team->getErrors(), "设置core_data失败");
            }
            // 创建关系
            if ($basisId != 0) {
                \Yii::$app->db->createCommand()->batchInsert('data_change_config', ['basis_id', 'resource_type', 'resource_id', 'origin_id'],
                    [[$basisId, 'team', $team->id, $originId]])->execute();
            }
            UpdateConfigService::initResourceUpdateConfig($team["id"], Consts::RESOURCE_TYPE_TEAM, $attribute["game"]);
        }
        $logNewTeam = $team->toArray();
        // 记录log
        $filter = array_keys($logNewTeam);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldTeam, $logNewTeam, $filter);

        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_TEAM,
                $logNewTeam['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewTeam],
                0,
                Consts::TAG_TYPE_CORE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $team;
    }

    public static function setTeamBaseData($teamId, $attribute, $userType, $userId, $basisId = 0)
    {
        // 这里不用id，以免产生冲突
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldTeamBase = [];
        $updateData = $attribute;
        unset($updateData['id']);
        $updateData['team_id'] = $teamId;
        $teamBase = TeamIntroduction::find()->where(['team_id' => $teamId])->one();
        if ($teamBase) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $logOldTeamBase = $teamBase->toArray();
            $teamBase->setAttributes($attribute);
            //修改就主表变更时间
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setTeamUpdateTime($teamId,$time);
        } else {
            $teamBase = new TeamIntroduction();
            $teamBase->setAttributes($updateData);
        }
        if (!$teamBase->save()) {
            throw new BusinessException($teamBase->getErrors(), "设置base_data失败");
        }
        $logNewTeamBase = $teamBase->toArray();

        // 记录log
        $filter = array_keys($logNewTeamBase);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldTeamBase, $logNewTeamBase, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_TEAM,
                $teamId,
                ["diff" => $diffInfo["diff"], "new" => $logNewTeamBase],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $teamBase;
    }


    public static function setTeamPlayers($teamId, $attribute, $userType, $userId, $basisId = 0)
    {
        // 检查player的diff，如果一致，则不修改，如果不一致，则删除重建
        if(isset($attribute['player_id'])){
            $attribute = $attribute['player_id'];
        }
        if(isset($attribute[0]['id'])){
            $attribute=array_column($attribute,'id');
        }

        $palyers = TeamPlayerRelation::find()->select('player_id')->where(['team_id' => $teamId])->asArray()->all();
        $idsOld = array_column($palyers,'player_id');
        sort($idsOld);
        $idsNew = $attribute;
        sort($idsNew);
//        $filter = ['team_player'];
        $old = [
            'id'=>$teamId,
            'rel_player_info'=>json_encode($idsOld),

        ];
        $new =[
            'id'=>$teamId,
            'rel_player_info'=>json_encode($idsNew),
        ];
        $diffInfo = Common::getDiffInfo($old,$new);
        if($diffInfo['changed']){
            TeamPlayerRelation::deleteAll(['team_id' => $teamId]);
            $model = new TeamPlayerRelation();
            foreach ($idsNew as $playerId) {
                $_model = clone $model;
                $_model->setAttribute('player_id', $playerId);
                $_model->setAttribute('team_id', $teamId);
                $_model->save();
            }
            $time = date("Y-m-d H:i:s",time());
            $upTime = self::setTeamUpdateTime($teamId,$time);

            OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                Consts::RESOURCE_TYPE_TEAM,
                $teamId,
                ["diff" => $diffInfo["diff"], "new" => $new],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
    }

    public static function getCountry($key)
    {
        return EnumCountry::find()->alias('c')
            ->andWhere(['or',
                ['c.e_name' => $key],
                ['c.two' => $key],
                ['c.three' => $key]
            ])->asArray()->one();
    }

    public static function getTeamsInfo($teamIds,$game_id){
        $teamIds = explode(',',$teamIds);
        $teamsInfo = Team::find()->where(['IN','id',$teamIds])->andWhere(['game'=>$game_id])->asArray()->all();
        if ($teamsInfo){
            foreach ($teamsInfo as $key => $val){
                $country = EnumCountry::find()->select('name')->where(['id'=>$val['country']])->asArray()->one();
                if ($country){
                    $teamsInfo[$key]['country_cn'] = $country['name'];
                }
            }
        }
        return $teamsInfo;
    }

}