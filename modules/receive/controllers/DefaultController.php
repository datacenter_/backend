<?php

namespace app\modules\receive\controllers;


use yii\web\Controller;


/**
 * Default controller for the `receive` module
 */
class DefaultController extends Controller
{
    public $layout = false;


//    public $layout = false;
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionWs()
    {

//        print_r(1);
        $html=  <<<LABEL
<head>
<link rel="stylesheet" href="/static/layui/css/layui.css">
<script src="/static/layui/layui.all.js"></script>
</head>

<div id="main">loading...</div>
<div id="side"></div>

<script>
 var $_GET = (function(){
    var url = window.document.location.href.toString();
    var u = url.split("?");
    if(typeof(u[1]) == "string"){
        u = u[1].split("&");
        var get = {};
        for(var i in u){
            var j = u[i].split("=");
            get[j[0]] = j[1];
        }
        return get;
    } else {
        return {};
    }
})();
console.log($_GET);
//window.onerror=function(){return true;}
//ws = new WebSocket("ws://shop1.test:8345");
//ws = new WebSocket("ws://123.56.138.90:8345");
ws = new WebSocket("ws://api.master.gc.veryapi.com:8345");
ws.onopen = function() {
    console.log("websocket connected success");
    var match_id=$_GET.match_id;
    console.log(match_id);
    ws.send(match_id);
};
ws.onmessage = function(e) {
    
    var myDate = new Date();
    
    var arr  = JSON.parse(e.data)['frames'];
    var events  = JSON.parse(e.data)['events'];
//    var mytime=myDate.toLocaleTimeString(); //获取当前时间
    
   try {
     var mytime=events['info']['log_time']; //获取当前时间
    } catch(err) {
      console.log(events);
    }
//    console.log(arr);
    var square_image  = arr.hasOwnProperty('map')&&arr["map"]?arr["map"]["image"]["square_image"]:'';
    var square_image_html = square_image?"<img width='252px' src="+square_image+"></img>":'';
    var rectangle_image  = arr.hasOwnProperty('map')&&arr["map"]?arr["map"]["image"]["rectangle_image"]:'';
    var rectangle_image_html  =square_image?"<img width='252px' src="+rectangle_image+"></img>":'';;
    var thumbnail  = arr.hasOwnProperty('map')&&arr["map"]?arr["map"]["image"]["thumbnail"]:'';
    var thumbnail_html  = square_image?"<img width='252px' src="+thumbnail+"></img>":'';
    //console.log(thumbnail_html);
    var match_score_0_image = arr["match_scores"][0]["image"]?"<img width='30px' src="+arr["match_scores"][0]["image"]+"></img>":"";
    var match_score_1_image = arr["match_scores"][1]["image"]?"<img width='30px' src="+arr["match_scores"][1]["image"]+"></img>":"";
    var ct_players = arr['side'][0]['players'];
    var t_players = arr['side'][1]['players'];
    //console.log(ct_players);
    //console.log(t_players);
    //history
            var ct_history = arr['side'][0]['rounds_history'];
            var t_history = arr['side'][1]['rounds_history'];
            var ct_h_content ='';
            var t_h_content ='';
            var ct_h_html ='';
            var t_h_html ='';
            if(ct_history){
                var ct_h_head =   "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                        "<th>round_ordinal</th>"+
                         "<th>ct</th>"+
                         "<th>survived_players</th>"+
                         "<th>terrorist</th>"+
                         "<th>survived_players</th>"+
                         "<th>round_end_type</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>";
                  var ct_h_end =   "</tbody>"+
                    "</table>";
                    
                for(ct_h_key in ct_history){
                   ct_h_content += "<tr>"+
                            "<td >"+ct_history[ct_h_key]["round_ordinal"]+"</td>"+
                          "<td >ct</td>"+
                          "<td >"+ct_history[ct_h_key]["survived_players"]+"</td>"+
                          "<td >terrorist</td>"+
                          "<td >"+t_history[ct_h_key]["survived_players"]+"</td>"+
                          "<td >"+ct_history[ct_h_key]["round_end_type"]+"</td>"+
                       "</tr>";  
                }
               
                 ct_h_html=ct_h_head+ct_h_content+ct_h_end;
            }
            if(t_history){
                var t_h_head =   "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='rounds_history'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>t</th>"+
                         "<th>round_ordinal</th>"+
                         "<th>survived_players</th>"+
                         "<th>round_end_type</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>";
                  var t_h_end =   "</tbody>"+
                    "</table>";
                    
                for(t_h_key in t_history){
                   t_h_content += "<tr>"+
                          "<td >t</td>"+
                          "<td >"+t_history[t_h_key]["round_ordinal"]+"</td>"+
                          "<td >"+t_history[t_h_key]["survived_players"]+"</td>"+
                          "<td >"+t_history[t_h_key]["round_end_type"]+"</td>"+
                       "</tr>";
                       
                }
                 t_h_html=t_h_head+t_h_content+t_h_end;
            }
           
                       
                   
    //    console.log(ct_h_html);
    var player_list_html = getPlayers(ct_players,t_players);
    function getWeapon(weapons){
         var wCt =[];
         var head =  "<table class='layui-table' lay-size='sm'>"+
                         "<colgroup>"+
                           "<col width='15'>"+
                           "<col width='15'>"+
                           "<col width='15'>"+
                           "<col width='15'>"+
                           "<col width='15'>"+
                         "</colgroup>"+
                         "<thead>"+
                           "<tr>"+
                             "<th>weapon_id</th>"+
                             "<th>external_id</th>"+
                             "<th>external_name</th>"+
                             "<th>name</th>"+
                             "<th>image</th>"+
                           "</tr>"+
                         "</thead>"+
                         "<tbody>";
        for(wCtKey in weapons){
                    wCt +=
                            
                           "<tr>"+
                             "<td>"+weapons[wCtKey]['weapon_id']+"</td>"+
                             "<td>"+weapons[wCtKey]['external_id']+"</td>"+
                             "<td>"+weapons[wCtKey]['external_name']+"</td>"+
                             "<td>"+weapons[wCtKey]['name']+"</td>"+
                             "<td><img src='"+weapons[wCtKey]['image']+"'></img></td>"+
                           "</tr>";
                         
            }
            
            var end = "</tbody>"+
                        "</table>";
            var res = head+wCt+end;
    //        console.log(res);
           return  res;
                
    }
    function getPlayers(ct_players,t_players){
        if(ct_players.length>t_players.length){
            var   players = ct_players;
           
        }else{
            var   players = t_players;
        }
        var player_list = [];
    //    console.log(ct_players.length);
    //    console.log(player_list);
          for(k in players){
           
            wCt = ct_players.hasOwnProperty(k)?getWeapon(ct_players[k]['weapon']):'';
            gCt = ct_players.hasOwnProperty(k)?getWeapon(ct_players[k]['grenades']):'';
            wT = t_players.hasOwnProperty(k)?getWeapon(t_players[k]['weapon']):'';
            gT = t_players.hasOwnProperty(k)?getWeapon(t_players[k]['grenades']):'';
    //        console.log(players);
    //        console.log(wCt);
    //        console.log(gCt);
    //        console.log(gT);
    //        console.log(wT);
            
             var ct_player_html=ct_players.hasOwnProperty(k)?
             "<td>" +
                    "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                          "<th>nick_name</th>"+
                          "<th>player_id</th>"+
                          "<th>hp</th>"+
                         "<th>has_kevlar</th>"+
                         "<th>has_helmet</th>"+
                         "<th>has_defusekit</th>"+
                         "<th>has_bomb</th>"+
                         "<th>is_alive</th>"+
                         "<th>blinded_time</th>"+
                         "<th>money</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                         "<td style='color:red'>"+ct_players[k]['player']['nick_name']+"</td>"+
                         "<td>"+ct_players[k]['player']['player_id']+"</td>"+
                         "<td>"+ct_players[k]['hp']+"</td>"+
                         "<td>"+ct_players[k]['has_kevlar']+"</td>"+
                         "<td>"+ct_players[k]['has_helmet']+"</td>"+
                         "<td>"+ct_players[k]['has_defusekit']+"</td>"+
                         "<td>"+ct_players[k]['has_bomb']+"</td>"+
                         "<td>"+ct_players[k]['is_alive']+"</td>"+
                         "<td>"+ct_players[k]['blinded_time']+"</td>"+
                         "<td>"+ct_players[k]['money']+"</td>"+
                         
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
                     "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>kills</th>"+
                         "<th>headshot_kills</th>"+
                         "<th>deaths</th>"+
                         "<th>assists</th>"+
                         "<th>flash_assist</th>"+
                         "<th>adr</th>"+
                         "<th>first_kills</th>"+
                         "<th>first_deaths</th>"+
                         "<th>multi_kills</th>"+
                         "<th>one_on_x_clutches</th>"+
                         "<th>kast</th>"+
                         "<th>ping</th>"+          
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                         "<td style='color:red'>"+ct_players[k]['kills']+"</td>"+
                         "<td>"+ct_players[k]['headshot_kills']+"</td>"+
                         "<td>"+ct_players[k]['deaths']+"</td>"+
                         "<td>"+ct_players[k]['assists']+"</td>"+
                         "<td>"+ct_players[k]['flash_assist']+"</td>"+
                         "<td>"+ct_players[k]['adr']+"</td>"+
                         "<td>"+ct_players[k]['first_kills']+"</td>"+
                         "<td>"+ct_players[k]['first_deaths']+"</td>"+
                         "<td>"+ct_players[k]['multi_kills']+"</td>"+
                         "<td>"+ct_players[k]['one_on_x_clutches']+"</td>"+
                         "<td>"+ct_players[k]['kast']+"</td>"+
                         "<td>"+ct_players[k]['ping']+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
                    wCt+
                    gCt+
              "</td>":'<td></td>';
              var t_player_html= t_players.hasOwnProperty(k)?
               "<td>" +
                    "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                          "<th>nick_name</th>"+
                          "<th>player_id</th>"+
                          "<th>hp</th>"+
                         "<th>has_kevlar</th>"+
                         "<th>has_helmet</th>"+
                         "<th>has_defusekit</th>"+
                         "<th>has_bomb</th>"+
                         "<th>is_alive</th>"+
                         "<th>blinded_time</th>"+
                         "<th>money</th>"+
                         
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                         "<td style='color:red'>"+t_players[k]['player']['nick_name']+"</td>"+
                        "<td>"+t_players[k]['player']['player_id']+"</td>"+
                         "<td>"+t_players[k]['hp']+"</td>"+
                         "<td>"+t_players[k]['has_kevlar']+"</td>"+
                         "<td>"+t_players[k]['has_helmet']+"</td>"+
                         "<td>"+t_players[k]['has_defusekit']+"</td>"+
                         "<td>"+t_players[k]['has_bomb']+"</td>"+
                         "<td>"+t_players[k]['is_alive']+"</td>"+
                         "<td>"+t_players[k]['blinded_time']+"</td>"+
                         "<td>"+t_players[k]['money']+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
                     "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>kills</th>"+
                         "<th>headshot_kills</th>"+
                         "<th>deaths</th>"+
                         "<th>assists</th>"+
                         "<th>flash_assist</th>"+
                         "<th>adr</th>"+
                         "<th>first_kills</th>"+
                         "<th>first_deaths</th>"+
                         "<th>multi_kills</th>"+
                         "<th>one_on_x_clutches</th>"+
                         "<th>kast</th>"+
                         "<th>ping</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td style='color:red'>"+t_players[k]['kills']+"</td>"+
                         "<td>"+t_players[k]['headshot_kills']+"</td>"+
                         "<td>"+t_players[k]['deaths']+"</td>"+
                         "<td>"+t_players[k]['assists']+"</td>"+
                         "<td>"+t_players[k]['flash_assist']+"</td>"+
                         "<td>"+t_players[k]['adr']+"</td>"+
                         "<td>"+t_players[k]['first_kills']+"</td>"+
                         "<td>"+t_players[k]['first_deaths']+"</td>"+
                         "<td>"+t_players[k]['multi_kills']+"</td>"+
                         "<td>"+t_players[k]['one_on_x_clutches']+"</td>"+
                         "<td>"+t_players[k]['kast']+"</td>"+
                         "<td>"+t_players[k]['ping']+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
                     wT+
                    gT+
              "</td>":'<td></td>';
              player_list += 
                "<tr>"+
                 "<td>player</td>"+
                 ct_player_html+
                 t_player_html+
                 "</tr>"
              ;
              
              
        }
         
        return player_list;
    } 
    
    match_winner_image = arr["match_winner"]["image"]?"<img width='30px' src="+arr["match_winner"]["image"]+"></img>":"";
    battle_winner_image = arr["battle_winner"]["image"]?"<img width='30px' src="+arr["battle_winner"]["image"]+"></img>":"";
    
    //  console.log(json);
    document.getElementById("main").innerHTML = 
           
           "<fieldset style='text-align:center' class='layui-elem-field'>"+
      "<legend>"+mytime+"</legend>"+
     " <div class='layui-field-box'>"+
       "event_id: <span style='color:red'>"+events.event_id+"</span> event_type :  <span style='color:red'>"+events.event_type+"</span>"+
      
      "</div>"+
    "</fieldset>"+
              "<hr class='layui-bg-blue'>"+
          
            //比赛
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                      
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>match_id</th>"+
                         "<th>battle_id</th>"+
                         "<th>battle_order</th>"+
                         "<th>duration</th>"+
                         "<th>is_bomb_planted</th>"+
                         "<th>time_since_plant</th>"+
                       
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td >"+arr["match_id"]+"</td>"+
                         "<td>"+arr["battle_id"]+"</td>"+
                         "<td>"+arr["battle_order"]+"</td>"+
                         "<td>"+arr["duration"]+"</td>"+
                         "<td>"+arr["is_bomb_planted"]+"</td>"+
                         "<td>"+arr["time_since_plant"]+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
            "<hr class='layui-bg-blue'>"+
             //round
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                      
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>current_round</th>"+
                         "<th>in_round_timestamp</th>"+
                         "<th>round_time</th>"+
                         "<th>is_finished</th>"+
                         "<th>is_pause</th>"+
                         "<th>is_live</th>"+
                         "<th>is_freeze_time</th>"+
                       
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td >"+arr["current_round"]+"</td>"+
                         "<td>"+arr["in_round_timestamp"]+"</td>"+
                         "<td>"+arr["round_time"]+"</td>"+
                         "<td>"+arr["is_finished"]+"</td>"+
                         "<td>"+arr["is_pause"]+"</td>"+
                         "<td>"+arr["is_live"]+"</td>"+
                         "<td>"+arr["is_freeze_time"]+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
             "<hr class='layui-bg-blue'>" +
            //地图
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                      
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th>map_id</th>"+
                         "<th>name</th>"+
                         "<th>name_cn</th>"+
                         "<th>external_id</th>"+
                         "<th>short_name</th>"+
                         "<th>map_type</th>"+
                         "<th>map_type_cn</th>"+
                         "<th>is_default</th>"+
                         "<th>square_image</th>"+
                         "<th>rectangle_image</th>"+
                         "<th>thumbnail</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td>"+arr["map"]["map_id"]+"</td>"+
                         "<td>"+arr["map"]["name"]+"</td>"+
                         "<td>"+arr["map"]["name_cn"]+"</td>"+
                         "<td>"+arr["map"]["external_id"]+"</td>"+
                         "<td>"+arr["map"]["short_name"]+"</td>"+
                         "<td>"+arr["map"]["map_type"]+"</td>"+
                         "<td>"+arr["map"]["map_type_cn"]+"</td>"+
                         "<td>"+arr["map"]["is_default"]+"</td>"+
                         "<td>"+square_image_html+"</td>"+
                         "<td>"+rectangle_image_html+"</td>"+
                         "<td>"+thumbnail_html+"</td>"+
                         
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
            //match_scores
            "<hr class='layui-bg-blue'>" +
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                      
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th style='color:green'>match_scores</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                         "<th>score</th>"+
                         "<th style='color:green'>match_scores</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                         "<th>score</th>"+
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td>ct</td>"+
                          "<td>"+arr["match_scores"][0]["team_id"]+"</td>"+
                          "<td> "+arr["match_scores"][0]["name"]+"</td>"+
                          "<td> "+match_score_0_image+"</td>"+
                          "<td> "+arr["match_scores"][0]["opponent_order"]+"</td>"+
                          "<td style='color:red'>"+arr["match_scores"][0]["score"]+"</td>"+
                          "<td>terrorist</td>"+
                          "<td> "+arr["match_scores"][1]["team_id"]+"</td>"+
                          "<td>"+arr["match_scores"][1]["name"]+"</td>"+
                          "<td>"+match_score_1_image+"</td>"+
                          "<td>"+arr["match_scores"][1]["opponent_order"]+"</td>"+
                          "<td style='color:red'>"+arr["match_scores"][1]["score"]+"</td>"+
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
            
            
          //winner
          "<hr class='layui-bg-blue'>" +
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th style='color:green'>match_winner</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                       
                         "<th style='color:green'>battle_winner</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                       
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td>match_winner</td>"+
                          "<td>"+arr["match_winner"]["team_id"]+"</td>"+
                          "<td>"+arr["match_winner"]["name"]+"</td>"+
                          "<td>"+match_winner_image+"</td>"+
                          "<td>"+arr["match_winner"]["opponent_order"]+"</td>"+
                          "<td>battle_winner</td>"+
                          "<td>"+arr["battle_winner"]["team_id"]+"</td>"+
                          "<td >"+arr["battle_winner"]["name"]+"</td>"+
                          "<td >"+battle_winner_image+"</td>"+
                          "<td >"+arr["battle_winner"]["opponent_order"]+"</td>"+
                        
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
            
            "<hr class='layui-bg-blue'>" +
           //starting_ct starting_t
      
             "<table class='layui-table' lay-size='sm'>"+
                     "<colgroup>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                       "<col width='15'>"+
                     "</colgroup>"+
                     "<thead>"+
                       "<tr>"+
                         "<th style='color:green'>starting_ct</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                      
                         "<th style='color:green'>starting_t</th>"+
                         "<th>team_id</th>"+
                         "<th>name</th>"+
                         "<th>image</th>"+
                         "<th>opponent_order</th>"+
                       
                       "</tr>"+
                     "</thead>"+
                     "<tbody>"+
                       "<tr>"+
                          "<td >starting_ct</td>"+
                          "<td >"+arr["starting_ct"]["team_id"]+"</td>"+
                          "<td >"+arr["starting_ct"]["name"]+"</td>"+
                          "<td >"+"<img width='30px' src="+arr["starting_ct"]["image"]+"></img>"+"</td>"+
                          "<td >"+arr["starting_ct"]["opponent_order"]+"</td>"+
                        
                          "<td >starting_t</td>"+
                          "<td >"+arr["starting_t"]["team_id"]+"</td>"+
                          "<td >"+arr["starting_t"]["name"]+"</td>"+
                          "<td >"+"<img width='30px' src="+arr["starting_t"]["image"]+"></img>"+"</td>"+
                          "<td >"+arr["starting_t"]["opponent_order"]+"</td>"+
                       
                       "</tr>"+
                     "</tbody>"+
                    "</table>"+
            
             
           
            
            //side players
           "<hr class='layui-bg-blue'>"+
    "<table class='layui-table' >"+
     "<colgroup>"+
       "<col width='30'>"+
       "<col width='150'>"+
       "<col width='200'>"+
       "<col>"+
     "</colgroup>"+
     "<thead>"+
       "<tr>"+
           "<th>field</th>"+
          "<th>Ct</th>"+
         "<th>Terrorist</th>"+
       "</tr>"+
     "</thead>"+
     "<tbody>"+
       "<tr>"+
         "<td>score</td>"+
         "<td style='color:red'>"+arr['side'][0]['score']+"</td>"+
         "<td style='color:red'>"+arr['side'][1]['score']+"</td>"+
        
       "</tr>"+
            player_list_html+
       "</tr>"+
     "</tbody>"+
    "</table>"+
    "<hr class='layui-bg-blue'>"+
    ct_h_html+
    
                           
            "<hr class='layui-bg-blue'>";
    
        
                   
            
        
    //document.getElementById("match_id").innerHTML = mytime+"<br>" + "<hr>" +"match_id : " +arr["match_id"]+"<hr>";
    //document.getElementsByTagName("body")[0].innerHTML=e.data;
    //    document.getElementById("content").innerHTML = mytime+"<br>"+e.data;
    //    console.log("receive：" + e.data);
};
layui.use('element', function() {
        var element = layui.element;
        element.init();
        });
</script>
LABEL;
        echo $html;
exit;
    }
}
