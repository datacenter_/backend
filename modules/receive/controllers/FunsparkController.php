<?php

namespace app\modules\receive\controllers;

use app\controllers\GuestController;
use app\modules\receive\models\ReceiveData5eplay;
use app\modules\receive\models\ReceiveDataFunspark;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\web\Controller;

/**
 * Default controller for the `receive` module
 */
class FunsparkController extends GuestController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUlti()
    {
        $info=file_get_contents("php://input");
        $mp=new ReceiveDataFunspark();
        $mp->setAttributes([
            'log'=>$info,
            'ip_address'=>$_SERVER['HTTP_X_REAL_IP']?$_SERVER['HTTP_X_REAL_IP']:\Yii::$app->getRequest()->getUserHost(),
        ]);
        $status = $mp->save();
        // 推送增量
        $tag = QueueServer::getTag(
            QueueServer::QUEUE_ORIGIN_LOG_FORMAT,
            QueueServer::QUEUE_ORIGIN_FIVE,
            '',
            '',
            QueueServer::QUEUE_ORIGIN_FIVE_EXT_FUNSPARK
        );
        $info = ReceiveDataFunspark::find()->where(['id'=>$mp->id])->one();
        $item = [
            "tag" => $tag,
            "params" => $info->toArray(),
        ];
        TaskRunner::addTask($item, 1);
        return $status;
    }
}
