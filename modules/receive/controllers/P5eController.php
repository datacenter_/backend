<?php

namespace app\modules\receive\controllers;

use app\controllers\GuestController;
use app\modules\common\services\RestHelper;
use app\modules\receive\models\ReceiveData5eplay;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\web\Controller;

/**
 * Default controller for the `receive` module
 */
class P5eController extends GuestController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLog()
    {
        $info = file_get_contents("php://input");
        $mp = new ReceiveData5eplay();
//        $ip = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : \Yii::$app->getRequest()->getUserHost();
        $ip=RestHelper::getRealIp(\Yii::$app->getRequest()->getUserHost());
        $mp->setAttributes([
            'log' => $info,
            'ip_address' => $ip,
        ]);
        $status = $mp->save();
        $redis_server = 'order_csgoformatlog_'.$ip;
        // 推送增量
        $tag = QueueServer::getTag(
            QueueServer::QUEUE_ORIGIN_LOG_FORMAT,
            QueueServer::QUEUE_ORIGIN_FIVE,
            '',
            '',
            QueueServer::QUEUE_ORIGIN_FIVE_EXT_5E
        );
        $info = ReceiveData5eplay::find()->where(['id'=>$mp->id])->one();
        $item = [
            "tag" => $tag,
            "params" => $info->toArray(),
            "redis_server"=>$redis_server
        ];
        TaskRunner::addTask($item, 9002);
        return $status;
    }
}
