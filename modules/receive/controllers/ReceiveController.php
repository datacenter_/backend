<?php
/**
 *
 */

namespace app\modules\receive\controllers;


use app\controllers\GuestController;
use app\modules\receive\models\ReceiveData5eplay;
use app\modules\receive\models\ReceiveDataFunspark;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class ReceiveController extends GuestController
{
    public function actionDebug()
    {
        // 这里用来接收ip地址，时间，日志类型，和日志，存入对应的结构中，并且添加触发时间
        $info = $this->pPost();
        $redis_server = 'order_csgoformatlog_'.$info['ip_address'];
        switch ($info['log_type']) {
            case 1:
                // 5e 的日志
                $mp = new ReceiveData5eplay();
                $mp->setAttributes([
                    'log' => $info['log'],
                    'ip_address' => $info['ip_address'],
                ]);
                $status = $mp->save();
                // 推送增量
                $tag = QueueServer::getTag(
                    QueueServer::QUEUE_ORIGIN_LOG_FORMAT,
                    QueueServer::QUEUE_ORIGIN_FIVE,
                    '',
                    '',
                    QueueServer::QUEUE_ORIGIN_FIVE_EXT_5E
                );
                $info = ReceiveData5eplay::find()->where(['id' => $mp->id])->one();
                $item = [
                    "tag" => $tag,
                    "params" => $info->toArray(),
                    "redis_server"=>$redis_server
                ];
                TaskRunner::addTask($item, 9002);
                return $status;
                break;
            case 2:
                // spark的日志
                $mp = new ReceiveDataFunspark();
                $mp->setAttributes([
                    'log' => $info['log'],
                    'ip_address' => $info['ip_address'],
                ]);
                $status = $mp->save();
                // 推送增量
                $tag = QueueServer::getTag(
                    QueueServer::QUEUE_ORIGIN_LOG_FORMAT,
                    QueueServer::QUEUE_ORIGIN_FIVE,
                    '',
                    '',
                    QueueServer::QUEUE_ORIGIN_FIVE_EXT_FUNSPARK
                );
                $info = ReceiveDataFunspark::find()->where(['id' => $mp->id])->one();
                $item = [
                    "tag" => $tag,
                    "params" => $info->toArray(),
                    "redis_server"=>$redis_server
                ];
                TaskRunner::addTask($item, 9002);
                return $status;
                break;
        }
    }
}