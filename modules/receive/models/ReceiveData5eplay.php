<?php

namespace app\modules\receive\models;

use Yii;

/**
 * This is the model class for table "receive_data_5eplay".
 *
 * @property int $id
 * @property string|null $log 日志信息
 * @property string|null $ip_address
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class ReceiveData5eplay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receive_data_5eplay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['ip_address'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log' => '日志信息',
            'ip_address' => 'Ip Address',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
