<?php
/**
 *
 */

namespace app\modules\task\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\EnumOrigin;
use app\modules\task\models\HotDataLog;
use app\modules\task\services\ContrastService;
use app\rest\exceptions\BusinessException;

class ContrastController extends WithTokenAuthController
{
    public function actionDetail()
    {
        $params = $this->pGet();
        return ContrastService::getDetail($params);
    }
    public function actionMasterData()
    {
        $params = $this->pPost();
        return ContrastService::getMasterData($params);
    }
    public function actionRefresh()
    {
        $paras = $this->pPost();
        return ContrastService::refresh($paras);
    }

    public function actionEnums()
    {
        $games=EnumService::getEnum('games');
        foreach($games as $key=>$val){
            $gameType[]=[
                'key'=>$key,
                'label'=>$val['name']
            ];
        }
        $origin = EnumService::getEnum('origin');
        $newOrigin = [];
        foreach ($origin as $key => $value){
            $newOrigin[$key]['key'] = $value['name'];
            $newOrigin[$key]['label'] = $value['desc'];
        }
        return [
            'origin_type' => $newOrigin,
            'resource_type' => [
                ['key' => 'player', 'label' => '选手'],
                ['key' => 'team', 'label' => '战队'],
                ['key' => 'match', 'label' => '比赛'],
                ['key' => 'tournament', 'label' => '赛事'],
                ['key' => Consts::METADATA_TYPE_DOTA2_HERO, 'label' => 'dota2英雄'],
                ['key' => Consts::METADATA_TYPE_DOTA2_ABILITY, 'label' => 'dota2技能'],
                ['key' => Consts::METADATA_TYPE_DOTA2_TALENT, 'label' => 'dota2天赋'],
                ['key' => Consts::METADATA_TYPE_DOTA2_ITEM, 'label' => 'dota2道具'],
                ['key' => Consts::METADATA_TYPE_LOL_RUNE, 'label' => 'lol符文'],
                ['key' => Consts::METADATA_TYPE_LOL_ITEM, 'label' => 'lol道具'],
                ['key' => Consts::METADATA_TYPE_LOL_ABILITY, 'label' => 'lol技能'],
                ['key' => Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, 'label' => 'lol召唤师技能'],
                ['key' => Consts::METADATA_TYPE_LOL_CHAMPION, 'label' => 'lol英雄'],
                ['key' => Consts::METADATA_TYPE_CSGO_MAP, 'label' => 'csgo地图'],
                ['key' => Consts::METADATA_TYPE_CSGO_WEAPON, 'label' => 'csgo武器'],
            ],
            'game_type' => $gameType,
        ];
    }

    public function actionMatch()
    {
        $matchId=$this->pGet('match_id');
        $info=HotDataLog::find()->where(['match_id'=>$matchId])->orderBy('id desc')->asArray()->one();
        if(!$info){
            throw new BusinessException([],'没有转化数据');
        }
        $info['info_rest']=json_decode($info['info_rest'],true);
        $info['info_format']=json_decode($info['info_format'],true);
        return $info;
    }
}