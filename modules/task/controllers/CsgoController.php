<?php
// 临时测试文件,不提交
namespace app\modules\task\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\data\models\ReceiveData5eplay;
use app\modules\data\models\ReceiveDataFunspark;
use app\modules\task\services\hot\five\FiveHotCsgoMatch;
use app\modules\task\services\logformat\CsgoFunspark;
use app\modules\task\services\hot\five\csgo\Match;
use app\modules\common\services\WinnerHelper;
use app\modules\match\controllers\MatchController;

class CsgoController extends WithTokenAuthController
{
    /**
     * 接口测试
     */
    public function actionTest()
    {
        $logBeginTime = '2020-07-23 16:07:35';
        $logEndTime = '2020-07-23 17:20:00';
        $ipAddress = '47.244.77.240';
        $list=\app\modules\task\services\logformat\Csgo5e::formatList($logBeginTime, $logEndTime, $ipAddress);
        return $list;
        // print_r($list);
    }

    /**
     * 临时测试,完成后删除
     */
    public function actionGame()
    {
        $list =\app\modules\task\services\logformat\Csgo5e::getPlayAndTeamInfo("");
        return $list;
    }

    /**
     * 临时测试,用完后删除
     */
    public function actionFive()
    {
        $data = new FiveHotCsgoMatch();
        $list = $data->playerConversionSteamId("","");
        return $list;
    }

    /**
     * 临时测试,用完后删除
     */
    public function actionFunspark()
    {
        $data = new CsgoFunspark();
        $list = $data->getFunsparkSwitchTeam("");
        return $list;
    }

    /**
     * 测试match文件相关
     */
    public function actionMatch()
    {
        $data = new Match();
        $info = $data->mapNameChangeId("");
        return $info;
    }

    /**
     * 查看日志信息(用完删除)  By  王傲渊
     */
    public function actionInfo()
    {
        $list = [];
        $find = 'log';
        // 约束条件
        // $map = ['>=', 'created_at', '2020-08-03 09:30:00'];
        // $where = ['<=','created_at','2020-08-03 23:59:59'];
        $ip = ['=','ip_address','213.202.219.80'];
        // $list = ReceiveData5eplay::find()
        $list = ReceiveDataFunspark::find()
            ->select($find)
            // ->where($map)
            // ->andwhere($where)
            ->where($ip)
            ->orderBy('id asc')
            ->asArray()
            ->all();
        $eventList = [];
        foreach ($list as $val) {
            $log = $val['log'];
            $listLines = explode("\n", $log);
            foreach($listLines as $key => &$value){
                if($value){
                    $eventList[] = $value;
                }
            }
        }
        return $eventList;
    }
}
