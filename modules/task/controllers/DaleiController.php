<?php
/**
 *
 */

namespace app\modules\task\controllers;


use app\controllers\RestController;

class DaleiController extends RestController
{
    public function actionUrl()
    {
        $params=[
            'scheduleid'=>'leoom250d55',
            'starttime'=>'1596531600',
            'matchorder'=>2,
        ];
        $params=$this->pGet();
        $infoJson=\app\modules\task\services\grab\orange\OrangeBase::getWsUrl('/lol/match/timeline',$params);
        return [
            'url'=>$infoJson,
        ];
    }
}