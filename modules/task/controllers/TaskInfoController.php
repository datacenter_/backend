<?php
/**
 *
 */

namespace app\modules\task\controllers;


use app\controllers\WithTokenAuthController;
use app\modules\task\services\TaskInfoService;

class TaskInfoController extends WithTokenAuthController
{
    public function actionList()
    {
        $params = $this->pGet();
        return TaskInfoService::getList($params);
    }

    public function actionDetail()
    {
        $id = $this->pGet('id');
        return TaskInfoService::detail($id);
    }

    public function actionReAdd()
    {
        $id = $this->pGet('id');
        return TaskInfoService::reAdd($id);
    }

    /**
     * 重抓全量
     * @return array|array[]|bool|object|object[]|string|string[]
     * @throws \app\rest\exceptions\BusinessException
     */
    public function actionGetOriginData()
    {
        $params=\Yii::$app->request->post();
        $resourceType = $params['resource_type'];
        $originId = $params['origin_id'];
        $gameId = $params['game_id'];
        $reason = $params['reason'] ? $params['reason'] : "";
        return TaskInfoService::getOriginData($resourceType,$originId,$gameId,$reason);
    }
}