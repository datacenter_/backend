<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "battles_all".
 *
 * @property int $id
 * @property int|null $matche_id 外键id
 * @property string|null $order_bat 排序
 * @property string|null $mapstats_id 对局统计数据ID
 * @property string|null $teamid_one 战队one
 * @property string|null $teamid_two 战队two
 * @property string|null $opponent_one 对战排序one
 * @property string|null $opponent_two 对战排序two
 * @property string|null $map 地图
 * @property string|null $score_one 比分one
 * @property string|null $score_two 比分two
 * @property string|null $starting_side 开局阵营
 * @property string|null $first_half_score 上半场比
 * @property string|null $end_half_score 下半场比分
 * @property string|null $ot_score 加时比分
 * @property string|null $team_rating 战队Rating
 * @property string|null $first_kills 首杀
 * @property string|null $clutches_won 残局获胜
 */
class BattlesAll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'battles_all';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matche_id'], 'integer'],
            [['order_bat', 'mapstats_id', 'teamid_one', 'teamid_two', 'opponent_one', 'opponent_two', 'map', 'score_one', 'score_two', 'starting_side', 'first_half_score', 'end_half_score', 'ot_score', 'team_rating', 'first_kills', 'clutches_won'], 'string', 'max' => 32],
            [['matche_id', 'order_bat', 'mapstats_id'], 'unique', 'targetAttribute' => ['matche_id', 'order_bat', 'mapstats_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matche_id' => 'Matche ID',
            'order_bat' => 'Order Bat',
            'mapstats_id' => 'Mapstats ID',
            'teamid_one' => 'Teamid One',
            'teamid_two' => 'Teamid Two',
            'opponent_one' => 'Opponent One',
            'opponent_two' => 'Opponent Two',
            'map' => 'Map',
            'score_one' => 'Score One',
            'score_two' => 'Score Two',
            'starting_side' => 'Starting Side',
            'first_half_score' => 'First Half Score',
            'end_half_score' => 'End Half Score',
            'ot_score' => 'Ot Score',
            'team_rating' => 'Team Rating',
            'first_kills' => 'First Kills',
            'clutches_won' => 'Clutches Won',
        ];
    }
}
