<?php

namespace app\modules\task\models;

use Yii;

class BayesCoverages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bayes_coverages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id'],'integer'],
            [['rel_identity_id','provider','types'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }
}
