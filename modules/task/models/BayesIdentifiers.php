<?php

namespace app\modules\task\models;

use Yii;

class BayesIdentifiers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bayes_identifiers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id'],'integer'],
            [['resource_type','rel_identity_id','type','value'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }
}
