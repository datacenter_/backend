<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "enum_origin".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $desc
 * @property string|null $info
 * @property string|null $config
 */
class EnumOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desc', 'info', 'config'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'info' => 'Info',
            'config' => 'Config',
        ];
    }
}
