<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "hot_data_log".
 *
 * @property int $id
 * @property int|null $hot_id
 * @property string|null $info_rest 获取的信息
 * @property string|null $info_format 格式化的信息
 * @property int|null $task_id 关联的task_id
 * @property int|null $deal_status 处理状态1待处理2处理中3成功4失败
 * @property int|null $match_id 刷到的id
 * @property string|null $time_begin 执行时间
 * @property string|null $time_end 执行时间
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 */
class HotDataLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hot_data_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hot_id', 'task_id', 'deal_status', 'match_id'], 'integer'],
            [['info_rest', 'info_format'], 'string'],
            [['time_begin', 'time_end', 'created_time', 'modified_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hot_id' => 'Hot ID',
            'info_rest' => 'Info Rest',
            'info_format' => 'Info Format',
            'task_id' => 'Task ID',
            'deal_status' => 'Deal Status',
            'match_id' => 'Match ID',
            'time_begin' => 'Time Begin',
            'time_end' => 'Time End',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
