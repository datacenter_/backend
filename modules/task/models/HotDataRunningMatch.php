<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "hot_data_running_match".
 *
 * @property int $id
 * @property int|null $standard_id 标准库id
 * @property int|null $match_id 主表match_id
 * @property int|null $origin_id 数据源
 * @property int|null $game_id 游戏（冗余，方便用）
 * @property string|null $rel_identity_id 数据源方的id（冗余，方便用）
 * @property string|null $match_start_time 比赛开始时间
 * @property string|null $match_end_time 比赛结束时间
 * @property int|null $auto_status 1刷新2停止
 * @property int|null $match_status 比赛状态1等待2开始3结束
 * @property int|null $deal_status 处理状态1待处理2处理中3处理完成4处理失败
 * @property int|null $cycle 周期，隔多久刷新一次s
 * @property string|null $last_deal_status 上次处理时间
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 * @property int|null $from_event_id 刷新到的event_id
 * @property int|null $to_event_id
 */
class HotDataRunningMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hot_data_running_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['standard_id', 'match_id', 'origin_id', 'game_id', 'auto_status', 'match_status', 'deal_status', 'cycle', 'from_event_id', 'to_event_id'], 'integer'],
            [['match_start_time', 'match_end_time', 'last_deal_status', 'created_time', 'modified_time'], 'safe'],
            [['rel_identity_id','auto_add_type','auto_add_sub_type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'standard_id' => 'Standard ID',
            'match_id' => 'Match ID',
            'origin_id' => 'Origin ID',
            'game_id' => 'Game ID',
            'rel_identity_id' => 'Rel Identity ID',
            'match_start_time' => 'Match Start Time',
            'match_end_time' => 'Match End Time',
            'auto_status' => 'Auto Status',
            'auto_add_type' => 'Auto Add Type',
            'match_status' => 'Match Status',
            'deal_status' => 'Deal Status',
            'cycle' => 'Cycle',
            'last_deal_status' => 'Last Deal Status',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'from_event_id' => 'From Event ID',
            'to_event_id' => 'To Event ID',
        ];
    }
}
