<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "match_all".
 *
 * @property int $id
 * @property int|null $matche_id 比事id
 * @property string|null $match_type 比赛类型
 * @property string|null $number_of_games 赛制局数
 * @property string|null $description 描述
 * @property string|null $tournament_id 赛事ID
 * @property string|null $scheduled_begin_at 计划开始时间
 * @property string|null $status 状态
 * @property string|null $maps 比赛地图
 * @property string|null $team_one 战队one
 * @property string|null $team_two 战队two
 * @property string|null $score_one 比分one
 * @property string|null $score_two 比分two
 * @property string|null $matchold_one 历史比赛one
 * @property string|null $matchold_two 历史比赛two
 * @property string|null $update_at 变更时间
 * @property string|null $created_at 创建时间
 * @property int|null $match_time 比赛开始时间
 * @property int|null $end_time 比赛结束时间+10分钟
 */
class MatchAll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_all';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matche_id', 'match_time', 'end_time'], 'integer'],
            [['update_at', 'created_at'], 'safe'],
            [['match_type', 'number_of_games', 'tournament_id', 'status', 'team_one', 'team_two', 'score_one', 'score_two'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 1000],
            [['scheduled_begin_at'], 'string', 'max' => 50],
            [['maps', 'matchold_one', 'matchold_two'], 'string', 'max' => 100],
            [['matche_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matche_id' => 'Matche ID',
            'match_type' => 'Match Type',
            'number_of_games' => 'Number Of Games',
            'description' => 'Description',
            'tournament_id' => 'Tournament ID',
            'scheduled_begin_at' => 'Scheduled Begin At',
            'status' => 'Status',
            'maps' => 'Maps',
            'team_one' => 'Team One',
            'team_two' => 'Team Two',
            'score_one' => 'Score One',
            'score_two' => 'Score Two',
            'matchold_one' => 'Matchold One',
            'matchold_two' => 'Matchold Two',
            'update_at' => 'Update At',
            'created_at' => 'Created At',
            'match_time' => 'Match Time',
            'end_time' => 'End Time',
        ];
    }

    public function getMatchById($realId){

    }
}
