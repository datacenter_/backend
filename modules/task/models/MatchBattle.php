<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "match_battle".
 *
 * @property int $id
 * @property int|null $game 游戏
 * @property int|null $match 比赛	
 * @property int|null $order 排序
 * @property string|null $begin_at 实际开始时间
 * @property string|null $end_at 实际结束时间
 * @property int|null $status 状态
 * @property int|null $is_draw 是否平局
 * @property int|null $if_forfeit 是否弃权
 * @property int|null $is_default_advantage 是否是默认领先获胜
 * @property int|null $map 比赛地图
 * @property int|null $duation 对局时长
 * @property int|null $winner 获胜战队
 * @property int|null $is_battle_detailed 是否有对局详情数据
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 */
class MatchBattle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game', 'match', 'order', 'status', 'is_draw', 'if_forfeit', 'is_default_advantage', 'map', 'duation', 'winner', 'is_battle_detailed'], 'integer'],
            [['begin_at', 'end_at', 'modified_at', 'created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game' => '游戏',
            'match' => '比赛	',
            'order' => '排序',
            'begin_at' => '实际开始时间',
            'end_at' => '实际结束时间',
            'status' => '状态',
            'is_draw' => '是否平局',
            'if_forfeit' => '是否弃权',
            'is_default_advantage' => '是否是默认领先获胜',
            'map' => '比赛地图',
            'duation' => '对局时长',
            'winner' => '获胜战队',
            'is_battle_detailed' => '是否有对局详情数据',
            'modified_at' => '变更日期',
            'created_at' => '创建日期',
            'deleted_at' => '删除日期',
        ];
    }
}
