<?php

namespace app\modules\task\models;

use Yii;

/**
 * Class MatchBattleRelation
 * @package app\modules\task\models
 */
class MatchBattleRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_battle_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id', 'order'], 'integer'],
            [['rel_match_id', 'rel_battle_id'], 'string'],
            [['modified_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => '比赛ID',
            'order' => '排序',
            'rel_match_id' => '数据源比赛ID',
            'rel_battle_id' => '数据源对局ID',
            'modified_at' => '变更日期',
            'created_at' => '创建日期',
        ];
    }
}
