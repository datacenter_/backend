<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_match".
 *
 * @property int $id
 * @property int $origin_id 数据源
 * @property string $rel_identity_id 源端标示
 * @property int|null $game 游戏id
 * @property int|null $game_id
 * @property int|null $tournament_id 赛事id
 * @property int|null $match_rule_id 赛制id
 * @property int|null $son_tournament_id 子赛事id
 * @property int|null $stage_id 阶段id
 * @property int|null $group_id 分组id
 * @property int|null $team_1_id 主队战队id
 * @property int|null $team_2_id 客队id
 * @property int|null $game_rules 比赛规则	1normal2battle_royale
 * @property string|null $match_type 比赛类型	
 * @property string|null $number_of_games 赛制局数	
 * @property string|null $original_scheduled_begin_at 原始计划开始时间	
 * @property string|null $scheduled_begin_at 计划开始时间	
 * @property string|null $slug
 * @property int|null $is_rescheduled 是否调整了时间表
 * @property string|null $relation
 * @property string|null $name 名称比赛
 * @property int|null $draw 是否平局
 * @property int|null $forfeit 是否弃权
 * @property string|null $end_at
 * @property string|null $begin_at 开始时间
 * @property int|null $status 比赛状态
 * @property int|null $deal_status
 * @property int|null $deleted 0-正常1-删除
 * @property int|null $cuser
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class MatchComingScoket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_coming_socket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'is_over1','is_over2','is_over3','flag','fpid1','fpid2','fpid3','connection_status1','connection_status2','connection_status3' ,'status','status1', 'status2', 'status3','status1_refresh','status2_refresh','status3_refresh'], 'integer'],
            [['begin_at', 'end_at'], 'safe'],
            [['spid1','spid2','spid3','connection_error1','connection_error2','connection_error3' ,'match_id'], 'string'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源ID',
            'game_id' => '游戏ID',
            'match_id' => '比赛ID',
            'begin_at' => '比赛开始时间',
            'end_at' => '比赛结束时间',
            'is_over' => '是否杀了进程',
            'status1' => '服务器1',
            'status2' => '服务器2',
            'status3' => '服务器3',
        ];
    }
}
