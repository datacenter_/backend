<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "match_lived_datas_offset".
 *
 * @property int $id
 * @property int|null $data_id
 * @property string|null $offset
 */
class MatchLivedDatasOffset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_lived_datas_offset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_id'], 'integer'],
            [['offset'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_id' => 'Data ID',
            'offset' => 'Offset',
        ];
    }
}
