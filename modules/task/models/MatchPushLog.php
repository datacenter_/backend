<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "match_push_log".
 *
 * @property int $id
 * @property int|null $match_id
 * @property int|null $battle_id
 * @property int|null $task_info_id
 * @property string|null $match_data
 * @property string|null $create_time
 * @property string|null $event_type
 * @property string|null $type
 */
class MatchPushLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_push_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id', 'battle_id', 'task_info_id'], 'integer'],
            [['match_data'], 'string'],
            [['create_time'], 'safe'],
            [['event_type'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'battle_id' => 'Battle ID',
            'task_info_id' => 'Task Info ID',
            'match_data' => 'Match Data',
            'create_time' => 'Create Time',
            'event_type' => 'Event Type',
            'type' => 'Type',
        ];
    }
}
