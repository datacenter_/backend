<?php

namespace app\modules\task\models;

use Yii;

class MatchSocketData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_socket_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'origin_id', 'match_id','battle_order'], 'integer'],
            [['match_data', 'type'], 'string'],
            [['create_time'], 'safe'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'origin_id' => 'Origin ID',
            'match_id' => 'Match ID',
            'battle_order' => 'Battle Order',
            'match_data' => 'Match Data',
            'type' => 'Type',
            'created_time' => 'Created Time'
        ];
    }
}
