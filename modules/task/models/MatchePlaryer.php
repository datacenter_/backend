<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "matche_plaryer".
 *
 * @property int $id
 * @property int|null $mapstats_id 外键id
 * @property string|null $tm_id 战队id
 * @property string|null $player_id 选手id
 * @property string|null $nick_name 选手昵称
 * @property string|null $kills 击杀
 * @property string|null $headshot_kills 爆头击杀
 * @property string|null $assists 助攻
 * @property string|null $flash_assists 闪光弹助攻
 * @property string|null $deaths 死亡
 * @property string|null $kast KAST
 * @property string|null $k_d_diff 击杀死亡差
 * @property string|null $adr 平均每局伤害
 * @property string|null $first_kills 首先击杀
 * @property string|null $first_deaths 首先死亡
 * @property string|null $first_kills_diff 首杀差
 * @property string|null $rating Rating 2.0
 * @property string|null $kpr 平均每局击杀
 * @property string|null $dpr 平均每局死亡
 * @property string|null $impact IMPACT
 */
class MatchePlaryer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matche_plaryer';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mapstats_id'], 'integer'],
            [['tm_id', 'player_id', 'nick_name', 'kills', 'headshot_kills', 'assists', 'flash_assists', 'deaths', 'kast', 'k_d_diff', 'adr', 'first_kills', 'first_deaths', 'first_kills_diff', 'rating', 'kpr', 'dpr', 'impact'], 'string', 'max' => 32],
            [['mapstats_id', 'player_id', 'nick_name'], 'unique', 'targetAttribute' => ['mapstats_id', 'player_id', 'nick_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mapstats_id' => 'Mapstats ID',
            'tm_id' => 'Tm ID',
            'player_id' => 'Player ID',
            'nick_name' => 'Nick Name',
            'kills' => 'Kills',
            'headshot_kills' => 'Headshot Kills',
            'assists' => 'Assists',
            'flash_assists' => 'Flash Assists',
            'deaths' => 'Deaths',
            'kast' => 'Kast',
            'k_d_diff' => 'K D Diff',
            'adr' => 'Adr',
            'first_kills' => 'First Kills',
            'first_deaths' => 'First Deaths',
            'first_kills_diff' => 'First Kills Diff',
            'rating' => 'Rating',
            'kpr' => 'Kpr',
            'dpr' => 'Dpr',
            'impact' => 'Impact',
        ];
    }
}
