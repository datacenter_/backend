<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "receive_data_5eplay_format_events_ws".
 *
 * @property int $id
 * @property string|null $info 日志信息
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class ReceiveData5epalyFormatEventsWs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receive_data_5eplay_format_events_ws';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_data','type','event_type'], 'string'],
            [['origin_id','game_id','match_id'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'OriginId',
            'game_id' => 'GameId',
            'match_id' => 'MatchId',
            'event_type' => 'EventType',
            'type' => 'Type',
            'info' => 'Info',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
