<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "receive_data_5eplay_format_events".
 *
 * @property int $id
 * @property string|null $info 日志信息
 * @property string|null $ip_address
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property int|null $is_handle
 */
class ReceiveData5eplayFormatEvents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receive_data_5eplay_format_events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info','event_type'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['is_handle','log_type','parent_id'], 'integer'],
            [['ip_address'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'info' => 'Info',
            'ip_address' => 'Ip Address',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'is_handle' => 'Is Handle',
            'type' => 'Source Type',
        ];
    }
}
