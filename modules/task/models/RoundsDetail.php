<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "rounds_detail".
 *
 * @property int $id
 * @property int|null $mapstats_id 外键id
 * @property string|null $round_ordinal 对局序号
 * @property string|null $teamone 对局战队id
 * @property string|null $teamtwo 对局战队id
 * @property string|null $ct_id 警关系
 * @property string|null $t_id 匪关系
 * @property string|null $winner 获胜方
 * @property string|null $win_type 获胜方式
 */
class RoundsDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rounds_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mapstats_id'], 'integer'],
            [['round_ordinal', 'teamone', 'teamtwo', 'ct_id', 't_id', 'winner', 'win_type'], 'string', 'max' => 32],
            [['mapstats_id', 'round_ordinal'], 'unique', 'targetAttribute' => ['mapstats_id', 'round_ordinal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mapstats_id' => 'Mapstats ID',
            'round_ordinal' => 'Round Ordinal',
            'teamone' => 'Teamone',
            'teamtwo' => 'Teamtwo',
            'ct_id' => 'Ct ID',
            't_id' => 'T ID',
            'winner' => 'Winner',
            'win_type' => 'Win Type',
        ];
    }
}
