<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_team".
 *
 * @property int $id
 * @property int|null $origin_id 数据源
 * @property string|null $rel_identity_id 源端标示
 * @property string|null $team_name 战队名称
 * @property int|null $clan_id 俱乐部id
 * @property int|null $game_id 游戏id
 * @property int|null $country 战队所属地
 * @property string|null $name 全称
 * @property string|null $short_name 简称
 * @property string|null $alias 别名
 * @property string|null $logo logo
 * @property int|null $cuser
 * @property int $deleted
 * @property string $created_at
 * @property string $modified_at
 */
class StandardDataGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'rel_identity_id', 'tournament_id', 'tree_id', 'game', 'deleted','deal_status','stage_id'], 'integer'],
            [['name', 'name_cn', 'begin_at', 'end_at','number_of_teams','advance','eliminate'
            ,'win_points','draw_ponits','lose_points'
            ], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源',
            'rel_identity_id' => '源端标示',
            'team_name' => '战队名称',
            'clan_id' => '俱乐部id',
            'game_id' => '游戏id',
            'country' => '战队所属地',
            'name' => '全称',
            'short_name' => '简称',
            'alias' => '别名',
            'logo' => 'logo',
            'cuser' => 'Cuser',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
