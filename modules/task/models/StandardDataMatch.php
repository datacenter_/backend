<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_match".
 *
 * @property int $id
 * @property int $origin_id 数据源
 * @property string $rel_identity_id 源端标示
 * @property int|null $game 游戏id
 * @property int|null $game_id
 * @property string|null $tournament_id serie_id
 * @property int|null $match_rule_id 赛制id
 * @property int|null $son_tournament_id 子赛事id
 * @property int|null $stage_id 阶段id
 * @property int|null $group_id 分组id
 * @property string|null $team_1_id 主队战队id
 * @property string|null $team_2_id 客队id
 * @property string|null $team_1_score 主队分数
 * @property string|null $team_2_score 客队分数
 * @property string|null $winner 获胜队伍
 * @property int|null $game_rules 比赛规则	1normal2battle_royale
 * @property string|null $match_type 比赛类型	
 * @property string|null $number_of_games 赛制局数	
 * @property string|null $original_scheduled_begin_at 原始计划开始时间	
 * @property string|null $scheduled_begin_at 计划开始时间	
 * @property string|null $slug
 * @property int|null $is_rescheduled 是否调整了时间表
 * @property string|null $relation
 * @property string|null $name 名称比赛
 * @property int|null $draw 是否平局
 * @property int|null $forfeit 是否弃权
 * @property string|null $end_at 结束时间
 * @property string|null $begin_at 开始时间
 * @property int|null $status 比赛状态
 * @property int|null $deal_status
 * @property int|null $deleted 1-删除,2正常
 * @property int|null $cuser
 * @property int|null $is_battle_detailed 是否有数据详情，1表示有，2表示无
 * @property int|null $is_pbpdata_supported 是否提供实时数据
 * @property string|null $live_url
 * @property string|null $embed_url 直播流嵌入地址
 * @property string|null $official_stream_url
 * @property string|null $default_advantage 默认领先战队
 * @property string|null $description 比赛名称/比赛简介中文
 * @property string|null $location 比赛地点
 * @property string|null $location_cn 比赛地点英文
 * @property string|null $round_order 轮次
 * @property string|null $round_name 轮次名称
 * @property string|null $round_name_cn 轮次名称中文
 * @property string|null $bracket_pos 对阵表位置
 * @property string|null $description_cn 比赛简介中文
 * @property int|null $auto_status 自动同步状态
 * @property string|null $map_info 地图阵容
 * @property string|null $map_ban_pick 地图信息
 * @property int|null $map_vetoes
 * @property int|null $is_streams_supported 是否提供视频直播源
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property string|null $unbound_identity_data 绑定标识数据
 * @property string|null $missing_field_for_binding 缺失字段
 * @property string|null $battle_forfeit 单图(比赛)弃权
 * @property string|null $coverage abios battle  coverage信息
 * @property string|null $battlesIds abios battle 的所有Ids
 * @property int|null $has_incident_report 是否支持incidents    1-是，2-否
 * @property string|null $deleted_at
 */
class StandardDataMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id'], 'required'],
            [['origin_id', 'game', 'game_id', 'match_rule_id', 'son_tournament_id', 'group_id', 'game_rules', 'is_rescheduled', 'draw', 'forfeit', 'status', 'deal_status', 'deleted', 'cuser', 'is_battle_detailed', 'is_pbpdata_supported', 'auto_status', 'map_vetoes', 'is_streams_supported', 'has_incident_report'], 'integer'],
            [['original_scheduled_begin_at', 'scheduled_begin_at', 'end_at', 'begin_at', 'created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['description', 'bracket_pos', 'description_cn', 'map_info', 'map_ban_pick', 'unbound_identity_data', 'coverage','game_version','perid','origin_modified_at'], 'string'],
            [['rel_identity_id', 'tournament_id','stage_id', 'team_1_id', 'team_2_id', 'relation'], 'string', 'max' => 100],
            [['team_1_score', 'team_2_score'], 'string', 'max' => 200],
            [['winner', 'match_type', 'number_of_games', 'slug', 'name', 'live_url', 'embed_url', 'official_stream_url', 'default_advantage', 'location', 'location_cn', 'round_order', 'round_name', 'round_name_cn', 'missing_field_for_binding', 'battle_forfeit', 'battlesIds'], 'string', 'max' => 255],
            [['origin_id', 'rel_identity_id', 'game_id'], 'unique', 'targetAttribute' => ['origin_id', 'rel_identity_id', 'game_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'rel_identity_id' => 'Rel Identity ID',
            'game' => 'Game',
            'game_id' => 'Game ID',
            'tournament_id' => 'Tournament ID',
            'match_rule_id' => 'Match Rule ID',
            'son_tournament_id' => 'Son Tournament ID',
            'stage_id' => 'Stage ID',
            'group_id' => 'Group ID',
            'team_1_id' => 'Team 1 ID',
            'team_2_id' => 'Team 2 ID',
            'team_1_score' => 'Team 1 Score',
            'team_2_score' => 'Team 2 Score',
            'winner' => 'Winner',
            'game_rules' => 'Game Rules',
            'match_type' => 'Match Type',
            'number_of_games' => 'Number Of Games',
            'original_scheduled_begin_at' => 'Original Scheduled Begin At',
            'scheduled_begin_at' => 'Scheduled Begin At',
            'slug' => 'Slug',
            'is_rescheduled' => 'Is Rescheduled',
            'relation' => 'Relation',
            'name' => 'Name',
            'draw' => 'Draw',
            'forfeit' => 'Forfeit',
            'end_at' => 'End At',
            'begin_at' => 'Begin At',
            'status' => 'Status',
            'deal_status' => 'Deal Status',
            'deleted' => 'Deleted',
            'cuser' => 'Cuser',
            'is_battle_detailed' => 'Is Battle Detailed',
            'is_pbpdata_supported' => 'Is Pbpdata Supported',
            'live_url' => 'Live Url',
            'embed_url' => 'Embed Url',
            'official_stream_url' => 'Official Stream Url',
            'default_advantage' => 'Default Advantage',
            'description' => 'Description',
            'location' => 'Location',
            'location_cn' => 'Location Cn',
            'round_order' => 'Round Order',
            'round_name' => 'Round Name',
            'round_name_cn' => 'Round Name Cn',
            'bracket_pos' => 'Bracket Pos',
            'description_cn' => 'Description Cn',
            'auto_status' => 'Auto Status',
            'map_info' => 'Map Info',
            'map_ban_pick' => 'Map Ban Pick',
            'map_vetoes' => 'Map Vetoes',
            'is_streams_supported' => 'Is Streams Supported',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'unbound_identity_data' => 'Unbound Identity Data',
            'missing_field_for_binding' => 'Missing Field For Binding',
            'battle_forfeit' => 'Battle Forfeit',
            'coverage' => 'Coverage',
            'battlesIds' => 'Battles Ids',
            'has_incident_report' => 'Has Incident Report',
            'deleted_at' => 'Deleted At',
        ];
    }
}
