<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_match_video_list".
 *
 * @property int $id
 * @property int|null $origin_id
 * @property int|null $game_id
 * @property string|null $title 房间标题
 * @property string|null $room_num 房间号
 * @property string|null $streamer 主播名称
 * @property string|null $platform_name 直播平台名称
 * @property int|null $country 国家id
 * @property int|null $platform_id 直播平台id
 * @property string|null $preview 预览图
 * @property string|null $official_stream_url 官方url
 * @property string|null $live_embed_url 视频地址
 * @property string|null $live_url 视频地址
 * @property string|null $video_rel_id 视频关联id
 * @property int|null $match_rel_id 赛事关联id
 * @property string|null $streams_origin_id 视频爬虫id
 * @property int|null $viewers 观看人数
 * @property int|null $status 1表示可添加，2表示已添加
 * @property string|null $live_url_identity 视频唯一url
 * @property int|null $flag 1正常，2删除
 * @property string|null $created_time 创建时间
 * @property string|null $modified_time 修改时间
 * @property int|null $video_origin_id 视频爬虫来源的id
 */
class StandardDataMatchVideoList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_match_video_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'country', 'platform_id', 'match_rel_id', 'viewers', 'status', 'flag', 'video_origin_id'], 'integer'],
            [['created_time', 'modified_time'], 'safe'],
            [['title', 'room_num', 'streamer', 'platform_name', 'official_stream_url'], 'string', 'max' => 200],
            [['preview', 'live_embed_url', 'live_url'], 'string', 'max' => 1000],
            [['video_rel_id', 'live_url_identity'], 'string', 'max' => 100],
            [['streams_origin_id'], 'string', 'max' => 20],
            [['live_url_identity'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'game_id' => 'Game ID',
            'title' => 'Title',
            'room_num' => 'Room Num',
            'streamer' => 'Streamer',
            'platform_name' => 'Platform Name',
            'country' => 'Country',
            'platform_id' => 'Platform ID',
            'preview' => 'Preview',
            'official_stream_url' => 'Official Stream Url',
            'live_embed_url' => 'Live Embed Url',
            'live_url' => 'Live Url',
            'video_rel_id' => 'Video Rel ID',
            'match_rel_id' => 'Match Rel ID',
            'streams_origin_id' => 'Streams Origin ID',
            'viewers' => 'Viewers',
            'status' => 'Status',
            'live_url_identity' => 'Live Url Identity',
            'flag' => 'Flag',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'video_origin_id' => 'Video Origin ID',
        ];
    }
}
