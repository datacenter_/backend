<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_match_video_list_relation".
 *
 * @property int $id
 * @property int|null $video_id standard_data_match_video_list  主键id
 * @property int|null $origin_id 来源id    
 * @property int|null $match_rel_id 原始match Id
 */
class StandardDataMatchVideoListRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_match_video_list_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['video_id', 'origin_id', 'match_rel_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_id' => 'Video ID',
            'origin_id' => 'Origin ID',
            'match_rel_id' => 'Match Rel ID',
        ];
    }
}
