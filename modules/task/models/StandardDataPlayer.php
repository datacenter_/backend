<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_player".
 *
 * @property int $id
 * @property int $origin_id 数据源
 * @property string $rel_identity_id 源端标示
 * @property string $nick_name 选手名称
 * @property string|null $birthday 生日
 * @property int $game_id 游戏id
 * @property int|null $nationality 选手国家
 * @property string|null $name 真实姓名英文
 * @property string|null $name_cn 真实姓名 英文
 * @property string|null $image 头像
 * @property int|null $position_id 位置id
 * @property string|null $introduction 选手介绍
 * @property int|null $deleted 0-恢复 1-删除
 * @property int|null $cuser 创建人
 * @property int|null $deal_status 绑定状态1待添加2待绑定3待绑定多4已绑定
 * @property string|null $slug slug
 * @property string|null $created_at 创建时间
 * @property string|null $modified_at 修改时间
 * @property string|null $role 角色位置
 */
class StandardDataPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id'], 'required'],
            [['origin_id', 'game_id','nationality', 'position_id', 'deleted', 'cuser', 'deal_status'], 'integer'],
            [['introduction','introduction_cn','steam_id','role','perid','origin_modified_at'], 'string'],
            [['created_at', 'modified_at','deleted_at'], 'safe'],
            [['rel_identity_id','deleted_at'], 'string', 'max' => 100],
            [['birthday','team'], 'string'],
            [['nick_name','name', 'name_cn'], 'string'],
            [['image'], 'string', 'max' => 300],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源',
            'rel_identity_id' => '源端标示',
            'nick_name' => '选手名称',
            'birthday' => '生日',
            'game_id' => '游戏id',
            'steam_id' => '战队Id',
            'nationality' => '选手国家',
            'name' => '真实姓名英文',
            'name_cn' => '真实姓名 英文',
            'image' => '头像',
            'position_id' => '位置id',
            'introduction' => '选手介绍',
            'introduction_cn' => '选手介绍英文',
            'deleted' => '0-恢复 1-删除',
            'cuser' => '创建人',
            'deal_status' => '绑定状态1待添加2待绑定3待绑定多4已绑定',
            'slug' => 'slug',
            'created_at' => '创建时间',
            'modified_at' => '修改时间',
        ];
    }
}
