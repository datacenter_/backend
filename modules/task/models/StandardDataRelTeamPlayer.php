<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_rel_team_player".
 *
 * @property int $id
 * @property int|null $origin_id 数据源
 * @property string|null $rel_identity_id 关联关系原始id
 * @property int|null $game_id
 * @property string|null $team_identity_id
 * @property string|null $player_identity_id
 * @property int|null $team_standard_id
 * @property int|null $player_standard_id
 * @property string|null $created_time
 * @property string|null $modify_at
 */
class StandardDataRelTeamPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_rel_team_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'team_standard_id', 'player_standard_id'], 'integer'],
            [['created_time', 'modify_at'], 'safe'],
            [['rel_identity_id', 'team_identity_id', 'player_identity_id'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源',
            'rel_identity_id' => '关联关系原始id',
            'game_id' => 'Game ID',
            'team_identity_id' => 'Team Identity ID',
            'player_identity_id' => 'Player Identity ID',
            'team_standard_id' => 'Team Standard ID',
            'player_standard_id' => 'Player Standard ID',
            'created_time' => 'Created Time',
            'modify_at' => 'Modify At',
        ];
    }
}
