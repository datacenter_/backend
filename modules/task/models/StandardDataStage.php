<?php

namespace app\modules\task\models;

use Yii;

class StandardDataStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id','tree_id','status','stage_type','is_rescheduled'
            ,'game','deleted','origin_id','rel_identity_id','deal_status'
            ], 'integer'],
            [['name','name_cn','stage_sort','stage_format','stage_format_extra'
            ,'begin_at', 'end_at','scheduled_begin_at','scheduled_end_at','original_scheduled_begin_at',
                'original_scheduled_end_at','slug'
            ], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }
}
