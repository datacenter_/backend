<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_team".
 *
 * @property int $id
 * @property int|null $origin_id 数据源
 * @property string|null $rel_identity_id 源端标示
 * @property int|null $clan_id 俱乐部id
 * @property int|null $game_id 游戏id
 * @property int|null $country 战队所属地
 * @property string|null $name 名称
 * @property string|null $short_name 简称
 * @property string|null $full_name 全称
 * @property string|null $alias 别名
 * @property string|null $logo logo
 * @property int|null $cuser
 * @property string|null $players 队员id
 * @property string|null $history_players 历史选手
 * @property string|null $slug
 * @property int|null $region 所属赛区
 * @property string|null $world_ranking 时间排名
 * @property string|null $ago_30 世界前30周
 * @property string|null $total_earnings 获得奖金
 * @property string|null $average_player_age 平均年龄
 * @property string|null $create_team_date 战队创建日期
 * @property string|null $close_team_date 战队解散日期
 * @property string|null $introduction 英文
 * @property string|null $introduction_cn 中文
 * @property string|null $clan_name 俱乐部名称
 * @property string|null $clan_full_name 俱乐部英文全称
 * @property string|null $clan_short_name 俱乐部简称
 * @property string|null $clan_slug
 * @property string|null $clan_alias 俱乐部别名
 * @property string|null $clan_image
 * @property string|null $clan_cuser
 * @property string|null $seo_url
 * @property int $deleted deleted 1表示删除，2表示正常
 * @property string $created_at
 * @property string $modified_at
 * @property int|null $deal_status 绑定状态1待添加2待绑定3待绑定多4已绑定
 */
class StandardDataTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'clan_id', 'game_id', 'country', 'cuser', 'region', 'deleted', 'deal_status'], 'integer'],
            [['logo', 'introduction', 'introduction_cn','perid'], 'string'],
            [['create_team_date', 'close_team_date', 'created_at', 'modified_at','deleted_at'], 'safe'],
            [['rel_identity_id', 'name', 'short_name','deleted_at'], 'string', 'max' => 100],
            [['full_name', 'players', 'history_players', 'slug', 'world_ranking', 'ago_30', 'total_earnings', 'average_player_age', 'clan_name', 'clan_full_name', 'clan_short_name', 'clan_slug', 'clan_alias', 'clan_image', 'clan_cuser', 'seo_url'], 'string'],
            [['alias','origin_modified_at'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'rel_identity_id' => 'Rel Identity ID',
            'clan_id' => 'Clan ID',
            'game_id' => 'Game ID',
            'country' => 'Country',
            'name' => 'Name',
            'short_name' => 'Short Name',
            'full_name' => 'Full Name',
            'alias' => 'Alias',
            'logo' => 'Logo',
            'cuser' => 'Cuser',
            'players' => 'Players',
            'history_players' => 'History Players',
            'slug' => 'Slug',
            'region' => 'Region',
            'world_ranking' => 'World Ranking',
            'ago_30' => 'Ago 30',
            'total_earnings' => 'Total Earnings',
            'average_player_age' => 'Average Player Age',
            'create_team_date' => 'Create Team Date',
            'close_team_date' => 'Close Team Date',
            'introduction' => 'Introduction',
            'introduction_cn' => 'Introduction Cn',
            'clan_name' => 'Clan Name',
            'clan_full_name' => 'Clan Full Name',
            'clan_short_name' => 'Clan Short Name',
            'clan_slug' => 'Clan Slug',
            'clan_alias' => 'Clan Alias',
            'clan_image' => 'Clan Image',
            'clan_cuser' => 'Clan Cuser',
            'seo_url' => 'Seo Url',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deal_status' => 'Deal Status',
        ];
    }
}
