<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "standard_data_tournament".
 *
 * @property int $id
 * @property int|null $origin_id 数据源
 * @property string|null $rel_identity_id 关联唯一id
 * @property string|null $rel_league_id 源端标示
 * @property string|null $match_name 赛事名称英文
 * @property string|null $match_name_cn 赛事名称中文
 * @property int|null $game_id 游戏id
 * @property int|null $tournament_status
 * @property string|null $begin_match_date 开始时间
 * @property string|null $end_match_date 结束时间
 * @property string|null $match_short_name 赛事简称
 * @property string|null $match_short_name_cn 赛事名称中文
 * @property string|null $son_match_sort 子赛事排序
 * @property string|null $address 举办地
 * @property int|null $country 举办国家
 * @property string|null $organizer 组织者
 * @property string|null $introduction 赛事简介
 * @property string|null $format 赛制介绍
 * @property string|null $logo
 * @property int|null $type 1-赛事-2子赛事
 * @property int|null $deal_status
 * @property string|null $created_at
 * @property string|null $modified_at
 */
class StandardDataTournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id', 'status', 'country', 'type', 'deal_status','type','parent_id',
                'region','deleted'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['introduction', 'introduction_cn','format','format_cn','tier','address','address_cn','version','prize_points',
                'prize_seed','map_pool','teams_condition','teams','number_of_teams','deleted_at','perid','origin_modified_at'], 'string'],
            [['rel_identity_id'], 'string', 'max' => 200],
            [['rel_league_id',  'organizer','organizer_cn','prize_bonus','prize_distribution'], 'string'],
            [['name', 'name_cn', 'short_name','slug', 'short_name_cn', 'son_sort','scheduled_begin_at','scheduled_end_at'], 'string'],
            [['image'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源',
            'rel_identity_id' => '关联唯一id',
            'rel_league_id' => '源端标示',
            'name' => '赛事名称英文',
            'name_cn' => '赛事名称中文',
            'game_id' => '游戏id',
            'status' => 'Tournament Status',
            'scheduled_begin_at' => '开始时间',
            'scheduled_end_at' => '结束时间',
            'short_name' => '赛事简称',
            'short_name_cn' => '赛事名称中文',
            'son_sort' => '子赛事排序',
            'address' => '举办地',
            'country' => '举办国家',
            'organizer' => '组织者',
            'introduction' => '赛事简介',
            'format' => '赛制介绍',
            'image' => 'Logo',
            'type' => '1-赛事-2子赛事',
            'deal_status' => 'Deal Status',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
