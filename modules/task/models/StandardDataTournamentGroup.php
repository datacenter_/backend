<?php

namespace app\modules\task\models;

use Yii;

class StandardDataTournamentGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_data_tournament_group';
    }

    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tree_id', 'tournament_id','deleted','game','origin_id','tournament_id'], 'integer'],
            [['name', 'name_cn','begin_at','end_at','number_of_teams',
                'advance','eliminate','win_points','draw_ponits','lose_points'],
                'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }
}
