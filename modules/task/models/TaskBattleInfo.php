<?php

namespace app\modules\task\models;

use Yii;

class TaskBattleInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_battle_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['battle_id', 'match_id','economic_diff'], 'string'],
            [['duration', 'index','status'], 'integer'],
        ];
    }

}
