<?php

namespace app\modules\task\models;

use Yii;


class TaskDataFeijingMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_feijing_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id','status','team_a_score','team_b_score','bo'], 'integer'],
            [['match_id', 'league_id','start_time','address', 'round_name','round_son_name',
            'team_a_id','team_b_id','game_id'
            ], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }
}
