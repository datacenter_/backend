<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_data_feijing_team".
 *
 * @property int $id
 * @property string|null $team_id
 * @property string|null $name
 * @property string|null $logo
 * @property string|null $name_en
 * @property string|null $short_name
 * @property string|null $introduction
 */
class TaskDataFeijingPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_feijing_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['introduction'], 'string'],
            [['position', 'country','nick_name', 'real_name','player_id','team_id'], 'string', 'max' => 100],
            [['avatar'], 'string', 'max' => 200],
            [['game_id'], 'integer'],
            [['player_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }
}
