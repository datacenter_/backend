<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_data_feijing_team".
 *
 * @property int $id
 * @property string|null $team_id
 * @property string|null $name
 * @property string|null $logo
 * @property string|null $name_en
 * @property string|null $short_name
 * @property string|null $introduction
 */
class TaskDataFeijingTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_feijing_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['introduction'], 'string'],
            [['team_id', 'name_en', 'short_name'], 'string', 'max' => 100],
            [['name', 'logo'], 'string', 'max' => 200],
            [['game_id'],'integer'],
            [['team_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'name' => 'Name',
            'logo' => 'Logo',
            'name_en' => 'Name En',
            'short_name' => 'Short Name',
            'introduction' => 'Introduction',
        ];
    }
}
