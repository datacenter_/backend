<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_data_feijing_team".
 *
 * @property int $id
 * @property string|null $team_id
 * @property string|null $name
 * @property string|null $logo
 * @property string|null $name_en
 * @property string|null $short_name
 * @property string|null $introduction
 */
class TaskDataFeijingTournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_feijing_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['league_id','address'], 'string'],
            [['game_id'], 'integer'],
            [['name', 'status','name_en','short_name', 'start_time','end_time','organizer'], 'string', 'max' => 100],
            [['logo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

    }
}
