<?php

namespace app\modules\task\models;

use Yii;

class TaskDataHero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_hero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hero_id', 'name','avatar'], 'string'],
            [['game_id'],'integer'],
        ];
    }

}
