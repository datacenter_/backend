<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_data_hot_info".
 *
 * @property int $id
 * @property int|null $origin_id 数据源
 * @property string|null $resource_type 资源类型
 * @property int|null $game_id 游戏id
 * @property string|null $identity_id 唯一标示
 * @property string|null $json_info 对应的数据记录，每种资源结构不一样
 * @property string|null $get_url 相关url
 * @property string|null $created_time 创建时间
 * @property string|null $update_time 修改时间
 */
class TaskDataHotInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_hot_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'game_id'], 'integer'],
            [['json_info'], 'string'],
            [['created_time', 'update_time'], 'safe'],
            [['resource_type', 'identity_id', 'get_url'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => '数据源',
            'resource_type' => '资源类型',
            'game_id' => '游戏id',
            'identity_id' => '唯一标示',
            'json_info' => '对应的数据记录，每种资源结构不一样',
            'get_url' => '相关url',
            'created_time' => '创建时间',
            'update_time' => '修改时间',
        ];
    }
}
