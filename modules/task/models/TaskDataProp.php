<?php

namespace app\modules\task\models;

use Yii;

class TaskDataProp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_prop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'name','description','text','price','image'], 'string'],
            [['game_id'],'integer']
        ];
    }

}
