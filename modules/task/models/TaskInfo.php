<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_info".
 *
 * @property int $id
 * @property int|null $run_type 1mq2不执行3即时执行
 * @property string|null $tag
 * @property string|null $type 任务类型，task类抓取时候指定
 * @property string|null $batch_id 批次
 * @property string|null $params 参数
 * @property string|null $response 抓取结果
 * @property string|null $error_info 执行状况
 * @property int|null $status 状态1，待执行，2执行中，3，已执行，4，失败
 * @property string|null $substeps json
 * @property string|null $begin_time 开始时间
 * @property string|null $end_time 结束时间
 * @property string|null $created_time 创建时间
 */
class TaskInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['run_type', 'status'], 'integer'],
            [['redis_server','params', 'response', 'error_info', 'substeps'], 'string'],
            [['begin_time', 'end_time', 'created_time'], 'safe'],
            [['tag', 'type'], 'string', 'max' => 200],
            [['batch_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'run_type' => '1mq2不执行3即时执行',
            'tag' => 'Tag',
            'type' => '任务类型，task类抓取时候指定',
            'batch_id' => '批次',
            'params' => '参数',
            'response' => '抓取结果',
            'error_info' => '执行状况',
            'status' => '状态1，待执行，2执行中，3，已执行，4，失败',
            'substeps' => 'json',
            'begin_time' => '开始时间',
            'end_time' => '结束时间',
            'created_time' => '创建时间',
        ];
    }
}
