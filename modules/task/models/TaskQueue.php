<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_queue".
 *
 * @property int $id
 * @property string|null $topic
 * @property string|null $tag
 * @property string|null $body
 * @property string|null $created_time
 * @property string|null $modify_at
 * @property string|null $error_info
 * @property string|null $sub
 * @property int|null $status 1待执行，2执行中，3成功，4失败
 * @property string|null $begin_time
 * @property string|null $end_time
 */
class TaskQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body', 'error_info', 'sub'], 'string'],
            [['created_time', 'modify_at', 'begin_time', 'end_time'], 'safe'],
            [['status'], 'integer'],
            [['topic'], 'string', 'max' => 200],
            [['tag'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic' => 'Topic',
            'tag' => 'Tag',
            'body' => 'Body',
            'created_time' => 'Created Time',
            'modify_at' => 'Modify At',
            'error_info' => 'Error Info',
            'sub' => 'Sub',
            'status' => '1待执行，2执行中，3成功，4失败',
            'begin_time' => 'Begin Time',
            'end_time' => 'End Time',
        ];
    }
}
