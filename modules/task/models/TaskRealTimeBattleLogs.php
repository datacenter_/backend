<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_real_time_battle_logs".
 *
 * @property int $id
 * @property int|null $game_id 游戏
 * @property int|null $origin_id 源
 * @property string|null $identity_id 标示id
 * @property string|null $type 类型
 * @property string|null $version 版本标示
 * @property string|null $info 详情
 * @property string|null $created_time 创建时间
 * @property string|null $deal_start 开始处理时间
 * @property string|null $deal_end 处理结束时间
 */
class TaskRealTimeBattleLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_real_time_battle_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'origin_id'], 'integer'],
            [['info'], 'string'],
            [['created_time', 'deal_start', 'deal_end'], 'safe'],
            [['identity_id', 'type'], 'string', 'max' => 11],
            [['version'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => '游戏',
            'origin_id' => '源',
            'identity_id' => '标示id',
            'type' => '类型',
            'version' => '版本标示',
            'info' => '详情',
            'created_time' => '创建时间',
            'deal_start' => '开始处理时间',
            'deal_end' => '处理结束时间',
        ];
    }
}
