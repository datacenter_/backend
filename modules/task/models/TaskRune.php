<?php

namespace app\modules\task\models;

use Yii;

class TaskRune extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_rune';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rune_id', 'name','description','detail','image'], 'string'],
            [['game_id'],'integer']
        ];
    }

}
