<?php

namespace app\modules\task\models;

use Yii;

class TaskSkill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['skill_id','name','description','name_en','image'], 'string'],
            [['game_id'],'integer'],
        ];
    }

}
