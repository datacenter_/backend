<?php

namespace app\modules\task\models;

use Yii;

/**
 * This is the model class for table "task_type".
 *
 * @property int $id
 * @property string $name
 * @property string|null $class_name
 * @property int|null $origin_id 数据源
 * @property string|null $crontab 执行时间
 * @property int|null $flag 是否开启1开启2关闭
 * @property int|null $status 执行状态（待）
 * @property string|null $created_time 创建时间
 * @property string|null $modify_at 修改时间
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'flag', 'status'], 'integer'],
            [['created_time', 'modify_at'], 'safe'],
            [['name', 'class_name', 'crontab'], 'string', 'max' => 200],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'class_name' => 'Class Name',
            'origin_id' => '数据源',
            'crontab' => '执行时间',
            'flag' => '是否开启1开启2关闭',
            'status' => '执行状态（待）',
            'created_time' => '创建时间',
            'modify_at' => '修改时间',
        ];
    }
}
