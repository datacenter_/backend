<?php

namespace app\modules\task\models;

use Yii;

class TeamPlayerRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_team_player_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'player_id'], 'string'],
            [['game_id'],'integer'],
        ];
    }

}
