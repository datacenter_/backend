<?php

namespace app\modules\task\models;

use Yii;

class TournamentTeamRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_feijing_tournament_team_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'tournament_id'], 'string'],
            [['game_id'],'integer'],
        ];
    }

}
