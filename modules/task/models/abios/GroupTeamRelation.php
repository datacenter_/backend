<?php

namespace app\modules\task\models\abios;

use Yii;

class GroupTeamRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_tournament_team_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id','group_id'],'integer'],
        ];
    }

}
