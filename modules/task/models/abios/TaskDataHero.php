<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataHero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_hero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['large_image', 'name','small_image'], 'string'],
            [['game_id','id','external_id'],'integer'],
        ];
    }

}
