<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'start','end','postponed_from','deleted_at','pbp_status'], 'string'],
            [['best_of','id','game','streamed','substage_id','team_1_id','team_2_id','tournament_id'],'integer'],
        ];
    }

}
