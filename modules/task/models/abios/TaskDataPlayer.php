<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name','nick_name','deleted_at',
                'images','country','roles','race'
                ], 'string'],
            [['game','id'],'integer'],
        ];
    }

}
