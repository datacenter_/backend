<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataProp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_prop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['images', 'name'], 'string'],
            [['game_id','id','external_id'],'integer'],
        ];
    }

}
