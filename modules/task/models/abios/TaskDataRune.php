<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataRune extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_rune';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','images'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
