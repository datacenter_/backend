<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'deleted_at'], 'string'],
            [['id','tournament_id'],'integer'],
        ];
    }

}
