<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataSubstages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_substages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'deleted_at','start','end','phase'], 'string'],
            [['stage_id','type','tournament_id','order','id'
            ],'integer'],
        ];
    }

}
