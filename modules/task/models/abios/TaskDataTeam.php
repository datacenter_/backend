<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'short_name','deleted_at','images','country'], 'string'],
            [['game','id'],'integer'],
        ];
    }

}
