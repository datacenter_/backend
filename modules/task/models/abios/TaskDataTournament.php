<?php

namespace app\modules\task\models\abios;

use Yii;

class TaskDataTournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'abbrev','country','city','description','short_description','format','start'
            ,'end','deleted_at','images','prizepool_string'
            ], 'string'],
            [['tier','id','game'],'integer'],
        ];
    }

}
