<?php

namespace app\modules\task\models\abios;

use Yii;

class TeamPlayerRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_team_player_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id','team_id','player_id'],'integer'],
        ];
    }

}
