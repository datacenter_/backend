<?php

namespace app\modules\task\models\abios;

use Yii;

class TournamentTeamRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_abios_tournament_team_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id','tournament_id'],'integer'],
        ];
    }

}
