<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataBattle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_battle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'winner','end_at','begin_at'], 'string'],
            [['detailed_stats','id','finished','forfeit','length','match_id'
            ,'position'
            ],'integer'],
        ];
    }

}
