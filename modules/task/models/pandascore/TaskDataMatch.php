<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_at', 'name','end_at','live','live_embed_url','live_url',
                'match_type','official_stream_url','original_scheduled_at','scheduled_at',
                'slug','status'
                ], 'string'],
            [['id','game','detailed_stats','draw','forfeit','game_advantage','number_of_games'
                ,'rescheduled','serie_id','tournament_id','winner_id','team_1_id','team_2_id'
            ],'integer'],
        ];
    }

}
