<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_url', 'name','birthday','current_videogame','first_name','last_name','nationality',
                'role','slug'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
