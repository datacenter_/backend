<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataProp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_prop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_url', 'name'], 'string'],
            [['game_id','id','gold_total','gold_purchasable','is_trinket'],'integer'],
        ];
    }

}
