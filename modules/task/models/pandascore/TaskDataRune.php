<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataRune extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_rune';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
