<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataSeries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_at', 'description','end_at','full_name','slug'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
