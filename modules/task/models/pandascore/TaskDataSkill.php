<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataSkill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','image_url'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
