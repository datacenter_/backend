<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['acronym','current_videogame','image_url','location','slug','name'], 'string'],
            [['game_id','id'],'integer'],
        ];
    }

}
