<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TaskDataTournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_at','end_at','name','slug'], 'string'],
            [['game_id','id','serie_id'],'integer'],
        ];
    }

}
