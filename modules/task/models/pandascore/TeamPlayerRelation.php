<?php

namespace app\modules\task\models\pandascore;

use Yii;

class TeamPlayerRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_data_pandascore_team_player_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id','team_id', 'player_id'],'integer'],
        ];
    }

}
