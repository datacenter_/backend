<?php

namespace app\modules\task\models\radarPurple;

use Yii;

/**
 * This is the model class for table "ez_battle".
 *
 * @property int $id
 * @property int|null $box_num 当前局数
 * @property string|null $match_id 比赛id
 * @property string|null $away_team 客队id
 * @property string|null $home_team 主队id
 * @property int|null $length_time bat时长
 * @property string|null $economy_lines 经济曲线
 * @property string|null $experience_lines 经验曲线
 * @property int|null $status_id 当前对局（当前时刻）比赛状态  1未开始，2进行中，3已结束
 * @property int|null $single_status_id 对局状态   1未开始，2进行中，3已结束
 * @property string|null $roshan_timer 肉山刷新倒计时   0：存活   其他：根据时长判断阶段
 * @property int|null $is_bp 是否是BP阶段  1是，2否
 * @property string|null $timer 计时器
 * @property string|null $visit_time 消息发送时间
 * @property string|null $bp_data bp信息
 * @property string|null $update_at
 * @property string|null $created_at
 */
class RadarPurpleBattle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ez_battle';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['box_num', 'length_time', 'status_id', 'single_status_id', 'is_bp'], 'integer'],
            [['economy_lines', 'experience_lines'], 'string'],
            [['visit_time', 'update_at', 'created_at'], 'safe'],
            [['match_id', 'away_team', 'home_team', 'roshan_timer'], 'string', 'max' => 32],
            [['timer'], 'string', 'max' => 200],
            [['bp_data'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'box_num' => 'Box Num',
            'match_id' => 'Match ID',
            'away_team' => 'Away Team',
            'home_team' => 'Home Team',
            'length_time' => 'Length Time',
            'economy_lines' => 'Economy Lines',
            'experience_lines' => 'Experience Lines',
            'status_id' => 'Status ID',
            'single_status_id' => 'Single Status ID',
            'roshan_timer' => 'Roshan Timer',
            'is_bp' => 'Is Bp',
            'timer' => 'Timer',
            'visit_time' => 'Visit Time',
            'bp_data' => 'Bp Data',
            'update_at' => 'Update At',
            'created_at' => 'Created At',
        ];
    }
}
