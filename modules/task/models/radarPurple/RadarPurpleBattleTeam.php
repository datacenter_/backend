<?php

namespace app\modules\task\models\radarPurple;

use Yii;

/**
 * This is the model class for table "ez_match_team".
 *
 * @property int $id
 * @property int|null $box_num 当前局数
 * @property string|null $match_id 比赛id
 * @property string|null $team_id team_id
 * @property int|null $barracks barracks(军营)
 * @property string|null $destroyed_barracks 摧毁军营
 * @property string|null $destroyed_tower 摧毁塔
 * @property int|null $score 比分
 * @property int|null $side 位置
 * @property int|null $tower tower
 * @property string|null $stats stats
 * @property int|null $experience 经验
 * @property int|null $economy 经济
 * @property int|null $first_tower 首塔
 * @property int|null $first_blood 一血
 * @property int|null $first_to_5_kills 五杀
 * @property int|null $first_to_10_kills 十杀
 * @property int|null $kills 击杀
 * @property int|null $duration 对局获胜
 * @property int|null $deaths 死亡
 * @property int|null $assists 助攻
 * @property int|null $Leading_economy 领先经济
 * @property string|null $ban_id ban_id
 * @property string|null $pick_id pick_id
 * @property string|null $update_at
 * @property string|null $created_at
 */
class RadarPurpleBattleTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ez_match_team';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['box_num', 'barracks', 'score', 'side', 'tower', 'experience', 'economy', 'first_tower', 'first_blood', 'first_to_5_kills', 'first_to_10_kills', 'kills', 'duration', 'deaths', 'assists', 'leading_economy'], 'integer'],
            [['update_at', 'created_at'], 'safe'],
            [['match_id', 'team_id', 'destroyed_barracks', 'destroyed_tower'], 'string', 'max' => 32],
            [['stats'], 'string', 'max' => 100],
            [['ban_id', 'pick_id'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'box_num' => 'Box Num',
            'match_id' => 'Match ID',
            'team_id' => 'Team ID',
            'barracks' => 'Barracks',
            'destroyed_barracks' => 'Destroyed Barracks',
            'destroyed_tower' => 'Destroyed Tower',
            'score' => 'Score',
            'side' => 'Side',
            'tower' => 'Tower',
            'stats' => 'Stats',
            'experience' => 'Experience',
            'economy' => 'Economy',
            'first_tower' => 'First Tower',
            'first_blood' => 'First Blood',
            'first_to_5_kills' => 'First To 5 Kills',
            'first_to_10_kills' => 'First To 10 Kills',
            'kills' => 'Kills',
            'duration' => 'Duration',
            'deaths' => 'Deaths',
            'assists' => 'Assists',
            'leading_economy' => 'Leading Economy',
            'ban_id' => 'Ban ID',
            'pick_id' => 'Pick ID',
            'update_at' => 'Update At',
            'created_at' => 'Created At',
        ];
    }
}
