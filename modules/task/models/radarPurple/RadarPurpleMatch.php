<?php

namespace app\modules\task\models\radarPurple;

use Yii;

class RadarPurpleMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ez_match';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'home_score', 'away_score','status_id','box'], 'integer'],
            [['match_time','update_at', 'source_updated_at', 'created_at'], 'safe'],
            [['match_id', 'source_id','tournament_id', 'home_name', 'away_name', 'home_team', 'away_team', 'score_two'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'source_id' => 'source id',
            'source_updated_at' => 'source_updated_at',
            'tournament_id' => 'Tournament ID',
            'game_id' => 'game_id',
            'home_score' => 'home_score',
            'away_score' => 'away_score',
            'home_name' => 'home_name',
            'away_name' => 'away_name',
            'status_id' => 'status_id',
            'box' => 'box',
            'match_time' => 'match_time',
            'home_team' => 'home_team',
            'away_team' => 'away_team',
            'update_at' => 'Update At',
            'created_at' => 'Created At'
        ];
    }
}
