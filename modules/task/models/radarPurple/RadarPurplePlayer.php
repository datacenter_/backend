<?php

namespace app\modules\task\models\radarPurple;

use Yii;

/**
 * This is the model class for table "ez_match_player".
 *
 * @property int $id
 * @property string|null $match_id 比赛id
 * @property string|null $player_id 选手id
 * @property int|null $box_num 对局
 * @property int|null $position 位置
 * @property int|null $player_slot player_slot
 * @property int|null $hero_id 角色id
 * @property int|null $level 等级
 * @property int|null $gold 当前金币
 * @property int|null $gold_spend 花费金币
 * @property string|null $hero_cd 角色cd/ 大招cd
 * @property string|null $buyback_cost 买活所需金币
 * @property int|null $ultimate_status ultimate_status
 * @property int|null $kills 击杀
 * @property int|null $deaths 死亡
 * @property int|null $assists 助攻
 * @property int|null $last_hits 正补
 * @property int|null $denies 反补
 * @property int|null $xpm 分均经验
 * @property int|null $gpm 分均金钱
 * @property int|null $net_worth 财产总和
 * @property string|null $equipments_id_all 装备栏
 * @property string|null $rune 符文
 * @property string|null $update_at
 * @property string|null $created_at
 */
class RadarPurplePlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ez_match_player';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['box_num', 'position', 'player_slot', 'hero_id', 'level', 'gold', 'gold_spend', 'ultimate_status', 'kills', 'deaths', 'assists', 'last_hits', 'denies', 'xpm', 'gpm', 'net_worth'], 'integer'],
            [['update_at', 'created_at'], 'safe'],
            [['match_id', 'player_id', 'hero_cd', 'buyback_cost'], 'string', 'max' => 32],
            [['equipments_id_all'], 'string', 'max' => 100],
            [['rune'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'player_id' => 'Player ID',
            'box_num' => 'Box Num',
            'position' => 'Position',
            'player_slot' => 'Player Slot',
            'hero_id' => 'Hero ID',
            'level' => 'Level',
            'gold' => 'Gold',
            'gold_spend' => 'Gold Spend',
            'hero_cd' => 'Hero Cd',
            'buyback_cost' => 'Buyback Cost',
            'ultimate_status' => 'Ultimate Status',
            'kills' => 'Kills',
            'deaths' => 'Deaths',
            'assists' => 'Assists',
            'last_hits' => 'Last Hits',
            'denies' => 'Denies',
            'xpm' => 'Xpm',
            'gpm' => 'Gpm',
            'net_worth' => 'Net Worth',
            'equipments_id_all' => 'Equipments Id All',
            'rune' => 'Rune',
            'update_at' => 'Update At',
            'created_at' => 'Created At',
        ];
    }
}
