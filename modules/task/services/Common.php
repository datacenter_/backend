<?php
/**
 *
 */

namespace app\modules\task\services;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\models\EnumIngameGoal;
use app\modules\common\models\EnumLane;
use app\modules\common\models\EnumPosition;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\match\models\Match;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\models\MatchStreamList;
use app\modules\match\services\StreamService;
use app\modules\metadata\models\EnumCsgoWeapon;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataMatchVideoList;
use app\modules\task\models\StandardDataMatchVideoListRelation;
use app\modules\task\models\TaskDataHotInfo;
use app\modules\task\services\grab\bayes\BayesBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;
use yii\db\Exception;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\common\services\OperationLogService;
class Common
{
    public static function requestGetCross($getUrl, $header, $params)
    {
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $urlWithQuery = $getUrl . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }
        $url = env("AGENCY_URL");
        $ch = curl_init();
        if($url){ //有代理时
            $info = [
                'url' => $urlWithQuery,
                'method' => 'get',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        }else{
            curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestGetCrossDev($getUrl, $header, $params)
    {
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $urlWithQuery = $getUrl . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }
        $url = env("ORANGE_AGENCY_URL");
        $ch = curl_init();
        if($url){
            $info = [
                'url' => $urlWithQuery,
                'method' => 'get',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        }else{
            curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestGet($url, $header, $params, $is_build = true)
    {
        if ($is_build) {
            $query = http_build_query($params);
            $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        } else {
            $query = $params;
        }
        $urlWithQuery = $url . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }
//        print_r($urlWithQuery);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestPost($url, $header, $params)
    {
        $query = http_build_query($params);
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestPostJsonString($url, $header, $jsonStr, $method)
    {
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * @param $oldInfo
     * @param $newInfo
     * @param $filterKeys
     * @return array
     * 比较两个一维数组差异，获取增量，如果第二个比第一个key多，比较不出来
     */
    public static function diffParams($oldInfo, $newInfo, $filterKeys = [], $arrayOpen = true)
    {
        // 获取array数据变化
        if ($arrayOpen) {
            $oldInfo = self::openArray($oldInfo);
            $newInfo = self::openArray($newInfo);
        }
        if ($filterKeys && count($filterKeys)) {
            $arrayFilter = array_fill_keys($filterKeys, "");
            $interset = array_intersect_key($oldInfo, $arrayFilter);
            $oldInfo = array_merge($arrayFilter, $interset);
        }
        $di = array_diff_assoc($oldInfo,$newInfo);
        $diff = [];
        foreach ($di as $key => $val) {
            $diff[$key] = [
                "key" => $key,
                "before" => isset($oldInfo[$key]) ? $oldInfo[$key] : "",
                "after" => isset($newInfo[$key]) ? $newInfo[$key] : "",
            ];
        }
        return $diff;

    }

    /**
     * @param $array
     * @param string $split
     * @return array
     * 展开数组
     */
    public static function openArray($array, $split = "|")
    {
        return self::_openArray($array, $split);
    }

    private static function _openArray($array, $split, $keyPrefix = "", $deep = 0)
    {
        $list = [];
        foreach ($array as $key => $val) {
            $keyP = $keyPrefix ? $keyPrefix . "|" : "";
            $keyTemp = $keyP . $key;
            if (is_array($val) && count($val)) {
                $list = array_merge($list, self::_openArray($val, $split, $keyTemp, $deep + 1));
            } else if (is_array($val) && !count($val)) {
                $list[$keyTemp] = json_encode($val);;
            } else {
                $list[$keyTemp] = $val;
            }
        }
        return $list;
    }


    public static function getDiffInfo($oldInfo, $newInfo, $filterKeys = [], $arrayOpen = true)
    {
        $diff = [];
        $changeType = QueueServer::QUEUE_TYPE_ADD;
        if ($oldInfo) {
            $diff = self::diffParams($oldInfo, $newInfo, $filterKeys, $arrayOpen);
            $changeType = QueueServer::QUEUE_TYPE_CHANGE;
            if (!count($diff)) {
                return ["changed" => false, 'change_type' => QueueServer::CHANGE_TYPE_EQUAL];
            }
        }
        return ["changed" => true,
            'change_type' => $changeType,
            'diff' => $diff,
            'old' => $oldInfo,
            'new' => $newInfo];
    }

    public static function array_columns($input, $column_keys = null, $index_key = null)
    {
        $result = array();
        $keys = isset($column_keys) ? $column_keys : array();
        if ($input) {
            foreach ($input as $k => $v) {
                // 指定返回列
                if ($keys) {
                    $tmp = array();
                    foreach ($keys as $key) {
                        $tmp[$key] = $v[$key];
                    }
                } else {
                    $tmp = $v;
                }
                // 指定索引列
                if (isset($index_key)) {
                    $result[$v[$index_key]] = $tmp;
                } else {
                    $result[] = $tmp;
                }

            }
        }
        return $result;
    }

    public static function getAbiosMapIdById($id)
    {
        $abiosGameMap = [
            1 => ['title' => 'Dota 2', 'standard_id' => 3],
            2 => ['title' => 'LoL', 'standard_id' => 2],
            3 => ['title' => 'SCII', 'standard_id' => 6],
            4 => ['title' => 'HoN', 'standard_id' => 0],
            5 => ['title' => 'CS:GO', 'standard_id' => 1],
            6 => ['title' => 'Hearthstone', 'standard_id' => 7],
            7 => ['title' => 'WoW', 'standard_id' => 0],
            8 => ['title' => 'SMITE', 'standard_id' => 0],
            9 => ['title' => 'HotS', 'standard_id' => 0],
            10 => ['title' => 'Smash', 'standard_id' => 0],
            11 => ['title' => 'CoD', 'standard_id' => 0], //使命召唤
            12 => ['title' => 'OW', 'standard_id' => 4], //守望先锋
            13 => ['title' => 'SC', 'standard_id' => 0],
            14 => ['title' => 'KoG', 'standard_id' => 8], //王者荣耀
            15 => ['title' => 'PUBG', 'standard_id' => 0],
        ];
        if ($abiosGameMap[$id]) {
            return $abiosGameMap[$id]['standard_id'];
        }
    }

    public static function addStandardDataMatchVideo($liveUrl, $liveEmbedUrl = "", $officialStreamUrl = false, $originId, $gameId, $matchRelId, $originType = null,$data = [])
    {
        if (!$liveUrl && !$liveEmbedUrl) {
            return null;
        }
        $newOriginId = $originId;
        //是否是官方url
        $is_official = 2;
        if ($officialStreamUrl) {
            $is_official = 1;
            $checkOfficial = StandardDataMatchVideoList::find()->where(['live_url' => $liveUrl])->one();
            if ($checkOfficial) {
                $matchStreamList = MatchStreamList::find()->where(['video_id' => $checkOfficial['id']])->one();
                if ($matchStreamList) {
                    $matchStreamList->setAttribute('is_official', 1);
                    $matchStreamList->save();
                }
            }
        }

        $murl = md5($liveUrl);
        $new = StandardDataMatchVideoList::find()->where(['live_url_identity' => $murl])->one();
        if (!$new) {
            $new = new StandardDataMatchVideoList();
            if ($originType == Consts::ORIGIN_HLTV) {
                $video_origin_id = 8;
                $streams_origin_id = "hltv-" . $data['id'];
            }
            $newData['match_rel_id'] =  $matchRelId;
        }else{
            unset($newOriginId);
        }

        if ($originType == Consts::ORIGIN_PANDASCORE) {
            if ($officialStreamUrl == true) {
                $officialStreamUrl = $liveUrl;
            } else {
                $officialStreamUrl = null;
            }
        } elseif ($originType == Consts::ORIGIN_ABIOS) {
            if ($officialStreamUrl) {
                $officialStreamUrl = $liveUrl;
            } else {
                $officialStreamUrl = null;
            }
        }else{
            $officialStreamUrl = null;
        }

        $platformData = StreamService::getLiveUrlInfo($liveUrl,$data['platform_name'],$liveEmbedUrl);
        if (!$platformData){
            return false;
        }
        if(empty($platformData['platform_name']) && $liveUrl) {
            $mssage = "match_rel_id:{$matchRelId},没有这个平台，liveUrl:{$liveUrl}";
            $url = env('PUSH_MESSAGE_DING');
            $typeOnle = env('CONFIG_TYPE');
            $params = [
                'type' =>'emptyPlatformName',
                'tag' =>$typeOnle.' platformName缺少',
                'info' =>$mssage,
            ];
            Common::requestPost($url,[],$params);
            return false;
        }
//
//        if (empty($data['platform_name'])) {
//            $platform_name = StreamService::getInfoFromUrl($liveUrl)['platform_name'];
//        } else {
//            $platform_name = $data['platform_name'];
//        }
//        $platform_id = (int)StreamService::getPlatformIdbyName($platform_name)['id'] ? (int)StreamService::getPlatformIdbyName($platform_name)['id'] : null;;

//        if (empty($liveEmbedUrl) && $liveUrl) {
//            $urlInfo = StreamService::getInfoFromUrl($liveUrl);
//            $liveEmbedUrl = StreamService::completionUrl($urlInfo['platform_name'], $urlInfo['room_id']);
//        }

        if ($platformData['platform_name'] == "Twitch") {
            $embedUrlPreg = preg_split('/&/', $platformData['liveEmbedUrl'], -1, PREG_SPLIT_NO_EMPTY);
            $NewliveEmbedUrl = isset($embedUrlPreg[0]) ? $embedUrlPreg[0] : '';
        } else {
            $NewliveEmbedUrl = $platformData['liveEmbedUrl'];
        }

        if ($originType == Consts::ORIGIN_HLTV) {

            if (empty($liveUrl) && $NewliveEmbedUrl) {
                $liveUrl = StreamService::transformationUrl($NewliveEmbedUrl);
            }

            if (empty($NewliveEmbedUrl) && empty($liveUrl)) {
                return false;
            }

        }

        $newData['live_url'] =  $liveUrl;
        $newData['streamer'] =  $data['streamer'] ?? null;
        $newData['title'] =  $data['title'] ?? null;
        $newData['viewers'] =  $data['viewers'] ?? null;
        $newData['live_url_identity'] =  $murl;
        $newData['live_embed_url'] =  $NewliveEmbedUrl;
        $newData['official_stream_url'] =  $officialStreamUrl;
        if (isset($newOriginId)){
            $newData['origin_id'] = $newOriginId;

        }
        $newData['game_id'] =  $gameId;
        $newData['platform_name'] =  $platformData['platform_name'];
        $newData['platform_id'] =  $platformData['platformId'];
        if (isset($video_origin_id)){
            $newData['video_origin_id'] =  $video_origin_id ?: null;
        }

        if (isset($streams_origin_id)) {
            $newData['streams_origin_id'] =  $streams_origin_id ?: null;
        }
        $newData['country'] = $data['country'] ? (int)StreamService::getcountryByName($data['country'])['id'] : null;

        if ($newData['platform_name']=='Douyu' || $newData['platform_id'] == '4') {
            return false;
        }
        if (strpos($newData['live_url'],'www.douyu.com') !== false || strpos($newData['live_url'],'www.douyu.com') === 0) {
            return false;
        }
        if ($newData['live_url'] == 'https://www.youtube.com/1' || empty($newData['platform_name'])) {
            return false;
        }

        $new->setAttributes(
            $newData
        );
        if (!$new->save()) {
            $errorMsg =  json_encode($new->getErrors(),320);
            $dataJson = json_encode($newData,320);
            $firstErrors = $c=$new->getFirstErrors();
            $firstErrorsJson = json_encode($firstErrors,320);
            throw new BusinessException($new->getErrors().$firstErrorsJson.$dataJson, "保存视频流失败-{$errorMsg}");
        }

        $relationInfo = ['video_id' => $new['id'], 'origin_id' => $originId, 'match_rel_id' => $matchRelId];

        $relationVideoList = StandardDataMatchVideoListRelation::find()->where($relationInfo)->one();
        if (!$relationVideoList) {
            $relationVideoList = new StandardDataMatchVideoListRelation();

        }

        $relationVideoList->setAttributes($relationInfo);
        if (!$relationVideoList->save()) {
            throw new BusinessException($new->getErrors(), "保存视频流到关联表失败");
        }


        //有源端id得到数据
        $standardDataMatch = StandardDataMatch::find()->select(['id'])
            ->where(['rel_identity_id' => $matchRelId, 'origin_id' => $originId])->asArray()->one();
        if (!$standardDataMatch) {
            return false;
        }
        $dataStandard = DataStandardMasterRelation::find()->select(['master_id'])
            ->where(['standard_id' => $standardDataMatch['id'], 'resource_type' => 'match'])->asArray()->one();
        if (empty($dataStandard)) {
            return false;
        }
        $match = Match::find()->where(['id' => $dataStandard['master_id']])->asArray()->one();
        if (empty($match)) {
            return false;
        }


        $stream_list['streamer'] = $new['streamer'];
        $stream_list['viewers'] = $new['viewers'];
        $stream_list['match_id'] = $match['id'];
        if ($originType == Consts::ORIGIN_HLTV){
            $stream_list['video_origin_id'] = $new['video_origin_id'];
        }
        $stream_list['live_url'] = $new['live_url'];
        $stream_list['live_embed_url'] = $new['live_embed_url'];
        $stream_list['country'] = $new['country'];
        $stream_list['platform_name'] = $new['platform_name'];
        $stream_list['platform_id'] = $new['platform_id'];
        $stream_list['video_id'] = $new['id'];
        $stream_list['is_official'] = $is_official;
        if ($originType == Consts::ORIGIN_HLTV) {
            $MatchStreamList = MatchStreamList::find()->where(['live_url' => $liveUrl, 'match_id' => $match['id']])->one();
            if (empty($MatchStreamList)) {
                StreamService::matchStreamListAdd($stream_list, $match['id']);

            }
        } else {
            StreamService::matchStreamListAdd($stream_list, $dataStandard['master_id'], 'select', null, $originId, null);

        }


//        $stream_list['streamer'] = $new['streamer'];
//        $stream_list['title'] = $new['title'];
//        $stream_list['viewers'] = $new['viewers'];
//        $stream_list['match_id'] = $match['id'];
//        $stream_list['live_url'] = $new['live_url'];
//        $stream_list['live_embed_url'] = $new['live_embed_url'];
//        $stream_list['video_id'] = $video_id;
//        $stream_list['platform_name'] = $platform_name;
//        $stream_list['platform_id'] = $platform_id;
//        $stream_list['is_official'] = $is_official;

    }
    /**
     * @param $liveUrl
     * @param string $liveEmbedUrl
     * @param string $officialStreamUrl
     * @param $originId
     * @param $gameId
     * @param $matchRelId
     * @return bool
     * @throws BusinessException
     * 待添加视频表中添加数据
     */
//    public static function addStandardDataMatchVideoList($liveUrl, $liveEmbedUrl = "", $officialStreamUrl = "",$originId, $gameId, $matchRelId)
//    {
//        //是否是官方url
//        $is_official =0;
//        if($officialStreamUrl){
//            $is_official = 1;
//            $checkOfficial = StandardDataMatchVideoList::find()->where(['live_url' => $liveUrl])->one();
//            if ($checkOfficial) {
//                $matchStreamList = MatchStreamList::find()->where(['video_id'=>$checkOfficial['id']])->one();
//                if($matchStreamList){
//                    $matchStreamList->setAttribute('is_official',1);
//                    $matchStreamList->save();
//                }
//            }
//        }
//
////        // 根据url判断重复，如果重复，则不添加
////        $check = StandardDataMatchVideoList::find()->where(['live_url' => $liveUrl])->one();
////        if ($check) {
////            return $check;
////        }
//
//        $murl =  md5($liveUrl);
//        if (empty($liveEmbedUrl) && $liveUrl) {
//            $urlInfo = StreamService::getInfoFromUrl($liveUrl);
//            $liveEmbedUrl = StreamService::completionUrl($urlInfo['platform_name'],$urlInfo['room_id']);
//        }
//        $new = StandardDataMatchVideoList::find()->where(['live_url_identity'=>$murl])->one();
//        if(!$new){
//            $new = new StandardDataMatchVideoList();
//        }
//        $platform_name = StreamService::getInfoFromUrl($liveUrl)['platform_name'];
//        $platform_id = (int)StreamService::getPlatformIdbyName($platform_name)['id'] ? (int)StreamService::getPlatformIdbyName($platform_name)['id'] : null;;
//
//        $new->setAttributes(
//            [
//                'live_url' => $liveUrl,
//                'live_url_identity' => $murl,
//                'live_embed_url' => $liveEmbedUrl,
//                'official_stream_url' => $officialStreamUrl,
//                'origin_id' => $originId,
//                'game_id' => $gameId,
//                'match_rel_id' => $matchRelId,
//                'platform_name' =>$platform_name,
//                'platform_id' =>$platform_id,
//            ]
//        );
//        if (!$new->save()) {
//            throw new BusinessException($new->getErrors(), "保存视频流失败");
//        }
////        $match_rel_id = $rRelationInfo->match_rel_id;
////        $video_id = $rRelationInfo->video_id;
//        $relationInfo = ['video_id' => $new['id'], 'origin_id' => $originId, 'match_rel_id' => $matchRelId];
//
//        $relationVideoList = StandardDataMatchVideoListRelation::find()->where($relationInfo)->one();
//        if (!$relationVideoList){
//            $relationVideoList = new StandardDataMatchVideoListRelation();
//        }
//
//        $relationVideoList->setAttributes($relationInfo);
//        if (!$relationVideoList->save()){
//            throw new BusinessException($new->getErrors(), "保存视频流到关联表失败");
//        }
//
//
//        $video_id = $relationVideoList['video_id'];
//        //有源端id得到数据
//        $standardDataMatch= StandardDataMatch::find()->select(['id'])
//            ->where(['rel_identity_id'=>$matchRelId,'origin_id'=>$new['origin_id']])->asArray()->one();
//        if(!$standardDataMatch){
//            return false;
//        }
//        $dataStandard = DataStandardMasterRelation::find()->select(['master_id'])
//            ->where(['standard_id'=>$standardDataMatch['id'],'resource_type'=>'match'])->asArray()->one();
//        if (empty($dataStandard)) {
//            return false;
//        }
//        $match = Match::find()->where(['id'=>$dataStandard['master_id']])->asArray()->one();
//        if (empty($match)) {
//          return false;
//        }
//        $stream_list['streamer'] = $new['streamer'];
//        $stream_list['viewers'] = $new['viewers'];
//        $stream_list['match_id'] = $match['id'];
//        $stream_list['live_url'] = $new['live_url'];
//        $stream_list['live_embed_url'] = $new['live_embed_url'];
//        $stream_list['video_id'] = $video_id;
//        $stream_list['platform_name'] = $platform_name;
//        $stream_list['platform_id'] = $platform_id;
//        $stream_list['is_official'] = $is_official;
//
//        StreamService::matchStreamListAdd($stream_list,$dataStandard['master_id'],'select',null,$new['origin_id'],'');
//
//
////        $MatchStreamList = MatchStreamList::find()
////            ->where(['live_url' => $new['live_url'],'match_id'=>$dataStandard['master_id']])->one();
////        if (!$MatchStreamList) {
////            $MatchStreamList = new MatchStreamList();
////            $stream_list['origin_id'] = $new['origin_id'];
////
////        }
////        $stream_list['streamer'] = $new['streamer'];
////        $stream_list['viewers'] = $new['viewers'];
////        $stream_list['match_id'] = $match['id'];
////        $stream_list['live_url'] = $new['live_url'];
////        $stream_list['live_embed_url'] = $new['live_embed_url'];
////        $stream_list['video_id'] = $video_id;
////        $stream_list['platform_name'] = $platform_name;
////        $stream_list['platform_id'] = $platform_id;
////        $stream_list['is_official'] = $is_official;
////        $MatchStreamList->setAttributes($stream_list);
////
////        if (!$MatchStreamList->save()) {
////            throw new BusinessException($new->getErrors(), "保存视频流失败");
////
////        }
//
//    }


//    public static function addMatchVideoByAbiosMatch($liveUrl, $liveEmbedUrl = "", $officialStreamUrl = false ,$originId, $gameId, $matchRelId,$streamer = null,$streamername = null,$title = null,$viewers = 0){
//        //是否是官方url
//        $is_official = 2;
//        if($officialStreamUrl){
//            $is_official = 1;
//            $checkOfficial = StandardDataMatchVideoList::find()->where(['live_url' => $liveUrl])->one();
//            if ($checkOfficial) {
//                $matchStreamList = MatchStreamList::find()->where(['video_id'=>$checkOfficial['id']])->one();
//                if($matchStreamList){
//                    $matchStreamList->setAttribute('is_official',1);
//                    $matchStreamList->save();
//                }
//            }
//        }
//
////        // 根据url判断重复，如果重复，则不添加
////        $check = StandardDataMatchVideoList::find()->where(['live_url' => $liveUrl])->one();
////        if ($check) {
////            return $check;
////        }
//
//        $murl =  md5($liveUrl);
//        if (empty($liveEmbedUrl) && $liveUrl) {
//            $urlInfo = StreamService::getInfoFromUrl($liveUrl);
//            $liveEmbedUrl = StreamService::completionUrl($urlInfo['platform_name'],$urlInfo['room_id']);
//        }
//        $new = StandardDataMatchVideoList::find()->where(['live_url_identity'=>$murl])->one();
//        if(!$new){
//            $new = new StandardDataMatchVideoList();
//        }
//        $platform_name = StreamService::getInfoFromUrl($liveUrl)['platform_name'];
//        $platform_id = (int)StreamService::getPlatformIdbyName($platform_name)['id'] ? (int)StreamService::getPlatformIdbyName($platform_name)['id'] : null;;
//
//        if ($officialStreamUrl){
//            $officialStreamUrl = $liveUrl;
//        }else{
//            $officialStreamUrl = null;
//        }
//
//        $new->setAttributes(
//            [
//                'live_url' => $liveUrl,
//                'streamer'=>$streamername,
//                'title'=>$title,
//                'viewers'=>$viewers,
//                'live_url_identity' => $murl,
//                'live_embed_url' => $liveEmbedUrl,
//                'official_stream_url' => $officialStreamUrl,
//                'origin_id' => $originId,
//                'game_id' => $gameId,
//                'match_rel_id' => $matchRelId,
//                'platform_name' =>$platform_name,
//                'platform_id' =>$platform_id,
//            ]
//        );
//        if (!$new->save()) {
//            throw new BusinessException($new->getErrors(), "保存视频流失败");
//        }
////        $match_rel_id = $rRelationInfo->match_rel_id;
////        $video_id = $rRelationInfo->video_id;
//        $relationInfo = ['video_id' => $new['id'], 'origin_id' => $originId, 'match_rel_id' => $matchRelId];
//
//        $relationVideoList = StandardDataMatchVideoListRelation::find()->where($relationInfo)->one();
//        if (!$relationVideoList){
//            $relationVideoList = new StandardDataMatchVideoListRelation();
//        }
//
//        $relationVideoList->setAttributes($relationInfo);
//        if (!$relationVideoList->save()){
//            throw new BusinessException($new->getErrors(), "保存视频流到关联表失败");
//        }
//
//
//        $video_id = $relationVideoList['video_id'];
//        //有源端id得到数据
//        $standardDataMatch= StandardDataMatch::find()->select(['id'])
//            ->where(['rel_identity_id'=>$matchRelId,'origin_id'=>$new['origin_id']])->asArray()->one();
//        if(!$standardDataMatch){
//            return false;
//        }
//        $dataStandard = DataStandardMasterRelation::find()->select(['master_id'])
//            ->where(['standard_id'=>$standardDataMatch['id'],'resource_type'=>'match'])->asArray()->one();
//        if (empty($dataStandard)) {
//            return false;
//        }
//        $match = Match::find()->where(['id'=>$dataStandard['master_id']])->asArray()->one();
//        if (empty($match)) {
//            return false;
//        }
//
//        $stream_list['streamer'] = $new['streamer'];
//        $stream_list['title'] = $new['title'];
//        $stream_list['viewers'] = $new['viewers'];
//        $stream_list['match_id'] = $match['id'];
//        $stream_list['live_url'] = $new['live_url'];
//        $stream_list['live_embed_url'] = $new['live_embed_url'];
//        $stream_list['video_id'] = $video_id;
//        $stream_list['platform_name'] = $platform_name;
//        $stream_list['platform_id'] = $platform_id;
//        $stream_list['is_official'] = $is_official;
//        StreamService::matchStreamListAdd($stream_list,$dataStandard['master_id'],'select',null,$new['origin_id'],'');
//
////        $MatchStreamList = MatchStreamList::find()
////            ->where(['live_url' => $new['live_url'],'match_id'=>$dataStandard['master_id']])->one();
////        if (!$MatchStreamList) {
////            $MatchStreamList = new MatchStreamList();
////            $stream_list['origin_id'] = $new['origin_id'];
////
////        }
////        $stream_list['streamer'] = $new['streamer'];
////        $stream_list['title'] = $new['title'];
////        $stream_list['viewers'] = $new['viewers'];
////        $stream_list['match_id'] = $match['id'];
////        $stream_list['live_url'] = $new['live_url'];
////        $stream_list['live_embed_url'] = $new['live_embed_url'];
////        $stream_list['video_id'] = $video_id;
////        $stream_list['platform_name'] = $platform_name;
////        $stream_list['platform_id'] = $platform_id;
////        $stream_list['is_official'] = $is_official;
////        $MatchStreamList->setAttributes($stream_list);
////
////        if (!$MatchStreamList->save()) {
////            throw new BusinessException($new->getErrors(), "保存视频流失败");
////
////        }
//
//    }


    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @param $jsonInfo
     * @return array
     * @throws BusinessException
     * 设置hotInfo，并且返回json_info的diff信息
     */
    public static function setHotLog($originId, $resourceType, $gameId, $identityId, $info,$refreshType ="")
    {
        $newInfo = $info;
        $oldInfo = '';
        $hotLog = TaskDataHotInfo::find()->where([
            'origin_id'     => (int)$originId,
            'resource_type' => (string)$resourceType,
            'game_id'       => (int)$gameId,
            'identity_id'   => (string)$identityId,
        ])->one();
        if ($hotLog) {
            if($refreshType == "refresh"){
                $hotLog->setAttribute('json_info',null);
                $hotLog->save();
                $oldInfo = '';
            }else{
                $oldInfo = json_decode($hotLog['json_info'],true);
            }

        }
        if ($newInfo == $oldInfo) {
            // 数据相同，不用比较
            return ["changed" => false,
                'change_type' => QueueServer::CHANGE_TYPE_EQUAL,
                'diff' => [],
                'old' => $oldInfo,
                'new' => $newInfo];
        }

        if (!$hotLog) {
            $hotLog = new TaskDataHotInfo();
            $hotLog->setAttributes(
                [
                    'origin_id' => $originId,
                    'resource_type' => $resourceType,
                    'game_id' => $gameId,
                    'identity_id' => (string)$identityId
                ]
            );
        }
        $jsonInfo=json_encode($info);
        $hotLog->setAttribute('json_info', $jsonInfo);
        if (!$hotLog->save()) {
            throw new BusinessException($hotLog->getErrors(), '保存log失败');
        }
        return self::getDiffInfo($oldInfo, $newInfo);
    }

    public static function getHotLog($originId, $resourceType, $gameId, $identityId)
    {
        $hotLog = TaskDataHotInfo::find()->where([
            'origin_id'     => (int)$originId,
            'resource_type' => (string)$resourceType,
            'game_id'       => (int)$gameId,
            'identity_id'   => (string)$identityId,
        ])->one();
        return json_decode($hotLog['json_info'], true);
    }

    /**
     * 根据数据源游戏id转换为自己平台的id
     * @param $originType 数据源类型(常量)
     * @param $mapKey   数据源id
     * @return array
     * @throws \yii\base\Exception
     */
    public static function getGameByMapperId($originType, $mapKey)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                1 => ['name' => 'LoL', 'id' => 2],
//                2=>['name'=>,'id'=>],
                3 => ['name' => 'CS:GO', 'id' => 1],
                4 => ['name' => 'Dota 2', 'id' => 3],
                14 => ['name' => 'Overwatch', 'id' => 5],
                20 => ['name' => 'PUBG', 'id' => 8],
                22 => ['name' => 'Rocket League', 'id' => 11],
                23 => ['name' => 'Call of Duty Modern Warfare', 'id' => 10],
                24 => ['name' => 'Rainbow 6 Siege', 'id' => 9],
                25 => ['name' => 'FIFA', 'id' => 19],
                26 => ['name' => 'Valorant', 'id' => 12]
            ],
            Consts::ORIGIN_ABIOS => [
                1 => ['name' => 'Dota 2', 'id' => 3],
//                2=>['name'=>,'id'=>],
                2 => ['name' => 'LOL', 'id' => 2],
                3 => ['name' => 'SCII', 'id' => 6],
                4 => ['name' => 'HoN', 'id' => 20],
                5 => ['name' => 'CS:GO', 'id' => 1],
                6 => ['name' => 'Hearthstone', 'id' => 7],
                7 => ['name' => 'WoW', 'id' => 13],
                8 => ['name' => 'SMITE', 'id' => 14],
                9 => ['name' => 'HotS', 'id' => 13],
                10 => ['name' => 'Smash', 'id' => 15],
                11 => ['name' => 'CoD', 'id' => 10],
                12 => ['name' => 'OW', 'id' => 5],
                13 => ['name' => 'SC', 'id' => 6],
                14 => ['name' => 'KoG', 'id' => 4],
                15 => ['name' => 'PUBG', 'id' => 8],
                16 => ['name' => 'Fortnite', 'id' => 18],
                17 => ['name' => 'R6', 'id' => 9],
                18 => ['name' => 'RL', 'id' => 11],
                19 => ['name' => 'FIFA', 'id' => 19],
                20 => ['name' => 'Valorant', 'id' => 12],
            ],
            Consts::ORIGIN_HLTV => [
                1 => ['name' => 'CS:GO', 'id' => 1],
            ],
            Consts::ORIGIN_BAYES => [
                1 => ['name' => 'OW', 'id' => 5],
                2 => ['name' => 'CS:GO', 'id' => 1],
                3 => ['name' => 'Dota 2', 'id' => 3],
                4 => ['name' => 'LOL', 'id' => 2],
                5 => ['name' => 'RL', 'id' => 11],
                6 => ['name' => 'R6', 'id' => 9],
                7 => ['name' => 'AOV', 'id' => 22],
                8 => ['name' => 'KOG', 'id' => 4],
                9 => ['name' => 'SMITE', 'id' => 14],
                10 => ['name' => 'NBA2K', 'id' => 23],
                11 => ['name' => 'COD', 'id' => 10],
                12 => ['name' => 'HS', 'id' => 7],
                13 => ['name' => 'SC2', 'id' => 6],
            ],
            Consts::ORIGIN_BAYES2 => [
//                1 => ['name' => 'OW', 'id' => 5],
                2 => ['name' => 'CS:GO', 'id' => 1],
                3 => ['name' => 'Dota 2', 'id' => 3],
                4 => ['name' => 'LOL', 'id' => 2],
//                5 => ['name' => 'RL', 'id' => 11],
//                6 => ['name' => 'R6', 'id' => 9],
//                7 => ['name' => 'AOV', 'id' => 22],
//                8 => ['name' => 'KOG', 'id' => 4],
//                9 => ['name' => 'SMITE', 'id' => 14],
//                10 => ['name' => 'NBA2K', 'id' => 23],
//                11 => ['name' => 'COD', 'id' => 10],
//                12 => ['name' => 'HS', 'id' => 7],
//                13 => ['name' => 'SC2', 'id' => 6],
            ],
            Consts::ORIGIN_RADAR_PURPLE => [
                1 => ['name' => 'LOL', 'id' => 2],
                2 => ['name' => 'Dota 2', 'id' => 3],
                3 => ['name' => 'CS:GO', 'id' => 1],
            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$mapKey])) {
            throw new \yii\base\Exception(sprintf("不存在对应的game映射:资源类型（%s）gameKey（%s）", $originType, $mapKey));
        }
        return $mapping[$originType][$mapKey];
    }

    /**
     * 根据自己平台id转换为数据源游戏id
     * @param $originType
     * @param $mapKey
     * @return array
     * @throws \yii\base\Exception
     */
//    public static function getOriginGameById($originType, $mapKey)
//    {
//        $mapping = [
//            Consts::ORIGIN_BAYES => [
//                5 => ['name' => 'OW', 'id' => 1],
//                1 => ['name' => 'CS:GO', 'id' => 2],
//                3 => ['name' => 'Dota 2', 'id' => 3],
//                2 => ['name' => 'LOL', 'id' => 4],
//                11 => ['name' => 'RL', 'id' => 5],
//                9 => ['name' => 'R6', 'id' => 6],
//                4 => ['name' => 'AOV', 'id' => 7],
//                4 => ['name' => 'AOV', 'id' => 8],
//                14 => ['name' => 'SMITE', 'id' => 9],
//                0 => ['name' => 'NBA2K', 'id' => 10],
//                10 => ['name' => 'COD', 'id' => 11],
//                7 => ['name' => 'HS', 'id' => 12],
//                6 => ['name' => 'SC2', 'id' => 13],
//            ],
//        ];
//        if (!isset($mapping[$originType])) {
//            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
//        }
//        if (!isset($mapping[$originType][$mapKey])) {
//            throw new \yii\base\Exception(sprintf("不存在对应的game映射:资源类型（%s）gameKey（%s）", $originType, $mapKey));
//        }
//        return $mapping[$originType][$mapKey];
//    }
    //转换lol位置id   role
    public static function getLolRoleStringByRoleId($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                '1' => 'Top',
                '2' => 'Jungle',
                '3' => 'Mid',
                '4' => 'ADC',
                '5' => 'Support'
            ],
            Consts::ORIGIN_ORANGE => [
                '1' => 'Top',
                '2' => 'Jungle',
                '3' => 'Mid',
                '4' => 'ADC',
                '5' => 'Support'
            ],
        ];
        return @$mapping[$originType][$positionTypeString];
    }
    //转换lol位置id   role
    public static function getLolPositionByString($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                'adc' => 4,
                'mid' => 3,
                'sup' => 5,
                'top' => 1,
                'jun' => 2,
                'jungle' => 2,
            ],
            Consts::ORIGIN_ABIOS => [
                'Top' => 1,
                'Mid' => 3,
                'Bot' => 4,
                'Jungle' => 2,
                'Support' => 5,
            ],
            Consts::ORIGIN_ORANGE => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5
            ],
            Consts::ORIGIN_BAYES => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5
            ],
        ];
        return @$mapping[$originType][$positionTypeString];
    }
    //转换lol位置id   lane
    public static function getLolLaneId($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 4
            ],
            Consts::ORIGIN_ORANGE => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 4
            ],
            Consts::ORIGIN_BAYES => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 4
            ],
        ];
        return @$mapping[$originType][$positionTypeString];
    }
    //转换lol位置id   lane
    public static function getLolLaneStringByLaneId($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                '1' => 'Top Lane',
                '2' => 'Jungle',
                '3' => 'Mid Lane',
                '4' => 'Bot Lane'
            ],
            Consts::ORIGIN_ORANGE => [
                '1' => 'Top Lane',
                '2' => 'Jungle',
                '3' => 'Mid Lane',
                '4' => 'Bot Lane'
            ],
        ];
        return @$mapping[$originType][$positionTypeString];
    }
    //转换dota 位置获取lane 分路
    public static function getDota2LaneByPositionId($originType,$positionCname)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                '1' => 5,
                '2' => 6,
                '3' => 7,
                '4' => 8,
                '5' => 8,
                '1/2' => 5,
                '2/3' => 6,
                '3/4' =>7,
                '4/5' =>8
            ],
            Consts::ORIGIN_ABIOS => [
                '1' => 5,
                '2' => 6,
                '3' => 7,
                '4' => 8,
                '5' => 8,
                '1/2' => 5,
                '2/3' => 6,
                '3/4' =>7,
                '4/5' =>8
            ],
            Consts::ORIGIN_RADAR_PURPLE => [
                '1' => 5,
                '2' => 6,
                '3' => 7,
                '4' => 8,
                '5' => 8
            ],
        ];
        return @$mapping[$originType][$positionCname];
    }
    //根据指定string转换dota位置
    public static function getDota2PositionByString($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                '1' => 6,
                '2' => 7,
                '3' => 8,
                '4' => 9,
                '5' => 10,
                '1/2' => 11,
                '2/3' => 24,
                '3/4' =>25,
                '4/5' =>26
            ],
            Consts::ORIGIN_ABIOS => [
                '1' => 6,
                '2' => 7,
                '3' => 8,
                '4' => 9,
                '5' => 10,
                '1/2' => 11,
                '2/3' => 24,
                '3/4' =>25,
                '4/5' =>26
            ],
            Consts::ORIGIN_RADAR_PURPLE => [
                '1' => 6,
                '2' => 7,
                '3' => 8,
                '4' => 9,
                '5' => 10,
            ],
        ];
        return @$mapping[$originType][$positionTypeString];
    }
    //转换dota 神符
    public static function getDota2RuneByItemInfo($originType,$item)
    {
        $mapping = [
            Consts::ORIGIN_BAYES => [
                'rune_arcane' => 'arcane',
                'rune_illusion' => 'illusion',
                'rune_regen' => 'regeneration',
                'rune_doubledamage' => 'double_damage',
                'rune_invis' => 'invisibility',
                'rune_haste' => 'haste'
            ],
        ];
        return @$mapping[$originType][$item];
    }
    //abios游戏位置转换(根据abios的来源id转换成码表id)
    public static function getAbiosPositionByRelId($gameId,$relId)
    {
        $mapping = [
            "2" =>[  //lol
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
            ],
            "3" => [  //dota2
                '10' => 6,
                '11' => 11,
                '12' => 7,
                '13' => 24,
                '14' => 8,
                '15' =>25,
                '16' =>9,
                '17' =>26,
                '18' =>10,
            ]
        ];
        if (!isset($mapping[$gameId]) || !isset($mapping[$gameId][$relId])) {
            return $relId;
        }
        return $mapping[$gameId][$relId];
    }

    //转换守望先锋位置
    public static function getStarOverwatchPositionByString($originType,$positionTypeString)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                'Tank' => 17,
                'DPS' => 18,
                'Damage' => 18,
                'offense' => 18,
                'Support' => 19,
                'sup' => 19,
                'Flex' => 20,
            ],
            Consts::ORIGIN_ABIOS => [
                'Tank' => 17,
                'DPS' => 18,
                'Damage' => 18,
                'offense' => 18,
                'Support' => 19,
                'sup' => 19,
                'Flex' => 20,
            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$positionTypeString])) {
            throw new \yii\base\Exception(sprintf("不存在对应的game映射:资源类型（%s）gameKey（%s）", $originType, $positionTypeString));
        }
        return $mapping[$originType][$positionTypeString];
    }

    public static function getOriginInfoByType($originType)
    {
        $originInfo=EnumService::getEnum('origin_key_name');
        return $originInfo[$originType];
    }

    /**
     * @param $ability 技能名称
     * @param $hero_name 英雄名字
     */
    public static function getability($ability,$hero_name)
    {
        if(isset($ability) && isset($hero_name)){
            $result =  trim(str_replace('_',' ',str_replace($hero_name,'',$ability)));
            return $result;
        }
    }

    public static function getMatchType($originType,$typeString,$gameId=0)
    {
        if(!$typeString){
            return $typeString;
        }
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                'best_of' => '1',
                'first_to' => '2',
                'ow_best_of' => '3',
            ],
            Consts::ORIGIN_ABIOS => [
            ],
            Consts::ORIGIN_HLTV => [

            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$typeString])) {
            return $typeString;
        }
        return $mapping[$originType][$typeString];
    }
    //根据比赛类型的num获取字符串的值
    public static function getMatchTypeStringByNum($originType,$typeString)
    {
        if(!$typeString){
            return $typeString;
        }
        $mapping = [
            'common' => [
                '1' => 'best_of',
                '2' => 'first_to',
                '3' => 'ow_best_of',
                '4' => 'best_ranking'
            ],
            Consts::ORIGIN_PANDASCORE => [
                '1' => 'best_of',
                '2' => 'first_to',
                '3' => 'ow_best_of',
                '4' => 'best_ranking'
            ],
            Consts::ORIGIN_ABIOS => [
            ],
            Consts::ORIGIN_HLTV => [

            ],
        ];
        if (!isset($mapping[$originType][$typeString])) {
            return $typeString;
        }
        return $mapping[$originType][$typeString];
    }
    //根据比赛规则的num获取字符串的值
    public static function getGameRulesStringByNum($originType,$typeString)
    {
        if(!$typeString){
            return $typeString;
        }
        $mapping = [
            'common' => [
                '1' => 'normal',
                '2' => 'brawl',
                '3' => 'battle_royale'
            ],
            Consts::ORIGIN_PANDASCORE => [
                '1' => 'normal',
                '2' => 'brawl',
                '3' => 'battle_royale'
            ],
            Consts::ORIGIN_ABIOS => [
            ],
            Consts::ORIGIN_HLTV => [

            ],
        ];
        if (!isset($mapping[$originType][$typeString])) {
            return $typeString;
        }
        return $mapping[$originType][$typeString];
    }
    public static function getPandaScoreMatchStatus($matchString){
        switch ($matchString){
            case "canceled":
                return 5;
                break;
            case "finished":
                return 3;
                break;
            case "not_started":
                return 1;
                break;
            case "postponed":
                return 4;
                break;
            case "running":
                return 2;
                break;
            case "not_played":
                return 3;
                break;
        }
    }

    public static function getAbiosMatchStatus($matchString){
        switch ($matchString){
            case "over-draw":
            case "over-forfeited":
            case "over":
                return 3;
                break;
            case "upcoming":
            case "unknown":
                return 1;
                break;
            case "postponed":
                return 4;
                break;
            case "live":
                return 2;
                break;
        }
    }

    public static function getHLTVMatchStatus($matchString){

        switch ($matchString['status']){
            case "LIVE":
                return 2;
                break;
            case "Match over":
                return 3;
                break;
            case "Match postponed":
                return 4;
                break;
            case "Match canceled":  //达雷没有这个状态   预先加了个这个状态
                return 5;
                break;
                default:
                    return 1;
        }
    }
    public static function getBayesMatchStatus($matchString){
        switch ($matchString){
            case "PENDING":
                return 1;
                break;
            case "ONGOING":
                return 2;
                break;
            case "CONCLUDED":
                return 3;
                break;
            case "DELAYED":
                return 4;
                break;
            case "CANCELLED":
                return 5;
                break;
        }
    }
    public static function getRadarpurpleMatchStatus($matchString)
    {
        switch ($matchString){
            case "1":
                return 1;
                break;
            case "2":
                return 2;
                break;
            case "3":
                return 3;
                break;
        }
    }
    public static function getAbiosPrimaryAttrCnV3($String){
        $primaryAttr = ['strength'=>"力量",'agility' =>'敏捷英雄','intelligence'=>'智力英雄'];

        return $primaryAttr[$String];

    }

    public static function getAbiosMatchStatusV3($matchString){
        switch ($matchString){
            case "over-draw":
            case "over-forfeited":
            case "over":
                return 3;
                break;
            case "upcoming":
            case "unknown":
                return 1;
                break;
            case "postponed":
                return 4;
                break;
            case "live":
                return 2;
                break;
        }
    }

    public static function getPandaScoreEventNum($eventStr){
        switch ($eventStr){
            case "first_blood":
                return 1;
                break;
            case "first_to_5_kills":
                return 2;
                break;
            case "first_to_10_kills":
                return 3;
                break;
            case "first_rift_herald":
                return 4;
                break;
            case "first_dragon":
                return 5;
                break;
            case "first_baron_nashor":
                return 6;
                break;
            case "first_elder_dragon":
                return 7;
                break;
            case "first_turret":
                return 8;
                break;
            case "first_inhibitor":
                return 9;
                break;
            default:
                return 0;
                break;
        }
    }
    //get elite ID
    public static function getEliteId($eventStr){
        switch ($eventStr){
            case "rift_herald":
                return 1;
                break;
            case "elite_infernal":
                return 2;
                break;
            case "elite_mountain":
                return 3;
                break;
            case "elite_ocean":
                return 4;
                break;
            case "elite_cloud":
                return 5;
                break;
            case "elder_dragon":
                return 6;
                break;
            case "baron_nashor":
                return 7;
                break;
            case "minion":
                return 8;
                break;
            case "neutral_minion":
                return 9;
                break;
        }
    }

    public static function getGameDota2EventNum($eventStr){
        switch ($eventStr){
            case "first_blood":
                return 10;
                break;
            case "first_to_5_kills":
                return 11;
                break;
            case "first_to_10_kills":
                return 12;
                break;
            case "first_roshan":
                return 13;
                break;
            case "first_tower":
                return 14;
                break;
            case "first_barracks":
                return 15;
                break;
        }
    }

    public static function getOrangeRole($roleString){
        switch ($roleString){
            case "Mid Lane":
                return 3;
                break;
            case "Support":
                return 5;
                break;
            case "Top Lane":
                return 1;
                break;
            case "AD Carry":
                return 4;
                break;
            case "Jungler":
                return 2;
                break;
        }
    }

    /**
     * @param $positionId
     * @return mixed
     * 根据positionId获取分路id
     */
    public static function getLaneIdByRole($positionId)
    {
        $laneInfo = EnumPosition::find()->alias('ep')
            ->select('enum_lane.id,enum_lane.name,enum_lane.c_name')
            ->leftJoin('enum_lane','ep.lane_id=enum_lane.id')
            ->where(['=','ep.id',$positionId])
            ->asArray()->one();
        if($laneInfo){
            return $laneInfo['id'];
        }
    }

    /**
     * @param $positionId
     * @return mixed
     * 根据positionId获取分路c_name
     */
    public static function getLaneCNameByRole($positionId)
    {
        $laneInfo = EnumPosition::find()->alias('ep')
            ->select('enum_lane.id,enum_lane.name,enum_lane.c_name')
            ->leftJoin('enum_lane','ep.lane_id=enum_lane.id')
            ->where(['=','ep.id',$positionId])
            ->asArray()->one();
        if($laneInfo){
            return $laneInfo['c_name'];
        }
    }

    /**
     * @param $lan_name
     * @return mixed
     */
    public static function getLaneIDByRoleName($lan_name)
    {
        $laneInfo = EnumLane::find()->select('id,name,c_name')
            ->where(['like','pandascore_lan_name',$lan_name])->asArray()->one();
        if($laneInfo){
            return $laneInfo['id'];
        }
    }
    /**
     * @param $lan_name
     * @return mixed
     */
    public static function getAbiosLaneIDByRoleName($lan_name)
    {
        $laneInfo = EnumLane::find()->select('id,name,c_name')
            ->where(['like','abios_lan_name',$lan_name])->asArray()->one();
        if($laneInfo){
            return $laneInfo['id'];
        }
    }


    /**
     * 根据数据源游戏id转换为自己平台的id
     * @param $originType (数据源类型)
     * @param $orginEvent  (原事件)
     * @return array
     * @throws \yii\base\Exception
     */
    public static function geGoalIdLOLEventTypeByEvent($originType,$orginEvent)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                'baron_nashor_kill' => 'elite_kill',//纳什男爵
                'rift_herald_kill' =>'elite_kill',//峡谷先锋
                'drake_kill' => 'elite_kill',//炼狱亚龙,山脉亚龙,海洋亚龙,云端亚龙
                'elder_dragon_kill' =>'elite_kill',//远古巨龙
                'player_kill' => 'player_kill',//玩家杀
                'suicide_kill' => 'player_suicide',//自杀
                'tower_kill' => 'building_kill',//塔
                'nexus_kill' =>'building_kill',//水晶/主堡
                'inhibitor_kill' => 'building_kill' //召唤水晶
            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$orginEvent])) {
//            throw new \yii\base\Exception(sprintf("不存在对应的映射:资源类型（%s）gameKey（%s）", $originType, $originType));
            return $orginEvent;
        }
        return $mapping[$originType][$orginEvent];
    }

    /**
     * 根据英文角色类型转换成中文角色
     * @param $originType (数据源类型)
     * @param $original  (英文字符串)
     * @return array
     * @throws \yii\base\Exception
     */
    public static function getChampionRoleCN($originType,$original)
    {
        $mapping = [
            Consts::ORIGIN_RIOTGAMES => [
                'Assassin' => '刺客',
                'Fighter' => '战士',
                'Mage' => '法师',
                'Marksman' => '射手',
                'Support' => '辅助',
                'Tank' => '坦克'
            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$original])) {
            return $original;
        }
        return $mapping[$originType][$original];
    }

    /**
     * @param $positionId
     * @return array|\yii\db\ActiveRecord
     * 通过主键id获取位置c_name
     */
    public static function getPositionCNameById($positionId)
    {
       $enumPosition = EnumPosition::find()->where(['id' => $positionId])->one();
       if($enumPosition){
           return $enumPosition['c_name'];
       }
    }
    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal['id'];
            }
        }
    }
    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalNameByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal['ingame_obj_name'];
            }
        }
    }
    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getEnumIngameGoalInfoByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                $result = [
                    'dragon_name' =>  $goal['ingame_obj_name'],
                    'dragon_name_cn' =>  $goal['ingame_obj_name_cn']
                ];
                return $result;
            }
        }
    }

    /**
     * @param $other
     * @return string
     *slug生成规则
     */
    public static function makeConstantMatching($other)
    {
        $preg='/[a-zA-Z0-9\s_-]/i';
        preg_match_all($preg,$other,$matchAll);
        $str=implode("",$matchAll[0]);
        $str=str_replace(' ','_',$str);
        $str=str_replace('-','_',$str);
        $str=strtolower($str);
        return $str;
    }

    /**
     * @param $metadataType
     * @return int
     * 根据元数据类型返回游戏id
     */
    public static function getGameIdByMetaDataType($metadataType)
    {
        switch ($metadataType)
        {
            case Consts::METADATA_TYPE_CSGO_MAP:
            case Consts::METADATA_TYPE_CSGO_WEAPON:
                return 1;
                break;
            case Consts::METADATA_TYPE_LOL_ABILITY:
            case Consts::METADATA_TYPE_LOL_CHAMPION:
            case Consts::METADATA_TYPE_LOL_ITEM:
            case Consts::METADATA_TYPE_LOL_RUNE:
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                return 2;
                break;
            case Consts::METADATA_TYPE_DOTA2_HERO:
            case Consts::METADATA_TYPE_DOTA2_ITEM:
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
            case Consts::METADATA_TYPE_DOTA2_TALENT:
                return 3;
                break;
            case Consts::METADATA_TYPE_KOG_CHAMPION:
            case Consts::METADATA_TYPE_KOG_ABILITY:
            case Consts::METADATA_TYPE_KOG_ITEM:
            case Consts::METADATA_TYPE_KOG_RUNE:
            case Consts::METADATA_TYPE_KOG_SUMMONER_SPELL:
                return 4;
                break;
        }
    }

    /**
     * @param $originType
     * @param $original
     * @return string
     * @throws \yii\base\Exception
     * 获取csgo的武器类型
     */
    public static function getKindByOrigin($originType,$original)
    {
        $mapping = [
            Consts::ORIGIN_PANDASCORE => [
                'primary' => '1',
                'secondary' => '2',
                'knife' => '3',
                'grenade' => '4',
                'gear' => '5'
            ],
        ];
        if (!isset($mapping[$originType])) {
            throw new \yii\base\Exception("不存在对应的数据源类型" . $originType);
        }
        if (!isset($mapping[$originType][$original])) {
            return $original;
        }
        return $mapping[$originType][$original];
    }

    /**
     * @param $value
     * @return mixed
     * 获取码表的武器kind
     */
    public static function getKindIdByPandascore($value)
    {
        $one =EnumCsgoWeapon::find()->select(['id'])->where(['like','panda_score',$value])->one();
        if($one){
            return $one['id'];
        }
    }

    /**
     * @param $array
     * @param $key
     * @param int $sort
     * @return mixed
     * 二维数组按指定key排序
     */
    public static function sort_for_arrays($array, $key, $sort = SORT_ASC)
    {
        $keys = array_column($array,$key);
        array_multisort($keys, $sort, $array);
        return $array;
    }


    /**
     * @param $resourceType
     * @param $originId
     * @param $isAll  //全部传 'all'  ,单个传 ’one‘
     * @param $relIdentityId   //全部不传 ， 单个传原始id
     * @return array
     */
    public static  function checkGrebIsDel($resourceType,$originId,$isAll,$relIdentityId = 0) {
        $url = '';
        switch ($resourceType){
            case Consts::RESOURCE_TYPE_MATCH:
                $url = 'api.pandascore.co/matches';
                break;
            case Consts::RESOURCE_TYPE_TEAM:
                $url = 'api.pandascore.co/teams';
                break;
            case Consts::RESOURCE_TYPE_PLAYER:
                $url = 'api.pandascore.co/players';
                break;
            case Consts::RESOURCE_TYPE_TOURNAMENT:
                $url = 'api.pandascore.co/series';
                break;
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                $url = 'api.pandascore.co/lol/spells'; //召唤师技能
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:
                $url = 'api.pandascore.co/lol/versions/all/champions';   //英雄
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:
                $url = 'api.pandascore.co/lol/versions/all/items';//道具
                break;
            case Consts::METADATA_TYPE_CSGO_MAP:
                $url = 'api.pandascore.co/csgo/maps'; //cs地图
                break;
            case Consts::METADATA_TYPE_CSGO_WEAPON:
                $url = 'api.pandascore.co/csgo/weapons';//cs武器
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:
                $url = 'api.pandascore.co/dota2/items';//道具
                break;
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
            case Consts::METADATA_TYPE_DOTA2_TALENT:
                $url = 'api.pandascore.co/dota2/abilities';//技能
                break;
            case Consts::METADATA_TYPE_DOTA2_HERO:
                $url = 'api.pandascore.co/dota2/heroes';//英雄
                break;

        }
        if ($isAll == 'all') {
            $newnewDiff =  self::checkGrebIsDelAll($url,$resourceType,$originId);
        }

        if ($isAll == 'one') {
            $newnewDiff =  self::checkGrebIsDelOne($url,$resourceType,$originId,$relIdentityId);

        }

        return $newnewDiff;

    }

    public static function checkGrebIsDelOne($url,$resourceType,$originId,$relIdentityId) {
        $standardDataActiveTable = OriginRunBase::getStandardActiveTable($resourceType);
        $standardData = explode('\\',$standardDataActiveTable);
        $model = end($standardData);
        $getStandardIds = self::getRefreshStand($model,$relIdentityId,$resourceType,$standardDataActiveTable,$originId);
        $standardIdentityIdStr = !empty($getStandardIds) ? implode(",", $getStandardIds) : 0;
        $params['token']='SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM';
        $params['filter[id]'] = $standardIdentityIdStr ;
        $output = Common::requestGetCross($url,[],$params);
        $outputData = json_decode($output,true);
        $outputMatchId = array_column($outputData,'id');
        $diffId=array_diff($getStandardIds,$outputMatchId);
        $newnewDiff = [];
        if (!empty($diffId)) {
            foreach ($diffId as $key => $val) {
                $newnewDiff[] = $val;
                if ($model  == 'StandardDataMetadata') {
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$val ,'metadata_type' => $resourceType ,'origin_id'=>$originId])->asArray()->one();

                }elseif($model  == 'StandardDataTournament'){
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$val ,'origin_id'=>$originId ,'type'=>1])->asArray()->one();

                }else{
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$val ,'origin_id'=>$originId])->asArray()->one();

                }
                $transInfo = [
                    'deleted'=>1
                ];
                $game['id'] = $standardDataMatchOne['game_id'];
                $diffInfo = OriginRunBase::setStandardData($originId, $resourceType, $game['id'], $val, $transInfo);

                if ($diffInfo['changed']) {
                    $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                        QueueServer::QUEUE_ORIGIN_PANDASCORE,
                        $resourceType, //修改类型
                        $diffInfo['change_type'],
                        $game['id']);  //game_id 修改
                    $item = [
                        "tag" => $tag,
                        "batch_id" => \app\modules\task\services\QueueServer::setBatchId(\app\modules\common\services\Consts::EXECUTE_TYPE_CTB,\app\modules\common\services\Consts::ORIGIN_PANDASCORE,$resourceType),
                        "params" => $diffInfo,
                    ];
                    TaskRunner::addTask($item,1);
                }
            }

        }

        return $newnewDiff;
    }
    public static function getRefreshStand($model,$relIdentityId,$resourceType,$standardDataActiveTable,$originId) {
        if ($model  == 'StandardDataMetadata') {
            $standardDataMatchOne = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['metadata_type' => $resourceType ,'origin_id'=>$originId]) ->andWhere(['>=','rel_identity_id',$relIdentityId])->orderBy('rel_identity_id asc')->limit('5')->asArray()->all();

        }elseif($model  == 'StandardDataTournament'){
            $standardDataMatchOne = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['origin_id'=>$originId])->andWhere(['type'=>1]) ->andWhere(['>=','rel_identity_id',$relIdentityId])->orderBy('rel_identity_id asc')->limit('5')->asArray()->all();

        }else{
            $standardDataMatchOne = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['origin_id'=>$originId]) ->andWhere(['>=','rel_identity_id',$relIdentityId])->orderBy('rel_identity_id asc')->limit('5')->asArray()->all();
        }
        $standardIdentityId = array_column($standardDataMatchOne,'rel_identity_id','id');

        return $standardIdentityId;

    }

    public static function checkGrebIsDelAll($url,$resourceType,$originId) {

        $standardDataActiveTable = OriginRunBase::getStandardActiveTable($resourceType);
        $standardData = explode('\\',$standardDataActiveTable);
        $model = end($standardData);
        if ($model  == 'StandardDataMetadata') {
            $standaraMatchCout = $standardDataActiveTable::find()->select('*')->where(['origin_id'=>$originId , 'metadata_type'=>$resourceType ,'deleted'=>2])->count();

        }elseif($model  == 'StandardDataTournament'){
            $standaraMatchCout = $standardDataActiveTable::find()->select('*')->where(['origin_id'=>$originId,'type'=>1 ,'deleted'=>2])->count();

        }else{
            $standaraMatchCout = $standardDataActiveTable::find()->select('*')->where(['origin_id'=>$originId ,'deleted'=>2])->count();
        }
        $pageSize = 50;
        $totalPage = ceil($standaraMatchCout / $pageSize);
        $j = 0;
        $newDiff = [];
        for ($i = 0; $i < $totalPage; $i++) {
            $j++;
            $offset = ($j - 1) * $pageSize;
            if ($model  == 'StandardDataMetadata') {
                $standardDataMatchData = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['origin_id'=>$originId ,'metadata_type'=> $resourceType ,'deleted'=>2])->orderBy('id asc')->limit($pageSize)->offset($offset)->asArray()->all();

            }elseif($model  == 'StandardDataTournament'){
                $standardDataMatchData = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['origin_id'=>$originId,'type'=>1 ,'deleted'=>2])->orderBy('id asc')->limit($pageSize)->offset($offset)->asArray()->all();

            }else{
                $standardDataMatchData = $standardDataActiveTable::find()->select(["id","rel_identity_id"])->where(['origin_id'=>$originId ,'deleted'=>2])->orderBy('id asc')->limit($pageSize)->offset($offset)->asArray()->all();

            }


            $standardIdentityId = array_column($standardDataMatchData,'rel_identity_id','id');
            $standardIdentityIdStr = !empty($standardIdentityId) ? implode(",", $standardIdentityId) : 0;

            $params['token']='SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM';
            $params['filter[id]'] = $standardIdentityIdStr ;
            $output = Common::requestGetCross($url,[],$params);
            $outputData = json_decode($output,true);
            $outputMatchId = array_column($outputData,'id');
            $diffId=array_diff($standardIdentityId,$outputMatchId);
            if (!empty($diffId)) {
                $newDiff[] = $diffId;
            }
        }
        $newnewDiff = [];
        foreach ($newDiff as $key => $val) {
            foreach ($val as $k => $v){
                $newnewDiff[] = $v;
                if ($model  == 'StandardDataMetadata') {
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'metadata_type' => $resourceType ,'origin_id'=>$originId ,'deleted'=>2])->asArray()->one();

                }elseif($model  == 'StandardDataTournament'){
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'origin_id'=>$originId,'type'=>1 ,'deleted'=>2])->asArray()->one();

                }else{
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'origin_id'=>$originId ,'deleted'=>2])->asArray()->one();

                }

                $transInfo = [
                    'deleted'=>1
                ];
                $game['id'] = $standardDataMatchOne['game_id'];
                $diffInfo = OriginRunBase::setStandardData($originId, $resourceType, $game['id'], $v, $transInfo);

                if ($diffInfo['changed']) {
                    $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                        QueueServer::QUEUE_ORIGIN_PANDASCORE,
                        $resourceType, //修改类型
                        $diffInfo['change_type'],
                        $game['id']);  //game_id 修改
                    $item = [
                        "tag" => $tag,
                        "batch_id" => \app\modules\task\services\QueueServer::setBatchId(\app\modules\common\services\Consts::EXECUTE_TYPE_CTB,\app\modules\common\services\Consts::ORIGIN_PANDASCORE,$resourceType),
                        "params" => $diffInfo,
                    ];
                    TaskRunner::addTask($item,1);
                }
            }
        }

        if (!empty($newnewDiff)){
            return implode($newnewDiff,',');

        }
        return '';
    }

    /**
     * @param $id
     * @param $resourceType
     * @param string $tag
     * @return array
     * @throws BusinessException
     * @throws \yii\base\Exception
     * 主表的删除操作
     */
    public static function deletedMaster($id,$resourceType,$tag = "")
    {
        if(!$id){
            return true;
        }
        $masterActiveTable = MasterDataService::getMasterActiveTable($resourceType);
        if(!$masterActiveTable){
            throw new \yii\base\Exception('获取对象失败');
        }
        $one = $masterActiveTable::find()->where(['id'=>$id])->one();
        if(!$one){
            return true;
        }
        $oldInfo = $one->toArray();
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            $one->setScenario('bing_tournament');
        }
        $one->setAttribute('deleted',1);
        $one->setAttribute('deleted_at',date('Y-m-d H:i:s'));
        if(!$one->save()){
            throw new BusinessException($one->getErrors(), '删除失败');
        }
        $newInfo = $one->toArray();
        $diffInfo = Common::getDiffInfo($oldInfo, $newInfo, [], true);
        if($diffInfo['changed']){
            OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
                $resourceType,
                $newInfo['id'],
                ["diff" => $diffInfo['diff'], "new" => $newInfo],
                0,
                $tag,
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                OperationLogService::getLoginUserId()
            );
        }
        return ['id' => $id, 'msg' => '删除成功'];
    }

    /**
     * @param $id
     * @param $resourceType
     * @param $tag
     * @return array
     * @throws BusinessException
     * @throws \yii\base\Exception
     * 主表的恢复操作
     */
    public static function recoverMaster($id,$resourceType,$tag)
    {
        if(!$id){
            return true;
        }
        $masterActiveTable = MasterDataService::getMasterActiveTable($resourceType);
        if(!$masterActiveTable){
            throw new \yii\base\Exception('获取对象失败');
        }
        $one = $masterActiveTable::find()->where(['id'=>$id])->one();
        if(!$one){
            return true;
        }
        $oldInfo = $one->toArray();
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            $one->setScenario('bing_tournament');
        }
        $one->setAttribute('deleted',2);
        $one->setAttribute('deleted_at',null);
        if(!$one->save()){
            throw new BusinessException($one->getErrors(), '恢复失败');
        }
        $newInfo = $one->toArray();
        $diffInfo = Common::getDiffInfo($oldInfo, $newInfo, [], true);
        if($diffInfo['changed']){//战队
            OperationLogService::addLog(QueueServer::CHANGE_TYPE_RECOVERY,
                $resourceType,
                $newInfo['id'],
                ["diff" => $diffInfo['diff'], "new" => $newInfo],
                0,
                $tag,
                null,
                0,
                Consts::USER_TYPE_ADMIN,
                OperationLogService::getLoginUserId()
            );
        }
        return ['id' => $id, 'msg' => '恢复成功'];
    }

    /**
     * 获取bayestitleList
     */
    public static function getBayesTitleList()
    {
        return [
            ["id"=>1,"name"=>"Overwatch"],
            ["id"=>2,"name"=>"Counter-Strike: Global Offensive"],
            ["id"=>3,"name"=>"Dota 2"],
            ["id"=>4,"name"=>"League of Legends"],
            ["id"=>5,"name"=>"Rocket League"],
            ["id"=>6,"name"=>"Rainbow Six"],
            ["id"=>7,"name"=>"Arena of Valor"],
            ["id"=>8,"name"=>"King of Glory"],
            ["id"=>9,"name"=>"Smite"],
            ["id"=>10,"name"=>"NBA2K"],
            ["id"=>11,"name"=>"Call of Duty"],
            ["id"=>12,"name"=>"Hearthstone"],
            ["id"=>13,"name"=>"StarCraft 2"],
        ];
    }
    /**
     * 获取下个月的日期
     */
    public static function getNextMonthDays($date){
        $timestamp=strtotime($date);
        $arr=getdate($timestamp);
        if($arr['mon'] == 12){
            $year=$arr['year'] +1;
            $month=$arr['mon'] -11;
            $firstday=$year.'-0'.$month.'-01';
            $lastday=date('Y-m-d',strtotime("$firstday +1 month -1 day"));
        }else{
            $firstday=date('Y-m-01',strtotime(date('Y',$timestamp).'-'.(date('m',$timestamp)+1).'-01'));
            $lastday=date('Y-m-d',strtotime("$firstday +1 month -1 day"));
        }
        return array($firstday,$lastday);
    }

    //守卫类型  bayes
    public static function getWardType($originType,$typeString){
        $mapping = [
            Consts::ORIGIN_BAYES => [
                'blueTrinket' => 'blue_trinket',
                'yellowTrinket' => 'yellow_trinket',
                'sight' => 'sight',
                'control' => 'control',
                'unknown' => 'unknown'
            ],
        ];
        return @$mapping[$originType][$typeString];
    }
    /**
     * bayes验证删除（比赛开始前检查）
     */
    public static  function checkBayesStandardIsDel($resourceType,$originId)
    {
        $action = '';
        switch ($resourceType) {
            case Consts::RESOURCE_TYPE_MATCH:
                $action = '/match';
                break;
            case Consts::RESOURCE_TYPE_TOURNAMENT:
                $action = '/tournament';
                break;
            case Consts::RESOURCE_TYPE_TEAM:
                $action = '/team';
                break;
            case Consts::RESOURCE_TYPE_PLAYER:
                $action = '/player';
                break;
        }
//        $curl = BayesBase::getCurlByParams($action,$params);
        $newnewDiff = self::checkBayesStandardIsDelAll($action, $resourceType, $originId);

        return $newnewDiff;
    }

    public static function checkBayesStandardIsDelAll($action,$resourceType,$originId) {

        $standardDataActiveTable = OriginRunBase::getStandardActiveTable($resourceType);
        $model = end($standardData);
        if ($resourceType  == 'tournament') {
            $standardData = $standardDataActiveTable::find()->select('*')
                ->where(['origin_id'=>$originId,'type'=>1 ,'deleted'=>2])->all();
        }elseif($resourceType == 'match'){
            $scheduled_begin_at = $a =  date("Y-m-d H:i:s",time());
            $standardData = $standardDataActiveTable::find()->select('*')
                ->where(['origin_id'=>$originId ,'deleted'=>2])->andWhere(['>=','scheduled_begin_at',$scheduled_begin_at])->all();
        }else{
            $standardData = $standardDataActiveTable::find()->select('*')
                ->where(['origin_id'=>$originId,'deleted'=>2])->all();
        }
        $newDiff = [];
        $standardIdentityId = array_column($standardData,'rel_identity_id','id');
        foreach ($standardIdentityId as $key=>$value){
            $action = $action."/".$value;
            $curlData = BayesBase::getCurlByParams($action,[]);
            $outputMatchId = array_column($curlData,'id');
        }
        $diffId=array_diff($standardIdentityId,$outputMatchId);
        if (!empty($diffId)) {
            $newDiff[] = $diffId;
        }
        $newnewDiff = [];
        foreach ($newDiff as $key => $val) {
            foreach ($val as $k => $v){
                $newnewDiff[] = $v;
                if ($model  == 'StandardDataMetadata') {
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'metadata_type' => $resourceType ,'origin_id'=>$originId ,'deleted'=>2])->asArray()->one();

                }elseif($model  == 'StandardDataTournament'){
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'origin_id'=>$originId,'type'=>1 ,'deleted'=>2])->asArray()->one();

                }else{
                    $standardDataMatchOne = $standardDataActiveTable::find()->where(['rel_identity_id'=>$v ,'origin_id'=>$originId ,'deleted'=>2])->asArray()->one();

                }

                $transInfo = [
                    'deleted'=>1
                ];
                $game['id'] = $standardDataMatchOne['game_id'];
                $diffInfo = OriginRunBase::setStandardData($originId, $resourceType, $game['id'], $v, $transInfo);

                if ($diffInfo['changed']) {
                    $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                        QueueServer::QUEUE_ORIGIN_PANDASCORE,
                        $resourceType, //修改类型
                        $diffInfo['change_type'],
                        $game['id']);  //game_id 修改
                    $item = [
                        "tag" => $tag,
                        "batch_id" => \app\modules\task\services\QueueServer::setBatchId(\app\modules\common\services\Consts::EXECUTE_TYPE_CTB,\app\modules\common\services\Consts::ORIGIN_PANDASCORE,$resourceType),
                        "params" => $diffInfo,
                    ];
                    TaskRunner::addTask($item,1);
                }
            }
        }

        if (!empty($newnewDiff)){
            return implode($newnewDiff,',');

        }
        return '';
    }

    /**
     * @param $typeString
     * @return string
     * 获取bayes round的胜利方式，转换码表id
     */
    public static function getBayesWinReason($typeString)
    {
        if(!$typeString){
            return $typeString;
        }
        $mapping = [
                'BOMB_DEFUSED' => 'bomb_defused',
                'BOMB_EXPLODED' => 'target_bombed',
                'TARGET_SAVED' => 'target_saved',
                'T_TEAM_ELIMINATION' => 'terrorists_win',
                'CT_TEAM_ELIMINATION'=>'cts_win',
                'UNKNOWN' => 'UNKNOWN',
        ];
        if (!isset($mapping[$typeString])) {
            return $typeString;
        }
        return $mapping[$typeString];
    }

    public static function getCsgoWeaponKindByKind($kind)
    {
        $mapping = [
                1 => 'primary',
                2 => 'secondary',
                3 => 'knife',
                4 => 'grenade',
                5 => 'gear'
        ];
        if (!isset($mapping[$kind])) {
            throw new \yii\base\Exception("不存在");
        }
        return $mapping[$kind];
    }

    public static function getSocketSubType($originType,$socketTypeString){
        $mapping = [
            Consts::ORIGIN_BAYES => [
                'live_data_riot' => 'LOL_RIOT_WEB_LIVESTATS',
                'live_data_carp' => 'DOTA2_BAYES_CARP',
            ],
            Consts::ORIGIN_BAYES2 => [
                'live_data_riot' => 'LOL_RIOT_WEB_LIVESTATS',
                'live_data_carp' => 'DOTA2_BAYES_CARP',
            ],
        ];
        return @$mapping[$originType][$socketTypeString];
    }

    public static function getSocketSubTypeByOriginId($originId,$socketTypeString){
        $mapping = [
            9 => [
                'live_data_riot' => 'LOL_RIOT_WEB_LIVESTATS',
                'live_data_carp' => 'DOTA2_BAYES_CARP',
            ],
            10 => [
                'live_data_riot' => 'LOL_RIOT_WEB_LIVESTATS',
                'live_data_carp' => 'DOTA2_BAYES_CARP',
            ],
        ];
        return @$mapping[$originId][$socketTypeString];
    }

    /**
     * 是否是中文
     * @param $params
     * @return bool
     */
    public static function isChinese($params)
    {
        if (preg_match("/^[\x7f-\xff]+$/", $params)) { //兼容gb2312,utf-8
            return true;
        } else {
            return false;
        }
    }
    public static function radarpurpleTransImageUrl($url)
    {
        $array_value = explode("/",$url);
        $pop = array_pop($array_value);
        $popUrl = urlencode($pop);
        array_push($array_value,$popUrl);
        $result = implode('/',$array_value);
        return $result;
    }

}
