<?php
/**
 *
 */

namespace app\modules\task\services;


use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelationExpect;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\data\services\binding\BindingTournamentService;
use app\modules\match\services\MatchService;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\services\PlayerService;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\OriginMetadataRunBase;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\tournament\services\TournamentService;

class ContrastService
{
    public static function getDetail($params)
    {
        $originType = $params['origin_type'];
        $resourceType = $params['resource_type'];
        $gameId = $params['game_type'];
        $relId = $params['rel_id'];
        if (empty($originType) || empty($resourceType) || empty($gameId) || empty($relId)){
            return [
                'stardard' => null,
                'curl' => null
            ];
        }
        $originInfo = Common::getOriginInfoByType($originType);
        $class=self::getServiceByType($resourceType);
        $standardInfo = $class::getInfoFromStandard($relId, $originInfo['id'], $resourceType, $gameId);
        if($resourceType == Consts::METADATA_TYPE_DOTA2_TALENT){
            $resourceType = Consts::METADATA_TYPE_DOTA2_ABILITY;
        }
        $hotInfo = $class::getOriginInfo($relId, $originInfo['id'], $resourceType, $gameId);


        return [
            'stardard' => $standardInfo,
//            'task' => $infoFromTaskInfo,
            'curl' => $hotInfo,
        ];
    }
    public static function getMasterData($params)
    {
        $originId = $params['origin_id'];
        $resourceType = $params['resource_type'];
        $gameId = $params['game_id'];
        $relId = $params['rel_id'];
        $class=self::getServiceByType($resourceType);
        $standardClass = StandardDataService::getStandardActiveTable($resourceType);
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            $standardId = $standardClass::find()->select('id')->where(['rel_identity_id' => $relId])
                ->andWhere(['game_id' => $gameId])->andWhere(['origin_id' => $originId])->andWhere(['type'=>1])->asArray()->one();
        }else{
            $standardId = $standardClass::find()->select('id')->where(['rel_identity_id' => $relId])
                ->andWhere(['game_id' => $gameId])->andWhere(['origin_id' => $originId])->asArray()->one();
        }
        if($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
            //递归获取子赛事
            $one = StandardDataTournament::find()->where(['and', ['id' => $standardId]])->one();
            if(is_numeric($one['rel_identity_id'])){  //应对玩家接口给的id不是int类型
                $sons = TournamentService::getTreeTournament($one['rel_identity_id'], $one['origin_id']);
                $standardInfo = $one->toArray();
                $standardInfo['son'] = $sons;
            }else{
                $standardInfo = $one->toArray();
            }
        }else{
            $standardInfo = $class::getInfoFromStandard($relId, $originId, $resourceType, $gameId);
        }
//        if($resourceType == Consts::METADATA_TYPE_DOTA2_TALENT){
//            $resourceType = Consts::METADATA_TYPE_DOTA2_ABILITY;
//        }
        $hotInfo = $class::getOriginInfo($relId, $originId, $resourceType, $gameId);

        $classNameMaster = MasterDataService::getMasterActiveTable($resourceType); //主表
        //有关联关系的情况
        $masterId = HotBase::getMainIdByRelIdentityId($resourceType,$relId,$originId,$gameId);
        //有推荐绑定关系
        $masterExpectId = HotBase::getMainIdExpectByRelIdentityId($resourceType,$relId,$originId);
        if($masterId){   //正式表有绑定关系
            if($resourceType == "team"){  //正式表绑定的信息
                $obj = TeamService::getTeam($masterId);
            }elseif($resourceType == "player"){
                $obj = PlayerService::getPlayer($masterId);
            }elseif ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
                $obj = TournamentService::getTournament($masterId);
            }elseif ($resourceType == Consts::RESOURCE_TYPE_MATCH){
                $obj = MatchService::getMatch($masterId);
            }
            else{
                $obj = $classNameMaster::find()->where(['id' => $masterId])->one();
            }
            return [
                'stardard' => $standardInfo,
                'curl' => $hotInfo,
                'master' => $obj
            ];
        }elseif ($masterExpectId){  //有推荐绑定关系
            $bindingExpectInfo = '';
            if($resourceType == Consts::RESOURCE_TYPE_PLAYER){
                $bindingExpectInfo = BindingPlayerService::getTeamBindingExceptInfo($standardId['id']);

            }elseif($resourceType == Consts::RESOURCE_TYPE_TEAM){
                $bindingExpectInfo = BindingTeamService::getTeamBindingExceptInfo($standardInfo['id']);
            }elseif($resourceType == Consts::RESOURCE_TYPE_MATCH){
                $bindingExpectInfo = BindingMatchService::getBindingExceptInfo($standardId['id']);
            }elseif($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT){
                $bindingExpectInfo = BindingTournamentService::getTournamentBindingExceptInfo($standardId['id']);
            }else{
                $masterTableName = $classNameMaster::tableName();
                $q = DataStandardMasterRelationExpect::find()->select('m.*')->alias('expect')->innerJoin($masterTableName.' as m','expect.master_id =m.id and expect.resource_type = '.'"'.$resourceType.'"')
                    ->where(['standard_id'=>$standardInfo['id']]);
                $sql=$q->createCommand()->getRawSql();
                $bindingExpectInfo = $q->asArray()->all();
            }

            return [
                'stardard' => $standardInfo,
                'curl' => $hotInfo,
                'master' => $bindingExpectInfo
            ];
        }else{
            return [
                'stardard' => $standardInfo,
                'curl' => $hotInfo,
            ];
        }

    }

    public static function refresh($params)
    {
        // 1.删除缓存数据，2添加并执行抓取任务
//        OriginRunBase::getOriginInfo($relId,$originInfo['id'],$resourceType,$gameId);
    }

    public static function getServiceByType($type)
    {
        $metadataType = [
            Consts::METADATA_TYPE_LOL_ABILITY => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_LOL_CHAMPION => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_LOL_ITEM => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_LOL_RUNE => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_DOTA2_ABILITY => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_DOTA2_HERO => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_DOTA2_ITEM => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_DOTA2_TALENT => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_CSGO_MAP => OriginMetadataRunBase::class,
            Consts::METADATA_TYPE_CSGO_WEAPON => OriginMetadataRunBase::class,
            Consts::TOURNAMENT_GROUP => OriginRunBase::class,
            Consts::TOURNAMENT_STAGE => OriginRunBase::class,
            Consts::RESOURCE_TYPE_TEAM => OriginRunBase::class,
            Consts::RESOURCE_TYPE_MATCH => OriginRunBase::class,
            Consts::RESOURCE_TYPE_PLAYER => OriginRunBase::class,
            Consts::RESOURCE_TYPE_TOURNAMENT => OriginRunBase::class,
        ];
        return $metadataType[$type];
    }
}