<?php
/**
 *
 */

namespace app\modules\task\services;

use app\modules\common\services\Consts;
use app\modules\data\models\StandardDataMetadata;
use app\modules\match\models\Match;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\org\models\Clan;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\tournament\models\Tournament;
use yii\base\Exception;

class MasterDataService
{
    const METADATA_TYPE_TO_CLASS = [
        Consts::RESOURCE_TYPE_MATCH => Match::class,
        Consts::RESOURCE_TYPE_TOURNAMENT => Tournament::class,
        Consts::RESOURCE_TYPE_PLAYER => Player::class,
        Consts::RESOURCE_TYPE_TEAM => Team::class,
        Consts::RESOURCE_TYPE_CLAN=> Clan::class,
        Consts::METADATA_TYPE_DOTA2_HERO => MetadataDota2Hero::class,
        Consts::METADATA_TYPE_DOTA2_ITEM => MetadataDota2Item::class,
        Consts::METADATA_TYPE_DOTA2_ABILITY => MetadataDota2Ability::class,
        Consts::METADATA_TYPE_DOTA2_TALENT => MetadataDota2Talent::class,
        Consts::METADATA_TYPE_LOL_CHAMPION => MetadataLolChampion::class,
        Consts::METADATA_TYPE_LOL_ABILITY => MetadataLolAbility::class,
        Consts::METADATA_TYPE_LOL_ITEM => MetadataLolItem::class,
        Consts::METADATA_TYPE_LOL_RUNE => MetadataLolRune::class,
        Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => MetadataLolSummonerSpell::class,
        Consts::METADATA_TYPE_CSGO_MAP => MetadataCsgoMap::class,
        Consts::METADATA_TYPE_CSGO_WEAPON => MetadataCsgoWeapon::class,

    ];

    /**
     * @param $resourceType
     * @return \yii\db\ActiveRecord
     * @throws Exception
     */
    public static function getMasterActiveTable($resourceType)
    {
        $mapInfo = self::METADATA_TYPE_TO_CLASS;
        if (isset($mapInfo[$resourceType])) {
            return $mapInfo[$resourceType];
        }
        throw new Exception('获取对象失败:getMasterActiveTable');
    }
}