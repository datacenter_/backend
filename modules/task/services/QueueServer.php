<?php
/**
 *
 */

namespace app\modules\task\services;

class QueueServer
{
    const QUEUE_TYPE_ADD = "add";
    const QUEUE_TYPE_CHANGE = "change";
    const QUEUE_TYPE_UPDATE = "update";
    const CHANGE_TYPE_EQUAL = "equal";
    const CHANGE_TYPE_DELETE = "delete";
    const CHANGE_TYPE_RECOVERY = "recovery";

    const QUEUE_MAJOR_GRAB = "grab"; // 抓取任务
    const QUEUE_MAJOR_ORIGIN_INCREMENT = "origin_increment"; // 源增量
    const QUEUE_MAJOR_STANDARD_INCREMENT = "standard_increment"; // 标准表增量
    const QUEUE_MAJOR_MAIN_INCREMENT = "main_increment"; // 主表增量
    const QUEUE_REFRESH_ES_API = "refresh_es_api"; // 结束完了刷高宇

    const QUEUE_MAJOR_OPERATE_CONFIG_REFRESH = "operate_config_refresh"; // 批量更新操作
    const QUEUE_MAJOR_OPERATE_HOT_REFRESH = 'hot_refresh';   //热数据更新
    const QUEUE_MAJOR_OPERATE_HOT_REFRESH_WS = 'hot_refresh_ws';   //热数据更新
    const QUEUE_ORIGIN_LOG_FORMAT = 'log_format'; // log格式化
    const QUEUE_ORIGIN_MESSAGE_DING_DING = 'message_ding_ding'; // 发送钉钉消息
    const QUEUE_MAJOR_CRONTAB = 'crontab'; // 计划任务
    const QUEUE_MAJOR_ES_HOT_REFRESH = 'es_hot_refresh'; // es增量
    const QUEUE_MAJOR_HOT_REFRESH_MATCH_5E = 'hot_refresh_match_5e'; // 5e match 比赛统计数据
    const QUEUE_MAJOR_HOT_REFRESH_TOURNAMENT_5E = 'hot_refresh_tournament_5e'; // 5e tournament 赛事统计数据
    const QUEUE_MAJOR_API_DATA_SYNC = 'queue_major_api_data_sync'; // api新加坡数据同步

    const QUEUE_ORIGIN_MATCH_FRAMES = 'log_add';
    const QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA = 'websocket_match_data';  //ws实时比赛数据
    const QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI = 'websocket_match_data_api';  //ws实时比赛数据
    const QUEUE_ORIGIN_EVENT_WS = "event_ws"; //ws实时更新比赛event
    const QUEUE_ORIGIN_EVENT_WS_TO_API = "event_ws_to_api"; //ws实时更新比赛event
    const QUEUE_ORIGIN_WS_TO_API = "ws_to_api"; //ws实时更新比赛信息
    const QUEUE_REFRESH_MATCH_SOCKET = "refresh_match_socket"; //重刷比赛 socket
    const QUEUE_ABROAD = "abroad"; //国外队列
    const QUEUE_REFRESH_HK = "refresh_hk";

    const QUEUE_ORIGIN_FEIJING = "feijing";
    const QUEUE_ORIGIN_PANDASCORE = "pandascore";
    const QUEUE_ORIGIN_ABIOS = "abios";
    const QUEUE_ORIGIN_RIOTGAMES = "riotgames";
    const QUEUE_ORIGIN_ORANGE = "orange";
    const QUEUE_ORIGIN_FIVE = "five";
    const QUEUE_ORIGIN_VALVE = "valve";
    const ORIGIN_BAYES = "bayes";
    const ORIGIN_BAYES2 = "bayesplus";
    const QUEUE_ORIGIN_FIVE_EXT_5E = "5eplay";
    const QUEUE_ORIGIN_FIVE_EXT_FUNSPARK = "funspark";

    const QUEUE_RESOURCE_TEAM = "team";
    // pandascore 分组
    const QUEUE_RESOURCE_TOURNAMENT_GROUP = "substage";
    const QUEUE_RESOURCE_HERO = "hero";
    const QUEUE_RESOURCE_PROP = "prop";
    const QUEUE_RESOURCE_SKILL = "skill";
    const QUEUE_RESOURCE_RUNE = "rune";
    const QUEUE_RESOURCE_SERIES = "series";
    const QUEUE_RESOURCE_TALENT = "talent";
    const QUEUE_RESOURCE_TOURNAMENT = "tournament";
    // abios 阶段
    const QUEUE_RESOURCE_AB_STAGE = "stage";
    const QUEUE_RESOURCE_SUBSTAGE = "substage";
    const QUEUE_RESOURCE_TOURNAMENT_TEAM_RELATION = "tournament_team_relation";
    const QUEUE_RESOURCE_PLAYER = "player";
    const QUEUE_RESOURCE_PLAYER_TEAM = "player_team_relation";
    // abios 赛事战队关系
    const QUEUE_RESOURCE_TOURNAMENT_TEAM = "tournament_team_relation";
    const QUEUE_RESOURCE_GROUP_TEAM = "group_team_relation";
    const QUEUE_RESOURCE_MATCH = "match";
    const QUEUE_RESOURCE_BATTLE = "battle";
    const QUEUE_RESOURCE_METADATA = "metadata";
    const QUEUE_RESOURCE_TO_DO = "data_resource_to_do";


    public static function getTag($major, $originType, $resourceType, $changeType, $extType = "", $ext2Type = "")
    {
        return sprintf("%s.%s.%s.%s.%s.%s", $major, $originType, $resourceType, $changeType, $extType,$ext2Type);
    }

    public static function analyseTag($tag)
    {
        $info = explode(".", $tag);
        return [
            "major" => $info[0],
            "origin_type" => $info[1],
            "resource_type" => $info[2],
            "change_type" => $info[3],
            "ext_type" => $info[4],
            "ext2_type" => @$info[5],
        ];
    }

    /**
     * @param $executeType
     * @param $resourceType
     * @param $origin
     * @param $extType
     * @return string
     */
    public static function setBatchId($executeType,$origin = "",$resourceType ="",$extType ="")
    {
        return sprintf("%s_%s_%s_%s_%s", $executeType, $origin,$resourceType,$extType, date("YmdHis"));
    }
    public static function getBatchId($value)
    {
        $info = explode("_", $value);
        return [
            "execute_type" => $info[0],
            "origin" => $info[1],
            "resource_type" => $info[2],
            "ext_type" => $info[3],
            "time" => $info[4],
        ];
    }

}
