<?php
/**
 *
 */

namespace app\modules\task\services;


use app\modules\common\services\Consts;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\grab\pandascore\PandascoreCSMapsList;
use app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList;
use app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList;
use app\modules\task\services\grab\pandascore\PandascoreDotaHeroList;
use app\modules\task\services\grab\pandascore\PandascoreDotaItemList;
use app\modules\task\services\grab\pandascore\PandascoreLOLHeroList;
use app\modules\task\services\grab\pandascore\PandascoreLOLItemList;
use app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList;
use app\modules\task\services\grab\pandascore\PandascoreMatchesList;
use app\modules\task\services\grab\pandascore\PandascorePlayerList;
use app\modules\task\services\grab\pandascore\PandascoreTeamList;
use app\modules\task\services\grab\pandascore\PandascoreTournamentList;
use app\modules\task\services\grab\riotgames\RiotAbility;
use app\modules\task\services\grab\riotgames\RiotChampions;
use app\modules\task\services\grab\riotgames\RiotItems;
use app\modules\task\services\grab\riotgames\RiotRunesReforged;
use app\modules\task\services\grab\riotgames\RiotSummoner;
use app\modules\task\services\grab\valve\ValueDotaItemList;
use app\modules\task\services\grab\valve\ValveDotaHeroList;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\BusinessException;

class RefreshResource
{
    public static function refreshByResourceId($resourceId, $resourceType, $originId, $gameId, $extType = "refresh")
    {
        $stageInfo = self::getRefreshStageInfo($resourceId, $resourceType, $originId, $gameId, $extType);
        $taskInfo =  TaskRunner::addTask($stageInfo, 3);
        $doneinio =TaskInfo::find()->where(['id'=>$taskInfo['id']])->asArray()->one();
        if($originId == 3){  //批量刷注释掉这个
            $re = json_decode($doneinio['response'],true);
            if(empty($re) || count($re) <= 0){
                $standardDataActiveTable = OriginRunBase::getStandardActiveTable($resourceType);
                $standardInfo = $standardDataActiveTable::find()->where([
                    'origin_id' => $originId,
                    'rel_identity_id' => $resourceId,
                    'game_id' => $gameId,
                ])->one();
                if($standardInfo){
                    $deletedIds = Common::checkGrebIsDel($resourceType,$originId,'one',$resourceId);
                    if($deletedIds){
                        if(in_array($resourceId,$deletedIds)){
                            throw new BusinessException([],'该Id'.$resourceId.'已被数据源删除');
                        }
                    }
                }
            }
        }
        if($doneinio){
            return $doneinio;
        }
    }

    public static function getRefreshStageInfo($resourceId, $resourceType, $originId, $gameId, $extType ="",$reason = "refresh", $response=null,$batchId="")
    {
        if(!$batchId){
            $batchId = date("YmdHis");
        }
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", $extType, $reason);
        $taskInfo = [];
        switch ($originId)
        {
            case 2:
                switch ($resourceType)
                {
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "tag"=> QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", $extType, "history_players"),
                            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTeamList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TEAM),
                            "params"=>[
                                "action"=>"/teams",
                                "params"=>[
                                    "filter"=>"id=".$resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_PLAYER:
                        $taskInfo = [
                            "tag"=>$tag,
                            "type"=>\app\modules\task\services\grab\abios\v3\AbiosPlayerList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_PLAYER),
                            "params"=>[
                                "action"=>"/players",
                                "params"=>[
                                    "filter"=>"id=".$resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TOURNAMENT:
                        $taskInfo = [
                            "tag"=>$tag,
                            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TOURNAMENT),
                            "params"=>[
                                "action"=>"/tournaments",
                                "params"=>[
                                    "filter"=>"id=".$resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosDotaItemList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=1,category=hero,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_DOTA2_ITEM:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosDotaItemList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=1,category=item,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_DOTA2_TALENT:
                    case Consts::METADATA_TYPE_DOTA2_ABILITY:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosDotaItemList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=1,category=spell,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_LOL_ITEM:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosLOLItemList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=2,category=item,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_LOL_CHAMPION:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosLOLItemList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=2,category=champion,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_CSGO_WEAPON:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosCSWeaponsList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/assets",
                                "params" => [
                                    "filter" => "game.id=5,category=weapon,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;

                    case Consts::RESOURCE_TYPE_MATCH:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/series",
                                "params" => [
                                    "filter" => "id=".$resourceId
                                ]
                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_CSGO_MAP:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\abios\v3\AbiosCSMapList::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT, Consts::ORIGIN_ABIOS, $resourceType),
                            "params" => [
                                "action" => "/maps",
                                "params" => [
                                    "filter" => "game.id=5,id=" . $resourceId
                                ]
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 3:
                switch ($resourceType)
                {
                    //csgo武器
                    case Consts::METADATA_TYPE_CSGO_WEAPON:
                        $taskInfo = [
                            "type" => PandascoreCSWeaponsList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/csgo/weapons',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;

                    //csgo地图
                    case Consts::METADATA_TYPE_CSGO_MAP:
                        $taskInfo = [
                            "type" => PandascoreCSMapsList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/csgo/maps',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;

                    //lol道具
                    case Consts::METADATA_TYPE_LOL_ITEM:
                        $taskInfo = [
                            "type" => PandascoreLOLItemList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/lol/versions/all/items',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;

                    //lol召唤师技能
                    case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                        $taskInfo = [
                            "type" => PandascoreLOLSummonerSpellList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/lol/spells',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;

                    //lol英雄
                    case Consts::METADATA_TYPE_LOL_CHAMPION:
                        $taskInfo = [
                            "type" => PandascoreLOLHeroList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/lol/versions/all/champions',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;
                    //dota2道具
                    case Consts::METADATA_TYPE_DOTA2_ITEM:
                        $taskInfo = [
                            "type" => PandascoreDotaItemList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/dota2/items',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;
                    //dota2英雄
                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        $taskInfo = [
                            "type" => PandascoreDotaHeroList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/dota2/heroes',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;
                    //dota2技能,天赋
                    case Consts::METADATA_TYPE_DOTA2_ABILITY:
                    case Consts::METADATA_TYPE_DOTA2_TALENT:
                        $taskInfo = [
                            "type" => PandascoreDotaAbilitiesList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/dota2/abilities',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_PLAYER:
                        $taskInfo = [
                            "type" => PandascorePlayerList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
//                                'response'=>$response,
                                'action'=>'/players',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_MATCH:
                        $taskInfo = [
                            "type" => PandascoreMatchesList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
//                                'response'=>$response,
                                'action'=>'/matches',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],

                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TOURNAMENT:
                        $taskInfo = [
                            "type" => PandascoreTournamentList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
//                                'response'=>$response,
                                'action'=>'/series',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "type" => PandascoreTeamList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
//                                'response'=>$response,
                                'action'=>'/teams',
                                'params'=>[
                                    'filter[id]' => $resourceId,
                                    'page'=>1,
                                ],
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 4:
                switch ($resourceType)
                {
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "", "", "", ""),
                            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_TEAM),
                            "params" => [
                                "action" => "/lol/event/list",
                                'filter[team]' => $resourceId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_PLAYER:
                        $taskInfo = [
                            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "", "", "", ""),
                            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_PLAYER),
                            "params" => [
                                "action" => "/lol/event/list",
                                'filter[player]' => $resourceId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_MATCH:
                        $taskInfo = [
                            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "", "", "", ""),
                            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_MATCH),
                            "params" => [
                                "action" => "/lol/event/list",
                                'filter[match]' => $resourceId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TOURNAMENT:
                        $taskInfo = [
                            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "", "", "", ""),
                            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_TOURNAMENT),
                            "params" => [
                                "action" => "/lol/event/list",
                                'filter[tournament]' => $resourceId,
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 7 :
                    $taskInfo = [
                            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "","","",""),
                            "type" => \app\modules\task\services\grab\hltv\HltvIncrementDispatcher::class,
                            "batch_id" => date("YmdHis"),
                            "params" =>[
                                "url" => env('HLTVREFRESH'),
                                "params" => [
                                    "resource_type" => $resourceType,
                                    "rel_identity_id" => $resourceId
                                ]
                            ],
                    ];
                break;
            case 8 :
                switch ($resourceType)
                {
                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        $taskInfo = [
                            "type" => ValveDotaHeroList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/IEconDOTA2_570/GetHeroes/v1',
                                'params'=>[
                                    "language"=>"zh,en",
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;
                    case Consts::METADATA_TYPE_DOTA2_ITEM:
                        $taskInfo = [
                            "type" => ValueDotaItemList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'/IEconDOTA2_570/GetGameItems/v1',
                                'params'=>[
                                    "language"=>"zh,en",
                                    'id' => $resourceId,
                                ],
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
            break;
            case 6 :
                switch ($resourceType)
                {
                    case Consts::METADATA_TYPE_LOL_ABILITY:
                        $taskInfo = [
                            "type" => RiotAbility::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'ability',
                                'params'=>[
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_LOL_CHAMPION:
                        $taskInfo = [
                            "type" => RiotChampions::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'champion',
                                'params'=>[
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_LOL_ITEM:
                        $taskInfo = [
                            "type" => RiotItems::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'item',
                                'params'=>[
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_LOL_RUNE:
                        $taskInfo = [
                            "type" => RiotRunesReforged::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'',
                                'params'=>[
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                        $taskInfo = [
                            "type" => RiotSummoner::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'response'=>$response,
                                'action'=>'summoner',
                                'params'=>[
                                    'id' => $resourceId,
                                ],

                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 9:
                switch ($resourceType)
                {
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","refresh","one_refresh"),
                            "type"=>\app\modules\task\services\grab\bayes\BayesTeam::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM),
                            "params"=>[
                                "action" => "/team/".$resourceId,
                                "game_id"=> $gameId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_PLAYER:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","refresh"),
                            "type"=>\app\modules\task\services\grab\bayes\BayesPlayer::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
                            'params' => [
                                "action" => "/player/".$resourceId,
                                "game_id"=> $gameId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_MATCH:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","refresh"),
                            "type"=>\app\modules\task\services\grab\bayes\BayesMatch::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
                            "params"=>[
                                "action" => "/match/".$resourceId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TOURNAMENT:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"","refresh"),
                            "type"=>\app\modules\task\services\grab\bayes\BayesTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
                            "params"=>[
                                "action" => "/tournament/".$resourceId,
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 10:
                switch ($resourceType)
                {
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM,"","refresh","one_refresh"),
                            "type"=>\app\modules\task\services\grab\bayes2\BayesTeam::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM),
                            "params"=>[
                                "action" => "/team/".$resourceId,
                                "game_id"=> $gameId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_PLAYER:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER,"","refresh","one_refresh"),
                            "type"=>\app\modules\task\services\grab\bayes2\BayesPlayer::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER),
                            'params' => [
                                "action" => "/player/".$resourceId,
                                "game_id"=> $gameId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_MATCH:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"","refresh"),
                            "type"=>\app\modules\task\services\grab\bayes2\BayesMatch::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
                            "params"=>[
                                "action" => "/match/".$resourceId,
                            ],
                        ];
                        break;
                    case Consts::RESOURCE_TYPE_TOURNAMENT:
                        $taskInfo = [
                            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","refresh"),
                            "type"=>\app\modules\task\services\grab\bayes2\BayesTournament::class,
                            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT),
                            "params"=>[
                                "action" => "/tournament/".$resourceId,
                            ],
                        ];
                        break;
                    default:
                        throw new BusinessException([], '该数据源没有此数据类型');
                }
                break;
            case 11:
                switch ($resourceType)
                {
                    case Consts::RESOURCE_TYPE_TEAM:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\radarpurple\RadarPurpleTeam::class,
                            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_RADAR_PURPLE,Consts::RESOURCE_TYPE_TEAM),
                            "params" => [
                                'type' => "team",
                                'offset' => "0",
                                'limit' => "1",
                                'where'=>" where `team_id` = '{$resourceId}'"
                            ],
                        ];
                        break;

                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        $taskInfo = [
                            "tag" => $tag,
                            "type" => \app\modules\task\services\grab\radarpurple\RadarPurpleDota2Hero::class,
                            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_RADAR_PURPLE,Consts::METADATA_TYPE_DOTA2_HERO),
                            "params" => [
                                'type' => $resourceType,
                                'offset' => "0",
                                'limit' => "1",
                                'where'=>" where `hero_id` = '{$resourceId}'"
                            ],
                        ];
                        break;

                }
                break;
            default:
                throw new BusinessException([], '该数据源没有此数据类型');
        }
        return $taskInfo;
    }
}