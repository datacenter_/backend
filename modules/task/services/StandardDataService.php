<?php
/**
 *
 */

namespace app\modules\task\services;


use app\modules\common\services\Consts;
use app\modules\data\models\StandardDataMetadata;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\task\models\StandardDataGroup;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataStage;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use yii\base\Exception;

class StandardDataService
{
    const METADATA_TYPE_TO_CLASS = [
        Consts::METADATA_TYPE_LOL_ABILITY => StandardDataMetadata::class,
        Consts::METADATA_TYPE_LOL_CHAMPION => StandardDataMetadata::class,
        Consts::METADATA_TYPE_LOL_ITEM => StandardDataMetadata::class,
        Consts::METADATA_TYPE_LOL_RUNE => StandardDataMetadata::class,
        Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => StandardDataMetadata::class,
        Consts::METADATA_TYPE_DOTA2_ABILITY => StandardDataMetadata::class,
        Consts::METADATA_TYPE_DOTA2_HERO => StandardDataMetadata::class,
        Consts::METADATA_TYPE_DOTA2_ITEM => StandardDataMetadata::class,
        Consts::METADATA_TYPE_DOTA2_TALENT => StandardDataMetadata::class,
        Consts::METADATA_TYPE_CSGO_WEAPON => StandardDataMetadata::class,
        Consts::METADATA_TYPE_CSGO_MAP => StandardDataMetadata::class,
        Consts::TOURNAMENT_GROUP => StandardDataGroup::class,
        Consts::TOURNAMENT_STAGE => StandardDataStage::class,
        Consts::RESOURCE_TYPE_TEAM => StandardDataTeam::class,
        Consts::RESOURCE_TYPE_MATCH => StandardDataMatch::class,
        Consts::RESOURCE_TYPE_PLAYER => StandardDataPlayer::class,
        Consts::RESOURCE_TYPE_TOURNAMENT => StandardDataTournament::class,
        Consts::RESOURCE_TYPE_SON_TOURNAMENT => StandardDataTournament::class,
        Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT => StandardDataTournament::class,
    ];

    /**
     * @param $resourceType
     * @return \yii\db\ActiveRecord
     * @throws Exception
     */
    public static function getStandardActiveTable($resourceType)
    {
        $mapInfo = self::METADATA_TYPE_TO_CLASS;
        if (isset($mapInfo[$resourceType])) {
            return $mapInfo[$resourceType];
        }
        throw new Exception('获取对象失败:getStandardActiveTable');
    }
}