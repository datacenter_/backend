<?php
/**
 *
 */

namespace app\modules\task\services;


interface TaskCatchInterface
{
    public static function getResponse($taskInfo);
}