<?php
/**
 *
 */

namespace app\modules\task\services;


use app\modules\common\services\Consts;
use app\modules\common\services\DbHelper;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\grab\riotgames\RiotAbility;
use app\modules\task\services\grab\riotgames\RiotChampions;
use app\modules\task\services\grab\riotgames\RiotItems;
use app\modules\task\services\grab\riotgames\RiotRunesReforged;
use app\modules\task\services\grab\riotgames\RiotSummoner;
use app\modules\task\services\grab\valve\ValueDotaItemList;
use app\modules\task\services\grab\valve\ValveDotaHeroList;
use app\rest\exceptions\BusinessException;

class TaskInfoService
{
    public static function getList($params)
    {
        $whereConfig=[
            "batch_id"=>[
                'type'=>'=',
                'key'=>'task_info.batch_id'
            ],
            "type"=>[
                'type'=>'=',
                'key'=>'task_info.type'
            ],
            "tag"=>[
                'type'=>'=',
                'key'=>'task_info.tag'
            ],
            "status"=>[
                'type'=>'=',
                'key'=>'task_info.status'
            ],
        ];
        $whereArray=DbHelper::getWhere($whereConfig,$params);
        $where=array_merge(['and'],$whereArray);
        $q = TaskInfo::find();
        $q->where($where);
        $count = $q->count();
        $page=(isset($params['page'])&&$params['page'])?$params['page']:1;
        $perPage=(isset($params['per_page'])&&$params['per_page'])?$params['per_page']:10;
        $list = $q->orderBy("id desc")->limit($perPage)->offset($perPage*($page-1))->asArray()->all();
        return [
            'total'=>$count,
            'list'=>$list,
        ];
    }

    public static function detail($id)
    {
        return TaskInfo::find()->where(['id'=>$id])->asArray()->all();
    }

    public static function reAdd($id)
    {
        $info=self::detail();
        unset($info['id']);
        $task=new TaskInfo();
        $task->setAttributes($info);
        if(!$task->save()){
            throw new BusinessException($task->getErrors(),'添加任务失败');
        }
        return $task->toArray();
    }

    public static function getOriginData($resourceType,$originId,$gameId,$reason)
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", "", $reason);
        $batchId = date("YmdHis");
        $taskInfo = [];
        switch ($originId)
        {
            case 3:
            switch ($resourceType)
            {
                //csgo武器
                case Consts::METADATA_TYPE_CSGO_WEAPON:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
                        "tag" => $tag,
                        "batch_id" =>$batchId,
                        "params" => [
                            'action'=>'/csgo/weapons',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;
                //csgo地图
                case Consts::METADATA_TYPE_CSGO_MAP:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreCSMapsList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/csgo/maps',
                            'params'=>[
                                'page'=>1,
                            ],

                        ],
                    ];
                break;
                //lol道具
                case Consts::METADATA_TYPE_LOL_ITEM:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/lol/items',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;
                case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                    //lol召唤师技能
                    $taskInfo = [
                        "tag" => $tag,
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
                        "batch_id" => $batchId,
                        "params" => [
                            "action" => "/lol/spells",
                            "params" => [
                                "page" => 1,
                            ]
                        ],
                    ];
                break;
                //lol英雄
                case Consts::METADATA_TYPE_LOL_CHAMPION:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/lol/champions',
                            'params'=>[
                                'page'=>1,
                            ],

                        ],
                    ];
                break;
                //dota2道具
                case Consts::METADATA_TYPE_DOTA2_ITEM:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/dota2/items',
                            'params'=>[
                                'page'=>1,
                            ],

                        ],
                    ];
                break;
                //dota2英雄
                case Consts::METADATA_TYPE_DOTA2_HERO:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/dota2/heroes',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;
                //dota2技能,天赋
                case Consts::METADATA_TYPE_DOTA2_ABILITY:
                case Consts::METADATA_TYPE_DOTA2_TALENT:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/dota2/abilities',
                            'params'=>[
                                'page'=>1,
                            ],

                        ],
                    ];
                break;
                case Consts::RESOURCE_TYPE_PLAYER:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascorePlayerList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/players',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;
                case Consts::RESOURCE_TYPE_TOURNAMENT:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/series',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;
                case Consts::RESOURCE_TYPE_TEAM:
                    $taskInfo = [
                        "type" => \app\modules\task\services\grab\pandascore\PandascoreTeamList::class,
                        "tag" => $tag,
                        "batch_id" => $batchId,
                        "params" => [
                            'action'=>'/teams',
                            'params'=>[
                                'page'=>1,
                            ],
                        ],
                    ];
                break;

                default:
                    throw new BusinessException([], '没有该类型的任务');
                    break;
            }
            break;
            case 8:
                switch ($resourceType)
                {
                    //daota2英雄
                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        $taskInfo = [
                            "type" => ValveDotaHeroList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'/IEconDOTA2_570/GetHeroes/v1',
                                'params'=>[
                                    "language"=>"zh,en",
                                ],

                            ],
                        ];
                        break;
                        //daota2道具
                    case Consts::METADATA_TYPE_DOTA2_ITEM:
                        $taskInfo = [
                            "type" => ValueDotaItemList::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'/IEconDOTA2_570/GetGameItems/v1',
                                'params'=>[
                                    "language"=>"zh,en",
                                ],
                            ],
                        ];
                        break;

                }
                break;
            case 6 :
                switch ($resourceType)
                {
                    //技能
                    case Consts::METADATA_TYPE_LOL_ABILITY:
                        $taskInfo = [
                            "type" => RiotAbility::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'ability',
                                'params'=>[
                                ],

                            ],
                        ];
                        break;
                    //英雄
                    case Consts::METADATA_TYPE_LOL_CHAMPION:
                        $taskInfo = [
                            "type" => RiotChampions::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'champion',
                                'params'=>[
                                ],

                            ],
                        ];
                        break;
                    //道具
                    case Consts::METADATA_TYPE_LOL_ITEM:
                        $taskInfo = [
                            "type" => RiotItems::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'item',
                                'params'=>[
                                ],

                            ],
                        ];
                        break;
                    //符文
                    case Consts::METADATA_TYPE_LOL_RUNE:
                        $taskInfo = [
                            "type" => RiotRunesReforged::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'',
                                'params'=>[
                                ],

                            ],
                        ];
                        break;
                    //召唤师技能
                    case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                        $taskInfo = [
                            "type" => RiotSummoner::class,
                            "tag" => $tag,
                            "batch_id" => $batchId,
                            "params" => [
                                'action'=>'summoner',
                                'params'=>[
                                ],

                            ],
                        ];
                        break;
                }
                break;
        }
        return TaskRunner::addTask($taskInfo, 1);
    }
}