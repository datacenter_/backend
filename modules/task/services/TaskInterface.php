<?php
/**
 *
 */

namespace app\modules\task\services;


interface TaskInterface
{
    public static function run($tag,$taskInfo);
}