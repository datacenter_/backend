<?php
/**
 *
 */

namespace app\modules\task\services;


use WBY\MQ\SDK\RocketMQ\AbstractConsumer;

class TaskMQConsumer extends AbstractConsumer
{
    public function businessProcess($message)
    {
        // 这里是处理逻辑，获取操作对象，处理之
        $infoJson=$message->getMessageBody();
        $tag=$message->getMessageTag();
        $info=json_decode($infoJson,true);
        if(isset($info['id'])&&$info['id']){
            print_r($info['id']);
            TaskRunner::run($info['id']);
        }
    }
}