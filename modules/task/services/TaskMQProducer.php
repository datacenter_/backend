<?php
/**
 *
 */

namespace app\modules\task\services;

use MQ\Model\TopicMessage;
use WBY\MQ\SDK\MQClientFactory;
use WBY\MQ\SDK\RocketMQ\Publisher;

class TaskMQProducer
{
    // 香港生产者
    public static function addQueue($taskInfo,$tag,$startDeliverTime=null)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'business_task';

        $producer = $client->getProducer($instanceId, $topicName);
        $publisher = new Publisher($producer, $tag);
        $messages = [json_encode($taskInfo)];
//        $startDeliverTime = null; //这里不延时
        $qReturn = $publisher->produce($messages, $startDeliverTime);
        $messageId=null;
        foreach ($qReturn as $re) {
            if($re instanceof TopicMessage){
                $messageId=$re->getMessageId();
            }
        }
        return $messageId;
    }
    public static function addPriorityQueue($taskInfo,$tag,$startDeliverTime=null)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'priority_list';

        $producer = $client->getProducer($instanceId, $topicName);
        $publisher = new Publisher($producer, $tag);
        $messages = [json_encode($taskInfo)];
//        $startDeliverTime = null; //这里不延时
        $qReturn = $publisher->produce($messages, $startDeliverTime);
        $messageId=null;
        foreach ($qReturn as $re) {
            if($re instanceof TopicMessage){
                $messageId=$re->getMessageId();
            }
        }
        return $messageId;

    }
    // 新加坡生产者
    public static function addSingaporeDataApiQueue($taskInfo,$tag)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'data_api';

        $producer = $client->getProducer($instanceId, $topicName);
        $publisher = new Publisher($producer, $tag);
        $messages = [json_encode($taskInfo)];
        $startDeliverTime = null; //这里不延时
        $qReturn = $publisher->produce($messages, $startDeliverTime);
        $messageId=null;
        foreach ($qReturn as $re) {
            if($re instanceof TopicMessage){
                $messageId=$re->getMessageId();
            }
        }
        return $messageId;
    }

    public static function addAbroadQueue($taskInfo,$tag)
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'abroad';

        $producer = $client->getProducer($instanceId, $topicName);
        $publisher = new Publisher($producer, $tag);
        $messages = [json_encode($taskInfo)];
        $startDeliverTime = null; //这里不延时
        $qReturn = $publisher->produce($messages, $startDeliverTime);
        $messageId=null;
        foreach ($qReturn as $re) {
            if($re instanceof TopicMessage){
                $messageId=$re->getMessageId();
            }
        }
        return $messageId;

    }

}
