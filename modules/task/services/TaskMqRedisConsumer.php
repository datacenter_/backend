<?php
/**
 *
 */

namespace app\modules\task\services;

class TaskMqRedisConsumer
{
    public function redisConsumer($message){
        // 这里是处理逻辑，获取操作对象，处理之
        $infoJson=$message->getMessageBody();
        $tag=$message->getMessageTag();
        $info=json_decode($infoJson,true);
//        if(isset($info['id'])&&$info['id']){
//            print_r($info['id']);
//            TaskRunner::run($info['id']);
//        }
        $redis = self::getRedis();
        $key='redisQueue_'.$tag;
        //限制本次队列只有一个进程在操作
        self::lockLimit($redis,$key,60*30);
        //获取当前队列长度
        $length = $redis->llen($key);
        for($i=0;$i<$length;$i++){
            try{
                $record = $redis->rpop($key);
                if(!empty($record)){
                    //处理业务逻辑
                    $record_decode=json_decode($record,true);
                    TaskRunner::run($record_decode['id']);
                }
            }catch(\Throwable $e){
//                //判断重试次数，这里设置超过3次重试就不再重试
//                if(!isset($record_decode['try_count']))$record_decode['try_count']=0;
//                if(isset($record_decode['try_count']) && $record_decode['try_count']<3 ){
//                    ++$record_decode['try_count'];
//                    $redis->lpush(self::FUEL_LIST_RECORD_LOG_KEY,json_encode($record_decode));
//                    $redis->expire(self::FUEL_LIST_RECORD_LOG_KEY, 60*60*24);
//                }
            }
        }
        $redis->del($key); //业务逻辑处理完毕，解锁
        $redis->close();
    }
    public static function lockLimit($redis,$key,$expire = 60){
        if(!$key) {
            return false;
        }
        do {
            if($acquired = ($redis->setnx($key, time()))) { // 如果redis不存在，则成功
                $redis->expire($key, $expire);
                break;
            }
            usleep($expire);
        } while (true);
        return true;
    }
    public static function getRedis()
    {
        $redis=\Yii::$app->redis;

        return $redis;
    }
}