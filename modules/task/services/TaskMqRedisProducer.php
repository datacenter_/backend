<?php
/**
 *
 */

namespace app\modules\task\services;

class TaskMqRedisProducer
{
    public static function addQueue($taskInfo,$redis_server)
    {
        $key='redisQueue_'.$redis_server;
        $redis=\Yii::$app->redis;

        $redis->lpush($key,json_encode($taskInfo,JSON_UNESCAPED_UNICODE));
        $redis->expire($key, 60*60*24);
    }
}