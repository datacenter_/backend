<?php
/**
 *
 */

namespace app\modules\task\services;

use app\modules\task\models\TaskInfo;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\HotDispatcher;
use app\modules\task\services\logadd\LivedMatchFramesDispatcher;
use app\modules\task\services\logformat\LogFormatDispatch;
use app\modules\task\services\mainIncrement\MainIncrementDispatcher;
use app\modules\task\services\operation\OperationConfigRefresh;
use app\modules\task\services\originIncrement\OriginIncrementDispatcher;
use app\modules\task\services\standardIncrement\StandardIncrementDispatcher;
use app\modules\task\services\wsdata\WebsocketRestApiDispatcher;
use app\rest\exceptions\RestException;
use yii\db\Exception;
use app\modules\task\services\wsdata\WebsocketDispatcher;
class TaskRedisRunner
{
    const STATUS_PENDING = 1;
    const STATUS_RUNNING = 2;
    const STATUS_SUCCESS = 3;
    const STATUS_FAIL = 4;

    public static function run($taskInfo)
    {
        return true;
        if(!$taskInfo){
            return true;
        }
        $tagInfo=QueueServer::analyseTag($taskInfo["tag"]);  //tag字段处理后的数组
        $major=$tagInfo["major"];
        switch($major){
            case QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA: //ws比赛
                self::runWebsocketMatch($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI: //ws比赛 rest api
                self::runWebsocketMatchRestApi($tagInfo,$taskInfo);
                break;
            default:
                throw new TaskException("no task class for tag: ".$taskInfo["tag"]);
        }
    }


    public static function runWebsocketMatchRestApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = WebsocketRestApiDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }

    public static function getInfoById($id)
    {
        return TaskInfo::find()->where(["id" => $id])->one();
    }

    public static function setTaskBegin($id)
    {
        return TaskInfo::updateAll(["begin_time" => self::getNowTime(), "status" => self::STATUS_RUNNING], ["id" => $id]);
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('Y-m-d H:i:s');
        return $time;
    }

    public static function setTaskSuccess($id)
    {
        return TaskInfo::updateAll(["end_time" => self::getNowTime(), "status" => self::STATUS_SUCCESS], ["id" => $id]);
    }

    public static function setTaskFail($id, $errorInfo)
    {
        return TaskInfo::updateAll(["end_time" => self::getNowTime(), "status" => self::STATUS_FAIL, "error_info" => $errorInfo], ["id" => $id]);
    }

    public static function addTask($taskInfo,$runType=3)
    {
        $task = new TaskInfo();
        // 这里用来处理非正常流程，比如需要同步阻塞执行的流程
        if(isset($taskInfo['run_type'])&&$taskInfo['run_type']){
            $runType=(int)$taskInfo['run_type'];
        }
       if (empty($taskInfo)) {
            return true;
       }

        if (isset($taskInfo["params"])) {
            $taskInfo["params"]= json_encode($taskInfo["params"]) ;
        }

        $taskInfo['run_type']=$runType;
        $task->setAttributes($taskInfo);
        $task->save();
        if($task->getErrors()){
            throw new Exception(json_encode($task->getErrors()));
        }
        $taskId=$task->id;
        // 判断执行类型
        switch ($runType){
            case 1:
                self::addMq($task,$taskInfo['tag']);
                break;
            case 2:
                break;
            case 3:
                self::run($taskId);
            case 9001:
                self::addRedis($task,$taskInfo['redis_server']);
        }
        return $task->toArray();
    }

    private static function addMq($task,$tag)
    {
        $taskInfo=$task->toArray();
        TaskMQProducer::addQueue($taskInfo,$tag);
    }

    private static function addRedis($task,$redis_server){
        $taskInfo=$task->toArray();
        TaskMqRedisProducer::addQueue($taskInfo,$redis_server);
    }

    public static function setTaskResponse($taskId, $response)
    {
        TaskInfo::updateAll(["response" => $response], ["id" => $taskId]);
    }

    public static function setTaskAddsInfo($taskId, $substeps)
    {
        TaskInfo::updateAll(["substeps" => json_encode($substeps)], ["id" => $taskId]);
    }
}