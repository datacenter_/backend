<?php
/**
 *
 */

namespace app\modules\task\services;

use app\modules\common\models\HelperImageToAli;
use app\modules\common\services\CommonLogService;
use app\modules\file\services\UploadService;
use app\modules\org\models\Player;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\esHotRefresh\EsHotRefreshDispatch;
use app\modules\task\services\esHotRefresh\HotRefresh5eDispatch;
use app\modules\task\services\esHotRefresh\HotRefresh5eTournamentDispatch;
use app\modules\task\services\apiDataSync\RefreshDataApi;
use app\modules\task\services\esHotRefresh\RefreshEsApiDispatch;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWsToApi;
use app\modules\task\services\hot\HotDispatcher;
use app\modules\task\services\hot\OrginDispatcher;
use app\modules\task\services\logadd\LivedMatchFramesDispatcher;
use app\modules\task\services\logformat\LogFormatDispatch;
use app\modules\task\services\mainIncrement\MainIncrementDispatcher;
use app\modules\task\services\operation\OperationConfigRefresh;
use app\modules\task\services\originIncrement\OriginIncrementDispatcher;
use app\modules\task\services\standardIncrement\StandardIncrementDispatcher;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketToApi;
use app\modules\task\services\wsdata\RefreshWebsocketApi;
use app\modules\task\services\wsdata\WebsocketRestApiDispatcher;
use app\modules\task\services\wsdata\WebsocketToApiDispatcher;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RestException;
use Gclibs\Redisqueue\QueueManager;
use OSS\OssClient;
use yii\db\Exception;
use app\modules\task\services\wsdata\WebsocketDispatcher;
class TaskRunner
{
    const STATUS_PENDING = 1;
    const STATUS_RUNNING = 2;
    const STATUS_SUCCESS = 3;
    const STATUS_FAIL = 4;
    const STATUS_EMPTY = 5; //请求数据为空
    private static $redisQueueServer;

    public static function run($taskId)
    {
        $taskInfo = self::getInfoById($taskId);   //数据库查出来的数据
        if(!$taskInfo){
            CommonLogService::record('no_task_info',"no task_info class for id: ".$taskId);
            return true;
        }
		if(!in_array($taskInfo['status'],[1,4,5])){
            CommonLogService::record('no_task_status',"no task_status class for id: ".$taskId. 'status:'.$taskInfo['status']);
            return true;
        }
        $tagInfo=QueueServer::analyseTag($taskInfo["tag"]);  //tag字段处理后的数组
        $major=$tagInfo["major"];
        switch($major){
            case QueueServer::QUEUE_MAJOR_GRAB:
                self::runGrab($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT:
                self::runOriginIncrement($tagInfo,$taskInfo);
                break;
                //数据出现增量
            case QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT:
                self::runStandardIncrement($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_MAIN_INCREMENT:
                self::runMainIncrement($tagInfo,$taskInfo);
                break;
                //调用高宇es api
            case QueueServer::QUEUE_REFRESH_ES_API:
                self::runRefreshEsApi($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_OPERATE_CONFIG_REFRESH:
                self::runOperateConfigRefresh($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH:
                self::runOperateHotRefresh($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_MATCH_FRAMES:
                self::runMatchFrameLog($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_ES_HOT_REFRESH:
            case QueueServer::QUEUE_MAJOR_HOT_REFRESH_MATCH_5E:
            case QueueServer::QUEUE_MAJOR_HOT_REFRESH_TOURNAMENT_5E:
            case QueueServer::QUEUE_ORIGIN_LOG_FORMAT:
                self::runCommonSwitch($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA: //ws比赛
                self::runWebsocketMatch($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI: //ws比赛 rest api
                self::runWebsocketMatchRestApi($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_EVENT_WS: //ws比赛
                self::runOrginEventWs($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_CRONTAB: //计划任务消费者
                self::runCrontab($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_EVENT_WS_TO_API: //queue_origin_event_ws_to_api
                self::FiveHotCsgoMatchWsToApi($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_REFRESH_MATCH_SOCKET: //refresh_match_socket 重刷socket
                self::refreshMatchByWebsocket($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_WS_TO_API: //websocket to api 刷新match 信息
                self::runWebsocketToApi($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_MAJOR_API_DATA_SYNC: //api 刷新
                self::runRefreshDataApi($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_REFRESH_HK:
                self::runHK($tagInfo,$taskInfo);
                break;
            default:
                CommonLogService::record("no_task_class","no task class for tag: ".$taskInfo["tag"]);
        }
    }

    private static function runCommonSwitch($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            $major=$tagInfo["major"];
            self::setTaskBegin($taskId);  //记录下任务开始时间
            // 不需要其他变量，直接run，这个是个分发
            switch($major){
                case QueueServer::QUEUE_ORIGIN_LOG_FORMAT:
                    $subs = LogFormatDispatch::run($tagInfo,$taskInfo);
                    break;
                case QueueServer::QUEUE_MAJOR_ES_HOT_REFRESH:
                    $subs = EsHotRefreshDispatch::run($tagInfo,$taskInfo);
                    break;
                case QueueServer::QUEUE_MAJOR_HOT_REFRESH_MATCH_5E:
                    $subs = HotRefresh5eDispatch::run($tagInfo,$taskInfo);
                    break;
                case QueueServer::QUEUE_MAJOR_HOT_REFRESH_TOURNAMENT_5E:
                    $subs = HotRefresh5eTournamentDispatch::run($tagInfo,$taskInfo);
                    break;
                default:
                    throw new TaskException("no task class for tag: ".$taskInfo["tag"]);
            }
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    private static function runCrontab($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId); //开始执行，记录执行状态，执行开始时间，该任务的执行状态
            // 如果是grab的操作组，这个type，是一个操作类
            $taskClass = $taskInfo["type"];
            if (new $taskClass instanceof TaskInterface) {
                // 获取抓取值
                $re = $taskClass::run($tagInfo,$taskInfo);
                self::setTaskResponse($taskId, @$re['logs']);   //获取的值并且放入task_info表
                self::setTaskSuccess($taskId);
            }
        }catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
//                "trace" => $e->getTrace(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    /**
     * @param $tagInfo  tag字段处理后的数组
     * @param $taskInfo  数据库查出来的数据
     * @throws \Exception
     */
    private static function runGrab($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId); //开始执行，记录执行状态，执行开始时间，该任务的执行状态
            // 如果是grab的操作组，这个type，是一个操作类
            $taskClass = $taskInfo["type"];
            if (new $taskClass instanceof TaskCatchInterface) {
                // 获取抓取值
                $response = $taskClass::getResponse($taskInfo);
                //pandascore网不好的情况
                $isPandascore = strpos($taskClass,"pandascore");
                if($isPandascore){
                    $responseArray = json_decode($response,true);
                    if($responseArray['error'] == "Internal Server Error"){
                        for($i=1;$i<=3;$i++){
                            sleep(3);
                            $response = $taskClass::getResponse($taskInfo);
                            $responseArray = json_decode($response,true);
                            if($responseArray['error'] == "Internal Server Error"){
                                continue;
                            }else{
                                break;
                            }
                        }
                    }
                }
                self::setTaskResponse($taskId, $response);   //获取的值并且放入task_info表
                // 重新获取一次带值的抓取（计划干掉）记录下返回值，方便debug
                $taskInfoWithResponse = self::getInfoById($taskId);
                // 主要逻辑，分析获取的值，并且把需要做的子任务按照对应格式数组返回回来
                $adds = $taskClass::run($taskInfo['tag'],$taskInfoWithResponse);
                self::setTaskAddsInfo($taskId, $adds); // 记录下返回值，方便debug

                if (!empty($adds) && is_array($adds)) {
                    foreach ($adds as $key => $val) {
                        self::addTask($val, $taskInfo['run_type']);
                    }
                }
                self::setTaskSuccess($taskId);
            }
        }catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => $e->getCode(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            if($errs["code"] == self::STATUS_EMPTY){
                $errorInfo = json_encode($errs);
                self::setTaskEmpty($taskId, $errorInfo);
            }else{
                $errorInfo = json_encode($errs);
                self::setTaskFail($taskId, $errorInfo);
            }
        }
    }

    private static function runOriginIncrement($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);  //记录下任务开始时间
            // 不需要其他变量，直接run，这个是个分发
            $subs = OriginIncrementDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    private static function runStandardIncrement($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = StandardIncrementDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    public static function runMainIncrement($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = MainIncrementDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    public static function runRefreshDataApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = RefreshDataApi::run($tagInfo,$taskInfo);
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }


    public static function runRefreshEsApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = RefreshEsApiDispatch::run($tagInfo,$taskInfo);
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    public static function runOperateConfigRefresh($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = OperationConfigRefresh::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    public static function runOperateHotRefresh($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = HotDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
            //throw $e;
        }
    }

    public static function FiveHotCsgoMatchWsToApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
                $subs = FiveHotCsgoMatchWsToApi::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
            //throw $e;
        }
    }

    private static function runMatchFrameLog($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);  //记录下任务开始时间
            //TODO
            $subs = LivedMatchFramesDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
        }
    }

    public static function runWebsocketMatch($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = WebsocketDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }
    public static function runWebsocketMatchRestApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            $subs = WebsocketRestApiDispatcher::run($tagInfo,$taskInfo);
            self::setTaskAddsInfo($taskId, $subs);
            if (!empty($subs) && is_array($subs)) {
                foreach ($subs as $key => $val) {
                    self::addTask($val, $taskInfo['run_type']);
                }
            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }
    //重刷socket
    public static function refreshMatchByWebsocket($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            //不需要子任务
            RefreshWebsocketApi::run($tagInfo,$taskInfo);
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }

    public static function runOrginEventWs($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            OrginDispatcher::run($tagInfo,$taskInfo);
//            $subs = WebsocketDispatcher::run($tagInfo,$taskInfo);
//            self::setTaskAddsInfo($taskId, $subs);
//            if (!empty($subs) && is_array($subs)) {
//                foreach ($subs as $key => $val) {
//                    self::addTask($val, $taskInfo['run_type']);
//                }
//            }
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }
    //websocket to api
    public static function runWebsocketToApi($tagInfo,$taskInfo)
    {
        try {
            $taskId=$taskInfo["id"];
            self::setTaskBegin($taskId);
            WebsocketToApiDispatcher::run($tagInfo,$taskInfo);
            self::setTaskSuccess($taskId);
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskId, $errorInfo);
//            throw $e;
        }
    }

    public static function getInfoById($id)
    {
        return TaskInfo::find()->where(["id" => $id])->one();
    }

    public static function setTaskBegin($id)
    {
        return TaskInfo::updateAll(["begin_time" => self::getNowTime(), "status" => self::STATUS_RUNNING], ["id" => $id]);
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('Y-m-d H:i:s');
        return $time;
    }

    public static function setTaskSuccess($id)
    {
        return TaskInfo::updateAll(["end_time" => self::getNowTime(), "status" => self::STATUS_SUCCESS], ["id" => $id]);
    }

    public static function setTaskFail($id, $errorInfo)
    {
        return TaskInfo::updateAll(["end_time" => self::getNowTime(), "status" => self::STATUS_FAIL, "error_info" => $errorInfo], ["id" => $id]);
    }
    public static function setTaskEmpty($id, $errorInfo)
    {
        return TaskInfo::updateAll(["end_time" => self::getNowTime(), "status" => self::STATUS_EMPTY, "error_info" => $errorInfo], ["id" => $id]);
    }

    public static function addTask($taskInfo,$runType=3,$startDeliverTime=null)
    {
        $task = new TaskInfo();
        // 这里用来处理非正常流程，比如需要同步阻塞执行的流程
        if(isset($taskInfo['run_type'])&&$taskInfo['run_type']){
            $runType=(int)$taskInfo['run_type'];
        }
       if (empty($taskInfo)) {
            return true;
       }

        if (isset($taskInfo["params"])) {
            $taskInfo["params"]= json_encode($taskInfo["params"]) ;
        }

        $taskInfo['run_type']=$runType;
        $task->setAttributes($taskInfo);
        $task->save();
        if($task->getErrors()){
            throw new Exception(json_encode($task->getErrors()));
        }
        $taskId=$task->id;
        // 判断执行类型
        switch ($runType){
            case 1:
                // 香港生产者
                self::addMq($task,$taskInfo['tag'],$startDeliverTime);
                break;
            case 2:
                break;
            case 3:
                self::run($taskId);
                break;
            case 4:
                self::addMqPriority($task,$taskInfo['tag'],$startDeliverTime);
                break;
            case 5:
                self::addAbroad($task,$taskInfo['tag']);
                break;
            case 6:
                // 新加坡生产者
                self::addSingaporeQueue($task,$taskInfo['tag']);
                break;
            case 9001:
                self::addRedis($task,$taskInfo['redis_server']);
                break;
            case 9002:
                self::addRedisQueue($task,$taskInfo['redis_server']);
                break;
        }
        return $task->toArray();
    }

    // 新加坡生产者
    private static function addSingaporeQueue($task,$tag)
    {
        $taskInfo=$task->toArray();
        $messageId = TaskMQProducer::addSingaporeDataApiQueue($taskInfo,$tag);
        print("生产者消息ID为：{$messageId} ...")."\n\n";
        TaskInfo::updateAll(['debug_info'=>$messageId],['id'=>$task['id']]);
    }
    // 香港生产者
    private static function addMq($task,$tag,$startDeliverTime=null)
    {
        $taskInfo=$task->toArray();
        $messageId = TaskMQProducer::addQueue($taskInfo,$tag,$startDeliverTime);
        TaskInfo::updateAll(['debug_info'=>$messageId],['id'=>$task['id']]);
    }
    private static function addMqPriority($task,$tag,$startDeliverTime=null)
    {
        $taskInfo=$task->toArray();
        $messageId = TaskMQProducer::addPriorityQueue($taskInfo,$tag,$startDeliverTime);
        TaskInfo::updateAll(['debug_info'=>$messageId],['id'=>$task['id']]);
    }

    private static function addAbroad($task,$tag)
    {
        $taskInfo=$task->toArray();
        $messageId = TaskMQProducer::addAbroadQueue($taskInfo,$tag);
        TaskInfo::updateAll(['debug_info'=>$messageId],['id'=>$task['id']]);
    }

    private static function addRedis($task,$redis_server){
        $taskInfo=$task->toArray();
        TaskMqRedisProducer::addQueue($taskInfo,$redis_server);
    }

    private static function addRedisQueue($task,$redis_server)
    {
        if(!self::$redisQueueServer){
            $redisConfig = [
                'host' => env("REDIS_HOST"),
                'database' => 1+env("REDIS_DATABASE"),
                'port' => env("REDIS_PORT")??6379,
                'auth' => env("REDIS_PASSWORD")
            ];
            self::$redisQueueServer=new QueueManager($redisConfig);
        }
        self::$redisQueueServer->addQueue($redis_server,$task->toArray());
    }

    public static function setTaskResponse($taskId, $response)
    {
        TaskInfo::updateAll(["response" => $response], ["id" => $taskId]);
    }

    public static function setTaskAddsInfo($taskId, $substeps)
    {
        TaskInfo::updateAll(["substeps" => json_encode($substeps)], ["id" => $taskId]);
    }

    /**
     * @param string $runType
     * @param string $type
     * @param string $batchId
     * 重跑错误数据
     */
    public static function runTaskInfoExceptionData($runType="",$type="",$batchId="")
    {
        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',4],
        ]);
        if($runType){
            $taskInfo->andwhere(['=','run_type',$runType]);
        }
        if($type){
            $taskInfo->andWhere(['=','type',$type]);
        }
        if($batchId){
            $taskInfo->andWhere(['=','batch_id',$batchId]);
        }
        $taskInfoArray = $taskInfo->asArray()->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                TaskRunner::run($value['id']);
            }
        }
    }
    public static function runHK($tag,$taskInfo)
    {
        try {
            if($taskInfo['type'] =="team"){
                $value = json_decode($taskInfo["params"], true);
                $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
                if($image['ali_url'] && !$image['hk_url']){  //有转存的
                    $aliUrl = self::mvImageToAli($image['ali_url']);
                    $insertData = [
                        'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                    ];
                    $image->setAttributes($insertData);
                    $image->save();
                }elseif($image['hk_url']){
                    $aliUrl = $image['hk_url'];
                }else{
                    $aliUrl= self::mvImageToAli($value['image']);
                }
                $team = Team::find()->where(['id'=>$value['id']])->one();
                $team->setAttributes([
                    'image'=>$aliUrl
                ]);
                if(!$team->save()){
                    throw new BusinessException($team->getErrors(), '保存失败');
                }
                self::setTaskSuccess($taskInfo['id']);
            }
            if($taskInfo['type'] =="player"){
                $value = json_decode($taskInfo["params"], true);
                $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
                if($image['ali_url'] && !$image['hk_url']){  //有转存的
                    $aliUrl = self::mvImageToAli($image['ali_url']);
                    $insertData = [
                        'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                    ];
                    $image->setAttributes($insertData);
                    $image->save();
                }elseif($image['hk_url']){
                    $aliUrl = $image['hk_url'];
                }else{
                    $aliUrl= self::mvImageToAli($value['image']);
                }
                $player = Player::find()->where(['id'=>$value['id']])->one();
                $player->setAttributes([
                    'image'=>$aliUrl
                ]);
                if(!$player->save()){
                    throw new BusinessException($player->getErrors(), '保存失败');
                }
                self::setTaskSuccess($taskInfo['id']);
            }
            if($taskInfo['type'] =="team_snapshot"){
                $value = json_decode($taskInfo["params"], true);
//                $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
//                if($image['ali_url'] && !$image['hk_url']){  //有转存的
//                    $aliUrl = self::mvImageToAli($image['ali_url']);
//                    $insertData = [
//                        'hk_url' =>$aliUrl,
////                    'ali_url' =>$aliUrl
//                    ];
//                    $image->setAttributes($insertData);
//                    $image->save();
//                }elseif($image['hk_url']){
//                    $aliUrl = $image['hk_url'];
//                }else{
//                    $aliUrl= self::mvImageToAli($value['image']);
//                }
                $aliUrl = self::mvImageToAli($value['image']);
                $team = TeamSnapshot::find()->where(['id'=>$value['id']])->one();
                $team->setAttributes([
                    'image'=>$aliUrl
                ]);
                if(!$team->save()){
                    throw new BusinessException($team->getErrors(), '保存失败');
                }
                self::setTaskSuccess($taskInfo['id']);
            }
            if($taskInfo['type'] =="player_snapshot"){
                $value = json_decode($taskInfo["params"], true);
                $aliUrl = self::mvImageToAli($value['image']);
                $team = PlayerSnapshot::find()->where(['id'=>$value['id']])->one();
                $team->setAttributes([
                    'image'=>$aliUrl
                ]);
                if(!$team->save()){
                    throw new BusinessException($team->getErrors(), '保存失败');
                }
                self::setTaskSuccess($taskInfo['id']);
            }
        } catch (\Exception $e) {
            $errs = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
            ];
            if($e instanceof RestException){
                $errorInfo['data']=$e->getData();
            }
            $errorInfo = json_encode($errs);
            self::setTaskFail($taskInfo['id'], $errorInfo);
        }
    }
    public static function mvImageToAli($url)
    {
        $localPath = \app\modules\common\services\ImageConversionHelper::downfile($url);
        if(!$localPath){
            throw new TaskException("下载失败");
        }
        $fileName = basename($localPath);

        $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
        if($ext == "jpg" || $ext == "jpeg"){
            $imageResource = imagecreatefromjpeg($localPath);
            if(!$imageResource){
                $imageResource = imagecreatefrompng($localPath);
            }
            $postion = strrpos($localPath,'.',0);
            $localPath = substr($localPath,0,$postion).'.png';
            $res = imagepng($imageResource,$localPath);
            if($res){
                $ext = "png";
            }else{
                throw new TaskException("转存失败");
            }
        }
//        $dst = "elmt/".substr(time(),0,5).'.'.$ext;
        $dst = "els/".base64_encode(date("ymd")).'/'.substr(md5(uniqid(mt_rand(), true)),1,12).'.'.$ext;
        $aliOriginUrl = self::upload($dst, $localPath);
        $aliUrl = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
        $is = strstr($aliUrl, 'http');
        if($is){
            $aliUrl = str_replace("http","https",$aliUrl);
        }
        if($aliUrl){
            return $aliUrl;
        }
    }
    public static function upload($dst, $getFile)
    {
        $config = array(
            'KeyId' => 'LTAI4G34n8nSEE3H7D7GUYWX',
            'KeySecret' => 'WqiMInx91XQ8ve2PIoQmZgKQ5JUGxy',
            'Endpoint' => 'oss-cn-hongkong.aliyuncs.com',
            'Bucket' => 'elmt',
        );
        $ossClient = new OssClient(
            $config['KeyId'],
            $config['KeySecret'],
            $config['Endpoint']);
        #执行阿里云上传
        $result = $ossClient->uploadFile(
            $config['Bucket'],
            $dst,
            $getFile);
        #返回
        return $result["info"]["url"];
    }
}
