<?php
/**
 *
 */

namespace app\modules\task\services;

use app\modules\task\models\EnumOrigin;
use app\modules\task\models\TaskType;
use app\modules\task\services\exceptions\TaskException;

class TaskTypes
{
    private static $instance;
    private $origins;
    private $types;
    //构造方法私有化，防止外部创建实例
    private function __construct(){
        $this->origins=$this->getOrigins();
    }

    public static function getInstance(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone(){}

    private function getOrigins()
    {
        $res= EnumOrigin::find()->all();
        $list=[];
        foreach($res as $val){
            $list[$val["name"]]=$val;
        }
        return $list;
    }

    public function getOriginByName($name)
    {
        return $this->origins[$name];
    }
}