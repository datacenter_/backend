<?php
/**
 *
 */

namespace app\modules\task\services;

use app\modules\match\services\BattleService;
use app\modules\common\services\Consts;
use app\modules\data\models\DataConfig;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match;
use app\modules\match\services\MatchService;
use app\modules\org\models\ClanLogo;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use app\modules\task\models\MatchBattle;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;
use function GuzzleHttp\default_ca_bundle;
use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\common\models\EnumGame;
use app\modules\common\services\HelperResourceFormat;

class UpdateConfig
{
    const ORIGIN_FEIJING = "feijing";

    const RESOURCE_TYPE_TEAM = "team";
    const RESOURCE_TYPE_PLAYER = "player";
    const RESOURCE_TYPE_MATCH = "match";
    const RESOURCE_TYPE_TOURNAMENT = "tournament";
    const METADATA_TYPE_LOL_CHAMPION = "lol_champion"; //lol英雄
    const METADATA_TYPE_LOL_ABILITY = 'lol_ability'; //技能
    const METADATA_TYPE_LOL_ITEM = 'lol_item'; //道具
    const METADATA_TYPE_LOL_SUMMONER_SPELL = 'lol_summoner_spell';  //召唤师技能
    const METADATA_TYPE_LOL_RUNE = 'lol_rune'; //符文
    const METADATA_TYPE_DOTA2_HERO = 'dota2_hero';  //英雄
    const METADATA_TYPE_DOTA2_ITEM = 'dota2_item'; //道具
    const METADATA_TYPE_DOTA2_ABILITY = 'dota2_ability'; //技能
    const METADATA_TYPE_DOTA2_TALENT = 'dota2_talent';  //天赋
    const METADATA_TYPE_CSGO_MAP = 'csgo_map';  //cs地图
    const METADATA_TYPE_CSGO_WEAPON ='csgo_weapon'; //cs武器

    const TAG_TYPE_CORE_DATA = "core_data";
    const TAG_TYPE_REAL_TIME = "real_time";
    const TAG_TYPE_BASE_DATA = "base_data";
    const TAG_TYPE_TEAM_PLAYER = "team_player";
    const TAG_TYPE_ATTEND_TEAM = "attend_team";
    const TAG_TYPE_TOURNAMENT_TEAM_RELATION = 'tournament_team_relation';
    const TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE= "tournament_price_distribute";

//    const TAG_TYPE_TOURNAMENT_CORE="tournament_core";
//    const TAG_TYPE_TOURNAMENT_BASE="tournament_base";
//    const TAG_TYPE_TOURNAMENT_TEAMS="tournament_teams";
//    const TAG_TYPE_SUB_TOURNAMENT_CORE="sub_tournament_core";
//    const TAG_TYPE_SUB_TOURNAMENT_BASE="sub_tournament_base";
//    const TAG_TYPE_SUB_TOURNAMENT_TEAMS="sub_tournament_teams";
//    const TAG_TYPE_STAGE_CORE="stage_core";
//    const TAG_TYPE_GROUP_CORE="group_core";
//    const TAG_TYPE_GROUP_TEAMS="group_core";

    private static $instance;
    private $origins;
    private $types;

    //构造方法私有化，防止外部创建实例
    private function __construct()
    {
//        $this->origins = $this->getOrigins();
    }

    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    public function getConfig($resourceType,$resourceId)
    {
        $list=DataConfig::find()->where(['resource_type'=>$resourceType,'resource_id'=>$resourceId])->asArray()->all();
        $infos=[];
        foreach($list as $key=>$val){
            $infos[$val['tag']]=$val;
        }
        return $infos;
    }

    private function getConfigsFromDb()
    {
        return [
            "1" => [
                self::RESOURCE_TYPE_TEAM => [

                ]
            ]
        ];
        return [
            "1" => [
                "1" => ["core_data_config" => '1',
                    "core_data_mode" => '1',
                    "base_data_config" => '1',
                    "base_data_mode" => '1',
                    "team_player_config" => '1',
                    "team_player_mode" => '1',]//战队配置
            ]
        ];
    }

    public function getChangeTagByDiffInfo($resourceType, $diffInfo, $originId,$gameId,$standardMathInfoId = null)
    {
        // 过滤资源类型
        $diffInfo = $this->changeResourceType($resourceType,$diffInfo, $originId,$gameId,$standardMathInfoId);
        // 根据不同的resource做不同的分割方法
        $tagKeys=$this->getTagConfig($resourceType);
        $tagDiff=[];
        foreach($tagKeys as $tag=>$keys){
            $tmp=array_intersect_key($diffInfo,array_fill_keys($keys,""));
            if(count($tmp)){
                $tagDiff[$tag]=$tmp;
            }
        }
        // 如果standard表的字段名字，或者属性跟主表不一样，要再转一次
        return $tagDiff;
    }

    /**
     * 过滤特定资源类型    By    王傲渊
     * @param $resourceType     资源类型
     * @param $diffInfo         差异数据
     * @param $originId
     * @param $gameId           游戏ID
     * 1.把多维降维度
     * 2.降维结构过滤validateAndFormat
     * 3.升维(unset的值，在升维之后，再unset一次)
     * 4.字段变异（比如logo=>image）
     * @return mixed
     */
    public function changeResourceType($resourceType,$diffInfo,$originId,$gameId,$standardMathInfoId)
    {
        $tag = QueueServer::QUEUE_TYPE_CHANGE;//更新
        // 过滤资源类型为选手,战队
//        if($resourceType === self::RESOURCE_TYPE_PLAYER || $resourceType === self::RESOURCE_TYPE_TEAM){
//            foreach($diffInfo as $key => &$value){
//                $params[$key] = $value['after'];
//            }
//        }
        // 过滤资源类型为选手,战队,赛事
        // if($resourceType === self::RESOURCE_TYPE_PLAYER || $resourceType === self::RESOURCE_TYPE_TEAM || $resourceType === self::RESOURCE_TYPE_TOURNAMENT){
        //     foreach($diffInfo as $key => &$value){
        //         $params[$key] = $value['after'];
        //     }
        // }
        $formatDiffInfo=HelperResourceFormat::validateAndFormatDiffInfo($diffInfo,$resourceType,$tag,$gameId);
        // 选手
        if($resourceType === self::RESOURCE_TYPE_PLAYER){
            // 选手国家
            if(isset($formatDiffInfo['nationality'])){
                $formatDiffInfo['player_country'] = $formatDiffInfo['nationality'];
                $formatDiffInfo['player_country']['key'] = "player_country";
                unset($formatDiffInfo['nationality']);
            }
            //选手slug
            if (isset($formatDiffInfo['nick_name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['nick_name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['nick_name']['after']);
            }elseif(empty($formatDiffInfo['nick_name']['after'])){
                unset($formatDiffInfo['slug']);
            }
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }
            // 是否删除
            if(isset($formatDiffInfo['deleted'])){
                if($formatDiffInfo['deleted']['after'] == 1){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }elseif ($formatDiffInfo['deleted']['after'] == 2){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        // 战队
        if($resourceType === self::RESOURCE_TYPE_TEAM){
            // 选手
            if(isset($formatDiffInfo['players']['after'])){
                $relIdentityIds= json_decode($formatDiffInfo['players']['after'],true);//这里可能会报错,下面方法会处理
               // 我们在这里把id转成我们自己的id
               $players=BindingPlayerService::getPlayersByStandardPlayerRelIdentityIds($relIdentityIds,$originId,$gameId);
                $formatDiffInfo['players']['after'] = array_values($players);
                $formatDiffInfo['player_id'] = $formatDiffInfo['players'];
                $formatDiffInfo['player_id']['key'] = "player_id";
                unset($formatDiffInfo['players']);
            }
            // 历史选手
            if(isset($formatDiffInfo['history_players']['after'])){
                $changeHistoryPlayers = json_decode($formatDiffInfo['history_players']['after'],true);
                //将历史选手ID转化为我们自己的ID
                $historyPlayers=BindingPlayerService::getPlayersByStandardPlayerRelIdentityIds($changeHistoryPlayers,$originId,$gameId);
                $historyArray = [];
                if(count($historyPlayers)>0){
                    foreach ($historyPlayers as $key => $value){
                        $historyArray[] = (int)$value['id'];
                    }
                }
                $formatDiffInfo['history_players']['after'] = json_encode($historyArray);
                $formatDiffInfo['history_players']['key'] = "history_players";
            }

            // 俱乐部
            if(isset($formatDiffInfo['clan_id'])){
                unset($formatDiffInfo['clan_id']);
            }
            // slug
            if ($formatDiffInfo['name']) {
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['key'] = 'slug';
            }elseif(empty($formatDiffInfo['name'])){
                unset($formatDiffInfo['slug']);
            }
            if(isset($formatDiffInfo['logo'])){
                if($formatDiffInfo['logo']['after'] == null){
                    $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] = 'image';
                }else{
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['logo']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['logo']['before'];
                    $formatDiffInfo['image']['key'] = 'image';
                }
            }
            // 是否删除
            if(isset($formatDiffInfo['deleted'])){
                if($formatDiffInfo['deleted']['after'] == 1){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }elseif ($formatDiffInfo['deleted']['after'] == 2){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        // 赛事
        if($resourceType === self::RESOURCE_TYPE_TOURNAMENT){
            if(isset($formatDiffInfo['address'])){
                $formatDiffInfo['location']['after'] = $formatDiffInfo['address']['after'];
                $formatDiffInfo['location']['before'] = $formatDiffInfo['address']['before'];
                $formatDiffInfo['location']['key'] = "location";
                unset($formatDiffInfo['address']);
            }
            if(isset($formatDiffInfo['address_cn'])){
                $formatDiffInfo['location_cn']['after'] = $formatDiffInfo['address_cn']['after'];
                $formatDiffInfo['location_cn']['before'] = $formatDiffInfo['address_cn']['before'];
                $formatDiffInfo['location_cn']['key'] = "location_cn";
                unset($formatDiffInfo['address_cn']);
            }
            if(isset($formatDiffInfo['son_sort'])){
                $formatDiffInfo['son_match_sort']['after'] = $formatDiffInfo['son_sort']['after'];
                $formatDiffInfo['son_match_sort']['before'] = $formatDiffInfo['son_sort']['before'];
                $formatDiffInfo['son_match_sort']['key'] = "son_match_sort";
                unset($formatDiffInfo['son_sort']);
            }
            // slug
            if ($formatDiffInfo['name']) {
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['key'] = 'slug';
            }elseif(empty($formatDiffInfo['name'])){
                unset($formatDiffInfo['slug']);
            }
            if ($formatDiffInfo['game_id']) {
                $formatDiffInfo['game']['after'] = $formatDiffInfo['game_id']['after'];
                $formatDiffInfo['game']['before'] =$formatDiffInfo['game_id']['before'];
                $formatDiffInfo['game']['key'] = 'slug';
            }elseif(empty($formatDiffInfo['game_id'])){
                unset($formatDiffInfo['game_id']);
            }

            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }
            //奖池
            if(isset($diffInfo['prize_distribution']['after']) && $diffInfo['prize_distribution']['after']){
                if(!empty(json_decode($diffInfo['prize_distribution']['after']))){
                    $prize_distribution = json_decode($diffInfo['prize_distribution']['after'],true);
                    foreach ($prize_distribution as $key => $value){
                        if(isset($value["team_id"]) && $value['team_id']){
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value['team_id'],$originId);
                            if($teamMasterId){
                                $prize_distribution[$key]['team_id'] = $teamMasterId;
                            }else{
                                unset($prize_distribution[$key]['team_id']);
                            }
                        }
                    }
                    $prize_distribution = json_encode($prize_distribution);
                    if ($prize_distribution){
                        $formatDiffInfo['prize_distribution']['after'] = $prize_distribution;
                        $formatDiffInfo['prize_distribution']['before'] = $diffInfo['prize_distribution']['before'];
                    }else{
                        unset($formatDiffInfo['prize_distribution']);
                    }
                }else{
                    $formatDiffInfo['prize_distribution']['after'] = "";
                    $formatDiffInfo['prize_distribution']['before'] = $diffInfo['prize_distribution']['before'];
                    $formatDiffInfo['prize_distribution']['key'] = "prize_distribution";
                }
            }else{
                if(HelperResourceFormat::checkIsNumberZero($diffInfo['prize_distribution']['after'])){
                    unset($formatDiffInfo['prize_distribution']);
                }
                elseif (!$diffInfo['prize_distribution']['after'] && $diffInfo['prize_distribution']['before']) {  //之前有值,后来变为空
                    $formatDiffInfo['prize_distribution']['after'] = $diffInfo['prize_distribution']['after'];
                    $formatDiffInfo['prize_distribution']['before'] = $diffInfo['prize_distribution']['before'];
                    $formatDiffInfo['prize_distribution']['key'] = "prize_distribution";
                }else{
                    //判断
                    $prize_distribution_one = array_key_exists('prize_distribution',$diffInfo);
                    if ($prize_distribution_one){
                        $prize_distribution_two = array_key_exists('after',$diffInfo['prize_distribution']);
                        if ($prize_distribution_two){
                            if ($diffInfo['prize_distribution']['after'] === null || $diffInfo['prize_distribution']['after'] === ''){
                                $formatDiffInfo['prize_distribution']['after'] = null;
                                $formatDiffInfo['prize_distribution']['before'] = $diffInfo['prize_distribution']['before'];
                            }else{
                                unset($formatDiffInfo['prize_distribution']);
                            }
                        }else{
                            unset($formatDiffInfo['prize_distribution']);
                        }
                    }else{
                        unset($formatDiffInfo['prize_distribution']);
                    }
                }
            }
            //参赛条件的更新
            if(isset($diffInfo['teams_condition']['after']) && $diffInfo['teams_condition']['after']){
                if(!empty(json_decode($diffInfo['teams_condition']['after']))){
                    $teamsCondition = json_decode($diffInfo['teams_condition']['after'],true);
                    $teamsCondition = Common::sort_for_arrays($teamsCondition,"team_sort");
                    foreach ($teamsCondition as $key => $value){
                        if(isset($value["team_id"]) && $value['team_id']){
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value['team_id'],$originId);
                            if($teamMasterId){
                                $teamsCondition[$key]['team_id'] = $teamMasterId;
                            }else{
                                $teamsCondition[$key]['team_id'] = null;
                            }
                        }
                    }
                    $teams_condition_json = json_encode($teamsCondition);
                    if ($teams_condition_json){
                        $formatDiffInfo['teams_condition']['after'] = $teams_condition_json;
                        $formatDiffInfo['teams_condition']['before'] = $diffInfo['teams_condition']['before'];
                    }else{
                        unset($formatDiffInfo['teams_condition']);
                    }
                }else{
                    $formatDiffInfo['teams_condition']['after'] = "";
                    $formatDiffInfo['teams_condition']['before'] = $diffInfo['teams_condition']['before'];
                    $formatDiffInfo['teams_condition']['key'] = "teams_condition";
                }
            }else{
                if(HelperResourceFormat::checkIsNumberZero($diffInfo['teams_condition']['after'])){
                    unset($formatDiffInfo['teams_condition']);
                }
                elseif (!$diffInfo['teams_condition']['after'] && $diffInfo['teams_condition']['before']) {
                    $formatDiffInfo['teams_condition']['after'] = $diffInfo['teams_condition']['after'];
                    $formatDiffInfo['teams_condition']['before'] = $diffInfo['teams_condition']['before'];
                    $formatDiffInfo['teams_condition']['key'] = "teams_condition";
                }
                else{
                    //判断
                    $teams_condition_one = array_key_exists('teams_condition',$diffInfo);
                    if ($teams_condition_one){
                        $teams_condition_two = array_key_exists('after',$formatDiffInfo['teams_condition']);
                        if ($teams_condition_two){
                            if ($diffInfo['teams_condition']['after'] === null || $diffInfo['teams_condition']['after'] === ''){
                                $formatDiffInfo['teams_condition']['after'] = null;
                                $formatDiffInfo['teams_condition']['before'] = $diffInfo['teams_condition']['before'];
                            }else{
                                unset($formatDiffInfo['teams_condition']);
                            }
                        }else{
                            unset($formatDiffInfo['teams_condition']);
                        }
                    }else{
                        unset($formatDiffInfo['teams_condition']);
                    }
                }
            }
            $teams = []; //参赛战队
            if(isset($diffInfo['teams']['after']) && $diffInfo['teams']['after']){
                if(!is_null(json_decode($diffInfo['teams']['after']))){
                    $teamS = json_decode($diffInfo['teams']['after'],true);
                    foreach ($teamS as $key => $value){
                        $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value,$originId);
                        static $tkey = 0;
                        if($teamMasterId){
                            $teams[$tkey] = $teamMasterId;
                            $tkey ++;
                        }
                    }
                    $teams_json = json_encode($teams);
                    if ($teams_json){
                        $formatDiffInfo['team_relation']['after'] = $teams_json;
                        $formatDiffInfo['team_relation']['before'] = $diffInfo['teams']['before'];
                        $formatDiffInfo['team_relation']['key'] = "team_relation";
                    }else{
                        unset($formatDiffInfo['teams']);
                    }
                }else{
                    unset($formatDiffInfo['teams']);
                }
            }else {
                if(HelperResourceFormat::checkIsNumberZero($diffInfo['teams']['after'])){
                    unset($formatDiffInfo['teams']);
                }
                elseif (!$diffInfo['teams']['after'] && $diffInfo['teams']['before']) {
                    $formatDiffInfo['team_relation']['after'] = $diffInfo['teams']['after'];
                    $formatDiffInfo['team_relation']['before'] = $diffInfo['teams']['before'];
                    $formatDiffInfo['team_relation']['key'] = "team_relation";
                } else {
                    //判断
                    $teams_condition_one = array_key_exists('teams', $diffInfo);
                    if ($teams_condition_one) {
                        $teams_condition_two = array_key_exists('after', $diffInfo['teams']);
                        if ($teams_condition_two) {
                            if ($diffInfo['teams']['after'] === null || $diffInfo['teams']['after'] === '') {
                                $formatDiffInfo['teams']['after'] = null;
                                $formatDiffInfo['teams']['before'] = $diffInfo['teams']['before'];
                            } else {
                                unset($formatDiffInfo['teams']);
                            }
                        } else {
                            unset($formatDiffInfo['teams']);
                        }
                    } else {
                        unset($formatDiffInfo['teams']);
                    }
                }
            }

            // 是否删除
            if(isset($formatDiffInfo['deleted'])){
                if($formatDiffInfo['deleted']['after'] == 1){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }elseif ($formatDiffInfo['deleted']['after'] == 2){
                    $formatDiffInfo['deleted_at']['key'] ='deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }

        // 比赛
        if($resourceType === self::RESOURCE_TYPE_MATCH) {
            // 比赛类型
            if (array_key_exists('match_type', $formatDiffInfo)) {
                // 如果是 0 '' null
                if (self::checkIsStringNull($formatDiffInfo['match_type']) || self::checkIsNumberZero($formatDiffInfo['match_type'])) {
                    unset($formatDiffInfo['match_type']);
                }
            }
            //502bug编号
            if(isset($info['is_battle_detailed']) || empty($info['is_battle_detailed'])){
                unset($info['is_battle_detailed']);
            }
            if(isset($info['is_streams_supported']) || empty($info['is_streams_supported'])){
                unset($info['is_streams_supported']);
            }
            if(isset($info['is_pbpdata_supported']) || empty($info['is_pbpdata_supported'])){
                unset($info['is_pbpdata_supported']);
            }
            // 是否平局
            if (isset($formatDiffInfo['draw'])) {
                $formatDiffInfo['is_draw']['key'] = 'is_draw';
                $formatDiffInfo['is_draw']['before'] = $formatDiffInfo['draw']['before'];
                $formatDiffInfo['is_draw']['after'] = $formatDiffInfo['draw']['after'];
            }
            if (isset($formatDiffInfo['game'])) {
                unset($formatDiffInfo['game']);
            }
            // 是否弃权
            if (isset($formatDiffInfo['forfeit'])) {
                $formatDiffInfo['is_forfeit']['key'] = 'is_forfeit';
                $formatDiffInfo['is_forfeit']['before'] = $formatDiffInfo['forfeit']['before'];
                $formatDiffInfo['is_forfeit']['after'] = $formatDiffInfo['forfeit']['after'];
            }
            // 主队
            if (isset($formatDiffInfo['team_1_id'])) {
                // 如果是 0 '' null
                if (self::checkIsStringNull($formatDiffInfo['team_1_id']) || self::checkIsNumberZero($formatDiffInfo['team_1_id'])) {
                    $formatDiffInfo['team_1_id']['after'] = -1;
                    $formatDiffInfo['team_1_id']['team_name'] = 'TBD';
                } else {
                    // 查询战队
                    $team1Id = HotBase::getMainIdByRelIdentityId('team', $formatDiffInfo['team_1_id']['after'], $originId, $gameId);
                    if (!$team1Id) {
                        $formatDiffInfo['team_1_id']['after'] = -1;
                        $formatDiffInfo['team_1_id']['team_name'] = 'TBD';
//                        unset($formatDiffInfo['team_1_id']);
                    } else {
                        $formatDiffInfo['team_1_id']['after'] = $team1Id;
                        $formatDiffInfo['team_1_id']['team_name'] = Team::find()->select('name')->where(['id' => $team1Id])->asArray()->one()['name'];
                    }
                }
            }
            // 客队
            if (isset($formatDiffInfo['team_2_id'])) {
                if (self::checkIsStringNull($formatDiffInfo['team_2_id']) || self::checkIsNumberZero($formatDiffInfo['team_2_id'])) {
                    $formatDiffInfo['team_2_id']['after'] = -1;
                    $formatDiffInfo['team_2_id']['team_name'] = 'TBD';
                } else {
                    // 查询战队
                    $team2Id = HotBase::getMainIdByRelIdentityId('team', $formatDiffInfo['team_2_id']['after'], $originId, $gameId);
                    if (!$team2Id) {
                        $formatDiffInfo['team_2_id']['after'] = -1;
                        $formatDiffInfo['team_2_id']['team_name'] = 'TBD';
                    } else {
                        $formatDiffInfo['team_2_id']['after'] = $team2Id;
                        $formatDiffInfo['team_2_id']['team_name'] = Team::find()->select('name')->where(['id' => $team2Id])->asArray()->one()['name'];
                    }
                }
            }
            // 是否调整时间表  已做完在 MatchService里面
//            if(isset($formatDiffInfo['is_rescheduled'])){
//                $original_scheduled_begin_at = '';
//                if($formatDiffInfo['scheduled_begin_at'] == $original_scheduled_begin_at){
//                    $formatDiffInfo['is_rescheduled']['after'] = 2;
//                }else{
//                    $formatDiffInfo['is_rescheduled']['after'] = 1;
//                }
//            }
            // unset 没用的值 需要的话 设置中进行添加
            foreach ($formatDiffInfo as $key => $value) {
                $fieldBool = self::isSetFieldConfig($key);
                // 如果没有设置unset
                if (!$fieldBool) {
                    unset($formatDiffInfo[$key]);
                }
            }
            // 实际开始时间
//            if(array_key_exists('begin_at',$formatDiffInfo)){
//                // 如果是 0 '' null
//                if (self::checkIsStringNull($formatDiffInfo['begin_at']) || self::checkIsNumberZero($formatDiffInfo['begin_at'])){
//                    unset($formatDiffInfo['begin_at']);
//                }
//            }
            // 实际结束时间
//            if(array_key_exists('end_at',$formatDiffInfo)){
//                // 如果是 0 '' null
//                if (self::checkIsStringNull($formatDiffInfo['end_at']) || self::checkIsNumberZero($formatDiffInfo['end_at'])){
//                    unset($formatDiffInfo['end_at']);
//                }
//            }
            // slug生成
//            $teamOneId = array_key_exists('team_1_id',$formatDiffInfo) ? $formatDiffInfo['team_1_id']:null;
//            $teamTwoId = array_key_exists('team_2_id',$formatDiffInfo) ? $formatDiffInfo['team_2_id']:null;
//            $scheduled_begin_at = isset($formatDiffInfo['scheduled_begin_at']) ? $formatDiffInfo['scheduled_begin_at']:'';
//            if($teamOneId||$teamTwoId){
//                $info['slug'] = self::matchTeamSlugGenerate($teamOneId,$teamTwoId,$scheduled_begin_at);
//            }
            // winner
            if (array_key_exists('winner', $formatDiffInfo)) {
                if ($formatDiffInfo['winner']['after']) {
                    $winnerTeamId = HotBase::getMainIdByRelIdentityId('team', $formatDiffInfo['winner']['after'], $originId, $gameId);
                    if ($winnerTeamId) {
                        $formatDiffInfo['winner']['after'] = self::getTeamIdByOrder($standardMathInfoId,$formatDiffInfo['winner']['after']);;
                    } else {
                        $formatDiffInfo['winner']['after'] = null;
                    }
                }
            }
            // 默认领先战队 存1或者2或者null set时处理了
            if (array_key_exists('default_advantage', $formatDiffInfo)) {

                if ($formatDiffInfo['default_advantage']['after']) {
                    $winnerTeamId = HotBase::getMainIdByRelIdentityId('team', $formatDiffInfo['default_advantage']['after'], $originId, $gameId);
                    if ($winnerTeamId) {
                        $formatDiffInfo['default_advantage']['after'] = self::getTeamIdByOrder($standardMathInfoId,$formatDiffInfo['default_advantage']['after']);;
                    } else {
                        unset($formatDiffInfo['default_advantage']);
                    }
                }
            }

            if (isset($formatDiffInfo['team_1_score']['after']) && isset($formatDiffInfo['team_2_score']['after'])){

                $standardInfo = StandardDataMatch::find()->where(['id'=>$standardMathInfoId])->asArray()->one();

                if (!empty($standardInfo)) {
                    $status = $formatDiffInfo['status']['after'] ?: $standardInfo['status'];
                    $number_of_games = $formatDiffInfo['number_of_games']['after'] ?: $standardInfo['number_of_games'];

                    if ($status == '3' && $number_of_games == '1' && $standardInfo['origin_id'] == '7'){
                        if ($formatDiffInfo['team_1_score']['after'] > $formatDiffInfo['team_2_score']['after']) {
                            $formatDiffInfo['team_1_score']['after'] = 1;
                            $formatDiffInfo['team_2_score']['after'] = 0;
                        }elseif ($formatDiffInfo['team_1_score']['after'] < $formatDiffInfo['team_2_score']['after']) {
                            $formatDiffInfo['team_1_score']['after'] = 0;
                            $formatDiffInfo['team_2_score']['after'] = 1;
                        }

                    }

                }
            }
            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
            // ban-pick
            if (array_key_exists('map_ban_pick', $formatDiffInfo)) {
                if ($formatDiffInfo['map_ban_pick']['after'] != null) {
                    $banPinkInfo = json_decode($formatDiffInfo['map_ban_pick']['after'], true);
                    foreach ($banPinkInfo as $key => $value) {
                        if (isset($value["team"]) && $value['team']) {
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM, $value['team'], $originId, $gameId);
                            if ($teamMasterId) {
                                $banPinkInfo[$key]['team'] = $teamMasterId;
                                $banPinkInfo[$key]['type'] = $value['type'];
                                $banPinkInfo[$key]['map'] = $value['map'];
                            } else {
                                $banPinkInfo[$key]['team'] = '';
                                $banPinkInfo[$key]['type'] = $value['type'];
                                $banPinkInfo[$key]['map'] = $value['map'];
                            }
                        } else {
                            $banPinkInfo[$key]['team'] = '';
                        }
                    }
                    $banPinkArray = json_encode($banPinkInfo);
                    if ($banPinkArray != '[]') {
                        $formatDiffInfo['map_ban_pick']['after'] = $banPinkArray;
                    } else {
                        unset($formatDiffInfo['map_ban_pick']);
                    }
                }
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }



            if(array_key_exists('default_advantage', $formatDiffInfo) &&  $formatDiffInfo['default_advantage']['after'] == null ) {
                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);
                if (!empty($matchId)){
                    $battleIds = MatchBattle::find()->where(['match'=>$matchId,'is_default_advantage'=>1,'game'=>$gameId])->asArray()->all();
                    foreach (@$battleIds as $key => $val) {

                        $battleType = 'advantage';//'advantage' 或者 forfeit
                        $dealType = 'delete';//create 或者 delete
                        //$originId = 3;//数据源ID
                        //$matchId = 789;//比赛ID
                        $battleOrder = '';// 具体orderID 或者 传空 是新建
                        $battleId = $val['id'];//如果传值就按照battleID 不按照battle order
                        $winnerType = '';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                        $winnerId= '';//4317  1
                        $rel_matchId = 0;//数据源比赛ID
                        $rel_battleId = 1;//数据源对局ID
                        HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                    }
                }
            }

            if(array_key_exists('battle_forfeit', $formatDiffInfo) &&  $formatDiffInfo['battle_forfeit']['after'] == null ) {
                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                if ($standardInfo['forfeit'] == 2){
                    $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);
                    if (!empty($matchId)){
                        $battleIds = MatchBattle::find()->where(['match'=>$matchId,'is_forfeit'=>1,'game'=>$gameId])->asArray()->all();
                        foreach (@$battleIds as $key => $val) {

                            $battleType = 'forfeit';//'advantage' 或者 forfeit
                            $dealType = 'delete';//create 或者 delete
                            //$originId = 3;//数据源ID
                            //$matchId = 789;//比赛ID
                            $battleOrder = '';// 具体orderID 或者 传空 是新建
                            $battleId = $val['id'];//如果传值就按照battleID 不按照battle order
                            $winnerType = '';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                            $winnerId= '';//4317  1
                            $rel_matchId = 0;//数据源比赛ID
                            $rel_battleId = 1;//数据源对局ID
                            HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                        }
                    }
                }
            }

            if (array_key_exists('is_forfeit', $formatDiffInfo)){
                if ($formatDiffInfo['is_forfeit']['after'] == 2){
                    $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                    $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);
                    if (!empty($matchId)){
                        $battleIds = MatchBattle::find()->where(['match'=>$matchId,'is_forfeit'=>1,'game'=>$gameId])->asArray()->all();
                        foreach (@$battleIds as $key => $val) {

                            $battleType = 'forfeit';//'advantage' 或者 forfeit
                            $dealType = 'delete';//create 或者 delete
                            //$originId = 3;//数据源ID
                            //$matchId = 789;//比赛ID
                            $battleOrder = '';// 具体orderID 或者 传空 是新建
                            $battleId = $val['id'];//如果传值就按照battleID 不按照battle order
                            $winnerType = '';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                            $winnerId= '';//4317  1
                            $rel_matchId = 0;//数据源比赛ID
                            $rel_battleId = 1;//数据源对局ID
                                HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                        }
                    }

                }
            }

            //单图弃权
            if ($formatDiffInfo['battle_forfeit']['after'] && isset($formatDiffInfo['battle_forfeit']['after'])) {
                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);
                if (!empty($matchId)) {
                    $BaorderArr = json_decode($formatDiffInfo['battle_forfeit']['after'], true);

                    $afterOrder = [];
                    $afterErr = [];
                    foreach ($BaorderArr as $BkK => $Bvv) {
                        $battleWinnerid = '';
                        if ($Bvv['rel_team_id'] == $standardInfo['team_1_id']) {
                            $battleWinnerid = $standardInfo['team_2_id'];
                        }
                        if ($Bvv['rel_team_id'] == $standardInfo['team_2_id']) {
                            $battleWinnerid = $standardInfo['team_1_id'];
                        }
                        $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $battleWinnerid, $originId, $gameId);
                        if (empty($bWinnerid)){
                            $afterErr[] = $Bvv['battle_order'];
                            continue;
                        }

                        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
                        if (@$updateInfo['origin_id'] == $originId) {
                            if ($Bvv['battle_order'] == 1 && !empty($standardInfo['default_advantage'])) {
                                $afterErr[] = $Bvv['battle_order'];
                                continue;
                            }
                            $afterOrder[] = $Bvv['battle_order'];
                            $battleType = 'forfeit';//'advantage' 或者 forfeit
                            $dealType = 'create';//create 或者 delete
//                            $originId = 3;//数据源ID
//                            $matchId = 789;//比赛ID
                            $battleOrder = $Bvv['battle_order'];// 具体orderID 或者 传空 是新建
                            $battleId = '';//如果传值就按照battleID 不按照battle order
                            $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                            $winnerId= $bWinnerid;//4317  1
                            $rel_matchId = $standardInfo['rel_identity_id'];//数据源比赛ID
                            $rel_battleId = 1;//数据源对局ID
                                HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                        }

                    }


                    if(!empty($formatDiffInfo['battle_forfeit']['before'])){
                        $Baorderbefore = json_decode($formatDiffInfo['battle_forfeit']['before'], true);
//                    $battleIds = MatchBattle::find()->where(['match'=>$matchId,'is_forfeit'=>1,'game'=>$gameId])->asArray()->all();
                        foreach (@$Baorderbefore as $befkey => $befval) {

                            if (in_array($befval['battle_order'],$afterOrder)){

                            }else{
                                if (in_array($befval['battle_order'],$afterErr)){

                                }else{

                                    $battleType = 'forfeit';//'advantage' 或者 forfeit
                                    $dealType = 'delete';//create 或者 delete
                                    //$originId = 3;//数据源ID
                                    //$matchId = 789;//比赛ID
                                    $battleOrder = $befval['battle_order'];// 具体orderID 或者 传空 是新建
                                    $battleId = '';//如果传值就按照battleID 不按照battle order
                                    $winnerType = '';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                                    $winnerId= '';//4317  1
                                    $rel_matchId = 0;//数据源比赛ID
                                    $rel_battleId = 1;//数据源对局ID
                                        HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                                }
                            }
                        }
                    }

                }
            }

            //默认领先
            if ($formatDiffInfo['default_advantage']['after'] && isset($formatDiffInfo['default_advantage']['after'])) {
                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);

                //判断更新配置 是不是自动更新
                $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
                if (@$updateInfo['origin_id'] == $originId && !empty($matchId)) {

                    $battleType = 'advantage';//'advantage' 或者 forfeit
                    $dealType = 'create';//create 或者 delete
//                    $originId = 3;//数据源ID
//                    $matchId = 789;//比赛ID
                    $battleOrder = '1';// 具体orderID 或者 传空 是新建
                    $battleId = '';//如果传值就按照battleID 不按照battle order
                    $winnerType = 'order';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                    $winnerId = $formatDiffInfo['default_advantage']['after'];//4317  1
                    $rel_matchId = $standardInfo['rel_identity_id'];//数据源比赛ID
                    $rel_battleId = 1;//数据源对局ID
                    HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                }
            }

            if ($formatDiffInfo['is_forfeit']['after'] && isset($formatDiffInfo['is_forfeit']['after']) && $formatDiffInfo['is_forfeit']['after'] == 1) {
                $standardInfo = StandardDataMatch::find()->where(['id' => $standardMathInfoId])->asArray()->one();
                $winnerTeamIdss = HotBase::getMainIdByRelIdentityId('team', $standardInfo['winner'], $originId, $gameId);

                $matchId = HotBase::getMainIdByRelIdentityId('match', $standardInfo['rel_identity_id'], $originId, $gameId);

                //判断更新配置 是不是自动更新
                $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
                if (@$updateInfo['origin_id'] == $originId && !empty($matchId)) {

                    $battleType = 'forfeit';//'advantage' 或者 forfeit
                    $dealType = 'create';//create 或者 delete
//                    $originId = 3;//数据源ID
//                    $matchId = 789;//比赛ID
                    $battleOrder = '1';// 具体orderID 或者 传空 是新建
                    $battleId = '';//如果传值就按照battleID 不按照battle order
                    $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                    $winnerId = $winnerTeamIdss;//4317  1
                    $rel_matchId = $standardInfo['rel_identity_id'];//数据源比赛ID
                    $rel_battleId = 1;//数据源对局ID
                    HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
                }
            }

        }
        //lol英雄
        if ($resourceType === self::METADATA_TYPE_LOL_CHAMPION) {
            //lol  slug
            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];

                    if (isset($formatDiffInfo['small_image']['after']) && $formatDiffInfo['small_image']['after'] == null) {
                        $formatDiffInfo['small_image']['after'] = $formatDiffInfo['image']['after'];
                        $formatDiffInfo['small_image']['before'] = $formatDiffInfo['image']['before'];
                        $formatDiffInfo['small_image']['key'] ='small_image';
                    }
                }
            }
            if(isset($formatDiffInfo['small_image'])){
                if($formatDiffInfo['small_image']['after'] == null || $formatDiffInfo['small_image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['small_image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['small_image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['small_image']['key'] ='small_image';
                }else{
                    $formatDiffInfo['small_image']['key'] ='small_image';
                    $formatDiffInfo['small_image']['after'] = $formatDiffInfo['small_image']['after'];
                    $formatDiffInfo['small_image']['before'] = $formatDiffInfo['small_image']['before'];
                }
            }

        }
        //lol技能
        if ($resourceType == self::METADATA_TYPE_LOL_ABILITY) {
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];

                    if (isset($formatDiffInfo['small_image']['after']) && $formatDiffInfo['small_image']['after'] == null) {
                        $formatDiffInfo['small_image']['after'] = $formatDiffInfo['image']['after'];
                        $formatDiffInfo['small_image']['before'] = $formatDiffInfo['image']['before'];
                        $formatDiffInfo['small_image']['key'] ='small_image';
                    }

                }
            }
            if(isset($formatDiffInfo['small_image'])){
                if($formatDiffInfo['small_image']['after'] == null || $formatDiffInfo['small_image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['small_image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['small_image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['small_image']['key'] ='small_image';
                }else{
                    $formatDiffInfo['small_image']['key'] ='small_image';
                    $formatDiffInfo['small_image']['after'] = $formatDiffInfo['small_image']['after'];
                    $formatDiffInfo['small_image']['before'] = $formatDiffInfo['small_image']['before'];
                }
            }

            //lol  slug
            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //lol道具
        if ($resourceType == self::METADATA_TYPE_LOL_ITEM) {
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }

            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }

        }
        //lol召唤师技能
        if ($resourceType == self::METADATA_TYPE_LOL_SUMMONER_SPELL) {
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();

            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }


            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //lol符文
        if ($resourceType == self::METADATA_TYPE_LOL_RUNE) {
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();

            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }

            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //daota英雄
        if ($resourceType == self::METADATA_TYPE_DOTA2_HERO) {
            $enumGame = EnumGame::find()->select('role_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['role_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['role_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['role_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }
            if(isset($formatDiffInfo['small_image'])){
                $simple_image = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();

                if($formatDiffInfo['small_image']['after'] == null || $formatDiffInfo['small_image']['after'] ==$simple_image['simple_image']){
                    $formatDiffInfo['small_image']['after'] = $simple_image['simple_image'];
                    $formatDiffInfo['small_image']['before'] = $simple_image['simple_image'];
                    $formatDiffInfo['small_image']['key'] ='small_image';
                }else{
                    $formatDiffInfo['small_image']['key'] ='small_image';
                    $formatDiffInfo['small_image']['after'] = $formatDiffInfo['small_image']['after'];
                    $formatDiffInfo['small_image']['before'] = $formatDiffInfo['small_image']['before'];
                }
            }
            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //daota 技能
        if ($resourceType == self::METADATA_TYPE_DOTA2_ABILITY) {
            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();

            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }

            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //daota 道具
        if ($resourceType == self::METADATA_TYPE_DOTA2_ITEM) {

            $enumGame = EnumGame::find()->select('item_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['item_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['item_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['item_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }

            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }
        //Daota 天赋
        if ($resourceType == self::METADATA_TYPE_DOTA2_TALENT) {
            if (isset($formatDiffInfo['external_name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['external_name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['external_name']['after']);
            }elseif(empty($formatDiffInfo['external_name']['after'])){
                unset($formatDiffInfo['slug']);
            }

//            if (isset($formatDiffInfo['external_name'])) {
//
//            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }
        }

        //CSGO 地图
        if ($resourceType == self::METADATA_TYPE_CSGO_MAP) {
            $enumGame = EnumGame::find()->select(['square_image','rectangle_image','thumbnail'])->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['square_image'])){
                if($formatDiffInfo['square_image']['after'] == null || $formatDiffInfo['square_image']['after'] ==$enumGame['square_image']){
                    $formatDiffInfo['square_image']['after'] = $enumGame['square_image'];
                    $formatDiffInfo['square_image']['before'] = $enumGame['square_image'];
                    $formatDiffInfo['square_image']['key'] ='square_image';
                }else{
                    $formatDiffInfo['square_image']['key'] ='square_image';
                    $formatDiffInfo['square_image']['after'] = $formatDiffInfo['square_image']['after'];
                    $formatDiffInfo['square_image']['before'] = $formatDiffInfo['square_image']['before'];


                }
            }
            if(isset($formatDiffInfo['rectangle_image'])){
                if($formatDiffInfo['rectangle_image']['after'] == null || $formatDiffInfo['rectangle_image']['after'] ==$enumGame['rectangle_image']){
                    $formatDiffInfo['rectangle_image']['after'] = $enumGame['rectangle_image'];
                    $formatDiffInfo['rectangle_image']['before'] = $enumGame['rectangle_image'];
                    $formatDiffInfo['rectangle_image']['key'] ='rectangle_image';
                }else{
                    $formatDiffInfo['rectangle_image']['key'] ='rectangle_image';
                    $formatDiffInfo['rectangle_image']['after'] = $formatDiffInfo['rectangle_image']['after'];
                    $formatDiffInfo['rectangle_image']['before'] = $formatDiffInfo['rectangle_image']['before'];
                }
            }

            if(isset($formatDiffInfo['thumbnail'])){
                if($formatDiffInfo['thumbnail']['after'] == null || $formatDiffInfo['thumbnail']['after'] ==$enumGame['thumbnail']){
                    $formatDiffInfo['thumbnail']['after'] = $enumGame['thumbnail'];
                    $formatDiffInfo['thumbnail']['before'] = $enumGame['thumbnail'];
                    $formatDiffInfo['thumbnail']['key'] ='thumbnail';
                }else{
                    $formatDiffInfo['thumbnail']['key'] ='thumbnail';
                    $formatDiffInfo['thumbnail']['after'] = $formatDiffInfo['thumbnail']['after'];
                    $formatDiffInfo['thumbnail']['before'] = $formatDiffInfo['thumbnail']['before'];
                }
            }



            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }

            if (!empty($formatDiffInfo['map_type_cn'])) {
                unset($formatDiffInfo['map_type_cn']);
            }

            if (isset($formatDiffInfo['map_type'])) {
                $formatDiffInfo['map_type_cn']['key'] = 'map_type_cn';
                $formatDiffInfo['map_type_cn']['before'] = $formatDiffInfo['map_type']['after'];
                $formatDiffInfo['map_type_cn']['after'] =  $formatDiffInfo['map_type']['after'];
            }elseif(empty($formatDiffInfo['map_type_cn']['after'])){
                unset($formatDiffInfo['map_type_cn']);
            }

        }
        //CSGo 武器
        if ($resourceType == self::METADATA_TYPE_CSGO_WEAPON) {

            $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $gameId])->asArray()->one();
            if(isset($formatDiffInfo['image'])){
                if($formatDiffInfo['image']['after'] == null || $formatDiffInfo['image']['after'] ==$enumGame['simple_image']){
                    $formatDiffInfo['image']['after'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['before'] = $enumGame['simple_image'];
                    $formatDiffInfo['image']['key'] ='image';
                }else{
                    $formatDiffInfo['image']['key'] ='image';
                    $formatDiffInfo['image']['after'] = $formatDiffInfo['image']['after'];
                    $formatDiffInfo['image']['before'] = $formatDiffInfo['image']['before'];
                }
            }
            if (isset($formatDiffInfo['name'])) {
                $formatDiffInfo['slug']['key'] = 'slug';
                $formatDiffInfo['slug']['before'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
                $formatDiffInfo['slug']['after'] = Common::makeConstantMatching($formatDiffInfo['name']['after']);
            }elseif(empty($formatDiffInfo['name']['after'])){
                unset($formatDiffInfo['slug']);
            }

            // 是否删除
            if (isset($formatDiffInfo['deleted'])) {
                if ($formatDiffInfo['deleted']['after'] == 1) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['before'] = date("Y-m-d H:i:s");
                    $formatDiffInfo['deleted_at']['after'] = date("Y-m-d H:i:s");
                }
                if ($formatDiffInfo['deleted']['after'] == 2) {
                    $formatDiffInfo['deleted_at']['key'] = 'deleted_at';
                    $formatDiffInfo['deleted_at']['after'] = null;
                }
            }


        }


        //todo 等资源多了，分开处理
        return $formatDiffInfo;
    }
    public static function checkIsStringNull($v)
    {
        if($v === null || $v === ''){
            return true;
        }else{
            return false;
        }
    }
    public static function checkIsNumberZero($v)
    {
        if ($v === 0 || $v === '0') {
            return true;
        }
        return false;
    }
    /**
     * 赛事数据源更新   By  王傲渊
     * TODO:临时方法,后期合并
     * $diffInfo           差异信息
     */
    public function tournamentChange($diffInfo)
    {
        // 名称(英文)
        if(isset($diffInfo['name']) && isset($data['output']['name'])){
            $diffInfo['name']['after'] = $data['output']['name'];
        }else{
            unset($diffInfo['name']);
        }
            // 游戏项目
        if(isset($diffInfo['game_id'])){
            unset($diffInfo['game_id']);
        }
    }

    private static function getTagConfig($resourceType)
    {
        $config = [
            // lol_item
            Consts::METADATA_TYPE_LOL_ITEM => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "total_cost",
                    "is_trinket",
                    "is_purchasable",
                    "slug",
                    "deleted",
                    "deleted_at"
                ],
            ],
            //lol_champion
            Consts::METADATA_TYPE_LOL_CHAMPION => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "primary_role",
                    "primary_role_cn",
                    "secondary_role_cn",
                    "secondary_role",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "slug",
                    "deleted",
                    "deleted_at"
                ],
            ],
            //lol_ability
            Consts::METADATA_TYPE_LOL_ABILITY => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "champion_id",
                    "name",
                    "name_cn",
                    "image",
                    "small_image",
                    "hotkey",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "slug",
                    "state",
                    "deleted",
                    "deleted_at"
                ],
            ],
            //lol_rune
            Consts::METADATA_TYPE_LOL_RUNE => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "path_name",
                    "path_name_cn",
                    "image",
                    "small_image",
                    "hotkey",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at"
                ],
            ],
            //lol_summoner_spell
            Consts::METADATA_TYPE_LOL_SUMMONER_SPELL => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at"
                ],
            ],
            //dota2英雄
            Consts::METADATA_TYPE_DOTA2_HERO => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "primary_attr",
                    "primary_attr_cn",
                    "roles",
                    "roles_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
                self::TAG_TYPE_BASE_DATA => [
                    "talents"
                ],
            ],
            //dota2道具
            Consts::METADATA_TYPE_DOTA2_ITEM => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "total_cost",
                    "is_recipe",
                    "is_secret_shop",
                    "is_home_shop",
                    "is_neutral_drop",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
            ],
            //dota2技能
            Consts::METADATA_TYPE_DOTA2_ABILITY => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "hero_id",
                    "hotkey",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
            ],
            //dota2天赋
            Consts::METADATA_TYPE_DOTA2_TALENT => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "hero_id",
                    "level",
                    "left_right",
                    "talent",
                    "talent_cn",
                    "state",
                    "name",
                    "name_cn",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
            ],
            //csgo地图
            Consts::METADATA_TYPE_CSGO_MAP => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "name_cn",
                    "state",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "map_type",
                    "map_type_cn",
                    "is_default",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "square_image",
                    "rectangle_image",
                    "thumbnail",
                    "short_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
            ],
            //csgo武器
            Consts::METADATA_TYPE_CSGO_WEAPON => [
                self::TAG_TYPE_CORE_DATA => [
                    "game_id",
                    "name",
                    "kind",
                    "state",
                    "cost",
                    "capacity",
                    "reload_time",
                    "is_ct_available",
                    "is_t_available",
                    "firing_mode",
                    "data_source",
                    "movement_speed",
                    "ammunition",
                    "kill_award",
                    "name_cn",
                    "image",
                    "small_image",
                    "description",
                    "description_cn",
                    "flag",
                    "title",
                    "title_cn",
                    "external_id",
                    "external_name",
                    "path_name_cn",
                    "slug",
                    "deleted",
                    "deleted_at",
                ],
            ],
            // 选手
            self::RESOURCE_TYPE_PLAYER => [
                self::TAG_TYPE_CORE_DATA => [
                    "nick_name",
                    "game_id",
                    "player_country",
                    "name",
                    "name_cn",
                    "slug",
                    "image",
                    'role',
                    'steam_id',
                    "deleted",
                    'deleted_at',
                ],
                self::TAG_TYPE_BASE_DATA => [
                    "player_id",
                    "birthday",
                    "introduction",
                    "introduction_cn"
                ],
            ],
            // 战队
            self::RESOURCE_TYPE_TEAM => [
                self::TAG_TYPE_CORE_DATA => [
                    "name",
                    "organization",
                    "game",
                    "country",
                    "full_name",
                    "short_name",
                    "alias",
                    "image",
                    'slug',
                    'deleted',
                    'deleted_at',
                ],
                self::TAG_TYPE_BASE_DATA => [
                    "steam_id",
                    "region",
                    "world_ranking",
                    "ago_30",
                    "total_earnings",
                    "average_player_age",
                    "history_players",
                    "create_team_date",
                    "close_team_date",
                    "introduction",
                    "introduction_cn",
                ],
                self::TAG_TYPE_TEAM_PLAYER => [
                    "team_id",
                    "player_id",
                    "status_id",
                ],
            ],
            // 赛事
            self::RESOURCE_TYPE_TOURNAMENT => [
                // 核心内容表
                self::TAG_TYPE_CORE_DATA => [
                    "name",//赛事英文名称
                    "name_cn",//赛事中文名称
                    "game",//游戏ID
                    "status",//状态
                    "begin_at",//开始时间
                    "end_at",//结束时间
                    "scheduled_begin_at",//计划开始日期
                    "scheduled_end_at",//计划结束日期
                    'original_scheduled_begin_at',//原计划开始日期
                    'original_scheduled_end_at',//原计划结束日期
                    'slug',
                    'is_rescheduled',//是否调整了时间表
                    'short_name',//赛事简称英文
                    'short_name_cn',//赛事简称中文
                    'son_match_sort',//子赛事排序
                    'image',
                    'type',//1.赛事,2.子赛事,3.目录
                    "deleted",
                    'deleted_at'
                ],
                // 基础数据表
                self::TAG_TYPE_BASE_DATA => [
                    "steam_id",
                    "series_match",
                    "number_of_teams",
                    "tier",
                    "country",
                    "location",
                    "location_cn",
                    "match_range",
                    "organizer",
                    "organizer_cn",
                    "version",
                    "prize_bonus",
                    "prize_points",
                    "map_pool",
                    "top_reward",
                    "introduction",
                    "introduction_cn",
                    "format",
                    "format_cn",
                    "region",
                    "prize_seed",
                ],
                //奖池
                self::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE =>[
                    "prize_distribution"
                ],
                //队伍条件
                self::TAG_TYPE_ATTEND_TEAM => [
                    "team_relation",
                ],
                self::TAG_TYPE_TOURNAMENT_TEAM_RELATION =>[
                    "teams_condition"
                ],
            ],
            // 比赛
            self::RESOURCE_TYPE_MATCH => [
                self::TAG_TYPE_CORE_DATA => [
                    'game',
                    'tournament_id',
                    'match_rule_id',
                    'son_tournament_id',
                    'stage_id',
                    'group_id',
                    'team_1_id',
                    'team_2_id',
                    'game_rules',
                    'match_type',
                    'number_of_games',
                    'original_scheduled_begin_at',
                    'scheduled_begin_at',
                    'slug',
                    'relation',
                    'is_rescheduled',
                    'deleted_at',
                    'deleted',
                ],
                self::TAG_TYPE_REAL_TIME => [
                    'map_ban_pick', // 地图BP
                    'map_info', // 比赛地图
                    'begin_at', // 实际开始时间
                    'end_at', // 实际结束时间
                    'status', // 比赛状态
                    'scheduled_begin_at',
                    'team_1_score', //
                    'team_2_score', //
                    'is_draw', // 是否平局
                    'is_forfeit', // 是否弃权
                    'winner', // 获胜战队
                    'default_advantage', // 默认领先战队
                    'has_incident_report', // 默认领先战队
                    'is_battle_detailed', // 是否有对局详情
                    'is_pbpdata_supported', // 是否提供实时数据
                    'is_streams_supported', // 是否提供视频直播源
                ],
                self::TAG_TYPE_BASE_DATA => [
                    'location',
                    'location_cn',
                    'round_order',
                    'round_name',
                    'round_name_cn',
                    'bracket_pos',
                    'description',
                    'description_cn',
                    'game_version',
                ]
            ]
        ];
        return $config[$resourceType];
    }

    public static function toTagInfoShow($resourceType,$info)
    {
        // 过滤    By   王傲渊
        $info = self::changeResource($resourceType,$info);

        $tagKeys = self::getTagConfig($resourceType);
        $tagInfo=[];
        foreach($tagKeys as $tag=>$keys){
            $tmp=array_intersect_key($info,array_fill_keys($keys,""));
            $tagInfo[$tag]=$tmp;
        }
        return $tagInfo;
    }

    /**
     * 过滤直接特定资源类型 By  王傲渊
     * $resourceType            类型(字符串)
     * $info                    信息(数组)
     */
    public static function changeResource($resourceType,$info)
    {
        $tag = QueueServer::QUEUE_TYPE_ADD;//新增
        // 过滤资源类型为选手player
        if($resourceType === self::RESOURCE_TYPE_PLAYER || $resourceType === self::RESOURCE_TYPE_TEAM || $resourceType == self::RESOURCE_TYPE_TOURNAMENT || $resourceType === self::RESOURCE_TYPE_MATCH
          || $resourceType === self::METADATA_TYPE_LOL_CHAMPION  || $resourceType === self::METADATA_TYPE_LOL_ABILITY || $resourceType === self::METADATA_TYPE_LOL_ITEM  || $resourceType === self::METADATA_TYPE_LOL_SUMMONER_SPELL
             || $resourceType === self::METADATA_TYPE_LOL_RUNE || $resourceType === self::METADATA_TYPE_DOTA2_HERO || $resourceType === self::METADATA_TYPE_DOTA2_ABILITY || $resourceType === self::METADATA_TYPE_DOTA2_ITEM
             || $resourceType === self::METADATA_TYPE_CSGO_MAP ||  $resourceType === self::METADATA_TYPE_CSGO_WEAPON || $resourceType === self::METADATA_TYPE_DOTA2_TALENT

        ){
            $helperResourceFormat = new HelperResourceFormat();
            $data = $helperResourceFormat->validateAndFormat($info,$resourceType,$tag,$info['game_id']);
        }
        // if($resourceType === self::RESOURCE_TYPE_PLAYER || $resourceType === self::RESOURCE_TYPE_TEAM || $resourceType === self::RESOURCE_TYPE_TOURNAMENT){
        //     $helperResourceFormat = new HelperResourceFormat();
        //     $data = $helperResourceFormat->validateAndFormat($info,$resourceType,$tag,$info['game_id']);
        // }
        if($data['check']){
            foreach($data['check'] as $key => &$value){
                $data['check'] = $value;
            }
            throw new BusinessException([], $data['check']);
        }
        unset($data['validate']);
        // 选手
        if($resourceType === self::RESOURCE_TYPE_PLAYER){
            // 名称
            if(isset($info['nick_name']) && isset($data['output']['nick_name'])){
                $info['nick_name'] = $data['output']['nick_name'];
            }else{
                unset($info['nick_name']);
            }
            // 游戏项目
            if(isset($info['game_id']) && isset($data['output']['game_id'])){
                $info['game_id'] = $data['output']['game_id'];
            }else{
                unset($info['game_id']);
            }
            // 选手国家
            if(isset($info['nationality']) && isset($data['output']['nationality'])){
                $info['player_country'] = $data['output']['nationality'];
                unset($info['nationality']);
            }else{
                unset($info['nationality']);
            }
            // 真实姓名(英文)
            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }
            // 真实姓名(中文)
            if(isset($info['name_cn']) && isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }
            // 角色位置
            if(isset($info['role']) && isset($data['output']['role'])){
                $info['role'] = $data['output']['role'];
            }else{
                unset($info['role']);
            }
            // slug
            if ($data['output']['nick_name']) {
                $info['slug'] = Common::makeConstantMatching($data['output']['nick_name']);
            }
            // slug
//            if(isset($info['slug']) && isset($data['output']['slug'])){
//                unset($info['slug']);
//            }else{
//                unset($info['slug']);
//            }
            // 头像
//            if(isset($info['image']) && isset($data['output']['image'])){
//                $info['image'] = $data['output']['image'];
//            }else{
//                unset($info['image']);
//            }
            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }
            // 生日
            if(isset($info['birthday']) && isset($data['output']['birthday'])){
                $info['birthday'] = $data['output']['birthday'];
            }else{
                unset($info['birthday']);
            }
            // 简介
            if(isset($info['introduction']) && isset($data['output']['introduction'])){
                $strlen = mb_strlen($data['output']['introduction']);
                if($strlen > 1000) {
                    $limitNum = mb_substr($data['output']['introduction'],0,1000);
                    $data['output']['introduction'] = $limitNum;
                }
                $info['introduction'] = $data['output']['introduction'];
            }else{
                unset($info['introduction']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
        // 战队
        if($resourceType === self::RESOURCE_TYPE_TEAM){
            // 战队选手
            if(isset($info['players'])){
                $info['players'] = json_decode($info['players'],true);
                $players = BindingPlayerService::getPlayersByStandardPlayerRelIdentityIds($info['players'],$info['origin_id'],$info['game_id']);
                $info['player_id'] = array_values($players);
                unset($info['players']);
            }
            if(isset($info['players'])){
                $info['player_id'] = "";
                unset($info['players']);
            }
            // 战队历史选手
            if(isset($info['history_players'])){
                $info['history_players'] = json_decode($info['history_players'],true);
                $historyPlayers = BindingPlayerService::getPlayersByStandardPlayerRelIdentityIds($info['history_players'],$info['origin_id'],$info['game_id']);
                $ids=array_column($historyPlayers,'id');
                $idsInt = [];
                foreach($ids as $id){
                    $idsInt[]=(int)$id;
                }
                $info['history_players'] = json_encode($idsInt);
            }
            // 名称
            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }
            // 总金额
            if(isset($info['total_earnings']) && isset($data['output']['total_earnings'])){
                $info['total_earnings'] = $data['output']['total_earnings'];
            }else{
                unset($info['total_earnings']);
            }
            if(isset($info['world_ranking']) && isset($data['output']['world_ranking'])){
                $info['world_ranking'] = $data['output']['world_ranking'];
            }else{
                unset($info['world_ranking']);
            }
            if(isset($info['average_player_age']) && isset($data['output']['average_player_age'])){
                $info['average_player_age'] = $data['output']['average_player_age'];
            }else{
                unset($info['average_player_age']);
            }
            // 游戏项目
            if(isset($info['game_id']) && isset($data['output']['game_id'])){
                $info['game'] = $data['output']['game_id'];
                unset($info['game_id']);
            }else{
                unset($info['game_id']);
            }
            // 俱乐部
            if(isset($info['organization'])){
                unset($info['organization']);
            }
            // 国家地区
            if(isset($info['country']) && isset($data['output']['country'])){
                $info['country'] = $data['output']['country'];
            }else{
                unset($info['country']);
            }
            // 全名
            if(isset($info['full_name']) && isset($data['output']['full_name'])){
                $info['full_name'] = $data['output']['full_name'];
            }else{
                unset($info['full_name']);
            }
            // 简称
            if(isset($info['short_name']) && isset($data['output']['short_name'])){
                $info['short_name'] = $data['output']['short_name'];
            }else{
                unset($info['short_name']);
            }
            // 别名
            if(isset($info['alias']) && isset($data['output']['alias'])){
                $info['alias'] = $data['output']['alias'];
            }else{
                unset($info['alias']);
            }
            // slug
            if($data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }else{
                unset($info['slug']);
            }
            // 图标
            if(isset($info['logo']) && isset($data['output']['logo']) && !empty($data['output']['logo'])){
                $info['image'] = $data['output']['logo'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }
            if(isset($info['region']) && isset($data['output']['region']) && !empty($data['output']['region'])){
                $info['region'] = $data['output']['region'];
            }else{
                $info['region'] = null;
            }
            //世界排名
            if(isset($info['world_ranking']) && isset($data['output']['world_ranking'])){
                $info['world_ranking'] = $data['output']['world_ranking'];
            }else{
                unset($info['world_ranking']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
        // 赛事
        if($resourceType === self::RESOURCE_TYPE_TOURNAMENT){
            // 名称
            if(isset($info['game_id']) && isset($data['output']['game_id'])){
                $info['game'] = $data['output']['game_id'];
            }else{
                unset($info['game_id']);
            }
            if(isset($data['output']['status'])){
                $info['status'] = $data['output']['status'];
            }
            if(in_array("status",$data['unsets'])){
                unset($info['status']);
            }
            if(isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }
            if(isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }
            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }
            if(isset($data['output']['scheduled_begin_at'])){
                $info['scheduled_begin_at'] = $data['output']['scheduled_begin_at'];
            }
            if(isset($data['output']['scheduled_end_at'])){
                $info['scheduled_end_at'] = $data['output']['scheduled_end_at'];
            }
            if(isset($data['output']['short_name'])){
                $info['short_name'] = $data['output']['short_name'];
            }
            if(isset($data['output']['short_name_cn'])){
                $info['short_name_cn'] = $data['output']['short_name_cn'];
            }
            if(isset($data['output']['introduction'])){
                $info['introduction'] = $data['output']['introduction'];
            }
            if(isset($data['output']['introduction_cn'])){
                $info['introduction_cn'] = $data['output']['introduction_cn'];
            }
            if(isset($data['output']['format'])){
                $info['format'] = $data['output']['format'];
            }
            if(isset($data['output']['format'])){
                $info['format_cn'] = $data['output']['format_cn'];
            }
            if(isset($data['output']['country'])){
                $info['country'] = $data['output']['country'];
            }
            if(in_array("country",$data['unsets'])){
                unset($info['country']);
            }
            if(isset($data['output']['region'])){
                $info['region'] = $data['output']['region'];
            }
            if(in_array("region",$data['unsets'])){
                unset($info['region']);
            }
            if(isset($info['address'])) {
                $info['location'] = $info['address'];
                unset($info['address']);
            }
            if(isset($info['address_cn'])) {
                $info['location_cn'] = $info['address_cn'];
                unset($info['address_cn']);
            }
            if(isset($data['output']['teams'])){
                $info['teams'] = $data['output']['teams'];
            }
            if(in_array("teams",$data['teams'])){
                unset($info['teams']);
            }

            if(isset($info['number_of_teams']) && isset($data['output']['number_of_teams']) && !empty($data['output']['number_of_teams'])){
                $info['number_of_teams'] = $data['output']['number_of_teams'];
            }else{
                unset($info['number_of_teams']);
            }

            //奖池
            if(isset($info['prize_distribution'])){
                if(!is_null(json_decode($info['prize_distribution']))){
                    $prize_distribution = json_decode($info['prize_distribution'],true);
                    foreach ($prize_distribution as $key => $value){
                        if(isset($value["team_id"]) && $value['team_id']){
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value['team_id'],$info['origin_id']);
                            if($teamMasterId){
                                $prize_distribution[$key]['team_id'] = $teamMasterId;
                            }else{
                                unset($prize_distribution[$key]['team_id']);
                            }
                        }
                    }
                    $prize_distribution = json_encode($prize_distribution);
                    if ($prize_distribution){
                        $info['prize_distribution'] = $prize_distribution;
                    }else{
                        unset($info['prize_distribution']);
                    }
                }else{
                    unset($info['prize_distribution']);
                }
            }else{
                unset($info['prize_distribution']);
            }


            $teams_condition = []; //参赛条件
            if(isset($info['teams_condition'])){
                if(!is_null(json_decode($info['teams_condition']))){
                    $teamsCondition = json_decode($info['teams_condition'],true);
                    $teamsCondition = Common::sort_for_arrays($teamsCondition,"team_sort");
                    foreach ($teamsCondition as $key => $value){
                        if(isset($value["team_id"]) && $value['team_id']){
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value['team_id'],$info['origin_id']);
                            if($teamMasterId){
                                $teamInfo = Team::find()->select(["name"])->where(['id'=>$teamMasterId])->asArray()->one();
                                $teamsCondition[$key]['team_id'] = $teamMasterId;
                            }else{
                                $teamsCondition[$key]['team_id'] = "";
                            }
//                            static $ckey = 0;
//                            if($teamMasterId){
//                                $teamInfo = Team::find()->select(["name"])->where(['id'=>$teamMasterId])->asArray()->one();
//                                $teams_condition[$ckey]['team_id'] = $teamMasterId;
//                                $teams_condition[$ckey]['type'] = $info['type'];
//                                $teams_condition[$ckey]['name'] = $teamInfo['name'];
//                                $teams_condition[$ckey]['match_condition'] = $value['match_condition'];
//                                $teams_condition[$ckey]['match_condition_cn'] = $value['match_condition_cn'];
//                                $teams_condition[$ckey]['team_sort'] = (String)$value['team_sort'];
//                                $ckey++;
//                            }
                        }
                    }
                    $teams_condition_json = json_encode($teamsCondition);

                    if ($teams_condition_json){
                        $info['teams_condition']= $teams_condition_json;
                    }else{
                        unset($info['teams_condition']);
                    }
                }else{
                    unset($info['teams_condition']);
                }
            }else{
                //判断
                unset($info['teams_condition']);
            }
            //参赛战队
            $teams = [];
            if(isset($info['teams']) && $info['teams']){
                if(!is_null($info['teams'])){
                    $teamS = json_decode($info['teams'],true);
                    foreach ($teamS as $key => $value){
                        $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$value,$info['origin_id']);
                        static $tkey = 0;
                        if($teamMasterId){
                            $teams[$tkey] = $teamMasterId;
                            $tkey ++;
                        }
                    }
                    $teams_json = json_encode($teams);
                    if ($teams_json){
                        $info['team_relation'] = $teams_json;
                    }
                }else{
                    unset($info['teams']);
                }
            }else{
                //判断
                unset($info['teams']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
        }
        // 比赛
        if($resourceType === self::RESOURCE_TYPE_MATCH){
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if(isset($info['game'])){
                unset($info['game']);
            }
            //502bug编号
            if(isset($info['is_battle_detailed']) || empty($info['is_battle_detailed'])){
                unset($info['is_battle_detailed']);
            }
            if(isset($info['is_streams_supported']) || empty($info['is_streams_supported'])){
                unset($info['is_streams_supported']);
            }
            if(isset($info['is_pbpdata_supported']) || empty($info['is_pbpdata_supported'])){
                unset($info['is_pbpdata_supported']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
            // 游戏项目
            if(isset($info['game_id']) && isset($data['output']['game_id']) && $data['output']['game_id']){
                $info['game'] = $data['output']['game_id'];
                unset($info['game_id']);
            }else{
                throw new BusinessException([], "游戏ID为空,不允许创建");
            }
            //比赛规则
            if(array_key_exists('game_rules',$info) && array_key_exists('game_rules',$data['output'])){
                $info['game_rules'] = $data['output']['game_rules'];
            }else{
                unset($info['game_rules']);
            }

            if(array_key_exists('battle_forfeit',$info) && array_key_exists('battle_forfeit',$data['output'])){
                $info['battle_forfeit'] = $data['output']['battle_forfeit'];
            }else{
                unset($info['battle_forfeit']);
            }

            if(array_key_exists('has_incident_report',$info) && array_key_exists('has_incident_report',$data['output'])){
                $info['has_incident_report'] = $data['output']['has_incident_report'];
            }else{
                unset($info['has_incident_report']);
            }

            //比赛类型
            if(array_key_exists('match_type',$info) && isset($data['output']['match_type'])){
                $info['match_type'] = $data['output']['match_type'];
            }else{
                unset($info['match_type']);
            }

            //赛制的局数
            if(array_key_exists('number_of_games',$info) && array_key_exists('number_of_games',$data['output']) && $data['output']['number_of_games']){
                $info['number_of_games'] = $data['output']['number_of_games'];
            }else{
                throw new BusinessException([], "赛制的局数为空,不允许创建");
            }
            //所属赛事
            if(array_key_exists('tournament_id',$info) && array_key_exists('tournament_id',$data['output']) && $data['output']['tournament_id']){
                $info['tournament_id'] = $data['output']['tournament_id'];
            }else{
                throw new BusinessException([], "所属赛事ID为空,不允许创建");
            }

            //所属分组
            if(array_key_exists('group_id',$info) && array_key_exists('group_id',$data['output'])){
                $info['group_id'] = $data['output']['group_id'];
            }else{
                unset($info['group_id']);
            }

            if(array_key_exists('map_info',$info) && array_key_exists('map_info',$data['output'])){
                $info['map_info'] = $data['output']['map_info'];
            }else{
                unset($info['map_info']);
            }

            // 计划开始时间
            if($info['status'] == 4) {
                if(array_key_exists('scheduled_begin_at',$info)){
                    //如果是 0 '' null
                    if(self::checkIsStringNull($info['scheduled_begin_at']) || self::checkIsNumberZero($info['scheduled_begin_at'])){
                        $info['scheduled_begin_at'] = '';
                    }
                }else{
                    $info['scheduled_begin_at'] = '';
                }
            }else{
                //如果是 0 '' null 不允许创建
                if(self::checkIsStringNull($info['scheduled_begin_at']) || self::checkIsNumberZero($info['scheduled_begin_at'])){
                    throw new BusinessException([], "计划开始时间为空,不允许创建");
                }
            }

            //原计划开始时间
            if(array_key_exists('original_scheduled_begin_at',$info) && isset($data['output']['original_scheduled_begin_at'])){
                $info['original_scheduled_begin_at'] = $data['output']['original_scheduled_begin_at'];
            }else{
                $info['original_scheduled_begin_at'] = $info['scheduled_begin_at'];
            }

            //是否调整了时间表
            if($data['output']['scheduled_begin_at'] == $data['output']['original_scheduled_begin_at']){
                $info['is_rescheduled'] = 2;
            }else{
                $info['is_rescheduled'] = 1;
            }

            //主队
            if(array_key_exists('team_1_id',$info)  && isset($data['output']['team_1_id'])){
                $info['team_1_id'] = $data['output']['team_1_id'];
            }else{
                $info['team_1_id'] = -1;
            }

            // 客队
            if(array_key_exists('team_2_id',$info) && isset($data['output']['team_2_id'])){
                $info['team_2_id'] = $data['output']['team_2_id'];
            }else{
                $info['team_2_id'] = -1;
            }

            // winner
            if (array_key_exists('winner', $info) && array_key_exists('winner', $data['output'])) {
                $teamId = HotBase::getMainIdByRelIdentityId('team', $data['output']['winner'], $info['origin_id'], $info['game']);
                if($teamId){
                    if ($teamId == $info['team_1_id']) {
                        $info['winner'] = 1;
                    }

                    if ($teamId == $info['team_2_id']) {
                        $info['winner'] = 2;
                    }

                }else{
                    unset($info['winner']);
                }
            }else{
                unset($info['winner']);

            }

            if(array_key_exists('map_ban_pick',$info) && array_key_exists('map_ban_pick',$data['output'])){
                if ($data['output']['map_ban_pick'] != null) {
                    $banPinkInfo = json_decode($data['output']['map_ban_pick'], true);
                    foreach ($banPinkInfo as $key => $value) {
                        if (isset($value["team"]) && $value['team']) {
                            $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM, $value['team'], $info['origin_id'], $info['game']);
                            if ($teamMasterId) {
                                $banPinkInfo[$key]['team'] = $teamMasterId;
                                $banPinkInfo[$key]['type'] = $value['type'];
                                $banPinkInfo[$key]['map'] = $value['map'];
                            } else {
                                $banPinkInfo[$key]['team'] = '';
                                $banPinkInfo[$key]['type'] = $value['type'];
                                $banPinkInfo[$key]['map'] = $value['map'];
                            }
                        } else {
                            $banPinkInfo[$key]['team'] = '';
                        }
                    }
                    $banPinkArray = json_encode($banPinkInfo);

                    $info['map_ban_pick'] = $banPinkArray;

                }else{
                    $info['map_ban_pick'] = $data['output']['map_ban_pick'];

                }

            }else{
                unset($info['map_ban_pick']);
            }


            // 默认领先战队 存1或者2或者null set时处理了
            if (array_key_exists('default_advantage', $info) && array_key_exists('default_advantage', $data['output'])) {
                $teamId = HotBase::getMainIdByRelIdentityId('team', $data['output']['default_advantage'], $info['origin_id'], $info['game']);
                if($teamId){
                    if ($teamId == $info['team_1_id']) {
                        $info['default_advantage'] = 1;
                    }

                    if ($teamId == $info['team_2_id']) {
                        $info['default_advantage'] = 2;
                    }

                }else{
                    unset($info['default_advantage']);
                }
            }else{
                unset($info['default_advantage']);

            }

            // 是否平局
            if(isset($info['draw']) && isset($data['output']['draw'])){
                $info['is_draw'] = $data['output']['draw'];
                unset($info['draw']);
            }else{
                $info['is_draw'] = 2;
                unset($info['draw']);
            }

            //是否弃权
            if(isset($info['forfeit']) && isset($data['output']['forfeit'])){
                $info['is_forfeit'] = $data['output']['forfeit'];
                unset($info['forfeit']);
            }else{
                $info['is_forfeit'] = 2;
                unset($info['forfeit']);
            }
            // 是否提供视频直播源
//            if(array_key_exists('is_streams_supported',$info) && isset($data['output']['is_streams_supported'])){
//                $info['is_streams_supported'] = $data['output']['is_streams_supported'];
//            }else{
//                unset($info['is_streams_supported']);
//            }

            // slug生成 主队id
            if(array_key_exists('team_1_id',$info) && array_key_exists('team_1_id',$data['output'])){
                $teamOneId = $data['output']['team_1_id'];
            }else{
                unset($info['team_1_id']);
            }
            // slug生成 客队id
            if(array_key_exists('team_2_id',$info) && array_key_exists('team_2_id',$data['output'])){
                $teamTwoId = $data['output']['team_2_id'];
            }else{
                unset($info['team_2_id']);
            }
            if(isset($teamOneId)&&isset($teamTwoId)){
                $info['slug'] = self::matchTeamSlugGenerate($teamOneId,$teamTwoId,$data['output']['scheduled_begin_at'],true);
            }else{
                throw new BusinessException([], "主队或客队Id不正确！");
            }

            if(array_key_exists('team_1_score',$info) && array_key_exists('team_1_score',$data['output'])){
                $info['team_1_score'] = $data['output']['team_1_score'];
            }else{
                unset($info['team_1_score']);
            }

            if(array_key_exists('team_2_score',$info) && array_key_exists('team_2_score',$data['output'])){
                $info['team_2_score'] = $data['output']['team_2_score'];
            }else{
                unset($info['team_2_score']);
            }
            // 战队比分
            if ($info['status'] == 1){
                $info['team_1_score'] = 0;
                $info['team_2_score'] = 0;
            }else{
                $info['team_1_score'] = intval($info['team_1_score']);
                $info['team_2_score'] = intval($info['team_2_score']);
            }
            //是否有对局数据详情
//            if(self::checkIsStringNull($info['is_battle_detailed']) || self::checkIsNumberZero($info['is_battle_detailed']) || !isset($info['is_battle_detailed'])){
//                $info['is_battle_detailed'] = 2;
//            }

            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }

            if(array_key_exists('end_at',$info) && array_key_exists('end_at',$data['output'])){
                $info['end_at'] = $data['output']['end_at'];
            }else{
                unset($info['end_at']);
            }

            if(array_key_exists('begin_at',$info) && array_key_exists('begin_at',$data['output'])){
                $info['begin_at'] = $data['output']['begin_at'];
            }else{
                unset($info['begin_at']);
            }


            if(array_key_exists('status',$info) && array_key_exists('status',$data['output'])){
                $info['status'] = $data['output']['status'];
            }else{
                unset($info['status']);
            }

            if ($info['number_of_games'] == 1 && $info['status'] == 3 && $info['origin_id'] == 7) {
                if ($info['team_1_score'] > $info['team_2_score']) {
                    $info['team_1_score'] = 1;
                    $info['team_2_score'] = 0;
                }elseif ($info['team_1_score'] < $info['team_2_score']) {
                    $info['team_1_score'] = 0;
                    $info['team_2_score'] = 1;
                }
            }

            if (empty($info['map_info'])){
                $info['map_info'] = MatchService::getMapLists($info['game'],'','normal',$info['number_of_games']);
            };


        }
        //lol英雄
        if($resourceType === self::METADATA_TYPE_LOL_CHAMPION){

            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
                if (empty($info['small_image'])) {
                    $info['small_image'] = $data['output']['image'];
                    $data['output']['small_image'] = $data['output']['image'];
                }
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }

            if(isset($info['small_image']) && isset($data['output']['small_image']) && !empty($data['output']['small_image'])){
                $info['small_image'] = $data['output']['small_image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['small_image'] = $enumGame['simple_image'];
            }

            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }

            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
        }

        //lol技能
        if($resourceType === self::METADATA_TYPE_LOL_ABILITY) {
            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }
            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];

            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }
            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }
          if(isset($info['champion_id']) && isset($data['output']['champion_id'])){
                $info['champion_id'] = $data['output']['champion_id'];
            }else{
                unset($info['champion_id']);
            }


            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
            }

        //lol道具
        if($resourceType === self::METADATA_TYPE_LOL_ITEM) {

            if(isset($info['state']) && isset($data['output']['state'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }

            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }

            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(isset($info['name_cn']) && isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }

             if(isset($info['total_cost']) && isset($data['output']['total_cost'])){
                $info['total_cost'] = $data['output']['total_cost'];
            }else{
                unset($info['total_cost']);
            }
            if(isset($info['is_trinket']) && isset($data['output']['is_trinket'])){
                $info['is_trinket'] = $data['output']['is_trinket'];
            }else{
                unset($info['is_trinket']);
            }

            if(isset($info['is_purchasable']) && isset($data['output']['is_purchasable'])){
                $info['is_purchasable'] = $data['output']['is_purchasable'];
            }else{
                unset($info['is_purchasable']);
            }


            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
        }
        //lol召唤师技能
        if ($resourceType === self::METADATA_TYPE_LOL_SUMMONER_SPELL) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(isset($info['name_cn']) && isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }

             if(isset($info['external_id']) && isset($data['output']['external_id'])){
                 $info['external_id'] = $data['output']['external_id'];
             }else{
                 unset($info['external_id']);
             }
             if(isset($info['state']) && isset($data['output']['state'])){
                 $info['state'] = $data['output']['state'];
             }else{
                 unset($info['state']);
             }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }

            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }
        //生成slug
        if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
            $info['slug'] = Common::makeConstantMatching($data['output']['name']);
        }
        // 是否删除
        if(isset($data['output']['deleted'])){
            if($data['output']['deleted'] == 1){
                $info['deleted_at']= date("Y-m-d H:i:s");
            }elseif ($data['output']['deleted'] == 2){
                $info['deleted_at'] = null;
            }
        }else{
            unset($info['deleted']);
        }
            }
        //lol符文
        if ($resourceType === self::METADATA_TYPE_LOL_RUNE) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(isset($info['path_name']) && isset($data['output']['path_name'])){
                $info['path_name'] = $data['output']['path_name'];
            }else{
                unset($info['path_name']);
            }
            if(isset($info['path_name_cn']) && isset($data['output']['path_name_cn'])){
                $info['path_name_cn'] = $data['output']['path_name_cn'];
            }else{
                unset($info['path_name_cn']);
            }
             if(isset($info['name_cn']) && isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }
            if(isset($info['state']) && isset($data['output']['state'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }
            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }


            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }
        }
        //daota英雄
        if ($resourceType === self::METADATA_TYPE_DOTA2_HERO) {

            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }

            if(array_key_exists('title_cn',$info) && isset($data['output']['title_cn'])){
                $info['title_cn'] = $data['output']['title_cn'];
            }else{
                unset($info['title_cn']);
            }


            if(array_key_exists('talents',$info) && array_key_exists('talents',$data['output'])){
                $info['talents'] = $data['output']['talents'];
            }else{
                unset($info['talents']);
            }
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }



            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];

            }else{
                $enumGame = EnumGame::find()->select('role_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['role_image'];
            }

            if(isset($info['small_image']) && isset($data['output']['small_image']) && !empty($data['output']['small_image'])){
                $info['small_image'] = $data['output']['small_image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['small_image'] = $enumGame['simple_image'];
            }


            if(isset($info['name']) && isset($data['output']['name'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(array_key_exists('name_cn',$info) && isset($data['output']['name_cn'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(isset($info['external_name']) && isset($data['output']['external_name'])){
                $info['external_name'] = $data['output']['external_name'];
            }else{
                unset($info['external_name']);
            }
            if(isset($info['title']) && isset($data['output']['title'])){
                $info['title'] = $data['output']['title'];
            }else{
                unset($info['title']);
            }

            if(array_key_exists('title_cn',$info) && isset($data['output']['title_cn'])){
                $info['title_cn'] = $data['output']['title_cn'];
            }else{
                unset($info['title_cn']);
            }

            if(array_key_exists('primary_attr',$info) && array_key_exists('primary_attr',$data['output'])){
                $info['primary_attr'] = $data['output']['primary_attr'];
            }else{
                unset($info['primary_attr']);
            }
            if(array_key_exists('primary_attr_cn',$info) && array_key_exists('primary_attr_cn',$data['output'])){
                $info['primary_attr_cn'] = $data['output']['primary_attr_cn'];
            }else{
                unset($info['primary_attr_cn']);
            }
            if(array_key_exists('roles',$info) && array_key_exists('roles',$data['output'])){
                $info['roles'] = $data['output']['roles'];
            }else{
                unset($info['roles']);
            }
            if(array_key_exists('roles_cn',$info) && array_key_exists('roles_cn',$data['output'])){
                $info['roles_cn'] = $data['output']['roles_cn'];
            }else{
                unset($info['roles_cn']);
            }

            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }

            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
        //daota技能
        if ($resourceType === self::METADATA_TYPE_DOTA2_ABILITY) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }
            if(isset($info['hero_id']) && isset($data['output']['hero_id'])){
                $info['hero_id'] = $data['output']['hero_id'];
            }else{
                unset($info['hero_id']);
            }

//            if(isset($info['hotkey']) && isset($data['output']['hotkey'])){
//                $info['hotkey'] = $data['output']['hotkey'];
//            }else{
//                unset($info['hotkey']);
//            }


            if(array_key_exists('name',$info) && array_key_exists('name',$data['output'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(array_key_exists('name_cn',$info) && array_key_exists('name_cn',$data['output'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }
            if(array_key_exists('hotkey',$info) && array_key_exists('hotkey',$data['output'])){
                $info['hotkey'] = $data['output']['hotkey'];
            }else{
                unset($info['hotkey']);
            }

            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }


            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }
            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }
            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }


        }
        //daota道具
        if ($resourceType === self::METADATA_TYPE_DOTA2_ITEM) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('item_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['item_image'];
            }

            if(array_key_exists('name',$info) && array_key_exists('name',$data['output'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(array_key_exists('name_cn',$info) && array_key_exists('name_cn',$data['output'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }


            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }


            if(array_key_exists('total_cost',$info) && array_key_exists('total_cost',$data['output'])){
                $info['total_cost'] = $data['output']['total_cost'];
            }else{
                unset($info['total_cost']);
            }

            if(array_key_exists('is_recipe',$info) && array_key_exists('is_recipe',$data['output'])){
                $info['is_recipe'] = $data['output']['is_recipe'];
            }else{
                unset($info['is_recipe']);
            }

            if(array_key_exists('is_secret_shop',$info) && array_key_exists('is_secret_shop',$data['output'])){
                $info['is_secret_shop'] = $data['output']['is_secret_shop'];
            }else{
                unset($info['is_secret_shop']);
            }

            if(array_key_exists('is_home_shop',$info) && array_key_exists('is_home_shop',$data['output'])){
                $info['is_home_shop'] = $data['output']['is_home_shop'];
            }else{
                unset($info['is_home_shop']);
            }
         if(array_key_exists('is_neutral_drop',$info) && array_key_exists('is_neutral_drop',$data['output'])){
                $info['is_neutral_drop'] = $data['output']['is_neutral_drop'];
            }else{
                unset($info['is_neutral_drop']);
            }


            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }
            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(isset($info['external_name']) && isset($data['output']['external_name'])){
                $info['external_name'] = $data['output']['external_name'];
            }else{
                unset($info['external_name']);
            }

            if(array_key_exists('description',$info) && array_key_exists('description',$data['output'])){
                $info['description'] = $data['output']['description'];
            }else{
                unset($info['description']);
            }

            if(array_key_exists('description_cn',$info) && array_key_exists('description_cn',$data['output'])){
                $info['description_cn'] = $data['output']['description_cn'];
            }else{
                unset($info['description_cn']);
            }

        }
        //data天赋
        if ($resourceType === self::METADATA_TYPE_DOTA2_TALENT) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['external_name']) || $info['external_name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['hero_id']) && isset($data['output']['hero_id'])){
                $info['hero_id'] = $data['output']['hero_id'];
            }else{
                unset($info['hero_id']);
            }

            if(array_key_exists('level',$info) && array_key_exists('level',$data['output'])){
                $info['level'] = $data['output']['level'];
            }else{
                unset($info['level']);
            }

            if(array_key_exists('left_right',$info) && array_key_exists('left_right',$data['output'])){
                $info['left_right'] = $data['output']['left_right'];
            }else{
                unset($info['left_right']);
            }
            if(array_key_exists('talent',$info) && array_key_exists('talent',$data['output'])){
                $info['talent'] = $data['output']['talent'];
            }else{
                unset($info['talent']);
            }
            if(array_key_exists('talent_cn',$info) && array_key_exists('talent_cn',$data['output'])){
                $info['talent_cn'] = $data['output']['talent_cn'];
            }else{
                unset($info['talent_cn']);
            }
            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }
            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }
            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(isset($info['external_name']) && isset($data['output']['external_name'])){
                $info['external_name'] = $data['output']['external_name'];
            }else{
                unset($info['external_name']);
            }

            //生成slug
            if(isset($info['external_name']) && isset($data['output']['external_name']) && $data['output']['external_name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['external_name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
        //CSGO 地图
        if ($resourceType === self::METADATA_TYPE_CSGO_MAP) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }

            if(isset($info['deleted_at'])){
                unset($info['map_type_cn']);
            }

            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }
            if(isset($info['square_image']) && isset($data['output']['square_image']) && !empty($data['output']['square_image'])){
                $info['square_image'] = $data['output']['square_image'];
            }else{
                $enumGame = EnumGame::find()->select('square_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['square_image'] = $enumGame['square_image'];
            }

            if(isset($info['rectangle_image']) && isset($data['output']['rectangle_image']) && !empty($data['output']['rectangle_image'])){
                $info['rectangle_image'] = $data['output']['rectangle_image'];
            }else{
                $enumGame = EnumGame::find()->select('rectangle_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['rectangle_image'] = $enumGame['rectangle_image'];
            }
            if(isset($info['thumbnail']) && isset($data['output']['thumbnail']) && !empty($data['output']['thumbnail'])){
                $info['thumbnail'] = $data['output']['thumbnail'];
            }else{
                $enumGame = EnumGame::find()->select('thumbnail')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['thumbnail'] = $enumGame['thumbnail'];
            }


            if(array_key_exists('name',$info) && array_key_exists('name',$data['output'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(array_key_exists('name_cn',$info) && array_key_exists('name_cn',$data['output'])){
                $info['name_cn'] = $data['output']['name_cn'];
            }else{
                unset($info['name_cn']);
            }

            if(array_key_exists('short_name',$info) && array_key_exists('short_name',$data['output'])){
                $info['short_name'] = $data['output']['short_name'];
            }else{
                unset($info['short_name']);
            }
             if(array_key_exists('map_type',$info) && array_key_exists('map_type',$data['output'])){
                $info['map_type'] = $data['output']['map_type'];
                 $info['map_type_cn'] = $data['output']['map_type'];

            }else{
                unset($info['map_type']);
            }

            if(array_key_exists('is_default',$info) && array_key_exists('is_default',$data['output'])){
                $info['is_default'] = $data['output']['is_default'];
            }else{
                unset($info['is_default']);
            }

            if(array_key_exists('slug',$info) && array_key_exists('slug',$data['output'])){
                $info['slug'] = $data['output']['slug'];
            }else{
                unset($info['slug']);
            }

            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }


            if(isset($info['external_id']) && isset($data['output']['external_id'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(isset($info['external_name']) && isset($data['output']['external_name'])){
                $info['external_name'] = $data['output']['external_name'];
            }else{
                unset($info['external_name']);
            }

            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }
            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
        //CSGO武器
        if ($resourceType === self::METADATA_TYPE_CSGO_WEAPON) {
            if(isset($info['deleted_at'])){
                unset($info['deleted_at']);
            }
            if (empty($info['name']) || $info['name'] === '') {
                throw new BusinessException([], "名称（英文）为null或空字符，不允许新增");
            }

            if(isset($info['image']) && isset($data['output']['image']) && !empty($data['output']['image'])){
                $info['image'] = $data['output']['image'];
            }else{
                $enumGame = EnumGame::find()->select('simple_image')->where(['id'=> $info['game_id']])->asArray()->one();
                $info['image'] = $enumGame['simple_image'];
            }

            if(array_key_exists('name',$info) && array_key_exists('name',$data['output'])){
                $info['name'] = $data['output']['name'];
            }else{
                unset($info['name']);
            }

            if(array_key_exists('slug',$info) && array_key_exists('slug',$data['output'])){
                $info['slug'] = $data['output']['slug'];
            }else{
                unset($info['slug']);
            }
            if(array_key_exists('kind',$info) && array_key_exists('kind',$data['output'])){
                $info['kind'] = $data['output']['kind'];
            }else{
                unset($info['kind']);
            }
            if(array_key_exists('state',$info) && array_key_exists('state',$data['output'])){
                $info['state'] = $data['output']['state'];
            }else{
                unset($info['state']);
            }
            if(array_key_exists('cost',$info) && array_key_exists('cost',$data['output'])){
                $info['cost'] = $data['output']['cost'];
            }else{
                unset($info['cost']);
            }

            if(array_key_exists('kill_award',$info) && array_key_exists('kill_award',$data['output'])){
                $info['kill_award'] = $data['output']['kill_award'];
            }else{
                unset($info['kill_award']);
            }

            if(array_key_exists('ammunition',$info) && array_key_exists('ammunition',$data['output'])){
                $info['ammunition'] = $data['output']['ammunition'];
            }else{
                unset($info['ammunition']);
            }



            if(array_key_exists('capacity',$info) && array_key_exists('capacity',$data['output'])){
                $info['capacity'] = $data['output']['capacity'];
            }else{
                unset($info['capacity']);
            }


            if(array_key_exists('movement_speed',$info) && array_key_exists('movement_speed',$data['output'])){
                $info['movement_speed'] = $data['output']['movement_speed'];
            }else{
                unset($info['movement_speed']);
            }






            if(array_key_exists('reload_time',$info) && array_key_exists('reload_time',$data['output'])){
                $info['reload_time'] = $data['output']['reload_time'];
            }else{
                unset($info['reload_time']);
            }

            if(array_key_exists('is_ct_available',$info) && array_key_exists('is_ct_available',$data['output'])){
                $info['is_ct_available'] = $data['output']['is_ct_available'];
            }else{
                unset($info['is_ct_available']);
            }

            if(array_key_exists('is_t_available',$info) && array_key_exists('is_t_available',$data['output'])){
                $info['is_t_available'] = $data['output']['is_t_available'];
            }else{
                unset($info['is_t_available']);
            }

            if(array_key_exists('firing_mode',$info) && array_key_exists('firing_mode',$data['output'])){
                $info['firing_mode'] = $data['output']['firing_mode'];
            }else{
                unset($info['firing_mode']);
            }

            if(array_key_exists('slug',$info) && array_key_exists('slug',$data['output'])){
                $info['slug'] = $data['output']['slug'];
            }else{
                unset($info['slug']);
            }

            if(array_key_exists('external_id',$info) && array_key_exists('external_id',$data['output'])){
                $info['external_id'] = $data['output']['external_id'];
            }else{
                unset($info['external_id']);
            }

            if(array_key_exists('external_name',$info) && array_key_exists('external_name',$data['output'])){
                $info['external_name'] = $data['output']['external_name'];
            }else{
                unset($info['external_name']);
            }

            if(array_key_exists('flag',$info) && array_key_exists('flag',$data['output'])){
                $info['flag'] = $data['output']['flag'];
            }else{
                unset($info['flag']);
            }

            //生成slug
            if(isset($info['name']) && isset($data['output']['name']) && $data['output']['name']){
                $info['slug'] = Common::makeConstantMatching($data['output']['name']);
            }
            // 是否删除
            if(isset($data['output']['deleted'])){
                if($data['output']['deleted'] == 1){
                    $info['deleted_at']= date("Y-m-d H:i:s");
                }elseif ($data['output']['deleted'] == 2){
                    $info['deleted_at'] = null;
                }
            }else{
                unset($info['deleted']);
            }

        }
            return $info;
    }
    // slug生成
    public static function matchTeamSlugGenerate($teamOneId,$teamTwoId,$dateTime,$isTrue = false){
        if($isTrue){
            if(!$dateTime){
                throw new BusinessException([], "计划开始时间不允许为空");
            }
        }
        if($teamOneId==-1){
            $teamOneName = 'TBD';
        }else{
            $teamOneName = self::teamIdGetTeamName($teamOneId);
        }
        if($teamTwoId==-1){
            $teamTwoName = 'TBD';
        }else{
            $teamTwoName = self::teamIdGetTeamName($teamTwoId);
        }
        // 提取计划开始时间
        $scheduledDate = date('Y-m-d', strtotime(date($dateTime)));
        return $teamOneName."_"."vs"."_".$teamTwoName."_".$scheduledDate;
    }
    // team简称
    public static function teamIdGetTeamName($teamId){
        $team = Team::find()->select('name')->where(['id'=>$teamId])->asArray()->one();
        return Common::makeConstantMatching($team['name']);
    }
    public static function isSetFieldConfig($field){
        // 设置可以为空的字段
        $configFieldArray = [
//            'game',
//            'game_id',
//            'game_rules',
            'match_type',
            'number_of_games', // 赛制局数
//            'tournament_id', // 赛事id
//            'group_id', // 分组id
            'scheduled_begin_at', // 计划开始时间
//            'original_scheduled_begin_at', // 原始计划开始时间
            'is_rescheduled', // 是否调整了时间表
            'team_1_id', // 主队战队id
            'team_2_id', // 客队id
            'deleted', // deleted
            'deleted_at', // deleted_at

//            'slug', // slug
            'relation', // relation
            'location', // 比赛地点英文
            'location_cn', // 比赛地点中文
            'round_order', // 轮次
            'round_name', // 轮次名称
            'round_name_cn', // 轮次名称中文
//            'bracket_pos', // 对阵表位置
            'description', // 比赛简介英文
            'description_cn', // 比赛简介中文

            'team_1_score', // 主队
            'team_2_score', // 客队
            'map_ban_pick', // 地图BP
            'map_info', // 比赛地图
            'begin_at', // 实际开始时间
            'end_at', // 实际结束时间
            'status', // 比赛状态
            'is_draw', // 是否平局
            'is_forfeit', // 是否弃权
            'winner', // 获胜战队
            'default_advantage', // 默认领先战队
            'is_battle_detailed', // 是否有对局详情
            'is_pbpdata_supported', // 是否提供实时数据
            'is_streams_supported', // 是否提供视频直播源
            'battle_forfeit', // 是否提供视频直播源
            'has_incident_report', // 是否提供视频直播源
            'game_version'
        ];
        return in_array($field,$configFieldArray);
    }

    public static function getTeamIdByOrder($standardMathInfoId,$winnerId){

        $standardInfo = StandardDataMatch::find()->select(['team_1_id','team_2_id'])->where(['id'=> $standardMathInfoId])->asArray()->one();
        if($standardInfo['team_1_id']==$winnerId && $standardInfo['team_1_id']){
            $winnerOrder = 1;
        }elseif ($standardInfo['team_2_id']==$winnerId && $standardInfo['team_2_id']){
            $winnerOrder = 2;
        }else{
            $winnerOrder = null;
        }
        return $winnerOrder;
    }
}