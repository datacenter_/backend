<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/06
 * Time: 22:13
 */
namespace app\modules\task\services\api;

use Yii;

class ApiBase
{
    // redis前缀
    const PREFIX_API = 'Api';
    const Symbol = ':';
    // es索引名称
    const MATCH_ES_INDEX = 'resource_match_list';
    const ES_MATCHES_ES_INDEX = 'resource_matches_list';
    const BATTLE_ES_INDEX = 'resource_battle_list';
    const GAME_ES_INDEX = 'resource_game_list';
    const ORGANIZATION_ES_INDEX = 'resource_organization_list';
    const TEAM_ES_INDEX = 'resource_team_list';
    const PLAYER_ES_INDEX = 'resource_player_list';
    const TOURNAMENT_ES_INDEX = 'resource_tournament_list';
    const CSGO_MAP_ES_INDEX = 'resource_csgomap_list';
    const CSGO_WEAPON_ES_INDEX = 'resource_weapon_list';
    const DOTA_HERO_ES_INDEX = 'resource_hero_list';
    const DOTA_ITEM_ES_INDEX = 'resource_dotaitem_list';
    const LOL_CHAMPION_ES_INDEX = 'resource_champion_list';
    const LOL_ITEM_ES_INDEX = 'resource_item_list';
    const LOL_SUMMONERSPELL_ES_INDEX = 'resource_summonerspell_list';
    const LOL_RUNE_ES_INDEX = 'resource_rune_list';
    const EXPIRES_AT = 1200;

    /**
     * 排序
     */
    public static function sortArrayByField($data, $field, $order = SORT_ASC)
    {
        $keys = array_keys($data);
        $array_column = array_column($data, $field);
        array_multisort(
            $array_column, $order, SORT_NUMERIC, $data, $keys
        );
        $data = array_combine($keys, $data);
        $currentData = array_merge($data);
        return $currentData;
    }

    /**
     * 递归空字符串转null
     */
    public static function convertNull($fieldArray)
    {
        if($fieldArray !== ''){
            if(is_array($fieldArray)){
                if(!empty($fieldArray)){
                    foreach($fieldArray as $key => $value){
                        if($value === ''){
                            $fieldArray[$key] = null;
                        }else{
                            $fieldArray[$key] = self::convertNull($value);
                        }
                        if(strstr($value,'img.elementsdata.cn') || strstr($value,'img.elementsdata.cn')){
                            $fieldArray[$key] = self::picture_address($value);
                        }
                    }
                }else{
                    $fieldArray = [];
                }
            }else{
                if (strstr($fieldArray,'img.elementsdata.cn') || strstr($fieldArray,'img.elementsdata.cn')) {
                    $fieldArray = self::picture_address($fieldArray);
                }
                if($fieldArray === '') {
                    $fieldArray = null;
                }
            }
        }else{
            $fieldArray = null;
        }
        return $fieldArray;
    }
    /**
     * 图片url处理
     */
    public static function picture_address($imageUrl)
    {
        $currentImage = null;
        $imageInfo = parse_url($imageUrl);
        if (\Yii::$app->params['refresh_key']) {
            if ($imageInfo['scheme']) {
                $imageInfo['scheme'] = 'https';
                $imageInfo['host']   = 'img.elements-data.com';
                $currentImage = un_parse_url($imageInfo);
            }
        } else {
            if ($imageInfo['scheme']) {
                $imageInfo['scheme'] = 'https';
                $imageInfo['host']   = 'img.elementsdata.cn';
                $currentImage = un_parse_url($imageInfo);
            }
        }
        return $currentImage;
    }
    /**
     *
     * Es - 设置
     *
     */
    public static function settingsEs(){
        return [
            'refresh_interval' => '1s',
            'number_of_shards' => 1,
            'number_of_replicas' => 1,
            'max_result_window' => 200000000,
            'analysis' => [
                'normalizer' => [
                    'my_normalizer' => [
                        'filter' => 'lowercase',
                        'type' => 'custom',
                    ]
                ]
            ]
        ];
    }
    /**
     * Redis
     * 封装方法
     */
    // key-value 存值
    public static function valueSet($key, $value, $overdue=false)
    {
        $keyName = self::handleKey($key);
        self::getRedis()->set($keyName,$value);
        if($overdue){
            self::getRedis()->expire($keyName, self::EXPIRES_AT);
        }
        return;
    }
    // key-value 取值
    public static function valueGet($key)
    {
        $keyName = self::handleKey($key);
        return self::getRedis()->get($keyName);
    }
    // hash 表中的字段赋值。成功返回1，失败返回0。若user表不存在会先创建表再赋值，若字段已存在会覆盖旧值。
    public static function hashSet($key, $field, $value, $overdue=false){
        $keyName = self::handleKey($key);
        self::getRedis()->hset($keyName,$field,$value);
        if($overdue){
            self::getRedis()->expire($keyName, self::EXPIRES_AT);
        }
        return;
    }
    // 删除hash表某条数据
    public static function hashDelKey($key,$battleId){
        $keyName = self::handleKey($key);
        return self::getRedis()->hdel($keyName,$battleId);
    }
    // hash 获取表中指定字段的值。若表不存在则返回false。
    public static function hashGet($key, $field){
        $keyName = self::handleKey($key);
        return self::getRedis()->hget($keyName,$field);
    }
    // 删除 redis-key
    public static function redisDelKey($key){
        $keyName = self::handleKey($key);
        return self::getRedis()->del($keyName);
    }
    // hash 获取某个表中所有的字段和值。
    public static function hashGetAll($key){
        $keyName = self::handleKey($key);
        return self::getRedis()->hgetall($keyName);
    }
    // redis - Key-Name
    public static function handleKey($keyArray)
    {
        self::getRedis()->select(env('ES_REDIS_DATABASE'));
        $keyString = implode(":", $keyArray);
        return self::PREFIX_API.self::Symbol.sprintf("%s", $keyString);
    }
    // 切换连接redis
    private static function getRedis()
    {
        if (\Yii::$app->params['refresh_key'] == "Singapore") {
            return Yii::$app->redis_spe;
        } else {
            return Yii::$app->redis;
        }
    }

//    // 删除redis Key (debug用)
//    public static function delRedis($key){
//        Yii::$app->redis->select(env('ES_REDIS_DATABASE'));
//        return Yii::$app->redis->del($key);
//    }
//    // 搜索key (debug用)
//    public static function searchKey($key){
//        Yii::$app->redis->select(env('ES_REDIS_DATABASE'));
//        $keyString = implode(":", $key);
//        $keyName = sprintf("%s", $keyString);
//        return Yii::$app->redis->keys("*{$keyName}*");
//    }
}
