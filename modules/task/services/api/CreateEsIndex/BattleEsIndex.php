<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class BattleEsIndex
{
    public static function structure(){
        $indexParam = [
            'index' => ApiBase::BATTLE_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'battle_id' => [
                            'type' => 'integer'
                        ],
                        'battle_order' => [
                            'type' => 'integer'
                        ],
                        'match_id' => [
                            'type' => 'integer'
                        ],
//                        'game_id' => [
//                            'type' => 'integer'
//                        ],
                        'match_status' => [
                            'type' => 'keyword'
                        ],
                        'match_delay' => [
                            'type' => 'integer'
                        ],
                        'match_end_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::BATTLE_ES_INDEX,$indexParam);
    }
}