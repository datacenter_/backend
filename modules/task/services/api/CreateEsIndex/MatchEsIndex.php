<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class MatchEsIndex
{
    public static function structure(){
        $indexParam = [
            'index' => ApiBase::MATCH_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'match_id' => [
                            'type' => 'integer'
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'delay' => [
                            'type' => 'integer'
                        ],
                        'game_id' => [
                            'type' => 'integer'
                        ],
                        'game_rules' => [
                            'type' => 'keyword'
                        ],
                        'match_type' => [
                            'type' => 'keyword'
                        ],
                        'number_of_games' => [
                            'type' => 'integer'
                        ],
                        'tournament_id' => [
                            'type' => 'integer'
                        ],
                        'group_id' => [
                            'type' => 'integer'
                        ],
                        'team_id' => [
                            'type' => 'long'
                        ],
                        'team_snapshot_name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 2048,
                                ]
                            ]
                        ],
                        'status' => [
                            'type' => 'keyword'
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'is_battle_detailed' => [
                            'type' => 'keyword'
                        ],
                        'is_pbpdata_supported' => [
                            'type' => 'keyword'
                        ],
                        'is_streams_supported' => [
                            'type' => 'keyword'
                        ],
                        'scheduled_begin_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                        'begin_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                        'end_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::MATCH_ES_INDEX,$indexParam);
    }
}