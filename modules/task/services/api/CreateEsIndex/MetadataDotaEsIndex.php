<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class MetadataDotaEsIndex
{
    public static function structureHero(){
        $indexParam = [
            'index' => ApiBase::DOTA_HERO_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'hero_id' => [
                            'type' => 'integer'
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'status' => [
                            'type' => 'keyword'
                        ],
                        'name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'title' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'external_id' => [
                            'type' => 'keyword'
                        ],
                        'external_name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::DOTA_HERO_ES_INDEX,$indexParam);
    }
    public static function structureItem(){
        $indexParam = [
            'index' => ApiBase::DOTA_ITEM_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'item_id' => [
                            'type' => 'integer'
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'status' => [
                            'type' => 'keyword'
                        ],
                        'name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'external_id' => [
                            'type' => 'keyword'
                        ],
                        'external_name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::DOTA_ITEM_ES_INDEX,$indexParam);
    }
}