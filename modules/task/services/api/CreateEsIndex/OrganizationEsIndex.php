<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class OrganizationEsIndex
{
    public static function structure(){
        $indexParam = [
            'index' => ApiBase::ORGANIZATION_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'organization_id' => [
                            'type' => 'integer'
                        ],
                        'name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'alias' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::ORGANIZATION_ES_INDEX,$indexParam);
    }
}