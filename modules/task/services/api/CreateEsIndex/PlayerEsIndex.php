<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class PlayerEsIndex
{
    public static function structure(){
        $indexParam = [
            'index' => ApiBase::PLAYER_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'player_id' => [
                            'type' => 'integer'
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'nick_name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'steam_id' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'game_id' => [
                            'type' => 'integer'
                        ],
                        'team_id' => [
                            'type' => 'long'
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis'
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::PLAYER_ES_INDEX,$indexParam);
    }
}