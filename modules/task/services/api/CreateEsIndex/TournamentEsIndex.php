<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/21
 * Time: 18:13
 */

namespace app\modules\task\services\api\CreateEsIndex;

use app\modules\task\services\api\ApiBase;
use app\modules\common\services\EsService;

class TournamentEsIndex
{
    public static function structure(){
        $indexParam = [
            'index' => ApiBase::TOURNAMENT_ES_INDEX,
            'body' => [
                'settings' => ApiBase::settingsEs(),
                'mappings' => [
                    "properties" => [
                        'tournament_id' => [
                            'type' => 'integer'
                        ],
                        'deleted' => [
                            'type' => 'integer'
                        ],
                        'name' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'game_id' => [
                            'type' => 'integer'
                        ],
                        'status' => [
                            'type' => 'keyword'
                        ],
                        'slug' => [
                            'type' => 'keyword',
                            'normalizer' => 'my_normalizer',
                        ],
                        'scheduled_begin_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis',
                        ],
                        'modified_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || epoch_millis',
                        ],
                    ]
                ],
            ]
        ];
        EsService::initConnect()->createEsIndex(ApiBase::TOURNAMENT_ES_INDEX,$indexParam);
    }
}