<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\models\EnumGame;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleTeam;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataDota2Map;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\org\models\TeamSnapshot;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\task\services\api\GetApi\GamesHandleLogic\CsgoHandleLogic;
use app\modules\task\services\api\GetApi\GamesHandleLogic\LolHandleLogic;
use app\modules\task\services\api\GetApi\GamesHandleLogic\DotaHandleLogic;

class BattleService
{
    const GAME_STATUS_CSGO = 1;
    const GAME_STATUS_LOL = 2;
    const GAME_STATUS_DOTA2 = 3;

    public static function getBattleList($match_id)
    {
        $battleIds = MatchBattle::find()->select(['id'])->where(['match'=>$match_id,'deleted'=>2])->asArray()->all();
        $matchBattles = [];
        foreach ($battleIds as $k => $battleVal) {
            $battle = self::getBattleDetail($battleVal['id']);
            if(!isset($battle['list'])){
                $matchBattles[] = $battle;
            }
        }
        return [
            'count' => count($matchBattles),
            'list' => array_values(array_filter($matchBattles)),
        ];
    }

    // new对局详情信息
    public static function getBattleDetail($battleId)
    {
        $battle = MatchBattle::find()->alias('battle')
            ->where(['battle.id' => $battleId,'battle.deleted' => 2])
            ->leftJoin('enum_match_state as battle_status', 'battle_status.id = battle.status')
            ->select(self::getBattleColumns())->asArray()->one();
        if (empty($battle)) return [];

        // battle所属比赛
        $match = self::getMatchData($battle['match']);
        // 主队客队
        $teamIds = [
            'team_1_id' => isset($match['team_1_id']) ? $match['team_1_id']:null,
            'team_2_id' => isset($match['team_2_id']) ? $match['team_2_id']:null,
        ];
        unset($match['team_1_id']);
        unset($match['team_2_id']);
        // battle所属比赛
        $battle['match'] = !empty($match) ? $match : [];
        // match游戏ID
        $gameId = !empty($battle['match']['game_id']) ? $battle['match']['game_id'] : null;
        // battle下游戏
        $battle['game'] = self::getGameData($gameId);
        // 赛事ID
        $tournamentId = !empty($battle['match']) ? $battle['match']['tournament_id'] : null;
        // 分组ID
        $groupId = !empty($battle['match']) ? $battle['match']['group_id'] : null;
        unset($battle['match']['tournament_id']);
        unset($battle['match']['group_id']);
        // battle参赛战队
        $battle['teams'] = self::getBattleTeamsData($teamIds, $tournamentId, $groupId);
        // 时间
        $battle['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['begin_at']);
        $battle['end_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['end_at']);
        // 是否平局
        $battle['is_draw'] = ConversionCriteria::issetTrueFalseType($battle['is_draw']);
        // 是否弃权
        $battle['is_forfeit'] = ConversionCriteria::issetTrueFalseType($battle['is_forfeit']);
        // 是否默认领先
        $battle['is_default_advantage'] = ConversionCriteria::issetTrueFalseType($battle['is_default_advantage']);
        // battle地图
        $battle['map'] = self::getBattleMapData($battle['map'], $gameId);
        unset($battle['map']['status']);
        unset($battle['map']['modified_at']);
        unset($battle['map']['created_at']);
        unset($battle['map']['deleted_at']);
        // battle比分
        $battle['scores'] = self::getBattleScoresData($battleId);
        // battle获胜战队
        if($battle['winner']){
            $battleWinnerCre = self::getBattleWinnerData($teamIds,$battle['winner'],$tournamentId,$groupId);
            $battleWinner = $battleWinnerCre;
            unset($battleWinner['team_snapshot']);
        }else{
            $battleWinnerCre = null;
            $battleWinner = null;
        }
        $battle['winner'] = $battleWinner;
        // 是否有详情数据
        $battle['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($battle['is_battle_detailed']);
        // 对局时长
        $battle['duration'] = intval($battle['duration']);
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // battle - cogo对局详情
                $battle['battle_detail'] = CsgoHandleLogic::getCsgoBattleDetail($battleWinnerCre,$battleId,$battle['duration'],$teamIds);
                break;
            case self::GAME_STATUS_LOL:
                // 战队和地图
                $battleEventTeamsAndMap = self::handelEventToTeamAndMap($battle['teams'],$battle['map']);
                // battle - LOL对局详情
                $battle['battle_detail'] = LolHandleLogic::getLolBattleDetail(
                    $battleWinnerCre,$battleId,$teamIds,$battle['duration'],$battle['status'],$battleEventTeamsAndMap['event_map'],$battleEventTeamsAndMap['event_teams']
                );
                break;
            case self::GAME_STATUS_DOTA2:
                // 战队和地图
                $battleEventTeamsAndMap = self::handelEventToTeamAndMap($battle['teams'],$battle['map']);
                // battle - Dota对局详情
                $battle['battle_detail'] = DotaHandleLogic::getDotaBattleDetail(
                    $battleWinnerCre,$battleId,$battle['duration'],$battle['status'],$battleEventTeamsAndMap['event_map'],$battleEventTeamsAndMap['event_teams']
                );
                break;
            default:
                $battle['battle_detail'] = null;
                break;
        }
        unset($battle['match']['game_id']);
        $battle['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['modified_at']);
        $battle['created_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['created_at']);
        $battle['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['deleted_at']);
        return $battle;
    }

    // 处理event里面用的战队和地图
    private static function handelEventToTeamAndMap($battleTeams,$battleMap)
    {
        // 拼接战队信息
        $battleEventsTeams = [];
        foreach ($battleTeams as &$bTeam){
            $battleEventsTeams[$bTeam['team_id']] = [
                'team_id' => $bTeam['team_id'],
                'name' => $bTeam['team_snapshot']['name'],
                'image' => $bTeam['team_snapshot']['image'],
                'opponent_order' => $bTeam['opponent_order'],
            ];
        }
        return [
            'event_teams' => $battleEventsTeams,
            'event_map' => $battleMap,
        ];
    }

    private static $gameArray = [];
    // 获取游戏数据
    private static function getGameData($gameId)
    {
        if(isset(self::$gameArray[$gameId])){
        }else {
            $game = EnumGame::find()->alias('game')->select(self::getGameColumns())
                ->leftJoin('enum_game_rules as rules', 'game.default_game_rule = rules.id')
                ->leftJoin('enum_game_match_type as type', 'game.default_match_type = type.id')
                ->where(["game.id" => $gameId])->asArray()->one();
            if (!empty($game)) {
                $game['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($game['modified_at']);
                $game['created_at'] = ConversionCriteria::DataTimeConversionCriteria($game['created_at']);
                $game['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($game['deleted_at']);
            }else{
                return null;
            }
            self::$gameArray[$gameId] = $game;
        }
        return self::$gameArray[$gameId];
    }

    // 获取完整比赛数据
    private static function getMatchData($matchId)
    {
        $match = Match::find()->alias('match')->select(self::getMatchColumns())
            ->leftJoin('match_real_time_info as match_r', 'match.id = match_r.id')
            ->leftJoin('enum_match_state as match_status', 'match_status.id = match_r.status')
            ->leftJoin('enum_game_rules as rules', 'match.game_rules = rules.id')
            ->leftJoin('enum_game_match_type as type', 'match.match_type = type.id')
            ->where(["match.id" => $matchId])
            ->asArray()->one();
        if (empty($match)) return null;
        $gameId = $match['game'];
        $match['game'] = self::getGameData($match['game']);
        $match['tournament'] = self::getTournamentData($match['tournament']);
        $match['group'] = self::getGroupData($match['group'],$gameId);
        // 计划开始时间 原计划开始时间
        $match['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['scheduled_begin_at']);
        $match['original_scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['original_scheduled_begin_at']);
        // 参赛战队与比分 共同数组
        $teams = [
            [
                'team_id' => $match['team_1_id'],
                'score' => $match['team_1_score'],
            ],
            [
                'team_id' => $match['team_2_id'],
                'score' => $match['team_2_score'],
            ]
        ];
        // 是否调整时间表
        $match['is_rescheduled'] = ConversionCriteria::issetTrueFalseType($match['is_rescheduled']);
        // 参赛战队
        $match['teams'] = self::getTeamsData($teams, $score = false, $match['tournament_id'], $match['group_id'],true);
        // 是否平局
        $match['is_draw'] = ConversionCriteria::issetTrueFalseType($match['is_draw']);
        // 是否弃权
        $match['is_forfeit'] = ConversionCriteria::issetTrueFalseType($match['is_forfeit']);
        // 默认领先战队
        $match['default_advantage'] = $match['default_advantage'] == '' ? null : self::getAdvantage($match['team_' . $match['default_advantage'] . '_id'], $match['default_advantage'], $match['tournament_id'], $match['group_id']);
        // 地图禁用
        $map_ban_pick = ConversionCriteria::issetJsonField($match['map_ban_pick']);
        $match['map_vetoes'] = $match['map_ban_pick'] ? self::getMapVetoesData($map_ban_pick,$match['team_1_id'],$match['team_2_id'],$gameId) : [];
        // 比赛地图
        $map_info = ConversionCriteria::issetJsonField($match['map_info']);
        $match['maps'] = $match['map_info'] ? self::getMatchMapsData($map_info, $gameId) : [];
        // 比分
        $match['scores'] = self::getTeamsData($teams, $score = true, $match['tournament_id'], $match['group_id'],false);
        // 获胜战队
        $match['winner'] = $match['winner'] > 0 ? self::getAdvantage($match['team_' . $match['winner'] . '_id'], $match['winner'], $match['tournament_id'], $match['group_id']) : [];
        // 是否有对局详情
        $match['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($match['is_battle_detailed']);

        $match['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['begin_at']);
        $match['end_at'] = ConversionCriteria::DataTimeConversionCriteria($match['end_at']);
        $match['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($match['modified_at']);
        $match['created_at'] = ConversionCriteria::DataTimeConversionCriteria($match['created_at']);
        $match['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($match['deleted_at']);
        unset($match['team_1_score']);
        unset($match['team_2_score']);
        unset($match['rules_id']);
        unset($match['type_id']);
        unset($match['map_info']);
        unset($match['map_ban_pick']);

        return $match;
    }

    // 地图禁用数据
    private static function getMatchMapsData($mapInfo, $gameId)
    {
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            switch ($gameId){
                case self::GAME_STATUS_LOL:
                case self::GAME_STATUS_DOTA2:
                    $map['map'] = self::getBattleMapData($v['map'],$gameId);
                    break;
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    break;
                default:
                    break;
            }
            $maps[] = $map;
        }
        return $maps;
    }

    // 地图禁用数据
    private static function getMapVetoesData($mapInfo,$team_1_id,$team_2_id,$gameId)
    {
        $type = array(
            1 => 'Ban',
            2 => 'Pick',
            3 => 'Left over'
        );
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            $map['team_id'] = intval($v['team']);
            if($team_1_id == $map['team_id']){
                $opponent_order = 1;
            }else if($team_2_id == $map['team_id']){
                $opponent_order = 2;
            }else{
                $opponent_order = null;
            }
            $map['opponent_order'] = $opponent_order;
            $map['type'] = isset($type[intval($v['type'])]) ? $type[intval($v['type'])]:null;
            switch ($gameId){
                case self::GAME_STATUS_LOL:
                case self::GAME_STATUS_DOTA2:
                    $map['map'] = self::getBattleMapData($v['map'],$gameId);
                    break;
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    break;
                default:
                    break;
            }
            $maps[] = $map;
        }
        return $maps;
    }

    // 获取赛事以及游戏数据
    private static function getTournamentData($tournamentId)
    {
        $tournament = Tournament::find()->alias('tour')
            ->select(self::getTournamentColumns())
            ->leftJoin('enum_match_state as tour_status', 'tour_status.id = tour.status')
            ->where(['tour.id'=>$tournamentId])
            ->asArray()->one();
        if(!empty($tournament)){
            $tournament['image'] = ImageConversionHelper::showFixedSizeConversion($tournament['image'],200,200);
            $tournament['game'] = self::getGameData($tournament['game']);
            $tournament['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['scheduled_begin_at']);
            $tournament['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['scheduled_end_at']);
            $tournament['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['modified_at']);
            $tournament['created_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['created_at']);
            $tournament['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['deleted_at']);
        }else{
            return null;
        }
        return $tournament;
    }

    // 获取分组以及游戏和赛事数据
    private static function getGroupData($groupId,$gameId)
    {
        $group = TournamentGroup::find()->alias('group')->select(self::getGroupColumns())->where(['group.id' => $groupId])->asArray()->one();
        if (!empty($group)){
            $group['game'] = self::getGameData($gameId);
//            $group['tournament'] = self::getTournamentData($group['tournament']);
            $group['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($group['scheduled_begin_at']);
            $group['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($group['scheduled_end_at']);
            $group['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($group['modified_at']);
            $group['created_at'] = ConversionCriteria::DataTimeConversionCriteria($group['created_at']);
            $group['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($group['deleted_at']);
        }else{
            return null;
        }
        return $group;
    }

    // 获取参赛战队数据
    private static function getTeamsData($teames, $score, $tournamentId, $groupId,$isSnap=false)
    {
        $teams = [];
        foreach ($teames as $k => $val) {
            $teams[$k]['team_id'] = $val['team_id'];
            if($isSnap) {
                $teams[$k]['team_snapshot'] = self::getTeamSnapshotData($val['team_id'], $tournamentId, $groupId);
            }
            $teams[$k]['opponent_order'] = $k + 1;
            if ($score) {
                $teams[$k]['score'] = $val['score'];
            }
        }
        return $teams;
    }

    private static $snapArray = [];
    // 公共方法获取战队快照
    private static function getTeamSnapshotData($teamId, $tournamentId, $groupId)
    {
        if(!$teamId){
            return null;
        }
        if ($groupId) {
            $codeKey = 'group-'.$groupId.'-'.$teamId;
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $groupId, 'team.type' => 'group'];
        } else if (empty($groupId) && $tournamentId) {
            $codeKey = 'tournament-'.$tournamentId.'-'.$teamId;
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $tournamentId, 'team.type' => 'tournament'];
        } else {
            return null;
        }
        if(isset(self::$snapArray[$codeKey])){
        }else {
            $snapData = TeamSnapshot::find()->alias('team')->select(self::getTeamSnapshotColumns())
                ->leftJoin('team_introduction_snapshot as team_i', 'team.id = team_i.team_id')
                ->leftJoin('enum_country as country', 'team.country = country.id')
                ->where($where)->asArray()->one();
            if (!empty($snapData)){
                $snap['name'] = $snapData['name'];
                if($snapData['country_id']) {
                    $snap['country']['id'] = $snapData['country_id'];
                    $snap['country']['name'] = $snapData['country_name'];
                    $snap['country']['name_cn'] = $snapData['country_name_cn'];
                    $snap['country']['short_name'] = $snapData['country_short_name'];
                    $snap['country']['image'] = $snapData['country_image'];
                }else{
                    $snap['country'] = null;
                }
                $snap['full_name'] = $snapData['full_name'];
                $snap['short_name'] = $snapData['short_name'];
                $snap['slug'] = $snapData['slug'];
                $snap['image'] = ImageConversionHelper::showFixedSizeConversion($snapData['image'],200,200);
                $snap['world_ranking'] = $snapData['world_ranking'];
                $snap['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['modified_at']);
                $snap['created_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['created_at']);
                $snap['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['deleted_at']);
            }else{
                return null;
            }
            self::$snapArray[$codeKey] = $snap;
        }
        return self::$snapArray[$codeKey];
    }

    // 默认领先战队与获胜战队通用 -- battle下参赛战队和获胜战队也通用
    private static function getAdvantage($teamId, $order, $tournamentId, $groupId,$isSnap=false)
    {
        $team['team_id'] = $teamId;
        if($isSnap) {
            $team['team_snapshot'] = self::getTeamSnapshotData($teamId, $tournamentId, $groupId);
        }
        $team['opponent_order'] = $order;
        return $team;
    }

    // battle下参赛战队
    private static function getBattleTeamsData($teamIds, $tournamentId, $groupId)
    {
        if($teamIds['team_1_id'] && $teamIds['team_2_id']) {
            $battleTeam = [
                [
                    'team_id' => $teamIds['team_1_id'],
                    'order' => 1,
                ],
                [
                    'team_id' => $teamIds['team_2_id'],
                    'order' => 2,
                ],
            ];
        }else{
            $battleTeam = [];
        }
        $teams = [];
        foreach ($battleTeam as $k => $val) {
            $team = self::getAdvantage($val['team_id'],$val['order'], $tournamentId,$groupId,true);
            $teams[] = $team;
        }
        return $teams;
    }

    // battle下比分
    private static function getBattleScoresData($battleId)
    {
        $battleScores = MatchBattleTeam::find()->select(['order', 'team_id', 'score'])
            ->orderBy('order asc')
            ->where(['battle_id'=>$battleId])
            ->asArray()->all();
        $teams = [];
        foreach ($battleScores as $k => $val) {
            $teams[$k]['team_id'] = $val['team_id'];
            $teams[$k]['opponent_order'] = $val['order'];
            $teams[$k]['score'] = $val['score'];
        }
        return $teams;
    }

    // battle下获胜战队
    private static function getBattleWinnerData($teamIds, $winner, $tournamentId, $groupId)
    {
        if($winner){
            $teamId = ($winner==1 && isset($teamIds['team_1_id'])) || ($winner==2 && isset($teamIds['team_2_id'])) ? $teamIds['team_'.$winner.'_id'] : null;
        }else{
            $teamId = null;
        }
        return self::getAdvantage($teamId, $winner, $tournamentId, $groupId,true);
    }

    // battle 下地图
    private static function getBattleMapData($mapId, $gameId)
    {
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // csgo地图
                $map = self::getCsgoMapData($mapId);
                break;
            case self::GAME_STATUS_LOL:
                // lol地图
                $map = MetadataLolMap::find()->alias('map')->select(self::getMapTosColumns())->where(['map.id'=>$mapId])->asArray()->one();
                break;
            case self::GAME_STATUS_DOTA2:
                // dota地图
                $map = MetadataDota2Map::find()->alias('map')->select(self::getMapTosColumns())->where(['map.id'=>$mapId])->asArray()->one();
                break;
            default:
                $map = null;
                break;
        }
        if(!empty($map) && $gameId!=self::GAME_STATUS_CSGO){
            $image = $map['image'];
            $map['status'] = ConversionCriteria::normalOrRemoveString($map['status']);
            $map['is_default'] = ConversionCriteria::issetTrueFalseType($map['is_default']);
            $map['image'] = [];
            $map['image']['square_image'] = null;
            $map['image']['rectangle_image'] = null;
            $map['image']['thumbnail'] = $image ?? null;
            $map['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($map['modified_at']);
            $map['created_at'] = ConversionCriteria::DataTimeConversionCriteria($map['created_at']);
            $map['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($map['deleted_at']);
        }
        return $map;
    }

    private static $csgoMapArray = [];
    // csgo 地图
    private static function getCsgoMapData($mapId){
        if(isset(self::$csgoMapArray[$mapId])){
        }else{
            $mapData = MetadataCsgoMap::find()->alias('map')->select(self::getMapColumns())
                ->leftJoin('enum_csgo_map as csgomap', 'csgomap.id = map.map_type')
                ->where(['map.id'=>$mapId])->asArray()->one();
            if(!empty($mapData)){
                $mapData['status'] = ConversionCriteria::normalOrRemoveString($mapData['status']);
                $mapData['is_default'] = ConversionCriteria::issetTrueFalseType($mapData['is_default']);
                $mapData['image'] = [];
                $mapData['image']['square_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['square_image'],256,144) ?? null;
                $mapData['image']['rectangle_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['rectangle_image'],675,84) ?? null;
                $mapData['image']['thumbnail'] = ImageConversionHelper::showFixedSizeConversion($mapData['thumbnail'],1380,930) ?? null;
                $mapData['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($mapData['modified_at']);
                $mapData['created_at'] = ConversionCriteria::DataTimeConversionCriteria($mapData['created_at']);
                $mapData['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($mapData['deleted_at']);
                unset($mapData['square_image']);
                unset($mapData['rectangle_image']);
                unset($mapData['thumbnail']);
            }else{
                return null;
            }
            self::$csgoMapArray[$mapId] = $mapData;
        }
        return self::$csgoMapArray[$mapId];
    }

    private static function getMapTosColumns(){
        return [
            'id as map_id',
            'name as name',
            'name_cn as name_cn',
            'state as status',
            'external_id as external_id',
            'external_name as external_name',
            'short_name as short_name',
            'map_type as map_type',
            'map_type_cn as map_type_cn',
            'is_default as is_default',
            'slug as slug',
            'image as image',
            'modified_at as modified_at',
            'created_at as created_at',
            'deleted_at as deleted_at',
        ];
    }

    // 地图字段
    private static function getMapColumns()
    {
        return [
            'map.id as map_id',
            'map.name as name',
            'map.name_cn as name_cn',
            'map.state as status',
            'map.external_id as external_id',
            'map.external_name as external_name',
            'map.short_name as short_name',
            'csgomap.e_name as map_type',
            'csgomap.c_name as map_type_cn',
//            'map.map_type as map_type',
//            'map.map_type_cn as map_type_cn',
            'map.is_default as is_default',
            'map.slug as slug',
            'map.is_default as image',
            'map.square_image as square_image',
            'map.rectangle_image as rectangle_image',
            'map.thumbnail as thumbnail',
            'map.modified_at as modified_at',
            'map.created_at as created_at',
            'map.deleted_at as deleted_at',
        ];
    }

    // 对局字段
    private static function getBattleColumns()
    {
        return [
            'battle.id as battle_id',
            'battle.game as game',
            'battle.match as match',
            'battle.order as order',
            'battle.order as teams',
            'battle.begin_at as begin_at',
            'battle.end_at as end_at',
            'battle_status.c_name as status',
//            'battle.status as status',
            'battle.is_draw as is_draw',
            'battle.is_forfeit as is_forfeit',
            'battle.is_default_advantage as is_default_advantage',
            'battle.map as map',
            'battle.duration as duration',
            'battle.winner as scores',
            'battle.winner as winner',
            'battle.is_battle_detailed as is_battle_detailed',
            'battle.winner as battle_detail',
            'battle.modified_at as modified_at',
            'battle.created_at as created_at',
            'battle.deleted_at as deleted_at',
        ];
    }
    // 游戏字段
    private static function getGameColumns()
    {
        return [
            'game.id as game_id',
            'game.e_name as name',
            'game.name as name_cn',
            'rules.name as default_game_rules',
            'type.name as default_match_type',
            'game.full_name as full_name',
            'game.short_name as short_name',
            'game.slug as slug',
            'game.image as image',
            'game.modified_at as modified_at',
            'game.created_at as created_at',
            'game.deleted_at as deleted_at',
        ];
    }
    // 比赛字段
    private static function getMatchColumns()
    {
        return [
            'match.id as match_id',
            'match.game as game',
            'match.game as game_id',
            'match.game_rules as rules_id',
            'match.match_type as type_id',
            'rules.name as game_rules',
            'type.name as match_type',
            'match.number_of_games as number_of_games',
            'match.tournament_id as tournament_id',
            'match.tournament_id as tournament',
            'match.group_id as group_id',
            'match.group_id as group',
            'match.scheduled_begin_at as scheduled_begin_at',
            'match.original_scheduled_begin_at as original_scheduled_begin_at',
            'match.is_rescheduled as is_rescheduled',
            'match.team_1_id as teams',
            'match.team_1_id as team_1_id',
            'match.team_2_id as team_2_id',
            'match.slug as slug',

            'match_r.begin_at as begin_at',
            'match_r.end_at as end_at',
            'match_status.c_name as status',
            'match_r.is_draw as is_draw',
            'match_r.is_forfeit as is_forfeit',
            'match_r.default_advantage as default_advantage',
            'match.game as map_vetoes',
            'match.game as maps',
            'match_r.map_ban_pick as map_ban_pick',
            'match_r.map_info as map_info',
            'match_r.team_1_score as scores',
            'match_r.team_1_score as team_1_score',
            'match_r.team_2_score as team_2_score',
            'match_r.winner as winner',
            'match_r.is_battle_detailed as is_battle_detailed',
            'match_r.data_speed as data_speed',

            'match.modified_at as modified_at',
            'match.created_at as created_at',
            'match.deleted_at as deleted_at',
        ];
    }
    // 赛事字段
    public static function getTournamentColumns()
    {
        return [
            'tour.id as tournament_id',
            'tour.name as name',
            'tour.name_cn as name_cn',
            'tour.game as game',
            'tour_status.c_name as status',
//            'tour.status as status',
            'tour.scheduled_begin_at as scheduled_begin_at',
            'tour.scheduled_end_at as scheduled_end_at',
            'tour.scheduled_begin_at as scheduled_begin_at',
            'tour.short_name as short_name',
            'tour.short_name_cn as short_name_cn',
            'tour.slug as slug',
            'tour.image as image',
            'tour.modified_at as modified_at',
            'tour.created_at as created_at',
            'tour.deleted_at as deleted_at',
        ];
    }
    // 分组字段
    public static function getGroupColumns()
    {
        return [
            'group.id as group_id',
            'group.name as name',
            'group.name_cn as name_cn',
            'group.game as game',
//            'group.tournament_id as tournament',
            'group.scheduled_begin_at as scheduled_begin_at',
            'group.scheduled_end_at as scheduled_end_at',
            'group.modified_at as modified_at',
            'group.created_at as created_at',
            'group.deleted_at as deleted_at',
        ];
    }
    // 战队快照字段
    public static function getTeamSnapshotColumns()
    {
        return [
            'team.name as name',

            'country.id as country_id',
            'country.e_name as country_name',
            'country.name as country_name_cn',
            'country.two as country_short_name',
            'country.image as country_image',

//            'team.country as country',
            'team.full_name as full_name',
            'team.short_name as short_name',
            'team.slug as slug',
            'team.image as image',

            'team_i.world_ranking as world_ranking',

            'team.modified_at as modified_at',
            'team.created_at as created_at',
            'team.deleted_at as deleted_at',
        ];
    }
}
