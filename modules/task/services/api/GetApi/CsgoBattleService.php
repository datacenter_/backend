<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\models\EnumGame;
use app\modules\match\models\EnumWinnerType;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\org\models\TeamSnapshot;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use yii\data\Pagination;

class CsgoBattleService
{
    const GAME_STATUS_CSGO = 1;

    public static function getBattleList($match_id)
    {
        $battleIds = MatchBattle::find()->select(['id'])->where(['match'=>$match_id,'deleted'=>2])->asArray()->all();
        $matchBattles = [];
        foreach ($battleIds as $k => $battleVal) {
            $battle = self::getBattleDetail($battleVal['id']);
            if(!isset($battle['list'])){
                $matchBattles[] = $battle;
            }
        }
        return [
            'count' => count($matchBattles),
            'list' => array_values(array_filter($matchBattles)),
        ];
    }

    // new对局详情信息
    public static function getBattleDetail($battleId)
    {
        $q = MatchBattle::find()->alias('battle')->where(['battle.id' => $battleId,'battle.deleted'=>2])
            ->leftJoin('enum_match_state as battle_status', 'battle_status.id = battle.status');
        $battle = $q->select(self::getBattleColumns())->asArray()->one();

        if (empty($battle)) return [];
        // battle所属比赛
        $match = self::getMatchData($battle['match']);
        // 主队客队
        $teamIds = [
            'team_1_id' => isset($match['team_1_id']) ? $match['team_1_id']:null,
            'team_2_id' => isset($match['team_2_id']) ? $match['team_2_id']:null,
        ];
        unset($match['team_1_id']);
        unset($match['team_2_id']);
        // battle所属比赛
        $battle['match'] = !empty($match) ? $match : [];
        // match游戏ID
        $gameId = !empty($battle['match']['game_id']) ? $battle['match']['game_id'] : null;
        // battle下游戏
        $battle['game'] = self::getGameData($gameId);
        // 赛事ID
        $tournamentId = !empty($battle['match']) ? $battle['match']['tournament_id'] : null;
        // 分组ID
        $groupId = !empty($battle['match']) ? $battle['match']['group_id'] : null;
        unset($battle['match']['tournament_id']);
        unset($battle['match']['group_id']);
        // battle参赛战队
        $battle['teams'] = self::getBattleTeamsData($teamIds, $tournamentId, $groupId);
        $battle['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['begin_at']);
        $battle['end_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['end_at']);
        // battle状态
//        $battle['status'] = self::getMatchBattleStateData($battle['status']);
        // 是否平局
        $battle['is_draw'] = ConversionCriteria::issetTrueFalseType($battle['is_draw']);
        // 是否弃权
        $battle['is_forfeit'] = ConversionCriteria::issetTrueFalseType($battle['is_forfeit']);
        // 是否默认领先
        $battle['is_default_advantage'] = ConversionCriteria::issetTrueFalseType($battle['is_default_advantage']);
        // battle地图
        $battle['map'] = self::getBattleMapData($battle['map'], $gameId);
        // battle比分
        $battle['scores'] = self::getBattleScoresData($battleId, $tournamentId, $groupId);
        // battle获胜战队
        $battle['winner'] = self::getBattleWinnerData($teamIds, $battle['winner'], $tournamentId, $groupId);
        // 是否有详情数据
        $battle['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($battle['is_battle_detailed']);
        // 对局时长
        $battle['duration'] = intval($battle['duration']);
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // battle - cogo对局详情
                $battle['battle_detail'] = self::getCsgoBattleDetailData($battle['winner'], $battleId,$battle['duration'],$teamIds);
                break;
            default:
                $battle['battle_detail'] = [];
                break;
        }
        unset($battle['match']['game_id']);
        $battle['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['modified_at']);
        $battle['created_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['created_at']);
        $battle['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['deleted_at']);
        return $battle;
    }

    // 对局表
    private static function getQuery()
    {
    }

    // 获取csgo对局详情数据
    private static function getCsgoBattleDetailData($battleWinner, $battleId,$duration,$teamIds)
    {
        // 获取 1-是否为官方完整数据 3-是否已结束 （字段）
        $extCsgoData = MatchBattleExtCsgo::find()->select(['is_confirmed'])->where(['id'=>$battleId])->asArray()->one();
        // 获取 2-对局时长 duration
        $detailData['is_confirmed'] = isset($extCsgoData['is_confirmed']) ? ConversionCriteria::issetTrueFalseType($extCsgoData['is_confirmed']) : null;
        $detailData['duration'] = intval($duration);
//        $detailData['is_finished'] = @$extCsgoData['is_finished'] ?? null;
        $detailData['winner'] = $battleWinner;

        // csgo数据详情
        $detail = self::getCsgoDataDetail($battleId,$teamIds);
        $detailData['teams'] = $detail['teams'];
        $detailData['special_events'] = $detail['special_events'];
        $detailData['rounds_detail'] = $detail['rounds_detail'];
        // 有效回合
        $detailData['live_rounds'] = $extCsgoData['live_rounds'] ? json_decode($extCsgoData['live_rounds'],true):null;
        return $detailData;
    }

    // 获取csgo数据
    private static function getCsgoDataDetail($battleId,$teamIds)
    {
        // 队伍
        $cogoData['teams'] = self::getCsgoTeamData($battleId);
        // 特殊事件
        $cogoData['special_events'] = self::getCsgoSpecialData($battleId,$teamIds);
        // 回合详情
        $cogoData['rounds_detail'] = self::getCsgoRoundsData($battleId,$teamIds);
        return $cogoData;
    }

    // teams计算格式
    private static $teamFields = [
        'kills' => 0,
        'headshot_kills' => 0,
        'deaths' => 0,
        'kd_diff' => 0,
        'assists' => 0,
        'flash_assists' => 0,
        'adr' => 0,
        'first_kills' => 0,
        'first_deaths' => 0,
        'first_kills_diff' => 0,
        'kast' => 0,
        'rating' => 0,
        'damage' => 0,
        'team_damage' => 0,
        'damage_taken' => 0,
        'hegrenade_damage_taken' => 0,
        'inferno_damage_taken' => 0,
        'planted_bomb' => 0,
        'defused_bomb' => 0,
        'chicken_kills' => 0,
        'blind_enemy_time' => 0,
        'blind_teammate_time' => 0,
        'team_kills' => 0,
        'multi_kills' => 0,
        'two_kills' => 0,
        'three_kills' => 0,
        'four_kills' => 0,
        'five_kills' => 0,
        'one_on_x_clutches' => 0,
        'one_on_one_clutches' => 0,
        'one_on_two_clutches' => 0,
        'one_on_three_clutches' => 0,
        'one_on_four_clutches' => 0,
        'one_on_five_clutches' => 0,
        'hit_generic' => 0,
        'hit_head' => 0,
        'hit_chest' => 0,
        'hit_stomach' => 0,
        'hit_left_arm' => 0,
        'hit_right_arm' => 0,
        'hit_left_leg' => 0,
        'hit_right_leg' => 0,
        'awp_kills' => 0,
        'knife_kills' => 0,
        'taser_kills' => 0,
        'shotgun_kills' => 0,
    ];

    // 获取csgo详情战队完整信息
    private static function getCsgoTeamData($battleId)
    {
        $teamsData = MatchBattleTeam::find()->alias('team')->select(self::getCsgoTeamAndExtColumns())
            ->leftJoin('match_battle_team_ext_csgo as team_ext', 'team.id = team_ext.id')
            ->where(["team.battle_id"=>$battleId])->asArray()->all();
        // battle下csgo选手数据
        $playersArray = [];
        $players = MatchBattlePlayerCsgo::find()->alias('b_player')->select(self::getCsgoBattlePlayerColumns())
            ->leftJoin('player as player', 'player.id = b_player.player_id')
            ->where(['b_player.battle_id' => $battleId])->asArray()->all();
        foreach ($players as $key => $player) {
            $teamOrder = $player['team_order'];
            unset($player['team_order']);
            $player['player'] = [];
            $player['player']['player_id'] = $player['player_id'];
            if(!empty($player['player_nick_name'])){
                $player['player']['nick_name'] = $player['player_nick_name'];
            }else{
                $player['player']['nick_name'] = $player['nick_name'];
            }
//            $player['is_knife_kill'] = ConversionCriteria::issetTrueFalseType($player['is_knife_kill']);
//            $player['is_ace_kill'] = ConversionCriteria::issetTrueFalseType($player['is_ace_kill']);
            unset($player['nick_name']);
            unset($player['player_id']);
            unset($player['player_nick_name']);
            if($teamOrder == 1){
                $playersArray[$teamOrder][] = $player;
            }
            if($teamOrder == 2){
                $playersArray[$teamOrder][] = $player;
            }
        }
        // 算数
        $teamsCount = [];
        foreach ($playersArray as $key=>$team){
            // teams计算格式
            $teamFields = [
                'kills' => 0,
                'headshot_kills' => 0,
                'deaths' => 0,
                'kd_diff' => 0,
                'assists' => 0,
                'flash_assists' => 0,
                'adr' => 0,
                'first_kills' => 0,
                'first_deaths' => 0,
                'first_kills_diff' => 0,
                'kast' => 0,
                'rating' => 0,
                'damage' => 0,
                'team_damage' => 0,
                'damage_taken' => 0,
                'hegrenade_damage_taken' => 0,
                'inferno_damage_taken' => 0,
                'planted_bomb' => 0,
                'defused_bomb' => 0,
                'chicken_kills' => 0,
                'blind_enemy_time' => 0,
                'blind_teammate_time' => 0,
                'team_kills' => 0,
                'multi_kills' => 0,
                'two_kills' => 0,
                'three_kills' => 0,
                'four_kills' => 0,
                'five_kills' => 0,
                'one_on_x_clutches' => 0,
                'one_on_one_clutches' => 0,
                'one_on_two_clutches' => 0,
                'one_on_three_clutches' => 0,
                'one_on_four_clutches' => 0,
                'one_on_five_clutches' => 0,
                'hit_generic' => 0,
                'hit_head' => 0,
                'hit_chest' => 0,
                'hit_stomach' => 0,
                'hit_left_arm' => 0,
                'hit_right_arm' => 0,
                'hit_left_leg' => 0,
                'hit_right_leg' => 0,
                'awp_kills' => 0,
                'knife_kills' => 0,
                'taser_kills' => 0,
                'shotgun_kills' => 0,
            ];
            foreach ($team as $kk=>$player){
                foreach ($player as $k=>$val){

                    if(isset($teamFields[$k])){
                        $teamFields[$k] = $teamFields[$k]+$val;
                    }
                }
            }
            $teamsCount[$key] = $teamFields;
        }
        $teams = [];
        foreach ($teamsData as $k => $v) {
            if(isset($teamsCount[$v['order']])){
                $teamsCount[$v['order']]['adr'] = round($teamsCount[$v['order']]['adr']/count($playersArray[$v['order']]),4);
                $teamsCount[$v['order']]['kast'] = round($teamsCount[$v['order']]['kast']/count($playersArray[$v['order']]),2);
                $teamsCount[$v['order']]['rating'] = round($teamsCount[$v['order']]['rating']/count($playersArray[$v['order']]),2);
                $teamsCount[$v['order']]['blind_enemy_time'] = round($teamsCount[$v['order']]['blind_enemy_time'],2);
                $teamsCount[$v['order']]['blind_teammate_time'] = round($teamsCount[$v['order']]['blind_teammate_time'],2);
                $v = array_merge($v,$teamsCount[$v['order']]);
            }else{
                $v = array_merge($v,self::$teamFields);
            }
            $v['players'] = isset($playersArray[$v['order']]) ? $playersArray[$v['order']] : [];
            unset($v['order']);
            $teams[] = $v;
        }
        return $teams;
    }

    // csgo match_battle_ext_csgo 获取csgo特殊事件完整信息
    private static function getCsgoSpecialData($battleId,$teamIds)
    {
        $special = MatchBattleExtCsgo::find()->alias('ext_csgo')->select(self::getCsgoBattleExtColumns())
            ->where(['ext_csgo.id' => $battleId])->asArray()->one();
        $sqecialEvents = [];
        if (!empty($special)) {
            $sqecialEvents['win_round_1']['side'] = $special['win_round_1_side'];
            $sqecialEvents['win_round_1']['team_id'] = isset($teamIds['team_'.$special['win_round_1_team'].'_id']) ? $teamIds['team_'.$special['win_round_1_team'].'_id']:$special['win_round_1_team'];
            $sqecialEvents['win_round_1']['detail'] = $special['win_round_1_detail'];
            $sqecialEvents['win_round_16']['side'] = $special['win_round_16_side'];
            $sqecialEvents['win_round_16']['team_id'] = isset($teamIds['team_'.$special['win_round_16_team'].'_id']) ? $teamIds['team_'.$special['win_round_16_team'].'_id']:$special['win_round_16_team'];
            $sqecialEvents['win_round_16']['detail'] = $special['win_round_16_detail'];
            $sqecialEvents['first_to_5_rounds_wins']['side'] = $special['first_to_5_rounds_wins_side'];
            $sqecialEvents['first_to_5_rounds_wins']['team_id'] = isset($teamIds['team_'.$special['first_to_5_rounds_wins_team'].'_id']) ? $teamIds['team_'.$special['first_to_5_rounds_wins_team'].'_id']:$special['first_to_5_rounds_wins_team'];
            $sqecialEvents['first_to_5_rounds_wins']['detail'] = $special['first_to_5_rounds_wins_detail'];
        }
        return $sqecialEvents;
    }
    private static $sideCount = [
        'deaths' => 0,
        'assists' => 0,
        'flash_assists' => 0,
        'is_first_kill' => null,
        'is_first_death' => null,
        'damage' => 0,
        'team_damage' => 0,
        'damage_taken' => 0,
        'hegrenade_damage_taken' => 0,
        'inferno_damage_taken' => 0,
        'is_planted_bomb' => null,
        'is_defused_bomb' => null,
        'chicken_kills' => 0,
        'blind_enemy_time' => 0,
        'blind_teammate_time' => 0,
        'team_kills' => 0,
        'multi_kills' => 0,
        'two_kills' => 0,
        'three_kills' => 0,
        'four_kills' => 0,
        'five_kills' => 0,
        'is_one_on_x_clutch' => null,
        'is_one_on_one_clutch' => null,
        'is_one_on_two_clutch' => null,
        'is_one_on_three_clutch' => null,
        'is_one_on_four_clutch' => null,
        'is_one_on_five_clutch' => null,
        'hit_generic' => 0,
        'hit_head' => 0,
        'hit_chest' => 0,
        'hit_stomach' => 0,
        'hit_left_arm' => 0,
        'hit_right_arm' => 0,
        'hit_left_leg' => 0,
        'hit_right_leg' => 0,
        'awp_kills' => 0,
        'knife_kills' => 0,
        'taser_kills' => 0,
        'shotgun_kills' => 0,
    ];
    // csgo回合详情完整数据
    private static function getCsgoRoundsData($battleId,$teamIds)
    {
        // 回合序号表
        $rounds = MatchBattleRoundCsgo::find()->alias('round')->select(['round_ordinal','winner_team_id','winner_end_type'])
            ->where(['round.battle_id'=>$battleId])->asArray()->all();
        // 回合阵营数据
        $roundsSideList = MatchBattleRoundSideCsgo::find()->alias('round_side')->select(self::getCsgoBattleRoundColumns())
            ->where(['round_side.battle_id' => $battleId])->asArray()->all();
        // 回合下选手数据
        $roundPlayers = MatchBattleRoundPlayerCsgo::find()->alias('round_player')->select(self::getCsgoBattleRoundPlayerColumns())
            ->leftJoin('player as player', 'player.id = round_player.player_id')
            ->where(['round_player.battle_id' => $battleId])->asArray()->all();
        // 胜利方式数据
        $winnerType = EnumWinnerType::find()->select(['id','winner_type'])->asArray()->all();
//        $weaponArray=[];
//        foreach($weaponList as $key=>$val){
//            $weaponArray[$val['weapon_id']]=$val;
//        }
        $winnerTypeArray=[];
        foreach($winnerType as $key=>$val){
            $winnerTypeArray[$val['id']]=$val['winner_type'];
        }
        $roundsSideArray=[];
        foreach($roundsSideList as $key=>$val){
            @$roundsSideArray[$val['round_ordinal']][$val['side_order']]=$val;
        }
        $roundsPlayersArray=[];
        foreach($roundPlayers as $key=>$val){
            $roundsPlayersArray[$val['round_ordinal']][$val['player_order']][]=$val;
        }
        // 结果数据
        $roundDetail = [];
        // 回合数据
        foreach ($rounds as $roundKey=>$round){
            $roundDetail[$roundKey]['round_ordinal'] = $round['round_ordinal'];
            // 回合日志
//            $eventLogArray=[];
            // 首杀
            $openingKillSide = [];
            // 安放炸弹
            $bombPlanted = [];
            // 刀杀
            $knifeKill = [];
            // ACE
            $aceKill = [];
            // 算数side
            $sideCountArray = [];
            // 回合阵容数据处理
            $roundSidesData = isset($roundsSideArray[$round['round_ordinal']]) ? $roundsSideArray[$round['round_ordinal']] : [];
            foreach ($roundSidesData as $sideKey=>$side){
                $tmpSide=$side;
//                $roundDetail[$roundKey]['side'][$side['side_order']]=$side;
                $players = isset($roundsPlayersArray[$round['round_ordinal']][$side['side_order']]) ? $roundsPlayersArray[$round['round_ordinal']][$side['side_order']] : [];

                if($tmpSide['is_opening_kill_side'] == 1){
                    $openingKillSide['side'] = $side['side'];
                    $openingKillSide['team_id'] = $side['team_id'];
                    $openingKillSide['detail'] = ['is_opening_kill_headshot'=>ConversionCriteria::issetTrueFalseType($side['is_opening_kill_headshot'])];
                }
                if($tmpSide['is_bomb_planted'] == 1){
                    $bombPlanted['side'] = $side['side'];
                    $bombPlanted['team_id'] = $side['team_id'];
                    $bombPlanted['detail'] = $side['detail'];
                }
                if($tmpSide['is_knife_kill'] == 1){
                    $knifeKill[] = [
                        'side' => $side['side'],
                        'team_id' => $side['team_id'],
                        'detail' => $side['detail']
                    ];
                }
                if($tmpSide['is_ace_kill'] == 1){
                    $aceKill['side'] = $side['side'];
                    $aceKill['team_id'] = $side['team_id'];
                    $aceKill['detail'] = $side['detail'];
                }
                // 计算roundplayer数
                if(!empty($roundsPlayersArray[$round['round_ordinal']])) {
                    $roundTeams = $roundsPlayersArray[$round['round_ordinal']];
                    foreach ($roundTeams as $key => $teamPlayer) {
                        // round-side-算数
                        $sideCount = [
                            'deaths' => 0,
                            'assists' => 0,
                            'flash_assists' => 0,
                            'is_first_kill' => null,
                            'is_first_death' => null,
                            'damage' => 0,
                            'team_damage' => 0,
                            'damage_taken' => 0,
                            'hegrenade_damage_taken' => 0,
                            'inferno_damage_taken' => 0,
                            'is_planted_bomb' => null,
                            'is_defused_bomb' => null,
                            'chicken_kills' => 0,
                            'blind_enemy_time' => 0,
                            'blind_teammate_time' => 0,
                            'team_kills' => 0,
                            'multi_kills' => 0,
                            'two_kills' => 0,
                            'three_kills' => 0,
                            'four_kills' => 0,
                            'five_kills' => 0,
                            'is_one_on_x_clutch' => null,
                            'is_one_on_one_clutch' => null,
                            'is_one_on_two_clutch' => null,
                            'is_one_on_three_clutch' => null,
                            'is_one_on_four_clutch' => null,
                            'is_one_on_five_clutch' => null,
                            'hit_generic' => 0,
                            'hit_head' => 0,
                            'hit_chest' => 0,
                            'hit_stomach' => 0,
                            'hit_left_arm' => 0,
                            'hit_right_arm' => 0,
                            'hit_left_leg' => 0,
                            'hit_right_leg' => 0,
                            'awp_kills' => 0,
                            'knife_kills' => 0,
                            'taser_kills' => 0,
                            'shotgun_kills' => 0,
                        ];
                        foreach ($teamPlayer as $kk=>$player){
                            foreach ($player as $k=>$val){
                                switch ($k){
                                    case 'is_planted_bomb':
                                    case 'is_defused_bomb':
                                    case 'is_one_on_x_clutch':
                                    case 'is_one_on_one_clutch':
                                    case 'is_one_on_two_clutch':
                                    case 'is_one_on_three_clutch':
                                    case 'is_one_on_four_clutch':
                                    case 'is_one_on_five_clutch':
                                        if(is_null($sideCount[$k])) {
                                            $sideCount[$k] = ConversionCriteria::issetTrueFalseType($val);
                                        }
                                        break;
                                    default :
                                        if(isset($sideCount[$k])){
                                            $sideCount[$k] = $sideCount[$k]+$val;
                                        }
                                        break;
                                }
                            }
                        }
                        $sideCountArray[$key] = $sideCount;
                    }
                }
                foreach ($players as $playerKey=>$player){
                    $players[$playerKey]['player'] = [];
                    $players[$playerKey]['player']['player_id'] = $player['player_id'];
                    if(!empty($player['player_nick_name'])){
                        $players[$playerKey]['player']['nick_name'] = $player['player_nick_name'];
                    }else{
                        $players[$playerKey]['player']['nick_name'] = $player['nick_name'];
                    }
                    $players[$playerKey]['is_died'] = ConversionCriteria::issetTrueFalseType($player['is_died']);
                    $players[$playerKey]['is_first_kill'] = ConversionCriteria::issetTrueFalseType($player['is_first_kill']);
                    $players[$playerKey]['is_first_death'] = ConversionCriteria::issetTrueFalseType($player['is_first_death']);
                    $players[$playerKey]['is_multi_kill'] = ConversionCriteria::issetTrueFalseType($player['is_multi_kill']);
                    $players[$playerKey]['is_one_on_x_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_x_clutch']);
                    $players[$playerKey]['is_knife_kill'] = ConversionCriteria::issetTrueFalseType($player['is_knife_kill']);
                    $players[$playerKey]['is_ace_kill'] = ConversionCriteria::issetTrueFalseType($player['is_ace_kill']);
                    unset($players[$playerKey]['round_ordinal']);
                    unset($players[$playerKey]['player_order']);
                    unset($players[$playerKey]['player_id']);
                    unset($players[$playerKey]['nick_name']);
                    unset($players[$playerKey]['player_nick_name']);
                }
                unset($tmpSide['detail']);
                unset($tmpSide['is_opening_kill_headshot']);
                unset($tmpSide['is_opening_kill_side']);
                unset($tmpSide['is_bomb_planted']);
                unset($tmpSide['is_knife_kill']);
                unset($tmpSide['is_ace_kill']);
                unset($tmpSide['round_ordinal']);
                if(isset($sideCountArray[$side['side_order']])){
                    $tmpSide = array_merge($tmpSide,$sideCountArray[$side['side_order']]);
                }else{
                    $tmpSide = array_merge($tmpSide,self::$sideCount);
                }
                unset($tmpSide['side_order']);
                $tmpSide['players'] = $players;
                $roundDetail[$roundKey]['side'][]=$tmpSide;
                // 回合日志
//                foreach($eventLogList as $key=>$val){
//                    if($round['round_ordinal'] == $val['round_ordinal']){
//                        $eventLogArray[] = $val;
//                        unset($eventLogList[$key]);
//                    }
//                }
            }
//            $roundDetail[$roundKey]['round_log'] = self::getCsgoRoundEventsTypeCo($eventLogArray,$weaponArray,$winnerTypeArray);
            $roundDetail[$roundKey]['special_events'] = [
                'opening_kill' => $openingKillSide,
                'bomb_planted' => $bombPlanted,
                'knife_kill' => $knifeKill,
                'ace_kill' => $aceKill,
            ];
            $winner_team_id = $round['winner_team_id'];
            if($winner_team_id){
                $winner_team_id = isset($teamIds['team_'.$winner_team_id.'_id']) ? $teamIds['team_'.$winner_team_id.'_id'] : $winner_team_id;
            }
            $roundDetail[$roundKey]['winner'] = $winner_team_id;
            $roundDetail[$roundKey]['win_type'] = isset($winnerTypeArray[$round['winner_end_type']]) ? $winnerTypeArray[$round['winner_end_type']] : null;
        }
        return $roundDetail;
    }

    // 获胜方和获胜方式
    private static function getCsgoRoundEventsTypeCo($eventData,$weaponArray,$winnerTypeArray)
    {
        $eventDataCo = [];
        foreach ($eventData as $k=>$v){
            $eventDataCo[$k]['event_id'] = $v['event_id'];
            $eventDataCo[$k]['event_type'] = $v['event_type'];
            $eventDataCo[$k]['round_ordinal'] = $v['round_ordinal'];
            $eventDataCo[$k]['in_round_timestamp'] = $v['in_round_timestamp'];
            $eventDataCo[$k]['round_time'] = $v['round_time'];
            $eventDataCo[$k]['is_bomb_planted'] = ConversionCriteria::issetTrueFalseType($v['is_bomb_planted']);
            $eventDataCo[$k]['time_since_plant'] = $v['time_since_plant'];
            if($v['event_type'] == 'round_end'){
                $eventDataCo[$k]['round_end_type'] = isset($winnerTypeArray[$v['round_end_type']]) ? $winnerTypeArray[$v['round_end_type']] : null;
                $eventDataCo[$k]['winner_side'] = $v['winner_side'];
                $eventDataCo[$k]['ct_score'] = $v['ct_score'];
                $eventDataCo[$k]['t_score'] = $v['t_score'];
            }
            if($v['event_type'] == 'player_kill'){
                $eventDataCo[$k]['killer']['player_id'] = $v['killer'];
                if(!empty($v['kill_player_name'])){
                    $eventDataCo[$k]['killer']['nick_name'] = $v['kill_player_name'];
                }else{
                    $eventDataCo[$k]['killer']['nick_name'] = $v['killer_nick_name'];
                }
                $eventDataCo[$k]['killer']['side'] = $v['killer_side'];
                $eventDataCo[$k]['killer']['position'] = $v['killer_position'];
                $eventDataCo[$k]['victim']['player_id'] = $v['victim'];
                if(!empty($v['victim_player_name'])){
                    $eventDataCo[$k]['victim']['nick_name'] = $v['victim_player_name'];
                }else{
                    $eventDataCo[$k]['victim']['nick_name'] = $v['victim_nick_name'];
                }
                $eventDataCo[$k]['victim']['side'] = $v['victim_side'];
                $eventDataCo[$k]['victim']['position'] = $v['victim_position'];
                $eventDataCo[$k]['assist']['player_id'] = $v['assist'];
                if(!empty($v['assist_player_name'])){
                    $eventDataCo[$k]['assist']['nick_name'] = $v['assist_player_name'];
                }else{
                    $eventDataCo[$k]['assist']['nick_name'] = $v['assist_nick_name'];
                }
                $eventDataCo[$k]['assist']['side'] = $v['assist_side'];
                $eventDataCo[$k]['flashassist']['player_id'] = $v['flashassist'];
                if(!empty($v['flashassist_player_name'])){
                    $eventDataCo[$k]['flashassist']['nick_name'] = $v['flashassist_player_name'];
                }else{
                    $eventDataCo[$k]['flashassist']['nick_name'] = $v['flashassistt_nick_name'];
                }
                $eventDataCo[$k]['flashassist']['side'] = $v['flashassist_side'];
                if(isset($weaponArray[$v['weapon']])){
                    $weapon = $weaponArray[$v['weapon']];
                    $weapon['image'] = ImageConversionHelper::showMfitSizeConversion($weapon['image'],28,0);
                    $eventDataCo[$k]['weapon'] = $weapon;
                }else{
                    $eventDataCo[$k]['weapon'] = [];
                }
                $eventDataCo[$k]['damage'] = $v['damage'];
                $eventDataCo[$k]['hit_group'] = $v['hit_group'];
                $eventDataCo[$k]['is_headshot'] = ConversionCriteria::issetTrueFalseType($v['is_headshot']);
            }
            if($v['event_type'] == 'bomb_planted'){
                $eventDataCo[$k]['player_id'] = $v['bomb_planted_player_id'];
                if(!empty($v['bomb_planted_player_name'])){
                    $eventDataCo[$k]['nick_name'] = $v['bomb_planted_player_name'];
                }else{
                    $eventDataCo[$k]['nick_name'] = $v['bomb_planted_nick_name'];
                }
                $eventDataCo[$k]['side'] = $v['bomb_planted_side'];
                $eventDataCo[$k]['position'] = $v['bomb_planted_position'];
            }
            if($v['event_type'] == 'bomb_defused'){
                $eventDataCo[$k]['player_id'] = $v['bomb_defused_player_id'];
                if(!empty($v['bomb_defused_player_name'])){
                    $eventDataCo[$k]['nick_name'] = $v['bomb_defused_player_name'];
                }else{
                    $eventDataCo[$k]['nick_name'] = $v['bomb_defused_nick_name'];
                }
                $eventDataCo[$k]['defused_side'] = $v['bomb_defused_side'];
                $eventDataCo[$k]['position'] = $v['bomb_defused_position'];
            }
            if($v['event_type'] == 'bomb_exploded'){
                $eventDataCo[$k]['bomb_exploded_time'] = $v['bomb_exploded_time'];
            }
            if($v['event_type'] == 'suicide'){
                $eventDataCo[$k]['player_id'] = $v['suicide_player_id'];
                if(!empty($v['suicide_player_name'])){
                    $eventDataCo[$k]['nick_name'] = $v['suicide_player_name'];
                }else{
                    $eventDataCo[$k]['nick_name'] = $v['suicide_nick_name'];
                }
                $eventDataCo[$k]['side'] = $v['suicide_side'];
                $eventDataCo[$k]['position'] = $v['suicide_position'];
            }
        }
        return $eventDataCo;
    }

    // csgo match_battle_player_csgo表字段
    private static function getCsgoBattlePlayerColumns()
    {
        return [
            'b_player.team_order as team_order',
            'b_player.player_id as player',
            'b_player.player_id as player_id',
            'b_player.nick_name as nick_name',
            'b_player.kills as kills',
            'b_player.headshot_kills as headshot_kills',
            'b_player.deaths as deaths',
            'b_player.k_d_diff as kd_diff',
            'b_player.assists as assists',
            'b_player.flash_assists as flash_assists',
            'b_player.adr as adr',
            'b_player.first_kills as first_kills',
            'b_player.first_deaths as first_deaths',
            'b_player.first_kills_diff as first_kills_diff',
            'b_player.multi_kills as multi_kills',
            'b_player.kast as kast',
            'b_player.rating as rating',
            'b_player.damage as damage',
            'b_player.team_damage as team_damage',
            'b_player.damage_taken as damage_taken',
            'b_player.hegrenade_damage_taken as hegrenade_damage_taken',
            'b_player.inferno_damage_taken as inferno_damage_taken',
            'b_player.planted_bomb as planted_bomb',
            'b_player.defused_bomb as defused_bomb',
            'b_player.chicken_kills as chicken_kills',
            'b_player.blind_enemy_time as blind_enemy_time',
            'b_player.blind_teammate_time as blind_teammate_time',
            'b_player.team_kills as team_kills',
            'b_player.multi_kills as multi_kills',
            'b_player.two_kills as two_kills',
            'b_player.three_kills as three_kills',
            'b_player.four_kills as four_kills',
            'b_player.five_kills as five_kills',
            'b_player.1_v_n_clutches as one_on_x_clutches',
            'b_player.one_on_one_clutches as one_on_one_clutches',
            'b_player.one_on_two_clutches as one_on_two_clutches',
            'b_player.one_on_three_clutches as one_on_three_clutches',
            'b_player.one_on_four_clutches as one_on_four_clutches',
            'b_player.one_on_five_clutches as one_on_five_clutches',
            'b_player.hit_generic as hit_generic',
            'b_player.hit_head as hit_head',
            'b_player.hit_chest as hit_chest',
            'b_player.hit_stomach as hit_stomach',
            'b_player.hit_left_arm as hit_left_arm',
            'b_player.hit_right_arm as hit_right_arm',
            'b_player.hit_left_leg as hit_left_leg',
            'b_player.hit_right_leg as hit_right_leg',
            'b_player.awp_kills as awp_kills',
            'b_player.knife_kills as knife_kills',
            'b_player.taser_kills as taser_kills',
            'b_player.shotgun_kills as shotgun_kills',

            'player.nick_name as player_nick_name',
        ];
    }

    // csgo match_battle_ext_csgo字段
    private static function getCsgoBattleExtColumns()
    {
        return [
            'ext_csgo.win_round_1_side as win_round_1_side',
            'ext_csgo.win_round_1_team as win_round_1_team',
            'ext_csgo.win_round_1_detail as win_round_1_detail',
            'ext_csgo.win_round_16_side as win_round_16_side',
            'ext_csgo.win_round_16_team as win_round_16_team',
            'ext_csgo.win_round_16_detail as win_round_16_detail',
            'ext_csgo.first_to_5_rounds_wins_side as first_to_5_rounds_wins_side',
            'ext_csgo.first_to_5_rounds_wins_team as first_to_5_rounds_wins_team',
            'ext_csgo.first_to_5_rounds_wins_detail as first_to_5_rounds_wins_detail',
        ];
    }

    // csgo match_battle_round_player_csgo字段
    private static function getCsgoBattleRoundPlayerColumns()
    {
        return [
            'round_player.player_id as player',
            'round_player.player_id as player_id',
            'round_player.nick_name as nick_name',
            'round_player.kills as kills',
            'round_player.headshot_kills as headshot_kills',
            'round_player.is_died as is_died',
            'round_player.assists as assists',
            'round_player.flash_assists as flash_assists',
            'round_player.is_first_kill as is_first_kill',
            'round_player.is_first_death as is_first_death',
            'round_player.damage as damage',
            'round_player.team_damage as team_damage',
            'round_player.damage_taken as damage_taken',
            'round_player.hegrenade_damage_taken as hegrenade_damage_taken',
            'round_player.inferno_damage_taken as inferno_damage_taken',
            'round_player.is_planted_bomb as is_planted_bomb',
            'round_player.is_defused_bomb as is_defused_bomb',
            'round_player.chicken_kills as chicken_kills',
            'round_player.blind_enemy_time as blind_enemy_time',
            'round_player.blind_teammate_time as blind_teammate_time',
            'round_player.team_kills as team_kills',
            'round_player.is_multi_kill as is_multi_kill',
            'round_player.is_1_v_n_clutche as is_one_on_x_clutch',
            'round_player.is_one_on_one_clutch as is_one_on_one_clutch',
            'round_player.is_one_on_two_clutch as is_one_on_two_clutch',
            'round_player.is_one_on_three_clutch as is_one_on_three_clutch',
            'round_player.is_one_on_four_clutch as is_one_on_four_clutch',
            'round_player.is_one_on_five_clutch as is_one_on_five_clutch',
            'round_player.hit_generic as hit_generic',
            'round_player.hit_head as hit_head',
            'round_player.hit_chest as hit_chest',
            'round_player.hit_stomach as hit_stomach',
            'round_player.hit_left_arm as hit_left_arm',
            'round_player.hit_right_arm as hit_right_arm',
            'round_player.hit_left_leg as hit_left_leg',
            'round_player.hit_right_leg as hit_right_leg',
            'round_player.awp_kills as awp_kills',
            'round_player.knife_kills as knife_kills',
            'round_player.taser_kills as taser_kills',
            'round_player.shotgun_kills as shotgun_kills',
            'round_player.round_ordinal as round_ordinal',
            'round_player.player_order as player_order',

            'player.nick_name as player_nick_name'
        ];
    }

    // csgo match_battle_round_side_csgo字段
    private static function getCsgoBattleRoundColumns()
    {
        return [
            'round_side.side as side',
            'round_side.team_id as team_id',
            'round_side.survived_players as survived_players',
            'round_side.kills as kills',
            'round_side.headshot_kills as headshot_kills',
            'round_side.side_order as side_order',
            'round_side.round_ordinal as round_ordinal',

            'round_side.team_id as team_id',
            'round_side.detail as detail',
            'round_side.is_opening_kill_headshot as is_opening_kill_headshot',
            'round_side.is_opening_kill_side as is_opening_kill_side',
            'round_side.is_bomb_planted as is_bomb_planted',
            'round_side.is_knife_kill as is_knife_kill',
            'round_side.is_ace_kill as is_ace_kill',
        ];
    }

    private static $gameArray = [];
    // 获取游戏数据
    private static function getGameData($gameId)
    {
        if(isset(self::$gameArray[$gameId])){
        }else {
            $game = EnumGame::find()->alias('game')->select(self::getGameColumns())
                ->leftJoin('enum_game_rules as rules', 'game.default_game_rule = rules.id')
                ->leftJoin('enum_game_match_type as type', 'game.default_match_type = type.id')
                ->where(["game.id" => $gameId])->asArray()->one();
            if (!empty($game)) {
                $game['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($game['modified_at']);
                $game['created_at'] = ConversionCriteria::DataTimeConversionCriteria($game['created_at']);
                $game['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($game['deleted_at']);
                unset($game['rules_id']);
                unset($game['type_id']);
            }
            self::$gameArray[$gameId] = $game;
        }
        return self::$gameArray[$gameId];
    }

    // 获取完整比赛数据
    private static function getMatchData($matchId)
    {
        $match = Match::find()->alias('match')->select(self::getMatchColumns())
            ->leftJoin('match_real_time_info as match_r', 'match.id = match_r.id')
            ->leftJoin('enum_match_state as match_status', 'match_status.id = match_r.status')
            ->leftJoin('enum_game_rules as rules', 'match.game_rules = rules.id')
            ->leftJoin('enum_game_match_type as type', 'match.match_type = type.id')
            ->where(["match.id" => $matchId,'match.deleted'=>2])->asArray()->one();
        if (empty($match)) return [];
        $gameId = $match['game'];
        $match['game'] = self::getGameData($match['game']);
        $match['tournament'] = self::getTournamentData($match['tournament']);
        $match['group'] = self::getGroupData($match['group'],$gameId);
        // 计划开始时间 原计划开始时间
        $match['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['scheduled_begin_at']);
        $match['original_scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['original_scheduled_begin_at']);
        // 参赛战队与比分 共同数组
        $teams = array(
            array('team_id' => $match['team_1_id'], 'score' => $match['team_1_score'], 'opponent_order'=>1),
            array('team_id' => $match['team_2_id'], 'score' => $match['team_2_score'], 'opponent_order'=>2)
        );
        // 是否调整时间表
        $match['is_rescheduled'] = ConversionCriteria::issetTrueFalseType($match['is_rescheduled']);
        // 参赛战队
        $match['teams'] = self::getTeamsData($teams, $score = false, $match['tournament_id'], $match['group_id']);
        // 状态
//        $match['status'] = self::getMatchBattleStateData($match['status']);
        // 是否平局
        $match['is_draw'] = ConversionCriteria::issetTrueFalseType($match['is_draw']);
        // 是否弃权
        $match['is_forfeit'] = ConversionCriteria::issetTrueFalseType($match['is_forfeit']);
        // 默认领先战队
        $match['default_advantage'] = $match['default_advantage'] == '' ? null : self::getAdvantage($match['team_' . $match['default_advantage'] . '_id'], $match['default_advantage'], $match['tournament_id'], $match['group_id']);
        // 地图禁用
        $map_ban_pick = ConversionCriteria::issetJsonField($match['map_ban_pick']);
        $match['map_vetoes'] = $match['map_ban_pick'] ? self::getMapVetoesData($map_ban_pick, $match['tournament_id'], $match['group_id'], $gameId) : [];
        // 比赛地图
        $map_info = ConversionCriteria::issetJsonField($match['map_info']);
        $match['maps'] = $match['map_info'] ? self::getMatchMapsData($map_info, $gameId) : [];
        // 比分
        $match['scores'] = self::getTeamsData($teams, $score = true, $match['tournament_id'], $match['group_id']);
        // 获胜战队
        $match['winner'] = $match['winner'] > 0 ? self::getAdvantage($match['team_' . $match['winner'] . '_id'], $match['winner'], $match['tournament_id'], $match['group_id']) : null;
        // 是否有对局详情
        $match['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($match['is_battle_detailed']);

        $match['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($match['begin_at']);
        $match['end_at'] = ConversionCriteria::DataTimeConversionCriteria($match['end_at']);
        $match['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($match['modified_at']);
        $match['created_at'] = ConversionCriteria::DataTimeConversionCriteria($match['created_at']);
        $match['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($match['deleted_at']);
        unset($match['team_1_score']);
        unset($match['team_2_score']);
        unset($match['rules_id']);
        unset($match['type_id']);
        unset($match['map_info']);
        unset($match['map_ban_pick']);

        return $match;
    }

    // 地图禁用数据
    private static function getMatchMapsData($mapInfo, $gameId)
    {
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            switch ($gameId){
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    break;
                default:
                    break;
            }
            $maps[] = $map;
        }
        return $maps;
    }

    // 地图禁用数据
    private static function getMapVetoesData($mapInfo, $tournamentId, $groupId, $gameId)
    {
        $type = array(
            1 => 'Ban',
            2 => 'Pick',
            3 => 'Left over',
            0 => ''
        );
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            $map['team_id'] = intval($v['team']);
            $map['type'] = $type[intval($v['type'])];
            $map['team_snapshot'] = self::getTeamSnapshotData($v['team'], $tournamentId, $groupId) ?? null;
            switch ($gameId){
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    break;
                default:
                    break;
            }
            $maps[] = $map;
        }
        return $maps;
    }

    // 获取赛事以及游戏数据
    private static function getTournamentData($tournamentId)
    {
        $tournament = Tournament::find()->alias('tour')->select(self::getTournamentColumns())
            ->leftJoin('enum_match_state as tour_status', 'tour_status.id = tour.status')
            ->where(['tour.id' => $tournamentId])->asArray()->one();
        if(!empty($tournament)){
//            $tournament['status'] = self::getMatchBattleStateData($tournament['status']);
            $tournament['image'] = ImageConversionHelper::showFixedSizeConversion($tournament['image'],200,200);
            $tournament['game'] = self::getGameData($tournament['game']);
            $tournament['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['scheduled_begin_at']);
            $tournament['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['scheduled_end_at']);
            $tournament['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['modified_at']);
            $tournament['created_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['created_at']);
            $tournament['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($tournament['deleted_at']);
        }else{
            return [];
        }
        return $tournament;
    }

    // 获取分组以及游戏和赛事数据
    private static function getGroupData($groupId,$gameId)
    {
        $group = TournamentGroup::find()->alias('group')->select(self::getGroupColumns())->where(['group.id' => $groupId])->asArray()->one();
        if (!empty($group)){
            $group['game'] = self::getGameData($gameId);
//            $group['tournament'] = self::getTournamentData($group['tournament']);
            $group['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($group['scheduled_begin_at']);
            $group['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($group['scheduled_end_at']);
            $group['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($group['modified_at']);
            $group['created_at'] = ConversionCriteria::DataTimeConversionCriteria($group['created_at']);
            $group['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($group['deleted_at']);
        }else{
            return [];
        }
        return $group;
    }

    // 获取参赛战队数据
    private static function getTeamsData($teames, $score, $tournamentId, $groupId)
    {
        $teams = [];
        foreach ($teames as $k => $val) {
            $teams[$k]['team_id'] = $val['team_id'];
            $teams[$k]['team_snapshot'] = self::getTeamSnapshotData($val['team_id'], $tournamentId, $groupId);
            $teams[$k]['opponent_order'] = $val['opponent_order'];
            if ($score) {
                $teams[$k]['score'] = $val['score'];
            }
        }
        return $teams;
    }

    private static $snapArray = [];
    // 公共方法获取战队快照
    private static function getTeamSnapshotData($teamId, $tournamentId, $groupId)
    {
        if(!$teamId){
            return [];
        }
        if ($groupId) {
            $codeKey = 'group-'.$groupId.'-'.$teamId;
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $groupId, 'team.type' => 'group'];
        } else if (empty($groupId) && $tournamentId) {
            $codeKey = 'tournament-'.$tournamentId.'-'.$teamId;
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $tournamentId, 'team.type' => 'tournament'];
        } else {
            return [];
        }
        if(isset(self::$snapArray[$codeKey])){
        }else {
            $snapData = TeamSnapshot::find()->alias('team')->select(self::getTeamSnapshotColumns())
                ->leftJoin('team_introduction_snapshot as team_i', 'team.id = team_i.team_id')
                ->leftJoin('enum_country as country', 'team.country = country.id')
                ->where($where)->asArray()->one();
            if (!empty($snapData)){
                $snap['name'] = $snapData['name'];
                $snap['country']['id'] = $snapData['country_id'];
                $snap['country']['name'] = $snapData['country_name'];
                $snap['country']['name_cn'] = $snapData['country_name_cn'];
                $snap['country']['short_name'] = $snapData['country_short_name'];
                $snap['country']['image'] = $snapData['country_image'];
                $snap['full_name'] = $snapData['full_name'];
                $snap['short_name'] = $snapData['short_name'];
                $snap['slug'] = $snapData['slug'];
                $snap['image'] = ImageConversionHelper::showFixedSizeConversion($snapData['image'],200,200);
                $snap['world_ranking'] = $snapData['world_ranking'];
                $snap['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['modified_at']);
                $snap['created_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['created_at']);
                $snap['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['deleted_at']);
            }
            self::$snapArray[$codeKey] = $snap;
        }
        return self::$snapArray[$codeKey];
    }

    // 默认领先战队与获胜战队通用 -- battle下参赛战队和获胜战队也通用
    private static function getAdvantage($teamId, $order, $tournamentId, $groupId)
    {
        $team['team_id'] = $teamId;
        $team['team_snapshot'] = self::getTeamSnapshotData($teamId, $tournamentId, $groupId);;
        $team['opponent_order'] = $order;
        return $team;
    }

    // battle下参赛战队
    private static function getBattleTeamsData($teamIds, $tournamentId, $groupId)
    {
        if($teamIds['team_1_id'] && $teamIds['team_2_id']) {
            $battleTeam = [
                [
                    'team_id' => $teamIds['team_1_id'],
                    'order' => 1,
                ],
                [
                    'team_id' => $teamIds['team_2_id'],
                    'order' => 2,
                ],
            ];
        }else{
            $battleTeam = [];
        }
        $teams = [];
        foreach ($battleTeam as $k => $val) {
            $team = self::getAdvantage($val['team_id'], $val['order'], $tournamentId, $groupId);
            $teams[] = $team;
        }
        return $teams;
    }

    // battle下比分
    private static function getBattleScoresData($battleId, $tournamentId, $groupId)
    {
        $battleScores = MatchBattleTeam::find()->select(['order', 'team_id', 'score'])->where(['battle_id'=>$battleId])->asArray()->all();
        $teams = [];
        foreach ($battleScores as $k => $val) {
            $teams[$k]['team_id'] = $val['team_id'];
            $teams[$k]['team_snapshot'] = self::getTeamSnapshotData($val['team_id'], $tournamentId, $groupId);
            $teams[$k]['opponent_order'] = $val['order'];
            $teams[$k]['score'] = $val['score'];
        }
        return $teams;
    }

    // battle下获胜战队
    private static function getBattleWinnerData($teamIds, $winner, $tournamentId, $groupId)
    {
        if($winner){
            $teamId = ($winner==1 && isset($teamIds['team_1_id'])) || ($winner==2 && isset($teamIds['team_2_id'])) ? $teamIds['team_'.$winner.'_id'] : '';
        }else{
            $teamId = null;
        }
        return self::getAdvantage($teamId, $winner, $tournamentId, $groupId);
    }

    // battle 下地图
    private static function getBattleMapData($mapId, $gameId)
    {
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // csgo地图
                $map = self::getCsgoMapData($mapId);
                break;
            default:
                $map = null;
                break;
        }
        if(!empty($map) && $gameId!=self::GAME_STATUS_CSGO){
            $map['is_default'] = $map['is_default']==1 ? true:false;
            $map['status'] = $map['status']==1 ? 'normal':'removed';
            $map['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($map['modified_at']);
            $map['created_at'] = ConversionCriteria::DataTimeConversionCriteria($map['created_at']);
            $map['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($map['deleted_at']);
        }
        return $map;
    }

    private static $csgoMapArray = [];
    // csgo 地图
    private static function getCsgoMapData($mapId){
        if(isset(self::$csgoMapArray[$mapId])){
        }else{
            $mapData = MetadataCsgoMap::find()->alias('map')->select(self::getMapColumns())
                ->leftJoin('enum_csgo_map as csgomap', 'csgomap.id = map.map_type')
                ->where(['map.id'=>$mapId])->asArray()->one();
            if(!empty($mapData)){
                $mapData['is_default'] = ConversionCriteria::issetTrueFalseType($mapData['is_default']);
                $mapData['status'] = $mapData['status']==1 ? 'normal':'removed';
                $mapData['image'] = [];
                $mapData['image']['square_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['square_image'],256,144);
                $mapData['image']['rectangle_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['rectangle_image'],675,84);
                $mapData['image']['thumbnail'] = ImageConversionHelper::showFixedSizeConversion($mapData['thumbnail'],1380,930);
                unset($mapData['square_image']);
                unset($mapData['rectangle_image']);
                unset($mapData['thumbnail']);
            }else{
                return null;
            }
            self::$csgoMapArray[$mapId] = $mapData;
        }
        return self::$csgoMapArray[$mapId];
    }
    // 地图字段
    private static function getMapColumns()
    {
        return [
            'map.id as map_id',
            'map.name as name',
            'map.name_cn as name_cn',
            'map.state as status',
            'map.external_id as external_id',
            'map.external_name as external_name',
            'map.short_name as short_name',
            'csgomap.e_name as map_type',
            'csgomap.c_name as map_type_cn',
//            'map.map_type as map_type',
//            'map.map_type_cn as map_type_cn',
            'map.is_default as is_default',
            'map.slug as slug',
            'map.is_default as image',
            'map.square_image as square_image',
            'map.rectangle_image as rectangle_image',
            'map.thumbnail as thumbnail',
            'map.modified_at as modified_at',
            'map.created_at as created_at',
            'map.deleted_at as deleted_at',
        ];
    }

    // 对局字段
    private static function getBattleColumns()
    {
        return [
            'battle.id as battle_id',
            'battle.game as game',
            'battle.match as match',
            'battle.order as order',
            'battle.order as teams',
            'battle.begin_at as begin_at',
            'battle.end_at as end_at',
            'battle_status.c_name as status',
//            'battle.status as status',
            'battle.is_draw as is_draw',
            'battle.is_forfeit as is_forfeit',
            'battle.is_default_advantage as is_default_advantage',
            'battle.map as map',
            'battle.duration as duration',
            'battle.winner as scores',
            'battle.winner as winner',
            'battle.is_battle_detailed as is_battle_detailed',
            'battle.winner as battle_detail',
            'battle.modified_at as modified_at',
            'battle.created_at as created_at',
            'battle.deleted_at as deleted_at',
        ];
    }
    // 游戏字段
    private static function getGameColumns()
    {
        return [
            'game.id as game_id',
            'game.e_name as name',
            'game.name as name_cn',
            'game.default_game_rule as rules_id',
            'game.default_match_type as type_id',
            'rules.name as default_game_rules',
            'type.name as default_match_type',
            'game.full_name as full_name',
            'game.short_name as short_name',
            'game.slug as slug',
            'game.image as image',
            'game.modified_at as modified_at',
            'game.created_at as created_at',
            'game.deleted_at as deleted_at',
        ];
    }
    // 比赛字段
    private static function getMatchColumns()
    {
        return [
            'match.id as match_id',
            'match.game as game',
            'match.game as game_id',
            'match.game_rules as rules_id',
            'match.match_type as type_id',
            'rules.name as game_rules',
            'type.name as match_type',
            'match.number_of_games as number_of_games',
            'match.tournament_id as tournament_id',
            'match.tournament_id as tournament',
            'match.group_id as group_id',
            'match.group_id as group',
            'match.scheduled_begin_at as scheduled_begin_at',
            'match.original_scheduled_begin_at as original_scheduled_begin_at',
            'match.is_rescheduled as is_rescheduled',
            'match.team_1_id as teams',
            'match.team_1_id as team_1_id',
            'match.team_2_id as team_2_id',
            'match.slug as slug',

            'match_r.begin_at as begin_at',
            'match_r.end_at as end_at',
            'match_status.c_name as status',
//            'match_r.status as status',
            'match_r.is_draw as is_draw',
            'match_r.is_forfeit as is_forfeit',
            'match_r.default_advantage as default_advantage',
            'match.game as map_vetoes',
            'match.game as maps',
            'match_r.map_ban_pick as map_ban_pick',
            'match_r.map_info as map_info',
            'match_r.team_1_score as scores',
            'match_r.team_1_score as team_1_score',
            'match_r.team_2_score as team_2_score',
            'match_r.winner as winner',
            'match_r.is_battle_detailed as is_battle_detailed',

            'match.modified_at as modified_at',
            'match.created_at as created_at',
            'match.deleted_at as deleted_at',
        ];
    }
    // 赛事字段
    public static function getTournamentColumns()
    {
        return [
            'tour.id as tournament_id',
            'tour.name as name',
            'tour.name_cn as name_cn',
            'tour.game as game',
            'tour_status.c_name as status',
//            'tour.status as status',
            'tour.scheduled_begin_at as scheduled_begin_at',
            'tour.scheduled_end_at as scheduled_end_at',
            'tour.scheduled_begin_at as scheduled_begin_at',
            'tour.short_name as short_name',
            'tour.short_name_cn as short_name_cn',
            'tour.slug as slug',
            'tour.image as image',
            'tour.modified_at as modified_at',
            'tour.created_at as created_at',
            'tour.deleted_at as deleted_at',
        ];
    }
    // 分组字段
    public static function getGroupColumns()
    {
        return [
            'group.id as group_id',
            'group.name as name',
            'group.name_cn as name_cn',
            'group.game as game',
//            'group.tournament_id as tournament',
            'group.scheduled_begin_at as scheduled_begin_at',
            'group.scheduled_end_at as scheduled_end_at',
            'group.modified_at as modified_at',
            'group.created_at as created_at',
            'group.deleted_at as deleted_at',
        ];
    }
    // 战队快照字段
    public static function getTeamSnapshotColumns()
    {
        return [
            'team.name as name',

            'country.id as country_id',
            'country.e_name as country_name',
            'country.name as country_name_cn',
            'country.two as country_short_name',
            'country.image as country_image',

//            'team.country as country',
            'team.full_name as full_name',
            'team.short_name as short_name',
            'team.slug as slug',
            'team.image as image',

            'team_i.world_ranking as world_ranking',

            'team.modified_at as modified_at',
            'team.created_at as created_at',
            'team.deleted_at as deleted_at',
        ];
    }

    // csgo match_battle_team表和match_battle_team_ext_csgo表字段
    private static function getCsgoTeamAndExtColumns()
    {
        return [
            'team_ext.starting_faction_side as starting_side',

            'team.order as order',
            'team.team_id as team_id',
            'team.score as score',

            'team_ext.1st_half_score as first_half_score',
            'team_ext.2nd_half_score as second_half_score',
            'team_ext.ot_score as ot_score',
        ];
    }

}