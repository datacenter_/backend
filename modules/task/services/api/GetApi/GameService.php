<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\models\EnumGame;
use app\modules\common\services\ConversionCriteria;
use yii\data\Pagination;

class GameService
{
    public static function getGameList($params)
    {
        $q = self::getQuery();
//        $where = self::getWhere($params);
//        $q = $q->where($where);
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        return [
            'count'=>$count,
            'list'=>self::getSmallSlug($list),
        ];
    }

    public static function getGameDetail($gameId)
    {
        $q = self::getQuery();
        $q = $q->where(['game.id' => $gameId]);
        $data = $q->select(self::getColumns())->asArray()->all();
        if(!empty($data)){
            $list = self::getSmallSlug($data);
        }else{
            $list = [];
        }
        return $list;
    }

    private static function getSmallSlug($data)
    {
        //转换小写
        foreach ($data as $k=>$v){
            if($v['deleted'] == 1){
                $data[$k]['deleted'] = 2;
            }else{
                $data[$k]['deleted'] = 1;
            }
            $data[$k]['slug'] = $v['slug'];
            $data[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $data[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $data[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return $data;
    }

    private static function getWhere($params)
    {
        return ['game.flag'=>1];
    }
    private static function getColumns()
    {
        return [
            'game.id as game_id',
            'game.flag as deleted',
            'game.e_name as name',
            'game.name as name_cn',
            'rule.name as default_game_rules',
            'match_type.name as default_match_type',
            'game.full_name as full_name',
            'game.e_short as short_name',
            'game.slug as slug',
            'game.logo as image',
            'game.modified_at',
            'game.created_at',
            'game.deleted_at'
        ];
    }

    private static function getQuery()
    {
        return EnumGame::find()->alias('game')
            ->leftjoin('enum_game_rules AS rule','game.default_game_rule = rule.id')
            ->leftjoin('enum_game_match_type AS match_type','game.default_match_type = match_type.id');
    }

}