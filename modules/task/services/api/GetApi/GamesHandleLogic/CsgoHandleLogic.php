<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/12/10
 * Time: 15:10
 */

namespace app\modules\task\services\api\GetApi\GamesHandleLogic;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\EnumWinnerType;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\task\services\api\ApiBase;

class CsgoHandleLogic
{
    const CSGO_EVENT_TYPE_ROUND_START = 'round_start';
    const CSGO_EVENT_TYPE_ROUND_END = 'round_end';
    const CSGO_EVENT_TYPE_PLAYER_KILL = 'player_kill';
    const CSGO_EVENT_TYPE_BOMB_PLANTED = 'bomb_planted';
    const CSGO_EVENT_TYPE_BOMB_DEFUSED = 'bomb_defused';
    const CSGO_EVENT_TYPE_BOMB_EXPLODED = 'bomb_exploded';
    const CSGO_EVENT_TYPE_PLAYER_SUICIDE = 'player_suicide';

    // 获取csgo对局详情数据
    public static function getCsgoBattleDetail($battleWinner,$battleId,$duration,$teamIds)
    {
        // 获取 1-是否为官方完整数据 3-是否已结束 （字段）
        $extCsgoInfo = MatchBattleExtCsgo::find()->select(self::getCsgoBattleExtColumns())->where(['id'=>$battleId])->asArray()->one();
        // 获取 2-对局时长 duration
        $detailData['is_confirmed'] = ConversionCriteria::issetTrueFalseType($extCsgoInfo['is_confirmed']);
        $detailData['duration'] = intval($duration);
        $detailData['winner'] = $battleWinner;
        $detailData['is_pause'] = ConversionCriteria::issetTrueFalseType($extCsgoInfo['is_pause']);
        $detailData['is_live'] = ConversionCriteria::issetTrueFalseType($extCsgoInfo['is_live']);
        $detailData['current_round'] = $extCsgoInfo['current_round'];
        $detailData['is_freeze_time'] = ConversionCriteria::issetTrueFalseType($extCsgoInfo['is_freeze_time']);
        $detailData['in_round_timestamp'] = $extCsgoInfo['in_round_timestamp'];
        $detailData['round_time'] = $extCsgoInfo['round_time'];
        $detailData['is_bomb_planted'] = ConversionCriteria::issetTrueFalseType($extCsgoInfo['is_bomb_planted']);
        $detailData['time_since_plant'] = $extCsgoInfo['time_since_plant'];
        // 武器
        if (!empty(GameBase::$weaponsArray)) {
            $weaponArray = GameBase::$weaponsArray;
        } else {
            $weapons = MetadataCsgoWeapon::find()->alias('wp')
                ->select(['wp.id as weapon_id', 'wp.name', 'wp.external_id', 'wp.external_name', 'kind_weapon.kind', 'wp.slug', 'wp.image'])
                ->leftJoin('enum_csgo_weapon as kind_weapon', 'kind_weapon.id = wp.kind')
                ->where(['deleted' => 2])
                ->asArray()->all();
            foreach ($weapons as $key => $val) {
                $val['image'] = ImageConversionHelper::showMfitSizeConversion($val['image'], 28, 0);
                GameBase::$weaponsArray[$val['weapon_id']] = $val;
            }
            $weaponArray = GameBase::$weaponsArray;
        }
        // 对局战队
        $detailData['teams'] = self::getCsgoTeamData($battleId,$weaponArray);
        // 特殊事件
        if (!empty($extCsgoInfo)) {
            $sqecialEvents['win_round_1']['side'] = $extCsgoInfo['win_round_1_side'];
            $sqecialEvents['win_round_1']['team_id'] = isset($teamIds['team_'.$extCsgoInfo['win_round_1_team'].'_id']) ?
                $teamIds['team_'.$extCsgoInfo['win_round_1_team'].'_id']:$extCsgoInfo['win_round_1_team'];
            $sqecialEvents['win_round_1']['detail'] = $extCsgoInfo['win_round_1_detail'];
            $sqecialEvents['win_round_16']['side'] = $extCsgoInfo['win_round_16_side'];
            $sqecialEvents['win_round_16']['team_id'] = isset($teamIds['team_'.$extCsgoInfo['win_round_16_team'].'_id']) ?
                $teamIds['team_'.$extCsgoInfo['win_round_16_team'].'_id']:$extCsgoInfo['win_round_16_team'];
            $sqecialEvents['win_round_16']['detail'] = $extCsgoInfo['win_round_16_detail'];
            $sqecialEvents['first_to_5_rounds_wins']['side'] = $extCsgoInfo['first_to_5_rounds_wins_side'];
            $sqecialEvents['first_to_5_rounds_wins']['team_id'] = isset($teamIds['team_'.$extCsgoInfo['first_to_5_rounds_wins_team'].'_id']) ?
                $teamIds['team_'.$extCsgoInfo['first_to_5_rounds_wins_team'].'_id']:$extCsgoInfo['first_to_5_rounds_wins_team'];
            $sqecialEvents['first_to_5_rounds_wins']['detail'] = $extCsgoInfo['first_to_5_rounds_wins_detail'];
        }else{
            $sqecialEvents = null;
        }
        $detailData['special_events'] = $sqecialEvents;
        // 回合详情
        $detailData['rounds_detail'] = self::getCsgoRoundsData($battleId,$teamIds,$weaponArray);
        // 有效回合
        $detailData['live_rounds'] = $extCsgoInfo['live_rounds'] ? json_decode($extCsgoInfo['live_rounds'],true):null;
        return $detailData;
    }

    // 获取csgo详情战队完整信息
    private static function getCsgoTeamData($battleId,$weaponArray)
    {
        $teamsData = MatchBattleTeam::find()->alias('team')
            ->select(self::getCsgoTeamAndExtColumns())
            ->leftJoin('match_battle_team_ext_csgo as team_ext', 'team.id = team_ext.id')
            ->where(["team.battle_id"=>$battleId])
            ->orderBy('team_ext.starting_faction_side desc')
            ->asArray()->all();
        // battle下csgo选手数据
        $playersArray = [];
        if (!empty($teamsData)) {
            $players = MatchBattlePlayerCsgo::find()->alias('b_player')
                ->select(self::getCsgoBattlePlayerColumns())
                ->leftJoin('player as player', 'player.id = b_player.player_id')
                ->where(['b_player.battle_id' => $battleId])
                ->asArray()->all();
            foreach ($players as $key => &$player) {
                $teamOrder = $player['team_order'];
                // 选手
                $player['player'] = [];
                $player['player']['player_id'] = $player['player_id'] ?? null;
                if (!empty($player['player_nick_name'])) {
                    $player['player']['nick_name'] = $player['player_nick_name'] ?? null;
                } else {
                    $player['player']['nick_name'] = $player['nick_name'] ?? null;
                }
                $player['player']['steam_id'] = $player['steam_id'] ?? null;
                // 选手武器
                $weaponsJson = $player['weapon'];
                $player['weapon'] = [];
                if(!empty($weaponsJson) && $weaponsJson!='[]'){
                    $weaponIds = json_decode($weaponsJson,true);
                    foreach ($weaponIds as $wId){
                        if(isset($weaponArray[$wId])) {
                            $player['weapon'][] = $weaponArray[$wId];
                        }
                    }
                }
                $player['has_kevlar'] = ConversionCriteria::issetTrueFalseType($player['has_kevlar']);
                $player['has_helmet'] = ConversionCriteria::issetTrueFalseType($player['has_helmet']);
                $player['has_defusekit'] = ConversionCriteria::issetTrueFalseType($player['has_defusekit']);
                $player['has_bomb'] = ConversionCriteria::issetTrueFalseType($player['has_bomb']);
                // 选手投掷物
                $grenadesJson = $player['grenades'];
                $player['grenades'] = [];
                if(!empty($grenadesJson) && $grenadesJson!='[]'){
                    $grenadeIds = json_decode($grenadesJson,true);
                    foreach ($grenadeIds as $gId){
                        if(isset($weaponArray[$gId])) {
                            $player['grenades'][] = $weaponArray[$gId];
                        }
                    }
                }
                $player['is_alive'] = ConversionCriteria::issetTrueFalseType($player['is_alive']);
                // 选手-KAST
                $player['kast'] = sprintf("%.4f",$player['kast']);
                // 选手-advanced数据结构
                $player['advanced'] = [];
                $player['advanced']['position'] = $player['position'];
                $player['advanced']['equipment_value'] = $player['equipment_value'];
                $player['advanced']['blinded_time'] = $player['blinded_time'];
                $player['advanced']['ping'] = $player['ping'];
                $player['advanced']['rating'] = $player['rating'];
                $player['advanced']['damage'] = $player['damage'];
                $player['advanced']['team_damage'] = $player['team_damage'];
                $player['advanced']['damage_taken'] = $player['damage_taken'];
                $player['advanced']['hegrenade_damage_taken'] = $player['hegrenade_damage_taken'];
                $player['advanced']['inferno_damage_taken'] = $player['inferno_damage_taken'];
                $player['advanced']['planted_bomb'] = $player['planted_bomb'];
                $player['advanced']['defused_bomb'] = $player['defused_bomb'];
                $player['advanced']['chicken_kills'] = $player['chicken_kills'];
                $player['advanced']['blind_enemy_time'] = $player['blind_enemy_time'];
                $player['advanced']['blind_teammate_time'] = $player['blind_teammate_time'];
                $player['advanced']['team_kills'] = $player['team_kills'];
                $player['advanced']['two_kills'] = $player['two_kills'];
                $player['advanced']['three_kills'] = $player['three_kills'];
                $player['advanced']['four_kills'] = $player['four_kills'];
                $player['advanced']['five_kills'] = $player['five_kills'];
                $player['advanced']['one_on_one_clutches'] = $player['one_on_one_clutches'];
                $player['advanced']['one_on_two_clutches'] = $player['one_on_two_clutches'];
                $player['advanced']['one_on_three_clutches'] = $player['one_on_three_clutches'];
                $player['advanced']['one_on_four_clutches'] = $player['one_on_four_clutches'];
                $player['advanced']['one_on_five_clutches'] = $player['one_on_five_clutches'];
                $player['advanced']['awp_kills'] = $player['awp_kills'];
                $player['advanced']['knife_kills'] = $player['knife_kills'];
                $player['advanced']['taser_kills'] = $player['taser_kills'];
                $player['advanced']['shotgun_kills'] = $player['shotgun_kills'];
                unset($player['team_order']);
                unset($player['nick_name']);
                unset($player['player_id']);
                unset($player['player_nick_name']);
                unset($player['steam_id']);
                unset($player['position']);
                unset($player['equipment_value']);
                unset($player['blinded_time']);
                unset($player['ping']);
                unset($player['rating']);
                unset($player['damage']);
                unset($player['team_damage']);
                unset($player['damage_taken']);
                unset($player['hegrenade_damage_taken']);
                unset($player['inferno_damage_taken']);
                unset($player['planted_bomb']);
                unset($player['defused_bomb']);
                unset($player['chicken_kills']);
                unset($player['blind_enemy_time']);
                unset($player['blind_teammate_time']);
                unset($player['team_kills']);
                unset($player['two_kills']);
                unset($player['three_kills']);
                unset($player['four_kills']);
                unset($player['five_kills']);
                unset($player['one_on_one_clutches']);
                unset($player['one_on_two_clutches']);
                unset($player['one_on_three_clutches']);
                unset($player['one_on_four_clutches']);
                unset($player['one_on_five_clutches']);
                unset($player['awp_kills']);
                unset($player['knife_kills']);
                unset($player['taser_kills']);
                unset($player['shotgun_kills']);

                $playersArray[$teamOrder][] = $player;
            }
        }
        $teams = [];
        foreach ($teamsData as $team) {
            // 处理teams结构
            $team['advanced'] = [];
            $team['advanced']['rating'] = $team['rating'];
            $team['advanced']['damage'] = $team['damage'];
            $team['advanced']['team_damage'] = $team['team_damage'];
            $team['advanced']['damage_taken'] = $team['damage_taken'];
            $team['advanced']['hegrenade_damage_taken'] = $team['hegrenade_damage_taken'];
            $team['advanced']['inferno_damage_taken'] = $team['inferno_damage_taken'];
            $team['advanced']['planted_bomb'] = $team['planted_bomb'];
            $team['advanced']['defused_bomb'] = $team['defused_bomb'];
            $team['advanced']['chicken_kills'] = $team['chicken_kills'];
            $team['advanced']['blind_enemy_time'] = $team['blind_enemy_time'];
            $team['advanced']['blind_teammate_time'] = $team['blind_teammate_time'];
            $team['advanced']['team_kills'] = $team['team_kills'];
            $team['advanced']['two_kills'] = $team['two_kills'];
            $team['advanced']['three_kills'] = $team['three_kills'];
            $team['advanced']['four_kills'] = $team['four_kills'];
            $team['advanced']['five_kills'] = $team['five_kills'];
            $team['advanced']['one_on_one_clutches'] = $team['one_on_one_clutches'];
            $team['advanced']['one_on_two_clutches'] = $team['one_on_two_clutches'];
            $team['advanced']['one_on_three_clutches'] = $team['one_on_three_clutches'];
            $team['advanced']['one_on_four_clutches'] = $team['one_on_four_clutches'];
            $team['advanced']['one_on_five_clutches'] = $team['one_on_five_clutches'];
            $team['advanced']['awp_kills'] = $team['awp_kills'];
            $team['advanced']['knife_kills'] = $team['knife_kills'];
            $team['advanced']['taser_kills'] = $team['taser_kills'];
            $team['advanced']['shotgun_kills'] = $team['shotgun_kills'];
            unset($team['rating']);
            unset($team['damage']);
            unset($team['team_damage']);
            unset($team['damage_taken']);
            unset($team['hegrenade_damage_taken']);
            unset($team['inferno_damage_taken']);
            unset($team['planted_bomb']);
            unset($team['defused_bomb']);
            unset($team['chicken_kills']);
            unset($team['blind_enemy_time']);
            unset($team['blind_teammate_time']);
            unset($team['team_kills']);
            unset($team['two_kills']);
            unset($team['three_kills']);
            unset($team['four_kills']);
            unset($team['five_kills']);
            unset($team['one_on_one_clutches']);
            unset($team['one_on_two_clutches']);
            unset($team['one_on_three_clutches']);
            unset($team['one_on_four_clutches']);
            unset($team['one_on_five_clutches']);
            unset($team['awp_kills']);
            unset($team['knife_kills']);
            unset($team['taser_kills']);
            unset($team['shotgun_kills']);
            // players - 结构
            $players = isset($playersArray[$team['order']]) ? $playersArray[$team['order']] : [];
            // players - rating倒序
            $team['players'] = ApiBase::sortArrayByField($players,'rating',SORT_DESC);
            unset($team['order']);
            $teams[] = $team;
        }
        return $teams;
    }

    // csgo回合详情完整数据
    private static function getCsgoRoundsData($battleId,$teamIds,$weaponArray)
    {
        // 回合序号表
        $rounds = MatchBattleRoundCsgo::find()->alias('round')
            ->select(['round_ordinal','winner_team_id','winner_end_type'])
            ->where(['round.battle_id'=>$battleId])
            ->asArray()->all();
        // 回合阵营数据
        $roundsSideList = MatchBattleRoundSideCsgo::find()->alias('round_side')
            ->select(self::getCsgoBattleRoundColumns())
            ->where(['round_side.battle_id' => $battleId])
            ->asArray()->all();
        // 回合下选手数据
        $roundPlayers = MatchBattleRoundPlayerCsgo::find()->alias('round_player')
            ->select(self::getCsgoBattleRoundPlayerColumns())
            ->leftJoin('player as player', 'player.id = round_player.player_id')
            ->where(['round_player.battle_id' => $battleId])
            ->asArray()->all();
        // 回合日志
        $eventLogList = MatchBattleRoundEventCsgo::find()->alias('event')
            ->select(self::getCsgoBattleEventColumns())
            ->leftJoin('player as kill_player', 'kill_player.id = event.killer')
            ->leftJoin('player as victim_player', 'victim_player.id = event.victim')
            ->leftJoin('player as assist_player', 'assist_player.id = event.assist')
            ->leftJoin('player as flashassist_player', 'flashassist_player.id = event.flashassist')
            ->leftJoin('player as bomb_planted_player', 'bomb_planted_player.id = event.bomb_planted_player_id')
            ->leftJoin('player as bomb_defused_player', 'bomb_defused_player.id = event.bomb_defused_player_id')
            ->leftJoin('player as suicide_player', 'suicide_player.id = event.suicide_player_id')
            ->where(['event.battle_id' => $battleId])
            ->asArray()->all();
        // 胜利方式数据
        $winnerType = EnumWinnerType::find()->select(['id','winner_type'])->asArray()->all();
        $winnerTypeArray=[];
        foreach($winnerType as $key=>$val){
            $winnerTypeArray[$val['id']]=$val['winner_type'];
        }
        // round - sides
        $roundsSideArray=[];
        foreach($roundsSideList as $side){
            $side['is_first_kill'] = ConversionCriteria::issetTrueFalseType($side['is_first_kill']);
            $side['is_first_death'] = ConversionCriteria::issetTrueFalseType($side['is_first_death']);
            $side['is_one_on_x_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_x_clutch']);

            $side['advanced'] = [];
            $side['advanced']['damage'] = $side['damage'];
            $side['advanced']['team_damage'] = $side['team_damage'];
            $side['advanced']['damage_taken'] = $side['damage_taken'];
            $side['advanced']['hegrenade_damage_taken'] = $side['hegrenade_damage_taken'];
            $side['advanced']['inferno_damage_taken'] = $side['inferno_damage_taken'];
            $side['advanced']['is_planted_bomb'] = ConversionCriteria::issetTrueFalseType($side['is_planted_bomb']);
            $side['advanced']['is_defused_bomb'] = ConversionCriteria::issetTrueFalseType($side['is_defused_bomb']);
            $side['advanced']['chicken_kills'] = $side['chicken_kills'];
            $side['advanced']['blind_enemy_time'] = $side['blind_enemy_time'];
            $side['advanced']['blind_teammate_time'] = $side['blind_teammate_time'];
            $side['advanced']['team_kills'] = $side['team_kills'];
            $side['advanced']['two_kills'] = $side['two_kills'];
            $side['advanced']['three_kills'] = $side['three_kills'];
            $side['advanced']['four_kills'] = $side['four_kills'];
            $side['advanced']['five_kills'] = $side['five_kills'];
            $side['advanced']['is_one_on_one_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_one_clutch']);
            $side['advanced']['is_one_on_two_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_two_clutch']);
            $side['advanced']['is_one_on_three_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_three_clutch']);
            $side['advanced']['is_one_on_four_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_four_clutch']);
            $side['advanced']['is_one_on_five_clutch'] = ConversionCriteria::issetTrueFalseType($side['is_one_on_five_clutch']);
            $side['advanced']['awp_kills'] = $side['awp_kills'];
            $side['advanced']['knife_kills'] = $side['knife_kills'];
            $side['advanced']['taser_kills'] = $side['taser_kills'];
            $side['advanced']['shotgun_kills'] = $side['shotgun_kills'];
            unset($side['damage']);
            unset($side['team_damage']);
            unset($side['damage_taken']);
            unset($side['hegrenade_damage_taken']);
            unset($side['inferno_damage_taken']);
            unset($side['is_planted_bomb']);
            unset($side['is_defused_bomb']);
            unset($side['chicken_kills']);
            unset($side['blind_enemy_time']);
            unset($side['blind_teammate_time']);
            unset($side['team_kills']);
            unset($side['two_kills']);
            unset($side['three_kills']);
            unset($side['four_kills']);
            unset($side['five_kills']);
            unset($side['is_one_on_one_clutch']);
            unset($side['is_one_on_two_clutch']);
            unset($side['is_one_on_three_clutch']);
            unset($side['is_one_on_four_clutch']);
            unset($side['is_one_on_five_clutch']);
            unset($side['awp_kills']);
            unset($side['knife_kills']);
            unset($side['taser_kills']);
            unset($side['shotgun_kills']);

            @$roundsSideArray[$side['round_ordinal']][$side['side_order']]=$side;
        }
        // round - Players
        $roundsPlayersArray=[];
        foreach($roundPlayers as $player){
            $player['player'] = [];
            $player['player']['player_id'] = $player['player_id'];
            if(!empty($player['player_nick_name'])){
                $player['player']['nick_name'] = $player['player_nick_name'];
            }else{
                $player['player']['nick_name'] = $player['nick_name'];
            }
            $player['player']['steam_id'] = $player['steam_id'];
            $player['is_died'] = ConversionCriteria::issetTrueFalseType($player['is_died']);
            $player['is_first_kill'] = ConversionCriteria::issetTrueFalseType($player['is_first_kill']);
            $player['is_first_death'] = ConversionCriteria::issetTrueFalseType($player['is_first_death']);
            $player['is_multi_kill'] = ConversionCriteria::issetTrueFalseType($player['is_multi_kill']);
            $player['is_one_on_x_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_x_clutch']);

            $player['advanced'] = [];
            $player['advanced']['damage'] = $player['damage'];
            $player['advanced']['team_damage'] = $player['team_damage'];
            $player['advanced']['damage_taken'] = $player['damage_taken'];
            $player['advanced']['hegrenade_damage_taken'] = $player['hegrenade_damage_taken'];
            $player['advanced']['inferno_damage_taken'] = $player['inferno_damage_taken'];
            $player['advanced']['is_planted_bomb'] = ConversionCriteria::issetTrueFalseType($player['is_planted_bomb']);
            $player['advanced']['is_defused_bomb'] = ConversionCriteria::issetTrueFalseType($player['is_defused_bomb']);
            $player['advanced']['chicken_kills'] = $player['chicken_kills'];
            $player['advanced']['blind_enemy_time'] = $player['blind_enemy_time'];
            $player['advanced']['blind_teammate_time'] = $player['blind_teammate_time'];
            $player['advanced']['team_kills'] = $player['team_kills'];
            $player['advanced']['is_one_on_one_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_one_clutch']);
            $player['advanced']['is_one_on_two_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_two_clutch']);
            $player['advanced']['is_one_on_three_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_three_clutch']);
            $player['advanced']['is_one_on_four_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_four_clutch']);
            $player['advanced']['is_one_on_five_clutch'] = ConversionCriteria::issetTrueFalseType($player['is_one_on_five_clutch']);
            $player['advanced']['awp_kills'] = $player['awp_kills'];
            $player['advanced']['knife_kills'] = $player['knife_kills'];
            $player['advanced']['taser_kills'] = $player['taser_kills'];
            $player['advanced']['shotgun_kills'] = $player['shotgun_kills'];

            unset($player['damage']);
            unset($player['team_damage']);
            unset($player['damage_taken']);
            unset($player['hegrenade_damage_taken']);
            unset($player['inferno_damage_taken']);
            unset($player['is_planted_bomb']);
            unset($player['is_defused_bomb']);
            unset($player['chicken_kills']);
            unset($player['blind_enemy_time']);
            unset($player['blind_teammate_time']);
            unset($player['team_kills']);
            unset($player['is_one_on_one_clutch']);
            unset($player['is_one_on_two_clutch']);
            unset($player['is_one_on_three_clutch']);
            unset($player['is_one_on_four_clutch']);
            unset($player['is_one_on_five_clutch']);
            unset($player['awp_kills']);
            unset($player['knife_kills']);
            unset($player['taser_kills']);
            unset($player['shotgun_kills']);

            $playerRound = $player['round_ordinal'];
            $playerOrder = $player['player_order'];
            unset($player['round_ordinal']);
            unset($player['player_order']);
            unset($player['player_id']);
            unset($player['nick_name']);
            unset($player['player_nick_name']);
            unset($player['steam_id']);
            @$roundsPlayersArray[$playerRound][$playerOrder][]=$player;
        }
        // 结果数据
        $roundDetail = [];
        // 回合数据
        foreach ($rounds as $roundKey=>$round){
            $roundDetail[$roundKey]['round_ordinal'] = $round['round_ordinal'];
            // 回合日志
            $eventLogArray=[];
            // 首杀
            $openingKillSide = [];
            // 安放炸弹
            $bombPlanted = [];
            // 刀杀
            $knifeKill = [];
            // ACE
            $aceKill = [];
            // 电击枪击杀
            $taser_kill = [];
            // 误杀队友
            $team_kill = [];
            // 回合阵容数据处理
            $roundSidesData = isset($roundsSideArray[$round['round_ordinal']]) ? $roundsSideArray[$round['round_ordinal']] : [];
            foreach ($roundSidesData as $sideKey=>$side){
                $tmpSide = $side;
                $players = isset($roundsPlayersArray[$round['round_ordinal']][$side['side_order']]) ? $roundsPlayersArray[$round['round_ordinal']][$side['side_order']] : [];
                if($tmpSide['is_opening_kill_side'] == 1){
                    $openingKillSide['side'] = $side['side'];
                    $openingKillSide['team_id'] = $side['team_id'];
                    $openingKillSide['detail'] = $side['opening_kill_details']&&$side['opening_kill_details']!='[]' ? json_decode($side['opening_kill_details'],true):null;
                }
                if($tmpSide['is_bomb_planted'] == 1){
                    $bombPlanted['side'] = $side['side'];
                    $bombPlanted['team_id'] = $side['team_id'];
                    $bombPlanted['detail'] = $side['bomb_planted_details']&&$side['bomb_planted_details']!='[]' ? json_decode($side['bomb_planted_details'],true):null;
                }
                if($tmpSide['is_knife_kill'] == 1){
                    $knifeKill[] = [
                        'side' => $side['side'],
                        'team_id' => $side['team_id'],
                        'detail' => $side['knife_kill_details']&&$side['knife_kill_details']!='[]' ? json_decode($side['knife_kill_details'],true):[]
                    ];
                }
                if($tmpSide['is_ace_kill'] == 1){
                    $aceKill['side'] = $side['side'];
                    $aceKill['team_id'] = $side['team_id'];
                    $aceKill['detail'] = $side['ace_kill_details']&&$side['ace_kill_details']!='[]' ? json_decode($side['ace_kill_details'],true):null;
                }
                if($tmpSide['taser_kills']){
                    $taser_kill[] = [
                        'side' => $side['side'],
                        'team_id' => $side['team_id'],
                        'detail' => $side['taser_kill_details']&&$side['taser_kill_details']!='[]' ? json_decode($side['taser_kill_details'],true):[]
                    ];
                }
                if($tmpSide['team_kills']){
                    $team_kill[] = [
                        'side' => $side['side'],
                        'team_id' => $side['team_id'],
                        'detail' => $side['team_kill_details']&&$side['team_kill_details']!='[]' ? json_decode($side['team_kill_details'],true):[]
                    ];
                }
                // unset - side不需要字段
                unset($tmpSide['round_ordinal']);
                unset($tmpSide['side_order']);
                unset($tmpSide['detail']);
                unset($tmpSide['is_opening_kill_side']);
                unset($tmpSide['is_bomb_planted']);
                unset($tmpSide['is_knife_kill']);
                unset($tmpSide['is_ace_kill']);
                // unset-detail
                unset($tmpSide['opening_kill_details']);
                unset($tmpSide['bomb_planted_details']);
                unset($tmpSide['knife_kill_details']);
                unset($tmpSide['taser_kill_details']);
                unset($tmpSide['ace_kill_details']);
                unset($tmpSide['team_kill_details']);
                $tmpSide['players'] = ApiBase::sortArrayByField($players,'damage',SORT_DESC);
                $roundDetail[$roundKey]['side'][]=$tmpSide;
                // 回合日志
                foreach($eventLogList as $key=>$val){
                    if($round['round_ordinal'] == $val['round_ordinal']){
                        $eventLogArray[] = $val;
                        unset($eventLogList[$key]);
                    }
                }
            }
            $roundDetail[$roundKey]['round_log'] = self::getCsgoRoundEventsTypeCo($eventLogArray,$weaponArray,$winnerTypeArray);
            $roundDetail[$roundKey]['special_events'] = [
                'opening_kill' => !empty($openingKillSide) ? $openingKillSide:null,
                'bomb_planted' => !empty($bombPlanted) ? $bombPlanted:null,
                'knife_kill' => $knifeKill,
                'taser_kill' => $taser_kill,
                'ace_kill' => !empty($aceKill) ? $aceKill:null,
                'team_kill' => $team_kill,
            ];
            $winner_team_id = $round['winner_team_id'];
            if($winner_team_id){
                $winner_team_id = isset($teamIds['team_'.$winner_team_id.'_id']) ? $teamIds['team_'.$winner_team_id.'_id'] : $winner_team_id;
            }
            $roundDetail[$roundKey]['winner'] = $winner_team_id;
            $roundDetail[$roundKey]['win_type'] = isset($winnerTypeArray[$round['winner_end_type']]) ? $winnerTypeArray[$round['winner_end_type']] : null;
        }
        return $roundDetail;
    }

    // 获胜方和获胜方式
    private static function getCsgoRoundEventsTypeCo($eventData,$weaponArray,$winnerTypeArray)
    {
        $eventDataInfo = [];
        foreach ($eventData as $k=>$v){
            if($v['round_end_type'] == 6){
                continue;
            }
            $eventDataCo = [];
            $eventDataCo['event_id'] = $v['event_id'];
            $eventDataCo['event_type'] = $v['event_type'];
            $eventDataCo['round_ordinal'] = $v['round_ordinal'];
            $eventDataCo['in_round_timestamp'] = $v['in_round_timestamp'];
            $eventDataCo['round_time'] = $v['round_time'];
            $eventDataCo['is_bomb_planted'] = ConversionCriteria::issetTrueFalseType($v['is_bomb_planted']);
            $eventDataCo['time_since_plant'] = $v['time_since_plant'];
            switch ($v['event_type']){
                case self::CSGO_EVENT_TYPE_ROUND_START:
                    break;
                case self::CSGO_EVENT_TYPE_ROUND_END:
                    $eventDataCo['round_end_type'] = isset($winnerTypeArray[$v['round_end_type']]) ? $winnerTypeArray[$v['round_end_type']] : null;
                    $eventDataCo['winner_side'] = $v['winner_side'];
                    $eventDataCo['ct_score'] = $v['ct_score'];
                    $eventDataCo['t_score'] = $v['t_score'];
                    break;
                case self::CSGO_EVENT_TYPE_PLAYER_KILL:
                    if($v['killer']) {
                        $eventDataCo['killer']['player_id'] = $v['killer'];
                        if (!empty($v['kill_player_name'])) {
                            $eventDataCo['killer']['nick_name'] = $v['kill_player_name'];
                        } else {
                            $eventDataCo['killer']['nick_name'] = $v['killer_nick_name'];
                        }
                        $eventDataCo['killer']['steam_id'] = $v['killer_steam_id'];
                        $eventDataCo['killer']['side'] = $v['killer_side'];
                        $eventDataCo['killer']['position'] = $v['killer_position'];
                    }else{
                        $eventDataCo['killer'] = null;
                    }
                    if($v['victim']){
                        $eventDataCo['victim']['player_id'] = $v['victim'];
                        if(!empty($v['victim_player_name'])){
                            $eventDataCo['victim']['nick_name'] = $v['victim_player_name'];
                        }else{
                            $eventDataCo['victim']['nick_name'] = $v['victim_nick_name'];
                        }
                        $eventDataCo['victim']['steam_id'] = $v['victim_steam_id'];
                        $eventDataCo['victim']['side'] = $v['victim_side'];
                        $eventDataCo['victim']['position'] = $v['victim_position'];
                    }else{
                        $eventDataCo['victim'] = null;
                    }
                    if($v['assist']) {
                        $eventDataCo['assist']['player_id'] = $v['assist'];
                        if (!empty($v['assist_player_name'])) {
                            $eventDataCo['assist']['nick_name'] = $v['assist_player_name'];
                        } else {
                            $eventDataCo['assist']['nick_name'] = $v['assist_nick_name'];
                        }
                        $eventDataCo['assist']['steam_id'] = $v['assist_steam_id'];
                        $eventDataCo['assist']['side'] = $v['assist_side'];
                    }else{
                        $eventDataCo['assist'] = null;
                    }
                    if($v['flashassist']) {
                        $eventDataCo['flashassist']['player_id'] = $v['flashassist'];
                        if (!empty($v['flashassist_player_name'])) {
                            $eventDataCo['flashassist']['nick_name'] = $v['flashassist_player_name'];
                        } else {
                            $eventDataCo['flashassist']['nick_name'] = $v['flashassistt_nick_name'];
                        }
                        $eventDataCo['flashassist']['steam_id'] = $v['flashassist_steam_id'];
                        $eventDataCo['flashassist']['side'] = $v['flashassist_side'];
                    }else{
                        $eventDataCo['flashassist'] = null;
                    }
                    if(isset($weaponArray[$v['weapon']])){
                        $weapon = $weaponArray[$v['weapon']];
                        $eventDataCo['weapon'] = $weapon;
                    }else{
                        $eventDataCo['weapon'] = null;
                    }
                    $eventDataCo['damage'] = $v['damage'];
                    $eventDataCo['hit_group'] = $v['hit_group'];
                    $eventDataCo['is_headshot'] = ConversionCriteria::issetTrueFalseType($v['is_headshot']);
                    $eventDataCo['special_description'] = $v['special_description'];
                    break;
                case self::CSGO_EVENT_TYPE_BOMB_PLANTED:
                    $eventDataCo['player_id'] = $v['bomb_planted_player_id'];
                    if(!empty($v['bomb_planted_player_name'])){
                        $eventDataCo['nick_name'] = $v['bomb_planted_player_name'];
                    }else{
                        $eventDataCo['nick_name'] = $v['bomb_planted_nick_name'];
                    }
                    $eventDataCo['steam_id'] = $v['steam_id'];
                    $eventDataCo['side'] = $v['bomb_planted_side'];
                    $eventDataCo['position'] = $v['bomb_planted_position'];
                    $eventDataCo['survived_players_ct'] = $v['survived_players_ct'];
                    $eventDataCo['survived_players_t'] = $v['survived_players_t'];
                    break;
                case self::CSGO_EVENT_TYPE_BOMB_DEFUSED:
                    $eventDataCo['player_id'] = $v['bomb_defused_player_id'];
                    if(!empty($v['bomb_defused_player_name'])){
                        $eventDataCo['nick_name'] = $v['bomb_defused_player_name'];
                    }else{
                        $eventDataCo['nick_name'] = $v['bomb_defused_nick_name'];
                    }
                    $eventDataCo['steam_id'] = $v['steam_id'];
                    $eventDataCo['side'] = $v['bomb_defused_side'];
                    $eventDataCo['position'] = $v['bomb_defused_position'];
                    break;
                // 这个事件没有数据
                case self::CSGO_EVENT_TYPE_BOMB_EXPLODED:
                    $eventDataCo['bomb_exploded_time'] = $v['bomb_exploded_time'];
                    break;
                case self::CSGO_EVENT_TYPE_PLAYER_SUICIDE:
                    $eventDataCo['player_id'] = $v['suicide_player_id'];
                    if(!empty($v['suicide_player_name'])){
                        $eventDataCo['nick_name'] = $v['suicide_player_name'];
                    }else{
                        $eventDataCo['nick_name'] = $v['suicide_nick_name'];
                    }
                    $eventDataCo['steam_id'] = $v['steam_id'];
                    $eventDataCo['side'] = $v['suicide_side'];
                    $eventDataCo['position'] = $v['suicide_position'];
                    if(isset($weaponArray[$v['weapon']])){
                        $weapon = $weaponArray[$v['weapon']];
                        $eventDataCo['weapon'] = $weapon;
                    }else{
                        $eventDataCo['weapon'] = null;
                    }
                    break;
                default :
                    continue;
            }
            $eventDataInfo[] = $eventDataCo;
        }
        return $eventDataInfo;
    }

    // csgo match_battle_team表和match_battle_team_ext_csgo表字段
    private static function getCsgoTeamAndExtColumns()
    {
        return [
            'team.order as order',
            'team_ext.starting_faction_side as starting_side',
            'team.team_id as team_id',
            'team.score as score',
            'team_ext.1st_half_score as first_half_score',
            'team_ext.2nd_half_score as second_half_score',
            'team_ext.ot_score as ot_score',
            'team_ext.kills as kills',
            'team_ext.headshot_kills as headshot_kills',
            'team_ext.deaths as deaths',
            'team_ext.kd_diff as kd_diff',
            'team_ext.assists as assists',
            'team_ext.flash_assists as flash_assists',
            'team_ext.adr as adr',
            'team_ext.first_kills as first_kills',
            'team_ext.first_deaths as first_deaths',
            'team_ext.first_kills_diff as first_kills_diff',
            'team_ext.multi_kills as multi_kills',
            'team_ext.one_on_x_clutches as one_on_x_clutches',
            'team_ext.kast as kast',

            // 下面准备 unset掉
            'team_ext.rating as rating',
            'team_ext.damage as damage',
            'team_ext.team_damage as team_damage',
            'team_ext.damage_taken as damage_taken',
            'team_ext.hegrenade_damage_taken as hegrenade_damage_taken',
            'team_ext.inferno_damage_taken as inferno_damage_taken',
            'team_ext.planted_bomb as planted_bomb',
            'team_ext.defused_bomb as defused_bomb',
            'team_ext.chicken_kills as chicken_kills',
            'team_ext.blind_enemy_time as blind_enemy_time',
            'team_ext.blind_teammate_time as blind_teammate_time',
            'team_ext.team_kills as team_kills',
            'team_ext.two_kills as two_kills',
            'team_ext.three_kills as three_kills',
            'team_ext.four_kills as four_kills',
            'team_ext.five_kills as five_kills',
            'team_ext.one_on_one_clutches as one_on_one_clutches',
            'team_ext.one_on_two_clutches as one_on_two_clutches',
            'team_ext.one_on_three_clutches as one_on_three_clutches',
            'team_ext.one_on_four_clutches as one_on_four_clutches',
            'team_ext.one_on_five_clutches as one_on_five_clutches',
            'team_ext.awp_kills as awp_kills',
            'team_ext.knife_kills as knife_kills',
            'team_ext.taser_kills as taser_kills',
            'team_ext.shotgun_kills as shotgun_kills',
        ];
    }

    // csgo match_battle_player_csgo表字段
    private static function getCsgoBattlePlayerColumns()
    {
        return [
            'b_player.team_order as team_order',

            'b_player.side as side',
            'b_player.player_id as player',
            'b_player.player_id as player_id',
            'b_player.nick_name as nick_name',
            'player.nick_name as player_nick_name',
            'b_player.steam_id as steam_id',
            'b_player.weapon as weapon',
            'b_player.has_kevlar as has_kevlar',
            'b_player.has_helmet as has_helmet',
            'b_player.has_defusekit as has_defusekit',
            'b_player.has_bomb as has_bomb',
            'b_player.grenades as grenades',
            'b_player.hp as hp',
            'b_player.is_alive as is_alive',
            'b_player.money as money',
            'b_player.kills as kills',
            'b_player.headshot_kills as headshot_kills',
            'b_player.deaths as deaths',
            'b_player.k_d_diff as kd_diff',
            'b_player.assists as assists',
            'b_player.flash_assists as flash_assists',
            'b_player.adr as adr',
            'b_player.first_kills as first_kills',
            'b_player.first_deaths as first_deaths',
            'b_player.first_kills_diff as first_kills_diff',
            'b_player.multi_kills as multi_kills',
            'b_player.1_v_n_clutches as one_on_x_clutches',
            'b_player.kast as kast',
            // 下面字段需要unset
            'b_player.position as position',
            'b_player.equipment_value as equipment_value',
            'b_player.blinded_time as blinded_time',
            'b_player.ping as ping',
            'b_player.rating as rating',
            'b_player.damage as damage',
            'b_player.team_damage as team_damage',
            'b_player.damage_taken as damage_taken',
            'b_player.hegrenade_damage_taken as hegrenade_damage_taken',
            'b_player.inferno_damage_taken as inferno_damage_taken',
            'b_player.planted_bomb as planted_bomb',
            'b_player.defused_bomb as defused_bomb',
            'b_player.chicken_kills as chicken_kills',
            'b_player.blind_enemy_time as blind_enemy_time',
            'b_player.blind_teammate_time as blind_teammate_time',
            'b_player.team_kills as team_kills',
            'b_player.two_kills as two_kills',
            'b_player.three_kills as three_kills',
            'b_player.four_kills as four_kills',
            'b_player.five_kills as five_kills',
            'b_player.one_on_one_clutches as one_on_one_clutches',
            'b_player.one_on_two_clutches as one_on_two_clutches',
            'b_player.one_on_three_clutches as one_on_three_clutches',
            'b_player.one_on_four_clutches as one_on_four_clutches',
            'b_player.one_on_five_clutches as one_on_five_clutches',
            'b_player.awp_kills as awp_kills',
            'b_player.knife_kills as knife_kills',
            'b_player.taser_kills as taser_kills',
            'b_player.shotgun_kills as shotgun_kills',
        ];
    }

    // csgo match_battle_ext_csgo字段
    private static function getCsgoBattleExtColumns()
    {
        return [
            'is_confirmed',
            'is_pause',
            'is_live',
            'current_round',
            'is_freeze_time',
            'in_round_timestamp',
            'round_time',
            'is_bomb_planted',
            'time_since_plant',
            'live_rounds',
            'win_round_1_side',
            'win_round_1_team',
            'win_round_1_detail',
            'win_round_16_side',
            'win_round_16_team',
            'win_round_16_detail',
            'first_to_5_rounds_wins_side',
            'first_to_5_rounds_wins_team',
            'first_to_5_rounds_wins_detail',
        ];
    }

    // csgo match_battle_round_player_csgo字段
    private static function getCsgoBattleRoundPlayerColumns()
    {
        return [
            'round_player.round_ordinal as round_ordinal',
            'round_player.player_order as player_order',

            'round_player.player_id as player',
            'round_player.player_id as player_id',
            'round_player.nick_name as nick_name',
            'player.nick_name as player_nick_name',
            'round_player.steam_id as steam_id',
            'round_player.kills as kills',
            'round_player.headshot_kills as headshot_kills',
            'round_player.is_died as is_died',
            'round_player.assists as assists',
            'round_player.flash_assists as flash_assists',
            'round_player.is_first_kill as is_first_kill',
            'round_player.is_first_death as is_first_death',
            'round_player.is_multi_kill as is_multi_kill',
            'round_player.is_1_v_n_clutche as is_one_on_x_clutch',
            // 下面字段unset
            'round_player.damage as damage',
            'round_player.team_damage as team_damage',
            'round_player.damage_taken as damage_taken',
            'round_player.hegrenade_damage_taken as hegrenade_damage_taken',
            'round_player.inferno_damage_taken as inferno_damage_taken',
            'round_player.is_planted_bomb as is_planted_bomb',
            'round_player.is_defused_bomb as is_defused_bomb',
            'round_player.chicken_kills as chicken_kills',
            'round_player.blind_enemy_time as blind_enemy_time',
            'round_player.blind_teammate_time as blind_teammate_time',
            'round_player.team_kills as team_kills',
            'round_player.is_one_on_one_clutch as is_one_on_one_clutch',
            'round_player.is_one_on_two_clutch as is_one_on_two_clutch',
            'round_player.is_one_on_three_clutch as is_one_on_three_clutch',
            'round_player.is_one_on_four_clutch as is_one_on_four_clutch',
            'round_player.is_one_on_five_clutch as is_one_on_five_clutch',
            'round_player.awp_kills as awp_kills',
            'round_player.knife_kills as knife_kills',
            'round_player.taser_kills as taser_kills',
            'round_player.shotgun_kills as shotgun_kills',
        ];
    }

    // csgo match_battle_round_side_csgo字段
    private static function getCsgoBattleRoundColumns()
    {
        return [
            'round_side.side_order as side_order',
            'round_side.round_ordinal as round_ordinal',

            'round_side.side as side',
            'round_side.team_id as team_id',
            'round_side.survived_players as survived_players',
            'round_side.kills as kills',
            'round_side.headshot_kills as headshot_kills',
            'round_side.deaths as deaths',
            'round_side.assists as assists',
            'round_side.flash_assists as flash_assists',
            'round_side.is_first_kill as is_first_kill',
            'round_side.is_first_death as is_first_death',
            'round_side.multi_kills as multi_kills',
            'round_side.is_one_on_x_clutch as is_one_on_x_clutch',
            // 下面字段unset
            'round_side.damage as damage',
            'round_side.team_damage as team_damage',
            'round_side.damage_taken as damage_taken',
            'round_side.hegrenade_damage_taken as hegrenade_damage_taken',
            'round_side.inferno_damage_taken as inferno_damage_taken',
            'round_side.is_planted_bomb as is_planted_bomb',
            'round_side.is_defused_bomb as is_defused_bomb',
            'round_side.chicken_kills as chicken_kills',
            'round_side.blind_enemy_time as blind_enemy_time',
            'round_side.blind_teammate_time as blind_teammate_time',
            'round_side.team_kills as team_kills',
            'round_side.two_kills as two_kills',
            'round_side.three_kills as three_kills',
            'round_side.four_kills as four_kills',
            'round_side.five_kills as five_kills',
            'round_side.is_one_on_one_clutch as is_one_on_one_clutch',
            'round_side.is_one_on_two_clutch as is_one_on_two_clutch',
            'round_side.is_one_on_three_clutch as is_one_on_three_clutch',
            'round_side.is_one_on_four_clutch as is_one_on_four_clutch',
            'round_side.is_one_on_five_clutch as is_one_on_five_clutch',
            'round_side.awp_kills as awp_kills',
            'round_side.knife_kills as knife_kills',
            'round_side.taser_kills as taser_kills',
            'round_side.shotgun_kills as shotgun_kills',
            'round_side.is_opening_kill_side as is_opening_kill_side',
            'round_side.is_bomb_planted as is_bomb_planted',
            'round_side.is_knife_kill as is_knife_kill',
            'round_side.is_ace_kill as is_ace_kill',
            // detail
            'round_side.opening_kill_details as opening_kill_details',
            'round_side.bomb_planted_details as bomb_planted_details',
            'round_side.knife_kill_details as knife_kill_details',
            'round_side.taser_kill_details as taser_kill_details',
            'round_side.ace_kill_details as ace_kill_details',
            'round_side.team_kill_details as team_kill_details',
        ];
    }

    // csgo match_battle_round_event_csgo 回合日志字段
    private static function getCsgoBattleEventColumns()
    {
        return [
            'event.event_id as event_id',
            'event.event_type as event_type',
            'event.round_ordinal as round_ordinal',
            'event.in_round_timestamp as in_round_timestamp',
            'event.round_time as round_time',
            'event.is_bomb_planted as is_bomb_planted',
            'event.time_since_plant as time_since_plant',

            // 回合开始
            'event.in_round_timestamp as in_round_timestamp',
            'event.round_time as round_time',
            // 回合结束
            'event.round_end_type as round_end_type',
            'event.winner_side as winner_side',
            'event.ct_score as ct_score',
            'event.t_score as t_score',
            // 击杀者事件
            'event.killer as killer',
            'event.killer_nick_name as killer_nick_name',
            'event.killer_steam_id as killer_steam_id',
            'event.killer_side as killer_side',
            'event.killer_position as killer_position',
            'event.special_description as special_description',
            // 受害者事件
            'event.victim as victim',
            'event.victim_nick_name as victim_nick_name',
            'event.victim_steam_id as victim_steam_id',
            'event.victim_side as victim_side',
            'event.victim_position as victim_position',
            // 助攻事件
            'event.assist as assist',
            'event.assist_nick_name as assist_nick_name',
            'event.assist_steam_id as assist_steam_id',
            'event.assist_side as assist_side',
            // 闪光弹助攻事件
            'event.flashassist as flashassist',
            'event.flashassistt_nick_name as flashassistt_nick_name',
            'event.flashassist_steam_id as flashassist_steam_id',
            'event.flashassist_side as flashassist_side',
            // 武器
            'event.weapon as weapon',
            // 伤害/击杀命中部位/是否为爆头击杀
            'event.damage as damage',
            'event.hit_group as hit_group',
            'event.is_headshot as is_headshot',
            // 炸弹安放（event_type=bomb_planted）
            'event.bomb_planted_player_id as bomb_planted_player_id',
            'event.bomb_planted_nick_name as bomb_planted_nick_name',
            'event.bomb_planted_side as bomb_planted_side',
            'event.bomb_planted_position as bomb_planted_position',
            'event.survived_players_ct as survived_players_ct',
            'event.survived_players_t as survived_players_t',
            // 炸弹拆除（event_type=bomb_defused）
            'event.bomb_defused_player_id as bomb_defused_player_id',
            'event.bomb_defused_nick_name as bomb_defused_nick_name',
            'event.bomb_defused_side as bomb_defused_side',
            'event.bomb_defused_position as bomb_defused_position',
            // 炸弹爆炸（event_type=bomb_exploded）
            'event.bomb_exploded_time as bomb_exploded_time',
            // 自杀事件
            'event.suicide_player_id as suicide_player_id',
            'event.suicide_nick_name as suicide_nick_name',
            'event.suicide_side as suicide_side',
            'event.suicide_position as suicide_position',

            // steam_id (bomb_planted/bomb_defused/player_suicide) 共用
            'event.steam_id as steam_id',

            // 本库选手名称
            'kill_player.nick_name as kill_player_name',
            'victim_player.nick_name as victim_player_name',
            'assist_player.nick_name as assist_player_name',
            'flashassist_player.nick_name as flashassist_player_name',
            'bomb_planted_player.nick_name as bomb_planted_player_name',
            'bomb_defused_player.nick_name as bomb_defused_player_name',
            'suicide_player.nick_name as suicide_player_name',
        ];
    }
}
