<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/12/05
 * Time: 19:05
 */

namespace app\modules\task\services\api\GetApi\GamesHandleLogic;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\MatchBattleEventDota2;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattlePlayerDota2;
use app\modules\match\models\MatchBattleTeam;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;

class DotaHandleLogic
{
    // 获取dota2对局详情数据
    public static function getDotaBattleDetail($battleWinner,$battleId,$duration,$battleStatus,$eventMap,$battleEventsTeams)
    {
        // 英雄所有数据
        if (!empty(GameBase::$heroesArray)) {
            $heroesArray = GameBase::$heroesArray;
        } else {
            $heroes = MetadataDota2Hero::find()->alias('hero')->select(self::getDotaHeroColumns())->where(['deleted' => 2])->asArray()->all();
            foreach ($heroes as $k => $heroInfo) {
                GameBase::$heroesArray[$heroInfo['hero_id']] = $heroInfo;
            }
            $heroesArray = GameBase::$heroesArray;
        }
        // ExtDota2数据
        $extDotaInfo = MatchBattleExtDota2::find()->where(['id' => $battleId])->asArray()->one();

        // 获取 2-对局时长
        $detailInfo['is_confirmed'] = ConversionCriteria::issetTrueFalseType($extDotaInfo['is_confirmed']);
        $detailInfo['duration'] = $duration;
        $detailInfo['winner'] = $battleWinner;
        $detailInfo['is_pause'] = ConversionCriteria::issetTrueFalseType($extDotaInfo['is_pause']);
        $detailInfo['is_live'] = ConversionCriteria::issetTrueFalseType($extDotaInfo['is_live']);
        $detailInfo['time_of_day'] = $extDotaInfo['time_of_day'];
        // 精英怪状态
        $detailInfo['elites_status'] = $extDotaInfo['elites_status']&&$extDotaInfo['elites_status']!='[]' ?
            json_decode($extDotaInfo['elites_status'],true):[
                'roshan_status' => null,
            ];
        // 英雄bp
        $detailInfo['ban_pick'] = empty($extDotaInfo['ban_pick']) ? [] : self::getBanPickData($extDotaInfo['ban_pick'],$heroesArray);
        // 所有道具数据
        if (!empty(GameBase::$itemsDotaArray)) {
            $itemArray = GameBase::$itemsDotaArray;
        } else {
            $itemAll = MetadataDota2Item::find()->alias('item')->select(self::getBattleItemColumns())->where(['deleted' => 2])->asArray()->all();
            foreach ($itemAll as $k => $item) {
                $item['is_recipe'] = ConversionCriteria::issetTrueFalseType($item['is_recipe']);
                $item['is_secret_shop'] = ConversionCriteria::issetTrueFalseType($item['is_secret_shop']);
                $item['is_home_shop'] = ConversionCriteria::issetTrueFalseType($item['is_home_shop']);
                $item['is_neutral_drop'] = ConversionCriteria::issetTrueFalseType($item['is_neutral_drop']);
                $item['image'] = ImageConversionHelper::showFixedSizeConversion($item['image'], 64, 88);
                GameBase::$itemsDotaArray[$item['item_id']] = $item;
            }
            $itemArray = GameBase::$itemsDotaArray;
        }
        // 阵营结构数据
        $factionsData = self::getFactionsData($battleId,$heroesArray,$itemArray);
        $detailInfo['factions'] = $factionsData['battle_factions'];
        // 第一次特殊事件
        $detailInfo['first_events'] = self::getFirstEventInfo($extDotaInfo,$factionsData['first_factions']);
        // 事件时间线
        $detailInfo['timeline'] = [
            'net_worth_diff_timeline' => $extDotaInfo['net_worth_diff_timeline'] ? json_decode($extDotaInfo['net_worth_diff_timeline'],true) : [],
            'experience_diff_timeline' => $extDotaInfo['experience_diff_timeline'] ? json_decode($extDotaInfo['experience_diff_timeline'],true) : [],
        ];
        // events
        $detailInfo['events'] = self::getEvents($battleId,$factionsData['first_factions'],$heroesArray,$eventMap,$battleEventsTeams,$battleWinner,$itemArray);
        return $detailInfo;
    }

    // 获取阵营数据
    private static function getFactionsData($battleId,$heroesArray,$itemArray)
    {
        // 阵营数据
        $factions = MatchBattleTeam::find()->alias('team')->select(self::getBattleFactionsExtColumns())
            ->leftJoin('match_battle_team_ext_dota2 as team_ext', 'team.id = team_ext.id')
            ->where(['team.battle_id' => $battleId])
            ->orderBy('team_ext.faction desc')
            ->asArray()->all();
        if(!$factions){
            return [];
        }
        // 获取players数据
        $battlePlayers = MatchBattlePlayerDota2::find()->alias('player')
            ->select(self::getDotaPlayerColumns())
            ->leftJoin('enum_position as position', 'position.id = player.role')
            ->leftJoin('enum_lane as lane', 'lane.id = player.lane')
            ->where(['in', 'player.team_order', [1,2]])
            ->andWhere(['player.battle_id'=>$battleId])
            ->orderBy('player.seed')->asArray()->all();
        // 技能升级时间线 - 技能/天赋 id数据
        $abilityIdArray = $talentIdArray = [];
        foreach ($battlePlayers as &$player) {
            // 选手的天赋
            if ($player['talents'] && $player['talents']!='[]') {
                $playerTalents = json_decode($player['talents'], true);
                foreach ($playerTalents as $pTalent) {
                    if ($pTalent['talent_id']) {
                        $talentIdArray = array_unique(array_merge($talentIdArray, [$pTalent['talent_id']]));
                    }
                }
            }
            // 技能升级时间线
            $abilitiesTimelineJson = $player['abilities_timeline'];
            if ($abilitiesTimelineJson && $abilitiesTimelineJson!='[]') {
                $abilitiesTimeline = json_decode($abilitiesTimelineJson,true);
                foreach ($abilitiesTimeline as $abilitiesInfos) {
                    foreach ($abilitiesInfos as $abilitiesInfo) {
                        if(isset($abilitiesInfo['ability_id'])){
                            array_push($abilityIdArray,$abilitiesInfo['ability_id']);
                        }
                        if(isset($abilitiesInfo['talent_id'])){
                            array_push($talentIdArray,$abilitiesInfo['talent_id']);
                        }
                    }
                }
            }
        }
        // 技能升级时间线 - 技能/天赋数据
        $abilityArray = $talentArray = [];
        // 技能数据
        if(!empty($abilityIdArray)){
            $abilityIdArray = array_unique($abilityIdArray);
            $abilitys = MetadataDota2Ability::find()->alias('ability')->select(self::getAbilityColumns())
                ->where(['in', 'id', $abilityIdArray])->asArray()->all();
            foreach ($abilitys as $ability){
                $ability['image'] = ImageConversionHelper::showFixedSizeConversion($ability['image'],50,50);
                $abilityArray[$ability['ability_id']] = $ability;
            }
        }
        // 天赋数据
        if(!empty($talentIdArray)){
            $talentIdArray = array_unique($talentIdArray);
            $talents = MetadataDota2Talent::find()->alias('talent')->select(self::getTalentColumns())
                ->where(['in', 'id', $talentIdArray])->asArray()->all();
            foreach ($talents as $talent){
                $talentArray[$talent['talent_id']] = $talent;
            }
        }
        // 处理battle-players结构
        $battlePlayerArray = [];
        foreach ($battlePlayers as $key=>&$playerInfo) {
            $playerItems = [
                'inventory' => [
                    'slot_1' => null,
                    'slot_2' => null,
                    'slot_3' => null,
                    'slot_4' => null,
                    'slot_5' => null,
                    'slot_6' => null,
                ],
                'backpack' => [
                    'slot_1' => null,
                    'slot_2' => null,
                    'slot_3' => null,
                ],
                'neutral' => null,
                'stash' => [
                    'slot_1' => null,
                    'slot_2' => null,
                    'slot_3' => null,
                    'slot_4' => null,
                    'slot_5' => null,
                    'slot_6' => null,
                ],
                'buffs' => [],
                'mob_inventory' => [
                    'slot_1' => null,
                    'slot_2' => null,
                    'slot_3' => null,
                ],
                'mob_backpack' => [
                    'slot_1' => null,
                    'slot_2' => null,
                    'slot_3' => null,
                ],
                'mob_neutral' => null,
            ];
            if(!empty($playerInfo['items']) && $playerInfo['items']!='[]'){
                $playerItemsInfo = json_decode($playerInfo['items'],true);
                foreach ($playerItemsInfo as $field=>$data){
                    if($field == 'neutral' || $field == 'mob_neutral'){
                        if (isset($playerItemsInfo[$field]['item_id']) && $playerItemsInfo[$field]['item_id']) {
                            unset($playerItemsInfo[$field]['image']);
                            $playerItems[$field] = array_merge($itemArray[$playerItemsInfo[$field]['item_id']], $playerItemsInfo[$field]);
                        }
                        continue;
                    }
                    foreach ($data as $k=>$itemVal){
                        if (array_key_exists($k,$playerItems[$field]) && $itemVal['item_id']) {
                            $playerItems[$field][$k] = array_merge($itemArray[$itemVal['item_id']], $itemVal);
                        }
                    }
                }
            }
            // 英雄天赋
            $playerHero = self::getHeroData($playerInfo['hero'],$heroesArray,true);
            $playerHeroTalent = json_decode($playerHero['talents'],true);
            unset($playerHero['talents']);
            $currentPlayerBaseData = [
                'seed' => $playerInfo['seed'],
                'faction' => $playerInfo['faction'],
                'role' => $playerInfo['role'],
                'lane' => $playerInfo['lane'],
                'player' => [
                    'player_id' => $playerInfo['player_id'],
                    'nick_name' => $playerInfo['nick_name'] ? $playerInfo['nick_name']:$playerInfo['rel_nick_name'],
                    'steam_id' => $playerInfo['steam_id'],
                ],
                'hero' => $playerHero,
                'level' => $playerInfo['level'],
                'is_alive' => ConversionCriteria::issetTrueFalseType($playerInfo['is_alive']),
                'kills' => $playerInfo['kills'],
                'deaths' => $playerInfo['deaths'],
                'assists' => $playerInfo['assists'],
                'kda' => $playerInfo['kda'],
                'participation' => $playerInfo['participation'],
                'last_hits' => $playerInfo['last_hits'],
                'lhpm' => $playerInfo['lhpm'],
                'denies' => $playerInfo['denies'],
                'items' => $playerItems,
            ];
            $unsetFields = [
                'seed', 'faction', 'role', 'lane', 'player_id', 'nick_name', 'rel_nick_name', 'steam_id', 'hero', 'level', 'is_alive',
                'kills', 'deaths', 'assists', 'kda', 'participation', 'last_hits', 'lhpm', 'denies', 'items',
            ];
            foreach ($unsetFields as $field){
                unset($playerInfo[$field]);
            }
            // 高级字段
            $playerAdvanced = $playerInfo;
            // 是否隐身
            $playerAdvanced['is_visible'] = ConversionCriteria::issetTrueFalseType($playerAdvanced['is_visible']);
            // 所有天赋
            $currentPlayerTalentsJson = $playerAdvanced['talents'];
            $playerAdvanced['talents'] = [];
            if ($currentPlayerTalentsJson && $currentPlayerTalentsJson!='[]') {
                $playerTalents = json_decode($currentPlayerTalentsJson,true);
                foreach ($playerTalents as $playerTalentInfo) {
                    if (isset($talentArray[$playerTalentInfo['talent_id']])) {
                        $currentPlayerTalent = $talentArray[$playerTalentInfo['talent_id']];
                        $currentPlayerTalent['is_chosen'] = $playerTalentInfo['is_chosen'];
                        foreach ($playerHeroTalent as $playerHeroTalentInfo) {
                            if ($playerHeroTalentInfo['left'] == $playerTalentInfo['talent_id']) {
                                $currentPlayerTalent['level'] = $playerHeroTalentInfo['id'];
                                $currentPlayerTalent['left_right'] = "left";
                            } elseif ($playerHeroTalentInfo['right'] == $playerTalentInfo['talent_id']) {
                                $currentPlayerTalent['level'] = $playerHeroTalentInfo['id'];
                                $currentPlayerTalent['left_right'] = "right";
                            }
                        }
                        $playerAdvanced['talents'][] = $currentPlayerTalent;
                    }
                }
            }
            // 技能升级时间线
            $abilitiesTimelineJson = $playerAdvanced['abilities_timeline'];
            $playerAdvanced['abilities_timeline'] = [];
            if($abilitiesTimelineJson && $abilitiesTimelineJson!='[]'){
                $abilitiesTalentsTimelineArray = json_decode($abilitiesTimelineJson,true);
                foreach ($abilitiesTalentsTimelineArray as &$abilitiesTalentTimelines) {
                    foreach ($abilitiesTalentTimelines as &$abilitiesTalentTimeline) {
                        if ($abilitiesTalentTimeline['type'] == 'ability') {
                            if (isset($abilityArray[$abilitiesTalentTimeline['ability_id']])) {
                                $playerAdvanced['abilities_timeline'][] = array_merge(
                                    $abilitiesTalentTimeline, $abilityArray[$abilitiesTalentTimeline['ability_id']]
                                );
                            }
                        }
                        if ($abilitiesTalentTimeline['type'] == 'talent') {
                            if (isset($talentArray[$abilitiesTalentTimeline['talent_id']])) {
                                $currentTalentTimeline = array_merge($abilitiesTalentTimeline, $talentArray[$abilitiesTalentTimeline['talent_id']]);
                                foreach ($playerHeroTalent as $playerHeroTalentInfo) {
                                    if ($playerHeroTalentInfo['left'] == $abilitiesTalentTimeline['talent_id']) {
                                        $currentTalentTimeline['level'] = $playerHeroTalentInfo['id'];
                                        $currentTalentTimeline['left_right'] = "left";
                                    } elseif ($playerHeroTalentInfo['right'] == $abilitiesTalentTimeline['talent_id']) {
                                        $currentTalentTimeline['level'] = $playerHeroTalentInfo['id'];
                                        $currentTalentTimeline['left_right'] = "right";
                                    }
                                }
                                $playerAdvanced['abilities_timeline'][] = $currentTalentTimeline;
                            }
                        }
                    }
                }
            }
            // 道具装备更新时间线
            $itemsTimelineJson = $playerAdvanced['items_timeline'];
            $playerAdvanced['items_timeline'] = [];
            if ($itemsTimelineJson && $itemsTimelineJson!='[]') {
                $itemsTimelines = json_decode($itemsTimelineJson,true);
                foreach ($itemsTimelines as $itemsTimelineInfo) {
                    $itemsTimelinesArray = [];
                    foreach ($itemsTimelineInfo['items_modified'] as $itemId){
                        if(isset($itemArray[$itemId])){
                            $itemsTimelinesArray[] = $itemArray[$itemId];
                        }
                    }
                    $playerAdvanced['items_timeline'][] = [
                        'ingame_timestamp' => $itemsTimelineInfo['ingame_timestamp'],
                        'items_modified' => $itemsTimelinesArray,
                    ];
                }
            }
            // 战队排序
            $teamOrder = $playerAdvanced['team_order'];
            unset($playerAdvanced['team_order']);
            // 高级结构拼接
            $currentPlayerBaseData['advanced'] = $playerAdvanced;
            // 最后结构
            $battlePlayerArray[$teamOrder][] = $currentPlayerBaseData;
        }
        // battle-阵营结果数据
        $battleFactions = [];
        // 第一次特殊事件里面用 拼接阵营和team_id
        $firstFactions = [];
        // 处理factions结构
        foreach ($factions as $faction) {
            // 拼接需要的数据字段
            $facitonInfo['faction'] = $faction['faction'];
            $facitonInfo['team_id'] = $faction['team_id'];
            $facitonInfo['kills'] = $faction['kills'];
            $facitonInfo['deaths'] = $faction['deaths'];
            $facitonInfo['assists'] = $faction['assists'];
            $facitonInfo['net_worth'] = $faction['net_worth'];
            $facitonInfo['net_worth_diff'] = $faction['net_worth_diff'];
            $facitonInfo['experience'] = $faction['experience'];
            $facitonInfo['experience_diff'] = $faction['experience_diff'];
            $facitonInfo['tower_kills'] = $faction['tower_kills'];
            $facitonInfo['barrack_kills'] = $faction['barrack_kills'];
            $facitonInfo['melee_barrack_kills'] = $faction['melee_barrack_kills'];
            $facitonInfo['ranged_barrack_kills'] = $faction['ranged_barrack_kills'];
            $facitonInfo['roshan_kills'] = $faction['roshan_kills'];
            // 战队排序
            $teamOrder = $faction['order'];
            // unset 不需要的数据字段
            $unsetFields = [
                'order', 'faction', 'team_id', 'kills', 'deaths', 'assists', 'net_worth', 'net_worth_diff', 'experience', 'experience_diff', 'tower_kills',
                'barrack_kills', 'melee_barrack_kills', 'ranged_barrack_kills', 'roshan_kills',
            ];
            foreach ($unsetFields as $field){
                unset($faction[$field]);
            }
            // 建筑物状态 - 数据结构
            if(!empty($faction['building_status']) && $faction['building_status']!='[]'){
                $facitonInfo['building_status'] = json_decode($faction['building_status'],true);
            }else{
                $facitonInfo['building_status'] = [
                    'outposts' => [ // 前哨
                        'top_outpost' => null,
                        'bot_outpost' => null,
                    ],
                    'towers'   => [ // 防御塔
                        'top_tier_1_tower' => null,
                        'top_tier_2_tower' => null,
                        'top_tier_3_tower' => null,
                        'top_tier_4_tower' => null,
                        'mid_tier_1_tower' => null,
                        'mid_tier_2_tower' => null,
                        'mid_tier_3_tower' => null,
                        'bot_tier_1_tower' => null,
                        'bot_tier_2_tower' => null,
                        'bot_tier_3_tower' => null,
                        'bot_tier_4_tower' => null,
                    ],
                    'barracks' => [ // 兵营
                        'top_ranged_barrack' => null,
                        'top_melee_barrack'  => null,
                        'mid_ranged_barrack' => null,
                        'mid_melee_barrack'  => null,
                        'bot_ranged_barrack' => null,
                        'bot_melee_barrack'  => null,
                    ],
                    'ancient'  => null, // 遗迹
                ];
            }
            unset($faction['building_status']);
            // 阵营下player数据
            $facitonInfo['players'] = isset($battlePlayerArray[$teamOrder]) ? $battlePlayerArray[$teamOrder]:[];
            // 高级字段
            $facitonInfo['advanced'] = $faction;
            // 第一次特殊事件里面用 拼接阵营和team_id
            $firstFactions[$teamOrder] = [
                'faction' => $facitonInfo['faction'],
                'team_id' => $facitonInfo['team_id'],
            ];
            // 最后数据结构
            $battleFactions[] = $facitonInfo;
        }
        return [
            'battle_factions' => $battleFactions,
            'first_factions' => $firstFactions,
        ];
    }

    // 第一次特殊事件数据
    private static function getFirstEventInfo($firstEventInfo,$teamFactions)
    {
        if(!$firstEventInfo || empty($teamFactions)){
            return [
                'first_blood' => null,
                'first_to_5_kills' => null,
                'first_to_10_kills' => null,
                'first_tower' => null,
                'first_barracks' => null,
                'first_roshan' => null,
            ];
        }
        if($firstEventInfo['first_blood_p_tid']){
            $firstEvent['first_blood']['ingame_timestamp'] = $firstEventInfo['first_blood_time'];
            $firstEvent['first_blood']['faction'] = $teamFactions[$firstEventInfo['first_blood_p_tid']]['faction'];
            $firstEvent['first_blood']['team_id'] = $teamFactions[$firstEventInfo['first_blood_p_tid']]['team_id'];
            $firstEvent['first_blood']['detail']  = json_decode($firstEventInfo['first_blood_details'],true);
        }else{
            $firstEvent['first_blood'] = null;
        }
        if($firstEventInfo['first_to_5_kills_p_tid']){
            $firstEvent['first_to_5_kills']['ingame_timestamp'] = $firstEventInfo['first_to_5_kills_time'];
            $firstEvent['first_to_5_kills']['faction'] = $teamFactions[$firstEventInfo['first_to_5_kills_p_tid']]['faction'];
            $firstEvent['first_to_5_kills']['team_id'] = $teamFactions[$firstEventInfo['first_to_5_kills_p_tid']]['team_id'];
            $firstEvent['first_to_5_kills']['detail']  = json_decode($firstEventInfo['first_to_5_kills_details'],true);
        }else{
            $firstEvent['first_to_5_kills'] = null;
        }
        if($firstEventInfo['first_to_10_kills_p_tid']){
            $firstEvent['first_to_10_kills']['ingame_timestamp'] = $firstEventInfo['first_to_10_kills_time'];
            $firstEvent['first_to_10_kills']['faction'] = $teamFactions[$firstEventInfo['first_to_10_kills_p_tid']]['faction'];
            $firstEvent['first_to_10_kills']['team_id'] = $teamFactions[$firstEventInfo['first_to_10_kills_p_tid']]['team_id'];
            $firstEvent['first_to_10_kills']['detail'] = json_decode($firstEventInfo['first_to_10_kills'],true);
        }else{
            $firstEvent['first_to_10_kills'] = null;
        }
        if($firstEventInfo['first_tower_p_tid']){
            $firstEvent['first_tower']['ingame_timestamp'] = $firstEventInfo['first_tower_time'];
            $firstEvent['first_tower']['faction'] = $teamFactions[$firstEventInfo['first_tower_p_tid']]['faction'];
            $firstEvent['first_tower']['team_id'] = $teamFactions[$firstEventInfo['first_tower_p_tid']]['team_id'];
            $firstEvent['first_tower']['detail']  = json_decode($firstEventInfo['first_tower'],true);
        }else{
            $firstEvent['first_tower'] = null;
        }
        if($firstEventInfo['first_barracks_p_tid']){
            $firstEvent['first_barracks']['ingame_timestamp'] = $firstEventInfo['first_barracks_time'];
            $firstEvent['first_barracks']['faction'] = $teamFactions[$firstEventInfo['first_barracks_p_tid']]['faction'];
            $firstEvent['first_barracks']['team_id'] = $teamFactions[$firstEventInfo['first_barracks_p_tid']]['team_id'];
            $firstEvent['first_barracks']['detail']  = json_decode($firstEventInfo['first_barracks'],true);
        }else{
            $firstEvent['first_barracks'] = null;
        }
        if($firstEventInfo['first_roshan_p_tid']){
            $firstEvent['first_roshan']['ingame_timestamp'] = $firstEventInfo['first_roshan_time'];
            $firstEvent['first_roshan']['faction'] = $teamFactions[$firstEventInfo['first_roshan_p_tid']]['faction'];
            $firstEvent['first_roshan']['team_id'] = $teamFactions[$firstEventInfo['first_roshan_p_tid']]['team_id'];
            $firstEvent['first_roshan']['detail']  = json_decode($firstEventInfo['first_roshan'],true);
        }else{
            $firstEvent['first_roshan'] = null;
        }
        return $firstEvent;
    }

    // 获取英雄bp数据
    private static function getBanPickData($banPickJson,$heroData)
    {
        $banPickData = [];
        if($banPickJson && $banPickJson!='[]') {
            $banPickArray = ConversionCriteria::issetJsonField($banPickJson, ['order', 'type', 'team', 'hero']);
            if(!empty($banPickArray)) {
                $type = [
                    1 => 'Ban',
                    2 => 'Pick'
                ];
                foreach ($banPickArray as $k => $v) {
                    $banPickData[$k]['order'] = isset($v['order']) ? $v['order'] : null;
                    $banPickData[$k]['type'] = isset($type[intval($v['type'])]) ? $type[intval($v['type'])] : null;
                    $banPickData[$k]['team_id'] = isset($v['team']) ? intval($v['team']) : null;
                    $banPickData[$k]['hero'] = isset($v['hero']) ? self::getHeroData($v['hero'],$heroData) : null;
                }
            }
        }
        return $banPickData;
    }

    const DOTA_EVENT_BATTLE_START = 'battle_start';
    const DOTA_EVENT_BATTLE_END = 'battle_end';

    // 获取所有事件数据
    private static function getEvents($battleId,$factions,$heroesArray,$eventMap,$battleEventsTeams,$battleWinner,$itemArray)
    {
        $battleEventArray['factions'] = [];
        $battleEventArray['events_timeline'] = [];
        foreach ($factions as $teamOrder=>$val){
            $info['faction'] = $val['faction'];
            $info['team_id'] = $val['team_id'];
            $info['opponent_order'] = $teamOrder;
            $battleEventArray['factions'][] = $info;
        }
        $battleEvents = MatchBattleEventDota2::find()->alias('event')->select(self::getEventsColumns())
            ->leftJoin('enum_first_events as first_event', 'first_event.id = event.first_event_type')
            // 击杀者游戏内对象类型
            ->leftJoin('enum_ingame_goal as kill_goal', 'kill_goal.id = event.killer_ingame_obj_type')
            // 击杀者游戏内对象子类型
            ->leftJoin('enum_ingame_goal as kill_goal_son', 'kill_goal_son.id = event.killer_sub_type')

            // 受害者游戏内对象类型
            ->leftJoin('enum_ingame_goal as victim_goal', 'victim_goal.id = event.victim_ingame_obj_type')
            // 受害者游戏内对象子类型
            ->leftJoin('enum_ingame_goal as victim_goal_son', 'victim_goal_son.id = event.victim_sub_type')

            // 助攻游戏内对象类型
            ->leftJoin('enum_ingame_goal as assist_goal', 'assist_goal.id = event.assist_ingame_obj_type')
            // 助攻游戏内对象子类型
            ->leftJoin('enum_ingame_goal as assist_goal_son', 'assist_goal_son.id = event.assist_sub_type')
            ->where(["event.battle_id" => $battleId])
            ->orderBy('event.ingame_timestamp')
            ->asArray()->all();

        foreach ($battleEvents as $event){
            $info = [];
            switch ($event['event_type']){
                case self::DOTA_EVENT_BATTLE_START: //对局开始
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['map'] = $eventMap;
                    $info['radiant'] = isset($battleEventsTeams[$event['killer']]) ? $battleEventsTeams[$event['killer']]:null;
                    $info['dire'] = isset($battleEventsTeams[$event['victim']]) ? $battleEventsTeams[$event['victim']]:null;
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case self::DOTA_EVENT_BATTLE_END: //对局结束
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['winner']['team_id'] = $battleWinner['team_id'];
                    $info['winner']['name'] = $battleWinner['team_snapshot']['name'];
                    $info['winner']['image'] = $battleWinner['team_snapshot']['image'];
                    $info['winner']['opponent_order'] = $battleWinner['opponent_order'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'battle_pause': //暂停
                case 'battle_unpause': //解除暂停
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'elite_spawned': //精英怪刷新（肉山）
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['elite'] = [
                        'ingame_obj_name' => $event['k_ingame_obj_name'],
                        'ingame_obj_name_cn' => $event['k_ingame_obj_name_cn'],
                        'ingame_obj_type' => $event['k_ingame_obj_type'],
                        'ingame_obj_sub_type' => $event['k_sub_type'],
                    ];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'player_spawned': //选手复活
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'player_kill': //选手击杀
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['killer_position'] = $event['killer_position'];
                    if ($event['killer']) {
                        $info['killer'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['killer'] = null;
                    }
                    $info['victim_position'] = $event['victim_position'];
                    if ($event['victim']) {
                        $info['victim'] = [
                            'player_id' => $event['victim'],
                            'nick_name' => $event['victim_player_name'] ? $event['victim_player_name'] : $event['victim_player_rel_name'],
                            'steam_id' => $event['victim_steam_id'],
                            'faction' => $event['victim_faction'],
                            'hero' => self::getHeroData($event['victim_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['v_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['v_sub_type'],
                        ];
                    } else {
                        $info['victim'] = null;
                    }
                    $info['is_first_event'] = ConversionCriteria::issetTrueFalseType($event['is_first_event']);
                    $info['first_event_type'] = $event['first_event'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'player_suicide': //选手自杀
                case 'player_denied': //选手反补
                case 'building_kill': //摧毁建筑物
                case 'building_denied': //反补建筑物
                case 'elite_kill': //击杀精英怪
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['killer_position'] = $event['killer_position'];
                    $info['killer'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $info['victim_position'] = $event['victim_position'];
                    $info['victim'] = self::dealEventVictim($event['v_ingame_obj_type'],$event,$heroesArray);
                    $info['is_first_event'] = ConversionCriteria::issetTrueFalseType($event['is_first_event']);
                    $info['first_event_type'] = $event['first_event'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
//                case 'player_denied': //选手反补
//                    break;
//                case 'building_kill': //摧毁建筑物
//                    break;
//                case 'building_denied': //反补建筑物
//                    break;
                case 'building_captured': //占领建筑物
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['capturer_position'] = $event['killer_position'];
                    $info['capturer'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $info['victim_position'] = $event['victim_position'];
                    $info['victim'] = self::dealEventVictim($event['v_ingame_obj_type'],$event,$heroesArray);
                    $info['is_first_event'] = ConversionCriteria::issetTrueFalseType($event['is_first_event']);
                    $info['first_event_type'] = $event['first_event'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
//                case 'elite_kill': //击杀精英怪
//                    break;
                case 'ward_placed': //放置守卫
                case 'ward_expired': //守卫过期
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['ward_type'] = $event['ward_type'];
                    $info['owner'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'ward_kill': //摧毁守卫
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['ward_type'] = $event['ward_type'];
                    $info['killer'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $info['owner'] = self::dealEventVictim($event['v_ingame_obj_type'],$event,$heroesArray);
                    $battleEventArray['events_timeline'][] = $info;
                    break;
//                case 'ward_expired': //守卫过期
//                    break;
                case 'courier_kill': //信使击杀
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['killer'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $info['owner'] = self::dealEventVictim($event['v_ingame_obj_type'],$event,$heroesArray);
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'special_ability_used': //使用特殊技能
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['special_ability_name'] = $event['special_ability_name'];
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'smoke_used': //开雾
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['owner'] = self::dealEventKiller($event['k_ingame_obj_type'],$event,$heroesArray);
                    $info['coverage'] = $event['coverages']&&$event['coverages']!='[]' ? json_decode($event['coverages'],true):[];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'smoke_broke': //破雾
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'rune_spawned': //神符刷新（强化神符）
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['rune_name'] = $event['rune_name'];
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'rune_picked_up': //神符拾取（强化神符）
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['rune_name'] = $event['rune_name'];
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'item_dropped': //道具掉落
                case 'item_denied': //摧毁道具
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $info['item'] = isset($itemArray[$event['item_id']]) ? $itemArray[$event['item_id']]:null;
                    $battleEventArray['events_timeline'][] = $info;
                    break;
                case 'item_picked_up': //道具拾取
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['is_steal'] = ConversionCriteria::issetTrueFalseType($event['is_steal']);
                    if ($event['killer']) {
                        $info['player'] = [
                            'player_id' => $event['killer'],
                            'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                            'steam_id' => $event['killer_steam_id'],
                            'faction' => $event['killer_faction'],
                            'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                            'ingame_obj_type' => $event['k_ingame_obj_type'],
                            'ingame_obj_sub_type' => $event['k_sub_type'],
                        ];
                    } else {
                        $info['player'] = null;
                    }
                    $info['item'] = isset($itemArray[$event['item_id']]) ? $itemArray[$event['item_id']]:null;
                    $battleEventArray['events_timeline'][] = $info;
                    break;
//                case 'item_denied': //摧毁道具
//                    break;
                default :
                    continue;
            }
        }
        return $battleEventArray;
    }

    // 处理event 击杀数据结构
    public static function dealEventKiller($type,$event,$heroesArray)
    {
        $killerEvent = [];
        switch ($type){
            case 'player':
                if ($event['killer']) {
                    $killerEvent = [
                        'player_id' => $event['killer'],
                        'nick_name' => $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'],
                        'steam_id' => $event['killer_steam_id'],
                        'faction' => $event['killer_faction'],
                        'hero' => self::getHeroData($event['killer_champion_id'], $heroesArray),
                        'ingame_obj_type' => $event['k_ingame_obj_type'],
                        'ingame_obj_sub_type' => $event['k_sub_type'],
                    ];
                } else {
                    $killerEvent = null;
                }
                break;
            case 'faction':
                $killerEvent['ingame_obj_name'] = $event['k_ingame_obj_name'];
                $killerEvent['ingame_obj_name_cn'] = $event['k_ingame_obj_name_cn'];
                $killerEvent['faction'] = $event['killer_faction'];
                $killerEvent['ingame_obj_type'] = $event['k_ingame_obj_type'];
                $killerEvent['ingame_obj_sub_type'] = $event['k_sub_type'];
                break;
            case 'neutral_creep':
            case 'elite':
            case 'unknown':
                $killerEvent['ingame_obj_name'] = $event['k_ingame_obj_name'];
                $killerEvent['ingame_obj_name_cn'] = $event['k_ingame_obj_name_cn'];
                $killerEvent['ingame_obj_type'] = $event['k_ingame_obj_type'];
                $killerEvent['ingame_obj_sub_type'] = $event['k_sub_type'];
                break;
            default:
                break;
        }
        if(empty($killerEvent)){
            return null;
        }
        return $killerEvent;
    }

    // 处理event 受害者数据结构
    public static function dealEventVictim($type,$event,$heroesArray)
    {
        $victimEvent = [];
        switch ($type){
            case 'player':
                if ($event['victim']) {
                    $victimEvent = [
                        'player_id' => $event['victim'],
                        'nick_name' => $event['victim_player_name'] ? $event['victim_player_name'] : $event['victim_player_rel_name'],
                        'steam_id' => $event['victim_steam_id'],
                        'faction' => $event['victim_faction'],
                        'hero' => self::getHeroData($event['victim_champion_id'], $heroesArray),
                        'ingame_obj_type' => $event['v_ingame_obj_type'],
                        'ingame_obj_sub_type' => $event['v_sub_type'],
                    ];
                } else {
                    $victimEvent = null;
                }
                break;
            case 'building':
                $victimEvent['lane'] = $event['v_lane'];
                $victimEvent['ingame_obj_name'] = $event['v_ingame_obj_name'];
                $victimEvent['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                $victimEvent['faction'] = $event['victim_faction'];
                $victimEvent['ingame_obj_type'] = $event['v_ingame_obj_type'];
                $victimEvent['ingame_obj_sub_type'] = $event['v_sub_type'];
                break;
            case 'elite':
                $victimEvent['ingame_obj_name'] = $event['v_ingame_obj_name'];
                $victimEvent['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                $victimEvent['ingame_obj_type'] = $event['v_ingame_obj_type'];
                $victimEvent['ingame_obj_sub_type'] = $event['v_sub_type'];
                break;
            default:
                break;
        }
        if(empty($victimEvent)){
            return null;
        }
        return $victimEvent;
    }

    // 获取英雄数据
    private static function getHeroData($heroId,$heroesArray,$isTalents=false)
    {
        if(isset($heroesArray[$heroId]) && $heroesArray[$heroId]){
            if(!$isTalents){
                unset($heroesArray[$heroId]['talents']);
            }
            $heroesArray[$heroId]['image'] = [];
            $heroesArray[$heroId]['image']['image'] = ImageConversionHelper::showFixedSizeConversion($heroesArray[$heroId]['images'], 144, 256) ?? null;
            $heroesArray[$heroId]['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($heroesArray[$heroId]['small_image'], 32, 32) ?? null;
            unset($heroesArray[$heroId]['images']);
            unset($heroesArray[$heroId]['small_image']);
            return $heroesArray[$heroId];
        }else{
            return null;
        }
    }

    // 阵营有关击杀情况字段
    public static function getBattleFactionsExtColumns()
    {
        return [
            'team.order as order',
            // 更新版
            'team_ext.faction as faction',
            'team.team_id as team_id',
            'team_ext.kills as kills',
            'team_ext.deaths as deaths',
            'team_ext.assists as assists',
            'team_ext.net_worth as net_worth',
            'team_ext.net_worth_diff as net_worth_diff',
            'team_ext.experience as experience',
            'team_ext.experience_diff as experience_diff',
            'team_ext.tower_kills as tower_kills',
            'team_ext.barrack_kills as barrack_kills',
            'team_ext.melee_barrack_kills as melee_barrack_kills',
            'team_ext.ranged_barrack_kills as ranged_barrack_kills',
            'team_ext.roshan_kills as roshan_kills',
            // 建筑物状态
            'team_ext.building_status as building_status',
//            'team_ext.building_status_outposts as building_status_outposts',
//            'team_ext.building_status_towers as building_status_towers',
//            'team_ext.building_status_barracks as building_status_barracks',
//            'team_ext.building_status_ancient as building_status_ancient',
            // advanced
            'team_ext.total_runes_pickedup as total_runes_pickedup',
            'team_ext.bounty_runes_pickedup as bounty_runes_pickedup',
            'team_ext.double_damage_runes_pickedup as double_damage_runes_pickedup',
            'team_ext.haste_runes_pickedup as haste_runes_pickedup',
            'team_ext.illusion_runes_pickedup as illusion_runes_pickedup',
            'team_ext.invisibility_runes_pickedup as invisibility_runes_pickedup',
            'team_ext.regeneration_runes_pickedup as regeneration_runes_pickedup',
            'team_ext.arcane_runes_pickedup as arcane_runes_pickedup',
            'team_ext.smoke_purchased as smoke_purchased',
            'team_ext.smoke_used as smoke_used',
            'team_ext.dust_purchased as dust_purchased',
            'team_ext.dust_used as dust_used',
            'team_ext.observer_wards_purchased as observer_wards_purchased',
            'team_ext.observer_wards_placed as observer_wards_placed',
            'team_ext.observer_wards_kills as observer_wards_kills',
            'team_ext.sentry_wards_purchased as sentry_wards_purchased',
            'team_ext.sentry_wards_placed as sentry_wards_placed',
            'team_ext.sentry_wards_kills as sentry_wards_kills',
        ];
    }
    // heroes
    private static function getDotaHeroColumns()
    {
        return [
            'hero.id as hero_id',
            'hero.talents as talents',
            'hero.name as name',
            'hero.name_cn as name_cn',
            'hero.external_id as external_id',
            'hero.external_name as external_name',
            'hero.title as title',
            'hero.title_cn as title_cn',
//            'hero.primary_attr as primary_attr',
//            'hero.primary_attr_cn as primary_attr_cn',
//            'hero.roles as roles',
//            'hero.roles_cn as roles_cn',
            'hero.slug as slug',
            'hero.image as image',
            'hero.image as images',
            'hero.small_image as small_image',
//            'hero.modified_at as modified_at',
//            'hero.created_at as created_at',
//            'hero.deleted_at as deleted_at',
        ];
    }

    // battle下player字段 新版
    public static function getDotaPlayerColumns()
    {
        return [
            'player.team_order as team_order',
            // 基本字段
            'player.seed as seed',
            'player.faction as faction',
            'position.c_name as role',
            'lane.c_name as lane',
            'player.player_id as player_id',
            'player.nick_name as nick_name',
            'player.rel_nick_name as rel_nick_name',
            'player.steam_id as steam_id',
            'player.hero as hero',
            'player.level as level',
            'player.is_alive as is_alive',
            'player.kills as kills',
            'player.deaths as deaths',
            'player.assists as assists',
            'player.kda as kda',
            'player.participation as participation',
            'player.last_hits as last_hits',
            'player.lhpm as lhpm',
            'player.denies as denies',
            'player.items as items',
            // advanced
            'player.talents as talents',
            'player.ultimate_cd as ultimate_cd',
            'player.coordinate as coordinate',
            'player.is_visible as is_visible',
            'player.respawntimer as respawntimer',
            'player.buyback_cooldown as buyback_cooldown',
            'player.buyback_cost as buyback_cost',
            'player.health as health',
            'player.health_max as health_max',
            'player.tower_kills as tower_kills',
            'player.barrack_kills as barrack_kills',
            'player.melee_barrack_kills as melee_barrack_kills',
            'player.ranged_barrack_kills as ranged_barrack_kills',
            'player.roshan_kills as roshan_kills',
            'player.double_kill as double_kill',
            'player.triple_kill as triple_kill',
            'player.ultra_kill as ultra_kill',
            'player.rampage as rampage',
            'player.largest_multi_kill as largest_multi_kill',
            'player.largest_killing_spree as largest_killing_spree',
            'player.lane_creep_kills as lane_creep_kills',
            'player.neutral_creep_kills as neutral_creep_kills',
            'player.net_worth as net_worth',
            'player.net_worth_percent as net_worth_percent',
            'player.gold_earned as gold_earned',
            'player.gold_spent as gold_spent',
            'player.gold_remaining as gold_remaining',
            'player.gold_reliable as gold_reliable',
            'player.gold_unreliable as gold_unreliable',
            'player.gold_herokill as gold_herokill',
            'player.gold_creepkill as gold_creepkill',
            'player.gpm as gpm',
            'player.experience as experience',
            'player.xpm as xpm',
            'player.damage_to_heroes as damage_to_heroes',
            'player.damage_to_heroes_hp_removal as damage_to_heroes_hp_removal',
            'player.damage_to_heroes_magical as damage_to_heroes_magical',
            'player.damage_to_heroes_physical as damage_to_heroes_physical',
            'player.damage_to_heroes_pure as damage_to_heroes_pure',
            'player.dpm_to_heroes as dpm_to_heroes',
            'player.damage_percent_to_heroes as damage_percent_to_heroes',
            'player.damage_taken as damage_taken',
            'player.damage_taken_hp_removal as damage_taken_hp_removal',
            'player.damage_taken_magical as damage_taken_magical',
            'player.damage_taken_physical as damage_taken_physical',
            'player.damage_taken_pure as damage_taken_pure',
            'player.dtpm as dtpm',
            'player.damage_taken_percent as damage_taken_percent',
            'player.damage_conversion_rate as damage_conversion_rate',
            'player.damage_to_buildings as damage_to_buildings',
            'player.damage_to_towers as damage_to_towers',
            'player.damage_to_objectives as damage_to_objectives',
            'player.total_crowd_control_time as total_crowd_control_time',
            'player.total_heal as total_heal',
            'player.total_runes_pickedup as total_runes_pickedup',
            'player.bounty_runes_pickedup as bounty_runes_pickedup',
            'player.double_damage_runes_pickedup as double_damage_runes_pickedup',
            'player.haste_runes_pickedup as haste_runes_pickedup',
            'player.illusion_runes_pickedup as illusion_runes_pickedup',
            'player.invisibility_runes_pickedup as invisibility_runes_pickedup',
            'player.regeneration_runes_pickedup as regeneration_runes_pickedup',
            'player.arcane_runes_pickedup as arcane_runes_pickedup',
            'player.smoke_purchased as smoke_purchased',
            'player.smoke_used as smoke_used',
            'player.dust_purchased as dust_purchased',
            'player.dust_used as dust_used',
            'player.observer_wards_purchased as observer_wards_purchased',
            'player.observer_wards_placed as observer_wards_placed',
            'player.observer_wards_kills as observer_wards_kills',
            'player.sentry_wards_purchased as sentry_wards_purchased',
            'player.sentry_wards_placed as sentry_wards_placed',
            'player.sentry_wards_kills as sentry_wards_kills',
            'player.abilities_timeline as abilities_timeline',
            'player.items_timeline as items_timeline',
        ];
    }
    // 道具
    private static function getBattleItemColumns()
    {
        return [
            'item.id as item_id',
            'item.name as name',
            'item.name_cn as name_cn',
            'item.external_id as external_id',
            'item.external_name as external_name',
            'item.total_cost as total_cost',
            'item.is_recipe as is_recipe',
            'item.is_secret_shop as is_secret_shop',
            'item.is_home_shop as is_home_shop',
            'item.is_neutral_drop as is_neutral_drop',
            'item.slug as slug',
            'item.image as image',
        ];
    }
    // 技能ability
    private static function getAbilityColumns()
    {
        return [
            'ability.id as ability_id',
            'ability.name as name',
            'ability.name_cn as name_cn',
            'ability.hotkey as hotkey',
            'ability.external_id as external_id',
            'ability.external_name as external_name',
            'ability.slug as slug',
            'ability.image as image',
        ];
    }
    // 天赋talent
    private static function getTalentColumns()
    {
        return [
            'talent.id as talent_id',
            'talent.external_id as external_id',
            'talent.external_name as external_name',
            'talent.level as level',
            'talent.left_right as left_right',
            'talent.talent as talent',
            'talent.talent_cn as talent_cn',
            'talent.slug as slug',
//            'talent.image as image',
        ];
    }
    // events
    private static function getEventsColumns()
    {
        return [
            'event.event_id as event_id',
            'event.ingame_timestamp as ingame_timestamp',
            'event.event_type as event_type',
            'event.position as position',

            'event.killer as killer',
            'event.killer_steam_id as killer_steam_id',
            'event.killer_position as killer_position',
            'event.killer_player_name as killer_player_name',
            'event.killer_player_rel_name as killer_player_rel_name',
            'event.killer_champion_id as killer_champion_id',
            'event.killer_faction as killer_faction',
            // 击杀者
            'kill_goal.ingame_obj_type as k_ingame_obj_type',
            'kill_goal.lane as k_lane',
            'kill_goal.ingame_obj_name as k_ingame_obj_name',
            'kill_goal.ingame_obj_name_cn as k_ingame_obj_name_cn',
            'kill_goal_son.sub_type as k_sub_type',
            'event.killer_champion_id as killer_champion_id',

            'event.victim as victim',
            'event.victim_steam_id as victim_steam_id',
            'event.victim_position as victim_position',
            'event.victim_player_name as victim_player_name',
            'event.victim_player_rel_name as victim_player_rel_name',
            'event.victim_champion_id as victim_champion_id',
            'event.victim_faction as victim_faction',
            // 受害者
            'victim_goal.ingame_obj_type as v_ingame_obj_type',
            'victim_goal.lane as v_lane',
            'victim_goal.ingame_obj_name as v_ingame_obj_name',
            'victim_goal.ingame_obj_name_cn as v_ingame_obj_name_cn',
            'victim_goal_son.sub_type as v_sub_type',
            'event.victim_champion_id as victim_champion_id',

            'event.assist as assist',
            'event.assist_ingame_obj_type as assist_ingame_obj_type',
            'event.assist_sub_type as assist_sub_type',
            // 助攻者
            'assist_goal.ingame_obj_name as a_ingame_obj_name',
            'assist_goal.ingame_obj_name_cn as a_ingame_obj_name_cn',
            'assist_goal.ingame_obj_type as a_ingame_obj_type',
            'assist_goal_son.sub_type as a_sub_type',

            'event.is_first_event as is_first_event',
            'first_event.first_event as first_event',
            'event.item_id as item_id',
            'event.rune_name as rune_name',
            'event.special_ability_name as special_ability_name',
            'event.ward_type as ward_type',
            'event.coverages as coverages',
            'event.is_steal as is_steal',
        ];
    }
}
