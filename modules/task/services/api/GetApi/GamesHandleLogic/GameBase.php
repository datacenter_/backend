<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2021/01/22
 * Time: 22:05
 */

namespace app\modules\task\services\api\GetApi\GamesHandleLogic;

class GameBase
{
    // lol
    // 英雄数据
    public static $championsArray = [];
    // 召唤师技能数据
    public static $summonersArray = [];
    // 物品数据
    public static $itemsArray = [];
    // 符文数据
    public static $runesArray = [];
    // 游戏内目标匹配数据
    public static $ingameObjNameArray = [];

    // csgo
    // 武器数据
    public static $weaponsArray = [];

    // dota
    // 英雄数据
    public static $heroesArray = [];
    // 物品数据
    public static $itemsDotaArray = [];
}