<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/12/05
 * Time: 19:05
 */

namespace app\modules\task\services\api\GetApi\GamesHandleLogic;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchBattlePlayerLol;
use app\modules\match\models\MatchBattleTeam;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;

class LolHandleLogic
{
    const LOL_EVENT_PLAYER = 'player';
    const LOL_EVENT_BUILDING = 'building';
    const LOL_EVENT_MINION = 'minion';
    const LOL_EVENT_ELITE = 'elite';
    const LOL_EVENT_UNKNOW = 'unknown';
    const LOL_EVENT_NEUTRAL_MINION = 'neutral_minion';
//    const LOL_EVENT_PLAYER_KILL = 'player_kill';
//    const LOL_EVENT_PLAYER_SUICIDE = 'player_suicide';
//    const LOL_EVENT_BUILDING_KILL = 'building_kill';
//    const LOL_EVENT_ELITE_KILL = 'elite_kill';

    // 获取lol对局详情数据
    public static function getLolBattleDetail($battleWinner,$battleId,$teamIdData,$duration,$battleStatus,$eventMap,$battleEventsTeams)
    {
        // 获取 1-是否为官方完整数据 3-是否已结束 （字段）
        $extLolInfo = MatchBattleExtLol::find()->where(['id' => $battleId])->asArray()->one();
        // 获取 2-对局时长
        $detailData['is_confirmed'] = ConversionCriteria::issetTrueFalseType($extLolInfo['is_confirmed']);
        $detailData['duration'] = $duration;
        $detailData['winner'] = $battleWinner;
        $detailData['is_pause'] = ConversionCriteria::issetTrueFalseType($extLolInfo['is_pause']);
        $detailData['is_live'] = ConversionCriteria::issetTrueFalseType($extLolInfo['is_live']);
        // 精英怪状态
        $detailData['elites_status'] = $extLolInfo['elites_status']&&$extLolInfo['elites_status']!='[]' ?
            json_decode($extLolInfo['elites_status'],true):[
                'rift_herald_status' => null,
                'dragon_status' => null,
                'baron_nashor_status' => null,
            ];
        // 英雄所有数据
        if (!empty(GameBase::$championsArray)) {
            $heroDataList = GameBase::$championsArray;
        } else {
            $heroData = MetadataLolChampion::find()->alias('champion')
                ->select(self::getLolChampionColumns())
                ->where(['deleted' => 2])
                ->asArray()->all();
            foreach ($heroData as $k => $hero) {
                $championInfo['champion_id'] = $hero['champion_id'];
                $championInfo['name'] = $hero['name'];
                $championInfo['name_cn'] = $hero['name_cn'];
                $championInfo['external_id'] = $hero['external_id'];
                $championInfo['external_name'] = $hero['external_name'];
                $championInfo['title'] = $hero['title'];
                $championInfo['title_cn'] = $hero['title_cn'];
                $championInfo['slug'] = $hero['slug'];
                $championInfo['image']['image'] = ImageConversionHelper::showFixedSizeConversion($hero['image'], 120, 120);
                $championInfo['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($hero['small_image'], 32, 32);
                GameBase::$championsArray[$hero['champion_id']] = $championInfo;
            }
            $heroDataList = GameBase::$championsArray;
        }
        // 英雄bp
        $detailData['ban_pick'] = empty($extLolInfo['ban_pick']) ?
            [] : self::getBanPickData($extLolInfo['ban_pick'],$heroDataList);
        // 所有游戏内目标匹配数据
        if (!empty(GameBase::$ingameObjNameArray)) {
            $ingameObjNameList = GameBase::$ingameObjNameArray;
        } else {
            $ingameObjNameData = EnumIngameGoal::find()->select(['id', 'ingame_obj_type', 'sub_type', 'ingame_obj_name', 'ingame_obj_name_cn'])->where(['game_id' => 2])->asArray()->all();
            foreach ($ingameObjNameData as $k => $ingame) {
                GameBase::$ingameObjNameArray[$ingame['id']] = $ingame;
            }
            $ingameObjNameList = GameBase::$ingameObjNameArray;
        }
        // 阵营数组两种结构
        $factionsArray = self::getFactionData($battleId,$heroDataList,$ingameObjNameList,$battleStatus);
        // 阵营数据结构
        $detailData['factions'] = $factionsArray['factions'];
        // 战队阵营数据结构
        $teamsData = $factionsArray['team_factions'];
        // 第一次特殊事件
        $detailData['first_events'] = self::getFirstEventData($extLolInfo,$teamsData,$teamIdData);
        // 事件时间线
        $detailData['timeline'] = self::getTimeLineData($extLolInfo['gold_diff_timeline'],$extLolInfo['experience_diff_timeline']);
        // events展示 - factions
        $detailData['team_factions'] = $teamsData;
        // events展示 - 事件时间线
        $detailData['events_timeline'] = self::getEventsTimeLineData($battleId,$heroDataList,$battleWinner,$eventMap,$battleEventsTeams,$ingameObjNameList);
        return $detailData;
    }

    // 获取阵营数据
    private static function getFactionData($battleId,$heroData,$ingameObjNameList,$battleStatus)
    {
        // 所有召唤师技能数据
        if (!empty(GameBase::$summonersArray)) {
            $summonerDataList = GameBase::$summonersArray;
        } else {
            $summonerData = MetadataLolSummonerSpell::find()->alias('spell')->select(self::getSummonerSpellColumns())
                ->where(['deleted' => 2])->asArray()->all();
            foreach ($summonerData as $k => $summoner) {
                GameBase::$summonersArray[$summoner['summoner_spell_id']] = $summoner;
            }
            $summonerDataList = GameBase::$summonersArray;
        }
        // 所有物品数据
        if (!empty(GameBase::$itemsArray)) {
            $itemDataList = GameBase::$itemsArray;
        } else {
            $itemData = MetadataLolItem::find()->alias('mli')->select(self::getItemColumns())
                ->where(['deleted' => 2])->asArray()->all();
            foreach ($itemData as $k => $item) {
                GameBase::$itemsArray[$item['item_id']] = $item;
            }
            $itemDataList = GameBase::$itemsArray;
        }
        // 所有符文数据
        if (!empty(GameBase::$runesArray)) {
            $runeDataList = GameBase::$runesArray;
        } else {
            $runeData = MetadataLolRune::find()->alias('rune')->select(self::getRuneColumns())->where(['deleted'=>2])->asArray()->all();
            foreach ($runeData as $k => &$rune) {
                $rune['image'] = ImageConversionHelper::showFixedSizeConversion($rune['image'], 50, 50);
                GameBase::$runesArray[$rune['rune_id']] = $rune;
            }
            $runeDataList = GameBase::$runesArray;
        }
        // teams阵营数据
        $battleFactions = MatchBattleTeam::find()->alias('team')->select(self::getBattleTeamExtColumns())
            ->leftJoin('match_battle_team_ext_lol as team_ext', 'team.id = team_ext.id')
            ->where(['team.battle_id' => $battleId])
            ->orderBy('team_ext.faction asc')
            ->asArray()->all();
        // battle-players数据
        $battlePlayers = MatchBattlePlayerLol::find()->alias('player')
            ->select(self::getLolPlayerColumns())
            ->leftJoin('enum_position as position', 'position.id = player.role')
            ->leftJoin('enum_lane as lane', 'lane.id = player.lane')
            ->where(['player.battle_id'=>$battleId])
            ->orderBy('player.seed')
            ->asArray()->all();

        // 取出所有的 - 技能升级时间线字段
        $abilitiesTimelineJsonArray = array_filter(array_column($battlePlayers,'abilities_timeline'), function($val) {
            if ($val && $val!='[]') {
                return $val;
            }
        });
        $abilitiesIds = [];
        foreach ($abilitiesTimelineJsonArray as &$infoJson) {
            $info = [];
            $info = json_decode($infoJson,true);
            $abilitiesId = array_filter(array_column($info,'ability'));
            if(!empty($abilitiesIds)){
                $abilitiesIds = array_unique(array_merge($abilitiesIds,$abilitiesId));
            }else{
                $abilitiesIds = $abilitiesId;
            }
        }
        // 所有技能数据
        $abilityList = [];
        if(!empty($abilitiesIds)) {
            $abilityData = MetadataLolAbility::find()->alias('ability')
                ->select(self::getAbilityColumns())->where(['in', 'ability.id', $abilitiesIds])
                ->asArray()->all();
            foreach ($abilityData as &$ability){
                $ability['image'] = ImageConversionHelper::showFixedSizeConversion($ability['image'], 50, 50);
                $abilityList[$ability['ability_id']] = $ability;
            }
        }

        // 处理完成之后的 battle-players数据
        $battlePlayersArray = self::getPlayerData($battlePlayers,$heroData,$itemDataList,$summonerDataList,$runeDataList,$abilityList,$battleStatus);
        // 战队阵营
        $teamFactions = [];
        // 阵营数据结构
        $factions = [];
        foreach ($battleFactions as $k => &$factionInfo) {
            // 战队阵营
            $faction = [];
            $faction['faction'] = $factionInfo['faction'];
            $faction['team_id'] = $factionInfo['team_id'];
            $faction['opponent_order'] = $factionInfo['order'];
            $teamFactions[] = $faction;
            // 阵营数据结构
            $dragonKillsDetail = $factionInfo['dragon_kills_detail'] ? json_decode($factionInfo['dragon_kills_detail'], true) : [];
            $dragonDetail = [];
            foreach ($dragonKillsDetail as $kk => $val) {
                $dragonDetail[$kk]['ingame_timestamp'] = intval($val['date']);
                $dragonDetail[$kk]['dragon_name'] = null;
                if(isset($ingameObjNameList[$val['drake']])){
                    $dragonDetail[$kk]['dragon_name'] = $ingameObjNameList[$val['drake']]['ingame_obj_name'];
                    $dragonDetail[$kk]['dragon_name_cn'] = $ingameObjNameList[$val['drake']]['ingame_obj_name_cn'];
                }
            }
            // 击杀元素巨龙详情
            $factionInfo['dragon_kills_detail'] = $dragonDetail;
            // 建筑物状态
            $factionInfo['building_status'] = $factionInfo['building_status']&&$factionInfo['building_status']!='[]' ?
                json_decode($factionInfo['building_status'],true):[
                    'turrets' => [
                        'top_outer_turret' => null,
                        'top_inner_turret' => null,
                        'top_inhibitor_turret' => null,
                        'mid_outer_turret' => null,
                        'mid_inner_turret' => null,
                        'mid_inhibitor_turret' => null,
                        'bot_outer_turret' => null,
                        'bot_inner_turret' => null,
                        'bot_inhibitor_turret' => null,
                        'top_nexus_turret' => null,
                        'bot_nexus_turret' => null,
                    ],
                    'inhibitors' => [
                        'top_inhibitor' => null,
                        'mid_inhibitor' => null,
                        'bot_inhibitor' => null,
                    ],
                    'nexus' => null
                ];
            // faction - 选手
            $factionInfo['players'] = isset($battlePlayersArray[$factionInfo['order']]) ? $battlePlayersArray[$factionInfo['order']]:[];
            // faction - 高级（暂时没数据）
            $factionInfo['advanced'] = [
                'wards_purchased' => $factionInfo['wards_purchased'],
                'wards_placed' => $factionInfo['wards_placed'],
                'wards_kills' => $factionInfo['wards_kills'],
            ];
            unset($factionInfo['wards_purchased']);
            unset($factionInfo['wards_placed']);
            unset($factionInfo['wards_kills']);
            unset($factionInfo['order']);
            $factions[] = $factionInfo;
        }
        return [
            'factions' => $factions,
            'team_factions' => $teamFactions,
        ];
    }

    // 获取阵营下players
    private static function getPlayerData($players,$heroData,$itemData,$summonerData,$runesData,$abilityData,$battleStatus)
    {
        $playerArray = [];
        foreach ($players as $k=>&$playerData) {
            if($playerData['team_order']) {
                // 基础 - 结构处理
                $playerInfo['seed'] = $playerData['seed'];
                $playerInfo['faction'] = $playerData['faction'];
                $playerInfo['role'] = $playerData['role'];
                $playerInfo['lane'] = $playerData['lane'];
                $playerInfo['player']['player_id'] = $playerData['player_id'];
                if ($playerData['nick_name']) {
                    $playerName = $playerData['nick_name'] ?? null;
                } else {
                    $playerName = $playerData['rel_nick_name'] ?? null;
                }
                $playerInfo['player']['nick_name'] = $playerName;
                // 英雄
                $playerInfo['champion'] = self::getChampionData($playerData['champion'], $heroData);
                $playerInfo['level'] = $playerData['level'];
                $playerInfo['is_alive'] = ConversionCriteria::issetTrueFalseType($playerData['is_alive']);
                $playerInfo['kills'] = $playerData['kills'];
                $playerInfo['deaths'] = $playerData['deaths'];
                $playerInfo['assists'] = $playerData['assists'];
                $playerInfo['kda'] = $playerData['kda'];
                $playerInfo['participation'] = $playerData['participation'];
                $playerInfo['cs'] = $playerData['cs'];
                $playerInfo['cspm'] = $playerData['cspm'];
                // 召唤师技能
                $playerInfo['summoner_spells'] = self::getSummonerSpellsData($playerData['summoner_spells'], $summonerData);
                // 道具装备
                $playerInfo['items'] = self::getItemsData($playerData['items'], $itemData);
                // 高级 - 结构处理 - advanced
                $advanced = [];
                if (isset($runesData[$playerData['keystone']])) {
                    $advanced['keystone'] = $runesData[$playerData['keystone']];
                }else{
                    $advanced['keystone'] = null;
                }
                $advanced['ultimate_cd'] = is_null($playerData['ultimate_cd']) ? null:intval($playerData['ultimate_cd']);
                $advanced['coordinate'] = $playerData['coordinate'];
                $advanced['respawntimer'] = is_null($playerData['respawntimer']) ? null:intval($playerData['respawntimer']);
                $advanced['health'] = $playerData['health'];
                $advanced['health_max'] = $playerData['health_max'];
                $advanced['turret_kills'] = $playerData['turret_kills'];
                $advanced['inhibitor_kills'] = $playerData['inhibitor_kills'];
                $advanced['rift_herald_kills'] = $playerData['rift_herald_kills'];
                $advanced['dragon_kills'] = $playerData['dragon_kills'];
                $advanced['baron_nashor_kills'] = $playerData['baron_nashor_kills'];
                $advanced['double_kill'] = $playerData['double_kill'];
                $advanced['triple_kill'] = $playerData['triple_kill'];
                $advanced['quadra_kill'] = $playerData['quadra_kill'];
                $advanced['penta_kill'] = $playerData['penta_kill'];
                $advanced['largest_multi_kill'] = $playerData['largest_multi_kill'];
                $advanced['largest_killing_spree'] = $playerData['largest_killing_spree'];
                $advanced['minion_kills'] = $playerData['minion_kills'];
                $advanced['total_neutral_minion_kills'] = $playerData['total_neutral_minion_kills'];
                $advanced['neutral_minion_team_jungle_kills'] = $playerData['neutral_minion_team_jungle_kills'];
                $advanced['neutral_minion_enemy_jungle_kills'] = $playerData['neutral_minion_enemy_jungle_kills'];
                $advanced['gold_earned'] = $playerData['gold_earned'];
                $advanced['gold_spent'] = $playerData['gold_spent'];
                $advanced['gold_remaining'] = $playerData['gold_remaining'];
                $advanced['gpm'] = $playerData['gpm'];
                $advanced['gold_earned_percent'] = $playerData['gold_earned_percent'];
                $advanced['experience'] = $playerData['experience'];
                $advanced['xpm'] = $playerData['xpm'];
                $advanced['damage_to_champions'] = $playerData['damage_to_champions'];
                $advanced['damage_to_champions_physical'] = $playerData['damage_to_champions_physical'];
                $advanced['damage_to_champions_magic'] = $playerData['damage_to_champions_magic'];
                $advanced['damage_to_champions_true'] = $playerData['damage_to_champions_true'];
                $advanced['dpm_to_champions'] = $playerData['dpm_to_champions'];
                $advanced['damage_percent_to_champions'] = $playerData['damage_percent_to_champions'];
                $advanced['total_damage'] = $playerData['total_damage'];
                $advanced['total_damage_physical'] = $playerData['total_damage_physical'];
                $advanced['total_damage_magic'] = $playerData['total_damage_magic'];
                $advanced['total_damage_true'] = $playerData['total_damage_true'];
                $advanced['damage_taken'] = $playerData['damage_taken'];
                $advanced['damage_taken_physical'] = $playerData['damage_taken_physical'];
                $advanced['damage_taken_magic'] = $playerData['damage_taken_magic'];
                $advanced['damage_taken_true'] = $playerData['damage_taken_true'];
                $advanced['dtpm'] = $playerData['dtpm'];
                $advanced['damage_taken_percent'] = $playerData['damage_taken_percent'];
                $advanced['damage_conversion_rate'] = $playerData['damage_conversion_rate'];
                $advanced['damage_selfmitigated'] = $playerData['damage_selfmitigated'];
                $advanced['damage_shielded_on_teammates'] = $playerData['damage_shielded_on_teammates'];
                $advanced['damage_to_buildings'] = $playerData['damage_to_buildings'];
                $advanced['damage_to_towers'] = $playerData['damage_to_towers'];
                $advanced['damage_to_objectives'] = $playerData['damage_to_objectives'];
                $advanced['total_crowd_control_time'] = $playerData['total_crowd_control_time'];
                $advanced['total_crowd_control_time_others'] = $playerData['total_crowd_control_time_others'];
                $advanced['total_heal'] = $playerData['total_heal'];
                $advanced['wards_purchased'] = $playerData['wards_purchased'];
                $advanced['wards_placed'] = $playerData['wards_placed'];
                $advanced['wards_kills'] = $playerData['wards_kills'];
                $advanced['vision_score'] = $playerData['vision_score'];
                // 符文
                $runes = [
                    'primary_path' => [
                        'path' => [
                            'path_name' => null,
                            'path_name_cn' => null,
                        ],
                        'keystone' => null,
                        'rune_1' => null,
                        'rune_2' => null,
                        'rune_3' => null,
                    ],
                    'secondary_path' => [
                        'path' => [
                            'path_name' => null,
                            'path_name_cn' => null,
                        ],
                        'rune_1' => null,
                        'rune_2' => null,
                    ]
                ];
                if($playerData['runes'] && $playerData['runes']!='[]'){
                    $runesIds = json_decode($playerData['runes'],true);
                    foreach ($runesIds as $runeKey=>&$rid){
                        if($runeKey == 0){
                            if (isset($runesData[$rid])) {
                                $runes['primary_path']['path']['path_name'] = $runesData[$rid]['path_name'];
                                $runes['primary_path']['path']['path_name_cn'] = $runesData[$rid]['path_name_cn'];
                                $runes['primary_path']['keystone'] = $runesData[$rid];
                            }
                            continue;
                        }
                        if (in_array($runeKey,[1,2,3])) {
                            $runes['primary_path']["rune_{$runeKey}"] = $runesData[$rid];
                        }
                        if (in_array($runeKey,[4,5])) {
                            $runeKeys = [4 => 1, 5 => 2];
                            if (isset($runesData[$rid])) {
                                $runes['secondary_path']['path']['path_name'] = $runesData[$rid]['path_name'];
                                $runes['secondary_path']['path']['path_name_cn'] = $runesData[$rid]['path_name_cn'];
                                $runes['secondary_path']["rune_{$runeKeys[$runeKey]}"] = $runesData[$rid];
                            }
                        }
                    }
                }
                $advanced['runes'] = $runes;
                // 技能升级时间线
                $advanced['abilities_timeline'] = [];
                if($playerData['abilities_timeline'] && $playerData['abilities_timeline']!='[]'){
                    $abilities_timeline = json_decode($playerData['abilities_timeline'],true);
                    foreach ($abilities_timeline as $abilitiesKey=>$abilities) {
                        if(!empty($abilities['ability']) && isset($abilityData[$abilities['ability']])){
                            $abilities['ability'] = $abilityData[$abilities['ability']];
                        }else{
                            $abilities['ability'] = null;
                        }
                        $advanced['abilities_timeline'][] = $abilities;
                    }
                }
                // 道具装备更新时间线
//                if ($battleStatus == 'completed') {
                    $items_timeline_data = [];
                    if (!empty($playerData['items_timeline']) && $playerData['items_timeline'] != '[]') {
                        $items_timeline = json_decode($playerData['items_timeline'], true);
                        foreach ($items_timeline as $key => $val) {
                            $value['ingame_timestamp'] = $val['ingame_timestamp'];
                            $items = $val['items'];
                            $itemsData = [];
                            foreach ($items as $k => &$v) {
                                if ($v) {
                                    $itemsData[] = self::getItemData($v, $itemData);
                                }
                            }
                            $value['items_modified'] = $itemsData;
                            $items_timeline_data[] = $value;
                        }
                    }
                    $advanced['items_timeline'] = $items_timeline_data;
//                } else {
//                    $advanced['items_timeline'] = [];
//                }

                $playerInfo['advanced'] = $advanced;

                $playerArray[$playerData['team_order']][] = $playerInfo;
            }
        }
        return $playerArray;
    }

    // 获取游戏数据
    private static function getItemData($itemId,$itemData)
    {
        if(isset($itemData[$itemId]) && !empty($itemData)){
            $itemData[$itemId]['is_trinket'] = ConversionCriteria::issetTrueFalseType($itemData[$itemId]['is_trinket']);
            $itemData[$itemId]['is_purchasable'] = ConversionCriteria::issetTrueFalseType($itemData[$itemId]['is_purchasable']);
            $itemData[$itemId]['total_cost'] = $itemData[$itemId]['total_cost']=='' ? null:intval($itemData[$itemId]['total_cost']);
            $itemData[$itemId]['image'] = ImageConversionHelper::showFixedSizeConversion($itemData[$itemId]['image'],50,50);
            return $itemData[$itemId];
        }else{
            return null;
        }
    }
    // players下符文
    public static function getItemsData($itemsJson,$itemData)
    {
        $itemsData = json_decode($itemsJson,true);
        $items = [];
        foreach ($itemsData as $k=>$itemArr){
            if(is_array($itemArr)){
                $itemId = $itemArr['item_id'];
            }else{
                $itemId = $itemArr;
            }
            if($itemId){
                $itemInfo = self::getItemData($itemId,$itemData);
                if(!empty($itemInfo)){
                    $itemInfo['purchase_time'] = $itemArr['purchase_time'];
                    $itemInfo['cooldown'] = $itemArr['cooldown'];
                }
            }else{
                $itemInfo = null;
            }
            $items["slot_".intval($k+1)] = $itemInfo;
        }
        return $items;
    }
    // players下召唤师技能数据
    public static function getSummonerSpellsData($summonerSpellsIds,$summonerData)
    {
        if(!empty($summonerSpellsIds)){
            $summonerSpellsIdArray = json_decode($summonerSpellsIds,true);
            $summonerArray = [];
            foreach ($summonerSpellsIdArray as $k=>$sumArr){
                if(is_array($sumArr)) {
                    $suId = $sumArr['spell_id'];
                }else{
                    $suId = $sumArr;
                }
                if(isset($summonerData[$suId])){
                    $summonerInfo = $summonerData[$suId];
                    $summonerInfo['image'] = ImageConversionHelper::showFixedSizeConversion($summonerInfo['image'],50,50);
                    $summonerInfo['cooldown'] = isset($sumArr['cooldown']) ? $sumArr['cooldown']:null;
                    $summonerArray[] = $summonerInfo;
                }
            }
        }else{
            return [];
        }
        return $summonerArray;
    }

    // 获取英雄bp数据
    private static function getBanPickData($banPick,$heroData)
    {
        if($banPick) {
            $banPickCo = ConversionCriteria::issetJsonField($banPick, ['hero', 'team', 'type', 'order']);
            $banPickData = [];
            if(!empty($banPickCo)) {
                $type = array(
                    1 => 'Ban',
                    2 => 'Pick',
                    0 => ''
                );
                foreach ($banPickCo as $k => $v) {
                    $banPickData[$k]['order'] = isset($v['order']) ? $v['order'] : null;
                    $banPickData[$k]['type'] = isset($type[intval($v['type'])]) ? $type[intval($v['type'])] : null;
                    $banPickData[$k]['team_id'] = isset($v['team']) ? intval($v['team']) : null;
                    $banPickData[$k]['champion'] = isset($v['hero']) ? self::getChampionData($v['hero'],$heroData) : null;
                }
            }
        }else{
            $banPickData = [];
        }
        return $banPickData;
    }

    // 获取英雄数据
    private static function getChampionData($heroId,$heroDataList)
    {
        if(isset($heroDataList[$heroId]) && $heroDataList[$heroId]){
            return $heroDataList[$heroId];
        }else{
            return null;
        }
    }

    // 第一次特殊事件数据
    private static function getFirstEventData($firstData,$teamsFaction,$teamIdData)
    {
        if(empty($firstData)){
            return [
                'first_blood' => null,
                'first_to_5_kills' => null,
                'first_to_10_kills' => null,
                'first_turret' => null,
                'first_inhibitor' => null,
                'first_rift_herald' => null,
                'first_dragon' => null,
                'first_baron_nashor' => null,
                'first_elder_dragon' => null,
            ];
        }
        $teamsData = [];
        foreach ($teamsFaction as $faction){
            $teamsData[$faction['opponent_order']] = $faction['faction'];
        }
        $first_blood_p_tid = $firstData['first_blood_p_tid'] ? ($firstData['first_blood_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_blood_p_tid) {
            $firstEvents['first_blood']['ingame_timestamp'] = $firstData['first_blood_time'];
            $firstEvents['first_blood']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_blood_p_tid']);
            $firstEvents['first_blood']['team_id'] = $first_blood_p_tid;
            $firstEvents['first_blood']['detail'] = $firstData['first_blood_detail']&&$firstData['first_blood_detail']!='[]' ?
                json_decode($firstData['first_blood_detail'],true):null;
        }else{
            $firstEvents['first_blood'] = null;
        }
        $first_to_5_kills_p_tid = $firstData['first_to_5_kills_p_tid'] ? ($firstData['first_to_5_kills_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_to_5_kills_p_tid) {
            $firstEvents['first_to_5_kills']['ingame_timestamp'] = $firstData['first_to_5_kills_time'];
            $firstEvents['first_to_5_kills']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_to_5_kills_p_tid']);
            $firstEvents['first_to_5_kills']['team_id'] = $first_to_5_kills_p_tid;
            $firstEvents['first_to_5_kills']['detail'] = $firstData['first_to_5_detail']&&$firstData['first_to_5_detail']!='[]' ?
                json_decode($firstData['first_to_5_detail'],true):null;
        }else{
            $firstEvents['first_to_5_kills'] = null;
        }
        $first_to_10_kills_p_tid = $firstData['first_to_10_kills_p_tid'] ? ($firstData['first_to_10_kills_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_to_10_kills_p_tid) {
            $firstEvents['first_to_10_kills']['ingame_timestamp'] = $firstData['first_to_10_kills_time'];
            $firstEvents['first_to_10_kills']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_to_10_kills_p_tid']);
            $firstEvents['first_to_10_kills']['team_id'] = $first_to_10_kills_p_tid;
            $firstEvents['first_to_10_kills']['detail'] = $firstData['first_to_10_detail']&&$firstData['first_to_10_detail']!='[]' ?
                json_decode($firstData['first_to_10_detail'],true):null;
        }else{
            $firstEvents['first_to_10_kills'] = null;
        }
        $first_turret_p_tid = $firstData['first_turret_p_tid'] ? ($firstData['first_turret_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_turret_p_tid) {
            $firstEvents['first_turret']['ingame_timestamp'] = $firstData['first_turret_time'];
            $firstEvents['first_turret']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_turret_p_tid']);
            $firstEvents['first_turret']['team_id'] = $first_turret_p_tid;
            $firstEvents['first_turret']['detail'] = $firstData['first_turret_detail']&&$firstData['first_turret_detail']!='[]' ?
                json_decode($firstData['first_turret_detail'],true):null;
        }else{
            $firstEvents['first_turret'] = null;
        }
        $first_inhibitor_p_tid = $firstData['first_inhibitor_p_tid'] ? ($firstData['first_inhibitor_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_inhibitor_p_tid) {
            $firstEvents['first_inhibitor']['ingame_timestamp'] = $firstData['first_inhibitor_time'];
            $firstEvents['first_inhibitor']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_inhibitor_p_tid']);
            $firstEvents['first_inhibitor']['team_id'] = $first_inhibitor_p_tid;
            $firstEvents['first_inhibitor']['detail'] = $firstData['first_inhibitor_detail']&&$firstData['first_inhibitor_detail']!='[]' ?
                json_decode($firstData['first_inhibitor_detail'],true):null;
        }else{
            $firstEvents['first_inhibitor'] = null;
        }
        $first_rift_herald_p_tid = $firstData['first_rift_herald_p_tid'] ? ($firstData['first_rift_herald_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_rift_herald_p_tid) {
            $firstEvents['first_rift_herald']['ingame_timestamp'] = $firstData['first_rift_herald_time'];
            $firstEvents['first_rift_herald']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_rift_herald_p_tid']);
            $firstEvents['first_rift_herald']['team_id'] = $first_rift_herald_p_tid;
            $firstEvents['first_rift_herald']['detail'] = $firstData['first_rift_herald_detail']&&$firstData['first_rift_herald_detail']!='[]' ?
                json_decode($firstData['first_rift_herald_detail'],true):null;
        }else{
            $firstEvents['first_rift_herald'] = null;
        }
        $first_dragon_p_tid = $firstData['first_dragon_p_tid'] ? ($firstData['first_dragon_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_dragon_p_tid) {
            $firstEvents['first_dragon']['ingame_timestamp'] = $firstData['first_dragon_time'];
            $firstEvents['first_dragon']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_dragon_p_tid']);
            $firstEvents['first_dragon']['team_id'] = $first_dragon_p_tid;
            $firstEvents['first_dragon']['detail'] = $firstData['first_dragon_detail']&&$firstData['first_dragon_detail']!='[]' ?
                json_decode($firstData['first_dragon_detail'],true):null;
        }else{
            $firstEvents['first_dragon'] = null;
        }
        $first_baron_nashor_p_tid = $firstData['first_baron_nashor_p_tid'] ? ($firstData['first_baron_nashor_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_baron_nashor_p_tid) {
            $firstEvents['first_baron_nashor']['ingame_timestamp'] = $firstData['first_baron_nashor_time'];
            $firstEvents['first_baron_nashor']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_baron_nashor_p_tid']);
            $firstEvents['first_baron_nashor']['team_id'] = $first_baron_nashor_p_tid;
            $firstEvents['first_baron_nashor']['detail'] = $firstData['first_baron_nashor_detail']&&$firstData['first_baron_nashor_detail']!='[]' ?
                json_decode($firstData['first_baron_nashor_detail'],true):null;
        }else{
            $firstEvents['first_baron_nashor'] = null;
        }
        $first_elder_dragon_p_tid = $firstData['first_elder_dragon_p_tid'] ? ($firstData['first_elder_dragon_p_tid']==1 ? $teamIdData['team_1_id']:$teamIdData['team_2_id']) : null;
        if($first_elder_dragon_p_tid) {
            $firstEvents['first_elder_dragon']['ingame_timestamp'] = $firstData['first_elder_dragon_time'];
            $firstEvents['first_elder_dragon']['faction'] = self::getFirstEventFaction($teamsData, $firstData['first_elder_dragon_p_tid']);
            $firstEvents['first_elder_dragon']['team_id'] = $first_elder_dragon_p_tid;
            $firstEvents['first_elder_dragon']['detail'] = $firstData['first_elder_dragon_detail']&&$firstData['first_elder_dragon_detail']!='[]' ?
                json_decode($firstData['first_elder_dragon_detail'],true):null;
        }else{
            $firstEvents['first_elder_dragon'] = null;
        }
        return $firstEvents;
    }
    // 第一次特殊事件数据
    private static function getFirstEventFaction($teamsData,$factionTid)
    {
        if(isset($teamsData[$factionTid])){
            $faction = $teamsData[$factionTid];
        }else{
            $faction = null;
        }
        return $faction;
    }
    const LOL_EVENT_BATTLE_START = 'battle_start';
    const LOL_EVENT_BATTLE_END = 'battle_end';
    const LOL_EVENT_BATTLE_PAUSE = 'battle_pause'; // 暂停
    const LOL_EVENT_BATTLE_UNPAUSE = 'battle_unpause'; // 解除暂停
    const LOL_EVENT_BATTLE_RELOADED = 'battle_reloaded'; // 回档
    const LOL_EVENT_ELITE_ANNOUNCED = 'elite_announced'; // 精英怪公告
    const LOL_EVENT_ELITE_SPAWNED = 'elite_spawned'; // 精英怪刷新
    const LOL_EVENT_PLAYER_SPAWNED = 'player_spawned'; // 选手复活
    const LOL_EVENT_PLAYER_KILL = 'player_kill'; // 选手击杀
    const LOL_EVENT_PLAYER_SUICIDE = 'player_suicide'; // 选手自杀
    const LOL_EVENT_BUILDING_KILL = 'building_kill'; // 摧毁建筑物
    const LOL_EVENT_ELITE_KILL = 'elite_kill'; // 击杀精英怪
    const LOL_EVENT_WARD_PLACED = 'ward_placed'; // 放置守卫
    const LOL_EVENT_WARD_KILL = 'ward_kill'; // 摧毁守卫
    const LOL_EVENT_WARD_EXPIRED = 'ward_expired'; // 守卫过期
    // 事件时间线  events_timeline
    public static function getEventsTimeLineData($battleId,$championDataArray,$battleWinner,$eventMap,$battleEventsTeams,$ingameObjNameList)
    {
        $where = [
            "event_lol.battle_id" => $battleId
        ];
        // 事件时间线  events_timeline
        $battleEvents = MatchBattleEventLol::find()->alias('event_lol')->select(self::getLolEventsColumns())
            ->leftJoin('enum_first_events as events', 'events.id = event_lol.first_event_type')
            // 击杀者游戏内对象类型
            ->leftJoin('enum_ingame_goal as kill_goal', 'kill_goal.id = event_lol.killer_ingame_obj_type')
            // 击杀者游戏内对象子类型
            ->leftJoin('enum_ingame_goal as kill_goal_son', 'kill_goal_son.id = event_lol.killer_sub_type')
            ->leftJoin('metadata_lol_champion as kill_champion', 'kill_champion.id = event_lol.killer_champion_id')
            // 受害者游戏内对象类型
            ->leftJoin('enum_ingame_goal as victim_goal', 'victim_goal.id = event_lol.victim_ingame_obj_type')
            // 受害者游戏内对象子类型
            ->leftJoin('enum_ingame_goal as victim_goal_son', 'victim_goal_son.id = event_lol.victim_sub_type')
            ->leftJoin('metadata_lol_champion as victim_champion', 'victim_champion.id = event_lol.victim_champion_id')
            // 助攻游戏内对象类型
            ->leftJoin('enum_ingame_goal as assist_goal', 'assist_goal.id = event_lol.assist_ingame_obj_type')
            // 助攻游戏内对象子类型
            ->leftJoin('enum_ingame_goal as assist_goal_son', 'assist_goal_son.id = event_lol.assist_sub_type')
            ->where($where)->orderBy('event_lol.ingame_timestamp')
            ->asArray()->all();
        // 新版事件数据结构
        $battleEventArray = [];
        foreach ($battleEvents as $k =>&$event){
            $info = [];
            switch ($event['event_type']){
                case self::LOL_EVENT_BATTLE_START:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['map'] = $eventMap;
                    $info['blue'] = isset($battleEventsTeams[$event['killer']]) ? $battleEventsTeams[$event['killer']]:null;
                    $info['red'] = isset($battleEventsTeams[$event['victim']]) ? $battleEventsTeams[$event['victim']]:null;
                    $battleEventArray[] = $info;
                    break;
                case self::LOL_EVENT_BATTLE_END:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['winner']['team_id'] = $battleWinner['team_id'];
                    $info['winner']['name'] = $battleWinner['team_snapshot']['name'];
                    $info['winner']['image'] = $battleWinner['team_snapshot']['image'];
                    $info['winner']['opponent_order'] = $battleWinner['opponent_order'];
                    $battleEventArray[] = $info;
                    break;
                case self::LOL_EVENT_BATTLE_PAUSE:
                case self::LOL_EVENT_BATTLE_UNPAUSE:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $battleEventArray[] = $info;
                    break;
                case self::LOL_EVENT_BATTLE_RELOADED:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['restore_to'] = $event['restore_to'];
                    $battleEventArray[] = $info;
                    break;
                // 精英怪公告
                case self::LOL_EVENT_ELITE_ANNOUNCED:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    if(isset($ingameObjNameList[$event['killer']])){
                        $info['elite']['ingame_obj_name'] = $ingameObjNameList[$event['killer']]['ingame_obj_name'];
                        $info['elite']['ingame_obj_name_cn'] = $ingameObjNameList[$event['killer']]['ingame_obj_name_cn'];
                        $info['elite']['ingame_obj_type'] = $ingameObjNameList[$event['killer']]['ingame_obj_type'];
                        $info['elite']['ingame_obj_sub_type'] = $ingameObjNameList[$event['killer']]['sub_type'];
                    }else{
                        $info['elite'] = null;
                    }
                    $info['spawn_time'] = $event['spawn_time'];
                    $battleEventArray[] = $info;
                    break;
                // 精英怪刷新
                case self::LOL_EVENT_ELITE_SPAWNED:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    if(isset($ingameObjNameList[$event['killer']])){
                        $info['elite']['ingame_obj_name'] = $ingameObjNameList[$event['killer']]['ingame_obj_name'];
                        $info['elite']['ingame_obj_name_cn'] = $ingameObjNameList[$event['killer']]['ingame_obj_name_cn'];
                        $info['elite']['ingame_obj_type'] = $ingameObjNameList[$event['killer']]['ingame_obj_type'];
                        $info['elite']['ingame_obj_sub_type'] = $ingameObjNameList[$event['killer']]['sub_type'];
                    }else{
                        $info['elite'] = null;
                    }
                    $battleEventArray[] = $info;
                    break;
                // 选手复活
                case self::LOL_EVENT_PLAYER_SPAWNED:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    if ($event['killer']) {
                        $info['player']['player_id'] = $event['killer'];
                        $info['player']['nick_name'] = $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'];
                        $info['player']['faction'] = $event['killer_faction'];
                        $info['player']['champion'] = self::dealEventChampion($event, 'k_');
                        $info['player']['ingame_obj_type'] = $event['k_ingame_obj_type'];
                        $info['player']['ingame_obj_sub_type'] = $event['k_sub_type'];
                    } else {
                        $info['player'] = null;
                    }
                    $battleEventArray[] = $info;
                    break;
                case self::LOL_EVENT_PLAYER_KILL:
                case self::LOL_EVENT_PLAYER_SUICIDE:
                case self::LOL_EVENT_BUILDING_KILL:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['killer'] = self::dealKillerEvent($event['k_ingame_obj_type'],$event);
                    $info['victim'] = self::dealVictimEvent($event['v_ingame_obj_type'],$event);
                    $info['assist'] = self::dealEventAssist($event['assist'],$event['a_ingame_obj_type'],$event['a_sub_type'],$championDataArray);
                    $info['is_first_event'] = ConversionCriteria::issetTrueFalseType($event['is_first_event']);
                    $info['first_event_type'] = $event['first_event'];
                    $battleEventArray[] = $info;
                    break;
                case self::LOL_EVENT_ELITE_KILL:
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    if($event['killer']) {
                        $info['killer']['player_id'] = $event['killer'];
                        $info['killer']['nick_name'] = $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'];
                        $info['killer']['faction'] = $event['killer_faction'];
                        $info['killer']['champion'] = self::dealEventChampion($event, 'k_');
                        $info['killer']['ingame_obj_type'] = $event['k_ingame_obj_type'];
                        $info['killer']['ingame_obj_sub_type'] = $event['k_sub_type'];
                    }else{
                        $info['killer'] = null;
                    }
                    $info['victim']['ingame_obj_name'] = $event['v_ingame_obj_name'];
                    $info['victim']['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                    $info['victim']['ingame_obj_type'] = $event['v_ingame_obj_type'];
                    $info['victim']['ingame_obj_sub_type'] = $event['v_sub_type'];
                    $info['assist'] = self::dealEventAssist($event['assist'],$event['a_ingame_obj_type'],$event['a_sub_type'],$championDataArray);
                    $info['is_first_event'] = ConversionCriteria::issetTrueFalseType($event['is_first_event']);
                    $info['first_event_type'] = $event['first_event'];
                    $battleEventArray[] = $info;
                    break;
                // 放置守卫
                case self::LOL_EVENT_WARD_PLACED:
                    $otherInfo = $event['other_info']&&$event['other_info']!='[]' ? json_decode($event['other_info'],true):[];
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['ward_type'] = isset($otherInfo['ward_type']) ? $otherInfo['ward_type']:null;
                    $info['owner'] = isset($otherInfo['owner']) ? $otherInfo['owner']:null;
                    $battleEventArray[] = $info;
                    break;
                // 摧毁守卫
                case self::LOL_EVENT_WARD_KILL:
                    $otherInfo = $event['other_info']&&$event['other_info']!='[]' ? json_decode($event['other_info'],true):[];
                    $info['ingame_timestamp'] = $event['ingame_timestamp'];
                    $info['event_id'] = $event['event_id'];
                    $info['event_type'] = $event['event_type'];
                    $info['position'] = $event['position'];
                    $info['ward_type'] = isset($otherInfo['ward_type']) ? $otherInfo['ward_type']:null;
                    $info['killer'] = isset($otherInfo['killer']) ? $otherInfo['killer']:null;
                    $info['owner'] = isset($otherInfo['owner']) ? $otherInfo['owner']:null;
                    $battleEventArray[] = $info;
                    break;
//                // 守卫过期 - 没数据
//                case self::LOL_EVENT_WARD_EXPIRED:
//                    break;
                default:
                    break;
            }
        }

        return $battleEventArray;
    }
    // 事件时间线 timeline
    public static function getTimeLineData($goldDiffJson,$experienceDiffJson)
    {
        $goldDiff = $experDiff = [];
        $ingame_timestamp = [];
        if($goldDiffJson && $goldDiffJson!='[]'){
            $goldDiffData = json_decode($goldDiffJson,true);
            foreach ($goldDiffData as $key=>$goldval){
                if($key == 0){
                    $ingame_timestamp['ingame_timestamp'] = $goldval['ingame_timestamp'];
                    $goldDiff[] = $goldval;
                }else{
                    if($goldval['ingame_timestamp']-$ingame_timestamp['ingame_timestamp'] >= 30){
                        $ingame_timestamp['ingame_timestamp'] = $goldval['ingame_timestamp'];
                        $goldDiff[] = $goldval;
                    }else{
                        continue;
                    }
                }
            }
        }
        $exper_timestamp = [];
        if($experienceDiffJson && $experienceDiffJson!='[]'){
            $experDiffData = json_decode($experienceDiffJson,true);
            foreach ($experDiffData as $key=>$experval){
                if($key == 0){
                    $exper_timestamp['ingame_timestamp'] = $experval['ingame_timestamp'];
                    $experDiff[] = $experval;
                }else{
                    if($experval['ingame_timestamp']-$exper_timestamp['ingame_timestamp'] >= 30){
                        $exper_timestamp['ingame_timestamp'] = $experval['ingame_timestamp'];
                        $experDiff[] = $experval;
                    }else{
                        continue;
                    }
                }
            }
        }
        $timeline['gold_diff_timeline'] = $goldDiff;
        $timeline['experience_diff_timeline'] = $experDiff;
        return $timeline;
    }
    // 处理LOLevent 助攻数据
    public static function dealEventAssist($assistJson,$ingame_obj_type,$ingame_sub_type,$championDataArray) {
        $eventAssistData = [];
        if(!empty($assistJson) && $assistJson!='[]'){
            $assistData = json_decode($assistJson,true);
//            if($assistData && $assistData!='[]') {
            foreach ($assistData as $k=>$v){
                $eventAssist['player_id'] = $v['player_id'];
                if(isset($v['player_nick_name']) && $v['player_nick_name']){
                    $eventAssist['nick_name'] = $v['player_nick_name'];
                }else{
                    if(isset($v['assist_player_name'])){
                        $eventAssist['nick_name'] = $v['assist_player_name'];
                    }else{
                        $eventAssist['nick_name'] = null;
                    }
                }
                $eventAssist['faction'] = isset($v['faction']) ? $v['faction']:null;
                $eventAssist['champion'] = isset($championDataArray[$v['champion_id']]) ? $championDataArray[$v['champion_id']] : null;
                $eventAssist['ingame_obj_type'] = $ingame_obj_type;
                $eventAssist['ingame_obj_sub_type'] = $ingame_sub_type;
                $eventAssistData[] = $eventAssist;
            }
//            }
        }
        return $eventAssistData;
    }
    // 处理LOLevent英雄
    public static function dealEventChampion($event,$str) {
        if($event[$str.'champion_id']) {
            $champion['champion_id'] = $event[$str . 'champion_id'];
            $champion['name'] = $event[$str . 'name'];
            $champion['name_cn'] = $event[$str . 'name_cn'];
            $champion['external_id'] = $event[$str . 'external_id'];
            $champion['external_name'] = $event[$str . 'external_name'];
            $champion['title'] = $event[$str . 'title'];
            $champion['title_cn'] = $event[$str . 'title_cn'];
            $champion['slug'] = $event[$str . 'slug'];
            $champion['image']['image'] = ImageConversionHelper::showFixedSizeConversion($event[$str . 'images'],120,120);
            $champion['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($event[$str . 'small_image'],32,32);
        }else{
            $champion = null;
        }
        return $champion;
    }
    // 处理Kill - event数据结构
    public static function dealKillerEvent($ingameObjType,$event) {
        $ingameType = [];
        switch ($ingameObjType){
            case self::LOL_EVENT_PLAYER:
                $ingameType['player_id'] = $event['killer'];
                $ingameType['nick_name'] = $event['killer_player_name'] ? $event['killer_player_name'] : $event['killer_player_rel_name'];
                $ingameType['faction'] = $event['killer_faction'];
                $ingameType['champion'] = self::dealEventChampion($event,'k_');
                break;
            case self::LOL_EVENT_BUILDING:
                $ingameType['lane'] = $event['k_lane'];
                $ingameType['ingame_obj_name'] = $event['k_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['k_ingame_obj_name_cn'];
                $ingameType['faction'] = $event['killer_faction'];
                break;
            case self::LOL_EVENT_MINION:
                $ingameType['ingame_obj_name'] = $event['k_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['k_ingame_obj_name_cn'];
                $ingameType['faction'] = $event['killer_faction'];
                break;
            case self::LOL_EVENT_ELITE:
            case self::LOL_EVENT_UNKNOW:
            case self::LOL_EVENT_NEUTRAL_MINION:
                $ingameType['ingame_obj_name'] = $event['k_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['k_ingame_obj_name_cn'];
                break;
            default:
                return null;
        }
        $ingameType['ingame_obj_type'] = $event['k_ingame_obj_type'];
        $ingameType['ingame_obj_sub_type'] = $event['k_sub_type'];
        return $ingameType;
    }
    // 处理victim - event数据结构
    public static function dealVictimEvent($ingameObjType,$event) {
        $ingameType = [];
        switch ($ingameObjType){
            case self::LOL_EVENT_PLAYER:
                $ingameType['player_id'] = $event['victim'];
                $ingameType['nick_name'] = $event['victim_player_name'] ? $event['victim_player_name'] : $event['victim_player_rel_name'];
                $ingameType['faction'] = $event['victim_faction'];
                $ingameType['champion'] = self::dealEventChampion($event,'v_');
                break;
            case self::LOL_EVENT_BUILDING:
                $ingameType['lane'] = $event['v_lane'];
                $ingameType['ingame_obj_name'] = $event['v_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                $ingameType['faction'] = $event['victim_faction'];
                break;
            case self::LOL_EVENT_MINION:
                $ingameType['ingame_obj_name'] = $event['v_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                $ingameType['faction'] = $event['victim_faction'];
                break;
            case self::LOL_EVENT_ELITE:
            case self::LOL_EVENT_UNKNOW:
            case self::LOL_EVENT_NEUTRAL_MINION:
                $ingameType['ingame_obj_name'] = $event['v_ingame_obj_name'];
                $ingameType['ingame_obj_name_cn'] = $event['v_ingame_obj_name_cn'];
                break;
            default:
                return null;
        }
        $ingameType['ingame_obj_type'] = $event['v_ingame_obj_type'];
        $ingameType['ingame_obj_sub_type'] = $event['v_sub_type'];
        return $ingameType;
    }
    private static function getLolEventsColumns()
    {
        return [
//            'event_lol.id as event_id',
            'event_lol.event_id as event_id',
            'event_lol.ingame_timestamp as ingame_timestamp',
            'event_lol.event_type as event_type',
            'event_lol.position as position',
            'event_lol.restore_to as restore_to',
            'event_lol.spawn_time as spawn_time',

            'event_lol.killer as killer',
            'event_lol.killer_player_name as killer_player_name',
            'event_lol.killer_player_rel_name as killer_player_rel_name',
            'event_lol.killer_champion_id as killer_champion_id',
            'event_lol.killer_faction as killer_faction',
            // 击杀者
            'kill_goal.ingame_obj_type as k_ingame_obj_type',
            'kill_goal.lane as k_lane',
            'kill_goal.ingame_obj_name as k_ingame_obj_name',
            'kill_goal.ingame_obj_name_cn as k_ingame_obj_name_cn',
            'kill_goal_son.sub_type as k_sub_type',
            // 击杀者英雄
            'kill_champion.id as k_champion_id',
            'kill_champion.name as k_name',
            'kill_champion.name_cn as k_name_cn',
            'kill_champion.external_id as k_external_id',
            'kill_champion.external_name as k_external_name',
            'kill_champion.title as k_title',
            'kill_champion.title_cn as k_title_cn',
            'kill_champion.slug as k_slug',
            'kill_champion.image as k_images',
            'kill_champion.small_image as k_small_image',

            'event_lol.victim as victim',
            'event_lol.victim_player_name as victim_player_name',
            'event_lol.victim_player_rel_name as victim_player_rel_name',
            'event_lol.victim_champion_id as victim_champion_id',
            'event_lol.victim_faction as victim_faction',
            // 受害者
            'victim_goal.ingame_obj_type as v_ingame_obj_type',
            'victim_goal.lane as v_lane',
            'victim_goal.ingame_obj_name as v_ingame_obj_name',
            'victim_goal.ingame_obj_name_cn as v_ingame_obj_name_cn',
            'victim_goal_son.sub_type as v_sub_type',
            // 死亡者英雄
            'victim_champion.id as v_champion_id',
            'victim_champion.name as v_name',
            'victim_champion.name_cn as v_name_cn',
            'victim_champion.external_id as v_external_id',
            'victim_champion.external_name as v_external_name',
            'victim_champion.title as v_title',
            'victim_champion.title_cn as v_title_cn',
            'victim_champion.slug as v_slug',
            'victim_champion.image as v_images',
            'victim_champion.small_image as v_small_image',

            'event_lol.assist as assist',
            'event_lol.assist_ingame_obj_type as assist_ingame_obj_type',
            'event_lol.assist_sub_type as assist_sub_type',
            // 助攻者
            'assist_goal.ingame_obj_name as a_ingame_obj_name',
            'assist_goal.ingame_obj_name_cn as a_ingame_obj_name_cn',
            'assist_goal.ingame_obj_type as a_ingame_obj_type',
            'assist_goal_son.sub_type as a_sub_type',

            'event_lol.is_first_event as is_first_event',
            'event_lol.other_info as other_info',
            'events.first_event'
        ];
    }

    // 召唤师技能字段
    private static function getSummonerSpellColumns()
    {
        return [
            'spell.id as summoner_spell_id',
            'spell.name as name',
            'spell.name_cn as name_cn',
//            'spell.state as status',
            'spell.external_id as external_id',
            'spell.external_name as external_name',
            'spell.slug as slug',
            'spell.image as image',
//            'spell.description as description',
//            'spell.description_cn as description_cn',
//            'spell.modified_at as modified_at',
//            'spell.created_at as created_at',
//            'spell.deleted_at as deleted_at',
        ];
    }
    // item
    private static function getItemColumns()
    {
        return [
            'mli.id as item_id',
            'mli.name as name',
            'mli.name_cn as name_cn',
            'mli.external_id as external_id',
            'mli.external_name as external_name',
            'mli.total_cost as total_cost',
            'mli.is_trinket as is_trinket',
            'mli.is_purchasable as is_purchasable',
            'mli.slug as slug',
            'mli.image as image',
//            'mli.description as description',
//            'mli.description_cn as description_cn',
//            'mli.modified_at as modified_at',
//            'mli.created_at as created_at',
//            'mli.deleted_at as deleted_at',
        ];
    }
    // metadata_lol_rune
    private static function getRuneColumns()
    {
        return [
            'rune.id as rune_id',
            'rune.name as name',
            'rune.name_cn as name_cn',
            'rune.external_id as external_id',
            'rune.external_name as external_name',
            'rune.path_name as path_name',
            'rune.path_name_cn as path_name_cn',
            'rune.slug as slug',
            'rune.image as image',
        ];
    }
    // metadata_lol_ability
    private static function getAbilityColumns()
    {
        return [
            'ability.id as ability_id',
            'ability.name as name',
            'ability.name_cn as name_cn',
            'ability.hotkey as hotkey',
            'ability.external_id as external_id',
            'ability.external_name as external_name',
            'ability.slug as slug',
            'ability.image as image',
        ];
    }

    // 阵营有关击杀情况字段
    public static function getBattleTeamExtColumns()
    {
        return [
            'team.order as order',
            'team_ext.faction as faction',
            'team.team_id as team_id',
            'team_ext.kills as kills',
            'team_ext.deaths as deaths',
            'team_ext.assists as assists',
            'team_ext.gold as gold',
            'team_ext.gold_diff as gold_diff',
            'team_ext.experience as experience',
            'team_ext.experience_diff as experience_diff',
            'team_ext.turret_kills as turret_kills',
            'team_ext.inhibitor_kills as inhibitor_kills',
            'team_ext.rift_herald_kills as rift_herald_kills',
            'team_ext.dragon_kills as dragon_kills',

            'team_ext.dragon_kills_detail as dragon_kills_detail',

            'team_ext.baron_nashor_kills as baron_nashor_kills',

            'team_ext.building_status as building_status',
            // advanced
            'team_ext.wards_purchased as wards_purchased',
            'team_ext.wards_placed as wards_placed',
            'team_ext.wards_kills as wards_kills',
        ];
    }

    // battle下player字段
    public static function getLolPlayerColumns()
    {
        return [
            'player.team_order as team_order',
            // 基础版本字段
            'player.seed as seed',
            'player.faction as faction',
            'position.c_name as role',
            'lane.c_name as lane',
            'player.player_id as player_id',
            'player.nick_name as nick_name',
            'player.rel_nick_name as rel_nick_name',
            'player.champion as champion',
            'player.level as level',
            'player.alive as is_alive',
            'player.kills as kills',
            'player.deaths as deaths',
            'player.assists as assists',
            'player.kda as kda',
            'player.participation as participation',
            'player.cs as cs',
            'player.cspm as cspm',
            'player.summoner_spells as summoner_spells',
            'player.items as items',
            // 高级版本字段
            'player.keystone as keystone',
            'player.ultimate_cd as ultimate_cd',
            'player.coordinate as coordinate',
            'player.respawntimer as respawntimer',
            'player.health as health',
            'player.health_max as health_max',
            // new 新加字段
            'player.turret_kills as turret_kills',
            'player.inhibitor_kills as inhibitor_kills',
            'player.rift_herald_kills as rift_herald_kills',
            'player.dragon_kills as dragon_kills',
            'player.baron_nashor_kills as baron_nashor_kills',
            'player.double_kill as double_kill',
            'player.triple_kill as triple_kill',
            'player.quadra_kill as quadra_kill',
            'player.penta_kill as penta_kill',
            'player.largest_multi_kill as largest_multi_kill',
            'player.largest_killing_spree as largest_killing_spree',
            'player.minion_kills as minion_kills',
            'player.total_neutral_minion_kills as total_neutral_minion_kills',
            'player.neutral_minion_team_jungle_kills as neutral_minion_team_jungle_kills',
            'player.neutral_minion_enemy_jungle_kills as neutral_minion_enemy_jungle_kills',
            'player.gold_earned as gold_earned',
            'player.gold_spent as gold_spent',
            'player.gold_remaining as gold_remaining',
            'player.gpm as gpm',
            'player.gold_earned_percent as gold_earned_percent',
            'player.experience as experience',
            'player.xpm as xpm',
            'player.damage_to_champions as damage_to_champions',
            'player.damage_to_champions_physical as damage_to_champions_physical',
            'player.damage_to_champions_magic as damage_to_champions_magic',
            'player.damage_to_champions_true as damage_to_champions_true',
            'player.dpm_to_champions as dpm_to_champions',
            'player.damage_percent_to_champions as damage_percent_to_champions',
            'player.total_damage as total_damage',
            'player.total_damage_physical as total_damage_physical',
            'player.total_damage_magic as total_damage_magic',
            'player.total_damage_true as total_damage_true',
            'player.damage_taken as damage_taken',
            'player.damage_taken_physical as damage_taken_physical',
            'player.damage_taken_magic as damage_taken_magic',
            'player.damage_taken_true as damage_taken_true',
            'player.dtpm as dtpm',
            'player.damage_taken_percent as damage_taken_percent',
            'player.damage_conversion_rate as damage_conversion_rate',
            'player.damage_selfmitigated as damage_selfmitigated',
            'player.damage_shielded_on_teammates as damage_shielded_on_teammates',
            'player.damage_to_buildings as damage_to_buildings',
            'player.damage_to_towers as damage_to_towers',
            'player.damage_to_objectives as damage_to_objectives',
            'player.total_crowd_control_time as total_crowd_control_time',
            'player.total_crowd_control_time_others as total_crowd_control_time_others',
            'player.total_heal as total_heal',
            'player.wards_purchased as wards_purchased',
            'player.wards_placed as wards_placed',
            'player.wards_kills as wards_kills',
            'player.vision_score as vision_score',
            'player.runes as runes',
            'player.abilities_timeline as abilities_timeline',
            'player.items_timeline as items_timeline',
        ];
    }

    // 英雄字段
    public static function getLolChampionColumns()
    {
        return [
            'champion.id as champion_id',
            'champion.name as name',
            'champion.name_cn as name_cn',
            'champion.external_id as external_id',
            'champion.external_name as external_name',
            'champion.title as title',
            'champion.title_cn as title_cn',
            'champion.slug as slug',
            'champion.image as image',
            'champion.small_image as small_image',
        ];
    }
}
