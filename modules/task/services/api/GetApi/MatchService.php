<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\models\EnumGame;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchStreamList;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataDota2Map;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\org\models\TeamSnapshot;
use yii\data\Pagination;

class MatchService
{
    const GAME_STATUS_CSGO = 1;
    const GAME_STATUS_LOL = 2;
    const GAME_STATUS_DOTA2 = 3;
    const PBP_DATA_URL = "ws://39.105.105.57:9997";
    public static function getMatchList($params)
    {
        $q = self::getQuery();
        // 赛事筛选
        if(isset($params['tournament_id']) && !empty($params['tournament_id'])){
            $q->andWhere(['m.tournament_id'=>$params['tournament_id']]);
        }
        // 游戏筛选
        if(isset($params['game_id']) && !empty($params['game_id'])){
            $q->andWhere(['m.game'=>$params['game_id']]);
        }
        // 状态筛选
        if(isset($params['status']) && !empty($params['status'])){
            print("现在正在状态为：{$params['status']}，match数据...")."\n";
            $q->andWhere(['match_r.status'=>$params['status']]);
        }
        // 多个比赛id筛选
        if(isset($params['match_ids']) && is_array($params['match_ids'])){
            $q->andWhere(['in', 'm.id', $params['match_ids']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 100 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getColumns())
            ->offset($pages->offset)->limit($pages->limit)
            ->orderBy('m.scheduled_begin_at desc')
            ->asArray()->all();
        // 获取战队
//        $team_1_id = array_column($list,'team_1_id');
//        $team_2_id = array_column($list,'team_2_id');
//        $team_1_id_Info = self::getTeam($team_1_id);
//        $team_2_id_Info = self::getTeam($team_2_id);
        return [
            'count'=>$count,
            'list'=>self::getFormat($list),
        ];
    }

    /**
     * @param $list
     * @param $team_1_id_Info
     * @param $team_2_id_Info
     * @param bool $isDetail
     * @return array
     */
    private static function getFormat($list)
    {
        $info = [];
        foreach ($list as &$value)
        {
            $tmp['deleted'] = $value['deleted'];
            $tmp['delay'] = $value['mr_data_speed'];
            $tmp['match_id'] = $value['match_id'];
            // 游戏
            if($value['eg_game_id']) {
                $tmp['game']['game_id'] = $value['eg_game_id'];
                $tmp['game']['name'] = $value['eg_name'];
                $tmp['game']['name_cn'] = $value['eg_name_cn'];
                $tmp['game']['default_game_rules'] = $value['eg_default_game_rules'];
                $tmp['game']['default_match_type'] = $value['eg_default_match_type'];
                $tmp['game']['full_name'] = $value['eg_full_name'];
                $tmp['game']['short_name'] = $value['eg_short_name'];
                $tmp['game']['slug'] = $value['eg_slug'];
                $tmp['game']['image'] = $value['eg_image'];
                $tmp['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_modified_at']);
                $tmp['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_created_at']);
                $tmp['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_deleted_at']);
            }else{
                $tmp['game'] = null;
            }
            $tmp['game_rules'] = $value['game_rules'];
            $tmp['match_type'] = $value['match_type'];
            $tmp['number_of_games'] = $value['number_of_games'];
            // 赛事
            $tmp['tournament'] = self::getTournament($value);
            if($value['tg_group_id']){
                $tmp['group']['group_id'] = $value['tg_group_id'];
                $tmp['group']['name'] = $value['tg_name'];
                $tmp['group']['name_cn'] = $value['tg_name_cn'];
                if($value['eg_game_id']) {
                    $tmp['group']['game']['game_id'] = $value['eg_game_id'];
                    $tmp['group']['game']['name'] = $value['eg_name'];
                    $tmp['group']['game']['name_cn'] = $value['eg_name_cn'];
                    $tmp['group']['game']['default_game_rules'] = $value['eg_default_game_rules'];
                    $tmp['group']['game']['default_match_type'] = $value['eg_default_match_type'];
                    $tmp['group']['game']['full_name'] = $value['eg_full_name'];
                    $tmp['group']['game']['short_name'] = $value['eg_short_name'];
                    $tmp['group']['game']['slug'] = $value['eg_slug'];
                    $tmp['group']['game']['image'] = $value['eg_image'];
                    $tmp['group']['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_modified_at']);
                    $tmp['group']['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_created_at']);
                    $tmp['group']['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_deleted_at']);
                }else{
                    $tmp['group']['game'] = null;
                }
                $tmp['group']['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tg_begin_at']);
                $tmp['group']['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tg_end_at']);
                $tmp['group']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tg_modified_at']);
                $tmp['group']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tg_created_at']);
                $tmp['group']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tg_deleted_at']);
            }else{
                $tmp['group'] = null;
            }
            $match_scheduled_begin_at = $value['scheduled_begin_at'];
            $tmp['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($value['scheduled_begin_at']);
            $tmp['original_scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($value['original_scheduled_begin_at']);
            // 是否调整时间表
            $tmp['is_rescheduled'] = ConversionCriteria::issetTrueFalseType($value['is_rescheduled']);
            // 比分 战队 数组
            $teams = [
                ['team_id' => $value['team_1_id']],
                ['team_id' => $value['team_2_id']]
            ];
            // 参赛战队
            $tmp['teams'] = self::getTeamsData($teams,$value['tournament_id'],$value['group_id']);
            $teamsProcessingData = [];
            foreach ($tmp['teams'] as $k=>$team){
                if($team['team_id'] != -1 && $team['team_id']) {
                    if(isset($value["team_{$team['opponent_order']}_id"])){
                        $teamsProcessingData[$team['opponent_order']] = $team;
                    }
                }else{
                    $teamsProcessingData[$k+1] = $team;
                }
            }
            $tmp['slug'] = $value['slug'];
            $tmp['round_order'] = $value['round_order'];
            $tmp['round_name'] = $value['round_name'];
            $tmp['round_name_cn'] = $value['round_name_cn'];
            $tmp['location'] = $value['location'];
            $tmp['location_cn'] = $value['location_cn'];
            $tmp['description'] = $value['description'];
            $tmp['description_cn'] = $value['description_cn'];
            $tmp['bracket_pos'] = $value['bracket_pos'];
            // 待完善～～
            $tmp['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($value['mr_begin_at']);
            $tmp['end_at'] = ConversionCriteria::DataTimeConversionCriteria($value['mr_end_at']);
            $tmp['status'] = $value['mr_status'];
            $tmp['is_draw'] = ConversionCriteria::issetTrueFalseType($value['mr_is_draw']);
            $tmp['is_forfeit'] = ConversionCriteria::issetTrueFalseType($value['mr_is_forfeit']);
            if(in_array($value['mr_default_advantage'],[1,2])){
                $default_advantage = $teamsProcessingData[$value['mr_default_advantage']];
                unset($default_advantage['team_snapshot']);
            }else{
                $default_advantage = null;
            }
            $tmp['default_advantage'] = $default_advantage;
            // 地图禁用
            $map_ban_pick = ConversionCriteria::issetJsonField($value['mr_map_ban_pick']);
            $tmp['map_vetoes'] = $value['mr_map_ban_pick'] ? self::getMapVetoesData($map_ban_pick,$value['eg_game_id']) : [];
            // 比赛地图
            $map_info = ConversionCriteria::issetJsonField($value['mr_map_info']);
            $tmp['maps'] = $value['mr_map_info'] ? self::getMatchMapsData($map_info, $value['eg_game_id']) : [];
            $scores = [];
            foreach ($teamsProcessingData as $order=>$teamScore){
                unset($teamScore['team_snapshot']);
                if(isset($value["mr_team_{$order}_score"])){
                    $teamScore['score'] = $value["mr_team_{$order}_score"];
                    $scores[] = $teamScore;
                }
            }
            $tmp['scores'] = $scores;
            if(in_array($value['mr_winner'],[1,2])){
                $winner = $teamsProcessingData[$value['mr_winner']];
                unset($winner['team_snapshot']);
            }else{
                $winner = null;
            }
            $tmp['winner'] = $winner;
            $tmp['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($value['mr_is_battle_detailed']);
            if($tmp['is_battle_detailed']) {
                $tmp['battles'] = self::getBattlesData($value['match_id'], $value['tournament_id'], $value['group_id'], $value['eg_game_id']);
            }else{
                $tmp['battles'] = [];
            }
            $is_supported = ConversionCriteria::issetTrueFalseType($value['mr_is_pbpdata_supported']);
            $opens_at = null;
            if ($is_supported) {
                if ($match_scheduled_begin_at) {
                    $match_scheduled_begin_at = date('Y-m-d H:i:s', strtotime( '-15 Minute', strtotime($match_scheduled_begin_at)));
                    $opens_at = ConversionCriteria::DataTimeConversionCriteria($match_scheduled_begin_at);
                }
                $tmp['pbpdata'] = [
                    'is_supported' => $is_supported,
                    'quality' => $value['mr_data_level'], // 实时数据质量
                    'opens_at' => $opens_at,
                    'endpoints' => [
                        [
                            'pbpdata_type' => 'frames',
                            'url' => \Yii::$app->params['pbp_data_url']."/pbpdata/{$value['match_id']}/frames",
                        ],
                        [
                            'pbpdata_type' => 'events',
                            'url' => \Yii::$app->params['pbp_data_url']."/pbpdata/{$value['match_id']}/events",
                        ],
                    ],
                ];
            } else {
                $tmp['pbpdata'] = [
                    'is_supported' => $is_supported,
                    'quality' => $value['mr_data_level'], // 实时数据质量
                    'opens_at' => $opens_at,
                    'endpoints' => [],
                ];
            }
            // 视频直播
            $tmp['streams']['is_supported'] = ConversionCriteria::issetTrueFalseType($value['mr_is_streams_supported']);
            $tmp['streams']['streams'] = self::getStreamsData($value['match_id']);
            $tmp['game_version'] = $value['game_version'];
            $tmp['team_version'] = $value['team_version'];
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['deleted_at']);

            $info[] = $tmp;
        }
        return $info;
    }
    private static function getStreamsData($matchId)
    {
        $field = [
            'stream.id','stream.streamer','stream.platform_id','stream.title','stream.is_official','stream.viewers','stream.preview','stream.live_url','stream.live_embed_url',
            'country.id as country_id','country.e_name as name','country.name as name_cn','country.two as short_name','country.image as image',
            'platform.name as platform_name','platform.image as platform_image'
        ];
        $streamData = MatchStreamList::find()->alias('stream')->select($field)
            ->leftjoin('enum_country as country','country.id = stream.country')
            ->leftjoin('enum_stream_platform as platform','platform.id = stream.platform_id')
            ->where(['stream.match_id'=>$matchId,'stream.flag'=>1])->asArray()->all();
        $streams = [];
        foreach ($streamData as $k=>$v){
            $stream['stream_id'] = $v['id'];
            $stream['streamer'] = $v['streamer'];
            $stream['platform']['platform_id'] = $v['platform_id'];
            $stream['platform']['name'] = $v['platform_name'];
            $stream['platform']['image'] = $v['platform_image'];
            $stream['title'] = $v['title'];
            $stream['is_official'] = ConversionCriteria::issetTrueFalseType($v['is_official']);
            if($v['country_id']) {
                $stream['country']['id'] = $v['country_id'];
                $stream['country']['name'] = $v['name'];
                $stream['country']['name_cn'] = $v['name_cn'];
                $stream['country']['short_name'] = $v['short_name'];
                $stream['country']['image'] = $v['image'];
            }else{
                $stream['country'] = null;
            }
            $stream['viewers'] = $v['viewers'];
            $stream['preview'] = $v['preview'];
            $stream['url'] = $v['live_url'];
            $stream['embed_url'] = $v['live_embed_url'];
            $streams[] = $stream;
        }
        return $streams;
    }
    private static function getTournament($value)
    {
        $tmp = [];
        if($value['tt_tournament_id']) {
            // 赛事新字段
            $tmp['is_private'] = $value['tt_is_private'];
            $tmp['is_private_delay'] = $value['tt_is_private_delay'];
            $tmp['private_delay_seconds'] = $value['tt_private_delay_seconds'];
            $tmp['public_delay'] = $value['tt_public_delay'];
            $tmp['public_data_devel'] = $value['tt_public_data_devel'];

            $tmp['tournament_id'] = $value['tt_tournament_id'];
            $tmp['name'] = $value['tt_name'];
            $tmp['name_cn'] = $value['tt_name_cn'];
            if ($value['eg_game_id']) {
                $tmp['game']['game_id'] = $value['eg_game_id'];
                $tmp['game']['name'] = $value['eg_name'];
                $tmp['game']['name_cn'] = $value['eg_name_cn'];
                $tmp['game']['default_game_rules'] = $value['eg_default_game_rules'];
                $tmp['game']['default_match_type'] = $value['eg_default_match_type'];
                $tmp['game']['full_name'] = $value['eg_full_name'];
                $tmp['game']['short_name'] = $value['eg_short_name'];
                $tmp['game']['slug'] = $value['eg_slug'];
                $tmp['game']['image'] = $value['eg_image'];
                $tmp['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_modified_at']);
                $tmp['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_created_at']);
                $tmp['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_deleted_at']);
            } else {
                $tmp['game'] = null;
            }
            $tmp['status'] = $value['tt_status'];
            $tmp['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tt_scheduled_begin_at']);
            $tmp['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tt_scheduled_end_at']);
//            $tmp['begin_at'] = $value['tt_begin_at'];
//            $tmp['end_at'] = $value['tt_end_at'];
//            $tmp['original_scheduled_begin_at'] = $value['tt_original_scheduled_begin_at'];
//            $tmp['original_scheduled_end_at'] = $value['tt_original_scheduled_end_at'];
//            $tmp['is_rescheduled'] = $value['tt_is_rescheduled'];
            $tmp['short_name'] = $value['tt_short_name'];
            $tmp['short_name_cn'] = $value['tt_short_name_cn'];
            $tmp['slug'] = $value['tt_slug'];
            $tmp['image'] = ImageConversionHelper::showFixedSizeConversion($value['tt_image'], 200, 200);
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tt_modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tt_created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['tt_deleted_at']);
        }else{
            $tmp = null;
        }
        return $tmp;
    }
    // 地图数据
    private static function getMatchMapsData($mapInfo, $gameId)
    {
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            switch ($gameId){
                case self::GAME_STATUS_LOL:
                case self::GAME_STATUS_DOTA2:
                    $map['map'] = self::getBattleMapData($v['map'],$gameId);
                    $maps[] = $map;
                    break;
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    $maps[] = $map;
                    break;
                default:
                    continue;
            }
        }
        return $maps;
    }

    // 地图禁用数据
    private static function getMapVetoesData($mapInfo, $gameId)
    {
        $type = array(
            1 => 'Ban',
            2 => 'Pick',
            3 => 'Left over',
            0 => ''
        );
        $maps = [];
        foreach ($mapInfo as $k => $v) {
            $map['order'] = $k + 1;
            $map['team_id'] = (int)$v['team'];
            $map['type'] = $type[intval($v['type'])];
            switch ($gameId){
                case self::GAME_STATUS_LOL:
                case self::GAME_STATUS_DOTA2:
                    $map['map'] = self::getBattleMapData($v['map'],$gameId);
                    $maps[] = $map;
                    break;
                case self::GAME_STATUS_CSGO:
                    $map['map'] = self::getCsgoMapData($v['map']);
                    $maps[] = $map;
                    break;
                default:
                    continue;
            }
        }
        return $maps;
    }
    private static $csgoMapArray = [];
    // csgo 地图
    private static function getCsgoMapData($mapId){
        if(isset(self::$csgoMapArray[$mapId])){
        }else{
            $mapData = MetadataCsgoMap::find()->alias('map')->select(self::getMapColumns())
                ->leftJoin('enum_csgo_map as csgomap', 'csgomap.id = map.map_type')
                ->where(['map.id'=>$mapId])->asArray()->one();
            if(!empty($mapData)){
                $mapData['is_default'] = ConversionCriteria::issetTrueFalseType($mapData['is_default']);
                $mapData['status'] = $mapData['status']==1 ? 'normal':'removed';
                $mapData['image'] = [];
                $mapData['image']['square_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['square_image'],256,144) ?? null;
                $mapData['image']['rectangle_image'] = ImageConversionHelper::showFixedSizeConversion($mapData['rectangle_image'],675,84) ?? null;
                $mapData['image']['thumbnail'] = ImageConversionHelper::showFixedSizeConversion($mapData['thumbnail'],1380,930) ?? null;
                unset($mapData['square_image']);
                unset($mapData['rectangle_image']);
                unset($mapData['thumbnail']);
            }else{
                return null;
            }
            self::$csgoMapArray[$mapId] = $mapData;
        }
        return self::$csgoMapArray[$mapId];
    }
    // 获取match 比分数据
    private static function getTeamsData($teames,$tournamentId,$groupId)
    {
        $teams = [];
        foreach ($teames as $k=>$val) {
            $teams[$k]['team_id'] = $val['team_id'];
            $teams[$k]['team_snapshot'] = self::getTeamSnapshotData($val['team_id'],$tournamentId,$groupId);
            $teams[$k]['opponent_order'] = $k+1;
        }
        return $teams;
    }
    public static function getBattlesData($matchId,$tournamentId,$groupId,$gameId)
    {
        $matchBattles = [];
        $battles = MatchBattle::find()->alias('battle')->where(['match' => $matchId,'deleted'=>2])
            ->leftJoin('enum_match_state as battle_status', 'battle_status.id = battle.status')
            ->select(self::getBattleColumns())->asArray()->all();
        foreach ($battles as $k=>$battle){
            // battle下游戏
            $battleCre = $battle;
            $battleCre['game'] = self::getGameData($gameId);
            // battle参赛战队
            $battleCre['teams'] = self::getBattleTeamsData($battle['battle_id'],$tournamentId,$groupId);
            $battleCre['begin_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['begin_at']);
            $battleCre['end_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['end_at']);
            // battle状态
            $battleCre['status'] = $battle['status'];
            // 是否平局
            $battleCre['is_draw'] = ConversionCriteria::issetTrueFalseType($battle['is_draw']);
            // 是否弃权
            $battleCre['is_forfeit'] = ConversionCriteria::issetTrueFalseType($battle['is_forfeit']);
            $battleCre['is_default_advantage'] = ConversionCriteria::issetTrueFalseType($battle['is_default_advantage']);
            // battle地图
            $battleCre['map'] = self::getBattleMapData($battle['map'],$gameId);
            // battle比分
            $battleCre['scores'] = self::getBattleScoresData($battle['battle_id']);
            // battle获胜战队
            $battleCre['winner'] = self::getBattleWinnerData($battle['battle_id'],$battle['winner'],$tournamentId,$groupId);
            $battleCre['is_battle_detailed'] = ConversionCriteria::issetTrueFalseType($battle['is_battle_detailed']);
            $battleCre['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['modified_at']);
            $battleCre['created_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['created_at']);
            $battleCre['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($battle['deleted_at']);
            $matchBattles[] = $battleCre;
        }
        return $matchBattles;
    }
    // battle下获胜战队
    private static function getBattleWinnerData($battleId,$winner,$tournamentId,$groupId)
    {
        $battleWinner = MatchBattleTeam::find()->select(['team_id','order'])->where(['battle_id'=>$battleId,'order'=>$winner])->asArray()->one();
        if(empty($battleWinner)) return null;
        return self::getAdvantage($battleWinner['team_id'],$battleWinner['order'],$tournamentId,$groupId);
    }

    // battle下比分
    private static function getBattleScoresData($battleId)
    {
        $battleScores = MatchBattleTeam::find()->select(['team_id','order','score'])->where(['battle_id'=>$battleId])->asArray()->all();
        $teams = [];
        foreach ($battleScores as $k=>$val) {
            $teams[$k]['team_id'] = $val['team_id'];
            $teams[$k]['opponent_order'] = $val['order'];
            $teams[$k]['score'] = $val['score'];
        }
        return $teams;
    }
    // battle下参赛战队
    private static function getBattleTeamsData($battleId,$tournamentId,$groupId)
    {
        $battleTeam = MatchBattleTeam::find()->select(['team_id','order'])->where(['battle_id'=>$battleId])->asArray()->all();
        $teams = [];
        foreach ($battleTeam as $k=>$v){
            $team = self::getAdvantage($v['team_id'],$v['order'],$tournamentId,$groupId,true);
            $teams[] = $team;
        }
        return $teams;
    }

    private static $gameArray = [];
    // 获取游戏数据
    private static function getGameData($gameId)
    {
        if(isset(self::$gameArray[$gameId])){
        }else {
            $game = EnumGame::find()->alias('game')->select(self::getGameColumns())
                ->leftJoin('enum_game_rules as rules', 'game.default_game_rule = rules.id')
                ->leftJoin('enum_game_match_type as type', 'game.default_match_type = type.id')
                ->where(["game.id" => $gameId])->asArray()->one();
            if (!empty($game)) {
                $game['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($game['modified_at']);
                $game['created_at'] = ConversionCriteria::DataTimeConversionCriteria($game['created_at']);
                $game['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($game['deleted_at']);
            }else{
                return null;
            }
            self::$gameArray[$gameId] = $game;
        }
        return self::$gameArray[$gameId];
    }

    private static function getAdvantage($teamId,$order,$tournamentId,$groupId,$isTrue=false)
    {
        $team['team_id'] = $teamId;
        if($isTrue) {
            $team['team_snapshot'] = self::getTeamSnapshotData($teamId, $tournamentId, $groupId);
        }
        $team['opponent_order'] = $order;
        return $team;
    }

    private static $snapArray = [];
    // 公共方法获取战队快照
    private static function getTeamSnapshotData($teamId,$tournamentId,$groupId){
        if(!$teamId){
            return null;
        }
        if ($groupId) {
            $codeKey = 'group-'.$groupId.'-'.$teamId;
            $where = ['team.team_id' => $teamId, 'team.relation_id' => $groupId, 'team.type' => 'group'];
        } else if (empty($groupId) && $tournamentId) {
            $codeKey = 'tournament-'.$tournamentId.'-'.$teamId;
            $where = ['team.team_id' => $teamId,'team.relation_id'=>$tournamentId,'team.type'=>'tournament'];
        } else {
            return [];
        }
        if(isset(self::$snapArray[$codeKey])){
        }else {
            $snapData = TeamSnapshot::find()->alias('team')->select(self::getTeamSnapshotColumns())
                ->leftJoin('team_introduction_snapshot as team_i', 'team.id = team_i.team_id')
                ->leftJoin('enum_country as country', 'team.country = country.id')
                ->where($where)->asArray()->one();
            if (!empty($snapData)){
                $snap['name'] = $snapData['name'];
                if($snapData['country_id']) {
                    $snap['country']['id'] = $snapData['country_id'];
                    $snap['country']['name'] = $snapData['c_name'];
                    $snap['country']['name_cn'] = $snapData['c_name_cn'];
                    $snap['country']['short_name'] = $snapData['c_short_name'];
                    $snap['country']['image'] = $snapData['c_image'];
                }else{
                    $snap['country'] = null;
                }
                $snap['full_name'] = $snapData['full_name'];
                $snap['short_name'] = $snapData['short_name'];
                $snap['slug'] = $snapData['slug'];
                $snap['image'] = ImageConversionHelper::showFixedSizeConversion($snapData['image'],200,200);
                $snap['world_ranking'] = $snapData['world_ranking'];
                $snap['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['modified_at']);
                $snap['created_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['created_at']);
                $snap['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($snapData['deleted_at']);
            }else{
                return null;
            }
            self::$snapArray[$codeKey] = $snap;
        }
        return self::$snapArray[$codeKey];
    }
    // 获取国家地区数据
    private static function getCountryData($countryId)
    {
    }
    // battle 下地图
    private static function getBattleMapData($mapId, $gameId)
    {
        switch ($gameId){
            case self::GAME_STATUS_CSGO:
                // csgo地图
                $map = self::getCsgoMapData($mapId);
                break;
            case self::GAME_STATUS_LOL:
                // lol地图
                $map = MetadataLolMap::find()->alias('map')->select(self::getMapTosColumns())->where(['map.id'=>$mapId])->asArray()->one();
                break;
            case self::GAME_STATUS_DOTA2:
                // dota地图
                $map = MetadataDota2Map::find()->alias('map')->select(self::getMapTosColumns())->where(['map.id'=>$mapId])->asArray()->one();
                break;
            default:
                $map = null;
                break;
        }
        if(!empty($map) && $gameId!=self::GAME_STATUS_CSGO){
            $image = $map['image'];
            $map['is_default'] = $map['is_default']==1 ? true:false;
            $map['status'] = $map['status']==1 ? 'normal':'removed';
            $map['image'] = [];
            $map['image']['square_image'] = null;
            $map['image']['rectangle_image'] = null;
            $map['image']['thumbnail'] = $image ?? null;
            $map['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($map['modified_at']);
            $map['created_at'] = ConversionCriteria::DataTimeConversionCriteria($map['created_at']);
            $map['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($map['deleted_at']);
        }
        return $map;
    }
    private static function getMapTosColumns(){
        return [
            'id as map_id',
            'name as name',
            'name_cn as name_cn',
            'state as status',
            'external_id as external_id',
            'external_name as external_name',
            'short_name as short_name',
            'map_type as map_type',
            'map_type_cn as map_type_cn',
            'is_default as is_default',
            'slug as slug',
            'image as image',
            'modified_at as modified_at',
            'created_at as created_at',
            'deleted_at as deleted_at',
        ];
    }
    // 地图字段
    private static function getMapColumns()
    {
        return [
            'map.id as map_id',
            'map.name as name',
            'map.name_cn as name_cn',
            'map.state as status',
            'map.external_id as external_id',
            'map.external_name as external_name',
            'map.short_name as short_name',
            'csgomap.e_name as map_type',
            'csgomap.c_name as map_type_cn',
            'map.is_default as is_default',
            'map.slug as slug',
            'map.is_default as image',
            'map.square_image as square_image',
            'map.rectangle_image as rectangle_image',
            'map.thumbnail as thumbnail',
            'map.modified_at as modified_at',
            'map.created_at as created_at',
            'map.deleted_at as deleted_at',
        ];
    }
    // 国家字段
    public static function getCountryColumns()
    {
        return [
            'country.id as country_id',
            'country.e_name as name',
            'country.name as name_cn',
            'country.two as short_name',
            'country.image as image',
        ];
    }
    // 战队快照字段
    public static function getTeamSnapshotColumns()
    {
        return [
            'team.name as name',
            'country.id as country_id',
            'country.e_name as c_name',
            'country.name as c_name_cn',
            'country.two as c_short_name',
            'country.image as c_image',
            'team.full_name as full_name',
            'team.short_name as short_name',
            'team.slug as slug',
            'team.image as image',

            'team_i.world_ranking as world_ranking',

            'team.modified_at as modified_at',
            'team.created_at as created_at',
            'team.deleted_at as deleted_at',
        ];
    }
    // 对局字段
    private static function getBattleColumns()
    {
        return [
            'battle.id as battle_id',
            'battle.game as game',
            'battle.order as order',
            'battle.order as teams',
            'battle.begin_at as begin_at',
            'battle.end_at as end_at',
            'battle_status.c_name as status',
//            'battle.status as status',
            'battle.is_draw as is_draw',
            'battle.is_forfeit as is_forfeit',
            'battle.is_default_advantage as is_default_advantage',
            'battle.map as map',
            'battle.duration as duration',
            'battle.winner as scores',
            'battle.winner as winner',
            'battle.is_battle_detailed as is_battle_detailed',
            'battle.modified_at as modified_at',
            'battle.created_at as created_at',
            'battle.deleted_at as deleted_at',
        ];
    }
    // 游戏字段
    private static function getGameColumns()
    {
        return [
            'game.id as game_id',
            'game.e_name as name',
            'game.name as name_cn',
            'rules.name as default_game_rules',
            'type.name as default_match_type',
            'game.full_name as full_name',
            'game.short_name as short_name',
            'game.slug as slug',
            'game.image as image',
            'game.modified_at as modified_at',
            'game.created_at as created_at',
            'game.deleted_at as deleted_at',
        ];
    }

    private static function getWhere($params)
    {
        return [];
    }
    private static function getColumns()
    {
        return [
            // 比赛
            'm.id as match_id',
            'm.deleted as deleted',
            'm.tournament_id as tournament_id',
            'm.group_id as group_id',
            'm.team_1_id as team_1_id',
            'm.team_2_id as team_2_id',
            'm_rules.name as game_rules',
            'm_type.name as match_type',
            'm.number_of_games as number_of_games',
            'm.scheduled_begin_at as scheduled_begin_at',
            'm.original_scheduled_begin_at as original_scheduled_begin_at',
            'm.is_rescheduled as is_rescheduled',
            'm.slug as slug',
            // news
            'match_r.begin_at as mr_begin_at',
            'match_r.end_at as mr_end_at',
            'm_status.c_name as mr_status',
            'match_r.is_draw as mr_is_draw',
            'match_r.is_forfeit as mr_is_forfeit',
            'match_r.default_advantage as mr_default_advantage',
            'match_r.map_ban_pick as mr_map_ban_pick',
            'match_r.map_info as mr_map_info',
            'match_r.team_1_score as mr_team_1_score',
            'match_r.team_2_score as mr_team_2_score',
            'match_r.winner as mr_winner',
            'match_r.is_battle_detailed as mr_is_battle_detailed',
            'match_r.is_pbpdata_supported as mr_is_pbpdata_supported',
            'match_r.is_streams_supported as mr_is_streams_supported',
            'match_r.data_level as mr_data_level',
            'match_r.data_speed as mr_data_speed',

            'mb.round_order as round_order',
            'mb.round_name as round_name',
            'mb.round_name_cn as round_name_cn',
            'mb.location as location',
            'mb.location_cn as location_cn',
            'mb.bracket_pos as bracket_pos',
            'mb.description as description',
            'mb.description_cn as description_cn',
            'm.game_version as game_version',
            'm.team_version as team_version',
            'm.modified_at as modified_at',
            'm.created_at as created_at',
            'm.deleted_at as deleted_at',
            // 赛事
            'tt.id as tt_tournament_id',
            'tt.son_match_sort as tt_son_match_sort',
            'tt.name as tt_name',
            'tt.name_cn as tt_name_cn',
            'tt_status.c_name as tt_status',
            'tt.scheduled_begin_at as tt_scheduled_begin_at',
            'tt.scheduled_end_at as tt_scheduled_end_at',
//            'tt.begin_at as tt_begin_at',
//            'tt.end_at as tt_end_at',
//            'tt.original_scheduled_begin_at as tt_original_scheduled_begin_at',
//            'tt.original_scheduled_end_at as tt_original_scheduled_end_at',
//            'tt.is_rescheduled as tt_is_rescheduled',
            'tt.short_name as tt_short_name',
            'tt.short_name_cn as tt_short_name_cn',
            'tt.slug as tt_slug',
            'tt.image as tt_image',
            'tt.modified_at as tt_modified_at',
            'tt.created_at as tt_created_at',
            'tt.deleted_at as tt_deleted_at',
            // 新加入字段
            'tt.is_private as tt_is_private',
            'tt.public_delay as tt_public_delay',
            'tt.public_data_devel as tt_public_data_devel',
            'tt.is_private_delay as tt_is_private_delay',
            'tt.private_delay_seconds as tt_private_delay_seconds',
            // 游戏
            'eg.id as eg_game_id',
            'eg.e_name as eg_name',
            'eg.name as eg_name_cn',
//            'eg.default_game_rule as eg_default_game_rules',
//            'eg.default_match_type as eg_default_match_type',
            'rules.name as eg_default_game_rules',
            'type.name as eg_default_match_type',
            'eg.full_name as eg_full_name',
            'eg.short_name as eg_short_name',
            'eg.slug as eg_slug',
            'eg.image as eg_image',
            'eg.modified_at as eg_modified_at',
            'eg.created_at as eg_created_at',
            'eg.deleted_at as eg_deleted_at',
            // Fenzu
            'tg.id as tg_group_id',
            'tg.name as tg_name',
            'tg.name_cn as tg_name_cn',
            'tg.scheduled_begin_at as tg_begin_at',
            'tg.scheduled_end_at as tg_end_at',
            'tg.modified_at as tg_modified_at',
            'tg.created_at as tg_created_at',
            'tg.deleted_at as tg_deleted_at',
        ];
    }

    private static function getQuery()
    {
        return Match::find()->alias('m')
            ->leftJoin('enum_game_rules as m_rules','m_rules.id = m.game_rules')
            ->leftJoin('enum_game_match_type as m_type','m_type.id = m.match_type')
            ->leftJoin('match_real_time_info as match_r','m.id = match_r.id')
            ->leftJoin('enum_match_state as m_status','m_status.id = match_r.status')
            ->leftJoin('match_base as mb','m.id = mb.match_id')
            ->leftjoin('enum_game as eg','m.game = eg.id')
            ->leftJoin('enum_game_rules as rules','eg.default_game_rule = rules.id')
            ->leftJoin('enum_game_match_type as type','eg.default_match_type = type.id')
            ->leftjoin('tournament as tt','tt.id = m.tournament_id')
            ->leftJoin('enum_match_state as tt_status','tt_status.id = tt.status')
            ->leftjoin('tournament_group as tg','tg.id = m.group_id');
    }

    public static function getMatchDetail($matchId=null,$params,$status = false)
    {
        $q = self::getQuery();
        if ($matchId) {
            $q = $q->where(['m.id' => $matchId]);
            $list = $q->select(self::getColumns())->asArray()->all();
            return [
                'list' => self::getFormat($list)
            ];
        }else {
            if ($status) {
                // 参数
                $deleted = !isset($params['deleted'])||empty($params['deleted']) ? 2 : $params['deleted'];
                $page = (empty($params['page']) ? 1 : $params['page'])-1;
                $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
                if ($status == 2) {
                    // 状态筛选0
                    $q = $q->where(['or', ['match_r.status' => $status]]);
//                    $dateTime = date("Y-m-d H:i:s", time());
                    $firstFiveDate = date("Y-m-d H:i:s", strtotime("-5 minute"));
//                    $afterFiveDate = date("Y-m-d H:i:s", strtotime("+5 minute"));
//                    $q->orWhere(['and', ['>=', "m.scheduled_begin_at", $firstFiveDate], ['<=', "m.scheduled_begin_at", $dateTime]]);
                    $q->orWhere(['and', ['>=', "match_r.end_at", $firstFiveDate]]);
                } else {
                    // 状态筛选
                    $q = $q->where(['match_r.status' => $status]);
                }
                // 筛选
                if (isset($params['tournament_id']) && !empty($params['tournament_id'])) {
                    $q->andWhere(['m.tournament_id' => $params['tournament_id']]);
                }
                if (isset($params['game_id']) && !empty($params['game_id'])) {
                    $q->andWhere(['m.game' => $params['game_id']]);
                }
                $q->andWhere(['m.deleted' => $deleted]);
                $count = $q->count();
                $pages = new Pagination([
                    'totalCount' => $count,
                    'pageSize' => $pageSize,  // 分页默认条数是20条
                    'page' => $page,
                ]);
                $list = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)->orderBy('m.scheduled_begin_at desc')->asArray()->all();
//                $sql=$q->createCommand()->getRawSql();
                return [
                    'count' => $count,
                    'list' => self::getFormat($list)
                ];
            }
        }
    }
}
