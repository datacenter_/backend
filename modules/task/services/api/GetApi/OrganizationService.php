<?php

namespace app\modules\task\services\api\GetApi;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\org\models\Clan;
use app\modules\org\models\Team;
use yii\data\Pagination;

class OrganizationService
{
    public static function getList($params)
    {
        $q = self::getQuery();
        if(isset($params['name'])){
            $q->where(['like', 'c.name', $params['name']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $clanList = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        // 战队国家
        $teamsList = self::getTeams($clanList);
        return [
            'count' => $count,
            'list' => self::getFormat($clanList, $teamsList, true),
        ];
    }

    public static function getTeams($List)
    {
        $ability = self::getTeam(array_column($List, 'organization_id'));
        $abilityList = $ability->select([
            // 战队
            't.id as team_id',
            't.organization as organization_id',
            't.name as t_name',
            't.full_name as t_full_name',
            't.short_name as t_short_name',
            't.alias as t_alias',
            't.slug as t_slug',
            't.image as t_image',
            't.modified_at as t_modified_at',
            't.created_at as t_created_at',
            't.deleted_at as t_deleted_at',
            // 游戏
            'eg.id as eg_game_id',
            'eg.e_name as eg_name',
            'eg.name as eg_name_cn',
            'eg.default_game_rule as eg_default_game_rule',
            'eg.default_match_type as eg_default_match_type',
            'eg.full_name as eg_full_name',
            'eg.short_name as eg_short_name',
            'eg.slug as eg_slug',
            'eg.image as eg_image',
            'eg.modified_at as eg_modified_at',
            'eg.created_at as eg_created_at',
            'eg.deleted_at as eg_deleted_at',

            'eg_rules.name as default_game_rules',
            'eg_type.name as default_match_type',
            // 战队国家
            'tec.id as tec_id',
            'tec.e_name as tec_name',
            'tec.name as tec_name_cn',
            'tec.two as tec_short_name',
            'tec.image as tec_country_image',

        ])->where(['deleted'=>2])->asArray()->all();
        return $abilityList;
    }

    private static function getTeam($ids)
    {
        return Team::find()->alias('t')
            ->leftjoin('enum_game AS eg', 'eg.id = t.game')
            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = eg.default_game_rule')
            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = eg.default_match_type')
            ->leftjoin('enum_country AS tec', 'tec.id = t.country')
            ->where(['in', 't.organization', $ids])
            ->orderBy('t.id');
    }

    public static function getDetail($Id)
    {
        $q = self::getQuery();
        $q = $q->where(['c.id' => $Id]);
        $clanList = $q->select(self::getColumns())->asArray()->all();
        // 战队国家
        $teamsList = self::getTeams($clanList);
        return self::getFormat($clanList, $teamsList, true);
    }

    private static function getWhere($params)
    {
        return [];
    }

    private static function getColumns()
    {
        return [
            'c.id as organization_id',
            'c.deleted as deleted',
            'c.name as name',
            'c.full_name as full_name',
            'c.short_name as short_name',
            'c.alias as alias',
            'c.slug as slug',
            'c.image as image',
            'c.modified_at as modified_at',
            'c.created_at as created_at',
            'c.deleted_at as deleted_at',

            'ci.introduction as introduction',
            'ci.introduction_cn as introduction_cn',
            // 国家
            'ec.id as ec_id',
            'ec.e_name as ec_name',
            'ec.name as ec_name_cn',
            'ec.two as ec_short_name',
            'ec.image as ec_country_image'
        ];
    }

    private static function getQuery()
    {
        return Clan::find()->alias('c')
            ->leftjoin('clan_introduction AS ci', 'c.id = ci.clan_id')
            ->leftjoin('enum_country AS ec', 'ec.id = c.country');
    }

    private static function getFormat($clanList, $teamsList, $isDetail = false)
    {
        $info = [];
        foreach ($clanList as $key => &$value) {
            $tmp['organization_id'] = $value['organization_id'];
            $tmp['deleted'] = $value['deleted'];
            $tmp['name'] = $value['name'];
            if ($value['ec_id']) {
                $tmp['country']['id'] = $value['ec_id'];
                $tmp['country']['name'] = $value['ec_name'];
                $tmp['country']['name_cn'] = $value['ec_name_cn'];
                $tmp['country']['short_name'] = $value['ec_short_name'];
                $tmp['country']['image'] = $value['ec_country_image'];
            }else{
                $tmp['country'] = null;
            }
            $tmp['full_name'] = $value['full_name'];
            $tmp['short_name'] = $value['short_name'];
            $tmp['alias'] = $value['alias'];
            $tmp['slug'] = $value['slug'];
            $tmp['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'],200,200);
            if($isDetail){
                $tmp['introduction'] = $value['introduction'];
                $tmp['introduction_cn'] = $value['introduction_cn'];
            }
            $a = [];
            foreach ($teamsList as $val) {
                if ($value['organization_id'] == $val['organization_id']) {
                    $t['team_id'] = $val['team_id'];
                    $t['name'] = $val['t_name'];
                    if ($val['eg_game_id']) {
                        $t['game']['game_id'] = $val['eg_game_id'];
                        $t['game']['name'] = $val['eg_name'];
                        $t['game']['name_cn'] = $val['eg_name_cn'];
                        $t['game']['default_game_rules'] = $val['default_game_rules'];
                        $t['game']['default_match_type'] = $val['default_match_type'];
                        $t['game']['full_name'] = $val['eg_full_name'];
                        $t['game']['short_name'] = $val['eg_short_name'];
                        $t['game']['slug'] = $val['eg_slug'];
                        $t['game']['image'] = $val['eg_image'];
                        $t['game']['modified_at'] = $val['eg_modified_at'];
                        $t['game']['created_at'] = $val['eg_created_at'];
                        $t['game']['deleted_at'] = $val['eg_deleted_at'];
                    }else{
                        $t['game'] = null;

                    }
                    if ($val['tec_id']) {
                        $t['country']['id'] = $val['tec_id'];
                        $t['country']['name'] = $val['tec_name'];
                        $t['country']['name_cn'] = $val['tec_name_cn'];
                        $t['country']['short_name'] = $val['tec_short_name'];
                        $t['country']['image'] = $val['tec_country_image'];
                    }else{
                        $t['country'] = null;
                    }
                    $t['full_name'] = $val['t_full_name'];
                    $t['short_name'] = $val['t_short_name'];
                    $t['alias'] = $val['t_alias'];
                    $t['slug'] = $val['t_slug'];
                    $t['image'] = ImageConversionHelper::showFixedSizeConversion($val['t_image'],200,200);;
                    $t['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['t_modified_at']);
                    $t['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['t_created_at']);
                    $t['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['t_deleted_at']);
                    $a[] = $t;
                }
            }
            $tmp['teams'] = $a ?: [];
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['deleted_at']);
//            $isDetail ? $info = $tmp : $info[] = $tmp;
            $info[] = $tmp;
        }
        return $info;
    }
}
