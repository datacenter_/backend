<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use yii\data\Pagination;

class PlayerService
{
    public static function getPlayerList($params)
    {
        $q = self::getQuery();
        $where = self::getWhere();
        $q = $q->where($where);
        if(isset($params['player_ids']) && is_array($params['player_ids'])){
            $q->andWhere(['in', 'p.id', $params['player_ids']]);
        }
//        if(isset($params['name'])){
//            $q->andWhere(['like',"p.name",$params['name']]);
//        }
//        if(isset($params['game_id']) && !empty($params['game_id'])){
//            $q->andWhere(['=',"p.game_id",$params['game_id']]);
//        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        $teams = self::getTeams($list);
        return [
            'count' => $count,
            'list' => self::getFormat($list,$teams,true),
        ];
    }

    public static function getTeams($list, $key = 'player_id')
    {
        if ($key == 'group_id') {
            $ids = isset($list['id']) ? [$list['id']] : array_column($list, 'id');
        } else {
            $ids = array_column($list, $key);
        }

        $team = Team::find()->alias('t');
        $f = [];
        if ($key == 'player_id') {
            $f = ['tr.player_id as player_id'];
            $team->leftjoin('team_player_relation AS tr', 't.id = tr.team_id');
        } elseif ($key == 'tournament_id') {
            $f = ['tr.tournament_id as tournament_id', 'tr.match_condition', 'tr.match_condition_cn', 'tr.team_sort'];
            $team->leftjoin('tournament_team_relation AS tr', 't.id = tr.team_id');
        } elseif ($key == 'group_id') {
            $team->leftjoin('tournament_team_group AS tr', 't.id = tr.team_id');
        }
        $team->leftjoin('enum_country AS tec', 't.country = tec.id')
            ->leftjoin('enum_game AS teg', 't.game = teg.id')
            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = teg.default_game_rule')
            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = teg.default_match_type')
            ->where(['in', "tr.$key", $ids])->andWhere(['t.deleted'=>2]);

        $filed = [
            // 战队
            't.id as team_id',
            't.name as name',
            't.full_name as full_name',
            't.short_name as short_name',
            't.alias as alias',
            't.slug as slug',
            't.image as image',
            't.modified_at as modified_at',
            't.created_at as created_at',
            't.deleted_at as deleted_at',
            // 选手游戏
            'teg.id as teg_game_id',
            'teg.e_name as teg_name',
            'teg.name as teg_name_cn',
//            'teg.default_game_rule as teg_default_game_rules',
//            'teg.default_match_type as teg_default_match_type',
            // 类型
            'eg_rules.name as default_game_rules',
            'eg_type.name as default_match_type',
            'teg.full_name as teg_full_name',
            'teg.short_name as teg_short_name',
            'teg.slug as teg_slug',
            'teg.image as teg_image',
            'teg.modified_at as teg_modified_at',
            'teg.created_at as teg_created_at',
            'teg.deleted_at as teg_deleted_at',

            // 选手国籍
            'tec.id as tec_id',
            'tec.e_name as tec_name',
            'tec.name as tec_name_cn',
            'tec.two as tec_short_name',
            'tec.image as tec_image',

        ];
        $filed = array_merge($filed, $f);
        $teamList = $team->select($filed)->asArray()->all();
        return $teamList;
    }

    public static function getPlayerDetail($id)
    {
        $q = self::getQuery();
        $q = $q->where(['p.id' => $id]);
        $list = $q->select(self::getColumns())->asArray()->all();
        $teams = self::getTeams($list);
        return self::getFormat($list, $teams,true);
    }

    private static function getWhere()
    {
//        return ['p.deleted' => 2];
        return [];
    }

    private static function getColumns()
    {
        return [
            'p.id as player_id',
            'p.deleted as p_deleted',
            'p.nick_name as p_nick_name',
            'p.name as p_name',
            'p.name_cn as p_name_cn',
            'position.c_name as p_role',
//            'p.role as p_role',
            'p.steam_id as p_steam_id',
            'p.slug as p_slug',
            'p.image as p_image',
            'p.modified_at as p_modified_at',
            'p.created_at as p_created_at',
            'p.deleted_at as p_deleted_at',
            // base
            'p_base.birthday','p_base.introduction','p_base.introduction_cn',
            // 游戏
            'eg.id as eg_game_id',
            'eg.e_name as eg_name',
            'eg.name as eg_name_cn',
//            'eg.default_game_rule as eg_default_game_rules',
//            'eg.default_match_type as eg_default_match_type',
            // 类型
            'eg_rules.name as default_game_rules',
            'eg_type.name as default_match_type',
            'eg.full_name as eg_full_name',
            'eg.short_name as eg_short_name',
            'eg.slug as eg_slug',
            'eg.image as eg_image',
            'eg.modified_at as eg_modified_at',
            'eg.created_at as eg_created_at',
            'eg.deleted_at as eg_deleted_at',


            // 城市
            'ec.id as ec_id',
            'ec.e_name as ec_name',
            'ec.name as ec_name_cn',
            'ec.two as ec_short_name',
            'ec.image as ec_image',
        ];
    }

    private static function getQuery()
    {
        return Player::find()->alias('p')
            ->leftjoin('player_base AS p_base','p_base.player_id = p.id')
            ->leftjoin('enum_position AS position','position.id = p.role')
            ->leftjoin('enum_game AS eg', 'p.game_id = eg.id')
            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = eg.default_game_rule')
            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = eg.default_match_type')
            ->leftjoin('enum_country AS ec', 'p.player_country = ec.id');
    }

    private static function getFormat($list,$teams,$isDetail=false)
    {
        $info = [];
        foreach ($list as $key => &$value) {
            $tmp['player_id'] = $value['player_id'];
            $tmp['deleted'] = $value['p_deleted'];
            $tmp['nick_name'] = $value['p_nick_name'];
            // 游戏
            if ($value['eg_game_id']){
            $tmp['game']['game_id'] = $value['eg_game_id'];
            $tmp['game']['name'] = $value['eg_name'];
            $tmp['game']['name_cn'] = $value['eg_name_cn'];
            $tmp['game']['default_game_rules'] = $value['default_game_rules'];
            $tmp['game']['default_match_type'] = $value['default_match_type'];
            $tmp['game']['full_name'] = $value['eg_full_name'];
            $tmp['game']['short_name'] = $value['eg_short_name'];
            $tmp['game']['slug'] = $value['eg_slug'];
            $tmp['game']['image'] = $value['eg_image'];
            $tmp['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_modified_at']);
            $tmp['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_created_at']);
            $tmp['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_deleted_at']);
            }else{
                $tmp['game'] = null;
            }
            $tmp['nationality'] = [];
            if($value['ec_id']) {
                $tmp['nationality']['id'] = $value['ec_id'];
                $tmp['nationality']['name'] = $value['ec_name'];
                $tmp['nationality']['name_cn'] = $value['ec_name_cn'];
                $tmp['nationality']['short_name'] = $value['ec_short_name'];
                $tmp['nationality']['image'] = $value['ec_image'];
            }else{
                $tmp['nationality'] = null;
            }
            $tmp['name'] = $value['p_name'];
            $tmp['name_cn'] = $value['p_name_cn'];
            $tmp['role'] = strval($value['p_role']);
            $tmp['steam_id'] = strval($value['p_steam_id']);
            $tmp['slug'] = strval($value['p_slug']);
            $tmp['image'] = ConversionCriteria::playerResizeConversion($value['p_image']);
            if($isDetail){
                $tmp['birthday'] = $value['birthday'];
                $tmp['introduction'] = $value['introduction'];
                $tmp['introduction_cn'] = $value['introduction_cn'];
            }

            // 选手
            $a = [];
            foreach ($teams as $val) {
                if ($val['player_id'] == $value['player_id']) {
                    $ab['team_id'] = $val['team_id'];
                    $ab['name'] = $val['name'];
                    if ($val['teg_game_id']) {
                    $ab['game']['game_id'] = $val['teg_game_id'];
                    $ab['game']['name'] = $val['teg_name'];
                    $ab['game']['name_cn'] = $val['teg_name_cn'];
                    $ab['game']['default_game_rules'] = $val['default_game_rules'];
                    $ab['game']['default_match_type'] = $val['default_match_type'];
                    $ab['game']['full_name'] = $val['teg_full_name'];
                    $ab['game']['short_name'] = $val['teg_short_name'];
                    $ab['game']['slug'] = $val['teg_slug'];
                    $ab['game']['image'] = $val['teg_image'];

                    $ab['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['teg_modified_at']);
                    $ab['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['teg_created_at']);
                    $ab['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['teg_deleted_at']);
                    }else{
                        $ab['game'] = null;
                    }
                    if ($val['tec_id']) {
                        $ab['country']['id'] = $val['tec_id'];
                        $ab['country']['name'] = $val['tec_name'];
                        $ab['country']['name_cn'] = $val['tec_name_cn'];
                        $ab['country']['short_name'] = $val['tec_short_name'];
                        $ab['country']['image'] = $val['tec_image'];
                    }else{
                        $ab['country'] = null;
                    }
                    $ab['full_name'] = $val['full_name'];
                    $ab['short_name'] = $val['short_name'];
                    $ab['alias'] = $val['alias'];
                    $ab['slug'] = $val['slug'];
                    $ab['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'],200,200);
                    $ab['status'] = self::getPlayerState(TeamPlayerRelation::find()->select(['status_id'])
                        ->where(["team_id"=>$val['team_id'],'player_id'=>$value['player_id']])->asArray()->one()['status_id']);
                    $ab['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['modified_at']);
                    $ab['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['created_at']);
                    $ab['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['deleted_at']);
                    $a[] = $ab;
                }
            }
            $tmp['teams'] = $a;

            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['p_modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['p_created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['p_deleted_at']);
//            $isDetail ? $info = $tmp : $info[] = $tmp;
            $info[] = $tmp;
        }
        return $info;
    }
    public static function getPlayerState($state){
        switch ($state){
            case 1:
                return 'Active';
                break;
            case 2:
                return 'Trial';
                break;
            case 3:
                return 'Two-way';
                break;
            case 4:
                return 'Inactive';
                break;
            case 5:
                return 'Loan';
                break;
            case 6:
                return 'Coach';
                break;
            default :
                return null;
                break;
        }
    }
}
