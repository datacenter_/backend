<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use yii\data\Pagination;

class TeamService
{
    public static function getTeamList($params)
    {
        $q = self::getQuery()->where(self::getWhere());
        if(isset($params['team_ids']) && is_array($params['team_ids'])){
            $q->andWhere(['in', 't.id', $params['team_ids']]);
        }
//        if(isset($params['name'])){
//            $q->andWhere(['like',"t.name",$params['name']]);
//        }
//        if(isset($params['game_id']) && !empty($params['game_id'])){
//            $q->andWhere(['=',"t.game",$params['game_id']]);
//        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        $players = self::getPlayers($list);
        return [
            'count'=>$count,
            'list'=>self::getFormat($list,$players,true),
        ];
    }
    public static function getPlayers($list)
    {
        $team = Player::find()->alias('p')
            ->leftjoin('enum_position AS position','position.id = p.role')
            ->leftjoin('team_player_relation AS tr','p.id = tr.player_id')
            ->leftjoin('enum_game AS peg','p.game_id = peg.id')
            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = peg.default_game_rule')
            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = peg.default_match_type')
            ->leftjoin('enum_country AS pec','p.player_country = pec.id')
            ->where(['in', 'tr.team_id', array_column($list,'team_id')])->andWhere(['p.deleted'=>2]);
        $teamList = $team->select([
            // 选手
            'tr.team_id as team_id',
            'p.id as player_id',
            'p.nick_name as p_nick_name',
            'p.name as p_name',
            'p.name_cn as p_name_cn',
            'position.c_name as p_role',
//            'p.role as p_role',
            'p.steam_id as p_steam_id',
            'p.slug as p_slug',
            'p.image as p_image',
            'p.modified_at as pp_modified_at',
            'p.created_at as p_created_at',
            'p.deleted_at as p_deleted_at',
            // 选手游戏
            'peg.id as peg_game_id',
            'peg.e_name as peg_name',
            'peg.name as peg_name_cn',
//            'peg.default_game_rule as peg_default_game_rules',
//            'peg.default_match_type as peg_default_match_type',
            // 类型
            'eg_rules.name as default_game_rules',
            'eg_type.name as default_match_type',

            'peg.full_name as peg_full_name',
            'peg.short_name as peg_short_name',
            'peg.slug as peg_slug',
            'peg.image as peg_image',
            'peg.modified_at as peg_modified_at',
            'peg.created_at as peg_created_at',
            'peg.deleted_at as peg_deleted_at',

            // 选手国籍
            'pec.id as pec_id',
            'pec.e_name as pec_name',
            'pec.name as pec_name_cn',
            'pec.two as pec_short_name',
            'pec.image as pec_image',

        ])->asArray()->all();
        return $teamList;
    }
    public static function getTeamDetail($id)
    {
        $q = self::getQuery();
        $q = $q->where(['t.id'=>$id]);
        $list = $q->select(self::getColumns())->asArray()->all();
        $players = self::getPlayers($list);
        return self::getFormat($list,$players,true);
    }

    private static function getWhere()
    {
//        return ['t.deleted' => 2];
        return [];
    }
    private static function getColumns()
    {
        return [
            't.id as team_id',
            't.deleted as deleted',
            't.name as name',
            't.full_name as full_name',
            't.short_name as short_name',
            't.alias as alias',
            't.slug as slug',
            't.image as image',
            't.modified_at as modified_at',
            't.created_at as created_at',
            't.deleted_at as deleted_at',

            // new
            't_intr.region',
            't_intr.world_ranking',
            't_intr.total_earnings',
            't_intr.average_player_age',
            't_intr.introduction',
            't_intr.introduction_cn',
            't_intr.history_players',
            //new
            'intr_ec.id as intr_id',
            'intr_ec.e_name as intr_name',
            'intr_ec.name as intr_name_cn',
            'intr_ec.two as intr_short_name',
            'intr_ec.image as intr_image',
            // 游戏
            'eg.id as eg_game_id',
            'eg.e_name as eg_name',
            'eg.name as eg_name_cn',
            'eg.default_game_rule as eg_default_game_rules',
            'eg.default_match_type as eg_default_match_type',
            'eg.full_name as eg_full_name',
            'eg.short_name as eg_short_name',
            'eg.slug as eg_slug',
            'eg.image as eg_image',
            'eg.modified_at as eg_modified_at',
            'eg.created_at as eg_created_at',
            'eg.deleted_at as eg_deleted_at',
            // 类型
            'eg_rules.name as default_game_rules',
            'eg_type.name as default_match_type',
            // 俱乐部
            'c.id as organization_id',
            'c.name as c_name',
            'c.full_name as c_full_name',
            'c.short_name as c_short_name',
            'c.alias as c_alias',
            'c.slug as c_slug',
            'c.image as c_image',
            'c.modified_at as c_modified_at',
            'c.created_at as c_created_at',
            'c.deleted_at as c_deleted_at',
            // 俱乐部下城市
            'ecc.id as ecc_id',
            'ecc.e_name as ecc_name',
            'ecc.name as ecc_name_cn',
            'ecc.two as ecc_short_name',
            'ecc.image as ecc_image',
            // 城市
            'ec.id as ec_id',
            'ec.e_name as ec_name',
            'ec.name as ec_name_cn',
            'ec.two as ec_short_name',
            'ec.image as ec_image',
        ];
    }

    private static function getQuery()
    {
        return Team::find()->alias('t')
            ->leftjoin('team_introduction AS t_intr','t_intr.team_id = t.id')
            ->leftjoin('enum_country AS intr_ec','intr_ec.id = t_intr.region')
            ->leftjoin('enum_game AS eg','t.game = eg.id')
            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = eg.default_game_rule')
            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = eg.default_match_type')
            ->leftjoin('clan AS c','t.organization = c.id')
            ->leftjoin('enum_country AS ec','t.country = ec.id')
            ->leftjoin('enum_country AS ecc','c.country = ecc.id');
    }

    private static function getFormat($list,$players,$isDetail=false)
    {
        $info = [];
        foreach ($list as $key=>&$value)
        {
            $tmp['team_id'] = $value['team_id'];
            $tmp['deleted'] = $value['deleted'];
            $tmp['name'] = $value['name'];
            // 游戏
            if($value['eg_game_id']) {
                $tmp['game']['game_id'] = $value['eg_game_id'];
                $tmp['game']['name'] = $value['eg_name'];
                $tmp['game']['name_cn'] = $value['eg_name_cn'];
                $tmp['game']['default_game_rules'] = $value['default_game_rules'];
                $tmp['game']['default_match_type'] = $value['default_match_type'];
                $tmp['game']['full_name'] = $value['eg_full_name'];
                $tmp['game']['short_name'] = $value['eg_short_name'];
                $tmp['game']['slug'] = $value['eg_slug'];
                $tmp['game']['image'] = $value['eg_image'];
                $tmp['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_modified_at']);
                $tmp['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_created_at']);
                $tmp['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['eg_deleted_at']);
            }else{
                $tmp['game'] = null;
            }
            // 俱乐部
            if($value['organization_id']) {
                $tmp['organization']['organization_id'] = $value['organization_id'];
                $tmp['organization']['name'] = $value['c_name'];
                $tmp['organization']['country'] = [];
                if($value['ecc_id']) {
                    $tmp['organization']['country']['id'] = $value['ecc_id'];
                    $tmp['organization']['country']['name'] = $value['ecc_name'];
                    $tmp['organization']['country']['name_cn'] = $value['ecc_name_cn'];
                    $tmp['organization']['country']['short_name'] = $value['ecc_short_name'];
                    $tmp['organization']['country']['image'] = $value['ecc_image'];
                }else{
                    $tmp['organization']['country'] = null;
                }
                $tmp['organization']['full_name'] = $value['c_full_name'];
                $tmp['organization']['short_name'] = $value['c_short_name'];
                $tmp['organization']['alias'] = $value['c_alias'];
                $tmp['organization']['slug'] = $value['c_slug'];
                $tmp['organization']['image'] = ImageConversionHelper::showFixedSizeConversion($value['c_image'], 200, 200);
                $tmp['organization']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['c_modified_at']);
                $tmp['organization']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['c_created_at']);
                $tmp['organization']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['c_deleted_at']);
            }else{
                $tmp['organization'] = null;
            }
            if ($value['ec_id']){
                $tmp['country']['id'] = $value['ec_id'];
                $tmp['country']['name'] = $value['ec_name'];
                $tmp['country']['name_cn'] = $value['ec_name_cn'];
                $tmp['country']['short_name'] = $value['ec_short_name'];
                $tmp['country']['image'] = $value['ec_image'];

            }else{
                $tmp['country'] = null;
            }


            $tmp['full_name'] = $value['full_name'];
            $tmp['short_name'] = $value['short_name'];
            $tmp['alias'] = $value['alias'];
            $tmp['slug'] = $value['slug'];
            $tmp['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'],200,200);

            if($isDetail){
                //intr_
                if($value['intr_id']) {
                    $tmp['region']['id'] = $value['intr_id'];
                    $tmp['region']['name'] = $value['intr_name'];
                    $tmp['region']['name_cn'] = $value['intr_name_cn'];
                    $tmp['region']['short_name'] = $value['intr_short_name'];
                    $tmp['region']['image'] = $value['intr_image'];
                }else{
                    $tmp['region'] = null;
                }
                $tmp['world_ranking'] = $value['world_ranking'];
                $tmp['total_earnings'] = $value['total_earnings'];
                $tmp['average_player_age'] = $value['average_player_age'];
                // 历史选手
                $hiJson = $value['history_players'];
                $playerIds = [];
                if(!empty($hiJson) && $hiJson!='[]'){
                    $his = json_decode($hiJson,true);
                    foreach ($his as $val){
                        if((int)$val){
                            $playerIds[$val]=(int)$val;
                        }
                    }
                }
                if(!empty($playerIds)) {
                    $playerss = Player::find()->alias('p')
                        ->select(['p.id as player_id', 'p.nick_name', 'p.game_id as game', 'p.player_country as nationality', 'p.name', 'p.name_cn', 'ep.c_name as role', 'p.steam_id', 'p.slug', 'p.image', 'p.modified_at', 'p.created_at', 'p.deleted_at'])
                        ->leftjoin('enum_position AS ep','ep.id = p.role')
                        ->where(['in', 'p.id', $playerIds])->andWhere(['p.deleted'=>2])->asArray()->all();
                    foreach ($playerss as $k => $v) {
                        $playerss[$k]['image'] = ConversionCriteria::playerResizeConversion($v['image']);
                        $playerss[$k]['game'] = EnumGame::find()->alias('eg')
                            ->select(['eg.id as game_id', 'eg.e_name as name', 'eg.name as name_cn', 'eg_rules.name as default_game_rule', 'eg_type.name as default_match_type', 'eg.full_name', 'eg.short_name', 'eg.slug', 'eg.image', 'eg.modified_at', 'eg.created_at', 'eg.deleted_at'])
                            ->leftjoin('enum_game_rules AS eg_rules', 'eg_rules.id = eg.default_game_rule')
                            ->leftjoin('enum_game_match_type AS eg_type', 'eg_type.id = eg.default_match_type')
                            ->where(["eg.id"=>$v['game']])->asArray()->one();
                        if(!empty($playerss[$k]['game'])) {
                            $playerss[$k]['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($playerss[$k]['game']['modified_at']);
                            $playerss[$k]['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($playerss[$k]['game']['created_at']);
                            $playerss[$k]['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($playerss[$k]['game']['deleted_at']);
                        }else{
                            $playerss[$k]['game'] = null;
                        }
                        $playerss[$k]['nationality'] = EnumCountry::find()->select(['id', 'e_name as name', 'name as name_cn', 'two as short_name', 'image'])
                            ->where(["id"=> $v['nationality']])->asArray()->one();
                        if(empty($playerss[$k]['nationality'])){
                            $playerss[$k]['nationality'] = null;
                        }
                    }
                    $tmp['history_players'] = $playerss;
                }else{
                    $tmp['history_players'] = [];
                }
                $tmp['introduction'] = $value['introduction'];
                $tmp['introduction_cn'] = $value['introduction_cn'];
            }

            // 选手
            $a = [];
            foreach ($players as $val)
            {
                if ($val['team_id'] == $value['team_id'])
                {
                    $ab['player_id'] = $val['player_id'];
                    $ab['nick_name'] = $val['p_nick_name'];
                    if ($val['peg_game_id']){

                    $ab['game']['game_id'] = $val['peg_game_id'];
                    $ab['game']['name'] = $val['peg_name'];
                    $ab['game']['name_cn'] = $val['peg_name_cn'];
                    $ab['game']['default_game_rules'] = $val['default_game_rules'];
                    $ab['game']['default_match_type'] = $val['default_match_type'];
                    $ab['game']['full_name'] = $val['peg_full_name'];
                    $ab['game']['short_name'] = $val['peg_short_name'];
                    $ab['game']['slug'] = $val['peg_slug'];
                    $ab['game']['image'] = $val['peg_image'];
                    $ab['game']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['peg_modified_at']);
                    $ab['game']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['peg_created_at']);
                    $ab['game']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['peg_deleted_at']);
                    }else{
                        $ab['game'] = null;
                    }

                    if ($val['pec_id']) {
                    $ab['nationality']['id'] = $val['pec_id'];
                    $ab['nationality']['name'] = $val['pec_name'];
                    $ab['nationality']['name_cn'] = $val['pec_name_cn'];
                    $ab['nationality']['short_name'] = $val['pec_short_name'];
                    $ab['nationality']['image'] = $val['pec_image'];
                    }else{
                        $ab['nationality'] = null;
                    }
                    $ab['name'] = $val['p_name'];
                    $ab['name_cn'] = $val['p_name_cn'];
                    $ab['role'] = $val['p_role'];
                    $ab['steam_id'] = $val['p_steam_id'];
                    $ab['slug'] = $val['p_slug'];
                    $ab['image'] = ConversionCriteria::playerResizeConversion($val['p_image']);
                    $ab['status'] = self::getPlayerState(TeamPlayerRelation::find()->select(['status_id'])
                        ->where(["team_id"=>$value['team_id'],'player_id'=>$val['player_id']])->asArray()->one()['status_id']);
                    $ab['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['pp_modified_at']);
                    $ab['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['p_created_at']);
                    $ab['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['p_deleted_at']);
                    $a[] = $ab;
                }
            }
            $tmp['players'] = $a ?: [];
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['deleted_at']);

//            $isDetail ? $info = $tmp : $info[] = $tmp;
            $info[] = $tmp;
        }

        return $info;
    }
    public static function getPlayerState($state){
        switch ($state){
            case 1:
                return 'Active';
                break;
            case 2:
                return 'Trial';
                break;
            case 3:
                return 'Two-way';
                break;
            case 4:
                return 'Inactive';
                break;
            case 5:
                return 'Loan';
                break;
            case 6:
                return 'Coach';
                break;
            default :
                return null;
                break;
        }
    }
}
