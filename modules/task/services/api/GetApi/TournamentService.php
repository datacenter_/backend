<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\GetApi;

use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\models\TeamPlayerRelationSnasphot;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentPriceDistribute;
use yii\data\Pagination;
use yii;

class TournamentService
{
    public static function getTournamentList($params){
        $q = self::getQuery();
        if(isset($params['game_id']) && !empty($params['game_id'])){
            $q->andWhere(["tt.game"=>$params['game_id']]);
        }
        if(isset($params['status']) && !empty($params['status'])){
            $q->andWhere(["tt.status"=>$params['status']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getTournamentColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        // 处理列表数据
        $tourData = self::getTournamentData($list,true);

        return [
            'count' => $count,
            'list' => $tourData,
        ];
    }
    public static function getTournamentDetail($tournamentId=null)
    {
        if(!$tournamentId){
            return [];
        }
        $q = self::getQuery();
        $list = $q->where(['tt.id'=>$tournamentId])->select(self::getTournamentColumns())->asArray()->all();
        // 处理列表数据
        $tourData = self::getTournamentData($list,true);
        return $tourData;
    }
    private static function getDetailQuery()
    {
        return Tournament::find()->alias('tt')
            ->leftJoin('tournament_base as base', 'tt.id = base.tournament_id')
            ->leftJoin('enum_match_state as sta_name', 'tt.status = sta_name.id');
    }
    private static function getQuery()
    {
        return Tournament::find()->alias('tt')
            ->leftJoin('tournament_base as base', 'tt.id = base.tournament_id')
            ->leftJoin('enum_match_state as sta_name', 'tt.status = sta_name.id');
    }

    private static function getTournamentData($data,$isDetail = false)
    {
        foreach ($data as $k=>$v){
            // 战队参赛条件
            $teams_condition = $v['teams_condition'];
            $teamsConditionArray = [];
            if(!$teams_condition || $teams_condition!='[]'){
                $teamsCondition = json_decode($teams_condition,true);
                foreach ($teamsCondition as $key=>$val){
                    $teamsConditionArray[$val['team_id']]['match_condition'] = $val['match_condition'];
                    $teamsConditionArray[$val['team_id']]['match_condition_cn'] = $val['match_condition_cn'];
                    $teamsConditionArray[$val['team_id']]['team_sort'] = $val['team_sort'];
                }
            }
            unset($data[$k]['teams_condition']);
            $data[$k]['name'] = strval($v['name']);
            $data[$k]['name_cn'] = strval($v['name_cn']);
            // 游戏
            $data[$k]['game'] = self::getGameData($v['game']) ?: null;
            //status
            $data[$k]['status'] = $v['status'];
            // 参赛战队
            $data[$k]['teams'] = self::getTeamsNapshotData($v['tournament_id'], 'tournament',true,$teamsConditionArray,null) ?: [];
            if($isDetail) {
                $data[$k]['number_of_teams'] = $v['number_of_teams'];
                $data[$k]['region'] = self::getCountryData($v['region']) ?: null;
                $data[$k]['country'] = self::getCountryData($v['country']) ?: null;
                $data[$k]['prize_pool'] = [];
                $data[$k]['prize_pool']['prize_bonus'] = $v['prize_bonus'] ?: null;
                $data[$k]['prize_pool']['prize_points'] = $v['prize_points'] ?: null;
                $data[$k]['prize_pool']['prize_seed'] = $v['prize_seed'] ?: null;
                $data[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
                $data[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
                $data[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
                unset($data[$k]['prize_bonus']);
                unset($data[$k]['prize_points']);
                unset($data[$k]['prize_seed']);
                $data[$k]['prize_distribution'] = self::getTournamentPriceData($v['tournament_id']) ?: [];
            }
            $data[$k]['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'], 200, 200) ?? null;
            $data[$k]['scheduled_begin_at'] = ConversionCriteria::DataTimeConversionCriteria($v['scheduled_begin_at']);
            $data[$k]['scheduled_end_at'] = ConversionCriteria::DataTimeConversionCriteria($v['scheduled_end_at']);
        }
        return $data;
    }
    private static $prizesArray = [];
    // 获取奖池分配
    private static function getTournamentPriceData($tournamentId)
    {
        if(isset(self::$prizesArray[$tournamentId])) {
        }else{
            $prizesData = TournamentPriceDistribute::find()->alias('prize')->where(['prize.tournament_id' => $tournamentId])
                ->select(self::getTournamentPriceColumns())->asArray()->all();
            if(!empty($prizesData)){
                self::$prizesArray[$tournamentId] = $prizesData;
            }else{
                return [];
            }
        }
        return self::$prizesArray[$tournamentId];
    }
    private static function getTournamentPriceColumns()
    {
        return [
            'prize.rank as place',
            'prize.price as prize_bonus',
            'prize.score as prize_points',
            'prize.num as prize_seed',
            'prize.team_id as team_id',
//            'prize.team_id as team_snapshot',
        ];
    }
    // 获取战队和快照信息
    private static function getTeamsNapshotData($tournamentId,$type='tournament',$isTrue=false,$teamsCondition,$teamId = null)
    {
        if ($isTrue){
            $where = "snap.relation_id={$tournamentId} and snap.type='{$type}' and rel.tournament_id={$tournamentId}";
        }else if(!$isTrue && $teamId){
            $where = "snap.relation_id={$tournamentId} and snap.type='{$type}' and snap.team_id={$teamId} and rel.tournament_id={$tournamentId}";
        }else{
            return [];
        }
        $field = 'snap.id,snap.team_id,rel.team_sort,rel.match_condition,rel.match_condition_cn,team_i.world_ranking,snap.name,snap.country,snap.full_name,snap.short_name,snap.slug,snap.image,snap.modified_at,snap.created_at,snap.deleted_at';
        $sql = "select {$field} from team_snapshot as snap left join tournament_team_relation as rel ON snap.team_id = rel.team_id and snap.relation_id = rel.tournament_id left join team_introduction_snapshot as team_i ON snap.id=team_i.team_id where {$where} order by rel.team_sort asc";
        $connection  = Yii::$app->db;
        $command = $connection->createCommand($sql);
        $snapTeamData = $command->queryAll();
        $teams = [];
        if(!empty($snapTeamData)) {
            // 查找下面的选手
            $teamIds = array_column($snapTeamData, 'id');
            $players = PlayerSnapshot::find()->alias('ps')
                ->select([
                    'tprs.team_id as tprs_team_id',
                    'ps.player_id',
                    'ps.nick_name',
                    'enum_country.id as c_id',
                    'enum_country.e_name as c_name',
                    'enum_country.name as c_name_cn',
                    'enum_country.two as c_short_name',
                    'enum_country.image as c_image',
                    'ps.name',
                    'ps.name_cn',
                    'enum_position.c_name as role',
                    'ps.slug',
                    'ps.image',
                    'p_state.e_name as status',
                    'ps.modified_at',
                    'ps.created_at',
                    'ps.deleted_at',
                ])
                ->leftJoin('team_player_relation_snapshot as tprs', 'ps.relation_id = tprs.id')
                ->leftJoin('enum_player_state as p_state', 'tprs.status_id = p_state.id')
                ->leftJoin('enum_country', 'ps.player_country = enum_country.id')
                ->leftJoin('enum_position', 'ps.role = enum_position.id')
                ->where(['in', 'tprs.team_id', $teamIds])->andWhere(['ps.deleted'=>2])->asArray()->all();
            if(!empty($players)){
                $snapPlayers = [];
                foreach ($players as $player){
                    $playerInfo['player_id'] = $player['player_id'];
                    $info['nick_name'] = $player['nick_name'];
                    if($player['c_id']) {
                        $info['nationality']['id'] = $player['c_id'];
                        $info['nationality']['name'] = $player['c_name'];
                        $info['nationality']['name_cn'] = $player['c_name_cn'];
                        $info['nationality']['short_name'] = $player['c_short_name'];
                        $info['nationality']['image'] = $player['c_image'];
                    }else{
                        $info['nationality'] = null;
                    }
                    $info['name'] = $player['name'];
                    $info['name_cn'] = $player['name_cn'];
                    $info['role'] = $player['role'];
                    $info['slug'] = $player['slug'];
                    $info['image'] = ConversionCriteria::playerResizeConversion($player['image']);
                    $info['status'] = $player['status'];
                    $info['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($player['modified_at']);
                    $info['created_at'] = ConversionCriteria::DataTimeConversionCriteria($player['created_at']);
                    $info['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($player['deleted_at']);
                    $playerInfo['player_snapshot'] = $info;
                    $snapPlayers[$player['tprs_team_id']][]=$playerInfo;
                }
            }
            foreach ($snapTeamData as $k => $v) {
                if ($isTrue) {
                    $team['team_id'] = $v['team_id'];
                }
                if ($v['id']) {
                    $team['team_snapshot']['name'] = $v['name'];
                    $team['team_snapshot']['country'] = self::getCountryData($v['country']) ?: null;
                    $team['team_snapshot']['full_name'] = $v['full_name'];
                    $team['team_snapshot']['short_name'] = $v['short_name'];
                    $team['team_snapshot']['slug'] = $v['slug'];
                    $team['team_snapshot']['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'], 200, 200) ?? null;
                    $team['team_snapshot']['world_ranking'] = $v['world_ranking'];
                    if(isset($snapPlayers[$v['id']])){
                        $team['team_snapshot']['players'] = $snapPlayers[$v['id']];
                    }else{
                        $team['team_snapshot']['players'] = [];
                    }
                    $team['team_snapshot']['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
                    $team['team_snapshot']['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
                    $team['team_snapshot']['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
                } else {
                    $team['team_snapshot'] = null;
                }
                if (isset($teamsCondition[$v['team_id']])) {
                    $team['qualifier_from'] = $teamsCondition[$v['team_id']]['match_condition'];
                    $team['qualifier_from_cn'] = $teamsCondition[$v['team_id']]['match_condition_cn'];
                    $team['order'] = $teamsCondition[$v['team_id']]['team_sort'];
                } else {
                    $team['qualifier_from'] = null;
                    $team['qualifier_from_cn'] = null;
                    $team['order'] = null;
                }
                $teams[] = $team;
            }
        }
        //        print_r(get_last_sql());
        return $teams;
    }

    // 游戏数组
    private static $gameArray = [];
    // 获取游戏数据
    private static function getGameData($gameId)
    {
        if(isset(self::$gameArray[$gameId])){
        }else {
            $game = EnumGame::find()->alias('game')
                ->leftJoin('enum_game_rules as rules', 'game.default_game_rule = rules.id')
                ->leftJoin('enum_game_match_type as type', 'game.default_match_type = type.id')
                ->where(["game.id" => $gameId])->select(self::getGameColumns())->asArray()->one();
            if (!empty($game)) {
                $game['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($game['modified_at']);
                $game['created_at'] = ConversionCriteria::DataTimeConversionCriteria($game['created_at']);
                $game['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($game['deleted_at']);
            }
            self::$gameArray[$gameId] = $game;
        }
        return self::$gameArray[$gameId];
    }

    private static $countryArray = [];
    // 获取国家地区数据
    private static function getCountryData($countryId)
    {
        if(isset(self::$countryArray[$countryId])){
        }else {
            $country = EnumCountry::find()->alias('country')->where(['country.id' => $countryId])->select(self::getCountryColumns())->asArray()->one();
            self::$countryArray[$countryId] = $country;
        }
        return self::$countryArray[$countryId];
    }

    // 国家字段
    public static function getCountryColumns()
    {
        return [
            'country.id as country_id',
            'country.e_name as name',
            'country.name as name_cn',
            'country.two as short_name',
            'country.image as image',
        ];
    }

    // 赛事字段
    public static function getTournamentColumns()
    {
        return [
            'tt.id as tournament_id',
            'tt.deleted as deleted',
            'tt.name as name',
            'tt.name_cn as name_cn',
            'tt.game as game',
            'sta_name.c_name as status',
            'tt.scheduled_begin_at as scheduled_begin_at',
            'tt.scheduled_end_at as scheduled_end_at',
            'tt.short_name as short_name',
            'tt.short_name_cn as short_name_cn',
            'tt.slug as slug',
            'tt.image as image',
            'base.tier as tier',
            'base.number_of_teams as number_of_teams',
            'base.region as region',
            'base.country as country',
            'base.location as location',
            'base.location_cn as location_cn',
            'base.organizer as organizer',
            'base.organizer_cn as organizer_cn',
            'base.version as version',
            'tt.game as prize_pool',
            'base.prize_bonus as prize_bonus',
            'base.prize_points as prize_points',
            'base.prize_seed as prize_seed',
            'base.map_pool as map_pool',
            'base.format as format',
            'base.format_cn as format_cn',
            'base.introduction as introduction',
            'base.introduction_cn as introduction_cn',
            'tt.game as prize_distribution',
            'tt.game as teams',
            'tt.modified_at as modified_at',
            'tt.created_at as created_at',
            'tt.deleted_at as deleted_at',
            'tt.teams_condition as teams_condition',
        ];
    }
    // 游戏字段
    private static function getGameColumns()
    {
        return [
            'game.id as game_id',
            'game.e_name as name',
            'game.name as name_cn',
//            'game.default_game_rule as rules_id',
//            'game.default_match_type as type_id',
            'rules.name as default_game_rules',
            'type.name as default_match_type',
            'game.full_name as full_name',
            'game.short_name as short_name',
            'game.slug as slug',
            'game.image as image',
            'game.modified_at as modified_at',
            'game.created_at as created_at',
            'game.deleted_at as deleted_at',
        ];
    }
}
