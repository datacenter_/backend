<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\Metadata;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use yii\data\Pagination;

class MetadataCsgoService
{
    public static function getMapsList($params)
    {
        $q = self::getMapQuery();
//        $where = self::getWhere($params);
//        $q = $q->where($where);
        if(isset($params['name'])){
            $q->where(['like',"map.name",$params['name']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getMapColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        return [
            'count'=>$count,
            'list'=>self::getMapsData($list),
        ];
    }

    public static function getMapsDetail($map_id)
    {
        $q = self::getMapQuery();
        $q = $q->where(['id' => $map_id]);
        $list = $q->select(self::getMapColumns())->asArray()->all();
        return self::getMapsData($list);
    }

    // 处理map结构
    private static function getMapsData($list)
    {
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            if($v['map_type'] == '1' && $v['map_type_cn'] == '1'){
                $list[$k]['map_type'] = 'Bomb defusal';
                $list[$k]['map_type_cn'] = '炸弹拆除';
            }
            if($v['map_type'] == '2' && $v['map_type_cn'] == '2'){
                $list[$k]['map_type'] = 'Hostage rescue';
                $list[$k]['map_type_cn'] = '人质解救';
            }
            $list[$k]['is_default'] = $v['is_default']==1 ? true:false;
            $list[$k]['image']['square_image'] = ImageConversionHelper::showFixedSizeConversion($v['square_image'],252,126) ?: null;
            $list[$k]['image']['rectangle_image'] = ImageConversionHelper::showFixedSizeConversion($v['rectangle_image'],675,84) ?: null;
            $list[$k]['image']['thumbnail'] = ImageConversionHelper::showFixedSizeConversion($v['thumbnail'],1380,930) ?: null;

            $list[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at1']);
            $list[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at1']);
            $list[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at1']);
            unset($list[$k]['square_image']);
            unset($list[$k]['rectangle_image']);
            unset($list[$k]['thumbnail']);
            unset($list[$k]['modified_at1']);
            unset($list[$k]['created_at1']);
            unset($list[$k]['deleted_at1']);
        }
        return $list;
    }

    private static function getMapQuery()
    {
        return MetadataCsgoMap::find()->alias('map');
    }

    private static function getMapColumns()
    {
        return [
            'map.id as map_id',
            'map.deleted as deleted',
            'map.name as name',
            'map.name_cn as name_cn',
            'map.state as status',
            'map.external_id as external_id',
            'map.external_name as external_name',
            'map.short_name as short_name',
            'map.map_type as map_type',
            'map.map_type_cn as map_type_cn',
            'map.is_default as is_default',
            'map.slug as slug',
            'map.square_image as square_image',
            'map.rectangle_image as rectangle_image',
            'map.thumbnail as thumbnail',
            'map.modified_at as modified_at1',
            'map.created_at as created_at1',
            'map.deleted_at as deleted_at1',
        ];
    }

    public static function getWeaponList($params)
    {
        $q = self::getWeaponQuery();
//        $where = self::getWhere($params);
//        $q = $q->where($where);
        if(isset($params['name'])){
            $q->where(['like',"weapon.name",$params['name']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getWeaponColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        foreach ($list as $k=>$v){
            $list[$k]['kind'] = self::getWeaponKind($v['kind']);
            $list[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            $list[$k]['cost'] = $v['cost']=='' ? null:intval($v['cost']);
            $list[$k]['kill_award'] = $v['kill_award']=='' ? null:intval($v['kill_award']);
            $list[$k]['is_ct_available'] = ConversionCriteria::issetTrueFalseType($list[$k]['is_ct_available']);
            $list[$k]['is_t_available'] = ConversionCriteria::issetTrueFalseType($list[$k]['is_t_available']);
            $list[$k]['image'] = ImageConversionHelper::showMfitSizeConversion($v['image'],28,0);
            $list[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $list[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $list[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return [
            'count'=>$count,
            'list'=>$list,
        ];
    }
    private static function getWeaponKind($kind){
        switch ($kind){
            case '1':
                return 'primary';
                break;
            case '2':
                return 'secondary';
                break;
            case '3':
                return 'knife';
                break;
            case '4':
                return 'grenade';
                break;
            case '5':
                return 'gear';
                break;
            default:
                return null;
        }
    }
    public static function getWeaponDetail($weapon_id)
    {
        $q = self::getWeaponQuery();
        $q = $q->where(['weapon.id' => $weapon_id]);
        $list = $q->select(self::getWeaponColumns())->asArray()->one();
        $weapon = [];
        if(!empty($list)) {
            $list['kind'] = self::getWeaponKind($list['kind']);
            $list['status'] = $list['status'] == 1 ? 'normal' : 'removed';
            $list['cost'] = $list['cost'] == '' ? null : intval($list['cost']);
            $list['kill_award'] = $list['kill_award'] == '' ? null : intval($list['kill_award']);
            $list['is_ct_available'] = ConversionCriteria::issetTrueFalseType($list['is_ct_available']);
            $list['is_t_available'] = ConversionCriteria::issetTrueFalseType($list['is_t_available']);
            $list['image'] = ImageConversionHelper::showMfitSizeConversion($list['image'],28,0);
            $list['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($list['modified_at']);
            $list['created_at'] = ConversionCriteria::DataTimeConversionCriteria($list['created_at']);
            $list['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($list['deleted_at']);
            $weapon[] = $list;
        }
        return $weapon;
    }

    private static function getWeaponQuery()
    {
        return MetadataCsgoWeapon::find()->alias('weapon');
    }

    private static function getWeaponColumns()
    {
        return [
            'weapon.id as weapon_id',
            'weapon.deleted as deleted',
            'weapon.name as name',
            'weapon.kind as kind',
            'weapon.state as status',
            'weapon.external_id as external_id',
            'weapon.external_name as external_name',
            'weapon.cost as cost',
            'weapon.kill_award as kill_award',
            'weapon.movement_speed as movement_speed',
            'weapon.ammunition as ammunition',
            'weapon.capacity as capacity',
            'weapon.reload_time as reload_time',
            'weapon.is_ct_available as is_ct_available',
            'weapon.is_t_available as is_t_available',
            'weapon.firing_mode as firing_mode',
            'weapon.slug as slug',
            'weapon.image as image',
            'weapon.modified_at as modified_at',
            'weapon.created_at as created_at',
            'weapon.deleted_at as deleted_at',
        ];
    }

    private static function getWhere($params)
    {
        return ['flag'=>1];
    }
}