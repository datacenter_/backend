<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\Metadata;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use yii\data\Pagination;

class MetadataDotaService
{
    public static function getHeroList($params)
    {
        $q=self::getQuery();
//        $where = self::getWhere($params);
//        $q=$q->where($where);
        if(isset($params['name'])){
            $q->where(['like',"mdh.name",$params['name']]);
        }
        $count=$q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $HeroListList = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        $abilityList = self::getAbility($HeroListList);
//        $talentList = self::getTalent($HeroListList);
        return [
            'count'=>$count,
            'list'=>self::getFormat($HeroListList,$abilityList),
        ];
    }

    public static function getItemsList($params)
    {
        $q=MetadataDota2Item::find();
        $where = self::getWhere($params);
        $q=$q->where($where);
        if(isset($params['name'])){
            $q->andWhere(['like',"name",$params['name']]);
        }
        $count=$q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $itemList = $q->select(['id as item_id','deleted','name','name_cn','state as status','external_id','external_name','total_cost','is_recipe','is_secret_shop','is_home_shop','is_neutral_drop','slug','image','description','description_cn','modified_at','created_at','deleted_at'])
            ->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        foreach ($itemList as $k=>$v){
            $itemList[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            $itemList[$k]['is_recipe'] = ConversionCriteria::issetTrueFalseType($itemList[$k]['is_recipe']);
            $itemList[$k]['is_secret_shop'] = ConversionCriteria::issetTrueFalseType($itemList[$k]['is_secret_shop']);
            $itemList[$k]['is_home_shop'] = ConversionCriteria::issetTrueFalseType($itemList[$k]['is_home_shop']);
            $itemList[$k]['is_neutral_drop'] = ConversionCriteria::issetTrueFalseType($itemList[$k]['is_neutral_drop']);
            $itemList[$k]['total_cost'] = $v['total_cost']=='' ? null:intval($v['total_cost']);
            $itemList[$k]['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'],88,64);
            $itemList[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $itemList[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $itemList[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return [
            'count'=>$count,
            'list'=>$itemList,
        ];
    }
    public static function getItemsDetail($item_id)
    {
        $q=MetadataDota2Item::find();
        $q=$q->where(['id' => $item_id]);
        $ItemsList=$q->select(
            ['id as item_id','deleted','name','name_cn','state as status','external_id','external_name','total_cost','is_recipe','is_secret_shop','is_home_shop','is_neutral_drop','slug','image','description','description_cn','modified_at','created_at','deleted_at']
        )->asArray()->one();
        $itemInfo = [];
        if(!empty($ItemsList)) {
            $ItemsList['status'] = $ItemsList['status']==1 ? 'normal':'removed';
            $ItemsList['is_recipe'] = ConversionCriteria::issetTrueFalseType($ItemsList['is_recipe']);
            $ItemsList['is_secret_shop'] = ConversionCriteria::issetTrueFalseType($ItemsList['is_secret_shop']);
            $ItemsList['is_home_shop'] = ConversionCriteria::issetTrueFalseType($ItemsList['is_home_shop']);
            $ItemsList['is_neutral_drop'] = ConversionCriteria::issetTrueFalseType($ItemsList['is_neutral_drop']);

            $ItemsList['total_cost'] = $ItemsList['total_cost'] == '' ? null : intval($ItemsList['total_cost']);
            $ItemsList['image'] = ImageConversionHelper::showFixedSizeConversion($ItemsList['image'], 64, 88);
            $ItemsList['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($ItemsList['modified_at']);
            $ItemsList['created_at'] = ConversionCriteria::DataTimeConversionCriteria($ItemsList['created_at']);
            $ItemsList['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($ItemsList['deleted_at']);
            $itemInfo[] = $ItemsList;
        }
        return $itemInfo;
    }

    public static function getTalent($HeroListList)
    {
        $talent = MetadataDota2Talent::find()->alias('mdt')->where(['in', "mdt.hero_id", array_column($HeroListList,'hero_id')]);
        $talentList=$talent->select([
            'mdt.id as talent_id',
            'mdt.hero_id as hero_id',
            'mdt.level as mdt_level',
            'mdt.left_right as mdt_left_right',
            'mdt.talent as mdt_talent',
            'mdt.talent_cn as mdt_talent_cn',
            'mdt.slug as mdt_slug',
            'mdt.modified_at as mdt_modified_at',
            'mdt.created_at as mdt_created_at',
            'mdt.deleted_at as mdt_deleted_at',

        ])->asArray()->all();
        return $talentList;
    }

    public static function getAbility($HeroListList)
    {
        $ability = self::getAbilityQuery(array_column($HeroListList,'hero_id'));
        $abilityList=$ability->select([
            'mda.id as ability_id',
            'mda.hero_id as hero_id',
            'mda.name as mda_name',
            'mda.name_cn as mda_name_cn',
            'mda.hotkey as mda_hotkey',
            'mda.slug as mda_slug',
            'mda.image as mda_image',
            'mda.description as mda_description',
            'mda.description_cn as mda_description_cn',
            'mda.modified_at as mda_modified_at',
            'mda.created_at as mda_created_at',
            'mda.deleted_at as mda_deleted_at',
            'mda.external_id as external_id',
            'mda.external_name as external_name'
        ])->where(['deleted' => 2])->asArray()->all();
        return $abilityList;
    }
    public static function getHeroDetail($hero_id)
    {
        $q=self::getQuery();
        $q=$q->where(['mdh.id' => $hero_id]);
        $HeroListList=$q->select(self::getColumns())->asArray()->all();
        $abilityList = self::getAbility($HeroListList);
//        $talentList = self::getTalent($HeroListList);
        return self::getFormat($HeroListList,$abilityList,true);
    }
    private static function getAbilityQuery($heroIds)
    {
        return MetadataDota2Ability::find()->alias('mda')->where(['in', "mda.hero_id", $heroIds])->andWhere(['deleted'=>2]);
    }
    private static function getQuery()
    {
        return MetadataDota2Hero::find()->alias('mdh');
    }
    private static function getWhere($params)
    {
        return ['flag'=>1];
    }

    private static function getColumns()
    {
        return [
            'mdh.id as hero_id',
            'mdh.deleted as deleted',
            'mdh.name as name',
            'mdh.name_cn as name_cn',
            'mdh.state as status',
            'mdh.external_id as external_id',
            'mdh.external_name as external_name',
            'mdh.title as title',
            'mdh.title_cn as title_cn',
            'mdh.primary_attr as primary_attr',
            'mdh.primary_attr_cn as primary_attr_cn',
            'mdh.roles as roles',
            'mdh.roles_cn as roles_cn',
            'mdh.slug as slug',
            'mdh.image as image',
            'mdh.small_image as small_image',
            'mdh.modified_at as modified_at',
            'mdh.created_at as created_at',
            'mdh.deleted_at as deleted_at',
            'mdh.talents as talents',

        ];
    }

    private static function getFormat($HeroListList,$abilityList,$isDetail = false)
    {
        $info = [];
        foreach ($HeroListList as $key=>&$value)
        {
            $tmp['hero_id'] = $value['hero_id'];
            $tmp['deleted'] = $value['deleted'];
            $tmp['name'] = $value['name'];
            $tmp['name_cn'] = $value['name_cn'];
            $tmp['status'] = $value['status'] == 1 ? 'normal' : 'removed';
            $tmp['external_id'] = $value['external_id'];
            $tmp['external_name'] = $value['external_name'];
            $tmp['title'] = $value['title'];
            $tmp['title_cn'] = $value['title_cn'];
            $tmp['primary_attr'] = $value['primary_attr'];
            $tmp['primary_attr_cn'] = $value['primary_attr_cn'];
            $tmp['roles'] = $value['roles'];
            $tmp['roles_cn'] = $value['roles_cn'];
            $tmp['slug'] = $value['slug'];
            $tmp['image']['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'],144,256) ?: null;
            $tmp['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($value['small_image'],32,32) ?: null ;

            $a = [];
            foreach ($abilityList as $val)
            {
                if ($val['hero_id'] == $value['hero_id']) {
                    $ab['ability_id'] = $val['ability_id'] ?? '';
                    $ab['name'] = $val['mda_name'] ?? '';
                    $ab['name_cn'] = $val['mda_name_cn'] ?? '';
                    $ab['hotkey'] = $val['mda_hotkey'] ?? '';
                    $ab['external_id'] = $val['external_id'] ?? '';
                    $ab['external_name'] = $val['external_name'] ?? '';
                    $ab['slug'] = $val['mda_slug'] ?? '';
                    $ab['image'] = ImageConversionHelper::showFixedSizeConversion($val['mda_image'],50,50) ?? '';
                    $ab['description'] = $val['mda_description'] ?? '';
                    $ab['description_cn'] = $val['mda_description_cn'] ?? '';
                    $ab['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mda_modified_at']);
                    $ab['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mda_created_at']);
                    $ab['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mda_deleted_at']);
                    $a[] = $ab;
                }
            }
            $tmp['abilities'] = $a ?: [];

            $talentsArray = array();
            $talents = json_decode($value['talents'],true);
            // 天赋处理
            foreach ((array)$talents as $k=>$v){
                if(!empty($v['left'])){
                    $left = MetadataDota2Talent::find()->select(['id as talent_id','external_id','external_name','talent','talent_cn','slug','modified_at','created_at','deleted_at'])
                        ->where(['id'=>(int)$v['left']])->andWhere(['deleted'=>2])->asArray()->one();
                    if(!empty($left)) {
                        $leftData['talent_id'] = $left['talent_id'];
                        $leftData['external_id'] = $left['external_id'];
                        $leftData['external_name'] = $left['external_name'];
                        $leftData['level'] = $v['id'];
                        $leftData['left_right'] = 'left';
                        $leftData['talent'] = $left['talent'];
                        $leftData['talent_cn'] = $left['talent_cn'];
                        $leftData['slug'] = $left['slug'];
                        $leftData['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($left['modified_at']);
                        $leftData['created_at'] = ConversionCriteria::DataTimeConversionCriteria($left['created_at']);
                        $leftData['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($left['deleted_at']);
                        array_push($talentsArray, $leftData);
                    }
                }
                if(!empty($v['right'])){
                    $right = MetadataDota2Talent::find()->select(['id as talent_id','external_id','external_name','talent','talent_cn','slug','modified_at','created_at','deleted_at'])
                        ->where(['id'=>(int)$v['right']])->andWhere(['deleted'=>2])->asArray()->one();
                    if(!empty($right)) {
                        $rightData['talent_id'] = $right['talent_id'];
                        $rightData['external_id'] = $right['external_id'];
                        $rightData['external_name'] = $right['external_name'];
                        $rightData['level'] = $v['id'];
                        $rightData['left_right'] = 'right';
                        $rightData['talent'] = $right['talent'];
                        $rightData['talent_cn'] = $right['talent_cn'];
                        $rightData['slug'] = $right['slug'];
                        $rightData['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($right['modified_at']);
                        $rightData['created_at'] = ConversionCriteria::DataTimeConversionCriteria($right['created_at']);
                        $rightData['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($right['deleted_at']);
                        array_push($talentsArray, $rightData);
                    }
                }
            }
            $tmp['talents'] = $talentsArray ?: [];
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['deleted_at']);
//            $tmp['talents'] = $value['talents'];
            $info[] = $tmp;
//            $isDetail ? $info = $tmp : $info[] = $tmp;
        }

        return $info;
    }

}
