<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\Metadata;

use app\modules\common\services\ConversionCriteria;
use app\modules\common\services\ImageConversionHelper;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use yii\data\Pagination;

class MetadataLolService
{
    public static function getChampionsList($params)
    {
        $params['page'] = empty($params['page']) ? 1 : $params['page'];
        $params['per_page'] = empty($params['per_page']) ? 10 : $params['per_page'];
        $q = self::getQuery();
//        $q = $q->where(self::getWhere($params));
        if(isset($params['name'])){
            $q->where(['like',"mlc.name",$params['name']]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $HeroListList = $q->select(self::getColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        $abilityList = self::getAbility($HeroListList);
        return [
            'count'=>$count,
            'list'=>self::getFormat($HeroListList,$abilityList),
        ];

    }
    public static function getAbility($HeroListList)
    {
        $ability = self::getAbilityQuery(array_column($HeroListList,'champion_id'));
        $abilityList=$ability->select([
            'mla.champion_id as champion_id',
            'mla.modified_at as mla_modified_at',
            'mla.created_at as mla_created_at',
            'mla.deleted_at as mla_deleted_at',
            'mla.id as ability_id',
            'mla.name as mla_name',
            'mla.name_cn as mla_name_cn',
            'mla.hotkey as mla_hotkey',
            'mla.slug as mla_slug',
            'mla.image as mla_image',
            'mla.description as mla_description',
            'mla.description_cn as mla_description_cn',
            'mla.external_id as external_id',
            'mla.external_name as external_name',
            'mla.deleted_at as deleted_at',
        ])->asArray()->all();
        return $abilityList;
    }
    private static function getAbilityQuery($heroIds)
    {
        return MetadataLolAbility::find()->alias('mla')->where(['in', "mla.champion_id", $heroIds])->andWhere(['deleted'=>2]);
    }
    public static function getChampionsDetail($champinonCreId)
    {
        $q=self::getQuery();
        $q=$q->where(['mlc.id' => $champinonCreId]);
        $HeroListList=$q->select(self::getColumns())->asArray()->all();
        $abilityList = self::getAbility($HeroListList);
        return self::getFormat($HeroListList,$abilityList,true);
    }

    public static function getItemList($params)
    {
        $q=self::getItemQuery();
//        $where = self::getItemWhere($params);
//        $q=$q->where(['deleted'=>2]);
        if(isset($params['name'])){
            $q->where(['like',"mli.name",$params['name']]);
        }
        $count=$q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getItemColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            $list[$k]['is_trinket'] = ConversionCriteria::issetTrueFalseType($v['is_trinket']);
            $list[$k]['is_purchasable'] = ConversionCriteria::issetTrueFalseType($v['is_purchasable']);
            $list[$k]['total_cost'] = $v['total_cost']=='' ? null:intval($v['total_cost']);
            $list[$k]['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'],50,50);
            $list[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $list[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $list[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return [
            'count'=>$count,
            'list'=>$list,
        ];
    }
    public static function getItemDetail($itemCreId)
    {
        $q=self::getItemQuery();
        $q=$q->where(['id' => $itemCreId]);
        $list=$q->select(self::getItemColumns())->asArray()->one();
        $info = [];
        if(!empty($list)) {
            $list['status'] = $list['status'] == 1 ? 'normal' : 'removed';
            $list['is_trinket'] = ConversionCriteria::issetTrueFalseType($list['is_trinket']);
            $list['is_purchasable'] = ConversionCriteria::issetTrueFalseType($list['is_purchasable']);
            $list['total_cost'] = $list['total_cost'] == '' ? null : intval($list['total_cost']);
            $list['image'] = ImageConversionHelper::showFixedSizeConversion($list['image'], 50, 50);
            $list['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($list['modified_at']);
            $list['created_at'] = ConversionCriteria::DataTimeConversionCriteria($list['created_at']);
            $list['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($list['deleted_at']);
            $info[] = $list;
        }
        return $info;
    }

    public static function getSummonerspellsList($params)
    {
        $q=self::getSummonerspellsQuery();
//        $where = self::getSummonerspellsWhere($params);
//        $q=$q->where(['deleted'=>2]);
        if(isset($params['name'])){
            $q->where(['like',"mls.name",$params['name']]);
        }
        $count=$q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getSummonerspellsColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            $list[$k]['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'],50,50);
            $list[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $list[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $list[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return [
            'count'=>$count,
            'list'=>$list,
        ];
    }
    public static function getSummonerspellsDetail($summonerspellCreId)
    {
        $q=self::getSummonerspellsQuery();
//        $where = self::getSummonerspellsWhere($summonerspellCreId);
        $q=$q->where(['id' => $summonerspellCreId]);
        $list=$q->select(self::getSummonerspellsColumns())->asArray()->one();
        $info = [];
        if(!empty($list)) {
            $list['status'] = $list['status'] == 1 ? 'normal' : 'removed';
            $list['image'] = ImageConversionHelper::showFixedSizeConversion($list['image'], 50, 50);
            $list['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($list['modified_at']);
            $list['created_at'] = ConversionCriteria::DataTimeConversionCriteria($list['created_at']);
            $list['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($list['deleted_at']);
            $info[] = $list;
        }
        return $info;
    }

    public static function getRunesList($params)
    {
        $q=self::getRunesQuery();
//        $where = self::getRunesWhere($params);
//        $q=$q->where(['deleted'=>2]);
        if(isset($params['name'])){
            $q->where(['like',"mlr.name",$params['name']]);
        }
        $count=$q->count();
        $page = (empty($params['page']) ? 1 : $params['page'])-1;
        $pageSize = empty($params['per_page']) ? 10 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $list = $q->select(self::getRunesColumns())->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v['status']==1 ? 'normal':'removed';
            $list[$k]['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'],50,50);
            $list[$k]['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($v['modified_at']);
            $list[$k]['created_at'] = ConversionCriteria::DataTimeConversionCriteria($v['created_at']);
            $list[$k]['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($v['deleted_at']);
        }
        return [
            'count'=>$count,
            'list'=>$list,
        ];
    }
    public static function getRunesDetail($runeCreId)
    {
        $q=self::getRunesQuery();
//        $where = self::getRunesWhere($params);
        $q=$q->where(['id' => $runeCreId]);
        $list=$q->select(self::getRunesColumns())->asArray()->one();
        $info = [];
        if(!empty($list)) {
            $list['status'] = $list['status'] == 1 ? 'normal' : 'removed';
            $list['image'] = ImageConversionHelper::showFixedSizeConversion($list['image'], 50, 50);
            $list['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($list['modified_at']);
            $list['created_at'] = ConversionCriteria::DataTimeConversionCriteria($list['created_at']);
            $list['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($list['deleted_at']);
            $info[] = $list;
        }
        return $info;
    }
    private static function getRunesColumns()
    {
        return [
            'mlr.id as rune_id',
            'mlr.deleted as deleted',
            'mlr.name as name',
            'mlr.name_cn as name_cn',
            'mlr.state as status',
            'mlr.external_id as external_id',
            'mlr.external_name as external_name',
            'mlr.path_name as path_name',
            'mlr.path_name_cn as path_name_cn',
            'mlr.slug as slug',
            'mlr.image as image',
            'mlr.description as description',
            'mlr.description_cn as description_cn',
            'mlr.modified_at as modified_at',
            'mlr.created_at as created_at',
            'mlr.deleted_at as deleted_at',
        ];
    }
    private static function getRunesQuery()
    {
        return MetadataLolRune::find()->alias('mlr');
    }
    private static function getRunesWhere($params)
    {
        if (isset($params['rune_id']))
        {
            $where = ['id' => $params['rune_id']];
        }else{
            $where = ['deleted'=>2];
        }
        return $where;
    }

    private static function getSummonerspellsWhere($summonerspellCreId)
    {
        if ($summonerspellCreId)
        {
            $where = ['id' => $summonerspellCreId];
        }else{
            $where = ['deleted'=>2];
        }
        return $where;
    }
    private static function getItemWhere($itemCreId)
    {
        if ($itemCreId)
        {
            $where = ['id' => $itemCreId];
        }else{
            $where = ['deleted'=>2];
        }
        return $where;
    }
    private static function getWhere($params)
    {
        return ['mlc.deleted'=>2];
    }
    private static function getColumns()
    {
        return [
            'mlc.id as champion_id',
            'mlc.deleted as deleted',
            'mlc.name as name',
            'mlc.name_cn as name_cn',
            'mlc.slug as slug',
            'mlc.state as status',
            'mlc.external_id as external_id',
            'mlc.external_name as external_name',
            'mlc.title as title',
            'mlc.title_cn as title_cn',
            'mlc.image as image',
            'mlc.small_image as small_image',
            'mlc.primary_role as primary_role',
            'mlc.primary_role_cn as primary_role_cn',
            'mlc.secondary_role as secondary_role',
            'mlc.secondary_role_cn as secondary_role_cn',
            'mlc.modified_at',
            'mlc.deleted_at',
            'mlc.created_at',


        ];
    }
    private static function getItemColumns()
    {
        return [
            'mli.id as item_id',
            'mli.deleted as deleted',
            'mli.name as name',
            'mli.name_cn as name_cn',
            'mli.state as status',
            'mli.external_id as external_id',
            'mli.external_name as external_name',
            'mli.total_cost as total_cost',
            'mli.is_trinket as is_trinket',
            'mli.is_purchasable as is_purchasable',
            'mli.slug as slug',
            'mli.image as image',
            'mli.description as description',
            'mli.description_cn as description_cn',
            'mli.modified_at as modified_at',
            'mli.created_at as created_at',
            'mli.deleted_at as deleted_at',
        ];
    }
    private static function getSummonerspellsColumns()
    {
        return [
            'mls.id as summoner_spell_id',
            'mls.deleted as deleted',
            'mls.name as name',
            'mls.name_cn as name_cn',
            'mls.state as status',
            'mls.external_id as external_id',
            'mls.external_name as external_name',
            'mls.slug as slug',
            'mls.image as image',
            'mls.description as description',
            'mls.description_cn as description_cn',
            'mls.modified_at as modified_at',
            'mls.created_at as created_at',
            'mls.deleted_at as deleted_at',
        ];
    }
    private static function getSummonerspellsQuery()
    {
        return MetadataLolSummonerSpell::find()->alias('mls');
    }

    private static function getItemQuery()
    {
        return MetadataLolItem::find()->alias('mli');
    }

    private static function getQuery()
    {
        return MetadataLolChampion::find()->alias('mlc');
    }

    private static function getFormat($HeroListList,$abilityList,$isDetail = false)
    {
        $info = [];
        foreach ($HeroListList as &$value)
        {
            $tmp['champion_id'] = $value['champion_id'];
            $tmp['deleted'] = $value['deleted'];
            $tmp['name'] = $value['name'];
            $tmp['name_cn'] = $value['name_cn'];
            $tmp['status'] = $value['status']==1 ? 'normal':'removed';
            $tmp['external_id'] = $value['external_id'];
            $tmp['external_name'] = $value['external_name'];
            $tmp['title'] = $value['title'];
            $tmp['title_cn'] = $value['title_cn'];
            $tmp['primary_role'] = $value['primary_role'];
            $tmp['primary_role_cn'] = $value['primary_role_cn'];
            $tmp['secondary_role'] = $value['secondary_role'];
            $tmp['secondary_role_cn'] = $value['secondary_role_cn'];
            $tmp['slug'] = $value['slug'];
            $tmp['image']['image'] = ImageConversionHelper::showFixedSizeConversion($value['image'],120,120) ?: null;
            $tmp['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($value['small_image'],32,32) ?: null;
            // 技能
            $a = [];
            foreach ($abilityList as $val)
            {
                if ($val['champion_id'] == $value['champion_id'])
                {
                    $ab['ability_id'] = $val['ability_id'];
                    $ab['name'] = $val['mla_name'];
                    $ab['name_cn'] =$val['mla_name_cn'];
                    $ab['hotkey'] = $val['mla_hotkey'];
                    $ab['external_id'] = $val['external_id'];
                    $ab['external_name'] = $val['external_name'];
                    $ab['slug'] = $val['mla_slug'];
                    $ab['image'] = ImageConversionHelper::showFixedSizeConversion($val['mla_image'],50,50);
                    $ab['description'] = $val['mla_description'];
                    $ab['description_cn'] = $val['mla_description_cn'];
                    $ab['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mla_modified_at']);
                    $ab['created_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mla_created_at']);
                    $ab['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($val['mla_deleted_at']);
                    $a[] = $ab;
                }
            }
            $tmp['abilities'] = $a ?: [];
            $tmp['modified_at'] = ConversionCriteria::DataTimeConversionCriteria($value['modified_at']);
            $tmp['created_at'] = ConversionCriteria::DataTimeConversionCriteria($value['created_at']);
            $tmp['deleted_at'] = ConversionCriteria::DataTimeConversionCriteria($value['deleted_at']);
//            $isDetail ? $info = $tmp : $info[] = $tmp;
            $info[] = $tmp;
        }

        return $info;
    }
}