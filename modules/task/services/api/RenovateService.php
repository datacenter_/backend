<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/07
 * Time: 00:52
 */
namespace app\modules\task\services\api;

use app\modules\task\services\api\GetApi\MatchService;
use app\modules\task\services\api\CreateEsIndex;
use app\modules\task\services\api\SetApi;
use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class RenovateService
{
    const INTERFACE_TYPE_GAME = 'game';
    const INTERFACE_TYPE_TEAM = 'team';
    const INTERFACE_TYPE_PLAYER = 'player';
    const INTERFACE_TYPE_TOURNAMENT = 'tournament';
    const INTERFACE_TYPE_MATCH_TOURNAMENT = 'match_tournament'; // 刷新match下赛事
    const INTERFACE_TYPE_CSGO_TOURNAMENT = 'csgo_tournament'; // 5e赛事
    const INTERFACE_TYPE_MATCH = 'match';
    const INTERFACE_TYPE_BATTLE = 'battle'; // 刷新当前比赛下所有battle
    const INTERFACE_TYPE_BATTLE_SINGLE = 'battle_single'; // 单个battle
    const INTERFACE_TYPE_CSGO_BATTLE = 'csgo_battle';   // 5e比赛battle
    const INTERFACE_TYPE_ORGANIZATION = 'organization';
    const INTERFACE_TYPE_LOL_CHAMPION = 'lol_champion';
    const INTERFACE_TYPE_LOL_ITEM = 'lol_item';
    const INTERFACE_TYPE_LOL_SUMMONERSPELL = 'lol_summonerspell';
    const INTERFACE_TYPE_LOL_RUNE = 'lol_rune';
    const INTERFACE_TYPE_DOTA_HERO = 'dota_hero';
    const INTERFACE_TYPE_DOTA_ITEM = 'dota_item';
    const INTERFACE_TYPE_CSGO_MAP = 'csgo_map';
    const INTERFACE_TYPE_CSGO_WEAPON = 'csgo_weapon';

    public static function addDataSync($apiType=null,$apiId=null,$gameId=null)
    {
        $apiTaskInfo = [
            "tag" => QueueServer::getTag(
                QueueServer::QUEUE_MAJOR_API_DATA_SYNC,
                "",
                $apiType,
                "", $apiId, $gameId),
            "type" => "",
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(
                Consts::EXECUTE_TYPE_CTB,
                "",
                $apiType."_GaoYu"
            ),
            "params" => [
                'api_type' => $apiType,
                'api_id' => $apiId,
                'game_id' => $gameId,
            ],
        ];
        TaskRunner::addTask($apiTaskInfo, 6);
    }
    /**
     * ALL       $apiType [] / String && $apiId - Int
     * setMatch   $apiType - String  && $apiId - Int / []
     * setTeam    $apiType - String  && $apiId - Int / []
     * setPlayer  $apiType - String  && $apiId - Int / []
     * $gameId - Int
     */
    public static function refurbishApi($apiType=null,$apiId=null,$gameId=null,$isAddSyncTask=true)
    {
        if (is_array($apiType) && !is_array($apiId) && $apiId){
            foreach ($apiType as $type){
                self::refurbishApi($type,$apiId,$gameId);
            }
        }
        if (env("IS_HONGKONG")) {
            if ($apiId && $isAddSyncTask) {
                // 同步新加坡 - 添加任务
                self::addDataSync($apiType,$apiId,$gameId);
            }
            // pbpUrl
            if ($apiType == self::INTERFACE_TYPE_MATCH) {
                if (\Yii::$app->params['refresh_key'] == 'Singapore') {
                    // 新加坡pbpUrl
                    \Yii::$app->params['pbp_data_url'] = 'wss://live.elements-data.com/v1';
                } else {
                    // 香港pbpUrl
                    \Yii::$app->params['pbp_data_url'] = 'wss://live.elementsdata.cn/v1';
                }
            }
        }else{
            // 北京pbpUrl
            \Yii::$app->params['pbp_data_url'] = 'ws://39.105.105.57:9997';
        }
        $oldDb = \Yii::$app->get('db');
        $gyDb  = \Yii::$app->get('db_gy');
        \Yii::$app->set('db',$gyDb);
        switch ($apiType) {
            case self::INTERFACE_TYPE_GAME:
                CreateEsIndex\GameEsIndex::structure();
                SetApi\GameSet::setGame($apiId);
                break;
            case self::INTERFACE_TYPE_ORGANIZATION:
                CreateEsIndex\OrganizationEsIndex::structure();
                SetApi\OrganizationSet::setOrganization($apiId);
                break;
            case self::INTERFACE_TYPE_LOL_CHAMPION:
                CreateEsIndex\MetadataLolEsIndex::structureChampion();
                SetApi\MetadataLolSet::setChampions($apiId);
                break;
            case self::INTERFACE_TYPE_LOL_ITEM:
                CreateEsIndex\MetadataLolEsIndex::structureItem();
                SetApi\MetadataLolSet::setItems($apiId);
                break;
            case self::INTERFACE_TYPE_LOL_SUMMONERSPELL:
                CreateEsIndex\MetadataLolEsIndex::structureSummonerspell();
                SetApi\MetadataLolSet::setSummonerspell($apiId);
                break;
            case self::INTERFACE_TYPE_LOL_RUNE:
                CreateEsIndex\MetadataLolEsIndex::structureRune();
                SetApi\MetadataLolSet::setRunes($apiId);
                break;
            case self::INTERFACE_TYPE_DOTA_HERO:
                CreateEsIndex\MetadataDotaEsIndex::structureHero();
                SetApi\MetadataDotaSet::setHero($apiId);
                break;
            case self::INTERFACE_TYPE_DOTA_ITEM:
                CreateEsIndex\MetadataDotaEsIndex::structureItem();
                SetApi\MetadataDotaSet::setItems($apiId);
                break;
            case self::INTERFACE_TYPE_CSGO_MAP:
                CreateEsIndex\MetadataCsgoEsIndex::structureMap();
                SetApi\MetadataCsgoSet::setMap($apiId);
                break;
            case self::INTERFACE_TYPE_CSGO_WEAPON:
                CreateEsIndex\MetadataCsgoEsIndex::structureWeapon();
                SetApi\MetadataCsgoSet::setWeapon($apiId);
                break;
            case self::INTERFACE_TYPE_TEAM:
                CreateEsIndex\TeamEsIndex::structure();
                SetApi\TeamSet::setTeam($apiId);
                break;
            case self::INTERFACE_TYPE_PLAYER:
                CreateEsIndex\PlayerEsIndex::structure();
                SetApi\PlayerSet::setPlayer($apiId);
                break;
            case self::INTERFACE_TYPE_TOURNAMENT:
                CreateEsIndex\TournamentEsIndex::structure();
                SetApi\TournamentSet::setTournament($apiId);
                break;
            case self::INTERFACE_TYPE_MATCH:
                // 旧版match索引
                CreateEsIndex\MatchEsIndex::structure();
                // 新版match索引
                CreateEsIndex\MatchesEsIndex::structure();
                SetApi\MatchSet::setMatch($apiId,$gameId);
                break;
            case self::INTERFACE_TYPE_BATTLE:
                CreateEsIndex\BattleEsIndex::structure();
                SetApi\BattleSet::setBattle($apiId,$gameId);
                break;
            case self::INTERFACE_TYPE_BATTLE_SINGLE:
                CreateEsIndex\BattleEsIndex::structure();
                SetApi\BattleSet::setBattle(null,null,$apiId);
                break;
            case self::INTERFACE_TYPE_MATCH_TOURNAMENT:
                // 旧版match索引
                CreateEsIndex\MatchEsIndex::structure();
                // 新版match索引
                CreateEsIndex\MatchesEsIndex::structure();
                CreateEsIndex\BattleEsIndex::structure();
                SetApi\MatchTournamentSet::setMatchTournament($apiId);
                break;
            case self::INTERFACE_TYPE_CSGO_BATTLE:
                SetApi\CsgoBattleSet::setCsgoBattle($apiId);
                break;
            case self::INTERFACE_TYPE_CSGO_TOURNAMENT:
                SetApi\CsgoTournamentSet::setCsgoTournament($apiId);
                break;
            default:
                break;
        }
        \Yii::$app->set('db',$oldDb);
        return true;
    }

    public static function getOngoingMatch($params=['page' => 1,'per_page' => 50])
    {
        // 查询进行中的比赛
        $matchInfo = MatchService::getMatchDetail(null,$params,2);
        $matchData = $matchInfo['list'];
        foreach ($matchData as $key=>$match) {
            // 比赛Id
            $matchId = $match['match_id'];

             // 判断刷新5e赛事
            $tournamentId = isset($match['tournament']['tournament_id']) ? $match['tournament']['tournament_id']:null;
            // 更换 5e 赛事id
            switch ($tournamentId){
                case 72:
                case 242:
                    print("现在正在添加-5E-比赛为：{$matchId} task任务...")."\n";
                    // 循环添加统计match任务
                    $item = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_MAJOR_HOT_REFRESH_MATCH_5E,
                            "",
                            Consts::RESOURCE_TYPE_MATCH,
                            "","",""
                        ),
                        "type" => "",
                        "batch_id" => \app\modules\task\services\QueueServer::setBatchId(
                            Consts::EXECUTE_TYPE_CTB,
                            "",
                            Consts::RESOURCE_TYPE_MATCH
                        ),
                        "params" => [
                            'match_id' => $matchId
                        ],
                    ];
                    // 同步香港
                    TaskRunner::addTask($item, 1);

                    sleep(1);

                    print("现在正在添加-5E-赛事为：{$tournamentId} task任务...")."\n\n";
                    // 循环添加统计tournament任务
                    $item = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_MAJOR_HOT_REFRESH_TOURNAMENT_5E,
                            "",
                            Consts::RESOURCE_TYPE_TOURNAMENT,
                            "","",""
                        ),
                        "type" => "",
                        "batch_id" => \app\modules\task\services\QueueServer::setBatchId(
                            Consts::EXECUTE_TYPE_CTB,
                            "",
                            Consts::RESOURCE_TYPE_TOURNAMENT
                        ),
                        "params" => [
                            'tournament_id' => $tournamentId
                        ],
                    ];
                    // 同步香港
                    TaskRunner::addTask($item, 1);
                    break;
                default:
                    break;
            }

            print("现在正在刷新比赛：{$matchId}...")."\n\n";
            // 循环添加任务
            $item = [
                "tag" => QueueServer::getTag(
                    QueueServer::QUEUE_MAJOR_ES_HOT_REFRESH,
                    "",
                    Consts::RESOURCE_TYPE_MATCH,
                    "","",""),
                "type" => "",
                "batch_id" => \app\modules\task\services\QueueServer::setBatchId(
                    Consts::EXECUTE_TYPE_CTB,
                    "",
                    Consts::RESOURCE_TYPE_MATCH
                ),
                "params" => [
                    'match_id' => $matchId
                ],
            ];
            // 同步香港
            TaskRunner::addTask($item, 1);
        }
        // 递归分页
        if(!empty($matchData)){
            $params['page'] = $params['page']+1;
            self::getOngoingMatch($params);
        }else{
            return true;
        }
    }
}
