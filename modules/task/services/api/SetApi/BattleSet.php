<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\task\services\api\GetApi\BattleService;
use app\modules\common\services\EsService;
use app\modules\task\services\api\ApiBase;
use yii\data\Pagination;

class BattleSet
{
    public static function setBattle($matchCreId=null,$gameId=null,$battleCreId=null)
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        // 刷新一场battle
        if($battleCreId) {
            // battle数据
            $battleInfo = BattleService::getBattleDetail($battleCreId);
            if (!empty($battleInfo)) {
                // 当前时间(记录间隔5秒数据作为redis-Key)
                $currentDate = date('Y-m-d~H_i_s',time());
                // 存入es
                $esInfo['battle_id'] = intval($battleInfo['battle_id']);
                $esInfo['battle_order'] = intval($battleInfo['order']);
                $esInfo['match_id'] = intval($battleInfo['match']['match_id']);
//                $esInfo['game_id'] = isset($battleInfo['game']['game_id']) ? $battleInfo['game']['game_id']:null;
                $esInfo['match_status'] = intval($battleInfo['match']['status']);
                $esInfo['match_delay'] = $battleInfo['match']['data_speed'];
                $esInfo['match_end_at'] = date('Y-m-d H:i:s',strtotime($battleInfo['match']['end_at']));
                $client->createIndexAdd(ApiBase::BATTLE_ES_INDEX,$battleInfo['battle_id'],$esInfo);
                // 存入redis
                $battleInfo = ApiBase::convertNull($battleInfo);
                unset($battleInfo['match']['data_speed']);
                $battleJson = json_encode($battleInfo);
                ApiBase::hashSet(['Match',$battleInfo['match']['match_id'],'Battles'],$battleCreId,$battleJson);

                if (true) {
                    // 获取原始battle信息
                    $matchBattlesData = ApiBase::hashGetAll(['Match',$battleInfo['match']['match_id'],'Battles']);
                    foreach ($matchBattlesData as $key=>$matchBattleJson) {
                        if (!is_numeric($matchBattleJson)) {
                            $currentBattleInfo = json_decode($matchBattleJson,true);
                            if ($currentBattleInfo['battle_id'] != $battleCreId) {
                                // 存入(间隔5秒数据)redis
                                ApiBase::hashSet(['Match', $esInfo['match_id'], $currentDate, 'Battles'], $currentBattleInfo['battle_id'], $matchBattleJson, true);
                            }
                        }
                    }
                    // 存入(间隔5秒当前battle数据)redis
                    ApiBase::hashSet(['Match', $esInfo['match_id'], $currentDate, 'Battles'], $battleCreId, $battleJson, true);
                }
            }
        }else {
            if ($matchCreId) {
                // 刷新一场match
                $matchIdArray = self::getMatch([],$matchCreId);
                self::setBattleEs($matchIdArray,$client,true);
            }else {
                for ($i = 1; $i < 9999999; $i++) {
                    print("现在正在查询第{$i}页，match数据...")."\n";
                    $matchIdArray = self::getMatch(['page' => $i, 'per_page' => 100],null,$gameId);
                    if (!$matchIdArray) {
                        break;
                    }
                    self::setBattleEs($matchIdArray, $client);
                }
            }
        }
        return $indexResponse;
    }
    // 获取matchId
    public static function getMatch($params,$matchCreId=null,$gameId=null){
        $q = Match::find()->select(['id']);
        if ($matchCreId) {
            // 刷新一场match
            $q->andWhere(['id' => $matchCreId]);
        }
        if($gameId){
            print("现在正在查询游戏Id为：{$gameId} 的所有match_id...")."\n";
            // 刷新某个游戏
            $q->andWhere(['game' => $gameId]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page']) - 1;
        $pageSize = empty($params['per_page']) ? 50 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $matchIdArray = $q->offset($pages->offset)->limit($pages->limit)
            ->orderBy("id desc")
            ->asArray()->all();
        return $matchIdArray;
    }
    // set es和redis
    public static function setBattleEs($matchIdArray,$client,$isInterval=false)
    {
        foreach ($matchIdArray as $key => $matchId) {
            print("现在正在查询比赛{$matchId['id']}数据...")."\n";
            $battleIdInfoArray = MatchBattle::find()->select(['id','deleted'])
                ->where(['match' => $matchId['id']])
                ->orderBy("begin_at asc")
                ->asArray()->all();
            $battleIdsInfo = [];
            foreach ($battleIdInfoArray as $ks=>&$val){
                if($val['deleted'] == 2){
                    $battleIdsInfo[] = $val['id'];
                }else{
                    // 删除redis-key中某条数据
                    ApiBase::hashDelKey(['Match', $matchId['id'], 'Battles'], $val['id']);
                    // 删除es - 数据
                    $client->deleteFile(ApiBase::BATTLE_ES_INDEX,$val['id']);
                }
            }

            // 清除redis-battle 错误数据
            $matchBattlesArray = ApiBase::hashGetAll(['Match', $matchId['id'], 'Battles']);
            $redisBattleIds = [];
            foreach ($matchBattlesArray as $battleJson) {
                if (is_numeric($battleJson)) {
                    $redisBattleIds[] = $battleJson;
                }
            }
            $diffBattleIds = array_diff($redisBattleIds,$battleIdsInfo);
            foreach ($diffBattleIds as $bid) {
                // 删除redis-key中某条数据
                ApiBase::hashDelKey(['Match', $matchId['id'], 'Battles'], $bid);
                // 删除es - 数据
//                $client->deleteFile(ApiBase::BATTLE_ES_INDEX,$bid);
            }

            // 当前时间(记录间隔5秒数据作为redis-Key)
            $currentDate = date('Y-m-d~H_i_s',time());
            // 进行刷新
            foreach ($battleIdsInfo as $k => &$battleId) {
                if ($battleId) {
                    // battle数据
                    print("现在正在查询比赛：{$matchId['id']}-battle：{$battleId}数据...")."\n";
                    $battleInfo = BattleService::getBattleDetail($battleId);
                    if (!empty($battleInfo)) {
                        // 存入es
                        $esInfo['battle_id'] = $battleId;
                        $esInfo['battle_order'] = intval($battleInfo['order']);
                        $esInfo['match_id'] = $matchId['id'];
//                        $esInfo['game_id'] = isset($battleInfo['game']['game_id']) ? $battleInfo['game']['game_id']:null;
                        $esInfo['match_status'] = $battleInfo['match']['status'];
                        $esInfo['match_delay'] = $battleInfo['match']['data_speed'];
                        $esInfo['match_end_at'] = date('Y-m-d H:i:s',strtotime($battleInfo['match']['end_at']));
                        print("现在正在存储es...")."\n";
                        $client->createIndexAdd(ApiBase::BATTLE_ES_INDEX,$battleId,$esInfo);
                        unset($battleInfo['match']['data_speed']);
                        $battleInfo = ApiBase::convertNull($battleInfo);
                        $battleJson = json_encode($battleInfo);
                        print("现在正在存储redis...")."\n";
                        // 存入(实时数据)redis
                        ApiBase::hashSet(['Match', $matchId['id'], 'Battles'], $battleId, $battleJson);
                        if($isInterval) {
                            // 存入(间隔5秒数据)redis
                            ApiBase::hashSet(['Match', $matchId['id'], $currentDate, 'Battles'], $battleId, $battleJson, true);
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
        return;
    }

}
