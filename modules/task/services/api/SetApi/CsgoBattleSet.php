<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\task\services\api\GetApi\CsgoBattleService;
use app\modules\common\services\EsService;
use app\modules\task\services\api\ApiBase;
use yii\data\Pagination;

class CsgoBattleSet
{
//    const BATTLE_CSGO_KEY_NAME = '5E-Battles';
    const BATTLE_CSGO_KEY_NAME = 'Battles-5E';
    public static function setCsgoBattle($matchCreId=null,$battleCreId=null)
    {
        $indexResponse = true;
        $index_info = 'resource_battle_5e_list';
        //初始化Es链接
//        $client = EsService::initConnect();
        $client = '';
        // 刷新一场battle
        if($battleCreId) {
            // battle数据
            $battleInfo = CsgoBattleService::getBattleDetail($battleCreId);
            if (!empty($battleInfo)) {
                // 存入es
//                $esInfo['battle_id'] = intval($battleInfo['battle_id']);
//                $esInfo['match_id'] = intval($battleInfo['match']['match_id']);
//                $indexParams = [
//                    'index' => $index_info,
//                    'id' => intval($battleInfo['battle_id']),
//                    'body' => $esInfo,
//                    'client' => [
//                        'timeout' => 10,
//                        'connect_timeout' => 10
//                    ]
//                ];
//                $client->index($indexParams);
                // 存入redis
                $battleJson = json_encode($battleInfo);
                ApiBase::hashSet(['Match',$battleInfo['match']['match_id'],self::BATTLE_CSGO_KEY_NAME],$battleCreId,$battleJson);
            }
        }else {
            if ($matchCreId) {
                // 刷新一场match
                $matchIdArray = self::getMatch([],$matchCreId);
                self::setBattleEs($matchIdArray,$client,$index_info);
            }else {
                for ($i = 1; $i < 9999999; $i++) {
                    $data = self::getMatch(['page' => $i, 'per_page' => 100]);
                    if (!$data) {
                        break;
                    }
                    self::setBattleEs($data, $client, $index_info);
                }
            }
        }
        return $indexResponse;
    }
    // 获取matchId
    public static function getMatch($params,$matchCreId=null){
        $q = Match::find()->select(['id'])->where(['deleted' => 2]);
        if ($matchCreId) {
            // 刷新一场match
            $q->andWhere(['id' => $matchCreId]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page']) - 1;
        $pageSize = empty($params['per_page']) ? 50 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $matchIdArray = $q->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        return $matchIdArray;
    }

    // set es和redis
    public static function setBattleEs($matchIdArray,$client,$index_info)
    {
        $agin = false;
        foreach ($matchIdArray as $key => $matchId) {
            $battleIdInfoArray = MatchBattle::find()->select(['id','deleted'])->where(['match' => $matchId['id']])->orderBy("begin_at asc")->asArray()->all();
            $battleIdInfo = [];
            foreach ($battleIdInfoArray as $ks=>$val){
                if($val['deleted'] == 2){
                    $info['id'] = $val['id'];
                    $battleIdInfo[] = $info;
                }else{
                    // 删除redis-key中某条数据
                    ApiBase::hashDelKey(['Match', $matchId['id'], self::BATTLE_CSGO_KEY_NAME], $val['id']);
                    // 删除redis-key
                    ApiBase::redisDelKey(['Match', $matchId['id'], 'players_num_5E']);
                }
            }
            // 最后结果match统计数据
            $matchCurrentCount = [];
            // 总回合数
            $roundZongOrdinalCount = 0;
            foreach ($battleIdInfo as $k => $battleId) {
                if ($battleId['id']) {

                    // 存入es
//                    $esInfo['battle_id'] = intval($battleId['id']);
//                    $esInfo['match_id'] = intval($matchId['id']);
//                    $indexParams = [
//                        'index' => $index_info,
//                        'id' => intval($battleId['id']),
//                        'body' => $esInfo,
//                        'client' => [
//                            'timeout' => 10,
//                            'connect_timeout' => 10
//                        ]
//                    ];
//                    $client->index($indexParams);
                    // battle数据
                    $battleInfo = CsgoBattleService::getBattleDetail($battleId['id']);
                    // 总回合数
                    $roundZongOrdinalCount = $roundZongOrdinalCount+$battleInfo['scores'][0]['score']+$battleInfo['scores'][1]['score'];
                    // 当前battle回合数
                    $roundOrdinalCount = $battleInfo['scores'][0]['score']+$battleInfo['scores'][1]['score'];
                    $battleTeams = $battleInfo['battle_detail']['teams'];
                    foreach ($battleTeams as $kk=>$team){
                        // 计算player
                        foreach ($team['players'] as $kp=>$player){
                            $matchPlayerCount = [
                                        'kills' => 0,
                                        'headshot_kills' => 0,
                                        'deaths' => 0,
                                        'kd_diff' => 0,
                                        'assists' => 0,
                                        'flash_assists' => 0,
                                        'adr' => 0,
                                        'first_kills' => 0,
                                        'first_deaths' => 0,
                                        'first_kills_diff' => 0,
                                        'kast' => 0,
                                        'rating' => 0,
                                        'damage' => 0,
                                        'team_damage' => 0,
                                        'damage_taken' => 0,
                                        'hegrenade_damage_taken' => 0,
                                        'inferno_damage_taken' => 0,
                                        'planted_bomb' => 0,
                                        'defused_bomb' => 0,
                                        'chicken_kills' => 0,
                                        'blind_enemy_time' => 0,
                                        'blind_teammate_time' => 0,
                                        'team_kills' => 0,
                                        'multi_kills' => 0,
                                        'two_kills' => 0,
                                        'three_kills' => 0,
                                        'four_kills' => 0,
                                        'five_kills' => 0,
                                        'one_on_x_clutches' => 0,
                                        'one_on_one_clutches' => 0,
                                        'one_on_two_clutches' => 0,
                                        'one_on_three_clutches' => 0,
                                        'one_on_four_clutches' => 0,
                                        'one_on_five_clutches' => 0,
                                        'hit_generic' => 0,
                                        'hit_head' => 0,
                                        'hit_chest' => 0,
                                        'hit_stomach' => 0,
                                        'hit_left_arm' => 0,
                                        'hit_right_arm' => 0,
                                        'hit_left_leg' => 0,
                                        'hit_right_leg' => 0,
                                        'awp_kills' => 0,
                                        'knife_kills' => 0,
                                        'taser_kills' => 0,
                                        'shotgun_kills' => 0,
                                        'adr_total' => 0,
                                        'kast_total' => 0,
                                        'rating_total' => 0,
                                    ];
                            foreach ($player as $field=>$val){
                                if(isset($matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field]) && $field!='player'){
                                    switch ($field){
                                        case 'adr':
                                        case 'rating':
                                        case 'kast':
                                            $sum = $val*$roundOrdinalCount;
                                            $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field] =
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field]+$sum;
                                            // 最后一次
                                            if($k+1 == count($battleIdInfo)){
                                                $agin = true;
                                                // match 【adr，rating，kast】总数
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field.'_total'] =
                                                    $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field];
//                                                $matchCountNum = self::$roundOrdinalCount;
                                                $matchCountNum = $roundZongOrdinalCount;
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field] =
                                                    $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field]/$matchCountNum;
                                            }
                                            break;
                                        default :
                                            $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field] =
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field]+$val;
                                            break;
                                    }
                                }else {
                                    if($field == 'player') {
                                        $matchCurrentCount[$team['team_id']][$player['player']['player_id']]['player']['player_id'] = $player['player']['player_id'];
                                        $matchCurrentCount[$team['team_id']][$player['player']['player_id']]['player']['nick_name'] = $player['player']['nick_name'];
                                    }
                                    if (isset($matchPlayerCount[$field])) {
                                        // adr * 回合数 / 总回合数
                                        switch ($field){
                                            case 'adr':
                                            case 'rating':
                                            case 'kast':
                                                $sum = $val*$roundOrdinalCount;
                                                if(count($battleIdInfo) == 1){
                                                    $agin = true;
                                                    // match 【adr，rating，kast】总数
                                                    $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field.'_total'] = $sum;
                                                    // match总回合数
                                                    $matchCountNum = $roundZongOrdinalCount;
//                                                    $sum = $val*$roundOrdinalCount/$matchCountNum;
                                                    $sum = $sum/$matchCountNum;
                                                }
                                                $matchPlayerCount[$field] = $matchPlayerCount[$field] + $sum;
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field] = $matchPlayerCount[$field];
                                                break;
                                            default :
                                                $matchPlayerCount[$field] = $matchPlayerCount[$field] + $val;
                                                $matchCurrentCount[$team['team_id']][$player['player']['player_id']][$field] = $matchPlayerCount[$field];
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if($agin) {
                        $matchPlayersNums = MatchBattleRoundPlayerCsgo::find()
                            ->select(['COUNT(*) AS player_num','player_id'])
                            ->where(['match'=>$battleInfo['match']['match_id']])->andWhere(['>','player_id',0])
                            ->groupBy(['player_id'])
                            ->asArray()->all();
                        if(!empty($matchPlayersNums)) {
                            $matchPlayersNumJson = json_encode($matchPlayersNums);
                            // player 每个人打的回合数存入redis
                            ApiBase::valueSet(['Match', $battleInfo['match']['match_id'], 'players_num_5E'], $matchPlayersNumJson);
                        }
                        $stats = [];
                        foreach ($matchCurrentCount as $key => $teams) {
                            $matchTeamCount = [
                                'team_id' => null,
                                'kills' => 0,
                                'headshot_kills' => 0,
                                'deaths' => 0,
                                'kd_diff' => 0,
                                'assists' => 0,
                                'flash_assists' => 0,
                                'adr' => 0,
                                'first_kills' => 0,
                                'first_deaths' => 0,
                                'first_kills_diff' => 0,
                                'kast' => 0,
                                'rating' => 0,
                                'damage' => 0,
                                'team_damage' => 0,
                                'damage_taken' => 0,
                                'hegrenade_damage_taken' => 0,
                                'inferno_damage_taken' => 0,
                                'planted_bomb' => 0,
                                'defused_bomb' => 0,
                                'chicken_kills' => 0,
                                'blind_enemy_time' => 0,
                                'blind_teammate_time' => 0,
                                'team_kills' => 0,
                                'multi_kills' => 0,
                                'two_kills' => 0,
                                'three_kills' => 0,
                                'four_kills' => 0,
                                'five_kills' => 0,
                                'one_on_x_clutches' => 0,
                                'one_on_one_clutches' => 0,
                                'one_on_two_clutches' => 0,
                                'one_on_three_clutches' => 0,
                                'one_on_four_clutches' => 0,
                                'one_on_five_clutches' => 0,
                                'hit_generic' => 0,
                                'hit_head' => 0,
                                'hit_chest' => 0,
                                'hit_stomach' => 0,
                                'hit_left_arm' => 0,
                                'hit_right_arm' => 0,
                                'hit_left_leg' => 0,
                                'hit_right_leg' => 0,
                                'awp_kills' => 0,
                                'knife_kills' => 0,
                                'taser_kills' => 0,
                                'shotgun_kills' => 0,
                            ];
                            $playerCount = count($matchCurrentCount[$key]);
                            foreach ($matchCurrentCount[$key] as $k => $player) {
                                $matchCurrentCount[$key][$k]['adr'] = round($matchCurrentCount[$key][$k]['adr'],2);
                                $matchCurrentCount[$key][$k]['kast'] = round($matchCurrentCount[$key][$k]['kast'],4);
                                $matchCurrentCount[$key][$k]['rating'] = round($matchCurrentCount[$key][$k]['rating'],2);
                                $matchCurrentCount[$key][$k]['blind_enemy_time'] = round($matchCurrentCount[$key][$k]['blind_enemy_time'],2);
                                $matchCurrentCount[$key][$k]['blind_teammate_time'] = round($matchCurrentCount[$key][$k]['blind_teammate_time'],2);
                                foreach ($player as $kk => $val) {
                                    if ($key) {
                                        $matchTeamCount['team_id'] = $key;
                                    }
                                    if (isset($matchTeamCount[$kk])) {
                                        $matchTeamCount[$kk] = $matchTeamCount[$kk] + $val;
                                    }
                                }
                            }
                            $matchTeamCount['adr'] = round($matchTeamCount['adr'] / $playerCount,2);
                            $matchTeamCount['kast'] = round($matchTeamCount['kast'] / $playerCount,4);
                            $matchTeamCount['rating'] = round($matchTeamCount['rating'] / $playerCount,2);
                            $matchTeamCount['blind_enemy_time'] = round($matchTeamCount['blind_enemy_time'],2);
                            $matchTeamCount['blind_teammate_time'] = round($matchTeamCount['blind_teammate_time'],2);
                            $stats[$key] = $matchTeamCount;
                            $stats[$key]['players'] = $matchCurrentCount[$key];
                        }
                    }
                    if (!empty($battleInfo)) {
                        $battleInfo['match']['stats'] = $stats;
                        $battleJson = json_encode($battleInfo);
                        // 存入redis
                        ApiBase::hashSet(['Match', $battleInfo['match']['match_id'], self::BATTLE_CSGO_KEY_NAME], $battleId['id'], $battleJson);
                    }
                }
            }
        }
        return;
    }

}