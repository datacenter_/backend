<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\common\services\EsService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattleRoundPlayerCsgo;
use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\GetApi\TournamentService;

class CsgoTournamentSet
{
    public static function setCsgoTournament($tournamentId=null)
    {
        // 5E - 赛事id
        switch ($tournamentId){
            case 72:
            case 242:
                break;
            default:
                return;
        }
//        $index_info = 'resource_tournament_5e_list';
        $tournamentJson = ApiBase::valueGet(['Tournaments',$tournamentId]);
        if($tournamentJson){
            $tournamentData = json_decode($tournamentJson,true);
        }else{
            $dataList = TournamentService::getTournamentDetail($tournamentId);
            $tournamentData = $dataList[0];
        }
        // 赛事下match状态
        $tournamentMatchStatus = [];
        // 赛事下所有match
        $tournamentMatchs = [];
        if(!empty($tournamentData)){
            // 查询matchId
            $matchIds = Match::find()->alias('m')
                ->select(['m.id','m_r.status'])
                ->leftJoin("match_real_time_info as m_r", "m_r.id = m.id")
                ->where(['m.tournament_id'=>$tournamentId,'m.deleted'=>2])
                ->asArray()->all();
            foreach ($matchIds as $match){
                // 查询redis是否有该结束比赛
                $matchStatusInfo = ApiBase::hashGet(['Tournaments','5E','Matchs',$tournamentId],$match['id']);
                // 重新刷新进行删除match统计结果数据
                if($matchStatusInfo && $match['status']==2){
                    // 删除redis-key中某条数据
                    ApiBase::hashDelKey(['Tournaments','5E','Matchs',$tournamentId],$match['id']);
                }else{
                    // 如果比赛结束有统计数据 跳出
                    if($matchStatusInfo){
                        continue;
                    }
                }
                // match状态
//                $tournamentMatchStatus[$match['id']] = $match['status'];
                // 从redis取出对应的 5e 数据
                $matchBattlesJson = ApiBase::hashGetAll(['Match', $match['id'], 'Battles-5E']);
//                $matchBattlesJson = ApiBase::hashGetAll(['Match', $match['id'], '5E-Battles']);
                if($matchBattlesJson){
                    // 查询match下：roundPlayer数量
                    $matchPlayersNumJson = ApiBase::valueGet(['Match', $match['id'], 'players_num_5E']);
                    // 处理matchRoundPlayer数量结构
                    $matchPlayersNumArray = [];
                    if($matchPlayersNumJson) {
                        $matchPlayersNum = json_decode($matchPlayersNumJson,true);
                        foreach ($matchPlayersNum as $key => $playerCount) {
                            $matchPlayersNumArray[$playerCount['player_id']] = $playerCount['player_num'];
                        }
                    }else{
//                        print_r("这个playerId：{$playerId}没取到：total_rounds");
                    }
                    // match计算的数据
                    $matchBattlesArray = [];
                    // 已完成比赛数量
                    $match_played = [];
                    // 获胜比赛数量
                    $match_won = [];
                    // 已完成对局数量
                    $battle_played = 0;
                    // 获胜对局数量
                    $battle_won = [];
                    // 已完成回合数量
                    $round_played = 0;
                    // 获胜回合数量
                    $round_won = [];
                    foreach ($matchBattlesJson as $battleId=>$battleJson){
                        if(!is_numeric($battleJson) && !empty($battleJson)){
                            $battleInfo = json_decode($battleJson,true);
                            // 已完成回合总数量
                            $round_played = $round_played+$battleInfo['scores'][0]['score']+$battleInfo['scores'][1]['score'];
                            if($battleInfo['status'] == 'completed') {
                                $battle_played = $battle_played + 1;
                            }
                            // 本场比赛获胜battle数量
                            if(isset($battleInfo['winner']['team_id'])){
                                if(isset($battle_won[$battleInfo['winner']['team_id']])) {
                                    $battle_won[$battleInfo['winner']['team_id']] = $battle_won[$battleInfo['winner']['team_id']]+1;
                                }else{
                                    $battle_won[$battleInfo['winner']['team_id']] = 1;
                                }
                            }
                            // 本场比赛获胜回合数量
                            if(!empty($battleInfo['scores'])) {
                                foreach ($battleInfo['scores'] as $teamScore){
                                    if (isset($round_won[$teamScore['team_id']])) {
                                        $round_won[$teamScore['team_id']] = $round_won[$teamScore['team_id']]+$teamScore['score'];
                                    }else{
                                        $round_won[$teamScore['team_id']] = $teamScore['score'];
                                    }
                                }
                            }
                            if(isset($battleInfo['match']['stats'])) {
                                // match状态
                                $tournamentMatchStatus[$match['id']] = $battleInfo['match']['status'];
                                // 计算match结束数量
                                if($battleInfo['match']['status'] == 'completed'){
                                    foreach ($battleInfo['teams'] as $teamInfo){
                                        if(isset($match_played[$teamInfo['team_id']])){
                                            $match_played[$teamInfo['team_id']] = $match_played[$teamInfo['team_id']]+1;
                                        }else{
                                            $match_played[$teamInfo['team_id']] = 1;
                                        }
                                    }
                                }
                                // 比赛获胜数量
                                if(isset($battleInfo['match']['winner'])){
                                    if(isset($match_won[$battleInfo['match']['winner']['team_id']])){
                                        $match_won[$battleInfo['match']['winner']['team_id']] = $match_won[$battleInfo['match']['winner']['team_id']]+1;
                                    }else{
                                        $match_won[$battleInfo['match']['winner']['team_id']] = 1;
                                    }
                                }
                                $matchCre = [];
                                // 比赛player统计数据
                                foreach ($battleInfo['match']['stats'] as $teamId=>$statsInfo){
                                    $matchStatsPlayers = $statsInfo['players'];
                                    foreach ($matchStatsPlayers as $playerId=>$player){
                                        unset($matchStatsPlayers[$playerId]['adr']);
                                        unset($matchStatsPlayers[$playerId]['kast']);
                                        unset($matchStatsPlayers[$playerId]['rating']);
                                        $matchStatsPlayers[$playerId]['total_rounds'] = 0;
                                        if(isset($matchPlayersNumArray[$playerId])) {
                                            $matchStatsPlayers[$playerId]['total_rounds'] = $matchPlayersNumArray[$playerId];
                                        }
                                    }
                                    $matchCre[$teamId] = $matchStatsPlayers;
                                }
                            }
                            $matchInfo = [
                                'match_won' => $match_won,
                                'match_played' => $match_played,
                                'battle_played' => $battle_played,
                                'battle_won' => $battle_won,
                                'round_played' => $round_played,
                                'round_won' => $round_won,
                            ];
                            $matchCre['match_info'] = $matchInfo;
                            $matchBattlesArray[$match['id']] = $matchCre;
                        }
                    }
                }
                if(!empty($matchBattlesArray)){
                    $tournamentMatchs[] = $matchBattlesArray;
                }
            }
        }

        // matchId作为key
        $tournamentMatchInfo = [];
        foreach ($tournamentMatchs as $key=>$matchTeamInfo){
            foreach ($matchTeamInfo as $matchId=>$teams){
                $tournamentMatchInfo[$matchId][$matchId] = $teams;
            }
        }
        // 开始检测比赛状态为3 -- 已结束的进行存储
        foreach ($tournamentMatchInfo as $matchId=>$matchTeamInfo){
            if(isset($tournamentMatchStatus[$matchId]) && $tournamentMatchStatus[$matchId]=='completed'){
                $matchJson = json_encode($matchTeamInfo);
                // 存入redis
                ApiBase::hashSet(['Tournaments','5E','Matchs',$tournamentId],$matchId,$matchJson);
            }
        }

        // 合并计算数据
        $tournamentMatchInfoJson = ApiBase::hashGetAll(['Tournaments','5E','Matchs',$tournamentId]);
        if($tournamentMatchInfoJson) {
            foreach ($tournamentMatchInfoJson as $key=>$matchInfoJson){
                if(!is_numeric($matchInfoJson)){
                    $matchInfo = json_decode($matchInfoJson,true);
                    foreach ($matchInfo as $matchId=>$matchData){
                        $tournamentMatchInfo[$matchId] = $matchInfo;
                    }
//                    $tournamentMatchInfo[] = $matchInfo;
                }
            }
        }

        // 开始计算整理结构
        $tournamentTeams = [];
        $tournamentPlayers = [];
        foreach ($tournamentMatchInfo as $key=>$matchTeamInfo){
            foreach ($matchTeamInfo as $matchId=>$teams){
                $teamsId = [];
                foreach ($teams as $teamId=>$players) {
                    if(is_numeric($teamId)) {
                        $teamsId[$teamId] = $teamId;
                        foreach ($players as $playerId => $player) {
                            foreach ($player as $field=>$val) {
                                if($field == 'player'){
                                    $tournamentPlayers[$teamId][$playerId][$field] = $player[$field];
                                } else {
                                    if (isset($tournamentPlayers[$teamId][$playerId])) {
                                        $tournamentPlayers[$teamId][$playerId][$field] = $tournamentPlayers[$teamId][$playerId][$field]+$val;
                                    } else {
                                        $tournamentPlayers[$teamId][$playerId][$field] = $val;
                                    }
                                }
                            }
                        }
                    }else{
                        // FIXME match_info 数据统计
                        $match_info = $teamId;
                        foreach ($teamsId as $team_id){
                            foreach ($teams[$match_info] as $field=>$val){
                                if(!isset($tournamentTeams[$team_id][$field])){
                                    if(is_array($val)){
                                        $tournamentTeams[$team_id][$field] = !isset($val[$team_id])?0:$val[$team_id];
                                    }else{
                                        $tournamentTeams[$team_id][$field] = $val;
                                    }
                                }else{
                                    if(is_array($val)){
                                        $tournamentTeams[$team_id][$field] = $tournamentTeams[$team_id][$field]+$val[$team_id];
                                    }else{
                                        $tournamentTeams[$team_id][$field] = $tournamentTeams[$team_id][$field]+$val;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
//        $stats = [];
        // 计算player统计
        foreach ($tournamentPlayers as $teamId=>$players){
            foreach ($players as $playerId=>$player){
                $tournamentPlayers[$teamId][$playerId]['blind_enemy_time'] = round($player['blind_enemy_time'],2);
                $tournamentPlayers[$teamId][$playerId]['blind_teammate_time'] = round($player['blind_teammate_time'],2);
                if($player['total_rounds']=='' || $player['total_rounds']==0) {
                    print_r("这个playerId：{$playerId}没取到：total_rounds");
                    return;
//                    $tournamentPlayers[$teamId][$playerId]['adr'] = 0;
//                    $tournamentPlayers[$teamId][$playerId]['kast'] = 0;
//                    $tournamentPlayers[$teamId][$playerId]['rating'] = 0;
                }else{
                    $tournamentPlayers[$teamId][$playerId]['adr'] = round($player['adr_total'] / $player['total_rounds'], 2);
                    $tournamentPlayers[$teamId][$playerId]['kast'] = round($player['kast_total'] / $player['total_rounds'], 4);
                    $tournamentPlayers[$teamId][$playerId]['rating'] = round($player['rating_total'] / $player['total_rounds'], 2);
                }
            }
        }
        $tournamentsPlayers = [];
        // 单把player提出去
        foreach ($tournamentPlayers as $team_id=>$teamPlayers){
            foreach ($teamPlayers as $player_id=>$player){
                $tournamentsPlayers[] = $player;
            }
        }
        // 赛事team统计结构 (暂时不用)
//        foreach ($tournamentTeams as $TeamId=>$teamInfo){
//            $teamInfo['team_id'] = $TeamId;
//            $teamInfo['players'] = $tournamentPlayers[$TeamId];
//            $stats[] = $teamInfo;
//        }
        // 最后存入redis 赛事统计结果数据
        if(!empty($tournamentData)) {
//            $tournamentData['stats'] = $stats;
            $tournamentsDescPlayers = self::sortArrayByField($tournamentsPlayers,'rating');
            $tournamentData['players'] = $tournamentsDescPlayers;
            $tournamentCurrentJson = json_encode($tournamentData,true);
//            $aa = json_last_error();
            // 存入redis
            ApiBase::valueSet(['Tournaments', '5E', $tournamentId], $tournamentCurrentJson);
        }
        return true;
    }
    // 排序
    public static function sortArrayByField($data, $field, $order = SORT_DESC)
    {
        $keys = array_keys($data);
        $array_column = array_column($data, $field);
        array_multisort(
            $array_column, $order, SORT_NUMERIC, $data, $keys
        );
        $data = array_combine($keys, $data);
        $dataCre = array_merge($data);
        return $dataCre;
    }
}