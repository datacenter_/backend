<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\GetApi\GameService;
use app\modules\common\services\EsService;

class GameSet
{
    public static function setGame($gameCreId=null,$params = ['page' => 1,'per_page' => 20])
    {
        if($gameCreId){
            $gameList = GameService::getGameDetail($gameCreId);
        }else{
            $gameData = GameService::getGameList($params);
            $gameList = $gameData['list'];
        }
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($gameList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['game_id'] = $value['game_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $game_id = $value['game_id'];
            $client->createIndexAdd(ApiBase::GAME_ES_INDEX,$game_id,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Games',$value['game_id']],$json);
        }
        // 递归分页
        if(!empty($gameList) && !$gameCreId){
            $params['page'] = $params['page']+1;
            self::setGame($gameCreId,$params);
        }else{
            return true;
        }
    }
}
