<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\GetApi\MatchService;
use app\modules\common\services\EsService;
use app\modules\task\services\api\ApiBase;

class MatchSet
{
    public static function setMatch($matchCreId=null,$gameId=null)
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        if(is_array($matchCreId) && !empty($matchCreId)){
            for($i=1;$i<9999999;$i++){
                print("现在正在查询第{$i}页，match数据...")."\n";
                $data = MatchService::getMatchList(['page'=>$i,'per_page'=>100,'game_id'=>$gameId,'match_ids'=>$matchCreId]);
                if(empty($data['list'])){
                    break;
                }
                $dataList = $data['list'];
                self::setMatchEs($dataList,$client);
            }
        }else {
            if ($matchCreId) {
                $data = MatchService::getMatchDetail($matchCreId, []);
                $dataList = $data['list'];
                self::setMatchEs($dataList, $client, true);
            } else {
                for ($i = 1; $i < 9999999; $i++) {
                    print("现在正在查询第{$i}页，match数据...") . "\n";
                    $data = MatchService::getMatchList(['page' => $i, 'per_page' => 100, 'game_id' => $gameId]);
                    if (empty($data['list'])) {
                        break;
                    }
                    $dataList = $data['list'];
                    self::setMatchEs($dataList, $client);
                }
            }
        }
        return $indexResponse;
    }
    // set es和redis
    public static function setMatchEs($dataList,$client,$isInterval=false)
    {
        foreach ($dataList as $k=>$value) {
            $esInfo = [];
            print("当前比赛为：{$value['match_id']}...")."\n";
            // 当前时间(记录间隔5秒数据作为redis-Key)
            $currentDate = date('Y-m-d~H_i_s',time());
            // es-数组
            $deleted = $value['deleted'];
            $delay = $value['delay'];
            unset($value['deleted']);
            unset($value['delay']);
            $is_private = $value['tournament']['is_private'];
            $is_private_delay = $value['tournament']['is_private_delay'];
            $private_delay_seconds = $value['tournament']['private_delay_seconds'];
            $public_delay = $value['tournament']['public_delay'];
            $public_data_devel = $value['tournament']['public_data_devel'];
            unset($value['tournament']['is_private']);
            unset($value['tournament']['is_private_delay']);
            unset($value['tournament']['private_delay_seconds']);
            unset($value['tournament']['public_delay']);
            unset($value['tournament']['public_data_devel']);
            $esInfo['match_id'] = intval($value['match_id']);
            $esInfo['deleted'] = $deleted;
            $esInfo['delay'] = $delay;
            $esInfo['game_id'] = isset($value['game']['game_id']) ? $value['game']['game_id']:null;
            $esInfo['game_rules'] = $value['game_rules'];
            $esInfo['match_type'] = $value['match_type'];
            $esInfo['number_of_games'] = intval($value['number_of_games']);
            $esInfo['tournament_id'] = isset($value['tournament']['tournament_id']) ? $value['tournament']['tournament_id']:null;
            $esInfo['group_id'] = isset($value['group']['group_id']) ? $value['group']['group_id']:null;
            if(!empty($value['teams'])){
                $teamIds = array_column($value['teams'],'team_id');
            }else{
                $teamIds = [];
            }
            $esInfo['team_id'] = $teamIds;
            $team_snapshot_name = [];
            foreach ($value['teams'] as $key=>$team){
                if(isset($team['team_snapshot']['name'])) {
                    array_push($team_snapshot_name,(String)$team['team_snapshot']['name']);
                }
            }
            $esInfo['team_snapshot_name'] = $team_snapshot_name;
            $esInfo['status'] = $value['status'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['is_battle_detailed'] = $value['is_battle_detailed'];
            $esInfo['is_pbpdata_supported'] = $value['pbpdata']['is_supported'];
            $esInfo['is_streams_supported'] = $value['streams']['is_supported'];
            $esInfo['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($value['scheduled_begin_at']));
            $esInfo['begin_at'] = date('Y-m-d H:i:s',strtotime($value['begin_at']));
            $esInfo['end_at'] = date('Y-m-d H:i:s',strtotime($value['end_at']));
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            // 转格式
            $value = ApiBase::convertNull($value);
            print("现在正在存储redis数据...")."\n";
            // 存入(实时数据)redis
            $matchJson = json_encode($value);
            ApiBase::valueSet(['Matchs',$value['match_id']],$matchJson);
            // 存入es
            print("现在正在存储es数据...")."\n";
            $client->createIndexAdd(ApiBase::MATCH_ES_INDEX,$value['match_id'],$esInfo);
            // 存入新索引es
            print("现在正在存储【新索引】es数据...")."\n";
            $esInfo['is_private'] = $is_private;
            $esInfo['is_private_delay'] = $is_private_delay;
            $esInfo['private_delay_seconds'] = $private_delay_seconds;
            $esInfo['public_delay'] = $public_delay;
            $esInfo['public_data_devel'] = $public_data_devel;
            $client->createIndexAdd(ApiBase::ES_MATCHES_ES_INDEX,$value['match_id'],$esInfo);
            if($isInterval) {
                // 存入(间隔5秒数据)redis
                ApiBase::valueSet(['Matchs',$value['match_id'],$currentDate],$matchJson,true);
            }
        }
        return;
    }

}