<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\match\models\Match;
use app\modules\task\services\api\GetApi\MatchService;
use app\modules\common\services\EsService;
use yii\data\Pagination;

class MatchTournamentSet
{
    public static function setMatchTournament($tournamentId=null)
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        if($tournamentId){
            // 刷新match下赛事
            for($i = 1; $i < 9999999; $i++){
                $data = MatchService::getMatchList(['page'=>$i,'per_page'=>100,'tournament_id'=>$tournamentId]);
                if(empty($data['list'])){
                    break;
                }
                $dataList = $data['list'];
                MatchSet::setMatchEs($dataList,$client);
            }

            // 刷新battle下match下赛事
            for ($i = 1; $i < 9999999; $i++) {
                // 获取比赛id
                $matchIdArray = self::getMatch(['page' => $i, 'per_page' => 100],$tournamentId);
                if (!$matchIdArray) {
                    break;
                }
                BattleSet::setBattleEs($matchIdArray,$client);
            }
        }
        return $indexResponse;
    }

    // 获取matchId
    public static function getMatch($params,$tournamentId=null){
        $q = Match::find()->select(['id']);
        if ($tournamentId) {
            // 刷新一场match
            $q->andWhere(['tournament_id' => $tournamentId]);
        }
        $count = $q->count();
        $page = (empty($params['page']) ? 1 : $params['page']) - 1;
        $pageSize = empty($params['per_page']) ? 50 : $params['per_page'];
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $matchIdArray = $q->offset($pages->offset)->limit($pages->limit)
            ->asArray()->all();
        return $matchIdArray;
    }
}