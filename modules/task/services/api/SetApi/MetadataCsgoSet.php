<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/03
 * Time: 14:28
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\Metadata\MetadataCsgoService;
use app\modules\common\services\EsService;

class MetadataCsgoSet
{
    public static function setMap($mapCreId=null,$params = ['page' => 1, 'per_page' => 50])
    {
        if($mapCreId){
            $dataList = MetadataCsgoService::getMapsDetail($mapCreId);
        }else{
            $dataInfo = MetadataCsgoService::getMapsList($params);
            $dataList = $dataInfo['list'];
        }
        //初始化Es链接
        $client = EsService::initConnect();
        $indexResponse = true;
        foreach ($dataList as $k => $value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // es - 数组
            $esInfo['map_id'] = $value['map_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['status'] = $value['status'];
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $mapJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Csgo','map',$value['map_id']],$mapJson);
            // 存入 - es
            $iD = $value['map_id'];
            $client->createIndexAdd(ApiBase::CSGO_MAP_ES_INDEX,$iD,$esInfo);
        }
        // 递归分页
        if (!empty($dataList) && !$mapCreId) {
            $params['page'] = $params['page'] + 1;
            self::setMap($mapCreId,$params);
        } else {
            return $indexResponse;
        }
    }

    public static function setWeapon($weaponCreId=null,$params = ['page' => 1, 'per_page' => 50])
    {
        if($weaponCreId){
            $dataList = MetadataCsgoService::getWeaponDetail($weaponCreId);
        }else{
            $dataInfo = MetadataCsgoService::getWeaponList($params);
            $dataList = $dataInfo['list'];
        }
        //初始化Es链接
        $client = EsService::initConnect();
        $indexResponse = true;
        foreach ($dataList as $k => $value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['weapon_id'] = $value['weapon_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['status'] = $value['status'];
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['kind'] = $value['kind'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['weapon_id'];
            $client->createIndexAdd(ApiBase::CSGO_WEAPON_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $weaponJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Csgo','weapon',$value['weapon_id']],$weaponJson);
        }
        // 递归分页
        if (!empty($dataList) && !$weaponCreId) {
            $params['page'] = $params['page'] + 1;
            self::setWeapon($weaponCreId,$params);
        } else {
            return $indexResponse;
        }
    }
}