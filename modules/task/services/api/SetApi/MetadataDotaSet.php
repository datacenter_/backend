<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/03
 * Time: 14:28
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\Metadata\MetadataDotaService;
use app\modules\common\services\EsService;

class MetadataDotaSet
{
    public static function setHero($heroCreId=null,$params = ['page' => 1, 'per_page' => 50])
    {
        if($heroCreId){
            $dataList = MetadataDotaService::getHeroDetail($heroCreId);
        }else{
            $dataInfo = MetadataDotaService::getHeroList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k => $value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['hero_id'] = $value['hero_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['title'] = $value['title'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['hero_id'];
            $client->createIndexAdd(ApiBase::DOTA_HERO_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $heroJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Dota','hero',$value['hero_id']],$heroJson);
        }
        // 递归分页
        if (!empty($dataList) && !$heroCreId) {
            $params['page'] = $params['page'] + 1;
            self::setHero($heroCreId,$params);
        } else {
            return $indexResponse;
        }
    }

    public static function setItems($itemCreId=null,$params = ['page' => 1, 'per_page' => 50])
    {
        if($itemCreId){
            $dataList = MetadataDotaService::getItemsDetail($itemCreId);
        }else{
            $dataInfo = MetadataDotaService::getItemsList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k => $value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['item_id'] = $value['item_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['item_id'];
            $client->createIndexAdd(ApiBase::DOTA_ITEM_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $itemJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Dota','item',$value['item_id']],$itemJson);
        }
        // 递归分页
        if (!empty($dataList) && !$itemCreId) {
            $params['page'] = $params['page'] + 1;
            self::setItems($itemCreId,$params);
        } else {
            return $indexResponse;
        }
    }
}