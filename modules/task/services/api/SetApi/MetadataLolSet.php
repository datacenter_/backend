<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/03
 * Time: 14:28
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\Metadata\MetadataLolService;
use app\modules\common\services\EsService;

class MetadataLolSet
{
    public static function setChampions($champinonCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        if($champinonCreId){
            $dataList = MetadataLolService::getChampionsDetail($champinonCreId);
        }else{
            $dataInfo = MetadataLolService::getChampionsList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['champion_id'] = $value['champion_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['title'] = $value['title'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['champion_id'];
            $client->createIndexAdd(ApiBase::LOL_CHAMPION_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $championJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Lol','champion',$value['champion_id']],$championJson);
        }
        // 递归分页
        if(!empty($dataList) && !$champinonCreId){
            $params['page'] = $params['page']+1;
            self::setChampions($champinonCreId,$params);
        }else{
            return $indexResponse;
        }
    }


    public static function setItems($itemCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        if($itemCreId){
            $dataList = MetadataLolService::getItemDetail($itemCreId);
        }else{
            $dataInfo = MetadataLolService::getItemList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['item_id'] = $value['item_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['item_id'];
            $client->createIndexAdd(ApiBase::LOL_ITEM_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $itemJson = json_encode($value);
            ApiBase::valueSet(['Metadata','Lol','item',$value['item_id']],$itemJson);
        }
        // 递归分页
        if(!empty($dataList) && !$itemCreId){
            $params['page'] = $params['page']+1;
            self::setItems($itemCreId,$params);
        }else{
            return $indexResponse;
        }
    }

    public static function setSummonerspell($summonerspellCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        if($summonerspellCreId){
            $dataList = MetadataLolService::getSummonerspellsDetail($summonerspellCreId);
        }else{
            $dataInfo = MetadataLolService::getSummonerspellsList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['summoner_spell_id'] = $value['summoner_spell_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['summoner_spell_id'];
            $client->createIndexAdd(ApiBase::LOL_SUMMONERSPELL_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Metadata','Lol','summonerSpell',$value['summoner_spell_id']],$json);
        }
        // 递归分页
        if(!empty($dataList) && !$summonerspellCreId){
            $params['page'] = $params['page']+1;
            self::setSummonerspell($summonerspellCreId,$params);
        }else{
            return $indexResponse;
        }
    }

    public static function setRunes($runeCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        if($runeCreId){
            $dataList = MetadataLolService::getRunesDetail($runeCreId);
        }else{
            $dataInfo = MetadataLolService::getRunesList($params);
            $dataList = $dataInfo['list'];
        }
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k=>$value) {
            $value['rune_id'] = intval($value['rune_id']);
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // 存入 - es
            $esInfo['rune_id'] = $value['rune_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['external_id'] = $value['external_id'];
            $esInfo['external_name'] = $value['external_name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['path_name'] = $value['path_name'];
            $esInfo['status'] = $value['status'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['rune_id'];
            $client->createIndexAdd(ApiBase::LOL_RUNE_ES_INDEX,$iD,$esInfo);
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Metadata','Lol','rune',$value['rune_id']],$json);
        }
        // 递归分页
        if(!empty($dataList) && !$runeCreId){
            $params['page'] = $params['page']+1;
            self::setRunes($runeCreId,$params);
        }else{
            return $indexResponse;
        }
    }
}