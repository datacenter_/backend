<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/03
 * Time: 14:28
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\GetApi\OrganizationService;
use app\modules\common\services\EsService;

class OrganizationSet
{
    public static function setOrganization($organizationCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        $indexResponse = true;
        if($organizationCreId){
            $dataList = OrganizationService::getDetail($organizationCreId);
        }else{
            $dataInfo = OrganizationService::getList($params);
            $dataList = $dataInfo['list'];
        }
        //初始化Es链接
        $client = EsService::initConnect();
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // es - 数组
            $esInfo['organization_id'] = $value['organization_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['alias'] = $value['alias'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Organizations',$value['organization_id']],$json);
            $iD = $value['organization_id'];
            // 存入 - es
            $client->createIndexAdd(ApiBase::ORGANIZATION_ES_INDEX,$iD,$esInfo);
        }
        // 递归分页
        if(!empty($dataList) && !$organizationCreId){
            $params['page'] = $params['page']+1;
            self::setOrganization($organizationCreId,$params);
        }else{
            return $indexResponse;
        }
    }
}