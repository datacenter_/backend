<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\GetApi\PlayerService;
use app\modules\common\services\EsService;
use app\modules\task\services\api\ApiBase;

class PlayerSet
{
    public static function setPlayer($playerCreId=null)
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        if(is_array($playerCreId) && !empty($playerCreId)){
            for ($i = 1; $i < 9999999; $i++) {
                $data = PlayerService::getPlayerList(['page'=>$i,'per_page'=>200,'player_ids'=>$playerCreId]);
                if (empty($data['list'])) {
                    break;
                }
                $playerList = $data['list'];
                self::setEsPlayer($playerList, $client);
            }
        } else {
            if ($playerCreId) {
                // 刷新单条redis
                $playerList = PlayerService::getPlayerDetail($playerCreId);
                self::setEsPlayer($playerList, $client);
            } else {
                for ($i = 1; $i < 9999999; $i++) {
                    $data = PlayerService::getPlayerList(['page' => $i, 'per_page' => 200]);
                    if (empty($data['list'])) {
                        break;
                    }
                    $playerList = $data['list'];
                    self::setEsPlayer($playerList, $client);
                }
            }
        }
        return $indexResponse;
    }

    // 刷新es
    public static function setEsPlayer($dataList,$client)
    {
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // es - 数组
            $esInfo['player_id'] = $value['player_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['nick_name'] = $value['nick_name'];
            $esInfo['steam_id'] = $value['steam_id'];
            $esInfo['game_id'] = isset($value['game']['game_id']) ? $value['game']['game_id']:null;
            if(!empty($value['teams'])){
                $teamIds = array_column($value['teams'],'team_id');
            }else{
                $teamIds = [];
            }
            $esInfo['team_id'] = $teamIds;
            $esInfo['slug'] = $value['slug'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $player_id = $value['player_id'];
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Players',$value['player_id']],$json);
            // 存入 - es
            $client->createIndexAdd(ApiBase::PLAYER_ES_INDEX,$player_id,$esInfo);
        }
        return;
    }
}