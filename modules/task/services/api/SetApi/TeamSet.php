<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/11/03
 * Time: 14:28
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\ApiBase;
use app\modules\task\services\api\GetApi\TeamService;
use app\modules\common\services\EsService;

class TeamSet
{
    public static function setTeam($teamCreId=null)
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        if(is_array($teamCreId) && !empty($teamCreId)){
            for ($i = 1; $i < 9999999; $i++) {
                $data = TeamService::getTeamList(['page' => $i,'per_page' => 200,'team_ids'=>$teamCreId]);
                if (empty($data['list'])) {
                    break;
                }
                $dataList = $data['list'];
                self::setEsTeam($dataList, $client);
            }
        }else {
            if ($teamCreId) {
                $dataList = TeamService::getTeamDetail($teamCreId);
                self::setEsTeam($dataList, $client);
            } else {
                for ($i = 1; $i < 9999999; $i++) {
                    $data = TeamService::getTeamList(['page' => $i, 'per_page' => 200]);
                    if (empty($data['list'])) {
                        break;
                    }
                    $dataList = $data['list'];
                    self::setEsTeam($dataList, $client);
                }
            }
        }
        return $indexResponse;
    }
    // 刷新es / redis
    public static function setEsTeam($dataList,$client)
    {
        foreach ($dataList as $k=>$value) {
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // es - 数组
            $esInfo['team_id'] = $value['team_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['game_id'] = isset($value['game']['game_id']) ? $value['game']['game_id']:null;
            $esInfo['organization_id'] = isset($value['organization']['organization_id']) ? $value['organization']['organization_id']:null;
            $esInfo['slug'] = $value['slug'];
            $esInfo['alias'] = $value['alias'];
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            $iD = $value['team_id'];
            // 存入 - redis
            $value = ApiBase::convertNull($value);
            $json = json_encode($value);
            ApiBase::valueSet(['Teams',$value['team_id']],$json);
            // 存入es
            $client->createIndexAdd(ApiBase::TEAM_ES_INDEX,$iD,$esInfo);
        }
        return;
    }
}