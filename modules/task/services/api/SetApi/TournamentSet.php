<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/28
 * Time: 22:13
 */

namespace app\modules\task\services\api\SetApi;

use app\modules\task\services\api\GetApi\TournamentService;
use app\modules\common\services\EsService;
use app\modules\task\services\api\ApiBase;

class TournamentSet
{
    public static function setTournament($tournamentCreId=null,$params = ['page' => 1,'per_page' => 50])
    {
        $indexResponse = true;
        //初始化Es链接
        $client = EsService::initConnect();
        if($tournamentCreId){
            $dataList = TournamentService::getTournamentDetail($tournamentCreId);
        }else{
            $data = TournamentService::getTournamentList($params);
            $dataList = $data['list'];
        }
        foreach ($dataList as $k=>$value) {
            print("Super记录log开始...")."\n\n";
            print("当前赛事为：{$value['tournament_id']}...")."\n";
            $deleted = $value['deleted'];
            unset($value['deleted']);
            // es-数组
            $esInfo['tournament_id'] = $value['tournament_id'];
            $esInfo['deleted'] = $deleted;
            $esInfo['name'] = $value['name'];
            $esInfo['game_id'] = isset($value['game']['game_id']) ? $value['game']['game_id']:null;
            $esInfo['status'] = $value['status'];
            $esInfo['slug'] = $value['slug'];
            $esInfo['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($value['scheduled_begin_at']));
            $esInfo['modified_at'] = date('Y-m-d H:i:s',strtotime($value['modified_at']));
            // 刷新redis
            $value = ApiBase::convertNull($value);
            $tournamentJson = json_encode($value);
            print("现在正在存储redis数据...")."\n";
            ApiBase::valueSet(['Tournaments',$value['tournament_id']],$tournamentJson);
            print("现在正在存储es数据...")."\n";
            // 刷新es
            $client->createIndexAdd(ApiBase::TOURNAMENT_ES_INDEX,$value['tournament_id'],$esInfo);
        }
        // 递归分页
        if(!empty($dataList) && !$tournamentCreId){
            $params['page'] = $params['page']+1;
            self::setTournament($tournamentCreId,$params);
        }else{
            return $indexResponse;
        }
    }

}