<?php
/**
 *
 */

namespace app\modules\task\services\apiDataSync;

use app\modules\task\services\api\RenovateService;

class RefreshDataApi
{
    public static function run($tagInfo,$taskInfo)
    {
        $params = json_decode($taskInfo['params'],true);
        for ($i = 0;$i<=2;$i++) {
            try {
                //调用刷新api变动结果
                RenovateService::refurbishApi($params['api_type'], $params['api_id'], $params['game_id'],false);
                break;
            }catch (\Exception $e){
                continue;
            }
        }
        return true;
    }
}
