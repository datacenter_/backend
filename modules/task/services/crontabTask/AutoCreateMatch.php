<?php


namespace app\modules\task\services\crontabTask;


use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\TaskInterface;
use app\rest\exceptions\BusinessException;

class AutoCreateMatch extends CrontabBase implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        self::setLogName();
        // 获取信息
        $runModel=@$tagInfo['ext_type'];
        if($runModel=='test'){
            $debugType=true;
        }else{
            $debugType=false;
        }
        // 所有需要刷新的tournament
        for ($i = 0; $i < 100000000; $i++) {
            $tournamentList = self::getTournamentList($i);
            if(!$tournamentList){
                break;
            }
            foreach ($tournamentList as $tournamentInfo) {
                // 一个tournament下面的所有match
                $matchList = self::getStandardMatchInfosByTournament($tournamentInfo);
                foreach ($matchList as $matchInfo) {
                    try{
                        self::setLog("start deal with ".$matchInfo['std_match_id']);
                        self::dealWithMatch($matchInfo, $tournamentInfo, $debugType);
                    }catch (BusinessException $e){
                        self::setLog($e->getMessage());
                        self::setLog($e->getData());
                    }catch (\Exception $e){
                        self::setLog($e->getMessage());
                    }
                }
            }
        }
        $logs=self::getLogTrance();
        return [
            'logs'=>$logs
        ];
    }

    public static function dealWithMatch($matchInfo, $tournamentInfo, $debugType = true)
    {

//        $matchInfo = array(
//            'std_match_id' => '1180',
//            'std_match_deal_status' => '1',
//            'game_id' => '2',
//            'rel_identity_id' => '565115',
//            'scheduled_begin_at' => NULL,
//            'trmt_master_id' => '9',
//            'tm1_master_id' => NULL,
//            'tm2_master_id' => NULL,
//            'like_ids' => NULL,
//        );

//        $tournamentInfo=array (
//            'id' => '701',
//            'origin_id' => '3',
//            'rel_identity_id' => '2738',
//            'rel_league_id' => '294',
//            'main_tournament_id' => '9',
//            'create_origin_id' => '3',
//            'create_update_type' => '1',
//            'binding_origin_id' => '1',
//            'binding_update_type' => '1',
//        );
        // 根据关联状态，确定关联关系
        if ($matchInfo['std_match_deal_status'] == 4) {
            throw new BusinessException($matchInfo, '当前match为已绑定，不能操作');
        }

        if ($matchInfo['tm1_master_id'] && $matchInfo['tm2_master_id'] && $matchInfo['scheduled_begin_at']) {
            // 相关字段已经确认，这里会操作绑定或者新增，取决于相似的id是否存在
            $likeIds =[];
            if($matchInfo['like_ids']){
                $likeIds = explode(",", $matchInfo['like_ids']);
            }
            if (count($likeIds) > 1) {
                throw new BusinessException($matchInfo, '同一个match下有多个相似的主表id，这里不能更新了');
            }
            if (count($likeIds) == 1) {
                // 添加绑定关系
                $masterId = $likeIds[0];
                $standardId = $matchInfo['std_match_id'];
                self::addBinding($masterId, $standardId, $tournamentInfo, $debugType);
            }
            if (!$likeIds) {
                // 使用数据源新增方法
                $standardId = $matchInfo['std_match_id'];
                self::addAndBinding($standardId, $tournamentInfo, $debugType);
            }

        } else {
            // 相关字段没绑定，不能确定有效值
            throw new BusinessException($matchInfo, 'match的绑定项缺失');
        }
    }

    public static function addBinding($masterId, $standardId, $tournamentInfo, $debugType = true)
    {
        // 检查配置
        if($tournamentInfo['binding_update_type']!=Consts::UPDATE_TYPE_AUTO){
            throw new BusinessException([],"本游戏类型不自动创建绑定关系,as game_id:".$tournamentInfo['game_id']);
        }
        if($debugType){
            self::setLog(sprintf("【debug】添加绑定关系masterId:%s,standardId:%s",$masterId,$standardId));
        }else{
            self::setLog(sprintf("【update】添加绑定关系masterId:%s,standardId:%s",$masterId,$standardId));
            BindingMatchService::setBinding($standardId,$masterId);
        }
    }

    public static function addAndBinding($standardId, $tournamentInfo, $debugType = true)
    {
        // 检查配置
        if(!($tournamentInfo['create_update_type']==Consts::UPDATE_TYPE_AUTO
            && $tournamentInfo['origin_id']==$tournamentInfo['create_origin_id'] )){
            throw new BusinessException([
                'origin_id'=>$tournamentInfo['origin_id'],
                'create_update_type'=>$tournamentInfo['create_update_type'],
                'create_origin_id'=>$tournamentInfo['create_origin_id'],
                ],
                "本游戏类型不自动创建比赛");
        }

        if($debugType){
            self::setLog(sprintf("【debug】创建比赛standardId:%s",$standardId));
        }else{
            self::setLog(sprintf("【update】创建比赛standardId:%s",$standardId));
            BindingMatchService::addAndBinding($standardId);
        }
    }

    public static function getStandardMatchInfosByTournament($tournamentInfo)
    {
        $sqlTpl=self::getSqlTemplate();
        $sql = $sqlTpl . sprintf(" and std_match.tournament_id = '%s' and std_match.origin_id= %s \n and std_match.scheduled_begin_at<date_add(now(), INTERVAL 30 day) \n group by std_match.id", $tournamentInfo['rel_identity_id'], $tournamentInfo['origin_id']);
        $matchList = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        return $matchList;
    }

    public static function getSqlTemplate()
    {
        // 这个sql用在这里和刷新推荐绑定关系，最好不要在上面添加条件或者做改动
        $sqlTpl = <<<SQL
SELECT
    std_match.id as std_match_id,
    std_match.deal_status as std_match_deal_status,
    std_match.game_id as game_id,
    std_match.rel_identity_id as rel_identity_id,
    std_match.scheduled_begin_at as scheduled_begin_at,
    rel_trmt.master_id as trmt_master_id,
    rel_tm1.master_id as tm1_master_id,
    rel_tm2.master_id as tm2_master_id,
    group_concat(main_match_like.id)  as like_ids
--     std_match.*
FROM standard_data_match std_match
-- 标准表赛事，team1,team2
         LEFT JOIN standard_data_tournament std_trmt ON std_match.tournament_id = std_trmt.rel_identity_id
         and std_match.origin_id = std_trmt.origin_id and std_match.game_id = std_trmt.game_id and std_trmt.type = '1'
    AND std_match.origin_id = std_trmt.origin_id
         LEFT JOIN standard_data_team std_tm1 ON std_match.team_2_id = std_tm1.rel_identity_id
    AND std_match.origin_id = std_tm1.origin_id
         LEFT JOIN standard_data_team std_tm2 ON std_match.team_1_id = std_tm2.rel_identity_id
    AND std_match.origin_id = std_tm2.origin_id
-- 标准表赛事，team1,team2的关联关系
         LEFT JOIN data_standard_master_relation rel_trmt ON rel_trmt.standard_id = std_trmt.id
    AND rel_trmt.resource_type = 'tournament'
         LEFT JOIN data_standard_master_relation rel_tm1 ON rel_tm1.standard_id = std_tm2.id
    AND rel_tm1.resource_type = 'team'
         LEFT JOIN data_standard_master_relation rel_tm2 ON rel_tm2.standard_id = std_tm1.id
    AND rel_tm2.resource_type = 'team'
--  相似的主表比赛，可能有多个，如果一个比赛下有多个，可能会出问题
         LEFT JOIN `match` main_match_like ON main_match_like.tournament_id = rel_trmt.master_id
    AND main_match_like.deleted = 2
    AND ((main_match_like.team_1_id = rel_tm2.master_id
        AND main_match_like.team_2_id = rel_tm1.master_id
        AND main_match_like.scheduled_begin_at BETWEEN date_add(std_match.scheduled_begin_at, INTERVAL -30 MINUTE)
              AND date_add(std_match.scheduled_begin_at, INTERVAL 30 MINUTE))
        or (main_match_like.team_1_id = rel_tm1.master_id
            AND main_match_like.team_2_id = rel_tm2.master_id
            AND main_match_like.scheduled_begin_at BETWEEN date_add(std_match.scheduled_begin_at, INTERVAL - 1 HOUR)
                AND date_add(std_match.scheduled_begin_at, INTERVAL 1 HOUR))
                                             )
where 1
  and std_match.deal_status<>4
SQL;
        return $sqlTpl;
    }

    public static function getBdMatchSqlTemplate()
    {
        // 这个sql用在这里和刷新推荐绑定关系，最好不要在上面添加条件或者做改动
        $sqlTpl = <<<SQL
SELECT
    std_match.id as std_match_id,
    std_match.deal_status as std_match_deal_status,
    std_match.game_id as game_id,
    std_match.rel_identity_id as rel_identity_id,
    std_match.scheduled_begin_at as scheduled_begin_at,
    rel_trmt.master_id as trmt_master_id,
    rel_tm1.master_id as tm1_master_id,
    rel_tm2.master_id as tm2_master_id,
    group_concat(main_match_like.id)  as like_ids
--     std_match.*
FROM standard_data_match std_match
-- 标准表赛事，team1,team2
         LEFT JOIN standard_data_tournament std_trmt ON std_match.tournament_id = std_trmt.rel_identity_id
         and std_match.origin_id = std_trmt.origin_id and std_match.game_id = std_trmt.game_id and std_trmt.type = '1'
    AND std_match.origin_id = std_trmt.origin_id
         LEFT JOIN standard_data_team std_tm1 ON std_match.team_2_id = std_tm1.rel_identity_id
    AND std_match.origin_id = std_tm1.origin_id
         LEFT JOIN standard_data_team std_tm2 ON std_match.team_1_id = std_tm2.rel_identity_id
    AND std_match.origin_id = std_tm2.origin_id
-- 标准表赛事，team1,team2的关联关系
         LEFT JOIN data_standard_master_relation rel_trmt ON rel_trmt.standard_id = std_trmt.id
    AND rel_trmt.resource_type = 'tournament'
         LEFT JOIN data_standard_master_relation rel_tm1 ON rel_tm1.standard_id = std_tm2.id
    AND rel_tm1.resource_type = 'team'
         LEFT JOIN data_standard_master_relation rel_tm2 ON rel_tm2.standard_id = std_tm1.id
    AND rel_tm2.resource_type = 'team'
--  相似的主表比赛，可能有多个，如果一个比赛下有多个，可能会出问题
         LEFT JOIN `match` main_match_like ON main_match_like.tournament_id = rel_trmt.master_id
    AND main_match_like.deleted = 2
    AND ((main_match_like.team_1_id = rel_tm2.master_id
        AND main_match_like.team_2_id = rel_tm1.master_id
        AND main_match_like.scheduled_begin_at BETWEEN date_add(std_match.scheduled_begin_at, INTERVAL -30 MINUTE)
              AND date_add(std_match.scheduled_begin_at, INTERVAL 30 MINUTE))
        or (main_match_like.team_1_id = rel_tm1.master_id
            AND main_match_like.team_2_id = rel_tm2.master_id
            AND main_match_like.scheduled_begin_at BETWEEN date_add(std_match.scheduled_begin_at, INTERVAL - 1 HOUR)
                AND date_add(std_match.scheduled_begin_at, INTERVAL 1 HOUR))
                                             )
where 1
  and std_match.deal_status = 4
  and std_match.status < 3
SQL;
        return $sqlTpl;
    }

    public static function getTournamentList($page = 1, $pageSize = 200)
    {
        $q = StandardDataTournament::find()
            ->select(['std_trmt.*',
                'trmt_rel.master_id as main_tournament_id',
                'config_create.origin_id as create_origin_id',
                'config_create.update_type as create_update_type',
                'config_binding.origin_id as binding_origin_id',
                'config_binding.update_type as binding_update_type',
                ])
            ->alias('std_trmt')
            ->leftJoin('data_standard_master_relation trmt_rel', 'std_trmt.id=trmt_rel.standard_id and trmt_rel.resource_type="tournament"')
            ->leftJoin('data_update_config_default config_create', 'config_create.game_id=std_trmt.game_id and config_create.slug="match.create_data"')
            ->leftJoin('data_update_config_default config_binding', 'config_binding.game_id=std_trmt.game_id and config_binding.slug="match.binding_data"')
            ->leftJoin('tournament m_trmt', 'm_trmt.id=trmt_rel.master_id')
            ->where(['>', 'trmt_rel.id', 0])
            ->andWhere(['<>', 'm_trmt.status', 3])
//            ->leftJoin('data_standard_master_match std_match','std_trmt.id=std_match.tournment_id')
//            ->leftJoin('tournament as main_trmt','main_trmt.id=trmt_rel.master_id')
        ;
        $sql=$q->createCommand()->getRawSql();
        $list = $q->asArray()->limit($pageSize)->offset($pageSize * ($page - 1))
            ->all();
        return $list;

    }
}
