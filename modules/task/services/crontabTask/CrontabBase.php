<?php


namespace app\modules\task\services\crontabTask;


use app\modules\common\services\Consts;
use app\modules\task\models\StandardDataTournament;
use app\rest\exceptions\BusinessException;

class CrontabBase
{
    public static function setLogName($logName="")
    {
        Log::init(get_called_class());
    }
    public static function setLog($info)
    {
        Log::info($info);
//        echo sprintf("%s|%s\n",date("Y-m-dH:i:s"),is_array($info)?json_encode($info):$info);
    }
    public static function getLogTrance()
    {
        return Log::getLogTrance();
    }
}