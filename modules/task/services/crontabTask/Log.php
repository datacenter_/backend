<?php


namespace app\modules\task\services\crontabTask;


use app\modules\common\services\Consts;
use app\modules\task\models\StandardDataTournament;
use app\rest\exceptions\BusinessException;

class Log
{
    static private $instance;  //  私有静态属性用以保存对象
    private $name;
    private $logs="";

    //私有属性的构造方法 防止被 new
    private function __construct ()
    {
    }

    //私有属性的克隆方法 防止被克隆
    private function __clone ()
    {

    }

    //静态方法 用以实例化调用
    static public function getInstance()
    {
        if (!self::$instance instanceof self)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function info($info)
    {
        return self::getInstance()->addLog($info);
    }

    public static function getLogTrance()
    {
        return self::getInstance()->getLogs();
    }

    public static function init($name)
    {
        self::getInstance()->setLogName($name);
    }

    public function setLogName($name)
    {
        $this->name=$name;
        $this->logs="";
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function addLog($info)
    {
        $logLine=sprintf("%s|%s\n",date("Y-m-dH:i:s"),is_array($info)?json_encode($info):$info);
        // 记录文本log

        $this->logs.=$logLine;
    }

}