<?php


namespace app\modules\task\services\crontabTask;


use app\modules\common\models\HelperImageToAli;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\org\models\Player;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\rest\exceptions\BusinessException;

class RefreshImage extends CrontabBase implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        self::setLogName();
        // 获取信息
        $params = json_decode($taskInfo['params'], true);
        self::refreshImage($params['table'], $params['col'], $params['id']);
        $logs = self::getLogTrance();
        return [
            'logs' => $logs
        ];
    }

    public static function refreshImage($table, $col, $id)
    {
        $sqlGetTpl = "select `%s` as image_url from `%s` where id=%s";
        $sqlGet = sprintf($sqlGetTpl, $col, $table, $id);
        self::setLog("getSql:" . $sqlGet);
        $info = \Yii::$app->getDb()->createCommand($sqlGet)->queryOne();
        $imageUrl = $info['image_url'];
        self::setLog("image:" . $imageUrl);
        $urlNew = ImageConversionHelper::mvImageToAli($imageUrl);
        $sqlUpdate = sprintf("update %s set %s='%s' where id=%s", $table, $col, $urlNew, $id);
        self::setLog("setSql:" . $sqlUpdate);
        $info = \Yii::$app->getDb()->createCommand($sqlUpdate)->execute();
        self::setLog("execStatus:" . $info);
    }

    public static function addTask($table,$col,$id)
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_CRONTAB,
                "", $table, "", $col,$id),
            "type" =>\app\modules\task\services\crontabTask\RefreshImage::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,"",''),
            "params" => [
                'table'=>$table,
                'col'=>$col,
                'id'=>$id
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($item);
    }

    public static function refresh($table,$col,$limit)
    {
        $sql=sprintf("select id from %s where %s like 'http:%%' limit %s",$table,$col,$limit);
        $list=$info = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        foreach($list as $key=>$val){
            self::addTask($table,$col,$val['id']);
        }
    }
}
