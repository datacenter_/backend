<?php
/**
 *
 */

namespace app\modules\task\services\esHotRefresh;


use app\modules\task\services\api\RenovateService;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardCommon;

class HotRefresh5eDispatch
{
    public static function run($tagInfo,$taskInfo)
    {
        // 暂时没用
//        $resourceType=$tagInfo["resource_type"];
//        $originType=$tagInfo["origin_type"];
//        $extType=$tagInfo['ext_type'];
        $info = json_decode($taskInfo['params'], true);
        // 刷新这个比赛
        if(isset($info['match_id']) && $info['match_id']) {
            // 刷新battle
            RenovateService::refurbishApi('csgo_battle', $info['match_id']);
        }
        return true;
    }

}