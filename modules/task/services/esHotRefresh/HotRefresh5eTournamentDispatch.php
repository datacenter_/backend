<?php
/**
 *
 */

namespace app\modules\task\services\esHotRefresh;


use app\modules\task\services\api\RenovateService;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardCommon;

class HotRefresh5eTournamentDispatch
{
    public static function run($tagInfo,$taskInfo)
    {
        // 暂时没用
//        $resourceType=$tagInfo["resource_type"];
//        $originType=$tagInfo["origin_type"];
//        $extType=$tagInfo['ext_type'];
        $info = json_decode($taskInfo['params'], true);
        // 刷新这个赛事统计
        if(isset($info['tournament_id']) && $info['tournament_id']) {
            // 刷新battle
            RenovateService::refurbishApi('csgo_tournament', $info['tournament_id']);
        }
        return true;
    }
}