<?php
/**
 *
 */

namespace app\modules\task\services\esHotRefresh;


use app\modules\common\services\Consts;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\mainIncrement\MainClan;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardCommon;

class RefreshEsApiDispatch
{
    public static function run($tagInfo,$taskInfo)
    {
        // 暂时没用
//        $resourceType=$tagInfo["resource_type"];
        $originType=$tagInfo["origin_type"];
        $matchId=$tagInfo['ext_type'];
        $resourceType=$tagInfo["resource_type"];
        RenovateService::refurbishApi($resourceType, $matchId);//调用刷新比赛结果

        return true;
    }

}
