<?php
/**
 *
 */

namespace app\modules\task\services\exceptions;


use Throwable;

class TaskClassNameException extends TaskException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}