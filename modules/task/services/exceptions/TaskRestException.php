<?php
/**
 *
 */

namespace app\modules\task\services\exceptions;


use Throwable;

class TaskRestException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if(!is_string($message)){
            $message=json_encode($message);
        }
        parent::__construct($message, $code, $previous);
    }
}