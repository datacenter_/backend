<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;


use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\tasks\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class AbiosBase
{
    const ABIOS_TOKEN_CACEH_KEY = "abios_token_caceh_key";
    public static $config=[
        'base_url'=>'atlas.abiosgaming.com',
        'abios_secret'=>'acf7f87dca034c8c873601d4da2f09b8',
    ];
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $action=$taskParams["action"];
        $params=$taskParams["params"];

        return self::getCurlInfo($action,$params);
    }

    public static function getCurlInfo($action,$params,$header=[])
    {
        $url="https://".self::$config["base_url"].$action;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Abios-Secret"=>self::$config['abios_secret']
        ];

        return Common::requestGet($url,$header,$params);
    }

    public static function getAuthKey()
    {
        // 这里用cache存一下token
        $cache = \Yii::$app->cache;
        $authKey=$cache->get(self::ABIOS_TOKEN_CACEH_KEY);
        if (!$authKey) {
            $params=[
                'grant_type'=>'client_credentials',
                'client_id'=>self::$config["client_id"],
                'client_secret'=>self::$config["client_secret"]
            ];
            $url="http://".self::$config["base_url"].'/v2/oauth/access_token';
            $infoJson = Common::requestPost($url,[],$params);
            $info=json_decode($infoJson,true);
            if($info["access_token"] && $info["expires_in"]){
                $cache->set(self::ABIOS_TOKEN_CACEH_KEY,$info["access_token"],$info["expires_in"]-10);
                return $info["access_token"];
            }else{
                throw new TaskRestException($infoJson);
            }
        }else{
            return $authKey;
        }
    }

    public static function refreshTeam($params,$batchId)
    {
        $teamId=$params["team_id"];
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $params["team_id"]=(string)$params["team_id"];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $teamInfo=TaskDataFeijingTeam::find()->where(["team_id"=>$teamId])->one();
        if($teamInfo){
            $oldInfo=$teamInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $teamInfo=new TaskDataFeijingTeam();
        }
        $teamInfo->setAttributes($params);
        // 更新数据，推送消息
        $teamInfo->save();
        if($teamInfo->getErrors()){
            print_r($teamInfo->getErrors());
            throw new TaskException(json_encode($teamInfo->getErrors()));
        }
        $newInfo=$teamInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TEAM,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return [$taskInfo];
    }

    public static function refreshPlayer($playerID,$playerInfo)
    {

    }

    public static function getGameIdByName($name)
    {

    }


}