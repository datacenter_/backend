<?php

namespace app\modules\task\services\grab\abios;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosCSWeaponsList extends AbiosBase implements TaskInterface,TaskCatchInterface
{

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if(empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];
        // 这里直接添加到player
        foreach ($responseInfo['weapons'] as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId,$refresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$refresh = "")
    {
        // 刷新player
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_CSGO_WEAPON, 1, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::METADATA_TYPE_CSGO_WEAPON,
                $diffInfo['change_type'],
                1); //1表示cs
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}