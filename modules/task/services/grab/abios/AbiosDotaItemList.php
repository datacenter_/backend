<?php
/**
 * 返回dota道具，技能，英雄
 * 请求url:  /v2/games/1/assets
 */
namespace app\modules\task\services\grab\abios;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosDotaItemList extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps=[];
        foreach ($responseInfo['items'] as $item){  //道具处理
            $subStep = self::addItemAndGetSubInfo($item, $batchId,$refresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        foreach ($responseInfo['spells'] as $spell){   //技能处理
            $subStep = self::addSpellsAndGetSubInfo($spell,$batchId,$refresh);
            if($subStep) {
                $subSteps[] = $subStep;
            }
        }
        foreach ($responseInfo['heroes'] as $heroes){   //英雄处理
            $subStep = self::addHeroesAndGetSubInfo($heroes,$batchId,$refresh);
            if($subStep) {
                $subSteps[] = $subStep;
            }
        }
        return $subSteps;
    }
    //dota道具处理
    public static function addItemAndGetSubInfo($item, $batchId,$refresh="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_DOTA2_ITEM, 3, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::METADATA_TYPE_DOTA2_ITEM,
                $diffInfo['change_type'],
                3);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
    //处理dota的技能
    public static function addSpellsAndGetSubInfo($item, $batchId,$refresh="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        // 区分dota2天赋还是技能
        if(strpos($item['name'],'special_bonus')!==false){
            $resourceType = Consts::METADATA_TYPE_DOTA2_TALENT;
        }else{
            $resourceType = Consts::METADATA_TYPE_DOTA2_ABILITY;
        }
        $diffInfo = Common::setHotLog($originInfo['id'], $resourceType, 3, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                $resourceType,
                $diffInfo['change_type'],
                3);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
    //处理dota英雄
    public static function addHeroesAndGetSubInfo($item, $batchId,$refresh="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_DOTA2_HERO, 3, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::METADATA_TYPE_DOTA2_HERO,
                $diffInfo['change_type'],
                3);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}