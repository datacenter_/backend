<?php


namespace app\modules\task\services\grab\abios;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosMatchList extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps =[];
        foreach ($responseInfo['data'] as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if (count($responseInfo['data'])) {
//            $pageNext = $taskInfo->toArray();
//            $params = json_decode($taskInfo["params"], true);
//            $paramsNext = $params;
//            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
//            $pageNext['params'] = $paramsNext;
//            $subSteps[] = $pageNext;

            $pageNext = $taskInfo->toArray();
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            if( $paramsNext['params']['page']>30){

            }else{
                $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId)
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, $gameInfo['id'], $identityId, $item);
        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                QueueServer::QUEUE_RESOURCE_MATCH,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}