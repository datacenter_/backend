<?php


namespace app\modules\task\services\grab\abios;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosTournament extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];
        $subStep = self::addItemAndGetSubInfo($responseInfo, $batchId);
        if ($subStep) {
            $subSteps = array_merge($subSteps, $subStep);
        }
        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId)
    {
        // 刷新player
        $subs = [];
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TOURNAMENT, $gameInfo['id'], $identityId, $item);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                QueueServer::QUEUE_RESOURCE_TOURNAMENT,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            $subs[] = $taskInfo;
        }

        if (isset($item['stages'])) {
            foreach ($item['stages'] as $stages) {
//                $subDiffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_SON_TOURNAMENT, $gameInfo['id'], $stages['id'], $stages);
//                if ($subDiffInfo['changed']) {
//                    $subTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
//                        QueueServer::QUEUE_ORIGIN_ABIOS,
//                        Consts::RESOURCE_TYPE_SON_TOURNAMENT,
//                        $diffInfo['change_type'],
//                        $gameInfo['id']);
//                    $subTask = [
//                        "tag" => $subTag,
//                        "batch_id" => $batchId,
//                        "params" => $subDiffInfo,
//                    ];
//                    $subs[] = $subTask;
//                }

                //第三级任务
                if (isset($stages['substages'])) {
                    foreach ($stages['substages'] as $substages) {
                        $substagesDiffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT, $gameInfo['id'], $substages['id'], $substages);
                        if ($substagesDiffInfo['changed']) {
                            $subTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                                QueueServer::QUEUE_ORIGIN_ABIOS,
                                Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT,
                                $diffInfo['change_type'],
                                $gameInfo['id']);
                            $subTask = [
                                "tag" => $subTag,
                                "batch_id" => $batchId,
                                "params" => $substagesDiffInfo,
                            ];
                            $subs[] = $subTask;
                        }
                    }
                }
            }
        }

        return $subs;
    }

}