<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;


use app\modules\common\services\Consts;
use app\modules\task\models\abios\TaskDataHero;
use app\modules\task\models\abios\TaskDataProp;
use app\modules\task\models\abios\TaskDataRune;
use app\modules\task\models\abios\TaskDataSpells;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class DotaAssets extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($originInfo, $taskInfo)
    {
        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // items
        $prop = $responseInfo['items'];
        unset($responseInfo['items']);
        foreach ($prop as $val) {
            $props[] = self::refreshProp($val,$batchId);
        }

        // dota 目前不需要
//        $runes = $responseInfo['runes'];
//        unset($responseInfo['runes']);
//        foreach ($runes as $val) {
//            $taskInfo[] = self::refreshRune($val,$batchId);
//        }

        $spells = $responseInfo['spells'];
        unset($responseInfo['spells']);
        foreach ($spells as $val) {
            $spell[] = self::refreshSkill($val,$batchId);
        }

        $heroes = $responseInfo['heroes'];
        unset($responseInfo['heroes']);
        foreach ($heroes as $val) {
            $heroe[] = self::refreshHero($val,$batchId);

        }

        return array_merge($props,$spell,$heroe);
    }

    private static function refreshSkill($skillInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $skillInfo["game_id"]=3;
        $skillInfo["images"]=$skillInfo["images"]['default'];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $skill=TaskDataSpells::find()->where(["id"=>$skillInfo["id"]])->one();
        if($skill){
            $oldInfo=$skill->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $skill=new TaskDataSpells();
        }
        $skill->setAttributes($skillInfo);
        // 更新数据，推送消息
        $skill->save();
        if($skill->getErrors()){
            print_r($skill->getErrors());
            throw new TaskException(json_encode($skill->getErrors()));
        }
        $newInfo=$skill->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_SKILL,
            $changeType,
            Consts::METADATA_TYPE_DOTA2_ABILITY);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }
//    private static function refreshRune($runeInfo,$batchId)
//    {
//        $diff=[];
//        $oldInfo=[];
//        $newInfo=[];
//        $runeInfo["game_id"]=3;
//        $runeInfo["images"]=json_encode($runeInfo['images']);
//        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
//        $rune=TaskDataRune::find()->where(["id"=>$runeInfo["id"]])->one();
//
//        if($rune){
//            $oldInfo=$rune->toArray();
//        }else{
//            $changeType=QueueServer::QUEUE_TYPE_ADD;
//            $rune=new TaskDataRune();
//        }
//
//        $rune->setAttributes($runeInfo);
//        // 更新数据，推送消息
//        $rune->save();
//        if($rune->getErrors()){
//            print_r($rune->getErrors());
//            throw new TaskException(json_encode($rune->getErrors()));
//        }
//        $newInfo=$rune->toArray();
//        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
//            $diff=Common::diffParams($oldInfo,$newInfo);
//            if (empty($diff)) {
//                return [];
//            }
//        }
//        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
//            QueueServer::QUEUE_ORIGIN_ABIOS,
//            QueueServer::QUEUE_RESOURCE_RUNE,
//            $changeType,
//            "");
//        $taskInfo=[
//            "tag"=>$tag,
//            "batch_id"=>$batchId,
//            "params"=>[
//                "diff"=>$diff,
//                "new"=>$newInfo,
//                "old"=>$oldInfo
//            ],
//        ];
//        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
//    }
    private static function refreshHero($heroInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $heroInfo["game_id"]=3;
        $heroInfo["large_image"]=$heroInfo['images']['large'];
        $heroInfo["small_image"]=$heroInfo['images']['small'];
        unset($heroInfo['images']);

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $hero=TaskDataHero::find()->where(["id"=>$heroInfo["id"]])->one();
        if($hero){
            $oldInfo=$hero->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $hero=new TaskDataHero();
        }
        $hero->setAttributes($heroInfo);
        // 更新数据，推送消息
        $hero->save();
        if($hero->getErrors()){
            print_r($hero->getErrors());
            throw new TaskException(json_encode($hero->getErrors()));
        }
        $newInfo=$hero->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
            if (empty($diff)) {
                return [];
            }
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_HERO,
            $changeType,
            Consts::METADATA_TYPE_DOTA2_HERO);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }

    private static function refreshProp($propInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];

        $propInfo["game_id"]=3;
        $propInfo["images"]=json_encode($propInfo['image']);
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $prop=TaskDataProp::find()->where(["id"=>$propInfo["id"]])->one();
        if($prop){
            $oldInfo=$prop->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $prop=new TaskDataProp();
        }
        $prop->setAttributes($propInfo);
        // 更新数据，推送消息
        $prop->save();
        if($prop->getErrors()){
            print_r($prop->getErrors());
            throw new TaskException(json_encode($prop->getErrors()));
        }
        $newInfo=$prop->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
            if (empty($diff)) {
                return [];
            }
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_PROP,
            $changeType,
            Consts::METADATA_TYPE_DOTA2_ITEM);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }
}