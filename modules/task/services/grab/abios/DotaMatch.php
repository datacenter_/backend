<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;

use app\modules\task\models\abios\TaskDataMatch;
use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class DotaMatch extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }

        // 更新比赛信息
        $tasks = self::refreshMatch($responseInfo,$batchId);

        // 对局信息
        // 主播基本信息
        if (isset($responseInfo['casters']) || !empty($responseInfo['casters'])) {

            // Stream - 流信息
        }

        // 比赛对阵表位置信息
        if (isset($responseInfo['bracket_pos']) || !empty($responseInfo['bracket_pos'])) {

        }
        return $tasks;
    }

    private static function refreshMatch($matchInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $matchData["id"]=$matchInfo['id'];
        $matchData["title"]=$matchInfo['title'];
        $matchData["best_of"]=$matchInfo['bestOf'];
        $matchData["start"]=$matchInfo['start'];
        $matchData["end"]=$matchInfo['end'];
        $matchData["postponed_from"]=$matchInfo['postponed_from'];
        $matchData["deleted_at"]=$matchInfo['deleted_at'];
        $matchData["streamed"]=$matchInfo['streamed'] ? 1 : 2;
        $matchData["substage_id"]=$matchInfo['substage_id'];
        $matchData["pbp_status"]=$matchInfo['pbp_status'];
        $matchData["team_1_id"]=$matchInfo['seeding'][1] ?? '';
        $matchData["team_2_id"]=$matchInfo['seeding'][2] ?? '';
        $matchData["tournament_id"]=$matchInfo['tournament_id'];
        if ($matchInfo['game']['id'] == 1) {
            $matchData["game"] = 3;
        }

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $match=TaskDataMatch::find()->where(["id"=>$matchData["id"]])->one();
        if($match){
            $oldInfo=$match->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $match=new TaskDataMatch();
        }
        $match->setAttributes($matchData);
        // 更新数据，推送消息
        $match->save();
        if($match->getErrors()){
            print_r($match->getErrors());
            throw new TaskException(json_encode($match->getErrors()));
        }
        $newInfo=$match->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_MATCH,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}