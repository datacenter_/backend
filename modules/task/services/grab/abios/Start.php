<?php
/**
 *
 */
namespace app\modules\task\services\grab\abios;

use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class Start
{
    // 此方法用于启动，刷新
    public static function run(){
        $startTask=[
            //道具，技能，英雄等结合的接口
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\abios\AbiosDotaItemList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/games/1/assets",  //dota道具url，1表示dota的游戏id，2表示lol，5表示csgo
//                    "params" => [
//                        "page" => 1,
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\abios\AbiosCSWeaponsList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/games/5/assets",  //5表示csgo
//                    "params" => [
//                        "page" => 1,
//                    ]
//                ],
//            ],
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\abios\AbiosLOLItemList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/games/2/assets",  //dota道具url，1表示dota的游戏id
//                    "params" => [
//                        "page" => 1,
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => AbiosPlayerList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/v2/players",
//                    "params" => [
//                        "page" => 1,
//                        'with[]' => 'game',
//                    ]
//                ],
//            ],
        //赛事
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\abios\AbiosTournamentList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/tournaments",
//                    "params" => [
//                        "page" => 1,
//                    ]
//                ],
//            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => AbiosTeamList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/v2/teams",
//                    "params" => [
//                        "page" => 1,
//                        'with[]' => 'game',
//                    ]
//                ],
//            ],
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => AbiosMatchList::class,
                "batch_id" => self::getNowTime(),
                "params" => [
                    "action" => "/v2/series",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\abios\AbiosTeamList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/v2/teams",
//                    "params" => [
//                        "with" => ['game'],
//                        "page" => 1
//                    ]
//                ],
//            ],
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
//        $DotaPropInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>DotaAssets::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/games/1/assets",
//                "params"=>[
//                    "offset"=>0,
//                    "limit"=>3,
//                ]
//            ],
//
//        ];
//        TaskRunner::addTask($DotaPropInfo);

//        $MathInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>MatchList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/series",
//                "params"=>[
//                    "offset"=>0,
//                    "limit"=>3,
//                    'games[]'=>1,
//                    'with[]' => 'matches',
//                    'sort' => 'desc'
//                ]
//            ],
//
//        ];
//        TaskRunner::addTask($MathInfo);
//        exit();

//        $tournamentInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>TournamentList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/tournaments",
//                "params"=>[
//                    'games'=>'games[]=1',
//                    'with' => 'with[]=series',
//                    'limit' => 1
//                ]
//            ],
//
//        ];

//        $teamInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>TeamList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/teams",
//                "params"=>[
//                    "offset"=>0,
//                    "limit"=>1,
//                    'games[]'=>1,
//                    'with[]' => 'game',
//                ]
//            ],
//
//        ];

//        $playerInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>PlayerList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/players",
//                "params"=>[
//                    "offset"=>0,
//                    "limit"=>1,
//                    'games[]'=>1,
//                    'with[]' => 'game',
//                ]
//            ],
//
//        ];

//        $playerInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>PlayerList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/players",
//                "params"=>[
//                    "offset"=>0,
//                    "limit"=>1,
//                    'games[]'=>1,
//                    'with[]' => 'game',
//                ]
//            ],
//
//        ];

//        $tournamentInfo=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>TournamentList::class,
//            "batch_id"=>self::getNowTime(),
//            "params"=>[
//                "action"=>"/v2/tournaments",
//                "params"=>[
//                    'games'=>'games[]=1',
//                    'with' => 'with[]=series',
//                    'limit' => 1
//                ]
//            ],
//
//        ];
//
//        TaskRunner::addTask($tournamentInfo);
//        TaskRunner::addTask($playerInfo);
//        TaskRunner::addTask($teamInfo);
//        TaskRunner::addTask($tournamentInfo);
//        TaskRunner::addTask($MathInfo);
//        TaskRunner::addTask($DotaPropInfo);
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('YmdHis');
        print_r($time);
        return $time;
    }
}