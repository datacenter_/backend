<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;

use app\modules\task\models\abios\GroupTeamRelation;
use app\modules\task\models\abios\TaskDataMatch;
use app\modules\task\models\abios\TaskDataSubstages;
use app\modules\task\models\abios\TaskDataTournament;
use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class SubStage extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新分组信息
        $tasks = self::refreshSubstage($responseInfo,$batchId);
        $list = [];
        // 小组参赛战队
        if (isset($responseInfo['rosters']) && !empty($responseInfo['rosters'])) {
            $list[] = self::refreshRosters($responseInfo['rosters'],$batchId,$responseInfo['id']);
        }

        return array_merge($tasks,$list);
    }

    private static function refreshSubstage($Info,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $object["id"]=$Info['id'];
        $object["title"]=$Info['title'];
        $object["stage_id"]=$Info['stage_id'];
        $object["type"]=$Info['type'];
        $object["tournament_id"]=$Info['tournament_id'];
        $object["order"]=$Info['order'];
        $object["phase"]=$Info['phase'];
        $object["start"]=$Info['start'];
        $object["end"]=$Info['end'];
        $object["deleted_at"]=$Info['deleted_at'];

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $subStage=TaskDataSubstages::find()->where(["id"=>$Info["id"]])->one();
        if($subStage){
            $oldInfo=$subStage->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $subStage=new TaskDataSubstages();
        }
        $subStage->setAttributes($object);
        // 更新数据，推送消息
        $subStage->save();
        if($subStage->getErrors()){
            print_r($subStage->getErrors());
            throw new TaskException(json_encode($subStage->getErrors()));
        }
        $newInfo=$subStage->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_SUBSTAGE,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }

    private static function refreshRosters($info,$batchId,$id)
    {
        $taskInfo = [];
        $diff=[];
        $relationInfo = GroupTeamRelation::find()->where(["group_id" => $id])->asArray()->all();
        if ($relationInfo) {

            $old = array_column($relationInfo, 'team_id');
            $new = array_column($info,'team_id');

            $diff = array_diff($old,$new);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                GroupTeamRelation::deleteAll(["tournament_id" => $id]);

                $relation = new GroupTeamRelation();
                foreach ($info as $key => $val) {
                    $r = clone $relation;
                    $r->setAttributes(["group_id" => $id, 'team_id' => $val['team_id']]);
                    $r->save();
                }

                $taskInfo = self::pushRefreshMessage($info, $changeType,$batchId,$diff,$relationInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $relation = new GroupTeamRelation();
            foreach ($info as $key => $val) {
                $r = clone $relation;
                $r->setAttributes(["group_id" => $id, 'team_id' => $val['team_id']]);
                $r->save();
            }

            $taskInfo = self::pushRefreshMessage($info, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($info, $changeType,$batchId,$diff,$newInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_GROUP_TEAM,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['info' => $info,'diff' => $diff,'player_info' => $newInfo],
        ];

        return $taskInfo;
    }
}