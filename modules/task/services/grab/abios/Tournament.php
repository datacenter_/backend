<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;

use app\modules\task\models\abios\TaskDataMatch;
use app\modules\task\models\abios\TaskDataStage;
use app\modules\task\models\abios\TaskDataTournament;
use app\modules\task\models\abios\TournamentTeamRelation;
use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class Tournament extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        $info = [];
        $subStage = [];
        // 处理阶段
        if (isset($responseInfo['stages']) && !empty($responseInfo['stages'])) {

            foreach ($responseInfo['stages'] as $val) {
                $data = self::refreshStage($val,$batchId,$responseInfo['id'],$responseInfo['game']);
                if (!empty($data)) {
                    $info[] = $data;
                }

                // 处理 分组
                if (isset($val['substages']) && !empty($val['substages']))
                {
                    foreach ($val['substages'] as $v) {
                        $subStage[] = self::getAddInfoByTeamItem($v['id'],$batchId);
                    }
                }

//                return $subStage;
            }


        }
//        return $subStage;
        $list = [];
        // 处理阵容列表
        if (isset($responseInfo['rosters']) && !empty($responseInfo['rosters'])) {
            $list[] = self::refreshRosters($responseInfo['rosters'],$batchId,$responseInfo['id']);
        }

        $tasks=[];
        // 更新赛事
        $tasks = self::refreshTournament($responseInfo,$batchId);

        // 阶段基本信息（在abios的结构里阶段只是个结构）
        return array_merge($tasks,$info,$list,$subStage);
    }
    private static function getAddInfoByTeamItem($Item,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>SubStage::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/v2/substages/".$Item,
                "params"=>[
                    'with'=>'with[]=rosters'
                ]
            ],
        ];
        return $info;
    }
    private static function refreshTournament($Info,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $object["id"]=$Info['id'];
        $object["title"]=$Info['title'];
        $object["abbrev"]=isset($Info['abbrev']) ? $Info['abbrev'] : "";
        $object["country"]=isset($Info['country']) ? $Info['country']['name'] : "";
        $object["city"]=$Info['city'];
        $object["tier"]=$Info['tier'];
        $object["description"]=$Info['description'];
        $object["short_description"]=$Info['short_description'];
        $object["format"]=$Info['format'];
        $object["start"]=$Info['start'];
        $object["end"]=$Info['end'];
        $object["deleted_at"]=$Info['deleted_at'];
        $object["images"]=json_encode($Info['images']);
        $object["prizepool_string"]=json_encode($Info['prizepool_string']);
        if ($Info['game']['id'] == 1) {
            $object["game"] = 3;
        }
        if ($Info['game']['id'] == 2) {
            $object["game"] = 2;
        }

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $tournament=TaskDataTournament::find()->where(["id"=>$Info["id"]])->one();
        if($tournament){
            $oldInfo=$tournament->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $tournament=new TaskDataTournament();
        }
        $tournament->setAttributes($object);
        // 更新数据，推送消息
        $tournament->save();
        if($tournament->getErrors()){
            print_r($tournament->getErrors());
            throw new TaskException(json_encode($tournament->getErrors()));
        }
        $newInfo=$tournament->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
    private static function refreshStage($Info,$batchId,$id,$game)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $object["id"]=$Info['id'];
        $object["tournament_id"]=$id;
        $object["title"]=$Info['title'];
        $object["deleted_at"]=$Info['deleted_at'];
        if ($game['id'] == 1) {
            $object["game"] = 3;
        }
        if ($game['id'] == 2) {
            $object["game"] = 2;
        }

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $stage=TaskDataStage::find()->where(["id"=>$Info["id"]])->one();
        if($stage){
            $oldInfo=$stage->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $stage=new TaskDataStage();
        }
        $stage->setAttributes($object);
        // 更新数据，推送消息
        $stage->save();
        if($stage->getErrors()){
            print_r($stage->getErrors());
            throw new TaskException(json_encode($stage->getErrors()));
        }
        $newInfo=$stage->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_AB_STAGE,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }

    private static function refreshRosters($info,$batchId,$id)
    {
        $taskInfo = [];
        $diff=[];
        $relationInfo = TournamentTeamRelation::find()->where(["tournament_id" => $id])->asArray()->all();
        if ($relationInfo) {
            // 选手所在得所有战队
            $oldPlayerIds = array_column($relationInfo, 'team_id');
            $newPlayerIds = array_column($info,'team_id');

            $diff = array_diff($oldPlayerIds,$newPlayerIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                TournamentTeamRelation::deleteAll(["tournament_id" => $id]);

                $relation = new TournamentTeamRelation();
                foreach ($info as $key => $val) {
                    $r = clone $relation;
                    $r->setAttributes(["tournament_id" => $id, 'team_id' => $val['team_id']]);
                    $r->save();
                }

                $taskInfo = self::pushRefreshMessagepushRefreshMessage($info, $changeType,$batchId,$diff,$relationInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $relation = new TournamentTeamRelation();
            foreach ($info as $key => $val) {
                $r = clone $relation;
                $r->setAttributes(["tournament_id" => $id, 'team_id' => $val['team_id']]);
                $r->save();
            }

            $taskInfo = self::pushRefreshMessage($info, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT_TEAM,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['players' => $players,'diff' => $diff,'player_info' => $PlayerInfo],
        ];

        return $taskInfo;
    }
}