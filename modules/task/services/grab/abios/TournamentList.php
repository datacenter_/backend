<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;


use app\modules\task\models\abios\TaskDataHero;
use app\modules\task\models\abios\TaskDataProp;
use app\modules\task\models\abios\TaskDataRune;
use app\modules\task\models\abios\TaskDataSpells;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class TournamentList extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($originInfo, $taskInfo)
    {
        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo) || empty($responseInfo['data'])){
            throw new TaskRestException($responseInfo);
        }

        $subSteps=[];
        foreach($responseInfo["data"] as $key=>$Item){
            $subStep=self::getAddInfoByTeamItem($Item,$batchId);
            $subSteps[]=$subStep;
        }

//        $subSteps[] = self::getAddInfoByTeamItem($responseInfo["data"][0],$batchId);
        return $subSteps;
    }
    private static function getAddInfoByTeamItem($Item,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>Tournament::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/v2/tournaments/".$Item['id'],
                "params"=>[
                    'with'=>'with[]=rosters&with[]=series&with[]=stages.substages'
                ]
            ],
        ];
        return $info;
    }
}