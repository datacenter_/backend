<?php
/**
 *
 */

namespace app\modules\task\services\grab\abios;

use app\modules\task\models\abios\TaskDataPlayer;
use app\modules\task\models\abios\TaskDataTeam;
use app\modules\task\models\abios\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class Player extends AbiosBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新选手
        $tasks = self::refreshPlayers($responseInfo,$batchId);
        return $tasks;

    }

    private static function refreshPlayers($Info,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $object["id"]=$Info['id'];
        $object["first_name"]=$Info['first_name'];
        $object["last_name"]=$Info['last_name'];
        $object["nick_name"]=$Info['nick_name'];
        $object["images"]=json_encode($Info['images']);
        $object["country"]=$Info['country']['name'];
        $object["deleted_at"]=$Info['deleted_at'];
        $object["roles"]=json_encode($Info['roles']);
        $object["game"] = 3;

        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $play=TaskDataPlayer::find()->where(["id"=>$Info["id"]])->one();
        if($play){
            $oldInfo=$play->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $play=new TaskDataPlayer();
        }
        $play->setAttributes($object);
        // 更新数据，推送消息
        $play->save();
        if($play->getErrors()){
            print_r($play->getErrors());
            throw new TaskException(json_encode($play->getErrors()));
        }
        $newInfo=$play->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_ABIOS,
            QueueServer::QUEUE_RESOURCE_PLAYER,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }

    private static function refreshRelation($players,$teamId,$batchId)
    {
        $taskInfo = [];
        $diff=[];
        $PlayerInfo = TeamPlayerRelation::find()->where(["team_id" => $teamId])->asArray()->all();
        if ($PlayerInfo) {
            // 选手所在得所有战队
            $oldPlayerIds = array_column($PlayerInfo, 'player_id');
            $newPlayerIds = array_column($players,'id');

            $diff = array_diff($oldPlayerIds,$newPlayerIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                TeamPlayerRelation::deleteAll(["team_id" => $teamId]);

                $Players = new TeamPlayerRelation();
                foreach ($players as $key => $val) {
                    $Player = clone $Players;
                    $Player->setAttributes(["player_id" => (string)$val['id'], 'team_id' => $teamId,'game_id' => 3]);
                    $Player->save();
                }

                $taskInfo = self::pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $PlayerInfo = new TeamPlayerRelation();
            foreach ($players as $key => $val) {
                $Player = clone $PlayerInfo;
                $Player->setAttributes(["player_id" => (string)$val['id'], 'team_id' => $teamId,'game_id' => 3]);
                $Player->save();
            }

            $taskInfo = self::pushRefreshMessage($players, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_PLAYER_TEAM,
            $changeType,
            "");
        $taskInfo = [[
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['players' => $players,'diff' => $diff,'player_info' => $PlayerInfo],
        ]];

        return $taskInfo;
    }
}