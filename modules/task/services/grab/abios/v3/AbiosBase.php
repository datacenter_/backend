<?php
/**
 *abios基础请求方法
 */

namespace app\modules\task\services\grab\abios\v3;

use app\modules\task\services\Common;

class AbiosBase
{
    public static $config=[
        'base_url'=>'https://atlas.abiosgaming.com/v3',
        'secret'=>'acf7f87dca034c8c873601d4da2f09b8',
    ];
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $action=$taskParams["action"];
        $params=$taskParams["params"];

        return self::getCurlInfo($action,$params);
    }

    public static function getCurlInfo($action,$params,$is_build=true)
    {
        $url=self::$config["base_url"].$action;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Abios-Secret"=>self::$config['secret']
        ];
        return Common::requestGet($url,$header,$params,$is_build);
    }
}