<?php


namespace app\modules\task\services\grab\abios\v3;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosCSWeaponsList extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps=[];

        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addTeamAndGetSubInfo($Item, $batchId,$refresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }

        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $params['params']['take'] = $params['params']['take'] ? $params['params']['take']: 50;
            $paramsNext = $params;

            $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
            $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }

        return $subSteps;
    }

    public static function addTeamAndGetSubInfo($item, $batchId, $refresh = "")
    {

        if ($item['category'] == 'weapon') {
            $resourceType = Consts::METADATA_TYPE_CSGO_WEAPON;
        }
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], $resourceType, $gameInfo['id'], $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                $resourceType,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }

    }


}