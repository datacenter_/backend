<?php


namespace app\modules\task\services\grab\abios\v3;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\RefreshResource;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class AbiosIncidents extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if($tagInfo['ext2_type']){
            $reason = $tagInfo['ext2_type'];
        }else{
            $reason = "refresh";
        }
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        //参数
        $params = json_decode($taskInfo["params"], true);
        //定义的时间内
        $since = $params['since'];
        $isempty = false;
        $tournamentArray = [];
        $substageArray = [];
        $seriesArray = [];
        $subSteps = [];
        foreach ($responseInfo as $key=>$value){
            if($value['created_at'] >= $since){
                $tournamentArray[] = $value['tournament']['id'];
                $substageArray[] = $value['substage']['id'];
                $seriesArray[] = $value['series']['id'];

            }else{
                $isempty = true;
                //不符合
            }
        }
        if($tournamentArray){
            $tournamentIds = implode(',',array_unique($tournamentArray));
            if($tournamentIds){
                $tournamentString ="{".$tournamentIds."}";
                $item = [
                    "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                        "","","","",$reason),
                    "type"=>\app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
                    "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TOURNAMENT),
                    "params"=>[
                        "action"=>"/tournaments",
                        "params"=>[
                            "filter"=>"id<=".$tournamentString
                        ]
                    ],
                ];
                $subSteps[] = $item;
            }
        }
        if($substageArray){
            $substageIds = implode(',',array_unique($substageArray));
            $substageString ="{".$substageIds."}";
            $item = [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","","",$reason),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosGrandsonTournament::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT),
                "params"=>[
                    "action"=>"/substages",
                    "params"=>[
                        "filter"=>"id<=".$substageString
                    ]
                ],
            ];
            $subSteps[] = $item;
        }
        if($seriesArray){
            $seriesIds = implode(',',array_unique($seriesArray));
            $seriesString ="{".$seriesIds."}";
            $item = [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "","","","",$reason),
                "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
                "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
                "params"=>[
                    "action"=>"/series",
                    "params"=>[
                        "filter"=>"id<=".$seriesString
                    ]
                ],
            ];
            $subSteps[] = $item;
        }
        if($isempty){

        }else{
            if (count($responseInfo)) {
                $pageNext = $taskInfo->toArray();
                unset($pageNext['response']);
                $pageNext['status'] = 1;
                $params = json_decode($taskInfo["params"], true);
                $params['params']['take'] = $params['params']['take'] ? $params['params']['take']: 50;
                $paramsNext = $params;

                $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
                $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }
        return $subSteps;
    }

}