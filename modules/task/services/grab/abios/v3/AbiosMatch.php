<?php


namespace app\modules\task\services\grab\abios\v3;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\RefreshResource;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class AbiosMatch extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];
        if(empty($response)){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        $responseInfo[] = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }

        $subSteps=[];
        $rosterIds = [];
        $matchesIds = [];
        // 详情里面有id等等
        // 单个获取
        // 组装数据
        //
//        $subStep = self::addTeamAndGetSubInfo($val, $batchId,$refresh);
//        if ($subStep) {
//            $subSteps[] = $subStep;
//        }

        foreach ($responseInfo as $responseKey => $responseVal) {
            if (!empty($responseVal['participants'])) {
                $rosterIds[] = $responseVal['participants'][0]['roster']['id'];
                $rosterIds[] = $responseVal['participants'][1]['roster']['id'];
            }

            if (!empty($responseVal['matches'])) {
                foreach ($responseVal['matches'] as $matchesIdsKey => $matchesIdsVal) {
                    $matchesIds[] = $matchesIdsVal['id'];
                }
            }

        }

        $newMatchesArr = [];
        if (!empty($matchesIds)) {
            $matchesIdss = array_unique($matchesIds);
//            $matchesNum = count($matchesIdss);
//            $matchesPage = ceil($matchesNum / 50);
            $offset = '';
            $matchesNewArr = [];
//            for ($i = 1; $i <= $matchesPage; $i++) {
//                $offset = ($i - 1) * 50;
//                $matchesIdsss = array_slice($matchesIdss, $offset, 50);
                $imIdmatches = implode(',', $matchesIdss);
                $matchesParams = 'filter=id<={' . $imIdmatches . '}';
                $matchesJson = AbiosBase::getCurlInfo("/matches", $matchesParams, false);
            $matchesNewArr = json_decode($matchesJson, true);
//                $matchesNewArr = array_merge($matchesNewArr, $matchesArr);

//            }

            foreach ($matchesNewArr as $matchesArrKey => $matchesArrVal) {
                $newMatchesArr[$matchesArrVal['id']] = $matchesArrVal;
                if (!empty($matchesArrVal['participants'])) {
                    $rosterIds[] = $matchesArrVal['participants'][0]['roster']['id'];
                    $rosterIds[] = $matchesArrVal['participants'][1]['roster']['id'];
                }

            }
        }

        if (!empty($rosterIds)) {
//            $improsterIdsun = array_unique(array_filter($rosterIds));

//            $rosterNum = count($improsterIdsun);
//            $rosterPage = ceil($rosterNum / 50);
            $rostersDataNewArr = [];
//            for ($j = 1; $j <= $rosterPage; $j++) {
//                $rosteroffset = ($j - 1) * 50;
//                $improsterIdsuns = array_slice($improsterIdsun, $rosteroffset, 50);
                $improsterIds = implode(',', $rosterIds);
                $rosterIdsParams = 'filter=id<={' . $improsterIds . '}';

                $rostersDataJson = AbiosBase::getCurlInfo("/rosters", $rosterIdsParams, false);
            $rostersDataNewArr = json_decode($rostersDataJson, true);
//                $rostersDataNewArr = array_merge($rostersDataNewArr, $rostersDataArr);

//            }
            $newTeams = [];
            foreach ($rostersDataNewArr as $rostersDatakey => $rostersDataVal) {
                $newTeams[$rostersDataVal['id']] = $rostersDataVal['team']['id'];
            }

        }

        foreach ($responseInfo as $endKey => &$endval) {
            if (!empty($endval['participants'])){
                $endval['participants'][0]['teamId'] = $newTeams[$endval['participants'][0]['roster']['id']];
                $endval['participants'][1]['teamId'] = $newTeams[$endval['participants'][1]['roster']['id']];
            }

            if (!empty($endval['matches'])) {
                foreach ($endval['matches'] as $endMatchsK => &$endMatchsV) {
                    $endMatchsV['matchesd'] = $newMatchesArr[$endMatchsV['id']];

                    if (!empty($endMatchsV['matchesd']['participants'])) {
                        $endMatchsV['matchesd']['participants'][0]['teamId'] = $endMatchsV['matchesd']['participants'][0]['roster']['id'];
                        $endMatchsV['matchesd']['participants'][1]['teamId'] = $endMatchsV['matchesd']['participants'][1]['roster']['id'];
                    }

                }
            }
        }



        foreach ($responseInfo as $key => $val) {
//            foreach ($val['participants'] as $partKry => $partVal ){
//
//                $rosterId = $partVal['roster']['id'];
//                $rostersJson = AbiosBase::getCurlInfo("/rosters/".$rosterId,[]);
//                $rostersarr = json_decode($rostersJson,true);
//
//                $val['participants'][$partKry]['teamId'] = $rostersarr['team']['id'];
//            }
            if (!empty($val['matches'])){
//                $matchesIds = array_column($val['matches'],'id','id');
//                $imIds = implode(',',$matchesIds);
//                $matchesParams = 'filter=id<={'.$imIds.'}';
//                $matchesJson = AbiosBase::getCurlInfo("/matches",$matchesParams,false);
//                $matchteamids = [];
//                $matchesArr = json_decode($matchesJson,true);
//                foreach ($matchesArr as $matchteamK => $matchteamV){
//                    $matchteamids[] = $matchteamV['participants'][0]['roster']['id'];
//                    $matchteamids[] = $matchteamV['participants'][1]['roster']['id'];
//                }
//                $matchteamStr = implode(',',array_unique($matchteamids));
//                $matchteamParams = 'filter=id<={'.$matchteamStr.'}';
//                $matchesteamJson = AbiosBase::getCurlInfo("/rosters",$matchteamParams,false);
//                $matchesteamArr = json_decode($matchesteamJson,true);
//                $newTeams = [];
//                foreach ($matchesteamArr as $tteamkey=>$tteamVal){
//                    $newTeams[$tteamVal['id']] = $tteamVal['team']['id'];
//                }
//                foreach ($matchesArr as $matchteamK => &$matchteamV){
//                    $matchteamV['participants'][0]['teamId'] = $newTeams[$matchteamV['participants'][0]['roster']['id']];
//                    $matchteamV['participants'][1]['teamId'] = $newTeams[$matchteamV['participants'][1]['roster']['id']];
//                }

//                $matchesArrKeyCaId = array_column($matchesArr,null,'id');
                foreach ($val['matches'] as $matchsKey=> &$matchVal){
//                    $matchVal['matchesd'] = $matchesArrKeyCaId[$matchVal['id']]?:[];
                    if (!empty($matchVal['id'])){
                        $coverageJson = AbiosBase::getCurlInfo("/matches/".$matchVal['id']."/coverage",[]);
                        $matchVal['coverage'] = $coverageJson;
                    }
                }
            }
            if (!empty($val['casters'])) {
                foreach ($val['casters'] as $castersK=> &$castersV){
                    if(!empty($castersV['caster']['id'])){
                        $castersJson = AbiosBase::getCurlInfo("/casters/".$castersV['caster']['id']."/streams",[]);
                        $castersArr = json_decode($castersJson,true);

                        $castersV['casterContent'] = $castersArr ?: [];
                    }
                }
            }

            $subStep = self::addTeamAndGetSubInfo($val, $batchId,$refresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if($reason == "incidents"){
            //incidents不翻页
        }else {
//            if (count($responseInfo)) {
//                $pageNext = $taskInfo->toArray();
//                unset($pageNext['response']);
//                $pageNext['status'] = 1;
//                $params = json_decode($taskInfo["params"], true);
//                $params['params']['take'] = $params['params']['take'] ? $params['params']['take'] : 50;
//                $paramsNext = $params;
//
//                $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
//                $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
//                $pageNext['params'] = $paramsNext;
//                $subSteps[] = $pageNext;
//            }
        }

        return $subSteps;
    }

    public static function addTeamAndGetSubInfo($item, $batchId, $refresh = "")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        //
       $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);

        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, $gameInfo['id'], $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                QueueServer::QUEUE_RESOURCE_MATCH,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}
