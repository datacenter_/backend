<?php


namespace app\modules\task\services\grab\abios\v3;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\RefreshResource;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class AbiosMatchList extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo = QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"] ?: '';
        $reason = $tagInfo['ext2_type'];
        if(empty($response)){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        $responseInfo = json_decode($response, true);

        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];
        $rosterIds = [];
        $tournaments = [];
        foreach ($responseInfo as $responseKey => $responseVal) {

            if ($refresh == 'overMatchendTime') {

                if (!empty($responseVal['participants'])) {
                    if (!empty($responseVal['participants'][0]['roster']['id'])) {
                        $rosterIds[$responseVal['participants'][0]['roster']['id']] = $responseVal['participants'][0]['roster']['id'];
                    }
                    if (!empty($responseVal['participants'][1]['roster']['id'])) {
                        $rosterIds[$responseVal['participants'][1]['roster']['id']] = $responseVal['participants'][1]['roster']['id'];
                    }

                }

                if (!empty($responseVal['tournament'])) {
                    $tournaments[$responseVal['tournament']['id']] = $responseVal['tournament']['id'];
                }

            }

            $tmpMatchId = $responseVal['id'];
            // 获取到数据，我就不用，只要id，添加子任务，单刷match
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", $refresh);
            $tmpSubStep = [
                'tag' => $tag,
                'type' => AbiosMatch::class,
                'batch_id' => $batchId,
                'params' => [
                    "action" => "/series/" . $tmpMatchId,
                    "params" => [
                    ]
                ],
            ];
            $subSteps[] = $tmpSubStep;
        }

        if ($reason == "incidents") {
            //incidents不翻页
        } else {
            if (count($responseInfo)) {
                $pageNext = $taskInfo->toArray();
                unset($pageNext['response']);
                $pageNext['status'] = 1;
                $params = json_decode($taskInfo["params"], true);
                $params['params']['take'] = $params['params']['take'] ? $params['params']['take'] : 50;
                $paramsNext = $params;

                $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
                $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
                $pageNext['params'] = $paramsNext;

                if ($paramsNext['params']['skip'] < 5000) {
                    $subSteps[] = $pageNext;

                }


            }
        }


        if ($refresh == 'overMatchendTime') {
            $rosterNum = count($rosterIds);
            $rosterPage = ceil($rosterNum / 50);
            $rostersDataNewArr = [];
            for ($j = 1; $j <= $rosterPage; $j++) {
                $rosteroffset = ($j - 1) * 50;
                $improsterIdsuns = array_slice($rosterIds, $rosteroffset, 50);
                $improsterIds = implode(',', $improsterIdsuns);
                $rosterIdsParams = 'filter=id<={' . $improsterIds . '}';
                $teamByRostersDataJson = AbiosBase::getCurlInfo("/rosters", $rosterIdsParams, false);
                $rostersDataArr = json_decode($teamByRostersDataJson, true);
                $rostersDataNewArr = array_merge($rostersDataNewArr, $rostersDataArr);
            }
            $allTeams = [];
            $allPlayer = [];
            foreach ($rostersDataNewArr as $rostersDatakey => $rostersDataVal) {
                if (is_numeric($rostersDataVal['team']['id'])) {
                    $allTeams[$rostersDataVal['team']['id']] = $rostersDataVal['team']['id'];
                }
            }

            if (!empty($tournaments)) {
                $tournamentsNum = count($tournaments);
                $tournamentsPage = ceil($tournamentsNum / 10);
                for ($k = 1; $k <= $tournamentsPage; $k++) {
                    $tournamentsoffset = ($k - 1) * 10;
                    $tournamentsSlice = array_slice($tournaments, $tournamentsoffset, 10);
                    $tournamentsImp = implode(',', $tournamentsSlice);
                    $tournamentsParams = '{' . $tournamentsImp . '}';

                    $tournamentsarr = self::taskInfoAdd(AbiosTournamentList::class, $batchId, '/tournaments', $tournamentsParams,$refresh);
                    $subSteps[] = $tournamentsarr;

                }

            }

            if (!empty($allTeams)) {
                foreach ($allTeams as $allTeamsK => $allTeamV) {
                    if (is_numeric($allTeamV)) {
                        $tmpSubStep = [
                            'tag' => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                "", "", "", $refresh, ''),
                            'type' => \app\modules\task\services\grab\abios\v3\AbiosTeam::class,
                            'batch_id' => $batchId,
                            'params' => [
                                "action" => "/teams/" . $allTeamV,
                            ],
                        ];
                        $subSteps[] = $tmpSubStep;
                    }
                }
            }
        }
        return $subSteps;
    }
    public static function taskInfoAdd($type, $batchId, $action, $ids,$refresh="")
    {
        $playersitem = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", $refresh,""),
            "type" => $type,
            "batch_id" => $batchId,
            "params" => [
                "action" => $action,
                "params" => [
                    "filter"=>"id<=".$ids
                ]
            ],
        ];
        return $playersitem;
//        TaskRunner::addTask($playersitem, 3);
    }

}
