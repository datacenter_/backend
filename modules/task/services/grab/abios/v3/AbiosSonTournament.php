<?php


namespace app\modules\task\services\grab\abios\v3;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class AbiosSonTournament extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];

        $params = json_decode($taskInfo["params"], true);
        $gameId = $params['game_id'];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps=[];

        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addTournamentsAndGetSubInfo($Item, $batchId,$refresh,$gameId);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }

        if($reason == "incidents"){
            //incidents不翻页
        }else {
            if (count($responseInfo) && $refresh != "overMatchendTime") {
                $pageNext = $taskInfo->toArray();
                unset($pageNext['response']);
                $pageNext['status'] = 1;
                $params = json_decode($taskInfo["params"], true);
                $params['params']['take'] = $params['params']['take'] ? $params['params']['take'] : 50;
                $paramsNext = $params;

                $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
                $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }
        return $subSteps;
    }

    public static function addTournamentsAndGetSubInfo($item, $batchId, $refresh ="",$gameId="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_SON_TOURNAMENT, $gameId, $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::RESOURCE_TYPE_SON_TOURNAMENT,
                $diffInfo['change_type'],
                $gameId);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}