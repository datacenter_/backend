<?php
/**
 * 战队列表
 */
namespace app\modules\task\services\grab\abios\v3;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskRunner;

class AbiosTeam extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $refresh2 = $tagInfo["ext2_type"]?$tagInfo["ext2_type"]:"";
        if(empty($response)){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }

        $subSteps = [];
        $subStep = self::addTeamAndGetSubInfo($responseInfo, $batchId,$refresh,$refresh2);
        if ($subStep) {
            $subSteps[] = $subStep;
        }
        return $subSteps;
    }

    public static function addTeamAndGetSubInfo($item, $batchId,$refresh="",$refresh2="")
    {
        //抓取阵容信息
        $rosters = AbiosBase::getCurlInfo("/teams/".$item['id']."/rosters",[]);
        $transPlayers = [];
        $transOldPlayers = [];
        if($rosters){
            $rosters = json_decode($rosters,true);
            if($rosters){
                if($refresh == "overMatchendTime"){
                        foreach ($rosters as $rosterKKey => $rosterVValue) {
                            foreach ($rosterVValue['line_up']['players'] as $vvv) {
                                $playersss[] = $vvv['id'];
                            }
                        }
                        if ($playersss) {
                            $playerssss = array_unique($playersss);
                            $PlayerNum = count($playerssss);
                             $playerPage = ceil($PlayerNum / 10);

                            for ($zz = 1; $zz <= $playerPage; $zz++) {
                                $playerPageoffset = ($zz - 1) * 10;
                                $playerSlice = array_slice($playerssss, $playerPageoffset, 10);
                                $playerImp = implode(',', $playerSlice);
                                $playerParams ="{".$playerImp."}";
                                $teamTaskInfo=[
                                    "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                                        "","","","overMatchendTime"),
                                    "type"=>\app\modules\task\services\grab\abios\v3\AbiosPlayerList::class,
                                    "batch_id" => $batchId,
                                    "params"=>[
                                        "action"=>"/players",
                                        "params"=>[
                                            "filter"=>"id<=".$playerParams
                                        ]
                                    ],

                                ];
                                TaskRunner::addTask($teamTaskInfo, 5);
                            }
                        }
                }
                foreach ($rosters as $rosterKey=>$rosterValue){
                    if($rosterValue['id'] == $item['standing_roster']['roster']['id']){
                        $transPlayers = $rosterValue['line_up']['players'];
                        unset($rosters[$rosterKey][$rosterValue]);
                    }elseif($rosterValue['line_up']['players']){
                        foreach ($rosterValue['line_up']['players'] as $v){
                            $transOldPlayers[] = $v['id'];
                        }
//                        $transOldPlayers[] = $rosterValue['line_up']['players'] ? $rosterValue['line_up']['players'] : [];
                    }
                }

            }
        }
        //player
        if($transPlayers){
            $players = array_column($transPlayers,'id');
            $item['players'] = $players;
        }
        //oldPlayer
        if($transOldPlayers){
            $oldPlayers = array_unique($transOldPlayers);
            $history_players=[];
            foreach ($oldPlayers as $oldKey => $oldVal)
            {
                if(isset($players)){
                    if(!in_array($oldVal,$players))
                    {
                        $history_players[] = $oldVal;
                    }
                }
            }
//            if(isset($players)){
//
//                if(!in_array($oldPlayers,$players))
//                {
//                    $history_players[] = $oldPlayers;
//                }
//            }
            if($history_players){
                $item['history_players'] = $history_players;
            }
        }

        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TEAM, $gameInfo['id'], $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::RESOURCE_TYPE_TEAM,
                $diffInfo['change_type'],
                $gameInfo['id'],
                $refresh2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}