<?php
/**
 * 战队列表
 */
namespace app\modules\task\services\grab\abios\v3;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskRunner;
use function PHPUnit\Framework\throwException;

class AbiosTeamList extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $refresh2 = $tagInfo["ext2_type"]?$tagInfo["ext2_type"]:"";
        if(empty($response)){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
//        //战队的请求
//        $rostersIds = [];
//        foreach ($responseInfo as $resKey=>$resValue){
//            if($resValue['standing_roster']['roster']['id']){
//                $rostersIds[] = $resValue['standing_roster']['roster']['id'];
//            }
//        }
//        $rostersIds = implode(',',array_unique($rostersIds));
//        $substageString ="{".$rostersIds."}";
//        $rostersResult = AbiosBase::getCurlInfo("/rosters",["filter"=>"id<=".$substageString]); //roster梭有的当前阵容
//        if($rostersResult){
//            $rostersArray = json_decode($rostersResult,true);
//            if($rostersArray){
//                foreach ($rostersArray as $resterKey=>$resterValue){
//                    foreach ($responseInfo as $resKey=>$resValue){
//                        $responseInfo[$resKey]['standing_roster']['roster']['id'];
//                        if($resterValue['id'] == $responseInfo[$resKey]['standing_roster']['roster']['id']){
//                            if(isset($resterValue['line_up']['players']) && $resterValue['line_up']['players']){
//                                $responseInfo[$resKey]['players'] = array_column($resterValue['line_up']['players'],'id');
//                            }
//                        }
//                    }
//                }
//            }
//        }

        $subSteps = [];
        foreach ($responseInfo as $responseKey => $responseValue){
            $teamId = $responseValue['id'];
            $tmpSubStep = [
                'tag' => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", $refresh,$refresh2),
                'type' => \app\modules\task\services\grab\abios\v3\AbiosTeam::class,
                'batch_id' => $batchId,
                'params' => [
                    "action" => "/teams/".$teamId,
                ],
            ];
            $subSteps[] = $tmpSubStep;
        }
//        foreach ($responseInfo as $key => $item) {
//            $subStep = self::addTeamAndGetSubInfo($item, $batchId,$refresh,$refresh2);
//            if ($subStep) {
//                $subSteps[] = $subStep;
//            }
//        }

        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $params['params']['take'] = $params['params']['take'] ? $params['params']['take']: 50;
            $paramsNext = $params;
            $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
            $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }

        return $subSteps;
    }

    public static function addTeamAndGetSubInfo($item, $batchId,$refresh="",$refresh2="")
    {
        if($refresh2 == "history_players"){
            //抓取阵容信息
            $rosters = AbiosBase::getCurlInfo("/teams/".$item['id']."/rosters",[]);
            $transPlayers = [];
            $transOldPlayers = [];
            if($rosters){
                $rosters = json_decode($rosters,true);
                if($rosters){
                    foreach ($rosters as $rosterKey=>$rosterValue){
                        if($rosterValue['id'] == $item['standing_roster']['roster']['id']){
                            $transPlayers = $rosterValue['line_up']['players'];
                            unset($rosters[$rosterKey][$rosterValue]);
                        }elseif($rosterValue['line_up']['players']){
                            foreach ($rosterValue['line_up']['players'] as $v){
                                $transOldPlayers[] = $v['id'];
                            }
//                        $transOldPlayers[] = $rosterValue['line_up']['players'] ? $rosterValue['line_up']['players'] : [];
                        }
                    }

                }
            }
            //player
            if($transPlayers){
                $players = array_column($transPlayers,'id');
//                $item['players'] = $players;
            }
            //oldPlayer
            if($transOldPlayers){
                $oldPlayers = array_unique($transOldPlayers);
                $history_players=[];
                foreach ($oldPlayers as $oldKey => $oldVal)
                {
                    if(isset($players)){
                        if(!in_array($oldVal,$players))
                        {
                            $history_players[] = $oldVal;
                        }
                    }
                }
                if($history_players){
                    $item['history_players'] = $history_players;
                }
            }
        }

        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TEAM, $gameInfo['id'], $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::RESOURCE_TYPE_TEAM,
                $diffInfo['change_type'],
                $gameInfo['id'],
                $refresh2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}