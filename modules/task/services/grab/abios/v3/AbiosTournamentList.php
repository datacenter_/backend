<?php


namespace app\modules\task\services\grab\abios\v3;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class AbiosTournamentList extends AbiosBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];

        if(empty($response)){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps=[];

        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addTournamentsAndGetSubInfo($Item, $batchId,$refresh,$reason);
            if ($subStep) {
                $subSteps = array_merge($subSteps,$subStep);
            }
        }
        if($reason == "incidents"){
            //incidents不翻页
        }else {
            if (count($responseInfo) && $refresh != "overMatchendTime") {
                $pageNext = $taskInfo->toArray();
                unset($pageNext['response']);
                $pageNext['status'] = 1;
                $params = json_decode($taskInfo["params"], true);
                $params['params']['take'] = $params['params']['take'] ? $params['params']['take'] : 50;
                $paramsNext = $params;

                $paramsNext['params']['skip'] = (int)$paramsNext['params']['take'] + (int)$paramsNext['params']['skip'];
                $paramsNext['params']['take'] = (int)$paramsNext['params']['take'];
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }

        return $subSteps;
    }

    public static function addTournamentsAndGetSubInfo($item, $batchId, $refresh ="",$reason="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ABIOS);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $item['game']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TOURNAMENT, $gameInfo['id'], $identityId, $item,$refresh);

        $tasks=[];
        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ABIOS,
                Consts::RESOURCE_TYPE_TOURNAMENT,
                $diffInfo['change_type'],
                $gameInfo['id'],$reason);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            $tasks[] = $taskInfo;
        }
        //子赛事
        $subtask = self::getSubInfo($item, $batchId, $refresh,$gameInfo['id'],$reason);
        if($subtask){
            $tasks[] = $subtask;
        }
        //孙赛事
        $substagesTask = self::getSubStages($item, $batchId, $refresh,$gameInfo['id'],$reason);
        if($substagesTask){
            $tasks[] = $substagesTask;
        }
        return $tasks;
    }

    //子赛事抓取任务
    public static function getSubInfo($item, $batchId, $refresh="",$gameId="",$reason="")
    {
        $identityId = $item['id'];
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", $refresh,$reason);
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosSonTournament::class,
            "params" => [
                'action'=>'/tournaments/'.$identityId.'/stages',
                'params'=>[
                    "skip" =>0,
                    "take" =>50,
                ],
                "game_id"=>$gameId
            ],
        ];
        return $taskInfo;
    }
    //孙赛事
    public static function getSubStages($item, $batchId, $refresh="",$gameId="",$reason="")
    {
        $identityId = $item['id'];
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", $refresh,$reason);
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosGrandsonTournament::class,
            "params" => [
                'action'=>'/tournaments/'.$identityId.'/substages',
                'params'=>[
                    "skip" =>0,
                    "take" =>50,
                ],
                "game_id"=>$gameId
            ],
        ];
        return $taskInfo;
    }

}