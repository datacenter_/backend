<?php
/**
 *
 */

namespace app\modules\task\services\grab\bayes;


use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\tasks\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class BayesBase
{
    const BAYES_ACCESS_TOKEN = "bayes_access_token";
    const BAYES_REFRESH_TOKEN = "bayes_refresh_token";

    const BAYES_ACCESS_TOKEN_TWO = "bayes_access_token_two";
    const BAYES_REFRESH_TOKEN_TWO = "bayes_refresh_token_two";
    private static $config=[
        'base_url'=>'https://api.esportsdirectory.info/v1',
        'username'=>'codingant@163.com',
        'password'=>'AllwinBedex1507',
        'username2'=>'ttom1106@hotmail.com',
        'password2'=>'AllwinBedex1507',
    ];
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $action=$taskParams["action"];
        $params=$taskParams["params"];
        //随机两个账户
        return self::getCurlByParams($action,$params);
    }
    public static function getCurlByParams($action,$params)
    {
        $chars =['token1','token2'];
        $key = array_rand($chars,1);
        $token = $chars[$key];
        if($token == 'token1'){
            return self::getCurlInfo($action,$params);
        }
        if($token == 'token2'){
            return self::getCurlInfo2($action,$params);
        }
    }
    public static function getCurlInfo($action,$params)
    {
        $url=self::$config["base_url"].$action;
        $accessToken = self::getAccessToken();
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $resultJson = Common::requestGet($url,$header,$params);
        if($resultJson){
            $resultArray = json_decode($resultJson,true);
            //请求过期的情况,重新通过用户名，密码获取新的refresh_token和access_token
            if($resultArray['detail'] == "Invalid JWT token."){
                $tokenInfo = self::getAuth();
                $url=self::$config["base_url"].$action;
                $header=[
                    "Content-Type"=>'application/json;charset=utf-8',
                    "Authorization"=>"Bearer ".$tokenInfo['access_token'],
                ];
                $resultJson = Common::requestGet($url,$header,$params);
                return $resultJson;
            }else{
                return $resultJson;
            }
        }
    }
    public static function getCurlInfo2($action,$params)
    {
        $url=self::$config["base_url"].$action;
        $accessToken = self::getAccessToken2();
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $resultJson = Common::requestGet($url,$header,$params);
        if($resultJson){
            $resultArray = json_decode($resultJson,true);
            //请求过期的情况,重新通过用户名，密码获取新的refresh_token和access_token
            if($resultArray['detail'] == "Invalid JWT token."){
                $tokenInfo = self::getAuth2();
                $url=self::$config["base_url"].$action;
                $header=[
                    "Content-Type"=>'application/json;charset=utf-8',
                    "Authorization"=>"Bearer ".$tokenInfo['access_token'],
                ];
                $resultJson = Common::requestGet($url,$header,$params);
                return $resultJson;
            }else{
                return $resultJson;
            }
        }
    }

    public static function getAccessToken()
    {
        // 这里用cache存一下token
        $cache = \Yii::$app->cache;
        $accessToken = $cache->get(self::BAYES_ACCESS_TOKEN);
        if (!$accessToken) {
            //是否有refresh_token，有的话用这个刷，没有的话重新登陆
            $refreshToken = $cache->get(self::BAYES_REFRESH_TOKEN);
            if ($refreshToken) {
                $refreshTokenUrl = self::$config["base_url"] . '/auth/refresh_token/';
                $refreshTokenParams = ["refresh_token" => $refreshToken];
                $accessTokenInfo = Common::requestPost($refreshTokenUrl, [], $refreshTokenParams);
                if ($accessTokenInfo) {
                    $accessTokenArray = json_decode($accessTokenInfo, true);
                    if($accessTokenArray["success"]){
                        if(isset($accessTokenArray["access_token"]) && $accessTokenArray["access_token"]){
                            $accessToken = $accessTokenArray["access_token"];
                        }else{
                            throw new TaskRestException($accessToken);
                        }
                        //设置access_token缓存并返回
                        $cache->set(self::BAYES_ACCESS_TOKEN, $accessToken, $accessTokenArray["expires_in"] - 60);
                        return $accessToken;
                    }else{
                        //refresh_token是错的，重新登录获取refresh_token和access_token
                        $cache->delete(self::BAYES_REFRESH_TOKEN);
                        $tokenInfo = self::getAuth();
                        return $tokenInfo['access_token'];
                    }
                } else {
                    throw new TaskRestException($accessToken);
                }
            } else {
                $tokenInfo = self::getAuth();
                return $tokenInfo['access_token'];
            }
        }
        return $accessToken;
    }

    public static function getAccessToken2()
    {
        // 这里用cache存一下token
        $cache = \Yii::$app->cache;
        $accessToken = $cache->get(self::BAYES_ACCESS_TOKEN_TWO);
        if (!$accessToken) {
            //是否有refresh_token，有的话用这个刷，没有的话重新登陆
            $refreshToken = $cache->get(self::BAYES_REFRESH_TOKEN_TWO);
            if ($refreshToken) {
                $refreshTokenUrl = self::$config["base_url"] . '/auth/refresh_token/';
                $refreshTokenParams = ["refresh_token" => $refreshToken];
                $accessTokenInfo = Common::requestPost($refreshTokenUrl, [], $refreshTokenParams);
                if ($accessTokenInfo) {
                    $accessTokenArray = json_decode($accessTokenInfo, true);
                    if($accessTokenArray["success"]){
                        if(isset($accessTokenArray["access_token"]) && $accessTokenArray["access_token"]){
                            $accessToken = $accessTokenArray["access_token"];
                        }else{
                            throw new TaskRestException($accessToken);
                        }
                        //设置access_token缓存并返回
                        $cache->set(self::BAYES_ACCESS_TOKEN_TWO, $accessToken, $accessTokenArray["expires_in"] - 60);
                        return $accessToken;
                    }else{
                        //refresh_token是错的，重新登录获取refresh_token和access_token
                        $cache->delete(self::BAYES_REFRESH_TOKEN_TWO);
                        $tokenInfo = self::getAuth2();
                        return $tokenInfo['access_token'];
                    }
                } else {
                    throw new TaskRestException($accessToken);
                }
            } else {
                $tokenInfo = self::getAuth2();
                return $tokenInfo['access_token'];
            }
        }
        return $accessToken;
    }
    /**
     * @return mixed
     * @throws TaskRestException
     * 登录并返回包含refresh_token,access_token的数组
     */
    public static function getAuth()
    {
        $authUrl = self::$config["base_url"].'/auth/';
        $authParams = [
            "username" => self::$config["username"],
            "password" => self::$config["password"],

        ];
        $tokenInfo = Common::requestPost($authUrl, [], $authParams);
        if ($tokenInfo) {
            $tokenArray = json_decode($tokenInfo, true);
            if($tokenArray["success"]){
                if(isset($tokenArray["refresh_token"]) && $tokenArray["refresh_token"]){
                    \Yii::$app->cache->set(self::BAYES_REFRESH_TOKEN,$tokenArray["refresh_token"],60*60*24*7);  //一周的refresh_token过期时间
                }
                if(isset($tokenArray["access_token"]) && $tokenArray["access_token"]){
                    \Yii::$app->cache->set(self::BAYES_ACCESS_TOKEN, $tokenArray["access_token"], $tokenArray["expires_in"] - 10);
                }
                return $tokenArray;
            }else{
                throw new TaskRestException($tokenInfo);
            }
        }else {
            throw new TaskRestException($tokenInfo);
        }
    }

    public static function getAuth2()
    {
        $authUrl = self::$config["base_url"].'/auth/';
        $authParams = [
            "username" => self::$config["username2"],
            "password" => self::$config["password2"],

        ];
        $tokenInfo = Common::requestPost($authUrl, [], $authParams);
        if ($tokenInfo) {
            $tokenArray = json_decode($tokenInfo, true);
            if($tokenArray["success"]){
                if(isset($tokenArray["refresh_token"]) && $tokenArray["refresh_token"]){
                    \Yii::$app->cache->set(self::BAYES_REFRESH_TOKEN_TWO,$tokenArray["refresh_token"],60*60*24*7);  //一周的refresh_token过期时间
                }
                if(isset($tokenArray["access_token"]) && $tokenArray["access_token"]){
                    \Yii::$app->cache->set(self::BAYES_ACCESS_TOKEN_TWO, $tokenArray["access_token"], $tokenArray["expires_in"] - 10);
                }
                return $tokenArray;
            }else{
                throw new TaskRestException($tokenInfo);
            }
        }else {
            throw new TaskRestException($tokenInfo);
        }
    }
}