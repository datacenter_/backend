<?php


namespace app\modules\task\services\grab\bayes;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class BayesMatch extends BayesBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];
        $responseInfo = json_decode($response, true);

        if (empty($responseInfo)){
            return [];
        }
        if(strpos($responseInfo['detail'],"Request was throttled") !== false){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        if($responseInfo['detail'] == "Not found."){
            return [];
        }
        if ($responseInfo['detail']['Invalid page.']){ //执行到最后一页
            return [];
        }
        if (isset($responseInfo['detail']) && !$responseInfo['detail']['Invalid page.']) {
            throw new TaskRestException($responseInfo);
        }

        $subSteps=[];
        $subStep = self::addMatchAndGetSubInfo($responseInfo, $batchId,$refresh);
        if ($subStep) {
            $subSteps[] = $subStep;
        }
        if ($reason == 'upcoming_by_teams') {
            if (!empty($responseInfo['teams'][0]['id']) && is_numeric($responseInfo['teams'][0]['id']) ) {
                $tmpSubStep = [
                    'tag' => $tag,
                    'type' => BayesTeam::class,
                    'batch_id' => $batchId,
                    'params' => [
                        "action" => "/team/".$responseInfo['teams'][0]['id'],
                        "game_id" => $responseInfo['title']['id'],
                    ],
                ];
                $subSteps[] = $tmpSubStep;
            }

            if (!empty($responseInfo['teams'][1]['id']) && is_numeric($responseInfo['teams'][1]['id']) ) {
                $tmpSubStep = [
                    'tag' => $tag,
                    'type' => BayesTeam::class,
                    'batch_id' => $batchId,
                    'params' => [
                        "action" => "/team/".$responseInfo['teams'][1]['id'],
                        "game_id" => $responseInfo['title']['id'],
                    ],
                ];
                $subSteps[] = $tmpSubStep;
            }
        }


        return $subSteps;
    }

    public static function addMatchAndGetSubInfo($item, $batchId, $refresh ="")
    {
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_BAYES);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_BAYES, $item['title']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, $gameInfo['id'], $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::ORIGIN_BAYES,
                Consts::RESOURCE_TYPE_MATCH,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}