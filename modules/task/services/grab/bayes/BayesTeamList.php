<?php

namespace app\modules\task\services\grab\bayes;

use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class BayesTeamList extends BayesBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $reason = $tagInfo['ext2_type'];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if(strpos($responseInfo['detail'],"Request was throttled") !== false){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        if($responseInfo['detail'] == "Not found."){
            return [];
        }
        if ($responseInfo['detail']['Invalid page.']){ //执行到最后一页
            return [];
        }
        if (isset($responseInfo['detail']) && !$responseInfo['detail']['Invalid page.']) {
            throw new TaskRestException($responseInfo);
        }
        $params = json_decode($taskInfo["params"], true);

        $subSteps=[];
        foreach ($responseInfo['results'] as $responseKey => $responseValue) {
            if($params['incidents_time']){  //incidents的情况
                if($responseValue['last_modified'] > $params['incidents_time']){
                    $teamId = $responseValue['id'];
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => BayesTeam::class,
                        'batch_id' => $batchId,
                        'params' => [
                            "action" => "/team/".$teamId,
                            "game_id" => $params['game_id'],
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    return $subSteps;
                }
            }else{
                $teamId = $responseValue['id'];
                $tmpSubStep = [
                    'tag' => $tag,
                    'type' => BayesTeam::class,
                    'batch_id' => $batchId,
                    'params' => [
                        "action" => "/team/".$teamId,
                        "game_id" => $params['game_id'],
                    ],
                ];
                $subSteps[] = $tmpSubStep;
            }
        }
        if (count($responseInfo)) {
            $taskInfoArray = $taskInfo->toArray();
            $pageNext['run_type'] = $taskInfoArray['run_type'];
            $pageNext['tag'] = $tag;
            $pageNext['type'] = $taskInfoArray['type'];
            $pageNext['batch_id'] = $taskInfoArray['batch_id'];

            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $pageNext['status'] = 1;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }
        return $subSteps;
    }
}