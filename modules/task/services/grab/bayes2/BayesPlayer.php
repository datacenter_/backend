<?php


namespace app\modules\task\services\grab\bayes2;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class BayesPlayer extends BayesBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }

        if(strpos($responseInfo['detail'],"Request was throttled") !== false){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        if(strpos($responseInfo['detail'],"found")){
            return [];
        }
        if ($responseInfo['detail']['Invalid page.']){ //执行到最后一页
            return [];
        }
        if (isset($responseInfo['detail']) && !$responseInfo['detail']['Invalid page.']) {
            throw new TaskRestException($responseInfo);
        }

        $params = json_decode($taskInfo["params"], true);
        if($reason == "one_refresh"){  //单抓
            $responseInfo['game_id'] = $params['game_id'];
        }else{  //list
            $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_BAYES2, $params['game_id']);
            $responseInfo['game_id'] = $gameInfo['id'];
        }

        $subSteps=[];
        $subStep = self::addPlayerAndGetSubInfo($responseInfo, $batchId,$params,$refresh);
        if ($subStep) {
            $subSteps[] = $subStep;
        }
        return $subSteps;
    }

    public static function addPlayerAndGetSubInfo($item, $batchId, $params,$refresh ="")
    {
//        if(isset($params['last_modified']) && $params['last_modified']){ //存在更新时间字段
//            $item['last_modified'] = $params['last_modified'];
//        }
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_BAYES2);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_PLAYER, $item['game_id'], $identityId, $item,$refresh);

        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::ORIGIN_BAYES2,
                Consts::RESOURCE_TYPE_PLAYER,
                $diffInfo['change_type'],
                $item['game_id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}