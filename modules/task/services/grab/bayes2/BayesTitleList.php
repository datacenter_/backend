<?php


namespace app\modules\task\services\grab\bayes2;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class BayesTitleList extends BayesBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];
        $responseInfo = json_decode($response, true);
        $params = json_decode($taskInfo["params"], true);
        if (empty($responseInfo) || ( array_key_exists("results",$responseInfo) && $responseInfo["results"] == null) ){
            return [];
        }
        if(strpos($responseInfo['detail'],"found")){
            return [];
        }
        if ($responseInfo['detail']['Invalid page.']){
            return [];
        }
        if (isset($responseInfo['detail']) && !$responseInfo['detail']['Invalid page.']) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps=[];
        //ext2_type == "allTeam"，先抓title再抓team
        if($params['team'] == "allTeam"){
            foreach ($responseInfo['results'] as $responseKey => $responseValue) {
                $relId = $responseValue['id'];
                if($params['incidents_time']){
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => \app\modules\task\services\grab\bayes2\BayesTeamList::class,
                        'batch_id' => $batchId,
                        'params' => [
                            'action' => "/team",
                            'params'=>[
                                'title' => $relId,
                                'ordering' => '-last_modified',
                                'page'=>1,
                            ],
                            'game_id' =>$relId,
                            'incidents_time' => $params['incidents_time']
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => \app\modules\task\services\grab\bayes2\BayesTeamList::class,
                        'batch_id' => $batchId,
                        'params' => [
                            'action' => "/team",
                            'params'=>[
                                'title' => $relId,
                                "ordering"=>"-last_modified",
                                'page'=>1,
                            ],
                            'game_id' =>$relId,
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }
            }
        }
        //ext2_type == "allPlayer"，先抓title再抓player
        if($params['player'] == "allPlayer"){
            foreach ($responseInfo['results'] as $responseKey => $responseValue) {
                $relId = $responseValue['id'];
                if($params['incidents_time']){
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => \app\modules\task\services\grab\bayes2\BayesPlayerList::class,
                        'batch_id' => $batchId,
                        'params' => [
                            'action' => "/player",
                            'params'=>[
                                'title' => $relId,
                                'ordering' => '-last_modified',
                                'page'=>1,
                            ],
                            'game_id' =>$relId,
                            'incidents_time' => $params['incidents_time']
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => \app\modules\task\services\grab\bayes2\BayesPlayerList::class,
                        'batch_id' => $batchId,
                        'params' => [
                            'action' => "/player",
                            'params'=>[
                                'title' => $relId,
                                "ordering"=>"-last_modified",
                                'page'=>1,
                            ],
                            'game_id' =>$relId,
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }
            }
        }


        if (count($responseInfo)) {
            $taskInfoArray = $taskInfo->toArray();
            $pageNext['run_type'] = $taskInfoArray['run_type'];
            $pageNext['tag'] = $tag;
            $pageNext['type'] = $taskInfoArray['type'];
            $pageNext['batch_id'] = $taskInfoArray['batch_id'];

            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $pageNext['status'] = 1;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }
        return $subSteps;
    }
}