<?php


namespace app\modules\task\services\grab\bayes2;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class BayesTournamentList extends BayesBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $refresh2 = $tagInfo["ext2_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo) || ( array_key_exists("results",$responseInfo) && $responseInfo["results"] == null) ){
            return [];
        }
        if(strpos($responseInfo['detail'],"Request was throttled") !== false){
            throw new \Exception("没有获取到数据",TaskRunner::STATUS_EMPTY);
        }
        if(strpos($responseInfo['detail'],"found")){
            return [];
        }
        if ($responseInfo['detail']['Invalid page.']){ //执行到最后一页
            return [];
        }
        if (isset($responseInfo['detail']) && !$responseInfo['detail']['Invalid page.']) {
            throw new TaskRestException($responseInfo);
        }
        $params = json_decode($taskInfo['params'],true);

        $subSteps=[];
        foreach ($responseInfo['results'] as $responseKey => $responseValue) {
            if($params['incidents_time']){
                if($responseValue['last_modified'] > $params['incidents_time']){
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => BayesTournament::class,
                        'batch_id' => $batchId,
                        'params' => [
                            "action" => "/tournament/".$responseValue['id'],
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    return $subSteps;
                }
            }elseif ($params['upcoming_time']){
                if($responseValue['date_start'] > $params['upcoming_time'] || !$responseValue['date_start']){
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => BayesTournament::class,
                        'batch_id' => $batchId,
                        'params' => [
                            "action" => "/tournament/".$responseValue['id'],
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    return $subSteps;
                }
            }elseif ($refresh2 == "have_no_end"){
                $timeNo = date("Y-m-d H:i:s",time()-60*60*8);  //当前时间
                $timeNow = str_replace(" ","T",$timeNo)."Z";
                if(!$responseValue['date_end'] || $timeNow <= $responseValue['date_end']){  //结束时间是null，当前时间<结束时间
                    $tmpSubStep = [
                        'tag' => $tag,
                        'type' => BayesTournament::class,
                        'batch_id' => $batchId,
                        'params' => [
                            "action" => "/tournament/".$responseValue['id'],
                        ],
                    ];
                    $subSteps[] = $tmpSubStep;
                }else{
                    return $subSteps;
                }
            } else{
                $tmpSubStep = [
                    'tag' => $tag,
                    'type' => BayesTournament::class,
                    'batch_id' => $batchId,
                    'params' => [
                        "action" => "/tournament/".$responseValue['id'],
                    ],
                ];
                $subSteps[] = $tmpSubStep;
            }
        }

        if (count($responseInfo)) {
            $taskInfoArray = $taskInfo->toArray();
            $pageNext['run_type'] = $taskInfoArray['run_type'];
            $pageNext['tag'] = $tag;
            $pageNext['type'] = $taskInfoArray['type'];
            $pageNext['batch_id'] = $taskInfoArray['batch_id'];

            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $pageNext['status'] = 1;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }
        return $subSteps;
    }
}