<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;


use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use yii\db\Exception;

class FeiJingBase
{
    public static $config=[
        'base_url'=>'esportsapi.feijing88.com',
        'access_key'=>'HGWDKi0DxsKQvtJVHVqnmQtLkx6vcnL5',
        'secret_key'=>'NPuRC2LjBLeXAaoCh2VNcgYDH8BhIUbm',
    ];
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $action=$taskParams["action"];
        $params=$taskParams["params"];
        $url="http://".self::$config["base_url"].$action;
        $time=time()*1000;
        $sign=self::getSign($action,$time);
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Accept-ApiAccess"=>self::$config["access_key"],
            "Accept-ClientTime"=>$time,
            "Accept-ApiSign"=>$sign
        ];
        return Common::requestGet($url,$header,$params);
    }

    public static function getSign($action,$time)
    {
        $str=implode("|",[self::$config["secret_key"],$time,$action]);
        return strtoupper(md5($str));
    }

    public static function refreshTeam($params,$batchId)
    {
        $teamId=$params["team_id"];
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $params["team_id"]=(string)$params["team_id"];
        $params["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $teamInfo=TaskDataFeijingTeam::find()->where(["team_id"=>$teamId])->one();
        if($teamInfo){
            $oldInfo=$teamInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $teamInfo=new TaskDataFeijingTeam();
        }
        $teamInfo->setAttributes($params);
        // 更新数据，推送消息
        $teamInfo->save();
        if($teamInfo->getErrors()){
            print_r($teamInfo->getErrors());
            throw new TaskException(json_encode($teamInfo->getErrors()));
        }
        $newInfo=$teamInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TEAM,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }

    public static function refreshPlayer($playerID,$playerInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $playerInfo["player_id"]=(string)$playerInfo["player_id"];
        $playerInfo["team_id"]=(string)$playerInfo["team_id"];
        $playerInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $PlayerInfo=TaskDataFeijingPlayer::find()->where(["player_id"=>$playerID])->one();
        if($PlayerInfo){
            $oldInfo=$PlayerInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $PlayerInfo=new TaskDataFeijingPlayer();
        }
        $PlayerInfo->setAttributes($playerInfo);
        // 更新数据，推送消息
        $PlayerInfo->save();
        if($PlayerInfo->getErrors()){
            print_r($PlayerInfo->getErrors());
            throw new TaskException(json_encode($PlayerInfo->getErrors()));
        }
        $newInfo=$PlayerInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }

        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_PLAYER,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];

        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }


}