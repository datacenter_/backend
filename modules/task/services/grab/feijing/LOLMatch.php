<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TaskBattleInfo;
use app\modules\task\models\TaskDataFeijingMatch;
use app\modules\task\models\TaskDataFeijingTournament;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\models\TournamentTeamRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLMatch extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($response["message"]);
        }

        // 更新比赛信息
        $tasks = self::refreshMatch($responseInfo["data"], $batchId);
        // TODO battle 分局信息
        if (!empty($responseInfo['data']['battle_list']))
        {
            self::refreshBattle($responseInfo['data']['battle_list']);
        }

        return $tasks;
    }

    private static function refreshBattle($battleList)
    {
        $matchId = $battleList[0]['match_id'];
       // 先删除，后增加
        TaskBattleInfo::deleteAll(['match_id' => $matchId]);

        $battle = new TaskBattleInfo();
        foreach ($battleList as $key => $val) {
            $battleObject = clone $battle;
            $battleObject->setAttribute('match_id',$val['match_id']);
            $battleObject->setAttribute('battle_id',$val['battle_id']);
            $battleObject->setAttribute('duration',$val['duration']);
            $battleObject->setAttribute('index',$val['index']);
            $battleObject->setAttribute('status',$val['status']);
            if (isset($val['economic_diff'])) {
                $battleObject->setAttribute('economic_diff',$val['economic_diff']);
            }
            $battleObject->save();
        }

        return true;
    }


    private static function refreshMatch($params,$batchId)
    {

        $matchId=$params["match_id"];
        $params["match_id"]=(string)$params["match_id"];
        $params["league_id"]=(string)$params["league_id"];
        $params["start_time"]=(string)$params["start_time"];
        $params["team_b_id"]=(string)$params["team_b_id"];
        $params["team_a_id"]=(string)$params["team_a_id"];
        $params["game_id"]= 2 ;
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $matchInfo=TaskDataFeijingMatch::find()->where(["match_id"=>$matchId])->one();
        if($matchInfo){
            $oldInfo=$matchInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $matchInfo=new TaskDataFeijingMatch();
        }

        $matchInfo->setAttributes($params);
        // 更新数据，推送消息
        $matchInfo->save();
        if($matchInfo->getErrors()){
            print_r($matchInfo->getErrors());
            exit();
            throw new TaskException(json_encode($matchInfo->getErrors()));
        }
        $newInfo=$matchInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_MATCH,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD  ? [$taskInfo] : [];
    }

}