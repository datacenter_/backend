<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLProp extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($response["message"]);
        }

        // 更新道具信息
        $tasks = self::refreshProp($responseInfo["data"],$batchId);

        return $tasks;
    }

    private static function refreshProp($propInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $propInfo["item_id"]=(string)$propInfo["item_id"];
        $propInfo["price"]=(string)$propInfo["price"];
        $propInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $prop=TaskDataProp::find()->where(["item_id"=>$propInfo["item_id"]])->one();
        if($prop){
            $oldInfo=$prop->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $prop=new TaskDataProp();
        }
        $prop->setAttributes($propInfo);
        // 更新数据，推送消息
        $prop->save();
        if($prop->getErrors()){
            print_r($prop->getErrors());
            throw new TaskException(json_encode($prop->getErrors()));
        }
        $newInfo=$prop->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_PROP,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}