<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;


use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class LOLPropList extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($originInfo, $taskInfo)
    {
        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($responseInfo["message"]);
        }
        $subSteps=[];
        foreach($responseInfo["data"] as $key=>$teamItem){
            $subStep=self::getAddInfoByTeamItem($teamItem,$batchId);
            $subSteps[]=$subStep;
        }
        return $subSteps;
    }

    private static function getAddInfoByTeamItem($teamItem,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>LOLProp::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/data-service/lol/raw/item",
                'params'=>["item_id"=>$teamItem['item_id']]
            ],
        ];
        return $info;
    }

}