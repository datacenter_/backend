<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TaskRune;
use app\modules\task\models\TaskSkill;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLRune extends FeiJingBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if ($responseInfo["code"] != 200) {
            throw new TaskRestException($response["message"]);
        }

        // 更新符文信息
        $tasks = [];
        foreach ($responseInfo['data'] as $key => $val) {
            $tasks[] = self::refreshRune($val, $batchId);
        }

        return $tasks;
    }

    private static function refreshRune($RuneInfo, $batchId)
    {
        $diff = [];
        $oldInfo = [];
        $newInfo = [];
        $RuneInfo["rune_id"] = (string)$RuneInfo["rune_id"];
        $RuneInfo["game_id"] = 2;
        $changeType = QueueServer::QUEUE_TYPE_CHANGE;
        $rune = TaskRune::find()->where(["rune_id" => $RuneInfo["rune_id"]])->one();
        if ($rune) {
            $oldInfo = $rune->toArray();
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;
            $rune = new TaskRune();
        }
        $rune->setAttributes($RuneInfo);
        // 更新数据，推送消息
        $rune->save();
        if ($rune->getErrors()) {
            print_r($rune->getErrors());
            throw new TaskException(json_encode($rune->getErrors()));
        }
        $newInfo = $rune->toArray();
        if ($changeType == QueueServer::QUEUE_TYPE_CHANGE && isset($oldInfo)) {
            $diff = Common::diffParams($oldInfo, $newInfo);
        }
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_RUNE,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => [
                "diff" => $diff,
                "new" => $newInfo,
                "old" => $oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }
}