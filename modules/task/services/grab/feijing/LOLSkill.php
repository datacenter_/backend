<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TaskDataHero;
use app\modules\task\models\TaskDataProp;
use app\modules\task\models\TaskSkill;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLSkill extends FeiJingBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if ($responseInfo["code"] != 200) {
            throw new TaskRestException($response["message"]);
        }

        // 更新技能信息
        $tasks = [];
        foreach ($responseInfo['data'] as $key => $val) {
            $tasks[] = self::refreshSkill($val, $batchId);
        }

        return $tasks;
    }

    private static function refreshSkill($SkillInfo, $batchId)
    {
        $diff = [];
        $oldInfo = [];
        $newInfo = [];
        $SkillInfo["skill_id"] = (string)$SkillInfo["skill_id"];
        $SkillInfo["game_id"] = 2;
        $changeType = QueueServer::QUEUE_TYPE_CHANGE;
        $skill = TaskSkill::find()->where(["skill_id" => $SkillInfo["skill_id"]])->one();
        if ($skill) {
            $oldInfo = $skill->toArray();
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;
            $skill = new TaskSkill();
        }
        $skill->setAttributes($SkillInfo);
        // 更新数据，推送消息
        $skill->save();
        if ($skill->getErrors()) {
            print_r($skill->getErrors());
            throw new TaskException(json_encode($skill->getErrors()));
        }
        $newInfo = $skill->toArray();
        if ($changeType == QueueServer::QUEUE_TYPE_CHANGE && isset($oldInfo)) {
            $diff = Common::diffParams($oldInfo, $newInfo);
        }
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_SKILL,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => [
                "diff" => $diff,
                "new" => $newInfo,
                "old" => $oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];
    }
}