<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLTeam extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($response["message"]);
        }

        // 更新战队信息
        $tasks = self::refreshTeam($responseInfo["data"],$batchId);

        $PlayerTemp = [];
        $relationInfo = [];
        if (!empty($responseInfo['data']['players']))
        {
            // 更新队员信息
            foreach ($responseInfo['data']['players'] as $key => $val) {
                $PlayerTemp[] = [
                    "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                        "","","",""),
                    "type"=>LOLPlayer::class,
                    "batch_id"=>$batchId,
                    "params"=>[
                        'action'=>"/data-service/lol/player/basic_info",
                        'params'=>["player_id"=>$val['player_id']]
                    ],
                ];

            }
            // 更新战队选手关系
            $relationInfo[] = self::refreshRelation($responseInfo['data']['players'],$responseInfo['data']['team_id'],$batchId);
        }

        return array_merge($tasks,$PlayerTemp,$relationInfo);
    }


    private static function refreshRelation($players,$teamId,$batchId)
    {
        $taskInfo = [];
        $diff=[];
        $PlayerInfo = TeamPlayerRelation::find()->where(["team_id" => $teamId])->asArray()->all();
        if ($PlayerInfo) {
            // 选手所在得所有战队
            $oldPlayerIds = array_column($PlayerInfo, 'player_id');
            $newPlayerIds = array_column($players,'player_id');

            $diff = array_diff($oldPlayerIds,$newPlayerIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                TeamPlayerRelation::deleteAll(["team_id" => $teamId]);

                $Players = new TeamPlayerRelation();
                foreach ($players as $key => $val) {
                    $Player = clone $Players;
                    $Player->setAttributes(["player_id" => (string)$val['player_id'], 'team_id' => (string)$val['team_id'],'game_id' => 2]);
                    $Player->save();
                }

                $taskInfo = self::pushRefreshMessagepushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $PlayerInfo = new TeamPlayerRelation();
            foreach ($players as $key => $val) {
                $Player = clone $PlayerInfo;
                $Player->setAttributes(["player_id" => (string)$val['player_id'], 'team_id' => (string)$val['team_id'],'game_id' => 2]);
                $Player->save();
            }

            $taskInfo = self::pushRefreshMessage($players, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_PLAYER_TEAM,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['players' => $players,'diff' => $diff,'player_info' => $PlayerInfo],
        ];

        return $taskInfo;
    }
}