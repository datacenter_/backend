<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;


use app\module\task\services\tasks\TaskRunner;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\tasks\exceptions\TaskException;
use app\modules\task\services\tasks\exceptions\TaskRestException;
use app\modules\task\services\tasks\TaskTypes;

class LOLTeamList extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($originInfo, $taskInfo)
    {
        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($responseInfo["message"]);
        }
        $subSteps=[];
        foreach($responseInfo["data"] as $key=>$teamItem){
            $subStep=self::getAddInfoByTeamItem($teamItem,$batchId);
            $subSteps[]=$subStep;
        }
        return $subSteps;
    }

    private static function getAddInfoByTeamItem($teamItem,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>LOLTeam::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/data-service/lol/team/basic_info",
                'params'=>["team_id"=>6]
            ],
        ];
        return $info;
    }

}