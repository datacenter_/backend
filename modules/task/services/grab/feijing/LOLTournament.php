<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TaskDataFeijingTournament;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\models\TournamentTeamRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLTournament extends FeiJingBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if($responseInfo["code"]!=200){
            throw new TaskRestException($response["message"]);
        }

        // 更新赛事信息
        $tasks = self::refreshTournament($responseInfo["data"],$batchId);
        // 如果赛事有战队 就更新战队关系
        $relationInfo = [];
        if (!empty($responseInfo['data']['team_ids']))
        {
            $relationInfo[] = self::refreshRelation($responseInfo['data']['team_ids'],$responseInfo['data']['league_id'],$batchId);
        }

        return array_merge($tasks,$relationInfo);
    }

    private static function refreshRelation($teamIds,$tournamentId,$batchId)
    {
        $taskInfo = [];
        $diff=[];
        $tournamentId = (string)$tournamentId;
        $TeamIdsInfo = TournamentTeamRelation::find()->where(["tournament_id" => $tournamentId])->asArray()->all();
        if ($TeamIdsInfo) {
            // 选手所在得所有战队
            $oldTeamIds = array_column($TeamIdsInfo, 'team_id');
            $newTeamIds = $teamIds;

            $diff = array_diff($oldTeamIds,$newTeamIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                TeamPlayerRelation::deleteAll(["tournament_id" => $tournamentId]);

                $tournament = new TournamentTeamRelation();
                foreach ($teamIds as $key => $val) {
                    $tournamentObject = clone $tournament;
                    $tournamentObject->setAttributes(["team_id" => (string)$val, 'tournament_id' => (string)$tournamentId,'game_id' => 2]);
                    $tournamentObject->save();
                }

                $taskInfo = self::pushRefreshMessagepushRefreshMessage($teamIds, $changeType,$batchId,$diff,$TeamIdsInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $tournament = new TournamentTeamRelation();
            foreach ($teamIds as $key => $val) {
                $tournamentObject = clone $tournament;
                $tournamentObject->setAttributes(["team_id" => (string)$val, 'tournament_id' => (string)$tournamentId,'game_id' => 2]);
                $tournamentObject->save();

            }

            $taskInfo = self::pushRefreshMessage($teamIds, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($teamIds, $changeType,$batchId,$diff,$oldTeamIdsInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT_TEAM_RELATION,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['teamIds' => $teamIds,'diff' => $diff,'oldTeamIdsInfo' => $oldTeamIdsInfo],
        ];

        return $taskInfo;
    }

    private static function refreshTournament($params,$batchId)
    {
        $leagueId=$params["league_id"];
        $params["league_id"]=(string)$params["league_id"];
        $params["status"]=(string)$params["status"];
        $params["start_time"]=(string)$params["start_time"];
        $params["end_time"]=(string)$params["end_time"];
        $params["game_id"]= 2 ;
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $tournamentInfo=TaskDataFeijingTournament::find()->where(["league_id"=>$leagueId])->one();
        if($tournamentInfo){
            $oldInfo=$tournamentInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $tournamentInfo=new TaskDataFeijingTournament();
        }
        $tournamentInfo->setAttributes($params);
        // 更新数据，推送消息
        $tournamentInfo->save();
        if($tournamentInfo->getErrors()){
            print_r($tournamentInfo->getErrors());
            throw new TaskException(json_encode($tournamentInfo->getErrors()));
        }
        $newInfo=$tournamentInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD  ? [$taskInfo] : [];
    }

}