<?php
/**
 *
 */

namespace app\modules\task\services\grab\feijing;

use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLTournamentList extends FeiJingBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if ($responseInfo["code"] != 200) {
            throw new TaskRestException($response["message"]);
        }

        $subSteps=[];
        foreach($responseInfo["data"] as $key=>$tournamentItem){
            $subStep=self::getAddInfoByTeamItem($tournamentItem,$batchId);
            $subSteps[]=$subStep;
        }
        return $subSteps;
    }

    private static function getAddInfoByTeamItem($teamItem,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>LOLTournament::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/data-service/lol/league/basic_info",
                'params'=>["league_id"=>$teamItem['league_id']]
            ],
        ];
        return $info;
    }
}