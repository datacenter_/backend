<?php
/**
 *
 */

namespace app\modules\task\services\grab\hltv;


use app\modules\common\services\Consts;
use app\modules\data\models\DbStream;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;

class HltvBase
{
    public static function getResponse($taskInfo)
    {
        $taskParams = json_decode($taskInfo["params"], true);
        $type = $taskParams["type"];
        $page = $taskParams["offset"];
        $pageCount = $taskParams["limit"];
        $where = '';
        if(isset($taskParams['where']) && $taskParams['where']){
            $where = $taskParams["where"];
        }
        return json_encode(self::getFromDb($type,$page,$pageCount,$where));
    }

    public static function getFromDb($type,$offset,$limit,$where = '')
    {
//        if($where){
//            $andWhereString = 'where';
//            foreach ($where as $key => $value){
//                $andWhereString = $andWhereString. " $key = '$value'";
//            }
//        }
        switch ($type) {
            case "player":

                $teamCount = DbStream::getDb()->createCommand(
                    "select * from player_all  $where order by id limit  {$offset},{$limit}"
                )->queryAll();

                break;
            case "team":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from team_all $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();

                break;

            case "match":
                // 比赛数据
                $matchDataInfo = DbStream::getDb()
                    ->createCommand("select * from match_all $where  order by id  limit  {$offset},{$limit}")
                    ->queryAll();
                foreach ($matchDataInfo as $matchKey => $matchValue){
                    if(isset($matchValue['update_at'])){
                        unset($matchDataInfo[$matchKey]['update_at']);
                    }
                }
                $matchIdS = array_column($matchDataInfo,'matche_id');
                $matchIdSstr = !empty($matchIdS) ? implode(",", $matchIdS) : 0;
                // 比赛banPink地图数据
                $mapVetoesInfo = DbStream::getDb()->createCommand(
                    "select `matche_id`,`team_id`,`map`,`types` from map_vetoes where `matche_id` in ({$matchIdSstr}) "
                )->queryAll();

                $liveVideoInfo = DbStream::getDb()->createCommand(
                    "select `matche_id`,`live_order`,`streamer`,`country`,`embed_url`,`viewers`,`url` from live_video where `matche_id`  in  ($matchIdSstr) "
                )->queryAll();
                if (!empty($liveVideoInfo)) {
                    $liveVideoInfoArr = [];
                    foreach ($liveVideoInfo as $key => $val){
                        $liveVideoInfoArr[$val['matche_id']][] = [
                            'matche_id' => $val['matche_id'],
                            'live_order' => $val['live_order'],
                            'streamer' => $val['streamer'],
                            'country' => $val['country'],
                            'embed_url' => $val['embed_url'],
                            'viewers' => $val['viewers'],
                            'url' => $val['url'],
                        ];
                    }
                }
                // 处理地图数据
                $mapsVetoesArray = [];
                $types = ['removed'=>'1','picked'=>'2','was left over'=>'3'];
                foreach ($mapVetoesInfo as $k => $map){
                    $mapsVetoesArray[$map['matche_id']][] = [
                        'team' => $map['team_id'],
                        'map' => $map['map'], // 比赛禁用地图 未转
                        'type' => $map['types'],//比赛禁用地图类型
                    ];
                }
                foreach ($matchDataInfo as $key =>$val ){
                    $matchDataInfo[$key]['map_ban_pick'] = isset($mapsVetoesArray[$val['matche_id']]) ?
                        json_encode($mapsVetoesArray[$val['matche_id']]) : '';
                    $matchDataInfo[$key]['live_video'] = isset($liveVideoInfoArr[$val['matche_id']]) ?
                        json_encode($liveVideoInfoArr[$val['matche_id']]) : '';

                }




//                // 处理比赛数据
//                $standardMatchArray = [];
//                foreach ($matchDataInfo as $key => $match) {
////                    $standardMatch['origin_id'] = 7; // hltv 数据源id
//                    $standardMatch['rel_identity_id'] = $match['matche_id']; // 比赛id
//                    $standardMatch['match_type'] = $match['match_type']; // 比赛类型 未转
//                    $standardMatch['number_of_games'] = $match['number_of_games']; // 赛制局数
//                    $standardMatch['description'] = $match['description']; // 描述
//                    $standardMatch['tournament_id'] = $match['tournament_id']; // 赛事ID
//                    $standardMatch['scheduled_begin_at'] = $match['scheduled_begin_at']; // 计划开始时间
//                    $standardMatch['status'] = Common::getHLTVMatchStatus(['status'=>$match['status']]); // 状态
//                    $standardMatch['map_info'] = $match['maps']; // 比赛地图 未转
//                    $standardMatch['map_ban_pick'] = isset($mapsVetoesArray[$match['matche_id']]) ?
//                        json_encode($mapsVetoesArray[$match['matche_id']]) : ''; // 比赛地图 未转
//                    $standardMatch['team_1_id'] = $match['team_one']; // 主队战队id
//                    $standardMatch['team_2_id'] = $match['team_two']; // 客队战队id
//                    $standardMatch['team_1_score'] = $match['score_one']; // 主队分数
//                    $standardMatch['team_2_score'] = $match['score_two']; // 客队分数
//                    $standardMatch['begin_at'] = $match['match_time']; // 比赛开始时间
//                    $standardMatchArray[] = $standardMatch;
//                }
                $teamCount = $matchDataInfo;
                break;
            case "tournament":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from tournament_all $where order by id limit  {$offset},{$limit}"
                )->queryAll();
                foreach ($teamCount as $key => $value){
                    $teamCount[$key]['teams'] = self::getTeamsByTournamentId($value['tournament_id']);
                    $teamCount[$key]['prize_distribution'] = self::getPrizeByTournamentId($value['tournament_id']);
                }
                break;
        }
        return $teamCount;
    }

    /**
     * @param $tournamentId
     * @return
     * 通过$tournamentId获取战队信息
     */
    public static function getTeamsByTournamentId($tournamentId)
    {
        $teams = DbStream::getDb()->createCommand(
            "select id,tournament_id,teams_id as team_id,name,qualifier_from as match_condition,order_team as team_sort from team_snapshot where tournament_id =".$tournamentId." order by order_team"
        )->queryAll();
        if($teams){
            return $teams;
        }
    }

    public static function getPrizeByTournamentId($tournamentId)
    {
        $prize = DbStream::getDb()->createCommand(
            "select tournament_id,place as rank,prize_bonus as price,points_seed,team_id from prize_distribution where tournament_id =".$tournamentId." order by pod"
        )->queryAll();
        if($prize){
            return $prize;
        }
    }

}