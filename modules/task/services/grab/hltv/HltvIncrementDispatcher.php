<?php
/**
 *
 */

namespace app\modules\task\services\grab\hltv;

use app\modules\common\services\Consts;
use app\modules\data\models\DbStream;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\rest\exceptions\BusinessException;

class HltvIncrementDispatcher extends HltvBase implements TaskInterface, TaskCatchInterface
{
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $params=$taskParams["params"];
        return Common::requestPost($taskParams['url'], [], $params);
    }

    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if (isset($responseInfo['error']) || empty($responseInfo)) {
            throw new TaskRestException($responseInfo);
        }
        if($responseInfo['code'] == 200 || $responseInfo['code'] == 500){
            $relIdentityId = $responseInfo['data']['rel_identity_id'];
            $teamCount = '';
            switch ($responseInfo['data']['resource_type']) {
                case Consts::RESOURCE_TYPE_PLAYER:
                    $teamCount = DbStream::getDb()->createCommand(
                        "select * from player_all where player_id = $relIdentityId"
                    )->queryOne();

                    break;
                case "team":
                    $teamCount = DbStream::getDb()->createCommand(
                        "select * from team_all where team_id = $relIdentityId"
                    )->queryOne();

                    break;

                case "match":

                    $teamCount = DbStream::getDb()->createCommand(
                        "select * from match_all where matche_id = $relIdentityId"
                    )->queryOne();
                    $mapVetoesInfo = DbStream::getDb()->createCommand(
                        "select `matche_id`,`team_id`,`map`,`types` from map_vetoes where `matche_id`  =  {$relIdentityId} "
                    )->queryAll();

                    $liveVideoInfo = DbStream::getDb()->createCommand(
                        "select `matche_id`,`live_order`,`streamer`,`country`,`embed_url`,`viewers`,`url` from live_video where `matche_id`  =  {$relIdentityId} "
                    )->queryAll();

                    if (!empty($liveVideoInfo)) {
                        $liveVideoInfoArr = [];
                        foreach ($liveVideoInfo as $key => $val){
                            $liveVideoInfoArr[$val['matche_id']][] = [
                                'matche_id' => $val['matche_id'],
                                'live_order' => $val['live_order'],
                                'streamer' => $val['streamer'],
                                'country' => $val['country'],
                                'embed_url' => $val['embed_url'],
                                'viewers' => $val['viewers'],
                                'url' => $val['url'],
                            ];
                        }
                    }

                    if (!empty($mapVetoesInfo)) {
                        $mapsVetoesArray = [];
                        $types = ['removed'=>'1','picked'=>'2','was left over'=>'3'];
                        foreach ($mapVetoesInfo as $k => $map){
                            $mapsVetoesArray[$map['matche_id']][] = [
                                'team' => $map['team_id'],
                                'map' => $map['map'], // 比赛禁用地图 未转
                                'type' => $map['types'],//比赛禁用地图类型
                            ];
                        }
                    }

                    if (!empty($teamCount)) {
//                        foreach ($teamCount as $key =>$val ){
                            $teamCount['map_ban_pick'] = isset($mapsVetoesArray[$teamCount['matche_id']]) ?
                                json_encode($mapsVetoesArray[$teamCount['matche_id']]) : '';
//                        }
                        $teamCount['live_video'] = isset($liveVideoInfoArr[$teamCount['matche_id']]) ?
                            json_encode($liveVideoInfoArr[$teamCount['matche_id']]) : '';
                    }
                    break;
                case "tournament":
                    $teamCount = DbStream::getDb()->createCommand(
                        "select * from tournament_all where tournament_id = $relIdentityId"
                    )->queryOne();
                    if($teamCount){
                        $teamCount['teams'] = self::getTeamsByTournamentId($teamCount['tournament_id']);
                        $teamCount['prize_distribution'] = self::getPrizeByTournamentId($teamCount['tournament_id']);
                    }
                    break;
                default:
                    throw new BusinessException([], '该数据源没有此数据类型');
            }
            $subSteps = [];
            if($teamCount){
                if($responseInfo['data']['resource_type'] == Consts::RESOURCE_TYPE_PLAYER){
                    $subSteps[] = HltvPlayer::addItemAndGetSubInfo($teamCount, $batchId);
                }
                if($responseInfo['data']['resource_type'] == "team"){
                    $subSteps[] = HltvTeam::addItemAndGetSubInfo($teamCount, $batchId);
                }
                if($responseInfo['data']['resource_type'] == Consts::RESOURCE_TYPE_MATCH){
                    $subSteps[] = HltvMatch::addItemAndGetSubInfo($teamCount, $batchId);
                }
                if($responseInfo['data']['resource_type'] == Consts::RESOURCE_TYPE_TOURNAMENT){
                    $subSteps[] = HltvTournament::addItemAndGetSubInfo($teamCount, $batchId);
                }
            }
            return $subSteps;
        }
    }
}