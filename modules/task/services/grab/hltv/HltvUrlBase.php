<?php
/**
 *
 */

namespace app\modules\task\services\grab\hltv;

use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\pandascore\TaskDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class HltvUrlBase
{
    public static $config=[
        'base_url'=>'http://47.93.17.196:5000',
    ];

    /**
     * @param $taskInfo  (数组配置文件)
     * @return bool|string
     */
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        $action=$taskParams["action"];
        $params=$taskParams["params"];
        $url="http://".self::$config["base_url"].$action;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8'
        ];
        return Common::requestGet($url,[],$params);
    }

    public static function getInfo($action,$params)
    {
        if(!isset($params['token'])){
            $params['token']='SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM';
        }

        $url="http://".self::$config["base_url"].$action;
        $time=time()*1000;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8'
        ];
        return Common::requestGetCross($url,[],$params);
    }
}