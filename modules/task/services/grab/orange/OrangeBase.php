<?php
/**
 *
 */

namespace app\modules\task\services\grab\orange;


use app\modules\task\services\Common;

class OrangeBase
{
    public static $config = [
        'base_url' => '47.95.227.118:8080',
        'appid' => '24',
        'private_key' => 'tt5cl5et3pr1dx5dl6qi2qr3cb0jd8gm',
    ];

    public static function getResponse($taskInfo)
    {
        $taskParams = json_decode($taskInfo["params"], true);
        $action = $taskParams["action"];
        $params = isset($taskParams["params"])?$taskParams["params"]:[];

        return self::curlGet($action, [], $params);
    }

    public function getWsUrl($action,$params)
    {
        $url = "ws://" . self::$config["base_url"] . $action;
        $time = time();
        $params['appid'] = self::$config['appid'];
//        $params['ts'] = $time;
        ksort($params);
        $sign = self::getSign($action, "GET", $params);
        $params['sign'] = $sign;
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $wsUrl = $url . "?" . $query;
        return $wsUrl;
    }

    public static function curlGet($action, $method = "GET", $params)
    {
        $url = "http://" . self::$config["base_url"] . $action;
        $time = time();
        $params['appid'] = self::$config['appid'];
        $params['ts'] = $time;
        ksort($params);
        $sign = self::getSign($action, "GET", $params);
        $params['sign'] = $sign;
        return Common::requestGetCrossDev($url,[],$params);
//        return Common::requestGet($url, [], $params);
    }

    public static function getSign($action, $method = "GET", $params)
    {
        // 数组序列化
        $str = "";
        $str = $str . $method;

        $str = $str . '&' . $action;
//        $str=$str.'&'.$action;
        foreach ($params as $key => $val) {
            $str = $str . '&' . $key . '=' . $val;
        }
        $str = $str . '&' . self::$config['private_key'];
        $sign = md5(md5($str));
        return $sign;
    }
}
