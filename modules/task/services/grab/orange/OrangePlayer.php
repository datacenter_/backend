<?php

/**
 * 选手列表
 */
namespace app\modules\task\services\grab\orange;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\grab\abios\AbiosTeam;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class OrangePlayer extends OrangeBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];

        foreach ($responseInfo['data']['players'] as $key => $Item) {  //选手处理
            $subStep = self::addAndGetSubInfo($Item, $batchId,$refresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
//        if (count($responseInfo)) {
//            $pageNext = $taskInfo->toArray();
//            $params = json_decode($taskInfo["params"], true);
//            $paramsNext = $params;
//            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
//            $pageNext['params'] = $paramsNext;
//            $subSteps[] = $pageNext;
//        }

        return $subSteps;
    }
    //选手列表
    public static function addAndGetSubInfo($item, $batchId,$refresh="")
    {
        // 刷新
        $identityId = $item['player_id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ORANGE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_PLAYER, 2, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            // 触发
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ORANGE,
                QueueServer::QUEUE_RESOURCE_PLAYER,
                $diffInfo['change_type'],
                2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}