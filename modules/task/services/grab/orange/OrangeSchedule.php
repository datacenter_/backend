<?php

/**
 * 比赛列表
 */
namespace app\modules\task\services\grab\orange;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\grab\abios\AbiosTeam;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class OrangeSchedule extends OrangeBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $reason = $tagInfo['ext2_type'];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];
        if($reason == "todo"){
            foreach ($responseInfo['data'] as $key => $value){
                unset($value['tournament_id']);
                $subStep = self::addAndGetSubInfo($value, $batchId,$refresh,$reason);
                if ($subStep) {
                    $subSteps[] = $subStep;
                }
            }
        }else{
            foreach ($responseInfo['data']['stageList'] as $key => $stageList) {  //比赛的处理
                foreach($stageList['schedule'] as $schedule){
                    $schedule['tournament_id'] = $responseInfo['data']['eid'];
                    $subStep = self::addAndGetSubInfo($schedule, $batchId,$refresh);
                    if ($subStep) {
                        $subSteps[] = $subStep;
                    }
                }
            }
        }
        return $subSteps;
    }
    //比赛处理
    public static function addAndGetSubInfo($item, $batchId,$refresh="",$reason="")
    {
        $taskInfo=[];
        $identityId = $item['scheduleid'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ORANGE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, 2, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            // 触发比赛增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ORANGE,
                Consts::RESOURCE_TYPE_MATCH,
                $diffInfo['change_type'],
                2,
                $reason
            );
            $taskInfo= [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];

        }
        return $taskInfo;
    }
}