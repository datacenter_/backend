<?php

namespace app\modules\task\services\grab\orange;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class OrangeTeam extends OrangeBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];

        $subStep = self::addAndGetSubInfo($responseInfo['data'], $batchId,$refresh);
        if ($subStep) {
            $subSteps=array_merge($subSteps,$subStep);
        }
        return $subSteps;
    }
    //战队origin增量，选手grab增量
    public static function addAndGetSubInfo($item, $batchId,$refresh="")
    {
        $taskInfo=[];
        // 刷新战队
        $identityId = $item['team_id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ORANGE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TEAM, 2, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ORANGE,
                Consts::RESOURCE_TYPE_TEAM,
                $diffInfo['change_type'],
                2);
            $taskInfo[] = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];

        }
        // 刷新player
        foreach($item['players'] as  $player){
            $taskInfo[] = OrangePlayer::addAndGetSubInfo($player, $batchId,$refresh);
//            $identityId = $player['player_id'];
//            $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_PLAYER, 2, $identityId, $player);
//            if ($diffInfo['changed']) {
//                // 触发赛事增量
//                $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
//                    QueueServer::QUEUE_ORIGIN_ORANGE,
//                    Consts::RESOURCE_TYPE_PLAYER,
//                    $diffInfo['change_type'],
//                    2);
//                $taskInfo[] = [
//                    "tag" => $tag,
//                    "batch_id" => $batchId,
//                    "params" => $diffInfo,
//                ];
//
//            }
        }
        return $taskInfo;
    }
}