<?php

/**
 * 比赛列表
 */

namespace app\modules\task\services\grab\orange;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class OrangeTournament extends OrangeBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps = [];
        $params = json_decode($taskInfo['params'],true);
        //抓单条赛事
        if($params['filter[tournament]']){
            foreach ($responseInfo['data']['list'] as $key => $item){
                if($item['eid'] == $params['filter[tournament]']){
                    $subStep = self::getOneTournamentSubInfo($item, $batchId,$refresh);
                    if ($subStep) {
                        $subSteps[]= $subStep;
                    }
                }
            }
        }elseif ($params['filter[team]']){  //抓单条战队
            foreach ($responseInfo['data']['list'] as $teamKey => $teamItem) {  //战队处理
                foreach ($teamItem['seedteams'] as $team) {
                    if ($team['id'] == $params['filter[team]']) {
                        $subStep = self::getTeamSubInfo($team, $batchId, $refresh);
                        if ($subStep) {
                            $subSteps[] = $subStep;
                        }
                    }
                }
            }
        }elseif ($params['filter[player]']){ //抓单条选手
            foreach ($responseInfo['data']['list'] as $tournamentKey=>$tournamentValue){
                foreach ($tournamentValue['seedteams'] as $teamValue){
                    $teamUrl = "/lol/event/team";
                    $requestParams = ['teamid' => $teamValue['id']];
                    $teamJsonInfo = \app\modules\task\services\grab\orange\OrangeBase::curlGet($teamUrl, 'GET', $requestParams);
                    if($teamJsonInfo){
                        $teamArrayInfo = json_decode($teamJsonInfo,true);
                        foreach ($teamArrayInfo['data']['players'] as $playerValue){
                            if($playerValue['player_id'] == $params['filter[player]']){
                                $subStep = OrangePlayer::addAndGetSubInfo($playerValue, $batchId, $refresh);
                                if ($subStep) {
                                    $subSteps[] = $subStep;
                                }
                            }
                        }
                    }
                }
            }
        }elseif ($params['filter[match]']){  //抓单条比赛
            foreach ($responseInfo['data']['list'] as $tournamentKey=>$tournamentValue){
                $tournamentUrl = "/lol/event/schedule";
                $tournamentRequestParams = ['eid' => $tournamentValue['eid']];
                $tournamentJsonInfo = \app\modules\task\services\grab\orange\OrangeBase::curlGet($tournamentUrl, 'GET', $tournamentRequestParams);
                if($tournamentJsonInfo){
                    $tournamentArrayInfo = json_decode($tournamentJsonInfo,true);
                    if($tournamentArrayInfo['data']['stageList']){
                        foreach ($tournamentArrayInfo['data']['stageList'] as $stage){
                            foreach ($stage['schedule'] as $itemMatch){
                                $itemMatch['tournament_id'] = $tournamentArrayInfo['data']['eid'];
                                if($params['filter[match]'] == $itemMatch['scheduleid']){
                                    $subStep = OrangeSchedule::addAndGetSubInfo($itemMatch,$batchId,$refresh);
                                    if ($subStep) {
                                        $subSteps[] = $subStep;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else{  //全量
            foreach ($responseInfo['data']['list'] as $key => $Item) {  //赛事处理
                $subStep = self::getTournamentSubInfo($Item, $batchId,$refresh);
                if ($subStep) {
                    $subSteps= array_merge($subSteps,$subStep);
                }
                foreach ($Item['seedteams'] as $team) {  //参赛的战队处理
                    $subStep = self::getTeamSubInfo($team, $batchId,$refresh);
                    if ($subStep) {
                        $subSteps[] = $subStep;
                    }
                }
            }

        }
        return $subSteps;
    }

    //赛事处理
    public static function getTournamentSubInfo($item, $batchId,$refresh ="")
    {
        $taskInfoArray= [];
        $identityId = $item['eid'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ORANGE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TOURNAMENT, 2, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tournamentTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ORANGE,
                QueueServer::QUEUE_RESOURCE_TOURNAMENT,
                $diffInfo['change_type'],
                2);
            $taskInfoTournament = [
                "tag" => $tournamentTag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            $taskInfoArray[] =$taskInfoTournament;
        }
        // 触发比赛的增量
        $matchTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
            "",
            "",
            "",
            $refresh);
        $taskInfoMatch = [
            "tag" => $matchTag,
            "batch_id" => $batchId,
            'type' => OrangeSchedule::class,
            "params" => [
                "action" => "/lol/event/schedule",
                "params" => ['eid' => $item['eid']]
            ],
        ];
        $taskInfoArray[] = $taskInfoMatch;

        return $taskInfoArray;
    }
    //参赛战队
    public static function getTeamSubInfo($item, $batchId,$refresh="")
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
            "",
            "",
            "",
            $refresh);
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            'type' => OrangeTeam::class,
            "params" => [
                "action" => "/lol/event/team",
                "params" => ['teamid' => $item['id']]
            ],
        ];
        return $taskInfo;
    }
    //刷新一条赛事
    public static function getOneTournamentSubInfo($item, $batchId,$refresh)
    {
        $identityId = $item['eid'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_ORANGE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TOURNAMENT, 2, $identityId, $item,$refresh);
        if ($diffInfo['changed']) {
            $tournamentTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_ORANGE,
                QueueServer::QUEUE_RESOURCE_TOURNAMENT,
                $diffInfo['change_type'],
                2);
            $taskInfoTournament = [
                "tag" => $tournamentTag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfoTournament;
        }
    }
}