<?php
/**
 *
 */

namespace app\modules\task\services\grab\orange;

use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class Start
{
    // 此方法用于启动，刷新
    public static function run()
    {

        $startTask = [
            //lol英雄列表
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/lol/event/list",
                    "params" => [
                    ]
                ],
            ],
        //lol道具
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\riotgames\RiotItems::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "item",
//                    "params" => [
//                    ]
//                ],
//            ],
////            lol道具
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/items",
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 15
//                    ]
//                ],
//            ],
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('YmdHis');
        print_r($time);
        return $time;
    }
}