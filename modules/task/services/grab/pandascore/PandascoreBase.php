<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;


use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\pandascore\TaskDataTeam;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class PandascoreBase
{
    public static $config=[
        'base_url'=>'api.pandascore.co',
        'token'=>'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
        'client_secret'=>'be1fbdf8-7bad-44a2-b42e-a4f22474a778',
    ];

    /**
     * @param $taskInfo  数组配置文件
     * @return bool|string
     */
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        if(isset($taskParams['response']) && $taskParams['response']){
            return  json_encode($taskParams['response']);
        }
        $action=$taskParams["action"];
        $params=$taskParams["params"];
        $params['token']='SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM';
        $url="http://".self::$config["base_url"].$action;
        $time=time()*1000;
//        $sign=self::getSign($action,$time);
        $header=[
            "Content-Type"=>'application/json;charset=utf-8'
        ];
        return Common::requestGetCross($url,[],$params);
        return Common::requestGet($url,[],$params);
    }

    public static function getInfo($action,$params)
    {
        if(!isset($params['token'])){
            $params['token']='SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM';
        }

        $url="http://".self::$config["base_url"].$action;
        $time=time()*1000;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8'
        ];
        return Common::requestGetCross($url,[],$params);
        return Common::requestGet($url,[],$params);
    }

    public static function refreshTeam($params,$batchId,$gameId = 2)
    {
        unset($params['players']);
        $teamId=$params["id"];
        $params['current_videogame']=json_encode($params["current_videogame"]);
        $params['game_id']=$gameId;
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $teamInfo=TaskDataTeam::find()->where(["id"=>$teamId])->one();
        if($teamInfo){
            $oldInfo=$teamInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $teamInfo=new TaskDataTeam();
        }
        $teamInfo->setAttributes($params);
        // 更新数据，推送消息
        $teamInfo->save();
        if($teamInfo->getErrors()){
            print_r($teamInfo->getErrors());
            throw new TaskException(json_encode($teamInfo->getErrors()));
        }
        $newInfo=$teamInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_TEAM,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];

    }

    public static function refreshPlayer($playerInfo,$batchId, $gameId = 2)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $playerInfo["current_videogame"]=json_encode($playerInfo['current_videogame']);
        $playerInfo["game_id"]=$gameId;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $PlayerInfo=TaskDataPlayer::find()->where(["id"=>$playerInfo['id']])->one();
        if($PlayerInfo){
            $oldInfo=$PlayerInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $PlayerInfo=new TaskDataPlayer();
        }
        $PlayerInfo->setAttributes($playerInfo);
        // 更新数据，推送消息
        $PlayerInfo->save();
        if($PlayerInfo->getErrors()){
            print_r($PlayerInfo->getErrors());
            throw new TaskException(json_encode($PlayerInfo->getErrors()));
        }
        $newInfo=$PlayerInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }

        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_PLAYER,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];

        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }


}