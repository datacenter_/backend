<?php


namespace app\modules\task\services\grab\pandascore;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class PandascoreDotaAbilitiesList extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId,$matchRefresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            if(isset($paramsNext['response'])){

            }else{
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$matchRefresh ="")
    {
        // 刷新player
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_PANDASCORE);
        // 区分dota2天赋还是技能
        if(strpos($item['name'],'special_bonus')!==false){
            $resourceType = Consts::METADATA_TYPE_DOTA2_TALENT;
        }else{
            $resourceType = Consts::METADATA_TYPE_DOTA2_ABILITY;
        }
        $diffInfo = Common::setHotLog($originInfo['id'], $resourceType, 3, $identityId, $item,$matchRefresh); //修改game_id
        if ($diffInfo['changed']) {

            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                $resourceType, //修改类型
                $diffInfo['change_type'],
                3);  //game_id 修改
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}