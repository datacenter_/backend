<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class PandascoreDotaHeroList extends PandascoreBase implements TaskInterface, TaskCatchInterface
{

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        $matchRefresh = $tagInfo["ext_type"];
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        // 这里直接添加到player
        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId,$matchRefresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            if(isset($paramsNext['response'])){

            }else{
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId ,$matchRefresh ="")
    {
        // 刷新player
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_PANDASCORE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_DOTA2_HERO, 3, $identityId, $item,$matchRefresh);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                Consts::METADATA_TYPE_DOTA2_HERO,
                $diffInfo['change_type'],
                3);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }


//    private static function getAddInfoByItem($DotaItem, $batchId)
//    {
//        $info = [
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "", "", "", ""),
//            "type" => LOLPlayer::class,
//            "batch_id" => $batchId,
//            "params" => [
//                'action' => "/lol/players",
//                'params' => [
//                    "filter[id]" => $DotaItem['id'],
//                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                ]
//            ],
//        ];
//        return $info;
//    }

}