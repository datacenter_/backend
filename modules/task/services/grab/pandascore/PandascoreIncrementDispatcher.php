<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\RefreshResource;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskRunner;

class PandascoreIncrementDispatcher extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    /**
     * (incidents新增)
     */
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $refresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if($tagInfo['ext2_type']){
            $reason = $tagInfo['ext2_type'];
        }else{
            $reason = "refresh";
        }
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps =[];
        $matchIds = [];
        $playerIds = [];
        $teamIds = [];
        $seriesIds = [];

        $gameMatchId = '';
        $gamePlayerId = '';
        $gameTeamId = '';
        $gameSerieId = '';
        $matchInfos=[];
        $playerInfos=[];
        $teamInfos=[];
        $seriesInfos=[];
        foreach ($responseInfo as $key => $Item) {
            if($Item['change_type'] == 'creation' || $Item['change_type'] == 'update') {
                if ($Item['type'] == "match") {  //比赛
                    $gameMatchId = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $Item['object']['videogame']['id']);
                    $matchIds[] = $Item['id'];
                    $matchInfos[]=$Item['object'];
                }
                if ($Item['type'] == "player") {  //选手
                    $gamePlayerId = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $Item['object']['current_videogame']['id']);
                    $playerIds[] = $Item['id'];
                    $playerInfos[]=$Item['object'];
                }
                if ($Item['type'] == "team") {  //
                    $gameTeamId = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $Item['object']['current_videogame']['id']);
                    $teamIds[] = $Item['id'];
                    $teamInfos[] = $Item['object'];
                }
                if ($Item['type'] == "serie") {  //赛事
                    $gameSerieId = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $Item['object']['videogame']['id']);
                    $seriesIds[] = $Item['id'];
                    $seriesInfos[] = $Item['object'];
                }
            }elseif($Item['change_type'] == 'deletion'){  //数据源删除标记
                $resource_type = '';
                switch ($Item['type'])
                {
                    case 'player':
                        $resource_type = Consts::RESOURCE_TYPE_PLAYER;
                        break;
                    case 'team':
                        $resource_type = Consts::RESOURCE_TYPE_TEAM;
                        break;
                    case 'match':
                        $resource_type = Consts::RESOURCE_TYPE_MATCH;
                        break;
                    case 'serie':
                        $resource_type = Consts::RESOURCE_TYPE_TOURNAMENT;
                        break;
                    case 'tournament':
                        $resource_type = Consts::RESOURCE_TYPE_SON_TOURNAMENT;
                        break;
                }
                $game = Common:: getGameByMapperId(Consts::ORIGIN_PANDASCORE, $Item['object']['videogame_id']);
//                $relation = HotBase::getMainIdByRelIdentityId($resource_type,$Item['id'],3,$game['id']);  //是否有绑定关系
//                if($relation){
                    //删除任务
                    $transInfo = [
                        'deleted'=>1
                    ];
                    $diffInfo = OriginRunBase::setStandardData(3, $resource_type, $game['id'], $Item['id'], $transInfo);

                    if ($diffInfo['changed']) {
                        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                            QueueServer::QUEUE_ORIGIN_PANDASCORE,
                            $resource_type, //修改类型
                            $diffInfo['change_type'],
                            $game['id']);  //game_id 修改
                        $item = [
                            "tag" => $tag,
                            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_PANDASCORE,$resource_type),
                            "params" => $diffInfo,
                        ];
                        TaskRunner::addTask($item,1);
                    }
//                }
            }
        }
        if($matchIds){
            $matchIdsString = implode(',',$matchIds);
            $subStep = RefreshResource::getRefreshStageInfo($matchIdsString, Consts::RESOURCE_TYPE_MATCH, 3, $gameMatchId,$refresh,$reason, $matchInfos,$batchId);
            $subSteps[] = $subStep;
        }
        if($playerIds){
            $playerIdsString = implode(',',$playerIds);
            $subStep = RefreshResource::getRefreshStageInfo($playerIdsString, Consts::RESOURCE_TYPE_PLAYER, 3, $gamePlayerId,$refresh,$reason, $playerInfos,$batchId);
            $subSteps[] = $subStep;
        }
        if($teamIds){
            $teamIdsString = implode(',',$teamIds);
            $subStep = RefreshResource::getRefreshStageInfo($teamIdsString, Consts::RESOURCE_TYPE_TEAM, 3, $gameTeamId, $refresh,$reason, $teamInfos,$batchId);
            $subSteps[] = $subStep;
        }
        if($seriesIds){
            $seriesIdsString = implode(',',$seriesIds);
            $subStep = RefreshResource::getRefreshStageInfo($seriesIdsString, Consts::RESOURCE_TYPE_TOURNAMENT, 3, $gameSerieId, $refresh,$reason, $seriesInfos,$batchId);
            $subSteps[] = $subStep;
        }

        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }
        return $subSteps;
    }
}