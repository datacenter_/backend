<?php

/**
 * 比赛列表
 */
namespace app\modules\task\services\grab\pandascore;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class PandascoreMatchesList extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);  //tag字段处理后的数组
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $reason = $tagInfo["ext2_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        // 这里直接添加到matches
        $subSteps = [];

        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId,$reason,$matchRefresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if($reason == "incidents"){

        }else{
            if (count($responseInfo)) {
                $pageNext = $taskInfo->toArray();
                unset($pageNext['response']);
                $pageNext['status'] = 1;
                $params = json_decode($taskInfo["params"], true);
                $paramsNext = $params;
                $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
                if(isset($paramsNext['response'])){

                }else{
                    $pageNext['params'] = $paramsNext;
                    $pageNext['response'] = "";
                    $subSteps[] = $pageNext;
                }

//                $pageNext = $taskInfo->toArray();
//                $params = json_decode($taskInfo["params"], true);
//                $paramsNext = $params;
//                if( $paramsNext['params']['page']>20){
//
//                }else{
//                    $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
//                    $pageNext['params'] = $paramsNext;
//                    $subSteps[] = $pageNext;
//                }
            }
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$reason,$refreshType = "")
    {
        // 刷新
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_PANDASCORE);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $item['videogame']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, $gameInfo['id'], $identityId, $item,$refreshType);
        if ($diffInfo['changed']) {
            // 这里有变化，触发matches增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                QueueServer::QUEUE_RESOURCE_MATCH,
                $diffInfo['change_type'],
                $gameInfo['id'],$reason);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

}