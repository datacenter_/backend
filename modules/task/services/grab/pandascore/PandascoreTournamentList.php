<?php

/**
 * 赛事
 */
namespace app\modules\task\services\grab\pandascore;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class PandascoreTournamentList extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        // 这里直接添加到team
        $subSteps=[];
        foreach ($responseInfo as $key => $Item) {
            $subStep = self::addItemAndGetSubInfo($Item, $batchId,$matchRefresh);   //新增加的team信息
            if ($subStep) {
                $subSteps= array_merge($subSteps,$subStep);
            }
        }

        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            if(isset($paramsNext['response'])){

            }else{
                $pageNext['substeps'] = null;
                $pageNext['params'] = $paramsNext;
                $subSteps[] = $pageNext;
            }

//测试用
//            $pageNext = $taskInfo->toArray();
//            $params = json_decode($taskInfo["params"], true);
//            $paramsNext = $params;
//            if( $paramsNext['params']['page']>10){
//
//            }else{
//                $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
//                $pageNext['params'] = $paramsNext;
//                $subSteps[] = $pageNext;
//            }
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$matchRefresh="")
    {
        // 刷新tournament
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_PANDASCORE); //数据源类型（数组）
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $item['videogame']['id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TOURNAMENT, $gameInfo['id'], $identityId, $item,$matchRefresh);
        $subs=[];
        //tournament变化值
        if ($diffInfo['changed']) {
            // 这里有变化，触发team增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                QueueServer::QUEUE_RESOURCE_TOURNAMENT,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            $subs[]= $taskInfo;
        }
        //son_tournament变化值
        if ($diffInfo['changed']) {
            if (isset($item['tournaments'])) {
                foreach ($item['tournaments'] as $subTournaments) {
                    $subDiffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_SON_TOURNAMENT, $gameInfo['id'], $subTournaments['id'], $subTournaments,$matchRefresh);
                    if($subDiffInfo['changed']) {
                        $subTag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                            QueueServer::QUEUE_ORIGIN_PANDASCORE,
                            Consts::RESOURCE_TYPE_SON_TOURNAMENT,
                            $diffInfo['change_type'],
                            $gameInfo['id']);
                        $subTask = [
                            "tag" => $subTag,
                            "batch_id" => $batchId,
                            "params" => $subDiffInfo,
                        ];
                        $subs[] = $subTask;
                    }
                }
            }
        }
        return $subs;
    }

}