<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class Start
{
    // 此方法用于启动，刷新
    public static function run()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $startTask = [
            //lol召唤师技能
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/spells",
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ],
//            //lol英雄列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/champions",
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ],
            //lol道具
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/lol/items",
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ],
            //csgo地图
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSMapsList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/csgo/maps",  //地图csgo/maps
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
////            //csgo武器
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/csgo/weapons",  //地图csgo/maps
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 10
//                    ]
//                ],
//            ],
//            //dota道具
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/items",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota技能天赋
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/abilities",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            $item = [  //dota英雄
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/dota2/heroes",
                    "params" => [
                        "page" => 1,
                    ]
                ],
            ],
            // 队员列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascorePlayerList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/players",
//                    "params" => [
//                        "filter[videogame_id]" => 1,
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ],
//            // 战队列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascoreTeamList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/teams",
//                    "params" => [
////                        "filter[videogame_id]" => 1,
//                        "page" => 1,
////                        'per_page' => 20
//                    ]
//                ],
//            ],
            // 比赛列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascoreMatchesList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/matches",
//                    "params" => [
//
//                        "page" => 1,
////                        'per_page' => 15
//                    ]
//                ],
//            ],
            // 比赛列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascoreMatchesList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/matches",
////                    "sort" => "-scheduled_at",
//                    "params" => [
//                        "filter[begin_at]" =>"2020-08-24",
//                        "page" => 1,
////                        'per_page' => 20
//                    ]
//                ],
//            ],
        //更新比赛
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
//                "batch_id" => date("YmdHis"),
//                "params" => [
//                    "action" => "/incidents",
//                    "params" => [
//                        "since" =>$incidentTime,
//                        "page" => 1,
////                        'per_page' => 20
//                    ]
//                ],
//            ],
        //一个比赛
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascoreMatchesList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/matches",
//                    "params" => [
//                        "filter[id]" =>"562371",
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ],
//            // 赛事列表
//            [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                    "", "", "", ""),
//                "type" => PandascoreTournamentList::class,
//                "batch_id" => self::getNowTime(),
//                "params" => [
//                    "action" => "/series/running",
//                    "params" => [
//                        "page" => 1,
//                        'per_page' => 20
//                    ]
//                ],
//            ]
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('YmdHis');
        print_r($time);
        return $time;
    }
}