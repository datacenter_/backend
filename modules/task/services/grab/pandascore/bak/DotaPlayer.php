<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class DotaPlayer extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新队员信息
        $tasks = self::refreshPlayer($responseInfo[0], $batchId,3);

        return $tasks;
    }
}