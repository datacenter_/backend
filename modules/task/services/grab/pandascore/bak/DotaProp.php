<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\pandascore\TaskDataProp;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class DotaProp extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }
        // 更新道具信息
        $tasks = self::refreshProp($responseInfo,$batchId);

        return $tasks;
    }

    private static function refreshProp($propInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];

        $propInfo["game_id"]=3;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $prop=TaskDataProp::find()->where(["id"=>$propInfo["id"]])->one();
        if($prop){
            $oldInfo=$prop->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $prop=new TaskDataProp();
        }
        $prop->setAttributes($propInfo);
        // 更新数据，推送消息
        $prop->save();
        if($prop->getErrors()){
            print_r($prop->getErrors());
            throw new TaskException(json_encode($prop->getErrors()));
        }
        $newInfo=$prop->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_PROP,
            $changeType,
            Consts::METADATA_TYPE_DOTA2_ITEM);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}