<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\pandascore\TaskDataHero;
use app\modules\task\models\pandascore\TaskDataTalent;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class DotaTalent extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }
        $tasks = [];
        if (strpos($responseInfo['name'],'special_bonus') === 0 )
        {
            // 更新天赋信息
            $tasks = self::refreshTalent($responseInfo,$batchId,1);
        }elseif(strpos($responseInfo['name'],'ability') === 0)
        {
            // 更新天赋信息
            $tasks = self::refreshTalent($responseInfo,$batchId,2);
        }else{
            return [];
        }

        return $tasks;
    }

    private static function refreshTalent($talentInfo,$batchId,$type)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $talentInfo["game_id"]=3;
        $talentInfo["type"]=$type;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $talent=TaskDataTalent::find()->where(["id"=>$talentInfo["id"]])->one();
        if($talent){
            $oldInfo=$talent->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $talent=new TaskDataTalent();
        }
        $talent->setAttributes($talentInfo);
        // 更新数据，推送消息
        $talent->save();
        if($talent->getErrors()){
            print_r($talent->getErrors());
            throw new TaskException(json_encode($talent->getErrors()));
        }
        $newInfo=$talent->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_TALENT,
            $changeType,
            $type);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}