<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;


use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskRestException;


class DotaTeamList extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($originInfo, $taskInfo)
    {
        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        foreach($responseInfo as $key=>$Item){
            $subStep=self::getAddInfoByTeamItem($Item,$batchId);
            $subSteps[]=$subStep;
        }
        // 翻页
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            $params=json_decode($taskInfo["params"],true);
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }

        return $subSteps;
    }

    private static function getAddInfoByTeamItem($Item,$batchId)
    {
        $info=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>Dotateam::class,
            "batch_id"=>$batchId,
            "params"=>[
                'action'=>"/dota2/teams",
                'params' => [
                    'filter[id]' => $Item['id'],
                    "token"=>'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        return $info;
    }

}