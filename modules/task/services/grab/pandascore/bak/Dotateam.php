<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\models\pandascore\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class Dotateam extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新战队信息
        $tasks = self::refreshTeam($responseInfo[0],$batchId,3);

        // 选手战队信息
        $players = [];
        if (isset($responseInfo[0]['players']) && !empty($responseInfo[0]['players'])) {
            $players[] = self::refreshRelation($responseInfo[0]['players'],$responseInfo[0]['id'],$batchId);
        }

        return array_merge($tasks,$players);
    }

    private static function refreshRelation($players,$teamId,$batchId)
    {
        $taskInfo = [];
        $diff=[];
        $PlayerInfo = TeamPlayerRelation::find()->where(["team_id" => $teamId,'game_id' => 3])->asArray()->all();
        if ($PlayerInfo) {
            // 选手所在得所有战队
            $oldPlayerIds = array_column($PlayerInfo, 'player_id');
            $newPlayerIds = array_column($players,'id');

            $diff = array_diff($oldPlayerIds,$newPlayerIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                TeamPlayerRelation::deleteAll(["team_id" => $teamId]);

                $Players = new TeamPlayerRelation();
                foreach ($players as $key => $val) {
                    $Player = clone $Players;
                    $Player->setAttributes(["player_id" => $val['id'], 'team_id' => $teamId,'game_id' => 3]);
                    $Player->save();
                }

                $taskInfo = self::pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo);
            }
        } else {
            $changeType = QueueServer::QUEUE_TYPE_ADD;

            $PlayerInfo = new TeamPlayerRelation();
            foreach ($players as $key => $val) {
                $Player = clone $PlayerInfo;
                $Player->setAttributes(["player_id" => $val['id'], 'team_id' => $teamId,'game_id' => 3]);
                $Player->save();
            }

            $taskInfo = self::pushRefreshMessage($players, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($players, $changeType,$batchId,$diff,$PlayerInfo = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_PLAYER_TEAM,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['players' => $players,'diff' => $diff,'player_info' => $PlayerInfo],
        ];

        return $taskInfo;
    }
}