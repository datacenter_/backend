<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\pandascore\TaskDataHero;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLHero extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }

        // 更新英雄信息
        $tasks = self::refreshHero($responseInfo,$batchId);

        return $tasks;
    }

    private static function refreshHero($heroInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $heroInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $hero=TaskDataHero::find()->where(["id"=>$heroInfo["id"]])->one();
        if($hero){
            $oldInfo=$hero->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $hero=new TaskDataHero();
        }
        $hero->setAttributes($heroInfo);
        // 更新数据，推送消息
        $hero->save();
        if($hero->getErrors()){
            print_r($hero->getErrors());
            throw new TaskException(json_encode($hero->getErrors()));
        }
        $newInfo=$hero->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_HERO,
            $changeType,
            Consts::METADATA_TYPE_LOL_CHAMPION);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}