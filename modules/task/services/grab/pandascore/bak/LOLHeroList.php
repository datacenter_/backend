<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;


use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class LOLHeroList extends PandascoreBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $params = json_decode($taskInfo["params"], true);
        $responseInfo = json_decode($response, true);
        if (isset($responseInfo['error']) || empty($response)) {
            throw new TaskRestException($response);
        }
        $subSteps = [];
        foreach ($responseInfo as $key => $teamItem) {
            $subStep = self::getAddInfoByTeamItem($teamItem, $batchId);
            $subSteps[] = $subStep;
        }
        // 翻页
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            $paramsNext = $params;
            $paramsNext['params']['page'] = $paramsNext['params']['page'] + 1;
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }
        return $subSteps;
    }

    private static function getAddInfoByTeamItem($Item, $batchId)
    {
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => LOLHero::class,
            "batch_id" => $batchId,
            "params" => [
                'action' => "/lol/champions/" . $Item['id'],
                'params' => [
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        return $info;
    }

}