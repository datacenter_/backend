<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\pandascore\TaskDataRune;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLRune extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskException($responseInfo);
        }

        // 更新英雄信息
        $tasks = self::refreshRune($responseInfo,$batchId);

        return $tasks;
    }

    private static function refreshRune($runeInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $runeInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $rune=TaskDataRune::find()->where(["id"=>$runeInfo["id"]])->one();
        if($rune){
            $oldInfo=$rune->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $rune=new TaskDataRune();
        }
        $rune->setAttributes($runeInfo);
        // 更新数据，推送消息
        $rune->save();
        if($rune->getErrors()){
            print_r($rune->getErrors());
            throw new TaskException(json_encode($rune->getErrors()));
        }
        $newInfo=$rune->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_RUNE,
            $changeType,
            Consts::METADATA_TYPE_LOL_RUNE);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}