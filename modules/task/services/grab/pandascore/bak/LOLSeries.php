<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\models\pandascore\TaskDataRune;
use app\modules\task\models\pandascore\TaskDataSeries;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLSeries extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新赛事
        $tasks = self::refreshSeries($responseInfo[0],$batchId);

        return $tasks;
    }

    private static function refreshSeries($seriesInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $seriesInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $series=TaskDataSeries::find()->where(["id"=>$seriesInfo["id"]])->one();
        if($series){
            $oldInfo=$series->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $series=new TaskDataSeries();
        }
        $series->setAttributes($seriesInfo);
        // 更新数据，推送消息
        $series->save();
        if($series->getErrors()){
            print_r($series->getErrors());
            throw new TaskException(json_encode($series->getErrors()));
        }
        $newInfo=$series->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_SERIES,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}