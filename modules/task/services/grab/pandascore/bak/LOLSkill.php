<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\pandascore\TaskDataSkill;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class LOLSkill extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);

        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }

        // 更新英雄信息
        $tasks = self::refreshSkill($responseInfo,$batchId);

        return $tasks;
    }

    private static function refreshSkill($skillInfo,$batchId)
    {
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $skillInfo["game_id"]=2;
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $skill=TaskDataSkill::find()->where(["id"=>$skillInfo["id"]])->one();
        if($skill){
            $oldInfo=$skill->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $skill=new TaskDataSkill();
        }
        $skill->setAttributes($skillInfo);
        // 更新数据，推送消息
        $skill->save();
        if($skill->getErrors()){
            print_r($skill->getErrors());
            throw new TaskException(json_encode($skill->getErrors()));
        }
        $newInfo=$skill->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_SKILL,
            $changeType,
            Consts::METADATA_TYPE_LOL_ABILITY);
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];
    }
}