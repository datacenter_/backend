<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\models\pandascore\GroupTeamRelation;
use app\modules\task\models\pandascore\TaskDataBattle;
use app\modules\task\models\pandascore\TaskDataMatch;
use app\modules\task\models\pandascore\TaskDataTournament;
use app\modules\task\models\pandascore\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class Match extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            return [];
            throw new TaskRestException($responseInfo);
        }

        // 更新比赛
        $tasks = self::refreshMatch($responseInfo[0],$batchId);
        // 处理对局信息
        $battle = [];
        if (isset($responseInfo[0]['games']) && !empty($responseInfo[0]['games'])) {

            foreach ($responseInfo[0]['games'] as $key => $val) {
                $battle[] = self::refreshBattle($val,$batchId);
            }

        }

        return array_filter(array_merge($tasks,$battle));
    }
    private static function refreshMatch($info,$batchId)
    {
        $matchId = $info["id"];

        $params = [];
        $params['begin_at'] = date('Y-m-d H:i:s',strtotime($info['begin_at']));
        $params['scheduled_at'] = date('Y-m-d H:i:s',strtotime($info['scheduled_at']));
        $params['original_scheduled_at'] = date('Y-m-d H:i:s',strtotime($info['original_scheduled_at']));
        $params['end_at'] = !empty($info['end_at']) ? date('Y-m-d H:i:s',strtotime($info['end_at'])) : '';
        $params['id'] = $matchId;
        $params['serie_id'] = $info['serie_id'];
        $params['slug'] = $info['slug'];
        $params['tournament_id'] = $info['tournament_id'];
        $params['status'] = $info['status'];
        $params['rescheduled'] = $info['rescheduled'] ? 1 : 2;
        $params['detailed_stats'] = $info['detailed_stats'] ? 1 : 2;
        $params['draw'] = $info['draw'] ? 1 : 2;
        $params['forfeit'] = $info['forfeit'] ? 1 : 2 ;
        $params['game_advantage'] = $info['game_advantage'];
        $params['live_embed_url'] = $info['live_embed_url'];
        $params['live_url'] = $info['live_url'];
        $params['match_type'] = $info['match_type'];
        $params['name'] = $info['name'];
        $params['number_of_games'] = $info['number_of_games'];
        $params['winner_id'] = $info['winner_id'];
        $params['official_stream_url'] = $info['official_stream_url'];
        $params['live'] = json_encode($info['live']);
        if ($info['videogame']['id'] == 4) {
            $params['game'] = 3;
        }elseif ($info['videogame']['id'] == 1){
            $params['game'] = 2;
        }elseif ($info['videogame']['id'] == 3){
            $params['game'] = 1;
        }
        // 比赛下属战队
        if (!empty($info['opponents']))
        {
            if (isset($info['opponents'][0]['opponent']['id']))
            {
                $params['team_1_id'] = $info['opponents'][0]['opponent']['id'];
            }

            if (isset($info['opponents'][1]['opponent']['id']))
            {
                $params['team_2_id'] = $info['opponents'][1]['opponent']['id'];
            }
        }
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $matchInfo=TaskDataMatch::find()->where(["id"=>$matchId])->one();
        if($matchInfo){
            $oldInfo=$matchInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $matchInfo=new TaskDataMatch();
        }
        $matchInfo->setAttributes($params);
        // 更新数据，推送消息
        $matchInfo->save();
        if($matchInfo->getErrors()){
            print_r($matchInfo->getErrors());
            throw new TaskException(json_encode($matchInfo->getErrors()));
        }
        $newInfo=$matchInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_MATCH,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];

    }

    private static function refreshBattle($info,$batchId)
    {
        $battleId = $info["id"];

        $params = [];
        $params['begin_at'] = !empty($info['end_at']) ? date('Y-m-d H:i:s',strtotime($info['begin_at'])) : '';
        $params['end_at'] = !empty($info['end_at']) ? date('Y-m-d H:i:s',strtotime($info['end_at'])) : '';
        $params['id'] = $battleId;
        $params['status'] = $info['status'];
        $params['detailed_stats'] = $info['detailed_stats'] ? 1 : 2;
        $params['finished'] = $info['finished'] ? 1 : 2;
        $params['length'] = $info['length'];
        $params['forfeit'] = $info['forfeit'] ? 1 : 2 ;
        $params['match_id'] = $info['match_id'];
        $params['position'] = $info['position'];
        $params['winner'] = json_encode($info['winner']);


        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $battleInfo=TaskDataBattle::find()->where(["id"=>$info['id']])->one();
        if($battleInfo){
            $oldInfo=$battleInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $battleInfo=new TaskDataBattle();
        }
        $battleInfo->setAttributes($params);
        // 更新数据，推送消息
        $battleInfo->save();
        if($battleInfo->getErrors()){
            print_r($battleInfo->getErrors());
            throw new TaskException(json_encode($battleInfo->getErrors()));
        }
        $newInfo=$battleInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_BATTLE,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? $taskInfo : [];

    }
}