<?php
/**
 *
 */

namespace app\modules\task\services\grab\pandascore;

use app\modules\task\models\pandascore\GroupTeamRelation;
use app\modules\task\models\pandascore\TaskDataTournament;
use app\modules\task\models\pandascore\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class Tournament extends PandascoreBase implements TaskInterface,TaskCatchInterface
{
    const ACTION="";
    public static function run($tag, $taskInfo)
    {

        $response=$taskInfo["response"];
        $batchId=$taskInfo["batch_id"];
        $responseInfo=json_decode($response,true);
        if(isset($responseInfo['error']) || empty($responseInfo)){
            throw new TaskRestException($responseInfo);
        }

        // 更新分组信息
        $tasks = self::refreshTournament($responseInfo[0],$batchId);
        // 分组战队信息
        $groupTeam = [];
        if (isset($responseInfo[0]['teams']) && !empty($responseInfo[0]['teams'])) {
            $groupTeam[] = self::refreshRelation($responseInfo[0]['teams'],$responseInfo[0]['id'],$batchId,$responseInfo[0]['serie_id']);
        }
        return array_merge($tasks,$groupTeam);
    }
    private static function refreshTournament($tournament,$batchId)
    {
        $tournamentId=$tournament["id"];

        $params = [];
        if ($tournament['videogame']['id'] == 4)
        {
            $params['game_id'] = 3;
        }
        if ($tournament['videogame']['id'] == 1)
        {
            $params['game_id'] = 2;
        }
        $params['begin_at'] = date('Y-m-d H:i:s',strtotime($tournament['begin_at']));
        $params['end_at'] = empty($tournament['end_at']) ? date('Y-m-d H:i:s',strtotime($tournament['end_at'])) : '';
        $params['id'] = $tournament['id'];
        $params['slug'] = $tournament['slug'];
        $params['serie_id'] = $tournament['serie_id'];
        $params['name'] = $tournament['name'];
        $diff=[];
        $oldInfo=[];
        $newInfo=[];
        $changeType=QueueServer::QUEUE_TYPE_CHANGE;
        $tournamentInfo=TaskDataTournament::find()->where(["id"=>$tournamentId])->one();
        if($tournamentInfo){
            $oldInfo=$tournamentInfo->toArray();
        }else{
            $changeType=QueueServer::QUEUE_TYPE_ADD;
            $tournamentInfo=new TaskDataTournament();
        }
        $tournamentInfo->setAttributes($params);
        // 更新数据，推送消息
        $tournamentInfo->save();
        if($tournamentInfo->getErrors()){
            print_r($tournamentInfo->getErrors());
            throw new TaskException(json_encode($tournamentInfo->getErrors()));
        }
        $newInfo=$tournamentInfo->toArray();
        if($changeType==QueueServer::QUEUE_TYPE_CHANGE&&isset($oldInfo)){
            $diff=Common::diffParams($oldInfo,$newInfo);
        }
        $tag=QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT_GROUP,
            $changeType,
            "");
        $taskInfo=[
            "tag"=>$tag,
            "batch_id"=>$batchId,
            "params"=>[
                "diff"=>$diff,
                "new"=>$newInfo,
                "old"=>$oldInfo
            ],
        ];
        return !empty($diff) && $changeType == QueueServer::QUEUE_TYPE_CHANGE || $changeType == QueueServer::QUEUE_TYPE_ADD ? [$taskInfo] : [];

    }
    private static function refreshRelation($teams,$groupId,$batchId,$serieId)
    {
        $taskInfo = [];
        $diff=[];
        $groupTeamInfo = GroupTeamRelation::find()->where(["group_id" => $groupId])->asArray()->all();

        if ($groupTeamInfo) {
            // 选手所在得所有战队
            $oldPlayerIds = array_column($groupTeamInfo, 'team_id');
            $newPlayerIds = array_column($teams,'id');

            $diff = array_diff($oldPlayerIds,$newPlayerIds);

            if (!empty($diff)) {
                $changeType = QueueServer::QUEUE_TYPE_CHANGE;
                // 先删除所有关系
                GroupTeamRelation::deleteAll(["group_id" => $groupId]);

                $groupTeam = new GroupTeamRelation();
                foreach ($teams as $key => $val) {
                    $gt = clone $groupTeam;
                    $gt->setAttributes(["team_id" => $val['id'], 'tournament_id' => $serieId,'group_id' => $groupId]);
                    $gt->save();
                }

                $taskInfo = self::pushRefreshMessage($teams, $changeType,$batchId,$diff,$groupTeamInfo);
            }
        } else {

            $changeType = QueueServer::QUEUE_TYPE_ADD;
            $groupTeam = new GroupTeamRelation();
            foreach ($teams as $key => $val) {
                $gt = clone $groupTeam;
                $gt->setAttributes(["team_id" => $val['id'], 'tournament_id' => $serieId,'group_id' => $groupId]);
                $gt->save();
            }
            $taskInfo = self::pushRefreshMessage($teams, $changeType,$batchId,$diff);
        }

        return $taskInfo;
    }

    private static function pushRefreshMessage($data, $changeType,$batchId,$diff,$Info = [])
    {
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
            QueueServer::QUEUE_ORIGIN_PANDASCORE,
            QueueServer::QUEUE_RESOURCE_GROUP_TEAM,
            $changeType,
            "");
        $taskInfo = [
            "tag" => $tag,
            "batch_id" => $batchId,
            "params" => ['teams' => $data,'diff' => $diff,'group_team_info' => $Info],
        ];

        return $taskInfo;
    }
}