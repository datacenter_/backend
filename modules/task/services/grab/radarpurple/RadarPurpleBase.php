<?php
/**
 *
 */

namespace app\modules\task\services\grab\radarpurple;

use app\modules\data\models\DbStream;
class RadarPurpleBase
{
    public static function getResponse($taskInfo)
    {
        $taskParams = json_decode($taskInfo["params"], true);
        $type = $taskParams["type"];
        $page = $taskParams["offset"];
        $pageCount = $taskParams["limit"];
        $where = '';
        if(isset($taskParams['where']) && $taskParams['where']){
            $where = $taskParams["where"];
        }
        return json_encode(self::getFromDb($type,$page,$pageCount,$where));
    }

    public static function getFromDb($type,$offset,$limit,$where = '')
    {
//        if($where){
//            $andWhereString = 'where';
//            foreach ($where as $key => $value){
//                $andWhereString = $andWhereString. " $key = '$value'";
//            }
//        }
        switch ($type) {
            case "player":

                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_player  $where order by id limit  {$offset},{$limit}"
                )->queryAll();

                break;
            case "team":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_team $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();
                break;

            case "dota2_item":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_equipments $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();
                break;

            case "dota2_talent":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_rune $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();
                break;

            case "tournament":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_tournament $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();
                break;

            case "match":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_match $where  order by id  limit  {$offset},{$limit}"
                )->queryAll();
                break;

            case "dota2_hero":
                $teamCount = DbStream::getDb()->createCommand(
                    "select * from ez_hero $where order by id limit  {$offset},{$limit}"
                )->queryAll();
                break;
        }
        return $teamCount;
    }

    /**
     * @param $tournamentId
     * @return
     * 通过$tournamentId获取战队信息
     */
    public static function getTeamsByTournamentId($tournamentId)
    {
        $teams = DbStream::getDb()->createCommand(
            "select id,tournament_id,teams_id as team_id,name,qualifier_from as match_condition,order_team as team_sort from team_snapshot where tournament_id =".$tournamentId." order by order_team"
        )->queryAll();
        if($teams){
            return $teams;
        }
    }

    public static function getPrizeByTournamentId($tournamentId)
    {
        $prize = DbStream::getDb()->createCommand(
            "select tournament_id,place as rank,prize_bonus as price,points_seed,team_id from prize_distribution where tournament_id =".$tournamentId." order by pod"
        )->queryAll();
        if($prize){
            return $prize;
        }
    }

}