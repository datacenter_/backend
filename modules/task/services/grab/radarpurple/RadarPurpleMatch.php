<?php
/**
 *
 */

namespace app\modules\task\services\grab\radarpurple;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class RadarPurpleMatch extends RadarPurpleBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag,$taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        $subSteps = [];
        foreach ($responseInfo as $key => $item) {
            $subStep = self::addItemAndGetSubInfo($item, $batchId,$matchRefresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['offset'] = $paramsNext['offset'] + $paramsNext['limit'];
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$matchRefresh="")
    {
        $identityId = $item['match_id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RADAR_PURPLE);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $item['game_id']);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_MATCH, $gameInfo['id'], $identityId, $item,$matchRefresh);
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                Consts::ORIGIN_RADAR_PURPLE,
                Consts::RESOURCE_TYPE_MATCH,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}