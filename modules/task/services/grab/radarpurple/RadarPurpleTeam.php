<?php
/**
 *
 */

namespace app\modules\task\services\grab\radarpurple;


use app\modules\common\services\Consts;
use app\modules\data\models\DbStream;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class RadarPurpleTeam extends RadarPurpleBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag,$taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        $matchRefresh = $tagInfo["ext_type"];

        $subSteps = [];
        foreach ($responseInfo as $key => $item) {
            $subStep = self::addItemAndGetSubInfo($item, $batchId,$matchRefresh);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        if (count($responseInfo)) {
            $pageNext = $taskInfo->toArray();
            unset($pageNext['response']);
            $pageNext['status'] = 1;
            $params = json_decode($taskInfo["params"], true);
            $paramsNext = $params;
            $paramsNext['offset'] = $paramsNext['offset'] + $paramsNext['limit'];
            $pageNext['params'] = $paramsNext;
            $subSteps[] = $pageNext;
        }

        return $subSteps;
    }

    public static function addItemAndGetSubInfo($item, $batchId,$matchRefresh = "")
    {
        // 添加到hotlog，对比变化，如果有变化插入子任务 如果没有不插入
        $identityId = $item['team_id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RADAR_PURPLE);
        $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $item['game_id']);

        $diffInfo = Common::setHotLog($originInfo['id'], Consts::RESOURCE_TYPE_TEAM, $gameInfo['id'], $identityId, $item,$matchRefresh);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                Consts::ORIGIN_RADAR_PURPLE,
                QueueServer::QUEUE_RESOURCE_TEAM,
                $diffInfo['change_type'],
                $gameInfo['id']);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}