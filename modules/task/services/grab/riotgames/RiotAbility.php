<?php

namespace app\modules\task\services\grab\riotgames;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class RiotAbility extends RiotgamesBase implements TaskInterface, TaskCatchInterface
{
    public static function getResponse($taskInfo)
    {
        $abilityUrl = "http://".env('OSS_Bucket').".".env('OSS_Endpoint')."/riotgames/curl/championFull.json";
        $abilityData = Common::requestGet($abilityUrl,[],[]);
        $abilityUrlZhCn = "http://".env('OSS_Bucket').".".env('OSS_Endpoint')."/riotgames/curl/championFullzh_CN.json";
        $abilityDataZhCn = Common::requestGet($abilityUrlZhCn,[],[]);
        $enusArray = json_decode($abilityData,true);
        $result['en_us'] = $enusArray;

        $zhcnArray = json_decode($abilityDataZhCn,true);
        $result['zh_cn'] = $zhcnArray;
        return json_encode($result);
    }

    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $taskParams = json_decode($taskInfo["params"], true);
        $params = $taskParams["params"];


        $responseInfo = json_decode($response, true);
        if (isset($responseInfo['error']) || empty($responseInfo)) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps =[];
        foreach ($responseInfo['en_us']['data'] as $en_usKey => $en_usValue){
            foreach ($en_usValue['spells'] as $spellKey => $spellValue){
                $subStep = [];
                if(isset($params['id']) && $params['id']) {
                    if ($responseInfo['en_us']['data'][$en_usKey]['spells'][$spellKey]['id'] ==  $params['id']){
                        $spellValue['champion_id'] = $en_usValue['key'];
                        $zh_chSpells = $responseInfo['zh_cn']['data'][$en_usKey]['spells'][$spellKey];
                        $subStep = self::addItemAndGetSubInfo($spellValue, $zh_chSpells, $batchId);
                    }
                }else{
                    $spellValue['champion_id'] = $en_usValue['key'];
                    $zh_chSpells = $responseInfo['zh_cn']['data'][$en_usKey]['spells'][$spellKey];
                    $subStep = self::addItemAndGetSubInfo($spellValue, $zh_chSpells, $batchId);
                }

                if($subStep){
                    $subSteps[] = $subStep;
                }
            }

        }
        return $subSteps;
    }

    public static function addItemAndGetSubInfo($spellValue, $zh_chSpells, $batchId)
    {
        $spellValue['name_cn'] = $zh_chSpells['name'];

        $spellValue['description_cn'] = $zh_chSpells['description'];

        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RIOTGAMES);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_LOL_ABILITY, 2, $spellValue['id'], $spellValue);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                Consts::ORIGIN_RIOTGAMES,
                Consts::METADATA_TYPE_LOL_ABILITY,
                $diffInfo['change_type'],
                2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}