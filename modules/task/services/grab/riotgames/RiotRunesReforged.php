<?php

namespace app\modules\task\services\grab\riotgames;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class RiotRunesReforged extends RiotgamesBase implements TaskInterface, TaskCatchInterface
{
//    public static function getResponse($taskInfo)
//    {
//          $base = \Yii::$app->basePath;
//          $runnes_path = $base.'/data/runesReforged.json';
//          $file = file_get_contents($runnes_path,'r');
//          $enusArray = json_decode($file,true);
//          $result['en_us'] = $enusArray;
//
//          $runnes_pathzhCN = $base.'/data/runesReforgedzh_CN.json';
//          $filezh_CN = file_get_contents($runnes_pathzhCN,'r');
//          $zhcnArray = json_decode($filezh_CN,true);
//          $result['zh_cn'] = $zhcnArray;
//          return json_encode($result);
//    }
    public static function getResponse($taskInfo)
    {
        $runesUrl = "http://".env('OSS_Bucket').".".env('OSS_Endpoint')."/riotgames/curl/runesReforged.json";
        $runesData = Common::requestGet($runesUrl,[],[]);
        $runesUrlZhCn = "http://".env('OSS_Bucket').".".env('OSS_Endpoint')."/riotgames/curl/runesReforgedzh_CN.json";
        $runesDataZhCn = Common::requestGet($runesUrlZhCn,[],[]);

        $enusArray = json_decode($runesData,true);
        $result['en_us'] = $enusArray;

        $zhcnArray = json_decode($runesDataZhCn,true);
        $result['zh_cn'] = $zhcnArray;

        return json_encode($result);
    }

    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        $taskParams = json_decode($taskInfo["params"], true);
        $params = $taskParams["params"];
        if (isset($responseInfo['error']) || empty($responseInfo)) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps =[];

        $infoMerge=[];
        foreach ($responseInfo['en_us'] as $key => $item) {
            foreach ($item['slots'] as $slots_item) {
                foreach ($slots_item['runes'] as $rItems){
                    $rItems['path_name'] = $item['key'];
                    $infoMerge[$rItems['id']]['en_us']=$rItems;
                }
            }
        }
        foreach ($responseInfo['zh_cn'] as $key => $item) {
            foreach ($item['slots'] as $slots_item) {
                foreach ($slots_item['runes'] as $rItems){
                    $rItems['path_name'] = $item['name'];
                    $infoMerge[$rItems['id']]['zh_cn']=$rItems;
                }
            }
        }
        foreach ($infoMerge as $key => $item){
            $subStep = [];
            if(isset($params['id']) && $params['id']) {
                if ($item['en_us']['id'] == $params['id']) {
                    $subStep = self::addItemAndGetSubInfo($item, $batchId);

                }
            }else{
                $subStep = self::addItemAndGetSubInfo($item, $batchId);
            }

            if($subStep){
                $subSteps[] = $subStep;
            }
        }
        return $subSteps;

//        foreach ($responseInfo as $key => $item) {
//                foreach ($item['slots'] as $slots_item) {
//                    foreach ($slots_item['runes'] as $Item){
//                        $Item['path_name'] = $item['key'];
//                        $subStep = self::addItemAndGetSubInfo($Item, $batchId);
//                        if ($subStep) {
//                            $subSteps[] = $subStep;
//                        }
//                      }
//                }
//        }
    }

    public static function addItemAndGetSubInfo($item, $batchId)
    {
        // 刷新player
        $identityId = $item['en_us']['id'];
        $item['en_us']['name_cn'] = $item['zh_cn']['name'];
        $item['en_us']['path_name_cn'] = $item['zh_cn']['path_name'];
        $item['en_us']['description_cn'] = $item['zh_cn']['shortDesc'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RIOTGAMES);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_LOL_RUNE, 2, $identityId, $item['en_us']);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                Consts::ORIGIN_RIOTGAMES,
                Consts::METADATA_TYPE_LOL_RUNE,
                $diffInfo['change_type'],
                2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}