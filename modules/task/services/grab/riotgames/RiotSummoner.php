<?php

namespace app\modules\task\services\grab\riotgames;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class RiotSummoner extends RiotgamesBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($originInfo, $taskInfo)
    {
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $responseInfo = json_decode($response, true);
        if (isset($responseInfo['error']) || empty($responseInfo)) {
            throw new TaskRestException($responseInfo);
        }
        $subSteps =[];
        foreach ($responseInfo['en_us']['data'] as $en_usKey => $en_usItem) {
            $subStep = [];
            if(isset($responseInfo['params']['id']) && $responseInfo['params']['id']) {
                if ($responseInfo['en_us']['data'][$en_usKey]['key'] == $responseInfo['params']['id']) {
                    $zh_cnItem=@$responseInfo['zh_cn']['data'][$en_usKey];
                    $subStep = self::addItemAndGetSubInfo($en_usItem,$zh_cnItem, $batchId);
                }

                }else{
                $zh_cnItem=@$responseInfo['zh_cn']['data'][$en_usKey];
                $subStep = self::addItemAndGetSubInfo($en_usItem,$zh_cnItem, $batchId);
            }


            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }

//        foreach ($responseInfo['en_us']['data'] as $en_usKey => $en_usItem) {
//            foreach ($responseInfo['zh_cn']['data'] as $zh_cnKey => $zh_cnItem){
//                $subStep = self::addItemAndGetSubInfo($en_usItem,$zh_cnItem, $batchId);
//                if ($subStep) {
//                    $subSteps[] = $subStep;
//                }
//            }
//        }
        return $subSteps;
    }
    public static function addItemAndGetSubInfo($en_usItem,$zh_cnItem, $batchId)
    {
        // 刷新player
        $identityId = $en_usItem['key'];
        $en_usItem['name_cn'] = $zh_cnItem['name'];
        $en_usItem['description_cn'] = $zh_cnItem['description'];

        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RIOTGAMES);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, 2, $identityId, $en_usItem);
        if ($diffInfo['changed']) {
            // 这里有变化，触发player增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                Consts::ORIGIN_RIOTGAMES,
                Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,
                $diffInfo['change_type'],
                2);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }

//    public static function addItemAndGetSubInfo($en_usItem,$zh_cnItem, $batchId)
//    {
//        // 刷新player
//        $identityId = $item['key'];
//        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_RIOTGAMES);
//        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_LOL_SUMMONER_SPELL, 2, $identityId, $item);
//        if ($diffInfo['changed']) {
//            // 这里有变化，触发player增量
//            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
//                Consts::ORIGIN_RIOTGAMES,
//                Consts::METADATA_TYPE_LOL_SUMMONER_SPELL,
//                $diffInfo['change_type'],
//                2);
//            $taskInfo = [
//                "tag" => $tag,
//                "batch_id" => $batchId,
//                "params" => $diffInfo,
//            ];
//            return $taskInfo;
//        }
//    }
}