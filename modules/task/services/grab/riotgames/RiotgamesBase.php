<?php
/**
 *
 */

namespace app\modules\task\services\grab\riotgames;


use app\modules\task\services\Common;

class RiotgamesBase
{
    public static $config = [
        'base_url' => '',
    ];

    public static function getResponse($taskInfo)
    {
        $newVersion = self::curlNewVersion();
        // 英文
        self::$config["base_url"]='ddragon.leagueoflegends.com/cdn/'.$newVersion.'/data/en_US/';
        $taskParams = json_decode($taskInfo["params"], true);
        $action = $taskParams["action"];
        $params = $taskParams["params"];
        $enus=self::curlGet($action, [], $params);
        $enusArray = json_decode($enus,true);
        $result['en_us'] = $enusArray;
        // 中文
        self::$config["base_url"]='ddragon.leagueoflegends.com/cdn/'.$newVersion.'/data/zh_CN/';
        $zhcn=self::curlGet($action, [], $params);
        $zhcnArray = json_decode($zhcn,true);
        $result['zh_cn'] = $zhcnArray;
        $result['params'] = $params;

        return json_encode($result);
    }

    public static function curlGet($action, $method = "GET", $params)
    {
        $url = "http://" . self::$config["base_url"] . $action.".json";
        $time = time();
        return Common::requestGet($url,[],$params);
    }
    public static function curlNewVersion()
    {
        $url = "https://ddragon.leagueoflegends.com/api/versions.json";
        $result = Common::requestGet($url, [],[]);
        $resultArray= explode(',',$result);
        $newVersion = str_replace(['[','"'],'',$resultArray[0]);
        return $newVersion;
    }
}