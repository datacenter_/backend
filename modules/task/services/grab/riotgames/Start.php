<?php
/**
 *
 */

namespace app\modules\task\services\grab\riotgames;

use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class Start
{
    // 此方法用于启动，刷新
    public static function run()
    {

        $startTask = [
            //lol技能
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotAbility::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "ability",
                    "params" => [
                    ]
                ],
            ],
            //lol英雄列表
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotChampions::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "champion",
                    "params" => [
                    ]
                ],
            ],
//        lol道具
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotItems::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "item",
                    "params" => [
                    ]
                ],
            ],
//            召唤师技能
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotSummoner::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "summoner",
                    "params" => [
                    ]
                ],
            ],
            //符文
            [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotRunesReforged::class,
                "batch_id" => date("YmdHis"),
            ],
        ];
        foreach ($startTask as $task) {
            TaskRunner::addTask($task, 1);
        }
    }

    private static function getNowTime()
    {
        date_default_timezone_set('PRC');
        $time = date('YmdHis');
        print_r($time);
        return $time;
    }
}