<?php


namespace app\modules\task\services\grab\valve;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;

class ValueDotaItemList extends ValveDotaBase implements TaskInterface, TaskCatchInterface
{
    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }
        $name_en = array_column($responseInfo['en']['result']['items'],'localized_name','id');

        foreach ($responseInfo['zh']['result']['items'] as $key => &$val) {
            $val['name_en'] = $name_en[$val['id']];
        }


        $subSteps = [];

        foreach ($responseInfo['zh']['result']['items'] as $key => $Item) {
            $subStep = [];
            //单抓一条的情况
            if(isset($responseInfo['params']['id']) && $responseInfo['params']['id']){
                if($responseInfo['zh']['result']['items'][$key]['id'] == $responseInfo['params']['id']){
                    $subStep = self::addItemAndGetSubInfo($Item, $batchId,$matchRefresh);
                }
            }else{
                $subStep = self::addItemAndGetSubInfo($Item, $batchId,$matchRefresh);
            }
//            $subStep = self::addItemAndGetSubInfo($Item, $batchId);
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        return $subSteps;
    }


    public static function addItemAndGetSubInfo($item, $batchId,$matchRefresh = "")
    {
        // 刷新player
        $identityId = $item['id'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_VALVE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_DOTA2_ITEM, 3, $identityId, $item,$matchRefresh); //修改game_id
        if ($diffInfo['changed']) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_VALVE,
                Consts::METADATA_TYPE_DOTA2_ITEM, //修改类型
                $diffInfo['change_type'],
                3);  //game_id 修改
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }


}