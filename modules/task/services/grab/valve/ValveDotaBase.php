<?php
/**
 * valve dota数据
 */

namespace app\modules\task\services\grab\valve;

use app\modules\task\services\Common;


class ValveDotaBase
{
    public static $config=[
        'base_url'=>'http://api.steampowered.com',
    ];

    /**
     * @param $taskInfo  (数组配置文件)
     * @return bool|string
     */
    public static function getResponse($taskInfo)
    {
        $taskParams=json_decode($taskInfo["params"],true);
        if(isset($taskParams['response']) && $taskParams['response']){
            return  json_encode($taskParams['response']);
        }
        $action=$taskParams["action"];
        $params=$taskParams["params"];
        $params['key']='48FC6CC98E6C2E795603BD7DFC9A555B';
        $url=self::$config["base_url"].$action;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8'
        ];
        if($params['language']){
            $res = [];
            $languageExt = explode(",",$params['language']);
            foreach ($languageExt as $value){
                $params['language'] = $value;
                $one = Common::requestGet($url,[],$params);
                if($one){
                    $res[$value] = json_decode($one,true);
                }
            }
            $res['params'] = $params;
            return json_encode($res);
        }else{
            $one =  Common::requestGet($url,[],$params);
            $res = json_decode($one,true);
            $res['params'] = $params;
            return  json_encode($res);
        }
    }
}