<?php
/**
 *
 */

namespace app\modules\task\services\grab\valve;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskCatchInterface;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\TaskTypes;

class ValveDotaHeroList extends ValveDotaBase implements TaskInterface, TaskCatchInterface
{
    const ACTION = "";

    public static function run($tag, $taskInfo)
    {
        $tagInfo=QueueServer::analyseTag($tag);
        $response = $taskInfo["response"];
        $batchId = $taskInfo["batch_id"];
        $matchRefresh = $tagInfo["ext_type"];
        $responseInfo = json_decode($response, true);
        if (empty($responseInfo)){
            return [];
        }
        if (isset($responseInfo['error'])) {
            throw new TaskRestException($responseInfo);
        }

        $subSteps = [];

        foreach ($responseInfo['en']['result']['heroes'] as $enkey => $enItem) {
            $subStep = [];
            //单抓一条的情况
            if(isset($responseInfo['params']['id']) && $responseInfo['params']['id']){
                if($responseInfo['en']['result']['heroes'][$enkey]['id'] == $responseInfo['params']['id']){
                    $zhItem = $responseInfo['zh']['result']['heroes'][$enkey];
                    $subStep = self::addItemAndGetSubInfo($enItem,$zhItem, $batchId, $matchRefresh);
                }
            }else{
                $zhItem = $responseInfo['zh']['result']['heroes'][$enkey];
                $subStep = self::addItemAndGetSubInfo($enItem,$zhItem, $batchId,$matchRefresh);
            }
            if ($subStep) {
                $subSteps[] = $subStep;
            }
        }
        return $subSteps;
    }

    public static function addItemAndGetSubInfo($enItem,$zhItem, $batchId,$matchRefresh="")
    {
        $identityId = $enItem['id'];
        $enItem['name_cn'] = $zhItem['localized_name'];
        $originInfo = Common::getOriginInfoByType(Consts::ORIGIN_VALVE);
        $diffInfo = Common::setHotLog($originInfo['id'], Consts::METADATA_TYPE_DOTA2_HERO, 3, $identityId, $enItem,$matchRefresh);
        if ($diffInfo['changed']) {
            // 这里有变化，触发增量
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_ORIGIN_INCREMENT,
                QueueServer::QUEUE_ORIGIN_VALVE,
                Consts::METADATA_TYPE_DOTA2_HERO,
                $diffInfo['change_type'],
                3);
            $taskInfo = [
                "tag" => $tag,
                "batch_id" => $batchId,
                "params" => $diffInfo,
            ];
            return $taskInfo;
        }
    }
}