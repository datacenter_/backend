<?php
/**
 *
 */

namespace app\modules\task\services\hot\bayes2;

use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\battle\BattleServiceBase;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\Common;
use app\modules\task\services\hot\bayes\BayesHotBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\rest\exceptions\BusinessException;

class BayesplusCommonMatch extends BayesplusHotBase implements HotMatchInterface
{
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $hotInfo = json_decode($taskInfo['params'], true);
        $hotId = $hotInfo['id'];
        $matchId=$hotInfo['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,10);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        $matchInfo=Match::find()->where(['id'=>$matchId])->one();
        // 获取curl数据
        $curlInfo=self::getCurlInfo($relIdentityId);
        if (empty($curlInfo['match'])){
            return false;
        }
        if ($curlInfo['match']['state_label'] == 'CONCLUDED') {
            $matchEndStatus = true;
        }else{
            $matchEndStatus = false;
        }
        // 转化，为了保证formatInfo的独立性，我们尽可能少在里面用id，多用（主队，客队，player_id,这样的id，尽量不要用teamid）
        $formatInfo=self::convert($curlInfo,$matchInfo,$refreshType);

        $extInfo=[
            'time_begin'=>'',
            'time_end'=>'',
            'task_id'=>$taskInfo['id'],
            'match_id'=>$matchId,
        ];
        // 添加绑定信息，转义,主要出现在battle的详情中
        self::addHotLog($hotId, $curlInfo,$formatInfo,$extInfo);

        // 换成自己的绑定ID

        // 刷进数据表
//        self::refresh($matchId,$formatInfo);
        self::refresh($matchId,$formatInfo,$refreshType);
        // 更新hotdata状态
        self::refreshHotDataStatusByMatchInfo($hotId,$matchEndStatus);
    }

    private static function convert($curlInfo,$matchInfo,$refreshType)
    {
        $match = self::getMatchData($curlInfo['match'],$matchInfo);
        return [
            'match'=>$match,
        ];
    }

    private static function refresh($matchId, $info, $refreshType)
    {
        self::setMatch($matchId, $info['match']);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function getCurlInfo($matchId)
    {
        $match = self::getMatch($matchId);
        return [
            'match' => $match,
        ];
    }
    /**
     * 处理match数据结构
     * @param $matchData
     * @param $matchInfo
     * @return mixed
     * @throws BusinessException
     */
    private static function getMatchData($matchData, $matchInfo)
    {
        $match['begin_at']         = isset($matchData['date_start']) && $matchData['date_start'] ? date('Y-m-d H:i:s', strtotime($matchData['date_start'])) : "";
        $match['end_at']           = isset($matchData['date_end']) && $matchData['date_end'] ? date('Y-m-d H:i:s', strtotime($matchData['date_end'])) : "";
        $match['is_battle_detailed'] = 2;
        foreach($matchData['results'] as $key=>$val){
            if($val['winner'] == true){
                $match['winner'] = $val['team_id'] ? self::getTeamOrderByTeamRelId($val['team_id'], $matchInfo) : null;
            }
        }
        $match['status']     = Common::getBayesMatchStatus($matchData['state_label']);
        foreach ($matchData['results'] as $key => $val) {
            $teamOrder = $val['team_id'] ? self::getTeamOrderByTeamRelId($val['team_id'], $matchInfo) : null;
            if ($teamOrder == 2) {
                $match['team_2_score'] = $val['score'];
            } else {
                $match['team_1_score'] = $val['score'];
            }
        }
        return $match;
    }


    private static function getMatch($matchId)
    {
        $url = '/match/'.$matchId;



        $jsonInfo = self::getRestInfo($url, []);
        $info = json_decode($jsonInfo, true);
        // 获取当前Match的数据
        return $info;
    }
}