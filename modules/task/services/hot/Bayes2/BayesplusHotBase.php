<?php
/**
 *
 */

namespace app\modules\task\services\hot\bayes2;
use app\modules\task\services\grab\bayes2\BayesBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class BayesplusHotBase extends HotBase
{
    public static function getRestInfo($action,$params)
    {
        return BayesBase::getCurlByParams($action,$params);
    }

    public static function getTeamOrderByTeamRelId($teamRelId,$matchInfo,$originId = 10)
    {
//        $masterId=self::getMainIdByRelIdentityId('team',$teamRelId,$originId,1);
        $masterId=self::getMainIdByRelIdentityId('team',$teamRelId,$originId);
        if($matchInfo['team_1_id']==$masterId){
            return 1;
        }
        if($matchInfo['team_2_id']==$masterId){
            return 2;
        }
        throw new BusinessException([],'没有找到这个比赛的信息，请检查战队绑定状态:'.$teamRelId);
    }
}