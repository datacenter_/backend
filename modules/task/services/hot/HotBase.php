<?php


namespace app\modules\task\services\hot;


use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\StandardDataMetadata;
use app\modules\match\models\Battle;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleEventDota2;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchLivedDatasTableRelation;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\metadata\models\MetadataLolAbility;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolRune;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\Player;
use app\modules\task\models\BayesIdentifiers;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\StandardDataService;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
use app\rest\exceptions\BusinessException;
use Redis;
use yii\base\Exception;

class HotBase
{
    const PREFIX_WS = 'ws';
    const HLTV_PREFIX_WS = 'hltv_ws';
    const PREFIX_PAPI = 'Papi';
    const PREFIX_PWS = 'Pws';
    const Symbol = ':';
    const KEY_TAG_CURRENT = 'current';
    const KEY_TAG_HISTORY = 'history';
    const KEY_TAG_TABLE = 'table';

    const KEY_TAG_PLAYER = 'player';
    const KEY_TAG_RECORD_PLAYER = 'record_player';
    const KEY_TAG_TEAM = 'team';
    const KEY_TAG_MAP = 'map';
    const KEY_TAG_WINNER = 'winner';

    // 选手相关
    const KEY_TAG_SIDE = 'side';
//    const KEY_TAG_LIVE = 'live';
    const KEY_TAG_BLOOD = 'blood';
    const KEY_TAG_WEAPONS = 'weapons';
    const KEY_TAG_MONEY = 'money';
    const KEY_TAG_STEAM_ID = 'steam_id';
    const KEY_TAG_HAS_BOMB = 'has_bomb';//是否拥有c4
    const KEY_TAG_PING = 'ping';//PING
    const KEY_TAG_BATTLE_UP_COMING = 'battle_up_coming';//battle_up_coming

    // 战队的初始阵营，当前阵营
    const KEY_TAG_START_SIDE = 'start_side';
    const KEY_TAG_NOW_SIDE = 'now_side';

    const KEY_TAG_ROUND_WIN_SIDE = 'round_win_side';//获胜战队阵营
    const KEY_TAG_ROUND_CT_SCORE = 'round_ct_score';//round结束ct分值
    const KEY_TAG_ROUND_T_SCORE = 'round_t_score';//round结束t分值

    const KEY_TAG_ROUND_START_CT_SCORE = 'round_start_ct_score';//round结束start_ct分值
    const KEY_TAG_ROUND_START_T_SCORE = 'round_start_t_score';//round结束start_t分值


    const KEY_TAG_ROUND_PLAYER = 'round_player';//round_player


    // t和ct的阵营信息,类似team_name
    const KEY_TAG_TEAM_T = 'team_t';
    const KEY_TAG_TEAM_CT = 'team_ct';
    // 战队得分，
    const KEY_TAG_TEAM_SCORE = 'team_score';
    // 战队名
    const KEY_TAG_TEAM_NAME = 'team_name';

    // current
    const KEY_TAG_MATCH = 'match';
    const KEY_TAG_SYNCHRON = 'synchron';
    const KEY_TAG_NO_QUEUE = 'no_queue';
    const KEY_TAG_NO_RELEASE = 'no_release';
    const KEY_TAG_LOCKED = 'locked';
    const KEY_TAG_CHANGE_BATTLE = 'change_battle';
    const KEY_TAG_CUSTOM_BATTLE_END = 'custom_battle_end';
    const KEY_TAG_CUSTOM_BATTLE_START = 'custom_battle_start';
    const KEY_TAG_SUICIDE = 'suicide';
    const KEY_TAG_HLTV_LIVE = 'hltv_live';
    const KEY_TAG_HLTV_LIVE_CHANGE_COUNT = 'hltv_live_change_count';
    const KEY_TAG_REFRESH_TASK_STR = 'refresh_task_str';
    const KEY_TAG_SAME_REFRESH_TASK_STR = 'same_refresh_task_str';
    const KEY_TAG_SERVER_CONFIG = 'server_config';
    const KEY_TAG_RELOAD = 'reload';
    const KEY_TAG_BATTLE = 'battle';
    const KEY_TAG_BATTLE_DURATION = 'battle_duration';
    const KEY_TAG_BATTLE_TEMP = 'battle_temp';
    const KEY_TAG_PLAYER_KILL_TEMP = 'player_kill_temp';
    const KEY_TAG_WARM_UP_BATTLE_ID = 'warm_up_battle_id';
    const KEY_TAG_WARM_UP_BATTLE_ORDER = 'warm_up_battle_order';
    const KEY_TAG_ORIGINAL_BATTLE_ORDER = 'original_battle_order';
    const KEY_TAG_REAL_ORDER = 'real_order';
    const KEY_TAG_NUMBER = 'number';
    const KEY_TAG_ROUND = 'round';
    const KEY_TAG_WIN_TEAM = 'win_team';
    const KEY_TAG_WIN_BATTLE = 'win_battle';
    const KEY_TAG_LOG = 'log';
    const KEY_TAG_LOG_STARTED= 'log_started';
    const KEY_TAG_LOG_CLOSED= 'log_closed';
    const KEY_TAG_CONSOLE= 'console';

    // round,battle,match 的相关状态
    const KEY_TAG_STATUS = 'status';
    const KEY_TAG_BEGIN_AT = 'begin_at';
    const KEY_TAG_END_AT = 'end_at';
    const KEY_TAG_HAS_EXPIRE = 'has_expire';
    const KEY_TAG_END = 'end';
    const KEY_TAG_ORDER = 'order';
    const KEY_TAG_LITTLE_ORDER = 'little_order';
    const KEY_TAG_RELOAD_ORDER = 'reload_order';
    const KEY_TAG_OLD_ORDER = 'old_order';
    const KEY_TAG_SAVE_ORDER = 'save_order';
    const KEY_TAG_IS_LIVE = 'is_live';
    const KEY_TAG_IS_DRAW = 'is_draw';
    const KEY_TAG_KILLS = 'kills';//击杀数量
    const KEY_TAG_TIME_SINCE_PLANT = 'time_since_plant';//安放炸弹后时间
    const KEY_TAG_IS_BOMB_PLANTED = 'is_bomb_planted';//炸弹是否已安放
    const KEY_TAG_LIST = 'list';//列表
    const KEY_TAG_lITTLE_LIST = 'little_list';//小回档列表
    const KEY_TAG_INFO = 'info';//信息
    const KEY_TAG_NAME = 'name';//名称
    const KEY_TAG_DETAILS = 'details';//详情
    const KEY_TAG_DURATION = 'duration';//对局时长
    const KEY_TAG_GAME_VERSION = 'game_version';//对局时长
    const KEY_TAG_SERVER_CVARS_INIT = 'server_cvars_init';//对局时长

    const KEY_TAG_IS_FREEZE_TIME = 'is_freeze_time';//是否是正式比赛数据
    const KEY_TAG_BATTLE_1_SCORE = 'battle_1_score';//主队分
    const KEY_TAG_BATTLE_2_SCORE = 'battle_2_score';//客队分
    const KEY_TAG_ROUNDS = 'rounds';//rounds
    const KEY_TAG_EVENTS = 'events';//events
    const KEY_TAG_MAIN_EVENTS = 'main_events';//main_events
    const KEY_TAG_REMOVE_EVENTS = 'remove_events';//main_events
    const KEY_TAG_KILL_EVENTS = 'kill_events';//events
    const KEY_TAG_GRENADES = 'grenades';//投掷物
    const KEY_TAG_FRAMES = 'frames';//frame
    const KEY_TAG_START_TEAM_CT = 'start_team_ct';//start_team_ct
    const KEY_TAG_START_TEAM_T = 'start_team_t';//start_team_t
    const KEY_TAG_IS_PAUSE = 'is_pause';//is_pause

    //历史记录列表的事件
    const KEY_TAG_PLAYER_SUICIDE = 'player_suicide';//自杀事件列表
    const KEY_TAG_GET_C4 = 'get_c4';//获得c4列表
    const KEY_TAG_LOST_C4 = 'lost_c4';//失去c4列表
    const KEY_TAG_GET_GOODS = 'get_goods';//获得物品列表
    const KEY_TAG_PURCHASE_GOODS = 'purchase_goods';//购买物品列表
    const KEY_TAG_LOST_GOODS = 'lost_goods';//失去物品列表
    const KEY_TAG_THROW_GOODS = 'throw_goods';//投掷物品列表
    const KEY_TAG_MONEY_CHANGE = 'money_change';//金钱变化列表
    const KEY_TAG_PLAYER_QUIT = 'player_quit';//玩家退出列表
    const KEY_TAG_PLAYER_Kill = 'player_kill';//玩家击杀列表
    const KEY_TAG_ASSIST = 'assist';//玩家助攻列表
    const KEY_TAG_ATTACKED = 'attacked';//玩家攻击列表
    const KEY_TAG_FLASHASSIST = 'flashassist';//玩家闪光弹助攻列表
    const KEY_TAG_FLASH_BLINDNESS = 'flash_blindness';//玩家闪光弹助攻列表
    const KEY_TAG_C4_KILL = 'KEY_TAG_C4_KILL';//玩家c4击杀列表
    const KEY_TAG_BINDING_CURRENT_IDENTIFICATION = 'binding_current_identification';//绑定当前对局标识列表
    const KEY_TAG_BATTLE_END = 'battle_end';//对局结束列表
    const KEY_TAG_ROUND_MONEY = 'round_money';//对局选手金钱列表
    const KEY_TAG_ROUND_WEAPONS = 'round_weapons';//对局选手武器列表
    const KEY_TAG_ROUND_WIN_TYPE = 'round_win_type';//回合结束方式列表
    const KEY_TAG_BOMB_PLANTED = 'bomb_planted';//安装炸弹列表

    //status详情
    const STATUS_COMING = 1; //即将开始
    const STATUS_GOING = 2; //进行中(暂停结束也会变成进行中)
    const STATUS_END = 3; //结束
    const STATUS_PAUSE = 4; //暂停

    //选手状态,属性
    const PLAYER_STATUS_ONLINE= 'online';//在线
    const PLAYER_STATUS_OFFLINE= 'offline';//离线
    const PLAYER_BLOOD= '100';//血量HP

    //回合详情
    const KEY_TAG_ROUND_ID= 'round_id';//回合序号
    const KEY_TAG_ROUND_ORDINAL= 'round_ordinal';//回合序号
    const KEY_TAG_ROUND_END_ORDER= 'round_end_order';//回合结束数量(如果回合序号>(回合结束数量+1),则以回合结束的数量为准)
    //battle详情
    const KEY_TAG_BATTLE_END_ORDER= 'battle_end_order';//回合结束数量(如果回合序号>(回合结束数量+1),则以回合结束的数量为准)
    const KEY_TAG_BATTLE_ID = 'battle_id';//battle_id
    const KEY_TAG_BAYES_BATTLE_ID = 'bayes_battle_id';//battle_id

    //回合结束方式的值
    const ROUND_END_TYPE_BOMB_DEFUSED ="bomb_defused";
    const ROUND_END_TYPE_CTS_WIN ="cts_win";
    const ROUND_END_TYPE_TARGET_SAVED ="target_saved";
    const ROUND_END_TYPE_TARGET_BOMBED ="target_bombed";
    const ROUND_END_TYPE_TERRORISTS_WIN ="terrorists_win";
    const ROUND_END_TYPE_ROUND_DRAW="round_draw";
    const ROUND_END_TYPE_LOST ="lost";

    const CSGO_EVENT_INFO_TYPE_NO = 'no';
    const CSGO_EVENT_INFO_TYPE_EVENTS = 'events';

    // 扩展信息
    const KEY_TAG_EXTEND_INFO = 'extend_info';// 大数组
    //redis 扩展信息
    const REDIS_EXPIRE_TIME = 432000;//5天
    const REDIS_SELECT_DB = 1;//db1

    //matchInfo和battle INFO
    const MATCH_ALL_INFO = 'match_all_info';//比赛所有信息
    const MATCH_BASE_INFO = 'match_base_info';//比赛base信息

    public $redis ;
    public $currentPre='' ;
    public $matchId ="";
    public function __construct()
    {

        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
//        $redis->connect(env('REDIS_HOST'),env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        $this->redis = $redis;
    }

    /**
     * 日志日期转北京日期
     * @param $dateTime
     * @return false|string|void
     */
    public function getDateTimeFormat($dateTime){
        if($dateTime) {
            list($usec, $sec) = explode(".", $dateTime);
//            $logDateStr = substr($usec, 0, 21);
            $logDate = str_replace(' - ', ' ', $usec);
            $logTime = strtotime($logDate);
            return date('Y-m-d H:i:s', $logTime).'.'.$sec;
        }else{
            return;
        }
    }

    /**
     * ws key 加前缀
     * @param $key
     * @return string
     */
    public function wsKey($key){
        if ($this->currentPre){
            return $this->currentPre.self::Symbol.$key;
        }
        return self::PREFIX_WS.self::Symbol.$key;
    }

    /**
     * ws key 加前缀
     * @param $key
     * @return string
     */
    public function hltvWsKey($key){
        return self::HLTV_PREFIX_WS.self::Symbol.$key;
    }

    /**
     * @param $key
     * @param $value
     * @param null $timeout
     * @return mixed
     */
    public function setKey($key, $value, $timeout = null)
    {
        $newKey = $this->wsKey($key);
        return $this->redis->set($newKey, $value, $timeout);
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setnx($key,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->setnx($newKey,$value);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getKey($key){
        $newKey = $this->wsKey($key);
        return $this->redis->get($newKey);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getHltvKey($key){
        $newKey = $this->hltvWsKey($key);
        return $this->redis->get($newKey);
    }

    /**
     * incr
     * @param $key
     * @return string
     */
    public function incr($key){
        $newKey = $this->wsKey($key);
        return $this->redis->incr($newKey);
    }

    /**
     * hSet
     * @param $key
     * @return string
     */
    public function hSet($key,$field,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->hSet($newKey,$field,$value);
    }


    /**
     * hSet
     * @param $key
     * @return array
     */
    public function sMembers($key){
        $newKey = $this->wsKey($key);
        return $this->redis->sMembers($newKey);
    }

    /**
     * hSetNx
     * @param $key
     * @return string
     */
    public function hSetNx($key,$field,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->hSetNx($newKey,$field,$value);
    }

    /**
     * hSetNx
     * @param $key
     * @return string
     */
    public function hIncrBy($key,$field,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->hIncrBy($newKey,$field,$value);
    }

    /**
     * hGet
     * @param $key
     * @return string
     */
    public function hGet($key,$field){
        $newKey = $this->wsKey($key);
        return $this->redis->hGet($newKey,$field);
    }
    /**
     * hGet
     * @param $key
     * @return string
     */
    public function hDel($key,$field){
        $newKey = $this->wsKey($key);
        return $this->redis->hDel($newKey,$field);
    }
    public function del($key){
        $newKey = $this->wsKey($key);
        return $this->redis->del($newKey);
    }


    /**
     * hMset  为hash表多个字段设定值
     * @param $key
     * @return string
     */
    public function hMset($key,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->hMset($newKey,$value);
    }

    /**
     * hGetAll
     * @param $key
     * @return array
     */
    public function hGetAll($key){
        $newKey = $this->wsKey($key);
        return $this->redis->hGetAll($newKey);
    }

    /**
     * redis lPush
     * @param $key
     * @param $value
     * @return string
     */
    public function lPush($key,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->lPush($newKey,$value);
    }

    /**
     * redis lPush
     * @param $key
     * @param $offset
     * @param $length
     * @return array
     */
    public function lRange($key,$offset,$length){
        $newKey = $this->wsKey($key);
        return $this->redis->lRange($newKey,$offset,$length);
    }

    /**
     * redis sAdd
     * @param
     * @return string
     */
    public function sAdd($key,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->sAdd($newKey,$value);
    }





    public function keyString($key)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return $this->wsKey($keyString);
    }

    public function keyStringByOriginAndMatchId($origin, $matchId, $key)
    {
        if (!$matchId || !$origin || empty($key)) {
            return false;
        }
        $keyString    = implode(":", $key);
        $keyNewString = sprintf("%s:%s:%s", $origin, $matchId, $keyString);
        return $keyNewString;
    }

    /**
     * redis sRem
     * @param
     * @return string
     */
    public function sRem($key,$value){
        $newKey = $this->wsKey($key);
        return $this->redis->sRem($newKey,$value);
    }


    public static function setStatus($hotId, $status)
    {
        $hotInfo = self::getHotInfo($hotId);
        $hotInfo->setAttribute('deal_status', $status);
        if (!$hotInfo->save()) {
            throw new BusinessException($hotInfo->getErrors(), '修改状态失败');
        }
    }

    public static function refreshHotDataStatusByMatchInfo($hotId,$refreshRes=false)
    {
        $hotInfo=HotDataRunningMatch::find()->where(['id'=>$hotId])->one();
        if(!$hotInfo){
            throw new BusinessException([],'不存在对应的hotData');
        }
        // 从real_time_info中获取信息，更新到hot表
        $info=MatchRealTimeInfo::find()->where(['id'=>$hotInfo['match_id']])->one();
        $statusInfo=[
            'match_status'=>$info['status'],
            'match_start_time'=>$info['begin_at'],
            'match_end_time'=>$info['end_at'],
        ];
        if ($info['status'] == 3 && $refreshRes){
            $statusInfo['deal_status'] = 3;
        }else{
            unset($statusInfo['match_status']);
        }
        $hotInfo->setAttributes($statusInfo);
        $hotInfo->save();
        //刷新高宇的API
//        if ($info['status'] == 3 && $refreshRes){
        RenovateService::refurbishApi('match',$hotInfo['match_id']);
        RenovateService::refurbishApi('battle',$hotInfo['match_id']);
//        }
//        if(in_array($statusInfo['match_status'],[3,5])){ //已结束已取消，取消自动更新状态
//            // 更新自动更新状态
//            $info->setAttribute('auto_status',2);
//            $info->save();
//            $hotInfo->setAttribute('auto_status',2);
//            $hotInfo->save();
//        }
    }

    public static function addHotLog($hotId, $infoRest , $infoFormat, $extInfo)
    {
        $infoRestString = json_encode($infoRest);
        $infoFormatString = json_encode($infoFormat);
        $l = new HotDataLog();
        $l->setAttributes(
            [
                'hot_id' => $hotId,
                'info_rest' => $infoRestString,
                'info_format' => $infoFormatString,
                'task_id' => isset($extInfo['task_id'])?$extInfo['task_id']:'',
                'time_begin' =>isset($extInfo['time_begin'])?$extInfo['time_begin']:'',
                'time_end' =>isset($extInfo['time_end'])?$extInfo['time_end']:'',
                'match_id' => isset($extInfo['match_id'])?$extInfo['match_id']:'',
            ]
        );
        if (!$l->save()) {
            throw new BusinessException($l->getErrors(), '保存失败');
        }
        return $l;
    }

    public static function getHotInfo($hotId)
    {
        return HotDataRunningMatch::find()->where(['id' => $hotId])->one();
    }

    /**
     * @param $resourceType
     * @param $originId
     * @param $gameId
     * @return null
     * @throws \yii\base\Exception
     */
    public static function getAllMainDataArray($resourceType,$originId,$gameId){
        $mainDataRes = self::getAllMainDataByOrginIdAndRelIdentityId($resourceType,$originId,$gameId);
        if ($mainDataRes){
            return array_column($mainDataRes, 'master_id', 'id');
        }else{
            return null;
        }
    }
    /**
     * @param $resourceType
     * @param $originId
     * @param $gameId
     * @return null
     * @throws \yii\base\Exception
     */
    public static function getMainDataArrayMasterIdAndRelIdentityId($resourceType,$originId,$gameId){
        $mainDataRes = self::getMainDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId);
        if ($mainDataRes){
            return array_column($mainDataRes, 'master_id', 'rel_identity_id');
        }else{
            return null;
        }
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getAllMainDataByOrginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        $sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getMainDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        $sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }


    public static function getBuildingDefaultStatusOnFrame($faction){

        if ($faction == 'radiant'){
            $top_outpost['is_captured']               = false;
            $top_outpost['health']                    = 450;
            $top_outpost['health_max']                = 450;
            $top_outpost['armor']                     = 0;
            $bot_outpost['is_captured']               = true;
            $bot_outpost['health']                    = 450;
            $bot_outpost['health_max']                = 450;
            $bot_outpost['armor']                     = 0;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        if ($faction == 'dire'){
            $top_outpost['is_captured']               = true;
            $top_outpost['health']                    = 450;
            $top_outpost['health_max']                = 450;
            $top_outpost['armor']                     = 0;
            $bot_outpost['is_captured']               = false;
            $bot_outpost['health']                    = 450;
            $bot_outpost['health_max']                = 450;
            $bot_outpost['armor']                     = 0;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        $top_tier_1_tower['is_alive']             = true;
        $top_tier_1_tower['health']               = 1800;
        $top_tier_1_tower['health_max']           = 1800;
        $top_tier_1_tower['armor']                = 12;
        $top_tier_2_tower['is_alive']             = true;
        $top_tier_2_tower['health']               = 2500;
        $top_tier_2_tower['health_max']           = 2500;
        $top_tier_2_tower['armor']                = 16;
        $top_tier_4_tower['is_alive']             = true;
        $top_tier_4_tower['health']               = 2600;
        $top_tier_4_tower['health_max']           = 2600;
        $top_tier_4_tower['armor']                = 21;

        $ranged_barrack['is_alive']             = true;
        $ranged_barrack['health']               = 1300;
        $ranged_barrack['health_max']           = 1300;
        $ranged_barrack['armor']                = 9;

        $melee_barrack['is_alive']             = true;
        $melee_barrack['health']               = 2200;
        $melee_barrack['health_max']           = 2200;
        $melee_barrack['armor']                = 15;

        $ancient['is_alive']             = true;
        $ancient['health']               = 4500;
        $ancient['health_max']           = 4500;
        $ancient['armor']                = 13;
        $returnData['towers']['top_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_4_tower'] = $top_tier_4_tower;
        $returnData['towers']['bot_tier_4_tower'] = $top_tier_4_tower;
        $returnData['barracks']['top_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['top_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['mid_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['mid_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['bot_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['bot_melee_barrack'] = $melee_barrack;
        $returnData['ancient'] = $ancient;
        return $returnData;

    }

    public static function getBuildingDefaultStatusOnFrameByPandascore($faction){

        if ($faction == 'radiant'){
            $top_outpost['is_captured']               = null;
            $top_outpost['health']                    = null;
            $top_outpost['health_max']                = null;
            $top_outpost['armor']                     = null;
            $bot_outpost['is_captured']               = null;
            $bot_outpost['health']                    = null;
            $bot_outpost['health_max']                = null;
            $bot_outpost['armor']                     = null;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        if ($faction == 'dire'){
            $top_outpost['is_captured']               = null;
            $top_outpost['health']                    = null;
            $top_outpost['health_max']                = null;
            $top_outpost['armor']                     = null;
            $bot_outpost['is_captured']               = null;
            $bot_outpost['health']                    = null;
            $bot_outpost['health_max']                = null;
            $bot_outpost['armor']                     = null;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        $top_tier_1_tower['is_alive']             = true;
        $top_tier_1_tower['health']               = null;
        $top_tier_1_tower['health_max']           = null;
        $top_tier_1_tower['armor']                = null;
        $top_tier_2_tower['is_alive']             = true;
        $top_tier_2_tower['health']               = null;
        $top_tier_2_tower['health_max']           = null;
        $top_tier_2_tower['armor']                = null;
        $top_tier_4_tower['is_alive']             = true;
        $top_tier_4_tower['health']               = null;
        $top_tier_4_tower['health_max']           = null;
        $top_tier_4_tower['armor']                = null;

        $ranged_barrack['is_alive']             = true;
        $ranged_barrack['health']               = null;
        $ranged_barrack['health_max']           = null;
        $ranged_barrack['armor']                = null;

        $melee_barrack['is_alive']             = true;
        $melee_barrack['health']               = null;
        $melee_barrack['health_max']           = null;
        $melee_barrack['armor']                = null;

        $ancient['is_alive']             = true;
        $ancient['health']               = null;
        $ancient['health_max']           = null;
        $ancient['armor']                = null;
        $returnData['towers']['top_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_4_tower'] = $top_tier_4_tower;
        $returnData['towers']['bot_tier_4_tower'] = $top_tier_4_tower;
        $returnData['barracks']['top_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['top_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['mid_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['mid_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['bot_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['bot_melee_barrack'] = $melee_barrack;
        $returnData['ancient'] = $ancient;
        return $returnData;

    }

    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @param $type (standard_data_tournament)必须
     * @return mixed
     * @throws \yii\base\Exception
     * 根据identityId和originId获取对应的主表id，有可能是空
     */
    public static function getMainIdByRelIdentityId($resourceType,$relIdentityId,$originId,$gameId = '')
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        if($gameId){
            $where[] = ['=','std.game_id',$gameId];
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
        //$sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info['master_id'];
        }
    }

    public static function zoneTimeToRealTime($startTime){
        $intTime = strtotime($startTime);
        return date("Y-m-d H:i:s",$intTime);
    }

    /**
     * 根据PerId返回MatchId
     * @param $perId
     * @return bool|mixed
     */
    public static function getMatchIdByPerId($perId,$originId)
    {
        $where         = ['perid' => $perId];
        $standardMatch = StandardDataMatch::find()->where($where)->one();
        if (!empty($standardMatch)) {
            $rel_identity_id = $standardMatch['rel_identity_id'];
            try {
                $matchId = self::getMainIdByRelIdentityId('match', $rel_identity_id, $originId);
                return [
                    'rel_identity_id' => $rel_identity_id,
                    'matchId' => $matchId
                ];
            } catch (Exception $e) {
                $errs = [
                    "msg"  => $e->getMessage(),
                    "file" => $e->getFile(),
                    "line" => $e->getLine(),
                ];
                //todo 如果获取matchId过程中报错
            }
        } else {
            return false;
        }
    }

    /**
     * 根据PerId返回MatchId
     * @param $perId
     * @return bool|mixed
     */
    public static function getRelMatchIdByPerId($perId,$originId)
    {
        $where         = ['perid' => $perId];
        $standardMatch = StandardDataMatch::find()->where($where)->one();
        if (!empty($standardMatch)) {
            $rel_identity_id = $standardMatch['rel_identity_id'];
            return $rel_identity_id;
        } else {
            return false;
        }
    }

    //bayes 根据Urn 获取平台ID
    public static function getMainIdOfBayesByUrn($resourceType,$originId,$urn_id,$gameId){
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where = [
            'and',
            ['=','std.origin_id',$originId],
            ['=','bi.resource_type',$resourceType],
            ['=','bi.value',$urn_id]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;

        }
        if($gameId){
            $where[] = ['=','std.game_id',$gameId];
            $where[] =
                ['=','std.deleted',2]
            ;
            $where[] =
                ['>','rel.master_id',0]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['rel.master_id'])
            ->leftJoin('bayes_identifiers as bi','bi.rel_identity_id = std.rel_identity_id')
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where)->orderBy('bi.rel_identity_id desc');
        $info=$q->asArray()->one();

        $master_id = null;
        if(@$info['master_id']){
            $master_id = $info['master_id'];
        }
        return $master_id;
    }
    //bayes 根据Urn 获取平台ID
    public static function getAllMainIdsOfBayesByUrn($resourceType,$originId,$urn_id,$gameId){
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where = [
            'and',
            ['=','std.origin_id',$originId],
            ['=','bi.resource_type',$resourceType],
            ['=','bi.value',$urn_id]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] = ['=','std.metadata_type',$resourceType];
        }
        if($gameId){
            $where[] = ['=','std.game_id',$gameId];
            $where[] = ['=','std.deleted',2];
            $where[] = ['>','rel.master_id',0];
        }
        $q=$standardClass::find()->alias('std')
            ->select(['rel.master_id'])
            ->leftJoin('bayes_identifiers as bi','bi.rel_identity_id = std.rel_identity_id')
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where)->orderBy('bi.rel_identity_id desc');
        $sql = $q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        //结果
        $masterIds = null;
        if (!empty($info)){
            foreach ($info as $val){
                $masterIds[] = $val['master_id'];
            }
        }
        return $masterIds;
    }
    public static function socketMatchingTeam($team_1,$team_2,$per_id1,$per_id2,$originId,$gameId){
        $team_list = [$team_1,$team_2];
        //查找对应的 master 队伍信息
        $masterIds1 = self::getAllMainIdsOfBayesByUrn('team', $originId, $per_id1, $gameId);
        $masterIds2 = self::getAllMainIdsOfBayesByUrn('team', $originId, $per_id2, $gameId);
        $masterInfoRes = [];
        $master1_id = $master2_id = null;
        $master1_ids = $master2_ids = [];
        if(!empty($masterIds1) && !empty($masterIds2)){
            //循环匹配
            foreach ($masterIds1 as $item){
                if (in_array($item,$team_list)){
                    $master1_ids[$item] = $item;
                }
            }
            $master1_ids = array_values($master1_ids);
            foreach ($masterIds2 as $item){
                if (in_array($item,$team_list)){
                    $master2_ids[$item] = $item;
                }
            }
            $master2_ids = array_values($master2_ids);
            $master1_ids_num = $master2_ids_num = null;
            //判断是不是都匹配到了结果
            //1、都匹配到了
            if (!empty($master1_ids) && !empty($master2_ids)){
                //1.判断两个结果是不是 一个
                $master1_ids_num = count($master1_ids);
                $master2_ids_num = count($master2_ids);
                if ($master1_ids_num == 1 && $master2_ids_num == 1){
                    $master1_id = $master1_ids[0];
                    $master2_id = $master2_ids[0];
                    //看看相不相等 不相等就是正确匹配
                    if ($master1_id != $master2_id){
                        $masterInfoRes[$per_id1] = (Int)$master1_id;
                        $masterInfoRes[$per_id2] = (Int)$master2_id;
                    }else{//相等就要 给一个匹配另外一个
                        if ($master1_id == $team_1){
                            $master2_id = $team_2;
                        }
                        if ($master1_id == $team_2){
                            $master1_id = $team_2;
                            $master2_id = $team_1;
                        }
                        $masterInfoRes[$per_id1] = (Int)$master1_id;
                        $masterInfoRes[$per_id2] = (Int)$master2_id;
                    }
                }elseif ($master1_ids_num == 2 && $master2_ids_num == 1){
                    //看等于1的那队伍是 队伍几  等于2的就取反
                    $master2_id = $master2_ids[0];
                    if ($master2_id == $team_1){
                        $master1_id = $team_2;
                        $master2_id = $team_1;
                    }
                    if ($master2_id == $team_2){
                        $master1_id = $team_1;
                        $master2_id = $team_2;
                    }
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }elseif ($master1_ids_num == 1 && $master2_ids_num == 2){
                    //看等于1的那队伍是 队伍几  等于2的就取反
                    $master1_id = $master1_ids[0];
                    if ($master1_id == $team_1){
                        $master1_id = $team_1;
                        $master2_id = $team_2;
                    }
                    if ($master1_id == $team_2){
                        $master1_id = $team_2;
                        $master2_id = $team_1;
                    }
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }else{
                    $master1_id = $team_1;
                    $master2_id = $team_2;
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }
            }elseif (!empty($master1_ids) && empty($master2_ids)){//1、1匹配到了 2没有匹配到
                $master1_ids_num = count($master1_ids);
                if ($master1_ids_num == 1){
                    $master1_id = $master1_ids[0];
                    //判断这个是哪个队伍
                    if ($master1_id == $team_1){
                        $master1_id = $team_1;
                        $master2_id = $team_2;
                    }
                    if ($master1_id == $team_2){
                        $master1_id = $team_2;
                        $master2_id = $team_1;
                    }
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }else{
                    $master1_id = $team_1;
                    $master2_id = $team_2;
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }
            }elseif (empty($master1_ids) && !empty($master2_ids)){//1、1没有匹配到了 2匹配到
                $master2_ids_num = count($master2_ids);
                if ($master2_ids_num == 1){
                    $master2_id = $master2_ids[0];
                    //判断这个是哪个队伍
                    if ($master2_id == $team_1){
                        $master1_id = $team_2;
                        $master2_id = $team_1;
                    }
                    if ($master2_id == $team_2){
                        $master1_id = $team_1;
                        $master2_id = $team_2;
                    }
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }else{
                    $master1_id = $team_1;
                    $master2_id = $team_2;
                    $masterInfoRes[$per_id1] = (Int)$master1_id;
                    $masterInfoRes[$per_id2] = (Int)$master2_id;
                }
            }else{
                $masterInfoRes[$per_id1] = null;
                $masterInfoRes[$per_id2] = null;
            }
        }elseif (empty($masterIds1) && !empty($masterIds2)) {//队伍1 直接为空  匹配队伍2
            //循环匹配
            foreach ($masterIds2 as $item){
                if (in_array($item,$team_list)){
                    $master2_ids[$item] = $item;
                }
            }
            $master2_ids = array_values($master2_ids);
            $master2_ids_num = count($master2_ids);
            if ($master2_ids_num == 1){//匹配到了一个
                $master2_id = $master2_ids[0];
                //判断这个是哪个队伍
                if ($master2_id == $team_1){
                    $master1_id = $team_2;
                    $master2_id = $team_1;
                }
                if ($master2_id == $team_2){
                    $master1_id = $team_1;
                    $master2_id = $team_2;
                }
                $masterInfoRes[$per_id1] = (Int)$master1_id;
                $masterInfoRes[$per_id2] = (Int)$master2_id;
            }elseif ( $master2_ids_num == 2 ){
                $master1_id = $team_1;
                $master2_id = $team_2;
                $masterInfoRes[$per_id1] = (Int)$master1_id;
                $masterInfoRes[$per_id2] = (Int)$master2_id;
            }else{
                $masterInfoRes[$per_id1] = null;
                $masterInfoRes[$per_id2] = null;
            }
        }elseif (!empty($masterIds1) && empty($masterIds2)) {// 匹配队伍1  队伍2  直接为空
            //循环匹配
            foreach ($masterIds1 as $item){
                if (in_array($item,$team_list)){
                    $master1_ids[$item] = $item;
                }
            }
            $master1_ids = array_values($master1_ids);
            $master1_ids_num = count($master1_ids);
            if ($master1_ids_num == 1){//匹配到了一个
                $master1_id = $master1_ids[0];
                //判断这个是哪个队伍
                if ($master1_id == $team_1){
                    $master1_id = $team_1;
                    $master2_id = $team_2;
                }
                if ($master1_id == $team_2){
                    $master1_id = $team_2;
                    $master2_id = $team_1;
                }
                $masterInfoRes[$per_id1] = (Int)$master1_id;
                $masterInfoRes[$per_id2] = (Int)$master2_id;
            }elseif ( $master1_ids_num == 2 ){
                $master1_id = $team_1;
                $master2_id = $team_2;
                $masterInfoRes[$per_id1] = (Int)$master1_id;
                $masterInfoRes[$per_id2] = (Int)$master2_id;
            }else{
                $masterInfoRes[$per_id1] = null;
                $masterInfoRes[$per_id2] = null;
            }
        }else{
            $masterInfoRes[$per_id1] = null;
            $masterInfoRes[$per_id2] = null;
        }
        return $masterInfoRes;
    }
        //bayes 根据Urn 获取平台ID
    public static function getMainIdOfBayesByUrnOnDota($resourceType,$originId,$urn_id,$gameId,$matchInfo=null){
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where = [
            'and',
            ['=','std.origin_id',$originId],
            ['=','bi.resource_type',$resourceType],
            ['=','bi.value',$urn_id]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;

        }
        if($gameId){
            $where[] = ['=','std.game_id',$gameId];
            $where[] =
                ['=','std.deleted',2]
            ;
            $where[] =
                ['>','rel.master_id',0]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['rel.master_id'])
            ->leftJoin('bayes_identifiers as bi','bi.rel_identity_id = std.rel_identity_id')
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where)->orderBy('bi.id desc');
        if ($matchInfo){
            $matchTeamArr = [];
            $matchTeamArr[] = $matchInfo['team_1_id'];
            $matchTeamArr[] = $matchInfo['team_2_id'];
            $info=$q->asArray()->all();
            foreach ($info as $k=>$v){
                if (in_array($v['master_id'],$matchTeamArr)){
                    return $v['master_id'];
                }
            }
            return null;
        }else{
            $info=$q->asArray()->one();
        }

        $master_id = null;
        if(@$info['master_id']){
            $master_id = $info['master_id'];
        }
        return $master_id;
    }
    //根据rel_match_ID 获取perId
    public static function getPerIdByRelMatchId($rel_match_id,$originId){
        $perid = null;
        $standardDataMatchInfo = StandardDataMatch::find()->select('perid')->where(['rel_identity_id' => $rel_match_id])->andWhere(['origin_id' => $originId])->asArray()->one();
        if ($standardDataMatchInfo){
            $perid = @$standardDataMatchInfo['perid'];
        }
        return $perid;
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @param $type (standard_data_tournament)必须
     * @return mixed
     * @throws \yii\base\Exception
     * 根据identityId和originId获取对应的主表id，有可能是空
     */
    public static function getMainIdByRelIdentityIdOrUnknown($resourceType,$relIdentityId,$originId,$gameId = '')
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        if($gameId){
            $where[] = ['=','std.game_id',$gameId];
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
        //$sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info['master_id']){
            return $info['master_id'];
        }else{
            return self::getMetaDataUnknownId($resourceType);
        }
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 推荐绑定关系的主表id
     */
    public static function getMainIdExpectByRelIdentityId($resourceType,$relIdentityId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',(int)$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation_expect as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info['master_id'];
        }
    }
    //获取选手信息
    public static function getPlayInfoById($id){
        $playerInfo = Player::find()->select(['nick_name'])->where(['id'=> $id])->asArray()->one();
        return $playerInfo;
    }
    //获取比赛信息
    public static function getMatchInfoByMatchId($matchId){
        $matchInfo = Match::find()->select(['team_1_id','team_2_id'])->where(['id'=> $matchId])->asArray()->one();
        return $matchInfo;
    }
    //获取比赛信息
    public static function getMatchAllInfoByMatchId($matchId){
        $matchInfo = Match::find()->where(['id'=> $matchId])->asArray()->one();
        return $matchInfo;
    }

    //获取team order
    public static function getTeamOrderByTeamId($teamId,$matchInfo)
    {
        if($matchInfo['team_1_id']==$teamId){
            return 1;
        }
        if($matchInfo['team_2_id']==$teamId){
            return 2;
        }
        throw new BusinessException([],'没有找到这个比赛的信息，请检查战队绑定状态:'.$teamId);
    }

    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return array|\yii\db\ActiveRecord
     * @throws \yii\base\Exception
     * 获取
     */
    public static function getMetadataByRelIdentityId($resourceType,$relIdentityId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',(int)$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id','std.external_id','std.external_name','std.name','std.name_cn','std.title','std.title_cn'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info;
        }
    }
    /**
     * @param $resourceType
     * @param $masterId
     * @param $originId
     * @return array|\yii\db\ActiveRecord
     * @throws \yii\base\Exception
     * 根据master_id 获取 rel_identity_id
     */
    public static function getMetadataByMasterId($resourceType,$masterId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','rel.master_id',$masterId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info;
        }
    }
    /**
     * @param $resourceType
     * @param $masterId
     * @param $originId
     * @return array|\yii\db\ActiveRecord
     * @throws \yii\base\Exception
     * 根据master_id 获取 rel_identity_id
     */
    public static function getRelIdByMasterId($resourceType,$masterId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','rel.master_id',$masterId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info['rel_identity_id'];
        }else{
            return null;
        }
    }
    /**
     * @param $resourceType
     * @param $externalName
     * @param $originId
     * @return mixed
     * @throws \yii\db\Exception
     * 根据官方名称获取主表id
     */
    public static function getExternalIdByExternalName($resourceType,$externalName,$originId)
    {
        $metadataClass = MetadataService::getObjByType($resourceType);

//        if($standardClass == StandardDataMetadata::class){
//            $where[] =[
//                ['=','std.resource_type',$resourceType]
//            ];
//        }
        $info=$metadataClass::find()->where(['external_name'=>$externalName])->one();
        if($info){
            return $info['id'];
        }
    }
    //处理
    public static function conversionPlayerSummonerSpells($summoner_spells){
//        $summoner_spells_default_data = [
//            'summoner_spell_id' => null,
//            'name' => null,
//            'name_cn' => null,
//            'external_id' => null,
//            'external_name' => null,
//            'image' => null,
//            'description' => null,
//            'description_cn' => null
//        ];

        $summoner_spells_data = [];
        foreach($summoner_spells as $key =>$value){
            $spellId = @self::getMainIdByRelIdentityIdOrUnknown('lol_summoner_spell',$value['id'],'3');
            $spell = MetadataLolSummonerSpell::find()->select('id,external_id,external_name,name,name_cn,slug,description,description_cn,image')->where(['id' => $spellId])->asArray()->one();
            if ($spell){
                $summoner_spells_data[$key]['summoner_spell_id'] = (Int)$spell['id'];
                $summoner_spells_data[$key]['name'] = @$spell['name'];
                $summoner_spells_data[$key]['name_cn'] = @$spell['name_cn'];
                $summoner_spells_data[$key]['external_id'] = @$spell['external_id'];
                $summoner_spells_data[$key]['external_name'] = @$spell['external_name'];
                $summoner_spells_data[$key]['slug'] = @$spell['slug'];
//                $summoner_spells_data[$key]['description'] = @$spell['description'];
//                $summoner_spells_data[$key]['description_cn'] = @$spell['description_cn'];
                $summoner_spells_data[$key]['image'] = @ImageConversionHelper::showFixedSizeConversion(@$spell['image'],50,50);
                $summoner_spells_data[$key]['cooldown'] = null;
            }else{
                $summoner_spells_data[$key] = 'Unknown';
            }
        }
        return $summoner_spells_data;
    }
    //处理 PandascoreLolApi
    public static function conversionPlayerSummonerSpellsIds($summoner_spells){
        $summoner_spells_data = [];
        foreach($summoner_spells as $key =>$value){
            $spellId = @self::getMainIdByRelIdentityIdOrUnknown('lol_summoner_spell',$value['id'],'3');
            $spell = MetadataLolSummonerSpell::find()->select('id,external_id,external_name,name,name_cn,description,description_cn,image')->where(['id' => $spellId])->asArray()->one();
            if ($spell){
                $spell_id = (Int)$spell['id'];
            }else{
                $spell_id = @self::getMetaDataUnknownId('lol_summoner_spell');
            }
            $spell_array = [
                'spell_id' => $spell_id,
                'cooldown' => null
            ];
            $summoner_spells_data[] = $spell_array;
        }
        return $summoner_spells_data;
    }
    /**
     * @param $playerItems
     * @throws \yii\base\Exception
     */
    public static function conversionPlayerItems($playerItems){
        $playerItemsDefaultInfo = [
            'item_id'=> null,
            'name'=> null,
            'name_cn'=> null,
            'external_id'=> null,
            'external_name'=> null,
            'total_cost'=> null,
            'is_trinket'=> null,
            'is_purchasable'=> null,
            'slug'=> null,
            'image'=> null,
            'purchase_time'=> null,
            'cooldown'=> null
        ];
        $originId = 3;
        //redis初始化
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redisNameRelation = "lol:items:relation:".$originId;
        $itemsList = "lol:items:list:".$originId;
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        $playerItemsKeys = [];
        foreach ($playerItems as $key => $value){
            $key_slot = $key + 1;
            $playerItemsKeys[] = 'slot_'.$key_slot;
            $keyItemName = $key;
            if ($value){
                $itemInfoId = $Redis->hget($redisNameRelation,$value['id']);
                if($itemInfoId){
                    $itemInfoId_val = (Int)$itemInfoId;
                    //获取当前
                    $itemInfoResRedis = $Redis->hget($itemsList,$itemInfoId_val);
                    if ($itemInfoResRedis){
                        $itemInfo = @json_decode($itemInfoResRedis,true);
                        if ($itemInfo['is_trinket'] == 1 && isset($itemInfo['is_trinket'])){//饰品
                            $playerOrnamentsInfo[$keyItemName]['item_id'] = (Int)@$itemInfo['id'];
                            $playerOrnamentsInfo[$keyItemName]['name'] = @$itemInfo['name'];
                            $playerOrnamentsInfo[$keyItemName]['name_cn'] = @$itemInfo['name_cn'];
                            $playerOrnamentsInfo[$keyItemName]['external_id'] = @$itemInfo['external_id'];
                            $playerOrnamentsInfo[$keyItemName]['external_name'] = @$itemInfo['external_name'];
                            $playerOrnamentsInfo[$keyItemName]['total_cost'] = @$itemInfo['total_cost'];
                            $playerOrnamentsInfo[$keyItemName]['is_trinket'] = @$itemInfo['is_trinket'] == 1 ? true : false;;
                            $playerOrnamentsInfo[$keyItemName]['is_purchasable'] = @$itemInfo['is_purchasable'] == 1 ? true : false;;
                            $playerOrnamentsInfo[$keyItemName]['slug'] = @$itemInfo['slug'];
                            $playerOrnamentsInfo[$keyItemName]['image'] = @ImageConversionHelper::showFixedSizeConversion(@$itemInfo['image'],50,50);
                            $playerOrnamentsInfo[$keyItemName]['purchase_time'] = null;
                            $playerOrnamentsInfo[$keyItemName]['cooldown'] = null;
                        }else {//道具
                            $playerItemsInfo[$keyItemName]['item_id'] = (Int)@$itemInfo['id'];
                            $playerItemsInfo[$keyItemName]['name'] = @$itemInfo['name'];
                            $playerItemsInfo[$keyItemName]['name_cn'] = @$itemInfo['name_cn'];
                            $playerItemsInfo[$keyItemName]['external_id'] = @$itemInfo['external_id'];
                            $playerItemsInfo[$keyItemName]['external_name'] = @$itemInfo['external_name'];
                            $playerItemsInfo[$keyItemName]['total_cost'] = @$itemInfo['total_cost'];
                            $playerItemsInfo[$keyItemName]['is_trinket'] = @$itemInfo['is_trinket'] == 1 ? true : false;;
                            $playerItemsInfo[$keyItemName]['is_purchasable'] = @$itemInfo['is_purchasable'] == 1 ? true : false;
                            $playerItemsInfo[$keyItemName]['slug'] = @$itemInfo['slug'];
                            $playerItemsInfo[$keyItemName]['image'] = @ImageConversionHelper::showFixedSizeConversion(@$itemInfo['image'],50,50);
                            $playerItemsInfo[$keyItemName]['purchase_time'] = null;
                            $playerItemsInfo[$keyItemName]['cooldown'] = null;
                        }
                    }else{
                        $playerItemsNull[$keyItemName] = 'Unknown';
                    }
                }else{
                    $playerItemsNull[$keyItemName] = 'Unknown';
                }
            }else{
                $playerItemsNull[$keyItemName] = $playerItemsDefaultInfo;
            }
        }
        $total_cost_array = array_column($playerItemsInfo,'total_cost');
        array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
        $playerItemsInfoResultMerge = array_merge($playerItemsInfo,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResult = array_combine($playerItemsKeys,$playerItemsInfoResultMerge);
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            if ($val_res != 'Unknown'){
                if (empty($val_res['item_id'])){
                    $playerItemsInfoResult[$key_res] = null;
                }
            }
        }
        return $playerItemsInfoResult;
    }
    /**
     * @param $playerItems
     * @throws \yii\base\Exception
     */
    public static function conversionPlayerItemsIds($playerItems){
        $playerItemsInfo = [];
        foreach ($playerItems as $key => $value){
            if ($value){
                $itemInfoId = @self::getMainIdByRelIdentityIdOrUnknown('lol_item',$value['id'],'3');
                $itemInfo = MetadataLolItem::find()->select('id,external_id,external_name,name,name_cn,total_cost,is_trinket,is_purchasable,image,description,description_cn')->where(['id' => $itemInfoId])->asArray()->one();
                if($itemInfo){
                    $playerItemsInfo[] = (Int)$itemInfo['id'];
                }else{
                    $playerItemsInfo[] = null;
                }
            }else{
                $playerItemsInfo[] = null;
            }
        }
        return $playerItemsInfo;
    }
    /**
     * @param $playerItems
     * @throws \yii\base\Exception
     */
    public static function conversionPlayerItemsIdsFromRedis($ingame_timestamp,$playerItems,$playerId,$battleId,$matchId,$originId){
        $need_change = 0;
        //redis初始化
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redisNameRelation = "lol:items:relation:".$originId;
        $itemsList = "lol:items:list:".$originId;
        $redisNameMatchLolPlayersItems = "match_lol:players:items_timeline:".$matchId.":".$battleId;
        //当前这个player的装备信息 然后获取上一个装备信息列表，判断有没有变化 TODO
        $playersItemsRedis = $Redis->hget($redisNameMatchLolPlayersItems,$playerId);
        if ($playersItemsRedis){
            $playersItemsBeforeLine = json_decode($playersItemsRedis,true);
            $playersItemsBefore = @end($playersItemsBeforeLine)['items'];
        }else{
            $playersItemsBeforeLine = $playersItemsBefore = [];
        }
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        foreach ($playerItems as $key => $value){
            if ($value){
                $itemInfoId = $Redis->hget($redisNameRelation,$value['id']);
                if($itemInfoId){
                    if (!in_array($itemInfoId,$playersItemsBefore)){
                        $need_change = 1;
                    }
                    $itemInfoId_val = (Int)$itemInfoId;
                    //获取当前
                    $itemInfoResRedis = $Redis->hget($itemsList,$itemInfoId_val);
                    if ($itemInfoResRedis){
                        $itemInfoRes = @json_decode($itemInfoResRedis,true);
                        if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                            $playerOrnamentsInfo[$key] = $itemInfoId_val;
                        }else{//道具
                            $playerItemsInfo[$key]['total_cost'] = @$itemInfoRes['total_cost'];
                            $playerItemsInfo[$key]['item_id'] = $itemInfoId_val;
                        }
                    }else{//道具
                        $playerItemsInfo[$key]['total_cost'] = 1;
                        $playerItemsInfo[$key]['item_id'] = (Int)@self::getMetaDataUnknownId('lol_items');
                    }
                }else{
                    $playerItemsInfo[$key]['total_cost'] = 1;
                    $playerItemsInfo[$key]['item_id'] = (Int)@self::getMetaDataUnknownId('lol_items');
                }
            }else{
                $playerItemsNull[$key] = null;
            }
        }
        $total_cost_array = array_column($playerItemsInfo,'total_cost');
        array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
        $playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
        $playerItemsInfoResult = array_merge($playerItemsInfoRet,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        //循环添加 purchase_time购买时间点 cooldown装备使用CD
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsInfo = null;
            if ($val_res){
                $ItemsInfo = [
                    'item_id' => $val_res,
                    'purchase_time' => null,
                    'cooldown' => null
                ];
            }
            $playerItemsInfoResultEnd[] = $ItemsInfo;
        }
        //需要变化则更新redis
        if ($need_change == 1){
            $playerItemsInfoArray = [
                'ingame_timestamp'=>$ingame_timestamp,
                'items'=>$playerItemsInfoResult
            ];
            $playersItemsBeforeLine[] = $playerItemsInfoArray;
            $playerItemsLine =  json_encode($playersItemsBeforeLine,320);
            $Redis->hset($redisNameMatchLolPlayersItems,$playerId,$playerItemsLine);
        }else{
            $playerItemsLine = $playersItemsRedis;
        }
        //设置返回数据
        $playerItemsInfoRes = json_encode($playerItemsInfoResultEnd,320);
        $result = [
            'items' => $playerItemsInfoRes,
            'items_timeline' => $playerItemsLine
        ];
        return $result;
    }
    /**
     * @param $playerItems
     * @throws \yii\base\Exception
     */
    public static function setPlayerItemsIds($playerItemsLine,$playerId,$battleId,$matchId){
        //redis初始化
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redisNameMatchLolPlayersItems = "match_lol:players:items_timeline:".$matchId.":".$battleId;
        $Redis->hset($redisNameMatchLolPlayersItems,$playerId,$playerItemsLine);
        return true;
    }
    /**
     * @param $matchId
     * @param $relOrderId
     * 根据比赛id跟order获取battleId
     */
    public static function getBattleIdByMatchIdAndRelBattleId($matchId,$rel_battleId)
    {
        //查询数据库battle 是不是有 默认领先和但图弃权
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        $battle_res = $Redis->hget($redis_name,$rel_battleId);
        if ($battle_res){
            $battle_res_explode = explode('-',$battle_res);
            $battle_id = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }else{
            return null;
        }
        $battle_Info = [
            'battle_id' => intval($battle_id),
            'battle_order' => intval($battle_order)
        ];
        return $battle_Info;
    }

    /**
     * @param $matchId
     * @param $battleOrder
     * @param $gameId
     * @return int
     * @throws \yii\db\Exception
     */
    public function createMatchBattleByMatchId($matchId,$battleOrder,$gameId)
    {
        $matchBattle         = new MatchBattle();
        $attributes['game']  = $gameId;
        $attributes['match'] = $matchId;
        $attributes['order'] = $battleOrder;
        $matchBattle->setAttributes($attributes);
        $matchBattle->save();
        $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
        TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);
        $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['id'=>$matchBattle->id,'match'=>$matchId]];
        TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrement,'');
        return $matchBattle->id;
    }
    /**
     * @param $matchId
     * @param $relOrderId
     * 根据比赛id跟order获取battleId
     */
    public static function getbattleIdByMatchIdAndOrder($matchId,$rel_matchId,$rel_battleId,$socket_time,$current_timestamp = 0)
    {
        //比赛信息
        $match_all_info_current_match_base_redis_name = self::MATCH_ALL_INFO.":".$matchId.":current:match_base";
        $match_battles_info_redis_name = self::MATCH_ALL_INFO.":".$matchId.":current:battles";
        //查询数据库battle 是不是有 默认领先和但图弃权
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        $battle_id = $battle_order = $match_start = null;
        //查询有没有设置过
        $match_battle_nums = $Redis->hlen($redis_name);
        if ($match_battle_nums){
            $battle_res = $Redis->hget($redis_name,$rel_battleId);
            if ($battle_res){
                $battle_res_explode = explode('-',$battle_res);
                $battle_id = $battle_res_explode[0];
                $battle_order = $battle_res_explode[1];
                $socket_time_start = @$Redis->hget($match_battles_info_redis_name.":".$battle_id.":base",'begin_at');
            }else{
                //来到这的肯定是 至少大于等于第二个battle
                $battle_order = $match_battle_nums+1;
                //self::chuLiBattleId($matchId,$battle_order);
                $battle_info_more = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>$battle_order])->andWhere(['deleted'=> 2])->asArray()->one();
                //判断是不是单图弃权  是order就是3
                if ($battle_info_more['is_forfeit'] == 1){
                    $Redis->hset($redis_name,'forfeit_'.$battle_order,$battle_info_more['id'].'-'.$battle_order);
                    $battle_order = $battle_order + 1;
                }
                //更新库并获得battle_id
                $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
                $socket_time_start = date("Y-m-d H:i:s",$begin_at);
                $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId);
                //先插入redis 后更新库
                $Redis->hset($redis_name,$rel_battleId,$battle_id.'-'.$battle_order);
                $Redis->hset($match_all_info_current_match_base_redis_name,'begin_at',$socket_time_start);
                $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'begin_at',$socket_time_start);
                $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'order',$battle_order);
                $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'id',$battle_id);
            }
        }else{
            //没有就期望第一个battle是1
            $battle_order = 1;
            //查询一下数据库order为1的 能不能用
            $battle_info1 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>1])->andWhere(['deleted'=> 2])->asArray()->one();
            if ($battle_info1){
                //判断是不是默认领先  是order就是2
                if ($battle_info1['is_default_advantage'] == 1){
                    $Redis->hset($redis_name,'advantage_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //判断是不是单图弃权  是order就是2
                if ($battle_info1['is_forfeit'] == 1){
                    $Redis->hset($redis_name,'forfeit_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //如果是2那么就查一下是不是单图弃权
                if ($battle_order == 2){
                    $battle_info2 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>2])->andWhere(['deleted'=> 2])->asArray()->one();
                    //判断是不是单图弃权  是order就是3
                    if ($battle_info2['is_forfeit'] == 1){
                        $Redis->hset($redis_name,'forfeit_'.$battle_order,$battle_info2['id'].'-'.$battle_order);
                        $battle_order = 3;
                    }
                    //如果是3那么就查一下是不是单图弃权
                    if ($battle_order == 3){
                        $battle_info3 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>3])->andWhere(['deleted'=> 2])->asArray()->one();
                        //判断是不是单图弃权  是order就是4
                        if ($battle_info3['is_forfeit'] == 1){
                            $Redis->hset($redis_name,'forfeit_'.$battle_order,$battle_info3['id'].'-'.$battle_order);
                            $battle_order = 4;
                        }
                    }
                }
            }
            $match_start = 1;
            $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
            $socket_time_start = date("Y-m-d H:i:s",$begin_at);
            $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId);
            $Redis->hset($redis_name,$rel_battleId,$battle_id.'-'.$battle_order);
            $Redis->hset($match_all_info_current_match_base_redis_name,'begin_at',$socket_time_start);
            $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'begin_at',$socket_time_start);
            $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'order',$battle_order);
            $Redis->hset($match_battles_info_redis_name.":".$battle_id.":base",'id',$battle_id);
        }
//        $Redis->expire($redis_name,432000);//5天有效期
        $battle_Info = [
            'battle_id' => intval($battle_id),
            'battle_order' => intval($battle_order),
            'match_start' => $match_start,
            'begin_at' => $socket_time_start
        ];
        return $battle_Info;
    }
    //生成battle
    public static function getbattleIdByMatchIdAndOrderNew($redis,$redis_base_name,$matchId,$rel_matchId,$rel_battleId,$socket_time,$current_timestamp = 0,$isSetBattleStartTime=true)
    {
        //比赛信息
        $match_battles_info_redis_name = $redis_base_name."battles";
        //查询数据库battle 是不是有 默认领先和但图弃权
        $redis_name = $redis_base_name."match_battles";
        $battle_id = $battle_order = $match_start = null;
        //查询有没有设置过
        //**********  match_all_info:45371:current:match_battles 1 12137-1-1 创建一个redis
        $match_battle_nums = $redis->hLen($redis_name);
        if ($match_battle_nums){
            $battle_res = $redis->hGet($redis_name,$rel_battleId);
            if ($battle_res){
                $battle_res_explode = explode('-',$battle_res);
                $battle_id = $battle_res_explode[0];
                $battle_order = $battle_res_explode[1];
            }else{
                //来到这的肯定是 至少大于等于第二个battle
                $battle_order = $match_battle_nums + 1;
                $battle_info_more = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>$battle_order])->andWhere(['deleted'=> 2])->asArray()->one();
                //判断是不是单图弃权  是order就是3
                if ($battle_info_more['is_forfeit'] == 1){
                    $redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info_more['id'].'-'.$battle_order);
                    $battle_order = $battle_order + 1;
                }
                //更新库并获得battle_id
                $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
                $socket_time_start = date("Y-m-d H:i:s",$begin_at);
                $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId,$isSetBattleStartTime);
                //先插入redis
                $redis->hSet($redis_name,$rel_battleId,$battle_id.'-'.$battle_order);
                $matchInfo = [
                    'begin_at' => $socket_time_start,
                    'order' => $battle_order,
                    'id' => $battle_id
                ];
                $redis->hMSet($match_battles_info_redis_name.":".$battle_id.":base",$matchInfo);
            }
        }else{
            $match_start = 1;
            //没有就期望第一个battle是1
            $battle_order = 1;
            //查询一下数据库order为1的 能不能用
            $battle_info1 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>1])->andWhere(['deleted'=> 2])->asArray()->one();
            if ($battle_info1){
                //判断是不是默认领先  是order就是2
                if ($battle_info1['is_default_advantage'] == 1){
                    $redis->hSet($redis_name,'advantage_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //判断是不是单图弃权  是order就是2
                if ($battle_info1['is_forfeit'] == 1){
                    $redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //如果是2那么就查一下是不是单图弃权
                if ($battle_order == 2){
                    $battle_info2 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>2])->andWhere(['deleted'=> 2])->asArray()->one();
                    //判断是不是单图弃权  是order就是3
                    if ($battle_info2['is_forfeit'] == 1){
                        $redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info2['id'].'-'.$battle_order);
                        $battle_order = 3;
                    }
                    //如果是3那么就查一下是不是单图弃权
                    if ($battle_order == 3){
                        $battle_info3 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>3])->andWhere(['deleted'=> 2])->asArray()->one();
                        //判断是不是单图弃权  是order就是4
                        if ($battle_info3['is_forfeit'] == 1){
                            $redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info3['id'].'-'.$battle_order);
                            $battle_order = 4;
                        }
                    }
                }
            }
            $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
            $socket_time_start = date("Y-m-d H:i:s",$begin_at);
            $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId,$isSetBattleStartTime);
            //先插入redis
            $redis->hSet($redis_name,$rel_battleId,$battle_id.'-'.$battle_order);
            $matchInfo = [
                'begin_at' => $socket_time_start,
                'order' => $battle_order,
                'id' => $battle_id
            ];
            $redis->hMSet($match_battles_info_redis_name.":".$battle_id.":base",$matchInfo);
        }
//        $redis->expire($redis_name,432000);//5天有效期
        $battle_Info = [
            'battle_id' => intval($battle_id),
            'battle_order' => intval($battle_order),
            'match_start' => $match_start
        ];
        return $battle_Info;
    }

    /**
     * @param $Redis
     * @param $redis_base_name
     * @param $matchId
     * @param $rel_matchId
     * @param $socket_time
     * @param int $current_timestamp
     * @return array
     * @throws BusinessException
     * 没有固定的battle ID 时候使用
     */
    public static function getBattleIdByMatchIdAndNoOrder($Redis,$redis_base_name,$matchId,$rel_matchId,$rel_order_id,$socket_time,$current_timestamp = 0,$isSetBattleStartTime = false)
    {
        $rel_battleId = $rel_order_id;
        //比赛信息
        $match_all_info_current_match_base_redis_name = $redis_base_name."match_base";
        $match_battles_info_redis_name = $redis_base_name."battles";
        //查询数据库battle 是不是有 默认领先和但图弃权
        $redis_name = $redis_base_name."match_battles";
        $battle_id = $battle_order = $match_start = null;
        //查询有没有设置过
        // ******** match_all_info:3109:current:match_battles  添加
        //  ********  如果是未结束，创建新battle 把 now_battle_num 删了
        $match_battle_nums = $Redis->hVals($redis_name);
        if ( count($match_battle_nums) > 0 ){
            $now_battle_num_redis = $Redis->get($redis_base_name."now_battle_num");
            if ($now_battle_num_redis){
                $battle_res = $Redis->hGet($redis_name,$now_battle_num_redis);
                $battle_res_explode = explode('-',$battle_res);
                $battle_id = $battle_res_explode[0];
                $battle_order = $battle_res_explode[1];
            }else{
                $battle_order_new = 0;
                foreach ($match_battle_nums as $battle_item){
                    $battle_item_val = explode('-',$battle_item);
                    $battle_status = $battle_item_val[2];//battle_status
                    if ($battle_status == 1){
                        $battle_order_new ++;
                    }
                }
                //来到这的肯定是 至少大于等于第二个battle
                $battle_order = $battle_order_new + 1;
                //self::chuLiBattleId($matchId,$battle_order);
                $battle_info_more = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>$battle_order])->andWhere(['deleted'=> 2])->asArray()->one();
                //判断是不是单图弃权  是order就是3
                if (@$battle_info_more['is_forfeit'] == 1){
                    $Redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info_more['id'].'-'.$battle_order.'-1');
                    $battle_order = $battle_order + 1;
                }
                //更新库并获得battle_id
                $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
                $socket_time_start = date("Y-m-d H:i:s",$begin_at);
                $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId,$isSetBattleStartTime);
                //先插入redis 后更新库
                //$Redis->hSet($redis_name,$rel_battleId,$battle_id.'-'.$battle_order);
                $Redis->hSet($redis_name,$battle_order,$battle_id.'-'.$battle_order.'-0');
                $Redis->hSet($match_all_info_current_match_base_redis_name,'begin_at',$socket_time_start);
                $matchInfo = [
                    'begin_at' => $socket_time_start,
                    'order' => $battle_order,
                    'id' => $battle_id,
                ];
                $Redis->hMSet($match_battles_info_redis_name.":".$battle_id.":base",$matchInfo);
                $Redis->set($redis_base_name."now_battle_num",$battle_order);
            }
        }else{
            //没有就期望第一个battle是1
            $battle_order = 1;
            //查询一下数据库order为1的 能不能用
            $battle_info1 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>1])->andWhere(['deleted'=> 2])->asArray()->one();
            if ($battle_info1){
                //判断是不是默认领先  是order就是2
                if ($battle_info1['is_default_advantage'] == 1){
                    $Redis->hSet($redis_name,'advantage_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //判断是不是单图弃权  是order就是2
                if ($battle_info1['is_forfeit'] == 1){
                    $Redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info1['id'].'-'.$battle_order);
                    $battle_order = 2;
                }
                //如果是2那么就查一下是不是单图弃权
                if ($battle_order == 2){
                    $battle_info2 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>2])->andWhere(['deleted'=> 2])->asArray()->one();
                    //判断是不是单图弃权  是order就是3
                    if ($battle_info2['is_forfeit'] == 1){
                        $Redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info2['id'].'-'.$battle_order);
                        $battle_order = 3;
                    }
                    //如果是3那么就查一下是不是单图弃权
                    if ($battle_order == 3){
                        $battle_info3 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>3])->andWhere(['deleted'=> 2])->asArray()->one();
                        //判断是不是单图弃权  是order就是4
                        if ($battle_info3['is_forfeit'] == 1){
                            $Redis->hSet($redis_name,'forfeit_'.$battle_order,$battle_info3['id'].'-'.$battle_order);
                            $battle_order = 4;
                        }
                    }
                }
            }
            $match_start = 1;
            $begin_at = strtotime($socket_time)-(Int)$current_timestamp;
            $socket_time_start = date("Y-m-d H:i:s",$begin_at);
            $battle_id = self::createLolBattle($matchId,$battle_order,$socket_time_start,$rel_matchId,$rel_battleId,$isSetBattleStartTime);
            $Redis->hSet($redis_name,$battle_order,$battle_id.'-'.$battle_order.'-0');
            $Redis->hSet($match_all_info_current_match_base_redis_name,'begin_at',$socket_time_start);
            $matchInfo = [
                'begin_at' => $socket_time_start,
                'order' => $battle_order,
                'id' => $battle_id,
            ];
            $Redis->hMSet($match_battles_info_redis_name.":".$battle_id.":base",$matchInfo);
            $Redis->set($redis_base_name."now_battle_num",$battle_order);
        }
//        $Redis->expire($redis_name,432000);//5天有效期
        $battle_Info = [
            'battle_id' => intval($battle_id),
            'battle_order' => intval($battle_order),
            'match_start' => $match_start
            //'begin_at' => $socket_time_start
        ];
        return $battle_Info;
    }
    //处理确定可以使用的battle
    public static function chuLiBattleId($Redis,$redis_name,$matchId,$rel_battleId,$battle_order){
        $battle_info1 = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>$battle_order])->asArray()->one();
        //判断是不是单图弃权  是order就是3
        if ($battle_info1['is_forfeit'] == 1){
            $Redis->hset($redis_name,$rel_battleId,'2-'.$battle_order);
            $battle_order = $battle_order + 1;
        }
        return $battle_order;
    }
    /**
     * @param $matchId
     * @param $relOrderId
     * 根据比赛id跟order获取battleId
     */
    public static function getbattleIdByMatchIdAndOrderByWs($matchId,$rel_matchId,$rel_battleId,$socket_time)
    {
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        $battle_id = $battle_order = $match_start = null;
        $battle_res = $Redis->hget($redis_name,$rel_battleId);
        if ($battle_res){
            $battle_res_explode = explode('-',$battle_res);
            $battle_id = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }else{
            return null;
        }
        $is_battle_start = false;
        //获取是否有数据了
        $is_have_ws_data_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_ws:battles:'.$battle_id.':is_have_ws_data';
        $is_have_ws_data = $Redis->get($is_have_ws_data_redis_name);
        if (!$is_have_ws_data){
            $is_battle_start = true;
            $Redis->set($is_have_ws_data_redis_name,1);
        }
        $battle_Info = [
            'battle_id' => $battle_id,
            'battle_order' => $battle_order,
            'match_start' => $match_start,
            'is_battle_start' => $is_battle_start
        ];
        return $battle_Info;
    }
    //获取event的order
    public static function getEventOrder($battle_id){
        $eventModel = MatchBattleEventLol::find()->where([
            'battle_id' => $battle_id
        ])->count();
        return $eventModel + 1;
    }
    //获取event的order
    public static function getEventOrderByGameType($battle_id,$gameType){
        $eventModel = 0;
        switch ($gameType){
            case 'lol':
                $eventModel = MatchBattleEventLol::find()->where(['battle_id' => $battle_id])->count();
                break;
            case 'dota':
                $eventModel = MatchBattleEventDota2::find()->where(['battle_id' => $battle_id])->count();
                break;
        }
        return $eventModel + 1;
    }

    //创建battle
    public static function createLolBattle($matchId,$battle_order,$socket_time,$rel_matchId,$rel_battleId,$isSetBattleStartTime=true){
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //先插入battle 获得battle_id
            // 找到对应的battleId，更新battle
            $battleBase=MatchBattle::find()->where([
                'match'=>$matchId,
                'order'=>$battle_order,
            ])->one();
            if(!$battleBase){
                //首先插入关系表
                $matchBattleRelation = new MatchBattleRelation();
                $matchBattleRelation->setAttributes([
                    'match_id' => (int)$matchId,
                    'order' => $battle_order,
                    'rel_match_id' => (String)$rel_matchId,
                    'rel_battle_id' => (String)$rel_battleId
                ]);
                if (!$matchBattleRelation->save()){
                    throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
                }
                //插入battle
                $battleBase=new MatchBattle();
                $battleArray = [
                    'match'=>$matchId,
                    'order'=>$battle_order
                ];
                if ($isSetBattleStartTime){
                    $battleArray['begin_at'] = $socket_time;
                }
                $battleBase->setAttributes($battleArray);
                $battleBase->save();

                $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
                TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

                $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleBase->id,'match'=>$matchId]];
                TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');

                // battle开始时间存入redis
                $Redis = \Yii::$app->get('redis');
                $Redis->select(env('REDIS_DATABASE'));
//                $Redis->set("Pws:".$matchId.":battle:".$battleBase['id'].":begin_at",$socket_time);
                $Redis->set("Pws:"."battle:".$battleBase['id'].":match:{$matchId}:begin_at",$socket_time);
            }
            $battle_id=$battleBase['id'];
            $transaction->commit();
            return $battle_id;
        }catch (\Exception $e) {
            $transaction->rollBack();
            throw new BusinessException($e->getMessage(), "添加失败,battle已经存在");
        }
    }
    //第一次事件
    public static function getFirstEventInfo($event_redis_name,$matchId,$battleId,$eventInfo,$events_from = 'api'){
        $redis_name_base = Consts::MATCH_ALL_INFO.':'.$matchId.':'.$event_redis_name.':battles:'.$battleId;
        $redis_name = $redis_name_base.':first_info';
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        //记录事件的列表  防治重复事件
        $match_battle_events_list_redis_name = $redis_name_base.':rel_events_list';
        $rel_event_id = @$eventInfo['id'];
        $rel_event_id_have = $Redis->hget($match_battle_events_list_redis_name,$rel_event_id);
        if ($rel_event_id_have){
            return null;
        }else{
            $Redis->hset($match_battle_events_list_redis_name,$rel_event_id,json_encode($eventInfo,320));
            $Redis->expire($match_battle_events_list_redis_name,3600);//1小时有效期
        }
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        $killed_type = $eventInfo['payload']['killed']['type'];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
                $killer_type = $eventInfo['payload']['killer']['type'];
                if ($killer_type == 'player'){//人击杀人
                    //设置初始值
                    $killer_team = $eventInfo['payload']['killer']['object']['team'];
                    $team_kills_key = 'team_kills_'.$killer_team;
                    //查询是否是一血
                    $first_blood = $Redis->hget($redis_name,'first_blood');
                    if (!$first_blood){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_blood'
                        ];
                        $Redis->hset($redis_name,'first_blood',1);
                    }
                    //查询 first_to_5_kills first_to_10_kills
                    //查询当前team击杀了多少人
                    //查询是否五杀了
                    $first_to_5_kills = $Redis->hget($redis_name,'first_to_5_kills');
                    $first_to_10_kills = $Redis->hget($redis_name,'first_to_10_kills');
                    $team_kills_res = $Redis->hget($redis_name,$team_kills_key);//击杀队伍
                    if ($team_kills_res){
                        $now_team_kills = $team_kills_res + 1;
                        $Redis->hset($redis_name,$team_kills_key,$now_team_kills);
                        if ($now_team_kills == 5 && empty($first_to_5_kills)){
                            $eventInfoRes = [
                                'is_first_event' => true,
                                'first_event_type' => 'first_to_5_kills'
                            ];
                            $Redis->hset($redis_name,'first_to_5_kills', $killer_team);
                        }
                        if ($now_team_kills == 10 && empty($first_to_10_kills)){
                            $eventInfoRes = [
                                'is_first_event' => true,
                                'first_event_type' => 'first_to_10_kills'
                            ];
                            $Redis->hset($redis_name,'first_to_10_kills', $killer_team);
                        }
                    }else{
                        $Redis->hset($redis_name,$team_kills_key, 1);
                    }
                }
                break;
            case 'rift_herald':
                $rift_herald = $Redis->hget($redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald'
                    ];
                    $Redis->hset($redis_name,'first_rift_herald',1);
                }
                break;
            case 'drake':
                $killed_sub_type = $eventInfo['payload']['killed']['object']['type'];
                if ($killed_sub_type == 'elder'){
                    $first_elder_dragon = $Redis->hget($redis_name,'first_elder_dragon');
                    if (!$first_elder_dragon){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_elder_dragon'
                        ];
                        $Redis->hset($redis_name,'first_elder_dragon',1);
                    }
                }else{
                    $first_dragon = $Redis->hget($redis_name,'first_dragon');
                    if (!$first_dragon){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_dragon'
                        ];
                        $Redis->hset($redis_name,'first_dragon',1);
                    }
                }
                break;
            case 'baron_nashor':
                $first_baron_nashor = $Redis->hget($redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor'
                    ];
                    $Redis->hset($redis_name,'first_baron_nashor',1);
                }
                break;
            case 'elder_dragon':
                $first_elder_dragon = $Redis->hget($redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon'
                    ];
                    $Redis->hset($redis_name,'first_elder_dragon',1);
                }
                break;
            case 'tower':
                $first_turret = $Redis->hget($redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret'
                    ];
                    $Redis->hset($redis_name,'first_turret',1);
                }
                break;
            case 'inhibitor':
                $first_inhibitor = $Redis->hget($redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor'
                    ];
                    $Redis->hset($redis_name,'first_inhibitor',1);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null
                ];
        }
        $Redis->expire($redis_name,432000);//5天有效期
        return $eventInfoRes;
    }
    //Abios dota第一次事件
    public static function getAbiosDotaFirstEventInfo($event_redis_name,$matchId,$battleId,$eventInfo,$events_from = 'api')
    {
        $redis_name = $event_redis_name.':'.$matchId.':'.$battleId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        //记录事件的列表  防治重复事件
        $match_battle_events_list_redis_name = Consts::MARCH_BATTLE_EVENTS."_".$events_from.":".$matchId.":".$battleId;
        $rel_event_id = @$eventInfo['uuid'];
        $rel_event_id_have = $Redis->hget($match_battle_events_list_redis_name, $rel_event_id);
        if ($rel_event_id_have) {
            return null;
        } else {
            $Redis->hset($match_battle_events_list_redis_name, $rel_event_id, json_encode($eventInfo, 320));
            $Redis->expire($match_battle_events_list_redis_name, 3600);//1小时有效期
        }
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        $first_turret = $Redis->hget($redis_name,'first_tower');
        if (!$first_turret){
            $eventInfoRes = [
                'is_first_event' => true,
                'first_event_type' => 'first_tower'
            ];
            $Redis->hset($redis_name,'first_tower',1);
        }
        $Redis->expire($redis_name,432000);//5天有效期
        return $eventInfoRes;
    }
    /**
     * 获取所有的事件
     * @param $relMatchId
     * @return mixed
     * @throws \yii\base\Exception
     */
    public static function getEventsByRelMatchId($originId,$relMatchId)
    {
        $where = [
            'origin_id' => $originId,
            'match_id' => (String)$relMatchId,
            'type' => 'events'
        ];
        $allEventsData = MatchLived::find()
            ->orderBy('id ASC')
            ->where($where)
            ->select('match_data,type')
            ->asArray()
            ->all();
        if($allEventsData){
            return $allEventsData;
        }else{
            return null;
        }
    }

    /**
     * 从事件中获取杀龙信息
     * @param $allEvents
     * @param $faction
     * @return null[]
     */
    public static function conversionDragonKills($allEvents,$faction,$rel_match_id,$rel_battle_id){
        if(empty($allEvents)){
            return [];
        }
        $blue_dragon_kills_detail = [];
        $kills_num = 0;
        foreach ($allEvents as $key => $item){
            $event_info = json_decode($item['match_data'],true);
            if (isset($event_info['type']) && $event_info['type'] == 'hello'){
                continue;
            }
            $killed_type = $event_info['payload']['killed']['type'];
            $killer_type = $event_info['payload']['killer']['type'];
            if ($killed_type == 'drake' && $killer_type == 'player'){
                if ($faction == $event_info['payload']['killer']['object']['team'] && $event_info['game']['id'] == $rel_battle_id && $event_info['match']['id'] == $rel_match_id){
                    $blue_dragon_kills_detail[$kills_num]['ingame_timestamp'] = $event_info['ingame_timestamp'];
                    $dragon_name_info = Common::getEnumIngameGoalInfoByPandascoreName($event_info['payload']['killed']['object']['type']);
                    $blue_dragon_kills_detail[$kills_num]['dragon_name'] = @$dragon_name_info['dragon_name'];
                    $blue_dragon_kills_detail[$kills_num]['dragon_name_cn'] = @$dragon_name_info['dragon_name_cn'];
                    $kills_num ++;
                }
            }
        }
        return $blue_dragon_kills_detail;
    }
    /**
     * 从事件中获取杀龙信息
     * @param $allEvents
     * @param $faction
     * @return null[]
     */
    public static function conversionDragonKillsId($allEvents,$faction,$rel_match_id,$rel_battle_id){
        if (empty($allEvents)){
            return null;
        }
        $blue_dragon_kills_detail = [
            0=>[
                'date'=>null,
                'drake' => null
            ]
        ];
        $kills_num = 0;
        $events_list = [];
        foreach ($allEvents as $key => $item){
            $event_info = json_decode($item['match_data'],true);
            if (isset($event_info['type']) && $event_info['type'] == 'hello'){
                continue;
            }
            if ($event_info['game']['id'] == $rel_battle_id && $event_info['match']['id'] == $rel_match_id){
                if (isset($events_list[$event_info['id']]) && $events_list[$event_info['id']] == 1){
                    continue;
                }
                $events_list[$event_info['id']] = 1;
                $killed_type = $event_info['payload']['killed']['type'];
                $killer_type = $event_info['payload']['killer']['type'];
                if ($killed_type == 'drake' && $killer_type == 'player'){
                    if ($faction == $event_info['payload']['killer']['object']['team']){
                        $blue_dragon_kills_detail[$kills_num]['date'] = $event_info['ingame_timestamp'];
                        $dragon_name = Common::getGoalIdByPandascoreName($event_info['payload']['killed']['object']['type']);
                        $blue_dragon_kills_detail[$kills_num]['drake'] = $dragon_name;
                        $kills_num ++;
                    }
                }
            }
        }
        return $blue_dragon_kills_detail;
    }
    //设置battleDetails
    public static function setBattleDetails($battleId, $data)
    {
        // 设置详情，一血等
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if (!$battleExtLol) {
            $battleExtLol = new MatchBattleExtLol();
            $battleExtLol->setAttribute('id', $battleId);
        }
        $battleExtLol->setAttributes($data);
        if (!$battleExtLol->save()) {
            throw new BusinessException($battleExtLol->getErrors(), '保存失败');
        }
    }
    public static function setGoldDiffTimeLine($battleId, $data)
    {
        // 设置阵容
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if (!$battleExtLol) {
            $battleExtLol = new MatchBattleExtLol();
            $battleExtLol->setAttribute('id', $battleId);
        }
        $data = [$data];
        if (isset($battleExtLol['gold_diff_timeline']) && $battleExtLol['gold_diff_timeline']){
            $gold_diff_timeline = json_decode($battleExtLol['gold_diff_timeline'],true);
            $data = array_merge($gold_diff_timeline,$data);
        }
        $battleExtLol->setAttribute('gold_diff_timeline', json_encode($data));
        if (!$battleExtLol->save()) {
            throw new BusinessException($battleExtLol->getErrors(), '保存经济差失败');
        }
    }
    public static function setGoldDiffTimeLineByRefresh($battleId, $data)
    {
        // 设置阵容
        $battleExtLol = MatchBattleExtLol::find()->where(['id' => $battleId])->one();
        if (!$battleExtLol) {
            $battleExtLol = new MatchBattleExtLol();
            $battleExtLol->setAttribute('id', $battleId);
        }
        $battleExtLol->setAttribute('gold_diff_timeline', json_encode($data));
        if (!$battleExtLol->save()) {
            throw new BusinessException($battleExtLol->getErrors(), '保存经济差失败');
        }
    }

    public static function getTeamOrderByTeamFaction($battle_id,$team_faction)
    {
        $MatchBattleTeamModel = MatchBattleTeam::find()->alias('mbt')
            ->select('mbt.id,mbt.order')
            ->leftJoin('match_battle_team_ext_lol as mbtel' ,'mbt.id = mbtel.id')
            ->where(['mbt.battle_id'=>$battle_id])
            ->andWhere(['mbtel.faction'=>$team_faction]);
        $sql=$MatchBattleTeamModel->createCommand()->getRawSql();
        $info = $MatchBattleTeamModel->asArray()->one();
        if ($info){
            return $info['order'];
        }else{
            return null;
        }
    }
    //更新HOT里面的 auto_add_type  为restapi 为了利用restapi重新覆盖 match数据
    public static function changeHotData($origin_id,$game_id,$match_id){
        //$HotDataRunningMatchRes = HotDataRunningMatch::find()->where(['origin_id'=>$origin_id,'game_id'=>$game_id,'match_id'=>$match_id])->one();
        $HotDataRunningMatchRes = HotDataRunningMatch::find()->where(['match_id'=>$match_id])->one();
        if ($HotDataRunningMatchRes){
            $now_date = date("Y-m-d H:i:s",time()+1600);
            $HotDataRunningMatchRes->setAttributes([
                'auto_add_type' => 'restapi',
                'deal_status' => 1,
                'match_start_time' => $now_date
            ]);
            $HotDataRunningMatchRes->save();
        }
        return;
    }

    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 获取关联关系表的一条数据
     */
    public static function getRelationByRelId($resourceType,$relIdentityId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',(int)$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['rel.standard_id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where)->asArray();
        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info;
        }
    }
    //查询当前battle是否弃权
    public static function getMatchBattleForfeitValue($matchId,$battleId){
        $redis_name = Consts::MARCH_BATTLE_IS_FORFEIT.':'.$matchId.":".$battleId;
        $Redis = \Yii::$app->get('redis');
//        $Redis->select(self::REDIS_SELECT_DB);
        $Redis->select(env('REDIS_DATABASE'));
        return $Redis->get($redis_name);
    }
    /**
     * @param $relIdentityId (比赛来源id)
     * @param $advantageTeamId (默认领先数据源战队id)
     * @throws BusinessException
     * @throws \yii\base\Exception
     * 比赛
     */
    public static function defaultAdvantageDealMatch($relIdentityId,$advantageTeamId,$originId,$type = 'socket')
    {
//        return;
        $matchMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_MATCH,$relIdentityId,$originId);
        if(!$matchMasterId){
            return;
        }
        //判断更新设置 是不是自动更新
        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchMasterId])->andWhere(['tag_type'=>'update_data'])->andWhere(['update_type'=>3])->asArray()->one();
        if (@$updateInfo['origin_id'] != $originId){
            return;
        }
        $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$advantageTeamId,$originId);
        if(!$teamMasterId){
            return;
        }
        if ($type == 'rest_api'){
            self::createMatchAdvantageBattle($matchMasterId,$teamMasterId,$relIdentityId,1,$type,$originId);
        }else{
            self::createMatchAdvantageBattle($matchMasterId,$teamMasterId,$relIdentityId,1,$type,$originId);
        }
    }
    /**
     * @param $matchId //平台MATCH ID
     * @param $winnerId //平台TEAM ID
     * @throws BusinessException
     * 默认领先创建battle
     */
    public static function createMatchAdvantageBattle($matchId,$winnerId,$rel_matchId = 0,$rel_battleId = 1,$type='rest_api',$originId){
        if ($type == 'rest_api'){
            $match_status = 1;
        }else{
            //首先
            $matchReal = MatchRealTimeInfo::find()->select(['*'])->where(['id'=>$matchId])->one();
            $match_status = $matchReal['status'];
        }
        if($match_status == 1) {
            $team_score1 = 0;
            $team_score2 = 0;
            // 每个battle有对局双方，从match获取
            $matchInfo = MatchService::getMergeMatch($matchId);
            $teamId1 = $matchInfo['team_1_id'];
            $teamId2 = $matchInfo['team_2_id'];
            $gameId = $matchInfo['game'];
            //设置地图
            $map_id = MatchService::getMapLists($gameId,'map_id','special',1);
            //开启事务
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($type == 'rest_api'){
                    if ($winnerId == 1) {
                        $team_score1 = 1;
                        $team_score2 = 0;
                    }
                    if ($winnerId == 2) {
                        $team_score1 = 0;
                        $team_score2 = 1;
                    }
                    //查询第一个默认领先的order 1
                    $battle = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>1])->one();
                    if (!$battle){
                        //创建battle
                        $battle = new MatchBattle();
                    }
                    //默认领先的
                    $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>1])->one();
                    if (!$matchBattleRelation){
                        //首先插入关系表
                        $matchBattleRelation = new MatchBattleRelation();
                        $matchBattleRelation->setAttributes([
                            'match_id' => (int)$matchId,
                            'order' => 1,
                            'rel_match_id' => (string)$rel_matchId,
                            'rel_battle_id' => (string)$rel_battleId
                        ]);
                        if (!$matchBattleRelation->save()) {
                            throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
                        }
                    }
                }else{
                    if ($winnerId == $teamId1) {
                        $team_score1 = 1;
                        $team_score2 = 0;
                        $winnerId = 1;
                    }
                    if ($winnerId == $teamId2) {
                        $team_score1 = 0;
                        $team_score2 = 1;
                        $winnerId = 2;
                    }
                    $value = MatchBattle::find()->where(['and',
                        ['=','match',$matchId],
                        ['=','is_default_advantage',1],
                    ])->one();
                    if($value){
                        throw new BusinessException([], "已经存在默认领先");
                    }
                    //首先插入关系表
                    $matchBattleRelation = new MatchBattleRelation();
                    $matchBattleRelation->setAttributes([
                        'match_id' => (int)$matchId,
                        'order' => 1,
                        'rel_match_id' => (string)$rel_matchId,
                        'rel_battle_id' => (string)$rel_battleId
                    ]);
                    if (!$matchBattleRelation->save()) {
                        throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
                    }
                    //创建battle
                    $battle = new MatchBattle();
                }
                $battle->setAttributes(
                    [
                        'map' => $map_id,//比赛地图
                        'begin_at' => null,//开始时间
                        'match' => $matchId,//比赛ID
                        'order' => 1,//对局ID
                        'end_at' => null,//结束时间
                        'status' => 3,//比赛结束
                        'winner' => $winnerId, //获胜方
                        'duration' => null,//对局时长
                        'game' => $gameId,
                        'is_default_advantage' => 1,//默认领先
                        'is_forfeit' => 2,//是否弃权
                        'is_battle_detailed' => 2,
                        'deleted_at'=>null,
                        'deleted'=>2
                    ]
                );
                if (!$battle->save()) {
                    throw new BusinessException($battle->getErrors(), '创建默认领先的battle失败');
                }
                $battle_id = $battle->id;
                //创建附属信息
                if ($gameId == 2){
                    $battleExtLol = new MatchBattleExtLol();
                    $battleExtLol->setAttribute('id', $battle_id);
                    if (!$battleExtLol->save()) {
                        throw new BusinessException($battle->getErrors(), '创建默认领先的battleExtLol失败');
                    }
                }
                if ($gameId == 3){
                    $battleExtLol = new MatchBattleExtDota2();
                    $battleExtLol->setAttribute('id', $battle_id);
                    if (!$battleExtLol->save()) {
                        throw new BusinessException($battle->getErrors(), '创建默认领先的battleExtDota2失败');
                    }
                }
                //创建队伍
                // 初始化主队，客队
                $mt1 = new MatchBattleTeam();
                $mt1->setAttributes(
                    [
                        'battle_id' => $battle_id,
                        'order' => 1,
                        'team_id' => $teamId1,
                        'score' => $team_score1,
                    ]
                );
                if (!$mt1->save()) {
                    throw new BusinessException($mt1->getErrors(), "添加battleteam1失败");
                }
                $mt2 = new MatchBattleTeam();
                $mt2->setAttributes(
                    [
                        'battle_id' => $battle_id,
                        'order' => 2,
                        'team_id' => $teamId2,
                        'score' => $team_score2,
                    ]
                );
                if (!$mt2->save()) {
                    throw new BusinessException($mt2->getErrors(), "添加battleteam2失败");
                }
                //redis创建一个占位的battle
                $redis_name = 'match_battle_info:' . $matchId;
                $Redis = \Yii::$app->get('redis');
                $Redis->select(env('REDIS_DATABASE'));
                $Redis->hset($redis_name,'advantage_1',$battle_id.'-1');
                //提交事务
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
//            throw new BusinessException($e->getMessage(), "添加默认领先battle失败");
                return;
            }
        }
    }
    /**
     * @param $relIdentityId  数据源比赛ID
     * @param $winnerRelTeamId 数据源 获胜的战队ID
     * @param $originId 数据源ID
     * @param string $battleOrderId battle的Order
     * @throws \yii\base\Exception
     */
    public static function defaultCreateForfeitBattle($relIdentityId,$winnerRelTeamId,$originId,$battleOrderId ='no_set')
    {
//        return;
        $matchMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_MATCH,$relIdentityId,$originId);
        if(!$matchMasterId){
            return;
        }
        //判断更新设置 是不是自动更新
        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchMasterId])->andWhere(['tag_type'=>'update_data'])->andWhere(['update_type'=>3])->asArray()->one();
        if (@$updateInfo['origin_id'] != $originId){
            return;
        }
        $teamMasterId = HotBase::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$winnerRelTeamId,$originId);
        if(!$teamMasterId){
            return;
        }
        if ($battleOrderId == 'no_set'){
            self::createForfeitBattle($originId,$matchMasterId,$teamMasterId);
        }else{
            self::createForfeitBattle($originId,$matchMasterId,$teamMasterId,$battleOrderId);
        }
    }
    /**
     * @param $originId 数据源ID
     * @param $matchId 比赛ID
     * @param $winnerId 获胜方的 masterId 主表ID
     * @param string $battleOrderId battle的Order
     * @return bool|void
     */
    public static function createForfeitBattle($originId,$matchId,$winnerId,$battleOrderId ='no_set'){
        if ($battleOrderId=='no_set'){
            //获取当前比赛的所有对局
            $matchBattleNum = (Int)MatchBattle::find()->where(['match'=>$matchId])->count();
            $order_id = $matchBattleNum + 1;
        }else{
            $order_id = (Int)$battleOrderId;
        }
        $team_score1 = 0;
        $team_score2 = 0;
        // 每个battle有对局双方，从match获取
        $matchInfo = MatchService::getMergeMatch($matchId);
        $teamId1 = $matchInfo['team_1_id'];
        $teamId2 = $matchInfo['team_2_id'];
        $gameId = $matchInfo['game'];
        //设置地图
        $map_id = MatchService::getMapLists($gameId,'map_id','special',1);
        //开启事务
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($winnerId == $teamId1) {
                $team_score1 = 1;
                $team_score2 = 0;
                $winnerId = 1;
            }
            if ($winnerId == $teamId2) {
                $team_score1 = 0;
                $team_score2 = 1;
                $winnerId = 2;
            }
            //获取当前的orderID 是否存在
            $battle = MatchBattle::find()->where(['match'=>$matchId])->andWhere(['order'=>$order_id])->one();
            if (!$battle){
                //创建battle
                $battle = new MatchBattle();
            }
            $battle->setAttributes([
                'map' => $map_id,//比赛地图
                'begin_at' => null,//开始时间
                'match' => $matchId,//比赛ID
                'order' => $order_id,//对局order
                'end_at' => null,//结束时间
                'status' => 3,//比赛结束
                'winner' => $winnerId, //获胜方
                'duration' => null,//对局时长
                'game' => $gameId,
                'is_default_advantage' => 2,//默认领先
                'is_forfeit' => 1,//是否弃权
                'is_battle_detailed' => 2,
                'deleted_at'=>null,
                'deleted'=>2
            ]);
            if (!$battle->save()) {
                throw new BusinessException($battle->getErrors(), '创建单图弃权的battle失败');
            }
            $battle_id = $battle->id;

            if ($gameId == 1){
                $battleExtLol = new MatchBattleExtCsgo();
                $battleExtLol->setAttribute('id', $battle_id);
                if (!$battleExtLol->save()) {
                    throw new BusinessException($battle->getErrors(), '创建单图弃权的battleExtCSGO失败');
                }
            }

            //创建附属信息
            if ($gameId == 2){
                $battleExtLol = new MatchBattleExtLol();
                $battleExtLol->setAttribute('id', $battle_id);
                if (!$battleExtLol->save()) {
                    throw new BusinessException($battle->getErrors(), '创建单图弃权的battleExtLol失败');
                }
            }
            if ($gameId == 3){
                $battleExtLol = new MatchBattleExtDota2();
                $battleExtLol->setAttribute('id', $battle_id);
                if (!$battleExtLol->save()) {
                    throw new BusinessException($battle->getErrors(), '创建单图弃权的battleExtDota2失败');
                }
            }
            //创建队伍
            // 初始化主队，客队
            $mt1 = new MatchBattleTeam();
            $mt1->setAttributes(
                [
                    'battle_id' => $battle_id,
                    'order' => 1,
                    'team_id' => $teamId1,
                    'score' => $team_score1,
                ]
            );
            if (!$mt1->save()) {
                throw new BusinessException($mt1->getErrors(), "添加battleteam1失败");
            }
            $mt2 = new MatchBattleTeam();
            $mt2->setAttributes(
                [
                    'battle_id' => $battle_id,
                    'order' => 2,
                    'team_id' => $teamId2,
                    'score' => $team_score2,
                ]
            );
            if (!$mt2->save()) {
                throw new BusinessException($mt2->getErrors(), "添加battleteam2失败");
            }
            //关联关系表
            $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$order_id])->one();
            if (!$matchBattleRelation){
                //首先插入关系表
                $matchBattleRelation = new MatchBattleRelation();
                $matchBattleRelation->setAttributes([
                    'match_id' => (int)$matchId,
                    'order' => $order_id,
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                $matchBattleRelation->save();
            }else{
                $matchBattleRelation->setAttributes([
                    'rel_match_id' => (string)0,
                    'rel_battle_id' => (string)0
                ]);
                $matchBattleRelation->save();
            }

            //第二部修改redis
            $redis_name = Consts::MARCH_BATTLE_IS_FORFEIT.':'.$matchId.":".$battle_id;
            $Redis = \Yii::$app->get('redis');
            $Redis->select(env('REDIS_DATABASE'));
            $Redis->set($redis_name,1);
            $Redis->expire($redis_name,Consts::MARCH_BATTLE_REDIS_EXPIRE_TIME);//5天有效期
            //match_battle_info
            $redis_match_battles = Consts::MARCH_BATTLE_INFO.':'.$matchId;
            $Redis->hset($redis_match_battles,'forfeit_'.$order_id,$battle_id.'-'.$order_id);
            //提交事务
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }
    /**
     * @param $battleType 类型 advantage 或者 forfeit
     * @param $dealType 处理方式
     * @param $originId 数据源ID
     * @param $matchId  比赛ID
     * @param $battleOrder 对局ORDER ID
     * @param string $battleId  对局ID
     * @param string $winnerType  获胜方的类型 order 或是 team_id
     * @param string $winnerId 获胜方ID 1或2  或者是team_id
     * @param int $rel_matchId  数据源比赛ID
     * @param int $rel_battleId 数据源的对局ID
     * @return bool|void
     * @throws BusinessException
     */
    public static function dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId='',$winnerType='order',$winnerId='',$rel_matchId = 0,$rel_battleId = 1){
        //判断更新设置 是不是自动更新
        $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchId])->andWhere(['tag_type'=>'update_data'])->andWhere(['update_type'=>3])->asArray()->one();
        //优先判断数据源是不是一样
        if (@$updateInfo['origin_id'] != $originId){
            return false;
        }
        $team_score1 = 0;
        $team_score2 = 0;
        // 每个battle有对局双方，从match获取
        $matchInfo = MatchService::getMergeMatch($matchId);
        $teamId1 = $matchInfo['team_1_id'];
        $teamId2 = $matchInfo['team_2_id'];
        $gameId = $matchInfo['game'];
        $gameType = self::getGameType($gameId);
        //优先把现有的删掉
        if ($battleId){
            BattleService::deleteBattle($gameType, $battleId,1,0,'all');
            if (empty($battleOrder)){
                $MatchBattleInfo = MatchBattle::find()->select('order')->where(['id'=>$battleId])->asArray()->one();
                if ($MatchBattleInfo){
                    $battleOrder = $MatchBattleInfo['order'];
                }else{
                    throw new BusinessException([], "没有battle_id为".$battleId."数据");
                }
            }
        }else{
            if ($battleOrder){
                //根据Order查询battle
                $MatchBattleInfo = MatchBattle::find()->select('id')->where(['order'=>$battleOrder])->andWhere(['match'=>$matchId])->asArray()->one();
                if ($MatchBattleInfo){
                    BattleService::deleteBattle($gameType, $MatchBattleInfo['id'],1,0,'all');
                }
            }else{
                //获取当前比赛的所有对局
                $matchBattleNum = (Int)MatchBattle::find()->where(['match'=>$matchId])->count();
                $battleOrder = $matchBattleNum + 1;
            }
        }
        if ($dealType == 'delete'){
            return true;
        }
        //判断处理类型
        if ($dealType == 'create'){
            //设置地图
            $map_id = MatchService::getMapLists($gameId,'map_id','special',1);
            //开启事务
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                //关联关系
                $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battleOrder])->one();
                if (!$matchBattleRelation){
                    //首先插入关系表
                    $matchBattleRelation = new MatchBattleRelation();
                    $matchBattleRelation->setAttributes([
                        'match_id' => (int)$matchId,
                        'order' => (int)$battleOrder,
                        'rel_match_id' => (string)$rel_matchId,
                        'rel_battle_id' => (string)$rel_battleId
                    ]);
                    if (!$matchBattleRelation->save()) {
                        throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
                    }
                }else{
                    $matchBattleRelation->setAttributes([
                        'rel_match_id' => (string)$rel_matchId,
                        'rel_battle_id' => (string)$rel_battleId
                    ]);
                    $matchBattleRelation->save();
                }
                //['=','is_default_advantage',1],
                $battle = MatchBattle::find()->where(['and',
                    ['=','match',$matchId],
                    ['=','order',$battleOrder]
                ])->one();
                if (!$battle){
                    //创建battle
                    $battle = new MatchBattle();
                }
                if ($winnerType == 'order'){
                    if ($winnerId == 1) {
                        $team_score1 = 1;
                        $team_score2 = 0;
                    }
                    if ($winnerId == 2) {
                        $team_score1 = 0;
                        $team_score2 = 1;
                    }
                }else{
                    if ($winnerId == $teamId1) {
                        $team_score1 = 1;
                        $team_score2 = 0;
                        $winnerId = 1;
                    }
                    if ($winnerId == $teamId2) {
                        $team_score1 = 0;
                        $team_score2 = 1;
                        $winnerId = 2;
                    }
                }
                $is_default_advantage = 2;
                $is_forfeit = 2;
                //判断是默认领先还是弃权
                if ($battleType == 'advantage'){
                    $is_default_advantage = 1;
                }
                if ($battleType =='forfeit'){
                    $is_forfeit = 1;
                }
                $battle->setAttributes([
                    'map' => $map_id,//比赛地图
                    'begin_at' => null,//开始时间
                    'match' => $matchId,//比赛ID
                    'order' => $battleOrder,//对局ID
                    'end_at' => null,//结束时间
                    'status' => 3,//比赛结束
                    'winner' => $winnerId, //获胜方
                    'duration' => null,//对局时长
                    'game' => $gameId,
                    'is_default_advantage' => $is_default_advantage,//默认领先
                    'is_draw' => 2,//默认平局
                    'is_forfeit' => $is_forfeit,//是否弃权
                    'is_battle_detailed' => 2,
                    'deleted_at'=>null,
                    'deleted'=>2
                ]);
                if (!$battle->save()) {
                    throw new BusinessException($battle->getErrors(), '创建默认领先的battle失败');
                }
                $battle_id = $battle->id;

                $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
                TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

                $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battle_id,'match'=>$matchId]];
                TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');

                //创建附属信息
                if ($gameId == 2){
                    $battleExtLol = new MatchBattleExtLol();
                    $battleExtLol->setAttribute('id', $battle_id);
                    if (!$battleExtLol->save()) {
                        throw new BusinessException($battle->getErrors(), '创建默认领先的battleExtLol失败');
                    }
                }
                if ($gameId == 3){
                    $battleExtLol = new MatchBattleExtDota2();
                    $battleExtLol->setAttribute('id', $battle_id);
                    if (!$battleExtLol->save()) {
                        throw new BusinessException($battle->getErrors(), '创建默认领先的battleExtDota2失败');
                    }
                }
                //初始化主队
                $mt1 = MatchBattleTeam::find()->where(['battle_id' => $battle_id])->andWhere(['order'=>1])->one();
                if (!$mt1){
                    $mt1 = new MatchBattleTeam();
                }
                $mt1->setAttributes([
                    'battle_id' => $battle_id,
                    'order' => 1,
                    'team_id' => $teamId1,
                    'score' => $team_score1,
                ]);
                if (!$mt1->save()) {
                    throw new BusinessException($mt1->getErrors(), "添加battleteam1失败");
                }
                //初始化客队
                $mt2 = MatchBattleTeam::find()->where(['battle_id' => $battle_id])->andWhere(['order'=>2])->one();
                if (!$mt2){
                    $mt2 = new MatchBattleTeam();
                }
                $mt2->setAttributes([
                    'battle_id' => $battle_id,
                    'order' => 2,
                    'team_id' => $teamId2,
                    'score' => $team_score2,
                ]);
                if (!$mt2->save()) {
                    throw new BusinessException($mt2->getErrors(), "添加battleteam2失败");
                }
                if ($battleType == 'advantage'){
                    //redis创建一个占位的battle
                    $redis_name = 'match_battle_info:' . $matchId;
                    $Redis = \Yii::$app->get('redis');
                    $Redis->select(env('REDIS_DATABASE'));
                    $Redis->hset($redis_name,'advantage_'.$battleOrder,$battle_id.'-'.$battleOrder);
                }
                if ($battleType == 'forfeit'){
                    //第二部修改redis
                    $redis_name = Consts::MARCH_BATTLE_IS_FORFEIT.':'.$matchId.":".$battle_id;
                    $Redis = \Yii::$app->get('redis');
                    $Redis->select(env('REDIS_DATABASE'));
                    $Redis->set($redis_name,1);
                    $Redis->expire($redis_name,Consts::MARCH_BATTLE_REDIS_EXPIRE_TIME);//5天有效期
                    //match_battle_info
                    $redis_match_battles = Consts::MARCH_BATTLE_INFO.':'.$matchId;
                    $Redis->hset($redis_match_battles,'forfeit_'.$battleOrder,$battle_id.'-'.$battleOrder);
                }
                //提交事务
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
//            throw new BusinessException($e->getMessage(), "添加默认领先battle失败");
                return false;
            }
        }
        return true;
    }
    //根据游戏 ID 获取 GameType
    public static function getGameType($game_id){
        $gameType = '';
        switch ($game_id){
            case 1:
                $gameType = BattleService::GAME_TYPE_CSGO;
                break;
            case 2:
                $gameType = BattleService::GAME_TYPE_LOL;
                break;
            case 3:
                $gameType = BattleService::GAME_TYPE_DOTA;
                break;
            default:
                $gameType = BattleService::GAME_TYPE_CSGO;
                break;
        }
        return $gameType;
    }
    //string 判断是不是空
    public static function checkAndChangeString($check_arr){
        if (empty($check_arr)){
            if ($check_arr === 0){
                $check_arr = (String)0;
            }else{
                $check_arr = null;
            }
        }else{
            $check_arr = (String)$check_arr;
        }
        return $check_arr;
    }
    //int 判断是不是空
    public static function checkAndChangeInt($check_arr){
        if (empty($check_arr)){
            if ($check_arr === 0){
                $check_arr = (Int)0;
            }else{
                $check_arr = null;
            }
        }else{
            $check_arr = (Int)$check_arr;
        }
        return $check_arr;
    }
    //Boolean 判断是不是空
    public static function checkAndChangeBoolean($check_arr){
        if (empty($check_arr)){
            if ($check_arr === false){
                $check_arr = false;
            }else{
                $check_arr = null;
            }
        }else{
            if ($check_arr ==='false'){
                $check_arr = false;
            }else{
                $check_arr = true;
            }
        }
        return $check_arr;
    }

    //查到对应的Unknown 的值
    public static function getMetaDataUnknownId($type){
        $res_id = null;
        switch ($type){
            case 'lol_item':
            case 'lol_items':
                $resInfo = MetadataLolItem::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'lol_champion':
                $resInfo = MetadataLolChampion::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'lol_ability':
                $resInfo = MetadataLolAbility::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'lol_rune':
                $resInfo = MetadataLolRune::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'lol_summoner_spell':
                $resInfo = MetadataLolSummonerSpell::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'dota2_item':
            case 'dota2_items':
                $resInfo = MetadataDota2Item::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'dota2_hero':
                $resInfo = MetadataDota2Hero::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'dota2_ability':
                $resInfo = MetadataDota2Ability::find()->where(['name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
            case 'dota2_talent':
                $resInfo = MetadataDota2Talent::find()->where(['external_name'=>'Unknown'])->asArray()->one();
                $res_id = (Int)@$resInfo['id'];
                break;
        }
        return $res_id;
    }

    //获取socket的列表
    public static function searchMatchLivedDataList($origin_id,$game_id,$rel_match_id){
        $socket_res = [];
        $match_res = MatchLivedDatasTableRelation::find()->select('table_name')
            ->where(['and',
                ['origin_id' => $origin_id],
                ['game_id' => $game_id],
                ['rel_match_id' => $rel_match_id]
            ])->asArray()->all();
        if ($match_res){
            $match_table_num = count($match_res);
            if ($match_table_num == 1){
                $search_sql_table_name = $match_res[0]['table_name'];
                $search_sql = "select id, origin_id,game_id,match_id,round,match_data,type,socket_time,deleted,deleted_at,lived_status,created_at,updated_at from ".$search_sql_table_name." where origin_id = ".$origin_id." and game_id = ".$game_id." and match_id = '".$rel_match_id."' and deleted = 2";
                $match_lived_data_list = \Yii::$app->getDb()->createCommand($search_sql)->queryAll();
                if (!empty($match_lived_data_list)){
                    $socket_res = $match_lived_data_list;
                }
            }else{
                foreach ($match_res as $val){
                    $search_sql_table_name = $val['table_name'];
                    $search_sql = "select id, origin_id,game_id,match_id,round,match_data,type,socket_time,deleted,deleted_at,lived_status,created_at,updated_at from ".$search_sql_table_name." where origin_id = ".$origin_id." and game_id = ".$game_id." and match_id = '".$rel_match_id."' and deleted = 2";
                    $match_lived_data_list = \Yii::$app->getDb()->createCommand($search_sql)->queryAll();
                    if (!empty($match_lived_data_list)){
                        $socket_res = array_merge($socket_res,$match_lived_data_list);
                    }
                }
            }
        }
        return $socket_res;
    }

    //socket录入服务器配置
    public function getSocketGameConfigure($game_type){
        $redis_Name_Str = 'enum:socket_';
        $enum_socket_redis_name = $redis_Name_Str.$game_type;
        $enum_socket_res = $this->redis->hGetAll($enum_socket_redis_name);
        if (empty($enum_socket_res)){
            $socketGameConfigure = \app\modules\match\models\SocketGameConfigure::find()->asArray()->all();
            foreach ($socketGameConfigure as $key => $item){
                if ($item['game_id'] == 1){
                    $csgo_socket_configure = [
                        'mp_maxrounds' => $item['mp_maxrounds'],
                        'mp_overtime_enable' => $item['mp_overtime_enable'],
                        'mp_overtime_maxrounds' => $item['mp_overtime_maxrounds'],
                        'mp_roundtime_defuse' => $item['mp_roundtime_defuse'],
                        'mp_freezetime' => $item['mp_freezetime'],
                        'mp_c4timer' => $item['mp_c4timer']
                    ];
                    $this->redis->hMSet($redis_Name_Str.'csgo',$csgo_socket_configure);
                    $enum_socket_res = $csgo_socket_configure;
                }
                if ($item['game_id'] == 2){
                    $lol_socket_configure = [
                        'inhibitor_reborn_time' => $item['inhibitor_reborn_time'],
                        'rift_herald_first_time' => $item['rift_herald_first_time'],
                        'rift_herald_reborn_time' => $item['rift_herald_reborn_time'],
                        'rift_herald_quit_time' => $item['rift_herald_quit_time'],
                        'rift_herald_max_num' => $item['rift_herald_max_num'],
                        'dragon_first_time' => $item['dragon_first_time'],
                        'dragon_reborn_time' => $item['dragon_reborn_time'],
                        'dragon_quit_num' => $item['dragon_quit_num'],
                        'baron_nashor_first_time' => $item['baron_nashor_first_time'],
                        'baron_nashor_reborn_time' => $item['baron_nashor_reborn_time'],
                        'elder_dragon_first_time' => $item['elder_dragon_first_time'],
                        'elder_dragon_reborn_time' => $item['elder_dragon_reborn_time']
                    ];
                    $this->redis->hMSet($redis_Name_Str.'lol',$lol_socket_configure);
                    $enum_socket_res = $lol_socket_configure;
                }
            }
        }
        return $enum_socket_res;
    }
}