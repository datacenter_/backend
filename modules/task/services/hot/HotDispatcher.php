<?php
/**
 *
 */

namespace app\modules\task\services\hot;

use app\modules\common\services\Consts;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\hot\abios\AbiosDotaMatch;
use app\modules\task\services\hot\abios\AbiosLolMatch;
use app\modules\task\services\hot\bayes\BayesCommonMatch;
use app\modules\task\services\hot\bayes2\BayesplusCommonMatch;
use app\modules\task\services\hot\five\FiveHotCsgoMatch;
use app\modules\task\services\hot\hltv\HltvMatchApi;
use app\modules\task\services\hot\orange\OrangeHotLolMatch;
use app\modules\task\services\hot\pandascore\PandascoreCommonMatch;
use app\modules\task\services\hot\pandascore\PandascoreCsgoMatch;
use app\modules\task\services\hot\pandascore\PandascoreDota2Match;
use app\modules\task\services\hot\pandascore\PandascoreLolMatch;
use app\modules\task\services\hot\radarPurple\RadarPurpleDotaMatch;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;

class HotDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $info = json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $gameId = $info['game_id'];
        $originType = self::getOriginTypeById($originId);
        $gameType = self::getGameTypeById($gameId);
        if(!$gameType){
            $gameType = "common";
        }
        $runClass = self::getRunClass($originType, $gameType);
        $tasks = $runClass::run($tagInfo, $taskInfo);
        return $tasks;
    }


    public static function getRunClass($originType, $gameType)
    {
        $map = [
            Consts::ORIGIN_PANDASCORE =>
                [
                    'lol' => PandascoreLolMatch::class,
                    'csgo' => PandascoreCsgoMatch::class,
                    'dota2' => PandascoreDota2Match::class,
                    'common' => PandascoreCommonMatch::class
                ],
            Consts::ORIGIN_ABIOS =>
                [
                    'lol' => AbiosLolMatch::class,
                    'dota2' => AbiosDotaMatch::class,
                ],
            Consts::ORIGIN_ORANGE =>
                [
                    'lol' => OrangeHotLolMatch::class,
                ],
//            Consts::ORIGIN_FIVE =>
//                [
//                    'csgo' => FiveHotCsgoMatch::class,
//                ],
            Consts::ORIGIN_HLTV =>
                [
                    'csgo' => HltvMatchApi::class,
                ],
            Consts::ORIGIN_BAYES =>
                [
                    'common'=> BayesCommonMatch::class,
                    'lol'=> BayesCommonMatch::class,
                    'csgo'=> BayesCommonMatch::class,
                    'dota2'=> BayesCommonMatch::class,
                ],
            Consts::ORIGIN_BAYES2 =>
                [
                    'common'=> BayesplusCommonMatcH::class,
                    'lol'=> BayesplusCommonMatch::class,
                    'csgo'=> BayesplusCommonMatch::class,
                    'dota2'=> BayesplusCommonMatch::class,
                ],
            Consts::ORIGIN_RADAR_PURPLE =>
                [
                    'dota2'=> RadarPurpleDotaMatch::class,
                ],
        ];

        if (isset($map[$originType]) && isset($map[$originType][$gameType])) {
            return $map[$originType][$gameType];
        }
        throw new BusinessException([], '没有对应的采集类');
    }

    public static function getOriginTypeById($id)
    {
        $mp = [
            2 => Consts::ORIGIN_ABIOS,
            3 => Consts::ORIGIN_PANDASCORE,
            4 => Consts::ORIGIN_ORANGE,
            5 => Consts::ORIGIN_FIVE,
            7 => Consts::ORIGIN_HLTV,
            9=> Consts::ORIGIN_BAYES,
            10=> Consts::ORIGIN_BAYES2,
            11=> Consts::ORIGIN_RADAR_PURPLE
        ];
        return $mp[$id];
    }

    public static function getGameTypeById($id)
    {
        $mp = [
            2 => 'lol',
            3 => 'dota2',
            1 => 'csgo'
        ];
        return $mp[$id];
    }
}