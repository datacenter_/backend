<?php
/**
 *
 */

namespace app\modules\task\services\hot;


interface HotMatchInterface
{
    public static function run($tagInfo, $taskInfo);
}