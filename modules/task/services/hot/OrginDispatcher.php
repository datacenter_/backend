<?php
/**
 *
 */

namespace app\modules\task\services\hot;

use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\wsdata\orange\OrangeDispatcher;
use app\modules\task\services\wsdata\pandascore\PandascoreLolws;
use app\modules\task\services\wsdata\pandascore\PandascoreCsgoWs;

class OrginDispatcher
{
    public static function run($tagInfo,$taskInfo){

        $originType=$tagInfo["origin_type"];
//        $extType=$tagInfo["ext_type"];
        switch ($originType){
            case '5eplay':
                $tasks= FiveHotCsgoMatchWs::run($tagInfo,$taskInfo);
                break;
            default:
                throw new TaskException("can not deal with this type :".$originType);
                break;
        }
        return $tasks;
    }
}