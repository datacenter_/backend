<?php

namespace app\modules\task\services\hot;

use app\modules\task\services\hot\HotBase;
use Throwable;
use Redis;
/**
 * ws到api同步数据的等待列表
 */
class WsToApiSynchron extends HotBase
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $event_id = 0;
    public $event_type = '';
    public $currentPre = "synchron";

    /**
     * @param $origin
     * @param $matchId
     * @param $contentArr
     * @return bool
     */
    public static function rPushSynchron($origin, $matchId, $contentArr)
    {
        $self = new self();
        if ($origin && is_numeric($matchId) && is_array($contentArr) && !empty($contentArr)) {
            $key         = $self->keyStringByOriginAndMatchId($origin, $matchId, [self::KEY_TAG_SYNCHRON]);
            $contentJson = json_encode($contentArr, JSON_UNESCAPED_UNICODE);
            $self->redis->rPush($key, $contentJson);
        } else {
            return false;
        }
    }

    /**
     * @param $origin
     * @param $matchId
     * @return bool|mixed
     */
    public static function getSynchron($origin, $matchId)
    {
        $self = new self();
        if ($origin && is_numeric($matchId)) {
//            $lockKey = $self->keyStringByOriginAndMatchId($origin, $matchId, [self::KEY_TAG_LOCKED]);
//            $nxRes = $self->redis->set($lockKey, true,['nx','ex'=>10]);

//            if ($nxRes){
                $key  = $self->keyStringByOriginAndMatchId($origin, $matchId, [self::KEY_TAG_SYNCHRON]);
                $list = $self->redis->lRange($key, 0, -1);
                if (!empty($list)) {
                    $countList       = count($list);
                    $countRoundEnd = 0;
                    $countBattleEnd = 0;
                    $countBattleStart = 0;
                    $countBattleReloaded = 0;
                    $battleEndContent = '';
                    $battleReloadContent = '';
                    $battleStartContent = '';
                    $roundEndContent = '';
                    $lastContent = '';
                    $lastType = '';
                    foreach ($list as $k => $v) {
                        $vArr       = json_decode($v, true);
                        if ($vArr['event_id']==57555460){
                            $battleStartContent = $v;
                            $res['task'] = $battleStartContent;
                            $res['event_type'] = 'battle';
                            return $res;
                        }
                        $type       = $vArr['event_type'];
                        $updateType = $vArr['updateType'];
                        $countK     = $k + 1;
                        if ($type == 'battle_end'){
                            $countBattleEnd++;
                            $battleEndContent = $v;
                        }
                        if ($type == 'battle_reloaded'){
                            $countBattleReloaded++;
                            $battleReloadContent = $v;
                        }
                        if ($type == 'round_end') {
                            $countRoundEnd++;
                            $roundEndContent = $v;
                        }
                        if ($type == 'battle_start'){
                            $countBattleStart++;
                            $battleStartContent = $v;
                        }
                        if ($countK == $countList) {
                            //最后一条
                            $lastContent = $v;
                            $lastType = $type;
                        }
                    }
                    foreach ($list as $k=>$v){
                        //消费掉当时获取的所有task
                        $self->redis->lPop($key);
                    }
                    //1.优先处理battle_end
                    if ($countBattleEnd>0){
                        $res['task'] = $battleEndContent;
                        $res['event_type'] = 'battle_end';
                        return $res;
                    }
                    //2.优先处理battle_start
                    if ($countBattleStart>0){
                        $res['task'] = $battleStartContent;
                        $res['event_type'] = 'battle_start';
                        return $res;
                    }
                    //3.优先处理battle_reload
                    if ($countBattleReloaded>0){
                        $res['task'] = $battleReloadContent;
                        $res['event_type'] = 'battle_reload';
                        return $res;
                    }
                    //4.处理round_end
                    if ($countRoundEnd>0){
                        $res['task'] = $roundEndContent;
                        $res['event_type'] = 'round_end';
                        $res['round_end_count'] = $countRoundEnd;
                        return $res;
                    }
                    //5.正常的事件
                    if ($lastContent){
                        $res['task'] = $lastContent;
                        $res['event_type'] = $lastType;
                        return $res;
                    }
                    //异常情况
                    return false;
                } else {
                    return false;
                }
            }
//        }else{
//            return false;
//        }
    }

}