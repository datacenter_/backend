<?php
/**
 *
 */
namespace app\modules\task\services\hot\abios;

use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\org\models\Player;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\rest\exceptions\BusinessException;

class AbiosDotaMatch extends AbiosHotBase implements HotMatchInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $timeStart=date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $hotId = $info['id'];
        $matchId=$info['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,2);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        $matchInfo=Match::find()->where(['id'=>$matchId])->asArray()->one();
        //$restInfos = json_decode($a,true);
        $restInfos = self::getInfos($relIdentityId,$matchInfo);
        if (isset($restInfos['match']) && count($restInfos['battles']) > 0){
            // 这里传matchInfo，用来判断主队客队
            $infoFormat = self::conversion($restInfos,$matchInfo);
            $infoFormatAddBindingInfo=self::addBindingInfo($infoFormat,$matchInfo);
            $timeEnd=date('Y-m-d H:i:s');
            $extInfo=[
                'time_begin'=>$timeStart,
                'time_end'=>$timeEnd,
                'task_id'=>$taskInfo['id'],
                'match_id'=>$matchId,
            ];
            //添加绑定信息，转义,主要出现在battle的详情中
            self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
//            // 更新到数据库
            $refreshRes = self::refreshInfo($matchId,$infoFormatAddBindingInfo,$refreshType);
            // 更新hotdata状态
            self::refreshHotDataStatusByMatchInfo($hotId,$refreshRes);
        }
        return true;
    }
    //更新信息
    public static function refreshInfo($matchId, $info,$refreshType)
    {
        $battle_last_status = 1;
        $match_status = $info['match']['status'];
        if ($refreshType == 'refresh'){
            foreach ($info['battles'] as $key => $battle) {
                // 根据battleId刷新对应的battle信息
                self::refreshBattle($matchId, $battle, $key,$refreshType);
            }
        }else{
            //这里面有特殊的情况 默认领先和单图弃权
            foreach ($info['battles'] as $key => $battle) {
                $battle_last_status = $battle['base']['status'];
                // 根据battleId刷新对应的battle信息
                self::refreshBattle($matchId, $battle, $key,$refreshType);
            }
        }
        //是否比赛完成
        $battle_complete = false;
        if ($match_status == 3 && $battle_last_status ==3){
            $battle_complete = true;
        }
        //更新match层信息
        self::setMatch($matchId, $info['match']);
        return $battle_complete;
    }
    //更新battle
    private static function refreshBattle($matchId, $battle, $key=0,$refreshType = '')
    {
        //判断有没有对局时长 没有就不创建battle
        if (!isset($battle['base']['duration'])){
            return true;
        }
        // 找到对应的battleId，更新battle
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
        }
        $battleId=$battleBase['id'];

        BattleService::setBattleInfo($battleId,'dota',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'dota',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId,'dota',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'dota',$battle['static'],BattleService::DATA_TYPE_STATICS);
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'dota',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        //判断关联关系
        self::setMatchBattleRelation($matchId,$battleId,$battle);
    }
    //关联关系
    private static function setMatchBattleRelation($matchId,$battleId,$battle){
        //插入关联关系
        $rel_matchId = @$battle['base']['rel_match_id'];
        $rel_battleId = @$battle['base']['rel_battle_id'];
        $battle_order = $battle['base']['order'];
        $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battle_order])->one();
        if (!$matchBattleRelation){
            //首先插入关系表
            $matchBattleRelation = new MatchBattleRelation();
            $matchBattleRelation->setAttributes([
                'match_id' => (int)$matchId,
                'order' => $battle_order,
                'rel_match_id' => (string)$rel_matchId,
                'rel_battle_id' => (string)$rel_battleId
            ]);
            if (!$matchBattleRelation->save()) {
                throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
            }
        }
        //设置redis 关系
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis->hset($redis_name,$rel_battleId,$battleId.'-'.$battle_order);
    }
    //更新比赛信息
    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    //获取信息
    public static function getInfos($relIdentityId,$matchInfo)
    {
        $matchId = $matchInfo['id'];
        $gameId = $matchInfo['game'];
        //设置redis 关系
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redis_name = 'match_info_abios_team_relation:'.$matchId."_".$gameId;
        //查询
        $action = '/v3/series/' . $relIdentityId;
        $params = [];
        $matchData = self::getRestInfo($action, $params);
        //循环matchData的participants 来获取队伍ID
        foreach ($matchData['participants'] as $key => $part){
            $match_roster = $part['roster']['id'];
            $rel_team_id = $Redis->hget($redis_name,$match_roster);
            if ($rel_team_id){
                $matchData['participants'][$key]['rel_team_id'] = $rel_team_id;
            }else{
                $roster_action = "/v3/rosters/{$match_roster}";
                $rosterInfo = self::getRestInfo($roster_action, []);
                $matchData['participants'][$key]['rel_team_id'] = @$rosterInfo['team']['id'];
                $Redis->hset($redis_name,$match_roster,@$rosterInfo['team']['id']);
            }
        }
        $battles = [];
        foreach ($matchData['matches'] as $k => $battle) {
            $action = "/v3/matches/{$battle['id']}";
            $battleBase = self::getRestInfo($action, []);
            if ($battleBase['lifecycle'] == 'deleted'){
                continue;
            }
            foreach ($battleBase['participants'] as $key_p => $val_p){
                $battle_roster = $val_p['roster']['id'];
                $battle_rel_team_id = $Redis->hget($redis_name,$battle_roster);
                if ($battle_rel_team_id){
                    $battleBase['participants'][$key_p]['rel_team_id'] = $battle_rel_team_id;
                }else {
                    $roster_action_b = "/v3/rosters/{$battle_roster}";
                    $rosterInfo_b = self::getRestInfo($roster_action_b, []);
                    $battleBase['participants'][$key_p]['rel_team_id'] = @$rosterInfo_b['team']['id'];
                    $Redis->hset($redis_name,$battle_roster,@$rosterInfo_b['team']['id']);
                }
            }
            $battles[$battle['id']]['base'] = $battleBase;
            $action = "/v3/matches/{$battle['id']}/postgame/server/summary";
            $battleDetail = self::getRestInfo($action, []);
            $battles[$battle['id']]['detail'] = $battleDetail;
        }
        return [
            'match' => $matchData,
            'battles' => $battles
        ];
    }
    //处理数据
    public static function conversion($restInfo,$matchInfo)
    {
        // 转化match
        $formatData = [
            'match' => '',
            'battles' => [],
        ];
        // 比赛数据
        $detailInfoConfig = [
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['start']));
            },
            'end_at' => function ($info) {
                if ($info['end']){
                    return date('Y-m-d H:i:s', strtotime($info['end']));
                }else{
                    return null;
                }
            },
            'team_1_score_old' => function ($info) {
                return $info['participants'][0]['score'];
            },
            'team1_id' => function ($info) {
                return $info['participants'][0]['rel_team_id'];
            },
            'team1_roster_id' => function ($info) {
                return $info['participants'][0]['roster']['id'];
            },
            'team_2_score_old' => function ($info) {
                return $info['participants'][1]['score'];
            },
            'team2_id' => function ($info) {
                return $info['participants'][1]['rel_team_id'];
            },
            'team2_roster_id' => function ($info) {
                return $info['participants'][1]['roster']['id'];
            },
            'is_draw' => function ($info) {
                if ($info['lifecycle'] == 'over-draw'){
                    return 1;
                }else{
                    return 2;
                }
            },
            'is_forfeit' => function ($info) {
                if ($info['lifecycle'] == 'over-forfeited'){
                    return 1;
                }else{
                    return 2;
                }
            },
            'status' => function($info){
                return Common::getAbiosMatchStatus($info['lifecycle']);
            },
            'winner' => function($info){
                if ($info['participants'][0]['winner']){
                    return $info['participants'][0]['rel_team_id'];
                }elseif($info['participants'][1]['winner']) {
                    return $info['participants'][1]['rel_team_id'];
                }else{
                    return null;
                }
            }
        ];
        $matchDetailInfo = Mapping::transformation($detailInfoConfig, $restInfo['match']);
        $formatData['match'] = $matchDetailInfo;
        if (count($restInfo['battles']) > 0){
            foreach ($restInfo['battles'] as $key => $restBattle) {
                // 格式化battle信息，这里需要一些team的信息
                $formatData['battles'][] = self::conversionBattle($restBattle,$restInfo,$matchInfo);
            }
        }
        return $formatData;
    }
    //转换数据
    public static function addBindingInfo($infoFormat,$matchInfo)
    {
        $teamInfoRes = [];
        $teamInfoRes[$infoFormat['match']['team1_id']] = null;
        $teamInfoRes[$infoFormat['match']['team2_id']] = null;
        //转化队伍
        $team1trueID = self::getTeamOrderByTeamRelId($infoFormat['match']['team1_id'],$matchInfo);
        if ($team1trueID == 2){
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_2_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_1_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 2;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 1;
        }else{
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_1_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_2_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 1;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 2;
        }
        //获胜者ID
        if (isset($infoFormat['match']['winner']) && $infoFormat['match']['winner']){
            $infoFormat['match']['winner'] = $teamInfoRes[$infoFormat['match']['winner']];
        }
        //获取所有的道具
        $allItems = MetadataDota2Item::find()->where(['deleted'=>2])->asArray()->all();
        $allItemsRes = @array_column($allItems, null, 'id');
//        $infoFormat['match']['map_info'] = json_encode([1]);
        foreach($infoFormat['battles'] as &$battle){
            //base TODO 地图
            $battle['base']['map'] = 1;  //数据源没该数据，
            //判断有没默认领先的battle
            if ($battle['base']['is_default_advantage'] == 1){
                $infoFormat['match']['default_advantage'] = $teamInfoRes[$battle['base']['winner']];
            }
            $battle_winner = null;
            if (isset($battle['base']['winner']) && $battle['base']['winner']){
                $battle['base']['winner'] = $teamInfoRes[$battle['base']['winner']];
                $battle_winner = $battle['base']['winner'];
            }
            //转化team
            $team_array = [];
            foreach($battle['static'] as &$static){
                if ($battle_winner == $static['order']){
                    $static['score'] = 1;
                }else{
                    $static['score'] = 0;
                }
                if ($battle['base']['is_forfeit'] != 1){
                    $static['team_id']= self::getMainIdByRelIdentityId('team', $static['rel_team_id'], 2);
                    $team_array[$static['rel_team_id']] = @$static['team_id'];
                }
            }
            // 更新选手中的选手id，战队id，item中的装备

            if (isset($battle['players']) && count($battle['players']) > 0) {
                $team1_seed = $team2_seed = 1;
                foreach ($battle['players'] as $key => &$player) {
                    $player['game'] = 3;
                    $player['match'] = @$matchInfo['id'];
                    $player['team_id'] = @$team_array[$player['rel_team_id']];
                    $player['hero'] = self::getMainIdByRelIdentityIdOrUnknown('dota2_hero', $player['hero'], 2);
                    $player['player_id'] = self::getMainIdByRelIdentityId('player', $player['player_id'], 2);
                    if ($player['player_id']){
                        //获取选手的昵称
                        $player_detail = Player::find()->select('nick_name')->where(['id'=>$player['player_id']])->asArray()->one();
                        $player['nick_name'] = @$player_detail['nick_name'];
                        $player['role'] = @$player_detail['role'];
                    }
                    $player['seed'] = $player['seed'] + 1;
                    //处理角色
//                    $abiosPlayerRole = $player['role'] + 1;//角色
//                    $player_role = Common::getDota2PositionByString(Consts::ORIGIN_ABIOS, $abiosPlayerRole);
//                    $player['role'] = $player_role;  //角色
//                    if ($player['team_order'] == 1){
//                        $player['seed'] = $team1_seed;//选手排序
//                        $team1_seed ++;
//                    }
//                    if ($player['team_order'] == 2){
//                        $player['seed'] = $team2_seed;//选手排序
//                        $team2_seed ++;
//                    }
                    $player['order'] = $key + 1;//排序
                    //$player['lane'] = Common::getAbiosLaneIDByRoleName($abiosPlayerRole);  //分路id
                    //处理item 道具装备 TODO
                    $player['items'] = self::conversionDota2PlayerItems($allItemsRes,$player['items_inventory'],$player['items_backpack'],$player['items_stash']);//,$player['items_buffs']);
                    unset($player['items_inventory']);//物品栏
                    unset($player['items_backpack']);//背包
                    unset($player['items_stash']);//储藏处
                    //下面会用到
                    $team_array[$player['rel_team_id']] = $player['team_id'];
                }
            }

            //转化ban_pick
            if(count($battle['ban_pick'])>0){
                foreach ($battle['ban_pick'] as &$ban_pick_val){
                    $ban_pick_val['hero'] = self::getMainIdByRelIdentityIdOrUnknown('dota2_hero',$ban_pick_val['hero'],2);
                    $ban_pick_val['team'] = @$team_array[$ban_pick_val['team']];
                }
            }
        }
        return $infoFormat;
    }
    //处理battle
    private static function conversionBattle($restBattleInfo,$restInfo,$matchInfo)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];

        $battleInfoConfig = [
            'rel_battle_id' => 'id',
            'rel_match_id' => function ($info) {
                return $info['series']['id'];
            },
            'map' => function ($info) {
                return $info['map']['id'];
            },
            'is_forfeit' => function ($info) {
                if ($info['lifecycle'] == 'over-forfeited'){
                    return 1;
                }else{
                    return 2;
                }
            },
            'is_draw' => function ($info) {
                if ($info['lifecycle'] == 'over-draw'){
                    return 1;
                }else{
                    return 2;
                }
            },
            'is_default_advantage' => function ($info) {
                if ($info['lifecycle'] == 'advantage'){
                    return 1;
                }else{
                    return 2;
                }
            },
            'status' => function ($info) {
                return $info['lifecycle'] == 'over' ? 3 : 2;
            },
            'order' => 'order',
            'winner' => function($info){
                if($info['participants'][0]['winner']){
                    return $info['participants'][0]['rel_team_id'];
                }
                if($info['participants'][1]['winner']){
                    return $info['participants'][1]['rel_team_id'];
                }
            },
            'team1_id' => function ($info) {
                return $info['participants'][0]['rel_team_id'];
            },
            'rel_team1_id' => function ($info) {
                return $info['participants'][0]['roster']['id'];
            },
            'team2_id' => function ($info) {
                return $info['participants'][1]['rel_team_id'];
            },
            'rel_team2_id' => function ($info) {
                return $info['participants'][1]['roster']['id'];
            }
        ];
        $battleBaseAtt = Mapping::transformation($battleInfoConfig, $restBattleInfo['base']);
        $battle_team_roster_array = [
            $battleBaseAtt['rel_team1_id'] => $battleBaseAtt['team1_id'],
            $battleBaseAtt['rel_team2_id'] => $battleBaseAtt['team2_id']
        ];
        //获取比赛时长
        $duration_minute = 1;
        if (isset($restBattleInfo['detail']['length']['milliseconds'])){
            $duration = @round($restBattleInfo['detail']['length']['milliseconds']/1000);
            $battleBaseAtt['duration']=(string)$duration;//转换下格式
            $duration_minute = round($restBattleInfo['detail']['length']['milliseconds']/1000/60,2);
        }
        //base
        $battleBaseAtt['map'] = 1;  //根据数据源ID 获取平台的ID
        $battleBaseAtt['is_battle_detailed'] = 1;
        $battleBaseAtt['flag'] = 1;
        $battleBaseAtt['deleted'] = 2;
        $battleBaseAtt['deleted_at'] = null;
        $battleBaseAtt['game'] = 3;
        // 获取battle详情,这里用来设置id
        $battleInfo['base'] = $battleBaseAtt;
        //处理其他的
        $teamStatic = $players = [];
        $battleDetail = [];
        $battleDetail['is_confirmed'] = 1;
        $playerOrderKey = 0;
        $players = [];
        //处理战队
        $teamInfosRest = $restBattleInfo['detail']['teams'];
        //判断是不是弃权
        if ($battleBaseAtt['is_forfeit'] == 1 || $battleBaseAtt['is_default_advantage'] == 1){
            $battleInfo['base']['is_battle_detailed'] = 2;
            $battleInfo['players'] = $players;
            $teamStatic[1]['team_id'] = $matchInfo['team_1_id'];
            $teamStatic[1]['order']= 1;
            $teamStatic[2]['team_id'] = $matchInfo['team_2_id'];
            $teamStatic[2]['order']= 2;
            //计算团队的信息
            $battleInfo['static'] = $teamStatic;
            $battleInfo['detail'] = $battleDetail;
            $battleInfo['ban_pick'] = [];
        }else {
            foreach ($teamInfosRest as $key_t => $team) {
                $faction = '';
                $rel_team_id = @$battle_team_roster_array[$team['roster']['id']];
                $team_order = self::getTeamOrderByTeamRelId($rel_team_id, $matchInfo, 2);
                //用于处理选手
                $teamStatic[$team_order]['order'] = $team_order;
                $teamStatic[$team_order]['rel_team_id'] = $rel_team_id;
                $teamStatic[$team_order]['rel_identity_id'] = $rel_team_id;
                if ($team['faction']['id'] == 5448) {
                    $teamStatic[$team_order]['faction'] = 'dire';
                    $faction = 'dire';
                }
                if ($team['faction']['id'] == 5449) {
                    $teamStatic[$team_order]['faction'] = 'radiant';
                    $faction = 'radiant';
                }
                $teamStatic[$team_order]['kills'] = $team['score'];
                $team_gold_earned = 0;
                $team_experience = 0;
                $team_roshan = 0;
                //选手
                $player_damage_to_heroes_all = $player_damage_taken_all = 0;
                //循环选手
                foreach ($team['players'] as $key_p => $playerRest) {
                    $playerConfig = [
                        'seed'=>'ui_index',
                        //'faction' => "faction",
                        //'role' => 'ui_index',
                        //lane
                        "player_id" => 'id',//选手id
                        "hero" => function ($params) {
                            return $params['hero']['id'];
                        },//英雄
                        "level" => "level",//等级
                        //"is_alive",//存活状态
                        //ultimate_cd
                        //coordinate
                        "kills" => function ($params) {
                            return $params['kills']['total'];
                        },//击杀
                        //kill_combos[
                        //double_kill
                        //triple_kill
                        //quadra_kill
                        //penta_kill
                        "largest_multi_kill" => function ($params) {
                            if ($params['multi_kills']) {
                                return $params['multi_kills'][0]['nr_kills'];
                            } else {
                                return 0;
                            }
                        },  //最大多杀
                        "largest_killing_spree" => function ($params) {
                            if ($params['kill_streaks']) {
                                return $params['kill_streaks'][0];
                            } else {
                                return 0;
                            }
                        }, //最大连杀
                        "deaths" => function ($params) {
                            return $params['deaths']['total'];
                        },//死亡
                        "assists" => function ($params) {
                            return $params['assists']['total'];
                        },//助攻
                        //kda 下面计算
                        //participation  下面计算
                        "last_hits" => function ($params) {
                            return $params['creeps']['faction']['kills']['total'] + $params['creeps']['neutrals']['kills']['total'];
                        },//补刀
                        "lane_creep_kills" => function ($params) {
                            return $params['creeps']['faction']['kills']['total'];
                        },//补刀详情-小兵击杀数
                        "neutral_creep_kills" => function ($params) {
                            return $params['creeps']['neutrals']['kills']['total'];
                        },//补刀详情-野怪击杀数
                        //lhpm 下面计算
                        "denies" => function ($params) {
                            return $params['creeps']['faction']['denies']['total'];
                        },//反补
                        "net_worth" => function ($params) {
                            return $params['gold']['net_worth'];
                        },
                        //"gold_earned",//下面计算//金币获取
                        //"gold_spent",//已花费金币
                        //"gold_remaining",//剩余金币
                        "gpm" => function ($params) {
                            return $params['gold']['per_minute'];
                        },//分均金钱
                        //experience 下面计算
                        "xpm" => function ($params) {
                            return $params['experience']['per_minute'];
                        },//分均经验
                        "damage_to_heroes" => function ($params) {
                            return $params['damage']['hero']['given']['by_hero']['total'];
                        },//对英雄伤害
                        //dpm_to_heroes 对英雄分均伤害  下面计算
                        //damage_percent_to_heroes  对英雄伤害占比 循环外计算
                        "damage_taken" => function ($params) {
                            return $params['damage']['hero']['taken']['from_hero']['total'];
                        },//承受伤害
                        //dtpm 下面计算
                        //damage_taken_percent 承伤占比 循环外计算
                        "damage_by_hero_hp_removal" => function ($params) {
                            return $params['damage']['hero']['given']['by_hero']['hp_removal'];
                        },//对英雄生命移除
                        "damage_by_hero_magical_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_hero']['magical'];
                        },//对英雄魔法伤害
                        "damage_by_hero_physical_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_hero']['physical'];
                        },//对英雄物理伤害
                        "damage_by_hero_pure_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_hero']['pure'];
                        },//纯粹伤害
                        "damage_by_mobs_hp_removal" => function ($params) {
                            return $params['damage']['hero']['given']['by_mobs']['hp_removal'];
                        },//召唤物生命移除
                        "damage_by_hero_magical_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_mobs']['magical'];
                        },//召唤物魔法伤害
                        "damage_by_hero_physical_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_mobs']['physical'];
                        },//召唤物物理伤害
                        "damage_by_hero_pure_dmg" => function ($params) {
                            return $params['damage']['hero']['given']['by_mobs']['pure'];
                        },//召唤物纯粹伤害
                        "damage_to_towers" => function ($params) {
                            return $params['damage']['structures']['towers']['given']['by_hero']['total'];
                        },//对防御塔伤害
                        "damage_from_heroes_hp_removal" => function ($params) {
                            return $params['damage']['hero']['taken']['from_hero']['hp_removal'];
                        },//来自英雄的生命移除
                        "damage_from_heroes_magical_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_hero']['magical'];
                        },//来自英雄的魔法伤害
                        "damage_from_heroes_physical_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_hero']['physical'];
                        },//来自英雄的物理伤害
                        "damage_from_heroes_pure_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_hero']['pure'];
                        },//来自英雄的纯粹伤害
                        "damage_from_mobs_hp_removal" => function ($params) {
                            return $params['damage']['hero']['taken']['from_mobs']['hp_removal'];
                        },//来自召唤物生命移除
                        "damage_from_mobs_magical_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_mobs']['magical'];
                        },//来自召唤物魔法伤害
                        "damage_from_mobs_physical_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_mobs']['physical'];
                        },//来自召唤物物理伤害
                        "damage_from_mobs_pure_dmg" => function ($params) {
                            return $params['damage']['hero']['taken']['from_mobs']['pure'];
                        },//来自召唤物纯粹伤害
                        //damage_conversion_rate 伤害转化率 下面计算
                        'total_heal' => function ($params) {
                            return $params['healing']['hero']['given']['by_hero']['total'] + $params['healing']['hero']['given']['by_mobs']['total'];
                        },//总治疗量
                        //total_crowd_control_time //总控制时长
                        //wards_purchased //买眼
                        //wards_placed //插眼
                        //wards_kills //排眼
                        //wards_detail 守卫详情
                        //"observer_wards_purchased",//购买侦查守卫
                        //"observer_wards_placed",//放置侦查守卫
                        //"observer_wards_kills",//摧毁侦查守卫
                        //"sentry_wards_purchased",//购买岗哨守卫
                        //"sentry_wards_placed",//放置岗哨守卫
                        //"sentry_wards_kills",//摧毁岗哨守卫
                        "camps_stacked" => function ($params) {
                            return $params['creeps']['neutrals']['stacks']['total'];
                        },//堆野数
                        'runes_detail_double_damage_runes' => function ($params) {
                            return $params['runes']['double_damage'];
                        },//双倍伤害神符
                        'runes_detail_haste_runes' => function ($params) {
                            return $params['runes']['haste'];
                        }, //极速神符
                        'runes_detail_illusion_runes' => function ($params) {
                            return $params['runes']['illusion'];
                        },  //幻象神符
                        'runes_detail_invisibility_runes' => function ($params) {
                            return $params['runes']['invisibility'];
                        },  //隐身神符
                        'runes_detail_regeneration_runes' => function ($params) {
                            return $params['runes']['regeneration'];
                        },  //恢复神符
                        'runes_detail_bounty_runes' => function ($params) {
                            return $params['runes']['bounty'];
                        },  //赏金神符
                        'runes_detail_arcane_runes' => function ($params) {
                            return $params['runes']['arcane'];
                        },  //奥术神符
                        "items_inventory" => function ($params) {
                            return $params['items']['hero']['inventory']['items'];
                        },//道具装备
                        "items_backpack" => function ($params) {
                            return $params['items']['hero']['backpack']['items'];
                        },//背包
                        "items_stash" => function ($params) {
                            return $params['items']['stash']['items'];
                        },//储藏处
                        //                    "items_buffs" => function ($params) {
                        //                        return $params['items']['hero']['backpack']['items'];
                        //                    },//增益
                        //"abilities",//技能升级时间线
                        'rel_identity_id' => 'id',//原始ID
                        'rel_id' => 'id'
                    ];
                    $player = Mapping::transformation($playerConfig, $playerRest);
                    $player['order'] = $key_p;
                    $player['rel_identity_id'] = (string)$player['rel_identity_id'];
                    $player['faction'] = $faction;
                    $player['rel_team_id'] = $rel_team_id;
                    $player['team_order'] = $team_order;
                    //以下需要计算得属性
                    //kda
                    $deaths = $player['deaths'] > 0 ? $player['deaths'] : 1;
                    $player['kda'] = (String)round((($player['kills'] + $player['assists']) / $deaths),2);
                    // participation 参团率
                    if ($team['score'] > 0) {
                        $player['participation'] = (String)round((($player['kills'] + $player['assists']) / $team['score']),4);
                    } else {
                        $player['participation'] = null;
                    }
                    //分均补刀 lhpm
                    $player['lhpm'] = (String)round(($player['last_hits'] / $duration_minute),2);
                    //金钱
                    $player['gold_earned'] = (Int)round($player['gpm'] * $duration_minute);
                    $team_gold_earned += $player['gold_earned'];
                    //经验
                    $player['experience'] = (int)round($player['xpm'] * $duration_minute);
                    $team_experience += $player['experience'];
                    //对英雄分均伤害  和  dtpm 分均承受伤害
                    //对英雄伤害
                    if (isset($player['damage_to_heroes'])) {
                        $player['dpm_to_heroes'] = (String)round(($player['damage_to_heroes'] / $duration_minute),2);
                    } else {
                        $player['dpm_to_heroes'] = null;
                    }
                    if (isset($player['damage_taken'])) {
                        $player['dtpm'] = (String)round(($player['damage_taken'] / $duration_minute),2);
                    } else {
                        $player['dtpm'] = null;
                    }
                    //damage_conversion_rate 伤害转化率
                    if (isset($player['gold_earned']) && $player['gold_earned'] && isset($player['damage_to_heroes'])) {
                        $player['damage_conversion_rate'] = (String)round(($player['damage_to_heroes'] / $player['gold_earned']),4);
                    } else {
                        $player['damage_conversion_rate'] = '';
                    }
                    //肉山
                    $team_roshan += $playerRest['creeps']['neutrals']['kills']['roshan'];
                    //damage_taken_percent
                    $player_damage_to_heroes_all += $player['damage_to_heroes'];
                    $player_damage_taken_all += $player['damage_taken'];
                    //最终结果
                    $players[$playerOrderKey] = $player;
                    $playerOrderKey++;
                }
                //记录队伍的信息
                $teamStatic[$team_order]['player_damage_to_heroes_all'] = $player_damage_to_heroes_all;
                $teamStatic[$team_order]['player_damage_taken_all'] = $player_damage_taken_all;
                //team 信息
                $teamStatic[$team_order]['gold_earned'] = (int)$team_gold_earned;
                $teamStatic[$team_order]['experience'] = (int)$team_experience;
                $teamStatic[$team_order]['tower_kills'] = 0;
                $teamStatic[$team_order]['roshan_kills'] = $team_roshan;
                //兵营状态 现在是主客队是反着的，要在下一步转化的时候调整过来
                $teamBuldingStatusBarracksInfo = self::conversionRestBuldingStatusBarracks(
                    $team['structures_status']['barracks']
                );
                if ($teamBuldingStatusBarracksInfo) {
                    $teamStatic[$team_order]['building_status_barracks'] = $teamBuldingStatusBarracksInfo['building_status_barracks_detail'];
                    $teamStatic[$team_order]['melee_barrack_kills'] = $teamBuldingStatusBarracksInfo['melee_barrack_kills_num'];
                    $teamStatic[$team_order]['ranged_barrack_kills'] = $teamBuldingStatusBarracksInfo['ranged_barrack_kills_num'];
                } else {
                    $teamStatic[$team_order]['building_status_barracks'] = null;
                    $teamStatic[$team_order]['melee_barrack_kills'] = null;
                    $teamStatic[$team_order]['ranged_barrack_kills'] = null;
                }
                //防御塔状态  tower_kills 现在是主客队是反着的，要在下一步转化的时候调整过来
                $teamBuldingStatusTowers = self::conversionRestBuldingStatusTowers(
                    $team['structures_status']['towers']
                );
                if ($teamBuldingStatusTowers) {
                    $teamStatic[$team_order]['building_status_towers'] = $teamBuldingStatusTowers['building_status_towers_detail'];
                    $teamStatic[$team_order]['tower_kills'] = $teamBuldingStatusTowers['tower_kills_num'];
                } else {
                    $teamStatic[$team_order]['building_status_towers'] = null;
                    $teamStatic[$team_order]['tower_kills'] = 0;
                }
            }
            //近战兵营状态 队伍互换
            if (isset($teamStatic[1]['melee_barrack_kills']) && isset($teamStatic[2]['melee_barrack_kills'])) {
                $melee_barrack_kills_tmp = $teamStatic[1]['melee_barrack_kills'];
                $teamStatic[1]['melee_barrack_kills'] = $teamStatic[2]['melee_barrack_kills'];
                $teamStatic[2]['melee_barrack_kills'] = $melee_barrack_kills_tmp;
            }
            //远战兵营状态 队伍互换
            if (isset($teamStatic[1]['ranged_barrack_kills']) && isset($teamStatic[2]['ranged_barrack_kills'])) {
                $ranged_barrack_kills_tmp = $teamStatic[1]['ranged_barrack_kills'];
                $teamStatic[1]['ranged_barrack_kills'] = $teamStatic[2]['ranged_barrack_kills'];
                $teamStatic[2]['ranged_barrack_kills'] = $ranged_barrack_kills_tmp;
            }
            //这里转换tower_kills
            if (isset($teamStatic[1]['tower_kills']) && isset($teamStatic[2]['tower_kills'])) {
                $tower_kills_tmp = $teamStatic[1]['tower_kills'];
                $teamStatic[1]['tower_kills'] = $teamStatic[2]['tower_kills'];
                $teamStatic[2]['tower_kills'] = $tower_kills_tmp;
            }
            //pick
            $pick_array = [];
            //计算对英雄伤害占比damage_percent_to_heroes 和 承伤占比 damage_taken_percent
            foreach ($players as $key => $item) {
                //对英雄伤害占比
                $team_damage_to_heroes_all = $teamStatic[$item['team_order']]['player_damage_to_heroes_all'];
                $team_damage_to_heroes_all_chushu = empty($team_damage_to_heroes_all) ? 1 : $team_damage_to_heroes_all;
                $players[$key]['damage_percent_to_champions'] = (String)round(($item['damage_to_heroes'] / $team_damage_to_heroes_all_chushu),4);
                //承伤占比
                $team_damage_taken_all = $teamStatic[$item['team_order']]['player_damage_taken_all'];
                $team_damage_taken_all_chushu = empty($team_damage_taken_all) ? 1 : $team_damage_taken_all;
                $players[$key]['damage_taken_percent'] = (String)round(($item['damage_taken'] / $team_damage_taken_all_chushu),4);
                //pick
                $pick_array[$key]['hero'] = @$item['hero'];
                $pick_array[$key]['team'] = @$item['rel_team_id'];
                $pick_array[$key]['type'] = 2;
                $pick_array[$key]['order'] = null;
            }
            $battleInfo['players'] = $players;
            //计算团队的信息
            $battleInfo['static'] = $teamStatic;
            //pick
            $battleInfo['ban_pick'] = $pick_array;
            //detail
            $battleInfo['detail'] = $battleDetail;
        }
        return $battleInfo;
    }
    //兵营状态
    public static function conversionRestBuldingStatusBarracks($building_status_barracks){
        if (empty($building_status_barracks)){
            return null;
        }
        $result = [];
        $melee_barrack_kills_num = 0;//近战兵营
        $ranged_barrack_kills_num = 0;//远程兵营
        $building_status_barracks_detail = [];
        //{"bottom_melee":true,"bottom_ranged":true,"middle_melee":true,"middle_ranged":true,"top_melee":true,"top_ranged":true}
        foreach ($building_status_barracks as $key => $item){
            $boolean_val = $item['standing'] ? 1 : 2;
            switch ($key){
                case "bot_melee":
                    if ($boolean_val == 2){//false 就是对方摧毁的近战兵营
                        $melee_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['bot_melee_barrack'] = $boolean_val;
                    break;
                case "bot_ranged":
                    if ($boolean_val == 2){//false 就是对方摧毁的远程兵营
                        $ranged_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['bot_ranged_barrack'] = $boolean_val;
                    break;
                case "mid_melee":
                    if ($boolean_val == 2){//false 就是对方摧毁的近战兵营
                        $melee_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['mid_melee_barrack'] = $boolean_val;
                    break;
                case "mid_ranged":
                    if ($boolean_val == 2){//false 就是对方摧毁的中路远程兵营
                        $ranged_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['mid_ranged_barrack'] = $boolean_val;
                    break;
                case "top_melee":
                    if ($boolean_val == 2){//false 就是对方摧毁的近战兵营
                        $melee_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['top_melee_barrack'] = $boolean_val;
                    break;
                case "top_ranged":
                    if ($boolean_val == 2){//false 就是对方摧毁的上路远程兵营
                        $ranged_barrack_kills_num ++;
                    }
                    $building_status_barracks_detail['top_ranged_barrack'] = $boolean_val;
                    break;
            }
        }
        $result['building_status_barracks_detail'] = json_encode($building_status_barracks_detail,320);
        $result['ranged_barrack_kills_num'] = $ranged_barrack_kills_num;
        $result['melee_barrack_kills_num'] = $melee_barrack_kills_num;
        return $result;
    }
    //防御塔状态
    public static function conversionRestBuldingStatusTowers($building_status_towers){
        if (empty($building_status_towers)){
            return null;
        }
        $result = [];
        $tower_kills_num = 0;
        $building_status_towers_detail = [];
        //{"ancient_bottom":true,"ancient_top":true,"bottom_tier_1":false,"bottom_tier_2":true,"bottom_tier_3":true,"middle_tier_1":true,"middle_tier_2":true,"middle_tier_3":true,"top_tier_1":true,"top_tier_2":true,"top_tier_3":true}
        foreach ($building_status_towers as $key => $item){
            $boolean_val = $item['standing'] ? 1 : 2;
            if ($boolean_val == 2){//false 就是对方摧毁的塔数量
                $tower_kills_num ++;
            }
            switch ($key){
                case "top_tier_1":
                    $building_status_towers_detail['top_tier_1_tower'] = $boolean_val;
                    break;
                case "top_tier_2":
                    $building_status_towers_detail['top_tier_2_tower'] = $boolean_val;
                    break;
                case "top_tier_3":
                    $building_status_towers_detail['top_tier_3_tower'] = $boolean_val;
                    break;
                case "top_tier_4":
                    $building_status_towers_detail['top_tier_4_tower'] = $boolean_val;
                    break;
                case "mid_tier_1":
                    $building_status_towers_detail['mid_tier_1_tower'] = $boolean_val;
                    break;
                case "mid_tier_2":
                    $building_status_towers_detail['mid_tier_2_tower'] = $boolean_val;
                    break;
                case "mid_tier_3":
                    $building_status_towers_detail['mid_tier_3_tower'] = $boolean_val;
                    break;
                case "bot_tier_1":
                    $building_status_towers_detail['bot_tier_1_tower'] = $boolean_val;
                    break;
                case "bot_tier_2":
                    $building_status_towers_detail['bot_tier_2_tower'] = $boolean_val;
                    break;
                case "bot_tier_3":
                    $building_status_towers_detail['bot_tier_3_tower'] = $boolean_val;
                    break;
                case "bot_tier_4":
                    $building_status_towers_detail['bot_tier_4_tower'] = $boolean_val;
                    break;
            }
        }
        $result['tower_kills_num'] = $tower_kills_num;
        $result['building_status_towers_detail'] = json_encode($building_status_towers_detail,320);
        return $result;
    }
    //处理玩家的道具
    private static function conversionDota2PlayerItems($allItems,$rel_inventory,$rel_backpack,$rel_stash,$rel_buffs = null){
        $inventory = $backpack = $stash = $buffs = [];
        //物品栏
        if (count($rel_inventory) > 0){
            $inventory_pipei = [];
            foreach ($rel_inventory as $key => $item){
                $inventory_item_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],2);
                $inventory_pipei[] = $allItems[$inventory_item_id];
            }
            $inventory_total_cost_array = @array_column($inventory_pipei,'total_cost');
            @array_multisort($inventory_total_cost_array,SORT_DESC,$inventory_pipei);
            $inventory = @array_column($inventory_pipei,'id');
        }
        //背包
        if (count($rel_backpack) > 0){
            $backpack_pipei = [];
            foreach ($rel_backpack as $key => $item){
                $backpack_item_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],2);
                $backpack_pipei[] = $allItems[$backpack_item_id];
            }
            $backpack_total_cost_array = @array_column($backpack_pipei,'total_cost');
            @array_multisort($backpack_total_cost_array,SORT_DESC,$backpack_pipei);
            $backpack = @array_column($backpack_pipei,'id');
        }
        //储藏处
        if (count($rel_stash) > 0){
            $stash_pipei = [];
            foreach ($rel_stash as $key => $item){
                $stash_item_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],2);
                $stash_pipei[] = $allItems[$stash_item_id];
            }
            $stash_total_cost_array = @array_column($stash_pipei,'total_cost');
            @array_multisort($stash_total_cost_array,SORT_DESC,$stash_pipei);
            $stash = @array_column($stash_pipei,'id');
        }
        //增益
        if (count($rel_buffs) > 0){
            $buffs_pipei = [];
            foreach ($rel_buffs as $key => $item){
                $buffs_item_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],2);
                $buffs_pipei[] = $allItems[$buffs_item_id];
            }
            $buffs_total_cost_array = @array_column($buffs_pipei,'total_cost');
            @array_multisort($buffs_total_cost_array,SORT_DESC,$buffs_pipei);
            $buffs = @array_column($buffs_pipei,'id');
        }
        $playerItemsRes = [
            "inventory" => $inventory,
            "backpack" => $backpack,
            "stash" => $stash,
            "buffs" => $buffs
        ];
        return json_encode($playerItemsRes,320);
    }
}