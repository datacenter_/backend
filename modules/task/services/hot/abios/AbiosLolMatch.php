<?php
/**
 *
 */

namespace app\modules\task\services\hot\abios;


use app\modules\task\services\grab\abios\AbiosBase;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\hot\pandascore\PandascoreLolMatch;

class AbiosLolMatch extends AbiosBase implements HotMatchInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        $matchId='257212';
        $relIdentityId = '257212';
        $infoFromRest = self::getInfos($relIdentityId);

        $conversionInfo = self::conversion($infoFromRest);

        self::setInfo($matchId, $conversionInfo);
    }

    public static function getInfos($relIdentityId)
    {
        $action = '/v2/series/' . $relIdentityId;
        $params = [
            'with' => [
                'tournament',
                'matches',
                'casters',
            ]
        ];

        $matchData = AbiosHotBase::getRestInfo($action, $params);
        $battles = [];
        foreach ($matchData['matches'] as $k => $battle) {
            $action = "/v2/matches/{$battle['id']}/light_summary";
            $battleDesc = AbiosHotBase::getRestInfo($action, array());
            $battles[$battle['id']] = $battleDesc;
        }
        return [
            'match' => $matchData,
            'battles' => $battles
        ];
    }

    public static function conversion($infoFromRest)
    {
// 比赛数据
        $matchData = $infoFromRest['match'];

        $match['begin_at'] = $matchData['start'];
        $match['end_at'] = $matchData['end'];
        $match['team_1_score'] = $matchData['scores'][$matchData['seeding'][1]];
        $match['team_2_score'] = $matchData['scores'][$matchData['seeding'][2]];
        $match['is_battle_detailed'] = $matchData['pbp_status'] ? 2 : 1;
        $match['is_pbpdata_supported'] = $matchData['pbp_status'] ? 2 : 1;
        $match['is_forfeit'] = $matchData['forfeit'][$matchData['seeding'][1]] == false && $matchData['forfeit'][$matchData['seeding'][2]] == false ? 1 : 2;

        // base 数据
        $base['match_id'] = $matchData['id'];
        // 获取对局数据
        $newMarchBattle = array();
        foreach ($matchData['matches'] as $k => $battle) {
            $marchBattle['base']['game'] = $matchData['game']['id'];
            $marchBattle['base']['match'] = $battle['series_id'];
            $marchBattle['base']['order'] = $battle['order'];
            $marchBattle['base']['is_forfeit'] = $battle['forfeit'][$battle['seeding'][1]] == false && $battle['forfeit'][$battle['seeding'][2]] == false ? 1 : 2;
            $marchBattle['base']['map'] = $battle['map']['id'] == 101 ? 1 : 2;
            $marchBattle['base']['is_battle_detailed'] = $battle['has_pbpstats']?1:2;
            $marchBattle['base']['winner'] = $battle['winner'];

//            $action = "/v2/matches/{$battle['id']}/light_summary";
//            $battleDesc = AbiosHotBase::getRestInfo($action,array());
//            print_r($battleDesc);
            $battleDesc = $infoFromRest['battles'][$battle['id']];

            // 对局时长
            $marchBattle['base']['duation'] = $battleDesc['length'];

            // battle_ext_lol数据
            $marchBattle['detail']['is_confirmed'] = $battleDesc['confirmed'] == true ? 1 : 2;
            $marchBattle['detail']['is_finished'] = $battleDesc['ended'] == true ? 1 : 2;


            // battle_event_lol 数据
//            $marchBattle['event']['battle_id'] = $battle['id'];
//            $marchBattle['event']['game'] = $matchData['game']['id'];
//            $marchBattle['event']['match'] = $battle['series_id'];
//            $marchBattle['event']['order'] = $battle['order'];
            // battle_event_lol 数据
            $param = array(
                'battle_id' => $battle['id'],
                'game' => $matchData['game']['id'],
                'match' => $battle['series_id'],
                'order' => $battle['order'],
                'building' => $battleDesc['building_kills']
            );
            $marchBattle['events'] = self::actionEventData($param);

            // 获取team数据
            $params = array(
                'red' => $battleDesc['red'],
                'blue' => $battleDesc['blue'],
                'order' => $battle['order'],
                'battle_id' => $battle['id'],
                'rosters' => $battle['rosters']
            );
//            $marchBattle['team'] = $this->actionBattleTeam($params);
            $teams = self::actionBattleTeam($params);
            // 获取team_ext数据
            $params = array(
                'red' => $battleDesc['red'],
                'blue' => $battleDesc['blue'],
                'battle_id' => $battle['id'],
                'game' => $matchData['game']['id'],
                'match' => $battle['series_id'],
                'order' => $battle['order'],
            );
//            $marchBattle['teamExt'] = $this->actionBattleTeamExt($params);
            $teamExt = self::actionBattleTeamExt($params);
            // team 与 teamExt 进行合并
            $marchBattle['static'] = self::actionBattleStatic($teams, $teamExt);


            // 获取players - one
            $playersOne = self::actionPlayersOne($battle['rosters']);

            $playersTwo = self::actionPlayersTwo($params);
            $marchBattle['players'] = self::actionPlayersData($playersTwo, $playersOne);
            $marchBattle['order'] = $battle['order'];
            array_push($newMarchBattle, $marchBattle);
        }
        return [
            'match'=>$match,
            'battles'=>$newMarchBattle
        ];

    }

    public static function setInfo($matchId, $conversionInfo)
    {
        PandascoreLolMatch::refreshInfo($matchId, $conversionInfo);
    }


    public function actionEventData($data)
    {
        $eventArr = array();
        foreach ($data['building'] as $k => $v) {
            $arr = array(
                'battle_id' => $data['battle_id'],
                'game' => $data['game'],
                'match' => $data['match'],
                'order' => $data['order'],
            );
            $arr['ingame_timestamp'] = $v['match_time'];
            $arr['killer'] = $v['attacker']['player']['id'];

            $assistsArr = array();
            foreach ($v['assists'] as $k2 => $v2) {
                $ass['assist_id'] = $v2['player']['id'];
                array_push($assistsArr, $ass);
            }
            $arr['assist'] = json_encode($assistsArr);

            array_push($eventArr, $arr);
        }
        return $eventArr;
    }

    public function actionBattleTeam($data)
    {
        $newTeam = array();
        if (isset($data['red'])) {
            $arr['order'] = $data['order'];
            $arr['battle_id'] = $data['battle_id'];
            $arr['score'] = $data['red']['score'];
//            foreach ($data['rosters'] as $k=>$v){
//                if($v['id'] == $data['red']['roster']['id']){
//                    $arr['team_id'] = $v['team_id'];
//                }
//            }
            $arr['team_id'] = $data['red']['roster']['id'];
            array_push($newTeam, $arr);
        }
        if (isset($data['blue'])) {
            $arr['order'] = $data['order'];
            $arr['battle_id'] = $data['battle_id'];
            $arr['score'] = $data['blue']['score'];
//            foreach ($data['rosters'] as $k=>$v){
//                if($v['id'] == $data['red']['roster']['id']){
//                    $arr['team_id'] = $v['team_id'];
//                }
//            }
            $arr['team_id'] = $data['blue']['roster']['id'];
            array_push($newTeam, $arr);
        }
        return $newTeam;
    }

    public function actionBattleTeamExt($data)
    {
        $newTeamExt = array();
        if (isset($data['red'])) {
            $faction['faction'] = 'red';
            $faction['score'] = $data['red']['score'];
            $faction['turret_kills'] = $data['red']['turrets']['destroyed'];
            $faction['gold'] = $data['red']['gold']['earned'];
            $faction['inhibitor_kills'] = $data['red']['inhibitors']['destroyed'];
            $faction['baron_nashor_kills'] = $data['red']['barons']['killed'];
            $faction['dragon_kills'] = $data['red']['dragons']['killed'];
            $faction['team_id'] = $data['red']['roster']['id'];
            array_push($newTeamExt, $faction);
        }
        if (isset($data['blue'])) {
            $faction['faction'] = 'blue';
            $faction['score'] = $data['blue']['score'];
            $faction['turret_kills'] = $data['blue']['turrets']['destroyed'];
            $faction['gold'] = $data['blue']['gold']['earned'];
            $faction['inhibitor_kills'] = $data['blue']['inhibitors']['destroyed'];
            $faction['baron_nashor_kills'] = $data['blue']['barons']['killed'];
            $faction['dragon_kills'] = $data['blue']['dragons']['killed'];
            $faction['team_id'] = $data['blue']['roster']['id'];
            array_push($newTeamExt, $faction);
        }
        return $newTeamExt;
    }

    public function actionBattleStatic($team, $teamE)
    {
        $teams = array();
        foreach ($team as $k => $v) {
            foreach ($teamE as $k2 => $v2) {
                if ($v['team_id'] == $v2['team_id']) {
                    $arr = array_merge($v, $v2);
                    array_push($teams, $arr);
                }
            }
        }
        return $teams;
    }

    public function actionPlayersOne($data)
    {
        $players = array();
        foreach ($data as $k => $v) {
            foreach ($v['players'] as $k2 => $v2) {
                $arr['id'] = $v2['id'];
                $arr['nick_name'] = $v2['nick_name'];
                array_push($players, $arr);
            }
        }
        return $players;
    }

    public function actionPlayersTwo($data)
    {
        $players = array();
        if (isset($data['red'])) {
            foreach ($data['red']['players'] as $k2 => $v2) {
                $playerW['player_id'] = $v2['player']['id'];
                $playerW['kills'] = $v2['kills'];
                $playerW['deaths'] = $v2['deaths'];
                $playerW['assists'] = $v2['assists'];
                $playerW['cs'] = $v2['cks'];
                $playerW['champion'] = $v2['champion']['id'];
                $playerW['order'] = $data['order'];
                $playerW['battle_id'] = $data['battle_id'];
                $playerW['game'] = $data['game'];
                $playerW['match'] = $data['match'];
                array_push($players, $playerW);
            }
        }
        if (isset($data['blue'])) {
            foreach ($data['blue']['players'] as $k2 => $v2) {
                $playerW['player_id'] = $v2['player']['id'];
                $playerW['kills'] = $v2['kills'];
                $playerW['deaths'] = $v2['deaths'];
                $playerW['assists'] = $v2['assists'];
                $playerW['cs'] = $v2['cks'];
                $playerW['champion'] = $v2['champion']['id'];
                $playerW['order'] = $data['order'];
                $playerW['battle_id'] = $data['battle_id'];
                $playerW['game'] = $data['game'];
                $playerW['match'] = $data['match'];
                array_push($players, $playerW);
            }
        }
        return $players;
    }

    public function actionPlayersData($newPlayerW, $newPlayer)
    {
        foreach ($newPlayerW as $k => $v) {
            foreach ($newPlayer as $k2 => $v2) {
                if ($v['player_id'] == $v2['id']) {
                    $newPlayerW[$k]['nick_name'] = $v2['nick_name'];
                }
            }
        }
        return $newPlayerW;
    }

}