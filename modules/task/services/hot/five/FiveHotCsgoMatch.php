<?php

namespace app\modules\task\services\hot\five;

use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\SortingLog;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\task\services\hot\five\csgo\Event;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\logformat\CsgoLogFormatBase;
use app\modules\task\services\logformat\CsgoLogUCC;

class FiveHotCsgoMatch extends HotBase
{
    static private $instance;
    const CSGO_GAME_ID = 1;
    /**
     * 消费 event queue
     * @param $tagInfo
     * @param $taskInfo
     */
    public static function run($tagInfo,$taskInfo)
    {
        $model = self::getInstance();
        $timeStart=date('Y-m-d H:i:s');
        $info=json_decode($taskInfo['params'],true);
        // 如果id差<100，则设置更新状态为只更新当前回合
        $updateType=1;
        $updateEventNum = $info['to_event_id']-$info['from_event_id'];
        if($updateEventNum > 0 && $updateEventNum < 100){
            $updateType=2;
        }
        $hotId=$info['id'];
        $hotInfo=self::getHotInfo($hotId);
        $sortingLog=SortingLog::find()->where(['id'=>$hotInfo['standard_id']])->one();
        $matchInfo=Match::find()->where(['id'=>$hotInfo['match_id']])->select('scheduled_begin_at')->one();
        $logBeginTime = $matchInfo['scheduled_begin_at'];
        $logEndTime =  date("Y-m-d H:i:s",strtotime("+1 day",strtotime($logBeginTime)));
        $ipAddress=$sortingLog['service_ip'];
        $matchId = $hotInfo['match_id'];
        if(!$matchId) {
            $matchConversionInfo = [];
            $battlesConversionInfo = [];
        }else {
            $matchConversionInfo = $model->conversionMatch($matchId);
            $battlesConversionInfo = $model->getMatchBattles($matchId);
        }
        $infoFormat = [
            'match' => $matchConversionInfo,
            'battles' => $battlesConversionInfo,
        ];
        $data = self::refresh($matchId, $infoFormat);
        return;
    }

    public function conversionMatch($matchId){
        //读取redis 组出来infoFormat
        $match_begin_at_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_BEGIN_AT]);
        $match_end_at_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
        $match_status_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS]);
        $match_team_1_score_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE]);
        $match_team_2_score_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]);
        $match_winner_value = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_WIN_TEAM,self::KEY_TAG_DETAILS]);
        $matchinfoFormat = [
            'begin_at' => $match_begin_at_value ? $match_begin_at_value:'',
            'end_at' => $match_end_at_value ? $match_end_at_value:'',
            'status' => $match_status_value ? intval($match_status_value):null,
            'team_1_score' => $match_team_1_score_value ? intval($match_team_1_score_value):null,
            'team_2_score' => $match_team_2_score_value ? intval($match_team_2_score_value):null,
            'winner' => $match_winner_value ? intval($match_winner_value):null,
            'is_battle_detailed' => 1,
            'is_pbpdata_supported' => 1,

        ];
        return $matchinfoFormat;
    }
    private function getMatchBattles($matchId){
        $battlesRes = [];
        //查找所有battle
        $battles = $this->getBattlesByMatchId($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_LIST,self::KEY_TAG_BATTLE],0,-1);
        if ($battles){
            foreach ($battles as $key => $battle){
                $battle_id = $battle;
                $battleInfo = $this->conversionBattles($matchId,$battle_id);
                if($battleInfo['order'])
                    $battlesRes[$key] = $battleInfo;
            }
        }
        return $battlesRes;
    }

    // base 数据格式(结果)
    private static $battleFieldConfig = [];
    // static 转换完数组(结果)
    private static $battleStaticFieldArray = [];
    // static 数据格式设置
    private static $battleStaticFieldConfig = [];
    // detail 数据格式设置(结果)
    private static $battleDetailFieldConfig = [];
    // battle下 players转换完数组(结果)
    private static $battlePlayersFieldArray = [];
    // rounds下 players转换完数组(结果)
    private static $roundPlayersFieldArray = [];
    // match下 teams转换完数组(结果)
    private static $teamsFieldArray = [];
    // battle下 rounds转换完数组(结果)
    private static $roundsFieldArray = [];
    // rounds下 events数据格式
    private static $roundEventsFieldConfig = [];
    // rounds下 events转换完数组(结果)
    private static $roundEventsFieldArray = [];
    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        // 初始化
        self::$battleFieldConfig = [
//            'status' => 3,
            'is_draw' => 2,
            'is_forfeit' => 2,
            'is_battle_detailed' => 1 //是否有对局详情数据
        ];
        self::$battleFieldConfig = array_merge(self::$battleFieldConfig, $baseResultData);
    }
    // set static数据
    public static function setBattleStatic($staticResultData)
    {
        // 初始化
        self::$battleStaticFieldConfig = [];
        self::$battleStaticFieldArray[] = array_merge(self::$battleStaticFieldConfig, $staticResultData);
    }
    // set detail数据
    public static function setBattleDetail($detailResultData)
    {
        // 初始化
        self::$battleDetailFieldConfig = [
            'is_confirmed' => 1,
            'is_finished' => null,
            'is_pause' => null,
            'is_live' => null,
            'current_round' => null,
            'is_bomb_planted' => null,
            'win_round_1_side' => '',
            'win_round_1_team' => null,
            'win_round_1_detail' => '',
            'win_round_16_side' => '',
            'win_round_16_team' => null,
            'win_round_16_detail' => '',
            'first_to_5_rounds_wins_side' => '',
            'first_to_5_rounds_wins_team' => null,
            'first_to_5_rounds_wins_detail' => '',
            'live_rounds' => ''
        ];
        self::$battleDetailFieldConfig[] = array_merge(self::$battleDetailFieldConfig, $detailResultData);
    }
    private function conversionBattles($matchId,$battleId){
        // 初始化
        self::initializationBattleInfo();
        // base
        $orderId = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_ORDER]);
        $beginAt = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BEGIN_AT]);
        $endAt = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_END_AT]);
        $mapId = $this->hGet([self::KEY_TAG_CURRENT,self::KEY_TAG_MAP,self::KEY_TAG_DETAILS],'id');
        $duration = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_DURATION]);
        $battleWinTeam = $this->getValue($matchId,[self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_WINNER]);
        $battleStatus = $this->getValue($matchId,[self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_STATUS]);
        if(!empty($orderId) && !empty($battleId)) {
            self::setBattleBase([
                'game' => self::CSGO_GAME_ID,
                'match' => $matchId,
                'order' => $orderId,
                'begin_at' => $beginAt ? $beginAt:'',
                'end_at' => $endAt ? $endAt:'',
                'is_default_advantage' => null, //是否是默认领先获胜
                'map' => $mapId ? $mapId:null,
                'duration' => $duration ? $duration:'', //对局时长
                'winner' => $battleWinTeam ? $battleWinTeam:null, //获胜战队
                'status' => $battleStatus ? $battleStatus:null, //battle状态
            ]);
            // detail
            $baseDetailData = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_DETAILS]);
            self::setBattleDetail([
                'is_finished' => @intval($baseDetailData['is_finished']),
                'is_pause' => @intval($baseDetailData['is_pause']),
                'is_live' => @intval($baseDetailData['is_live']),
                'current_round' => @intval($baseDetailData['current_round']),
                'is_bomb_planted' => @intval($baseDetailData['is_bomb_planted']),
                'win_round_1_side' => @$baseDetailData['win_round_1_side'],
                'win_round_1_team' => @intval($baseDetailData['win_round_1_team']),
                'win_round_1_detail' => @$baseDetailData['win_round_1_detail'],
                'win_round_16_side' => @$baseDetailData['win_round_16_side'],
                'win_round_16_team' => @intval($baseDetailData['win_round_16_team']),
                'win_round_16_detail' => @$baseDetailData['win_round_16_detail'],
                'first_to_5_rounds_wins_side' => @$baseDetailData['first_to_5_rounds_wins_side'],
                'first_to_5_rounds_wins_team' => @intval($baseDetailData['first_to_5_rounds_wins_team']),
                'first_to_5_rounds_wins_detail' => @$baseDetailData['first_to_5_rounds_wins_detail'],
                'live_rounds' => @$baseDetailData['live_rounds']
            ]);
        }else{
            return ['order' => null];
        }
        // battle-players
        $battlePlayersData = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_PLAYER,self::KEY_TAG_LIST]);
        // round-players
        $roundPlayersData = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND_PLAYER,self::KEY_TAG_LIST]);
        // static(teams)
        $battleStaticData = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_TEAM,self::KEY_TAG_LIST]);
        // rounds
        $battleRoundsData = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUNDS,self::KEY_TAG_LIST]);
        // events
        $roundEventsDataJson = $this->hGetAlls($matchId,[self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_EVENTS,self::KEY_TAG_LIST]);

        if($battlePlayersData) {
            foreach ($battlePlayersData as $key => $battlePlayer) {
                self::$battlePlayersFieldArray[] = json_decode($battlePlayer, true);
            }
        }
        if($roundPlayersData) {
            foreach ($roundPlayersData as $key => $roundPlayer) {
                self::$roundPlayersFieldArray[] = json_decode($roundPlayer, true);
            }
        }
        if($battleStaticData) {
            foreach ($battleStaticData as $key => $team) {
                self::$teamsFieldArray[] = json_decode($team, true);
            }
        }
        if($battleRoundsData) {
            foreach ($battleRoundsData as $key => $round) {
                self::$roundsFieldArray[] = json_decode($round, true);
            }
        }
        $roundEventsData = [];
        if($roundEventsDataJson) {
            foreach ($roundEventsDataJson as $key => $eventJson) {
                $roundEventsData[] = json_decode($eventJson, true);
            }
        }
        foreach ($roundEventsData as $k => $event) {
            self::roundEventChange($event,$event['event_type']);
        }
        return [
            'order' => intval($orderId),
            'base' => self::$battleFieldConfig,
            'static' => self::$teamsFieldArray,
            'players' => self::$battlePlayersFieldArray,
            'detail' => self::$battleDetailFieldConfig,
            'rounds' => self::$roundsFieldArray,
            'round_players' => self::$roundPlayersFieldArray,
            'events' => self::$roundEventsFieldArray
        ];
    }

    const EVENT_TYPE_ROUND_START =  'round_start';
    const EVENT_TYPE_ROUND_END =  'round_end';
    const EVENT_TYPE_PLAYER_KILL =  'player_kill';
    const EVENT_TYPE_BOMB_PLANTED =  'bomb_planted'; // 炸弹安放
    const EVENT_TYPE_BOMB_DEFUSED =  'bomb_defused'; // 炸弹拆除
    const EVENT_TYPE_PLAYER_SUICIDE =  'player_suicide'; // 选手自杀
    // 转换event
    public function roundEventChange($event,$eventType){
        switch ($eventType){
            case self::EVENT_TYPE_ROUND_START:
                break;
            case self::EVENT_TYPE_ROUND_END:
                    self::roundEventChangeResult([
                        'round_end_type' => @$event['round_end_type'],
                        'winner_side' => @$event['winner_side'],
                        'ct_score' => @$event['ct_score'],
                        't_score' => @$event['t_score'],
                    ]);
                break;
            case self::EVENT_TYPE_PLAYER_KILL:
                self::roundEventChangeResult([
                    'killer' => @$event['killer']['player_id'],
                    'killer_nick_name' => @$event['killer']['nick_name'],
                    'killer_side' => @$event['killer']['side'],
                    'killer_position' => @$event['position'],
                    'victim' => @$event['victim']['player_id'],
                    'victim_nick_name' => @$event['victim']['nick_name'],
                    'victim_side' => @$event['victim']['side'],
                    'victim_position' => @$event['victim']['position'],
                    'assist' => @$event['assist']['player_id'],
                    'assist_nick_name' => @$event['assist']['nick_name'],
                    'assist_side' => @$event['assist']['side'],
                    'flashassist' => @$event['flashassist']['player_id'],
                    'flashassistt_nick_name' => @$event['flashassist']['nick_name'],
                    'flashassist_side' => @$event['flashassist']['side'],
                    'weapon' => @$event['weapon']['weapon_id'],
                    'damage' => @$event['damage'],
                    'hit_group' => @$event['hit_group'],
                    'is_headshot' => @$event['is_headshot']==true ? 1:2
                ]);
                break;
            case self::EVENT_TYPE_BOMB_PLANTED:
                self::roundEventChangeResult([
                    'bomb_planted_player_id' => @$event['player_id'],
                    'bomb_planted_nick_name' => @$event['nick_name'],
                    'bomb_planted_side' => @$event['side'],
                    'bomb_planted_position' => @$event['position'],
                ]);
                break;
            case self::EVENT_TYPE_BOMB_DEFUSED:
                self::roundEventChangeResult([
                    'bomb_defused_player_id' => @$event['player_id'],
                    'bomb_defused_nick_name' => @$event['nick_name'],
                    'bomb_defused_side' => @$event['side'],
                    'bomb_defused_position' => @$event['position'],
                ]);
                break;
            case self::EVENT_TYPE_PLAYER_SUICIDE:
                self::roundEventChangeResult([
                    'suicide_player_id' => @$event['player_id'],
                    'suicide_nick_name' => @$event['nick_name'],
                    'suicide_side' => @$event['side'],
                    'suicide_position' => @$event['position'],
                ]);
                break;
            default:
                return;
        }
        self::roundEventChangeResult([
//            'event_id' => @$event['event_id'],
            'order' => @$event['event_id'],
            'event_type' => @$event['event_type'],
            'round_ordinal' => @$event['round_ordinal'],
            'in_round_timestamp' => @$event['in_round_timestamp'],
            'round_time' => @$event['round_time'],
            'is_bomb_planted' => @$event['is_bomb_planted'] == true ? 1 : 2,
            'time_since_plant' => @$event['time_since_plant'],
        ]);
        self::$roundEventsFieldArray[] = self::$roundEventsFieldConfig;
    }
    // 转换event结果数据
    public function roundEventChangeResult($eventArray){
        // 初始化
        self::$roundEventsFieldConfig = $this->eventAtts;
        self::$roundEventsFieldConfig = array_merge(self::$roundEventsFieldConfig,$eventArray);
    }
    // 初始化
    public function initializationBattleInfo(){
        self::$battlePlayersFieldArray = [];
        self::$roundPlayersFieldArray = [];
        self::$teamsFieldArray = [];
        self::$battleFieldConfig = [];
        self::$battleDetailFieldConfig = [];
        self::$roundsFieldArray = [];
        self::$roundEventsFieldArray = [];
    }

    private static function refresh($matchId,$formatInfo)
    {
        // todo getMountedId,有可能会找不到，找不到的留空
        self::setMatch($matchId,$formatInfo['match']);
        foreach($formatInfo['battles'] as $battle){
            self::setBattle($matchId,$battle);
        }
    }
    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['order'],
            ]);
            $battleBase->save();
        }
        $battleId=$battleBase['id'];
        $battle['base']['match'] = $matchId;
        BattleService::setBattleInfo($battleId,'csgo',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'csgo',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['rounds'],BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['events'],BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['round_players'],BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    public static function getBattleFormat()
    {
    }

    public function lRanges($matchId,$key,$offset,$length)
    {
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::lRange($keyString,$offset,$length);
    }
    //redis
    public function getValue($matchId,$keys)
    {
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::getKey($keyString);
    }

    public function hGetAlls($matchId,$key)
    {
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::hGetAll($keyString);
    }

    /**
     * @param $matchId
     * @param $key
     * @param $offset
     * @param $length
     */
    public function getBattlesByMatchId($matchId,$key,$offset,$length)
    {
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::lRange($keyString,$offset,$length);
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    private $eventAtts = [
        'game' => 1,
        'order' => null,//排序
        'event_type' => "",//事件类型
        'round_side' => "",//round阵营
        'team_id' => null,//回合触发特殊事件的战队ID
        'detail' => "",//详情
        'round_ordinal' => null,//回合序号
        'in_round_timestamp' => "",//回合内时间戳
        'round_time' => "",//回合内时间
        'is_bomb_planted' => "",//炸弹是否已安放
        'time_since_plant' => "",//安放炸弹后时间
        'killer' => null,//击杀者ID
        'killer_nick_name' => "",//击杀者昵称
        'killer_side' => "",//击杀者阵营
        'killer_position' => "",//击杀者位置
        'victim' => null,//受害人ID
        'victim_nick_name' => "",//受害人昵称
        'victim_side' => "",//受害人阵营
        'victim_position' => "",//受害人位置
        'assist' => null,//是否有助攻
        'assist_nick_name' => "",//助攻者昵称
        'assist_side' => "",//助攻者阵营
        'flashassist' => "",//闪光弹助攻ID
        'flashassistt_nick_name' => "",//闪光弹助攻者昵称
        'flashassist_side' => "",//闪光弹助攻阵营
        'weapon' => null,//武器ID
        'damage' => "",//伤害
        'hit_group' => "",//击杀命中部位
        'is_headshot' => "",//是否为爆头击杀
        'round_end_type' => null,// round_end- 回合获胜方式 - 有码表 转为ID
        'winner_side' => '',// round_end- 获胜方阵营
        'ct_score' => null,// round_end- CT比分
        't_score' => null,// round_end- T比分
        'bomb_planted_player_id' => null,// 炸弹安放- 选手ID
        'bomb_planted_nick_name' => '',// 炸弹安放- 选手昵称
        'bomb_planted_side' => '',// 炸弹安放- 阵营
        'bomb_planted_position' => '',// 炸弹安放- 位置
        'bomb_defused_player_id' => null,// 炸弹拆除- 选手ID
        'bomb_defused_nick_name' => '',// 炸弹拆除- 选手昵称
        'bomb_defused_side' => '',// 炸弹拆除- 阵营
        'bomb_defused_position' => '',// 炸弹拆除- 位置
        'suicide_player_id' => null,//选手自杀ID
        'suicide_nick_name' => '',// 选手自杀昵称
        'suicide_side' => '',// 选手自杀阵营
        'suicide_position' => '',// 选手自杀位置
        'weapon_name' => '',// 原始武器名字
        'bomb_exploded_time' => '',// 炸弹爆炸- 时间戳
    ];
}