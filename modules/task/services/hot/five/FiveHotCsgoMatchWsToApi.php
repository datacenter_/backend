<?php

namespace app\modules\task\services\hot\five;

use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\SortingLog;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\five\csgo\Event;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\WsToApiSynchron;
use app\modules\task\services\logformat\CsgoLogFormatBase;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskMQConsumer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use Throwable;

/**
 * Csgo Event Ws
 **/
class FiveHotCsgoMatchWsToApi extends FiveHotCsgoMatchWs
{
    static private $instance;
    const CSGO_GAME_ID = 1;
    public $updateType = 1;
    public $round_end_count = 0;
    public $info = [];

    /**
     * 消费 event queue
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     */
    public static function run($tagInfo, $taskInfo)
    {

        //info
        $self = self::getInstance();
        $info = json_decode($taskInfo['params'], true);
        if (!$info['match_id']) {
            return false;
        }
        //match info
        $updateType       = $info['updateType'];
        $matchId          = $info['match_id'];
        $self->currentPre = array_key_exists('currentPre', $info) ? $info['currentPre'] : 'ws';
        $redisQueue       = WsToApiSynchron::getSynchron($self->currentPre, $matchId);
        if (empty($redisQueue) && $redisQueue['task']) {
            $noQueueKey = $self->keyStringByOriginAndMatchId($self->currentPre, $matchId, [self::KEY_TAG_NO_QUEUE]);
            //同步器中没有数据
            $self->redis->lPush($noQueueKey, json_encode(['time' => date('Y-m-d H:i:s')], JSON_UNESCAPED_UNICODE));
            return false;
        } else {
            $info = json_decode($redisQueue['task'], true);
            if (array_key_exists('event_type', $info) && $info['event_type'] == 'battle_start') {
                $self->clearNoUseEvents();
            }
            if ($redisQueue['round_end_count']) {
                $self->round_end_count = $redisQueue['round_end_count'];
            }
        }
        if (array_key_exists('event_id', $info) && $info['event_id']) {
            $self->event_id = $info['event_id'];
        }
        if (array_key_exists('log_time', $info) && $info['log_time']) {
            $self->log_time = $info['log_time'];
        }
        $self->info = $info;
        if (!$matchId) {
            $infoFormat = [
                'match'   => null,
                'battles' => []
            ];
        } else {
            //获取比赛信息
            $matchConversionInfo   = $self->conversionMatch($matchId);
            $infoFormat['match']   = $matchConversionInfo;
            //获取battle信息
            $battlesConversionInfo = $self->getMatchBattles($matchId, $updateType);
            $infoFormat['battles'] = $battlesConversionInfo;
        }
        //refresh到数据库
        self::refresh($matchId, $infoFormat);
        //刷gaoyu接口
        if (array_key_exists('event_type', $info) && $info['event_type'] == 'battle_end') {
           $self->refreshEsByMatchId($matchId);
        }
    }


    /**
     * 查询match信息
     * @param $matchId
     * @return array
     */
    public function conversionMatch($matchId)
    {
        $this->matchId = $matchId;
        //读取redis 组出来infoFormat
        $match_begin_at_value     = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BEGIN_AT]);
        $match_end_at_value       = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
        $match_status_value       = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS]);
        $match_team_1_score_value = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE]);
        $match_team_2_score_value = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]);
        $match_game_version       = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_GAME_VERSION]);
        $match_winner_value       = $this->getMatchWinner();
        if (!empty($match_begin_at_value)) {
            $length = strlen($match_begin_at_value);
            if ($length == 23) {
                $match_begin_at_value = substr($match_begin_at_value, 0, 19);
            }
        }
        if (!empty($match_end_at_value)) {
            $length = strlen($match_end_at_value);
            if ($length == 23) {
                $match_end_at_value = substr($match_end_at_value, 0, 19);
            }
        }
        if ($match_game_version) {
            $this->setMatchGameVersion($matchId, $match_game_version);
        }
        $matchinfoFormat = [
            'begin_at'             => $match_begin_at_value == 'null' ? '' : (string)$match_begin_at_value,
            'end_at'               => $match_end_at_value == 'null' ? '' : (string)$match_end_at_value,
            'status'               => $match_status_value ? intval($match_status_value) : 1,
            'team_1_score'         => intval($match_team_1_score_value),
            'team_2_score'         => intval($match_team_2_score_value),
            'winner'               => $match_winner_value['opponent_order']?intval($match_winner_value['opponent_order']):null,
            'is_battle_detailed'   => 1,
            'is_pbpdata_supported' => 1,
        ];
        return $matchinfoFormat;
    }

    /**
     * 刷新高宇es接口
     * @param $matchId
     * @throws \yii\db\Exception
     */
    public function refreshEsByMatchId($matchId){
        sleep(1);
        //battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                7,   RenovateService::INTERFACE_TYPE_BATTLE, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //csgo_battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                7, RenovateService::INTERFACE_TYPE_CSGO_BATTLE , "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //match
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                7, RenovateService::INTERFACE_TYPE_MATCH, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //csgo_tournament
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                7, RenovateService::INTERFACE_TYPE_CSGO_TOURNAMENT, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
    }

    /**
     * 设置游戏版本
     * @param $matchId
     * @param $gameVersion
     * @return bool
     */
    public function setMatchGameVersion($matchId, $gameVersion)
    {
        if ($matchId && $gameVersion) {
            $match = Match::find()->where(['id' => $matchId])->one();
            if (!empty($match)) {
                $match->setAttribute('game_version', $gameVersion);
                return $match->save();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 获取所有battles
     * @param $matchId
     * @param int $updateType
     * @return array
     */
    private function getMatchBattles($matchId, $updateType = 1)
    {
        $this->updateType = $updateType;
        $battlesRes       = [];
        //查找所有battle
        $battles = $this->getBattles();
        if ($battles) {
            $battles = array_map([__CLASS__, 'mapJson'], $battles);
            $battleCount =0;
            $battleOrder = [];
            foreach ($battles as $key => $battle) {
                if (strstr($key, 'live') && $battle['is_use']) {
                    $battleCount++;
                    $battleOrder[] = $battle['real_order'];
                }
            }
            if (($updateType == 3 || $updateType == 2) && $battleCount >= 1) {
                $maxOrder = max($battleOrder);
                foreach ($battles as $key => $battle) {
                    if (strstr($key, 'live') && $battle['is_use'] && $battle['real_order'] > 0 && $battle['real_order'] == $maxOrder) {
                        $battle_id  = $battle['battle_id'];
                        $battleInfo = $this->conversionBattles($matchId, $battle_id, $battle);
                        if ($battleInfo['order']) {
                            $battlesRes[$battle['real_battle_id']] = $battleInfo;
                        }
                    }
                }
            } else {
                foreach ($battles as $key => $battle) {
                    if (strstr($key, 'live') && $battle['is_use'] && $battle['real_order']>0) {
                        $battle_id  = $battle['battle_id'];
                        $battleInfo = $this->conversionBattles($matchId, $battle_id, $battle);
                        if ($battleInfo['order']) {
                            $battlesRes[$battle['real_battle_id']] = $battleInfo;
                        }
                    }
                }
            }

        }
        return $battlesRes;
    }

    // base 数据格式(结果)
    private static $battleFieldConfig = [];
    // static 转换完数组(结果)
    private static $battleStaticFieldArray = [];
    // static 数据格式设置
    private static $battleStaticFieldConfig = [];
    // detail 数据格式设置(结果)
    private static $battleDetailFieldConfig = [];
    // battle下 players转换完数组(结果)
    private static $battlePlayersFieldArray = [];
    // rounds下 players转换完数组(结果)
    private static $roundPlayersFieldArray = [];
    // match下 teams转换完数组(结果)
    private static $teamsFieldArray = [];
    // battle下 rounds转换完数组(结果)
    private static $roundsFieldArray = [];
    // rounds下 events转换完数组(结果)
    private static $roundEventsFieldArray = [];
    private static $WinEndTypeArray = [
        'bomb_defused'   => 1,
        'cts_win'        => 2,
        'target_saved'   => 3,
        'target_bombed'  => 4,
        'terrorists_win' => 5,
        'round_draw'     => 6,
    ];

    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        // 初始化
        self::$battleFieldConfig = [
//            'status' => 3,
            'is_draw'            => 2,
            'is_forfeit'         => 2,
            'is_battle_detailed' => 1 //是否有对局详情数据
        ];
        self::$battleFieldConfig = array_merge(self::$battleFieldConfig, $baseResultData);
    }

    // set static数据
    public static function setBattleStatic($staticResultData)
    {
        // 初始化
        self::$battleStaticFieldConfig  = [];
        self::$battleStaticFieldArray[] = array_merge(self::$battleStaticFieldConfig, $staticResultData);
    }


    private function conversionBattles($matchId, $battleId, $battle)
    {
        // 初始化
        self::initializationBattleInfo();
        // base
        $orderId = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER]);
        if (!empty($orderId)) {
            // detail
            $this->setDetails($battle);
            if (!empty($battle['begin_at'])) {
                $length = strlen($battle['begin_at']);
                if ($length == 23) {
                    $battle['begin_at'] = substr($battle['begin_at'], 0, 19);
                }
            }
            if (!empty($battle['end_at'])) {
                $length = strlen($battle['end_at']);
                if ($length == 23) {
                    $battle['end_at'] = substr($battle['end_at'], 0, 19);
                }
            }
            //base
            self::setBattleBase([
                'game'                 => self::CSGO_GAME_ID,
                'match'                => $matchId,
                'order'                => @$battle['real_order'] ? $battle['real_order'] : 0,
                'begin_at'             => @$battle['begin_at'],
                'end_at'               => @$battle['end_at'],
                'status'               => @$battle['status'], //battle状态
                'is_draw'              => @$battle['is_draw'] ? 1 : 2,
                'is_forfeit'           => @$battle['is_forfeit'] ? 1 : 2,
                'is_default_advantage' => @$battle['is_default_advantage'] ? 1 : 2, //是否是默认领先获胜
                'map'                  => @$battle['map_details']['map_id'],
                'map_name'             => @$battle['map_details']['name'],
                'duration'             => @$battle['duration'], //对局时长
                'winner'               => @$battle['winner']['opponent_order'] ? (int)$battle['winner']['opponent_order'] : null, //获胜战队order
                'deleted'               => 2, //获胜战队order
                'deleted_at'               => null, //获胜战队order
            ]);

        } else {
            return ['order' => null];
        }
        $realBattleId = $battle['real_battle_id'];
        // rounds, events
        $this->getBattleRoundsDataByRedis($battleId, $realBattleId, $matchId);
        // battle-players
        $this->getBattlePlayersByBattleId($battle);
        // round-players
        $this->getAllRoundPlayerByBattleId($battleId);
        // static(teams)
        $this->getBattleTeams($battle);

        return [
            'order'         => @intval($battle['real_order']),
            'base'          => self::$battleFieldConfig,
            'static'        => self::$teamsFieldArray,
            'players'       => self::$battlePlayersFieldArray,
            'detail'        => self::$battleDetailFieldConfig,
            'rounds'        => self::$roundsFieldArray,
            'round_players' => self::$roundPlayersFieldArray,
            'events'        => self::$roundEventsFieldArray
        ];
    }

    public function getHistoryBattleCurrentRound($battleId)
    {
        $rounds   = $this->getRoundsByBattleId($battleId);
        $keyArray = array_keys($rounds);
        return count($keyArray);
    }

    /**
     * 获取round_time 回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
     * @param $event
     * @return false|string
     */
    public function getRoundTime($time)
    {
        if ($this->event['event_type'] == 'round_start' || $this->event['event_type'] == 'battle_start') {
            return 115;
        }
        $round_start_time = strtotime($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT]));
        $time             = 115 - (strtotime($time) - $round_start_time);
        return ($time > 0 && $time <= 115) ? $time : 0;
    }

    /**
     * getTimeSincePlant
     * @return mixed
     */
    public function getTimeSincePlant($time)
    {

        //40秒倒计时
        $time_since_plant = 0;
        //转化
        $time_since_plant_time = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_TIME_SINCE_PLANT]);
        if ($time_since_plant_time) {
            $time_since_plant = 40 - (strtotime($time) - strtotime($time_since_plant_time));
        }
        if ($time_since_plant > 40) {
            $time_since_plant = 0;
        }
        $time_since_plant = $time_since_plant > 0 ? $time_since_plant : 0;
        return (int)$time_since_plant;
    }

    /**
     * @param $log_time
     * @return int
     */
    public function getInRoundTimestamp($log_time)
    {

        $round_start_time = strtotime($this->getCurrentRoundBeginAt());
        $time             = strtotime($log_time) - $round_start_time;
        if (empty($round_start_time)) {
            $time = 0;
        }
        return ($time > 0&&$time<=115) ? (int)$time : 0;
    }


    public function setDetails($battle)
    {
        if ($this->currentPre == 'hltv_ws') {
            $log_time   = $this->log_time;
        } else {
            $event      = ReceiveData5eplayFormatEvents::find()->where(['id' => $this->event_id])->asArray()->one();
            $event_info = json_decode($event['info'], true);
            $time       = str_replace(' - ', ' ', $event_info['log_time']);
            $log_time   = date('Y-m-d H:i:s', strtotime($time));
        }

        if ($this->isLive()){
            self::$battleDetailFieldConfig['current_round'] =$this->currentRound();
        }
        $inRoundTime = @intval($this->getInRoundTimestamp($log_time));
        $roundTime = 115 - $inRoundTime;
        if ($roundTime > 0 && $roundTime <= 115) {

        } else {
            $roundTime = 0;
        }
        if ($this->currentPre == 'hltv_ws') {
            $isFreezeTime = null;
            $is_pause = null;
        }else{
            $isFreezeTime= @intval($this->isFreezeTime() ? 1 : 2);
            $is_pause = intval($battle['is_pause'] ? 1 : 2);
        }
        self::$battleDetailFieldConfig = [
            'is_confirmed'       => 1,
            'is_finished'        => @$battle['status'] == 3 ? 1 : 2,
            'is_pause'           => $is_pause,
            'is_live'            => @intval($battle['is_live'] ? 1 : 2),
            'current_round'      => @intval($this->getHistoryBattleCurrentRound($battle['battle_id'])),
            'is_freeze_time'     => $isFreezeTime,
            'in_round_timestamp' => $inRoundTime,
            'round_time'         => @$roundTime,
            'time_since_plant'   => @intval($this->getTimeSincePlant($log_time)),

            'is_bomb_planted'               => @intval($battle['is_bomb_planted'] ? 1 : 2),
            'win_round_1_side'              => @$battle['win_round_1_side'],
            'win_round_1_team'              => @$battle['win_round_1_team']?intval($battle['win_round_1_team']):null,
            'win_round_1_detail'            => @$battle['win_round_1_detail'],
            'win_round_16_side'             => @$battle['win_round_16_side'],
            'win_round_16_team'             => @$battle['win_round_16_team'] ? @intval($battle['win_round_16_team']) : null,
            'win_round_16_detail'           => @$battle['win_round_16_detail'],
            'first_to_5_rounds_wins_side'   => @$battle['first_to_5_rounds_wins_side'],
            'first_to_5_rounds_wins_team'   => @$battle['first_to_5_rounds_wins_team']?intval($battle['first_to_5_rounds_wins_team']):null,
            'first_to_5_rounds_wins_detail' => @$battle['first_to_5_rounds_wins_detail'],
            'live_rounds'                   => @$this->getLiveRoundsId($battle['battle_id'])
        ];

    }


    public function getBattleTeams($battle)
    {
        if (empty($battle['start_ct']['opponent_order'])){
            return [];
        }
        $teamArr                             = [];
        $teamArr[0]['order']                 = $battle['start_ct']['opponent_order'];
        $teamArr[0]['score']                 = $battle['start_ct']['score'];
        $teamArr[0]['game']                  = self::CSGO_GAME_ID;
        $teamArr[0]['identity_id']           = $battle['start_ct']['name'];
        $teamArr[0]['rel_identity_id']       = $battle['start_ct']['name'];
        $teamArr[0]['team_id']               = $battle['start_ct']['team_id'];
        $teamArr[0]['name']                  = $battle['start_ct']['name'];
        $teamArr[0]['starting_faction_side'] = 'ct';
        $teamArr[0]['1st_half_score']        = $battle['start_ct']['1st_half_score'];
        $teamArr[0]['2nd_half_score']        = $battle['start_ct']['2nd_half_score'];
        $teamArr[0]['ot_score']              = $battle['start_ct']['ot_score'];

        $teamArr[0]['kills']                  = @$battle['start_ct']['kills'];
        $teamArr[0]['headshot_kills']         = @$battle['start_ct']['headshot_kills'];
        $teamArr[0]['deaths']                 = @$battle['start_ct']['deaths'];
        $teamArr[0]['kd_diff']                = @$battle['start_ct']['kd_diff'];
        $teamArr[0]['assists']                = @$battle['start_ct']['assists'];
        $teamArr[0]['flash_assists']          = @$battle['start_ct']['flash_assists'];
        $teamArr[0]['adr']                    = @(string)$battle['start_ct']['adr'];
        $teamArr[0]['first_kills']            = @$battle['start_ct']['first_kills'];
        $teamArr[0]['first_deaths']           = @$battle['start_ct']['first_deaths'];
        $teamArr[0]['first_kills_diff']       = @$battle['start_ct']['first_kills_diff'];
        $teamArr[0]['kast']                   = @(string)$battle['start_ct']['kast'];
        $teamArr[0]['rating']                 = @(string)$battle['start_ct']['rating'];
        if ($this->currentPre=='hltv_ws'){
            $teamArr[0]['damage']                 = null;
            $teamArr[0]['team_damage']            = null;
            $teamArr[0]['damage_taken']           = null;
            $teamArr[0]['hegrenade_damage_taken'] = null;
            $teamArr[0]['inferno_damage_taken']   = null;
            $teamArr[0]['chicken_kills']   = null;
            $teamArr[0]['blind_enemy_time']   = null;
            $teamArr[0]['blind_teammate_time']   = null;
        }else{
            $teamArr[0]['damage']                 = @$battle['start_ct']['damage'];
            $teamArr[0]['team_damage']            = @$battle['start_ct']['team_damage'];
            $teamArr[0]['damage_taken']           = @$battle['start_ct']['damage_taken'];
            $teamArr[0]['hegrenade_damage_taken'] = @$battle['start_ct']['hegrenade_damage_taken'];
            $teamArr[0]['inferno_damage_taken']   = @$battle['start_ct']['inferno_damage_taken'];
            $teamArr[0]['chicken_kills']          = @$battle['start_ct']['chicken_kills'];
            $teamArr[0]['blind_enemy_time']       = @(string)$battle['start_ct']['blind_enemy_time'];
            $teamArr[0]['blind_teammate_time']    = @(string)$battle['start_ct']['blind_teammate_time'];
        }

        $teamArr[0]['planted_bomb']           = @$battle['start_ct']['planted_bomb'];
        $teamArr[0]['defused_bomb']           = @$battle['start_ct']['defused_bomb'];


        $teamArr[0]['team_kills']             = @$battle['start_ct']['team_kills'];
        $teamArr[0]['one_kills']              = @(int)$battle['start_ct']['one_kills'];
        $teamArr[0]['multi_kills']              = @$battle['start_ct']['multi_kills'];
        $teamArr[0]['two_kills']              = @$battle['start_ct']['two_kills'];
        $teamArr[0]['three_kills']            = @$battle['start_ct']['three_kills'];
        $teamArr[0]['four_kills']             = @$battle['start_ct']['four_kills'];
        $teamArr[0]['five_kills']             = @$battle['start_ct']['five_kills'];
        $teamArr[0]['one_on_x_clutches']    = @$battle['start_ct']['one_on_x_clutches'];
        $teamArr[0]['one_on_one_clutches']    = @$battle['start_ct']['one_on_one_clutches'];
        $teamArr[0]['one_on_two_clutches']    = @$battle['start_ct']['one_on_two_clutches'];
        $teamArr[0]['one_on_three_clutches']  = @$battle['start_ct']['one_on_three_clutches'];
        $teamArr[0]['one_on_four_clutches']   = @$battle['start_ct']['one_on_four_clutches'];
        $teamArr[0]['one_on_five_clutches']   = @$battle['start_ct']['one_on_five_clutches'];
        $teamArr[0]['hit_generic']            = @$battle['start_ct']['hit_generic'];
        $teamArr[0]['hit_head']               = @$battle['start_ct']['hit_head'];
        $teamArr[0]['hit_chest']              = @$battle['start_ct']['hit_chest'];
        $teamArr[0]['hit_stomach']            = @$battle['start_ct']['hit_stomach'];
        $teamArr[0]['hit_left_arm']           = @$battle['start_ct']['hit_left_arm'];
        $teamArr[0]['hit_right_arm']          = @$battle['start_ct']['hit_right_arm'];
        $teamArr[0]['hit_left_leg']           = @$battle['start_ct']['hit_left_leg'];
        $teamArr[0]['hit_right_leg']          = @$battle['start_ct']['hit_right_leg'];
        $teamArr[0]['awp_kills']              = @$battle['start_ct']['awp_kills'];
        $teamArr[0]['knife_kills']            = @$battle['start_ct']['knife_kills'];
        $teamArr[0]['taser_kills']            = @$battle['start_ct']['taser_kills'];
        $teamArr[0]['shotgun_kills']          = @$battle['start_ct']['shotgun_kills'];

//        $teamArr[0]['camp']                  = $battle['start_ct'][''];
        //terrorist
        $teamArr[1]['order']                 = $battle['start_t']['opponent_order'];
        $teamArr[1]['score']                 = $battle['start_t']['score'];
        $teamArr[1]['game']                  = self::CSGO_GAME_ID;
        $teamArr[1]['identity_id']           = $battle['start_t']['name'];
        $teamArr[1]['rel_identity_id']       = $battle['start_t']['name'];
        $teamArr[1]['team_id']               = $battle['start_t']['team_id'];
        $teamArr[1]['name']                  = $battle['start_t']['name'];
        $teamArr[1]['starting_faction_side'] = 'terrorist';
        $teamArr[1]['1st_half_score']        = $battle['start_t']['1st_half_score'];
        $teamArr[1]['2nd_half_score']        = $battle['start_t']['2nd_half_score'];
        $teamArr[1]['ot_score']              = $battle['start_t']['ot_score'];


        $teamArr[1]['kills']                  = @$battle['start_t']['kills'];
        $teamArr[1]['headshot_kills']         = @$battle['start_t']['headshot_kills'];
        $teamArr[1]['deaths']                 = @$battle['start_t']['deaths'];
        $teamArr[1]['kd_diff']                = @$battle['start_t']['kd_diff'];
        $teamArr[1]['assists']                = @$battle['start_t']['assists'];
        $teamArr[1]['flash_assists']          = @$battle['start_t']['flash_assists'];
        $teamArr[1]['adr']                    = @(string)$battle['start_t']['adr'];
        $teamArr[1]['first_kills']            = @$battle['start_t']['first_kills'];
        $teamArr[1]['first_deaths']           = @$battle['start_t']['first_deaths'];
        $teamArr[1]['first_kills_diff']       = @$battle['start_t']['first_kills_diff'];
        $teamArr[1]['kast']                   = @(string)$battle['start_t']['kast'];
        $teamArr[1]['rating']                 = @(string)$battle['start_t']['rating'];
        if ($this->currentPre == 'hltv_ws') {
            $teamArr[1]['damage']                 = null;
            $teamArr[1]['team_damage']            = null;
            $teamArr[1]['damage_taken']           = null;
            $teamArr[1]['hegrenade_damage_taken'] = null;
            $teamArr[1]['inferno_damage_taken']   = null;
            $teamArr[1]['chicken_kills']   = null;
            $teamArr[1]['blind_enemy_time']   = null;
            $teamArr[1]['blind_teammate_time']   = null;
        } else {
            $teamArr[1]['damage']                 = @$battle['start_t']['damage'];
            $teamArr[1]['team_damage']            = @$battle['start_t']['team_damage'];
            $teamArr[1]['damage_taken']           = @$battle['start_t']['damage_taken'];
            $teamArr[1]['hegrenade_damage_taken'] = @$battle['start_t']['hegrenade_damage_taken'];
            $teamArr[1]['inferno_damage_taken']   = @$battle['start_t']['inferno_damage_taken'];
            $teamArr[1]['chicken_kills']          = @$battle['start_t']['chicken_kills'];
            $teamArr[1]['blind_enemy_time']       = @(string)$battle['start_t']['blind_enemy_time'];
            $teamArr[1]['blind_teammate_time']    = @(string)$battle['start_t']['blind_teammate_time'];
        }

        $teamArr[1]['planted_bomb']           = @$battle['start_t']['planted_bomb'];
        $teamArr[1]['defused_bomb']           = @$battle['start_t']['defused_bomb'];

        $teamArr[1]['team_kills']             = @$battle['start_t']['team_kills'];
        $teamArr[1]['multi_kills']             = @$battle['start_t']['multi_kills'];
        $teamArr[1]['one_kills']              = @(int)$battle['start_t']['one_kills'];
        $teamArr[1]['two_kills']              = @$battle['start_t']['two_kills'];
        $teamArr[1]['three_kills']            = @$battle['start_t']['three_kills'];
        $teamArr[1]['four_kills']             = @$battle['start_t']['four_kills'];
        $teamArr[1]['five_kills']             = @$battle['start_t']['five_kills'];
        $teamArr[1]['one_on_x_clutches']    = @$battle['start_t']['one_on_x_clutches'];
        $teamArr[1]['one_on_one_clutches']    = @$battle['start_t']['one_on_one_clutches'];
        $teamArr[1]['one_on_two_clutches']    = @$battle['start_t']['one_on_two_clutches'];
        $teamArr[1]['one_on_three_clutches']  = @$battle['start_t']['one_on_three_clutches'];
        $teamArr[1]['one_on_four_clutches']   = @$battle['start_t']['one_on_four_clutches'];
        $teamArr[1]['one_on_five_clutches']   = @$battle['start_t']['one_on_five_clutches'];
        $teamArr[1]['hit_generic']            = @$battle['start_t']['hit_generic'];
        $teamArr[1]['hit_head']               = @$battle['start_t']['hit_head'];
        $teamArr[1]['hit_chest']              = @$battle['start_t']['hit_chest'];
        $teamArr[1]['hit_stomach']            = @$battle['start_t']['hit_stomach'];
        $teamArr[1]['hit_left_arm']           = @$battle['start_t']['hit_left_arm'];
        $teamArr[1]['hit_right_arm']          = @$battle['start_t']['hit_right_arm'];
        $teamArr[1]['hit_left_leg']           = @$battle['start_t']['hit_left_leg'];
        $teamArr[1]['hit_right_leg']          = @$battle['start_t']['hit_right_leg'];
        $teamArr[1]['awp_kills']              = @$battle['start_t']['awp_kills'];
        $teamArr[1]['knife_kills']            = @$battle['start_t']['knife_kills'];
        $teamArr[1]['taser_kills']            = @$battle['start_t']['taser_kills'];
        $teamArr[1]['shotgun_kills']          = @$battle['start_t']['shotgun_kills'];
//        $teamArr[1]['camp']                  = 'terrorist';
        self::$teamsFieldArray               = $teamArr;
    }

    /**
     * getRoundPlayerWeaponBySteamId
     * @param $steamId
     * @return array
     */
    public function getRoundPlayerWeaponInfoBySteamId($steamId)
    {
        $weapon_list = $this->getCurrentPlayerWeaponsBySteamId($steamId);
        $list        = [];
        if ($weapon_list) {
            foreach ($weapon_list as $k => $v) {
                if ($v!='vesthelm'){
                    $list[$k] = $this->getWeaponByName($v);
                }
            }
        }
        return $list;
    }


    /**
     * getRoundPlayerGrenadesBySteamId
     * @param $steamId
     * @return array
     */
    public function getRoundPlayerGrenadesInfoBySteamId($steamId)
    {
        $weapon_list = $this->getCurrentPlayerGrenadesBySteamId($steamId);
        $list        = [];
        if ($weapon_list) {
            foreach ($weapon_list as $k => $v) {
                $list[$k] = $this->getWeaponByName($v);
            }
        }
        $list = array_slice($list,0,4);//投掷物最多四个限制容错
        return $list;
    }




    public function getBattlePlayersByBattleId($battle)
    {
        $battleId        = $battle['battle_id'];
        $realBattleOrder = @$battle['real_order'];
        if ($this->currentPre == 'hltv_ws') {
            $playerList      = $this->getBattlePlayerListByType('all', $battleId,'steam_id');
        }else{
            $playerList      = $this->getBattlePlayerListByType('all', $battleId);
        }
        $i               = 0;
        foreach ($playerList as $k => $v) {
            $i++;
            $playerArr                       = [];
            $playerArr['game']               = self::CSGO_GAME_ID;
            $playerArr['order']              = $i;
            $playerArr['team_order']         = @$v['team_order'];
            $playerArr['side']         = @$v['side'];
            $weaponArr = [];
            $weapon_list                          = $this->getRoundPlayerWeaponInfoBySteamId($v['steam_id']);
            if (!empty($weapon_list)) {
                foreach ($weapon_list as $k_weapon => $v_weapon) {
                    if ($v_weapon['id']){
                        $weaponArr[] = (int)$v_weapon['id'];
                    }
                }
                if (!empty($weaponArr)){
                    $playerArr['weapon']         = @json_encode($weaponArr,320);
                }else{
                    $playerArr['weapon'] = json_encode([],320);
                }
            }else{
                $playerArr['weapon'] = json_encode([],320);
            }
            $playerArr['has_kevlar']    = @$v['has_kevlar']?1:2;
            $playerArr['has_helmet']    = @$v['has_helmet']?1:2;
            $playerArr['has_defusekit'] = @$v['has_defusekit']?1:2;
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['has_bomb'] = null;
            }else{
                $playerArr['has_bomb'] = @$v['has_bomb']?1:2;
            }

            $grenades_list                  = $this->getRoundPlayerGrenadesInfoBySteamId($v['steam_id']);
            $grenadesArr =[];
            if (!empty($grenades_list)) {
                foreach ($grenades_list as $k_grenades => $v_grenade) {
                    $grenadesArr[] = (int)$v_grenade['id'];
                }
                if (!empty($grenadesArr)){
                    $playerArr['grenades']         = @json_encode($grenadesArr,320);
                }else{
                    $playerArr['grenades'] = json_encode([],320);
                }
            }else{
                $playerArr['grenades'] = json_encode([],320);
            }

            $playerArr['hp']              = @(int)$v['hp'];
            $playerArr['is_alive']        = @$v['is_alive'] ? 1 : 2;
            $playerArr['money']           = @(int)$v['money'];
            $playerArr['position']        = @(string)$v['position'];

            if ($this->currentPre == 'hltv_ws') {
                $playerArr['equipment_value'] = null;
            }else{
                $weaponMoney = $this->getWeaponCast($weapon_list, $grenades_list,$v['has_kevlar'],$v['has_helmet'],$v['has_defusekit']);
                $playerArr['equipment_value'] = $weaponMoney ? (int)$weaponMoney : 0;
            }

            $playerArr['blinded_time']    = @$v['steam_id'] ? (string)$this->getFlashBlindnessTime($v['steam_id']) : 0;
            if ($playerArr['blinded_time'] > 100) {
                $playerArr['blinded_time'] = (string)0;
            }
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['blinded_time'] = null;
                $playerArr['ping']            = null;
            }else{
                $playerArr['ping']            = @(int)$v['ping'];
            }



            $playerArr['team_id']            = @$v['team_id'];
            $playerArr['player_id']          = @$v['player_id'];
            $playerArr['nick_name']          = @$v['nick_name'];
            $playerArr['kills']              = @$v['kills'];
            $playerArr['headshot_kills']     = @$v['headshot_kills'];
            $playerArr['deaths']             = @$v['deaths'];
            $playerArr['k_d_diff']           = @$v['k_d_diff'];
            $playerArr['assists']            = @$v['assists'];
            $playerArr['flash_assists']      = @$v['flash_assist'];
            $playerArr['adr']                = @$v['adr'];
            $playerArr['first_kills']        = @$v['first_kills'];
            $playerArr['first_deaths']       = @$v['first_deaths'];
            $playerArr['first_kills_diff']   = @$v['first_kills_diff'];
            $playerArr['multi_kills']        = @$v['multi_kills'];
            $playerArr['1_v_n_clutches']     = @$v['one_on_x_clutches'];
            $playerArr['knife_kill']         = @$v['knife_kill'];
            $playerArr['ace_kill']           = @$v['ace_kill'];
            $playerArr['money']              = @$v['money'];
            $playerArr['coordinate']         = (string)$v['coordinate'];
            $playerArr['team_name']          = @$v['team_name'];

            $playerArr['steam_id']           = @$v['steam_id'];
            //5e需要的额外字段
            $playerArr['kast']            = @(string)round($v['kast'],4);
            $playerArr['rating']            = @(string)round($v['rating'],2);
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['damage']            = null;
                $playerArr['team_damage']            = null;
                $playerArr['damage_taken']           = null;
                $playerArr['hegrenade_damage_taken'] = null;
                $playerArr['inferno_damage_taken']   = null;
                $playerArr['chicken_kills']          = null;
                $playerArr['blind_enemy_time']       = null;
                $playerArr['blind_teammate_time']    = null;
            }else{
                $playerArr['damage']             = @$v['real_damage'];
                $playerArr['team_damage']            = @$v['team_damage'];
                $playerArr['damage_taken']           = @$v['damage_taken'];
                $playerArr['hegrenade_damage_taken'] = @$v['hegrenade_damage_taken'];
                $playerArr['inferno_damage_taken']   = @$v['inferno_damage_taken'];
                $playerArr['chicken_kills']          = @$v['chicken_kills'];
                $playerArr['blind_enemy_time']       = @(string)$v['blind_enemy_time'];
                $playerArr['blind_teammate_time']    = @(string)$v['blind_teammate_time'];
            }

            $playerArr['planted_bomb']           = @$v['planted_bomb'];
            $playerArr['defused_bomb']           = @$v['defused_bomb'];
            $playerArr['team_kills']             = @$v['team_kills'];
            $playerArr['one_kills']              = @$v['one_kills'];
            $playerArr['two_kills']              = @$v['two_kills'];
            $playerArr['three_kills']            = @$v['three_kills'];
            $playerArr['four_kills']             = @$v['four_kills'];
            $playerArr['five_kills']             = @$v['five_kills'];
            $playerArr['one_on_one_clutches']    = @$v['one_on_one_clutches'];
            $playerArr['one_on_two_clutches']    = @$v['one_on_two_clutches'];
            $playerArr['one_on_three_clutches']  = @$v['one_on_three_clutches'];
            $playerArr['one_on_four_clutches']   = @$v['one_on_four_clutches'];
            $playerArr['one_on_five_clutches']   = @$v['one_on_five_clutches'];
            $playerArr['hit_generic']            = @$v['hit_generic'];
            $playerArr['hit_head']               = @$v['hit_head'];
            $playerArr['hit_chest']              = @$v['hit_chest'];
            $playerArr['hit_stomach']            = @$v['hit_stomach'];
            $playerArr['hit_left_arm']           = @$v['hit_left_arm'];
            $playerArr['hit_right_arm']          = @$v['hit_right_arm'];
            $playerArr['hit_left_leg']           = @$v['hit_left_leg'];
            $playerArr['hit_right_leg']          = @$v['hit_right_leg'];
            $playerArr['awp_kills']              = @$v['awp_kills'];
            $playerArr['knife_kills']            = @$v['knife_kills'];
            $playerArr['taser_kills']            = @$v['taser_kills'];
            $playerArr['shotgun_kills']          = @$v['shotgun_kills'];
            $playerArr['steam_id']               = @(string)SteamHelper::playerConversionSteamId($v['steam_id']);
            self::$battlePlayersFieldArray[] = $playerArr;
        }
    }

    public function getRoundsByBattleIdOnApi($battleId)
    {
        $i=1;
        $newRounds =[];
        $rounds = $this->getRoundsByBattleId($battleId);
        foreach ($rounds as $k=>$v){
            $newRounds[$i] = $v;
            $newRounds[$i]['round_ordinal_new'] = $i;
            $i++;
        }
        if ($this->updateType == 2) {
            $roundsLength = count($newRounds);
            $round_end_count  = $this->round_end_count;
            $offset =2;
            if ($round_end_count&&is_numeric($round_end_count)){
                $sliceRoundsLength = $round_end_count+$offset;
            }else{
                $sliceRoundsLength = $offset;
            }
            //default refresh 2 rounds
            if ($roundsLength >= $sliceRoundsLength) {
                $newRounds = array_slice($newRounds, -$sliceRoundsLength, $sliceRoundsLength, true);
            }
        }
        return $newRounds;
    }

    public function getBattleRoundsDataByRedis($battleId, $realBattleId, $matchId)
    {
        $doTypeArray = [
            'round_start',
            'round_end',
            'player_kill',
            'bomb_planted',
            'bomb_defused',
            'player_suicide',
        ];
        $rounds = $this->getRoundsByBattleIdOnApi($battleId);
        foreach ($rounds as $key => $round) {
            $roundEvents = $this->getEventsByBattleIdAndRoundOrder($battleId, $round['round_ordinal']);
            $i           = 0;
            if ($roundEvents) {
                $countSuicide = 0;
                foreach ($roundEvents as $k => $v) {
                    if ($v['event_type'] == 'player_suicide') {
                        $countSuicide++;
                    }
                }
                $roundStartCount = 0;
                foreach ($roundEvents as $k => $v) {
                    if (in_array($v['event_type'], $doTypeArray)) {
                        $i++;
                        $v['game']            = self::CSGO_GAME_ID;
                        $v['match']           = $matchId;
                        $v['order']           = $i;
                        $v['is_bomb_planted'] = @$v['is_bomb_planted'] ? 1 : 2;
                        if ($v['event_type'] == 'round_start'){
                            $roundStartCount++;
                        }
                        if ($v['event_type'] == 'round_start'&&$roundStartCount>1) {
                            continue;
                        }
                        if ($v['event_type'] == 'round_end') {
                            if (array_key_exists(@$v['round_end_type'], self::$WinEndTypeArray)) {
                                @$v['round_end_type'] = @(int)self::$WinEndTypeArray[@$v['round_end_type']];
                                $v['winner_side'] = @$v['winner_side'] ?? '';
                            } else {
                                $v['round_end_type'] = null;
                                $v['winner_side']  = '';
                            }
                        }
                        if ($v['event_type'] == 'player_kill') {
                            $v['killer_nick_name'] = @$v['killer']['nick_name'];
                            $v['killer_side']      = @$v['killer']['side'];
                            $v['killer_position']  = @$v['killer']['position'];
                            $v['killer_steam_id']  = @(string)$v['killer']['steam_id'];
                            $v['killer']           = @(int)$v['killer']['player_id'];

                            $v['victim_nick_name'] = @$v['victim']['nick_name'];
                            $v['victim_side']      = @$v['victim']['side'];
                            $v['victim_position']  = @$v['victim']['position'];
                            $v['victim_steam_id']  = @(string)$v['victim']['steam_id'];
                            $v['victim']           = @(int)$v['victim']['player_id'];

                            $v['assist_nick_name'] = @$v['assist']['nick_name'];
                            $v['assist_side']      = @$v['assist']['side'];
                            $v['assist_steam_id']      = @(string)$v['assist']['steam_id'];
                            $v['assist']           = @(int)$v['assist']['player_id'];

                            $v['flashassistt_nick_name'] = @$v['flashassist']['nick_name'];
                            $v['flashassist_side']       = @$v['flashassist']['side'];
                            $v['flashassist_steam_id']   = @(string)$v['flashassist']['steam_id'];
                            $v['flashassist']            = @(int)$v['flashassist']['player_id'];

                            $v['weapon_name'] = @$v['weapon']['weapon_name'];
                            $v['weapon']      = @(int)$v['weapon']['weapon_id'];
                            $v['is_headshot'] = @$v['is_headshot'] ? 1 : 2;
                        }
                        if ($v['event_type'] == 'bomb_planted') {
                            $v['bomb_planted_player_id'] = @(int)$v['player_id'];
                            $v['bomb_planted_nick_name'] = @$v['nick_name'];
                            $v['steam_id'] = @(string)$v['steam_id'];
                            $v['bomb_planted_side']      = @$v['side'];
                            $v['bomb_planted_position']  = @$v['position'];

                        }
                        if ($v['event_type'] == 'bomb_defused') {
                            $v['bomb_defused_player_id'] = @(int)$v['player_id'];
                            $v['bomb_defused_nick_name'] = @$v['nick_name'];
                            $v['steam_id']      = @(string)$v['steam_id'];
                            $v['bomb_defused_side']      = @$v['side'];
                            $v['bomb_defused_position']  = @$v['position'];

                        }
                        if ($v['event_type'] == 'player_suicide' ) {
                            if ($countSuicide < 6){
                                $v['weapon_name'] = @$v['weapon']['weapon_name'];
                                $v['weapon']      = @(int)$v['weapon']['weapon_id'];
                                $v['suicide_player_id'] = @(int)$v['player_id'];
                                $v['suicide_nick_name'] = @$v['nick_name'];
                                $v['steam_id'] = @(string)$v['steam_id'];
                                $v['suicide_side']      = @$v['side'];
                                $v['suicide_position']  = @$v['position'];
                            }else{
                                unset($v);
                            }
                        }
                        if (!empty($v)){
                            self::$roundEventsFieldArray[] = $v;
                        }
                    }
                }
            }
            $j                         = 0;
            $roundArr                  = [];
            $roundArr['battle_id']     = $realBattleId;
            $roundArr['round_ordinal'] = @$round['round_ordinal_new'];
            //winner_team
            if ($round['winner_side']) {
                $roundArr['winner_team_id']                   = @$round['winner_order'];
                $roundArr['winner_team']['team_id']           = @$round['winner_order'];

            } else {
                $roundArr['winner_team_id']    = null;
                $roundArr['winner_team']       = [];
            }
            //info
            $roundArr['begin_at'] = @(string)$round['begin_at'];
            $roundArr['end_at']   = @(string)$round['end_at'];
            $roundArr['status']   = @$round['status'];
            $roundArr['end_time'] = @(string)$round['end_at'];
            $j++;
            //ct
            $roundArr['side']['ct']['order']                    = $j;
            $roundArr['side']['ct']['score']                    = @$round['round_ct_score'];
            $roundArr['side']['ct']['game']                     = self::CSGO_GAME_ID;
            $roundArr['side']['ct']['identity_id']              = @$round['ct']['name'];
            $roundArr['side']['ct']['rel_identity_id']          = @$round['ct']['name'];
            $roundArr['side']['ct']['team_id']                  = @$round['ct']['team_id'];
            $roundArr['side']['ct']['name']                     = @$round['ct']['name'];
            $roundArr['side']['ct']['starting_faction_side']    = @$round['ct']['starting_faction_side'];
            $roundArr['side']['ct']['camp']                     = 'ct';
            $roundArr['side']['ct']['survived_players']         = @$round['survived_players_ct'];
            $roundArr['side']['ct']['is_opening_kill_side']     = @$round['first_kills_side'] == 'ct' ? 1 : 2;
            $roundArr['side']['ct']['is_opening_kill_headshot'] = @$round['is_first_kills_headshot_side'] == 'ct' ? 1 : 2;
            $roundArr['side']['ct']['is_knife_kill']            = @$round['ct_is_knife_kill'] ? 1 : 2;
            $roundArr['side']['ct']['is_ace_kill']              = @$round['ace_kill_side'] == 'ct' ? 1 : 2;
            $roundArr['side']['ct']['kills']                    = @$round['ct_kills']?$round['ct_kills']:0;
            $roundArr['side']['ct']['headshot_kills']           = @$round['ct_headshot_kills']?$round['ct_headshot_kills']:0;
            $roundArr['side']['ct']['side']                     = 'ct';
            $roundArr['side']['ct']['round_ordinal']            = @(int)$round['round_ordinal_new'];
            $roundArr['side']['ct']['side_order']               = @$round['ct']['opponent_order'];

            $roundArr['side']['ct']['is_first_kill']          = @$round['ct']['is_first_kill']?1:2;
            $roundArr['side']['ct']['is_first_death']         = @$round['ct']['is_first_death']?1:2;
            $roundArr['side']['ct']['is_planted_bomb']        = @$round['ct']['is_planted_bomb']?1:2;
            $roundArr['side']['ct']['is_bomb_planted']        = @$round['ct']['is_planted_bomb']?1:2;
            $roundArr['side']['ct']['is_defused_bomb']        = @$round['ct']['is_defused_bomb']?1:2;
            $roundArr['side']['ct']['is_one_on_x_clutch']     = @$round['ct']['is_one_on_x_clutch']?1:2;
            $roundArr['side']['ct']['is_one_on_one_clutch']   = @$round['ct']['is_one_on_one_clutch']?1:2;
            $roundArr['side']['ct']['is_one_on_two_clutch']   = @$round['ct']['is_one_on_two_clutch']?1:2;
            $roundArr['side']['ct']['is_one_on_three_clutch'] = @$round['ct']['is_one_on_three_clutch']?1:2;
            $roundArr['side']['ct']['is_one_on_four_clutch']  = @$round['ct']['is_one_on_four_clutch']?1:2;
            $roundArr['side']['ct']['is_one_on_five_clutch']  = @$round['ct']['is_one_on_five_clutch']?1:2;
            $roundArr['side']['ct']['kills']                  = @$round['ct']['kills']?$round['ct']['kills']:0;
            $roundArr['side']['ct']['headshot_kills']         = @$round['ct']['headshot_kills']?$round['ct']['headshot_kills']:0;
            $roundArr['side']['ct']['deaths']                 = @$round['ct']['deaths']?$round['ct']['deaths']:0;
            $roundArr['side']['ct']['assists']                = @$round['ct']['assists']?$round['ct']['assists']:0;
            $roundArr['side']['ct']['flash_assists']          = @$round['ct']['flash_assists']?$round['ct']['flash_assists']:0;

            if ($this->currentPre == 'hltv_ws') {
                $roundArr['side']['ct']['damage']                 = null;
                $roundArr['side']['ct']['team_damage']            = null;
                $roundArr['side']['ct']['damage_taken']           = null;
                $roundArr['side']['ct']['hegrenade_damage_taken'] = null;
                $roundArr['side']['ct']['inferno_damage_taken']   = null;
                $roundArr['side']['ct']['chicken_kills']          = null;
                $roundArr['side']['ct']['blind_enemy_time']       = null;
                $roundArr['side']['ct']['blind_teammate_time']    = null;
            }else{
                $roundArr['side']['ct']['damage']                 = @$round['ct']['damage'];
                $roundArr['side']['ct']['team_damage']            = @$round['ct']['team_damage'];
                $roundArr['side']['ct']['damage_taken']           = @$round['ct']['damage_taken'];
                $roundArr['side']['ct']['hegrenade_damage_taken'] = @$round['ct']['hegrenade_damage_taken'];
                $roundArr['side']['ct']['inferno_damage_taken']   = @$round['ct']['inferno_damage_taken'];
                $roundArr['side']['ct']['chicken_kills']          = @$round['ct']['chicken_kills'];
                $roundArr['side']['ct']['blind_enemy_time']       = @(string)$round['ct']['blind_enemy_time'];
                $roundArr['side']['ct']['blind_teammate_time']    = @(string)$round['ct']['blind_teammate_time'];
            }

            $roundArr['side']['ct']['team_kills']             = @$round['ct']['team_kills']?$round['ct']['team_kills']:0;
            $roundArr['side']['ct']['multi_kills']            = @$round['ct']['multi_kills']?$round['ct']['multi_kills']:0;
            $roundArr['side']['ct']['two_kills']              = @$round['ct']['two_kills']?$round['ct']['two_kills']:0;
            $roundArr['side']['ct']['three_kills']            = @$round['ct']['three_kills']?$round['ct']['three_kills']:0;
            $roundArr['side']['ct']['four_kills']             = @$round['ct']['four_kills']?$round['ct']['four_kills']:0;
            $roundArr['side']['ct']['five_kills']             = @$round['ct']['five_kills']?$round['ct']['five_kills']:0;
            $roundArr['side']['ct']['hit_generic']            = @$round['ct']['hit_generic']?$round['ct']['hit_generic']:0;
            $roundArr['side']['ct']['hit_head']               = @$round['ct']['hit_head']?$round['ct']['hit_head']:0;
            $roundArr['side']['ct']['hit_chest']              = @$round['ct']['hit_chest']?$round['ct']['hit_chest']:0;
            $roundArr['side']['ct']['hit_stomach']            = @$round['ct']['hit_stomach']?$round['ct']['hit_stomach']:0;
            $roundArr['side']['ct']['hit_left_arm']           = @$round['ct']['hit_left_arm']?$round['ct']['hit_left_arm']:0;
            $roundArr['side']['ct']['hit_right_arm']          = @$round['ct']['hit_right_arm']?$round['ct']['hit_right_arm']:0;
            $roundArr['side']['ct']['hit_left_leg']           = @$round['ct']['hit_left_leg']?$round['ct']['hit_left_leg']:0;
            $roundArr['side']['ct']['hit_right_leg']          = @$round['ct']['hit_right_leg']?$round['ct']['hit_right_leg']:0;
            $roundArr['side']['ct']['awp_kills']              = @$round['ct']['awp_kills']?$round['ct']['awp_kills']:0;
            $roundArr['side']['ct']['knife_kills']            = @$round['ct']['knife_kills']?$round['ct']['knife_kills']:0;
            $roundArr['side']['ct']['taser_kills']            = @$round['ct']['taser_kills']?$round['ct']['taser_kills']:0;
            $roundArr['side']['ct']['shotgun_kills']          = @$round['ct']['shotgun_kills']?$round['ct']['shotgun_kills']:0;

            $first_kill_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_first_kill'];
            $first_details_arr = $this->lRange($first_kill_key,0,-1);
            if (!empty($first_details_arr)){
                $first_details_arr=$this->unSetSpecialEvent($first_details_arr,'opening_kill_details');
                $roundArr['side']['ct']['opening_kill_details'] = $first_details_arr[0];
            }

            $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_bomb_planted'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)){
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'bomb_planted_details');
                $roundArr['side']['ct']['bomb_planted_details'] = $bomb_planted_details_arr[0];
            }

            $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_knife_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)){
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'knife_kill_details');
                foreach ($bomb_planted_details_arr as $k6=>$v6){
                    $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                }
                $json = json_encode($bomb_planted_details_arr,320);
                $roundArr['side']['ct']['knife_kill_details'] = $json;
            }

            $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_taser_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)){
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'taser_kill_details');
                foreach ($bomb_planted_details_arr as $k6=>$v6){
                    $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                }
                $json = json_encode($bomb_planted_details_arr,320);
                $roundArr['side']['ct']['taser_kill_details'] = $json;
            }

            $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_ace_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)){
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'ace_kill_details');
                $roundArr['side']['ct']['ace_kill_details'] = $bomb_planted_details_arr[0];
            }

            $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_team_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)){
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'team_kill_details');
                foreach ($bomb_planted_details_arr as $k6=>$v6){
                    $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                }
                $json = json_encode($bomb_planted_details_arr,320);
                $roundArr['side']['ct']['team_kill_details'] = $json;
            }

            $j++;
            //terrorist
            $roundArr['side']['terrorist']['order']                    = $j;
            $roundArr['side']['terrorist']['score']                    = @$round['round_t_score'];
            $roundArr['side']['terrorist']['game']                     = self::CSGO_GAME_ID;
            $roundArr['side']['terrorist']['identity_id']              = @$round['terrorist']['name'];
            $roundArr['side']['terrorist']['rel_identity_id']          = @$round['terrorist']['name'];
            $roundArr['side']['terrorist']['team_id']                  = @$round['terrorist']['team_id'];
            $roundArr['side']['terrorist']['name']                     = @$round['terrorist']['name'];
            $roundArr['side']['terrorist']['starting_faction_side']    = @$round['terrorist']['starting_faction_side'];
            $roundArr['side']['terrorist']['camp']                     = 'terrorist';
            $roundArr['side']['terrorist']['survived_players']         = @$round['survived_players_t'];
            $roundArr['side']['terrorist']['is_opening_kill_side']     = @$round['first_kills_side'] == 'terrorist' ? 1 : 2;
            $roundArr['side']['terrorist']['is_opening_kill_headshot'] = @$round['is_first_kills_headshot_side'] == 'terrorist' ? 1 : 2;
            $roundArr['side']['terrorist']['is_bomb_planted']          = @$round['is_bomb_planted'] ? 1 : 2;
            $roundArr['side']['terrorist']['is_knife_kill']            = @$round['terrorist_is_knife_kill'] ? 1 : 2;
            $roundArr['side']['terrorist']['is_ace_kill']              = @$round['ace_kill_side'] == 'terrorist' ? 1 : 2;
            $roundArr['side']['terrorist']['kills']                    = @$round['terrorist_kills']?$round['terrorist_kills']:0;
            $roundArr['side']['terrorist']['headshot_kills']           = @$round['terrorist_headshot_kills']?$round['terrorist_headshot_kills']:0;
            $roundArr['side']['terrorist']['side']                     = 'terrorist';
            $roundArr['side']['terrorist']['round_ordinal']            = @(int)$round['round_ordinal_new'];
            $roundArr['side']['terrorist']['side_order']               = @$round['terrorist']['opponent_order'];

            $roundArr['side']['terrorist']['is_first_kill']          = @$round['terrorist']['is_first_kill']?1:2;
            $roundArr['side']['terrorist']['is_first_death']         = @$round['terrorist']['is_first_death']?1:2;
            $roundArr['side']['terrorist']['is_planted_bomb']        = @$round['terrorist']['is_planted_bomb']?1:2;
            $roundArr['side']['terrorist']['is_bomb_planted']        = @$round['terrorist']['is_planted_bomb']?1:2;
            $roundArr['side']['terrorist']['is_defused_bomb']        = @$round['terrorist']['is_defused_bomb']?1:2;
            $roundArr['side']['terrorist']['is_one_on_x_clutch']     = @$round['terrorist']['is_one_on_x_clutch']?1:2;
            $roundArr['side']['terrorist']['is_one_on_one_clutch']   = @$round['terrorist']['is_one_on_one_clutch']?1:2;
            $roundArr['side']['terrorist']['is_one_on_two_clutch']   = @$round['terrorist']['is_one_on_two_clutch']?1:2;
            $roundArr['side']['terrorist']['is_one_on_three_clutch'] = @$round['terrorist']['is_one_on_three_clutch']?1:2;
            $roundArr['side']['terrorist']['is_one_on_four_clutch']  = @$round['terrorist']['is_one_on_four_clutch']?1:2;
            $roundArr['side']['terrorist']['is_one_on_five_clutch']  = @$round['terrorist']['is_one_on_five_clutch']?1:2;
            $roundArr['side']['terrorist']['kills']                  = @$round['terrorist']['kills']?$round['terrorist']['kills']:0;
            $roundArr['side']['terrorist']['headshot_kills']         = @$round['terrorist']['headshot_kills']?$round['terrorist']['headshot_kills']:0;
            $roundArr['side']['terrorist']['deaths']                 = @$round['terrorist']['deaths']?$round['terrorist']['deaths']:0;
            $roundArr['side']['terrorist']['assists']                = @$round['terrorist']['assists']?$round['terrorist']['assists']:0;
            $roundArr['side']['terrorist']['flash_assists']          = @$round['terrorist']['flash_assists']?$round['terrorist']['flash_assists']:0;
            if ($this->currentPre == 'hltv_ws') {
                $roundArr['side']['terrorist']['damage']                 = null;
                $roundArr['side']['terrorist']['team_damage']            = null;
                $roundArr['side']['terrorist']['damage_taken']           = null;
                $roundArr['side']['terrorist']['hegrenade_damage_taken'] = null;
                $roundArr['side']['terrorist']['inferno_damage_taken']   = null;
                $roundArr['side']['terrorist']['chicken_kills']          = null;
                $roundArr['side']['terrorist']['blind_enemy_time']       = null;
                $roundArr['side']['terrorist']['blind_teammate_time']    = null;
            }else{
                $roundArr['side']['terrorist']['damage']                 = @$round['terrorist']['damage'];
                $roundArr['side']['terrorist']['team_damage']            = @$round['terrorist']['team_damage'];
                $roundArr['side']['terrorist']['damage_taken']           = @$round['terrorist']['damage_taken'];
                $roundArr['side']['terrorist']['hegrenade_damage_taken'] = @$round['terrorist']['hegrenade_damage_taken'];
                $roundArr['side']['terrorist']['inferno_damage_taken']   = @$round['terrorist']['inferno_damage_taken'];
                $roundArr['side']['terrorist']['chicken_kills']          = @$round['terrorist']['chicken_kills'];
                $roundArr['side']['terrorist']['blind_enemy_time']       = @(string)$round['terrorist']['blind_enemy_time'];
                $roundArr['side']['terrorist']['blind_teammate_time']    = @(string)$round['terrorist']['blind_teammate_time'];
            }

            $roundArr['side']['terrorist']['team_kills']             = @$round['terrorist']['team_kills']?:0;
            $roundArr['side']['terrorist']['multi_kills']            = @$round['terrorist']['multi_kills']?:0;
            $roundArr['side']['terrorist']['two_kills']              = @$round['terrorist']['two_kills']?:0;
            $roundArr['side']['terrorist']['three_kills']            = @$round['terrorist']['three_kills']?:0;
            $roundArr['side']['terrorist']['four_kills']             = @$round['terrorist']['four_kills']?:0;
            $roundArr['side']['terrorist']['five_kills']             = @$round['terrorist']['five_kills']?:0;
            $roundArr['side']['terrorist']['hit_generic']            = @$round['terrorist']['hit_generic']?:0;
            $roundArr['side']['terrorist']['hit_head']               = @$round['terrorist']['hit_head']?:0;
            $roundArr['side']['terrorist']['hit_chest']              = @$round['terrorist']['hit_chest']?:0;
            $roundArr['side']['terrorist']['hit_stomach']            = @$round['terrorist']['hit_stomach']?:0;
            $roundArr['side']['terrorist']['hit_left_arm']           = @$round['terrorist']['hit_left_arm']?:0;
            $roundArr['side']['terrorist']['hit_right_arm']          = @$round['terrorist']['hit_right_arm']?:0;
            $roundArr['side']['terrorist']['hit_left_leg']           = @$round['terrorist']['hit_left_leg']?:0;
            $roundArr['side']['terrorist']['hit_right_leg']          = @$round['terrorist']['hit_right_leg']?:0;
            $roundArr['side']['terrorist']['awp_kills']              = @$round['terrorist']['awp_kills']?:0;
            $roundArr['side']['terrorist']['knife_kills']            = @$round['terrorist']['knife_kills']?:0;
            $roundArr['side']['terrorist']['taser_kills']            = @$round['terrorist']['taser_kills']?:0;
            $roundArr['side']['terrorist']['shotgun_kills']          = @$round['terrorist']['shotgun_kills']?:0;
            //terrorist_first_kill
            $first_kill_key = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_first_kill'];
            $first_details_arr = $this->lRange($first_kill_key,0,-1);
            if (!empty($first_details_arr)) {
                $first_details_arr=$this->unSetSpecialEvent($first_details_arr,'opening_kill_details');
                $roundArr['side']['terrorist']['opening_kill_details'] = $first_details_arr[0];
            }
            //terrorist_bomb_planted
            $bomb_planted_key         = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_bomb_planted'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)) {
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'bomb_planted_details');
                $roundArr['side']['terrorist']['bomb_planted_details'] = $bomb_planted_details_arr[0];
            }
            //terrorist_knife_kill
            $bomb_planted_key         = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_knife_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)) {
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'knife_kill_details');
                foreach ($bomb_planted_details_arr as $k6 => $v6) {
                    $bomb_planted_details_arr[$k6] = json_decode($v6, true);
                }
                $json                                                = json_encode($bomb_planted_details_arr, 320);
                $roundArr['side']['terrorist']['knife_kill_details'] = $json;
            }
            //terrorist_taser_kill
            $bomb_planted_key         = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_taser_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)) {
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'taser_kill_details');
                foreach ($bomb_planted_details_arr as $k6 => $v6) {
                    $bomb_planted_details_arr[$k6] = json_decode($v6, true);
                }
                $json                                                = json_encode($bomb_planted_details_arr, 320);
                $roundArr['side']['terrorist']['taser_kill_details'] = $json;
            }
            //terrorist_ace_kill
            $bomb_planted_key         = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_ace_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)) {
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'ace_kill_details');
                $roundArr['side']['terrorist']['ace_kill_details'] = $bomb_planted_details_arr[0];
            }
            //terrorist_team_kill
            $bomb_planted_key         = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundArr['round_ordinal'], 'terrorist_team_kill'];
            $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
            if (!empty($bomb_planted_details_arr)) {
                $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'team_kill_details');
                foreach ($bomb_planted_details_arr as $k6 => $v6) {
                    $bomb_planted_details_arr[$k6] = json_decode($v6, true);
                }
                $json                                               = json_encode($bomb_planted_details_arr, 320);
                $roundArr['side']['terrorist']['team_kill_details'] = $json;
            }

            //winner_end_type
            $roundArr['winner_end_type'] = array_key_exists($round['round_win_type'], self::$WinEndTypeArray) ? self::$WinEndTypeArray[$round['round_win_type']] : null;
            if ($round['ct']['team_id']){
                self::$roundsFieldArray[]    = $roundArr;
            }
        }
        return true;
    }


    public function unSetSpecialEvent($first_details_arr,$type){
        $newArr = [];
        foreach ($first_details_arr as $k=>$v){
            $vNewArr = [];
            $vArr = json_decode($v,true);
            if ($type == 'opening_kill_details') {
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['killer'] = $vArr['killer'];
                $vNewArr['victim'] = $vArr['victim'];
                $vNewArr['assist'] = $vArr['assist'];
                $vNewArr['flashassist'] = $vArr['flashassist'];
                $vNewArr['weapon'] = $vArr['weapon'];
                $vNewArr['damage'] = $vArr['damage'];
                $vNewArr['hit_group'] = $vArr['hit_group'];
                $vNewArr['is_headshot'] = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }elseif ($type == 'bomb_planted_details'){
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['player_id'] = $vArr['player_id'];
                $vNewArr['nick_name'] = $vArr['nick_name'];
                $vNewArr['steam_id'] = $vArr['steam_id'];
                $vNewArr['side'] = $vArr['side'];
                $vNewArr['position'] = $vArr['position'];
                $vNewArr['survived_players_ct'] = $vArr['survived_players_ct'];
                $vNewArr['survived_players_t'] = $vArr['survived_players_t'];
            }elseif ($type == 'knife_kill_details'){
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['killer'] = $vArr['killer'];
                $vNewArr['victim'] = $vArr['victim'];
                $vNewArr['assist'] = $vArr['assist'];
                $vNewArr['flashassist'] = $vArr['flashassist'];
                $vNewArr['weapon'] = $vArr['weapon'];
                $vNewArr['damage'] = $vArr['damage'];
                $vNewArr['hit_group'] = $vArr['hit_group'];
                $vNewArr['is_headshot'] = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }elseif ($type == 'taser_kill_details'){
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['killer'] = $vArr['killer'];
                $vNewArr['victim'] = $vArr['victim'];
                $vNewArr['assist'] = $vArr['assist'];
                $vNewArr['flashassist'] = $vArr['flashassist'];
                $vNewArr['weapon'] = $vArr['weapon'];
                $vNewArr['damage'] = $vArr['damage'];
                $vNewArr['hit_group'] = $vArr['hit_group'];
                $vNewArr['is_headshot'] = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }elseif ($type == 'ace_kill_details'){
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['killer'] = $vArr['killer'];
                $vNewArr['victim'] = $vArr['victim'];
                $vNewArr['assist'] = $vArr['assist'];
                $vNewArr['flashassist'] = $vArr['flashassist'];
                $vNewArr['weapon'] = $vArr['weapon'];
                $vNewArr['damage'] = $vArr['damage'];
                $vNewArr['hit_group'] = $vArr['hit_group'];
                $vNewArr['is_headshot'] = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }elseif ($type == 'team_kill_details'){
                $vNewArr['in_round_timestamp'] = $vArr['in_round_timestamp'];
                $vNewArr['round_time'] = $vArr['round_time'];
                $vNewArr['is_bomb_planted'] = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant'] = $vArr['time_since_plant'];
                $vNewArr['killer'] = $vArr['killer'];
                $vNewArr['victim'] = $vArr['victim'];
                $vNewArr['assist'] = $vArr['assist'];
                $vNewArr['flashassist'] = $vArr['flashassist'];
                $vNewArr['weapon'] = $vArr['weapon'];
                $vNewArr['damage'] = $vArr['damage'];
                $vNewArr['hit_group'] = $vArr['hit_group'];
                $vNewArr['is_headshot'] = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }
            $newArr[$k] = json_encode($vNewArr,320);
        }
        return $newArr;

    }


    public function getAllRoundPlayerByBattleId($battleId)
    {

        $rounds = $this->getRoundsByBattleIdOnApi($battleId);
        foreach ($rounds as $k => $v) {
            $roundPlayers = $this->getRoundPlayersByRoundOrderAndBattleId($battleId, $v['round_ordinal'],$this->currentPre);
            if ($roundPlayers) {
                $roundCtId = $v['ct']['team_id'];
                $roundTId  = $v['terrorist']['team_id'];
//                $playerOrder = 0;
                $i = 0;
                $arrayPlayerIdAll = array_column($roundPlayers,'player_id');
//                $arrayPlayerIdAll = array_column($list_all,'player_id');
                if (!empty($arrayPlayerIdAll)){
                    $playerUn = array_unique($arrayPlayerIdAll);
                    $repeat_arr = array_diff_assoc ( $arrayPlayerIdAll, $playerUn );
                    if (!empty($repeat_arr)){
                        foreach ($roundPlayers as $k3=>$v3){
                            if (in_array($v3['player_id'],$repeat_arr)){
                                unset($roundPlayers[$k3]);
                                $repeatKey = array_search($v3['player_id'],$repeat_arr);
                                unset($repeat_arr[$repeatKey]);
                            }
                        }
                    }
                }
                foreach ($roundPlayers as $k_player => $v_player) {

                    $i++;
                    $playerArr                  = [];
                    $playerArr['game']          = self::CSGO_GAME_ID;
                    $playerArr['order']         = $i;
                    $playerArr['player_order']  = @$v_player['team_order'];
                    $playerArr['round_ordinal'] = @$v['round_ordinal_new'];
                    //get round player side by teamId
                    if (@$v_player['team_id'] == $roundCtId) {
                        $side = 'ct';
                    } else {
                        $side = 'terrorist';
                    }
                    $playerArr['side']              = @$side;
                    $playerArr['team_id']           = @$v_player['team_id'];
                    $playerArr['player_id']         = @$v_player['player_id'];
                    $playerArr['nick_name']         = @$v_player['nick_name'];
                    $playerArr['kills']             = @$v_player['kills']?$v_player['kills']:0;
                    $playerArr['headshot_kills']    = @$v_player['headshot_kills']?$v_player['headshot_kills']:0;
                    $playerArr['is_died']           = @$v_player['is_alive'] ? 2 : 1;
                    $playerArr['assists']           = @$v_player['assists']?$v_player['assists']:0;
                    $playerArr['flash_assists']     = @$v_player['flash_assist']?$v_player['flash_assist']:0;

                    $playerArr['is_first_kill']     = @$v_player['first_kills'] ? 1 : 2;
                    $playerArr['is_first_death']    = @$v_player['first_deaths'] ? 1 : 2;
                    $playerArr['is_multi_kill']     = @$v_player['multi_kills'] ? 1 : 2;
                    $playerArr['is_1_v_n_clutche']  = @$v_player['one_on_x_clutches'] ? 1 : 2;
                    $playerArr['is_knife_kill']     = @$v_player['knife_kill'] ? 1 : 2;
                    $playerArr['is_ace_kill']       = @$v_player['ace_kill'] ? 1 : 2;
                    $playerArr['steam_id']          = @$v_player['steam_id'];
                    $playerArr['camp']              = @$side;
                    $playerArr['player_name']       = @$v_player['playerName'];
                    $playerArr['team_name']         = @$v_player['team_name'];
                    //额外字段
                    if ($this->currentPre == 'hltv_ws') {
                        $playerArr['damage']                 = null;
                        $playerArr['team_damage']            = null;
                        $playerArr['damage_taken']           = null;
                        $playerArr['hegrenade_damage_taken'] = null;
                        $playerArr['inferno_damage_taken']   = null;
                        $playerArr['chicken_kills']          = null;
                        $playerArr['blind_enemy_time']       = null;
                        $playerArr['blind_teammate_time']    = null;
                    } else {
                        $playerArr['damage']                 = @$v_player['real_damage'];
                        $playerArr['team_damage']            = @$v_player['team_damage'];
                        $playerArr['damage_taken']           = @$v_player['damage_taken'];
                        $playerArr['hegrenade_damage_taken'] = @$v_player['hegrenade_damage_taken'];
                        $playerArr['inferno_damage_taken']   = @$v_player['inferno_damage_taken'];
                        $playerArr['chicken_kills']          = @$v_player['chicken_kills'];
                        $playerArr['blind_enemy_time']       = @(string)$v_player['blind_enemy_time'];
                        $playerArr['blind_teammate_time']    = @(string)$v_player['blind_teammate_time'];
                    }

                    $playerArr['is_planted_bomb']        = @$v_player['planted_bomb']?1:2;
                    $playerArr['is_defused_bomb']        = @$v_player['defused_bomb']?1:2;

                    $playerArr['team_kills']             = @$v_player['team_kills']?$v_player['team_kills']:0;
                    $playerArr['one_kills']              = @$v_player['one_kills']?$v_player['one_kills']:0;
                    $playerArr['two_kills']              = @$v_player['two_kills']?$v_player['two_kills']:0;
                    $playerArr['three_kills']            = @$v_player['three_kills']?$v_player['three_kills']:0;
                    $playerArr['four_kills']             = @$v_player['four_kills']?$v_player['four_kills']:0;
                    $playerArr['five_kills']             = @$v_player['five_kills']?$v_player['five_kills']:0;
                    $playerArr['is_one_on_one_clutch']   = @$v_player['one_on_one_clutches']?1:2;
                    $playerArr['is_one_on_two_clutch']   = @$v_player['one_on_two_clutches']?1:2;
                    $playerArr['is_one_on_three_clutch'] = @$v_player['one_on_three_clutches']?1:2;
                    $playerArr['is_one_on_four_clutch']  = @$v_player['one_on_four_clutches']?1:2;
                    $playerArr['is_one_on_five_clutch']  = @$v_player['one_on_five_clutches']?1:2;
                    $playerArr['hit_generic']            = @$v_player['hit_generic']?$v_player['hit_generic']:0;
                    $playerArr['hit_head']               = @$v_player['hit_head']?$v_player['hit_head']:0;
                    $playerArr['hit_chest']              = @$v_player['hit_chest']?$v_player['hit_chest']:0;
                    $playerArr['hit_stomach']            = @$v_player['hit_stomach']?$v_player['hit_stomach']:0;
                    $playerArr['hit_left_arm']           = @$v_player['hit_left_arm']?$v_player['hit_left_arm']:0;
                    $playerArr['hit_right_arm']          = @$v_player['hit_right_arm']?$v_player['hit_right_arm']:0;
                    $playerArr['awp_kills']              = @$v_player['awp_kills']?$v_player['awp_kills']:0;
                    $playerArr['knife_kills']            = @$v_player['knife_kills']?$v_player['knife_kills']:0;
                    $playerArr['taser_kills']            = @$v_player['taser_kills']?$v_player['taser_kills']:0;
                    $playerArr['shotgun_kills']          = @$v_player['shotgun_kills']?$v_player['shotgun_kills']:0;
                    $playerArr['steam_id']               = @(string)SteamHelper::playerConversionSteamId($v_player['steam_id']);
                    self::$roundPlayersFieldArray[] = $playerArr;
                }
            }
        }
        return true;
    }


    // 初始化
    public function initializationBattleInfo()
    {
        self::$battlePlayersFieldArray = [];
        self::$roundPlayersFieldArray  = [];
        self::$teamsFieldArray         = [];
        self::$battleFieldConfig       = [];
        self::$battleDetailFieldConfig = [];
        self::$roundsFieldArray        = [];
        self::$roundEventsFieldArray   = [];
    }


    private static function refresh($matchId, $formatInfo)
    {
        // todo getMountedId,有可能会找不到，找不到的留空
        self::setMatch($matchId, $formatInfo['match']);
        foreach ($formatInfo['battles'] as $battle) {
            self::setBattle($matchId, $battle);
        }
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $battle['order'],
        ])->one();
        if (!$battleBase) {
            $battleBase = new MatchBattle();
            $battleBase->setAttributes([
                'match' => $matchId,
                'order' => $battle['order'],
            ]);
            $battleBase->save();
        }
        $battleId                = $battleBase['id'];
        $battle['base']['match'] = $matchId;
        BattleService::setBattleInfo($battleId, 'csgo', $battle['base'], BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['detail'], BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['static'], BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['players'], BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['rounds'], BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['events'], BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['round_players'], BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    public static function getBattleFormat()
    {

    }


    /**
     * @return string
     */
    public function getBattles()
    {
        return $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST]);
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}