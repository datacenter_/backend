<?php


namespace app\modules\task\services\hot\five\csgo;


use app\modules\task\services\logformat\CsgoLogFormatBase;
use yii\base\WidgetEvent;

class Battle extends FormatBase
{
    /**
     * @var Round
     */
    private $currentRound;
    private $disturbRounds;
    private $base = [
//        'id' => 'ID',
        'game' => '1',
        'match' => '',
        'order' => '',
        'begin_at' => '',//实际开始时间
        'end_at' => '',//实际结束时间
        'status' => '',//状态
        'is_draw' => '',//是否平局
        'is_forfeit' => '',//是否弃权
        'is_default_advantage' => '',//是否是默认领先获胜
        'map' => '',//比赛地图
        'map_name' => '',//比赛地图名称
        'duration' => '',//对局时长
        'winner' => '',//获胜战队
        'is_battle_detailed' => '1',
//        'modified_at' => '变更日期',
//        'created_at' => '创建日期',
//        'deleted_at' => '删除日期',
    ];
    private $battleDetail = [
        //'id' => 'ID',
        'is_confirmed' => '1',//Is Confirmed
        'is_finished' => '2',//Is Finished
        'is_pause' => '2',//Is Pause
        'is_live' => '1',//Is Live
        'current_round' => '1',//Current Round
        'is_bomb_planted' => '2',//Is Bomb Planted
        'win_round_1_side' => '',//Win Round 1 Side
        'win_round_1_team' => '',//Win Round 1 Team
        'win_round_1_detail' => '',//Win Round 1 Detail
        'win_round_16_side' => '',//Win Round 16 Side
        'win_round_16_team' => '',//Win Round 16 Team
        'win_round_16_detail' => '',//Win Round 16 Detail
        'first_to_5_rounds_wins_side' => '',//First To 5 Rounds Wins Side
        'first_to_5_rounds_wins_team' => '',//First To 5 Rounds Wins Team
        'first_to_5_rounds_wins_detail' => '',//First To 5 Rounds Wins Detail
        'first_to_5_rounds_wins_team_name' => '',
        'live_rounds' => '',//Live Rounds
        'begin_at' => '',//Live Rounds
        'modified_at' => '',//Modified At
        'created_at' => '',//Created At
        'deleted_at' => '',//Deleted At
    ];
    private $rounds = [];
    private $isPause = 2;
    private $teams = [];
    private $players = [];
    private $events = [];
    // 总伤害
    private $teamInfo = [
        'id' => 'ID',
        'starting_faction_side' => 'Starting Faction Side',
        '1st_half_score' => '1st Half Score',
        '2nd_half_score' => '2nd Half Score',
        'ot_score' => 'Ot Score',
        'score' => '',
    ];
    private $playerInfo = [
//        'id' => 'ID',
//        'battle_id' => 'Battle ID',
        'game' => '1',
//        'match' => '',
        'order' => '',
        'team_order' => '',
//        'is_pause' => 'Is Pause',
//        'is_live' => 'Is Live',?
        'team_id' => '',
        'player_id' => '',
        'nick_name' => '',
        'kills' => 0,
        'headshot_kills' => 0,
        'deaths' => 0,
        'k_d_diff' => 0,//击杀死亡差
        'assists' => 0,
        'flash_assists' => 0,
        'adr' => 0,//平均每局伤害
        'first_kills' => 0,
        'first_deaths' => 0,
        'first_kills_diff' => 0,
        'multi_kills' => 0,
        '1_v_n_clutches' => 0,
        'knife_kill' => 0,
        'ace_kill' => 0,
//        'kast' => 'Kast',
//        'rating' => 'Rating',
//        'primary_weapon' => 'Primary Weapon',
//        'secondary_weapon' => 'Secondary Weapon',
//        'knive' => 'Knive',
//        'grenades' => 'Grenades',
//        'kevlar' => 'Kevlar',
//        'helmet' => 'Helmet',
//        'defusekit' => 'Defusekit',
//        'C4' => 'C4',
//        'hp' => 'Hp',
//        'is_alive' => 2,
        'money' => 0,
        'coordinate' => '',
//        'modified_at' => 'Modified At',
//        'created_at' => 'Created At',
//        'deleted_at' => 'Deleted At',
    ];

    /**
     * @var string
     * log关联的唯一标识
     */
    private $battleRelIdentity="";

    /**
     * @return string
     */
    public function getBattleRelIdentity(): string
    {
        return $this->battleRelIdentity;
    }

    /**
     * @param string $battleRelIdentity
     */
    public function setBattleRelIdentity(string $battleRelIdentity): void
    {
        $this->battleRelIdentity = $battleRelIdentity;
    }

    public function __construct()
    {
        $this->currentRound = new Round();
        $this->currentRound->setStatus(self::STATUS_NO_START);
        if(count($this->teams)){
            $this->currentRound->teamSetUpInit($this->teams);
        }
        $this->currentRound->setDetail(['round_ordinal'=>count($this->rounds)+1]);
    }

    public function addEvent(Event $event)
    {
        $attr=$event->getAtts();
        $attr;
        switch ($event->getEventType()){
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_START:
                $this->setStatus(self::STATUS_ON_GOING);
                $attrs = $event->getAtts();
                $logMapName = $attrs['log_map_name'];
                $this->setBeginAt($event->getEventTime());
                $this->setBattleBase(['map_name' => $logMapName,'begin_at'=>$event->getEventTime(),'status'=>self::STATUS_ON_GOING]);
                // 设置战队信息
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT])){
                    $this->setTeams(new Event($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT][0]));
                }
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T])){
                    $this->setTeams(new Event($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T][0]));
                }
                $this->currentRound->teamSetUpInit($this->teams);
                break;
                //比赛地图名称
            case CsgoLogFormatBase::EVENT_TYPE_MAP_START:
                // 记录地图
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_PAUSE:
            case CsgoLogFormatBase::EVENT_TYPE_MP_PAUSE_MATCH:
                $this->setPause(1,$event);
                // 比赛暂停
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_UNPAUSE:
            case CsgoLogFormatBase::EVENT_TYPE_MP_UNPAUSE_MATCH:
                $this->setPause(2,$event);
                // 比赛暂停结束
                break;
//            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T:
//            case CsgoLogFormatBase::EVENT_TYPE_TEAM_PLAYING_T:
//                // 选择阵营
//                $this->setTeams($event);
//                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_SWITCH_TEAM:
            case CsgoLogFormatBase::EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH:
                // 比赛换边
                $this->switchTeam($event);
                break;
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_START:
                // 比赛开始，检查当前round，如果状态不是进行中，则修改状态，跑到废弃list
                if($this->currentRound->getStatus()!=self::STATUS_NO_START){
                    if($this->currentRound->getStatus()==3){
                        $this->roundPush();
                    }else{
                        $this->roundPush($this->currentRound->getStatus());
                    }

                }
                $this->currentRound->teamSetUpInit($this->teams);
                break;
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_END:
                // 记录比分，ct和t
                $attr=$event->getAtts();
                $scores=@$attr['ext_log']['win_type'][0];
                if($scores){
                    $this->setScore($scores['t_score'],$scores['ct_score']);
                }
                // 如果是回档事件
                if(isset($attr['ext_log']['round_draw'])){
                    $scores=@$attr['ext_log']['round_draw'][0];
                    if($scores){
                        $this->setScore($scores['t_score'],$scores['ct_score']);
                    }
                }
                if(isset($attr['ext_log']['match_reloaded'])){
                    return ;
                }
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_RELOADED:
                // 回滚round
                $attr=$event->getAtts();
                $roundTo=@$attr['ext_log']['round_to'][0]['round_to'];
                if($roundTo){
                    // 删除多出来的回合
//                    $roundTmp=[];
                    $this->rounds=array_slice($this->rounds,0,$roundTo);
                }
                $this->roundPush(4);
                // 设置战队分组
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT])){
                    $this->setTeams(new Event($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT][0]),true);
                }
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_TEAM_PLAYING_T])){
                    $this->setTeams(new Event($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_TEAM_PLAYING_T][0]),true);
                }
                $this->players=[];
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_END:
                // battle结束，这里应该是在round结束之后，所以没问题
                // battle如果没开始，就不要标结束
                if($this->getStatus()==self::STATUS_NO_START){
                    return ;
                }
                $this->setStatus(self::STATUS_FINISHED);
                $this->setEndAt($event->getEventTime());
                $this->setBattleBase(['begin_at'=>$this->beginAt,'end_at'=>$this->endAt,'status'=>self::STATUS_FINISHED]);
                $this->setBattleBase(['duration'=>$this->getDateCreTime($this->endAt,$this->beginAt)]);
                // 设置获胜方
                $eventAttr = $event->getAtts();
                $winCamp = @$eventAttr['ext_log']['win_type'][0]['log_win_camp'];
                $winnerTeam = [];
                foreach ($this->teams as $team) {
                    if ($team['camp'] == $winCamp) {
                        $winnerTeam = $team;
                    }
                }
                if($this->currentRound->getStatus() == 3){
                    $this->roundPush();
                }
                $this->setBattleBase(['winner_team'=>$winnerTeam]);
                return;
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_REL_IDENTITY:
                // 设置battle的标识
                $attr=$event->getAtts();
                $battleRelIdentity=$attr['battle_identity'];
                $this->setBattleRelIdentity($battleRelIdentity);
                return;
                break;
            default:
                break;
        }
        // 算出对局时长
        $eventTime = floatval($event->getEventTime());
        $beginTime = floatval($this->beginAt);
        $this->setBattleBase(['duration'=>$this->getDateCreTime($eventTime,$beginTime)]);
        // 如果已经结束的比赛，是不可能再接收事件的，这里加一层验证尝试我的设想
        if($this->getStatus()==3){
            throw new \Exception('夸张了兄弟，这battle结束了，咋还能进来事件儿的？！');
        }
        if($this->getStatus()==self::STATUS_ON_GOING){
            // 根据时间和状态结束当前局，如果对局是结束状态的时候，距离结束时间内5s的时间，计入当前局，时间大于5秒，则刷出新局
            if ($this->currentRound->getStatus() == self::STATUS_FINISHED) {
                $timeDiff=self::getTimeSecondsDiff($this->currentRound->getEndAt(),$event->getEventTime());
                if($timeDiff>=5){
                    $this->roundPush();
                }
            }
            $this->currentRound->addEvent($event);
            $this->currentRound->getStatus();
        }
    }

    public static function getTimeSecondsDiff($first,$last)
    {
        $firstTimestamp=$first/1000;
        $lastTimestamp=$last/1000;
        return $lastTimestamp-$firstTimestamp;
    }
    // 日志时间转时间戳
    public function getDateCreTime($date,$beginTime){
        if($date && $date!=$beginTime) {
//            list($usec, $sec) = explode(".", $date);
//            $time = substr($usec, 0, 21);
//            $time = str_replace(' - ', ' ', $time);
//            $time = (strtotime($time).$sec-$beginTime)/1000;
            $time = (floatval($date) - floatval($beginTime))/1000;
            return sprintf("%.2f",substr(sprintf("%.3f", $time), 0, -2));
        }else{
            return sprintf("%.2f",substr(sprintf("%.3f", floatval($beginTime)), 0, -2));
        }
    }
    public function setStatus(int $status): void
    {
        parent::setStatus($status);
        $this->setBattleBase(['status'=>self::STATUS_FINISHED]);
    }

    public function setScore($tScore,$ctScore)
    {
        foreach($this->teams as &$team){
            if($team['camp']=='ct'){
                $team['score']=$ctScore;
            }else{
                $team['score']=$tScore;
            }
        }
    }

    /**
     * @param null $wrongStatus
     * 将round刷入rounds，并刷新battle信息
     */
    public function roundPush($wrongStatus = null)
    {
        if ($wrongStatus) {
            $this->disturbRounds[$wrongStatus][] = $this->currentRound;
        } else {
            $this->rounds[] = $this->currentRound;
        }

        $this->currentRound = new Round();
        $this->currentRound->setStatus(self::STATUS_NO_START);
        $this->currentRound->teamSetUpInit($this->teams);
        $this->currentRound->setDetail(['round_ordinal'=>count($this->rounds)+1]);
        // 刷新battle详情
        $this->refreshBattleInfo();
    }

    public function addRound(Round $round)
    {
        $round = $this->refreshRound($round);
        $this->rounds[] = $round;
        $this->refreshBattleInfo();
    }

    public function refreshRound(Round $round)
    {
        // 更新round的id，
        $round->setDetail(['order' => count($this->rounds) + 1]);
        $round->setDetail(['round_ordinal' => count($this->rounds) + 1]);

        // 更新round的team信息
        //
        // 更新
        return $round;
    }

    public function staticAddPlayers($battlePlayers)
    {
        // 按照streamId做统计
        foreach ($battlePlayers as $player) {
            $playerId = $player['steam_id'];
            // 可以用来初始化的值
            $setPlayerInfo['nick_name'] = @$player['player_name'];
            $setPlayerInfo['team_name'] = @$player['team_name'];
            $this->setPlayer($playerId, $setPlayerInfo);
            // 需要自加的值
            $this->playerAddUp($playerId, 'kills', $player['kills']);
            $this->playerAddUp($playerId, 'headshot_kills', $player['headshot_kills']);
            $this->playerAddUp($playerId, 'assists', $player['assists']);
            $this->playerAddUp($playerId, 'damage' ,$player['damage']);
            $this->playerAddUp($playerId, 'flash_assists', $player['flash_assists']);
            if ((int)$player['is_died'] == 1) $this->playerAddUp($playerId, 'deaths', intval($player['is_died']));
            if ((int)$player['is_first_kill'] == 1) $this->playerAddUp($playerId, 'first_kills', intval($player['is_first_kill']));
            if ((int)$player['is_first_death'] == 1) $this->playerAddUp($playerId, 'first_deaths', intval($player['is_first_death']));
            if ((int)$player['is_multi_kill'] == 1) $this->playerAddUp($playerId, 'multi_kills', intval($player['is_multi_kill']));
            if ((int)$player['is_1_v_n_clutche'] == 1) $this->playerAddUp($playerId, '1_v_n_clutches', intval($player['is_1_v_n_clutche']));
            if ((int)$player['is_knife_kill'] == 1) $this->playerAddUp($playerId, 'knife_kill', intval($player['is_knife_kill']));
            if ((int)$player['is_ace_kill'] == 1) $this->playerAddUp($playerId, 'ace_kill', intval($player['is_ace_kill']));
        }
    }

    public function setPlayer($playerId, $info)
    {
        $playerInfo = [];
        if (isset($this->players[$playerId])) {
            $playerInfo = $this->players[$playerId];
        }else{
            $playerInfo = $this->playerInfo;
        }
        $this->players[$playerId] = array_merge($playerInfo, $info);
    }

    public function playerAddUp($playerId, $addType, $value)
    {
        $damageAdd = $value;
        if (isset($this->players[$playerId])) {
            $damageAdd = @$this->players[$playerId][$addType] + $damageAdd;
        }
        $this->setPlayer($playerId, [$addType => $damageAdd]);
    }

    public function refreshBattleInfo()
    {
        $base = [];
        $detail = [];
        $eventList = [];
        $teamScore = [];
        $liveRounds = [];
        // 初始化合计数据
        foreach ($this->players as $key => $player) {
            $this->setPlayer($key, ['kills' => 0,
                'headshot_kills' => 0,
                'deaths' => 0,
                'k_d_diff' => 0,//击杀死亡差
                'assists' => 0,
                'flash_assists' => 0,
                'adr' => 0,//平均每局伤害
                'first_kills' => 0,
                'first_deaths' => 0,
                'first_kills_diff' => 0,
                'multi_kills' => 0,
                '1_v_n_clutches' => 0,
                'knife_kill' => 0,
                'ace_kill' => 0,
            ]);
        }
//        $rounds = $this->getRounds();
        foreach ($this->rounds as $key => $round) {
            $roundDetail = $round->getDetail();
            $roundPlayers = $round->getPlayers();
            $this->staticAddPlayers($roundPlayers);
            // 战队统计
            $winner = $roundDetail['winner_team'];
            // 第1回合胜利战队
            if ($roundDetail['round_ordinal'] == 1) $this->setDetailData($roundDetail, 1);
            // 第16回合胜利战队
            if ($roundDetail['round_ordinal'] == 16) $this->setDetailData($roundDetail, 16);
            if ($winner) {
                $winnerTeamName = $winner['name'];
                if (!isset($teamScore[$winnerTeamName])) {
                    $teamScore[$winnerTeamName] = [
                        '1st_half_score' => 0,
                        '2nd_half_score' => 0,
                        'ot_score' => 0,
                        'total' => 0,
                    ];
                }
                $teamScore[$winnerTeamName]['total'] += 1;
                if ($key < 15) {
                    $teamScore[$winnerTeamName]['1st_half_score'] += 1;
                } elseif ($key < 30) {
                    $teamScore[$winnerTeamName]['2nd_half_score'] += 1;
                } else {
                    $teamScore[$winnerTeamName]['ot_score'] += 1;
                }
                if ($teamScore[$winnerTeamName]['total'] == 5) {
                    if (!$this->battleDetail['first_to_5_rounds_wins_team_name']) {
                        $this->battleDetail['first_to_5_rounds_wins_team_name'] = $winnerTeamName;
                        $this->battleDetail['first_to_5_rounds_wins_side'] = $roundDetail['winner_team']['camp'];
                    }
                }
            }
            $liveRounds[] = $roundDetail['round_ordinal'];
        }
        // 更新team信息
        foreach ($teamScore as $teamName => $teamInfo) {
            $this->teams[$teamName] = array_merge($this->teams[$teamName], $teamInfo);
        }
        // 计算统计数据,这里遍历$player,计算属性，
        foreach ($this->players as &$player) {
            $player['k_d_diff']=$player['kills']-$player['deaths'];
            $player['first_kills_diff']=$player['first_kills']-$player['first_deaths'];
//            $count = count($this->rounds);
//            $player['adr']=$player['damage'];
        }
        //计算current_round
        $this->setBattleDetail(['current_round' => count($this->rounds)]);
        $this->setBattleDetail(['live_rounds' => json_encode($liveRounds)]);
    }

    public function setDetailData($roundDetailData, $round)
    {
        if ($roundDetailData['round_ordinal'] == $round) {
            $battleDetail = [
                'is_confirmed' => 1,
                'win_round_' . $round . '_side' => @$roundDetailData['winner_team']['camp'],
                'win_round_' . $round . '_team' => @$roundDetailData['winner_team']['team_id'],
                'win_round_' . $round . '_team_name' => @$roundDetailData['winner_team']['name'],
                'win_round_' . $round . '_detail' => '',
            ];
        } else {
            $battleDetail = [];
        }
        $this->battleDetail = array_merge($this->battleDetail, $battleDetail);
    }

    public function getBattle()
    {
        // 如果进行中的round，加入到rounds
        $rounds=$this->rounds;
        $roundStatus = $this->currentRound->getStatus();
        if($roundStatus==self::STATUS_ON_GOING || $roundStatus==self::STATUS_FINISHED){
            $rounds[]=$this->currentRound;
        }
        return [
            'detail' => $this->battleDetail,
            'base' => $this->base,
            'rounds' => $rounds,
            'teams' => $this->teams,
            'players' => $this->players,
            'events' => $this->events,
        ];
    }

    public function getRounds()
    {
        $rounds=$this->rounds;
        if(in_array($this->currentRound->getStatus(),[self::STATUS_ON_GOING,self::STATUS_FINISHED])){
            $rounds[]=$this->currentRound;
        }
        return $rounds;
    }

    public function setBattleBase($base)
    {
        $this->base = array_merge($this->base, $base);
    }

    public function setBattleDetail($detail)
    {
        $this->battleDetail = array_merge($this->battleDetail, $detail);
    }

    /**
     * @param $status
     * @param $event
     * 设置暂停状态，1暂停2暂停结束
     */
    public function setPause($status, $event)
    {
        $this->isPause = $status;
    }

    /**
     * @param $teamOrder
     * @param $teamInfo
     * @param $event
     */
    public function setTeams(Event $event, $isReloadSet=false)
    {
        // 更新team状态
        $info = $event->getAtts();
        switch ($event->getEventType()) {
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T:
                $tTeamName = $info['log_team_name'];
                $teamOrderT = 2;
                $team = [
                    'order' => $teamOrderT,//战队排序
                    'score' => 0,//比分
                    'game' => 1,
                    'identity_id' => $tTeamName,//战队名字
                    'rel_identity_id' => $tTeamName,//原始战队ID
                    'team_id' => null,//对应的teamID
                    'name' => $tTeamName,//战队名字
                    'starting_faction_side' => "terrorist",//开局阵营
                    'camp' => 'terrorist',
                ];
                if($isReloadSet){
                    $team=$this->teams[$tTeamName];
                    $team['camp']='terrorist';
                }
                $this->teams[$tTeamName] = $team;
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT:
                $ctTeamName = $info['log_team_name'];
                $team = [
                    'order' => '',//战队排序
                    'score' => 0,//比分
                    'game' => 1,
                    'identity_id' => $ctTeamName,//战队名字
                    'rel_identity_id' => $ctTeamName,//原始战队ID
                    'team_id' => null,//对应的teamID
                    'name' => $ctTeamName,//战队名字
                    'starting_faction_side' => "ct",//开局阵营
                    'camp' => 'ct',
                ];
                if($isReloadSet){
                    $team=$this->teams[$ctTeamName];
                    $team['camp']='ct';
                }
                $this->teams[$ctTeamName] = $team;
                break;
        }
    }

    /**
     * @param $event
     * 比赛换边
     */
    public function switchTeam($event)
    {
        // 切换战队,战队变化，从t到ct，ct到t
        foreach ($this->teams as &$team) {
            $team['camp'] = $team['camp'] == 'ct' ? 'terrorist' : 'ct';
        }
    }

    public function battleStart(Event $event)
    {
        $base = [
            'begin_at' => $event->getEventTime(),
            'status' => 2
        ];
        $this->setBattleBase($base);
    }

    public function getBattleBase()
    {
        return $this->base;
    }

    public function battleEnd(Event $event)
    {
        // 设置获胜战队
        $eventAttr = $event->getAtts();
        $winCamp = @$eventAttr['ext_log']['win_type'][0]['terrorist'];
        $winnerTeam = [];
        foreach ($this->teams as $team) {
            if ($team['camp'] == $winCamp) {
                $winnerTeam = $team;
            }
        }
        $base = [
            'end_at' => $event->getEventTime(),
            'status' => 3,
        ];
        // 计算获胜战队，根据比分

        $this->setBattleBase($base);
    }

    public function getTeamInfo()
    {
        return $this->teams;
    }
}