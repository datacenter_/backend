<?php


namespace app\modules\task\services\hot\five\csgo;


use app\modules\task\services\logformat\Csgo5e;
use app\modules\task\services\logformat\CsgoLogFormatBase;

class Event extends FormatBase
{
    private $atts = [
        'game' => "1",
        'order' => "",//排序
        'event_type' => "",//事件类型
        'round_side' => "",//round阵营
        'team_id' => "",//回合触发特殊事件的战队ID
        'detail' => "",//详情
        'round_ordinal' => "",//回合序号
        'in_round_timestamp' => "",//回合内时间戳
        'round_time' => "",//回合内时间
        'is_bomb_planted' => "",//炸弹是否已安放
        'time_since_plant' => "",//安放炸弹后时间
        'killer' => null,//击杀者ID
        'killer_nick_name' => "",//击杀者昵称
        'killer_side' => "",//击杀者阵营
        'killer_position' => "",//击杀者位置
        'victim' => null,//受害人ID
        'victim_nick_name' => "",//受害人昵称
        'victim_side' => "",//受害人阵营
        'victim_position' => "",//受害人位置
        'assist' => null,//是否有助攻
        'assist_nick_name' => "",//助攻者昵称
        'assist_side' => "",//助攻者阵营
        'flashassist' => "",//闪光弹助攻ID
        'flashassistt_nick_name' => "",//闪光弹助攻者昵称
        'flashassist_side' => "",//闪光弹助攻阵营
        'weapon' => '',//武器ID
        'damage' => "",//伤害
        'hit_group' => "",//击杀命中部位
        'is_headshot' => "",//是否为爆头击杀

        'round_end_type' => '',// round_end- 回合获胜方式 - 有码表 转为ID
        'winner_side' => '',// round_end- 获胜方阵营
        'ct_score' => '',// round_end- CT比分
        't_score' => '',// round_end- T比分
        'bomb_planted_player_id' => '',// 炸弹安放- 选手ID
        'bomb_planted_nick_name' => '',// 炸弹安放- 选手昵称
        'bomb_planted_side' => '',// 炸弹安放- 阵营
        'bomb_planted_position' => '',// 炸弹安放- 位置
        'bomb_defused_player_id' => '',// 炸弹拆除- 选手ID
        'bomb_defused_nick_name' => '',// 炸弹拆除- 选手昵称
        'bomb_defused_side' => '',// 炸弹拆除- 阵营
        'bomb_defused_position' => '',// 炸弹拆除- 位置

        'suicide_player_id' => null,//选手自杀ID
        'suicide_nick_name' => '',// 选手自杀昵称
        'suicide_side' => '',// 选手自杀阵营
        'suicide_position' => '',// 选手自杀位置
        'weapon_name' => '',// 原始武器名字

        'bomb_exploded_time' => '',// 炸弹爆炸- 时间戳
    ];

    /**
     * @var string
     * eventType
     */
    private $eventType;

    public function __construct($atts)
    {
        //公共类
        //转化类
        switch ($atts['log_event_type']) {
            // 击杀
            case CsgoLogFormatBase::EVENT_TYPE_PLAYER_KILL:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['self_nickname'] = $atts['killer']['name'];//私用触发事件选手昵称
                $atts['self_steam_id'] = $atts['killer']['player_id'];//steam_id选手唯一标识(未转化之前的选手加密的ID)
                $atts['killer_nick_name'] = $atts['killer']['name'];//击杀者昵称
                $atts['killer_side'] = $atts['killer']['camp'];//击杀者阵营
                $atts['killer_position'] = $atts['killer']['coordinate'];//击杀者位置
                $atts['self_killer'] = $atts['killer'];
                $atts['killer'] = $atts['killer']['player_id'];//击杀者ID
                $atts['victim'] = $atts['deader']['player_id'];//受害者ID
                $atts['victim_steam_id'] = $atts['deader']['player_id'];//受害者steam_id
                $atts['victim_nick_name'] = $atts['deader']['name'];//受害者昵称
                $atts['victim_side'] = $atts['deader']['camp'];//受害者阵营
                $atts['victim_position'] = $atts['deader']['coordinate'];//受害者位置
                $atts['weapon'] = @$atts['armsId'];//武器ID
                $atts['arms_name'] = $atts['with'];//武器名称
                $atts['weapon_name'] = $atts['with'];//武器名称
//                $atts['hit_group'] = $atts['body'];//击杀部位
                $atts['is_headshot'] = $atts['is_headshot'];//是否为爆头击杀
                // 闪光弹助攻者steam_id
                if (isset($atts['ext_log']['flashassist']) && $atts['ext_log']['flashassist']) {
                    $atts['flashassist_steam_id'] = $atts['ext_log']['flashassist'][0]['assistsSteamId'];
                    // 闪光弹助攻者昵称
                    $atts['flashassistt_nick_name'] = $atts['ext_log']['flashassist'][0]['assistsPlayerNickName'];
                    // 闪光弹助攻者阵营
                    $atts['flashassist_side'] = $atts['ext_log']['flashassist'][0]['assistsPlayerCamp'];
                }
                // 助攻者steam_id
                if (isset($atts['ext_log']['assist']) && $atts['ext_log']['assist']) {
                    $atts['assist_player_steam_id'] = $atts['ext_log']['assist'][0]['assistsPlayerSteamId'];
                    // 助攻者昵称
                    $atts['assist_nick_name'] = $atts['ext_log']['assist'][0]['assistsPlayerNickName'];
                    // 助攻者阵营
                    $atts['assist_side'] = $atts['ext_log']['assist'][0]['assistsPlayerCamp'];
                }

                if (isset($atts['ext_log']['attacked']) && $atts['ext_log']['attacked']) {
                    // 击杀时命中部位
                    $atts['hit_group'] = $atts['ext_log']['attacked'][0]['body'];
                    // 助攻者昵称
                    $atts['damage'] = $atts['ext_log']['attacked'][0]['damage'];
                }

                break;
            // round开始
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_START:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['detail'] = [
                    'round_start_time' => $atts['log_time'],//round开始时间
                ];
                break;
            // round结束
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_END:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                // new
                $atts['round_end_type'] = @$atts['ext_log']['win_type'][0]['log_win_type'];
                $atts['winner_side'] = @$atts['ext_log']['win_type'][0]['log_win_camp'];
                $atts['ct_score'] = @$atts['ext_log']['win_type'][0]['ct_score'];
                $atts['t_score'] = @$atts['ext_log']['win_type'][0]['t_score'];
                $atts['detail'] = [
                    'round_end_time' => $atts['log_time'],//round结束时间
                    'win_type' => @$atts['ext_log']['win_type'][0]['log_win_type'],//round获胜方式
                    'win_camp' => @$atts['ext_log']['win_type'][0]['log_win_camp'],//获胜者阵营
                ];
                break;
            // 炸弹安放
            case CsgoLogFormatBase::EVENT_TYPE_BOMB_PLANTED:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                // new
                $atts['bomb_planted_nick_name'] = $atts['playerName'];
                $atts['bomb_planted_side'] = $atts['camp'];
                $atts['self_steam_id'] = $atts['playID'];
                $atts['is_bomb_planted'] = 1;//炸弹是否安放
                $atts['time_since_plant'] = 40;//安放炸弹后时间:csgo炸弹安放倒计时为40秒
                $atts['detail'] = [
                    'planted_time' => $atts['log_time'],//安装炸弹的时间
                    'player_nickname' => $atts['playerName'],//选手昵称
                    'camp' => $atts['camp'],//选手阵营
                    'round_side' => $atts['round_side'],//round中阵营
                ];
                break;
            // 炸弹被拆除
            case CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['self_steam_id'] = $atts['playID'];
                // new
                $atts['bomb_defused_nick_name'] = $atts['playerName'];
                $atts['bomb_defused_side'] = $atts['camp'];
                $atts['detail'] = [
                    'defused_time' => $atts['log_time'],//炸弹被拆除的时间
                    'player_nickname' => $atts['playerName'],//选手昵称
                    'camp' => $atts['camp'],//选手阵营
                    'round_side' => $atts['round_side'],//round中阵营
                ];
                break;
            // 事件拆除炸弹
            case CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED:
                $atts['event_type'] = CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED;//事件类型
                $atts['self_steam_id'] = $atts['playID'];//选手steam_id
//                $atts['player_nickname'] = $atts['playerName'];//选手昵称
                $atts['bomb_defused_nick_name'] = $atts['playerName'];//选手昵称
                $atts['bomb_defused_side'] = $atts['camp'];//
            break;
            // 炸弹爆炸
            case CsgoLogFormatBase::EVENT_TYPE_TARGET_BOMBED:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                // new
                $atts['bomb_exploded_time'] = $atts['log_time'];
                $atts['detail'] = [
                    'defused_time' => $atts['log_time'],//炸弹爆炸的时间
                    'player_nickname' => $atts['playerName'],//选手昵称
                    'camp' => $atts['camp'],//选手阵营
                    'round_side' => $atts['round_side'],//round中阵营
                ];
                break;
            // 攻击
            case CsgoLogFormatBase::EVENT_TYPE_ATTACKED:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['detail'] = [
                    'self_steam_id' => $atts['attackSteamId'],//攻击选手steam_id
                    'damage' => $atts['damage'],//攻击伤害
                    'attacked' => $atts['attackNickname'],//攻击选手昵称
                    'attacked_time' => $atts['log_time'],//攻击时间
                    'attacked_camp' => $atts['attackCamp'],//攻击选手阵营
                    'attack_location' => $atts['attackLocation'],//攻击选手位置
                    'victim_steam_id' => $atts['victimSteamId'],//受害人选手steam_id
                    'victim_nickname' => $atts['victimNickName'],//受害人选手昵称
                    'victim_camp' => $atts['victimCamp'],//受害人阵营
                    'victim_location' => $atts['victimLocation'],//受害人所在位置
                ];
                break;
            // 自杀
            case CsgoLogFormatBase::EVENT_TYPE_SUICIDE:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['suicide_nick_name'] = $atts['playerName'];//选手昵称
                $atts['suicide_side'] = $atts['player_camp'];//选手阵营
                $atts['suicide_position'] = @$atts['coordinate'];//选手所持装备
                $atts['weapon_name'] = @$atts['with'];//选手所持装备

                $atts['self_steam_id'] = $atts['playID'];//选手steam_id
                $atts['detail'] = [
                    'self_steam_id' => $atts['playID'],//选手steam_id
                ];
                break;
            //离开购买区
            case CsgoLogFormatBase::EVENT_TYPE_LEFT_BUY_ZONE_WITH:
                $atts['event_type'] = $atts['log_event_type'];//事件类型
                $atts['detail'] = [
                    'left_time' => $atts['log_time'],//离开购买区的时间
                    'self_steam_id' => $atts['playID'],//选手steam_id
                    'player_name' => $atts['playerName'],//选手昵称
                    'player_camp' => $atts['detail']['player_camp'],//选手阵营
                    'player_arms' => @$atts['detail']['arms'],//选手所持装备
                ];
                break;
            default:
                null;
        }
        $this->setEventType($atts['log_event_type']);
        $this->setAtts($atts);
    }

    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    public function setAtts($atts)
    {
        $this->atts = array_merge($this->atts, $atts);
    }

    public function getAtts()
    {
        return $this->atts;
    }

    /**
     * 不知是否要改时间格式
     */
    public function getEventTime()
    {
        if (isset($this->atts['log_time'])) {
            $time = substr($this->atts['log_time'], 0, 23);
            $mc = substr($this->atts['log_time'], 20);
            $time = str_replace(' - ', ' ', $time);
            $timeStamp = strtotime($time);
            $timestampMc = $timeStamp * 1000 + $mc;
            return strval($timestampMc);
        }
    }

    private function getTime($timeFromLog)
    {
        $time = substr($timeFromLog, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }

    /**
     * 临时获取时间方法
     */
    public function getEventTimeWAY()
    {
        if (isset($this->atts['log_time'])) {
            $time = substr($this->atts['log_time'], 0, 21);
            $time = str_replace(' - ', ' ', $time);
            $timeStamp = strtotime($time);
            $time = date("Y-m-d H:i:s", $timeStamp);
            return $time;
        }
    }
}