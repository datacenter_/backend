<?php


namespace app\modules\task\services\hot\five\csgo;


class FormatBase
{
    const STATUS_NO_START=1;
    const STATUS_ON_GOING=2;
    const STATUS_FINISHED=3;

    /**
     * @var int
     * 状态，1未开始，2进行中，3已结束
     */
    protected $status=1;
    /**
     * @var string 开始时间
     */
    protected $beginAt='';
    /**
     * @var string 结束时间
     */
    protected $endAt='';

    // 测试setAttributeMerge
    private $testBase=[
        'teamName'=>'',
        'teamOrder'=>'',
    ];
    // 测试setAttributeSubMerge和setAttributeSubAddVal
    private $testTeams=[
        't'=>[
            'name'=>'iamt',
            'score'=>0,
            'kill'=>1,
        ],
        'ct'=>[
            'name'=>'iamct',
            'score'=>0,
            'kill'=>1,
        ],
    ];

    /**
     * @param $attKey
     * @param $values
     * 设置属性的值，属性为array，这里用merge
     */
    protected function setAttributeMerge($attKey,$values)
    {
        $this->$attKey;
        $this->$attKey=array_merge($this->$attKey,$values);
    }

    /**
     * @param $attKey
     * @param $id
     * @param $values
     * 设置属性的值，属性为二维array，这里用merge，给子属性merge
     */
    protected function setAttributeSubMerge($attKey,$id,$values)
    {
        $this->$attKey[$id]=array_merge($this->$attKey[$id],$values);
    }

    /**
     * @param $attKey
     * @param $id
     * @param $key
     * @param $addVal
     * 设置属性的值，属性为二维array，这里用merge，给子子属性做自加
     */
    protected function setAttributeSubAddVal($attKey,$id,$key,$addVal)
    {
        if(!isset($this->$attKey[$id][$key])){
            $this->$attKey[$id][$key]=0;
        }
        $this->$attKey[$id][$key]+=$addVal;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getBeginAt(): string
    {
        return $this->beginAt;
    }

    /**
     * @param string $beginAt
     */
    public function setBeginAt(string $beginAt): void
    {
        $this->beginAt = $beginAt;
    }

    /**
     * @return string
     */
    public function getEndAt(): string
    {
        return $this->endAt;
    }

    /**
     * @param string $endAt
     */
    public function setEndAt(string $endAt): void
    {
        $this->endAt = $endAt;
    }


}