<?php


namespace app\modules\task\services\hot\five\csgo;


use app\modules\common\services\WinnerHelper;
use app\modules\match\models\EnumWinnerType;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\models\SortingLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\grab\abios\Player;
use app\modules\task\services\logformat\Csgo5e;
use app\modules\task\services\logformat\CsgoLogFormatBase;
use app\rest\exceptions\BusinessException;
use app\modules\common\services\SteamHelper;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;

class Match extends FormatBase
{
    /**
     * @var Battle
     */
    private $currentBattle;
    private $disturbBattles;
    private $mapNameMapId=[];
    private $matchDetail=[
        // 'id' => '',//match_id
//        'auto_origin_id' => '自动数据源id',
//        'auto_status' => '自动同步状态',
//        'map_ban_pick' => '',//地图阵容 现在反了这个字段
//        'map_info' => '',//地图信息  现在反了这个字段
        'begin_at' => '',//实际开始时间
        'end_at' => '',//实际结束时间
        'status' => '',//比赛状态
//        'is_draw' => '',//是否平局
//        'is_forfeit' => '',//是否弃权
//        'default_advantage' => '',//默认领先战队
//        'map_vetoes' => '',//Map Vetoes
        'team_1_score' => '',//主队
        'team_2_score' => '',//客队
        'winner' => '',//获胜战队
        'is_battle_detailed' => '1',
        'is_pbpdata_supported' => '1',
        'is_streams_supported' => '2',
//        'created_time' => '创建时间',
//        'modified_time' => '修改时间',
    ];

    private $startScore=[
        1=>0,
        2=>0,
    ];

    /**
     * @var array
     */
    private $battles=[];
    /**
     * @var \app\modules\match\models\Match
     */
    private $matchBase;
    /**
     * @var SortingLog
     */
    private $sortingLog;
    /**
     * @var HotDataRunningMatch
     */
    private $hotInfo;

    private $teamNameToIdMapping=[];
    private $teamNameToOrderMapping=[];
//    private $playerStreamIdToTeamIdMapping=[];
//    private $playerStreamIdToTeamOrderMapping=[];
    private $playerIdBySteamId=[];
    private $playerTeamIdBySteamId=[];
    // 武器
    private $weaponNameCreId=[];
    // 地图
    private $mapNameCreId=[];

    private $realTimeInfo;

    private $updateType=1;

    /**
     * @var bool
     * 比赛预开始状态，如果没有预开始，则不接收事件，对局结束后，比赛状态回归为false
     */
    private $upComingStatus=false;

    /**
     * 热更新情况下，刷新round的数量
     */
    const REFRESH_ROUNDS_NUMBER=2;

    public function __construct()
    {
        $this->currentBattle=new Battle();
        $this->currentBattle->setStatus(self::STATUS_NO_START);
        //        $this->currentBattle->setBattleBase(['order'=>count($this->battles)+1]);
    }

    public function setUpdateType($updateType)
    {
        $this->updateType=$updateType;
    }

    public function addEvent(Event $event)
    {
        $attrs = $event->getAtts();
        $tTeamName_CT_Order = $tTeamName_T_Order = null;
        // 这里根据不同的事件做处理
        switch ($event->getEventType()){
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_UP_COMING:
                $this->upComingStatus=true;
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BATTLE_START:
                if(!$this->upComingStatus){
                    return ;
                }
                //匹配战队
                $attrs = $event->getAtts();
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT])){
                    $tTeamName_CT = $attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT][0]['log_team_name'];
                    $tTeamName_CT_Order = $this->getTeamOrderByTeamName($tTeamName_CT);
                }
                if(isset($attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T])){
                    $tTeamName_T = $attrs['ext_log'][CsgoLogFormatBase::EVENT_TYPE_BATTLE_TEAM_PLAYING_T][0]['log_team_name'];
                    $tTeamName_T_Order = $this->getTeamOrderByTeamName($tTeamName_T);
                }
                if ($tTeamName_CT_Order && $tTeamName_T_Order){
                    // battle开启，如果当前battle状态是不是未开启，则刷新battle
                    if($this->currentBattle->getStatus()!==self::STATUS_NO_START){
                        $this->battlePush($this->currentBattle->getStatus());
                    }
                }else{
                    return;
                }
//                $this->setBeginAt($event->getEventTime());
                break;
//            case CsgoLogFormatBase::EVENT_TYPE_MATCH_UPCOMING:
//                $this->setStatus(self::STATUS_ON_GOING);
//                break;
        }
//        if($this->getStatus()!=self::STATUS_ON_GOING){
//            return;
//        }
        if(!$this->upComingStatus){
            return ;
        }
        $this->currentBattle->addEvent($event);

        if($this->currentBattle->getStatus()==self::STATUS_FINISHED){
            $this->upComingStatus=false;
            $this->battlePush();
        }
        // 设置对局信息
        if($this->realTimeInfo['map_info']){
            $mapName=$this->currentBattle->getBattleBase()['map_name'];
            if($mapName){
                $battleOrder=$this->getBattleOrderByMapName($mapName);
                $this->currentBattle->setBattleBase(['order'=>$battleOrder]);
            }
        }else{
            $this->currentBattle->setBattleBase(['order'=>count($this->battles)+1]);
        }
        // 设置比赛开始时间
        if(count($this->battles)==0){
            if($this->currentBattle->getStatus() == 2){
                $this->matchDetail['begin_at'] = $this->getMicrotimeFormat($this->currentBattle->getBeginAt());
            }
        }
    }

    /**
     * 将当前battle刷入battle池，刷新比赛比分等
     */
    public function battlePush($wrongBattleStatus=null)
    {
        if($wrongBattleStatus){
            $this->disturbBattles[$wrongBattleStatus][]=$wrongBattleStatus;
        }else{
            $this->battles[]=$this->currentBattle;
        }
        $this->currentBattle=new Battle();
        $this->currentBattle->setStatus(self::STATUS_NO_START);
//        $this->currentBattle->setBattleBase(['order'=>count($this->battles)+1]);
        // 刷新match详情
        $this->refreshMatch();
    }

    public function refreshMatch()
    {
        // 获取team的比分状态
        // 开始时间是第一个battle的开始事件
        $team1Score=$this->startScore[1];
        $team2Score=$this->startScore[2];
        // 是不是默认领先，
        $beginAt=null;
        $endAt=null;
        $matchStatus=self::STATUS_NO_START;
        if(count($this->battles)){
            $matchStatus=self::STATUS_ON_GOING;
        }
        foreach($this->battles as $key=>$battle){
            $battleInfo=$battle->getBattleBase();
            if($key==0){
                // 第一个battle的开始事件
                $beginAt=$battleInfo['begin_at'];
            }
            $winnerTeam=@$battleInfo['winner_team'];
            if($winnerTeam){
                $teamOrder=$this->getTeamOrderByTeamName($winnerTeam['identity_id']);
                if($teamOrder==2){
                    $team2Score+=1;
                }
                if($teamOrder==1){
                    $team1Score+=1;
                }
            }
            $endAt=$battleInfo['end_at'];
        }
        $this->matchDetail['team_1_score']=$team1Score;
        $this->matchDetail['team_2_score']=$team2Score;
        if($beginAt){
            $this->matchDetail['begin_at']=$this->getMicrotimeFormat($beginAt);
        }
        // 设置比赛比分 状态 平局 弃权
        $statusInfo=WinnerHelper::csgoWinnerInfo($team1Score,$team2Score,$this->matchBase['number_of_games']);
        if($statusInfo['is_finish']==1){
            $matchStatus=self::STATUS_FINISHED;
            $this->matchDetail['is_draw'] = $statusInfo['is_draw'];
            $this->matchDetail['winner'] = $statusInfo['winner_team'];
        }
        $this->setStatus($matchStatus);
        $this->matchDetail['status']=$matchStatus;
        if($matchStatus==3&&$endAt){
            //TODO 查看是否已经修改了比赛的结束时间
            $this->matchDetail['end_at']=$endAt;
        }
    }

    public function initMatchInfoByHotId($hotId)
    {
        $hotInfo=HotDataRunningMatch::find()->where(['id' => $hotId])->one();
        $matchBase=\app\modules\match\models\Match::find()->where(['id'=>$hotInfo['match_id']])->one();
        $logInfo=SortingLog::find()->where(['id'=>$hotInfo['standard_id']])->one();
        $realTimeInfo=MatchRealTimeInfo::find()->where(['id'=>$hotInfo['match_id']])->one();
        $this->realTimeInfo=$realTimeInfo;
        $this->hotInfo=$hotInfo;
        $this->matchBase=$matchBase;
        $this->sortingLog=$logInfo;
        //如果是有默认的领先
        $default_advantage = $this->realTimeInfo['default_advantage'];
        if($default_advantage > 0){
            $csgo_Map = MetadataCsgoMap::find()->where(['external_name' => 'default'])->asArray()->one();
            $tmpBattle=new Battle();
            $tmpBattle->setBattleBase([
                'map'=>$csgo_Map['id'],//比赛地图
                'begin_at'=>null,//开始时间
                'end_at'=>null,//结束时间
                'status'=>3,//比赛结束
                'winner' => $this->realTimeInfo['winner'], //获胜方
                'duration'=>null,//对局时长
                'is_default_advantage'=>1,//默认领先队伍
                'is_forfeit'=> 2,//是否弃权
                'is_battle_detailed'=>1
            ]);
            $this->battles[]=$tmpBattle;

            //设置比赛比分
            if ($default_advantage == 1){
                $this->startScore['1']=1;
            }else{
                $this->startScore['2']=1;
            }
//            $realTimeInfo->save();
        }
    }

    public function getDetail()
    {
        // 刷新列表
        // 当前回合必刷，
        // 根据当前battle的当前round的状态，计算是否需要刷新前一个对局的回合
        $lastBattleRoundsNumber=0;
        $roundsOfActiveBattle=count($this->currentBattle->getRounds());
        if($roundsOfActiveBattle<=self::REFRESH_ROUNDS_NUMBER){
            // 需要向前刷新的值为0，
            $lastBattleRoundsNumber=self::REFRESH_ROUNDS_NUMBER-$lastBattleRoundsNumber;
            $currentBattleRoundsNumber=$roundsOfActiveBattle;
        }else{
            $currentBattleRoundsNumber=self::REFRESH_ROUNDS_NUMBER;
        }

        // 这里有计算数据；
        $info=$this->matchDetail;
        $this->matchDetail['end_at'] = $this->getMicrotimeFormat($info['end_at']);
        $formatBattles=[];
        // 这里会返回一个大树形结构
        // 前方高能，这里只更新当前回合和当前效果
        foreach($this->battles as $key=>$battle){
            // 判断battle是否为latest
            if($this->updateType==2){
                if($key==(count($this->battles)-1)){
                    $tmpBattleFormat=$this->formatBattle($battle->getBattle(),$this->updateType,$lastBattleRoundsNumber);
                }
            }else{
                $tmpBattleFormat=$this->formatBattle($battle->getBattle());
            }
            // 结构变化
            if(isset($tmpBattleFormat)){
                $formatBattles[]=$tmpBattleFormat;
            }
        }
        // 如果battle是进行中，则将进行中的battle也加入都battle队列
        if($this->currentBattle->getStatus()==self::STATUS_ON_GOING){
            $formatBattles[]=$this->formatBattle($this->currentBattle->getBattle(),$this->updateType,$currentBattleRoundsNumber);
        }
        $info=[
            'match'=>$this->matchDetail,
            'battles'=>$formatBattles,
        ];
        return $info;
    }

    public function formatBattle($battleInfo,$updateType=1,$roundCount=self::REFRESH_ROUNDS_NUMBER)
    {
        // 格式化战队
        $teamFormat = [];
        //补充ID
        $battleInfo['detail']['win_round_1_team'] = @$this->getTeamIdByName($battleInfo['detail']['win_round_1_team_name']);
        $battleInfo['detail']['win_round_16_team'] = @$this->getTeamIdByName($battleInfo['detail']['win_round_16_team_name']);
        $battleInfo['detail']['first_to_5_rounds_wins_team'] = @$this->getTeamIdByName($battleInfo['detail']['first_to_5_rounds_wins_team_name']);
        $battleInfo['base']['match'] = @$this->matchBase['id'];
        $battleInfo['base']['winner'] = @$this->getTeamOrderByTeamName($battleInfo['base']['winner_team']['name']);
        foreach ($battleInfo['teams'] as $val) {
            $tmp = $val;
            $tmp['order'] = $this->getTeamOrderByTeamName($val['name']);
            $tmp['team_id'] = $this->getTeamIdByName($val['name']);
            $teamFormat[] = $tmp;
//        $winnerScore=[];
            foreach ($battleInfo['teams'] as $val) {
                $tmp = $val;
                $tmp['order'] = $this->getTeamOrderByTeamName($val['name']);
                $tmp['team_id'] = $this->getTeamIdByName($val['name']);
//            if(empty($winnerScore)) {
//                $winnerScore['order'] = $tmp['order'];
//                $winnerScore['score'] = $val['score'];
//            }else{
//                if ((int)$val['score'] > (int)$winnerScore['score']){
//                    $battleInfo['base']['winner'] = $tmp['order'];
//                }elseif ((int)$val['score'] < (int)$winnerScore['score']){
//                    $battleInfo['base']['winner'] = $winnerScore['order'];
//                }else{
//                    $battleInfo['base']['winner'] = null;
//                }
//            }
                $teamFormat[] = $tmp;
            }

            $eventList = [];
            $roundList = [];
            $roundPlayers = [];
            $playerOrderId = 0;
            $overWinnerType = [];
            foreach ($battleInfo['rounds'] as $key => $round) {
                // 如果不需要刷新，则跳过
                if ($updateType == 2 && (count($battleInfo['rounds']) - $key > $roundCount)) {
                    continue;
                }
                foreach ($round->getEvents() as $eKey => $event) {
                    $event->setAtts([
                        'match' => $this->matchBase['id'],
                        'round_order' => $key + 1,
                        'round_ordinal' => $key + 1,
                        'order' => $eKey + 1,
                    ]);
                    $eventInfo = $event->getAtts();
                    $eventType = [
                        CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED => 'bomb_defused_player_id',
                        CsgoLogFormatBase::EVENT_TYPE_BOMB_PLANTED => 'bomb_planted_player_id',
                    ];
                    switch ($eventInfo['event_type']) {
                        case CsgoLogFormatBase::EVENT_TYPE_ROUND_START:
                            $eventList[] = $eventInfo;
                            break;
                        case CsgoLogFormatBase::EVENT_TYPE_PLAYER_KILL:
                            // 闪光弹助攻player_id
                            isset($eventInfo['flashassist_steam_id']) ? $eventInfo['flashassist'] = $this->getPlayerIdBySteamId($eventInfo['flashassist_steam_id']) : null;
                            // 助攻id
                            isset($eventInfo['assist_player_steam_id']) ? $eventInfo['assist'] = $this->getPlayerIdBySteamId($eventInfo['assist_player_steam_id']) : null;
                            // 武器名字转ID
                            $eventInfo['weapon'] = $this->weaponNameChangeId(@$eventInfo['with']);
                            $eventInfo['killer'] = $this->getPlayerIdBySteamId(@$eventInfo['killer']);
                            $eventInfo['victim'] = $this->getPlayerIdBySteamId(@$eventInfo['victim']);
                            $eventList[] = $eventInfo;
                            break;
                        case CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED:
                        case CsgoLogFormatBase::EVENT_TYPE_BOMB_PLANTED:
                            $eventInfo[$eventType[$eventInfo['event_type']]] = $this->getPlayerIdBySteamId($eventInfo['self_steam_id']);
                            $eventList[] = $eventInfo;
                            break;
                        case CsgoLogFormatBase::EVENT_TYPE_SUICIDE:
                            // 武器名字转ID
                            $eventInfo['weapon'] = $this->weaponNameChangeId(@$eventInfo['with']);
                            $eventInfo['suicide_player_id'] = $this->getPlayerIdBySteamId(@$eventInfo['suicide_nick_name']);
                            $eventList[] = $eventInfo;
                            break;
                        case CsgoLogFormatBase::EVENT_TYPE_ROUND_END:
                            // 回合结束方式
                            $eventInfo['round_end_type'] = $this->getWinnerEndTypeId($eventInfo['round_end_type']);
                            if (!isset($overWinnerType[$key + 1])) {
                                $overWinnerType[$key + 1] = $eventInfo['round_end_type'];
                            }
                            if ($eventInfo['round_end_type'] == 4) {
                                // 炸弹爆炸 时间
                                $eventInfo['bomb_exploded_time'] = $this->getDateTimeFormat($eventInfo['log_time']);
                            }
                            $eventList[] = $eventInfo;
                            break;
                    }
                    $roundDetail = $round->getDetail();
                    $roundTeams = $round->getTeamInfo();
                    foreach ($roundTeams as $kk => $vv) {
                        $roundTeams[$kk]['side'] = $vv['camp'];
                        $roundTeams[$kk]['round_ordinal'] = $key + 1;
                        $roundTeams[$kk]['side_order'] = @$this->getTeamOrderByTeamName($vv['name']);
                        $roundTeams[$kk]['team_id'] = @$this->getTeamIdByName($roundTeams[$kk]['name']);
                    }
                    $roundDetail['side'] = $roundTeams;
                    $roundDetail['winner_team_id'] = @$this->getTeamIdByName($roundDetail['winner_team']['name']);
                    $roundDetail['winner_end_type'] = @$overWinnerType[$roundDetail['round_ordinal']] ?? null;
                    $roundList[] = $roundDetail;
                    $roundPlayersFromRound = $round->getPlayers();
                    sort($roundPlayersFromRound);
                    $playerOrderId = 0;
                    foreach ($roundPlayersFromRound as $k2 => $player) {
                        $player['round_ordinal'] = $key + 1;
                        $player['player_id'] = $this->getPlayerIdBySteamId($player['steam_id']);
                        $player['team_id'] = @$this->getTeamIdByName($player['team_name']);
                        $player['nick_name'] = @$player['player_name'];
                        $player['side'] = $player['camp'];
                        $player['player_order'] = @$this->getTeamOrderByTeamName($player['team_name']);
                        $player['order'] = ++$playerOrderId;
                        $roundPlayers[] = $player;
                    }
                    // 计算平均伤害
                    $item = [];
                    foreach ($roundPlayers as $k => $v) {
                        if (!isset($item[$v['player_id']])) {
                            $item[$v['player_id']] = $v['damage'];
                        } else {
                            $item[$v['player_id']] = $item[$v['player_id']] + $v['damage'];
                        }
                    }
                    // 格式化player
                    $playerInfos = $battleInfo['players'];
                    $playerFormats = [];
                    $playerOrder = 0;
                    foreach ($playerInfos as $key => $val) {
                        $tmp = $val;
                        $tmp['player_id'] = $this->getPlayerIdBySteamId($key);
                        $tmp['team_order'] = $this->getTeamOrderByTeamName(@$val['team_name']);
                        $tmp['steam_id'] = $key;
                        $tmp['order'] = ++$playerOrder;
                        // 平均伤害
                        $adr = @$item[$tmp['player_id']] / @count(json_decode($battleInfo['detail']['live_rounds'], true));
                        $tmp['adr'] = round(@$adr, 2);
                        $playerFormats[] = $tmp;
                    }
//        foreach($battleInfo['events'] as $event){
//            $event->setAtts(['match'=>$this->matchBase['id']]);
//            $eventList[]=$event->getAtts();
//        }
                    // 格式化事件
                    // 格式化round

                    $battleInfo['base'] = $this->reSetBase($battleInfo['base']);
                    $battleInfo['order'] = $battleInfo['base']['order'];
                    $battleInfo['static'] = $teamFormat;
                    $battleInfo['players'] = $playerFormats;
                    $battleInfo['events'] = $eventList;
                    $battleInfo['round_players'] = $roundPlayers;
                    $battleInfo['rounds'] = $roundList;

                    return $battleInfo;
                }
            }
        }
    }
    public function reSetBase($data)
    {
        $base=[
            'map' => $this->mapNameChangeId($data['map_name']),
            'begin_at' => $this->getMicrotimeFormat($data['begin_at']),
            'end_at' => $this->getMicrotimeFormat($data['end_at']),
        ];
        return array_merge($data, $base);
    }
    /**
     * 武器名称转ID
     * $weaponName    武器名称
     */
    public function weaponNameChangeId($weaponName)
    {
        if(isset($this->weaponNameCreId[$weaponName])){

        }else {
            $this->weaponNameCreId[$weaponName] = MetadataCsgoWeapon::find()->where(['name' => $weaponName])->select('id')->one();
        }
        return $this->weaponNameCreId[$weaponName]['id'];
    }
    // 地图id
    public function mapNameChangeId($mapName)
    {
        if(isset($this->mapNameCreId[$mapName])){

        }else {
            $this->mapNameCreId[$mapName] = MetadataCsgoMap::find()->where(['external_name' => $mapName])->select('id')->one();
        }
        return $this->mapNameCreId[$mapName]['id'];
    }
    // 胜利方式
    public function getWinnerEndTypeId($winnerTypeName)
    {
        $weapon = EnumWinnerType::find()->where(['winner_type' => $winnerTypeName])->select('id')->one();
        return $weapon ? $weapon['id'] : null;
    }

    public function getTeamOrderByTeamName($teamName)
    {
        if(isset($this->teamNameToOrderMapping[$teamName])){

        }else{
            $teamLongName1=array_filter(explode(";",$this->sortingLog['team_1_log_name']));
            $teamLongName2=array_filter(explode(";",$this->sortingLog['team_2_log_name']));
            for($i=0;$i<count($teamLongName1);$i++){
                if(trim($teamLongName1[$i]) == trim($teamName)){
                    $this->teamNameToOrderMapping[$teamName] = 1;
                    break;
                }
            }
            for($i=0;$i<count($teamLongName2);$i++){
                if(trim($teamLongName2[$i]) == trim($teamName)){
                    $this->teamNameToOrderMapping[$teamName] = 2;
                    break;
                }
            }
        }
        return @$this->teamNameToOrderMapping[$teamName];
    }

    public function getTeamIdByName($teamName)
    {
        if(isset($this->teamNameToIdMapping[$teamName])){
//            return $this->teamNameToIdMapping[$teamName];
        }else{
            $order=$this->getTeamOrderByTeamName($teamName);
            if($order==1){
                $this->teamNameToIdMapping[$teamName]=$this->matchBase['team_1_id'];
            }
            if($order==2){
                $this->teamNameToIdMapping[$teamName]=$this->matchBase['team_2_id'];
            }
        }
        return $this->teamNameToIdMapping[$teamName];

    }

    public function getPlayerIdBySteamId($steamId)
    {
        if(isset($this->playerIdBySteamId[$steamId])){

        }else{
            // 解析steam_id
            $id = SteamHelper::playerConversionSteamId($steamId);
            // 从主库查到对应的teamId
            $playerInfo=\app\modules\org\models\Player::find()->where(['steam_id'=>$id])->one();
            $this->playerIdBySteamId[$steamId]=$playerInfo['id'];
        }
        return $this->playerIdBySteamId[$steamId];
    }

    public function getPlayerTeamIdBySteamId($steamId)
    {
        if(isset($this->playerTeamIdBySteamId[$steamId])){

        }else{
            // 从主库查到对应的teamId
            $playerId=self::getPlayerIdBySteamId($steamId);
            if($playerId){
                // 获取player的战队
                $teamRelInfo=TeamPlayerRelation::find()->where(['player_id'=>$playerId])->one();
                $this->playerTeamIdBySteamId[$steamId]= @$teamRelInfo['team_id'];
            }
        }
        return $this->playerTeamIdBySteamId[$steamId];
    }
    /**
     * 时间戳转日期格式：精确到毫秒
     * x 代表毫秒
     */
    public function getMicrotimeFormat($time)
    {
        if($time) {
            $time = $time*0.001;
            if (strstr($time, '.')) {
                sprintf("%01.3f", $time); //小数点。不足三位补0
                list($usec, $sec) = explode(".", $time);
                $sec = str_pad($sec, 3, "0", STR_PAD_RIGHT); //不足3位。右边补0
            } else {
                $usec = $time;
                $sec = "000";
            }
            $date = date("Y-m-d H:i:s.x", $usec);
            return str_replace('x', $sec, $date);
        }else{
            return;
        }
    }

    public function getBattleOrderByMapName($mapName)
    {
        if(isset($this->mapNameMapId[$mapName])){

        }else{
            $mapInfo=$this->realTimeInfo['map_ban_pick'];
            if($mapInfo){
                //获取地图的ID
                $csgo_Map = MetadataCsgoMap::find()->where(['external_name' => $mapName])->asArray()->one();
                $csgo_MapId = $csgo_Map['id'];
                $mapList=json_decode($mapInfo,true);
                foreach ($mapList as $key=>$val){
                    //去找地图名
                    if($val['map']==$csgo_MapId){
                        $this->mapNameMapId[$mapName]= $key+1;
                        break;
                    }
                }
            }
        }
        return @$this->mapNameMapId[$mapName];

    }
    // 日志日期转北京日期
    public function getDateTimeFormat($dateTime){
        if($dateTime) {
            list($usec, $sec) = explode(".", $dateTime);
            $logDateStr = substr($usec, 0, 21);
            $logDate = str_replace(' - ', ' ', $logDateStr);
            $logTime = strtotime($logDate);
            return date('Y-m-d H:i:s', $logTime);
        }else{
            return;
        }
    }


}