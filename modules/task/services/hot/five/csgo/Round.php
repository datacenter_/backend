<?php


namespace app\modules\task\services\hot\five\csgo;


use app\modules\task\services\logformat\CsgoLogFormatBase;

class Round extends FormatBase
{
    private $roundDetail = [
        'battle_id' => '',
        'round_ordinal' => '',
        'winner_team_id' => '',
        'winner_team_order' => '',
        'winner_camp' => '',
        'winner_team' => '',
        'begin_at' => '',
        'end_at' => '',
    ];
    private $teamInfo = [
//        'id' => 'ID',
//        'battle_id' => 'Battle ID',
        'game' => '1',
//        'match' => 'Match',
        'order' => '',
        'side_order' => '',
        'round_ordinal' => '',
        'side' => '',
        'team_id' => '',
        'survived_players' => '',//存活人数
        'kills' => '',
        'headshot_kills' => '',
        'is_winner' => '2',
        'round_end_type' => '',
        'is_opening_kill_side' => '2',//是否首杀
        'is_opening_kill_headshot' => '2',//首杀是否爆头
        'is_bomb_planted' => '2',//是否安放了炸弹
        'is_knife_kill' => '2',
        'is_ace_kill' => '2',
//        'modified_at' => 'Modified At',
//        'created_at' => 'Created At',
//        'deleted_at' => 'Deleted At',
    ];

    private $playerInfo = [
//        'id' => 'ID',
//        'battle_id' => 'Battle ID',
        'game' => '1',
//        'match' => 'Match',
        'order' => '',
        'player_order' => '',
        'round_ordinal' => '',
        'side' => '',
        'team_id' => '',
        'player_id' => '',
        'nick_name' => '',
        'kills' => 0,
        'headshot_kills' => 0,
        'is_died' => '2',
        'assists' => 0,
        'flash_assists' => 0,
        'damage' => 0,
        'is_first_kill' => '2',
        'is_first_death' => '2',
        'is_multi_kill' => '2',//击杀数大于等于二
        'is_1_v_n_clutche' => '2',// 他们队只剩一个人，对方大于等于1，最后获胜了
        'is_knife_kill' => '2',
        'is_ace_kill' => '2', // 一杀五
        'steam_id'=>'',
        'camp' => '',
//        'modified_at' => '2',
//        'created_at' => '2',
//        'deleted_at' => '2',
    ];
    private $events = [];
    private $teams = [];
    private $players = [];
    private $startTimeMc = 0;
    private $bombStartTime = 0;
    private $roundCountDown = 0;
    private $bombCountDown = 0;
    private $bombStatus = 2;
    protected $status = 0;
    private $winnerCamp = '';
    private $first=[
        'kill'=>null,
        'death'=>null,
    ];
    public function __construct()
    {
    }

    public function getPlayers()
    {
        return $this->players;
    }

    public function getTeamInfo()
    {
        return $this->teams;
    }

    public function addEvent(Event $event)
    {
        // 判断事件类型，
        switch ($event->getEventType()) {
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_START:
                // 这里返回了毫秒
                $this->startTimeMc = $event->getEventTime();
                $this->status = 2;
                $this->setDetail(['begin_at'=>$this->beginAt,'end_at'=>$this->endAt]);
                break;
            case CsgoLogFormatBase::EVENT_TYPE_LEFT_BUY_ZONE_WITH:
                // 离开购买区，初始化队员？认证队员
                $this->refreshPlayers($event);
                return;
                break;
                // 安放炸弹
            case CsgoLogFormatBase::EVENT_TYPE_BOMB_PLANTED:
                $this->bombStatus = 1;
                $this->bombStartTime = $event->getEventTime();
                $info = $event->getAtts();
                if($info['camp'] == "ct"){
                    $this->setTeam('ct',['is_bomb_planted' => 1]);
                }else{
                    $this->setTeam('terrorist',['is_bomb_planted' => 1]);
                }
                break;
            case CsgoLogFormatBase::EVENT_TYPE_BOMB_DEFUSED:
                $this->bombStatus = 2;
//                $this->bombStartTime = $event->getEventTime();
                break;
            case CsgoLogFormatBase::EVENT_TYPE_TARGET_BOMBED:
                $this->bombStartTime = $event->getEventTime();
                break;
            case CsgoLogFormatBase::EVENT_TYPE_ROUND_END:
                $eventAttr = $event->getAtts();
                $winCamp = @$eventAttr['ext_log']['win_type'][0]['log_win_camp'];
                $winnerTeam = [];
                foreach ($this->teams as $team) {
                    if (@$team['camp'] == $winCamp) {
                        $winnerTeam = $team;
                    }
                }
//                $this->setTeam($winCamp,['score'=>1]);
                $upDetail = [
                    'winner_team' => $winnerTeam,
                    'status' => 3,
                    'begin_at'=>$this->beginAt,
                    'end_time' => $event->getEventTime(),
                ];
//                $this->setDetail($upDetail);
                $this->status = 3;
                $this->setEndAt($event->getEventTime());
                $this->setStatus(self::STATUS_FINISHED);
                $this->setDetail($upDetail);
                break;
            case CsgoLogFormatBase::EVENT_TYPE_ATTACKED:
                // 攻击，不算事件？
                $info = $event->getAtts();
                $playerId = $info['attackSteamId'];
                $damage = $info['damage'] > 100 ? 100 : $info['damage'];
                // 如果没有player，初始化
                if(!isset($this->players[$info['attackSteamId']])){
                    $playerInfo=$this->playerInfo;
                    $playerInfo['player_name'] = @$info['attackNickname'];
                    $playerInfo['steam_id'] = @$info['attackSteamId'];
                    $playerInfo['camp'] = @$info['attackCamp'];//选手阵营
                    $this->setPlayer($info['attackSteamId'], $playerInfo);
                }
//                $this->setPlayer($info['attackSteamId'], $playerInfo);
                $this->playerAddUp($playerId, 'damage', $damage);
                return;
                break;
            case CsgoLogFormatBase::EVENT_TYPE_PLAYER_KILL:
                // 添加击杀统计
                $info = $event->getAtts();
                // 如果没有player，初始化
                if(!isset($this->players[$info['self_steam_id']])){
                    $playerInfo=$this->playerInfo;
                    $playerInfo['player_name'] = @$info['player_name'];
                    $playerInfo['steam_id'] = @$info['self_steam_id'];
                    $playerInfo['camp'] = @$info['camp'];//选手阵营
                    $this->setPlayer($info['self_steam_id'], $playerInfo);
                }

                if(!isset($this->players[$info['victim_steam_id']])){
                    $playerInfo=$this->playerInfo;
                    $playerInfo['player_name'] = @$info['victim_steam_nick_name'];
                    $playerInfo['steam_id'] = @$info['victim_steam_id'];
                    $playerInfo['camp'] = @$info['victim_side'];//选手阵营
                    $this->setPlayer($info['victim_steam_id'], $playerInfo);
                }
                $killerId = $info['self_steam_id'];//击杀者选手ID
                $armsName = @$info['arms_name'];
                $roundOrdinal=$this->roundDetail['round_ordinal'];
                if(@$info['deader']['camp'] == @$info['self_killer']['camp']){
                    $this->playerAddUp($killerId, 'kills', -1);
                }else{
                    $this->playerAddUp($killerId, 'kills', 1);
                }
                $victimSteamId = $info['victim_steam_id'];
                // 存活状态修改
                $this->setPlayer($victimSteamId, ['is_died' => 1]);
                // 刀杀统计
                $armsName = strstr($armsName, "knife");
                if($armsName){
                    $this->setPlayer($killerId, ['is_knife_kill' => 1]);
                }
                // 记录首杀，首死
                if(!$this->first['kill']){
                    $this->first['kill']=$killerId;
                    $this->first['death']=$victimSteamId;
//                    $firstKnife=$armsName?1:2;
                    $this->setPlayer($killerId, ['is_first_kill'=>1]);
                    $this->setPlayer($victimSteamId, ['is_first_death'=>1]);
                }

                //闪光弹助攻统计
                if(isset($info['flashassist_steam_id']) && $info['flashassist_steam_id']){
                    $flashassist = $info['flashassist_steam_id'];//闪光弹助攻选手ID
                    if(@$info['deader']['camp'] != @$info['self_killer']['camp']){
                        $this->playerAddUp($flashassist,'flash_assists',1);
                    }
                }
                //助攻数统计
                if(isset($info['assist_player_steam_id']) && $info['assist_player_steam_id']){
                    $assists = $info['assist_player_steam_id'];//助攻选手ID
                    $this->playerAddUp($assists,'assists',1);
                }
                //爆头击杀统计
                if(isset($info['hit_group']) && $info['hit_group'] == "head"){
                    $headshotKills = $info['self_steam_id'];//击杀者选手ID
                    $this->playerAddUp($headshotKills,'headshot_kills',1);
                }

                //算team统计数
                // 当前阵营击杀数统计
                $camp = $info['killer_side'];//击杀者阵营名称
                $this->teamAddUp($camp,'kills',1);
                // 爆头击杀数统计
                if(isset($info['is_headshot']) && $info['is_headshot']==1){
                    $this->teamAddUp($camp,'headshot_kills',1);
                }
                break;
            // 自杀事件
            case CsgoLogFormatBase::EVENT_TYPE_SUICIDE:
                // 添加自杀统计
                $info = $event->getAtts();
                $killerId = $info['self_steam_id'];//选手ID
                // 击杀-1 死亡+1
                $this->playerAddUp($killerId, 'kills', -1);
                // 存活状态修改
                $this->setPlayer($killerId, ['is_died' => 1]);
                // c4 死亡+1 击杀+1 不在这事件里面
                break;
            case CsgoLogFormatBase::EVENT_TYPE_ASSIST:
                // 添加助攻统计  咱不需要
                return;
                break;
            case CsgoLogFormatBase::EVENT_TYPE_FLASHASSIST:
                // 添加闪光弹助攻统计 咱不需要
                return;
                break;

        }
        $event = $this->refreshEvent($event);
        $this->events[] = $event;
        $this->refreshRound();
    }

    public function setDetail($detail)
    {
        $this->roundDetail = array_merge($this->roundDetail, $detail);
    }

    public function getDetail()
    {
        return $this->roundDetail;
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function refreshRound()
    {
        // 更新player信息
        foreach($this->players as &$player)
        {
            if($player["kills"]>=2){
                $player['is_multi_kill']=1;
            }
            if($player["kills"]>=5){
                $player['is_ace_kill']=1;
            }
        }
        // 更新team
        // 循环player统计
        // 初始化teamInfo
        foreach($this->teams as $camp=>$team){
            $team=[
                'survived_players'=>5,
                'is_opening_kill_side'=>2,
                'is_opening_kill_headshot'=>2,
                'is_knife_kill'=>2,
                'is_ace_kill'=>2,
            ];
            $this->setTeam($camp,$team);
        }
        foreach($this->players as $key => $value){
            if(isset($value['is_died']) && $value['is_died'] == 1){
                $this->teamAddUp($value['camp'],'survived_players',-1);
            }
            // 是否首杀
            if(isset($value['is_first_kill']) && $value['is_first_kill'] == 1){
                $this->setTeam($value['camp'],['is_opening_kill_side' => 1]);
            }

            // 首杀是否爆头
            if(isset($value['is_first_kill']) && $value['is_first_kill'] == 1 && $value['headshot_kills'] > 0){
                $this->setTeam($value['camp'],['is_opening_kill_headshot' => 1]);
            }

            // 是否刀杀
            if(isset($value['is_knife_kill']) && $value['is_knife_kill'] == 1){
                $this->setTeam($value['camp'],['is_knife_kill' => 1]);
            }

            // 是否有ACE
            if($value['is_ace_kill'] == 1){
                $this->setTeam($value['camp'],['is_ace_kill' => 1]);
            }
        }
    }

    public function refreshPlayers(Event $event)
    {
        // 初始化player，round里面的player会在这里被刷新
        $info = $event->getAtts();
        // 这里有选手，
        $playerInfo=[];
        $playerInfo['player_name'] = $info['playerName'];
        $playerInfo['steam_id'] =$info['playID'];
        $playerInfo['camp'] = strtolower($info['detail']['player_camp']);//选手阵营
        // 刷新选手对应的team
        foreach($this->teams as $team){
            if(@$team['camp']==$playerInfo['camp']){
                $playerInfo['team_name']=$team['name'];
            }
        }
        if(isset($this->players[$info['playID']])){
            $playerInfo=array_merge($this->players[$info['playID']],$playerInfo);
        }
        $this->setPlayer($info['playID'], $playerInfo);
    }

    public function setPlayer($playerId, $info)
    {
        $playerInfo=[];
        if (isset($this->players[$playerId])) {
            $playerInfo = $this->players[$playerId];
        }else{
            $playerInfo = $this->playerInfo;
        }
        $this->players[$playerId] = array_merge($playerInfo,$info);
    }

    /**
     * 将信息添加到阵营数组中    By    王傲渊
     * $camp        阵营
     * $info        信息
     */
    public function setTeam($camp, $info)
    {
        $teamInfo=[];
        if (isset($this->teams[$camp])) {
            $teamInfo = $this->teams[$camp];
        }
        $this->teams[$camp] = array_merge($teamInfo,$info);
    }

    /**
     * @param $playerId
     * @param $addType
     * @param $value
     * 这里累计player的信息
     */
    public function playerAddUp($playerId, $addType, $value)
    {
        $damageAdd = $value;
        if (isset($this->players[$playerId])) {
            $damageAdd = @$this->players[$playerId][$addType] + $damageAdd;
        }
        $this->setPlayer($playerId, [$addType => $damageAdd]);
    }

    /**
     * 累计计算team的信息    By    王傲渊
     * $camp            阵营
     * $addType         添加信息类型
     * $value           累计值
     */
    public function teamAddUp($camp,$addType,$value)
    {
        $damageAdd = $value;
        if (isset($this->teams[$camp])) {
            $damageAdd = @$this->teams[$camp][$addType] + $damageAdd;
        }
        $this->setTeam($camp, [$addType => $damageAdd]);
    }

    public function teamSetUpInit($teamInfo)
    {
        foreach($teamInfo as $team){
            $camp=$team['camp'];
            $teamInfo=$team;
            if(!isset($this->teams[$camp])){
                $this->teams[$camp]=$teamInfo;
            }else{
                $this->teams[$camp]=array_merge($this->teams[$camp],$teamInfo);
            }
        }
    }

    public function refreshEvent(Event $event)
    {
        // 时间相关的几个属性
        $eventTime = $event->getEventTime();
        $info = [
            'in_round_timestamp' => round(($eventTime - $this->startTimeMc)/1000,2),//正的，开始到当前的时间
            'round_time' => round((115 * 1000 + $this->startTimeMc - $eventTime)/1000,2),//回合倒计时
            'is_bomb_planted' => $this->bombStatus,//炸弹安放状态
            'time_since_plant' => $this->bombStatus==1 ? round((40 * 1000 + $this->bombStartTime - $eventTime)/1000,2) : null,//炸弹倒计时
        ];
        $event->setAtts($info);
        return $event;
    }
}