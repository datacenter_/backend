<?php

namespace app\modules\task\services\hot\hltv;

use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\MatchePlaryer;
use app\modules\task\models\RoundsDetail;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\models\MatchAll;
use app\modules\task\models\BattlesAll;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 * HltvMatchApi
 **/
class HltvMatchApi extends HotBase implements HotMatchInterface
{
    static private $instance;
    const CSGO_GAME_ID = 1;
    const  Match_STATUS_DELETE = "Match deleted";
    const  Match_STATUS_END = "Match over";
    const  Match_STATUS_POSTPONED = "Match postponed";
    const  Match_STATUS_LIVE = "LIVE";
    public $updateType = 1;
    public $info = [];
    public $match = [];
    public $dbMatch = '';
    public $matchId = 0;
    public $relIdentityId = 0;
    // base 数据格式(结果)
    private static $battleFieldConfig = [];
    // static 转换完数组(结果)
    private static $battleStaticFieldArray = [];
    // static 数据格式设置
    private static $battleStaticFieldConfig = [];
    // detail 数据格式设置(结果)
    private static $battleDetailFieldConfig = [];
    // battle下 players转换完数组(结果)
    private static $battlePlayersFieldArray = [];
    // rounds下 players转换完数组(结果)
    private static $roundPlayersFieldArray = [];
    // match下 teams转换完数组(结果)
    private static $teamsFieldArray = [];
    // battle下 rounds转换完数组(结果)
    private static $roundsFieldArray = [];
    // rounds下 events转换完数组(结果)
    private static $roundEventsFieldArray = [];
    //team的临时数据
    private static $teamStatisticsArray1 = [];
    private static $teamStatisticsArray2 = [];
    private static $WinEndTypeArray = [
        'bomb_defused'   => 1,
        'ct_win'        => 2,
        'stopwatch'   => 3,
        'bomb_exploded'  => 4,
        't_win' => 5,
    ];
    public static $WinSideByEndTypeArray = [
        'bomb_defused'   => 'ct',
        'ct_win'        => 'ct',
        'stopwatch'   => 'ct',
        'bomb_exploded'  => 'terrorists',
        't_win' => 'terrorists',
    ];
    /**
     * run
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     */
    public static function run($tagInfo, $taskInfo)
    {
        $self = self::getInstance();
        $refreshType=@$tagInfo['ext2_type'];
        //hotInfo
        $hotInfo       = json_decode($taskInfo['params'], true);
        $hotId = $hotInfo['id'];
        $matchId       = $hotInfo['match_id'];
        $updateType    = $hotInfo['updateType'];
        if ($refreshType == 'refresh') {
            $relIdentityId = self::getRelIdByMasterId('match',$matchId,7);
        }else{
            $relIdentityId = $hotInfo['rel_identity_id'];
        }

        if (!$matchId) {
            return false;
        }else{
            $self->matchId = $matchId;
            $self->relIdentityId = $relIdentityId;
        }
        //matchFormat
        $self->dbMatch           = Match::find()->where(['id' => $matchId])->asArray()->one();
        $matchConversionInfo   = $self->formatMatch($relIdentityId);
        if (empty($matchConversionInfo)){
            return 'not find match';
        }
        if ($matchConversionInfo['status']==3){
            $matchEndStatus= true;
        }else{
            $matchEndStatus= false;
        }
        $infoFormat['match']   = $matchConversionInfo;
        $battlesConversionInfo = $self->formatBattles($relIdentityId, $updateType);
        $infoFormat['battles'] = $battlesConversionInfo;
        //refresh
        self::refresh($matchId, $infoFormat);
        if ($infoFormat['match']['status'] == 3) {

        }

    }


    /**
     * @param $relIdentityId
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getMatchById($relIdentityId){
        return  MatchAll::find()->where(['matche_id'=>$relIdentityId])->asArray()->one();
    }

    public function getMatchStatus($status){
        switch ($status) {
            //删除(相当于结束)
            case self::Match_STATUS_END:
                return 3;
                break;
            //结束
            case self::Match_STATUS_DELETE:
                return 3;
                break;
            //推迟(相当于未开始)
            case self::Match_STATUS_POSTPONED:
                return 1;
                break;
            //进行中
            case self::Match_STATUS_LIVE:
                return 2;
                break;
            default:
                return 2;
        }
    }

    public function getTeamScore(){
        $resArr          = [];
        $status          = $this->match['status'];
        $score1          = $this->match['score_one'];
        $score2          = $this->match['score_two'];
        $number_of_games = $this->match['number_of_games'];
        $match_type = $this->match['match_type'];
        if ($status == 'Match deleted' || strstr($status, 'd :') || $status == 'Match postponed') {
            return false;
        }
        if ($number_of_games == 1&&strstr($match_type,'Best of')&&$status=='Match over') {
            if ($score1 > $score2) {
                $resArr['score1'] = 1;
                $resArr['score2'] = 0;
            } else {
                $resArr['score1'] = 0;
                $resArr['score2'] = 1;
            }
        } else {
            $resArr['score1'] = $score1;
            $resArr['score2'] = $score2;
        }
        return $resArr;
    }

    public function getMatchWinner($match_team_1_score,$match_team_2_score,$battleCount){
        $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
        return $winnerInfo;
        if ($winnerInfo['is_finish'] == 1){
            return $winnerInfo['winner_team'];
        }else{
            return '';
        }
    }

    public function formatMatch($relIdentityId)
    {
        $match = $this->getMatchById($relIdentityId);
        if (!empty($match)) {
            $this->match = $match;
        } else {
            return false;
        }

        $realTeamOneId = $this->getTeamIdByRelId($match['team_one']);
        if ($realTeamOneId&&$this->dbMatch['team_1_id']&&$this->dbMatch['team_1_id']!=$realTeamOneId){
            $temp = $this->match['score_one'];
            $this->match['score_one'] = $this->match['score_two'];
            $this->match['score_two'] = $temp;

            $temp2 = $this->match['team_one'];
            $this->match['team_one'] = $this->match['team_two'];
            $this->match['team_two'] = $temp2;

        }

        //获取数据
        $match_begin_at_value = $match['match_time'] ? date('Y-m-d H:i:s', $match['match_time']) : '';
        $match_end_at_value   = $match['end_time'] ? date('Y-m-d H:i:s', $match['end_time']) : '';
        $match_status_value   = $this->getMatchStatus($match['status']);
        $teamScoreArr         = $this->getTeamScore();
        if (empty($teamScoreArr)) {
            return false;
        }

        $match_team_1_score_value = $teamScoreArr['score1'];
        $match_team_2_score_value = $teamScoreArr['score2'];
        $is_match_forfeit = $this->isMatchForfeit($match);

        $match_winner_arr       = $this->getMatchWinner($match_team_1_score_value, $match_team_2_score_value, $this->match['number_of_games']);
        if ($match_winner_arr['is_finish'] == 1){
            $match_winner_value =  $match_winner_arr['winner_team'];
        }else{
            $match_winner_value =  '';
        }
        if ($is_match_forfeit==1){
            if ($match_team_1_score_value>$match_team_2_score_value){
                $match_winner_value=1;
            }else{
                $match_winner_value=2;
            }
        }
        //match format
        $matchinfoFormat = [
            'begin_at'             => $match_begin_at_value == 'null' ? '' : (string)$match_begin_at_value,
            'end_at'               => $match_end_at_value == 'null' ? '' : (string)$match_end_at_value,
            'status'               => $match_status_value ? intval($match_status_value) : 2,
            'team_1_score'         => intval($match_team_1_score_value),
            'team_2_score'         => intval($match_team_2_score_value),
            'winner'               => intval($match_winner_value),
            'is_draw'               => $match_winner_arr['is_draw'],
            'is_battle_detailed'   => 1,
            'is_pbpdata_supported' => 1,

        ];
        return $matchinfoFormat;
    }

    public function isMatchForfeit($params){
        //比赛级弃权

        if (empty($params['description'])) {
            return 2;
        }
        $descriptionLtrim = ltrim($params['description'], "* ");
        $descriptionstr = preg_replace("/(>>>\*+)/", " * ", $descriptionLtrim);
        $descriptionArr = explode('*', $descriptionstr);
        $forfeitarrCC = '';
        foreach ($descriptionArr as $key => $val) {
            $patterns = "/.*forfeit.*the\smatch.*/";
            preg_match_all($patterns,$val,$arr);
            if (!empty($arr[0]) && $params['status'] == "Match over"){
                return 1;
            }

            $patterns2 = "/.*withdrew from the event.*/";
            preg_match_all($patterns2,$val,$arr2);
            if (!empty($arr2[0]) && $params['status'] == "Match over"){
                return 1;
            }

            $forfeitPreg = "/.*forfeit.*/";
            preg_match_all($forfeitPreg,$val,$forfeitarr);
            if (!empty($forfeitarr[0]) && $params['status'] == "Match over" &&  $params['number_of_games'] == '1'){
                return 1;
            }
            $forfeitarrCC = $forfeitarr[0];
        }

        if (!empty($forfeitarrCC)) {
            $mapsArr = explode('--',$params['maps']);
            $uniquemaps= array_unique($mapsArr);
            foreach ($uniquemaps as $mapKey => $mapVal) {
                $forfeitMap = strpos($forfeitarrCC[0], 'map');
                if ($forfeitMap === false && $params['number_of_games'] != '1' && $params['status'] == "Match over") {
                    return  1;
                }
            }
        }

        return 2;
    }

    private function formatBattles($matchId, $updateType = 1)
    {
        $this->updateType = $updateType;
        $battlesRes       = [];
        //查找所有battle
        $battles = $this->getBattles();
        if ($battles) {
            foreach ($battles as $key => $battle) {
                $battleInfo = $this->conversionBattles($battle);
                if ($battleInfo['order']) {
                    $battlesRes[$battleInfo['order']] = $battleInfo;
                }
            }
        }
        return $battlesRes;
    }



    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        // 初始化
        self::$battleFieldConfig = [
//            'status' => 3,
            'is_draw'            => 2,
            'is_forfeit'         => 2,
            'is_battle_detailed' => 1 ,//是否有对局详情数据
            'deleted'=>               2,
          'deleted_at'           => null
        ];
        self::$battleFieldConfig = array_merge(self::$battleFieldConfig, $baseResultData);
    }

    // set static数据
    public static function setBattleStatic($staticResultData)
    {
        // 初始化
        self::$battleStaticFieldConfig  = [];
        self::$battleStaticFieldArray[] = array_merge(self::$battleStaticFieldConfig, $staticResultData);
    }


    public function getBattleWinner($battle){
        $score1  = $battle['score_one'];
        $score2  = $battle['score_two'];
        if ($score1>$score2){
            return 1;
        }elseif ($score2>$score1){
            return 2;
        }else{
            return 3;
        }
    }

    /**
     * @param $battle
     * @return array|null[]
     */
    private function conversionBattles($battle)
    {
        $battleId = $battle['id'];
        $matchId  = $this->matchId;
        $mapInfo = MetadataCsgoMap::find()->where(['name'=>$battle['map']])->asArray()->one() ;
        $mapId = !empty($mapInfo)?$mapInfo['id']:10;
        // 初始化
        self::initializationBattleInfo();
        // battle_order
        $orderId = $battle['order_bat'];
        if (!empty($orderId)) {
            $battleBase = MatchBattle::find()->where([
                'match' => $matchId,
                'order' => $orderId,
            ])->one();

            $baseArr = [
                'game'    => self::CSGO_GAME_ID,
                'match'   => $matchId,
                'order'   => @$battle['order_bat'] ? $battle['order_bat'] : 0,
                'status'  => 3, //battle状态 有数据证明已经完成
                'is_draw' => @$battle['score_one'] == $battle['score_two'] && $battle['score_one'] && $battle['score_two'] ? 1 : 2,
                'winner'  => $this->getBattleWinner($battle), //获胜战队order
                'is_default_advantage' => 2, //是否是默认领先获胜
//                'duration' => null, //duration
                'map'  => $mapId, //地图
            ];
            if (!empty($battleBase['id'])){
                $baseArr['id'] = $battleBase['id'] ;
            }
            //base
            self::setBattleBase($baseArr);
        } else {
            return ['order' => null];
        }
        $mapstats_id = $battle['mapstats_id'];
        // rounds
        $this->getBattleRoundsDataByRedis($mapstats_id);
        // battle-players
        $this->getBattlePlayersByBattleId($mapstats_id);
        // static(teams)
        $this->getBattleTeams($battle);

        return [
            'order'         => @intval($battle['order_bat']),
            'base'          => self::$battleFieldConfig,
            'static'        => self::$teamsFieldArray,
            'players'       => self::$battlePlayersFieldArray,
            'detail'        => self::$battleDetailFieldConfig,
            'rounds'        => self::$roundsFieldArray,
            'round_players' => self::$roundPlayersFieldArray,
            'events'        => self::$roundEventsFieldArray
        ];
    }



    public function setDetails($battle)
    {
        self::$battleDetailFieldConfig = [
            'is_confirmed'                  => 1,
//            'round_time'                  => null,
//            'in_round_timestamp'                  => null,
//            'time_since_plant'                  => null,
            'win_round_1_side'              => @$battle['win_round_1_side'],
            'win_round_1_team'              => @intval($battle['win_round_1_team']),
            'win_round_1_detail'            => @$battle['win_round_1_detail'],
            'win_round_16_side'             => @$battle['win_round_16_side'],
            'win_round_16_team'             => @$battle['win_round_16_team'] ? @intval($battle['win_round_16_team']) : null,
            'win_round_16_detail'           => @$battle['win_round_16_detail'],
            'first_to_5_rounds_wins_side'   => @$battle['first_to_5_rounds_wins_side'],
            'first_to_5_rounds_wins_team'   => @intval($battle['first_to_5_rounds_wins_team']),
            'first_to_5_rounds_wins_detail' => @$battle['first_to_5_rounds_wins_detail'],
            'live_rounds'                   => @$battle['live_rounds']
        ];
    }


    public function trimArr($arr){
        foreach ($arr as $k=>$v){
            $arr[$k] = trim($v);
        }
        return $arr;
    }
    public function getBattleTeams($battle)
    {
        $startingSide         = $battle['starting_side'];
        $sideArr              = ['ct' => 'ct', 't' => 'terrorist'];
        $sideRevArr           = ['ct' => 'terrorist', 't' => 'ct'];
        $first_half_score     = $battle['first_half_score'];
        $end_half_score       = $battle['end_half_score'];
        $ot_score             = $battle['ot_score'];
        $team_rating          = $battle['team_rating'];
        $first_kills          = $battle['first_kills'];
        $clutches_won         = $battle['clutches_won'];
        $first_half_score_arr = explode(':', $first_half_score);
        $end_half_score_arr   = explode(':', $end_half_score);
        $ot_score_arr         = explode(':', $ot_score);
        $team_rating_arr      = explode(':', $team_rating);
        $first_kills_arr      = explode(':', $first_kills);
        $clutches_won_arr     = explode(':', $clutches_won);
        $first_half_score_arr = $this->trimArr($first_half_score_arr);
        $end_half_score_arr   = $this->trimArr($end_half_score_arr);
        $ot_score_arr         = $this->trimArr($ot_score_arr);
        $team_rating_arr      = $this->trimArr($team_rating_arr);
        $first_kills_arr      = $this->trimArr($first_kills_arr);
        $clutches_won_arr     = $this->trimArr($clutches_won_arr);
        $teamArr                             = [];
        $teamOneId                           = $this->getTeamIdByRelId($battle['teamid_one']);
        $team                                = Team::find()->where(['id' => $teamOneId])->asArray()->one();
       if ($this->dbMatch['team_1_id']==$teamOneId){
           $order_one = 1;
           $order_two = 2;
       }else{
           $order_one = 2;
           $order_two = 1;
       }
        $teamArr[0]['order']                 = $order_one;
        $teamArr[0]['score']                 = $battle['score_one'];
        $teamArr[0]['game']                  = self::CSGO_GAME_ID;

        $teamArr[0]['identity_id']           = $team['name'];
        $teamArr[0]['rel_identity_id']       = $battle['teamid_one'];
        $teamArr[0]['team_id']               = $team['id'];
        $teamArr[0]['name']                  = $team['name'];
        $teamArr[0]['starting_faction_side'] = $sideArr[$startingSide];
        $teamArr[0]['1st_half_score']        = $first_half_score_arr[0];
        $teamArr[0]['2nd_half_score']        = $end_half_score_arr[0];
        $teamArr[0]['ot_score']              = $ot_score_arr[0];
        $teamArr[0]['rating']                = $team_rating_arr[0];
        $teamArr[0]['first_kills']           = $first_kills_arr[0];
        $teamArr[0]['one_on_x_clutches']     = @$clutches_won_arr[0];
//        $teamArr[0]['multi_kills']     =null;

        $teamArr1 = self::$teamStatisticsArray1;
        if (is_array($teamArr1)){
            $teamArr[0] = array_merge($teamArr[0],$teamArr1);
        }

        //terrorist
        $teamArr[1]['order']                 = $order_two;
        $teamArr[1]['score']                 = $battle['score_two'];
        $teamArr[1]['game']                  = self::CSGO_GAME_ID;
        $teamTwoId                           = $this->getTeamIdByRelId($battle['teamid_two']);
        $team                                = Team::find()->where(['id' => $teamTwoId])->asArray()->one();
        $teamArr[1]['identity_id']           = $team['name'];
        $teamArr[1]['rel_identity_id']       = $battle['teamid_two'];
        $teamArr[1]['team_id']               = $team['id'];
        $teamArr[1]['name']                  = $team['name'];
        $teamArr[1]['starting_faction_side'] = $sideRevArr[$startingSide];
        $teamArr[1]['1st_half_score']        = $first_half_score_arr[1];
        $teamArr[1]['2nd_half_score']        = $end_half_score_arr[1];
        $teamArr[1]['ot_score']              = $ot_score_arr[1];
        $teamArr[1]['rating']                = $team_rating_arr[1];
        $teamArr[1]['first_kills']           = $first_kills_arr[1];
        $teamArr[1]['one_on_x_clutches']     = @$clutches_won_arr[1];
//        $teamArr[1]['multi_kills']     = null;

        $teamArr2 = self::$teamStatisticsArray2;
        if (is_array($teamArr2)){
            $teamArr[1] = array_merge($teamArr[1],$teamArr2);
        }
        self::$teamsFieldArray               = $teamArr;
    }

    public function getBattlePlayerListByMapstatsId($mapstats_id)
    {
        $where['mapstats_id'] = $mapstats_id;
        return MatchePlaryer::find()->where($where)->asArray()->all();
    }

    public function getTeamIdByRelId($relTeamId)
    {
        try {
            return self::getMainIdByRelIdentityId('team', $relTeamId, 7);
        } catch (Throwable $e) {
            $this->error_catch_log(['msg' => $e->getMessage()]);
        }
    }

    /**
     * @param $mapstats_id
     * @throws \yii\base\Exception
     */
    public function getBattlePlayersByBattleId($mapstats_id)
    {
        $i                = 0;
        $playerList       = $this->getBattlePlayerListByMapstatsId($mapstats_id);
        $teamArr1         = [];
        $teamArr2         = [];
        $countTeamPlayer1 = 0;
        $countTeamPlayer2 = 0;
        foreach ($playerList as $k => $v) {
            $v['headshot_kills'] = trim($v['headshot_kills'],'()');
            $v['flash_assists'] = trim($v['flash_assists'],'()');
            $v['assists'] = $v['assists']-$v['flash_assists'];
            $v['k_d_diff'] = trim($v['k_d_diff'],'+');
            $v['first_kills_diff'] = trim($v['first_kills_diff'],'+');
            $v['kast'] = trim($v['kast'],'%')/100;
            $i++;
            $playerArr          = [];
            $playerArr['game']  = self::CSGO_GAME_ID;
            //player
            $playerArr['player_id'] = @self::getMainIdByRelIdentityId('player', $v['player_id'], 7, 1);
            if (self::$battleFieldConfig['id']){
                if ($playerArr['player_id']){
                    $dbPlayer = MatchBattlePlayerCsgo::find()->where(['battle_id'=>self::$battleFieldConfig['id'],'player_id'=>$playerArr['player_id']])->asArray()->one();
                    if ($dbPlayer['order']){
                        $playerArr['order'] = $dbPlayer['order'];
                    }else{
                        continue;
                    }
                }else{
                    continue;
                }

            }else{
                $playerArr['order'] = $i;
            }


            //team
            $playerArr['team_order'] = @$this->getOpponentByBattleTeam($v['tm_id']);
            $playerArr['team_id']    = @$this->getTeamIdByRelId($v['tm_id']);
            $team                    = Team::find()->where(['id' => $playerArr['team_id']])->asArray()->one();
            $playerArr['team_name']  = @$team['name'];

            $player                 = Player::find()->where(['id' => $playerArr['player_id']])->asArray()->one();
            $playerArr['nick_name'] = @$player['nick_name'];
//            $playerArr['steam_id']  = @$player['steam_id'];
            //统计数据
            if ($playerArr['team_order'] == 1) {
                $countTeamPlayer1++;
                $teamArr1['kills']            += @$v['kills'];
                $teamArr1['headshot_kills']   += @$v['headshot_kills'];
                $teamArr1['deaths']           += @$v['deaths'];
                $teamArr1['kd_diff']         = $teamArr1['kills']-$teamArr1['deaths'];
                $teamArr1['assists']          += @$v['assists'];
                $teamArr1['flash_assists']    += @$v['flash_assists'];
                $teamArr1['kast']             += @$v['kast'];
                $teamArr1['adr']              += @$v['adr'];
                $teamArr1['first_kills']      += @$v['first_kills'];
                $teamArr1['first_deaths']     += @$v['first_deaths'];
                $teamArr1['first_kills_diff'] += @$v['first_kills_diff'];
                $teamArr1['rating']           += @$v['rating'];
            } else {
                $countTeamPlayer2++;
                $teamArr2['kills']            += @$v['kills'];
                $teamArr2['headshot_kills']   += @$v['headshot_kills'];
                $teamArr2['deaths']           += @$v['deaths'];
                $teamArr2['kd_diff']         = $teamArr2['kills']-$teamArr2['deaths'];
                $teamArr2['assists']          += @$v['assists'];
                $teamArr2['flash_assists']    += @$v['flash_assists'];
                $teamArr2['kast']             += @$v['kast'];
                $teamArr2['adr']              += @$v['adr'];
                $teamArr2['first_kills']      += @$v['first_kills'];
                $teamArr2['first_deaths']     += @$v['first_deaths'];
                $teamArr2['first_kills_diff'] += @$v['first_kills_diff'];
                $teamArr2['rating']           += @$v['rating'];
            }


            $playerArr['kills']              = @$v['kills'];
            $playerArr['headshot_kills']     = @$v['headshot_kills'];
            $playerArr['deaths']             = @$v['deaths'];
            $playerArr['k_d_diff']           = @$v['k_d_diff'];
            $playerArr['assists']            = @$v['assists'];
            $playerArr['flash_assists']      = @$v['flash_assists'];
            $playerArr['kast']               = (string)@$v['kast'];
            $playerArr['adr']                = (string)@$v['adr'];
            $playerArr['first_kills']        = @$v['first_kills'];
            $playerArr['first_deaths']       = @$v['first_deaths'];
            $playerArr['first_kills_diff']   = @$v['first_kills_diff'];
            $playerArr['rating']             = @(string)round($v['rating'], 2);
            self::$battlePlayersFieldArray[] = $playerArr;
        }
        if ($countTeamPlayer1) {
            $teamArr1['adr']    = (string)round(@$teamArr1['adr'] / $countTeamPlayer1, 2);
            $teamArr1['kast']   = (string)round(@$teamArr1['kast'] / $countTeamPlayer1, 4);
            $teamArr1['rating'] = (string)round(@$teamArr1['rating'] / $countTeamPlayer1, 2);
        } else {
            $teamArr1['adr']    = (string)0;
            $teamArr1['kast']   = (string)0;
            $teamArr1['rating'] = (string)0;
        }
        if ($countTeamPlayer2) {
            $teamArr2['adr']    = (string)round(@$teamArr2['adr'] / $countTeamPlayer2, 2);
            $teamArr2['kast']   = (string)round(@$teamArr2['kast'] / $countTeamPlayer2, 4);
            $teamArr2['rating'] = (string)round(@$teamArr2['rating'] / $countTeamPlayer2, 2);
        } else {
            $teamArr2['adr']    = (string)0;
            $teamArr2['kast']   = (string)0;
            $teamArr2['rating'] = (string)0;
        }
        //$teamArr1 $teamArr2是team统计为了给下一个方法中battleTeam使用
        self::$teamStatisticsArray1 = $teamArr1;
        self::$teamStatisticsArray2 = $teamArr2;
    }

    public function getRoundsByBattleIdOnApi($mapstats_id)
    {
        $where['mapstats_id'] = $mapstats_id;
        $list                 = RoundsDetail::find()->where($where)->asArray()->all();
        $last_ages = array_column($list,'round_ordinal');
        array_multisort($last_ages ,SORT_ASC,$list);
        return $list;
    }

    public function getOpponentByBattleTeam($teamId){
        $match = $this->match;
        $team1 = $match['team_one'];
        if ($teamId == $team1) {
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * @param $mapstats_id
     * @return bool
     */
    public function getBattleRoundsDataByRedis($mapstats_id)
    {
        $ctCount=0;
        $tCount=0;
        $rounds = $this->getRoundsByBattleIdOnApi($mapstats_id);
        foreach ($rounds as $key => $round) {
            if ($round['win_type']!='emptyHistory'){
                $roundArr = [];
                if (!empty($arrTeam)) {
                    $teamCtId = $arrTeam[$round['ct_id']];
                    $teamTId  = $arrTeam[$round['t_id']];
                } else {
                    $teamCtId                 = $this->getTeamIdByRelId($round['ct_id']);
                    $arrTeam[$round['ct_id']] = $teamCtId;
                    $teamTId                  = $this->getTeamIdByRelId($round['t_id']);
                    $arrTeam[$round['t_id']]  = $teamTId;
                }

                $roundArr['round_ordinal'] = (int)$round['round_ordinal'];
                $ctOpponent = $this->getOpponentByBattleTeam($round['ct_id']);
                $roundArr['side']['ct']['order'] = 1;
                $roundArr['side']['ct']['game'] = 1;
                $roundArr['side']['ct']['camp'] = 'ct';
                $roundArr['side']['ct']['side'] = 'ct';
                $roundArr['side']['ct']['round_ordinal'] = (int)@$round['round_ordinal'];
                $roundArr['side']['ct']['side_order'] = $ctOpponent;
                $roundArr['side']['ct']['team_id'] = (int)$teamCtId;

                $tOpponent = $this->getOpponentByBattleTeam($round['t_id']);
                $roundArr['side']['terrorist']['order'] = 1;
                $roundArr['side']['terrorist']['game'] = 1;
                $roundArr['side']['terrorist']['camp'] = 'terrorist';
                $roundArr['side']['terrorist']['side'] = 'terrorist';
                $roundArr['side']['terrorist']['round_ordinal'] = (int)@$round['round_ordinal'];
                $roundArr['side']['terrorist']['side_order'] = $tOpponent;
                $roundArr['side']['terrorist']['team_id'] = (int)$teamTId;

                $opponent = $this->getOpponentByBattleTeam($round['winner']);
                //winner_team
                if ($round['winner']) {
                    $roundArr['winner_team_id']         = $opponent;
                    $roundArr['winner_team']['team_id'] = $opponent;
                } else {
                    $roundArr['winner_team_id'] = null;
                    $roundArr['winner_team']    = [];
                }
                if (@$round['round_ordinal']==1){

                    $battle['win_round_1_side']= array_key_exists($round['win_type'], self::$WinSideByEndTypeArray) ?self::$WinSideByEndTypeArray[$round['win_type']]:null;
                    $battle['win_round_1_team']=(int)$opponent;
                    $battle['win_round_1_detail']="";
                }
                if (@$round['round_ordinal']==16) {
                    $battle['win_round_16_side']   = array_key_exists($round['win_type'], self::$WinSideByEndTypeArray) ? self::$WinSideByEndTypeArray[$round['win_type']] : null;
                    $battle['win_round_16_team']   = (int)$opponent;
                    $battle['win_round_16_detail'] = "";
                }
                if (array_key_exists($round['win_type'], self::$WinSideByEndTypeArray)){
                    $side = self::$WinSideByEndTypeArray[$round['win_type']];
                    if ($side=='ct'){
                        $ctCount++;
                    }else{
                        $tCount++;
                    }
                }
                if ((@$ctCount == 5 && $tCount < 5) || ($tCount == 5 && $ctCount < 5)) {
                    $battle['first_to_5_rounds_wins_side']= array_key_exists($round['win_type'], self::$WinSideByEndTypeArray) ?self::$WinSideByEndTypeArray[$round['win_type']]:null;
                    $battle['first_to_5_rounds_wins_team']=$opponent;
                    $battle['first_to_5_rounds_wins_detail']="";
                }
                $liveRoundsArr[] = $round['round_ordinal'];
                $battle['live_rounds'] = json_encode($liveRoundsArr,320);
                // detail
                $this->setDetails($battle);
                //winner_end_type
                $roundArr['winner_end_type'] = array_key_exists($round['win_type'], self::$WinEndTypeArray) ? self::$WinEndTypeArray[$round['win_type']] : null;
                $roundArr['status']          = 3;
                self::$roundsFieldArray[]    = $roundArr;
            }

        }
        return true;
    }




    // 初始化
    public function initializationBattleInfo()
    {
        self::$battlePlayersFieldArray = [];
        self::$roundPlayersFieldArray  = [];
        self::$teamsFieldArray         = [];
        self::$battleFieldConfig       = [];
        self::$battleDetailFieldConfig = [];
        self::$roundsFieldArray        = [];
        self::$roundEventsFieldArray   = [];
    }


    private static function refresh($matchId, $formatInfo)
    {
        // todo getMountedId,有可能会找不到，找不到的留空
        self::setMatch($matchId, $formatInfo['match']);
        foreach ($formatInfo['battles'] as $battle) {
            self::setBattle($matchId, $battle);
        }
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $battle['order'],
        ])->one();
        if (!$battleBase) {
            //battle创建信息
            $hotBase = new HotBase();
            $battleId = $hotBase->createMatchBattleByMatchId($matchId,$battle['order'],1);
        }else{
            $battleId                = $battleBase['id'];
        }
        $battle['base']['match'] = $matchId;
        BattleService::setBattleInfo($battleId, 'csgo', $battle['base'], BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['detail'], BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['static'], BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['players'], BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['rounds'], BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['events'], BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['round_players'], BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    public static function getBattleFormat()
    {

    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getBattles()
    {
        return BattlesAll::find()->where(['matche_id'=>$this->relIdentityId])->asArray()->all();
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}