<?php

namespace app\modules\task\services\hot\hltv;

use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\services\BattleService;
use app\modules\org\models\Team;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\hot\WsToApiSynchron;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Redis;
use yii\base\Exception;
use Throwable;
use app\modules\match\models\MatchLived;


class HltvMatchWs extends FiveHotCsgoMatchWs
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $killEvent = '';
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "hltv_ws";
    public $ctTeam = [];
    public $battle_duration = 0;
    public $muti = '';
    public $battleId = '';
    public $playerList = [];
    public $CurrentPlayerList = [];
    public $currentRoundDetails = [];
    public $relMatchId = '';
    public $needOutPutBattleEndEvent = '';
    public $needOutPutReloadEvent = '';
    public $needOutPutKillEvent = '';
    public $needOutPutUpComingEvent = '';
    public $needOutPutBattleStartEvent = '';
    public $needOutPutReStartEvent = '';
    public $needOutPutFrame = '';
    public $tempEventType = '';
    public $roundOrder = '';
    public $operate_type= '';

    public $assistDbList = [];
    public $battlePlayerList = [];
    const ORIGIN_ID = 7;

    /**
     * 消费 event queue
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function run($tagInfo, $taskInfo)
    {
        //info
        $self               = self::getInstance();
        $self->refresh_task = true;
        $refreshType        = @$tagInfo['ext2_type'];
        $refreshType        = $refreshType ? $refreshType : "one";
        $info               = json_decode($taskInfo['params'], true);
        $self->info         = $info;
        $originId           = $info['origin_id'];
        //match
        $relMatchId = $info['match_id'];
        if ($refreshType == 'refresh') {
            $self->refresh_task     = true;
            $self->refresh_task_str = [];
            $relMatchId             = $info['rel_match_id'];
            $self->matchId          = $info['match_id'];
            $self->operate_type = @$info['operate_type'];
        } else {
            $self->matchId = $self->getMatchIdByRealMatchId($relMatchId);
        }
        $self->relMatchId = $relMatchId;
        if (empty($self->matchId) || $originId != 7) {
            return false;
        }
        $self->match = $self->getMatchInfo($self->matchId);
        if (empty($self->match)) {
            return false;
        }
        if ($refreshType == 'refresh') {
            try {
                $redisDelKey = $self->currentPre . ':' . $self->matchId . ':';
                $self->batchDelRedisKey($redisDelKey);
                $self->clearBattleMatchId($self->matchId);
            } catch (Throwable $e) {
                $self->error_catch_log(['msg' => $e->getMessage()]);
            }
            $self->doAssistDbList();

            $livedObj = MatchLived::find()
//                ->andWhere(['<=','id',57552530])
                ->andWhere(['match_id' => (string)$relMatchId]);
            $count    = $livedObj->count();
            $limit    = 1000;
            if ($count > $limit) {
                $countSlice = (int)ceil($count / $limit);
                for ($i = 0; $i < $countSlice; $i++) {
                    $offset    = 0 + $limit * $i;
                    $list      = $livedObj->orderBy('id')->offset($offset)->limit($limit)->asArray()->all();
                    $countList = count($list);
                    foreach ($list as $k => $v) {
                        $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                        $matchEndIndex = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, 'match_end_index']);
                        $matchIndex  = $self->getMatchIndex();
                        if ($matchEnd&&$matchIndex-$matchEndIndex>5) {
                            $self->error_catch_log(['match_end' => true]);
                            $self->batchExpireRedisKey($redisDelKey);
                            return false;
                        }
                        if ($k + 1 == $countList) {
                            $self->refresh = true;
                            $self->error_catch_log(['end_refresh' => true]);
                        }
                        $self->info              = $v;
                        $self->info['socket_id'] = $v['id'];
                        $self->match_data        = json_decode($v['match_data'], true);
                        $self->doRun();
                    }
                }
            } else {
                $list      = $livedObj->orderBy('socket_time')->limit($limit)->asArray()->all();
                $countList = count($list);
                foreach ($list as $k => $v) {
                    $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                    if ($matchEnd) {
                        $self->error_catch_log(['match_end1' => true]);
                        return false;
                    }
                    if ($k + 1 == $countList) {
                        $self->refresh = true;
                        $self->error_catch_log(['end_refresh' => true]);
                    }
                    $self->info              = $v;
                    $self->info['socket_id'] = $v['id'];
                    $self->match_data        = json_decode($v['match_data'], true);
                    $self->doRun();
                }
            }
        } else {
            $self->match_data = $info['match_data'];
            $self->doRun();
        }
    }

    public function doAssistDbList(){
        $data = [];
        $assistEventList = MatchLived::find()->andwhere(['match_id'=>(string)$this->relMatchId])
            ->andWhere(['like','match_data','"killEventId"'])
            ->andWhere(['and',['type'=>'events'],['<','length(`match_data`)',1000]])
            ->asArray()->all();

        if (!empty($assistEventList)){
            foreach ($assistEventList as $k=>$v){
                $match_data = json_decode($v['match_data'],true);
                $killEventId = $match_data['log'][0]['Assist']['killEventId'];
                if (!empty($match_data)&&$killEventId){
                    $data[$killEventId] = $match_data['log'][0]['Assist'];
                }
            }
        }
        $this->assistDbList = $data;
        return $data;
    }

    public function setFrameSomeFieldsNull($frame)
    {

        if (!empty($frame['side']) && !empty($frame['side'][0]['players'])) {
            foreach ($frame['side'][0]['players'] as $k => $v) {
                $frame['side'][0]['players'][$k]['advanced']['equipment_value']        = null;
                $frame['side'][0]['players'][$k]['advanced']['blinded_time']           = null;
                $frame['side'][0]['players'][$k]['advanced']['ping']                   = null;
                $frame['side'][0]['players'][$k]['advanced']['damage']                 = null;
                $frame['side'][0]['players'][$k]['advanced']['damage_taken']                 = null;
                $frame['side'][0]['players'][$k]['advanced']['team_damage']            = null;
                $frame['side'][0]['players'][$k]['advanced']['hegrenade_damage_taken'] = null;
                $frame['side'][0]['players'][$k]['advanced']['inferno_damage_taken']   = null;
                $frame['side'][0]['players'][$k]['advanced']['chicken_kills']          = null;
                $frame['side'][0]['players'][$k]['advanced']['blind_enemy_time']       = null;
                $frame['side'][0]['players'][$k]['advanced']['blind_teammate_time']    = null;
            }
        }

        if (!empty($frame['side']) && !empty($frame['side'][1]['players'])) {
            foreach ($frame['side'][1]['players'] as $k => $v) {
                $frame['side'][1]['players'][$k]['advanced']['equipment_value']        = null;
                $frame['side'][1]['players'][$k]['advanced']['blinded_time']           = null;
                $frame['side'][1]['players'][$k]['advanced']['ping']                   = null;
                $frame['side'][1]['players'][$k]['advanced']['damage']                 = null;
                $frame['side'][1]['players'][$k]['advanced']['damage_taken']                 = null;
                $frame['side'][1]['players'][$k]['advanced']['team_damage']            = null;
                $frame['side'][1]['players'][$k]['advanced']['hegrenade_damage_taken'] = null;
                $frame['side'][1]['players'][$k]['advanced']['inferno_damage_taken']   = null;
                $frame['side'][1]['players'][$k]['advanced']['chicken_kills']          = null;
                $frame['side'][1]['players'][$k]['advanced']['blind_enemy_time']       = null;
                $frame['side'][1]['players'][$k]['advanced']['blind_teammate_time']    = null;
            }
        }
        return $frame;
    }

    public function tempEventToDb($needOutPutEvent, $type)
    {
        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_EVENTS], json_encode($needOutPutEvent, 320));
        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_EVENTS], json_encode($needOutPutEvent, 320));
        //存入socket
        $event_data_ws  = [
            'match_data' => json_encode($needOutPutEvent, 320),
            'type'       => 'events',
            'game_id'    => 1,
            'match_id'   => $this->matchId
        ];
        $outputEventArr = ['round_time_frozen_start', 'round_freezetime_start', 'battle_switch_team', 'team_switchside', 'battle_up_coming', 'battle_upcoming', 'battle_restart', 'battle_start', 'battle_end', 'battle_pause', 'battle_unpause', 'battle_reloaded', 'player_joined', 'player_quit', 'round_start', 'round_end', 'player_kill', 'bomb_planted', 'bomb_defused', 'player_suicide', 'suicide'];
        if (in_array($needOutPutEvent['event_type'], $outputEventArr)) {
            $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_MAIN_EVENTS], json_encode($needOutPutEvent, 320));
            $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_MAIN_EVENTS], json_encode($needOutPutEvent, 320));
            $this->saveSocketToDb($event_data_ws);
        }
        if ($needOutPutEvent['event_type'] == 'battle_end'){
            $this->setBattleWinner('');//battle winner 重置
            $this->setBattleStatus(2);
            $this->battle_duration = 0;
            //create battle tag (set warmUp-1)
            $current_order = $this->createBattleTagOnBattleStart($this->info['socket_time']);
            $this->setCurrentOrder($current_order);
        }
        $this->$type = '';
        return true;
    }

    public function  doRun(){
        try {

            $matchEnd = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
            if ($matchEnd) {
                $has_expire = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE]);
                if (!$has_expire){
                    $redisDelKey = $this->currentPre .':' . $this->matchId . ':';
                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE],1);
                    $this->batchExpireRedisKey($redisDelKey);
                }
                $this->error_hash_log('match end after events',['event_id'=>$this->info['socket_id']]);
//                return  false;
            }
            $this->record_first_kill = false;
            $this->record_bomb_planted = false;
            $this->record_knife_kill = false;
            $this->record_taser_kill = false;
            $this->record_ace_kill = false;
            $this->record_team_kill = false;
            $this->event = [];
            $this->frame = [];
            $countInfoKey = $this->currentPre . ':tag_csgo_count_info:' . $this->matchId;
            $this->redis->incr($countInfoKey);
            $this->redis->expire($countInfoKey, 3600);
//            if ($this->info['socket_id']<13420280){
//
////                $this->error_catch_log(['test'=>true]);
//                return false;
//            }
            //判断事件的类型 websocket类型
            switch ($this->info['type']) {
                case self::CSGO_EVENT_INFO_TYPE_NO:
                    $this->transFramesWs();
                    if ($this->needOutPutKillEvent){
                        $this->tempEventToDb($this->needOutPutKillEvent,'needOutPutKillEvent');
                    }
                    if ($this->needOutPutUpComingEvent){
                        $this->tempEventToDb($this->needOutPutUpComingEvent,'needOutPutUpComingEvent');
                    }
                    if ($this->needOutPutReStartEvent){
                        $this->tempEventToDb($this->needOutPutReStartEvent,'needOutPutReStartEvent');
                    }
                    if ($this->needOutPutBattleStartEvent){
                        $this->tempEventToDb($this->needOutPutBattleStartEvent,'needOutPutBattleStartEvent');
                    }
                    //tempEvents
                    if ($this->needOutPutBattleEndEvent){
                        $this->tempEventType = 'battle_end';
                    }else{
                        $this->tempEventType='frame';
                    }
                    //battleEndEvent
                    if ($this->needOutPutBattleEndEvent){
                        $this->tempEventToDb($this->needOutPutBattleEndEvent,'needOutPutBattleEndEvent');
                    }
                    if (!empty($this->frame)){
                        $this->frame = $this->setFrameSomeFieldsNull($this->frame);

                        //to api
                        $this->refreshToApi();
                        $nowTime     = time();
                        $redisKey    = $this->currentPre . ':tag_csgo_frame_refresh_time:' . $this->matchId;
                        $refreshTime = $this->redis->get($redisKey);
                        if ($refreshTime) {
                            $diff_time = $nowTime - $refreshTime;
                            if ($diff_time >= 1 || $this->match_data['frozen']==true || $this->frame['match_winner']['team_id']) {
                                $this->redis->set($redisKey, $nowTime, 3600);
                            } else {
                                $this->frame =[];
                                break;
                            }
                        } else {
                            $this->redis->set($redisKey, $nowTime, 3600);
                        }
                        //push frame
                        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_FRAMES], json_encode($this->frame,320));
                        //存入socket
                        $frames_data    = [
                            'match_data' => json_encode($this->frame,320),
                            'type'       => 'frames',
                            'game_id'    => 1,
                            'match_id'   => $this->matchId
                        ];
                        if (!empty($this->frame)){
                            $this->saveSocketToDb($frames_data);
                        }
                    }

                    $this->frame =[];
                    break;
                case self::CSGO_EVENT_INFO_TYPE_EVENTS:
                    $this->transEventsWs();
                    if ($this->needOutPutKillEvent){
                        $this->tempEventToDb($this->needOutPutKillEvent,'needOutPutKillEvent');
                    }
                    if ($this->needOutPutUpComingEvent){
                        $this->tempEventToDb($this->needOutPutUpComingEvent,'needOutPutUpComingEvent');
                    }
                    if ($this->needOutPutReStartEvent){
                        $this->tempEventToDb($this->needOutPutReStartEvent,'needOutPutReStartEvent');
                    }
                    if ($this->needOutPutReloadEvent){
                        $this->tempEventToDb($this->needOutPutReloadEvent,'needOutPutReloadEvent');
                        $this->tempEventType = 'battle_reloaded';
                    }
                    //tempEvents
                    if ($this->needOutPutBattleEndEvent){
                        $this->tempEventType = 'battle_end';
                    }

                    if (!empty($this->killEvent)){
                        $this->doSpecialEventDetails($this->killEvent);
                        $this->killEvent = '';
                    }

                    if (!empty($this->event)){
                        //to api
                        $this->refreshToApi();
                        if (empty($this->killEvent)){
                            $this->doSpecialEventDetails($this->event);
                        }
                        if ($this->needOutPutFrame){
                            $this->frame = $this->needOutPutFrame;
                            $this->frame = $this->setFrameSomeFieldsNull($this->frame);
                            //push frame
                            $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_FRAMES], json_encode($this->frame,320));
                            //存入socket
                            $frames_data    = [
                                'match_data' => json_encode($this->frame,320),
                                'type'       => 'frames',
                                'game_id'    => 1,
                                'match_id'   => $this->matchId
                            ];
                            if (!empty($this->frame)){
                                $this->saveSocketToDb($frames_data);
                            }
                            $this->needOutPutFrame = '';
                            $this->frame =[];
                        }
                        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_EVENTS], json_encode($this->event,320));
                        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_EVENTS], json_encode($this->event,320));
                        //存入socket
                        $event_data_ws = [
                            'match_data' => json_encode($this->event,320),
                            'type'       => 'events',
                            'game_id'    => 1,
                            'match_id'   => $this->matchId
                        ];
                        $outputEventArr = ['round_time_frozen_start','round_freezetime_start','battle_switch_team', 'team_switchside', 'battle_up_coming','battle_upcoming', 'battle_restart', 'battle_start', 'battle_end', 'battle_pause', 'battle_unpause', 'battle_reloaded', 'player_joined', 'player_quit', 'round_start', 'round_end', 'player_kill', 'bomb_planted', 'bomb_defused', 'player_suicide', 'suicide'];
                        if (in_array($this->event['event_type'],$outputEventArr)){
                            $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_MAIN_EVENTS], json_encode($this->event, 320));
                            $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_MAIN_EVENTS], json_encode($this->event, 320));
                            $this->saveSocketToDb($event_data_ws);
                        }
                        $this->event =[];
                        //tempEvents
                        if ($this->needOutPutBattleEndEvent){
                            $this->tempEventToDb($this->needOutPutBattleEndEvent,'needOutPutBattleEndEvent');

                        }
                    }
                    $this->tempEventType='';
                    $this->event =[];
                    break;
            }
        } catch (Throwable $e) {
            $this->error_catch_log(['msg'=>$e->getMessage()]);
        }

    }

    /**
     * 把socket存到db
     * @param $data_ws
     * @return int
     * @throws \yii\db\Exception
     */
    public function saveSocketToDb($data_ws){
        if ($this->operate_type != 'xiufu') {
            KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'),$data_ws);
        }
    }

    /**
     * 单例
     * @return HltvMatchWs
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    public function doTempKillEvent($event_type = ''){
        $tempKillEvent = $this->getTempPlayerKillEvent();
        $typeArr = [
            'player_kill',
            'round_end'
        ];
        if (!empty($tempKillEvent) && $tempKillEvent['socket_time'] && ((strtotime($this->info['socket_time']) - strtotime($tempKillEvent['socket_time']) > 1) || in_array($event_type,$typeArr))) {
            unset($tempKillEvent['socket_time']);
            unset($tempKillEvent['orgin_eventId']);
            $this->needOutPutKillEvent = $tempKillEvent;
            $this->setTempPlayerKillEvent('');
        }
    }


    public function createNewBattleOrUpcoming($type)
    {

        $match_data  = $this->match_data;
        $matchIndex  = $this->getMatchIndex();
        $upComingNew = $this->getUpComingNew();
        if ($type == 'frame') {
            if (!empty($upComingNew) && $upComingNew['index'] && $upComingNew['index'] + 1 == $matchIndex) {
                if (@$match_data['currentRound'] == 1) {
                    //符合新upcoming
                    $upcomingContent               = $this->getValue([self::KEY_TAG_CURRENT, 'match', 'content_battle_up_coming']);
                    $this->needOutPutUpComingEvent = json_decode($upcomingContent, true);
                    //restart
                    $contentRestartContent               = $this->getValue([self::KEY_TAG_CURRENT, 'match', 'content_restart']);
                    $this->needOutPutReStartEvent = json_decode($contentRestartContent, true);
                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], true);
                } else {
                    $upComingKey = ['current', 'match', 'upComingNew'];
                    $this->setValue($upComingKey, '');//重置upcoming
                }
            }
            $battleStartNew = $this->getBattleStartNew();
            if (!empty($battleStartNew) && $battleStartNew['index'] && $battleStartNew['index'] + 1 == $matchIndex) {
                if (@$match_data['currentRound'] == 1) {
                    //符合新的battle_start
                    $this->createNewBattle();
                } else {
                    $battleStartKey = ['current', 'match', 'battleStartNew'];
                    $this->setValue($battleStartKey, '');//重置battleStart
                }
            }
        } else {
            if (!empty($upComingNew) && $upComingNew['index'] && $upComingNew['index'] + 1 == $matchIndex) {
                if ($this->info['socket_time'] != $upComingNew['matchStarted']) {

                    //符合新upcoming
                    $upcomingContent               = $this->getValue([self::KEY_TAG_CURRENT, 'match', 'content_battle_up_coming']);
                    $this->needOutPutUpComingEvent = json_decode($upcomingContent, true);
                    //restart
                    $contentRestartContent               = $this->getValue([self::KEY_TAG_CURRENT, 'match', 'content_restart']);
                    $this->needOutPutReStartEvent = json_decode($contentRestartContent, true);
                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], true);
                } else {
                    $upComingKey = ['current', 'match', 'upComingNew'];
                    $this->setValue($upComingKey, '');//重置upcoming
                }
            }
        }

    }

    public function createNewBattle(){

        //创建battle new
        //create battle tag
//        $old_order = $this->getBattleId();
        $current_order = $this->createBattleTagOnBattleStart($this->info['socket_time']);
        if (empty($current_order)){
            return false;
        }
        //符合新battleStart
        $battleStartContent               = $this->getValue([self::KEY_TAG_CURRENT, 'match', 'content_battle_start']);
        $this->needOutPutBattleStartEvent = json_decode($battleStartContent, true);
        $this->setCurrentOrder($current_order);

        //clear round order && set battle begin_at
        $this->setRoundOrder(1);
        $this->setBattleBeginAt($this->info['socket_time']);//battle 开始时间
        $this->setBattleWinner('');//battle winner 重置
        $this->setBattleStatus(2);//battle status
        //clear round score
        $this->clearRoundScore();
        //isUpComing
        $upComing = $this->getUpComing();
        if ($upComing) {
            //set is_live true
            $this->setIsLive(true);
            //set match status
            $this->setMatchStatus(self::STATUS_GOING);//match 状态
            //battle_order
            $this->setBattleIdByRealOrder();
            $now_battle_order = $this->getBattleOrder();

        } else {
            //设置试抢局的battle
            $this->setWarmUpBattleId();
        }
        //清除选手身上的武器
        $this->clearPlayerWeapons();
        //大初始化
        $this->createSnapshot('battle');
        $list = $this->getPlayerListByType('all');
        foreach ($list as $k => $v) {
            if ($v['side'] == 'ct') {
                $team = $this->getCurrentCt();
            }
            if ($v['side'] == 'terrorist') {
                $team = $this->getCurrentT();
            }
            $playerArr['team_log_name'] = @$team['log_name'];
            $playerArr['team_name']     = @$team['name'];
            $playerArr['team_id']       = @$team['team_id']?$team['team_id']:null;
            $playerArr['team_order']    = @$team['opponent_order'];
            $this->setPlayerBySteamId($v['steam_id'], $playerArr);
        }
        //battle details
        $this->updateBattleDetailsOnBattleStart();
    }

    /**
     * @return array
     */
    public function transFramesWs()
    {

        $matchIndexKey = ['current', 'match', 'index'];
        $this->incr($matchIndexKey);
        $this->createNewBattleOrUpcoming('frame');
        $this->doTempKillEvent();//temp kill event
        $this->setBattleIdByMap();//map
        $doTeamRes = $this->doTeamName();//terroristTeamName ctTeamName
        if (empty($doTeamRes)) {
            return [];
        }
        $this->doCurrentRound();//currentRound
//        $this->setBombPlanted($this->match_data['bombPlanted']);//bombPlanted
        $this->objectSet();//during
        $this->doLive();//terroristTeamName ctTeamName
        //playerList
        $this->playerList          = $this->getTablePlayerList();

        $this->battlePlayerList    = $this->getBattlePlayerListByType('all', $this->getBattleId(),'steam_id','hltv');
        $this->currentRoundDetails = $this->getRoundDetails();
        $this->battleId   = $this->getBattleId();
//        $this->diffBattleList = $this->battle
        if (!empty($this->match_data['TERRORIST'])) {
            $terrorist = $this->match_data['TERRORIST'];
            foreach ($terrorist as $k => $v) {
                if (empty($this->playerList) || !array_key_exists($v['steamId'], $this->playerList)) {
                    $this->getHltvPlayerInfoBySteamId($v['dbId'], $v['nick'],$v['steamId']);
                }
                $arrTPlayerKeys[] = $v['steamId'];
            }
        }

        if (!empty($this->match_data['CT'])) {
            $ct = $this->match_data['CT'];
            foreach ($ct as $k => $v) {
                if (empty($this->playerList) || !array_key_exists($v['steamId'], $this->playerList)) {
                    $this->getHltvPlayerInfoBySteamId($v['dbId'], $v['nick'],$v['steamId']);
                }
                $arrCTPlayerKeys[] = $v['steamId'];
            }
        }
        if (!empty($this->battlePlayerList)&&!empty($arrTPlayerKeys)&&!empty($arrCTPlayerKeys)){
            foreach ($this->battlePlayerList as $k=>$v){
                if (!in_array($v['steamId'],$arrTPlayerKeys)&&!in_array($v['steamId'],$arrCTPlayerKeys)){
                    $keyCurrent = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
                    $keyHistory = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->battleId, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
                    unset($this->battlePlayerList[$v['steamId']]);
                    $this->setBattlePlayerBySteamId($v['steamId'],['is_deleted'=>1,'is_alive'=>false]);
                    $this->setCurrentPlayerBySteamId($v['steamId'],['is_deleted'=>1,'is_alive'=>false]);
//                    $this->hDel($keyCurrent,$v['steamId']);
//                    $this->hDel($keyHistory,$v['steamId']);
//                    $this->error_hash_log('player_list_error',$v['steamId'].'选手多余了');
                }
            }
        }

        //muti

        $this->roundOrder = $this->getRoundOrdinal();
        $this->muti       = $this->redis->multi(Redis::PIPELINE);//pipe
        if (!empty($this->match_data['TERRORIST'])) {
            $this->doTerrorist();
        }
        if (!empty($this->match_data['CT'])) {
            $this->doCt();
        }
        $this->redis->exec();//muti end

        //round history
        if (!empty($this->match_data['ctMatchHistory'])) {
            $this->doCtMatchHistory();
        }
        if (!empty($this->match_data['terroristMatchHistory'])) {
            $this->doTerroristMatchHistory();
        }
        $this->doScore();
        $this->doFreezeTime();

        $this->frame = [];
        //根据redis的元数据
        $this->frame['match_id']           = (int)$this->matchId;
        $this->frame['game_rules']         = $this->getGameRules();
        $this->frame['match_type']         = $this->getMatchType();
        $this->frame['number_of_games']    = $this->getNumberOfGames();
        $battleId                          = $this->getOutPutBattleId();
        $this->frame['battle_id']          = $battleId ? (int)$battleId : null;
        $this->frame['battle_order']       = $this->getOutPutBattleOrder();
        $this->frame['map']                = $this->outPutMap($this->getCurrentMap());
        $this->frame['duration']           = $this->battle_duration;
        $this->frame['is_battle_finished'] = $this->isBattleFinished();
        $this->frame['is_match_finished']  = $this->isFinished();
        $this->frame['match_scores']       = $this->outPutMatchScores($this->getMatchScores());
        $this->frame['match_winner']       = $this->outPutMatchWinner($this->getMatchWinner());
        $this->frame['battle_winner']      = $this->outPutBattleWinner($this->getBattleWinner());
        $this->frame['starting_ct']        = $this->outPutTeam($this->getStartingCt());
        $this->frame['starting_t']         = $this->outPutTeam($this->getStartingT());
        $this->frame['is_pause']           = null;
        $this->frame['is_live']            = $this->isLive();
        $this->frame['current_round']      = $this->currentRound();
        $this->frame['is_freeze_time']     = null;
        $this->frame['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);
        $this->frame['round_time']         = $this->frame['in_round_timestamp']==0?115:$this->getRoundTime($this->info['socket_time']);
        $this->frame['is_bomb_planted']    = $this->getIsBombPlanted();
        $this->frame['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
        $this->frame['side']               = $this->getSide('hltv');
        $this->frame['server_config']      = $this->getServerConfigFrames();
        if ($this->needOutPutBattleEndEvent&&$this->needOutPutBattleEndEvent['event_type']=='battle_end'){
            $this->setBattleWinner('');//battle winner 重置
            $this->setBattleStatus(2);
            //create battle tag (set warmUp-1)
            $current_order = $this->createBattleTagOnBattleStart($this->info['socket_time']);
            $this->setCurrentOrder($current_order);
        }
        return $this->frame;
    }

    /**
     * @return mixed
     */
    public function getMatchIndex(){
        $matchIndexKey =['current','match','index'];
        return $this->getValue($matchIndexKey);
    }



    public function transEventsWs()
    {
//        return false;//test
        $match_data = $this->match_data;
        if (key($match_data['log'][0]) != 'Status') {
            $matchIndexKey = ['current', 'match', 'index'];
            $this->incr($matchIndexKey);
        }
        $this->objectSet();
        if (!empty($match_data['log'][0])&&count($match_data['log'])==1){
            $event_type                        = key($match_data['log'][0]);
            $realEventType                     = $this->getRealEventType($event_type);
            $this->event['match_id']           = (int)$this->matchId;
            $battleId                          = $this->getOutPutBattleId();
            $this->event['battle_id']          = $battleId ? (int)$battleId : null;
            $battle_order  = $this->getOutPutBattleOrder();
            $this->event['battle_order']       = $battle_order?(int)$battle_order:null;
            $this->event['battle_timestamp']   = $this->battle_duration;
            $this->event['event_id']           = (int)$this->info['socket_id'];
            $this->event['event_type']         = $realEventType;
            $this->event['round_ordinal']      = $this->getRoundOrdinal();
            $this->event['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);//回合内时间戳(event的log_time)
            $this->event['round_time']         = $this->getRoundTime($this->info['socket_time']); //回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
            $this->event['is_bomb_planted']    = $this->getIsBombPlanted();
            $this->event['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);

            $this->doTempKillEvent($realEventType);


            switch ($event_type) {
                //比赛即将开始 battle_up_coming
                case  'MatchStarted':
                    return $this->matchStartedEvent();
                    break;
                case  'RoundStart':
                    return $this->roundStartEvent();
                    break;
                case  'PlayerQuit':
                    return $this->playerQuitEvent();
                    break;
                case  'PlayerJoin':
                    return $this->playerJoinEvent();
                    break;
                case  'Kill':
                    return $this->killEvent();
                    break;
                case  'Assist':
                    return $this->assistEvent();
                    break;
                case  'Restart':
                    return $this->restartEvent();
                    break;
                case  'Suicide':
                    return $this->suicideEvent();
                    break;
                case  'RoundEnd':
                    return $this->roundEndEvent();
                    break;
                case  'BombPlanted':
                    return $this->bombPlantedEvent();
                    break;
                case  'BombDefused':
                    return $this->bombDefusedEvent();
                    break;
            }
        }else{
            //错误事件,无需处理
            $this->event = [];
        }
    }

    /**
     * ws到api
     * @return bool
     * @throws \yii\db\Exception
     */
    public function refreshToApi()
    {
        if (!$this->isLive()) {
            return false;
        }
        //task refresh api
        $nowTime     = time();
        $redisKey    = $this->currentPre . ':tag_csgo_api_refresh_time:' . $this->matchId;
        $refreshTime = $this->redis->get($redisKey);
        $eventType   = $this->event['event_type'];
        if ($eventType == 'default') {
            //心跳事件
            return false;
        }
        if ($refreshTime) {
            $diff_time      = $nowTime - $refreshTime;
            $updateTypeArr3 = ['battle_reloaded', 'battle_end', 'battle_start'];
            if ($diff_time > 2 || $this->refresh == true || in_array($this->tempEventType, $updateTypeArr3) || in_array($this->event['event_type'], $updateTypeArr3) || (!empty($this->event) && $this->event['event_type'] == 'round_end')) {
                if ($this->refresh == true || in_array($this->tempEventType, $updateTypeArr3) || in_array($this->event['event_type'], $updateTypeArr3)) {
                    if (in_array($this->tempEventType, $updateTypeArr3)) {
                        $eventType           = $this->tempEventType;
                        $this->tempEventType = '';
                    }
                    $updateType = 3;
                } else {
                    $updateType = 2;
                }
                $eventType = $eventType?$eventType:$this->tempEventType;
                $this->refresh = false;
                $paramArr      = [
                    'updateType' => $updateType,//hot_refresh
                    'match_id'   => $this->matchId,
                    'currentPre' => 'hltv_ws',
                    'event_id'   => @(int)$this->info['socket_id'],
                    'event_type' => @$eventType,
                    'log_time'   => @$this->info['socket_time'],
                ];
                $this->lPush(['ws_to_api'],json_encode($paramArr,320));
                WsToApiSynchron::rPushSynchron('hltv_ws', $this->matchId, $paramArr);
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_EVENT_WS_TO_API,
                        "5eplay", "", "", ""),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => $paramArr,
                ];
                TaskRunner::addTask($item, 4);
                $this->redis->set($redisKey, $nowTime, 3600);
            }
        } else {
            $this->redis->set($redisKey, $nowTime, 3600);
        }
    }

    public function getTablePlayerList(){
        $list = $this->hGetAll(['table','player']);
        $newList = [];
        if (!empty($list)){
            foreach ($list as $k=>$v){
                $newList[$k] = json_decode($v,true);
            }
        }
        return $newList;

    }

    public function getMatchIdByRealMatchId($relMatchId){
        $redisKey    = $this->currentPre.':tag_csgo_real_match:' . $relMatchId;
        $matchId = $this->redis->get($redisKey);
        if ($matchId){
            return $matchId;
        }else{
            $matchId = self::getMainIdByRelIdentityId('match', $relMatchId, self::ORIGIN_ID);
            $this->redis->set($redisKey, $matchId, 3600);
            return $matchId;
        }

    }

    /**
     * getIsBombPlanted
     * @return mixed
     */
    public function getIsBombPlanted()
    {
        if ($this->event['event_type'] == 'bomb_planted') {
            return true;
        }
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_BOMB_PLANTED];
        return $this->getValue($key) ? true : false;
    }

    /**
     * battle end
     * @return mixed
     */
    public function doFreezeTime(){
        $match_data = $this->match_data;
        $froze      = $match_data['frozen'];
        if ($froze == true) {
            return false;
            $battleFinish = $this->isBattleFinished();
            if ($battleFinish){
                return false;
            }
//            $countMap = $this->getNewBattleCountByMap($this->match_data['mapName']);
//            if ($countMap>=1){
//                return false;
//            }
//            $this->incrNewBattleCountEveryMap($this->match_data['mapName']);
            //battle_end
            //比较最近的battle_end事件
            $recentRoundEndTime = $this->getRoundEndAtFlag();
            if ($recentRoundEndTime&&(strtotime($this->info['socket_time'])-strtotime($recentRoundEndTime)<2)){
                $nowOutPut = true;
            }else{
                $nowOutPut = false;
            }
            //update kills
            $this->updateKillsOnFrozen();
            //update kast
//            $this->updateKastOnRoundEnd();
            //update rating
            $this->updateRatingOnRoundEnd();
            //statis
            $this->setRoundPlayerStatisOnRoundEnd('ct');
            $this->setRoundPlayerStatisOnRoundEnd('t');
            //statis
            $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
            $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
            if ($match_data['counterTerroristScore'] >$match_data['terroristScore']) {
                $winner = 'ct';
            } elseif($match_data['counterTerroristScore'] < $match_data['terroristScore']){
                $winner = 'terrorist';
            }else {
                return false;
            }
            $this->setIsLive(false);
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], false);
            $this->setBattleStatus(self::STATUS_END);
            if ($winner == 'ct') {
                $winnerTeam     = $this->getCurrentCt();
                $opponent_order = $winnerTeam['opponent_order'];
            } else {
                $winnerTeam     = $this->getCurrentT();
                $opponent_order = $winnerTeam['opponent_order'];
            }
            $this->event['match_id']           = (int)$this->matchId;
            $this->event['battle_id']          = (int)$this->getOutPutBattleId();
            $this->event['battle_order']       = (int)$this->getOutPutBattleOrder();
            $this->event['battle_timestamp']   = $this->battle_duration;
            $this->event['event_id']           = (int)$this->info['socket_id'];
            $this->event['event_type']         = 'battle_end';
            $this->event['round_ordinal']      = $this->getRoundOrdinal();
            $this->event['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);//回合内时间戳(event的log_time)
            $this->event['round_time']         = $this->getRoundTime($this->info['socket_time']); //回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
            $this->event['is_bomb_planted']    = $this->getIsBombPlanted();
            $this->event['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
            $winnerTeam['team_id']             = $winnerTeam['team_id'] ? (int)$winnerTeam['team_id'] : null;
            $winnerTeam['image']=ImageConversionHelper::showFixedSizeConversion(@ $winnerTeam['image'],200,200);
            $this->event['winner']             = $winnerTeam;
            $this->setValue([self::KEY_TAG_CURRENT,self::KEY_TAG_EVENTS,self::KEY_TAG_BATTLE_END],json_encode($this->event,320));
            if ($nowOutPut){
                $this->needOutPutBattleEndEvent = $this->event;
            }else{
                $this->setTempBattleEndEvent(json_encode($this->event,320));
            }

            if ($opponent_order == 1) {
                $this->incrMatch1Score();
                $this->setMatch2Score($this->getMatch2Score());
            } else {
                $this->incrMatch2Score();
                $this->setMatch1Score($this->getMatch1Score());
            }
            $match_team_1_score = $this->getMatch1Score();
            $match_team_2_score = $this->getMatch2Score();
            $battleCount        = $this->match['number_of_games'];
            //set match end&&winner
            $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
            //计算比赛结束
            if ($winnerInfo['is_finish'] == 1) {
                $matchIndex = $this->getMatchIndex();
                $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, 'match_end_index'],$matchIndex);
                $this->setMatchStatus(self::STATUS_END);
                if ($nowOutPut){
                    $this->setMatchEndAt($this->info['socket_time']);
                }

                $this->setMatchWinner($winnerTeam);
            } elseif ($winnerInfo['is_draw'] == 1) {
                $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_IS_DRAW], true);
            }
            //update battle details score
            $this->setBattleEndAt($this->info['socket_time']);
            $this->setBattleWinner($winnerTeam, $winner);
            if ($match_data['ctTeamId'] == $match_data['startingCt']) {
                $baseArr['start_ct']['score'] = @$match_data['counterTerroristScore'];
                $baseArr['start_t']['score']  = @$match_data['terroristScore'];
            } else {
                $baseArr['start_ct']['score'] = @$match_data['terroristScore'];
                $baseArr['start_t']['score']  = @$match_data['counterTerroristScore'];
            }
            $this->setCurrentBattleDetails($baseArr);

        }
        return true;
    }

    public function setTempBattleEndEvent($value){
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BATTLE_TEMP];
        $this->setValue($key,$value);
    }
    public function getTempBattleEndEvent(){
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BATTLE_TEMP];
        $res = $this->getValue($key);
        return !empty($res)? json_decode($this->getValue($key),true):null;
    }

    /**
     * @param $event_type
     * @return string
     */
    public function getRealEventType($event_type)
    {
        $typeArr = [
            'MatchStarted' => 'battle_start',
            'RoundStart'   => 'round_start',
            'PlayerQuit'   => 'player_quit',
            'PlayerJoin'   => 'player_joined',
            'Kill'         => 'player_kill',
            'Assist'       => 'assist',
            'Restart'      => 'battle_restart',
            'Suicide'      => 'player_suicide',
            'RoundEnd'     => 'round_end',
            'BombPlanted'  => 'bomb_planted',
            'BombDefused'  => 'bomb_defused',
        ];
        return array_key_exists($event_type, $typeArr) ? $typeArr[$event_type] : 'default';
    }

    public function getUpComingNew(){
        $upComingKey                 = ['current', 'match', 'upComingNew'];
        $res = $this->getValue($upComingKey);
        if (!empty($res)){
            return json_decode($res,true);
        }else{
            return $res;
        }
    }

    public function getBattleStartNew(){
        $upComingKey                 = ['current', 'match', 'battleStartNew'];
        $res = $this->getValue($upComingKey);
        if (!empty($res)){
            return json_decode($res,true);
        }else{
            return $res;
        }
    }
    /**
     * battle_start
     * @return array
     */
    public function matchStartedEvent(){
        $match_data = $this->match_data;
        //判断新的upcoming
        $matchIndex                  = $this->getMatchIndex();
        $upComingArr                 = [];
        $upComingArr['matchStarted'] = $this->info['socket_time'];
        $upComingArr['index']        = $matchIndex;
        $upComingKey                 = ['current', 'match', 'upComingNew'];
        $battleStartKey              = ['current', 'match', 'battleStartNew'];
        $this->setValue($battleStartKey, json_encode($upComingArr,320));
        $upComingNew = $this->getUpComingNew();
        if (!empty($upComingNew) && $upComingNew['restart'] && empty($upComingNew['matchStarted'])) {
            $upComingArr['restart'] = $upComingNew['restart'];
            $this->setValue($upComingKey, json_encode($upComingArr,320));
        }else{
            $this->setValue($upComingKey, '');//重置upcoming
        }


        //根据地图名称查询缓存中的地图信息
//        print_r($match_data['log'][0]);exit;
        $map                                            = $this->findOneMapByName($match_data['log'][0]['MatchStarted']['map']);
        $this->event['map']['map_id']                   = @(int)$map['id'];
        $this->event['map']['name']                     = @$map['name'];
        $this->event['map']['name_cn']                  = @$map['name_cn'];
        $this->event['map']['external_id']              = @$map['external_id'];
        $this->event['map']['external_name']            = @$map['external_name'];
        $this->event['map']['short_name']               = @$map['short_name'];
        $this->event['map']['map_type']                 = @$map['e_name'];
        $this->event['map']['map_type_cn']              = @$map['c_name'];
        $this->event['map']['is_default']               = @$map['is_default'] == 1;
        $this->event['map']['slug']               = @$map['slug'];
        $this->event['map']['image']['square_image']    = @$map['square_image'];
        $this->event['map']['image']['rectangle_image'] = @$map['rectangle_image'];
        $this->event['map']['image']['thumbnail']       = @$map['thumbnail'];
        //team_ct
        $team_ct                                      = $this->getCurrentCt();
        if ($team_ct['team_id']){
            $this->event['starting_ct']['team_id']        = @$team_ct['team_id'] ? (int)$team_ct['team_id'] : null;//TODO
            $this->event['starting_ct']['name']           = @$team_ct['name'];
            $this->event['starting_ct']['image']          = @ImageConversionHelper::showFixedSizeConversion($team_ct['image'], 200, 200);
            $this->event['starting_ct']['opponent_order'] = @$team_ct['opponent_order'];
        }else{
            $this->event['starting_ct'] = null;
        }
        //team_t
        $team_t                                      = $this->getCurrentT();
        if ($team_t['team_id']){
            $this->event['starting_t']['team_id']        = @$team_t['team_id'] ? (int)$team_t['team_id'] : null;
            $this->event['starting_t']['name']           = @$team_t['name'];
            $this->event['starting_t']['image']          = @@ImageConversionHelper::showFixedSizeConversion($team_t['image'], 200, 200);
            $this->event['starting_t']['opponent_order'] = @$team_t['opponent_order'];
        }else{
            $this->event['starting_t'] = null;
        }

        $this->objectSet();
        //battle_timestamp
        $this->event['battle_timestamp'] = $this->battle_duration;
        $battleId = $this->getOutPutBattleId();
        $this->event['battle_id']          = $battleId ? (int)$battleId : null;
        $this->event['battle_order']       = $this->getOutPutBattleOrder();
        $event = $this->event;
        $this->event = [];
        return $this->setValue([self::KEY_TAG_CURRENT, 'match', 'content_battle_start'], json_encode($event,320));
//        return $this->event;
    }


    public function roundStartEvent(){


        $roundOrder = $this->getRoundOrdinal();
        $roundTo    = $roundOrder - 1;
        $roundTo    = $roundTo <= 0 ? 1 : $roundTo;
        $roundListKey= ['history',$this->getBattleId(),'roundStart','list'];
        //监测本battle是否后前面的回合数据都存在
        $roundList = $this->hGetAll($roundListKey);
        $battleId = $this->getBattleId();
        $reloadFlag = false;
        if (!empty($roundList)){
            $countRoundList = count($roundList);
            if (!empty($countRoundList)&&$roundOrder-$countRoundList>14){
                //开始大回挡
                if ($this->isLive()){
                    $reloadFlag = true;
                    $battleIdArr = explode('_',$battleId);
                    if (count($battleIdArr)==3){
                        if (is_numeric($battleIdArr[2])){
                            $oldBattleId = '';
                            for ($i = 1; $i < $battleIdArr[2]; $i++) {
                                $newBattleId  = $battleIdArr[0] . '_live_' . $i;
                                $roundListKey = ['history', $newBattleId, 'roundStart', 'list'];
                                //监测本battle是否后前面的回合数据都存在
                                $roundListOld = $this->hGetAll($roundListKey);
                                if (!empty($roundListOld)) {
                                    if ($roundOrder-count($roundListOld) <= 1) {
                                        $oldBattleId = $newBattleId;
                                        break;
                                    }
                                }
                            }
                            if ($oldBattleId) {
                                $count_use_round = count($this->getRoundsByBattleId($oldBattleId));
                                if ($count_use_round+1 >= $roundOrder) {
                                    $this->bigReload($roundTo, $oldBattleId, 'lessRoundStart');
                                }
                            }
                        }
                    }

                }
            }
        }
        if (!$reloadFlag){
            //判断在这个battle这个round是否曾经round_start过
            $roundOrderExist = $this->hGet($roundListKey,$roundOrder);
            if ($roundOrderExist){
                //说明已经round_start
                //开始大回挡
                if ($this->isLive()) {
                    $roundListOld = $this->hGetAll($roundListKey);
                    foreach ($roundListOld as $k => $v) {
                        if ($k > $roundOrder) {
                            $this->hDel($roundListKey, $k);
                        }
                    }
                    $count_use_round = count($this->getRoundsByBattleId($battleId));
                    if ($count_use_round == $roundOrder) {
                        $this->createHltvReload($roundTo);
                        //小回档
                        $this->littleReload('hltv',$this->info['socket_id'],'moreRoundStart');
                    }else{
                        $this->bigReload($roundTo, $battleId, 'moreRoundStart');
                    }
                }
            }
        }

        //统计round_start的列表
        $this->hSet($roundListKey,$roundOrder,$this->info['socket_time']);
        $this->event['is_bomb_planted']= false;
        $this->setRoundBeginAt($this->info['socket_time']);
        $this->setBombPlanted(false);
        $this->setTimeSincePlant('');
        //初始化
        $this->createSnapshot('round','hltv');
        //update round details
        $this->setRoundDetailsOnRoundStart();
        $this->createNewBattleOrUpcoming('roundStart');
        //update round survived_players
        $this->updateRoundSurvived('hltv');
        //update kills
        $this->updateKillsOnFrozen();
        //update rating
//        $this->updateRatingOnRoundEnd();
        //update kast
//        $this->updateKastOnRoundEnd();
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT],$this->info['socket_time']);
        $this->setRoundDetailsOnRoundStart();
        $this->needOutPutFrameData();

        $list = $this->getPlayerListByType('all');
        foreach ($list as $k => $v) {
            if ($v['side'] == 'ct') {
                $team = $this->getCurrentCt();
            }
            if ($v['side'] == 'terrorist') {
                $team = $this->getCurrentT();
            }
            $playerArr['team_log_name'] = @$team['log_name'];
            $playerArr['team_name']     = @$team['name'];
            $playerArr['team_id']       = @$team['team_id']?$team['team_id']:null;
            $playerArr['team_order']    = @$team['opponent_order'];
            $this->setPlayerBySteamId($v['steam_id'], $playerArr);
        }
        return $this->event;
    }
    /**
     * @param $log_time
     * @return int
     */
    public function getInRoundTimestamp($log_time)
    {
        if ($this->event['event_type'] == 'round_start' || $this->event['event_type'] == 'battle_start'){
            return 0;
        }
        $round_start_time = strtotime($this->getCurrentRoundBeginAt());
        $time             = strtotime($log_time) - $round_start_time;
        if (empty($round_start_time)) {
            $time = 0;
        }
        if ($time > 115) {
            return 115;
        }
        return $time > 0 ? (int)$time : 0;
    }

    public function playerQuitEvent(){
        $match_data = $this->match_data;
        $player   = $this->getHltvPlayerByName($match_data['log'][0]['PlayerQuit']['playerNick']);
        $this->event['player_id'] = !empty($player) ? @(int)$player['id'] : null;
        $this->event['nick_name'] = !empty($player) ? $player['nick_name'] : $match_data['log'][0]['PlayerQuit']['playerNick'];
        $steamId                  = $this->getSteamId2ByName($this->event['nick_name']);
        $returnSteamId            = SteamHelper::playerConversionSteamId($steamId);
        $this->event['steam_id']  = !empty($steamId) ? $returnSteamId : null;
        $this->event['side']      = strtolower($match_data['log'][0]['PlayerQuit']['playerSide']);
        return $this->event;
    }

    public function getHltvPlayerByName($name){
        $redisRes = $this->hGet(['table','player_name'] , $name);
        if (!empty($redisRes)){
            $res = json_decode($redisRes,true);
            return $res;
        }else{
            $player = \app\modules\org\models\Player::find()->where(['nick_name' => $name])->asArray()->one();
            if (!empty($player)) {
                $this->hSet(['table','player_name'] , $name, json_encode($player,320));
                return $player;
            }else{
                return [];
            }

        }
    }

    public function getSteamId2ByName($name)
    {
        $list = $this->getCurrentPlayerList();
        foreach ($list as $k => $v) {
            if ($v['nick_name'] == $name) {
                return $v['steam_id'];
            }
        }
        return null;
    }

    public function getCurrentPlayerList(){
        $key  = [self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_LIST];
        $list =  $this->hGetAll($key);
        $newList = [];
        foreach($list as $k=>$v){
            $newList[] = json_decode($v, true);
        }
        return $newList;
    }

    public function playerJoinEvent()
    {
        $match_data               = $this->match_data;
        $player                   = $this->getHltvPlayerByName($match_data['log'][0]['PlayerJoin']['playerNick']);
        $this->event['player_id'] = !empty($player) ? @(int)$player['id'] : null;
        $this->event['nick_name'] = !empty($player) ? $player['nick_name'] : $match_data['log'][0]['PlayerJoin']['playerNick'];
        $steamId                  = $this->getSteamId2ByName($this->event['nick_name']);
        $returnSteamId            = SteamHelper::playerConversionSteamId($steamId);
        $this->event['steam_id']  = !empty($steamId) ? $returnSteamId : null;
        return $this->event;
    }

    /**
     * killEvent
     * @return array
     */
    public function killEvent(){

        $match_data  = $this->match_data;
        $killerNick  = $match_data['log'][0]['Kill']['killerNick'];
        $killerSide  = strtolower($match_data['log'][0]['Kill']['killerSide']);
        $victimNick  = $match_data['log'][0]['Kill']['victimNick'];
        $victimSide  = strtolower($match_data['log'][0]['Kill']['victimSide']);
        $weapon      = $match_data['log'][0]['Kill']['weapon'];
        $headShot    = $match_data['log'][0]['Kill']['headShot'];
        $eventId     = $match_data['log'][0]['Kill']['eventId'];
        $flasherNick = @$match_data['log'][0]['Kill']['flasherNick'];
        $flasherSide = @strtolower($match_data['log'][0]['Kill']['flasherSide']);
        $victimX     = @$match_data['log'][0]['Kill']['victimX'];
        $victimY     = @$match_data['log'][0]['Kill']['victimY'];
        $killerX     = @$match_data['log'][0]['Kill']['killerX'];
        $killerY     = @$match_data['log'][0]['Kill']['killerY'];
        if ($killerX && $killerY) {
            $killerPosition = $killerX . ' ' . $killerY.' 0';
        } else {
            $killerPosition = null;
        }
        if ($victimX && $victimY) {
            $victimPosition = $victimX . ' ' . $victimY.' 0';
        } else {
            $victimPosition = null;
        }
        $killerSteamId = $this->getSteamId2ByName($killerNick);
        //round details
        $round               = $this->getRoundDetails();
        //is diff side
        $diff_side = $victimSide != $killerSide;
        //victim info
        $deaderInfo = $this->getHltvPlayerByName($victimNick);
        //kill info
        $killerInfo = $this->getHltvPlayerByName($killerNick);
        //killer
        if ($diff_side) {
            //statistics fields
            $updateArrStatistics = [];
            $killer_side         = $killerSide;
            if ($headShot) {
                $updateArrStatistics[] = 'headshot_kills';
                $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
                //update round details
                $baseArr                                   = [];
                $baseArr[$killer_side . '_headshot_kills'] = $round[$killer_side . '_headshot_kills'] + 1;
                $this->setCurrentRoundDetails($baseArr);
            }
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'kills';
            $this->setRoundPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
            if (!$round['kills']) {
                $this->record_first_kill = true;
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'first_kills';
                $this->setRoundPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
                //update round details first_kills_side,first_death_side
                $baseRoundArr                     = [];
                $baseRoundArr['first_kills_side'] = $killer_side;
                $baseRoundArr['first_death_side'] = $victimSide;
                if ($headShot) {
                    $baseRoundArr['is_first_kills_headshot_side'] = $killer_side;
                }
                $this->setCurrentRoundDetails($baseRoundArr);
            }else{
                $this->record_first_kill = false;
            }
            //knife kill
            $withWeapon = $weapon;
            if (strstr($withWeapon, 'knife')) {
                $updateArrStatistics[] = 'knife_kill';
                $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
                //update round details
                $baseArr[$killer_side . '_is_knife_kill'] = true;
                $this->setCurrentRoundDetails($baseArr);
            }
            //position
            $this->setPlayerBySteamId($killerSteamId, ['position'=>@$killerPosition]);
            //multi_kills (every round do once)
            $roundPlayer = $this->getRoundPlayerBySteamId($killerSteamId);
            if (!empty($roundPlayer)) {
                //ace_kills (every round do once)
                if ($roundPlayer['kills'] == 5) {
                    $this->record_ace_kill = true;
                    $baseArr                  = [];
                    $baseArr['ace_kill_side'] = $killer_side;
                    $this->setCurrentRoundDetails($baseArr);
                }
            }
        }else{
            $this->record_team_kill = true;
            //statistics fields
            $updateArrStatistics = ['kills'];
            $this->setRoundPlayerBySteamId($killerSteamId, $updateArrStatistics, 'decr');
            //team_kills
            $updateArrStatistics2 = [];
            $updateArrStatistics2[] = 'team_kills';
            $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics2, 'incr');
        }

        $this->event['killer']['player_id'] = @$killerInfo['id'] ?(int)$killerInfo['id']: null;
        $this->event['killer']['nick_name'] = @$killerInfo['nick_name'] ?? $killerNick;
        $returnSteamId                      = SteamHelper::playerConversionSteamId($killerSteamId);
        $this->event['killer']['steam_id']  = !empty($returnSteamId) ? $returnSteamId : null;
        $this->event['killer']['side']      = @$killerSide ?? null;
        $this->event['killer']['position']  = $killerPosition;
        /**
         * victim
         */
        $this->event['victim']['player_id'] = @$deaderInfo['id'] ?(int)$deaderInfo['id']: null;
        $this->event['victim']['nick_name'] = @$deaderInfo['nick_name'] ?? $victimNick;
        $steamId                            = $this->getSteamId2ByName($victimNick);
        $returnSteamId                      = SteamHelper::playerConversionSteamId($steamId);
        //statistics fields
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'deaths';
        if (!$round['deaths']) {
            $updateArrStatistics[] = 'first_deaths';
        }
        $this->setRoundPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        //update round deaths
        $baseArr           = [];
        $baseArr['deaths'] = $round['deaths'] + 1;
        $this->setCurrentRoundDetails($baseArr);
        //status fields
        $updateArrStatus             = [];
        $updateArrStatus['is_alive'] = false;
        $updateArrStatus['position'] = $victimPosition;
        $this->setPlayerBySteamId($steamId, $updateArrStatus);
        //clear All
        $this->clearAllIfPlayerDeadBySteamId($steamId);
        if ($diff_side){
            $updatePlayerLogArr = [];
            $updatePlayerLogArr['killer_steam_id'] = $killerSteamId;
            $updatePlayerLogArr['victim_steam_id'] = $steamId;
            $updatePlayerLogArr['log_time'] = $this->info['socket_time'];
            $killEventsKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_KILL_EVENTS];
            $this->lPush($killEventsKey, json_encode($updatePlayerLogArr,320));
            $listKillEvents = $this->lRange($killEventsKey,0,-1);
            if (!empty($listKillEvents)){
                foreach ($listKillEvents as $k=>$v){
                    $arrV = json_decode($v,true);
                    if ((strtotime($this->info['socket_time']) - strtotime($arrV['log_time']) < 4) && $arrV['killer_steam_id'] == $steamId) {
                        $update = [];
                        $update['trade'] = 1;
                        $this->setRoundPlayerBySteamId($arrV['victim_steam_id'],$update);
                    }
                }
            }
        }


        $this->event['victim']['steam_id']  = !empty($steamId) ? $returnSteamId : null;
        $this->event['victim']['side']      = @$victimSide ?? null;
        $this->event['victim']['position']  = $victimPosition;
        /**
         * assist
         */
        $dbAssist = $this->assistDbList;
        if ($this->refresh_task) {
            if (array_key_exists($eventId, $dbAssist)) {
                $assistMatchData = $dbAssist[$eventId];
                $assistNick      = $assistMatchData['assisterNick'];
                $assistSide      = strtolower($assistMatchData['assisterSide']);
                $steamId         = $this->getSteamId2ByName($assistNick);
                $returnSteamId   = SteamHelper::playerConversionSteamId($steamId);
            } else {
//                $this->error_catch_log(['assistTest'=>true]);
                $assistNick      = '';
                $assistSide      = '';
                $assistMatchData = [];
            }
        } else {
//            $this->error_catch_log(['assistTest'=>true]);
            if (array_key_exists($eventId, $dbAssist)) {
                $assistMatchData = $dbAssist[$eventId];
                $assistNick      = $assistMatchData['assisterNick'];
                $assistSide      = strtolower($assistMatchData['assisterSide']);
                $steamId                  = $this->getSteamId2ByName($assistNick);
                $returnSteamId            = SteamHelper::playerConversionSteamId($steamId);
            } else {
                $assistEvent = MatchLived::find()->andwhere(['match_id' => (string)$this->relMatchId])
                    ->andWhere(['like', 'match_data', '"killEventId":' . $eventId])
                    ->asArray()->one();
                if (!empty($assistEvent)) {
                    $assistMatchData = json_decode($assistEvent['match_data'], true);
                    $assistNick      = $assistMatchData['log'][0]['Assist']['assisterNick'];
                    $assistSide      = strtolower($assistMatchData['log'][0]['Assist']['assisterSide']);
                    $steamId                  = $this->getSteamId2ByName($assistNick);
                    $returnSteamId            = SteamHelper::playerConversionSteamId($steamId);
                } else {
                    $assistNick      = '';
                    $assistSide      = '';
                    $assistMatchData = [];
                }
            }
        }
        if (!empty($assistMatchData)) {
            $assistInfo = $this->getHltvPlayerByName($assistNick);
            //statistics fields
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'assists';
            $this->setRoundPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
            if (!empty($assistInfo)) {
                $this->event['assist']['player_id'] = $assistInfo['id']?(int)$assistInfo['id']:null;
                $this->event['assist']['nick_name'] = $assistInfo['nick_name'] ?? $assistNick;
                $this->event['assist']['steam_id'] = $returnSteamId;
                $this->event['assist']['side'] = $assistSide;
            } else {
                $this->event['assist'] = null;
            }
        } else {
            $this->event['assist'] = null;
        }
        //flashassist
        if ($flasherNick) {
            $flasherInfo = $this->getHltvPlayerByName($flasherNick);
            if (!empty($flasherInfo)) {

                $this->event['flashassist']['player_id'] = $flasherInfo['id']?(int)$flasherInfo['id']:null;
                $this->event['flashassist']['nick_name'] = $flasherInfo['nick_name'] ? $flasherInfo['nick_name'] : $flasherNick;
                $steamId                                 = $this->getSteamId2ByName($this->event['flashassist']['nick_name']);
                //statistics fields
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'flash_assist';
                $this->setRoundPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
                $returnSteamId                           = SteamHelper::playerConversionSteamId($steamId);
                $this->event['flashassist']['steam_id']  = $steamId ? $returnSteamId : null;
                $this->event['flashassist']['side']      = $flasherSide;
            } else {
                $this->event['flashassist'] = null;
            }
        } else {
            $this->event['flashassist'] = null;
        }
        /**
         * round
         */
        $killer_side                          = $killerSide;
        $updateRound[$killer_side . '_kills'] = $round[$killer_side . '_kills'] + 1;
        $updateRound['kills']                 = $round['kills'] + 1;
        $this->setCurrentRoundDetails($updateRound);
        //update round survived_players
        $survivedArr = $this->updateRoundSurvived('hltv');
        if ($survivedArr['survived_players_ct'] == 1 || $survivedArr['survived_players_t'] == 1) {
            $this->updateSurvivedPlayersSteamIdCountByType('ct',$survivedArr);
            $this->updateSurvivedPlayersSteamIdCountByType('t',$survivedArr);
        }

        $weaponInfo = $this->getWeaponByName($weapon);
        //weapon
        $this->event['weapon']['weapon_id']     = @$weaponInfo['id']?(int)$weaponInfo['id']:null;
        $this->event['weapon']['name']          = @$weaponInfo['name'];
        $this->event['weapon']['external_id']   = !empty($weaponInfo['external_id']) ? $weaponInfo['external_id'] : null;
        $this->event['weapon']['external_name'] = @$weaponInfo['external_name'];
        $this->event['weapon']['kind']          = @$weaponInfo['kind_name'];
        $this->event['weapon']['slug']          = @$weaponInfo['slug'];
        $this->event['weapon']['image']         = ImageConversionHelper::showMfitSizeConversion(@$weaponInfo['image'],28,0);
        if ($weapon == 'awp') {
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'awp_kills';
            $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
        } elseif (strstr($weapon, 'knife')) {
            $this->record_knife_kill = true;
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'knife_kills';
            $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
        } elseif ($weapon == 'taser') {
            $this->record_taser_kill = true;
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'taser_kills';
            $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
        } elseif ($weapon == 'mag7' || $weapon == 'sawedoff' || $weapon == 'xm1014') {
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'shotgun_kills';
            $this->setPlayerBySteamId($killerSteamId, $updateArrStatistics, 'incr');
        }
        //other
        $this->event['damage']              = null;
        $this->event['hit_group']           = null;
        $this->event['is_headshot']         = $headShot;
        $this->event['special_description'] = null;
        $this->event['orgin_eventId'] = $eventId;
        $this->event['socket_time'] = $this->info['socket_time'];
        $this->killEvent = $this->event;
        $this->setTempPlayerKillEvent(json_encode($this->event,320));
        $this->event = [];
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
        $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
        return [];
//        return $this->event;
    }

    public function setTempPlayerKillEvent($value){
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_PLAYER_KILL_TEMP];
        $this->setValue($key,$value);
    }

    public function getTempPlayerKillEvent(){
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_PLAYER_KILL_TEMP];
        $res = $this->getValue($key);
        return !empty($res)? json_decode($this->getValue($key),true):null;
    }

    public function getNeedOutPutEventsCommon($event_type){
        $this->event['match_id']           = (int)$this->matchId;
        $this->event['battle_id']          = (int)$this->getOutPutBattleId();
        $this->event['battle_order']       = (int)$this->getOutPutBattleOrder();
        $this->event['battle_timestamp']   = $this->battle_duration;
        $this->event['event_id']           = (int)$this->info['socket_id'];
        $this->event['event_type']         = $event_type;
        $this->event['round_ordinal']      = $this->getRoundOrdinal();
        $this->event['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);//回合内时间戳(event的log_time)
        $this->event['round_time']         = $this->getRoundTime($this->info['socket_time']); //回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
        $this->event['is_bomb_planted']    = $this->getIsBombPlanted();
        $this->event['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
    }


    public function assistEvent()
    {
        $match_data    = $this->match_data;
        $tempKillEvent = $this->getTempPlayerKillEvent();
        $assistEventId = $match_data['log'][0]['Assist']['killEventId'];
        $assistNick    = $match_data['log'][0]['Assist']['assisterNick'];
        $assisterSide  = strtolower($match_data['log'][0]['Assist']['assisterSide']);
        if (!empty($tempKillEvent) && $tempKillEvent['orgin_eventId'] == $assistEventId) {
            $assistInfo                           = $this->getHltvPlayerByName($assistNick);
            $steamId                              = $this->getSteamId2ByName($assistNick);
            $returnSteamId                        = SteamHelper::playerConversionSteamId($steamId);
            $tempKillEvent['assist']['player_id'] = $assistInfo['id'] ? (int)$assistInfo['id'] : null;
            $tempKillEvent['assist']['nick_name'] = $assistInfo['nick_name'] ?? $assistNick;
            $tempKillEvent['assist']['steam_id']  = $returnSteamId;
            $tempKillEvent['assist']['side']      = $assisterSide;
            $this->setTempPlayerKillEvent('');
            unset($tempKillEvent['socket_time']);
            unset($tempKillEvent['orgin_eventId']);
            $this->event = $tempKillEvent;
        }
        return $this->event;
    }



    /**
     * UpComing
     * @return array
     */
    public function restartEvent(){
        //判断新的upcoming
        $upComingArr = [];
        $upComingArr['restart'] = $this->info['socket_time'];
        $upComingKey = ['current','match','upComingNew'];
        $this->setValue($upComingKey,json_encode($upComingArr,320));
        //设置比赛状态
        $this->event['battle_restart_in'] = null;
//        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], true);


        $event['match_id']           = (int)$this->matchId;
        $event['battle_id']          = $this->getOutPutBattleId();
        $event['battle_order']       = $this->getOutPutBattleOrder();
        $event['battle_timestamp']   = $this->battle_duration;
        $event['event_id']           = (int)$this->info['socket_id'];
        $event['event_type']         = 'battle_upcoming';
        $event['round_ordinal']      = $this->getRoundOrdinal();
        $event['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);//回合内时间戳(event的log_time)
        $event['round_time']         = $this->getRoundTime($this->info['socket_time']); //回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
        $event['is_bomb_planted']    = $this->getIsBombPlanted();
        $event['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
        $restartEvent = $this->event;
        $this->event=[];
        //判断新的upcoming
        $this->setValue([self::KEY_TAG_CURRENT, 'match', 'content_battle_up_coming'], json_encode($event,320));
        $this->setValue([self::KEY_TAG_CURRENT, 'match', 'content_restart'], json_encode($restartEvent,320));
//        $this->needOutPutUpComingEvent = $event;
        return [];
//        return $this->event;
    }

    public function suicideEvent()
    {
        //update round survived_players
        $this->updateRoundSurvived('hltv');
        $match_data = $this->match_data;
        $playerNick = $match_data['log'][0]['Suicide']['playerNick'];
        $side       = strtolower($match_data['log'][0]['Suicide']['side']);
        $weapon     = $match_data['log'][0]['Suicide']['weapon'];
        $player     = $this->getHltvPlayerByName($playerNick);
        //player
        $this->event['player_id'] = !empty($player) ? @(int)$player['id'] : null;
        $this->event['nick_name'] = !empty($player) ? @$player['nick_name'] : $playerNick;
        $steamId = $this->getSteamId2ByName($playerNick);
        $returnSteamId = SteamHelper::playerConversionSteamId($steamId);
        $this->event['steam_id'] = !empty($steamId) ? @$returnSteamId : null;
        $this->event['side']      = $side;
        $this->event['position']  = null;
        //weapon
        $weaponData                             = $this->getWeaponByName($weapon);
        $this->event['weapon']['weapon_id']     = @$weaponData['id']?(int)$weaponData['id']:null;
        $this->event['weapon']['name']          = @$weaponData['name'];
        $this->event['weapon']['external_id']   = @$weaponData['external_id']?$weaponData['external_id']:null;
        $this->event['weapon']['external_name'] = @$weaponData['external_name'];

        $this->event['weapon']['kind']          = @$weaponData['kind_name'];
        $this->event['weapon']['slug']          = @$weaponData['slug'];
        $this->event['weapon']['image']         = ImageConversionHelper::showMfitSizeConversion(@$weaponData['image'],28,0);
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_SUICIDE];
        $value['event_id'] = (int)$this->info['socket_id'];
        $value['socket_time'] = $this->info['socket_time'];
        //status fields
        $updateArrStatus             = [];
        $updateArrStatus['is_alive'] = false;
        $this->setPlayerBySteamId($steamId, $updateArrStatus);
        //statistics fields
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'deaths';
        $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        //statistics fields
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'kills';
        $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'decr');
        $jsonValue = json_encode($value,320);
        $this->lPush($key,$jsonValue);
        return $this->event;
    }

    public function setRoundEndAtFlag($roundEndTime){
        $key= [self::KEY_TAG_CURRENT,self::KEY_TAG_ROUND,self::KEY_TAG_END_AT];
        return  $this->setValue($key,$roundEndTime);
    }
    public function getRoundEndAtFlag(){
        $key= [self::KEY_TAG_CURRENT,self::KEY_TAG_ROUND,self::KEY_TAG_END_AT];
        return  $this->getValue($key);
    }

    public function batchUpdateBattleRedisKey($battleOrder,$newOrder)
    {
        $recordKey = [self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_CHANGE_BATTLE];
        $recordArr['old_battle_id'] = $battleOrder;
        $recordArr['new_battle_id'] = $newOrder;
        $recordArr['event_id'] = $this->info['socket_id'];
        $recordJson = json_encode($recordArr,320);
        $this->lPush($recordKey,$recordJson);
        $matchId = $this->matchId;
        $redis_key   = 'hltv_ws:'.$matchId.':history:battle:' . $battleOrder . ':';

        if (!$redis_key || $redis_key == '*' || (!strstr($redis_key, 'ws') && !strstr($redis_key, 'hltv_ws'))) {
            return false;
        }
        $redis = $this->redis;
        $pre   = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 100)) {
            if (is_array($arr_keys) && !empty($arr_keys)) {
                foreach ($arr_keys as $k => $v) {
                    $new_key = str_replace($battleOrder, $newOrder, $v);
                    $redis->rename($v, $new_key);
                }
            }
        }
        $battle_list_key         = 'hltv_ws:'.$matchId.':history:battle:list';
        $battle_details          = $redis->hGet($battle_list_key, $battleOrder);
        $battle_arr              = json_decode($battle_details, true);
        $battle_arr['battle_id'] = $newOrder;
        $battle_details          = json_encode($battle_arr, 320);
        $redis->hSet($battle_list_key, $newOrder, $battle_details);
        $redis->hDel($battle_list_key, $battleOrder);
        $round_list_key         = 'hltv_ws:'.$matchId.':history:battle:'.$newOrder.':round:list';
        $battle_details          = $redis->hGetAll($round_list_key);
        if (is_array($battle_details)&&!empty($battle_details)){
            foreach ($battle_details as $k=>$v){
                $vArr = json_decode($v,true);
                $vArr['battle_id'] = $newOrder;
                $vArr['real_battle_id'] = $this->getRealBattleId();
                $v = json_encode($vArr,320);
                $redis->hSet($round_list_key,$k,$v);
            }
        }
        //hltv_ws:47421:history:battle:2_live_1:round:1:main_events
        $roundOrder = $this->getRoundOrdinal();
        $realOrder = $this->getRealOrder();
        $realId =  $this->getRealBattleId();
        for ($i=1;$i<=$roundOrder;$i++){
            $main_events_key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$newOrder,self::KEY_TAG_ROUND,$i,self::KEY_TAG_MAIN_EVENTS];
            $main_events_list = $this->lRange($main_events_key,0,-1);
            $str_main_events_key = $this->keyString($main_events_key);
            if (!empty($main_events_list)){
                foreach ($main_events_list as $k=>$v){
                    $vArr = json_decode($v,true);
                    $vArr['battle_id'] =  $realId;
                    $vArr['battle_order'] = $realOrder;
                    $v = json_encode($vArr,320);
                    $redis->lSet($str_main_events_key,$k,$v);
                }
            }
        }

        $this->refresh_task_str = [];//重置
        return true;
    }


    public function getHltvReloadToBattleByRoundTotal($roundTotal)
    {
        $battles = $this->getBattles();
        $battleOrder = $this->getRealOrder();
        foreach ($battles as $k => $v) {
            $arr_v = json_decode($v,true);
            $vBattleRoundCount = $arr_v['start_ct']['score']+$arr_v['start_t']['score'];
            if ($arr_v['order']==$battleOrder&&$roundTotal==$vBattleRoundCount) {
                return $arr_v;
            }
        }
        return false;
    }


    /**
     * @param $counterTerroristScore
     * @param $terroristScore
     * @return bool
     */
    public function createHltvReload($round_to)
    {
        $data = $this->event;
        $data['event_type'] = 'battle_reloaded';
        $map = $this->getCurrentMap();
        $data['map']['map_id']                   = (int)$map['id'];
        $data['map']['name']                     = @$map['name'];
        $data['map']['name_cn']                  = @$map['name_cn'];
        $data['map']['external_id']              = @$map['external_id'];
        $data['map']['external_name']            = @$map['external_name'];
        $data['map']['short_name']               = @$map['short_name'];
        $data['map']['map_type']                 = @$map['e_name'];
        $data['map']['map_type_cn']              = @$map['c_name'];
        $data['map']['is_default']               = @$map['is_default'] == 1;
        $data['map']['slug']               = @$map['slug'];
        $data['map']['image']['square_image']    = @$map['square_image'];
        $data['map']['image']['rectangle_image'] = @$map['rectangle_image'];
        $data['map']['image']['thumbnail']       = @$map['thumbnail'];

        $team_ct = $this->getCurrentCt();
        $team_t = $this->getCurrentT();
        $data['current_ct']['team_id']        = $team_ct['team_id']?(int)$team_ct['team_id']:null;
        $data['current_ct']['name']           = $team_ct['name'];
        $data['current_ct']['image']          = ImageConversionHelper::showFixedSizeConversion(@$team_ct['image'],200,200);
        $data['current_ct']['opponent_order'] = $team_ct['opponent_order'];
        //开局作为恐怖分子
        $data['current_t']['team_id']        = (int)$team_t['team_id'];
        $data['current_t']['name']           = $team_t['name'];
        $data['current_t']['image']          = ImageConversionHelper::showFixedSizeConversion(@$team_t['image'],200,200);
        $data['current_t']['opponent_order'] = $team_t['opponent_order'];
        $data['restore_to'] = intval($round_to)?intval($round_to):$this->currentRound();
        $this->needOutPutReloadEvent = $data;
    }


    public function bigReload($round_to,$customBattleIdTo='',$reason=''){

        $this->error_catch_log([$this->matchId.'_big_reload'=>'大回挡开始']);
        //大回挡
        $this->bigReload=true;
        //big Reload
        $battle = $this->getHltvReloadToBattleByRoundTotal($round_to);

        if ($customBattleIdTo){
            $battle = $this->getBattleDetailsById($customBattleIdTo);
            $battleId     = $customBattleIdTo;
        }else{
            $battleId     = $battle['battle_id'];
        }
        if (empty($battle)){
            return false;
        }

        $battleTo     = $battle['real_order'];
        $realBattleId = $battle['real_battle_id'];
        if ($battleTo && $battleId &&$battle['order']) {

            $this->setCurrentBattleDetails(['is_use' => false]);
            if ($reason == 'lessRoundStart' || $reason == 'moreRoundStart') {
                $this->event['round_ordinal']=$round_to+1;
            }else{
                $this->event['round_ordinal']=$round_to;
            }
            $this->setBattleBeginAt($battle['begin_at']);
            $this->event['battle_timestamp'] = $this->getDuration($this->info['socket_time']);
            $roundTo = intval($round_to);
            $this->createHltvReload($round_to);
            if ($this->refresh_task){
                $temp_refresh_task = true;
            }else{
                $temp_refresh_task = false;
            }
            $this->refresh_task_str = [];
            $this->refresh_task = false;
            $this->error_catch_log([$this->matchId.'_big_reload'=>'存大数组开始']);
            $battle['event_id'] = $this->info['socket_id'];
            $battle['roundTo'] = $round_to;
            $battle['reason'] = $reason;
            //save array to redis
            $this->saveBigArray($battle);
            $this->error_catch_log([$this->matchId.'_big_reload'=>'重新set redis 开始']);
            //set $battleId
            //reset battle and round
            $this->reSetOnReload($battleTo, $roundTo, $battleId,$reason);
            $this->error_catch_log([$this->matchId.'_big_reload'=>'重新battle 统计 开始']);
            //重新统计
            $this->reSetBattleDataOnReload('hltv',$round_to);
            //hltv_ws:47640:history:battle:1_live_1:round:3:main_events
            $eventKey = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$this->getBattleId(),self::KEY_TAG_ROUND,$this->getRoundOrdinal(),self::KEY_TAG_MAIN_EVENTS];
            $eventKeyStr = $this->keyString($eventKey);
            $this->redis->del($eventKeyStr);
            $this->refresh_task = true;
            if ($temp_refresh_task){
                $this->refresh_task = true;
            }
            $this->error_catch_log([$this->matchId.'_big_reload'=>'team数据开始']);
            $battleDetails               = $this->getCurrentBattleDetails();
            $ctDetails['team_id']        = $battleDetails['start_ct']['team_id'];
            $ctDetails['name']           = $battleDetails['start_ct']['name'];
            $ctDetails['image']          = $battleDetails['start_ct']['image'];
            $ctDetails['opponent_order'] = $battleDetails['start_ct']['opponent_order'];

            $tDetails['team_id']        = $battleDetails['start_t']['team_id'];
            $tDetails['name']           = $battleDetails['start_t']['name'];
            $tDetails['image']          = $battleDetails['start_t']['image'];
            $tDetails['opponent_order'] = $battleDetails['start_t']['opponent_order'];

            $this->setStartingCt($ctDetails);
            $this->setStartingT($tDetails);

            //clear api Battle
            $this->clearBattleByBattleId($realBattleId);
            $this->error_catch_log([$this->matchId.'_big_reload'=>'大回挡结束']);
        } else {
            return false;
        }
    }



    /**
     * set battle end_at
     * 两队比分之和<=30，RoundEnd事件，有一边的比分=16
     * 两队比分之和>30，RoundEnd事件，有一边的比分=16+3*X，另外一边的比分<=14+3*X
     * @return array
     */
    public function roundEndEvent(){


        $match_data             = $this->match_data;
        $win_type               = $match_data['log'][0]['RoundEnd']['winType'];
        $winner                 = strtolower($match_data['log'][0]['RoundEnd']['winner']);
        $terroristScore         = $match_data['log'][0]['RoundEnd']['terroristScore'];
        $counterTerroristScore  = $match_data['log'][0]['RoundEnd']['counterTerroristScore'];

        if (strtolower($win_type) == 'round_draw') {
            $key     = [self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH,self::KEY_TAG_SUICIDE];
            $keyString = $this->keyString($key);
            $last    = $this->redis->lIndex($keyString, 0);
            $lastTen = $this->redis->lIndex($keyString, 6);

            if (!empty($last) && !empty($lastTen)) {
                $lastArr    = json_decode($last, true);
                $lastTenArr = json_decode($lastTen, true);
                if (is_array($lastArr) && !empty($lastArr) && $lastArr['socket_time'] && is_array($lastTenArr) && !empty($lastTenArr) && $lastTenArr['socket_time']) {
                    $diffTime = strtotime($lastArr['socket_time']) - strtotime($lastTenArr['socket_time']);
                    if ($diffTime>=0&&$diffTime <= 2) {
                        $round_to = $terroristScore + $counterTerroristScore;
                        //用真实回合序号比
                        $real_round = $this->currentRound();
                        if ($real_round  == $round_to) {
                            $this->createHltvReload($round_to);
                            //小回档
                            $this->littleReload('hltv',$this->info['socket_id'],'roundEndIsDraw');

                        } else {
                            $this->bigReload($round_to,'','round_end');
                        }
                    }
                }
            }
        }


        $battleId = $this->getBattleId();
        $liveTrue = $this->getValue([self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_HLTV_LIVE]);
        if (strstr($battleId,'warmUp')&&$liveTrue){
            $battleEndTime = $this->getValue(['current','battle','end_at']);
            if (strtotime($this->info['socket_time']) - strtotime($battleEndTime) < 5) {

            }else{
                //说明没有restart事件,这里需要容错
                $this->incrRealOrder();
                $real_order = $this->getRealOrder();
                $this->setNumber(1);
                $currentOrder=  $real_order.'_live_1';
                if ($currentOrder=='1_live_1'){
                    $begin_at = $this->getBattleBeginAt();
                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT], '');//match 结束时间
                    $this->setnx([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BEGIN_AT], $begin_at);//match 开始时间
                }
                $this->setCurrentOrder($currentOrder);
                //set is_live true
                $this->setIsLive(true);
                //set match status
                $this->setMatchStatus(self::STATUS_GOING);//match 状态
//            $currentOrder=  $this->getBattleId();
                //battle_order
                $this->setBattleIdByRealOrder();
                $this->batchUpdateBattleRedisKey($battleId,$currentOrder);
                //battle details
                $this->updateBattleDetailsOnBattleStart();
                //clear events
//            $this->clearNoUseEvents();
                //up_coming
                $this->setBattleUpComing(true);
                $recordKey = [self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_CUSTOM_BATTLE_START];
                $recordArr['battle_order'] = $this->getBattleId();
                $recordArr['event_id'] = $this->info['socket_id'];
                $recordJson = json_encode($recordArr,320);
                $this->lPush($recordKey,$recordJson);
            }



        }
        //为battle_end判断加的时间
        $this->setRoundEndAtFlag($this->info['socket_time']);
        //update 1vn
        $this->update1vnOnRoundEnd($winner,'hltv');
        if (strtolower($win_type) != 'round_draw'){
            //update kills
            $this->updateKillsOnFrozen();
            //update rating
            $this->updateRatingOnRoundEnd();
            //update kast
//            $this->updateKastOnRoundEnd();
            //statis
            $this->setRoundPlayerStatisOnRoundEnd('ct');
            $this->setRoundPlayerStatisOnRoundEnd('t');
            //statis
            $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
            $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
        }

        $this->setRoundEndAt($this->info['socket_time']);

        //update round survived_players
        $this->updateRoundSurvived('hltv');
        //status end
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_STATUS], self::STATUS_END);//
        //record battle details
        $this->recordBattleDetailsOnRoundEnd();
        $halfScoreKey =['current','battle','halfScore'];

        $roundOrder = $this->getRoundOrdinal();

        $diff = $roundOrder-$counterTerroristScore-$terroristScore;
        $arrNoDo=[
            14,15,29,30,32,33,35,36,38,39,41,42,44,45,47,48,50,51,53,54,56,57,59,60,62,63,65,66,68,69,71,72,74,75,77,78,80,81,83,84,86,87,89,90,92,93,95,96,98,99
        ];

        if (in_array($diff,$arrNoDo)){

            $halfScoreArrJson = $this->getValue($halfScoreKey);
            $halfScoreArrValue = json_decode($halfScoreArrJson,true);
            $counterTerroristScore= $halfScoreArrValue['t']+$counterTerroristScore;
            $terroristScore= $halfScoreArrValue['ct']+$terroristScore;

        }
        $this->setStartingCtScore($counterTerroristScore);
        $this->setStartingTScore($terroristScore);


        if ($this->currentRound()>=31){
            $v['type'] = $win_type;
            $type = strtolower($win_type);
            $side = $winner;
            if ($side=='ct'){
                $winner_order = $this->getCurrentCt()['opponent_order'];
            }else{
                $winner_order = $this->getCurrentT()['opponent_order'];
            }
            $updateArr                  = [];
            $v['roundOrdinal'] = $this->getRoundOrdinal();
            $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
            $roundDetails=json_decode($this->hGet($roundKey,$v['roundOrdinal']), true);
            if (empty($roundDetails['round_win_type'] )&& $type != 'round_draw'){
                if ($type) {
                    $updateArr['round_win_type'] = $type;
                }
                if ($side) {
                    $updateArr['winner_side'] = $side;
                }
                if ($winner_order) {
                    $updateArr['winner_order'] = $winner_order;
                }
                $this->setRoundDetails($updateArr, $v['roundOrdinal']);
            }

        }
        $tempBattleEnd = $this->getTempBattleEndEvent();


        if (!empty($tempBattleEnd)){
            $match_team_1_score = $this->getMatch1Score();
            $match_team_2_score = $this->getMatch2Score();
            $battleCount        = $this->match['number_of_games'];
            //set match end&&winner
            $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
            //计算比赛结束
            if ($winnerInfo['is_finish'] == 1) {
                $this->setMatchEndAt($this->info['socket_time']);
            }
            $this->setTempBattleEndEvent('');
            $tempBattleEnd['battle_timestamp'] = $this->battle_duration;
            $this->needOutPutBattleEndEvent =  $tempBattleEnd;
        }else{
            //新battleEnd
            $this->createBattleEndByScore($terroristScore,$counterTerroristScore,'round_end');
        }





        $this->event['round_end_type'] = strtolower($win_type);
        $this->event['winner_side']    = @$winner;
        $this->event['ct_score']       = @$counterTerroristScore;
        $this->event['t_score']        = @$terroristScore;
        $this->needOutPutFrameData();
        $arrNoDo=[
            15,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,75,78,81,84,87,90,93,96,99
        ];
        if (in_array($roundOrder,$arrNoDo)){
            //单独存储半场比分
            $halfScoreKey =['current','battle','halfScore'];
            $halfScoreArr['ct'] = $counterTerroristScore;
            $halfScoreArr['t'] = $terroristScore;
            $this->setValue($halfScoreKey,json_encode($halfScoreArr,true));
        }
        return $this->event;
    }

    public function createBattleEndByScore($terroristScore,$counterTerroristScore,$type){

        if (strstr('warmUp',$this->getBattleId())){
            return false;
        }
        $battleEndTime = $this->getValue(['current','battle','end_at']);
        if (strtotime($this->info['socket_time']) - strtotime($battleEndTime) < 5) {
            return false;
        }
        //两队比分之和<=30，RoundEnd事件，有一边的比分=16
        //两队比分之和>30，RoundEnd事件，有一边的比分=15+3*X，另外一边的比分<=15+3*X-2
        $total = $terroristScore + $counterTerroristScore;
        if ($total <= 30) {
            if ($counterTerroristScore == 16 || $terroristScore == 16) {
                $battleFinish = $this->isBattleFinished();
                if (!$battleFinish) {
                    $this->createBattleEndByRoundEnd($counterTerroristScore,$terroristScore,$type);
                }
            }
        } else {
            $maxScore     = 0;
            $anotherScore = 0;
            if ($counterTerroristScore > $terroristScore) {
                $maxScore     = $counterTerroristScore;
                $anotherScore = $terroristScore;
            }
            if ($terroristScore > $counterTerroristScore) {
                $maxScore     = $terroristScore;
                $anotherScore = $counterTerroristScore;
            }
            if ($maxScore && $anotherScore) {


                $x = ($maxScore - 16) / 3;
                if (is_int($x)) {
                    $sumScore = 14 + 3 * $x;
                    if ($anotherScore <= $sumScore) {
                        $battleFinish = $this->isBattleFinished();
                        if (!$battleFinish) {
                            $this->createBattleEndByRoundEnd($counterTerroristScore,$terroristScore,$type);
                        }
                    }
                }
            }

        }
    }

    /**
     * @param $counterTerroristScore
     * @param $terroristScore
     * @param string $type
     * @return bool
     */
    public function createBattleEndByRoundEnd($counterTerroristScore, $terroristScore,$type='round_end')
    {
        $recordKey = [self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_CUSTOM_BATTLE_END];
        $recordArr['battle_order'] = $this->getBattleId();
        $recordArr['event_id'] = $this->info['socket_id'];
        $recordJson = json_encode($recordArr,320);
        $this->lPush($recordKey,$recordJson);

        $match_data['counterTerroristScore'] = $counterTerroristScore;
        $match_data['terroristScore']        = $terroristScore;
        //update kills
        $this->updateKillsOnFrozen();
        //update kast
//        $this->updateKastOnRoundEnd();
        //update rating
        $this->updateRatingOnRoundEnd();
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
        $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
        if ($match_data['counterTerroristScore'] > $match_data['terroristScore']) {
            $winner = 'ct';
        } elseif ($match_data['counterTerroristScore'] < $match_data['terroristScore']) {
            $winner = 'terrorist';
        } else {
            return false;
        }
        $this->setIsLive(false);
        $this->setValue([self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_HLTV_LIVE],false);
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], false);
        $this->setBattleStatus(self::STATUS_END);
        if ($winner == 'ct') {
            $winnerTeam     = $this->getCurrentCt();
            $opponent_order = $winnerTeam['opponent_order'];
        } else {
            $winnerTeam     = $this->getCurrentT();
            $opponent_order = $winnerTeam['opponent_order'];
        }
        $event['match_id']           = (int)$this->matchId;
        $event['battle_id']          = (int)$this->getOutPutBattleId();
        $event['battle_order']       = (int)$this->getOutPutBattleOrder();
        $event['battle_timestamp']   = $this->battle_duration;
        $event['event_id']           = (int)$this->info['socket_id'];
        $event['event_type']         = 'battle_end';
        $event['round_ordinal']      = $this->getRoundOrdinal();
        $event['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);//回合内时间戳(event的log_time)
        $event['round_time']         = $this->getRoundTime($this->info['socket_time']); //回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
        $event['is_bomb_planted']    = $this->getIsBombPlanted();
        $event['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
        $winnerTeam['team_id']       = $winnerTeam['team_id'] ? (int)$winnerTeam['team_id'] : null;
        $winnerTeam['image']         = ImageConversionHelper::showFixedSizeConversion(@ $winnerTeam['image'], 200, 200);
        $event['winner']             = $winnerTeam;
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_EVENTS, self::KEY_TAG_BATTLE_END], json_encode($event, 320));
        //比较最近的battle_end事件
        $recentRoundEndTime = $this->getRoundEndAtFlag();
        if ($recentRoundEndTime&&(strtotime($this->info['socket_time'])-strtotime($recentRoundEndTime)<2)){
            $nowOutPut = true;
        }else{
            $nowOutPut = false;
        }
        if ($nowOutPut||$type=='round_end'){
            $this->needOutPutBattleEndEvent = $event;
        }else{
            $this->setTempBattleEndEvent(json_encode($event,320));
        }
        if ($opponent_order == 1) {
            $this->incrMatch1Score();
            $this->setMatch2Score($this->getMatch2Score());
        } else {
            $this->incrMatch2Score();
            $this->setMatch1Score($this->getMatch1Score());
        }
        $match_team_1_score = $this->getMatch1Score();
        $match_team_2_score = $this->getMatch2Score();
        $battleCount        = $this->match['number_of_games'];
        //set match end&&winner
        $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
        //计算比赛结束
        if ($winnerInfo['is_finish'] == 1) {
            $this->setMatchStatus(self::STATUS_END);
            $matchIndex = $this->getMatchIndex();
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, 'match_end_index'],$matchIndex);
            $this->setMatchEndAt($this->info['socket_time']);
            $this->setMatchWinner($winnerTeam);
        } elseif ($winnerInfo['is_draw'] == 1) {
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_IS_DRAW], true);
        }
        //update battle details score
        $this->setBattleEndAt($this->info['socket_time']);
        $this->setBattleWinner($winnerTeam, $winner);
        if ($match_data['ctTeamId'] == $match_data['startingCt']) {
            $baseArr['start_ct']['score'] = @$match_data['counterTerroristScore'];
            $baseArr['start_t']['score']  = @$match_data['terroristScore'];
        } else {
            $baseArr['start_ct']['score'] = @$match_data['terroristScore'];
            $baseArr['start_t']['score']  = @$match_data['counterTerroristScore'];
        }
        $this->setCurrentBattleDetails($baseArr);
        return true;
    }


    public function bombPlantedEvent()
    {
        $this->setTimeSincePlant($this->info['socket_time']);
        $match_data = $this->match_data;
        $playerNick = $match_data['log'][0]['BombPlanted']['playerNick'];
        $ctPlayers  = $match_data['log'][0]['BombPlanted']['ctPlayers'];
        $tPlayers   = $match_data['log'][0]['BombPlanted']['tPlayers'];
        $steamId    = $this->getSteamId2ByName($playerNick);
        if ($steamId) {
            $playerInfo = $this->getCurrentPlayer($steamId);
            //bomb_planted
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'planted_bomb';
            $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        } else {
            $playerInfo = [];
        }
        $this->event['player_id']           = $playerInfo['player_id']?@(int)$playerInfo['player_id']:null;
        $this->event['nick_name']           = isset($playerInfo['nick_name']) ? $playerInfo['nick_name'] : $playerNick;
        $this->event['steam_id']           = SteamHelper::playerConversionSteamId($steamId);
        $this->event['side']                = @$playerInfo['side'];
        $this->event['position']            = null;
        $this->event['survived_players_ct'] = $ctPlayers;
        $this->event['survived_players_t']  = $tPlayers;
        //setBombPlanted
        $this->setBombPlanted(true);
        $this->needOutPutFrameData();
        $this->record_bomb_planted = true;
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
        $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
        return $this->event;
    }

    public function setBombPlanted($value)
    {
        if ($value == false && $this->event['event_type'] == 'bomb_defused') {
            $this->temp_time_since_plant = $this->getTimeSincePlant($this->event);
        }
        //更新炸弹是否已安放(frame使用)
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_BOMB_PLANTED], $value);

        //update round details
        $baseArr                    = [];
        $baseArr['is_bomb_planted'] = $value;
        $this->setCurrentBattleDetails($baseArr);
        return $this->setCurrentRoundDetails($baseArr);


    }

    /**
     * 必须要输出的事件卡点
     */
    public function needOutPutFrameData()
    {
        $this->frame = [];
        //根据redis的元数据
        $this->frame['match_id']           = (int)$this->matchId;
        $this->frame['game_rules']         = $this->getGameRules();
        $this->frame['match_type']         = $this->getMatchType();
        $this->frame['number_of_games']    = $this->getNumberOfGames();
        $battleId = $this->getOutPutBattleId();
        $this->frame['battle_id']          = $battleId ? (int)$battleId : null;
        $this->frame['battle_order']       = $this->getOutPutBattleOrder();
        $this->frame['map']                = $this->outPutMap($this->getCurrentMap());
        $this->frame['duration']           = $this->battle_duration;
        $this->frame['is_battle_finished'] = $this->isBattleFinished();
        $this->frame['is_match_finished']  = $this->isFinished();
        $this->frame['match_scores']       = $this->outPutMatchScores($this->getMatchScores());
        $this->frame['match_winner']       = $this->outPutMatchWinner($this->getMatchWinner());
        $this->frame['battle_winner']      = $this->outPutBattleWinner($this->getBattleWinner());
        $this->frame['starting_ct']        = $this->outPutTeam($this->getStartingCt());
        $this->frame['starting_t']         = $this->outPutTeam($this->getStartingT());
        $this->frame['is_pause']           = null;
        $this->frame['is_live']            = $this->isLive();
        $this->frame['current_round']      = $this->currentRound();
        $this->frame['is_freeze_time']     = null;
        $this->frame['in_round_timestamp'] = $this->getInRoundTimestamp($this->info['socket_time']);
        $this->frame['round_time']         = $this->getRoundTime($this->info['socket_time']);
        $this->frame['is_bomb_planted']    = $this->getIsBombPlanted();
        $this->frame['time_since_plant']   = $this->getTimeSincePlant($this->info['socket_time']);
        $this->frame['side']               = $this->getSide('hltv');
        $this->frame['server_config']      = $this->getServerConfigFrames();
        $this->needOutPutFrame             = $this->frame;
    }

    public function bombDefusedEvent()
    {
        $this->setBombPlanted(false);
        $match_data = $this->match_data;
        $playerNick = $match_data['log'][0]['BombDefused']['playerNick'];
        $steamId    = $this->getSteamId2ByName($playerNick);
        if ($steamId) {
            $playerInfo = $this->getCurrentPlayer($steamId);
            //bomb_defused
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'defused_bomb';
            $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        } else {
            $playerInfo = [];
        }
        $this->event['player_id'] = !empty($playerInfo) ? @(int)$playerInfo['player_id'] : null;
        $this->event['nick_name'] = !empty($playerInfo) ? @$playerInfo['nick_name'] : $playerNick;
        $this->event['steam_id'] = SteamHelper::playerConversionSteamId($steamId);
        $this->event['side']      = @$playerInfo['side'];
        $this->event['position']  = null;//没有值可以取
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct','hltv');
        $this->setBattlePlayerStatisOnBattleEnd('t','hltv');
        return $this->event;
    }

    /**
     * 减少redis操作
     */
    public function objectSet(){
        $this->battle_duration = $this->getDuration($this->info['socket_time']);
    }

    public function doLive(){
        $this->setValue([self::KEY_TAG_CURRENT,self::KEY_TAG_MATCH,self::KEY_TAG_HLTV_LIVE],$this->match_data['live']);
        if (strstr($this->getBattleId(), 'live')) {
            $updateArr            = [];
            $updateArr['duration'] = $this->battle_duration;
            $this->setCurrentBattleDetails($updateArr);
        }
    }

    public function getTeamIdByRelId($relTeamId){
        try {
            return self::getMainIdByRelIdentityId('team', $relTeamId, self::ORIGIN_ID);
        } catch (Throwable $e) {
            $this->error_catch_log(['msg'=>$e->getMessage()]);
        }
    }
    public function doTeamName(){
        $match_data = $this->match_data;

        if (empty($match_data['tTeamId']) || empty($match_data['ctTeamId'])) {
            return false;
        }

//        if ($match_data['live'] == false && $this->isLive()){
//            return  false;
//        }


        //ct
        $ctSocketTeamName = $match_data['ctTeamName'];
        $ctRealId         = $match_data['ctTeamId'];
        $ctRealStartingId = $match_data['startingCt'];
        $ctTeamInfo       = $this->getTeamById($ctRealId, $ctSocketTeamName);
        $arrNoDo=[
            15,33,39,45,51,57,63,69,75,81,87,93,99
        ];
        if (!in_array($match_data['currentRound'],$arrNoDo)){
            $this->setTeamCt($ctTeamInfo);
        }

        //t
        $tSocketTeamName = $match_data['terroristTeamName'];
        $tRealId         = $match_data['tTeamId'];
        $tRealStartingId = $match_data['startingT'];
        $tTeamInfo       = $this->getTeamById($tRealId, $tSocketTeamName);
        if (!in_array($match_data['currentRound'],$arrNoDo)){
            $this->setTeamT($tTeamInfo);
        }

        //startingCt StartingT
        if ($tRealId == $tRealStartingId) {
            $startCtTeamInfo       = $this->getTeamById($ctRealStartingId, $ctSocketTeamName);
            $startTTeamInfo       = $this->getTeamById($tRealStartingId, $tSocketTeamName);
        } else {
            $startCtTeamInfo       = $this->getTeamById($ctRealStartingId, $tSocketTeamName);
            $startTTeamInfo       = $this->getTeamById($tRealStartingId, $ctSocketTeamName);
        }
        $reload_battle_id=$this->getValue([self::KEY_TAG_RELOAD,self::KEY_TAG_BATTLE_ID]);
        if ($match_data['currentRound'] < 3&&$reload_battle_id!=$this->getBattleId()) {
            $this->setStartingCt($startCtTeamInfo);
            $this->setStartingT($startTTeamInfo);
        }

        return true;
    }

    /**
     * 通过名称获取team
     * @param $id
     * @param $order
     * @param $type
     * @return string
     */
    public function getTeamById($realId,$socketTeamName='')
    {

        //缓存
        $redisTeam = $this->getValue(['table','team' ,$realId]);
        if ($redisTeam) {
            return json_decode($redisTeam, true);
        } else {
            $teamOrder = PandascoreHotBase::getTeamOrderByTeamRelId(@$realId, $this->match, self::ORIGIN_ID);
            $teamId    = $this->getTeamIdByRelId($realId);
            $team                   = Team::find()->select(['id', 'name', 'image'])->where(['id' => $teamId])->asArray()->one();
            $team['team_id']        = $team['id'];
            $team['real_id']        = $realId;
            $team['opponent_order'] = $teamOrder;
            $this->setValue(['table','team' ,$realId], json_encode($team,320));
//            //获取frame和events teams 信息

            $teamRes['team_id']        = (int)$team['id'];
            $teamRes['real_id']        = $realId;
            $teamRes['name']           = $team['name'] ?? $socketTeamName;
            $teamRes['log_name']       = $socketTeamName;
            $teamRes['image']          = $team['image'];
            $teamRes['opponent_order'] = $teamOrder;
            return $teamRes;
        }
    }

    /**
     * update ct round score, battle details score,start_ct score
     * @return bool
     */
    public function doScore()
    {
        $match_data = $this->match_data;
        if ($match_data['live'] == true || $match_data['currentRound']>1) {
            $ctScore        = $match_data['ctTeamScore'];
            $tScore         = $match_data['tTeamScore'];
            if ($this->getTempBattleEndEvent()){
                $match_team_1_score = $this->getMatch1Score();
                $match_team_2_score = $this->getMatch2Score();
                $battleCount        = $this->match['number_of_games'];
                //set match end&&winner
                $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
                //计算比赛结束
                if ($winnerInfo['is_finish'] == 1) {
                    $this->setMatchEndAt($this->info['socket_time']);
                }
                $this->setTempBattleEndEvent('');
                $tempBattleEnd['battle_timestamp'] = $this->battle_duration;
                $this->needOutPutBattleEndEvent =  $tempBattleEnd;

            }else{
                $this->createBattleEndByScore($tScore,$ctScore,'frame');
            }
//

            $currentCtScore = $this->getCtScore();
            $currentTScore  = $this->getTScore();

            $currentTotal   = $currentCtScore + $currentTScore;


            $updateArr                                = [];
            $tTeam                                    = $this->getCurrentT();
            $ctTeam                                   = $this->getCurrentCt();
            $ctTeamAndRealId                   = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_CT, self::KEY_TAG_DETAILS]), true);
            if ($ctTeamAndRealId['real_id'] == $match_data['ctTeamId']){
                //容错只有frame给我们的比分正确的时候才赋值
                //set round score
                $this->setRoundCtScore(@$ctScore);
                $this->setRoundTScore(@$tScore);
            }else{
                //容错只有frame给我们的比分正确的时候才赋值
                //set round score
                $this->setRoundCtScore(@$tScore);
                $this->setRoundTScore(@$ctScore);
                $temp = $tScore;
                $tScore = $ctScore;
                $ctScore = $temp;
            }

            $updateArr['round_ct_score']              = $ctScore;
            $updateArr['round_t_score']               = $tScore;
            $updateArr['ct']['team_id']               = $ctTeam['team_id'];
            $updateArr['ct']['opponent_order']        = $ctTeam['opponent_order'];
            $updateArr['ct']['name']                  = $ctTeam['name'];
            $updateArr['ct']['image']                 = $ctTeam['image'];
            $updateArr['terrorist']['team_id']        = $tTeam['team_id'];
            $updateArr['terrorist']['opponent_order'] = $tTeam['opponent_order'];
            $updateArr['terrorist']['name']           = $tTeam['name'];
            $updateArr['terrorist']['image']          = $tTeam['image'];
            $this->setCurrentRoundDetails($updateArr);
            //set start_ct score
            $baseArr = [];
            $startCt = $this->getStartingCt();
            $tableCt = $this->getTeamById($match_data['ctTeamId']);
//            $tableCt = $this->getTeamById($match_data['ctTeamId']);
//            $countMap = $this->getNewBattleCountByMap($this->match_data['mapName']);
//            if ($countMap<1){
            if ($tableCt['id'] == $startCt['team_id']) {
                $baseArr['start_ct']['score'] = $match_data['ctTeamScore'];
                $baseArr['start_t']['score']  =$match_data['tTeamScore'];
            } else {
                //$ctScore
                $baseArr['start_ct']['score'] = @$match_data['tTeamScore'];
                $baseArr['start_t']['score']  = @$match_data['ctTeamScore'];
            }
//            }

            //set start_ct win_round_1_side,first_to_5_rounds_wins_side,win_round_16_side
            $total         = $ctScore + $tScore;
            if ($currentTotal!=$total){
                $updateArr = [];
                $updateArr['end_at'] = $this->info['socket_time'];
                $this->setCurrentRoundDetails($updateArr);
            }
            $currentDetail = $this->getCurrentBattleDetails();
            if ($total == 0 && $this->getBattleBeginAt()) {
                $updateArr = [];
                $updateArr['begin_at'] = $this->getBattleBeginAt();
                $this->setCurrentRoundDetails($updateArr);
            }
            if ($total == 1) {
                if (array_key_exists(0,$match_data['ctMatchHistory']['firstHalf'])){
                    if ($match_data['ctMatchHistory']['firstHalf'][0]['type']=='lost'){
                        $log_win_camp = 'terrorist';
                        $win_team = $this->getCurrentT();
                    }else{
                        $log_win_camp = 'ct';
                        $win_team = $this->getCurrentCt();
                    }
                    $baseBattleArr                          = [];
                    $baseBattleArr['win_round_1_side']      = $log_win_camp;
                    $baseBattleArr['win_round_1_team']      = $win_team['opponent_order'];
                    $baseBattleArr['win_round_1_team_name'] = $win_team['name'];
                    $baseBattleArr['win_round_1_detail']    = "";
                    $this->setCurrentBattleDetails($baseBattleArr);
                }
            }
            if (($ctScore  == 5 && $tScore < 5) || ($tScore == 5 && $ctScore  < 5)) {
                if ($ctScore  == 5) {
                    $firstFiveTeam                                     = $this->getCurrentCt();
                    $baseBattleArr['first_to_5_rounds_wins_side']      = 'ct';
                    $baseBattleArr['first_to_5_rounds_wins_team']      = $firstFiveTeam['opponent_order'];
                    $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                    $baseBattleArr['first_to_5_rounds_wins_team_name'] = $firstFiveTeam['name'];
                    $this->setCurrentBattleDetails($baseBattleArr);
                } else {
                    $firstFiveTeam                                     = $this->getCurrentT();
                    $baseBattleArr['first_to_5_rounds_wins_side']      = 'terrorist';
                    $baseBattleArr['first_to_5_rounds_wins_team']      = $firstFiveTeam['opponent_order'];
                    $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                    $baseBattleArr['first_to_5_rounds_wins_team_name'] = $firstFiveTeam['name'];
                    $this->setCurrentBattleDetails($baseBattleArr);
                }
            }
//            if ($ctScore  == 16 || $tScore  == 16){
//
//                if ($ctScore  == 16){
//                    $winnerTeamInfo = $this->getCurrentCt();
//                    $this->setBattleWinner($winnerTeamInfo,'ct');
//                }else{
//                    $winnerTeamInfo = $this->getCurrentT();
//                    $this->setBattleWinner($winnerTeamInfo,'terrorist');
//                }
//            }
            if ($total == 16) {
                if (array_key_exists(0, $match_data['ctMatchHistory']['secondHalf'])) {
                    if ($match_data['ctMatchHistory']['secondHalf'][0]['type'] == 'lost') {
                        $log_win_camp = 'terrorist';
                        $win_team     = $this->getCurrentT();
                    } else {
                        $log_win_camp = 'ct';
                        $win_team     = $this->getCurrentCt();
                    }
                    $baseBattleArr                           = [];
                    $baseBattleArr['win_round_16_side']      = $log_win_camp;
                    $baseBattleArr['win_round_16_team']      = $win_team['opponent_order'];
                    $baseBattleArr['win_round_16_team_name'] = $win_team['name'];
                    $baseBattleArr['win_round_16_detail']    = "";
                    $this->setCurrentBattleDetails($baseBattleArr);
                }
            }

            //set start_ct 1st_half_score,2nd_half_score,ot_score
            if ($total <= 15&&$match_data['currentRound']<=15) {
                $baseBattleArr                               = [];
                $baseBattleArr['start_ct']['1st_half_score'] = (int)$ctScore;
                $baseBattleArr['start_t']['1st_half_score']  = (int)$tScore;
                $this->setCurrentBattleDetails($baseBattleArr);
            } elseif ($total > 15 && $total <= 30) {
                $baseBattleArr                               = [];
                $baseBattleArr['start_ct']['2nd_half_score'] = $tScore - $currentDetail['start_ct']['1st_half_score'];
                $baseBattleArr['start_t']['2nd_half_score']  = $ctScore - $currentDetail['start_t']['1st_half_score'];
                $baseBattleArr['start_ct']['2nd_half_score'] = $baseBattleArr['start_ct']['2nd_half_score'] > 0 ? $baseBattleArr['start_ct']['2nd_half_score'] : 0;
                $baseBattleArr['start_t']['2nd_half_score']  = $baseBattleArr['start_t']['2nd_half_score'] > 0 ? $baseBattleArr['start_t']['2nd_half_score'] : 0;
                $this->setCurrentBattleDetails($baseBattleArr);
            } elseif ($total > 30) {
                $baseBattleArr = [];
                $currentCt     = $this->getCurrentCt();
                $startCt       = $this->getStartingCt();
                if ($currentCt['opponent_order'] == $startCt['opponent_order']) {
                    $baseBattleArr['start_ct']['ot_score'] = $ctScore - 15;
                    $baseBattleArr['start_t']['ot_score']  = $tScore - 15;
                    $baseBattleArr['start_ct']['ot_score'] = $baseBattleArr['start_ct']['ot_score'] > 0 ? $baseBattleArr['start_ct']['ot_score'] : 0;
                    $baseBattleArr['start_t']['ot_score']  = $baseBattleArr['start_t']['ot_score'] > 0 ? $baseBattleArr['start_t']['ot_score'] : 0;
                } else {
                    $baseBattleArr['start_ct']['ot_score'] = $tScore - 15;
                    $baseBattleArr['start_t']['ot_score']  = $ctScore - 15;
                    $baseBattleArr['start_ct']['ot_score'] = $baseBattleArr['start_ct']['ot_score'] > 0 ? $baseBattleArr['start_ct']['ot_score'] : 0;
                    $baseBattleArr['start_t']['ot_score']  = $baseBattleArr['start_t']['ot_score'] > 0 ? $baseBattleArr['start_t']['ot_score'] : 0;
                }
                $this->setCurrentBattleDetails($baseBattleArr);
            }
            $this->setStartingCtScore($baseArr['start_ct']['score']);
            $this->setStartingTScore($baseArr['start_t']['score']);
            $this->setCurrentBattleDetails($baseArr);
        }

        return true;
    }

    public function doTerrorist()
    {
        $terrorist = $this->match_data['TERRORIST'];
        foreach ($terrorist as $k => $v) {
            $updateArr             = [];
            $updateArr['steamId']  = $v['steamId'];
            $updateArr['steam_id'] = $v['steamId'];
            $updateArr['is_deleted'] = 2;
            if (!empty($this->playerList) && array_key_exists($v['steamId'], $this->playerList)) {
                $updateArr['player_id'] = $this->playerList[$v['steamId']]['id'];
            } else {
                $updateArr['player_id'] = null;
            }
            $updateArr['k_d_diff']          = $v['score'] - $v['deaths'];
            $updateArr['first_kills_diff']  = $v['advancedStats']['entryKills'] - $v['advancedStats']['entryDeaths'];
            $updateArr['playerName']        = $v['name'];
            $updateArr['nick_name']         = $v['nick'];
            $updateArr['side']              = "terrorist";
            $updateArr['kills']             = $v['score'] ? $v['score'] : 0;
            $updateArr['deaths']            = $v['deaths'] ? $v['deaths'] : 0;
            $updateArr['assists']           = $v['assists'] ? $v['assists'] : 0;
            $updateArr['is_alive']          = $v['hp'] == 0 ? false : $v['alive'];
            $updateArr['money']             = $v['money'] ? $v['money'] : 0;
            $updateArr['adr']               = round($v['damagePrRound'], 2);
            $updateArr['hp']                = $v['hp'];
            $updateArr['has_kevlar']        = $v['kevlar'] ? $v['kevlar'] : false;
            $updateArr['has_helmet']        = $v['helmet'] ? $v['helmet'] : false;
            $updateArr['has_defusekit']     = $v['hasDefusekit'] ? $v['hasDefusekit'] : false;
            if (array_key_exists('advancedStats', $v)&&!empty($v['advancedStats'])) {
                $updateArr['kast']              = $this->match_data['currentRound']>0?round($v['advancedStats']['kast']/$this->match_data['currentRound'],4):'0';
                $updateArr['first_kills']       = $v['advancedStats']['entryKills'] ? $v['advancedStats']['entryKills'] : 0;
                $updateArr['first_deaths']      = $v['advancedStats']['entryDeaths'] ? $v['advancedStats']['entryDeaths'] : 0;
                $updateArr['multi_kills']       = $v['advancedStats']['multiKillRounds'] ? $v['advancedStats']['multiKillRounds'] : 0;
                $updateArr['one_on_x_clutches'] = $v['advancedStats']['oneOnXWins'] ? $v['advancedStats']['oneOnXWins'] : 0;
                $updateArr['flash_assist']      = $v['advancedStats']['flashAssists'] ? $v['advancedStats']['flashAssists'] : 0;

            }
            $this->setHltvBattlePlayerBySteamId($v['steamId'], $updateArr, @$v['primaryWeapon']);
        }
    }


    /**
     * @param $dbId
     * @param string $nick_name
     * @return array|bool|\yii\db\ActiveRecord|null
     */
    public function getHltvPlayerInfoBySteamId($dbId,$nick_name='',$steamId)
    {

        $redis_res = $this->hGet(['table','player'], $steamId);
        if ($redis_res) {
            return json_decode($redis_res,true);
        } else {
            if ($dbId=='-1'){
                return false;
//                    $player = \app\modules\org\models\Player::find()->where(['steam_id' => $steamId])->asArray()->all();
            }else{
                $player = BindingPlayerService::getPlayersByStandardPlayerRelIdentityIds($dbId,7,1);
            }

            if (!empty($player)) {
                $currentPlayer = current($player);
                $this->hSet(['table','player'] , $steamId, json_encode($currentPlayer,320));
                if ($nick_name){
                    $this->hSet(['table','player_name'], $nick_name, json_encode($currentPlayer,320));
                }

            }else{
                return false;
            }



            return $player;
        }
    }

    public function mutiHsetKey($key){
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        return  sprintf("%s:%s", $matchId, $keyString);
    }
    public function setHltvBattlePlayerBySteamId($steamId, $updateArr,$primaryWeapon='')
    {
        if (empty($steamId)) {
            return false;
        }
        //battle player
        $key = $this->mutiHsetKey([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->battleId, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST]);
        $newKey = $this->wsKey($key);
        if (!empty($this->battlePlayerList[$steamId]) && is_array($this->battlePlayerList[$steamId])) {
            $updateArr = array_merge($this->battlePlayerList[$steamId],$updateArr);
        }else{

        }
        //内存的值
        if ($this->refresh_task) {
            $keyString                          = implode("_", [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->battleId, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST]);
            $this->refresh_task_str[$keyString][$steamId] = json_encode($updateArr,320);
        }
        $this->muti->hSet($newKey,$steamId,json_encode($updateArr,320));

        //current player
        $keyCurrent = $this->mutiHsetKey([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST]);
        $newKeyCurrent = $this->wsKey($keyCurrent);
        //内存的值
        if ($this->refresh_task) {
            $keyString                          = implode("_", [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST]);
            $this->refresh_task_str[$keyString][$steamId] = json_encode($updateArr,320);
        }
        $this->muti->hSet($newKeyCurrent,$steamId,json_encode($updateArr,320));

        //weapon
        if ($primaryWeapon){
            $keyWeapon = $this->mutiHsetKey([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->battleId, self::KEY_TAG_ROUND, $this->roundOrder,
                self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS]);
            $newKeyWeapon = $this->wsKey($keyWeapon);
            $weapons_json = json_encode([$primaryWeapon],320);
            //内存的值
            if ($this->refresh_task) {
                $keyString                          = implode("_", [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->battleId, self::KEY_TAG_ROUND, $this->roundOrder,
                    self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS]);
                $this->refresh_task_str[$keyString][$steamId] = $weapons_json;
                $keyString2                          = implode("_",[self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS]);
                $this->refresh_task_str[$keyString2][$steamId] = $weapons_json;
            }
            $keyWeapon2 = $this->mutiHsetKey([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS]);
            $newKeyWeapon2 = $this->wsKey($keyWeapon2);
            $this->muti->hSet($newKeyWeapon, $steamId, $weapons_json);
            $this->muti->hSet($newKeyWeapon2, $steamId, $weapons_json);
        }
        return true;
    }


    public function doCt()
    {
        $ct = $this->match_data['CT'];
        foreach ($ct as $k => $v) {
            $updateArr                  = [];
            $updateArr['steamId']           = $v['steamId'];
            $updateArr['steam_id']           = $v['steamId'];
            $updateArr['is_deleted']           = 2;
            if (!empty($this->playerList)&&array_key_exists($v['steamId'],$this->playerList)){
                $updateArr['player_id']    = $this->playerList[$v['steamId']]['id'];
            }else{
                $updateArr['player_id'] = null;
            }


            $updateArr['k_d_diff'] = $v['score']-$v['deaths'];
            $updateArr['first_kills_diff'] = $v['advancedStats']['entryKills']-$v['advancedStats']['entryDeaths'];
            $updateArr['playerName']    = $v['name'];
            $updateArr['nick_name']     = $v['nick'];
            $updateArr['side']          = "ct";
            $updateArr['kills']         = $v['score'];
            $updateArr['deaths']        = $v['deaths'];
            $updateArr['assists']       = $v['assists'];
            $updateArr['is_alive']      = $v['hp']==0?false:$v['alive'];
            $updateArr['money']         = $v['money'];
            $updateArr['adr']           = round($v['damagePrRound'],2);
            $updateArr['hp']            = $v['hp'];
            $updateArr['has_kevlar']    = $v['kevlar'];
            $updateArr['has_helmet']    = $v['helmet'];
            $updateArr['has_defusekit'] = $v['hasDefusekit'];
            if (array_key_exists('advancedStats', $v)&&!empty($v['advancedStats'])) {
                $updateArr['kast']              = $this->match_data['currentRound']>0?round($v['advancedStats']['kast']/$this->match_data['currentRound'],4):'0';
                $updateArr['first_kills']       = $v['advancedStats']['entryKills'];
                $updateArr['first_deaths']      = $v['advancedStats']['entryDeaths'];
                $updateArr['multi_kills']       = $v['advancedStats']['multiKillRounds'];
                $updateArr['one_on_x_clutches'] = $v['advancedStats']['oneOnXWins'];
                $updateArr['flash_assist']      = $v['advancedStats']['flashAssists'];
            }
            $this->setHltvBattlePlayerBySteamId($v['steamId'], $updateArr ,@$v['primaryWeapon']);
        }

    }

    public function doCtMatchHistory()
    {
        $ctHistory = $this->match_data['ctMatchHistory'];
        $ctOrder = @$this->getCurrentCt()['opponent_order'];
        $tOrder = @$this->getCurrentT()['opponent_order'];
        if (!empty($ctHistory['firstHalf'])) {
            foreach ($ctHistory['firstHalf'] as $k => $v) {
                if (!empty(@$v['roundOrdinal'])) {
                    $winner_order = '';
                    $type         = '';
                    $side         = '';
                    if ($v['type'] == 'Terrorists_Win') {
                        $type         = 'terrorists_win';
                        $side         = 'terrorist';
                        $winner_order = $tOrder;
                    } elseif ($v['type'] == 'CTs_Win') {
                        $type         = 'cts_win';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    } elseif ($v['type'] == 'Target_Bombed') {
                        $type         = 'target_bombed';
                        $side         = 'terrorist';
                        $winner_order = $tOrder;
                    } elseif ($v['type'] == 'Bomb_Defused') {
                        $type         = 'bomb_defused';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    }elseif ($v['type'] == 'Target_Saved') {
                        $type         = 'target_saved';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    }

                    $updateArr                  = [];
                    $updateArr['round_ordinal'] = $v['roundOrdinal'];
                    $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
                    $roundDetails=json_decode($this->hGet($roundKey,$v['roundOrdinal']), true);
                    if (empty($roundDetails['round_win_type'])){
                        if ($type) {
                            $updateArr['round_win_type'] = $type;
                        }
                        if ($side) {
                            $updateArr['winner_side'] = $side;
                        }
                        if ($winner_order) {
                            $updateArr['winner_order'] = $winner_order;
                        }
                        $updateArr['survived_players_ct'] = $v['survivingPlayers'];
                        $this->setRoundDetails($updateArr, $v['roundOrdinal']);
                    }
                }

            }
        }
        if (!empty($ctHistory['secondHalf'])) {
            foreach ($ctHistory['secondHalf'] as $k => $v) {
                if (!empty($v['roundOrdinal'])) {
                    $winner_order = '';
                    $type         = '';
                    $side         = '';
                    if ($v['type'] == 'Terrorists_Win') {
                        $type         = 'terrorists_win';
                        $side         = 'terrorist';
                        $winner_order = $tOrder;
                    } elseif ($v['type'] == 'CTs_Win') {
                        $type         = 'cts_win';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    } elseif ($v['type'] == 'Target_Bombed') {
                        $type         = 'target_bombed';
                        $side         = 'terrorist';
                        $winner_order = $tOrder;
                    } elseif ($v['type'] == 'Bomb_Defused') {
                        $type         = 'bomb_defused';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    }elseif ($v['type'] == 'Target_Saved') {
                        $type         = 'target_saved';
                        $side         = 'ct';
                        $winner_order = $ctOrder;
                    }
                    $updateArr                  = [];
                    $updateArr['round_ordinal'] = $v['roundOrdinal'];
                    $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
                    $roundDetails=json_decode($this->hGet($roundKey,$v['roundOrdinal']), true);
                    if (empty($roundDetails['round_win_type'])){
                        if ($type) {
                            $updateArr['round_win_type'] = $type;
                        }
                        if ($side) {
                            $updateArr['winner_side'] = $side;
                        }
                        if ($winner_order) {
                            $updateArr['winner_order'] = $winner_order;
                        }
                        $updateArr['survived_players_ct'] = $v['survivingPlayers'];
                        $this->setRoundDetails($updateArr, $v['roundOrdinal']);
                    }
                }
            }
        }

    }

    public function doTerroristMatchHistory()
    {
        $terroristHistory = $this->match_data['terroristMatchHistory'];
        $ctOrder = @$this->getCurrentCt()['opponent_order'];
        $tOrder = @$this->getCurrentT()['opponent_order'];
        if (!empty($terroristHistory['firstHalf'])) {
            foreach ($terroristHistory['firstHalf'] as $k => $v) {
                $winner_order = '';
                $type         = '';
                $side         = '';
                if ($v['type'] == 'Terrorists_Win') {
                    $type         = 'terrorists_win';
                    $side         = 'terrorist';
                    $winner_order = $tOrder;
                } elseif ($v['type'] == 'CTs_Win') {
                    $type         = 'cts_win';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                } elseif ($v['type'] == 'Target_Bombed') {
                    $type         = 'target_bombed';
                    $side         = 'terrorist';
                    $winner_order = $tOrder;
                } elseif ($v['type'] == 'Bomb_Defused') {
                    $type         = 'bomb_defused';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                }elseif ($v['type'] == 'Target_Saved') {
                    $type         = 'target_saved';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                }
                $updateArr                  = [];
                $updateArr['round_ordinal'] = $v['roundOrdinal'];
                $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
                $roundDetails=json_decode($this->hGet($roundKey,$v['roundOrdinal']), true);
                if (empty($roundDetails['round_win_type'])){
                    if ($type) {
                        $updateArr['round_win_type'] = $type;
                    }
                    if ($side) {
                        $updateArr['winner_side'] = $side;
                    }
                    if ($winner_order) {
                        $updateArr['winner_order'] = $winner_order;
                    }
                    $updateArr['survived_players_t'] = $v['survivingPlayers'];
                    $this->setRoundDetails($updateArr, $v['roundOrdinal']);
                }

            }
        }
        if (!empty($terroristHistory['secondHalf'])) {
            foreach ($terroristHistory['secondHalf'] as $k => $v) {
                $winner_order = '';
                $type         = '';
                $side         = '';
                if ($v['type'] == 'Terrorists_Win') {
                    $type         = 'terrorists_win';
                    $side         = 'terrorist';
                    $winner_order = $tOrder;
                } elseif ($v['type'] == 'CTs_Win') {
                    $type         = 'cts_win';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                } elseif ($v['type'] == 'Target_Bombed') {
                    $type         = 'target_bombed';
                    $side         = 'terrorist';
                    $winner_order = $tOrder;
                } elseif ($v['type'] == 'Bomb_Defused') {
                    $type         = 'bomb_defused';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                }elseif ($v['type'] == 'Target_Saved') {
                    $type         = 'target_saved';
                    $side         = 'ct';
                    $winner_order = $ctOrder;
                }
                $updateArr                  = [];
                $updateArr['round_ordinal'] = $v['roundOrdinal'];
                $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
                $roundDetails=json_decode($this->hGet($roundKey,$v['roundOrdinal']), true);
                if (empty($roundDetails['round_win_type'])){
                    if ($type) {
                        $updateArr['round_win_type'] = $type;
                    }
                    if ($side) {
                        $updateArr['winner_side'] = $side;
                    }
                    if ($winner_order) {
                        $updateArr['winner_order'] = $winner_order;
                    }
                    $updateArr['survived_players_t'] = $v['survivingPlayers'];
                    $this->setRoundDetails($updateArr, $v['roundOrdinal']);
                }

            }
        }
    }


    /**
     * 获取round_time 回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
     * @param $event
     * @return false|string
     */
    public function getRoundTime($time)
    {
        if ($this->event['event_type']=='round_start'||$this->event['event_type']=='battle_start'){
            return 115;
        }
        $round_start_time = strtotime($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT]));
        $time             = 115 - (strtotime($time) - $round_start_time);
        return $time > 0 ? $time : 0;
    }

    public function doCurrentRound()
    {
        $match_data = $this->match_data;
        if (@$match_data['currentRound']) {
            $roundOrdinal = $this->getRoundOrdinal();

//            if ($match_data['currentRound']==$roundOrdinal+1||$match_data['currentRound']+1==$roundOrdinal){
            //当round新建的时候立即赋值round的begin_at


            $this->setRoundOrder($match_data['currentRound']);


            //set round weapon
//                $this->setRoundWeaponOnFrozenStart();
//            }

            //update round survived_players
            $this->updateRoundSurvived('hltv');
        } else {
            if (!$this->getRoundOrdinal()){
                $this->setRoundOrder(1);
            }

        }
    }


    /**
     * getTimeSincePlant
     * @return mixed
     */
    public function getTimeSincePlant($time)
    {
        if (!empty($this->event)&&!$this->event['is_bomb_planted']){
            return 0;
        }
        if ($this->event['event_type'] == 'round_start' || $this->match_data['frozen']==true ) {
            return 0;
        }
        if ($this->event['event_type']=='bomb_planted'){
            return 40;
        }
        //40秒倒计时
        $time_since_plant = 0;
        //转化
        $time_since_plant_time = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_TIME_SINCE_PLANT]);
        if ($time_since_plant_time) {
            $time_since_plant = 40 - (strtotime($time) - strtotime($time_since_plant_time));
        }
        if ($time_since_plant > 40) {
            $time_since_plant = 0;
        }
        $time_since_plant = $time_since_plant > 0 ? $time_since_plant : 0;
        return (int)$time_since_plant;
    }





    public function setBattleIdByMap()
    {
        $match_data = $this->match_data;
        $map  = $this->findOneMapByName($match_data['mapName']);
        if (!empty($map)){
            $data['map_id']                   = (int)$map['id'];
            $data['name']                     = $map['name'];
            $data['name_cn']                  = $map['name_cn'];
            $data['external_id']              = $map['external_id'];
            $data['external_name']            = $map['external_name'];
            $data['short_name']               = $map['short_name'];
            $data['map_type']                 = $map['e_name'];
            $data['map_type_cn']              = $map['c_name'];
            $data['is_default']               = $map['is_default'] == 1;
            $data['slug']              = $map['slug'];
            $data['image']['square_image']    = $map['square_image'];
            $data['image']['rectangle_image'] = $map['rectangle_image'];
            $data['image']['thumbnail']       = $map['thumbnail'];
            $this->setCurrentMap($data);
        }
        return true;

    }

    public function incrNewBattleCountEveryMap($mapName){
        return $this->incr([self::KEY_TAG_HISTORY,self::KEY_TAG_MAP,$mapName]);
    }

    public function getNewBattleCountByMap($mapName){
        return $this->getValue([self::KEY_TAG_HISTORY,self::KEY_TAG_MAP,$mapName]);
    }

    public function getOutPutBattleOrder()
    {
        $battleId    = $this->getBattleId();
        $battleOrder = $this->getBattleOrder();
        if (strstr($battleId, 'warmUp')) {
            return null;
        } else {
            return (int)$battleOrder;
        }
    }

    /**
     * getDuration 对局时长
     * @param $socketTime
     * @return false|int
     */
    public function getDuration($socketTime)
    {
        $match_start_time = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT]);
        $match_start_time = !$match_start_time ? 0 : strtotime($match_start_time);
        $duration         = $match_start_time == 0 ? 0 : (strtotime($socketTime) - $match_start_time);
        return $duration < 0 ? 0 : $duration;
    }

    public function getOutPutBattleId()
    {
        $battleId = $this->getRealBattleId();
        if (!$battleId) {
            return null;
        } else {
            return $battleId;
        }
    }

    /**
     * @return mixed|null
     */
    public function getRealBattleId()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID]) : null;
    }


    public function batchDelRedisKey($redis_key)
    {
        if (!$redis_key || $redis_key == '*' || (!strstr($redis_key,'ws')&&!strstr($redis_key,'hltv_ws'))) {
            return false;
        }
        $redis = $this->redis;
        $pre   = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 100)) {
            call_user_func_array([$redis, 'del'], $arr_keys);
//            echo var_export($arr_keys, true) . PHP_EOL;
        }
        return true;
    }



    /**
     * clear api Battle
     * @param $matchId
     */
    public function clearBattleMatchId($matchId,$type='')
    {
        if (empty($matchId)){
            return false;
        }
        $battles = $this->getMatchBattleByMatchId($matchId);
        //clear api Battle
        foreach ($battles as $kBattle => $vBattle) {
            if ($vBattle['id']) {
                //调用jianqi的清除battle方法
                BattleService::deleteBattle('csgo', $vBattle['id']);
            }
        }
    }

    public function batchExpireRedisKey($redis_key)
    {
        if (!$redis_key || $redis_key == '*' || (!strstr($redis_key,'ws')&&!strstr($redis_key,'hltv_ws'))) {
            return false;
        }
        $redis = $this->redis;
        $pre   = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 200)) {
            if (!empty($arr_keys)){
                foreach ($arr_keys as $k=>$v){
                    $redis->expire($v,86400);
                }
            }
        }
        return true;
    }

}