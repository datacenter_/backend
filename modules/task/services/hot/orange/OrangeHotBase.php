<?php
/**
 *
 */

namespace app\modules\task\services\hot\orange;


use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\hot\HotBase;

class OrangeHotBase extends HotBase
{
    public static function getRestInfo($action,$params)
    {
//        return OrangeBase::getInfo($action,$params);
        return OrangeBase::curlGet($action,'GET',$params);
    }
}