<?php
namespace app\modules\task\services\hot\orange;
use app\modules\common\services\Consts;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\battle\LolBattleService;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\org\models\Player;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class OrangeHotLolMatch extends OrangeHotBase implements HotMatchInterface
{
    private static $instance;
    private static $lol_items;
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $OrangeHotLolMatch = self::getInstance();
        $redis = $OrangeHotLolMatch->redis;
        //获取类型的type
        $refreshType = @$tagInfo['ext2_type'];
        $timeStart = date('Y-m-d H:i:s');
        $info = @json_decode($taskInfo['params'],true);
        $hotId = $info['id'];
        $matchId=$info['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,4);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        $matchInfo=Match::find()->where(['id'=>$matchId])->asArray()->one();
        $tournament_id = $matchInfo['tournament_id'];
        //获取数据源的赛事ID
        $eid = @HotBase::getRelIdByMasterId('tournament',$tournament_id,4);
        if (empty($eid)){
            return "比赛没有绑定的赛事ID";
        }
        $restInfos = self::getCurlInfo($eid,$relIdentityId);
        if (isset($restInfos['match']) && count($restInfos['battles']) > 0){
            $conversionInfo = self::conversion($restInfos,$matchInfo);
            $infoFormatAddBindingInfo = self::addBindingInfo($redis,$conversionInfo,$matchInfo);
            $timeEnd=date('Y-m-d H:i:s');
            $extInfo=[
                'time_begin'=>$timeStart,
                'time_end'=>$timeEnd,
                'task_id'=>$taskInfo['id'],
                'match_id'=>$matchId,
            ];
            //add HotLog
            self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
            // 更新到数据库
            $refreshRes = self::refreshInfo($redis,$matchId,$infoFormatAddBindingInfo,$refreshType);
            // 更新hotdata状态
            self::refreshHotDataStatusByMatchInfo($hotId,$refreshRes);
        }
        return 'success';
    }

    public static function getCurlInfo($eid,$relMatchId)
    {
        $info=[
            'match'=>[],
            'battles'=>[],
        ];
        $matchInfo=self::getMatchInfo($eid,$relMatchId);
        $battleIds=$matchInfo['matches'];
        foreach($battleIds as $battleId){
            $battleInfo=self::getBattle($battleId);
            $info['battles'][]=$battleInfo['data'];
        }
        $info['match']=$matchInfo;
        return $info;
    }

    public static function getMatchInfo($eid,$relMatchId)
    {
        $infoMatchList=self::getMatchList($eid);
        return $infoMatchList[$relMatchId];
    }

    public static function getMatchList($eid)
    {
        $action='/lol/event/schedule';
        $params=[
            'eid'=>$eid
        ];
        $infoJson=OrangeBase::curlGet($action,'GET',$params);
        $info=@json_decode($infoJson,true);
        $matchList=[];
        foreach($info['data']['stageList'] as $stage){
            foreach($stage['schedule'] as $match){
                $matchList[$match['scheduleid']] = $match;
                $matchList[$match['scheduleid']]['game_version'] = $info['data']['gameversion'];
            }
        }
        return $matchList;
    }

    private static function getBattle($battleId)
    {
        $action='/lol/schedule/match';
        $params=[
            'matchid'=>$battleId,
        ];
        $infoJson=self::getRestInfo($action,$params);
        if($infoJson){
            $info = json_decode($infoJson, true);
            return $info;
        }
    }

    public static function refreshInfo($redis,$matchId, $info,$refreshType)
    {
        if ($refreshType == 'refresh'){

        }
        foreach ($info['battles'] as $key => $battle) {
            // 根据battleId刷新对应的battle信息
            self::refreshBattle($redis,$matchId, $battle);
        }
        //更新match层信息
        $match_base_info = [
            'game_version' => @$info['match']['game_version']
        ];
        self::setMatchBase($matchId, $match_base_info);
        unset($info['match']['game_version']);
        self::setMatch($matchId, $info['match']);
        $match_complete = false;
        if ($info['match']['status'] == 3){
            $match_complete = true;
        }
        return $match_complete;
    }

    private static function refreshBattle($redis,$matchId, $battle)
    {
        //判断有没有对局时长 没有就不创建battle
        if (!isset($battle['base']['duration'])){
            return true;
        }
        $have_battle = false;
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
            $have_battle = true;
        }
        $battleId=$battleBase['id'];

        if ($have_battle){
            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleId,'match'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');
        }

        BattleService::setBattleInfo($battleId,'lol',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'lol',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'lol',$battle['static'],BattleService::DATA_TYPE_STATICS);
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'lol',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        //判断关联关系
        self::setMatchBattleRelation($redis,$matchId,$battleId,$battle);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    private static function setMatchBase($id, $attribute)
    {
        $matchBase = MatchBase::find()->where(['match_id' => $id])->one();
        if ($matchBase) {
            $matchBase->setAttributes($attribute);
            if (!$matchBase->save()) {
                throw new BusinessException($matchBase->getErrors(), "设置match_base_data失败");
            }
        }
        return true;
    }

    public static function setStatus($hotId, $status)
    {
        $hotInfo = self::getHotInfo($hotId);
        $hotInfo->setAttribute('deal_status', $status);
        if (!$hotInfo->save()) {
            throw new BusinessException($hotInfo->getErrors(), '修改状态失败');
        }
    }

    public static function getHotInfo($hotId)
    {
        return HotDataRunningMatch::find()->where(['id' => $hotId])->one();
    }
    //conversion
    public static function conversion($infoFormat,$matchInfo)
    {
        $formatData = [
            'match' => '',
            'battles' => [],
        ];
        $matchData = $infoFormat['match'];
        $teamRelIdRelation = [];
        $matchInfoConfig = [
            'auto_origin_id' => 4,
            'team_1_score_old' => 'onewin',
            'team_2_score_old' => 'twowin',
            'team1_id' => 'oneseedid',
            'rel_team1_id' => 'oneseedid',
            'team2_id' => 'twoseedid',
            'rel_team2_id' => 'twoseedid',
            'winner' => function($params){
                $winner = '';
                if($params['bonum'] == 3){
                    if($params['onewin'] == 2){
                        $winner = $params['oneseedid'];
                    }elseif ($params['twowin'] == 2){
                        $winner = $params['twoseedid'];
                    }
                }elseif ($params['bonum'] == 5){
                    if($params['onewin'] == 3){
                        $winner = $params['oneseedid'];
                    }elseif ($params['twowin'] == 3){
                        $winner = $params['twoseedid'];
                    }
                }else{
                    if($params['onewin'] == 1){
                        $winner = $params['oneseedid'];
                    }elseif ($params['twowin'] == 1){
                        $winner = $params['twoseedid'];
                    }
                }
                return $winner;
            },
            'is_battle_detailed' => function ($params) {
                if(isset($params['matches']) && count($params['matches'])){
                    return 1;
                }else{
                    return 2;
                }
            },
            'begin_at' =>function($params){
                if ($params['starttime']){
                    return date('Y-m-d H:i:s',$params['starttime']);
                }else{
                    return null;
                }
            },
            'game_version' => 'game_version'
        ];
        $formatData['match']  = Mapping::transformation($matchInfoConfig, $matchData);
        $team1_id = self::getMainIdByRelIdentityId('team',$formatData['match']['team1_id'],4);
        $team2_id = self::getMainIdByRelIdentityId('team',$formatData['match']['team2_id'],4);
        $formatData['match']['team1_id'] = (Int)$team1_id;
        $formatData['match']['team2_id'] = (Int)$team2_id;
        //队伍原始和平台ID 关系
        $teamRelIdRelation[$formatData['match']['rel_team1_id']] = (Int)$team1_id;
        $teamRelIdRelation[$formatData['match']['rel_team2_id']] = (Int)$team2_id;
        if (!empty($formatData['match']['winner'])){
            $match_winner_id = $teamRelIdRelation[$formatData['match']['winner']];
            $match_winner_order = self::getTeamOrderByTeamId($match_winner_id,$matchInfo);
            $formatData['match']['winner'] = $match_winner_order;
        }
        foreach ($infoFormat['battles'] as $battle){
            $battleInfo = self::conversionBattle($battle,$matchInfo,$teamRelIdRelation);
            $formatData['battles'][] = $battleInfo;
        }
        return $formatData;
    }
    //处理battle
    private static function conversionBattle($restBattleInfo,$matchInfo,$teamRelIdRelation)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'ban_pick' => '',
            'static' => '',
            'players' => ''
        ];
        //对局base
        $battleInfoConfig = [
            'rel_battle_id' => 'matchorder',
            'rel_match_id' => 'matchid',
            'begin_at' => function ($params) {
                return date('Y-m-d H:i:s', $params['matchcreation']);  //对局开始时间
            },
            'end_at' => function ($params) {
                return date('Y-m-d H:i:s', ($params['matchcreation'] + $params['matchduration']));
            },
            'duration' => function($params){
                 return (string)$params['matchduration'];  //对局持续时间
            },
            'order' =>'matchorder', //第几场
            'winner' => 'winner',  //胜利的队伍rel ID
        ];
        $battleBaseAtt  = Mapping::transformation($battleInfoConfig, $restBattleInfo);
        //base
        $battleBaseAtt['map'] = 1;  //数据源没该数据，取值为metadata_lol_map表的召唤师峡谷
        $battleBaseAtt['is_battle_detailed'] = 1;
        $battleBaseAtt['is_default_advantage'] = 2;
        $battleBaseAtt['is_draw'] = 2;
        $battleBaseAtt['is_forfeit'] = 2;
        $battleBaseAtt['flag'] = 1;
        $battleBaseAtt['deleted'] = 2;
        $battleBaseAtt['deleted_at'] = null;
        $battleBaseAtt['game'] = 2;
        //为了下面计算用
        $duration = (Int)$battleBaseAtt['duration'];
        //detail
        $battleDetail = [];
        $battleDetail['is_confirmed'] = 1;
        $battleDetail['is_pause'] = 2;
        $battleDetail['is_live'] = 1;
        //设置battle的ban_pick
        $banPickConfig = [
            'ban_pick' =>function($params){
                $ban_pick1 = [];
                $ban_pick2 = [];
                $ban_pick3 = [];
                $ban_pick4 = [];
                if($params['stats']['bans']){  //ban_pick英雄
                    if(isset($params['stats']['bans']['oneteam']) && $params['stats']['bans']['oneteam']){
                        foreach ($params['stats']['bans']['oneteam'] as $key =>$team){
                            $ban_pick1[$key]['type'] = 1;
                            $ban_pick1[$key]['hero'] = $team['chherokey'];
                            $ban_pick1[$key]['team'] = $team['teamid'];
                        }
                    }
                    if(isset($params['stats']['bans']['twoteam']) && $params['stats']['bans']['twoteam']){
                        foreach ($params['stats']['bans']['twoteam'] as $key=>$team){
                            $ban_pick2[$key]['type'] = 1;
                            $ban_pick2[$key]['hero'] = $team['chherokey'];
                            $ban_pick2[$key]['team'] = $team['teamid'];
                        }
                    }
                    if(isset($params['stats']['picks']['oneteam']) && $params['stats']['picks']['oneteam']){
                        foreach ($params['stats']['picks']['oneteam'] as $key=>$team){
                            $ban_pick3[$key]['type'] = 2;
                            $ban_pick3[$key]['hero'] = $team['cpherokey'];
                            $ban_pick3[$key]['team'] = $team['teamid'];
                        }
                    }
                    if(isset($params['stats']['picks']['twoteam']) && $params['stats']['picks']['twoteam']){
                        foreach ($params['stats']['picks']['twoteam'] as $key=>$team){
                            $ban_pick4[$key]['type'] = 2;
                            $ban_pick4[$key]['hero'] = $team['cpherokey'];
                            $ban_pick4[$key]['team'] = $team['teamid'];
                        }
                    }
                }
                return array_merge($ban_pick1,$ban_pick2,$ban_pick3,$ban_pick4);
            }
        ];
        $battle_ban_pick = Mapping::transformation($banPickConfig, $restBattleInfo);
        $battleDetail['ban_pick'] = $battle_ban_pick['ban_pick'];
        $battleInfo['detail'] = $battleDetail;
        //设置static
        $teamStaticColor = $teamOrderArray = $teamFactionArray = $teamStaticWinnnerArray = [];
        foreach ($restBattleInfo['teamstats'] as $keyOrigin=>$value){
            $teamInfo = [];
            if ($keyOrigin == 'oneteam'){
                $rel_team_id = $restBattleInfo['blue'];
                $team_faction = 'blue';
            }else{
                $rel_team_id = $restBattleInfo['red'];
                $team_faction = 'red';
            }
            $team_id = @$teamRelIdRelation[$rel_team_id];//self::getMainIdByRelIdentityId('team',$rel_team_id,4);
            $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
            $teamOrderArray[$rel_team_id] = $team_order;
            $teamInfo['rel_team_id'] = $rel_team_id;
            $teamInfo['rel_identity_id'] = $rel_team_id;
            $teamInfo['team_id'] = (Int)$team_id;
            $teamInfo['order'] = $team_order;
            $teamInfo['kills'] = $value['kills'];
            $teamInfo['deaths'] = 0;
            $teamInfo['assists'] = 0;
            $teamInfo['wards_purchased'] = null;
            $teamInfo['wards_placed'] = 0;
            $teamInfo['wards_kills'] = 0;
            //$teamInfo['experience'] = 0;
            $teamInfo['gold'] = $value['gold'];
            $teamInfo['faction'] = $team_faction;
            $teamInfo['baron_nashor_kills'] = $value['baronkills'];
            $teamInfo['dragon_kills'] = $value['dragonkills'];
            $teamInfo['turret_kills'] = $value['towerkills'];
            $teamInfo['inhibitor_kills'] = $value['inhibitorKills'];
            $teamInfo['rift_herald_kills'] = null;
            $teamInfo['dragon_kills_detail'] = null;
            //组合需要的数据
            $teamStaticColor[$team_faction] = $team_order;
            $teamStaticWinnnerArray[$rel_team_id] = $team_order;
            $teamFactionArray[$team_faction] = $teamInfo;
        }
        if (!empty($battleBaseAtt['winner'])){
            $battleBaseAtt['winner'] = $teamStaticWinnnerArray[$battleBaseAtt['winner']];
            $battleBaseAtt['status'] = 3;
        }else{
            $battleBaseAtt['winner'] = null;
            $battleBaseAtt['status'] = 2;
        }
        // 获取battle详情
        $battleInfo['base'] = $battleBaseAtt;
        //队伍的经济差
        $team_gold_diff = $teamFactionArray['blue']['gold'] - $teamFactionArray['red']['gold'];
        $teamFactionArray['blue']['gold_diff'] = $team_gold_diff;
        $teamFactionArray['red']['gold_diff'] = -$team_gold_diff;
        //设置选手信息
        $blue_team_players = $red_team_players = [];
        $oneTeamPlayers = isset($restBattleInfo['stats']['players']['oneteam']) && $restBattleInfo['stats']['players']['oneteam'] ? $restBattleInfo['stats']['players']['oneteam'] : null;
        if ($oneTeamPlayers){
            $blue_team_players = self::conversionPlayers($oneTeamPlayers,@$teamFactionArray['blue'],$duration,$matchInfo);
        }
        $twoTeamPlayers = isset($restBattleInfo['stats']['players']['twoteam']) && $restBattleInfo['stats']['players']['twoteam'] ? $restBattleInfo['stats']['players']['twoteam'] : null;
        if ($twoTeamPlayers){
            $red_team_players = self::conversionPlayers($twoTeamPlayers,@$teamFactionArray['red'],$duration,$matchInfo);
        }
        //players
        $battleInfo['players'] = array_merge($blue_team_players['players'],$red_team_players['players']);
        //static
        $battleInfo['static'] = array_merge($blue_team_players['teams_info'],$red_team_players['teams_info']);

        return $battleInfo;
    }

    //转化
    private static function addBindingInfo($redis,$infoFormat,$matchInfo)
    {
        //玩家英雄
        $redisChampionNameIdRelationArray = $redis->hGetAll("lol:champions:name_relation_id:6");
        $champion_unknown_redis = $redis->get('lol:champions:list:unknown');
        $champion_unknown_info = @json_decode($champion_unknown_redis,true);
        $champion_unknown_id = @$champion_unknown_info['id'];
        //装备道具列表
        $itemsInfoList = $redis->hGetAll("lol:items:list:6");
        $LolItemsRelationList = $redis->hGetAll('lol:items:relation:6');
        $item_unknown_redis = $redis->get('lol:items:list:unknown');
        $item_unknown_info = @json_decode($item_unknown_redis,true);
        $item_unknown_id = @$item_unknown_info['id'];
        //召唤师技能
        $summonerSpellNameRelationIdList = $redis->hGetAll('lol:summoner_spell:relation:6');
        $summoner_spell_unknown_info_redis = $redis->get("lol:summoner_spell:list:unknown");
        $summoner_spell_unknown_info = @json_decode($summoner_spell_unknown_info_redis,true);
        $summoner_spell_unknown_id = @$summoner_spell_unknown_info['id'];
        //比赛的开始时间 begin_at
        if(isset($infoFormat['battles'][0]['base'])){
            $infoFormat['match']['begin_at'] = $infoFormat['battles'][0]['base']['begin_at']; //取第一场battle的开始时间作为比赛实际开始时间
        }
        if(count($infoFormat['battles']) > 0){
            $end_battle = end($infoFormat['battles']);
            $infoFormat['match']['end_at'] = $end_battle['base']['end_at']; //取第一场battle的开始时间作为比赛实际开始时间
        }
        $infoFormat['match']['status'] = 2;
        //判断是不是有比赛的winnner
        if ($infoFormat['match']['winner']){
            $infoFormat['match']['status'] = 3;
        }
        //整理比分
        if($matchInfo['team_1_id'] == $infoFormat['match']['team1_id']){
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_1_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_2_score_old'];
        }
        if($matchInfo['team_1_id'] == $infoFormat['match']['team2_id']){
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_2_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_1_score_old'];
        }
        //unset一些信息
        unset($infoFormat['match']['team_1_score_old']);
        unset($infoFormat['match']['team_2_score_old']);
        unset($infoFormat['match']['rel_team1_id']);
        unset($infoFormat['match']['rel_team2_id']);
        //转换battle
        foreach($infoFormat['battles'] as &$battle){
            $battle_winner = null;
            if (isset($battle['base']['winner']) && $battle['base']['winner']){
                $battle_winner = $battle['base']['winner'];
            }
            //记录战队
            $team_array = [];
            foreach($battle['static'] as &$static) {
                if ($battle_winner == $static['order']) {
                    $static['score'] = 1;
                } else {
                    $static['score'] = 0;
                }
                $team_array[$static['rel_team_id']] = @$static['team_id'];
            }
            //detail
            foreach($battle['detail']['ban_pick'] as $key => &$val){
                $hero_id = (Int)@$redisChampionNameIdRelationArray[$val['hero']];
                if (empty($hero_id)){
                    $hero_id = $champion_unknown_id;
                }
                $val['hero'] = $hero_id;
                $val['order'] = $key + 1;
                $val['team'] = @$team_array[$val['team']];
            }
            // player  TODO
            foreach($battle['players'] as $p_key => &$player){
                $player['order'] = $p_key + 1;
                //items
                $player['items'] = self::conversionPlayerItemsRestApi($player['items'],$LolItemsRelationList,$itemsInfoList,$item_unknown_id);
                //summoner_spells
                $player['summoner_spells'] = self::conversionPlayerSummonerSpellsRestApi($player['summoner_spells'],$summonerSpellNameRelationIdList,$summoner_spell_unknown_id);
                //champion
                $player['champion'] = (Int)@$redisChampionNameIdRelationArray[$player['champion_name']];
                //获取平台的玩家ID
                $player['player_id'] = (Int)@self::getMainIdByRelIdentityId('player', $player['player_id'], 4);
                if ($player['player_id']){
                    //获取选手的昵称
                    $player_detail = Player::find()->select('nick_name')->where(['id'=>$player['player_id']])->asArray()->one();
                    $player['nick_name'] = @$player_detail['nick_name'];
                }
                $player['seed'] = (Int)$player['seed'];
                $player['role'] = Common::getLolPositionByString('orange',$player['seed']);
                $player['lane'] = Common::getLolLaneId('orange',@$player['role']);
                //role跟码表的主键id正好对应上(未转，不影响)
//                if(isset($player['runes']) && $player['runes']){
//                    $runesTmp = json_decode($player['runes'],true);
//                    $tmpNew=[];
//                    foreach ($runesTmp as $key=>&$val){
//                        $val['rune_id']= self::getMainIdByRelIdentityId(Consts::METADATA_TYPE_LOL_RUNE,$val['runeId'],'6');
//                        unset($val['rank']);
//                        unset($val['runeId']);
//                        if($key<=3){
//                            $tmpNew['primary_path'][] = $val;
//                        }elseif($key>3 && $key<6){
//                            $tmpNew['secondary_path'][] = $val;
//                        }
//                    }
//                    $player['runes'] = json_encode($tmpNew);
//                }
                //转换格式
                $player['minion_kills'] = (string)$player['minion_kills'];
                $player['total_neutral_minion_kills'] = (string)$player['total_neutral_minion_kills'];
                $player['neutral_minion_team_jungle_kills'] = (string)$player['neutral_minion_team_jungle_kills'];
                $player['neutral_minion_enemy_jungle_kills'] = (string)$player['neutral_minion_enemy_jungle_kills'];
                $player['damage_to_champions'] = (string)$player['damage_to_champions'];
                $player['damage_to_champions_physical'] = (string)$player['damage_to_champions_physical'];
                $player['damage_to_champions_magic'] = (string)$player['damage_to_champions_magic'];
                $player['damage_to_champions_true'] = (string)$player['damage_to_champions_true'];
                $player['damage_taken'] = (string)$player['damage_taken'];
                $player['damage_taken_physical'] = (string)$player['damage_taken_physical'];
                $player['damage_taken_magic'] = (string)$player['damage_taken_magic'];
                $player['damage_taken_true'] = (string)$player['damage_taken_true'];
                $player['total_heal'] = (string)$player['total_heal'];
                $player['total_crowd_control_time'] = (string)$player['total_crowd_control_time'];
            }
        }
        return $infoFormat;
    }

    //处理玩家信息
    private static function conversionPlayers($teamPlayers,$teamFactionArray,$duration,$matchInfo){
        $player_damage_to_champions_all1 = 0;
        $player_damage_taken_all1 = 0;
        $players = [];
        foreach ($teamPlayers as $player_item){
            $playerConfig = [
                'rel_identity_id' => "playerid",
                'player_id' => "playerid",
                "rel_nick_name" => 'playername',//选手 playername
                //team_order
                //team_id
                "order"=> "meta",
                "seed" => "meta",
                "role" => "meta",  //位置(上，野，辅)需转换
                "game" => 2,//游戏
                "champion_name" => "cpherokey",//英雄  待转换
                "level" => "endlevel",//等级
                "kills" => "kills",//击杀
                "double_kill" => "doubleKills",//双杀
                "triple_kill" => "tripleKills",//三杀
                "quadra_kill" => "quadraKills",//四杀
                "penta_kill" => "pentaKills",//五杀
                "largest_multi_kill" => "largestMultiKill",//最大多杀
                "largest_killing_spree" => "largestKillingSpree",//最大连杀
                "deaths" => "deaths",//死亡
                "assists" => "assists",//助攻
                "cs" =>"lasthit", //补兵数
                "minion_kills" =>"minionsKilled",
                'total_neutral_minion_kills' => 'neutralMinionsKilled',
                'neutral_minion_team_jungle_kills' => 'neutralMinionsKilledTeamJungle',
                'neutral_minion_enemy_jungle_kills' => 'neutralMinionsKilledEnemyJungle',
                'gold_earned' => 'gold',
                'gold_spent' => 'goldSpent',
                //                "gold_remaining" => "",//剩余金币
                //                "gpm" => "",//分均金钱
                //                "gold_earned_percent" => "",//经济占比
                //                "experience" => "",//总经验
                //                "xpm" => "",//分均经验
                'damage_to_champions' => 'totalDamageDealtToChampions',
                'damage_to_champions_physical' => 'physicalDamageDealtToChampions',
                'damage_to_champions_magic' => 'magicDamageDealtToChampions',
                'damage_to_champions_true' => 'trueDamageDealtToChampions',
                //'dpm_to_champions'
                //'damage_percent_to_champions'
                'damage_taken' => 'totalDamageTaken',
                'damage_taken_physical' => 'physicalDamageTaken',
                'damage_taken_magic' => 'magicDamageTaken',
                'damage_taken_true' => 'trueDamageTaken',
                //'dtpm'
                //'damage_taken_percent'
                //'damage_conversion_rate'
                'total_heal' => 'totalHeal',
                'total_crowd_control_time' => 'totalTimeCrowdControlDealt',
                //'wards_purchased'
                'wards_kills' => 'wardsKilled',
                'wards_placed' => 'wardsPlaced',
                "runes" => "runes", //符文 需转换
                "items" => "itemcache",
                'summoner_spells' => function ($params) {
                    $summoner[] = $params['skill1id'];
                    $summoner[] = $params['skill2id'];
                    return $summoner;
                },  //待转换
            ];
            $player = Mapping::transformation($playerConfig, $player_item);
            //首先获取 team_id 和order
            $player['team_id'] = (Int)@$teamFactionArray['team_id'];
            $player['team_order'] = (Int)@$teamFactionArray['order'];
            //计算
            $deaths = $player['deaths'] ? $player['deaths'] : 1;
            $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
            //队伍击杀
            $teamkill = $teamFactionArray['kills'];
            if ($teamkill) {
                $player['participation'] = (String)round((($player['kills'] + $player['assists']) / $teamkill),4);
            } else {
                $player['participation'] = null;
            }
            $team_gold = isset($teamFactionArray['gold']) ? $teamFactionArray['gold'] : 1;
            if (isset($player['gold_earned']) && $player['gold_earned']){
                $player['gold_earned_percent'] = (String)round(($player['gold_earned'] / $team_gold),4);
                $player['damage_conversion_rate'] = (String)round(($player['damage_to_champions'] / $player['gold_earned']),4);
            }else{
                $player['gold_earned_percent'] = null;
                $player['damage_conversion_rate'] = null;
            }
            if (isset($player['gold_spent']) && isset($player['gold_earned'])) {
                $player['gold_remaining'] = $player['gold_earned'] - $player['gold_spent'];
            }
            if ($duration) {
                $player['gpm'] = (String)round(($player['gold_earned'] / ($duration / 60)),2);
            } else {
                $player['gpm'] = null;
            }
            if ($duration) {
                $player['dpm_to_champions'] = (String)round(($player['damage_to_champions'] / ($duration / 60)),2);
            } else {
                $player['dpm_to_champions'] = null;
            }
            if (isset($player['damage_taken']) && $duration) {
                $player['dtpm'] = (string)round(($player['damage_taken'] / ($duration / 60)), 2);
            }else{
                $player['dtpm'] = null;
            }
            if (isset($player['cs']) && $duration) {
                $player['cspm'] = (String)round(($player['cs'] / ($duration / 60)), 2);
            } else {
                $player['cspm'] = null;
            }
            //组合其他的
            $player['rel_team_id'] = $teamFactionArray['rel_team_id'];
            $player['game'] = 2;
            $player['match'] = (string)$matchInfo['id'];
            $player['faction'] = $teamFactionArray['faction'];
            //总结果
            $players[] = $player;
            //统计
            $teamFactionArray['wards_placed'] += (Int)$player['wards_placed'];
            $teamFactionArray['wards_kills'] += (Int)$player['wards_kills'];
            $teamFactionArray['deaths'] += (Int)$player['deaths'];
            $teamFactionArray['assists'] += (Int)$player['assists'];
            //统计选手
            $player_damage_to_champions_all1 += $player['damage_to_champions'];
            $player_damage_taken_all1 += $player['damage_taken'];
        }
        $player_damage_to_champions_all = $player_damage_to_champions_all1 == 0 ? 1 : $player_damage_to_champions_all1;
        $player_damage_taken_all = $player_damage_taken_all1 == 0 ? 1 : $player_damage_taken_all1;
        foreach ($players as $key => $item) {
            $players[$key]['damage_percent_to_champions'] = (string)round(($item['damage_to_champions'] / $player_damage_to_champions_all),4);
            $players[$key]['damage_taken_percent'] = (string)round(($item['damage_taken'] / $player_damage_taken_all),4);
        }
        $teams_info[] = $teamFactionArray;
        $result = [
            'players' => $players,
            'teams_info' => $teams_info
        ];
        return $result;
    }
    //转化装备
    private static function conversionPlayerItemsRestApi($playerItems,$LolItemsRelationList,$lolItems,$item_unknown_id){
        //redis初始化
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        foreach ($playerItems as $key_item => $value_item){
            if ($value_item){
                $itemInfoId = $LolItemsRelationList[$value_item];
                if($itemInfoId){
                    $itemInfoId_val = (Int)$itemInfoId;
                    $itemInfoResRedis = @$lolItems[$itemInfoId_val];
                    if ($itemInfoResRedis){
                        $itemInfoRes = @json_decode($itemInfoResRedis,true);
                        if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                            $playerOrnamentsInfo[$key_item] = $itemInfoId_val;
                        }else{//道具
                            $playerItemsInfo[$key_item]['total_cost'] = @$itemInfoRes['total_cost'];
                            $playerItemsInfo[$key_item]['item_id'] = $itemInfoId_val;
                        }
                    }else{//道具
                        $playerItemsInfo[$key_item]['total_cost'] = 1;
                        $playerItemsInfo[$key_item]['item_id'] = (Int)$item_unknown_id;
                    }
                }else{
                    $playerItemsInfo[$key_item]['total_cost'] = 1;
                    $playerItemsInfo[$key_item]['item_id'] = (Int)$item_unknown_id;
                }
            }
        }
        $total_cost_array = array_column($playerItemsInfo,'total_cost');
        array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
        $playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
        //判断是不是够6个
        $playerItemsInfoNum = count($playerItemsInfoRet);
        if ($playerItemsInfoNum < 6){
            $chaNum = 6 - $playerItemsInfoNum;
            for ($i=0;$i<$chaNum;$i++){
                $playerItemsNull[] = null;
            }
        }
        $playerItemsInfoResult = array_merge($playerItemsInfoRet,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        //循环添加 purchase_time购买时间点 cooldown装备使用CD
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsInfo = null;
            if ($val_res){
                $ItemsInfo = [
                    'item_id' => $val_res,
                    'purchase_time' => null,
                    'cooldown' => null
                ];
            }
            $playerItemsInfoResultEnd[] = $ItemsInfo;
        }
        return json_encode($playerItemsInfoResultEnd,320);
    }
    //转化召唤师技能
    private static function conversionPlayerSummonerSpellsRestApi($player_summoner_spells,$summonerSpellNameRelationIdList,$summoner_spell_unknown_id){
        $player_spell = [];
        foreach ($player_summoner_spells as $key => $val){
            $spell_id = @$summonerSpellNameRelationIdList[$val];
            if (empty($spell_id)){
                $spell_id = $summoner_spell_unknown_id;
            }
            $player_spell_item = [
                'spell_id' => $spell_id,
                'cooldown' => null
            ];
            $player_spell[] = $player_spell_item;
        }
        return json_encode($player_spell,320);
    }
    //
    private static function setMatchBattleRelation($redis,$matchId,$battleId,$battle){
        //插入关联关系
        $rel_matchId = @$battle['base']['rel_match_id'];
        $rel_battleId = @$battle['base']['rel_battle_id'];
        $battle_order = $battle['base']['order'];
        $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battle_order])->one();
        if (!$matchBattleRelation){
            //首先插入关系表
            $matchBattleRelation = new MatchBattleRelation();
            $matchBattleRelation->setAttributes([
                'match_id' => (int)$matchId,
                'order' => $battle_order,
                'rel_match_id' => (string)$rel_matchId,
                'rel_battle_id' => (string)$rel_battleId
            ]);
            if (!$matchBattleRelation->save()) {
                throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
            }
        }
        //设置redis 关系
        $redis_name = 'match_battle_info:'.$matchId.":match_battles_rest_api";
        $redis->hSet($redis_name,$rel_battleId,$battleId.'-'.$battle_order);
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}