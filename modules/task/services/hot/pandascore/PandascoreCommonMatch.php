<?php
/**
 *
 */

namespace app\modules\task\services\hot\pandascore;


use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\services\battle\BattleServiceBase;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\abios\AbiosHotBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class PandascoreCommonMatch extends PandascoreHotBase implements HotMatchInterface
{
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $hotInfo = json_decode($taskInfo['params'], true);
        $hotId = $hotInfo['id'];
        $matchId=$hotInfo['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,3);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        $matchInfo=Match::find()->where(['id'=>$matchId])->one();
        // 获取curl数据
        $curlInfo=self::getCurlInfo($relIdentityId);
        if (empty($curlInfo['match'])){
            return false;
        }
        if ($curlInfo['match']['status'] == 'finished') {
            $matchEndStatus = true;
        }else{
            $matchEndStatus = false;
        }
        // 转化，为了保证formatInfo的独立性，我们尽可能少在里面用id，多用（主队，客队，player_id,这样的id，尽量不要用teamid）
        $formatInfo=self::convert($curlInfo,$matchInfo,$refreshType);

        $extInfo=[
            'time_begin'=>'',
            'time_end'=>'',
            'task_id'=>$taskInfo['id'],
            'match_id'=>$matchId,
        ];
        // 添加绑定信息，转义,主要出现在battle的详情中
        self::addHotLog($hotId, $curlInfo,$formatInfo,$extInfo);

        // 换成自己的绑定ID

        // 刷进数据表
//        self::refresh($matchId,$formatInfo);
        self::refresh($matchId,$formatInfo,$refreshType);
        // 更新hotdata状态
        self::refreshHotDataStatusByMatchInfo($hotId,$matchEndStatus);
    }

    private static function convert($curlInfo,$matchInfo,$refreshType)
    {
        $match = self::getMatchData($curlInfo['match'],$matchInfo);
        return [
            'match'=>$match,
        ];
    }

    private static function refresh($matchId, $info, $refreshType)
    {
        self::setMatch($matchId, $info['match']);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['order'],
            ]);
            $battleBase->save();
        }
        $battleId=$battleBase['id'];

        $battle['base']['match'] = $matchId;
        $battle['base']['flag'] = 1;
        BattleService::setBattleInfo($battleId,'csgo',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'csgo',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['rounds'],BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['events'],BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['round_players'],BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    private static function getMasterIdFromIdentityId($resourceType,$relIdentityId)
    {
        return [];
    }

    private static function getCurlInfo($matchId)
    {
        $match = self::getMatch($matchId);
        return [
            'match' => $match,
        ];
    }
    // 默认领先创建一个battle
    private static function getWinnerBattleData($defaultAdvantage,$matchInfo,$teams)
    {
        $battle = BattleServiceBase::getBattleByMatchIdOnWs($matchInfo->id, 1);
        if ($battle && $battle['is_default_advantage'] == 1 && $battle['winner']) {
            //已经有默认领先则使用已经创建好的
        }else{
            $map    = MetadataCsgoMap::find()->where(['name' => 'Default'])->asArray()->one();
            $battle = [
//                'rel_battle_id' => ,
                'begin_at'             => '',
                'game'                 => 1,
                'end_at'               => '',
                'is_battle_detailed'   => 2,
                'is_default_advantage' => 1,
                'order'                => 1,
                'status'               => 3,
                'duration'             => "",
                'map'                  => !empty($map) ? $map['id'] : null,
                'winner'               => $defaultAdvantage,
                'is_draw'              => 2,
                'is_forfeit'           => 2,
            ];

        }

        $battlesCo['order'] = 1;
        $battlesCo['base'] = $battle;
        // team 数据
        $battlesCo['static'] = self::getWinnerStaticData($teams,$defaultAdvantage,$matchInfo);
        // round 和 side 数据
        $battlesCo['rounds'] = [];
        // players 数据
        $battlesCo['players'] = [];
        // event-csgo数据
        $battlesCo['events'] = [];
        // ext-csgo数据
        $battlesCo['detail'] = [
            'is_confirmed' => 1
        ];
        // round下选手数据
        $battlesCo['round_players'] = [];
        return $battlesCo;
    }
    private static function getWinnerStaticData($teams,$defaultAdvantage,$matchInfo)
    {
        $teamsCo = [];
        foreach ($teams as $k=>$v){
            $order = $v['id'] ? self::getTeamOrderByTeamRelId($v['id'], $matchInfo) : null;
            $teamId = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3');
            $teamsCo[$k]['order'] = $order;
            $teamsCo[$k]['team_id'] = $teamId;
            $teamsCo[$k]['rel_identity_id'] = $v['id'];
            $teamsCo[$k]['identity_id'] = $v['name'];
            if($defaultAdvantage == $order){
                $teamsCo[$k]['score'] = 1;
            }else{
                $teamsCo[$k]['score'] = 0;
            }
        }
        return $teamsCo;
    }
    // 处理team数据结构
    private static function getTeamsData($teams,$round,$rounds,$battlesScore,$matchInfo,$battlePlayers=[])
    {
        $teamScores=[
            1=>[
                '1st_half_score'=>0,
                '2nd_half_score'=>0,
                'ot_score'=>0,
            ],
            2=>[
                '1st_half_score'=>0,
                '2nd_half_score'=>0,
                'ot_score'=>0,
            ]
        ];
        $liveRoundOrder=0;
        foreach($rounds as $key=>$val){
            if('有效'){
                $liveRoundOrder=$liveRoundOrder+1;
            }
            if($liveRoundOrder<=15){
                $scoreKey='1st_half_score';
            }elseif($liveRoundOrder<=30){
                $scoreKey='2nd_half_score';
            }else{
                $scoreKey='ot_score';
            }
            $teamOrderId=$val['winner_team']?self::getTeamOrderByTeamRelId($val['winner_team'],$matchInfo):null;
            $teamScores[$teamOrderId][$scoreKey]=$teamScores[$teamOrderId][$scoreKey]+1;
        }

        $teamsData = [];
        foreach ($teams as $k=>$v){
//            $tmpTeamScore=$teamScores[$k+1];
//            $tmpVal=array_merge($v,$tmpTeamScore);
//            $teamsData[$k]=$teamScores[$k+1];
            foreach ($battlesScore as $kk=>$vv){
                if($v['id'] == $vv['team_id']){
//                    $teamsData[$k]['order'] = PandascoreHotBase::getTeamOrderByTeamRelId($v['id'],$matchInfo);
                    $teamOrder = $v['id']?self::getTeamOrderByTeamRelId($v['id'],$matchInfo):null;
                    $teamsData[$k]['order'] = $teamOrder;
                    $teamsData[$k]['score'] = $vv['score'];
                    $teamsData[$k]['team_id'] = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3',1);
                    $teamsData[$k]['rel_identity_id'] = $v['id'];
                    $teamsData[$k]['identity_id'] = $v['name'];
                    $teamsData[$k]=array_merge($teamScores[$teamOrder],$teamsData[$k]);
                }else{
//                    $teamsData[$k]['order'] = PandascoreHotBase::getTeamOrderByTeamRelId($v['id'],$matchInfo);
                    $teamOrder = $v['id']?self::getTeamOrderByTeamRelId($v['id'],$matchInfo):null;
                    $teamsData[$k]['order'] = $teamOrder;
                    $teamsData[$k]['team_id'] = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3',1);
                    $teamsData[$k]['rel_identity_id'] = $v['id'];
                    $teamsData[$k]['identity_id'] = $v['name'];
                    $teamsData[$k]=array_merge($teamScores[$teamOrder],$teamsData[$k]);
                }
                if(isset($round['ct']) && $round['ct']==$v['id']){
                    $teamsData[$k]['starting_faction_side'] = 'ct';
                }
                if(isset($round['terrorists']) && $round['terrorists']==$v['id']){
                    $teamsData[$k]['starting_faction_side'] = 'terrorist';
                }
            }
            //battle players 统计
            if (!empty($battlePlayers)) {
                $countPlayer = 0;
                foreach ($battlePlayers as $kPlayer => $vPlayer) {
                    if ($vPlayer['team']['id'] == $v['id']) {
                        $teamsData[$k]['kills']            += $vPlayer['kills'];
                        $teamsData[$k]['assists']          += $vPlayer['assists'];
                        $teamsData[$k]['deaths']           += $vPlayer['deaths'];
                        $teamsData[$k]['first_kills_diff'] += $vPlayer['first_kills_diff'];
                        $teamsData[$k]['flash_assists']    += $vPlayer['flash_assists'];
                        $teamsData[$k]['headshot_kills']   += $vPlayer['headshots'];
                        $teamsData[$k]['kd_diff']          += $vPlayer['k_d_diff'];
                        $teamsData[$k]['adr']              += $vPlayer['adr'];
                        $teamsData[$k]['kast']             += $vPlayer['kast'];
                        $teamsData[$k]['rating']           += $vPlayer['rating'];
                        $countPlayer++;
                    }
                }
            }
            $teamsData[$k]['adr']    = (string)($teamsData[$k]['adr'] / $countPlayer);
            $teamsData[$k]['kast']   = (string)($teamsData[$k]['kast'] / $countPlayer);
            $teamsData[$k]['rating'] = (string)($teamsData[$k]['rating'] / $countPlayer);

        }

        return $teamsData;
    }
    // 计算上半场/下半场/加时比分
    private static function getCountSource($data)
    {
        $count = array_count_values(array_column($data,'winner_side'));
        $word = ['ct','terrorists'];
        $wordCount = [];
        foreach ($word as $k=>$v){
            $sl = isset($count[$v])? $count[$v] : 0;
            $wordCount[] = [
                'winner_side' => $v,
                'sl' => $sl,
            ];
        }
        return $wordCount;
    }

    /**
     * 处理match数据结构
     * @param $matchData
     * @param $matchInfo
     * @return mixed
     * @throws BusinessException
     */
    private static function getMatchData($matchData, $matchInfo)
    {
        $match['begin_at']           = isset($matchData['begin_at']) && $matchData['begin_at'] ? date('Y-m-d H:i:s', strtotime($matchData['begin_at'])) : "";
        $match['end_at']             = isset($matchData['end_at']) && $matchData['end_at'] ? date('Y-m-d H:i:s', strtotime($matchData['end_at'])) : "";
        $match['is_battle_detailed'] = 2;
        $match['is_forfeit']         = $matchData['forfeit'] == true ? 1 : 2;
        $match['is_draw']            = $matchData['draw'] == true ? 1 : 2;
        $match['default_advantage']  = $matchData['game_advantage'] ? self::getTeamOrderByTeamRelId($matchData['game_advantage'], $matchInfo) : null;
        $match['winner']             = $matchData['winner_id'] ? self::getTeamOrderByTeamRelId($matchData['winner_id'], $matchInfo) : null;
        $match['status']             = Common::getPandaScoreMatchStatus($matchData['status']);
        foreach ($matchData['results'] as $key => $val) {
            $teamOrder = $val['team_id'] ? self::getTeamOrderByTeamRelId($val['team_id'], $matchInfo) : null;
            if ($teamOrder == 2) {
                $match['team_2_score'] = $val['score'];
            } else {
                $match['team_1_score'] = $val['score'];
            }
        }
        return $match;
    }


    private static function getMatch($matchId)
    {
        $url = '/matches';
        $params = [
            'filter[id]' => $matchId,
        ];
        $jsonInfo = self::getRestInfo($url, $params);
        $info = json_decode($jsonInfo, true);
        // 获取当前Match的数据
        return $info[0];
    }
}