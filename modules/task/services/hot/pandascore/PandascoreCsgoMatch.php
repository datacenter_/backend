<?php
/**
 *
 */

namespace app\modules\task\services\hot\pandascore;


use app\modules\common\services\Consts;
use app\modules\common\services\TaskService;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\services\battle\BattleServiceBase;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\abios\AbiosHotBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\db\Query;

class PandascoreCsgoMatch extends PandascoreHotBase implements HotMatchInterface
{
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $timeStart=date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $hotId = $info['id'];
//        $hotId = 226029;
        $hotInfo = self::getHotInfo($hotId);
        $matchId=$hotInfo['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,3);
        $matchInfo=Match::find()->where(['id'=>$matchId])->one();
        if (!$relIdentityId){
            return false;
        }
        // 获取curl数据
        $curlInfo=self::getCurlInfo($relIdentityId);
        if (empty($curlInfo['match'] ||  count($curlInfo['battles']) == 0)){
            return false;
        }
        $endBattle = end($curlInfo['battles']);
        if ($curlInfo['match']['status'] == 'finished' && $endBattle['complete']) {
            $matchEndStatus = true;
        }else{
            $matchEndStatus = false;
        }
        // 转化，为了保证formatInfo的独立性，我们尽可能少在里面用id，多用（主队，客队，player_id,这样的id，尽量不要用teamid）
        $formatInfo=self::convert($curlInfo,$matchInfo,$refreshType);

        $extInfo=[
            'time_begin'=>'',
            'time_end'=>'',
            'task_id'=>$taskInfo['id'],
            'match_id'=>$matchId,
        ];
        // 添加绑定信息，转义,主要出现在battle的详情中
        self::addHotLog($hotId, $curlInfo,$formatInfo,$extInfo);

        // 换成自己的绑定ID

        // 刷进数据表
//        self::refresh($matchId,$formatInfo);
        self::refresh($matchId,$formatInfo,$refreshType);
        // 更新hotdata状态
        self::refreshHotDataStatusByMatchInfo($hotId,$matchEndStatus);
    }

    private static function convert($curlInfo,$matchInfo,$refreshType)
    {
        $match = self::getMatchData($curlInfo['match'],$matchInfo);
        $battles = self::getBattleData($curlInfo['battles'],$matchInfo,$match['default_advantage'],$refreshType);
        return [
            'match'=>$match,
            'battles'=>$battles
        ];
    }

    private static function refresh($matchId, $info, $refreshType)
    {
        $battle_is_forfeit_order_id = null;
        //判断是不是有默认领先
        if (isset($info['match']['default_advantage']) && $info['match']['default_advantage']) {
            $battle = BattleServiceBase::getBattleByMatchIdOnWs($matchId, 1);
            if (empty($battle)) {
                HotBase::createMatchAdvantageBattle($matchId, $info['match']['default_advantage'], 0, 1, $type = 'rest_api', 3);
            }
        }
        foreach ($info['battles'] as $key => $battle) {
            if ($battle['order']&&$battle['order'] == $battle['base']['order']) {
                // 根据battleId刷新对应的battle信息
                self::setBattle($matchId, $battle);
            }
        }
        //更新match层信息
//        if (!empty( $info['match'])&&array_key_exists('map_info',$info['match'])){
//            unset($info['match']['map_info']);
//        }
        self::setMatch($matchId, $info['match']);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        $have_battle = false;
        // todo 数据库的battle数量>返回的数量
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['order'],
            ]);
            $battleBase->save();
            $have_battle = true;
        }
        $battleId=$battleBase['id'];

        if ($have_battle){
            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleId,'match'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');
        }

        $battle['base']['match'] = $matchId;
        $battle['base']['flag'] = 1;
        BattleService::setBattleInfo($battleId,'csgo',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'csgo',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['rounds'],BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['events'],BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['round_players'],BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    private static function getMasterIdFromIdentityId($resourceType,$relIdentityId)
    {
        return [];
    }

    private static function getCurlInfo($matchId)
    {
        $match = self::getMatch($matchId);
        $battles = [];
        if(isset($match['games'])){
            foreach ($match['games'] as $k => $battleVal) {
                if ($battleVal['detailed_stats'] && $battleVal['begin_at'] && $match['detailed_stats']) {
                    $battle    = self::getBattle($battleVal['id']);
                    $battles[] = $battle;
                }
            }
        }
        return [
            'match' => $match,
            'battles' => $battles,
        ];
    }
    // 默认领先创建一个battle
    private static function getWinnerBattleData($defaultAdvantage,$matchInfo,$teams)
    {
        $battle = BattleServiceBase::getBattleByMatchIdOnWs($matchInfo->id, 1);
        if ($battle && $battle['is_default_advantage'] == 1 && $battle['winner']) {
            //已经有默认领先则使用已经创建好的
        }else{
            $map    = MetadataCsgoMap::find()->where(['name' => 'Default'])->asArray()->one();
            $battle = [
//                'rel_battle_id' => ,
                'begin_at'             => '',
                'game'                 => 1,
                'end_at'               => '',
                'is_battle_detailed'   => 2,
                'is_default_advantage' => 1,
                'order'                => 1,
                'status'               => 3,
                'duration'             => "",
                'map'                  => !empty($map) ? $map['id'] : null,
                'winner'               => $defaultAdvantage,
                'is_draw'              => 2,
                'is_forfeit'           => 2,
            ];

        }

        $battlesCo['order'] = 1;
        $battlesCo['base'] = $battle;
        // team 数据
        $battlesCo['static'] = self::getWinnerStaticData($teams,$defaultAdvantage,$matchInfo);
        // round 和 side 数据
        $battlesCo['rounds'] = [];
        // players 数据
        $battlesCo['players'] = [];
        // event-csgo数据
        $battlesCo['events'] = [];
        // ext-csgo数据
        $battlesCo['detail'] = [
            'is_confirmed' => 1
        ];
        // round下选手数据
        $battlesCo['round_players'] = [];
        return $battlesCo;
    }

    /**
     * 处理battle数据结构
     * @param $BattleData
     * @param $matchInfo
     * @param $defaultAdvantage
     * @param $refreshType
     * @return array
     * @throws BusinessException
     * @throws \yii\base\Exception
     */
    private static function getBattleData($BattleData, $matchInfo, $defaultAdvantage, $refreshType)
    {
        $battles = [];
        if ($defaultAdvantage ) {
            $battlesCo = self::getWinnerBattleData($defaultAdvantage, $matchInfo, @$BattleData[0]['teams'] ?? []);
            $battles[] = $battlesCo;
        }
        foreach ($BattleData as $k => $battleRow) {
            // battle 数据
            $battle['game']        = 1;
            $battle['rel_battle_id']        = $battleRow['id'];
            $battle['begin_at']             = isset($battleRow['begin_at']) && $battleRow['begin_at'] ? date('Y-m-d H:i:s', strtotime($battleRow['begin_at'])) : "";
            $battle['end_at']               = isset($battleRow['end_at']) && $battleRow['end_at'] ? date('Y-m-d H:i:s', strtotime($battleRow['end_at'])) : "";
            $battle['is_battle_detailed']   = $battleRow['detailed_stats'] == true ? 1 : 2;
            $battle['is_default_advantage'] = 2;
            $battle['is_forfeit']           = $battleRow['forfeit'] == true ? 1 : 2;
            if ($defaultAdvantage ) {
                $battle['order']                = $battleRow['position']+1;
            }else{
                $battle['order']                = $battleRow['position'];
            }

            $battle['status']               = Common::getPandaScoreMatchStatus($battleRow['status']);
            $battle['duration']             = strval($battleRow['length']);
            $mapId                          = self::getMainIdByRelIdentityId(Consts::METADATA_TYPE_CSGO_MAP, $battleRow['map']['id'], '3', 1);
            $battle['map']                  = $mapId ?? null;
            $battle['winner']               = $battleRow['winner']['id'] ? PandascoreHotBase::getTeamOrderByTeamRelId(@$battleRow['winner']['id'], $matchInfo) : null;
            $battle['flag']                 = 1;
            $battle['deleted']              = 2;
            $battle['deleted_at']           = null;
            if ($battleRow['status'] == 'finished') {
                if (@$battleRow['rounds_score'][0]['score'] == @$battleRow['rounds_score'][1]['score']) {
                    $battle['is_draw'] = 1;
                } else {
                    $battle['is_draw'] = 2;
                }
            } else {
                $battle['is_draw'] = 2;
            }
            if ($defaultAdvantage) {
                $battlesCo['order'] = $battleRow['position'] + 1;
            } else {
                $battlesCo['order'] = $battleRow['position'];
            }
            $battlesCo['base'] = $battle;
            // team 数据
            $battlesCo['static'] = self::getTeamsData($battleRow['teams'], @$battleRow['rounds'][0], $battleRow['rounds'], $battleRow['rounds_score'], $matchInfo, $battleRow['players']);
            // round 和 side 数据
            $battlesCo['rounds'] = self::getRoundsData($battleRow['rounds'], $battleRow['id'], $matchInfo, $battlesCo['order']);
            // players 数据
            $battlesCo['players'] = self::getPlayersData($battleRow['players'], $matchInfo);
            // event-csgo数据
            $battlesCo['events'] = [];
            // ext-csgo数据
            $battlesCo['detail'] = self::getDetailData($battleRow['rounds'], $matchInfo);
            // round下选手数据
            $battlesCo['round_players'] = [];

            $battles[] = $battlesCo;
        }
        return $battles;
    }

    /**
     * 处理ext csgo数据结构
     * @param $rounds
     * @param $matchInfo
     * @return array
     * @throws BusinessException
     */
    private static function getDetailData($rounds,$matchInfo)
    {
        $detail = [];
        if (!empty($rounds)) {
            // 第1回合获胜战队与阵营
            $detail['win_round_1_side'] = @$rounds[0]['winner_side'] ?? "";
            $detail['win_round_1_team'] = @$rounds[0]['winner_team'] ? PandascoreHotBase::getTeamOrderByTeamRelId(@$rounds[0]['winner_team'], $matchInfo) : null;
            // 第16回合获胜战队与阵营
            $detail['win_round_16_side'] = @$rounds[15]['winner_side'] ?? "";
            $detail['win_round_16_team'] = @$rounds[15]['winner_team'] ? PandascoreHotBase::getTeamOrderByTeamRelId(@$rounds[15]['winner_team'], $matchInfo) : null;
            // 连续胜利5回合
            $winnerRound = [];
            foreach ($rounds as $k => $v) {
                $b['winner_team'] = $v['winner_team'];
                array_push($winnerRound, $b);
                if (!empty($winnerRound)) {
                    $arr = array_column($winnerRound, 'winner_team');//把值提取出来转成一维数组
                    $arr = array_count_values($arr);
                    if (@$arr[$v['ct']] == 5) {
                        $detail['first_to_5_rounds_wins_side'] = $v['winner_side'];
                        $detail['first_to_5_rounds_wins_team'] = @$v['ct'] ? PandascoreHotBase::getTeamOrderByTeamRelId(@$v['ct'], $matchInfo) : null;
                        break;
                    }
                    if (@$arr[$v['terrorists']] == 5) {
                        $detail['first_to_5_rounds_wins_side'] = $v['winner_side'];
                        $detail['first_to_5_rounds_wins_team'] = @$v['terrorists'] ? PandascoreHotBase::getTeamOrderByTeamRelId(@$v['terrorists'], $matchInfo) : null;
                        break;
                    }
                }
            }
            $detail['live_rounds']  = json_encode(array_column($rounds, 'round'));
            $detail['is_confirmed'] = 1;
        }
        return $detail;
    }
    // 处理players数据结构
    private static function getPlayersData($players,$matchInfo)
    {
        $plarersCo = [];
        foreach ($players as $k=>$v){
            $player['order'] = @$v['player']['id']+1;
            $player['adr'] = $v['adr'];
            $player['assists'] = $v['assists'];
            $player['deaths'] = $v['deaths'];
            $player['first_kills_diff'] = $v['first_kills_diff'];
            $player['flash_assists'] = $v['flash_assists'];
            $player['game'] = 1;
            $player['headshot_kills'] = $v['headshots'];
            $player['k_d_diff'] = $v['k_d_diff'];
            $player['kast'] = $v['kast']?$v['kast']/100:$v['kast'];
            $player['kills'] = $v['kills'];
            $player['player_id']=self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_PLAYER,@$v['player']['id'],'3');
            $playerInfo = Player::find()->where(['id'=>$player['player_id']])->asArray()->one();
            $player['steam_id'] = $playerInfo['steam_id'] ? $playerInfo['steam_id'] : null;
            $player['nick_name'] = $v['player']['name'];
            $player['rating'] = $v['rating'];
            $player['team_id'] = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,@$v['team']['id'],'3');
            if (!empty($matchInfo)) {
//                $player['team_order'] = $teams[$v['team']['id']];
                $player['team_order'] = $v['team']['id'] ? PandascoreHotBase::getTeamOrderByTeamRelId($v['team']['id'], $matchInfo) : null;
            }
            $plarersCo[] = $player;
        }
        return $plarersCo;
    }
    // 处理rounds数据结构
    private static function getRoundsData($rounds,$battleId,$matchInfo,$battleOrder=null)
    {
        if ($battleOrder){
            $dbBattleId = MatchBattle::find()->where(['order'=>$battleOrder,'match'=>$matchInfo->id])->asArray()->one();
            if (!empty($dbBattleId)){
                $dbRounds = MatchBattleRoundCsgo::find()->where(['battle_id'=>$dbBattleId['id']])->asArray()->all();
                if (!empty($dbRounds)){
                    $countDbRounds = count($dbRounds);
                    $countRounds = count($rounds);
                    if ($countDbRounds>$countRounds){
                        $diff = $countDbRounds-$countRounds;
                        if ($diff>0){
                            $diffId=$countDbRounds-$diff;
//                            MatchBattleRoundCsgo::deleteAll(['and',['battle_id' => $dbBattleId['id']],['>','round_ordinal',$diffId]]);
//                            MatchBattleRoundSideCsgo::deleteAll(['and',['battle_id' => $dbBattleId['id']],['>','round_ordinal',$diffId]]);
                        }
                    }
                }
            }
        }

        $roundsCo = [];
        foreach ($rounds as $k=>$v){
            $round = [
                'rel_battle_id' => $battleId,
                'round_ordinal' => $v['round'],
                'winner_team_id' => $v['winner_team']?PandascoreHotBase::getTeamOrderByTeamRelId($v['winner_team'],$matchInfo):null,
            ];
            $type = [
                "planted_eliminated-terrorists" => "5",
                "timeout-ct" => "3",
                "defused-ct" => "1",
                "eliminated-terrorists" => "5",
                "eliminated-ct" => "2",
                "exploded-terrorists" => "4",
            ];
            $roundType = $type[$v['outcome'].'-'.$v['winner_side']];
            $roundsSide = [];
            if(isset($v['ct'])){
                $roundCo = [
//                    'battle_id' => $battleId,
                    'side_order' => $v['ct'] ? PandascoreHotBase::getTeamOrderByTeamRelId($v['ct'], $matchInfo) : null,
//                    'order' => count($roundsSide)+1,
                    'round_ordinal' => $v['round'],
                    'side' => 'ct',
                    'team_id' => self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['ct'],'3'),
                    'is_winner' => $v['winner_team']==$v['ct'] ? 1 : 2,
//                    'is_bomb_planted' => null,
                ];
                if($roundType == '1'||$roundType == '2'||$roundType == '3'){
                    $roundCo['round_end_type'] = $roundType;
                    $round['winner_end_type'] = $roundType;
                }
                array_push($roundsSide,$roundCo);
            }
            if(isset($v['terrorists'])){
                $roundCo = [
//                    'battle_id' => $battleId,
                    'side_order' => $v['terrorists'] ? PandascoreHotBase::getTeamOrderByTeamRelId($v['terrorists'],$matchInfo) : null,
//                    'order' => count($roundsSide)+1,
                    'round_ordinal' => $v['round'],
                    'side' => 'terrorist',
                    'team_id' => self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['terrorists'],'3'),
                    'is_winner' => $v['winner_team']==$v['terrorists'] ? 1 : 2,
//                    'is_bomb_planted' => $v['outcome']=='eliminated'||$v['outcome']=='timeout' ? 2 : 1,
                ];
                if($roundType == '4'||$roundType == '5'){
                    $roundCo['round_end_type'] = $roundType;
                    $round['winner_end_type'] = $roundType;
                }
                array_push($roundsSide,$roundCo);
            }
            $round['side'] = $roundsSide;
            $roundsCo[] = $round;
        }
        return $roundsCo;
    }
    // 处理team数据结构
    private static function getWinnerStaticData($teams,$defaultAdvantage,$matchInfo)
    {
        $teamsCo = [];
        foreach ($teams as $k=>$v){
            $order = $v['id'] ? self::getTeamOrderByTeamRelId($v['id'], $matchInfo) : null;
            $teamId = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3');
            $teamsCo[$k]['order'] = $order;
            $teamsCo[$k]['team_id'] = $teamId;
            $teamsCo[$k]['rel_identity_id'] = $v['id'];
            $teamsCo[$k]['identity_id'] = $v['name'];
            if($defaultAdvantage == $order){
                $teamsCo[$k]['score'] = 1;
            }else{
                $teamsCo[$k]['score'] = 0;
            }
        }
        return $teamsCo;
    }
    // 处理team数据结构
    private static function getTeamsData($teams,$round,$rounds,$battlesScore,$matchInfo,$battlePlayers=[])
    {
        $teamScores=[
            1=>[
                '1st_half_score'=>0,
                '2nd_half_score'=>0,
                'ot_score'=>0,
            ],
            2=>[
                '1st_half_score'=>0,
                '2nd_half_score'=>0,
                'ot_score'=>0,
            ]
        ];
        $liveRoundOrder=0;
        foreach($rounds as $key=>$val){
            if('有效'){
                $liveRoundOrder=$liveRoundOrder+1;
            }
            if($liveRoundOrder<=15){
                $scoreKey='1st_half_score';
            }elseif($liveRoundOrder<=30){
                $scoreKey='2nd_half_score';
            }else{
                $scoreKey='ot_score';
            }
            $teamOrderId=$val['winner_team']?self::getTeamOrderByTeamRelId($val['winner_team'],$matchInfo):null;
            $teamScores[$teamOrderId][$scoreKey]=$teamScores[$teamOrderId][$scoreKey]+1;
        }

        $teamsData = [];
        foreach ($teams as $k=>$v){
//            $tmpTeamScore=$teamScores[$k+1];
//            $tmpVal=array_merge($v,$tmpTeamScore);
//            $teamsData[$k]=$teamScores[$k+1];
            foreach ($battlesScore as $kk=>$vv){
                if($v['id'] == $vv['team_id']){
//                    $teamsData[$k]['order'] = PandascoreHotBase::getTeamOrderByTeamRelId($v['id'],$matchInfo);
                    $teamOrder = $v['id']?self::getTeamOrderByTeamRelId($v['id'],$matchInfo):null;
                    $teamsData[$k]['order'] = $teamOrder;
                    $teamsData[$k]['score'] = $vv['score'];
                    $teamsData[$k]['team_id'] = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3',1);
                    $teamsData[$k]['rel_identity_id'] = $v['id'];
                    $teamsData[$k]['identity_id'] = $v['name'];
                    $teamsData[$k]=array_merge($teamScores[$teamOrder],$teamsData[$k]);
                }else{
//                    $teamsData[$k]['order'] = PandascoreHotBase::getTeamOrderByTeamRelId($v['id'],$matchInfo);
                    $teamOrder = $v['id']?self::getTeamOrderByTeamRelId($v['id'],$matchInfo):null;
                    $teamsData[$k]['order'] = $teamOrder;
                    $teamsData[$k]['team_id'] = self::getMainIdByRelIdentityId(Consts::RESOURCE_TYPE_TEAM,$v['id'],'3',1);
                    $teamsData[$k]['rel_identity_id'] = $v['id'];
                    $teamsData[$k]['identity_id'] = $v['name'];
                    $teamsData[$k]=array_merge($teamScores[$teamOrder],$teamsData[$k]);
                }
                if(isset($round['ct']) && $round['ct']==$v['id']){
                    $teamsData[$k]['starting_faction_side'] = 'ct';
                }
                if(isset($round['terrorists']) && $round['terrorists']==$v['id']){
                    $teamsData[$k]['starting_faction_side'] = 'terrorist';
                }
            }
            //battle players 统计
            if (!empty($battlePlayers)) {
                $countPlayer = 0;
                foreach ($battlePlayers as $kPlayer => $vPlayer) {
                    if ($vPlayer['team']['id'] == $v['id']) {
                        $teamsData[$k]['kills']            += $vPlayer['kills'];
                        $teamsData[$k]['assists']          += $vPlayer['assists'];
                        $teamsData[$k]['deaths']           += $vPlayer['deaths'];
                        $teamsData[$k]['first_kills_diff'] += $vPlayer['first_kills_diff'];
                        $teamsData[$k]['flash_assists']    += $vPlayer['flash_assists'];
                        $teamsData[$k]['headshot_kills']   += $vPlayer['headshots'];
                        $teamsData[$k]['kd_diff']          += $vPlayer['k_d_diff'];
                        $teamsData[$k]['adr']              += $vPlayer['adr'];
                        $teamsData[$k]['kast']             += $vPlayer['kast'];
                        $teamsData[$k]['rating']           += $vPlayer['rating'];
                        $countPlayer++;
                    }
                }
            }
            $teamsData[$k]['adr']    = (string)($teamsData[$k]['adr'] / $countPlayer);
            $teamsData[$k]['kast']   = (string)($teamsData[$k]['kast'] / $countPlayer);
            $teamsData[$k]['rating'] = (string)($teamsData[$k]['rating'] / $countPlayer);

        }

        return $teamsData;
    }
    // 计算上半场/下半场/加时比分
    private static function getCountSource($data)
    {
        $count = array_count_values(array_column($data,'winner_side'));
        $word = ['ct','terrorists'];
        $wordCount = [];
        foreach ($word as $k=>$v){
            $sl = isset($count[$v])? $count[$v] : 0;
            $wordCount[] = [
                'winner_side' => $v,
                'sl' => $sl,
            ];
        }
        return $wordCount;
    }

    /**
     * 处理match数据结构
     * @param $matchData
     * @param $matchInfo
     * @return mixed
     * @throws BusinessException
     */
    private static function getMatchData($matchData, $matchInfo)
    {
        $match['begin_at']           = isset($matchData['begin_at']) && $matchData['begin_at'] ? date('Y-m-d H:i:s', strtotime($matchData['begin_at'])) : "";
        $match['end_at']             = isset($matchData['end_at']) && $matchData['end_at'] ? date('Y-m-d H:i:s', strtotime($matchData['end_at'])) : "";
        $match['is_battle_detailed'] = $matchData['detailed_stats'] == true ? 1 : 2;
        $match['is_forfeit']         = $matchData['forfeit'] == true ? 1 : 2;
        $match['is_draw']            = $matchData['draw'] == true ? 1 : 2;
        $match['default_advantage']  = $matchData['game_advantage'] ? self::getTeamOrderByTeamRelId($matchData['game_advantage'], $matchInfo) : null;
        $match['winner']             = $matchData['winner_id'] ? self::getTeamOrderByTeamRelId($matchData['winner_id'], $matchInfo) : null;
        $match['status']             = Common::getPandaScoreMatchStatus($matchData['status']);
        foreach ($matchData['results'] as $key => $val) {
            $teamOrder = $val['team_id'] ? self::getTeamOrderByTeamRelId($val['team_id'], $matchInfo) : null;
            if ($teamOrder == 2) {
                $match['team_2_score'] = $val['score'];
            } else {
                $match['team_1_score'] = $val['score'];
            }
        }
        return $match;
    }


    private static function getMatch($matchId)
    {
        $url = '/csgo/matches';
        $params = [
            'filter[id]' => $matchId,
        ];
        $jsonInfo = self::getRestInfo($url, $params);
        $info = json_decode($jsonInfo, true);
        // 获取当前Match的数据
        return $info[0];
    }
    private static function getBattle($battleId)
    {
        $url = '/csgo/games/' . $battleId;
        $jsonInfo = PandascoreBase::getInfo($url, []);
        $info = json_decode($jsonInfo, true);
        return $info;
    }
    // 地图id
//    private static function mapNameChangeId($mapName)
//    {
//        if(isset(self::mapNameCreId[$mapName])){
//
//        }else {
//            self::mapNameCreId[$mapName] = MetadataCsgoMap::find()->where(['name' => $mapName])->select('id')->one();
//        }
//        return self::mapNameCreId[$mapName]['id'];
//    }
}