<?php
/**
 *
 */

namespace app\modules\task\services\hot\pandascore;


use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\TaskService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\db\Query;

class PandascoreDota2Match extends PandascoreHotBase implements HotMatchInterface
{
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $timeStart=date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $hotId = $info['id'];
        $hotInfo = self::getHotInfo($hotId);
        $matchId=$hotInfo['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,3);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        //$relIdentityId=$hotInfo['rel_identity_id'];
        $matchInfo=Match::find()->where(['id'=>$matchId])->one();
//        self::setStatus($hotInfo, 2);
        $restInfos = self::getInfos($relIdentityId);
        if (!empty($restInfos['match']) && count($restInfos['battles']) > 0){
            // 这里传matchInfo，用来判断主队客队
            $infoFormat = self::conversion($restInfos,$matchInfo);
            $infoFormatAddBindingInfo=self::addBindingInfo($infoFormat,$matchInfo);
            $timeEnd=date('Y-m-d H:i:s');
            $extInfo=[
                'time_begin'=>$timeStart,
                'time_end'=>$timeEnd,
                'task_id'=>$taskInfo['id'],
                'match_id'=>$matchId,
            ];
            // 添加绑定信息，转义,主要出现在battle的详情中
            self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
            // 更新到数据库
            $refreshRes = self::refreshInfo($matchId,$infoFormatAddBindingInfo,$refreshType);
            // 更新hotdata状态
            self::refreshHotDataStatusByMatchInfo($hotId,$refreshRes);
        }
        return;
    }

    public static function addBindingInfo($infoFormat,$matchInfo)
    {
        $teamInfoRes = [];
        $teamInfoRes[$infoFormat['match']['team1_id']] = null;
        $teamInfoRes[$infoFormat['match']['team2_id']] = null;
        $matchId = $matchInfo['id'];
        //转化队伍
        $team1trueID = self::getTeamOrderByTeamRelId($infoFormat['match']['team1_id'],$matchInfo);
        if ($team1trueID == 2){
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_2_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_1_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 2;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 1;
        }else{
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_1_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_2_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 1;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 2;
        }
        //$team2trueID = self::getTeamOrderByTeamRelId($infoFormat['match']['team2_id'],$matchInfo);
        if (isset($infoFormat['match']['default_advantage']) && $infoFormat['match']['default_advantage']){
            $infoFormat['match']['default_advantage'] = $teamInfoRes[$infoFormat['match']['default_advantage']];
        }
        if (isset($infoFormat['match']['winner']) && $infoFormat['match']['winner']){
            $infoFormat['match']['winner'] = $teamInfoRes[$infoFormat['match']['winner']];
        }
//        $infoFormat['match']['map_info'] = json_encode([1]);
        foreach($infoFormat['battles'] as &$battle){
            if (empty($battle)){
                continue;
            }
            //base
            $battle['base']['map'] = 1;  //数据源没该数据，
            $battle_winner = null;
            if (isset($battle['base']['winner']) && $battle['base']['winner']){
                $battle['base']['winner'] = $teamInfoRes[$battle['base']['winner']];
                $battle_winner = $battle['base']['winner'];
            }
            //转化team
            $team_array = [];
            foreach($battle['static'] as &$static){
                if ($battle_winner == $static['order']){
                    $static['score'] = 1;
                }else{
                    $static['score'] = 0;
                }
                if ($battle['base']['is_forfeit'] != 1){
                    $static['team_id']= self::getMainIdByRelIdentityId('team', $static['rel_team_id'], '3');
                    $team_array[$static['rel_team_id']] = @$static['team_id'];
                }
            }
            // 更新选手中的选手id，战队id，item中的装备
            $team1_seed = $team2_seed = 1;
            if (isset($battle['players']) && count($battle['players']) > 0) {
                foreach ($battle['players'] as $key => &$player) {
                    $player['game'] = 3;
                    $player['match'] = $matchId;
                    $player['team_id'] = @$team_array[$player['rel_team_id']];//self::getMainIdByRelIdentityId('team', $player['rel_team_id'], '3');
                    $player['hero'] = self::getMainIdByRelIdentityIdOrUnknown('dota2_hero', $player['hero'], '3');
                    $player['player_id'] = self::getMainIdByRelIdentityId('player', $player['player_id'], '3');
                    //处理角色
                    $roleList = [1,2,3,4,5,1/2,2/3,3/4,4/5];
                    $originPlayerRole = $player['role'];//角色
                    if (in_array($originPlayerRole,$roleList)){
                        $player['role'] = null;
                        $player['lane'] = null;
                    }else{
                        $player['role'] = null;
                        $player['lane'] = null;
                    }
                    //$player['role'] = $player_role;  //角色
                    if ($player['team_order'] == 1){
                        $player['seed'] = $team1_seed;//选手排序
                        $team1_seed ++;
                    }
                    if ($player['team_order'] == 2){
                        $player['seed'] = $team2_seed;//选手排序
                        $team2_seed ++;
                    }
                    $player['order'] = $key + 1;//排序
                    //处理item 道具装备
                    $player['items'] = self::conversionDota2PlayerItems($player['items']);
                    //处理abilities技能升级时间线
                    $abilitiesArr = self::conversionDota2PlayerAbilities($player['abilities']);

                    $player['abilities_timeline'] = $abilitiesArr['abilities_timeline'];
                    $player['talents'] = $abilitiesArr['talents'];
                    //下面会用到
                    $team_array[$player['rel_team_id']] = $player['team_id'];
                }
            }

            //转化ban_pick
            if(count($battle['ban_pick'])>0){
                foreach ($battle['ban_pick'] as &$ban_pick_val){
                    $ban_pick_val['hero'] = self::getMainIdByRelIdentityId('dota2_hero',$ban_pick_val['hero'],'3');
                    $ban_pick_val['team'] = @$team_array[$ban_pick_val['team']];
                }
            }
        }
        return $infoFormat;
    }

    public static function getInfosTest()
    {
        //
        $info=HotDataLog::find()->where(['id'=>17])->one();
        $infoRest=json_decode($info['info_rest'],true);
        return $infoRest;
    }

    public static function refreshInfo($matchId, $info,$refreshType)
    {
        $battle_complete = true;
        $battle_order_num = 0;
        $battle_is_forfeit_order_id = null;
        if ($refreshType == 'refresh'){
            //有battle
            $battleList = MatchBattle::find()->where(['match'=>$matchId])->asArray()->all();
            if (count($battleList)){
                foreach ($battleList as $key =>$item){
                    //判断默认领先
                    $battle_is_default_advantage = $item['is_default_advantage'];
                    if ($battle_is_default_advantage == 1){
                        $battle_order_num = $item['order'];
                    }
                }
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }

            }
        }else{
//            $info['match']['default_advantage'] = 1;//测试用
            //判断是不是有默认领先
            if (isset($info['match']['default_advantage']) && $info['match']['default_advantage']){
                HotBase::createMatchAdvantageBattle($matchId,$info['match']['default_advantage'],0,1,$type='rest_api',3);
                $battle_order_num = 1;
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    $battle_complete = @$battle['base']['complete'];
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }
            }
        }
        //更新match层信息
        self::setMatch($matchId, $info['match']);
        return $battle_complete;
    }

    private static function refreshBattle($matchId, $battle, $key=0,$refreshType = '')
    {
        //判断有没有对局时长 没有就不创建battle
        if (!isset($battle['base']['duration'])){
            return true;
        }
        $have_battle = false;
        // 找到对应的battleId，更新battle
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
            $have_battle = true;
        }
        $battleId=$battleBase['id'];

        if ($have_battle){
            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleId,'match'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');
        }

        BattleService::setBattleInfo($battleId,'dota',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'dota',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId,'dota',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'dota',$battle['static'],BattleService::DATA_TYPE_STATICS);
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'dota',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        //判断关联关系
        self::setMatchBattleRelation($matchId,$battleId,$battle);
    }
    private static function setMatchBattleRelation($matchId,$battleId,$battle){
        //插入关联关系
        $rel_matchId = @$battle['base']['rel_match_id'];
        $rel_battleId = @$battle['base']['rel_battle_id'];
        $battle_order = $battle['base']['order'];
        $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battle_order])->one();
        if (!$matchBattleRelation){
            //首先插入关系表
            $matchBattleRelation = new MatchBattleRelation();
            $matchBattleRelation->setAttributes([
                'match_id' => (int)$matchId,
                'order' => $battle_order,
                'rel_match_id' => (string)$rel_matchId,
                'rel_battle_id' => (string)$rel_battleId
            ]);
            if (!$matchBattleRelation->save()) {
                throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
            }
        }
        //设置redis 关系
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis->hset($redis_name,$rel_battleId,$battleId.'-'.$battle_order);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    public static function getStandardMatchInfo($standardId)
    {
        return StandardDataMatch::find()->where(['id' => $standardId])->one();
    }

    public static function getBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('match.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('match', 'rel.master_id=match.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_MATCH]);
        return $q->one();
    }

    public static function getInfos($relMatchId)
    {
        $matchInfo = self::getMatch($relMatchId);
        $battles = $matchInfo['games'];
        $battleInfos = [];
        if ($battles){
            //数据源创建5个battle，当battle是空的时候不保存
            foreach ($battles as $key => $battle) {
                $battleId = $battle['id'];
                if ($battle['status'] == 'not_started' || $battle['status'] == 'canceled' ) {
                    unset($battles[$key]);
                    continue;
                }else{
                    $battleBase = self::getBattle($battleId);
//                    $events = self::getEvents($battleId);
//                    $frames = self::getFrames($battleId);
                    $battleInfos[] = [
                        'base' => $battleBase,
//                        'events' => $events,
//                        'frames' => $frames,
                    ];
                }
            }
        }
        return [
            'match' => $matchInfo,
            'battles' => $battleInfos
        ];
    }
    /**
     * @param $restInfo
     * @param $matchInfo 比赛基础信息这里有主队客队id
     * @return array
     *
     */
    public static function conversion($restInfo,$matchInfo)
    {
        // 转化match
        $formatData = [
            'match' => '',
            'battles' => [],
        ];
        // 这里记录了关联的team
        $teamInitInfo = [];
        // 遍历result，这里面有主队客队对应关系
        $detailInfoConfig = [
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                if ($info['end_at']){
                    return date('Y-m-d H:i:s', strtotime($info['end_at']));
                }else{
                    return null;
                }
            },
            'team_1_score_old' => function ($info) {
                return $info['results'][0]['score'];
            },
            'team1_id' => function ($info) {
                return $info['results'][0]['team_id'];
            },
            'team_2_score_old' => function ($info) {
                return $info['results'][1]['score'];
            },
            'team2_id' => function ($info) {
                return $info['results'][1]['team_id'];
            },
            'is_battle_detailed' => function ($info) {
                return $info['detailed_stats'] ? 1 : 2;
            },
            'is_draw' => function ($info) {
                return $info['draw'] ? 1 : 2;
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            'default_advantage' => function ($info) {
                return $info['game_advantage'] ? $info['game_advantage'] : null;
            },
            'status' => function($info){
                return Common::getPandaScoreMatchStatus($info['status']);
            },
            'winner' => 'winner_id',
        ];
        $matchDetailInfo = Mapping::transformation($detailInfoConfig, $restInfo['match']);
        $formatData['match'] = $matchDetailInfo;
        if (count($restInfo['battles']) > 0){
            foreach ($restInfo['battles'] as $key => $restBattle) {
                // 格式化battle信息，这里需要一些team的信息
                $battleInfo = self::conversionBattle($restBattle, $restInfo,$matchInfo);
                $formatData['battles'][] = $battleInfo;
            }
        }
        return $formatData;
    }
    //转化battle
    private static function conversionBattle($restBattleInfo, $restInfo, $matchInfo)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];

        $battleInfoConfig = [
            'rel_battle_id' => 'id',
            'rel_match_id' => 'match_id',
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                if ($info['end_at']){
                    return date('Y-m-d H:i:s', strtotime($info['end_at']));
                }else{
                    return null;
                }
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            'status' => function ($info) {
                return $info['status'] == 'finished' ? 3 : 2;
            },
//            'is_draw' => 0,
            'complete'=>'complete',
            'order' => 'position',
            'duration' => function ($info) {
                if ($info['length']){
                    return $info['length'];
                }else{
                    return null;
                }
            },
//            'is_battle_detailed' => 1 ,
            'winner' => function($params){
                if(isset($params['winner']['id']) && $params['winner']['id']){
                    return $params['winner']['id'];
                }
            },
            "rel_battle_id" => "id",
            "rel_match_id" => function($params){
                if(isset($params['match']['id']) && $params['match']['id']){
                    return $params['match']['id'];
                }
            },
        ];
        $battleBaseAtt = Mapping::transformation($battleInfoConfig, $restBattleInfo['base']);
        if (isset($battleBaseAtt['duration'])){
            $battleBaseAtt['duration']=(string)$battleBaseAtt['duration'];//转换下格式
        }
        $battleBaseAtt['is_battle_detailed'] = 1;
        $battleBaseAtt['is_default_advantage'] = 2;
        $battleBaseAtt['is_draw'] = 2;
        $battleBaseAtt['flag'] = 1;
        $battleBaseAtt['deleted'] = 2;
        $battleBaseAtt['deleted_at'] = null;
        $battleBaseAtt['game'] = 3;
        $battleBaseAtt['match'] = $matchInfo['id'];
        // 获取battle详情,这里用来设置id
        $battleInfo['base'] = $battleBaseAtt;
        $teamOrderArray = $teamStatic = $players = [];
        $battleDetail = [];
        $battleDetail['is_confirmed'] = 1;
        //处理战队
        $teamInfosRest = $restBattleInfo['base']['teams'];
        if ($battleBaseAtt['is_forfeit'] == 1){
            $battleInfo['base']['is_battle_detailed'] = 2;
            $battleInfo['players'] = $players;

            $teamStatic[1]['team_id'] = $matchInfo['team_1_id'];
            $teamStatic[1]['order']= 1;
            $teamStatic[2]['team_id'] = $matchInfo['team_2_id'];
            $teamStatic[2]['order']= 2;
            //计算团队的信息
            $battleInfo['static'] = $teamStatic;
            $battleInfo['detail'] = $battleDetail;
            $battleInfo['ban_pick'] = [];
        }else{
            if (empty($teamInfosRest)){
                return [];
            }
            foreach ($teamInfosRest as $key => $team) {
                $team_order = self::getTeamOrderByTeamRelId(@$team['team']['id'], $matchInfo);
                //用于处理选手
                $teamOrderArray[$team['team']['id']] = $team_order;
                // 一血
                if ($team['first_blood']) {
                    $battleDetail['first_blood_p_tid'] = $team_order;
                }
                //首肉山
                if ($team['first_roshan']) {
                    $battleDetail['first_roshan_p_tid'] = $team_order;
                }
                //首塔
                if ($team['first_tower']) {
                    $battleDetail['first_tower_p_tid'] = $team_order;
                }
                $teamStatic[$team_order]['order']= $team_order;
                $teamStatic[$team_order]['net_worth']= null;
                $teamStatic[$team_order]['experience']= null;
                $teamStatic[$team_order]['rel_team_id']=@$team['team']['id'];
                $teamStatic[$team_order]['rel_identity_id']=$team['team']['id'];
                $teamStatic[$team_order]['faction'] = $team['faction'];
                $teamStatic[$team_order]['kills'] = $team['score'];
                $teamStatic[$team_order]['kills'] = $team['score'];
                $teamStatic[$team_order]['gold_earned'] = 0;
//            $teamStatic[$team_order]['experience'] = 0;
                $teamStatic[$team_order]['tower_kills'] = 0;
                $teamStatic[$team_order]['roshan_kills'] = null;
                $building_status = HotBase::getBuildingDefaultStatusOnFrameByPandascore($team['faction']);
                //兵营状态 现在是主客队是反着的，要在下一步转化的时候调整过来
                $teamBuldingStatusBarracksInfo = self::conversionRestBuldingStatusBarracks($building_status,$team['barracks_status']);
                if ($teamBuldingStatusBarracksInfo){
                    $building_status = $teamBuldingStatusBarracksInfo['building_status'];
//                    $teamStatic[$team_order]['building_status_barracks'] = $teamBuldingStatusBarracksInfo['building_status_barracks_detail'];
                    $teamStatic[$team_order]['melee_barrack_kills'] = $teamBuldingStatusBarracksInfo['melee_barrack_kills_num'];
                    $teamStatic[$team_order]['ranged_barrack_kills'] = $teamBuldingStatusBarracksInfo['ranged_barrack_kills_num'];
                }else{
                    $teamStatic[$team_order]['building_status_barracks'] = null;
                    $teamStatic[$team_order]['melee_barrack_kills'] = null;
                    $teamStatic[$team_order]['ranged_barrack_kills'] = null;
                }
                //防御塔状态  tower_kills 现在是主客队是反着的，要在下一步转化的时候调整过来
                $teamBuldingStatusTowers = self::conversionRestBuldingStatusTowers($building_status,$team['tower_status']);
                if ($teamBuldingStatusTowers){
                    $building_status = $teamBuldingStatusTowers['building_status'];
                    $teamStatic[$team_order]['tower_kills'] = $teamBuldingStatusTowers['tower_kills_num'];
                }else{
                    $teamStatic[$team_order]['building_status_towers'] = null;
                    $teamStatic[$team_order]['tower_kills'] = null;
                }
                if (!empty($building_status)){
                    if ($team['team']['id']==$restBattleInfo['base']['winner']['id']){
                        $building_status['ancient']['is_alive'] = true;
                    }else{
                        $building_status['ancient']['is_alive'] = false;
                    }
                    $teamStatic[$team_order]['building_status'] = json_encode($building_status,320);
                }
            }
            //近战兵营状态 队伍互换
            if (isset($teamStatic[1]['melee_barrack_kills']) && isset($teamStatic[2]['melee_barrack_kills'])){
                $melee_barrack_kills_tmp = $teamStatic[1]['melee_barrack_kills'];
                $teamStatic[1]['melee_barrack_kills'] = $teamStatic[2]['melee_barrack_kills'];
                $teamStatic[2]['melee_barrack_kills'] = $melee_barrack_kills_tmp;
            }
            //远战兵营状态 队伍互换
            if (isset($teamStatic[1]['ranged_barrack_kills']) && isset($teamStatic[2]['ranged_barrack_kills'])){
                $ranged_barrack_kills_tmp = $teamStatic[1]['ranged_barrack_kills'];
                $teamStatic[1]['ranged_barrack_kills'] = $teamStatic[2]['ranged_barrack_kills'];
                $teamStatic[2]['ranged_barrack_kills'] = $ranged_barrack_kills_tmp;
            }

            $teamStatic[1]['barrack_kills'] = (int)($teamStatic[1]['ranged_barrack_kills']+$teamStatic[1]['melee_barrack_kills']);
            $teamStatic[2]['barrack_kills'] = (int)($teamStatic[2]['ranged_barrack_kills']+$teamStatic[2]['melee_barrack_kills']);

            //这里转换tower_kills
            if (isset($teamStatic[1]['tower_kills']) && isset($teamStatic[2]['tower_kills'])){
                $tower_kills_tmp = $teamStatic[1]['tower_kills'];
                $teamStatic[1]['tower_kills'] = $teamStatic[2]['tower_kills'];
                $teamStatic[2]['tower_kills'] = $tower_kills_tmp;
            }
            //detail
            $battleInfo['detail'] = $battleDetail;
            //pick_array
            $pick_array = [];
            foreach ($restBattleInfo['base']['players'] as $key=>$player){
                $pick_array[$key]['hero'] = $player['hero']['id'];
                $pick_array[$key]['team'] = $player['team']['id'];
                $pick_array[$key]['type'] = 2;
                $pick_array[$key]['order'] = null;
            }
            //ban
            $banArray = [];
            foreach ($restBattleInfo['base']['teams'] as $playerTeams){
                foreach ($playerTeams['bans'] as $banKey=>$ban){
                    $tmp=[
                        'hero'=>$ban,
                        'team'=>$playerTeams['team']['id'],
                        'type'=>1,
                        'order'=> null
                    ];
                    $banArray[] = $tmp;
                }
            }
            $battleInfo['ban_pick'] = array_merge($banArray,$pick_array);
            //选手
            $players = [];
            $player_damage_to_heroes_all = $player_damage_taken_all = 0;
            $playersRest = $restBattleInfo['base']['players'];
            foreach ($playersRest as $key => $playerRest) {
                //注销的是没有
                $playerConfig = [
                    //'seed'
                    'faction' => "faction",
//                    'role' => function($params){
//                        return $params['player']['role'];
//                    },
                    'role' =>null,
                    //lane
                    "player_id" => function($params){
                        return $params['player']['id'];
                    },//选手id
                    "nick_name" => function($params){
                        return $params['player']['name'];
                    },
                    "rel_nick_name" => function($params){
                        return $params['player']['name'];
                    },

                    "hero" => function($params){
                        return $params['hero']['id'];
                    },//英雄
                    "level" => "hero_level",//等级
                    "is_alive" => null,//存活状态
                    //ultimate_cd
                    //coordinate
                    "kills" => "kills",//击杀
                    "tower_kills" => "tower_kills",//tower_kills
                    //kill_combos[
                    //double_kill
                    //triple_kill
                    //quadra_kill
                    //penta_kill
                    //largest_multi_kill
                    //largest_killing_spree ]
                    "deaths" => "deaths",//死亡
                    "assists" => "assists",//助攻
                    //kda 下面计算
                    //participation  下面计算
                    "last_hits" => "last_hits",//补刀
                    "lane_creep_kills"=>"lane_creep",//补刀详情-小兵击杀数
                    "neutral_creep_kills" => "neutral_creep",//补刀详情-野怪击杀数
                    //lhpm 下面计算
                    "denies" => "denies",//反补
                    //net_worth
                    "gold_earned" => function ($params) {
                        return $params['gold_remaining'] + $params['gold_spent'];
                    },//金币获取
                    "gold_spent" => "gold_spent",//已花费金币
                    "gold_remaining" => "gold_remaining",//剩余金币
                    "gpm" => "gold_per_min",//分均金钱
                    //experience
                    "xpm" => "xp_per_min",//分均经验
                    "damage_to_heroes"=>"hero_damage",//对英雄伤害
                    //dpm_to_heroes 对英雄分均伤害  下面计算
                    //damage_percent_to_heroes  对英雄伤害占比 循环外计算
                    "damage_taken" => "damage_taken",//承受伤害
                    //damage_taken_percent 承伤占比 循环外计算
                    //damage_detail 里面 damage_to_heroes_detail 对英雄伤害详情  和 damage_taken_detail 承受伤害详情  没有
                    "damage_to_towers"=>"tower_damage",//对防御塔伤害
                    //damage_conversion_rate 伤害转化率 下面计算
                    //support_detail 辅助详情 total_heal 总治疗量  total_crowd_control_time总控制时长
                    'total_heal'=>'heal',
                    "wards_purchased" => function ($params) {
                        return $params['observer_wards_purchased'] + $params['sentry_wards_purchased'];
                    },//买眼 =  购买侦查守卫 + 购买岗哨守卫
                    "wards_placed" => function ($params) {
                        return $params['observer_used'] + $params['sentry_used'];
                    },//插眼 =  放置侦查守卫 +  放置岗哨守卫
                    "wards_kills" => function ($params) {
                        return $params['observer_wards_destroyed'] + $params['sentry_wards_destroyed'];
                    },//排眼 = 摧毁侦查守卫 + 摧毁岗哨守卫
                    //wards_detail 守卫详情
                    "observer_wards_purchased" => "observer_wards_purchased",//购买侦查守卫
                    "observer_wards_placed" => "observer_used",//放置侦查守卫
                    "observer_wards_kills" => "observer_wards_destroyed",//摧毁侦查守卫
                    "sentry_wards_purchased" => "sentry_wards_purchased",//购买岗哨守卫
                    "sentry_wards_placed" => "sentry_used",//放置岗哨守卫
                    "sentry_wards_kills" => "sentry_wards_destroyed",//摧毁岗哨守卫
                    "camps_stacked" => "camps_stacked",//堆野数
                    //runes_detail [ 神符详情
                    //double_damage_runes //双倍伤害神符
                    //haste_runes //极速神符
                    //illusion_runes //幻象神符
                    //invisibility_runes //隐身神符
                    //regeneration_runes //恢复神符
                    //bounty_runes //赏金神符
                    //arcane_runes //奥术神符
                    //double_damage_runes ] //双倍伤害神符
                    "items" => "items",//道具装备
                    "abilities" => "abilities",//技能升级时间线
                    "rel_team_id" => function ($params) {
                        return $params['team']['id'];
                    },
                    'rel_player' => 'player',
                    'rel_identity_id' => function ($params) {
                        return (string)$params['player']['id'];
                    },
                    'rel_id' => function ($params) {
                        return $params['player']['id'];
                    },
                ];
                $player_db_id = HotBase::getMainIdByRelIdentityId('player', $playerRest['player']['id'], '3');
                $player_db_info = Player::find()->where(['id'=>$player_db_id])->asArray()->one();
                $player = Mapping::transformation($playerConfig, $playerRest);
                $player['order']=$key;
                $player['steam_id']=$player_db_info['steam_id']?$player_db_info['steam_id']:null;
                $team_order_player = $teamOrderArray[$playerRest['team']['id']];
                $player['team_order']= $team_order_player;//  self::getTeamOrderByTeamRelId($playerRest['team']['id'],$matchInfo);
                //以下需要计算得属性
                //kda
                $deaths = $player['deaths'] > 0 ? $player['deaths'] : 1;
                $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
                // participation 参团率
                $teamkill = self::getTeamKills($playerRest['team']['id'],$restBattleInfo);
                if($teamkill){
                    $player['participation'] = (String)round((($player['kills'] + $player['assists'])/$teamkill),4);
                }else{
                    $player['participation'] = null;
                }
                //分均补刀 lhpm
                $player['lhpm'] = (String)round(($player['last_hits']/($restBattleInfo['base']['length']/60)),2);
                //对英雄分均伤害  和  dtpm 分均承受伤害
                if(isset($restBattleInfo['base']['length']) && $restBattleInfo['base']['length']){
                    //对英雄伤害
                    if (isset($player['damage_to_heroes'])){
                        $player['dpm_to_heroes'] = (String)round(($player['damage_to_heroes']/($restBattleInfo['base']['length']/60)),2);
                    }else{
                        $player['dpm_to_heroes'] = null;
                    }
                    if (isset($player['damage_taken'])){
                        $player['dtpm'] = (String)round(($player['damage_taken']/($restBattleInfo['base']['length']/60)),2);
                    }else{
                        $player['dtpm'] = null;
                    }
                }else{
                    $player['dpm_to_heroes'] = null;
                    $player['dtpm'] = null;
                }
                //damage_conversion_rate 伤害转化率
                if(isset($player['gold_earned']) && $player['gold_earned'] && isset($player['damage_to_heroes'])){
                    $player['damage_conversion_rate'] = (String)round(($player['damage_to_heroes']/$player['gold_earned']),4);
                }else{
                    $player['damage_conversion_rate'] = '';
                }

                //计算总数
                $player['damage_to_heroes'] = isset($player['damage_to_heroes']) ? $player['damage_to_heroes'] : 0;
                $player_damage_to_heroes_all += $player['damage_to_heroes'];
                $player['damage_taken'] = isset($player['damage_taken']) ? $player['damage_taken'] : 0;
                $player_damage_taken_all += $player['damage_taken'];
                $teamStatic[$team_order_player]['gold_earned'] +=$player['gold_earned'];
                $teamStatic[$team_order_player]['deaths'] +=$player['deaths'];
                $teamStatic[$team_order_player]['assists'] +=$player['assists'];
                $teamStatic[$team_order_player]['observer_wards_purchased'] +=$player['observer_wards_purchased'];
                $teamStatic[$team_order_player]['observer_wards_placed'] +=$player['observer_wards_placed'];
                $teamStatic[$team_order_player]['observer_wards_kills'] +=$player['observer_wards_kills'];
                $teamStatic[$team_order_player]['sentry_wards_purchased'] +=$player['sentry_wards_purchased'];
                $teamStatic[$team_order_player]['sentry_wards_placed'] +=$player['sentry_wards_placed'];
                $teamStatic[$team_order_player]['sentry_wards_kills'] +=$player['sentry_wards_kills'];
                //最终结果
                $players[] = $player;
            }
            //计算对英雄伤害占比damage_percent_to_heroes 和 承伤占比 damage_taken_percent
            foreach ($players as $key => $item){
                $player_damage_to_heroes_all = $player_damage_to_heroes_all == 0 ? 1 : $player_damage_to_heroes_all;
                $player_damage_taken_all = $player_damage_taken_all == 0 ? 1 : $player_damage_taken_all;
                $players[$key]['damage_percent_to_heroes'] = (String)round(($item['damage_to_heroes']/$player_damage_to_heroes_all),4);
                $players[$key]['damage_taken_percent'] = (String)round(($item['damage_taken']/$player_damage_taken_all),4);
            }
            $battleInfo['players'] = $players;
            //计算团队的信息
            $battleInfo['static'] = $teamStatic;
        }
        return $battleInfo;
    }

    private static function getMatch($matchId)
    {
        $url = '/dota2/matches';
        $params = [
            'filter[id]' => $matchId,
        ];
        $jsonInfo = self::getRestInfo($url, $params);
        $info = json_decode($jsonInfo, true);
        // 获取当前battle的数据
        return $info[0];
    }

    private static function getBattle($battleId)
    {
        $url = '/dota2/games/' . $battleId;

        $jsonInfo = PandascoreBase::getInfo($url, []);
        $info = json_decode($jsonInfo, true);
        return $info;
    }

    public static function getTeamKills($teamId,$restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val){
            if($val['team']['id'] == $teamId){
                return $val['score'];
            }
        }
    }

    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal['id'];
            }
        }
    }

    public static function getGoalIdBySubType($subType)
    {
        $goal = EnumIngameGoal::find()->where(['=','sub_type',$subType])
            ->asArray()->one();
        if($goal){
            return $goal['id'];
        }
    }

    public static function getTeamToDamage($teamId,$restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val){
            if($val['team']['id'] == $teamId){
                return $val['kills'];
            }
        }
    }
    //兵营状态
    public static function conversionRestBuldingStatusBarracks($building_status,$building_status_barracks){
        if (empty($building_status_barracks)){
            return null;
        }
        $result = [];
        $melee_barrack_kills_num = 0;//近战兵营
        $ranged_barrack_kills_num = 0;//远程兵营
        $building_status_barracks_detail = [];
        //{"bottom_melee":true,"bottom_ranged":true,"middle_melee":true,"middle_ranged":true,"top_melee":true,"top_ranged":true}
        foreach ($building_status_barracks as $key => $item){
            $boolean_val = $item ? true : false;
            switch ($key){
                 case "bottom_melee":
                     if ($boolean_val == false){//false 就是对方摧毁的近战兵营
                         $melee_barrack_kills_num ++;
                     }
                     $building_status['barracks']['bot_melee_barrack']['is_alive'] = $boolean_val;
                     break;
                 case "bottom_ranged":
                     if ($boolean_val == false){//false 就是对方摧毁的远程兵营
                         $ranged_barrack_kills_num ++;
                     }
                     $building_status['barracks']['bot_ranged_barrack']['is_alive'] = $boolean_val;
                     break;
                 case "middle_melee":
                     if ($boolean_val == false){//false 就是对方摧毁的近战兵营
                         $melee_barrack_kills_num ++;
                     }
                     $building_status['barracks']['mid_melee_barrack']['is_alive'] = $boolean_val;
                     break;
                 case "middle_ranged":
                     if ($boolean_val == false){//false 就是对方摧毁的中路远程兵营
                         $ranged_barrack_kills_num ++;
                     }
                     $building_status['barracks']['mid_ranged_barrack']['is_alive'] = $boolean_val;
                     break;
                 case "top_melee":
                     if ($boolean_val == false){//false 就是对方摧毁的近战兵营
                         $melee_barrack_kills_num ++;
                     }
                     $building_status['barracks']['top_melee_barrack']['is_alive'] = $boolean_val;
                     break;
                 case "top_ranged":
                     if ($boolean_val == false){//false 就是对方摧毁的上路远程兵营
                         $ranged_barrack_kills_num ++;
                     }
                     $building_status['barracks']['top_ranged_barrack']['is_alive'] = $boolean_val;
                     break;
             }
        }
        $result['building_status'] = $building_status;
        $result['ranged_barrack_kills_num'] = $ranged_barrack_kills_num;
        $result['melee_barrack_kills_num'] = $melee_barrack_kills_num;
        return $result;
    }
    //防御塔状态
    public static function conversionRestBuldingStatusTowers($building_status,$building_status_towers){
        if (empty($building_status_towers)){
            return null;
        }
        $result = [];
        $tower_kills_num = 0;
        $building_status_towers_detail = [];
        //{"ancient_bottom":true,"ancient_top":true,"bottom_tier_1":false,"bottom_tier_2":true,"bottom_tier_3":true,"middle_tier_1":true,"middle_tier_2":true,"middle_tier_3":true,"top_tier_1":true,"top_tier_2":true,"top_tier_3":true}
        foreach ($building_status_towers as $key => $item){
            $boolean_val = $item ? true : false;
            if ($boolean_val == false){//false 就是对方摧毁的塔数量
                $tower_kills_num ++;
            }
            switch ($key){
                case "top_tier_1":
                    $building_status['towers']['top_tier_1_tower']['is_alive'] = $boolean_val;
                    break;
                case "top_tier_2":
                    $building_status['towers']['top_tier_2_tower']['is_alive'] = $boolean_val;
                    break;
                case "top_tier_3":
                    $building_status['towers']['top_tier_3_tower']['is_alive'] = $boolean_val;
                    break;
                case "middle_tier_1":
                    $building_status['towers']['mid_tier_1_tower']['is_alive'] = $boolean_val;
                    break;
                case "middle_tier_2":
                    $building_status['towers']['mid_tier_2_tower']['is_alive'] = $boolean_val;
                    break;
                case "middle_tier_3":
                    $building_status['towers']['mid_tier_3_tower']['is_alive'] = $boolean_val;
                    break;
                case "bottom_tier_1":
                    $building_status['towers']['bot_tier_1_tower']['is_alive'] = $boolean_val;
                    break;
                case "bottom_tier_2":
                    $building_status['towers']['bot_tier_2_tower']['is_alive']= $boolean_val;
                    break;
                case "bottom_tier_3":
                    $building_status['towers']['bot_tier_3_tower']['is_alive'] = $boolean_val;
                    break;
                case "ancient_top":
                    $building_status['towers']['top_tier_4_tower']['is_alive'] = $boolean_val;
                    break;
                case "ancient_bottom":
                    $building_status['towers']['bot_tier_4_tower']['is_alive'] = $boolean_val;
                    break;
            }
        }
        $result['tower_kills_num'] = $tower_kills_num;
        $result['building_status'] = $building_status;
        return $result;
    }
    //处理玩家的道具
    private static function conversionDota2PlayerItems($playerItems){
        $inventory = $backpack = $stash = $buffs = [];
        //首先判断他有几个 大于6个就放到backpack 背包
        $playerItemsNum = count($playerItems);
        $inventory = self::getFormatFrameItem( $playerItems, 'itemsInventory');
        $backpackArr = self::getFormatFrameItem(null,'itemsBackpack');
        $neutralArr = self::getFormatFrameItem(null,'itemNeutral');
        $stashArr = self::getFormatFrameItem(null,'itemsStash');
        $mob_inventoryArr = self::getFormatFrameItem(null,'itemsBear');
        $mob_backpackArr = self::getFormatFrameItem(null,'mob_backpack');
        $returnArr['inventory'] = $inventory;//物品栏
        $returnArr['backpack'] = $backpackArr;//背包
        $returnArr['neutral'] = $neutralArr;//野外掉落
        $returnArr['stash'] = $stashArr;//储藏处
        $returnArr['buffs'] = null;//增益
        $returnArr['mob_inventory'] = $mob_inventoryArr;//召唤物物品栏
        $returnArr['mob_backpack'] = $mob_backpackArr;//召唤物背包
        $returnArr['mob_neutral'] = null;//召唤物野外掉落
        return json_encode($returnArr,320);
    }

    //道具
    public static function getFormatFrameItem( $item, $itemKey){

        $inventoryArr = [];

        $mutiArr = [
            'itemsInventory',
            'itemsBackpack',
            'itemsStash',
            'itemsBear',
            'mob_backpack',
        ];
        if (in_array($itemKey,$mutiArr)){
            if (empty($item)){
                if ($itemKey=='itemsInventory'||$itemKey=='itemsStash'||$itemKey=='itemsBear'){
                    for ($i=1;$i<=6;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }else{
                    for ($i=1;$i<=3;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }
            }else{
                $inventoryArrRes = [];
                $i = 1;
                foreach ($item as $k => $v) {
                    $i = $k + 1;

                    $inventoryArrData = [];
                    $dbItemId =  self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$v['id'],3);
                    $dbItem         = MetadataDota2Item::find()->where(['id'=>$dbItemId])->asArray()->one();
                    if ($dbItem['name_cn'] == '回城卷轴'){
                        continue;
                    }
                    $inventoryArrData['item_id']         = (int)$dbItem['id'];
                    $inventoryArrData['purchase_time']   = null;
                    $inventoryArrData['cooldown']        = null;
                    $inventoryArrData['total_cost']        = $dbItem['total_cost'];
                    $inventoryArrRes[] = $inventoryArrData;

                }
                $total_cost_array = array_column($inventoryArrRes,'total_cost');
                array_multisort($total_cost_array,SORT_DESC,$inventoryArrRes);
                foreach ($inventoryArrRes as $k=>$v){
                    unset($inventoryArrRes[$k]['total_cost']);
                }
                if ($itemKey == 'itemsInventory' || $itemKey == 'itemsStash' || $itemKey == 'itemsBear') {
                    if ($i < 6) {
                        for ($j = $i+1; $j <= 6; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                    if ($i > 6) {
                        $inventoryArrRes = array_slice($inventoryArrRes,0,6);
                    }
                }else{
                    if ($i < 3) {
                        for ($j = $i+1; $j <= 3; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                }

                $inventoryArrMiddle = [];
                foreach ($inventoryArrRes as $index_key => $val){
                    $inventoryKey = $index_key + 1;
                    $inventoryArrMiddle['slot_'.$inventoryKey] = $val;
                }
                $inventoryArr = $inventoryArrMiddle;
            }

        }else{
            if (empty($item)){
                return null;
            }
            $dbItemId =  self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],3);
            $dbItem         = MetadataDota2Item::find()->where(['id'=>$dbItemId])->asArray()->one();
            if (empty($dbItem)){
                return null;
            }
            $inventoryArr['item_id']         = (int)$dbItem['id'];
            $inventoryArr['purchase_time']   = null;
            $inventoryArr['cooldown']        = null;
        }
        return $inventoryArr;
    }
    //处理技能升级
    private static function conversionDota2PlayerAbilities($playerAbilities){
        $abilities_timeline = [];
        $talents = [];
        if (count($playerAbilities)>0){
            $ability_level = 1;
            $ability_level_arr = [];
            $talent_level_arr = [];
            foreach ($playerAbilities as $key => $item){
                //当前英雄等级
                $hero_level = $item['level'];
                if (strstr($item['name'],'special_bonus')){
                    //天赋
                    $talent_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_talent',$item['id'],3);
                    $talents[$key]['talent_id'] = $talent_id;
                    $talents[$key]['is_chosen'] = true;
                    //查询技能ID
//                    $ability_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_ability',$item['id'],3);
                    $talent_level_arr[$item['id']]  =  array_key_exists($item['id'],$talent_level_arr)?$talent_level_arr[$item['id']]+1:1;
                    $item_abilities_timeline = [
                        'ingame_timestamp' => null,
                        'hero_level' => $hero_level,
                        'type' => 'talent',
                        'ability_level' => $talent_level_arr[$item['id']],
                        'talent_id' => $talent_id
                    ];
                    $abilities_timeline[$key] = [$item_abilities_timeline];
                    continue;
                }

                //查询技能ID
                $ability_id = self::getMainIdByRelIdentityIdOrUnknown('dota2_ability',$item['id'],3);
                $ability_level_arr[$item['id']]  =  array_key_exists($item['id'],$ability_level_arr)?$ability_level_arr[$item['id']]+1:1;
                $item_abilities_timeline = [
                    'ingame_timestamp' => null,
                    'hero_level' => $hero_level,
                    'type' => 'ability',
                    'ability_level' => $ability_level_arr[$item['id']],
                    'ability_id' => $ability_id
                ];
                $abilities_timeline[$key] = [$item_abilities_timeline];
            }
        }
        $abilities_timeline = array_values($abilities_timeline);
        $talents = array_values($talents);
        $abilities['abilities_timeline'] = json_encode($abilities_timeline,320);
        $abilities['talents'] = json_encode($talents,320);
        return $abilities;
    }
    private static function getMatchMapInfo($matchFromRest, $matchId)
    {
        // 主队客队绑定,
        // 取match信息中的战队信息，与rest中对比，如果不对，报警
        // 根据主队中的映射关系，获取对应关系
        $matchInfo = Match::find()->where(['id' => $matchId])->one();
        $team1Id = $matchInfo['team_1_id'];
        $team2Id = $matchInfo['team_2_id'];

        $teamFromRestMapTeamId = [];
//        $teamMap = [];
        $hasOrder = true;
        foreach ($matchFromRest['results'] as $key => $val) {
            $info = self::getTeamFromRel($val['team_id']);
            $order = $info && $matchInfo ? ($matchInfo['team_1_id'] == $info['id'] ? 1 : ($matchInfo['team_2_id'] == $info['id'] ? 2 : 0)) : 0;
            $teamFromRestMapTeamId[$val['team_id']] = [
                'team_id' => $info['id'],
                'order' => $order,
                'origin_order' => $key
            ];
//            $teamFromRestMapOrder[$key] = [
//                'team_id' => $info['id'],
//                'order' => $order];
            if (!$order) {
                $hasOrder = false;
            }
        }
        // 如果是false,表示不能找到所有战队的映射关系，用原版的排序
        if (!$hasOrder) {
            foreach ($teamFromRestMapTeamId as $key => $val) {
                $teamFromRestMapTeamId[$key]['order'] = $teamFromRestMapTeamId[$key]['origin_order'] + 1;
            }
        }
        return $teamFromRestMapTeamId;
    }

    private static function getTeamFromRel($teamId)
    {
        $teamInfo = Team::find()
            ->leftJoin('data_standard_master_relation rel', 'team.id=rel.master_id and rel.resource_type="team"')
            ->leftJoin('standard_data_team std', 'std.id=rel.standard_id')
            ->where(['std.rel_identity_id' => $teamId])->one();
        return $teamInfo;
    }
}