<?php
/**
 *
 */

namespace app\modules\task\services\hot\pandascore;


use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class PandascoreHotBase extends HotBase
{
    public static function getRestInfo($action,$params)
    {
        return PandascoreBase::getInfo($action,$params);
    }

    public static function getTeamOrderByTeamRelId($teamRelId,$matchInfo,$originId = 3)
    {
//        $masterId=self::getMainIdByRelIdentityId('team',$teamRelId,$originId,1);
        $masterId=self::getMainIdByRelIdentityId('team',$teamRelId,$originId);
        if($matchInfo['team_1_id']==$masterId){
            return 1;
        }
        if($matchInfo['team_2_id']==$masterId){
            return 2;
        }
        throw new BusinessException([],'没有找到这个比赛的信息，请检查战队绑定状态:'.$teamRelId);
    }
}