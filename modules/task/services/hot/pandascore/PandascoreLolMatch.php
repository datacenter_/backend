<?php
/**
 *
 */

namespace app\modules\task\services\hot\pandascore;


use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\TaskService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\services\battle\LolBattleService;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\db\Query;

class PandascoreLolMatch extends PandascoreHotBase implements HotMatchInterface
{
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $timeStart=date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $hotId = $info['id'];
        $hotInfo = self::getHotInfo($hotId);
        $matchId=$hotInfo['match_id'];
        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,3);
        if (empty($relIdentityId)){
            return "比赛没有绑定的relMatchId";
        }
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        //$relIdentityId=$hotInfo['rel_identity_id'];
        $matchInfo=Match::find()->where(['id'=>$matchId])->one();
        //请求PS的rest API 结果
        $restInfos = self::getInfos($relIdentityId);
        if (isset($restInfos['match']) && count($restInfos['battles']) > 0){
            // 这里传matchInfo，用来判断主队客队
            $infoFormat = self::conversion($Redis,$restInfos,$matchInfo);
            $infoFormatAddBindingInfo=self::addBindingInfo($Redis,$infoFormat,$matchInfo);
            $timeEnd=date('Y-m-d H:i:s');
            $extInfo=[
                'time_begin'=>$timeStart,
                'time_end'=>$timeEnd,
                'task_id'=>$taskInfo['id'],
                'match_id'=>$matchId,
            ];
            // 添加绑定信息，转义,主要出现在battle的详情中
            self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
            // 更新到数据库
            $refreshRes = self::refreshInfo($matchId,$infoFormatAddBindingInfo,$refreshType);
            // 更新hotdata状态
            self::refreshHotDataStatusByMatchInfo($hotId,$refreshRes);
        }
        return 'success';
    }

    public static function addBindingInfo($Redis,$infoFormat,$matchInfo)
    {
        $teamInfoRes = [];
        $teamInfoRes[$infoFormat['match']['team1_id']] = null;
        $teamInfoRes[$infoFormat['match']['team2_id']] = null;
        $matchId = $matchInfo['id'];
        //转化队伍
        $team1trueID = self::getTeamOrderByTeamRelId($infoFormat['match']['team1_id'],$matchInfo);
        if ($team1trueID == 2){
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_2_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_1_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 2;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 1;
        }else{
            $infoFormat['match']['team_1_score'] = $infoFormat['match']['team_1_score_old'];
            $infoFormat['match']['team_2_score'] = $infoFormat['match']['team_2_score_old'];
            $teamInfoRes[$infoFormat['match']['team1_id']] = 1;
            $teamInfoRes[$infoFormat['match']['team2_id']] = 2;
        }
        //$team2trueID = self::getTeamOrderByTeamRelId($infoFormat['match']['team2_id'],$matchInfo);
        if (isset($infoFormat['match']['default_advantage']) && $infoFormat['match']['default_advantage']){
            $infoFormat['match']['default_advantage'] = $teamInfoRes[$infoFormat['match']['default_advantage']];
        }
        if (isset($infoFormat['match']['winner']) && $infoFormat['match']['winner']){
            $infoFormat['match']['winner'] = $teamInfoRes[$infoFormat['match']['winner']];
        }
//        $infoFormat['match']['map_info'] = json_encode([1]);
//        for ($i=0;$i<3;$i++){
//            $maps[] = ['map'=>1];
//        }
//        $infoFormat['match']['map_info'] = json_encode($maps,320);
        foreach($infoFormat['battles'] as &$battle){
            if (empty($battle)){
                continue;
            }
            $end_battle_ingame_timestamp = $battle['base']['duration'];
            $battle_winner = null;
            if (isset($battle['base']['winner']) && $battle['base']['winner']){
                $battle['base']['winner'] = $teamInfoRes[$battle['base']['winner']];
                $battle_winner = $battle['base']['winner'];
            }
            //记录战队
            $team_array = [];
            $winner_team_order_id_relation_array = [];
            foreach($battle['static'] as &$static){
                if ($battle_winner == $static['order']){
                    $static['score'] = 1;
                }else{
                    $static['score'] = 0;
                }
                if ($battle['base']['is_forfeit'] != 1){
                    $static['team_id']=self::getMainIdByRelIdentityId('team',$static['rel_team_id'],'3');
                    $team_array[$static['rel_team_id']] = @$static['team_id'];
                    //转换team_rift_herald_kill
                    //$team_faction_rift_herald_kill = $static['faction']."_team_rift_herald_kill";
                    //$static['rift_herald_kills'] = empty(@$battle[$team_faction_rift_herald_kill]) ? 0 : $battle[$team_faction_rift_herald_kill];
                }
                $winner_team_order_id_relation_array[$static['order']] = $static['team_id'];
            }
            // 更新选手中的选手id，战队id，item中的装备
            if (isset($battle['players']) && count($battle['players']) > 0) {
                $team1_seed = $team2_seed = 1;
                foreach ($battle['players'] as $key => &$player) {
                    $player['game'] = 2;
                    $player['match'] = $matchId;
                    $player['team_id'] = @$team_array[$player['rel_team_id']];
                    $player['champion'] = @self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $player['champion'], '3');
                    $player['player_id'] = @self::getMainIdByRelIdentityId('player', $player['player_id'], '3');
                    if ($player['player_id']){
                        //获取选手的昵称
                        $player_detail = Player::find()->select('nick_name')->where(['id'=>$player['player_id']])->asArray()->one();
                        $player['nick_name'] = @$player_detail['nick_name'];
                    }
                    $spell1_info = self::getMainIdByRelIdentityIdOrUnknown('lol_summoner_spell', $player['spells'][0]['id'], '3');
                    if ($spell1_info){
                        $spell1 = $spell1_info;
                    }else{
                        $spell1 = self::getMetaDataUnknownId('lol_summoner_spell');
                    }
                    $spell1_array = [
                        'spell_id' => $spell1,
                        'cooldown' => null
                    ];
                    $spell2_info = self::getMainIdByRelIdentityIdOrUnknown('lol_summoner_spell', $player['spells'][1]['id'], '3');
                    if ($spell2_info){
                        $spell2 = $spell2_info;
                    }else{
                        $spell2 = self::getMetaDataUnknownId('lol_summoner_spell');
                    }
                    $spell2_array = [
                        'spell_id' => $spell2,
                        'cooldown' => null
                    ];
                    $player['summoner_spells'] = json_encode([$spell1_array, $spell2_array]);  //召唤师技能
                    $originPlayerRole = $player['role'];//角色
                    $player_role = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE, $originPlayerRole);
                    $player['role'] = $player_role;  //角色
                    $player['seed'] = $player_role;
//                    if ($player['team_order'] == 1){
//                        $player['seed'] = $team1_seed;//选手排序
//                        $team1_seed ++;
//                    }
//                    if ($player['team_order'] == 2){
//                        $player['seed'] = $team2_seed;//选手排序
//                        $team2_seed ++;
//                    }
                    $player['order'] = $key + 1;//排序
                    $player['lane'] = Common::getLolLaneId('pandascore',$player_role);
                    $player['items'] = self::conversionPlayerItemsApi($Redis,$player['items'],3);
                }
            }
            //事件  $infoFormat['battles']['detail']['']
            if(isset($battle['events']) && $battle['events']){
                foreach($battle['events'] as $key => &$event){
                    //循环处理events
                    $event = self::transEvents($event,$matchId,$matchInfo);
                    if ($event['is_first_event'] == 1){
                        //在处理一下第一次的详情
                        $FirstEventsDetail = self::conversionFirstEventsDetail($Redis,$event);
                        if ($FirstEventsDetail){
                            $battle['detail'] = array_merge($battle['detail'],$FirstEventsDetail);
                        }
                    }
                }
                //添加battle_start
                $battle_start_event = [
                    'game' => $matchInfo['game'],
                    'match' => $matchInfo['id'],
                    'order' => 1,
                    'ingame_timestamp' => (Int)$end_battle_ingame_timestamp,
                    'event_type' => 'battle_start',
                    'killer' => $battle['static'][1]['team_id'],//蓝队
                    'killer_faction' => (string)$battle['static'][1]['order'],
                    'victim' => $battle['static'][2]['team_id'],//红队
                    'victim_faction' => (string)$battle['static'][2]['order'],
                    'is_first_event' => 2,
                    'first_event_type' => 0
                ];
                //$battle['events'][1] = $battle_start_event;
                array_unshift($battle['events'],$battle_start_event);
                if ($battle_winner){
                    $battle_end_order = count($battle['events']) + 1;
                    $battle_end_event = [
                        'game' => $matchInfo['game'],
                        'match' => $matchInfo['id'],
                        'order' => $battle_end_order,
                        'ingame_timestamp' => (Int)$end_battle_ingame_timestamp,
                        'event_type' => 'battle_end',
                        'killer' => $winner_team_order_id_relation_array[$battle_winner],
                        'killer_faction' => (string)$battle_winner,
                        'is_first_event' => 2,
                        'first_event_type' => 0
                    ];
                    $battle['events'][] = $battle_end_event;
                }
            }
            //ban_pick
            if(count($battle['ban_pick'])>0){
                foreach ($battle['ban_pick'] as &$ban_pick_val){
                    $ban_pick_val['hero'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$ban_pick_val['hero'],'3');
                    $ban_pick_val['team'] = @$team_array[$ban_pick_val['team']];
                }
            }
        }
        return $infoFormat;
    }

    public static function getInfosTest()
    {
        //
        $info=HotDataLog::find()->where(['id'=>17])->one();
        $infoRest=json_decode($info['info_rest'],true);
        return $infoRest;
    }

    public static function refreshInfo($matchId, $info,$refreshType)
    {
        $battle_complete = true;
        $battle_order_num = 0;
        $battle_is_forfeit_order_id = null;
        if ($refreshType == 'refresh'){
            //有battle
            $battleList = MatchBattle::find()->where(['match'=>$matchId])->asArray()->all();
            if (count($battleList)){
                foreach ($battleList as $key =>$item){
                    //判断默认领先
                    $battle_is_default_advantage = $item['is_default_advantage'];
                    if ($battle_is_default_advantage == 1){
                        $battle_order_num = $item['order'];
                    }
                }
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }

            }
        }else{
//            $info['match']['default_advantage'] = 1;//测试用
            //判断是不是有默认领先
            if (isset($info['match']['default_advantage']) && $info['match']['default_advantage']){
                HotBase::createMatchAdvantageBattle($matchId,$info['match']['default_advantage'],0,1,$type='rest_api',3);
                $battle_order_num = 1;
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    $battle_complete = @$battle['base']['complete'];
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }

            }
        }
        //更新match层信息
        $match_base_info = [
            'game_version' => @$info['match']['game_version']
        ];
        self::setMatchBase($matchId, $match_base_info);
        unset($info['match']['game_version']);
        self::setMatch($matchId, $info['match']);
        return $battle_complete;
    }

    private static function refreshBattle($matchId, $battle, $key=0,$refreshType = '')
    {
        //判断有没有对局时长 没有就不创建battle
        if (!isset($battle['base']['duration'])){
            return true;
        }
        $have_battle = false;
        // 找到对应的battleId，更新battle
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
            $have_battle = true;
        }
        $battleId=$battleBase['id'];
        if ($have_battle){
            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleId,'match'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');
        }

        BattleService::setBattleInfo($battleId,'lol',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'lol',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId,'lol',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'lol',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        if(isset($battle['events']) && count($battle['events']) > 0){
            //处理覆盖逻辑
            $events_res = self::chuliEvents($matchId,$battleId,$battle['events']);
            $battle['events'] = $events_res['events_res'];
            BattleService::setBattleInfo($battleId,'lol',$battle['events'],BattleService::DATA_TYPE_EVENTS);
            //需要删除的事件 列表
            $need_del_list = $events_res['need_del_list'];
            if ($need_del_list){
                LolBattleService::delEvents($battleId,$need_del_list);
            }
        }
        //特殊事件
        if (isset($battle['first_events']) && $battle['first_events']){
            BattleService::setBattleInfo($battleId,'lol',$battle['first_events'],BattleService::DATA_TYPE_FIRST_EVENTS);
        }
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'lol',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        //判断关联关系
        self::setMatchBattleRelation($matchId,$battleId,$battle);
    }

    private static function setMatchBattleRelation($matchId,$battleId,$battle){
        //插入关联关系
        $rel_matchId = @$battle['base']['rel_match_id'];
        $rel_battleId = @$battle['base']['rel_battle_id'];
        $battle_order = $battle['base']['order'];
        $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battle_order])->one();
        if (!$matchBattleRelation){
            //首先插入关系表
            $matchBattleRelation = new MatchBattleRelation();
            $matchBattleRelation->setAttributes([
                'match_id' => (int)$matchId,
                'order' => $battle_order,
                'rel_match_id' => (string)$rel_matchId,
                'rel_battle_id' => (string)$rel_battleId
            ]);
            if (!$matchBattleRelation->save()) {
                throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
            }
        }
        //设置redis 关系
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        $Redis->hset($redis_name,$rel_battleId,$battleId.'-'.$battle_order);
    }
    //处理覆盖
    public static function chuliEvents($match_id,$battle_id,$events){
        $events_res = [];
        $need_del_list = [];
        $events_building_kill_list = $events_player_kill_list = $events_elite_kill_list = [];
        //查询battle_id 的所有events
        $MatchBattleEventLolList = MatchBattleEventLol::find()->where(['battle_id'=>$battle_id])->asArray()->all();
        if ($MatchBattleEventLolList){
            $events_num = count($MatchBattleEventLolList);
            foreach ($events as $key => $item){
                $events_type = $item['event_type'];
                switch ($events_type){
                    case 'player_kill':
                        $events_player_kill_list[] = $item;
                        break;
                    case 'player_suicide':
                        $events_player_kill_list[] = $item;
                        break;
                    case 'elite_kill':
                        $events_elite_kill_list[] = $item;
                        break;
                    case 'building_kill':
                        $events_building_kill_list[] = $item;
                        break;
                }
            }
            //处理杀人事件
            if ($events_player_kill_list){
                $playerkillListRes = self::chuliPlayerKill($events_player_kill_list,$MatchBattleEventLolList,$events_num);
                if (count($playerkillListRes['events'])>0){
                    $events_num = $playerkillListRes['events_num'];
                    foreach ($playerkillListRes['events'] as $val){
                        $events_res[] = $val;
                    }
                }
                if (count($playerkillListRes['old_events_list'])>0){
                    $need_del_list[] = $playerkillListRes['old_events_list'];
                }
            }
            //处理推塔
            if ($events_building_kill_list){
                //处理建筑
                $buildingkillListRes = self::chuliBuildingKill($events_building_kill_list,$MatchBattleEventLolList,$events_num);
                if (count($buildingkillListRes['events'])>0){
                    $events_num = $buildingkillListRes['events_num'];
                    foreach ($buildingkillListRes['events'] as $val){
                        $events_res[] = $val;
                    }
                }
                if (count($buildingkillListRes['old_events_list'])>0){
                    $need_del_list[] = $buildingkillListRes['old_events_list'];
                }
            }
            //处理杀龙
            if ($events_elite_kill_list){
                //处理建筑
                $elitekillListRes = self::chuliEliteKill($events_elite_kill_list,$MatchBattleEventLolList,$events_num);
                if (count($elitekillListRes['events'])>0){
                    $events_num = $elitekillListRes['events_num'];
                    foreach ($elitekillListRes['events'] as $val){
                        $events_res[] = $val;
                    }
                }
                if (count($elitekillListRes['old_events_list'])>0){
                    $need_del_list[] = $elitekillListRes['old_events_list'];
                }
            }
        }else{
            $events_res = $events;
        }
        $result = [
            'need_del_list'=>$need_del_list,
            'events_res'=>$events_res,
        ];
        return $result;
    }
    //处理 事件的 player kill
    private static function chuliPlayerKill($events_player_kill_list,$old_events,$events_num){
        $events_player_kill_list_new = [];
        foreach ($events_player_kill_list as $key => &$value){
            $ingame_timestamp = (Int)$value['ingame_timestamp'];
            $start_timestamp = $ingame_timestamp - 5;
            $end_timestamp = $ingame_timestamp + 30;
            $value['merge_flag'] = 0;//
            $events_player_kill_pipei = 0;
            foreach ($old_events as $key1 => &$item){
                if ($events_player_kill_pipei == 1){
                    continue;
                }
                if ($item['merge_flag'] == 1){
                    continue;
                }
                if ($item['event_type'] != 'player_kill'){
                    $item['merge_flag'] = 1;
                    if ($item['event_type'] == 'player_suicide'){
                        $item['merge_flag'] = 0;
                    }
                }else{
                    $item['merge_flag'] = 0;
                }
                if ($start_timestamp < $item['ingame_timestamp'] && $end_timestamp > $item['ingame_timestamp']){
                    if ($value['victim'] == $item['victim']){
                        //判断新的击杀者 是不是 unknown 51 是unknown
                        if ($value['killer_ingame_obj_type'] == 51 ){
                            $value['order'] = $item['order'];
                            $value['merge_flag'] = 1;
                            $item['merge_flag'] = 1;
                            $events_player_kill_pipei = 1;
                            unset($value['killer']);
                            unset($value['killer_player_name']);
                            unset($value['killer_player_rel_name']);
                            unset($value['killer_champion_id']);
                            unset($value['killer_faction']);
                            unset($value['killer_ingame_obj_type']);
                            unset($value['killer_sub_type']);
                            unset($value['event_type']);
//                            continue;
                        }
                        if ($value['killer_ingame_obj_type'] == 50 && $item['killer_ingame_obj_type'] == 50){
                            if ($value['killer'] == $item['killer']){
                                $value['order'] = $item['order'];
                                $value['merge_flag'] = 1;
                                $item['merge_flag'] = 1;
                                $events_player_kill_pipei = 1;
//                                continue;
                            }
                        }
                    }
                }
            }
            if ($value['merge_flag'] == 0){
                $events_num = $events_num + 1;
                $value['order'] = $events_num;
                $value['merge_flag'] = 1;
                //$item['merge_flag'] = 1;
            }
            $events_player_kill_list_new[] = $value;
        }
        $old_events_list = [];
        //最终循环一下 是不是有没有匹配上的老书记
        foreach ($old_events as $val){
            if ($val['merge_flag'] == 0 && $val['event_type'] == 'player_kill'){
                $old_events_list = $val;
            }
        }
        $result = [
            'old_events_list'=>$old_events_list,
            'events' => $events_player_kill_list_new,
            'events_num' => $events_num
        ];
        return $result;
    }
    //处理 事件的 elite Kill
    public static function chuliEliteKill($events_elite_kill_list,$old_events,$events_num){
        $events_elite_kill_list_new = [];
        foreach ($events_elite_kill_list as $key => &$value){
            $ingame_timestamp = (Int)$value['ingame_timestamp'];
            $start_timestamp = $ingame_timestamp - 10;
            $end_timestamp = $ingame_timestamp + 30;
            $value['merge_flag'] = 0;//
            $events_elite_kill_pipei = 0;
            foreach ($old_events as $key1 => &$item){
                if ($events_elite_kill_pipei == 1){
                    continue;
                }
                if ($item['merge_flag'] == 1){
                    continue;
                }
                if ($item['event_type'] != 'elite_kill' ){
                    $item['merge_flag'] = 1;
                }else{
                    $item['merge_flag'] = 0;
                }
                if ($start_timestamp < $item['ingame_timestamp'] && $end_timestamp > $item['ingame_timestamp']){
                    if ($item['event_type']=='elite_kill' && $value['victim'] == $item['victim'] && $value['killer_ingame_obj_type'] == 50){
                        $value['order'] = $item['order'];
                        $value['merge_flag'] = 1;
                        $item['merge_flag'] = 1;
                        $events_elite_kill_pipei = 1;
                        continue;
                    }
                }
            }
            if ($value['merge_flag'] == 0){
                $events_num = $events_num + 1;
                $value['order'] = $events_num;
                $value['merge_flag'] = 1;
            }
            $events_elite_kill_list_new[] = $value;
        }
        $old_events_list = [];
        //最终循环一下 是不是有没有匹配上的
        foreach ($old_events as $val){
            if ($val['merge_flag'] == 0 && $val['event_type'] == 'elite_kill'){
                $old_events_list = $val;
            }
        }
        $result = [
            'old_events_list'=>$old_events_list,
            'events' => $events_elite_kill_list_new,
            'events_num' => $events_num
        ];
        return $result;
    }
    //处理 事件的 BuildingKill
    public static function chuliBuildingKill($events_building_kill_list,$old_events,$events_num){
        $events_building_kill_list_new = [];
        foreach ($events_building_kill_list as $key => &$value){
            $ingame_timestamp = (Int)$value['ingame_timestamp'];
            $start_timestamp = $ingame_timestamp - 10;
            $end_timestamp = $ingame_timestamp + 20;
            $value['merge_flag'] = 0;//
            $events_building_kill_pipei = 0;
            foreach ($old_events as $key1 => &$item){
                if ($events_building_kill_pipei == 1){
                    continue;
                }
                if ($item['merge_flag'] == 1){
                    continue;
                }
                if ($item['event_type'] != 'building_kill' ){
                    $item['merge_flag'] = 1;
                }else{
                    $item['merge_flag'] = 0;
                }
                if ($start_timestamp < $item['ingame_timestamp'] && $end_timestamp > $item['ingame_timestamp']){
                    if ($item['event_type']=='building_kill' && $value['victim'] == $item['victim'] && $value['killer_ingame_obj_type'] == 50){
                        //判断是玩家
                        if ($item['killer_ingame_obj_type'] == 50 && $value['killer'] == $item['killer']){
                            $value['order'] = $item['order'];
                            $value['merge_flag'] = 1;
                            $item['merge_flag'] = 1;
                            $events_building_kill_pipei = 1;
                            continue;
                        }
                    }
                    if ($item['event_type']=='building_kill' && $value['victim'] == $item['victim'] && $value['killer_ingame_obj_type'] != 50){
//                        if($item['killer_ingame_obj_type'] == 8) {
                            $value['order'] = $item['order'];
                            $value['merge_flag'] = 1;
                            $item['merge_flag'] = 1;
                            $events_building_kill_pipei = 1;
                            continue;
//                        }
//                        if($item['killer_ingame_obj_type'] == 1) {
//                            $value['order'] = $item['order'];
//                            $value['merge_flag'] = 1;
//                            $item['merge_flag'] = 1;
//                            unset($value['killer']);
//                            unset($value['killer_player_name']);
//                            unset($value['killer_player_rel_name']);
//                            unset($value['killer_champion_id']);
//                            unset($value['killer_faction']);
//                            unset($value['killer_ingame_obj_type']);
//                            unset($value['killer_sub_type']);
//                            $events_building_kill_pipei = 1;
//                            continue;
//                        }
                    }
                }
            }
            if ($value['merge_flag'] == 0){
                $events_num = $events_num + 1;
                $value['order'] = $events_num;
                $value['merge_flag'] = 1;
//                $item['merge_flag'] = 1;
            }
            $events_building_kill_list_new[] = $value;
        }
        $old_events_list = [];
        //最终循环一下 是不是有没有匹配上的老书记
        foreach ($old_events as $val){
            if ($val['merge_flag'] == 0 && $val['event_type'] == 'building_kill'){
                $old_events_list = $val;
            }
        }
        $result = [
            'old_events_list'=>$old_events_list,
            'events' => $events_building_kill_list_new,
            'events_num' => $events_num
        ];
        return $result;
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    private static function setMatchBase($id, $attribute)
    {
        $matchBase = MatchBase::find()->where(['match_id' => $id])->one();
        if ($matchBase) {
            $matchBase->setAttributes($attribute);
            if (!$matchBase->save()) {
                throw new BusinessException($matchBase->getErrors(), "设置match_base_data失败");
            }
        }
        return true;
    }

    public static function getStandardMatchInfo($standardId)
    {
        return StandardDataMatch::find()->where(['id' => $standardId])->one();
    }

    public static function getBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('match.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('match', 'rel.master_id=match.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_MATCH]);
        return $q->one();
    }

    public static function getInfos($relMatchId)
    {
        $matchInfo = self::getMatch($relMatchId);
        $battles = $matchInfo['games'];
        $battleInfos = [];
        if ($battles){
            //数据源创建5个battle，当battle是空的时候不保存
            foreach ($battles as $key => $battle) {
                $battleId = $battle['id'];
                if ($battle['status'] == 'not_started' || $battle['status'] == 'canceled' ) {
                    unset($battles[$key]);
                    continue;
                }else{
                    $battleBase = self::getBattle($battleId);
                    $events = self::getEvents($battleId);
                    $frames = self::getFrames($battleId);
                    $battleInfos[] = [
                        'base' => $battleBase,
                        'events' => $events,
                        'frames' => $frames,
                    ];
                }
            }
        }
        return [
            'match' => $matchInfo,
            'battles' => $battleInfos
        ];
    }

    private static function getMatchMapInfo($matchFromRest, $matchId)
    {
        // 主队客队绑定,
        // 取match信息中的战队信息，与rest中对比，如果不对，报警
        // 根据主队中的映射关系，获取对应关系
        $matchInfo = Match::find()->where(['id' => $matchId])->one();
        $team1Id = $matchInfo['team_1_id'];
        $team2Id = $matchInfo['team_2_id'];

        $teamFromRestMapTeamId = [];
//        $teamMap = [];
        $hasOrder = true;
        foreach ($matchFromRest['results'] as $key => $val) {
            $info = self::getTeamFromRel($val['team_id']);
            $order = $info && $matchInfo ? ($matchInfo['team_1_id'] == $info['id'] ? 1 : ($matchInfo['team_2_id'] == $info['id'] ? 2 : 0)) : 0;
            $teamFromRestMapTeamId[$val['team_id']] = [
                'team_id' => $info['id'],
                'order' => $order,
                'origin_order' => $key
            ];
//            $teamFromRestMapOrder[$key] = [
//                'team_id' => $info['id'],
//                'order' => $order];
            if (!$order) {
                $hasOrder = false;
            }
        }
        // 如果是false,表示不能找到所有战队的映射关系，用原版的排序
        if (!$hasOrder) {
            foreach ($teamFromRestMapTeamId as $key => $val) {
                $teamFromRestMapTeamId[$key]['order'] = $teamFromRestMapTeamId[$key]['origin_order'] + 1;
            }
        }
        return $teamFromRestMapTeamId;
    }

    private static function getTeamFromRel($teamId)
    {
        $teamInfo = Team::find()
            ->leftJoin('data_standard_master_relation rel', 'team.id=rel.master_id and rel.resource_type="team"')
            ->leftJoin('standard_data_team std', 'std.id=rel.standard_id')
            ->where(['std.rel_identity_id' => $teamId])->one();
        return $teamInfo;
    }

    /**
     * @param $restInfo
     * @param $matchInfo 比赛基础信息这里有主队客队id
     * @return array
     *
     */
    public static function conversion($Redis,$restInfo,$matchInfo)
    {
        // 转化match
        $formatData = [
            'match' => '',
            'battles' => [],
        ];

        // 这里记录了关联的team
        $teamInitInfo = [];

        // 遍历result，这里面有主队客队对应关系

        $detailInfoConfig = [
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                if ($info['end_at']){
                    return date('Y-m-d H:i:s', strtotime($info['end_at']));
                }else{
                    return null;
                }
            },
            'team_1_score_old' => function ($info) {
                return $info['results'][0]['score'];
            },
            'team1_id' => function ($info) {
                return $info['results'][0]['team_id'];
            },
            'team_2_score_old' => function ($info) {
                return $info['results'][1]['score'];
            },
            'team2_id' => function ($info) {
                return $info['results'][1]['team_id'];
            },
            'is_battle_detailed' => function ($info) {
                return $info['detailed_stats'] ? 1 : 2;
            },
            'is_draw' => function ($info) {
                return $info['draw'] ? 1 : 2;
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            //比赛中 没有
//            'finished' => function($info) {
//                return $info['finished'] ? 3 : 2;
//            },
            'default_advantage' => function ($info) {
                return $info['game_advantage'] ? $info['game_advantage'] : null;
            },
            'status' => function($info){
                return Common::getPandaScoreMatchStatus($info['status']);
            },
            'winner' => 'winner_id',
            'game_version' => function ($info) {
                if ($info['videogame_version']){
                    return $info['videogame_version']['name'];
                }else{
                    return null;
                }
            }
        ];
        $matchDetailInfo = Mapping::transformation($detailInfoConfig, $restInfo['match']);
        $formatData['match'] = $matchDetailInfo;
        if (count($restInfo['battles']) > 0){
            foreach ($restInfo['battles'] as $key => $restBattle) {
                // 格式化battle信息，这里需要一些team的信息
                $battleInfo = self::conversionBattle($Redis,$restBattle, $restInfo,$matchInfo);
                $battleInfoEvent = self::conversionBattleEvent($Redis,$restBattle, $restInfo,$matchInfo);
                if($battleInfoEvent){
                    $formatData['battles'][] = array_merge($battleInfo,$battleInfoEvent);
                }else{
                    $formatData['battles'][] = $battleInfo;
                }
            }
        }
        return $formatData;
    }
    //处理events 事件
    private static function conversionBattleEvent($Redis,$restBattleInfo, $restInfo, $matchInfo)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'events' => '',
            'first_events' => '',
//            'blue_team_rift_herald_kill'=>0,
//            'red_team_rift_herald_kill'=>0
        ];
        //事件
        $eventListRst = $restBattleInfo['events'];
        if(isset($eventListRst['error']) && $eventListRst['error']){
            return [];
        }
        if (empty($eventListRst)){
            return [];
        }
        $event= $battle_events_detail = [];
        foreach ($eventListRst as $key => $eventRst) {
            $eventConfig = [
                "game" => 2,
                "event_type" => function ($params) {
                    if(isset($params['type']) && $params['type']){
                        return $params['type'];
                    }
                },//事件类型
                "ingame_timestamp" => "ingame_timestamp",//游戏内时间戳
                "killer_player_id" => function ($params) {
                    if(isset($params['payload']['killer']['object']['player_id']) && $params['payload']['killer']['object']['player_id']){
                        return $params['payload']['killer']['object']['player_id'];
                    }
                },//killer
                "killer_player_rel_name" => function ($params) {
                    if(isset($params['payload']['killer']['object']['player_name']) && $params['payload']['killer']['object']['player_name']){
                        return $params['payload']['killer']['object']['player_name'];
                    }
                },//killer
                "killer_champion_id" => function ($params) {
                    if(isset($params['payload']['killer']['object']['champion']['id']) && $params['payload']['killer']['object']['champion']['id']){
                        return $params['payload']['killer']['object']['champion']['id'];
                    }
                },//killer_champion_id
                "killer_faction" => function ($params) {
                    if(isset($params['payload']['killer']['object']['side']) && $params['payload']['killer']['object']['side']){
                        return $params['payload']['killer']['object']['side'];
                    }
                },//killer_faction
                "killer_object_name" => function($params) {
                    if(isset($params['payload']['killer']['object']['player_name']) && $params['payload']['killer']['object']['player_name']){
                        return $params['payload']['killer']['object']['player_name'];
                    }
                },
                "killer_object_other_name" => function($params) {
                    if(isset($params['payload']['killer']['object']['name']) && $params['payload']['killer']['object']['name']){
                        return $params['payload']['killer']['object']['name'];
                    }
                },
                "killer_ingame_obj_type" => function ($params) {
                    if(isset($params['payload']['killer']['type']) && $params['payload']['killer']['type']){
                        return $params['payload']['killer']['type'];
                    }
                },
                "killer_sub_type" =>function($params) {
                    if(isset($params['payload']['killer']['type']) && $params['payload']['killer']['type']){
                        return $params['payload']['killer']['type'];
                    }
                },
                //被害者
                "victim_name" => function ($params) {
                    if (isset($params['payload']['victim']['object']['name']) && $params['payload']['victim']['object']['name']){
                        return $params['payload']['victim']['object']['name'];
                    }
                },
                "victim_player_id" => function($params){
                    if(isset($params['payload']['victim']['object']['player_id']) && $params['payload']['victim']['object']['player_id']){
                        return $params['payload']['victim']['object']['player_id'];
                    }
                },
                "victim_player_rel_name" => function($params){
                    if(isset($params['payload']['victim']['object']['player_name']) && $params['payload']['victim']['object']['player_name']){
                        return $params['payload']['victim']['object']['player_name'];
                    }
                },
                "victim_champion_id" => function($params){
                    if(isset($params['payload']['victim']['object']['champion']['id']) && $params['payload']['victim']['object']['champion']['id']){
                        return $params['payload']['victim']['object']['champion']['id'];
                    }
                },
                "victim_faction" => function($params){
                    if(isset($params['payload']['victim']['object']['side']) && $params['payload']['victim']['object']['side']){
                        return $params['payload']['victim']['object']['side'];
                    }
                },
                "victim_ingame_obj_type" => function ($params) {
                    if(isset($params['payload']['victim']['type']) && $params['payload']['victim']['type']){
                        return $params['payload']['victim']['type'];
                    }
                },//受害者游戏内对象类型
                "victim_sub_type" =>function($params) {
                    if(isset($params['payload']['victim']['object']['type']) && $params['payload']['victim']['object']['type']){
                        return $params['payload']['victim']['object']['type'];
                    }
                },  //受害者子类型sub_type
                //助攻
                "assist" => function ($params) {
                    if(isset($params['payload']['assists']) && $params['payload']['assists']){
                        $assist_array = [];
                        foreach ($params['payload']['assists'] as $key => $assist){
                            $assist_array[$key]['assist_player_id'] = @$assist['object']['player_id'];
                            $assist_array[$key]['assist_player_name'] = @$assist['object']['player_name'];
                            $assist_array[$key]['assist_player_faction'] = @$assist['object']['side'];
                            $assist_array[$key]['assist_champion_id'] = @$assist['object']['champion']['id'];
                        }
                        return $assist_array;
                    }
                },//助攻
                "assist_ingame_obj_type" => function($params){
                    if(isset($params['payload']['assists'])){
//                        foreach ($params['payload']['assists'] as $assist){
//                            return $assist['type'];
//                        }
                        //暂时只是到是player
                        return 'player';
                    }
                },
                "assist_sub_type" => function($params){
                    if(isset($params['payload']['assists'])){
//                        foreach ($params['payload']['assists'] as $assist){
//                            return $assist['type'];
//                        }
                        //暂时只是到是player
                        return 'player';
                    }
                },
//                "is_first_event" => function ($params) {
//                    return @$params['is_first'] ? 1 : 2;
//                },//是否为第一次特殊事件
//                "first_event_type" => function ($params) {
//                    return $params['type'];
//                },
            ];
            $eventInfo = Mapping::transformation($eventConfig, $eventRst);
            $eventInfo['order'] = (int)$key+2;//为了增加battle_start 和end

            //设置初始值
            $killer_type = @$eventRst['payload']['killer']['type'];
            if ($killer_type == 'unknown'){
                $victim_type = @$eventRst['payload']['victim']['object']['type'];
                if ($victim_type == 'blue'){
                    $killer_team = 'red';
                }else{
                    $killer_team = 'blue';
                }
            }else{
                $killer_team = @$eventRst['payload']['killer']['object']['side'];
            }
            $killed_type = @$eventRst['payload']['victim']['type'];
//            if ($killed_type == 'rift_herald'){
//                //根据killer team 获取team
//                $battleInfo[$killer_team."_team_rift_herald_kill"] += 1;
//            }
            $first_event_Info = self::getRestFirstEventInfo($Redis,'rest_api_match_battle_event_info',$matchInfo['id'],$restBattleInfo['base']['id'],$eventRst,$killer_team,$killed_type,$killer_type);
            $is_first_event = $first_event_Info['is_first_event'];
            if ($is_first_event){
                $eventInfo['is_first_event'] = 1;
                $eventInfo['first_event_type'] = @Common::getPandaScoreEventNum($first_event_Info['first_event_type']);
                $eventInfo['first_event_type_string'] = $first_event_Info['first_event_type'];
                $battle_events_detail[] = $first_event_Info['battle_first_event_type'];
            }else{
                $eventInfo['is_first_event'] = 2;
                $eventInfo['first_event_type'] = 0;
                $eventInfo['first_event_type_string'] = null;
            }
            $event[] = $eventInfo;
        }
        $battleInfo['events'] = $event;
        $battleInfo['first_events'] = $battle_events_detail;
        return $battleInfo;
    }

    private static function conversionBattle($Redis,$restBattleInfo, $restInfo, $matchInfo)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];

        $battleInfoConfig = [
            'rel_battle_id' => 'id',
            'rel_match_id' => 'match_id',
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                if ($info['end_at']){
                    return date('Y-m-d H:i:s', strtotime($info['end_at']));
                }else{
                    return null;
                }
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            'status' => function ($info) {
                return $info['status'] == 'finished' ? 3 : 2;
            },
            'complete'=>'complete',
            'order' => 'position',
            'duration' => function ($info) {
                if ($info['length']){
                    return $info['length'];
                }else{
                    return null;
                }
            },
            'winner' => function($params){
                if(isset($params['winner']['id']) && $params['winner']['id']){
                    return $params['winner']['id'];
                }
            }
        ];
        $battleBaseAtt = Mapping::transformation($battleInfoConfig, $restBattleInfo['base']);
        if (isset($battleBaseAtt['duration'])){
            $battleBaseAtt['duration']=(string)$battleBaseAtt['duration'];//转换下格式
        }
        //base
        $battleBaseAtt['map'] = 1;  //数据源没该数据，取值为metadata_lol_map表的召唤师峡谷
        $battleBaseAtt['is_battle_detailed'] = 1;
        $battleBaseAtt['is_default_advantage'] = 2;
        $battleBaseAtt['is_draw'] = 2;
        $battleBaseAtt['flag'] = 1;
        $battleBaseAtt['deleted'] = 2;
        $battleBaseAtt['deleted_at'] = null;
        $battleBaseAtt['game'] = 2;
        // 获取battle详情,这里用来设置id
        $battleInfo['base'] = $battleBaseAtt;

        $teamOrderArray = $teamStatic = $players = [];
        $battleDetail = [];
        $battleDetail['is_confirmed'] = 1;
        $battleDetail['is_pause'] = 2;
        $battleDetail['is_live'] = 1;
        //处理战队
        $teamStaticColor = [];
        $teamInfosRest = $restBattleInfo['base']['teams'];
        //判断是不是弃权
        if ($battleBaseAtt['is_forfeit'] == 1){
            $battleInfo['base']['is_battle_detailed'] = 2;
            $battleInfo['players'] = $players;

            $teamStatic[1]['team_id'] = $matchInfo['team_1_id'];
            $teamStatic[1]['order']= 1;
            $teamStatic[2]['team_id'] = $matchInfo['team_2_id'];
            $teamStatic[2]['order']= 2;
            //计算团队的信息
            $battleInfo['static'] = $teamStatic;
            $battleInfo['detail'] = $battleDetail;
            $battleInfo['ban_pick'] = [];
        }else {
            if (empty($teamInfosRest)){
                return [];
            }
            foreach ($teamInfosRest as $key => $team) {
                $team_order = self::getTeamOrderByTeamRelId(@$team['team']['id'], $matchInfo);
                $teamOrderArray[$team['team']['id']] = $team_order;
                // 一血
                if ($team['first_blood']) {
                    $battleDetail['first_blood_p_tid'] = $team_order;
                }
                if ($team['first_baron']) {
                    $battleDetail['first_baron_nashor_p_tid'] = $team_order;
                }
                if ($team['first_dragon']) {
                    $battleDetail['first_dragon_p_tid'] = $team_order;
                }

                if ($team['first_inhibitor']) {
                    $battleDetail['first_inhibitor_p_tid'] = $team_order;
                }
                if ($team['first_tower']) {
                    $battleDetail['first_turret_p_tid'] = $team_order;
                }
                $teamStatic[$team_order]['rel_team_id'] = @$team['team']['id'];
                $teamStatic[$team_order]['rel_identity_id'] = $team['team']['id'];
                $teamStatic[$team_order]['kills'] = empty($team['kills']) ? 0 : $team['kills'];
                $teamStatic[$team_order]['deaths'] = 0;
                $teamStatic[$team_order]['assists'] = 0;
                $teamStatic[$team_order]['wards_purchased'] = 0;//暂时为空
                $teamStatic[$team_order]['wards_placed'] = 0;
                $teamStatic[$team_order]['wards_kills'] = 0;
                $team_gold_earned = empty(@$team['gold_earned']) ? 0 : $team['gold_earned'];
                $teamStatic[$team_order]['gold'] = $team_gold_earned;
                $teamStatic[$team_order]['faction'] = $team['color'];
                $teamStatic[$team_order]['inhibitor_kills'] = empty(@$team['inhibitor_kills']) ? 0 : $team['inhibitor_kills'];
                $teamStatic[$team_order]['turret_kills'] = empty(@$team['tower_kills']) ? 0 : $team['tower_kills'];
                $teamStatic[$team_order]['baron_nashor_kills'] = empty(@$team['baron_kills']) ? 0 : $team['baron_kills'];
                $teamStatic[$team_order]['dragon_kills'] = empty(@$team['dragon_kills']) ? 0 : $team['dragon_kills'];
                $teamStatic[$team_order]['rift_herald_kills'] = empty(@$team['herald_kills']) ? 0 : $team['herald_kills'];
                $dragon_kills_detail = self::conversionRestDragonKillsId(
                    $restBattleInfo['events'],
                    $team['color'],
                    $restBattleInfo['base']['match_id'],
                    $restBattleInfo['base']['id']
                );
                if (empty($dragon_kills_detail)) {
                    $teamStatic[$team_order]['dragon_kills_detail'] = null;
                } else {
                    $teamStatic[$team_order]['dragon_kills_detail'] = json_encode($dragon_kills_detail);
                }
                $teamStatic[$team_order]['order'] = $team_order;
                $teamStaticColor[$team['color']] = $team_order;
                // 队员信息转化
            }
            foreach ($teamStaticColor as $key_faction => $val_faction){
                if ($key_faction == 'blue'){
                    $team_gold_diff = (Int)($teamStatic[$val_faction]['gold'] - $teamStatic[$teamStaticColor['red']]['gold']);
                    $teamStatic[$val_faction]['gold_diff'] = $team_gold_diff;
                    $teamStatic[$teamStaticColor['red']]['gold_diff'] = -$team_gold_diff;
                }
            }
            $battleInfo['detail'] = $battleDetail;

            //pick_array
            $pick_array = [];
            foreach ($restBattleInfo['base']['players'] as $key => $player) {
                $pick_array[$key]['hero'] = $player['champion']['id'];
                $pick_array[$key]['team'] = $player['team']['id'];
                $pick_array[$key]['type'] = 2;
                $pick_array[$key]['order'] = null;
            }
            //ban
            $banArray = [];
            foreach ($restBattleInfo['base']['teams'] as $playerTeams) {
                foreach ($playerTeams['bans'] as $banKey => $ban) {
                    $tmp = [
                        'hero' => $ban,
                        'team' => $playerTeams['team']['id'],
                        'type' => 1,
                        'order' => null
                    ];
                    $banArray[] = $tmp;
                }
            }
            $battleInfo['ban_pick'] = array_merge($banArray, $pick_array);
            $player_damage_to_champions_all1 = $player_damage_to_champions_all2 = $player_damage_taken_all1 = $player_damage_taken_all2 = 0;
            $playersRest = $restBattleInfo['base']['players'];
            foreach ($playersRest as $key => $playerRest) {
                $playerConfig = [
                    //                "rel_team_id" => "",//
                    //                'rel_player_id' => "",
                    //                "order" => "",//排序
                    //                "seed" => "",//选手排序
                    "player_id" => function ($params) {
                        return $params['player']['id'];
                    },//选手id
                    "rel_nick_name" => function ($params) {
                        return $params['player']['name'];
                    },//选手id
                    "game" => "game_id",//游戏
                    "spells" => "spells",//游戏
                    //                "match" => "",//比赛
                    //                "team_id" => "",//战队ID
                    //                "faction" => "",//阵营
                    "role" => "role",//角色
                    //                "lane" => "",//分路
                    //                "player_id" => "",//选手id
                    //                "nick_name" => "",//选手昵称
                    "champion" => function ($params) {
                        return $params['champion']['id'];
                    },//英雄
                    "level" => "level",//等级
                    //                "alive" => "",//存活状态
                    //                "ultimate_cd" => "",//大招状态
                    //                "coordinate" => "",//坐标
                    "kills" => "kills",//击杀
                    "double_kill" => function ($params) {
                        return $params['kills_series']['double_kills'];
                    },//双杀
                    "triple_kill" => function ($params) {
                        return $params['kills_series']['triple_kills'];
                    },//三杀
                    "quadra_kill" => function ($params) {
                        return $params['kills_series']['quadra_kills'];
                    },//四杀
                    "penta_kill" => function ($params) {
                        return $params['kills_series']['penta_kills'];
                    },//五杀
                    "largest_multi_kill" => "largest_multi_kill",//最大多杀
                    "largest_killing_spree" => "largest_killing_spree",//最大连杀
                    "deaths" => "deaths",//死亡
                    "assists" => "assists",//助攻
                    //                "kda" => "",//KDA
                    //                "participation" => "",//参团率
                    "cs" => "minions_killed",//补刀得分
                    //"minion_kills" => "minions_killed",//小兵击击杀数
                    //                "total_neutral_kills" => "",//野怪击杀总数
                    //                "neutral_team_jungle_kills" => "",//本方野怪击杀数
                    //                "neutral_enemy_jungle_kills" => "",//敌方野怪击杀数
                    //                "cspm" => "",//分均补刀
                    "gold_earned" => "gold_earned",//金币获取
                    "gold_spent" => "gold_spent",//已花费金币
                    //                "gold_remaining" => "",//剩余金币
                    //                "gpm" => "",//分均金钱
                    //                "gold_earned_percent" => "",//经济占比
                    //                "experience" => "",//总经验
                    //                "xpm" => "",//分均经验
                    "damage_to_champions" => function ($params) {
                        return (String)$params['total_damage']['dealt_to_champions'];
                    },//对英雄伤害
                    "damage_to_champions_physical" => function ($params) {
                        return (String)$params['physical_damage']['dealt_to_champions'];
                    },//对英雄物理伤害
                    "damage_to_champions_magic" => function ($params) {
                        return (String)$params['magic_damage']['dealt_to_champions'];
                    },//对英雄魔法伤害
                    "damage_to_champions_true" => function ($params) {
                        return (String)$params['true_damage']['dealt_to_champions'];
                    },//对英雄真实伤害
                    //                "dpm_to_heroes" => "",//对英雄分均伤害
                    //                "damage_percent_to_heroes" => "",//对英雄伤害占比
                    "damage_taken" => function ($params) {
                        return (String)$params['total_damage']['taken'];
                    },//承受伤害
                    "damage_taken_physical" => function ($params) {
                        return (String)$params['physical_damage']['taken'];
                    },//承受物理伤害
                    //                "damage_taken_magic" => function ($params) {
                    //                },//承受魔法伤害
                    "damage_taken_true" => function ($params) {
                        return (String)$params['true_damage']['taken'];
                    },//承受真实伤害
                    //                "dtpm" => "",//分均承受伤害
                    //                "damage_taken_percent" => "",//承伤占比
                    "total_damage" => function ($params) {
                        return (String)$params['total_damage']['dealt'];
                    },//总伤害
                    //                "total_damage_physical" => "",//总物理伤害
                    "total_damage_physical" => function ($params) {
                        return (String)$params['physical_damage']['dealt'];
                    },//总魔法伤害
                    "total_damage_magic" => function ($params) {
                        return (String)$params['magic_damage']['dealt'];
                    },//总魔法伤害
                    //                "total_damage_true" => "",//总真实伤害
                    "total_damage_true" => function ($params) {
                        return null;//(String)$params['true_damage']['dealt']; //暂时没有
                    },//总魔法伤害
                    //                "damage_conversion_rate" => "",//伤害转化率
                    "total_heal"  => function ($params) {//总治疗量
                        return (String)$params['total_heal'];
                    },
                    "total_crowd_control_time" => function ($params) {
                        return (String)$params['total_time_crowd_control_dealt'];
                    },//总控制时长
                    "wards_purchased" => function ($params) {
                        return $params['wards']['sight_wards_bought_in_game'] + $params['wards']['vision_wards_bought_in_game'];
                    },//买眼
                    "wards_placed" => function ($params) {
                        return $params['wards']['placed'];
                    },//插眼
                    "wards_kills" => function ($params) {
                        return $params['kills_counters']['wards'];
                    },//排眼
                    "warding_totems_purchased" => function ($params) {
                        return $params['wards']['sight_wards_bought_in_game'];
                    },//购买监视图腾
                    "control_wards_purchased" => function ($params) {
                        return $params['wards']['vision_wards_bought_in_game'];
                    },//购买控制守卫
                    "total_neutral_minion_kills" => function ($params) {
                        return (String)$params['kills_counters']['neutral_minions'];
                    },//野怪总数
                    "neutral_minion_team_jungle_kills" => function ($params) {
                        return (String)$params['kills_counters']['neutral_minions_team_jungle'];
                    },
                    "neutral_minion_enemy_jungle_kills" => function ($params) {
                        return (String)$params['kills_counters']['neutral_minions_enemy_jungle'];
                    },
                    "items" => "items",
                    "rel_team_id" => function ($params) {
                        return $params['team']['id'];
                    },
                    'rel_player' => 'player',
                    'rel_identity_id' => function ($params) {
                        return (string)$params['player']['id'];
                    },
                    'rel_id' => function ($params) {
                        return $params['player']['id'];
                    },
                    'team_order' => function ($params) {
                        return 1;
                    },
                    'turret_kills' => function ($params) {
                        return (int)$params['kills_counters']['turrets'];
                    },
                    'inhibitor_kills' => function ($params) {
                        return (int)$params['kills_counters']['inhibitors'];
                    },
                ];
                $player = Mapping::transformation($playerConfig, $playerRest);
                $player['order'] = $key;
                $team_order_player = $teamOrderArray[$playerRest['team']['id']];
                $player['team_order'] = $team_order_player;//  self::getTeamOrderByTeamRelId($playerRest['team']['id'],$matchInfo);
                $teamStatic[$team_order_player]['deaths'] += (Int)$player['deaths'];
                $teamStatic[$team_order_player]['assists'] += (Int)$player['assists'];
                $teamStatic[$team_order_player]['wards_purchased'] += (Int)$player['wards_purchased'];//暂时没有
                $teamStatic[$team_order_player]['wards_placed'] += (Int)$player['wards_placed'];
                $teamStatic[$team_order_player]['wards_kills'] += (Int)$player['wards_kills'];
                $player['game'] = 2;
                $player['faction'] = $teamStatic[$team_order_player]['faction'];
                // 需要计算得属性
                $deaths = $player['deaths'] ? $player['deaths'] : 1;
                $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
                $teamkill = self::getTeamKills($playerRest['team']['id'], $restBattleInfo);
                if ($teamkill) {
                    $player['participation'] = (String)round((($player['kills'] + $player['assists']) / $teamkill),4);
                } else {
                    $player['participation'] = null;
                }
                $team_gold = isset($teamStatic[$team_order_player]['gold']) ? $teamStatic[$team_order_player]['gold'] : 1;
                if (isset($player['gold_earned']) && $player['gold_earned']){
                    $player['gold_earned_percent'] = (String)round(($player['gold_earned'] / $team_gold),4) ;
                }else{
                    $player['gold_earned_percent'] = null;
                }
                if (isset($player['gold_spent']) && isset($player['gold_earned'])) {
                    $player['gold_remaining'] = $player['gold_earned'] - $player['gold_spent'];
                }
                if (isset($restBattleInfo['base']['length']) && $restBattleInfo['base']['length'] && isset($player['gold_earned'])) {
                    $player['gpm'] = (String)round(($player['gold_earned'] / ($restBattleInfo['base']['length'] / 60)),2);
                } else {
                    $player['gpm'] = null;
                }
                if (isset($restBattleInfo['base']['length']) && $restBattleInfo['base']['length']) {
                    $player['dpm_to_champions'] = (String)round(($player['damage_to_champions'] / ($restBattleInfo['base']['length'] / 60)),2);
                } else {
                    $player['dpm_to_champions'] = null;
                }
                $player['dtpm'] = (String)round(($player['damage_taken'] / ($restBattleInfo['base']['length'] / 60)),2);
                //$player['damage_taken_percent'] = $player['damage_taken']/$restBattleInfo['base']['length'];
                if (isset($player['gold_earned']) && isset($player['gold_earned'])) {
                    $player['damage_conversion_rate'] = (String)round(($player['damage_to_champions'] / $player['gold_earned']),4);
                } else {
                    $player['damage_conversion_rate'] = null;
                }
                if (isset($player['cs']) && isset($restBattleInfo['base']['length'])) {
                    $player['cspm'] = (String)round(($player['cs'] / ($restBattleInfo['base']['length'] / 60)), 2);
                } else {
                    $player['cspm'] = null;
                }
                $players[] = $player;
                //计算总数
                if ($team_order_player == 1) {
                    $player_damage_to_champions_all1 += $player['damage_to_champions'];
                    $player_damage_taken_all1 += $player['damage_taken'];
                }
                if ($team_order_player == 2) {
                    $player_damage_to_champions_all2 += $player['damage_to_champions'];
                    $player_damage_taken_all2 += $player['damage_taken'];
                }
            }
            //计算damage_percent_to_champions 和damage_taken_percent
            foreach ($players as $key => $item) {
                if ($item['team_order'] == 1) {
                    $player_damage_to_champions_all = $player_damage_to_champions_all1 == 0 ? 1 : $player_damage_to_champions_all1;
                    $player_damage_taken_all = $player_damage_taken_all1 == 0 ? 1 : $player_damage_taken_all1;
                }
                if ($item['team_order'] == 2) {
                    $player_damage_to_champions_all = $player_damage_to_champions_all2 == 0 ? 1 : $player_damage_to_champions_all2;
                    $player_damage_taken_all = $player_damage_taken_all2 == 0 ? 1 : $player_damage_taken_all2;
                }
                $players[$key]['damage_percent_to_champions'] = round(($item['damage_to_champions'] / $player_damage_to_champions_all),4);
                $players[$key]['damage_taken_percent'] = round(($item['damage_taken'] / $player_damage_taken_all),4);
            }
            $battleInfo['players'] = $players;
            $battleInfo['static'] = $teamStatic;
        }
        return $battleInfo;
    }

    private static function getMatch($matchId)
    {
        $url = '/lol/matches';
        $params = [
            'filter[id]' => $matchId,
        ];
        $jsonInfo = self::getRestInfo($url, $params);
        $info = json_decode($jsonInfo, true);
        // 获取当前battle的数据
        return $info[0];
    }

    private static function getBattle($battleId)
    {
        $url = '/lol/games/' . $battleId;

        $jsonInfo = PandascoreBase::getInfo($url, []);
        $info = json_decode($jsonInfo, true);
        return $info;
    }

    private static function getEvents($battleId)
    {
        // 这里会翻页
        $url = '/lol/games/' . $battleId . '/events';
        $infoAll = [];
        for ($i = 1; $i < 100; $i++) {
            $page = $i;
            $perPage = 100;
            $jsonInfo = PandascoreBase::getInfo($url, ['page' => $page, 'per_page' => $perPage]);
            $infoList = json_decode($jsonInfo, true);
            if (count($infoList)>0){
                $infoAll = array_merge($infoAll, $infoList);
            }
            if (count($infoList) < $perPage) {
                break;
            }
        }
        return $infoAll;
    }

    private static function getFrames($battleId)
    {
        $url = '/lol/games/' . $battleId . '/frames';
        $infoAll = [];
        for ($i = 1; $i < 100; $i++) {
            $page = $i;
            $perPage = 100;
            $jsonInfo = PandascoreBase::getInfo($url, ['page' => $page, 'per_page' => $perPage]);
            $infoList = json_decode($jsonInfo, true);
            if (count($infoList)>0){
                $infoAll = array_merge($infoAll, $infoList);
            }
            if (count($infoList) < $perPage) {
                break;
            }
        }
        return $infoAll;
    }

    public static function getTeamKills($teamId,$restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val){
            if($val['team']['id'] == $teamId){
                return $val['kills'];
            }
        }
    }

    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal['id'];
            }
        }
    }

    public static function getGoalIdBySubType($subType)
    {
        $goal = EnumIngameGoal::find()->where(['=','sub_type',$subType])
            ->asArray()->one();
        if($goal){
            return $goal['id'];
        }
    }

    public static function getTeamToDamage($teamId,$restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val){
            if($val['team']['id'] == $teamId){
                return $val['kills'];
            }
        }
    }
    //转化事件
    public static function transEvents($event,$matchId,$matchInfo)
    {
        $event['game'] = $matchInfo['game'];
        $event['match'] = $matchInfo['id'];
        $event['order'] = $event['order'];
        $event['ingame_timestamp'] = $event['ingame_timestamp'];
        $event_type = str_replace('_kill','',$event['event_type']);
        $event['event_type'] = Common::geGoalIdLOLEventTypeByEvent(Consts::ORIGIN_PANDASCORE,$event_type.'_kill');
        //击杀者
        if($event['killer_ingame_obj_type'] == "player"){  //击杀者id
            $event['killer'] = self::getMainIdByRelIdentityId('player',$event['killer_player_id'],'3');
            $player_detail = Player::find()->select('nick_name')->where(['id'=>$event['killer']])->asArray()->one();
            $event['killer_player_name'] = @$player_detail['nick_name'];
            $event['killer_champion_id'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $event['killer_champion_id'], '3');
            $killerType = self::getGoalIdByPandascoreName($event['killer_ingame_obj_type']);
        }elseif ($event['killer_ingame_obj_type'] == "unknown"){
            $killerType = 51;//unknown
            $event['killer'] = $killerType;
        }else{
            $killerType= self::getGoalIdByPandascoreName($event['killer_object_other_name']);
            $event['killer'] = $killerType;
        }
        $event['killer_ingame_obj_type'] = $killerType;
        $event['killer_sub_type'] = $killerType;
        //待处理
//        if ($event['killer_faction']){
//            $event['killer_faction'] = $event['killer_faction'];
//        }
//        if ($event['killer_ingame_obj_type'] == 'unknown'){
//            $event['killer_ingame_obj_type'] = $event['killer'];
//            $event['killer_sub_type'] = $event['killer'];
//        }else{
//            $event['killer_ingame_obj_type'] = self::getGoalIdByPandascoreName($event['killer_ingame_obj_type']);
//            if ($event['killer_sub_type']){
//                $event['killer_sub_type'] =self::getGoalIdByPandascoreName($event['killer_sub_type']);
//            }else{
//                $event['killer_sub_type'] = $event['killer_ingame_obj_type'];
//            }
//        }
        //受害者
        if($event['victim_ingame_obj_type'] == "player"){
            $event['victim'] = self::getMainIdByRelIdentityId('player',$event['victim_player_id'],'3');
            $player_detail_victim = Player::find()->select('nick_name')->where(['id'=>$event['victim']])->asArray()->one();
            $event['victim_player_name'] = @$player_detail_victim['nick_name'];
            $event['victim_champion_id'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $event['victim_champion_id'], '3');
            $VictimType = self::getGoalIdByPandascoreName($event['victim_ingame_obj_type']);
        }else{
            if($event['victim_name']){  //被击杀的为野怪
                $VictimType = self::getGoalIdByPandascoreName($event['victim_name']);
                $event['victim'] = $VictimType;
            }else{
                $VictimType = 51;//unknown
                $event['victim'] = $VictimType;
            }
        }
        if ($event['victim_faction']){
            $event['victim_faction'] = $event['victim_faction'];
        }
//        $event['victim_ingame_obj_type'] = self::getGoalIdByPandascoreName($event['victim_ingame_obj_type']);
//        if ($event['victim_sub_type']){
//            $event['victim_sub_type'] =self::getGoalIdByPandascoreName($event['victim_sub_type']);
//        }else{
//            $event['victim_sub_type'] = $event['victim_ingame_obj_type'];
//        }
        $event['victim_ingame_obj_type'] = $VictimType;
        $event['victim_sub_type'] = $VictimType;
        //助攻
        $assist_array = [];
        if(isset($event['assist']) && $event['assist']){
            foreach ($event['assist'] as $key => $value){
                $assist_player_id = self::getMainIdByRelIdentityId('player',$value['assist_player_id'],'3');
                $assist_array[$key]['player_id'] = $assist_player_id;
                $player_detail_assist = Player::find()->select('nick_name')->where(['id'=>$assist_player_id])->asArray()->one();
                $assist_array[$key]['player_nick_name'] = @$player_detail_assist['nick_name'];
                $assist_array[$key]['assist_player_name'] = $value['assist_player_name'];
                $assist_array[$key]['faction'] = $value['assist_player_faction'];
                $assist_array[$key]['champion_id'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $value['assist_champion_id'], '3');
            }
        }
        $event['assist'] = @json_encode($assist_array,320);
        //助攻类型
        $event['assist_ingame_obj_type'] = self::getGoalIdByPandascoreName($event['assist_ingame_obj_type']);
        $event['assist_sub_type'] = self::getGoalIdByPandascoreName($event['assist_sub_type']);
//        $event['is_first_event'] = $event['is_first_event'];
//        $event["first_event_type"] = empty($event["first_event_type"]) ? 2 : 2;
        return $event;
    }
    /**
     * 从事件中获取杀龙信息
     * @param $allEvents
     * @param $faction
     * @return null[]
     */
    public static function conversionRestDragonKillsId($allEvents,$faction,$rel_match_id,$rel_battle_id){
        if (empty($allEvents)){
            return null;
        }
        $blue_dragon_kills_detail = [];
        $kills_num = 0;
        foreach ($allEvents as $key => $event_info){
            $killed_type = $event_info['payload']['victim']['type'];
            $killer_type = $event_info['payload']['killer']['type'];
            if ($killed_type == 'drake' && $killer_type == 'player'){
                if ($faction == $event_info['payload']['killer']['object']['side'] && $event_info['game_id'] == $rel_battle_id && $event_info['match_id'] == $rel_match_id){
                    $blue_dragon_kills_detail[$kills_num]['date'] = $event_info['ingame_timestamp'];
                    $dragon_name = Common::getGoalIdByPandascoreName($event_info['payload']['victim']['object']['type']);
                    $blue_dragon_kills_detail[$kills_num]['drake'] = $dragon_name;
                    $kills_num ++;
                }
            }
        }
        return $blue_dragon_kills_detail;
    }

    //第一次事件
    public static function getRestFirstEventInfo($Redis,$event_redis_name,$matchId,$battleId,$eventInfo,$killer_team,$killed_type,$killer_type){
        $redis_name = $event_redis_name.'_'.$matchId.'_'.$battleId;
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null,
            'battle_first_event_type' => [
                'first_event_type_key'=>null,
                'first_event_faction' =>null,
                'first_blood_time' =>null,
            ]
        ];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
//                $killer_type = $eventInfo['payload']['killer']['type'];
                $team_kills_key = 'team_kills_'.$killer_team;
                if ($killer_type == 'player') {//人击杀人
                    //查询是否是一血
                    $first_blood = $Redis->hget($redis_name, 'first_blood');
                    if (!$first_blood) {
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_blood',
                            'battle_first_event_type' => [
                                'first_event_type_key' => 'first_blood_p_tid',
                                'first_event_faction' => $killer_team,
                                'first_blood_time' => $eventInfo['ingame_timestamp']
                            ]
                        ];
                        $Redis->hset($redis_name, 'first_blood', 1);
                    }
                    //查询 first_to_5_kills first_to_10_kills
                    //查询当前team击杀了多少人
                    //查询是否五杀了
                    $first_to_5_kills = $Redis->hget($redis_name,'first_to_5_kills');
                    $first_to_10_kills = $Redis->hget($redis_name,'first_to_10_kills');
                    $team_kills_res = $Redis->hget($redis_name,$team_kills_key);//击杀队伍
                    if ($team_kills_res){
                        $now_team_kills = $team_kills_res + 1;
                        $Redis->hset($redis_name,$team_kills_key,$now_team_kills);
                        if ($now_team_kills == 5 && empty($first_to_5_kills)){
                            $eventInfoRes = [
                                'is_first_event' => true,
                                'first_event_type' => 'first_to_5_kills',
                                'battle_first_event_type' => [
                                    'first_event_type_key'=>'first_to_5_kills_p_tid',
                                    'first_event_faction' => $killer_team,
                                    'first_to_5_kills_time' =>$eventInfo['ingame_timestamp']
                                ]
                            ];
                            $Redis->hset($redis_name,'first_to_5_kills', $killer_team);
                        }
                        if ($now_team_kills == 10 && empty($first_to_10_kills)){
                            $eventInfoRes = [
                                'is_first_event' => true,
                                'first_event_type' => 'first_to_10_kills',
                                'battle_first_event_type' => [
                                    'first_event_type_key'=>'first_to_10_kills_p_tid',
                                    'first_event_faction' => $killer_team,
                                    'first_to_10_kills_time' =>$eventInfo['ingame_timestamp']
                                ]
                            ];
                            $Redis->hset($redis_name,'first_to_10_kills', $killer_team);
                        }
                    }else{
                        $Redis->hset($redis_name,$team_kills_key, 1);
                    }
                }
                break;
            case 'rift_herald':
                $rift_herald = $Redis->hget($redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald',
                        'battle_first_event_type' => [
                            'first_event_type_key'=>'first_rift_herald_p_tid',
                            'first_event_faction' => $killer_team,
                            'first_rift_herald_time' =>$eventInfo['ingame_timestamp']
                        ]
                    ];
                    $Redis->hset($redis_name,'first_rift_herald',1);
                }
                break;
            case 'drake':
                $killed_sub_type = $eventInfo['payload']['victim']['object']['type'];
                if ($killed_sub_type == 'elder'){
                    $first_elder_dragon = $Redis->hget($redis_name,'first_elder_dragon');
                    if (!$first_elder_dragon){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_elder_dragon',
                            'battle_first_event_type' => [
                                'first_event_type_key'=>'first_elder_dragon_p_tid',
                                'first_event_faction' => $killer_team,
                                'first_elder_dragon_time' =>$eventInfo['ingame_timestamp']
                            ]
                        ];
                        $Redis->hset($redis_name,'first_elder_dragon',1);
                    }
                }else{
                    $first_dragon = $Redis->hget($redis_name,'first_dragon');
                    if (!$first_dragon){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_dragon',
                            'battle_first_event_type' => [
                                'first_event_type_key'=>'first_dragon_p_tid',
                                'first_event_faction' => $killer_team,
                                'first_dragon_time' =>$eventInfo['ingame_timestamp']
                            ]
                        ];
                        $Redis->hset($redis_name,'first_dragon',1);
                    }
                }
                break;
            case 'baron_nashor':
                $first_baron_nashor = $Redis->hget($redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor',
                        'battle_first_event_type' => [
                            'first_event_type_key'=>'first_baron_nashor_p_tid',
                            'first_event_faction' => $killer_team,
                            'first_baron_nashor_time' =>$eventInfo['ingame_timestamp']
                        ]
                    ];
                    $Redis->hset($redis_name,'first_baron_nashor',1);
                }
                break;
            case 'elder_dragon':
                $first_elder_dragon = $Redis->hget($redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon',
                        'battle_first_event_type' => [
                            'first_event_type_key'=>'first_elder_dragon_p_tid',
                            'first_event_faction' => $killer_team,
                            'first_elder_dragon_time' =>$eventInfo['ingame_timestamp']
                        ]
                    ];
                    $Redis->hset($redis_name,'first_elder_dragon',1);
                }
                break;
            case 'tower':
                $first_turret = $Redis->hget($redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret',
                        'battle_first_event_type' => [
                            'first_event_type_key'=>'first_turret_p_tid',
                            'first_event_faction' => $killer_team,
                            'first_turret_time' =>$eventInfo['ingame_timestamp']
                        ]
                    ];
                    $Redis->hset($redis_name,'first_turret',1);
                }
                break;
            case 'inhibitor':
                $first_inhibitor = $Redis->hget($redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor',
                        'battle_first_event_type' => [
                            'first_event_type_key'=>'first_inhibitor_p_tid',
                            'first_event_faction' => $killer_team,
                            'first_inhibitor_time' =>$eventInfo['ingame_timestamp']
                        ]
                    ];
                    $Redis->hset($redis_name,'first_inhibitor',1);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null,
                    'battle_first_event_type' => [
                        'first_event_type_key'=>null,
                        'first_event_faction'=>null,
                        'first_inhibitor_time' =>null
                    ]
                ];
        }
        $Redis->expire($redis_name,60);
        return $eventInfoRes;
    }
    //转化装备
    public static function conversionPlayerItemsApi($Redis,$playerItems,$origin_id){
        //查询所有道具
        $lolItems = HotBase::getMainDataArrayMasterIdAndRelIdentityId('lol_item',3,2);
        //redis初始化
        $itemsList = "lol:items:list:".$origin_id;
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        foreach ($playerItems as $key_item => $value_item){
            if ($value_item){
                $itemInfoId = $lolItems[$value_item['id']];
                if($itemInfoId){
                    $itemInfoId_val = (Int)$itemInfoId;
                    $itemInfoResRedis = $Redis->hget($itemsList,$itemInfoId_val);
                    if ($itemInfoResRedis){
                        $itemInfoRes = @json_decode($itemInfoResRedis,true);
                        if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                            $playerOrnamentsInfo[$key_item] = $itemInfoId_val;
                        }else{//道具
                            $playerItemsInfo[$key_item]['total_cost'] = @$itemInfoRes['total_cost'];
                            $playerItemsInfo[$key_item]['item_id'] = $itemInfoId_val;
                        }
                    }else{//道具
                        $playerItemsInfo[$key_item]['total_cost'] = 1;
                        $playerItemsInfo[$key_item]['item_id'] = (Int)@self::getMetaDataUnknownId('lol_items');
                    }
                }else{
                    $playerItemsInfo[$key_item]['total_cost'] = 1;
                    $playerItemsInfo[$key_item]['item_id'] = (Int)@self::getMetaDataUnknownId('lol_items');
                }
            }
        }
        $total_cost_array = array_column($playerItemsInfo,'total_cost');
        array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
        $playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
        //判断是不是够6个
        $playerItemsInfoNum = count($playerItemsInfoRet);
        if ($playerItemsInfoNum < 6){
            $chaNum = 6 - $playerItemsInfoNum;
            for ($i=0;$i<$chaNum;$i++){
                $playerItemsNull[] = null;
            }
        }
        $playerItemsInfoResult = array_merge($playerItemsInfoRet,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        //循环添加 purchase_time购买时间点 cooldown装备使用CD
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsInfo = null;
            if ($val_res){
                $ItemsInfo = [
                    'item_id' => $val_res,
                    'purchase_time' => null,
                    'cooldown' => null
                ];
            }
            $playerItemsInfoResultEnd[] = $ItemsInfo;
        }
        return json_encode($playerItemsInfoResultEnd,320);
    }
    //存第一次事件详情
    private static function conversionFirstEventsDetail($Redis,$event){
        $first_events_list = [];
        $killerType = $event['killer_ingame_obj_type'];
        switch ($event['first_event_type_string']){
            case 'first_blood':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim_champion = self::getChampionInfoByRedis($Redis,$event['victim_champion_id']);
                $victim = [
                    'player_id' => $event['victim'],
                    'nick_name' => $event['victim_player_name'],
                    'faction' => $event['victim_faction'],
                    'champion' => $victim_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_blood_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_blood_detail'] = @json_encode($first_blood_detail,320);
                break;
            case 'first_to_5_kills':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim_champion = self::getChampionInfoByRedis($Redis,$event['victim_champion_id']);
                $victim = [
                    'player_id' => $event['victim'],
                    'nick_name' => $event['victim_player_name'],
                    'faction' => $event['victim_faction'],
                    'champion' => $victim_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_to_5_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_to_5_detail'] = @json_encode($first_to_5_detail,320);
                break;
            case 'first_to_10_kills':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim_champion = self::getChampionInfoByRedis($Redis,$event['victim_champion_id']);
                $victim = [
                    'player_id' => $event['victim'],
                    'nick_name' => $event['victim_player_name'],
                    'faction' => $event['victim_faction'],
                    'champion' => $victim_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_to_10_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_to_10_detail'] = @json_encode($first_to_10_detail,320);
                break;
            case 'first_rift_herald':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim = [
                    'ingame_obj_name' => 'Rift Herald',
                    'ingame_obj_name_cn' => '峡谷先锋',
                    'ingame_obj_type' => 'elite',
                    'ingame_obj_sub_type' => 'rift_herald'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_rift_herald_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_rift_herald_detail'] = @json_encode($first_rift_herald_detail,320);
                break;
            case 'first_dragon':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim = null;
                //判断是哪个龙
                if ($event['victim'] == 2){
                    $victim = [
                        'ingame_obj_name' => 'Infernal Drake',
                        'ingame_obj_name_cn' => '炼狱亚龙',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'drake'
                    ];
                }
                if ($event['victim'] == 3){
                    $victim = [
                        'ingame_obj_name' => 'Mountain Drake',
                        'ingame_obj_name_cn' => '山脉亚龙',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'drake'
                    ];
                }
                if ($event['victim'] == 4){
                    $victim = [
                        'ingame_obj_name' => 'Ocean Drake',
                        'ingame_obj_name_cn' => '海洋亚龙',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'drake'
                    ];
                }
                if ($event['victim'] == 5){
                    $victim = [
                        'ingame_obj_name' => 'Cloud Drake',
                        'ingame_obj_name_cn' => '云端亚龙',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'drake'
                    ];
                }
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_dragon_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_dragon_detail'] = @json_encode($first_dragon_detail,320);
                break;
            case 'first_baron_nashor':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim = [
                    'ingame_obj_name' => 'Baron Nashor',
                    'ingame_obj_name_cn' => '纳什男爵',
                    'ingame_obj_type' => 'elite',
                    'ingame_obj_sub_type' => 'baron_nashor'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_baron_nashor_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_baron_nashor_detail'] = @json_encode($first_baron_nashor_detail,320);
                break;
            case 'first_elder_dragon':
                $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                $killer = [
                    'player_id' => $event['killer'],
                    'nick_name' => $event['killer_player_name'],
                    'faction' => $event['killer_faction'],
                    'champion' => $killer_champion,
                    'ingame_obj_type' => 'player',
                    'ingame_obj_sub_type' => 'player'
                ];
                $victim = [
                    'ingame_obj_name' => 'Elder Dragon',
                    'ingame_obj_name_cn' => '远古巨龙',
                    'ingame_obj_type' => 'elite',
                    'ingame_obj_sub_type' => 'elder_dragon'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_elder_dragon_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_elder_dragon_detail'] = @json_encode($first_elder_dragon_detail,320);
                break;
            case 'first_turret':
                $killer = [];
                if ($killerType == 50){
                    $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => $event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                }
                if($killerType == 1){
                    $killer = [
                        'ingame_obj_name' => 'Rift Herald',
                        'ingame_obj_name_cn' => '峡谷先锋',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'rift_herald'
                    ];
                }
                if($killerType == 8){//Minion
                    $killer = [
                        'ingame_obj_name' => 'Minion',
                        'ingame_obj_name_cn' => '小兵',
                        'faction' => $event['killer_faction'],
                        'ingame_obj_type' => 'minion',
                        'ingame_obj_sub_type' => 'minion'
                    ];
                }
                if($killerType == 51){//Minion
                    $killer = [
                        'ingame_obj_name' => 'unknown',
                        'ingame_obj_name_cn' => '未知',
                        'ingame_obj_type' => 'unknown',
                        'ingame_obj_sub_type' => 'unknown'
                    ];
                }
                $victim = [
                    'lane' => null,
                    'ingame_obj_name' => 'Turret',
                    'ingame_obj_name_cn' => '防御塔',
                    'faction' => $event['victim_faction'],
                    'ingame_obj_type' => 'building',
                    'ingame_obj_sub_type' => 'turret'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_turret_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_turret_detail'] = @json_encode($first_turret_detail,320);
                break;
            case 'first_inhibitor':
                $killer = [];
                if ($killerType == 50){
                    $killer_champion = self::getChampionInfoByRedis($Redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => $event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                }
                if($killerType == 1){
                    $killer = [
                        'ingame_obj_name' => 'Rift Herald',
                        'ingame_obj_name_cn' => '峡谷先锋',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'rift_herald'
                    ];
                }
                if($killerType == 8){//Minion
                    $killer = [
                        'ingame_obj_name' => 'Minion',
                        'ingame_obj_name_cn' => '小兵',
                        'faction' => $event['killer_faction'],
                        'ingame_obj_type' => 'minion',
                        'ingame_obj_sub_type' => 'minion'
                    ];
                }
                if($killerType == 51){//Minion
                    $killer = [
                        'ingame_obj_name' => 'unknown',
                        'ingame_obj_name_cn' => '未知',
                        'ingame_obj_type' => 'unknown',
                        'ingame_obj_sub_type' => 'unknown'
                    ];
                }
                $victim = [
                    'lane' => null,
                    'ingame_obj_name' => 'Inhibitor',
                    'ingame_obj_name_cn' => '召唤水晶',
                    'faction' => $event['victim_faction'],
                    'ingame_obj_type' => 'building',
                    'ingame_obj_sub_type' => 'inhibitor'
                ];
                $assist = [];
                if (count($event['assist']) > 0){
                    $assist_list = @json_decode($event['assist'],true);
                    foreach ($assist_list as $key => $val){
                        $assist_val = [
                            'player_id' => $val['player_id'],
                            'nick_name' => $val['player_nick_name'],
                            'faction' => $val['faction'],
                            'champion' => self::getChampionInfoByRedis($Redis,$val['champion_id']),
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                        $assist[] = $assist_val;
                    }
                }
                $first_inhibitor_detail = [
                    'position' => null,
                    'killer' => $killer,
                    'victim' => $victim,
                    'assist' => $assist
                ];
                $first_events_list['first_inhibitor_detail'] = @json_encode($first_inhibitor_detail,320);
                break;
        }
        return $first_events_list;
    }
    //获取英雄信息
    private static function getChampionInfoByRedis($Redis,$champions_id){
        $champion = null;
        $championInfoRedis = $Redis->hget('lol:champions:list:3',$champions_id);
        if ($championInfoRedis){
            $championInfo = @json_decode($championInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
                $small_image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $Redis->get("lol:champions:list:unknown");
            $championInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
                $small_image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }
        return $champion;
    }
}