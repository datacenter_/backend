<?php
/**
 *
 */

namespace app\modules\task\services\hot\radarPurple;


use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\TaskService;
use app\modules\common\services\WinnerHelper;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\services\battle\LolBattleService;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\radarPurple\RadarPurpleBattle;
use app\modules\task\models\radarPurple\RadarPurpleBattleTeam;
use app\modules\task\models\radarPurple\RadarPurpleMatch;
use app\modules\task\models\radarPurple\RadarPurplePlayer;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\HotMatchInterface;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\db\Query;

class RadarPurpleDotaMatch extends RadarPurpleHotBase implements HotMatchInterface
{
    private static $origin_id_ezplay = 11;//数据源ID
    private static $team1_score = 0;
    private static $team2_score = 0;
    /**
     * @param $tagInfo
     * @param $taskInfo
     * 这里的操作，大体上分三个步骤，
     * 1.获取所需数据，可能需需要多次请求或者访问
     * 2.拼装数据成标准格式
     * 3.把整个结构更新到数据库
     */
    public static function run($tagInfo, $taskInfo)
    {
        $refreshType=@$tagInfo['ext2_type'];
        $timeStart=date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $hotId = $info['id'];
        $hotInfo = self::getHotInfo($hotId);
        $matchId=$hotInfo['match_id'];
        //TODO 做修改
        $relIdentityId = 'W9rcN8osaWg%3D';
//        $relIdentityId = @HotBase::getRelIdByMasterId('match',$matchId,self::$origin_id_ezplay);
//        if (empty($relIdentityId)){
//            return "比赛没有绑定的relMatchId";
//        }
        $matchInfo = Match::find()->where(['id'=>$matchId])->one();
        //请求PS的rest API 结果
        $restInfos = self::getInfos($relIdentityId);
        if (isset($restInfos['match']) && count($restInfos['battles']) > 0){
            $redis = new \Redis();
            $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
            $redis->auth(env('REDIS_PASSWORD'));
            $redis->select(env('REDIS_DATABASE'));
            // 这里传matchInfo，用来判断主队客队
            $infoFormat = self::conversion($redis,$restInfos,$matchInfo);
            $infoFormatAddBindingInfo=self::addBindingInfo($redis,$infoFormat,$matchInfo);
            $timeEnd=date('Y-m-d H:i:s');
            $extInfo=[
                'time_begin'=>$timeStart,
                'time_end'=>$timeEnd,
                'task_id'=>$taskInfo['id'],
                'match_id'=>$matchId,
            ];
            // 添加绑定信息，转义,主要出现在battle的详情中
            self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
            // 更新到数据库
            $refreshRes = self::refreshInfo($matchId,$infoFormatAddBindingInfo,$refreshType);
            // 更新hotdata状态
            self::refreshHotDataStatusByMatchInfo($hotId,$refreshRes);
        }
        return 'success';
    }

    public static function addBindingInfo($Redis,$infoFormat,$matchInfo)
    {
        //获取英雄的基础数据
        $heroRelationListRedisName = 'dota2:hero:relation:'.self::$origin_id_ezplay;
        $heroRelationList = $Redis->hGetAll($heroRelationListRedisName);
        $heroUnknownInfo = $Redis->get('dota2:hero:list:unknown');
        $heroUnknownInfoArray = json_decode($heroUnknownInfo,true);
        $heroUnknownId = $heroUnknownInfoArray['id'];
        $matchId = $matchInfo['id'];
        $team_rel_id_and_order_relation = $team_rel_id_and_master_id_relation = $team_order_and_score_relation = [];
        foreach($infoFormat['battles'] as &$battle){
            if (empty($battle)){
                continue;
            }
            $battle_order = $battle['base']['order'];
            $battle_winner = null;
            //记录战队
            $team_rel_score = 0;
            foreach($battle['static'] as &$static){
                $team_rel_score += $static['rel_score'];
                //组合需要的数据
                $team_order_and_score_relation[$static['order']] = $static['rel_score'];
                $team_rel_id_and_order_relation[$static['rel_identity_id']] = $static['order'];
                $team_rel_id_and_master_id_relation[$static['rel_identity_id']] = $static['team_id'];
            }
            //如果相等就是battle 结束了
            if ($team_rel_score == $battle_order){
                //这种情况是直接比分一边倒的
                if ($team_order_and_score_relation[1] == $battle_order){
                    self::$team1_score += 1;
                    $battle['static'][1]['score'] = 1;
                    $battle['static'][2]['score'] = 0;
                    $battle['base']['winner'] = 1;
                }elseif($team_order_and_score_relation[2] == $battle_order){
                    self::$team2_score += 1;
                    $battle['static'][1]['score'] = 0;
                    $battle['static'][2]['score'] = 1;
                    $battle['base']['winner'] = 2;
                }else{
                    $team1_score_res = self::$team1_score + 1;
                    $team2_score_res = self::$team2_score + 1;
                    foreach ($team_order_and_score_relation as $t_key => $t_item){
                        if ($t_item == $team1_score_res){
                            if ($t_key != 1 ){
                                self::$team1_score += 1;
                                $battle['static'][1]['score'] = 1;
                                $battle['static'][2]['score'] = 0;
                                $battle['base']['winner'] = 1;
                            }
                        }
                        if ($t_item == $team2_score_res){
                            if ($t_key != 2){
                                self::$team2_score += 1;
                                $battle['static'][1]['score'] = 0;
                                $battle['static'][2]['score'] = 1;
                                $battle['base']['winner'] = 2;
                            }
                        }
                    }
                }
            }
            unset($battle['static'][1]['rel_score']);
            unset($battle['static'][2]['rel_score']);
            // 更新选手中的选手id，战队id，item中的装备
            if (isset($battle['players']) && count($battle['players']) > 0) {
                foreach ($battle['players'] as $key => &$player) {
                    $player['game'] = 3;
                    $player['match'] = $matchId;
                    $player['player_id'] = @self::getMainIdByRelIdentityId('player', $player['player_id'], self::$origin_id_ezplay);
                    if ($player['player_id']){
                        //获取选手的昵称
                        $player_detail = Player::find()->select('nick_name')->where(['id'=>$player['player_id']])->asArray()->one();
                        $player['nick_name'] = @$player_detail['nick_name'];
                    }
                    $player['hero'] = @self::getMainIdByRelIdentityIdOrUnknown('dota2_hero', $player['hero'], self::$origin_id_ezplay);
                    $player['items'] = self::radarPurpleConversionPlayerItems($Redis,$player['items'],self::$origin_id_ezplay);
                    $player['talents'] = self::radarPurpleConversionPlayerTalents($Redis,$player['talents'],self::$origin_id_ezplay);
                }
            }
            //ban_pick
            if(count($battle['ban_pick'])>0){
                foreach ($battle['ban_pick'] as &$ban_pick_val){
                    $ban_pick_hero_id = $heroRelationList[$ban_pick_val['hero']];
                    if (empty($ban_pick_hero_id)){
                        $ban_pick_hero_id = $heroUnknownId;
                    }
                    $ban_pick_val['hero'] = $ban_pick_hero_id;
                    $ban_pick_val['team'] = $team_rel_id_and_master_id_relation[$ban_pick_val['team']];
                }
            }
        }
        $teamInfoRes = [];
        //转化队伍
        $home_team_id_trueID = $team_rel_id_and_order_relation[$infoFormat['match']['home_team_id']];
        if ($home_team_id_trueID == 2){
            $infoFormat['match']['team_1_score'] = (Int)$infoFormat['match']['away_score_score_old'];
            $infoFormat['match']['team_2_score'] = (Int)$infoFormat['match']['home_score_score_old'];
            $teamInfoRes[$infoFormat['match']['home_team_id']] = 2;
            $teamInfoRes[$infoFormat['match']['away_team_id']] = 1;
            $teamWinnerInfoRes[1] = 2;
            $teamWinnerInfoRes[2] = 1;
        }else{
            $infoFormat['match']['team_1_score'] = (Int)$infoFormat['match']['home_score_score_old'];
            $infoFormat['match']['team_2_score'] = (Int)$infoFormat['match']['away_score_score_old'];
            $teamInfoRes[$infoFormat['match']['home_team_id']] = 1;
            $teamInfoRes[$infoFormat['match']['away_team_id']] = 2;
            $teamWinnerInfoRes[1] = 1;
            $teamWinnerInfoRes[2] = 2;
        }
        unset($infoFormat['match']['home_score_score_old']);
        unset($infoFormat['match']['away_score_score_old']);
        unset($infoFormat['match']['home_team_id']);
        unset($infoFormat['match']['away_team_id']);
        unset($infoFormat['match']['home_name']);
        unset($infoFormat['match']['away_name']);
        //计算比赛的胜算
        $statusInfo = WinnerHelper::DotaWinnerInfo($infoFormat['match']['team_1_score'],$infoFormat['match']['team_2_score'],$matchInfo['number_of_games']);
        if($statusInfo['is_finish']==1){
            $infoFormat['match']['status'] = 3;
            if ($statusInfo['is_draw'] == 1){//判断是不是平局
                $match_real_time_info['winner'] = null;
                $match_real_time_info['is_draw'] = 1;
            }else{
                $infoFormat['match']['winner'] = $teamWinnerInfoRes[$statusInfo['winner_team']];
            }
        }else{
            //没有结束呢 说明他不是结束时间
            $infoFormat['match']['end_at'] = null;
        }

        return $infoFormat;
    }

    public static function refreshInfo($matchId, $info,$refreshType)
    {
        $battle_complete = true;
        $battle_order_num = 0;
        $battle_is_forfeit_order_id = null;
        if ($refreshType == 'refresh'){
            //有battle
            $battleList = MatchBattle::find()->where(['match'=>$matchId])->asArray()->all();
            if (count($battleList)){
                foreach ($battleList as $key =>$item){
                    //判断默认领先
                    $battle_is_default_advantage = $item['is_default_advantage'];
                    if ($battle_is_default_advantage == 1){
                        $battle_order_num = $item['order'];
                    }
                }
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }

            }
        }else{
//            $info['match']['default_advantage'] = 1;//测试用
            //判断是不是有默认领先
            if (isset($info['match']['default_advantage']) && $info['match']['default_advantage']){
                HotBase::createMatchAdvantageBattle($matchId,$info['match']['default_advantage'],0,1,$type='rest_api',3);
                $battle_order_num = 1;
            }
            foreach ($info['battles'] as $key => $battle) {
                if ($battle['base']['order']){
                    $battle['base']['order'] = $battle['base']['order'] + $battle_order_num;
                    $battle_complete = @$battle['base']['complete'];
                    // 根据battleId刷新对应的battle信息
                    self::refreshBattle($matchId, $battle, $key,$refreshType);
                }

            }
        }
        //更新match层信息
        self::setMatch($matchId, $info['match']);
        return $battle_complete;
    }

    private static function refreshBattle($matchId, $battle, $key=0,$refreshType = '')
    {
        //判断有没有对局时长 没有就不创建battle
        if (!isset($battle['base']['duration'])){
            return true;
        }
        $have_battle = false;
        // 找到对应的battleId，更新battle
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
            $have_battle = true;
        }
        $battleId=$battleBase['id'];
        if ($have_battle){
            $infoMainIncrement  = ["diff" => ['battle'=>'add'], "new" => ['match_id'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_MATCH,QueueServer::QUEUE_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_REAL_TIME);

            $infoMainIncrementBattle  = ["diff" => [], "new" => ['id'=>$battleId,'match'=>$matchId]];
            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::QUEUE_TYPE_ADD,$infoMainIncrementBattle,'');
        }

        BattleService::setBattleInfo($battleId,'dota',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'dota',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId,'dota',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'dota',$battle['detail'],BattleService::DATA_TYPE_DETAILS);

        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'dota',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        //判断关联关系
        self::setMatchBattleRelation($matchId,$battleId,$battle);
    }

    private static function setMatchBattleRelation($matchId,$battleId,$battle){
        //插入关联关系
        $rel_matchId = @$battle['base']['rel_match_id'];
        $rel_battleId = @$battle['base']['rel_battle_id'];
        $battle_order = $battle['base']['order'];
        $matchBattleRelation = MatchBattleRelation::find()->where(['match_id'=>$matchId])->andWhere(['order'=>$battle_order])->one();
        if (!$matchBattleRelation){
            //首先插入关系表
            $matchBattleRelation = new MatchBattleRelation();
            $matchBattleRelation->setAttributes([
                'match_id' => (int)$matchId,
                'order' => $battle_order,
                'rel_match_id' => (string)$rel_matchId,
                'rel_battle_id' => (string)$rel_battleId
            ]);
            if (!$matchBattleRelation->save()) {
                throw new BusinessException($matchBattleRelation->getErrors(), "保存battleRelation失败");
            }
        }
        //设置redis 关系
        $redis_name = 'match_battle_info:'.$matchId;
        $Redis = \Yii::$app->get('redis');
        $Redis->select(env('REDIS_DATABASE'));
        $Redis->hset($redis_name,$rel_battleId,$battleId.'-'.$battle_order);
    }


    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    private static function setMatchBase($id, $attribute)
    {
        $matchBase = MatchBase::find()->where(['match_id' => $id])->one();
        if ($matchBase) {
            $matchBase->setAttributes($attribute);
            if (!$matchBase->save()) {
                throw new BusinessException($matchBase->getErrors(), "设置match_base_data失败");
            }
        }
        return true;
    }

    public static function getStandardMatchInfo($standardId)
    {
        return StandardDataMatch::find()->where(['id' => $standardId])->one();
    }

    public static function getBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('match.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('match', 'rel.master_id=match.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_MATCH]);
        return $q->one();
    }

    public static function getInfos($relMatchId)
    {
        $battleInfos = [];
        $matchInfo = self::getMatch($relMatchId);
        if ($matchInfo){
            $match_status_id = $matchInfo['status_id'];
            if ($match_status_id < 5){
                $battles = $matchInfo['box'];
                if ($battles){
                    //数据源创建5个battle，当battle是空的时候不保存
                    for ($i = 1; $i <= $battles;$i++) {
                        $battleBase = self::getBattle($relMatchId,$i);
                        if (!empty($battleBase)){
                            $battleInfos[] = $battleBase;
                        }
                    }
                }
            }
        }
        return [
            'match' => $matchInfo,
            'battles' => $battleInfos
        ];
    }
    /**
     * @param $restInfo
     * @param $matchInfo 比赛基础信息这里有主队客队id
     * @return array
     *
     */
    public static function conversion($Redis,$restInfo,$matchInfo)
    {
        // 转化match
        $formatData = [
            'match' => '',
            'battles' => [],
        ];
        //battle base信息
        $detailInfoConfig = [
            'begin_at' => function ($info) {
                if ($info['match_time']){
                    return $info['match_time'];
                }else{
                    return null;
                }
            },
            'end_at' => function ($info) {
                if ($info['source_updated_at']){
                    return $info['source_updated_at'];
                }else{
                    return null;
                }
            },
            'home_score_score_old' => function ($info) {
                return $info['home_score'];
            },
            'home_name' => function ($info) {
                return $info['home_name'];
            },
            'away_score_score_old' => function ($info) {
                return $info['away_score'];
            },
            'away_name' => function ($info) {
                return $info['away_name'];
            },
            'status' => function($info){
                return $info['status_id'];
            },
            'home_team_id' => function ($info) {
                return $info['home_team'];
            },
            'away_team_id' => function ($info) {
                return $info['away_team'];
            }
        ];
        $matchDetailInfo = Mapping::transformation($detailInfoConfig, $restInfo['match']);
        $formatData['match'] = $matchDetailInfo;
        $formatData['match']['is_draw'] = 2;
        $formatData['match']['is_forfeit'] = 2;
        $formatData['match']['is_battle_detailed'] = 1;
        if (count($restInfo['battles']) > 0){
            foreach ($restInfo['battles'] as $key => $restBattle) {
                // 格式化battle信息，这里需要一些team的信息
                $battleInfo = self::conversionBattle($Redis,$restBattle, $restInfo,$matchInfo);
                if($battleInfo){
                    $formatData['battles'][] = $battleInfo;
                }
            }
        }
        return $formatData;
    }
    //处理battle
    private static function conversionBattle($Redis,$restBattleInfo, $restInfo, $matchInfo)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];
        //$info base_info
        $battleInfoConfig = [
            'rel_battle_id' => 'box_num',
            'rel_match_id' => 'match_id',
            'status' => 'status_id',
            'order' => 'box_num',
            'duration' => 'length_time',
        ];
        $battleBaseAtt = Mapping::transformation($battleInfoConfig, $restBattleInfo['base_info']);
        //base
        $battleBaseAtt['map'] = 1;  //数据源没该数据，取值为metadata_lol_map表的召唤师峡谷
        $battleBaseAtt['is_battle_detailed'] = 1;
        $battleBaseAtt['is_default_advantage'] = 2;
        $battleBaseAtt['is_forfeit'] = 2;
        $battleBaseAtt['is_draw'] = 2;
        $battleBaseAtt['flag'] = 1;
        $battleBaseAtt['deleted'] = 2;
        $battleBaseAtt['deleted_at'] = null;
        $battleBaseAtt['game'] = 2;
        //后面要计算battle的比分 和winner
        $duration = (Int)$battleBaseAtt['duration'];
        //队伍数据 选手数据
        $teamStatic = $players = [];
        $battleDetail = [];
        $battleDetail['is_confirmed'] = 1;
        $battleDetail['is_pause'] = 2;
        $battleDetail['is_live'] = 1;
        //经济曲线
        $rel_net_worth_diff_timeline = $restBattleInfo['base_info']['economy_lines'];
        if ($rel_net_worth_diff_timeline){
            $net_worth_diff_timeline = null;
            $rel_net_worth_diff_timeline_array = json_decode($rel_net_worth_diff_timeline,true);
            foreach ($rel_net_worth_diff_timeline_array as $nw_item){
                $net_worth_diff_timeline_tmp = [
                    'ingame_timestamp' => (Int)$nw_item['time'],
                    'net_worth_diff' => (Int)$nw_item['value']
                ];
                $net_worth_diff_timeline[] = $net_worth_diff_timeline_tmp;
            }
            $battleDetail['net_worth_diff_timeline'] = @json_encode($net_worth_diff_timeline,320);
        }
        //经验曲线
        $rel_experience_diff_timeline = $restBattleInfo['base_info']['economy_lines'];
        if ($rel_experience_diff_timeline){
            $experience_diff_timeline = null;
            $rel_experience_diff_timeline_array = json_decode($rel_experience_diff_timeline,true);
            foreach ($rel_experience_diff_timeline_array as $nw_item){
                $experience_diff_timeline_tmp = [
                    'ingame_timestamp' => (Int)$nw_item['time'],
                    'experience_diff' => (Int)$nw_item['value']
                ];
                $experience_diff_timeline[] = $experience_diff_timeline_tmp;
            }
            $battleDetail['experience_diff_timeline'] = @json_encode($experience_diff_timeline,320);
        }
        //处理战队
        $teamInfosRest = $restBattleInfo['battle_teams'];
        if (empty($teamInfosRest)){
            return $battleInfo;
        }
        $teamJiSuanInfo = [];
        //循环队伍和玩家信息
        foreach ($teamInfosRest as $key => $team) {
            $team_id=self::getMainIdByRelIdentityId('team',$team['team_id'],self::$origin_id_ezplay);
            if (empty($team_id)){
                continue;
            }
            $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
            $teamStatic[$team_order]['order'] = $team_order;
            $teamStatic[$team_order]['rel_score'] = $team['score'];
            // 一血
            if ($team['first_blood'] == 1) {
                $battleDetail['first_blood_p_tid'] = $team_order;
            }
            if ($team['first_tower'] == 1) {
                $battleDetail['first_tower_p_tid'] = $team_order;
            }
            if ($team['first_to_5_kills'] == 1) {
                $battleDetail['first_to_5_kills_p_tid'] = $team_order;
            }
            if ($team['first_to_10_kills'] == 1) {
                $battleDetail['first_to_10_kills_p_tid'] = $team_order;
            }
            $teamStatic[$team_order]['rel_team_id'] = @$team['team_id'];
            $teamStatic[$team_order]['rel_identity_id'] = $team['team_id'];
            $teamStatic[$team_order]['team_id'] = $team_id;
            //faction
            if ($team['side'] == 1){
                $teamStatic[$team_order]['faction'] = 'radiant';
            }else{
                $teamStatic[$team_order]['faction'] = 'dire';
            }
            $teamJiSuanInfo[$teamStatic[$team_order]['faction']] = $team_order;
            //kills
            $team_kills = empty($team['kills']) ? 0 : $team['kills'];
            $teamStatic[$team_order]['kills'] = $team_kills;
            //net_worth
            $teamStatic[$team_order]['net_worth'] = $team['economy'];
            //gold_earned
            $team_gold = $team['economy'];
            $teamStatic[$team_order]['gold_earned'] = $team_gold;
            //experience
            $teamStatic[$team_order]['experience'] = $team['experience'];
            //tower_kills
            $teamStatic[$team_order]['tower_kills'] = $team['destroyed_tower'];
            //melee_barrack_kills
            //ranged_barrack_kills
            //roshan_kills
            //building_status
            $building_status = HotBase::getBuildingDefaultStatusOnFrameByPandascore($team['faction']);
            $team_building_status = json_encode($building_status,320);
            $teamStatic[$team_order]['building_status'] = $team_building_status;
            //building_status_towers
            //building_status_barracks
            //building_status_ancient
            //deaths
            $teamStatic[$team_order]['deaths'] = empty($team['deaths']) ? 0 : $team['deaths'];
            //assists
            $teamStatic[$team_order]['assists'] = empty($team['assists']) ? 0 : $team['assists'];
            //net_worth_diff
            $teamStatic[$team_order]['net_worth_diff'] = $team['leading_economy'];
            //experience_diff 后续计算
            //barrack_kills 摧毁兵营
            $teamStatic[$team_order]['barrack_kills'] = $team['destroyed_barracks'];
            //building_status_outposts
            //total_runes_pickedup
            //bounty_runes_pickedup
            //double_damage_runes_pickedup
            //haste_runes_pickedup
            //illusion_runes_pickedup
            //invisibility_runes_pickedup
            //regeneration_runes_pickedup
            //arcane_runes_pickedup
            //smoke_purchased
            //smoke_used
            //dust_purchased
            //dust_used
            //observer_wards_purchased
            //observer_wards_placed
            //observer_wards_kills
            //sentry_wards_purchased
            //sentry_wards_placed
            //sentry_wards_kills
            //初始化一些需要计算的信息
            $team_net_worth = 0;
            // 队员信息转化
            $rel_players = $team['players'];
            foreach ($rel_players as $k_p => $playerRest){
                //注销的是没有
                $playerConfig = [
                    //order  下面赋值
                    'rel_identity_id' => 'player_id',
                    "rel_team_id" => 'team_id',
                    //team_order  下面赋值
                    'seed' => 'position',
                    //battle_id
                    //game
                    //match
                    //team_id 下面赋值
                    //faction
                    //role
                    //lane
                    "player_id" => 'player_id',//选手id
                    //nick_name
                    "rel_nick_name" => 'name_en',
                    "hero" => 'hero_id',//英雄
                    "level" => "level",//等级
                    //is_alive
                    'ultimate_cd' => 'ultimate_status',
                    //coordinate
                    "kills" => "kills",//击杀
                    //double_kill
                    //triple_kill
                    //ultra_kill
                    //rampage
                    //largest_multi_kill
                    //largest_killing_spree
                    "deaths" => "deaths",//死亡
                    "assists" => "assists",//助攻
                    //kda 下面计算
                    //participation  下面计算
                    "last_hits" => "last_hits",//补刀
                    //lane_creep_kills
                    //neutral_creep_kills
                    //neutral_minion_team_jungle_kills
                    //neutral_minion_enemy_jungle_kills
                    //lhpm 下面计算
                    "denies" => "denies",//反补
                    'net_worth' => 'net_worth',
                    "gold_earned" => function ($params) {
                        return $params['gold'] + $params['gold_spend'];
                    },//金币获取
                    "gold_spent" => "gold_spend",//已花费金币
                    "gold_remaining" => "gold",//剩余金币
                    "gpm" => "gpm",//分均金钱
                    //gold_earned_percent 下面计算
                    //experience
                    "xpm" => "xpm",//分均经验
                    //damage_to_heroes
                    //dpm_to_heroes
                    //damage_percent_to_heroes
                    //damage_taken
                    //dtpm
                    //damage_taken_percent 承伤占比
                    //damage_to_heroes_hp_removal
                    //damage_to_heroes_magical
                    //damage_to_heroes_physical
                    //damage_to_heroes_pure
                    //damage_by_mobs_hp_removal
                    //damage_by_mobs_magical_dmg
                    //damage_by_mobs_physical_dmg
                    //damage_by_mobs_pure_dmg
                    //damage_to_towers 对防御塔伤害
                    //damage_from_heroes_hp_removal
                    //damage_from_heroes_magical_dmg
                    //damage_from_heroes_physical_dmg
                    //damage_from_heroes_pure_dmg
                    //damage_from_mobs_hp_removal
                    //damage_from_mobs_magical_dmg
                    //damage_from_mobs_physical_dmg
                    //damage_from_mobs_pure_dmg
                    //damage_conversion_rate 伤害转化率
                    //total_heal
                    //total_crowd_control_time
                    //wards_purchased
                    //wards_placed
                    //wards_kills
                    //observer_wards_purchased
                    //observer_wards_placed
                    //observer_wards_kills
                    //sentry_wards_purchased
                    //sentry_wards_placed
                    //sentry_wards_kills
                    //camps_stacked
                    //runes_detail_double_damage_runes
                    //runes_detail_haste_runes
                    //runes_detail_illusion_runes
                    //runes_detail_invisibility_runes
                    //runes_detail_regeneration_runes
                    //runes_detail_bounty_runes
                    //runes_detail_arcane_runes
                    'items' => 'equipments_id_all',
                    //summoner_spells
                    //abilities_timeline
                    //items_timeline
                    'talents' => 'rune',
                    //is_visible
                    //respawntimer
                    //buyback_cooldown
                    'buyback_cost' => 'buyback_cost',
                    //health
                    //health_max
                    //tower_kills
                    //barrack_kills
                    //melee_barrack_kills
                    //ranged_barrack_kills
                    //roshan_kills
                    //net_worth_percent
                    //gold_reliable
                    //gold_unreliable
                    //gold_herokill
                    //gold_creepkill
                    //damage_taken_hp_removal
                    //damage_taken_magical
                    //damage_taken_physical
                    //damage_taken_pure
                    //damage_to_buildings
                    //damage_to_objectives
                    //total_runes_pickedup
                    //bounty_runes_pickedup
                    //double_damage_runes_pickedup
                    //haste_runes_pickedup
                    //illusion_runes_pickedup
                    //invisibility_runes_pickedup
                    //regeneration_runes_pickedup
                    //arcane_runes_pickedup
                    //smoke_purchased
                    //smoke_used
                    //dust_purchased
                    //dust_used
                    //steam_id
                ];
                $player = Mapping::transformation($playerConfig, $playerRest);
                $player['order']=$k_p+1;
                $player['team_order']= $team_order;
                $player['team_id']= $team_id;
                //以下需要计算得属性
                //kda
                $deaths = $player['deaths'] > 0 ? $player['deaths'] : 1;
                $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
                // participation 参团率
                if($team_kills){
                    $player['participation'] = (String)round((($player['kills'] + $player['assists'])/$team_kills),4);
                }else{
                    $player['participation'] = null;
                }
                $team_gold_res = $team_gold ? $team_gold : 1;
                $player['gold_earned_percent'] = (String)round(($player['gold_earned'] / $team_gold_res),4) ;
                //分均补刀 lhpm
                $player['lhpm'] = (String)round(($player['last_hits']/($duration/60)),2);
                //最终结果
                $players[] = $player;
            }
        }
        //循环team 信息
        foreach ($teamJiSuanInfo as $k_jisuan => $item_jisuan){
            if ($k_jisuan == 'radiant'){
                if ($item_jisuan == 1){
                    $team_experience_diff = $teamStatic[1]['experience'] - $teamStatic[2]['experience'];
                    $teamStatic[1]['experience_diff'] = $team_experience_diff;
                    $teamStatic[2]['experience_diff'] = -$team_experience_diff;
                }
                if ($item_jisuan == 2){
                    $team_experience_diff = $teamStatic[2]['experience'] - $teamStatic[1]['experience'];
                    $teamStatic[2]['experience_diff'] = $team_experience_diff;
                    $teamStatic[1]['experience_diff'] = -$team_experience_diff;
                }
            }
        }
        $battleInfo['detail'] = $battleDetail;
        //ban_pick
        $banArray = $pickArray = [];
        //循环获取ban_pick
        foreach ($restBattleInfo['battle_teams'] as $ban_pick_key => $ban_pick_item) {
            $rel_team_id = $ban_pick_item['team_id'];
            if ($ban_pick_item['ban_id']){
                $ban_ids_array = json_decode($ban_pick_item['ban_id'],true);
                foreach ($ban_ids_array as $ban_key => $ban_item){
                    $ban_tmp = [
                        'hero' => $ban_item,
                        'team' => $rel_team_id,
                        'type' => 1,
                        'order' => null
                    ];
                    $banArray[] = $ban_tmp;
                }
            }
            if ($ban_pick_item['pick_id']){
                $pick_ids_array = json_decode($ban_pick_item['pick_id'],true);
                foreach ($pick_ids_array as $pick_key => $pick_item){
                    $pick_tmp = [
                        'hero' => $pick_item,
                        'team' => $rel_team_id,
                        'type' => 2,
                        'order' => null
                    ];
                    $pickArray[] = $pick_tmp;
                }
            }
        }
        $battleInfo['ban_pick'] = array_merge($banArray, $pickArray);
        $battleInfo['players'] = $players;
        $battleInfo['static'] = $teamStatic;
        //基础数据
        $battleInfo['base'] = $battleBaseAtt;
        return $battleInfo;
    }

    private static function getMatch($rel_matchId)
    {
        //获取比赛信息
        return RadarPurpleMatch::find()->where(['match_id' => $rel_matchId])->asArray()->one();
    }

    private static function getBattle($rel_matchId,$rel_battleId)
    {
        $res = [];
        //获取battle base信息
        $battle_base = RadarPurpleBattle::find()->where(['match_id'=>$rel_matchId])->andWhere(['box_num'=>$rel_battleId])->asArray()->one();
        //获取队伍信息 2个队伍
        $battle_teams = RadarPurpleBattleTeam::find()->where(['match_id'=>$rel_matchId])->andWhere(['box_num'=>$rel_battleId])->asArray()->all();
        if ($battle_teams){
            foreach ($battle_teams as $key => &$item){
                $team_id = $item['team_id'];
                //获取player信息
                $battle_players = RadarPurplePlayer::find()->where(['match_id'=>$rel_matchId])->andWhere(['box_num'=>$rel_battleId])->andWhere(['team_id'=>$team_id])->asArray()->all();
                $item['players'] = $battle_players;
            }
        }
        if (!empty($battle_base) && !empty($battle_teams)){
            $res = [
                'base_info' =>$battle_base,
                'battle_teams' => $battle_teams
            ];
        }
        return $res;
    }
    //处理玩家的道具
    private static function radarPurpleConversionPlayerItems($Redis,$playerItems){
        $playerItemsArray = [];
        if ($playerItems){
            $playerItemsArray = json_decode($playerItems,true);
        }
        $inventory = $backpack = $stash = $buffs = [];
        //首先判断他有几个 大于6个就放到backpack 背包
        $inventory = self::getFormatFrameItem( $Redis,$playerItemsArray, 'itemsInventory');
        $backpackArr = self::getFormatFrameItem($Redis,null,'itemsBackpack');
        $neutralArr = self::getFormatFrameItem($Redis,null,'itemNeutral');
        $stashArr = self::getFormatFrameItem($Redis,null,'itemsStash');
        $mob_inventoryArr = self::getFormatFrameItem($Redis,null,'itemsBear');
        $mob_backpackArr = self::getFormatFrameItem($Redis,null,'mob_backpack');
        $returnArr['inventory'] = $inventory;//物品栏
        $returnArr['backpack'] = $backpackArr;//背包
        $returnArr['neutral'] = $neutralArr;//野外掉落
        $returnArr['stash'] = $stashArr;//储藏处
        $returnArr['buffs'] = null;//增益
        $returnArr['mob_inventory'] = $mob_inventoryArr;//召唤物物品栏
        $returnArr['mob_backpack'] = $mob_backpackArr;//召唤物背包
        $returnArr['mob_neutral'] = null;//召唤物野外掉落
        return json_encode($returnArr,320);
    }
    //道具
    public static function getFormatFrameItem($Redis, $item, $itemKey){
        $itemsListRedisName = 'dota2:items:list';
        $itemsRelationListRedisName = 'dota2:items:relation:'.self::$origin_id_ezplay;
        //获取匹配不到的数据
        $itemUnknownInfo = $Redis->get('dota2:items:list:unknown');
        //下面转化
        $inventoryArr = [];
        $mutiArr = [
            'itemsInventory',
            'itemsBackpack',
            'itemsStash',
            'itemsBear',
            'mob_backpack',
        ];
        if (in_array($itemKey,$mutiArr)){
            if (empty($item)){
                if ($itemKey=='itemsInventory'||$itemKey=='itemsStash'||$itemKey=='itemsBear'){
                    for ($i=1;$i<=6;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }else{
                    for ($i=1;$i<=3;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }
            }else{
                $inventoryArrRes = [];
                $item_num = 0;
                foreach ($item as $k => $v) {
                    if ($v['equipments_id']){
                        $item_num ++;
                        $inventoryArrData = [];
                        //查找装备的主表ID
                        $dbItemId = $Redis->hGet($itemsRelationListRedisName,$v['equipments_id']);
                        if (empty($dbItemId)){
                            $itemResInfo = $itemUnknownInfo;
                        }else{
                            $itemResInfo = $Redis->hGet($itemsListRedisName,$dbItemId);
                        }
                        $itemResInfoArray = json_decode($itemResInfo,true);
                        //处理结果 并赋值
                        $inventoryArrData['item_id']         = (int)$itemResInfoArray['id'];
                        $inventoryArrData['purchase_time']   = null;
                        $inventoryArrData['cooldown']        = null;
                        $inventoryArrData['total_cost']      = (Int)$itemResInfoArray['total_cost'];
                        $inventoryArrRes[] = $inventoryArrData;
                    }
                }
                $total_cost_array = array_column($inventoryArrRes,'total_cost');
                array_multisort($total_cost_array,SORT_DESC,$inventoryArrRes);
                foreach ($inventoryArrRes as $k=>$v){
                    unset($inventoryArrRes[$k]['total_cost']);
                }
                if ($itemKey == 'itemsInventory' || $itemKey == 'itemsStash' || $itemKey == 'itemsBear') {
                    if ($item_num < 6) {
                        for ($j = $item_num+1; $j <= 6; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                    if ($item_num > 6) {
                        $inventoryArrRes = array_slice($inventoryArrRes,0,6);
                    }
                }else{
                    if ($item_num < 3) {
                        for ($j = $item_num+1; $j <= 3; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                }
                $inventoryArrMiddle = [];
                foreach ($inventoryArrRes as $index_key => $val){
                    $inventoryKey = $index_key + 1;
                    $inventoryArrMiddle['slot_'.$inventoryKey] = $val;
                }
                $inventoryArr = $inventoryArrMiddle;
            }

        }else{
            if (empty($item)){
                return null;
            }
            $dbItemId =  self::getMainIdByRelIdentityIdOrUnknown('dota2_item',$item['id'],3);
            $dbItem         = MetadataDota2Item::find()->where(['id'=>$dbItemId])->asArray()->one();
            if (empty($dbItem)){
                return null;
            }
            $inventoryArr['item_id']         = (int)$dbItem['id'];
            $inventoryArr['purchase_time']   = null;
            $inventoryArr['cooldown']        = null;
            $inventoryArr['total_cost']      = $dbItem['total_cost'];
        }
        return $inventoryArr;
    }

    //处理天赋
    private static function radarPurpleConversionPlayerTalents($Redis,$playerTalents,$origin_id){
        $talents = [];
        if (count($playerTalents)>0){
            $playerTalentsArray = json_decode($playerTalents,true);
            $talentsRelationRedisName = 'dota2:talent:relation:'.$origin_id;
            $talentsUnknownInfo = $Redis->get('dota2:talent:list:unknown');
            $talentsUnknownInfoArray = json_decode($talentsUnknownInfo,true);
            $talentsUnknownId = $talentsUnknownInfoArray['id'];
            foreach ($playerTalentsArray as $key => $item){
                if ($item['is_choose']){
                    $talent_id = $Redis->hGet($talentsRelationRedisName,$item['id']);
                    if (empty($talent_id)){
                        $talent_id = $talentsUnknownId;
                    }
                    $talents[$key]['talent_id'] = $talent_id;
                    $talents[$key]['is_chosen'] = true;
                }
            }
        }
        if ($talents){
            $talents = array_values($talents);
            $talents_res = json_encode($talents,320);
        }else{
            $talents_res = null;
        }
        return $talents_res;
    }
}