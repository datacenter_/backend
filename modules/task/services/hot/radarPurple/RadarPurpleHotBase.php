<?php
/**
 *
 */

namespace app\modules\task\services\hot\radarPurple;

use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class RadarPurpleHotBase extends HotBase
{
    public static function getTeamOrderByTeamId($teamId,$matchInfo)
    {
        if($matchInfo['team_1_id']==$teamId){
            return 1;
        }
        if($matchInfo['team_2_id']==$teamId){
            return 2;
        }
        throw new BusinessException([],'比赛的2个队伍信息，未匹配战队ID为:'.$teamId);
    }
}