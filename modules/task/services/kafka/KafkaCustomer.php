<?php


namespace app\modules\task\services\kafka;
use app\modules\common\services\Consts;
use app\modules\match\models\MatchLived;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchLivedDatasOffset;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use RdKafka;
use Throwable;
use Redis;

/**
 * kafka生产者功能
 * Class KafkaProducer
 * @package app\modules\task\services\kafka
 */
class KafkaCustomer
{
    public $redis;
    public $offset;
    public $topic;
    public $kafka_time;

    public static function customLiveData($groupId,$topicName,$broken)
    {
        if (empty($groupId)||empty($topicName)){
            return false;
        }

        $self        = new self();
        $self->topic = $topicName;
        //设置消费组 test-2(测试) consumer-group3(正式)
        $conf = new \RdKafka\Conf();
        $conf->set('group.id', $groupId);
        $conf->set('enable.auto.commit', 0);
        //设置broker BROKERS_OUT(外网ip)  BROKERS_INTERNAL(内网ip)
        $rk = new \RdKafka\Consumer($conf);
        $rk->addBrokers($broken);
        //设置topic
        $topicConf = new \RdKafka\TopicConf();
        $topic     = $rk->newTopic($topicName, $topicConf);
        //设置redis
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        $self->redis = $redis;
        //消费起始位
        if ($topicName == 'sparkstreamingsql_test') {
            $offsetListKey = 'kafka:offset_list';
        } else {
            $offsetListKey = 'kafka:' . $topicName . ':offset_list';
        }
        $endOffset = $redis->lIndex($offsetListKey, 0);
        if ((is_numeric($endOffset) && $endOffset > 0) || $endOffset === 0) {
            $nowStartOffset = $endOffset + 1;
            //如果offset存在则从offset继续消费
            $topic->consumeStart(0, $nowStartOffset);
        } else {
            //如果没有offset则从最新的开始消费
            $topic->consumeStart(0, RD_KAFKA_OFFSET_END);
        }

        $repeatCount = 0;
        $newCount    = 0;
        $count       = 0;

        while (true) {
            //参数1表示消费分区，这里是分区0
            //参数2表示同步阻塞多久
            $message = $topic->consume(0, 12 * 1000);
            if (is_null($message)) {
                sleep(1);
                echo 'No more messages' . PHP_EOL;
                continue;
            }
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    //阻止重复消费offset
                    $dataOffsetList = $redis->lRange($offsetListKey, 0, -1);
                    if (!empty($dataOffsetList) && is_array($dataOffsetList) && in_array($message->offset, $dataOffsetList)) {
                        //记录尝试重复消费的offset
                        $offsetRepeatKey = 'kafka:' . $topicName . ':repeat_list';
                        $redis->lPush($offsetRepeatKey, $message->offset);
                        if ($redis->lLen($offsetRepeatKey) > 1000) {
                            $redis->rPop($offsetRepeatKey);//删除旧的重复offset,只保留最近1000条,防止list无限增长
                        }
                        echo 'repeatOffset ' . $message->offset . ' topic' . $self->topic . PHP_EOL;
                        break;
                    }
                    //记录每个topic的offset
                    $self->kafka_time = date('Y-m-d H:i:s', substr($message->timestamp, 0, -3));
                    $self->offset     = $message->offset;
                    $redis->lPush($offsetListKey, $message->offset);
                    if ($redis->lLen($offsetListKey) > 100) {
                        $redis->rPop($offsetListKey);//删除旧的offset,只保留最近100条,防止list无限增长
                    }
                    if ($message->payload) {
                        //双服务器的排重原则: 插入自己的,判断对面的
                        $revNum          = '';
                        $redisOldKey     = '';
                        $arrRev          = [1 => 2, 2 => 1];
                        $redisKey        = 'kafka:' . $message->payload;
                        $payLoadArr      = json_decode($message->payload, true);
                        $match_data_json = $payLoadArr['match_data'];
                        $match_data_arr  = json_decode($match_data_json, true);

                        if (array_key_exists('server_id',$match_data_arr)){
                            $serverNum = $match_data_arr['server_id'];
                            $payLoadArr['socket_time'] = substr($match_data_arr['socket_time'],0,19);//dalei的时间带有毫秒
                            if ($serverNum == 1 || $serverNum == 2) {
                                //判断是否带有服务器编号
                                $revNum                = $arrRev[$serverNum];
                                $match_data_field      = $match_data_arr['payload'];
                                $match_data_field_json = json_encode($match_data_field, 320);
                                if ($match_data_field=='ping'){
                                    echo 'pingInfo ' . $message->offset . ' topic' . $self->topic . PHP_EOL;
                                    break;
                                }
                                //插入jianqi的统计信息(比赛的最后一条的比赛时间),除了bayes的比赛
                                if ($payLoadArr['origin_id'] != 9&&$match_data_field_json!='{"log":[{"Status":[]}]}') {
                                    $nowDay = date('Y-m-d');
                                    $redis->hSet('match_last_socket_time:' . $nowDay, $payLoadArr['match_id'] . '_s' . $serverNum, $payLoadArr['socket_time']);
                                }
                            } else {
                                echo 'errorServerId ' . $message->offset . ' topic' . $self->topic . PHP_EOL;
                                break;
                            }

                            if ($revNum) {
                                $redisKey    = 'kafka:' . $revNum . ':' . $match_data_field_json;
                                $redisOldKey = 'kafka:' . $serverNum . ':' . $match_data_field_json;
                            }else{
                                echo 'errorServerId2 ' . $message->offset . ' topic' . $self->topic . PHP_EOL;
                                break;
                            }

                            $payLoadArr['match_data'] = $match_data_field_json;
                            $matchContent = $payLoadArr;
                        } else {
                            $arr                = json_decode($message->payload, true);
                            $arr['socket_time'] = $self->kafka_time;
                            $matchContent   = $arr;
                        }
                        if ($revNum && $redisOldKey) {
                            //原则: 插入自己的,判断对面的
                            $getOther  = $redis->get($redisKey);
                            $setResult = !$getOther;
                            if (empty($getOther)) {
                                //说明另一个服务器10秒内没有插入过,是正确的数据
                                $redis->set($redisOldKey, 'yes', ['nx', 'ex' => 10]);
                            }
                        }else{
                            $setResult          = $redis->set($redisKey, 'yes',['nx','ex'=>10]);
                        }
                        if ($setResult) {
                            //不重复的数据
                            $self->doLivesData($matchContent);//插入到表,插入rocket队列
                            $newCount++;
                        } else {
                            //重复的数据
                            $repeatCount++;
                        }
                    }
                    $count++;
                    echo 'success ' . $count .' common ' . $newCount.' repeat ' . $repeatCount . ' offset ' . $message->offset.PHP_EOL;
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:

                    echo 'No more messages; will wait for more' . PHP_EOL;
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo 'Timed out' . PHP_EOL;
                    break;
                default:
                    print_r($message->err);
                    break;
            }
        }
    }

    public function doLivesData($data)
    {
        $origin_id    = $data['origin_id'];
        $game_id      = $data['game_id'];
        $rel_match_id = $data['match_id'];
        $round        = $data['round'];
        $match_data   = $data['match_data'];
        $type         = $data['type'];
        $socket_time  = $data['socket_time'];
        $task         = $data['task'];
        if (empty($origin_id) || empty($game_id)) {
            echo 'no origin_id or no game_id';
            return false;
        }
        if (empty($socket_time)) {
            $socket_time = date('Y-m-d H:i:s');
        }
        if ($origin_id == 2 && $game_id == 3) {//Abios dota 需要根据battleID 获取原始比赛ID
            //查询原始比赛ID
            $match_data_array = @json_decode($match_data, true);
            if ($match_data_array) {
                $rel_battle_id     = $match_data_array['payload']['match']['id'];
                $standardMatchInfo = StandardDataMatch::find()->where(['like', 'battlesIds', $rel_battle_id])->asArray()->one();
                $rel_match_id      = @$standardMatchInfo['rel_identity_id'];
            }
        }
        if ($origin_id == 4 && $match_data == 'ping'){
            return true;
        }
        $insertData      = [
            'origin_id'   => $origin_id,
            'game_id'     => $game_id,
            'match_id'    => (string)$rel_match_id,
            'round'       => (string)$round,
            'match_data'  => $match_data,
            'type'        => $type,
            'socket_time' => $socket_time
        ];
        $MatchLivedModel = new MatchLived();
        $MatchLivedModel->setAttributes($insertData);
        if ($MatchLivedModel->save()) {
            $redisStatus = $this->getAutoStatusByRedis($origin_id, $rel_match_id);
            if ($redisStatus){
                if ($redisStatus == 1) {
                    $auto_status = true;
                } else {
                    $auto_status = false;
                }
            }else{
                $db_status = MatchService::getMatchConfigAndHotDataByLog($origin_id, $game_id, $rel_match_id, $socket_time);
                if ($db_status == 1) {
                    $auto_status = true;
                } else {
                    $auto_status = false;
                }
                $this->redis->set('kafka:autoStatus:' . $origin_id . ':' . $rel_match_id,$db_status,60);
            }

            $socket_id   = $MatchLivedModel->id;
            $modelMatchOffset = new MatchLivedDatasOffset();
            $modelMatchOffset->setAttribute('data_id',(int)$socket_id);
            $modelMatchOffset->setAttribute('offset',(string)$this->offset);
            $modelMatchOffset->setAttribute('topic',(string)$this->topic);
            $modelMatchOffset->setAttribute('help_field',(string)$this->kafka_time);
            $modelMatchOffset->save();
            if ($task == 1 && in_array($game_id, [1, 2, 3]) && $auto_status) {
                //添加到队列
                $insertData['socket_id']     = $socket_id;
                $newInsertData               = @$insertData;
                $newInsertData['match_data'] = @json_decode($match_data, true);
                $api_server    = null;
                $socket_server = null;
                $taskType      = 4;
                if ($game_id == 2) {
                    $taskType = '9002';
                    if ($origin_id == 4||$origin_id == 9 || $origin_id == 10||$origin_id == 3) {//玩家的是ws to api
                        $socket_server = 'order_lol_' . $rel_match_id;
                    } else {//现在是有3 ps
                        $api_server    = 'order_lol_' . $rel_match_id;
                        //$socket_server = 'order_lol_socket_' . $rel_match_id;
                    }
                }
                if ($game_id == 1) {
                    if ($origin_id == 7||$origin_id == 9 || $origin_id == 10) {
                        $socket_server = 'order_hltvcsgo_' . $rel_match_id;
                    } else {
                        //origin_id 3
                        $api_server    = 'order_csgo_' . $rel_match_id;
                        $socket_server = 'order_csgo_' . $rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($game_id == 3) {
                    if ($origin_id == 9 || $origin_id == 10 || $origin_id == 3) {
                        $socket_server = 'order_dota_' . $rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($api_server) {
                    //设置队列
                    $item = [
                        "tag"          => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $game_id),
                        "type"         => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                        "batch_id"     => date("YmdHis"),
                        "params"       => $newInsertData,
                        "redis_server" => $api_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }
                if ($socket_server) {
                    $item = [
                        "tag"          => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $game_id),
                        "type"         => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id"     => date("YmdHis"),
                        "params"       => $newInsertData,
                        "redis_server" => $socket_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }

            }
        } else {
            echo '插入数据库错误';
            return false;
//            throw new BusinessException($MatchLivedModel->getErrors(), '插入数据库错误');
        }
        return true;
    }

    public static function test1(){
        while (true){
            echo '1'.PHP_EOL;
            sleep(1);
        }
    }



    public function getAutoStatusByRedis($origin_id, $rel_match_id)
    {
        $res = $this->redis->get('kafka:autoStatus:' . $origin_id . ':' . $rel_match_id);
        return $res;

    }
    /**
     * set error_info
     * @param $key
     * @param $info
     */
    public static function error_catch_log($info)
    {
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        if (is_array($info)) {
            $info['error_time'] = date('Y-m-d-H:i:s');
        }
        $redis->lPush('kafka:error_catch_log', json_encode($info, 320));
    }

    public static function incr_log()
    {
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        $redis->incr('kafka:match_producer');

    }


}