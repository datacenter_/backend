<?php


namespace app\modules\task\services\kafka;
use RdKafka;
use Throwable;
use Redis;

/**
 * kafka生产者功能
 * Class KafkaProducer
 * @package app\modules\task\services\kafka
 */
class KafkaProducer
{


    public static function KafkaMatchData($topic, $match_data)
    {

        if (!is_array($match_data) || empty($match_data)) {
            self::error_catch_log(['action'=>'kafka match_data error']);
            return false;
        }
        try {
            $rk = new RdKafka\Producer();
            $rk->setLogLevel(LOG_DEBUG);
            $rk->addBrokers(env('KAFKA_BROKERS_INTERNAL'));//env('KAFKA_BROKERS_INTERNAL')   env('KAFKA_BROKERS_OUT')
            $topic       = $rk->newTopic($topic);
            $arr['data'] = $match_data;
            $data        = json_encode($arr, 320);
            $res         = $topic->produce(RD_KAFKA_PARTITION_UA, 0, $data);
            $rk->flush(-1);
            self::incr_log();
        }catch(Throwable $e){
            self::error_catch_log(['action'=>'kafka try error','msg'=>$e->getMessage()]);
        }

    }

    /**
     * set error_info
     * @param $key
     * @param $info
     */
    public static function error_catch_log($info)
    {
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        if (is_array($info)) {
            $info['error_time'] = date('Y-m-d-H:i:s');
        }
        $redis->lPush('kafka:error_catch_log', json_encode($info, 320));
    }

    public static function incr_log()
    {
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        $redis->incr('kafka:match_producer');

    }


}