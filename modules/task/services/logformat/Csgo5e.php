<?php

namespace app\modules\task\services\logformat;

use app\modules\data\models\ReceiveData5eplay;
use app\modules\match\models\SortingLog;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\rest\exceptions\BusinessException;
use app\modules\common\services\SteamHelper;

/**
 * Class Csgo5e
 * @package app\modules\task\services\logformat
 * 我们在这里把所有的事件提取出来存入新表中
 */
class Csgo5e
{
    // match开始
    const EVENT_TYPE_MATCH_UP_COMING = "Match_Upcoming";//match比赛开始

//battle开始结束判断

    //5E：Pug_Start 对局即将开始（关键信息：对局马上开始，可以开始日志分拣×）
    //5E：Pug_End 对局已经结束（关键信息：对局已经结束，可以截断日志×）

//battle

    const EVENT_TYPE_MATCH_STARTED = "Match_Start";//对局开始（关键信息：时间√、对局开始√、地图√、起始阵营（子事件×）√）
    const EVENT_TYPE_TEAM_PLAYING_CT = "Team playing CT";//起始为反恐精英的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_TEAM_PLAYING_T = "Team playing TERRORIST";//起始为恐怖分子的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_GAME_OVER = "Game Over";//对局结束（应该用World triggered \"Match_End\"更为准确）（关键信息：时间√、对局结束√、地图（子事件×）×、结束时比分（子事件×）×、比赛时长（子事件×）×）
    const EVENT_TYPE_MP_PAUSE_MATCH = "Match pause is enabled - mp_pause_match";//地图指令比赛暂停（关键信息：暂停√）
    const EVENT_TYPE_MP_UNPAUSE_MATCH = "Match pause is disabled - mp_unpause_match";//地图指令比赛暂停取消（关键信息：取消暂停√）
    const EVENT_TYPE_MATCH_SWITCH_TEAM = "Match_SwitchTeam";//常规换边（关键信息：战队当前阵营更换√）
    //Match_SwitchTeamOvertime//加时换边（关键信息：战队当前阵营更换×）

//round

    const EVENT_TYPE_ROUND_START = "round_start";//回合开始（关键信息：时间√、回合开始√）
    const EVENT_TYPE_ROUND_END = "round_end";//回合结束（关键信息：时间√、回合结束√、获胜方式（子事件√）√、回合结束后的当前比分（子事件√）×、存活人员（子事件√）×、存活人数（子事件√）×）
    const EVENT_TYPE_DETAIL_WIN_TYPE = 'win_type';//获胜类型
    const TYPE_WIN_TYPE_T_WIN = 'T_win';//反恐精英全部阵亡（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_CT_WIN = 'CT_win';//恐怖分子被全歼（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_TARGET_BOMBED = 'Bomb exploded';//炸弹爆炸（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_BOMB_DEFUSED = 'Bomb defused';//拆除炸弹（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_TARGET_SAVED = 'Target saved';//时间耗尽（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_ALIVE_PLAYERS = 'alive_players';//存活人员（关键信息：存活人员、存活人数）
    const TYPE_LEFT_BUY_ZONE_WITH = "left buyzone with";//离开购买区（5E：应该用Starting Freeze period初始化更准确）（关键信息：时间√、回合统计数据初始化√）
    const EVENT_TYPE_KILLED = "player_kill";//击杀（关键信息：时间√、杀手信息√、受害者信息√、武器√、是否爆头√、致死伤害（子事件√）×、命中部位（子事件√）×、助攻者信息（子事件√）×、闪光弹助攻者信息（子事件√）×、）
    const EVENT_TYPE_ATTACKED = "attacked";//攻击（关键信息：致死伤害、命中部位）
    const EVENT_TYPE_ASSISTED_KILLING = "assist";//助攻（关键信息：助攻者信息）
    const EVENT_TYPE_FLASH_ASSISTED_KILLING = "flashassist";//闪光弹助攻（关键信息：闪光弹助攻者信息）
    const EVENT_TYPE_PLANTED_THE_BOMB = "bomb_planted";//安放炸弹（关键信息：安放炸弹、安放炸弹的选手）
    const EVENT_TYPE_DEFUSED_THE_BOMB = "bomb_defused";//拆除炸弹（关键信息：拆除炸弹、拆除炸弹的选手）
    // 特殊事件
    const EVENT_TYPE_ROUND_DRAW = "round_draw";//比赛平局
    const EVENT_TYPE_MATCH_RELOADED = "match_reloaded";//重新加载


//没用到的？
    const TYPE_EVENT_PAUSE_START = "Pause start";//暂停指令
    const TYPE_EVENT_PAUSE_END = "Pause end";//暂停结束
    const EVENT_TYPE_LOADING_MAP = "Loading map";//加载地图
    const EVENT_TYPE_MATCH_HAS_END = "match_has_end";//比赛结束
    const EVENT_TYPE_MAP_START = "map_start";//地图开启
    // const EVENT_TYPE_ROUND_DRAW = "SFUI_Notice_Round_Draw";//比赛平局(重复抛掉)
    const EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH = "detail_choose_team_switch"; //加时换边，找到之'Match_SwitchTeamOvertime'
    const EVENT_TYPE_KNIFE_ROUND_START = "knife_round_start";//刀局开始
    const EVENT_TYPE_ROUND_SCORES = 'round_score';
    const EVENT_TYPE_BATTLE_SCORES = 'battle_scores';





    public static function run($tagInfo, $taskInfo)
    {
        $params=json_decode($taskInfo['params'],true);
        // 这里面是待执行的消息
        self::dealWithLine($params);
        return [];
    }

    /**
     * @param $logBeginTime
     * @param $logEndTime
     * @param $ipAddress
     * @return array
     * 获取日志列表并格式化
     */
    public static function formatList($logBeginTime, $logEndTime, $ipAddress)
    {
        $find = 'log';
        // 约束条件
        $map = ['>=', 'created_at', $logBeginTime];
        $where = ['<=', 'created_at', $logEndTime];
        $ip = ['=', 'ip_address', $ipAddress];
        $q = ReceiveData5eplay::find()
            ->select($find)
            ->orderBy('id')
            ->where($map)
            ->andWhere($ip)
            ->andWhere($where);
        $perPage = 1000;
        $count = $q->count();
        $eventList = [];
        $eventListTimeKey = [];
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset($perPage * ($page - 1))
                ->asArray()->all();
            if ($list) {
                foreach ($list as $val) {
                    $log = $val['log'];
                    $listLines = explode("\n", $log);
                    foreach ($listLines as $v) {
                        $event = self::formatOne($v);
                        if ($event) {
                            $eventList[] = $event;
                            $eventListTimeKey[$event['log_time']][] = $event;
                        }
                    }
                }
            }
        }
        // 主次事件合并
        $timeMergeEventList = self::mergeEvent($eventListTimeKey);
        $mergeEventList=[];
        foreach($timeMergeEventList as $vEventList){
            foreach($vEventList as $event){
                $mergeEventList[]=$event;
            }
        }
        // todo 回合结束时，继续杀人的问题，回合结束后移，循环找到回合结束后，判断其后的100条数据，如果有aliveplayer事件，则推后round
        return $mergeEventList;
    }

    /**
     * 查询转化后的事件
     * $logBeginTime        开始时间
     * $logEndTime          结束时间
     * $ipAddress           IP地址
     * $logType             日志类型
     */
    public static function getHandleEvent($logBeginTime, $logEndTime, $ipAddress,$logType)
    {
        // $logBeginTime = '2020-07-23 16:07:35';
        // $logEndTime = '2020-07-23 17:20:00';
        // $ipAddress = '47.244.77.240';
        $find = 'info';
        // 约束条件
        $ipAddressArray=explode(";",$ipAddress);
        $map = ['>=', 'created_at', $logBeginTime];
        $where = ['<=', 'created_at', $logEndTime];
        $ip = ['in', 'ip_address', $ipAddressArray];
        $logType = ['=','log_type',$logType];
        $q = ReceiveData5eplayFormatEvents::find()
            ->select($find)
            ->orderBy('parent_id asc,id asc')
            ->where($map)
            ->andWhere($ip)
            ->andWhere($where)
            ->andWhere($logType)
            ->asArray();
        $sql=$q->createCommand()->getRawSql();
        $data=$q->all();
        $info = [];
        foreach($data as $key => &$value){
            $value = json_decode($value['info'],true);
            $info[] = $value;
        }
        return $info;
    }

    public static function getListAndDo()
    {
        // 这里循环整个表 按照顺序取出待处理的数据
        $where=[
            'and',
            ['=','is_handle',2],
            ['>=','created_at','2020-07-23 00:00:00'],
            ['<=','created_at','2020-07-23 23:59:59'],
        ];
        $q = ReceiveData5eplay::find()
            ->orderBy('id')
            ->where($where);
        $perPage = 1000;
        $count = $q->count();
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset(0)
                ->asArray()->all();
            $sql=$q->createCommand()->getRawSql();
            if ($list) {
                foreach($list as $line){
                    self::dealWithLine($line);
                }
            }else{
                break;
            }
        }
    }

    public static function dealWithLine($lineInfo)
    {
        $log = $lineInfo['log'];
        $listLines = explode("\n", $log);
        $eventListTimeKey=[];
        foreach ($listLines as $v) {
            $event = self::formatOne($v);
            if ($event) {
                $eventList[] = $event;
                $eventListTimeKey[$event['log_time']][] = $event;
            }
        }
        $timeMergeEventList = self::mergeEvent($eventListTimeKey);
        $mergeEventList=[];
        foreach($timeMergeEventList as $vEventList){
            foreach($vEventList as $event){
                $mergeEventList[]=$event;
            }
        }
        // 把合并之后的事件推入到新表中，并且标志本行处理状态
        foreach($mergeEventList as $key=>$val){
            $formatEvent=new ReceiveData5eplayFormatEvents();
            $lineInfo['created_at'] = isset($lineInfo['created_at']) ? $lineInfo['created_at'] : "";
            $formatEvent->setAttributes(
                [
                    'event_type' => $val['log_event_type'],
                    'log_type' => 1,
                    'parent_id' => $lineInfo['id'],
                    'info'=> json_encode($val),
                    'ip_address'=>$lineInfo['ip_address'],
                    'created_at'=>$lineInfo['created_at'],
                ]
            );
            if(!$formatEvent->save()){
                throw new BusinessException([], '没有这条记录');
            }
        }
        // 修改状态
        \app\modules\receive\models\ReceiveData5eplay::updateAll(['is_handle'=>1],['id'=>$lineInfo['id']]);
    }

    public static function mergeEvent($eventListTimeKey)
    {
        $eventAddingMapping = [
            // 闪光弹助攻
            self::EVENT_TYPE_FLASH_ASSISTED_KILLING => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ],
            // 助攻
            self::EVENT_TYPE_ASSISTED_KILLING => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ],
            // 获胜方式,这里有比分
            self::EVENT_TYPE_DETAIL_WIN_TYPE => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            //todo 活了多少人
            self::EVENT_TYPE_ALIVE_PLAYERS => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            // 重新加载
            self::EVENT_TYPE_MATCH_RELOADED => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            // 平局
            self::EVENT_TYPE_ROUND_DRAW => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            //选战队
            self::EVENT_TYPE_TEAM_PLAYING_CT => [
                'main_event' => self::EVENT_TYPE_MATCH_STARTED,
            ],
            //选战队
            self::EVENT_TYPE_TEAM_PLAYING_T => [
                'main_event' => self::EVENT_TYPE_MATCH_STARTED,
            ],
            //攻击
            self::EVENT_TYPE_ATTACKED => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ]
        ];
        foreach($eventListTimeKey as $key=>$timeDetail){
            // 单位事件内的所有事件
            if(count($timeDetail)<=1){
                continue ;
            }
            $tmpTimeDetail= $timeDetail;
            foreach($timeDetail as $event){
                // 如果是子事件
                if(isset($eventAddingMapping[$event['log_event_type']])){
                    // 遍历这个单位时间的事件列表，找到主事件，添加到['ext_log']
                    foreach($tmpTimeDetail as &$tmpDetail){
                        if($tmpDetail['log_event_type']==$eventAddingMapping[$event['log_event_type']]['main_event']){
                            $tmpDetail['ext_log'][$event['log_event_type']][]=$event;
                        }
                    }
                }
            }
            $eventListTimeKey[$key]=$tmpTimeDetail;
        }
        return $eventListTimeKey;
    }

    public static function formatOne($log)
    {
        $event = [
            'log_time' => substr($log, 0, 25),
            'log_map_name' => "",
            'log_event_type' => "",
            'log_team_name' => '',
            'log_team_camp' => "",
            'log_duration' => '',
            'log_flag_event_type' => '',
        ];

        $matchMap = [
            // match开始
            [
                'reg' => 'World triggered "Pug_Start"',
                'event_type' => self::EVENT_TYPE_MATCH_UP_COMING,
            ],
            // 存活事件
            [
                'reg' => 'Alive players: ',
                'event_type' => self::EVENT_TYPE_ALIVE_PLAYERS,
                'deal_func' => function ($logLine) {
                    return self::alivePlayerDetail($logLine);
                },
            ],
            // 比赛开始
            [
                'reg' => 'World triggered "Match_Start"',
                'event_type' => self::EVENT_TYPE_MATCH_STARTED,
                'deal_func' => function ($logLine) {
                    return self::getMatchStartDetail($logLine);
                },
            ],
            // 选择阵营为反恐精英
            [
                'reg' => 'Team playing "CT"',
                'event_type' => self::EVENT_TYPE_TEAM_PLAYING_CT,
                'deal_func' => function ($logLine) {
                    return self::getTeamPlayingDetail($logLine);
                },
            ],
            // 选择阵营为恐怖分子
            [
                'reg' => 'Team playing "TERRORIST"',
                'event_type' => self::EVENT_TYPE_TEAM_PLAYING_T,
                'deal_func' => function ($logLine) {
                    return self::getTeamPlayingDetail($logLine);
                },
            ],
            // battle结束
            [
                'reg' => 'Game Over:',
                'event_type' => self::EVENT_TYPE_GAME_OVER,
                'deal_func' => function ($logLine) {
                    return self::getGameOverDetail($logLine);
                }
            ],
            // 刀局开始
            [
                'reg' => 'World triggered "Knife_Round_Start"',
                'event_type' => self::EVENT_TYPE_KNIFE_ROUND_START,
            ],
            // round开始(一场round开始的基点)
            [
                'reg' => 'World triggered "Round_Start"',
                'event_type' => self::EVENT_TYPE_ROUND_START,
                'log_flag_event_type' => self::EVENT_TYPE_ROUND_START,
            ],
            // round结束(一场round结束的基点)
            [
                'reg' => 'World triggered "Round_End"',
                'event_type' => self::EVENT_TYPE_ROUND_END,
                'log_flag_event_type' => self::EVENT_TYPE_ROUND_END,
            ],
            // 炸弹拆除
            [
                'reg' => 'SFUI_Notice_Bomb_Defused',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_BOMB_DEFUSED,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getBombDefusedInfo($line);
                }
            ],
            // 炸弹爆炸
            [
                'reg' => 'Team "TERRORIST" triggered "SFUI_Notice_Target_Bombed"',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_TARGET_BOMBED,
                'log_win_camp' => 'terrorist',
                'deal_func' => function ($line) {
                    return self::getTargetBombed($line);
                }
            ],
            // 反恐精英获胜
            [
                'reg' => 'SFUI_Notice_CTs_Win',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_CT_WIN,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getCtsWinDetail($line);
                }
            ],
            // 目标保存完好(反恐精英获胜)
            [
                'reg' => 'SFUI_Notice_Target_Saved',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_TARGET_SAVED,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getTargetSavedDetail($line);
                }
            ],
            // 通知恐怖分子获胜(恐怖分子获胜)
            [
                'reg' => 'SFUI_Notice_Terrorists_Win',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_T_WIN,
                'log_win_camp' => 'terrorist',
                'deal_func' => function ($line) {
                    return self::getTWinDetail($line);
                }
            ],
            // 击杀
            [
                'reg' => 'killed "',
                'event_type' => self::EVENT_TYPE_KILLED,
                'log_flag_event_type' => self::EVENT_TYPE_KILLED,
                'deal_func' => function ($line) {
                    return self::getKillDetailInfoByLogLine($line);
                },
            ],
            // 比赛换边
            [
                'reg' => 'World triggered "Match_SwitchTeam"',
                'event_type' => self::EVENT_TYPE_MATCH_SWITCH_TEAM,
            ],
            // 加时赛换边
            [
                'reg' => 'World triggered "Match_SwitchTeamOvertime"',
                'event_type' => self::EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH,
            ],
            // 助攻
            [
                'reg' => ' assisted killing ',
                'event_type' => self::EVENT_TYPE_ASSISTED_KILLING,
                'deal_func' => function ($line) {
                    return self::assistedKillingDetail($line);
                }
            ],
            // 安装炸弹
            [
                'reg' => 'triggered "Planted_The_Bomb"',
                'event_type' => self::EVENT_TYPE_PLANTED_THE_BOMB,
                'deal_func' => function ($log) {
                    return self::plantedTheBomb($log);
                },
            ],
            // 攻击
            [
                'reg' => ' attacked ',
                'event_type' => self::EVENT_TYPE_ATTACKED,
                'deal_func' => function ($log) {
                    return self::attackedDetail($log);
                },
            ],
            // 闪光弹助攻
            [
                'reg' => ' flash-assisted killing ',
                'event_type' => self::EVENT_TYPE_FLASH_ASSISTED_KILLING,
                'deal_func' => function ($log) {
                    return self::flashAssistedKillingDetail($log);
                },
            ],
            // 拆除炸弹
            [
                'reg' => "Defused_The_Bomb",
                'event_type' => self::EVENT_TYPE_DEFUSED_THE_BOMB,
                'deal_func' => function ($log) {
                    return self::defusedBombInfo($log);
                },
            ],
            // 离开购买区
            [
                'reg' => ' left buyzone with [ ',
                'event_type' => self::TYPE_LEFT_BUY_ZONE_WITH,
                'deal_func' => function ($log) {
                    return self::leftBuyZoneWith($log);
                },
            ],
            // 指令暂停
            [
                'reg' => ' Pause start ',
                'event_type' => self::TYPE_EVENT_PAUSE_START,
            ],
            // 暂停结束
            [
                'reg' => 'Pause end',
                'event_type' => self::TYPE_EVENT_PAUSE_END,
            ],
            // 地图指令,比赛暂停
            [
                'reg' => 'Match pause is enabled - mp_pause_match',
                'event_type' => self::EVENT_TYPE_MP_PAUSE_MATCH,
            ],
            // 地图指令,比赛暂停结束
            [
                'reg' => 'Match pause is disabled - mp_unpause_match',
                'event_type' => self::EVENT_TYPE_MP_UNPAUSE_MATCH,
            ],
            // 平局
            [
                'reg' => 'World triggered "SFUI_Notice_Round_Draw"',
                'event_type' => self::EVENT_TYPE_ROUND_DRAW,
                'deal_func' => function ($log){
                    return self::roundDraw($log);
                }
            ],
            // 重新加载
            [
                'reg' => 'World triggered "Match_Reloaded"',
                'event_type' => self::EVENT_TYPE_MATCH_RELOADED,
                'deal_func' => function($log){
                    return self::matchReloaded($log);
                }
            ],

        ];
        foreach ($matchMap as $key => $val) {
            if (strstr($log, $val['reg']) !== false) {
                $event['log_event_type'] = $val['event_type'];
                if (isset($val['deal_func'])&&$val['deal_func']) {
                    $dealInfo = $val['deal_func']($log);
                    $event = array_merge($event, $dealInfo);
                }
                if(isset($val['log_win_type'])){
                    $event['log_win_type']=$val['log_win_type'];
                    $event['log_win_camp'] = $val['log_win_camp'];
                }
                return $event;
            }
        }
        return null;
    }

    /**
     * 重新加载
     * $logLine        单条日志
     */
    public static function matchReloaded($logLine)
    {
        // $logLine = '07/05/2020 - 13:24:05.891 - World triggered "Match_Reloaded" on "de_dust2"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $mapTemp = explode('on "',$logLine)[1];
        $map = explode('"',$mapTemp)[0];//重新加载的地图
        return $info = [
            'log_standard_time' => $logTime,
            'log_map_name' => $map,
        ];
    }

    /**
     * 比赛回档
     * $logLine        单条日志
     */
    public static function roundDraw($logLine)
    {
        // $logLine = '07/05/2020 - 13:24:05.891 - World triggered "SFUI_Notice_Round_Draw" (CT "2") (T "3")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $ctScoreTemp = explode('(CT "',$logLine)[1];
        $ctScore = explode('")',$ctScoreTemp)[0];//反恐精英分数
        $tScoreTemp = explode('(T "',$logLine)[1];
        $tScore = explode('")',$tScoreTemp)[0];//恐怖分子分数
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 炸弹被拆除了详情    By    王傲渊
     * $logLine                 单条日志
     */
    public static function defusedBombInfo($logLine)
    {
        // $logLine = '07/05/2020 - 13:32:14.039 - "polite^<13><STEAM_1:1:28960488><CT>" triggered "Defused_The_Bomb"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode('"', $logLine);
        $data = $log[1];
        $data = explode("<", $data);
        $playerName = $data[0];//选手昵称
        $playerID = explode(">", $data[2]);
        $playerID = $playerID[0];//选手ID
        $camp = explode(">", $data[3]);
        $camp = $camp[0];//选手阵营
        $camp = $camp == "CT" ? "ct" : "terrorist";
        return $info = [
            'log_standard_time' => $logTime,
            'playID' => $playerID,
            'playerName' => $playerName,
            'camp' => $camp,
            'roundEventType' => self::EVENT_TYPE_DEFUSED_THE_BOMB,
            'round_side' => "ct",
        ];
    }

    /**
     * 选手离开购买区   By   王傲渊
     * $logLine             单条日志
     */
    public static function leftBuyZoneWith($logLine)
    {
        // $logLine = '07/23/2020 - 16:08:24.034 - "XinKoiNg<75><STEAM_1:0:77733600><TERRORIST>" left buyzone with [ weapon_knife_karambit weapon_glock weapon_smokegrenade weapon_flashbang weapon_c4 C4 ]';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode('- ', $logLine);
        $tmp = explode('<', $log[2]);
        $playerName = explode('"',$tmp[0])[1];//选手昵称
        $playerID = explode('>', $tmp[2]);
        $playerID = $playerID[0];//选手ID
        $playCamp = explode(">",$tmp[3]);
        $playCamp = $playCamp[0];
        $playCamp = $playCamp == "CT" ? "ct" : "terrorist";//选手阵营
        $arms = explode("[ ",$tmp[3]);
        $arms = explode(" ]",$arms[1]);
        $arms = explode(" ",$arms[0]);//武器数组
        // 详情
        $detail = [
            'player_camp' => $playCamp,
            'arms' => $arms,
        ];
        return $info = [
            'log_standard_time' => $logTime,
            'playerName' => $playerName,
            'playID' => $playerID,
            'detail' => $detail,
        ];
    }

    /**
     * 安装炸弹详情  By  王傲渊
     * $logLine         单条日志
     */
    public static function plantedTheBomb($logLine)
    {
        // $logLine = '07/05/2020 - 13:19:08.687 - "Marek77.SINA<10><STEAM_1:0:42735273><TERRORIST>" triggered "Planted_The_Bomb"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $log = explode('"', $logLine);
        $data = $log[1];
        $data = explode("<", $data);
        $playerName = $data[0];//选手昵称
        $playerID = explode(">", $data[2]);
        $playerID = $playerID[0];//选手ID
        $camp = explode(">", $data[3]);
        $camp = $camp[0];//选手阵营
        $camp = $camp == "CT" ? "ct" : "terrorist";
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_PLANTED_THE_BOMB,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'playID' => $playerID,
            'playerName' => $playerName,
            'camp' => $camp,
            'round_side' => "terrorist",
        ];
    }

    /**
     * 闪光弹助攻详情   By  王傲渊
     * $logLine            单条日志
     */
    public static function flashAssistedKillingDetail($logLine)
    {
        //    $logLine = '07/23/2020 - 16:16:38.440 - "Ayaya.SINA<74><STEAM_1:0:55551924><CT>" flash-assisted killing "Marek77.SINA<76><STEAM_1:0:42735273><CT>"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $log = explode(' ', $logLine);
        $assistsSteamId = explode('<',$log[4]);
        $assistsSteamId = explode('>',$assistsSteamId[2])[0];//闪光弹助攻选手steam_id
        $assistsPlayerNickName = explode('<', $log[4])[0];//助攻者昵称
        $assistsPlayerCamp = explode('<', $log[4]);
        $assistsPlayerCamp = explode('>', $assistsPlayerCamp[3])[0];//助攻者阵营
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_FLASH_ASSISTED_KILLING,
            'roundTime' => $roundTime,
            'assistsSteamId' => $assistsSteamId,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'assistsPlayerNickName' => $assistsPlayerNickName,
            'assistsPlayerCamp' => $assistsPlayerCamp,
        ];
    }

    /**
     * 攻击详情  By  王傲渊
     * $logLine     单条日志
     */
    public static function attackedDetail($logLine)
    {
        //$logLine = '07/23/2020 - 17:29:48.186 - "WestmeloN<87><STEAM_1:0:73487156><TERRORIST>" [-1127 -740 11776] attacked "Ayaya.SINA<86><STEAM_1:0:55551924><CT>" [-154 -397 11882] with "ak47" (damage "33") (damage_armor "4") (health "30") (armor "89") (hitgroup "stomach")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $log = explode(' ', $logLine);
        $victimSteamId = explode('<',$log[4])[2];
        $victimSteamId = explode('>',$victimSteamId)[0];//受害人steam_id
        $victimNickName = explode('<', $log[9])[0];
        $victimNickName = explode('"', $victimNickName)[1];//受害人昵称
        $victimCamp = explode('<', $log[9]);
        $victimCamp = explode('>', $victimCamp[3])[0];
        $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";//受害者阵营
        $victimLocation = $log[10] . " " . $log[11] . " " . $log[12];//受害者位置
        $tmp = $log[4];
        $attackSteamId = explode('<',$tmp)[2];
        $attackSteamId = explode('>',$attackSteamId)[0];//攻击选手steam_id
        $attackNickname = explode('<', $tmp)[0];
        $attackNickname = explode('"', $attackNickname)[1];//攻击选手昵称
        $attackLocation = $log[5] . " " . $log[6] . " " . $log[7];//攻击者选手位置
        $attackCamp = $log[4];
        $attackCamp = explode('<', $attackCamp);
        $attackCamp = explode('>', $attackCamp[3])[0];
        $attackCamp = $attackCamp == "CT" ? "ct" : "terrorist";//攻击选手阵营
        $damage = explode(')', $log[16])[0];
        $damage = explode('"', $damage)[1];//伤害
        $logTmp = explode('(hitgroup "',$logLine);
        $body = explode('")',$logTmp[1])[0];//打击部位
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_ATTACKED,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'victimSteamId' => $victimSteamId,
            'victimNickName' => $victimNickName,
            'victimCamp' => $victimCamp,
            'victimLocation' => $victimLocation,
            'attackSteamId' => $attackSteamId,
            'attackNickname' => $attackNickname,
            'attackCamp' => $attackCamp,
            'attackLocation' => $attackLocation,
            'damage' => $damage,
            'body' => $body,
        ];
    }

    /**
     * 助攻事件详情  By  王傲渊
     * $logLine         单条事件日志
     */
    public static function assistedKillingDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:09:23.378 - "我热爱的-。-<71><STEAM_1:0:82392660><TERRORIST>" assisted killing "Ayaya.SINA<74><STEAM_1:0:55551924><CT>"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $log = explode(' ', $logLine);
        $tmp = $log[7];
        $victimNickName = explode('<', $tmp)[0];//受害人昵称
        $victimCamp = explode('>', $tmp);
        $victimCamp = explode('<', $victimCamp[2])[1];
        $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";//受害者阵营
        $victimSteamId = explode('<',$tmp)[2];
        $victimSteamId = explode('>',$victimSteamId)[0];//受害者steam_id
        $assistsPlayerNickName = $log[4];
        $assistsPlayerNickName = explode('<', $assistsPlayerNickName)[0];
        $assistsPlayerNickName = explode('"',$assistsPlayerNickName)[1];//助攻选手昵称
        $tmp = $log[4];
        $assistsPlayerSteamId = explode('<',$tmp)[2];
        $assistsPlayerSteamId = explode('>',$assistsPlayerSteamId)[0];//助攻选手steam_id
        $assistsPlayerCamp = explode('<', $tmp);
        $assistsPlayerCamp = explode('>', $assistsPlayerCamp[3])[0];
        $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";//助攻选手阵营
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_ASSISTED_KILLING,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'victimNickName' => $victimNickName,
            'victimCamp' => $victimCamp,
            'victimSteamId' => $victimSteamId,
            'assistsPlayerNickName' => $assistsPlayerNickName,
            'assistsPlayerCamp' => $assistsPlayerCamp,
            'assistsPlayerSteamId' => $assistsPlayerSteamId,
        ];
    }

    /**
     * 回合内时间戳   By   王傲渊
     * $roundTime         回合内时间
     */
    public static function roundTimeStamp($roundTime)
    {
        $timeStamp = substr($roundTime, 0, 21);
        $timeStamp = str_replace(' - ', ' ', $timeStamp);
        $timeStamp = strtotime($timeStamp);
        return $timeStamp;
    }

    /**
     * 时间转换csgo专用  By  王傲渊
     * $time    要转换的时间(字符串)
     */
    public static function changeTime($time)
    {
        $time = substr($time, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }

    /**
     * 获取单条记录的击杀详情
     * $logLine             单条日志
     */
    public static function getKillDetailInfoByLogLine($logLine)
    {
        // $logLine='07/05/2020 - 13:28:13.235 - "f0rsakeN.SINA<11><STEAM_1:1:90560955><TERRORIST>" [-275 1389 -28] killed "bottleljr<8><STEAM_1:0:43495634><CT>" [-163 1263 64] with "ak47" (headshot)';
        // 格式化内容
        $log = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$log[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$log[1];//日志时间
        $info['log_standard_time'] = $logTime;
        $time = substr($logLine, 0, 25);
        $logLine = substr($logLine, 25);
        $withInfo = explode(" with ", $logLine);
        $withBodyInfo = explode(" ", isset($withInfo[1]) ? $withInfo[1] : '');
        $killArray = explode(" killed ", $withInfo[0]);
        $reg = "/\"(.*)<.*><(STEAM_\d:\d:\d+)><(TERRORIST|CT)>\" (\[[- \d]+\])/i";
        preg_match_all($reg, $killArray[0], $pregInfo1);
        $info['roundEventType'] = self::EVENT_TYPE_KILLED;//对局事件类型
        $info['killer'] = [
            'player_id' => @$pregInfo1[2][0],//击杀者选手ID
            'name' => @$pregInfo1[1][0],//选手昵称
            'camp' => @$pregInfo1[3][0],//阵营
            'coordinate' => @$pregInfo1[4][0],//位置坐标
        ];
        if ($info['killer']['camp'] == "CT") {
            $info['killer']['camp'] = "ct";
        } else {
            $info['killer']['camp'] = "terrorist";
        }
        preg_match_all($reg, isset($killArray[1]) ? $killArray[1] : '', $pregInfo2);
        $info['deader'] = [
            'player_id' => @$pregInfo2[2][0],//受害者选手ID
            'name' => @$pregInfo2[1][0],//选手昵称
            'camp' => @$pregInfo2[3][0],//阵营
            'coordinate' => @$pregInfo2[4][0],//位置坐标
        ];
        $info['deader']['camp'] = $info['deader']['camp'] == "CT" ? "ct" : "terrorist";//受害者阵营
        $info['time'] = $time;//日志内时间
        $info['with'] = trim($withBodyInfo[0], '"');//武器名称
        $info['body'] = trim(isset($withBodyInfo[1]) ? $withBodyInfo[1] : '', "()");//打击部位
        // 是否是爆头
        if ($info['body'] == "headshot") {
            $isHeadshot = 1;
        } else {
            $isHeadshot = 2;
        }
        $info['is_headshot'] = $isHeadshot;//是否是爆头击杀
        $info['roundTimeStamp'] = (string)self::roundTimeStamp($time);//对局内时间戳
        $info['roundTime'] = self::changeTime($time);//对局内时间
        $info['round_side'] = $info['killer']['camp'];
        return $info;
    }

    /**
     * 获取武器ID和是否是刀杀类型  By  王傲渊
     * $armsName      武器名称
     */
    //public static function getArmsID($armsName)
    //{
    //    $data = new MetadataCsgoWeapon();
    //    $armsInfo = $data->find()->where(['name' => $armsName])->select('id,kind')->one();
    //    // 类型三是刀
    //    if (isset($armsInfo['kind']) && $armsInfo['kind'] == 3) {
    //        $killType = "knife_kill";//击杀类型为刀杀
    //    } else {
    //        $armsName = strstr($armsName, "knife");
    //        if ($armsName) {
    //            $killType = "knife_kill";
    //        } else {
    //            $killType = "";
    //        }
    //    }
    //    return $killInfo = [
    //        'armsId' => $armsInfo['id'],//武器ID
    //        'killType' => $killType,//击杀类型
    //    ];
    //}


    /**
     * 比赛开始详情
     * $logLine         单条日志
     */
    public static function getMatchStartDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:07:10.383 - World triggered "Match_Start" on "de_mirage"';
        $log = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$log[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$log[1];//日志时间
        $map = explode('"',$logLine)[3];//地图
        return $info = [
            'event_type' => self::EVENT_TYPE_MATCH_STARTED,//事件类型
            'log_standard_time' => $logTime,
            'log_map_name' => $map,
        ];
    }

    /**
     * 选择阵营详情
     * $logLine         单条日志
     */
    public static function getTeamPlayingDetail($logLine)
    {
        // $logLine = '06/25/2020 - 16:59:42.000 - Team playing "CT": Huat Zai';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode('"',$logLine);
        $camp = explode('"',$log[1])[0];
        $camp = $camp == "CT" ? "ct" : "terrorist";//阵营
        $teamName = explode(': ',$log[2])[1];//战队昵称
        return $info = [
            'log_standard_time' => $logTime,
            'log_team_name' => $teamName,
            'log_team_camp' => $camp,
        ];
    }

    /**
     * 炸弹拆除详情
     * $logLine             单条日志
     */
    public static function getBombDefusedInfo($logLine)
    {
        // $logLine = '07/23/2020 - 16:11:24.768 - Team "CT" triggered "SFUI_Notice_Bomb_Defused" (CT "1") (T "1")';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 炸弹爆炸详情
     * $logLine         单条日志
     */
    public static function getTargetBombed($logLine)
    {
        // $logLine = '07/23/2020 - 16:15:09.458 - Team "TERRORIST" triggered "SFUI_Notice_Target_Bombed" (CT "3") (T "2")';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 反恐精英获胜详情
     * $logLine         单条日志
     */
    public static function getCtsWinDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:12:36.143 - Team "CT" triggered "SFUI_Notice_CTs_Win" (CT "2") (T "1")';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 目标被保护完好详情
     * $logLine         单条日志
     */
    public static function getTargetSavedDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:17:24.458 - Team "CT" triggered "SFUI_Notice_Target_Saved" (CT "4") (T "2")';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 恐怖分子获胜
     * $logLine         单条日志
     */
    public static function getTWinDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:07:14.853 - Team "TERRORIST" triggered "SFUI_Notice_Terrorists_Win" (CT "0") (T "1")';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * Game Over详情
     * $logLine         单条日志
     */
    public static function getGameOverDetail($logLine)
    {
        // $logLine = "07/23/2020 - 17:11:11.823 - Game Over: competitive mg_active de_mirage score 16:19 after 64 min";
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $overDetailInfo = explode(' ', $logLine);
        $duration = (string)($overDetailInfo[12] * 60);//battle时长
        $map = explode('mg_active ',$logLine);
        $map = explode(' ',$map[1])[0];//battle地图
        return [
            'log_standard_time' => $logTime,
            'log_duration' => $duration,
            'battle_map' => $map,
        ];
    }

    /**
     * 选手存活事件详情
     * $logLine         单条日志
     */
    public static function alivePlayerDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:10:53.496 - Alive players: "4" in "CT" <STEAM_1:0:55551924><STEAM_1:0:42735273><STEAM_1:1:90560955><STEAM_1:0:47773249> vs "4" in "TERRORIST" <STEAM_1:1:88758867><STEAM_1:0:82392660><STEAM_1:0:73487156><STEAM_1:0:77733600>';
        $logTmp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTmp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTmp[1];//日志时间
        $log = explode('Alive players:',$logLine);
        $log = explode(' vs ',$log[1]);
        $ctAliveNumber = explode('"',$log[0])[1];//反恐精英存活人数
        $ctAlivePlayers = explode('in "CT" ',$log[0])[1];
        $ctAlivePlayers = str_replace('><',' ',$ctAlivePlayers);
        $ctAlivePlayers = str_replace('<','',$ctAlivePlayers);
        $ctAlivePlayers = str_replace('>','',$ctAlivePlayers);
        $ctAlivePlayers = explode(' ',$ctAlivePlayers);//反恐精英存活选手
        $tAliveNumber = explode('"',$log[1])[1];//恐怖分子存活人数
        $tAlivePlayers = explode('in "TERRORIST" ',$log[1])[1];
        $tAlivePlayers = str_replace('><',' ',$tAlivePlayers);
        $tAlivePlayers = str_replace('<','',$tAlivePlayers);
        $tAlivePlayers = str_replace('>','',$tAlivePlayers);
        $tAlivePlayers = explode(' ',$tAlivePlayers);//恐怖分子存活选手
        return $info = [
            'log_standard_time' => $logTime,
            't_alive_number' => $tAliveNumber,
            'ct_alive_number' => $ctAliveNumber,
            't_alive_players' => $tAlivePlayers,
            'ct_alive_players' => $ctAlivePlayers,
        ];
    }
}