<?php

namespace app\modules\task\services\logformat;

use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\data\models\ReceiveDataFunspark;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\rest\exceptions\BusinessException;

// Funspark数据源
class CsgoFunspark
{
    // match
    const EVENT_TYPE_MATCH_UP_COMING = "Match_Upcoming";//funspark数据源比赛开始
//battle

    const EVENT_TYPE_MATCH_STARTED = "Match_Start";//对局开始（关键信息：时间√、对局开始√、地图√、起始阵营（子事件×）√）
    const EVENT_TYPE_TEAM_PLAYING_CT = "Team playing CT";//起始为反恐精英的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_TEAM_PLAYING_T = "Team playing TERRORIST";//起始为恐怖分子的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_GAME_OVER = "Game Over";//对局结束（应该用World triggered \"Match_End\"更为准确）（关键信息：时间√、对局结束√、地图（子事件×）×、结束时比分（子事件×）×、比赛时长（子事件×）×）
    const EVENT_TYPE_MP_PAUSE_MATCH = "Match pause is enabled - mp_pause_match";//地图指令比赛暂停（关键信息：暂停√）
    const EVENT_TYPE_MP_UNPAUSE_MATCH = "Match pause is disabled - mp_unpause_match";//地图指令比赛暂停取消（关键信息：取消暂停√）
    const EVENT_TYPE_MATCH_SWITCH_TEAM = "Match_SwitchTeam";//常规换边（关键信息：战队当前阵营更换√）
    //Match_SwitchTeamOvertime//加时换边（关键信息：战队当前阵营更换×）

//round

    const EVENT_TYPE_ROUND_START = "round_start";//回合开始（关键信息：时间√、回合开始√）
    const EVENT_TYPE_ROUND_END = "round_end";//回合结束（关键信息：时间√、回合结束√、获胜方式（子事件√）√、回合结束后的当前比分（子事件√）×、存活人员（子事件√）×、存活人数（子事件√）×）
    const EVENT_TYPE_DETAIL_WIN_TYPE = 'win_type';//获胜类型
    const TYPE_WIN_TYPE_T_WIN = 'T_win';//反恐精英全部阵亡（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_CT_WIN = 'CT_win';//恐怖分子被全歼（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_TARGET_BOMBED = 'Bomb exploded';//炸弹爆炸（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_BOMB_DEFUSED = 'Bomb defused';//拆除炸弹（关键信息：获胜方式、回合结束后当前比分）
    const TYPE_WIN_TYPE_TARGET_SAVED = 'Target saved';//时间耗尽（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_ALIVE_PLAYERS = 'alive_players';//存活人员（关键信息：存活人员、存活人数）
    const TYPE_LEFT_BUY_ZONE_WITH = "left buyzone with";//离开购买区（5E：应该用Starting Freeze period初始化更准确）（关键信息：时间√、回合统计数据初始化√）
    const EVENT_TYPE_KILLED = "player_kill";//击杀（关键信息：时间√、杀手信息√、受害者信息√、武器√、是否爆头√、致死伤害（子事件√）×、命中部位（子事件√）×、助攻者信息（子事件√）×、闪光弹助攻者信息（子事件√）×、）
    const EVENT_TYPE_ATTACKED = "attacked";//攻击（关键信息：致死伤害、命中部位）
    const EVENT_TYPE_ASSISTED_KILLING = "assist";//助攻（关键信息：助攻者信息）
    const EVENT_TYPE_FLASH_ASSISTED_KILLING = "flashassist";//闪光弹助攻（关键信息：闪光弹助攻者信息）
    const EVENT_TYPE_PLANTED_THE_BOMB = "bomb_planted";//安放炸弹（关键信息：安放炸弹、安放炸弹的选手）
    const EVENT_TYPE_DEFUSED_THE_BOMB = "bomb_defused";//拆除炸弹（关键信息：拆除炸弹、拆除炸弹的选手）
    // 特殊事件
    const EVENT_TYPE_ROUND_DRAW = "round_draw";//比赛平局
    const EVENT_TYPE_MATCH_RELOADED = "match_reloaded";//重新加载
    // funspark数据源用事件
    const EVENT_TYPE_FUNSPARK_SWITCH_TEAM = "funspark_switch_team";//funspark数据源换边

//没用到的？
    const TYPE_EVENT_PAUSE_START = "Pause start";//暂停指令
    const TYPE_EVENT_PAUSE_END = "Pause end";//暂停结束
    const EVENT_TYPE_LOADING_MAP = "Loading map";//加载地图
    const EVENT_TYPE_MATCH_HAS_END = "match_has_end";//比赛结束
    const EVENT_TYPE_MAP_START = "map_start";//地图开启
    // const EVENT_TYPE_ROUND_DRAW = "SFUI_Notice_Round_Draw";//比赛平局(重复抛弃)
    const EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH = "detail_choose_team_switch"; //加时换边，找到之'Match_SwitchTeamOvertime'
    const EVENT_TYPE_KNIFE_ROUND_START = "knife_round_start";//刀局开始
    const EVENT_TYPE_ROUND_SCORES = 'round_score';
    const EVENT_TYPE_BATTLE_SCORES = 'battle_scores';

    public static function run($tagInfo, $taskInfo)
    {
        $params=json_decode($taskInfo['params'],true);
        // 这里面是待执行的消息
        self::dealWithLine($params);
        return [];
    }

    /**
     * @param $logBeginTime
     * @param $logEndTime
     * @param $ipAddress
     * @return array
     * 获取日志列表并格式化
     */
    public static function formatList($logBeginTime, $logEndTime, $ipAddress)
    {
        $find = 'log';
        // 约束条件
        $ipAddressArray=explode(";",$ipAddress);
        $map = ['>=', 'created_at', $logBeginTime];
        $where = ['<=', 'created_at', $logEndTime];
        $ip = ['in', 'ip_address', $ipAddressArray];
        $q = ReceiveDataFunspark::find()
            ->select($find)
            ->orderBy('id')
            ->where($map)
            ->andWhere($ip)
            ->andWhere($where);
        $perPage = 1000;
        $count = $q->count();
        $eventList = [];
        $eventListTimeKey = [];
        $eventSwitchListTimeKey = [];
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset($perPage * ($page - 1))
                ->asArray()->all();
            if ($list) {
                foreach ($list as $val) {
                    $log = $val['log'];
                    $listLines = explode("\n", $log);
                    foreach ($listLines as $v) {
                        $event = self::formatOne($v);
                        if ($event) {
                            if($event['log_event_type'] == self::EVENT_TYPE_FUNSPARK_SWITCH_TEAM){
                                $eventSwitchListTimeKey[$event['log_time']][] = $event;
                            }else{
                                $eventList[] = $event;
                                $eventListTimeKey[$event['log_time']][] = $event;
                            }
                        }
                    }
                }
            }
        }
        foreach($eventSwitchListTimeKey as $sk=>$sEventList){
            if(count($sEventList)>8){
                // 记录一条换边的日志
                $tmpEvent=[
                    'log_event_type'=> self::EVENT_TYPE_MATCH_SWITCH_TEAM,
                    'log_time'=>$sEventList[1]['log_time']
                ];
                // todo 检查换边和时间
                $eventListTimeKey[$sk][]=$tmpEvent;
            }
        }
        // 主次事件合并
        $timeMergeEventList = self::mergeEvent($eventListTimeKey);
        $mergeEventList=[];
        foreach($timeMergeEventList as $vEventList){
            foreach($vEventList as $event){
                $mergeEventList[]=$event;
            }
        }
        // todo 回合结束时，继续杀人的问题，回合结束后移，循环找到回合结束后，判断其后的100条数据，如果有aliveplayer事件，则推后round
        return $mergeEventList;
    }

    /**
     * 查询转化后的事件
     * $logBeginTime        开始时间
     * $logEndTime          结束时间
     * $ipAddress           IP地址
     * $logType             日志类型
     */
    public static function getHandleEvent($logBeginTime, $logEndTime, $ipAddress,$logType)
    {
        // $logBeginTime = '2020-07-23 16:07:35';
        // $logEndTime = '2020-07-23 17:20:00';
        // $ipAddress = '47.244.77.240';
        $find = 'info';
        // 约束条件
        $ipAddressArray=explode(";",$ipAddress);
        $map = ['>=', 'created_at', $logBeginTime];
        $where = ['<=', 'created_at', $logEndTime];
        $ip = ['in', 'ip_address', $ipAddressArray];
        $logType = ['=','log_type',$logType];
        $q = ReceiveData5eplayFormatEvents::find()
            ->select($find)
            ->orderBy('parent_id asc,id asc')
            ->where($map)
            ->andWhere($ip)
            ->andWhere($where)
            ->andWhere($logType)
            ->asArray();
        $sql=$q->createCommand()->getRawSql();
        $data=$q->all();
        $info = [];
        foreach($data as $key => &$value){
            $value = json_decode($value['info'],true);
            $info[] = $value;
        }
        return $info;
    }

    public static function getListAndDo()
    {
        // 这里循环整个表 按照顺序取出待处理的数据
        $where=[
            'and',
            ['=','is_handle',2],
            ['>=','created_at','2020-08-08 00:00:00'],
            ['<=','created_at','2020-08-15 07:00:00'],
        ];
        $q = ReceiveDataFunspark::find()
            ->orderBy('id')
            ->where($where);
        $perPage = 1000;
        $count = $q->count();
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset(0)
                ->asArray()->all();
            $sql=$q->createCommand()->getRawSql();
            if ($list) {
                foreach($list as $line){
                    self::dealWithLine($line);
                }
            }else{
                break;
            }
        }
    }

    public static function getReceiveDataFunsparkListAndDoByTime($s_time,$e_time,$is_handle=2,$ipAddress)
    {
        // 这里循环整个表 按照顺序取出待处理的数据
        $where=[
            'and',
            ['=','is_handle',$is_handle],
            ['>=','created_at',$s_time],
            ['<=','created_at',$e_time],
            ['=', 'ip_address', $ipAddress]
        ];
        $q = ReceiveDataFunspark::find()
            ->orderBy('id')
            ->where($where);
        $perPage = 1000;
        $count = $q->count();
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset(0)
                ->asArray()->all();
            $sql=$q->createCommand()->getRawSql();
            if ($list) {
                foreach($list as $line){
                    self::dealWithLine($line);
                }
            }else{
                break;
            }
        }
    }

    public static function dealWithLine($lineInfo)
    {
        $log = $lineInfo['log'];
        $listLines = explode("\n", $log);
        $eventListTimeKey=[];
        $eventSwitchListTimeKey = [];
        foreach ($listLines as $v) {
            $event = self::formatOne($v);
            if ($event) {
                if($event['log_event_type'] == self::EVENT_TYPE_FUNSPARK_SWITCH_TEAM){
                    $eventSwitchListTimeKey[$event['log_time']][] = $event;
                }else{
                    $eventList[] = $event;
                    $eventListTimeKey[$event['log_time']][] = $event;
                }
            }
        }
        foreach($eventSwitchListTimeKey as $sk=>$sEventList){
            if(count($sEventList)>8){
                // 记录一条换边的日志
                $tmpEvent=[
                    'log_event_type'=> self::EVENT_TYPE_MATCH_SWITCH_TEAM,
                    'log_time'=>$sEventList[1]['log_time']
                ];
                // todo 检查换边和时间
                $eventListTimeKey[$sk][]=$tmpEvent;
            }
        }
        $timeMergeEventList = self::mergeEvent($eventListTimeKey);
        $mergeEventList=[];
        foreach($timeMergeEventList as $vEventList){
            foreach($vEventList as $event){
                $mergeEventList[]=$event;
            }
        }
        // 把合并之后的事件推入到新表中，并且标志本行处理状态
        foreach($mergeEventList as $key=>$val){
            $formatEvent=new ReceiveData5eplayFormatEvents();
            $lineInfo['created_at'] = isset($lineInfo['created_at']) ? $lineInfo['created_at'] : "";
            $formatEvent->setAttributes(
                [
                    'event_type' => $val['log_event_type'],
                    'log_type' => 2,
                    'parent_id' => $lineInfo['id'],
                    'info'=> json_encode($val),
                    'ip_address'=>$lineInfo['ip_address'],
                    'created_at'=>$lineInfo['created_at'],
                ]
            );
            if(!$formatEvent->save()){
                throw new BusinessException([], '没有这条记录');
            }
        }
        // 修改状态
        \app\modules\receive\models\ReceiveDataFunspark::updateAll(['is_handle'=>1],['id'=>$lineInfo['id']]);
    }

    public static function mergeEvent($eventListTimeKey)
    {
        $eventAddingMapping = [
            // 闪光弹助攻
            self::EVENT_TYPE_FLASH_ASSISTED_KILLING => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ],
            // 助攻
            self::EVENT_TYPE_ASSISTED_KILLING => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ],
            // 获胜方式,这里有比分
            self::EVENT_TYPE_DETAIL_WIN_TYPE => [
                'main_event' => [
                    self::EVENT_TYPE_ROUND_END,
                    self::EVENT_TYPE_GAME_OVER,
                ]
            ],
            // 重新加载
            self::EVENT_TYPE_MATCH_RELOADED => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            // 平局
            self::EVENT_TYPE_ROUND_DRAW => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            // //todo 活了多少人
            // self::EVENT_TYPE_ALIVE_PLAYERS => [
            //     'main_event' => self::EVENT_TYPE_ROUND_END,
            // ],
            //选择阵营为反恐精英
            self::EVENT_TYPE_TEAM_PLAYING_CT => [
                'main_event' => self::EVENT_TYPE_MATCH_STARTED,
            ],
            //选择阵营为恐怖分子
            self::EVENT_TYPE_TEAM_PLAYING_T => [
                'main_event' => self::EVENT_TYPE_MATCH_STARTED,
            ],
            //攻击
            self::EVENT_TYPE_ATTACKED => [
                'main_event' => self::EVENT_TYPE_KILLED,
            ]
        ];
        foreach($eventListTimeKey as $key=>$timeDetail){
            // 单位事件内的所有事件
            if(count($timeDetail)<=1){
                continue ;
            }
            $tmpTimeDetail= $timeDetail;
            foreach($timeDetail as $event){
                // 如果是子事件
                if(isset($eventAddingMapping[$event['log_event_type']])){
                    if(is_array($eventAddingMapping[$event['log_event_type']]['main_event'])){
                        // 循环配置里面的主事件列表，一个子事件会属于多个主事件
                        foreach($eventAddingMapping[$event['log_event_type']]['main_event'] as $tmpEvent){
                            foreach($tmpTimeDetail as &$tmpDetail){
                                if($tmpDetail['log_event_type']==$tmpEvent){
                                    $tmpDetail['ext_log'][$event['log_event_type']][]=$event;
                                }
                            }
                        }
                    }else{
                        // 遍历这个单位时间的事件列表，找到主事件，添加到['ext_log']
                        foreach($tmpTimeDetail as &$tmpDetail){
                            if($tmpDetail['log_event_type']==$eventAddingMapping[$event['log_event_type']]['main_event']){
                                $tmpDetail['ext_log'][$event['log_event_type']][]=$event;
                            }
                        }
                    }
                }
            }
            $eventListTimeKey[$key]=$tmpTimeDetail;
        }
        return $eventListTimeKey;
    }

    public static function formatOne($log)
    {
        $event = [
            'log_time' => substr($log, 0, 25),
            'log_map_name' => "",
            'log_event_type' => "",
            'log_team_name' => '',
            'log_team_camp' => "",
            'log_duration' => '',
            'log_flag_event_type' => '',
        ];
        $matchMap = [
            // 存活事件
            // [
            //     'reg' => 'Alive players: ',
            //     'event_type' => self::EVENT_TYPE_ALIVE_PLAYERS,
            //     'deal_func' => function ($logLine) {
            //         return self::alivePlayerDetail($logLine);
            //     },

            // ],
            // match开始
            [
                'reg' => 'Match starts in 5 seconds',
                'event_type' => self::EVENT_TYPE_MATCH_UP_COMING,
            ],
            // battle开始
            [
                'reg' => 'World triggered "Match_Start"',
                'event_type' => self::EVENT_TYPE_MATCH_STARTED,
                'deal_func' => function ($logLine) {
                    return self::getMatchStartDetail($logLine);
                },
            ],
            // 选择阵营为反恐精英
            [
                'reg' => 'Team playing "CT"',
                'event_type' => self::EVENT_TYPE_TEAM_PLAYING_CT,
                'deal_func' => function ($logLine) {
                    return self::getTeamPlayingDetail($logLine);
                },
            ],
            // 选择阵营为恐怖分子
            [
                'reg' => 'Team playing "TERRORIST"',
                'event_type' => self::EVENT_TYPE_TEAM_PLAYING_T,
                'deal_func' => function ($logLine) {
                    return self::getTeamPlayingDetail($logLine);
                },
            ],
            // battle结束
            [
                'reg' => 'Game Over:',
                'event_type' => self::EVENT_TYPE_GAME_OVER,
                'deal_func' => function ($logLine) {
                    return self::getGameOverDetail($logLine);
                }
            ],
            // 刀局开始
            [
                'reg' => 'World triggered "Knife_Round_Start"',
                'event_type' => self::EVENT_TYPE_KNIFE_ROUND_START,
            ],
            // round开始(一场round开始的基点)
            [
                'reg' => 'World triggered "Round_Start"',
                'event_type' => self::EVENT_TYPE_ROUND_START,
                'log_flag_event_type' => self::EVENT_TYPE_ROUND_START,
            ],
            // round结束(一场round结束的基点)
            [
                'reg' => 'World triggered "Round_End"',
                'event_type' => self::EVENT_TYPE_ROUND_END,
                'log_flag_event_type' => self::EVENT_TYPE_ROUND_END,
            ],
            // 炸弹拆除
            [
                'reg' => 'SFUI_Notice_Bomb_Defused',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_BOMB_DEFUSED,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getBombDefusedInfo($line);
                }
            ],
            // 炸弹爆炸
            [
                'reg' => 'Team "TERRORIST" triggered "SFUI_Notice_Target_Bombed"',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_TARGET_BOMBED,
                'log_win_camp' => 'terrorist',
                'deal_func' => function ($line) {
                    return self::getTargetBombed($line);
                }
            ],
            // 反恐精英获胜
            [
                'reg' => 'SFUI_Notice_CTs_Win',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_CT_WIN,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getCtsWinDetail($line);
                }
            ],
            // 目标保存完好(反恐精英获胜)
            [
                'reg' => 'SFUI_Notice_Target_Saved',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_TARGET_SAVED,
                'log_win_camp' => 'ct',
                'deal_func' => function ($line) {
                    return self::getTargetSavedDetail($line);
                }
            ],
            // 通知恐怖分子获胜(恐怖分子获胜)
            [
                'reg' => 'SFUI_Notice_Terrorists_Win',
                'event_type' => self::EVENT_TYPE_DETAIL_WIN_TYPE,
                'log_win_type' => self::TYPE_WIN_TYPE_T_WIN,
                'log_win_camp' => 'terrorist',
                'deal_func' => function ($line) {
                    return self::getTWinDetail($line);
                }
            ],
            // 击杀
            [
                'reg' => 'killed "',
                'event_type' => self::EVENT_TYPE_KILLED,
                'log_flag_event_type' => self::EVENT_TYPE_KILLED,
                'deal_func' => function ($line) {
                    return self::getKillDetailInfoByLogLine($line);
                },
            ],
            // // 比赛换边
            // [
            //     'reg' => 'World triggered "Match_SwitchTeam"',
            //     'event_type' => self::EVENT_TYPE_MATCH_SWITCH_TEAM,
            // ],
            // // 加时赛换边
            // [
            //     'reg' => 'World triggered "Match_SwitchTeamOvertime"',
            //     'event_type' => self::EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH,
            // ],
            // 比赛换边
            [
                'reg' => ' switched from team ',
                'event_type' => self::EVENT_TYPE_FUNSPARK_SWITCH_TEAM,
                'log_event_type' => self::EVENT_TYPE_FUNSPARK_SWITCH_TEAM,
                'deal_func' => function ($line){
                    return self::getFunsparkSwitchTeam($line);
                }
            ],
            // 助攻
            [
                'reg' => ' assisted killing ',
                'event_type' => self::EVENT_TYPE_ASSISTED_KILLING,
                'deal_func' => function ($line) {
                    return self::assistedKillingDetail($line);
                }
            ],
            // 安装炸弹
            [
                'reg' => 'triggered "Planted_The_Bomb"',
                'event_type' => self::EVENT_TYPE_PLANTED_THE_BOMB,
                'deal_func' => function ($log) {
                    return self::plantedTheBomb($log);
                },
            ],
            // 攻击
            [
                'reg' => ' attacked ',
                'event_type' => self::EVENT_TYPE_ATTACKED,
                'deal_func' => function ($log) {
                    return self::attackedDetail($log);
                },
            ],
            // 闪光弹助攻
            [
                'reg' => ' flash-assisted killing ',
                'event_type' => self::EVENT_TYPE_FLASH_ASSISTED_KILLING,
                'deal_func' => function ($log) {
                    return self::flashAssistedKillingDetail($log);
                },
            ],
            // 拆除炸弹
            [
                'reg' => "Defused_The_Bomb",
                'event_type' => self::EVENT_TYPE_DEFUSED_THE_BOMB,
                'deal_func' => function ($log) {
                    return self::defusedBombInfo($log);
                },
            ],
            // 离开购买区
            [
                'reg' => ' left buyzone with [ ',
                'event_type' => self::TYPE_LEFT_BUY_ZONE_WITH,
                'deal_func' => function ($log) {
                    return self::leftBuyZoneWith($log);
                },
            ],
            // // 指令暂停
            // [
            //     'reg' => ' Pause start ',
            //     'event_type' => self::TYPE_EVENT_PAUSE_START,
            // ],
            // // 暂停结束
            // [
            //     'reg' => 'Pause end',
            //     'event_type' => self::TYPE_EVENT_PAUSE_END,
            // ],
            // 地图指令,比赛暂停
            [
                'reg' => 'Match pause is enabled - mp_pause_match',
                'event_type' => self::EVENT_TYPE_MP_PAUSE_MATCH,
            ],
            // 地图指令,比赛暂停结束
            [
                'reg' => 'Match pause is disabled - mp_unpause_match',
                'event_type' => self::EVENT_TYPE_MP_UNPAUSE_MATCH,
            ],
            // 平局
            [
                'reg' => 'World triggered "SFUI_Notice_Round_Draw"',
                'event_type' => self::EVENT_TYPE_ROUND_DRAW,
                'deal_func' => function ($log){
                    return self::roundDraw($log);
                }
            ],
            // 重新加载
            [
                'reg' => 'World triggered "Match_Reloaded"',
                'event_type' => self::EVENT_TYPE_MATCH_RELOADED,
                'deal_func' => function($log){
                    return self::matchReloaded($log);
                }
            ],
        ];
        foreach ($matchMap as $key => $val) {
            if (strstr($log, $val['reg']) !== false) {
                $event['log_event_type'] = $val['event_type'];
                if (isset($val['deal_func'])&&$val['deal_func']) {
                    $dealInfo = $val['deal_func']($log);
                    $event = array_merge($event, $dealInfo);
                }
                if(isset($val['log_win_type'])){
                    $event['log_win_type']=$val['log_win_type'];
                    $event['log_win_camp'] = $val['log_win_camp'];
                }
                return $event;
            }
        }
        return null;
    }

    /**
     * 重新加载
     * $logLine        单条日志
     */
    public static function matchReloaded($logLine)
    {
        // $logLine = '08/08/2020 - 13:38:44.128 - World triggered "Match_Reloaded" on "de_inferno"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $mapTemp = explode('on "',$logLine)[1];
        $map = explode('"',$mapTemp)[0];//重新加载的地图
        return $info = [
            'log_standard_time' => $logTime,
            'log_map_name' => $map,
        ];
    }

    /**
     * 比赛回档
     * $logLine        单条日志
     */
    public static function roundDraw($logLine)
    {
        // $logLine = '08/08/2020 - 13:38:44.128 - World triggered "SFUI_Notice_Round_Draw" (CT "3") (T "10")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $ctScoreTemp = explode('(CT "',$logLine)[1];
        $ctScore = explode('")',$ctScoreTemp)[0];//反恐精英分数
        $tScoreTemp = explode('(T "',$logLine)[1];
        $tScore = explode('")',$tScoreTemp)[0];//恐怖分子分数
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 比赛开始详情
     * $logLine         单条日志
     */
    public static function getMatchStartDetail($logLine)
    {
        // $logLine = '06/25/2020 - 16:59:42.000 - World triggered "Match_Start" on "de_nuke"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $map = explode('"',$logLine)[3];//地图
        return $info = [
            'event_type' => self::EVENT_TYPE_MATCH_STARTED,//事件类型
            'log_standard_time' => $logTime,
            'log_map_name' => $map,
        ];
    }

    /**
     * Game Over详情
     * $logLine         单条日志
     */
    public static function getGameOverDetail($logLine)
    {
        // $logLine = "06/25/2020 - 17:52:26.000 - Game Over: competitive random_classic de_nuke score 11:16 after 43 min";
        // $logLine = "08/08/2020 - 12:06:06.847 - Game Over: competitive mg_active de_dust2 score 16:12 after 58 min";
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $overDetailInfo = explode(' ', $logLine);
        $duration = (string)($overDetailInfo[12] * 60);//battle时长
        return [
            'log_standard_time' => $logTime,
            'log_duration' => $duration,
        ];        
    }

    /**
     * 选择阵营详情
     * $logLine         单条日志
     */
    public static function getTeamPlayingDetail($logLine)
    {
        // $logLine = '06/25/2020 - 16:59:42.000 - Team playing "CT": Huat Zai ';
        // $logLine = '06/25/2020 - 16:59:42.000 - Team playing "TERRORIST": invictus Gaming';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode('"',$logLine);
        $camp = explode('"',$log[1])[0];
        $camp = $camp == "CT" ? "ct" : "terrorist";//阵营
        $teamName = explode(': ',$log[2])[1];//战队昵称
        return $info = [
            'log_standard_time' => $logTime,
            'log_team_name' => $teamName,
            'log_team_camp' => $camp,
        ];
    }

    /**
     * 恐怖分子获胜
     * $logLine         单条日志
     */
    public static function getTWinDetail($logLine)
    {
        // $logLine = '06/25/2020 - 17:09:48.000 - Team "TERRORIST" triggered "SFUI_Notice_Terrorists_Win" (CT "0") (T "1")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 反恐精英获胜详情
     * $logLine         单条日志
     */
    public static function getCtsWinDetail($logLine)
    {
        // $logLine = '06/25/2020 - 17:18:19.000 - Team "CT" triggered "SFUI_Notice_CTs_Win" (CT "1") (T "3")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 炸弹爆炸详情
     * $logLine         单条日志
     */
    public static function getTargetBombed($logLine)
    {
        // $logLine = '06/25/2020 - 17:50:40.000 - Team "TERRORIST" triggered "SFUI_Notice_Target_Bombed" (CT "11") (T "15")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 炸弹拆除详情
     * $logLine             单条日志
     */
    public static function getBombDefusedInfo($logLine)
    {
        // $logLine = '06/25/2020 - 17:20:40.000 - Team "CT" triggered "SFUI_Notice_Bomb_Defused" (CT "2") (T "3")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 目标被保护完好详情
     * $logLine         单条日志
     */
    public static function getTargetSavedDetail($logLine)
    {
        // $logLine = '07/23/2020 - 16:17:24.458 - Team "CT" triggered "SFUI_Notice_Target_Saved" (CT "4") (T "2")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode(' "',$logLine);
        $ctScore = explode('")',$log[3])[0];//反恐精英比分
        $tScore = explode('"',$log[4])[0];//恐怖分子比分
        return $info = [
            'log_standard_time' => $logTime,
            'ct_score' => $ctScore,
            't_score' => $tScore,
        ];
    }

    /**
     * 炸弹被拆除了详情    By    王傲渊
     * $logLine                 单条日志
     */
    public static function defusedBombInfo($logLine)
    {
        // $logLine = '06/25/2020 - 17:20:40.000 - "xiaosaGe<10><STEAM_1:1:112788938><CT>" triggered "Defused_The_Bomb"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode('"', $logLine);
        $data = $log[1];
        $data = explode("<", $data);
        $playerName = $data[0];//选手昵称
        $playerID = explode(">", $data[2]);
        $playerID = $playerID[0];//选手ID
        $camp = explode(">", $data[3]);
        $camp = $camp[0];//选手阵营
        $camp = $camp == "CT" ? "ct" : "terrorist";
        return $info = [
            'log_standard_time' => $logTime,
            'playID' => $playerID,
            'playerName' => $playerName,
            'camp' => $camp,
            'roundEventType' => self::EVENT_TYPE_DEFUSED_THE_BOMB,
            'round_side' => "ct",
        ];
    }

    /**
     * 选手离开购买区   By   王傲渊
     * $logLine             单条日志
     */
    public static function leftBuyZoneWith($logLine)
    {
        // $logLine = '06/25/2020 - 16:59:47.000 - "bnwgiggs`<4><STEAM_1:1:56741280><CT>" left buyzone with [ weapon_knife_css weapon_usp_silencer weapon_awp weapon_smokegrenade weapon_flashbang weapon_incgrenade defuser kevlar(100) helmet ]';
        // $logLine = '06/25/2020 - 17:01:01.000 - "splashske<5><STEAM_1:0:21800><CT>" left buyzone with [ ]';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $log = explode('- ', $logLine);
        if(isset($log[3])){
            $log[2]=implode('- ',[$log[2],$log[3]]);
        }
        $infoStr=substr($logLine,28);
        $infosReg='/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\sleft buyzone with \[\s?(.*)\s?\]$/i';
        $regTest=preg_match_all($infosReg,$infoStr,$pregMatch);
        if($regTest){

        }else{
            //throw new \Exception("匹配错误啦！！");
        }

//        $tmp = explode('<', $log[2]);
//        $playerName = explode('"',$tmp[0])[1];//选手昵称
//        $playerID = explode('>', $tmp[2]);
//        $playerID = $playerID[0];//选手ID
//        $playCamp = explode(">",$tmp[3]);
//        $playCamp = $playCamp[0];
//        $playCamp = $playCamp == "CT" ? "ct" : "terrorist";//选手阵营
//        $arms = explode("[ ",$tmp[3]);
//        $arms = explode("]",$arms[1]);
//        $arms = explode(" ",$arms[0]);//武器数组
//        $arms = array_filter ($arms);
        // 详情
        $arms=[];
        $armsStr=@$pregMatch[5][0];
        if($armsStr){
            $arms=explode(" ",trim($armsStr));
        }
        $detail = [
            'player_camp' => @$pregMatch[4][0]=='CT'?'ct':'terrorist',
            'arms' => $arms,
        ];
        $info = [
            'log_standard_time' => $logTime,
            'playerName' => @$pregMatch[1][0],
            'playID' => @$pregMatch[3][0],
            'detail' => $detail,
        ];
        return $info;
    }

    /**
     * 击杀详情    By    王傲渊
     * $logLine         单条日志
     */
    public static function getKillDetailInfoByLogLine($logLine)
    {
        // $logLine = '06/25/2020 - 17:00:56.000 - "splashske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1" (headshot)';
        // $logLine = '06/25/2020 - 17:00:56.000 - "splashske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTimeData = $logTimeTemp.' '.$logTemp[1];//日志时间
        $logTime = substr($logLine, 0, 25);//日志内时间
        $time = self::transformationTime($logTime);//日志内转化出的带有毫秒的时间
        $standardTime = self::roundTimeStamp($logTime);//回合内时间转化出的时间戳不带毫秒
        $temp = explode('"',$logLine);
        $playerName = explode('<',$temp[1])[0];//击杀者选手昵称
        $killPlayerSteamId = explode('<',$temp[1]);
        $killPlayerSteamId = explode('>',$killPlayerSteamId[2])[0];//击杀者选手steam_id
        $killCamp = explode('<',$temp[1])[3];
        $killCamp = explode('>',$killCamp)[0];
        $killCamp = $killCamp == "CT" ? "ct" : "terrorist";//击杀者选手阵营
        $killPosition = explode(' ',$temp[2]);
        $killPosition = $killPosition[1] . ' ' . $killPosition[2] . ' ' . $killPosition[3];//击杀者选手位置
        $victimName = explode('<',$temp[3])[0];//被杀选手昵称
        $victimPlayerSteamId = explode('<',$temp[3])[2];
        $victimPlayerSteamId = explode('>',$victimPlayerSteamId)[0];//被击杀者选手steam_id
        $victimCamp = explode('<',$temp[3])[3];
        $victimCamp = explode('>',$victimCamp)[0];
        $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";//被击杀者阵营
        $victimPosition = explode(' ',$temp[4]);
        $victimPosition = $victimPosition[1] . " " . $victimPosition[2] . " " . $victimPosition[3];//被击杀者位置
        $arms = $temp[5];//武器名称
        $body = @explode('(',$temp[6])[1];
        $body = @explode(')',$body)[0];//击杀部位
        $isHeadshot = $body == "headshot" ? 1 : 2;//是否爆头
        return $info = [
            'log_standard_time' => $logTimeData,
            'roundEventType' => self::EVENT_TYPE_KILLED,//对局事件类型
            'event_type' => self::EVENT_TYPE_KILLED,//对局事件类型
            'killer' => [
                'player_id' => $killPlayerSteamId,
                'name' => $playerName,
                'camp' => $killCamp,
                'coordinate' => $killPosition,
            ],
            'deader' => [
                'player_id' => $killPlayerSteamId,
                'name' => $victimName,
                'camp' => $victimCamp,
                'coordinate' => $victimPosition,
            ],
            'time' => $logTime,
            'with' => $arms,
            'body' => $body,
            'roundTimeStamp' => $standardTime,
            'roundTime' => $time,
            'is_headshot' => $isHeadshot,
            'round_side' => $killCamp,
        ];
    }

    /**
     * 助攻事件详情  By  王傲渊
     * $logLine         单条事件日志
     */
    public static function assistedKillingDetail($logLine)
    {
        // $logLine = '08/03/2020 - 09:39:23.681 - "Tony<11><BOT><CT>" assisted killing "Brad<6><BOT><TERRORIST>"';
        // $logLine = '06/25/2020 - 17:03:00.000 - "bnwgiggs`<4><STEAM_1:1:56741280><CT>" assisted killing "Brad<8><BOT><TERRORIST>"';
        // $logLine = '06/25/2020 - 17:04:16.000 - "Ba nk<14><BOT><TERRORIST>" assisted killing "splas hske<5><STEAM_1:0:21800><CT>';
        // $logLine = '06/25/2020 - 17:06:13.000 - "bnwgi ggs`<4><STEAM_1:1:56741280><CT>" assisted killing "Oi ix<11><STEAM_1:0:35445242><TERRORIST>"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $assistsPlayerNickName = explode('"', $logLine);
        $assistsPlayerNickName = explode("<",$assistsPlayerNickName[1])[0];//助攻选手昵称
        $assistsPlayerSteamId = explode('><',$logLine)[1];//助攻选手steam_id
        $assistsPlayerCamp = explode('><',$logLine)[2];
        $assistsPlayerCamp = explode('>',$assistsPlayerCamp)[0];
        $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";//助攻选手阵营
        $victimNickName = explode('" assisted killing "',$logLine)[1];
        $victimNickName = explode('<',$victimNickName)[0];//受害者选手昵称
        $victimSteamId = explode('" assisted killing "',$logLine)[1];
        $victimSteamId = explode('><',$victimSteamId)[1];//受害者选手steam_id
        $victimCamp = explode('" assisted killing "',$logLine)[1];
        $victimCamp = explode('><',$victimCamp)[2];
        $victimCamp = explode('>',$victimCamp)[0];
        $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";//受害者阵营
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_ASSISTED_KILLING,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'victimNickName' => $victimNickName,
            'victimCamp' => $victimCamp,
            'victimSteamId' => $victimSteamId,
            'assistsPlayerNickName' => $assistsPlayerNickName,
            'assistsPlayerCamp' => $assistsPlayerCamp,
            'assistsPlayerSteamId' => $assistsPlayerSteamId,
        ];
    }

    /**
     * 攻击详情  By  王傲渊
     * $logLine     单条日志
     */
    public static function attackedDetail($logLine)
    {
        // $logLine = '06/25/2020 - 17:00:56.000 - "splas hske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] attacked "xiaos aGe<6><STEAM_1:1:112788938><TERRORIST>" [-1596 -1111 -417] with "m4a1" (damage "88") (damage_armor "18") (health "12") (armor "81") (hitgroup "head")';
        // $logLine = '08/03/2020 - 09:36:05.267 - "John<7><BOT><CT>" [-47 -1660 -168] attacked "Victor<10><BOT><TERRORIST>" [188 -1571 -174] with "flashbang" (damage "1") (damage_armor "0") (health "99") (armor "98") (hitgroup "generic")';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $attackNickname = explode('"',$logLine)[1];
        $attackNickname = explode('<',$attackNickname)[0];//攻击选手昵称
        $attackSteamId = explode('"',$logLine)[1];
        $attackSteamId = explode('><',$attackSteamId)[1];//攻击选手steam_id
        $attackCamp = explode('"',$logLine)[1];
        $attackCamp = explode('><',$attackCamp)[2];
        $attackCamp = explode('>',$attackCamp)[0];
        $attackCamp = $attackCamp == "CT" ? "ct" : "terrorist";//攻击选手阵营
        $attackLocation = explode('" ',$logLine)[1];
        $attackLocation = explode(' attacked',$attackLocation)[0];//攻击选手位置
        $victimNickName = explode('attacked "',$logLine)[1];
        $victimNickName = explode('<',$victimNickName)[0];//受害者选手昵称
        $victimSteamId = explode('attacked "',$logLine)[1];
        $victimSteamId = explode('><',$victimSteamId)[1];//受害者选手steam_id
        $victimCamp = explode('attacked "',$logLine)[1];
        $victimCamp = explode('><',$victimCamp)[2];
        $victimCamp = explode('>',$victimCamp)[0];
        $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";//受害者阵营
        $victimLocation = explode('attacked "',$logLine)[1];
        $victimLocation = explode('><',$victimLocation)[2];
        $victimLocation = explode('" ',$victimLocation)[1];
        $victimLocation = explode(' with ',$victimLocation)[0];//受害者选手位置
        $with = explode(' with "',$logLine)[1];
        $with = explode('"',$with)[0];//攻击选手使用武器
        $damage = explode('(damage "',$logLine)[1];
        $damage = explode('")',$damage)[0];//攻击伤害
        $health = explode('(health "',$logLine)[1];
        $health = explode('")',$health)[0];//剩余血量
        $body = explode('(hitgroup "',$logLine)[1];
        $body = explode('")',$body)[0];//打击部位
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_ATTACKED,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'victimSteamId' => $victimSteamId,
            'victimNickName' => $victimNickName,
            'victimCamp' => $victimCamp,
            'victimLocation' => $victimLocation,
            'attackSteamId' => $attackSteamId,
            'attackNickname' => $attackNickname,
            'attackCamp' => $attackCamp,
            'attackLocation' => $attackLocation,
            'damage' => $damage,
            'body' => $body,
            'with' => $with,
            'health' => $health,
        ];
    }

    /**
     * 闪光弹助攻详情   By  王傲渊
     * $logLine            单条日志
     */
    public static function flashAssistedKillingDetail($logLine)
    {
        // $logLine = '06/25/2020 - 17:06:33.000 - "Ba nk<14><BOT><TERRORIST>" flash-assisted killing "nephh<16><STEAM_1:0:128929642><CT>"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $assistsPlayerNickName = explode(' "',$logLine)[1];
        $assistsPlayerNickName = explode('<',$assistsPlayerNickName)[0];//闪光弹助攻者昵称
        $assistsSteamId = explode(' "',$logLine)[1];
        $assistsSteamId = explode('><',$assistsSteamId)[1];//闪光弹助攻选手steam_id
        $assistsPlayerCamp = explode(' "',$logLine)[1];
        $assistsPlayerCamp = explode('><',$assistsPlayerCamp)[2];
        $assistsPlayerCamp = explode('>"',$assistsPlayerCamp)[0];
        $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";//闪光弹助攻选手阵营
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_FLASH_ASSISTED_KILLING,
            'roundTime' => $roundTime,
            'assistsSteamId' => $assistsSteamId,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'assistsPlayerNickName' => $assistsPlayerNickName,
            'assistsPlayerCamp' => $assistsPlayerCamp,
        ];
    }

    /**
     * 安装炸弹详情  By  王傲渊
     * $logLine         单条日志
     */
    public static function plantedTheBomb($logLine)
    {
        // $logLine = '06/25/2020 - 17:13:04.000 - "Bobos aur<20><STEAM_1:0:4842207><TERRORIST>" triggered "Planted_The_Bomb"';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $roundTime = self::changeTime($logLine);//回合内时间
        $roundTimeStamp = self::roundTimeStamp($logLine);//回合内时间戳
        $log = explode('"', $logLine);
        $data = $log[1];
        $data = explode("<", $data);
        $playerName = $data[0];//选手昵称
        $playerID = explode(">", $data[2]);
        $playerID = $playerID[0];//选手ID
        $camp = explode(">", $data[3]);
        $camp = $camp[0];//选手阵营
        $camp = $camp == "CT" ? "ct" : "terrorist";
        return $info = [
            'log_standard_time' => $logTime,
            'roundEventType' => self::EVENT_TYPE_PLANTED_THE_BOMB,
            'roundTime' => $roundTime,
            'roundTimeStamp' => (string)$roundTimeStamp,
            'playID' => $playerID,
            'playerName' => $playerName,
            'camp' => $camp,
            'round_side' => "terrorist",
        ];
    }

    /**
     * funspark数据源换边
     * $logLine         单条日志
     */
    public static function getFunsparkSwitchTeam($logLine)
    {
        // $logLine = '06/25/2020 - 17:09:18.000 - "destroyeR<17><STEAM_1:0:42556902>" switched from team <CT> to <TERRORIST>';
        $logTemp = explode(' - ',$logLine);
        $logTimeTemp = explode('/',$logTemp[0]);
        $logTimeTemp = $logTimeTemp[2].'-'.$logTimeTemp[0].'-'.$logTimeTemp[1];
        $logStardTime = $logTimeTemp.' '.$logTemp[1];//日志时间
        $logTime = substr($logLine,0,25);//事件发生日志中的时间
        return $info = [
            'log_time' => $logTime,
            'log_standard_time' => $logStardTime,
        ];
    }

    /**
     * 回合内时间戳   By   王傲渊
     * $roundTime         回合内时间
     */
    public static function roundTimeStamp($roundTime)
    {
        $timeStamp = substr($roundTime, 0, 21);
        $timeStamp = str_replace(' - ', ' ', $timeStamp);
        $timeStamp = strtotime($timeStamp);
        return $timeStamp;
    }

    /**
     * 时间转换csgo专用  By  王傲渊
     * $time    要转换的时间(字符串)
     */
    public static function changeTime($time)
    {
        $time = substr($time, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }

    /**
     * 日志内时间转换,带毫秒
     * $time        日志内时间
     */
    public static function transformationTime($logTime)
    {
        $time = str_replace(' - ', ' ', $logTime);
        $time = str_replace('/','-',$time);
        return $time;
    }

    /**
     * 获取武器ID和是否是刀杀类型  By  王傲渊
     * $armsName      武器名称
     */
    // public static function getArmsID($armsName)
    // {
    //     $data = new MetadataCsgoWeapon();
    //     $armsInfo = $data->find()->where(['name' => $armsName])->select('id,kind')->one();
    //     // 类型三是刀
    //     if (isset($armsInfo['kind']) && $armsInfo['kind'] == 3) {
    //         $killType = "knife_kill";//击杀类型为刀杀
    //     } else {
    //         $armsName = strstr($armsName, "knife");
    //         if ($armsName) {
    //             $killType = "knife_kill";
    //         } else {
    //             $killType = "";
    //         }
    //     }
    //     return $killInfo = [
    //         'armsId' => $armsInfo['id'],//武器ID
    //         'killType' => $killType,//击杀类型
    //     ];
    // }

    // /**
    //  * @param $logBeginTime
    //  * @param $logEndTime
    //  * @param $ipAddress
    //  * @return array
    //  * 获取日志列表并格式化
    //  */
    // public static function formatList($logBeginTime, $logEndTime, $ipAddress)
    // {
    //     $info=file_get_contents(__DIR__.'/123456.log');
    //     $list=explode("\n",$info);
    //     //$find = 'log';
    //     //// 约束条件
    //     //$map = ['>=', 'created_at', $logBeginTime];
    //     //$where = ['<=', 'created_at', $logEndTime];
    //     //$ip = ['=', 'ip_address', $ipAddress];
    //     //$q = ReceiveDataFunspark::find()
    //     //    ->select($find)
    //     //    ->orderBy('id')
    //     //    ->where($map)
    //     //    ->andwhere($ip)
    //     //    ->andwhere($where);
    //     //$perPage = 1000;
    //     //$count = $q->count();
    //     $eventList = [];
    //     $eventListTimeKey = [];
    //     //for ($page = 1; $page <= $count / $perPage + 1; $page++) {
    //     //    $list = $q->limit($perPage)->offset($perPage * ($page - 1))
    //     //        ->asArray()->all();
    //        if ($list) {
    //            //foreach ($list as $val) {
    //                //$log = $val['log'];
    //                //$listLines = explode("\n", $log);
    //                //foreach ($listLines as $v) {
    //                foreach($list as $v){
    //                    $event = self::formatOne($v);
    //                    if ($event) {
    //                        $eventList[] = $event;
    //                        $eventListTimeKey[$event['log_time']][] = $event;
    //                    }
    //                }
    //            //}
    //        }
    //     //}
    //     // 主次事件合并
    //     $timeMergeEventList = self::mergeEvent($eventListTimeKey);
    //     $mergeEventList=[];
    //     foreach($timeMergeEventList as $vEventList){
    //         foreach($vEventList as $event){
    //             $mergeEventList[]=$event;
    //         }
    //     }
    //     // todo 回合结束时，继续杀人的问题，回合结束后移，循环找到回合结束后，判断其后的100条数据，如果有aliveplayer事件，则推后round
    //     return $mergeEventList;
    // }
}