<?php

namespace app\modules\task\services\logformat;

use app\modules\data\models\ReceiveData5eplay;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\rest\exceptions\BusinessException;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\QueueServer;

class CsgoLogFive extends CsgoLogFormatBase
{
    public static function run($tagInfo, $taskInfo)
    {
        $params = json_decode($taskInfo['params'], true);
        // 这里面是待执行的消息
        self::dealWithRecord($params);
        return [];
    }
    public static function getListAndDo()
    {
        // 这里循环整个表 按照顺序取出待处理的数据
        $where = [
            'and',
            ['=', 'is_handle', 2],
//            ['>=', 'created_at', '2020-08-09 00:00:00'],
//            ['<=', 'created_at', '2020-08-10 07:00:00'],
        ];
        $q = ReceiveData5eplay::find()
            ->orderBy('id')
            ->where($where);
        $perPage = 1000;
        $count = $q->count();
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset(0)
                ->asArray()->all();
            $sql = $q->createCommand()->getRawSql();
            if ($list) {
                foreach ($list as $line) {
                    self::dealWithRecord($line);
                }
            } else {
                break;
            }
        }
    }

    public static function getEventMapReg()
    {
        $eventMapReg = [
            /**
             * 对局准备开始：这个将来可以做成多个条件对应同一个事件
             * 事件类型：主事件（时间、对局准备开始)
             * 输出形式：只记录不输出
             */
            [
                'type'=> 'reg',
                'reg' => '/^World triggered "Pug_Start"$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_UP_COMING,
                'test_lines'=>[
                    '06/25/2020 - 17:10:22.000 - World triggered "Pug_Start"',
                ],
            ],
            /**
             * Game_Commencing
             * 事件类型：zi事件
             * 输出形式：只记录不输出
             */
            [
                'type'=> 'reg',
                'reg' => '/^World triggered "Game_Commencing"$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_GAME_COMMENCING,
                'test_lines'=>[
                    '06/25/2020 - 17:10:22.000 - World triggered "Game_Commencing"',
                ],
            ],
            /**
             * 回档
             * 事件类型：主事件、子事件（时间、回档、地图）
             * 所属主事件：回合结束（如果存在回档子事件，则该回合不记录）
             * 子事件：选择阵营（时间、战队、当前阵营）
             * 子事件：回档位置（时间、回档至多少回合）
             * 输出形式：socket frames、socket events、rest api详情
             */
            [
                'type' => 'reg',
                'reg' => '/^World triggered "Match_Reloaded" on "(.*)"$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_RELOADED,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - World triggered "Match_Reloaded" on "de_< in >f"er -  ewqe .no"',
                ],
                'deal_func' => function($regMatchAll,$line){
                    return [
                        'log_map_name' => @$regMatchAll[1][0],
                    ];
                }
            ],
            /**
             * 对局开始
             * 事件类型：主事件（时间、对局开始、地图）
             * 子事件：选择阵营（时间、战队、起始阵营）
             * 输出形式：socket frames、socket events、rest api详情
             */
            [
                'type'=> 'reg',
                'reg' => '/^World triggered "Match_Start" on "(.*)"$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_START,
                'deal_func' => function ($regMatchAll,$line) {
                    return [
                        'log_map_name'=>@$regMatchAll[1][0]
                    ];
                },
                'test_lines'=>[
                    '08/10/2020 - 16:13:05.723 - World triggered "Match_Start" on "de_inferno"',
                ],
            ],
            /**
             * 绑定当前对局标识
             * 事件类型：主事件（时间、对局标识）
             * 子事件：选择阵营（时间、战队、起始阵营）
             * 输出形式：只记录不输出
             */
            [
                'type'=> 'reg',
                'reg' => '/^Tools: upload backup file backup_round(\d+).txt to remote folder\s(\w+)\/backup_round(\d+).txt$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_BINDING_CURRENT_IDENTIFICATION,
                'deal_func' => function ($regMatchAll,$line) {

                    return [
//                        $regMatchAll,
                        'round_order' => @$regMatchAll[1][0],
                        'identification' =>  @$regMatchAll[2][0],
                    ];
                },
                'test_lines'=>[
                    '06/25/2020 - 16:59:42.000 - Tools: upload backup file backup_round02.txt to remote folder 20200716154531d54780/backup_round02.txt',
                ],
            ],
            /**
             * 选择阵营为反恐精英
             * 事件类型：子事件（时间、战队、起始阵营（对局开始）、当前阵营（回档））
             * 所属主事件：对局开始、回档
             * 输出形式：跟随主事件输出
             */
            [
                'type'=> 'reg',
                'reg' => '/^Team playing "(CT)": (.*)$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[1][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";//阵营
                    return [
                        'log_team_name' => @$regMatchAll[2][0],
                        'log_team_camp' => $camp,
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 16:59:42.000 - Team playing "CT": Tiger',
                ],
            ],
            /**
             * 选择阵营为恐怖分子
             * 事件类型：子事件（时间、战队、起始阵营（对局开始）、当前阵营（回档））
             * 所属主事件：对局开始、回档
             * 输出形式：跟随主事件输出
             */
            [
                'type'=> 'reg',
                'reg' => '/^Team playing "(TERRORIST)": (.*)$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_TEAM_PLAYING_T,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[1][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";//阵营
                    return [
                        'log_team_name' => @$regMatchAll[2][0],
                        'log_team_camp' => $camp,
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 16:59:42.000 - Team playing "TERRORIST": invictus Gaming',
                ]
            ],
            /**
             * 对局结束
             * 事件类型：主事件（时间、对局结束、地图、比分、对局时长（分钟））
             * 子事件：回合结束方式（时间、获胜方、回合结束方式、比分）
             * 输出形式：socket frames、socket events、rest api详情
             */
            [
                'type' => 'reg',
                'reg' => '/^Game Over: competitive\s.*\s(.*)\sscore\s(\d+:\d+)\safter\s(.*)$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_END,
                'deal_func' => function($regMatchAll,$line){

                    return [
//                        $regMatchAll,
                        'map_name' => @$regMatchAll[1][0],
                        'score' => @$regMatchAll[2][0],
                        'log_duration' => @$regMatchAll[3][0],
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 12:06:06.847 - Game Over: competitive mg_active de_dust2 score 16:12 after 58 min',
                    '08/08/2020 - 12:06:06.847 - Game Over: competitive mg_bomb de_inferno score 16:4 after 35 min',
                    '08/08/2020 - 12:06:06.847 - Game Over: competitive mg_active de_dust2 score 17:19 after 73 min',
                    '08/08/2020 - 12:06:06.847 - Game Over: competitive random_classic de_nuke score 11:16 after 43 min',
                    '08/08/2020 - 12:06:06.847 - Game Over: competitive mg_active de_vertigo score 16:11 after 48 min',
                ],
            ],
            /**
             * 回合开始
             * 事件类型：主事件（时间、回合开始）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             *
             */
            [
                'type' => 'reg',
                'reg' => '/^World triggered "Round_Start"$/i',
                'event_type' => self::EVENT_TYPE_ROUND_START,
                'test_lines' => [
                    '06/25/2020 - 17:12:32.000 - World triggered "Round_Start"',
                ],
            ],
            /**
             * 回合结束：（这里需要保存选手身上的武器道具，金钱，回档要用到）
             * 事件类型：主事件（时间、回合结束）
             * 子事件：回合结束方式（非平局）（时间、获胜方、获胜方式、比分）
             * 子事件：回合结束方式（平局）（时间、平局、比分）
             * 子事件：回档（时间、回档、地图）（若回档，则不记录该回合）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^World triggered "Round_End"$/i',
                'event_type' => self::EVENT_TYPE_ROUND_END,
                'test_lines' => [
                    '06/25/2020 - 17:09:48.000 - World triggered "Round_End"',
                ],

            ],
            /**
             * 回合结束方式：（正常）
             * 事件类型：子事件（时间、获胜方、获胜方式、比分）
             * 所属主事件：回合结束
             * 输出形式：跟随主事件输出
             */
            [
                'type' => 'reg',
                'reg' => '/^Team "(TERRORIST|CT)" triggered "SFUI_Notice_(.*)" \(CT "(\d+)"\) \(T\s"(\d+)"\)$/i',
                'event_type' => self::EVENT_TYPE_ROUND_WIN_TYPE,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp='';
                    $logWinType=self::EVENT_TYPE_BOMB_DEFUSED;
                    switch ($regMatchAll[2][0]){
                        case 'Bomb_Defused':
                            $logWinType=self::EVENT_TYPE_BOMB_DEFUSED;
                            break;
                        case 'Target_Bombed':
                            $logWinType=self::EVENT_TYPE_TARGET_BOMBED;
                            break;
                        case 'CTs_Win':
                            $logWinType=self::EVENT_TYPE_CTS_WIN;
                            break;
                        case 'Target_Saved':
                            $logWinType=self::EVENT_TYPE_TARGET_SAVED;
                            break;
                        case 'Terrorists_Win':
                            $logWinType=self::EVENT_TYPE_TERRORISTS_WIN;
                            break;
                    }
                    if($regMatchAll[1][0]=="CT"){
                        $camp='ct';
                    }else{
                        $camp='terrorist';
                    }
                    return [
                        'log_win_camp' => $camp,
                        'camp' => $camp,
                        'ct_score' => @$regMatchAll[3][0],
                        't_score' => @$regMatchAll[4][0],
                        'log_win_type' => $logWinType,
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:20:40.000 - Team "TERRORIST" triggered "SFUI_Notice_Terrorists_Win" (CT "4") (T "3")',
                    '06/25/2020 - 17:20:40.000 - Team "CT" triggered "SFUI_Notice_CTs_Win" (CT "1") (T "0")',
                    '06/25/2020 - 17:20:40.000 - Team "TERRORIST" triggered "SFUI_Notice_Target_Bombed" (CT "1") (T "5")',
                    '06/25/2020 - 17:20:40.000 - Team "CT" triggered "SFUI_Notice_Bomb_Defused" (CT "1") (T "1")',
                    '06/25/2020 - 17:20:40.000 - Team "CT" triggered "SFUI_Notice_Target_Saved" (CT "4") (T "2")',
                ]
            ],
            /**
             * 击杀：（这里要更新死亡的选手身上的道具装备为空）
             * 事件类型：主事件（时间、击杀、击杀者选手昵称、击杀者Ping、击杀者Steam ID、击杀者阵营、击杀者坐标、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被击杀者阵营、被击杀者坐标、武器、击杀特殊描述）
             * 子事件：攻击（时间、攻击者选手昵称、攻击者Ping、攻击者Steam ID、攻击者阵营、被攻击者选手昵称、被攻击者Ping、被攻击者Steam ID、被攻击者阵营、武器、武器伤害、造成护甲损伤、被攻击者剩余血量、被攻击者剩余护甲、命中部位）
             * 子事件：助攻（时间、助攻者选手昵称、助攻者Ping、助攻者Steam ID、助攻者阵营、被击杀者选手昵称、被击杀者者Ping、被击杀者Steam ID、被攻击者阵营）
             * 子事件：闪光弹助攻（时间、闪光弹助攻者选手昵称、闪光弹助攻者Ping、闪光弹助攻者Steam ID、闪光弹助攻者阵营、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被攻击者阵营）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\skilled\s"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\swith\s"(.*)"\s?\(?([\w\s]*)\)?$/i',
                'event_type' => self::EVENT_TYPE_PLAYER_KILL,
                'deal_func' => function ($regMatchAll,$line) {
                    $killCamp = @$regMatchAll[4][0];
                    $killCamp = $killCamp == "CT" ? "ct" : "terrorist";
                    $victimCamp = @$regMatchAll[9][0];
                    $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";
                    $isHeadshot = 2;
                    if(isset($regMatchAll[12][0]) && !empty($regMatchAll[12][0])){
                        $body = $regMatchAll[12][0];
                        $isHeadshotMatch = strstr($body,'headshot');
                        if($isHeadshotMatch){
                            $isHeadshot = 1;
                        }
                    }
                    return [
//                        $regMatchAll,
                        'roundEventType' => self::EVENT_TYPE_PLAYER_KILL,
                        'event_type' => self::EVENT_TYPE_PLAYER_KILL,
                        'killer' => [
                            'player_id' => @$regMatchAll[3][0],
                            'ping' => @$regMatchAll[2][0],
                            'name' => @$regMatchAll[1][0],
                            'camp' => $killCamp,
                            'coordinate' => @$regMatchAll[5][0],
                        ],
                        'deader' => [
                            'player_id' => @$regMatchAll[8][0],
                            'ping' => @$regMatchAll[7][0],
                            'name' => @$regMatchAll[6][0],
                            'camp' => @$victimCamp,
                            'coordinate' => @$regMatchAll[10][0],
                        ],
                        'with' => @$regMatchAll[11][0],
                        'is_headshot' => $isHeadshot,
                        'round_side' => $killCamp,
                        'log_event_type' => self::EVENT_TYPE_PLAYER_KILL,
                        'special_description' =>  $regMatchAll[12][0],
                    ];
                },
                'test_lines' => [
                    //'06/25/2020 - 17:00:56.000 - "spl <3>ash<CT>" [ ]ske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1" (headshot)',
                    //'06/25/2020 - 17:00:56.000 - "splashske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1"',
                    '06/25/2020 - 17:00:56.000 - "leaf<3><STEAM_1:1:27735361><TERRORIST>" [1891 -184 256] killed "viz<25><STEAM_1:0:216946317><CT>" [2612 -149 197] with "ak47" (headshot throughsmoke)',
                    '06/25/2020 - 17:00:56.000 - "Xeppaa<20><STEAM_1:0:141074787><TERRORIST>" [69 3028 160] killed "bwills-tbp-<28><STEAM_1:1:59654408><CT>" [61 2642 224] with "ak47" (throughsmoke)',
                    '08/09/2020 - 12:07:19.056 - "Zellsis<91><STEAM_1:0:93426123><CT>" [-285 -2174 -173] killed "oNe pesa<90><STEAM_1:0:147857885><TERRORIST>" [-78 -2155 17] with "usp_silencer" (attackerblind)',
                ]
            ],
            /**
             * 杀鸡：（这里要更新死亡的选手身上的道具装备为空）
             * 事件类型：主事件（时间、击杀、击杀者选手昵称）
             * 输出形式：rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\skilled\sother\s"chicken<(\d+)>"\s\[(.*)\]\swith\s"(.*)"$/i',
                'event_type' => self::EVENT_TYPE_CHICKEN_KILL,
                'deal_func' => function ($regMatchAll,$line) {
                    $killCamp = @$regMatchAll[4][0];
                    $killCamp = $killCamp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'roundEventType' => self::EVENT_TYPE_CHICKEN_KILL,
                        'event_type' => self::EVENT_TYPE_CHICKEN_KILL,
                        'killer' => [
                            'steam_id' => @$regMatchAll[3][0],
                            'ping' => @$regMatchAll[2][0],
                            'name' => @$regMatchAll[1][0],
                            'camp' => $killCamp,
                            'coordinate' => @$regMatchAll[5][0],
                        ],
                        'chicken' => [
                            'ping' => @$regMatchAll[6][0],
                            'coordinate' => @$regMatchAll[7][0],
                        ],
                        'with' => @$regMatchAll[8][0],
                        'log_event_type' => self::EVENT_TYPE_CHICKEN_KILL,
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:00:56.000 - "aumaNttGMO<6><STEAM_1:0:23111849><CT>" [2393 853 156] killed other "chicken<113>" [1562 1021 155] with "hegrenade"',
                ]
            ],
            /**
             * 选手更换阵营
             * 事件类型：主事件、子事件（时间、选手昵称、Ping、Steam ID、更换前阵营、更换后阵营）
             * 所属主事件：队伍更换阵营、自杀
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)>"\sswitched from team <(TERRORIST|CT|Unassigned|Spectator)> to <(TERRORIST|CT|Unassigned|Spectator)>$/i',
                'event_type' => self::EVENT_TYPE_PLAYER_SWITCH_TEAM,
                'deal_func' => function ($regMatchAll,$line) {
                    return [
                            'playerName'     => $regMatchAll[1][0],
                            'ping'     => $regMatchAll[2][0],
                            'steamId'     => $regMatchAll[3][0],
                            'beforeSide'     => strtolower($regMatchAll[4][0]),
                            'afterSide'     => strtolower($regMatchAll[5][0]),
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:01:05.000 - "spielz<7><STEAM_1:0:124605938>" switched from team <TERRORIST> to <CT>'
                ],
            ],
            /**
             * 助攻
             * 事件类型：子事件（时间、助攻、助攻者选手昵称、助攻者Ping、助攻者Steam ID、助攻者阵营、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被攻击者阵营）
             * 所属主事件：击杀
             * 输出形式：随主事件一起输出
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\sassisted killing\s"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"$/i',
                'event_type' => self::EVENT_TYPE_ASSIST,
                'deal_func' => function ($regMatchAll,$line) {
                    $assistsPlayerCamp = @$regMatchAll[4][0];
                    $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";
                    $victimCamp = @$regMatchAll[8][0];
                    $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";
                    // 傲渊说 助攻和死的人可能是一个阵营，这里支持
                    return [
//                        $regMatchAll,
                        'roundEventType' => self::EVENT_TYPE_ASSIST,
                        'victimNickName' => @$regMatchAll[5][0],
                        'victimPing' => @$regMatchAll[6][0],
                        'victimCamp' => @$victimCamp,
                        'victimSteamId' => @$regMatchAll[7][0],
                        'assistsPlayerNickName' => @$regMatchAll[1][0],
                        'assistsPing' => @$regMatchAll[2][0],
                        'assistsPlayerCamp' => @$assistsPlayerCamp,
                        'assistsPlayerSteamId' => @$regMatchAll[3][0],
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:06:13.000 - "bnwgi ggs`<4><STEAM_1:1:56741280><CT>" assisted killing "Oi ix<11><STEAM_1:0:35445242><TERRORIST>"',
                ]
            ],
            /**
             * 安放炸弹
             * 事件类型：主事件（时间、安放炸弹、安放炸弹者昵称、安放炸弹者ping、安放炸弹者Steam ID、安放炸弹者阵营）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\striggered "Planted_The_Bomb"$/i',
                'event_type' => self::EVENT_TYPE_BOMB_PLANTED,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
                        'roundEventType' => self::EVENT_TYPE_BOMB_PLANTED,
                        'playID' => @$regMatchAll[3][0],
                        'playerName' => @$regMatchAll[1][0],
                        'camp' => @$camp,
                        'round_side' => "terrorist",
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:13:04.000 - "Bo<20>bos aur<20><STEAM_1:0:4842207><TERRORIST>" triggered "Planted_The_Bomb"',
                ]
            ],
            /**
             * 攻击：（这里要更新攻击选手身上的武器，更新被攻击选手血量、护甲值，更新被攻击选手受到总伤害大于40排名第1的选手（自杀判定助攻时候用））
             * 事件类型：主事件、子事件（时间、攻击、攻击者选手昵称、攻击者Ping、攻击者Steam ID、攻击者阵营、被攻击者选手昵称、被攻击者Ping、被攻击者Steam ID、
             *         被攻击者阵营、武器、武器伤害、造成护甲损伤、被攻击者剩余血量、被攻击者剩余护甲、命中部位）
             * 所属主事件：击杀
             *输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\sattacked\s"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\swith\s"(.*)"\s\(damage\s"(\d+)"\)\s\(damage_armor\s"(\d+)"\)\s\(health\s"(\d+)"\)\s\(armor\s"(\d+)"\)\s\(hitgroup\s"(.*)"\)$/i',
                'event_type' => self::EVENT_TYPE_ATTACKED,
                'deal_func' => function ($regMatchAll,$line) {
                    $victimCamp = @$regMatchAll[9][0];
                    $victimCamp = $victimCamp == "CT" ? "ct" : "terrorist";
                    $attackCamp = @$regMatchAll[4][0];
                    $attackCamp = $attackCamp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'victimSteamId' => @$regMatchAll[8][0],
                        'victimNickName' => @$regMatchAll[6][0],
                        'victimPing' => @$regMatchAll[7][0],
                        'victimCamp' => @$victimCamp,
                        'victimLocation' => @$regMatchAll[10][0],
                        'attackSteamId' => @$regMatchAll[3][0],
                        'attackNickname' => @$regMatchAll[1][0],
                        'attackPing' => @$regMatchAll[2][0],
                        'attackCamp' => $attackCamp,
                        'attackLocation' => @$regMatchAll[5][0],
                        'damage' => @$regMatchAll[12][0],
                        'body' => @$regMatchAll[16][0],
                        'damage_armor' => @$regMatchAll[13][0],
                        'armor' => @$regMatchAll[15][0],
                        'with' => @$regMatchAll[11][0],
                        'health' => @$regMatchAll[14][0],
                    ];
                },
                'test_lines' => [
                    '08/03/2020 - 09:36:05.267 - "John<7><BOT><CT>" [-47 -1660 -168] attacked "Victor<10><BOT><TERRORIST>" [188 -1571 -174] with "flashbang" (damage "1") (damage_armor "0") (health "99") (armor "98") (hitgroup "generic")',
                    '08/03/2020 - 09:36:05.267 - "FUNSPARK OB<34><STEAM_1:1:198818230><CT>" [951 -666 -415] attacked "LMBT_R * VulkanBET<35><STEAM_1:1:26637623><TERRORIST>" [746 -1069 -416] with "m4a1" (damage "24") (damage_armor "0") (health "54") (armor "95") (hitgroup "right leg")',
                ]
            ],
            /**
             * 闪光弹助攻
             * 事件类型：子事件（时间、闪光弹助攻、闪光弹助攻者选手昵称、闪光弹助攻者Ping、闪光弹助攻者Steam ID、闪光弹助攻者阵营、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被攻击者阵营）
             * 所属主事件：击杀
             * 输出形式：随主事件一起输出
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\sflash-assisted killing\s"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"$/i',
                'event_type' => self::EVENT_TYPE_FLASHASSIST,
                'deal_func' => function ($regMatchAll,$line) {
                    $assistsPlayerCamp = @$regMatchAll[4][0];
                    $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";
                    $victimPlayerCamp = @$regMatchAll[8][0];
                    $victimPlayerCamp = $victimPlayerCamp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'roundEventType' => self::EVENT_TYPE_FLASHASSIST,
                        'assistsPlayerNickName' => @$regMatchAll[1][0],
                        'assistsPing' => @$regMatchAll[2][0],
                        'assistsPlayerCamp' => @$assistsPlayerCamp,
                        'assistsSteamId' => @$regMatchAll[3][0],
                        'victimSteamId' => @$regMatchAll[7][0],
                        'victimPlayerNickName' => @$regMatchAll[5][0],
                        'victimPing' => @$regMatchAll[6][0],
                        'victimPlayerCamp' => $victimPlayerCamp,
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:06:33.000 - "Ba nk<14><BOT><TERRORIST>" flash-assisted killing "nephh<16><STEAM_1:0:128929642><CT>"',
                ],
            ],

            /**
             * 闪光弹致盲：（这里要记录被闪到的人恢复正常的时间和闪他的人（自杀判定助攻时候用），裁判和未定义通过正则滤掉）
             * 事件类型：主事件（时间、闪光弹致盲、被闪者选手昵称、被闪者ping、被闪者Steam ID、被闪者阵营、被闪时长、投闪者选手昵称、投闪者ping、投闪者Steam ID、投闪者阵营、闪光弹参数）
             * 输出形式：socket frames（这个前端实现可能有点难）
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" blinded for\s(.*)\sby\s"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\sfrom flashbang entindex\s(\d+)\s$/i',
                'event_type' => self::EVENT_TYPE_FLASH_Blindness,
                'deal_func' => function ($regMatchAll,$line) {
                    $assistsPlayerCamp = @$regMatchAll[4][0];
                    $assistsPlayerCamp = $assistsPlayerCamp == "CT" ? "ct" : "terrorist";
                    $victimPlayerCamp = @$regMatchAll[8][0];
                    $victimPlayerCamp = $victimPlayerCamp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'roundEventType' => self::EVENT_TYPE_FLASHASSIST,
                        'assistsPlayerNickName' => @$regMatchAll[1][0],
                        'assistsPlayerPing' => @$regMatchAll[2][0],
                        'assistsPlayerCamp' => @$assistsPlayerCamp,
                        'assistsSteamId' => @$regMatchAll[3][0],
                        'flash_time' => @$regMatchAll[5][0],
                        'victimPlayerNickName' => @$regMatchAll[6][0],
                        'victimPlayerPing' => @$regMatchAll[7][0],
                        'victimPlayerCamp' => @$victimPlayerCamp,
                        'victimSteamId' => @$regMatchAll[8][0],
                        'flashParameter' => @$regMatchAll[9][0],
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:06:33.000 - "hallzerk<28><STEAM_1:0:50050791><CT>" blinded for 0.25 by "hallzerk<28><STEAM_1:0:50050791><CT>" from flashbang entindex 314 ',
                ],
            ],
            /**
             * 拆除炸弹
             * 事件类型：主事件（时间、拆除炸弹、拆除炸弹者昵称、拆除炸弹者ping、拆除炸弹者Steam ID、拆除炸弹者阵营）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\striggered "Defused_The_Bomb"$/i',
                'event_type' => self::EVENT_TYPE_BOMB_DEFUSED,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
                        'playID'=> @$regMatchAll[3][0],
                        'playerName' => @$regMatchAll[1][0],
                        'camp' => @$camp,
                        'roundEventType' => self::EVENT_TYPE_BOMB_DEFUSED,
                        'round_side' => @$camp,
                        'ping' => @$regMatchAll[2][0],
                    ];
                },
                'test_lines' => [
                    '06/25/2020 - 17:20:40.000 - "xiaaGe<10><STEAM_1:1:112788938><CT>" triggered "Defused_The_Bomb"',
                ],
            ],
            /**
             * 离开购买区域：（这里要更新选手身上的道具装备）
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、离开购买区、身上的武器道具)
             * 输出形式：只记录不输出
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\sleft buyzone with \[\s?(.*)\s?\]$/i',
                'event_type' => self::EVENT_TYPE_LEFT_BUY_ZONE_WITH,
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    $arms = @trim($regMatchAll[5][0]);
                    $arms = explode(' ',$arms);
                    $arms  = array_filter($arms);
                    $detail = [
                        'player_camp' => $camp,
                        'arms' => $arms,
                    ];
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'playerPing' => @$regMatchAll[2][0],
                        'playID' => @$regMatchAll[3][0],
                        'detail' => $detail,
                        'camp' => $camp,
                    ];
                },
                'test_lines' => [
                    '08/09/2020 - 11:44:08.235 - "CJ-O-OB<"BC"><80><STEAM_1:1:198950207><CT>" left buyzone with [ weapon_knife weapon_usp_silencer weapon_awp kevlar(100) ]',
                    '06/25/2020 - 17:01:01.000 - "splash - ske<5><STEAM_1:0:21800><CT>" left buyzone with [ ]',
                ]
            ],
            /**
             * 暂停开启
             * 事件类型：主事件（时间、对局暂停）
             * 输出形式：socket frames、socket events
             */
            [
                'type' => 'reg',
                'reg' => '/^Match pause is enabled - (.*)$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_PAUSE,
                'test_lines' => [
                    '06/25/2020 - 17:09:18.000 - Match pause is enabled - mp_pause_match',
                    '06/25/2020 - 17:09:18.000 - Match pause is enabled - sv_matchpause_auto_5v5',
                    '06/25/2020 - 17:09:18.000 - Match pause is enabled - mp_backup_restore_load_autopause',
                ],
            ],
            /**
             * 暂停结束：（这里需要做处理，如果后续有人离开购买区域，也算作暂停结束，因为有暂停失败的情况）
             * 事件类型：主事件（时间、对局暂停结束）
             * 输出形式：socket frames、socket events
             */
            [
                'type' => 'reg',
                'reg' => '/^Match pause is disabled - (.*)$/i',
                'event_type' => self::EVENT_TYPE_BATTLE_UNPAUSE,
                'test_lines' => [
                    '06/25/2020 - 17:10:32.000 - Match pause is disabled - mp_unpause_match',
                ],
            ],
            /**
             * 回合结束方式：（平局）
             * 事件类型：子事件（时间、平局、比分）
             * 所属主事件：回合结束
             * 输出形式：跟随主事件输出
             */
            [
                'type' => 'reg',
                'reg' => '/^World triggered "SFUI_Notice_Round_Draw"\s\(CT "(\d+)"\) \(T\s"(\d+)"\)$/i',
                'event_type' => self::EVENT_TYPE_ROUND_DRAW,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - World triggered "SFUI_Notice_Round_Draw" (CT "3") (T "10")',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    return [
                        'ct_score' => @$regMatchAll[1][0],
                        't_score' => @$regMatchAll[2][0],
                    ];
                }
            ],
            /**
             * 冻结时间开始：（初始化回合选手回合统计数据和HP（身上的装备道具、金钱不初始化））
             * 事件类型：主事件（时间、冻结时间开始
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^Starting Freeze period$/i',
                'event_type' => self::EVENT_TYPE_ROUND_TIME_FROZEN_START,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - Starting Freeze period',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    return [
                        'ct_score' => @$regMatchAll[1][0],
                        't_score' => @$regMatchAll[2][0],
                    ];
                }
            ],
            /**
             * 获得C4
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、获得C4）
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" triggered "Got_The_Bomb"$/i',
                'event_type' => self::EVENT_TYPE_GET_C4,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "spielz<174><STEAM_1:0:124605938><TERRORIST>" triggered "Got_The_Bomb"',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,

                    ];
                }
            ],
            /**
             * 失去C4
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、失去C4）
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" triggered "Dropped_The_Bomb"$/i',
                'event_type' => self::EVENT_TYPE_LOST_C4,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "spielz<174><STEAM_1:0:124605938><TERRORIST>" triggered "Dropped_The_Bomb"',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    return [
//                       $regMatchAll,
                       'playName'=>$regMatchAll[1][0],
                       'ping'=>$regMatchAll[2][0],
                       'steamId'=>$regMatchAll[3][0],
                       'side'=>strtolower($regMatchAll[4][0]),
                    ];
                }
            ],
            /**
             * 获得物品：（5E没有这个日志）
             * 事件类型：主事件、子事件（时间、选手昵称、Ping、Steam ID、阵营、获得物品）
             * 所属主事件：购买物品
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" picked up "(\w+)"$/i',
                'event_type' => self::EVENT_TYPE_GET_GOODS,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "friberg<35><STEAM_1:1:12147600><CT>" picked up "knife"',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,
                        'goods' => @$regMatchAll[5][0],
                    ];
                }
            ],
            /**
             * 购买物品
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、购买物品）
             * 子事件：获得物品（时间、选手昵称、Ping、Steam ID、阵营、物品）（如果存在子事件，则不处理购买物品主事件的武器装备新增）
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" purchased "(\w+)"$/i',
                'event_type' => self::EVENT_TYPE_PURCHASE_GOODS,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "dobuMergen^<181><STEAM_1:1:79945956><CT>" purchased "item_kevlar"',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,
                        'goods' => @$regMatchAll[5][0],
                    ];
                }
            ],
            /**
             * 失去物品：（5E没有这个日志，和throw会重复，并且不在同一秒，需要处理方案）
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、失去物品）
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT|Unassigned|Spectator)>" dropped "(\w+)"$/i',
                'event_type' => self::EVENT_TYPE_LOST_GOODS,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "friberg<35><STEAM_1:1:12147600><CT>" dropped "hegrenade"',
                    '08/08/2020 - 13:38:44.128 - "friberg<35><STEAM_1:1:12147600><Unassigned>" dropped "hegrenade"',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,
                        'goods' => @$regMatchAll[5][0],
                    ];
                }
            ],
            /**
             * 投掷物品
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、投掷物品、投掷物落地的坐标、闪光弹参数（不一定存在））
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" threw\s(.*)\s\[(.*)\]\s?.*?\s?(\d+)?\)?$/i',
                'event_type' => self::EVENT_TYPE_THROW_GOODS,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "NOPEEj<34><STEAM_1:1:100264490><CT>" threw flashbang [1337 1301 473] flashbang entindex 421)',
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,
                        'goods' => @$regMatchAll[5][0],
                        'coordinate' => @$regMatchAll[6][0],
                        'flash_parameter' => @$regMatchAll[7][0],
                    ];
                }
            ],
            /**
             * 金钱变化：（5E没有这个日志）
             * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、变化前的金钱、增/减、金钱变化量、变化后的金钱、购买的武器道具（不一定存在））
             * 输出形式：socket frames
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>" money change (\d+)(\+|\-)(\d+)\s\=\s\$(\d+)\s\(tracked\)\s?\(?p?u?r?c?h?a?s?e?\:?\s?(\w+)?\)?$/i',
                'event_type' => self::EVENT_TYPE_MONEY_CHANGE,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "RIZZ<8><STEAM_1:0:15915335><TERRORIST>" money change 44100-2700 = $41400 (tracked) (purchase: weapon_ak47)',//购买物品金钱变化
                    '08/08/2020 - 13:38:44.128 - "pr🥺<7><STEAM_1:0:74664970><TERRORIST>" money change 150+300 = $450 (tracked)',//安放C4金钱变化
                    '08/08/2020 - 13:38:44.128 - "foxL<6><STEAM_1:0:969768><TERRORIST>" money change 2200+3500 = $5700 (tracked)',//回合结束金钱变化
                    '08/08/2020 - 13:38:44.128 - "NOPEEj<9><STEAM_1:1:100264490><TERRORIST>" money change 11550+300 = $11850 (tracked)',//杀人金钱变化（杀队友是扣钱）
                ],
                'deal_func' => function ($regMatchAll,$line) {
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'ping' => @$regMatchAll[2][0],
                        'SteamID' => @$regMatchAll[3][0],
                        'player_camp' => $camp,
                        'last_money' => @$regMatchAll[5][0],
                        'symbol' => @$regMatchAll[6][0],
                        'change_money' => @$regMatchAll[7][0],
                        'now_money' => @$regMatchAll[8][0],
                        'goods' => @$regMatchAll[9][0],
                    ];
                }
            ],
            /**
             * 自杀：（这里要更新死亡的选手身上的道具装备为空，如果是手雷自杀、火自杀、摔死自杀，需要根据之前受到伤害的血量判断是否有助攻，根据是否恢复了被闪状态判断是否有闪光弹助攻，有助攻就不算闪光弹助攻）
             *      事件类型：主事件（自杀、自杀者昵称、自杀者ping、自杀者Steam ID、自杀者阵营、自杀者坐标、自杀使用的武器（world有多种情况）（如果有没子事件，则是摔死自杀））
             * 子事件：C4炸弹炸死（如果有C4炸弹炸死子事件，则不是摔死自杀）
             * 子事件：选手更换阵营（时间、选手昵称、Ping、Steam ID、更换前阵营、更换后阵营）
             * 输出形式：socket frames、socket events、rest api详情、rest api events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\scommitted suicide with "(.*)"$/i',
                'event_type' => self::EVENT_TYPE_SUICIDE,
                'deal_func' => function($regMatchAll,$line){
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
                        'playerName' => @$regMatchAll[1][0],
                        'playID' => @$regMatchAll[3][0],
                        'player_camp' => @$camp,
                        'coordinate' => @$regMatchAll[5][0],
                        'with' => @$regMatchAll[6][0],
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "Yoona<2><STEAM_1:1:5307647><TERRORIST>" [1003 1201 65] committed suicide with "hegrenade"',
                ]
            ],
            /**
             * C4炸死：
             * 事件类型：子事件（C4炸死、被炸死者昵称、被炸死者ping、被炸死者Steam ID、被炸死者阵营、被炸死者坐标）
             * 所属主事件：自杀
             * 输出形式：随主事件一起输出
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT)>"\s\[(.*)\]\swas killed by the bomb.$/i',
                'event_type' => self::EVENT_TYPE_C4_KILL,
                'deal_func' => function($regMatchAll,$line){
                    $camp = @$regMatchAll[4][0];
                    $camp = $camp == "CT" ? "ct" : "terrorist";
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'playerPing' => @$regMatchAll[2][0],
                        'playID' => @$regMatchAll[3][0],
                        'player_camp' => @$camp,
                        'coordinate' => @$regMatchAll[5][0],
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "laNgod<24><STEAM_1:0:91801291><TERRORIST>" [58 -453 177] was killed by the bomb.',
                ]
            ],
            /**
             * 退出游戏：
             * 事件类型：主事件（退出游戏、退出游戏者昵称、退出游戏者ping、退出游戏者Steam ID、退出游戏者阵营、退出游戏者坐标、原因）
             * 输出形式：socket events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><(TERRORIST|CT|Unassigned|Spectator)>" disconnected \(reason "(.*)"\)$/i',
                'event_type' => self::EVENT_TYPE_PLAYER_QUIT,
                'deal_func' => function($regMatchAll,$line){
                    $camp = @$regMatchAll[4][0];
                    return [
//                        $regMatchAll,
                        'playerName' => @$regMatchAll[1][0],
                        'playID' => @$regMatchAll[3][0],
                        'ping' => @$regMatchAll[2][0],
                        'player_camp' => strtolower(@$camp),
                        'reason' => @$regMatchAll[5][0],
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "westmelon<70><STEAM_1:0:73487156><TERRORIST>" disconnected (reason "Disconnect")',
                ]
            ],
            /**
             * 加入游戏：
             * 事件类型：主事件（进入游戏、昵称、ping、Steam ID、阵营（空，选了阵营之后才会有阵营，现在是未定义））
             * 输出形式：socket events
             */
            [
                'type' => 'reg',
                'reg' => '/^"(.*)<(\d+)><(.*)><.*>"\sentered the game$/i',
                'event_type' => self::EVENT_TYPE_PLAYER_JOINED,
                'deal_func' => function($regMatchAll,$line){
                    $camp = @$regMatchAll[4][0];
                    return [
                        'playerName' => @$regMatchAll[1][0],
                        'playID' => @$regMatchAll[3][0],
                        'ping' => @$regMatchAll[2][0],
                        'side' => strtolower($camp),
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "GeT_RiGhT<16><STEAM_1:0:10885595><>" entered the game',
                ]
            ],
            /**
             * 回档位置：（没有回档位置则回档至上一回合结束，这里需要读取回档后的选手武器道具、金钱）
             * 事件类型：子事件（时间、回档至多少回合）
             * 所属主事件：回档
             * 输出形式：只记录不输出
             */
            [
                'type' => 'reg',
                'reg' => '/^rcon from ".*command "mp_backup_restore_load_file\s.*round(\d+).txt"$/i',
                'event_type' => self::EVENT_TYPE_ROUND_TO,
                'deal_func' => function($regMatchAll,$line){
                    return [
                        'round_to' => @$regMatchAll[1][0],
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:38670": command "mp_backup_restore_load_file ebot_1863_round13.txt"',
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:40812": command "mp_backup_restore_load_file ebot_1896_round13.txt"'
                ]
            ],
            /**
             * LOG开始：（log_started）
             * 事件类型：主事件（LOG开始、日志目录及名称、游戏目录、游戏版本）
             * 输出形式：只记录不输出
             */
            [
                'type' => 'reg',
                'reg' => '/^Log file started \(file "(.*)"\)\s\((.*)\)\s\(version\s"(.*)"\)$/i',
                'event_type' => self::EVENT_TYPE_LOG_STARTED,
                'deal_func' => function($regMatchAll,$line){
                    return [
                        'LogDirectoryAndName' => @$regMatchAll[1][0],//日志目录及名称
                        'gameDirectory' => @$regMatchAll[2][0],//游戏目录
                        'gameVersion' => @$regMatchAll[3][0],//游戏目录
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - Log file started (file "logs/L192_168_010_121_27015_202007161546_002.log") (game "/home/csgoserver/csgo") (version "7886")',
                    '08/08/2020 - 13:38:44.128 - Log file started (file "logfiles\L068_232_164_055_27015_202008101617_000.log") (game "c:\games\68_232_164_55_27015\1446465\csgo\csgo") (version "7929")',
                    '08/08/2020 - 13:38:44.128 - Log file started (file "logfiles/L085_195_101_115_20002_202008081333_002.log") (game "/home/hz00014/csgo/csgo") (version "7929")',
                    '08/08/2020 - 13:38:44.128 - Log file started (file "logs/L000_000_000_000_27015_202011291532_000.log") (game "/home/csgoserver1/csgo") (version "8012")',

                ]
            ],
            /**
             * LOG关闭：（log_closed）
             * 事件类型：主事件（LOG结束）
             * 输出形式：只记录不输出
             */
            [
                'type'       => 'reg',
                'reg'        => '/^Log file closed$/i',
                'event_type' => self::EVENT_TYPE_LOG_CLOSED,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - Log file closed',
                ]
            ],
            /**
             * 服务器配置初始化
             * 事件类型：主事件（服务器配置初始化）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^server cvars start$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CVARS_INIT,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - server cvars start',
                ]
            ],
            /**
             * 赋值 (服务器配置初始化)
             * 事件类型：主事件（服务器配置初始化赋值）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^"(.*)"\s=\s"(.*)"$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CVARS_ASSIGN,
                'deal_func'  => function ($regMatchAll, $line) {
                    return [
//                        $regMatchAll,
                        'configKey' => @$regMatchAll[1][0],//赋值选项
                        'configValue' => @$regMatchAll[2][0],//值
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - "mp_maxrounds" = "30"',
                    '08/08/2020 - 13:38:44.128 - "mp_roundtime" = "1.92"',
                ]
            ],
            /**
             * 结束：（服务器配置初始化结束）
             * 事件类型：主事件（服务器配置初始化结束）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^server cvars end$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CVARS_END,
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - server cvars end',
                ]
            ],
            /**
             * 服务器配置修改：server_changed_cvars（增量更新）
             * 事件类型：主事件（服务器配置修改-配置文件）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^server_cvar: "(.*)"\s"(.*)"$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CHANGED_CVARS,
                'deal_func'  => function ($regMatchAll, $line) {
                    return [
                        'configKey' => @$regMatchAll[1][0],//赋值选项
                        'configValue' => @$regMatchAll[2][0],//值
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - server_cvar: "mp_freezetime" "6"',
                    '08/08/2020 - 13:38:44.128 - server_cvar: "mp_maxrounds" "30"',
                ]
            ],
            /**
             * 服务器配置修改：server_changed_console  控制台（可能是单条，可能是多条命令，需要用分号分割，然后再把key和value按照空格分割，有的有多个value，带引号的字符串类型的要去掉引号？，然后再保存）（增量更新）
             * 事件类型：主事件（服务器配置修改-控制台）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^rcon from\s"(.*)":\scommand\s"((?!status).*)"$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CHANGED_CONSOLE,
                'deal_func'  => function ($regMatchAll, $line) {
                    return [
                        'ipAndPort' => @$regMatchAll[1][0],//ip和端口号
                        'configContent' => @$regMatchAll[2][0],//配置内容
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:38670": command "csay_all "eBot: Streamers are not ready yet!""',
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:59364": command "mp_maxrounds 30"',
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:59364": command "mp_roundtime 1.92"',
//                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:59364": command "status"',
                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:59364": command "mp_warmuptime 3600; mp_warmup_pausetimer 1; mp_maxmoney 60000; mp_startmoney 60000; mp_free_armor 1; mp_warmup_start"',
                ]
            ],


            /**
             * 对局重开：（battle_restart）
             * 事件类型：主事件（重开）
             * 输出形式：socket events
             */
            [
                'type'       => 'reg',
                'reg'        => '/^World triggered "Restart_Round_\((\d+)_seconds?\)"$/i',
                'event_type' => self::EVENT_TYPE_SERVER_BATTLE_RESTART,
                'deal_func'  => function ($regMatchAll, $line) {
                    return [
                        'second' => @$regMatchAll[1][0],//秒
                    ];
                },
                'test_lines' => [
                    '08/08/2020 - 13:38:44.128 - World triggered "Restart_Round_(3_seconds)"',
                ]
            ],

        ];
        return $eventMapReg;
    }
    public static function getEventStatusMatchReg()
    {
        $eventMapReg = [
            /**
             * 服务器配置修改：server_changed_console  控制台（可能是单条，可能是多条命令，需要用分号分割，然后再把key和value按照空格分割，有的有多个value，带引号的字符串类型的要去掉引号？，然后再保存）（增量更新）
             * 事件类型：主事件（服务器配置修改-控制台）
             * 输出形式：socket frames
             */
            [
                'type'       => 'reg',
                'reg'        => '/^rcon from\s"(.*)":\scommand\s"status"$/i',
                'event_type' => self::EVENT_TYPE_SERVER_CHANGED_CONSOLE,
                'deal_func'  => function ($regMatchAll, $line) {
                    return [
                        'ipAndPort' => @$regMatchAll[1][0],//ip和端口号
                        'configContent' => @$regMatchAll[2][0],//配置内容
                    ];
                },
                'test_lines' => [
//                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:38670": command "csay_all "eBot: Streamers are not ready yet!""',

                    '08/08/2020 - 13:38:44.128 - rcon from "62.141.46.157:59364": command "status"',
                ]
            ],
       ];
        return $eventMapReg;
    }

    public static function dealWithRecord($record)
    {

        $log = $record['log'];
        $listLines = explode("\n", $log);
        $eventListTimeKey = [];
        $eventSwitchListTimeKey = [];
        // 获取配置信息，这里用来做转换效果
        $regMatch = self::getEventMapReg();
        $regStatusMatch = self::getEventStatusMatchReg();
        $i=0;
        foreach ($listLines as $v) {
            $v=trim($v,"\r");
            $status_event = self::formatEventByLineAndMapConfig($v, $regStatusMatch);
            if ($status_event){
                continue;
            }
            $event = self::formatEventByLineAndMapConfig($v, $regMatch);
            if ($event) {
                if ($event['log_event_type'] == self::EVENT_TYPE_PLAYER_SWITCH_TEAM) {
//                    $eventList[] = $event;

                    $log_time = $event['log_time'];
                    $eventListTimeKey[$event['log_time']][] = $event;
                    if (($event['afterSide'] == 'ct' || $event['afterSide'] == 'terrorist') && ($event['beforeSide'] == 'ct' || $event['beforeSide'] == 'terrorist')) {
                        $i++;
                    }
//
                } else {
                    $eventList[] = $event;
                    $eventListTimeKey[$event['log_time']][] = $event;
                }

            }

        }
        //server_cvars_init 把server_cvars_assign里面的内容合并到里面
        if (!empty($eventListTimeKey)) {
            foreach ($eventListTimeKey as $k => $v) {
                $serverCvarsInitTrue    = false;
                $serverCvarsAssignTrue  = false;
                $serverCvarsEndTrue     = false;
                $serverCvarsAssignArray = [];
                $serverCvarsInitKey     = '';
                foreach ($v as $k1 => $v1) {
                    if ($v1['event_type'] == 'server_cvars_init') {
                        $serverCvarsInitTrue = true;
                        $serverCvarsInitKey  = $k1;
                    }
                    if ($v1['event_type'] == 'server_cvars_assign') {
                        $serverCvarsAssignTrue                    = true;
                        $serverCvarsAssignArray[$v1['configKey']] = $v1['configValue'];
                        unset($eventListTimeKey[$k][$k1]);
                    }
                    if ($v1['event_type'] == 'server_cvars_end') {
                        $serverCvarsEndTrue = true;
                        unset($eventListTimeKey[$k][$k1]);
                    }
                    if ($v1['event_type'] == 'server_changed_console') {
                        if ($v1['configContent']=="status"){
                            unset($eventListTimeKey[$k][$k1]);
                        }
                    }
                }
                if ($serverCvarsInitTrue && $serverCvarsAssignTrue && $serverCvarsEndTrue) {
                    $eventListTimeKey[$k][$serverCvarsInitKey]['configContent'] = $serverCvarsAssignArray;
                }
            }
        }
        if ($i>=7&&$i<=10){

            // 记录一条换边的日志
            $tmpEvent = [
                'log_event_type' => self::EVENT_TYPE_BATTLE_SWITCH_TEAM,
                'event_type' => self::EVENT_TYPE_BATTLE_SWITCH_TEAM,
                'log_time' => $log_time
            ];

            foreach ($eventListTimeKey[$log_time] as $k=>$v){
                if ($v['event_type']=='round_time_frozen_start'){
                    unset($eventListTimeKey[$log_time][$k]);
                    array_unshift($eventListTimeKey[$log_time],$v);
                }
            }
            array_unshift($eventListTimeKey[$log_time],$tmpEvent);


        }


//        foreach ($eventSwitchListTimeKey as $sk => $sEventList) {
//            if (count($sEventList) > 8) {
//                // 记录一条换边的日志
//                $tmpEvent = [
//                    'log_event_type' => self::EVENT_TYPE_BATTLE_SWITCH_TEAM,
//                    'log_time' => $sEventList[1]['log_time']
//                ];
//                // todo 检查换边和时间
////                $eventListTimeKey[$sk][] = $sEventList;
//                $eventListTimeKey[$sk][] = $tmpEvent;
//            }
//        }
        $timeMergeEventList = self::mergeEvent($eventListTimeKey,self::getEventMergeMapping());
        $mergeEventList = [];
        foreach ($timeMergeEventList as $vEventList) {
            foreach ($vEventList as $event) {
                $mergeEventList[] = $event;
            }
        }
        // 把合并之后的事件推入到新表中，并且标志本行处理状态
        foreach ($mergeEventList as $key => $val) {
            $formatEvent = new ReceiveData5eplayFormatEvents();
            $lineInfo['created_at'] = isset($lineInfo['created_at']) ? $lineInfo['created_at'] : "";
            $formatEvent->setAttributes(
                [
                    'event_type' => $val['log_event_type'],
                    'log_type' => 2,
                    'parent_id' => $record['id'],
                    'info' => json_encode($val),
                    'ip_address' => $record['ip_address'],
                    'created_at' => $record['created_at'],
                ]
            );

            $save_res = $formatEvent->save();
            if ($save_res) {
                $event_id = $formatEvent['id'];
                $redis_server = 'order_csgolog_'.$record['ip_address'];
                $taskType = '9002';
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_EVENT_WS,
                        "5eplay", "", "", ""),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => [
                        'event_type' => $val['log_event_type'],
                        'log_type'   => 2,
                        'parent_id'  => $record['id'],
                        'info'       => json_encode($val),
                        'ip_address' => $record['ip_address'],
                        'created_at' => $record['created_at'],
                        'event_id' => $event_id,
                    ],
                    "redis_server"=>$redis_server
                ];
                TaskRunner::addTask($item, $taskType);
            } else {
                throw new BusinessException([], '没有这条记录');
            }
        }
        // 修改状态
        \app\modules\receive\models\ReceiveData5eplay::updateAll(['is_handle' => 1], ['id' => $record['id']]);
    }

    public static function getEventMergeMapping()
    {
        //这里把 get_goods lost_goods round_time_frozen_start 作为battle_start的子事件
        //为了初始化battle开始的第一局的装备
        $mapping = $eventAddingMapping = [
            // 闪光弹助攻
            self::EVENT_TYPE_FLASHASSIST            => [
                'main_event' => self::EVENT_TYPE_PLAYER_KILL,
            ],
            // 助攻
            self::EVENT_TYPE_ASSIST                 => [
                'main_event' => self::EVENT_TYPE_PLAYER_KILL,
            ],
            // 获胜方式,这里有比分
            self::EVENT_TYPE_ROUND_WIN_TYPE         => [
                'main_event' => [
                    self::EVENT_TYPE_ROUND_END,
                    self::EVENT_TYPE_BATTLE_END,
                ]
            ],
            // 重新加载
            self::EVENT_TYPE_BATTLE_RELOADED        => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            // 平局
            self::EVENT_TYPE_ROUND_DRAW             => [
                'main_event' => self::EVENT_TYPE_ROUND_END,
            ],
            //选择阵营为反恐精英
            self::EVENT_TYPE_BATTLE_TEAM_PLAYING_CT => [
                'main_event' => [
                    self::EVENT_TYPE_BATTLE_START,
                    self::EVENT_TYPE_BATTLE_RELOADED,
                    self::EVENT_TYPE_SUICIDE
                ],
            ],
            //选择阵营为恐怖分子
            self::EVENT_TYPE_BATTLE_TEAM_PLAYING_T  => [
                'main_event' => [
                    self::EVENT_TYPE_BATTLE_RELOADED,
                    self::EVENT_TYPE_BATTLE_START,
                    self::EVENT_TYPE_SUICIDE
                ]
            ],
            //攻击
            self::EVENT_TYPE_ATTACKED               => [
                'main_event' => self::EVENT_TYPE_PLAYER_KILL,
            ],
            //回档位置
            self::EVENT_TYPE_ROUND_TO               => [
                'main_event' => self::EVENT_TYPE_BATTLE_RELOADED,
            ],
            //获得物品
            self::EVENT_TYPE_GET_GOODS              => [
                'main_event' => [
                    self::EVENT_TYPE_PURCHASE_GOODS,
                    self::EVENT_TYPE_BATTLE_START,
                ],
            ],
            //失去物品
            self::EVENT_TYPE_LOST_GOODS             => [
                'main_event' => [
                    self::EVENT_TYPE_BATTLE_START,
                ],
            ],
            //冻结时间开始
            self::EVENT_TYPE_ROUND_TIME_FROZEN_START             => [
                'main_event' => [
                    self::EVENT_TYPE_BATTLE_START,
                ],
            ],
            //C4炸死
            self::EVENT_TYPE_C4_KILL                => [
                'main_event' => self::EVENT_TYPE_SUICIDE,
            ],
            //EVENT_TYPE_BATTLE_GAME_COMMENCING
            self::EVENT_TYPE_BATTLE_GAME_COMMENCING                => [
                'main_event' => self::EVENT_TYPE_BATTLE_START,
            ],

        ];
        return $mapping;
    }
}