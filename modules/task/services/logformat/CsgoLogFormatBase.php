<?php


namespace app\modules\task\services\logformat;


use app\modules\data\models\ReceiveDataFunspark;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\rest\exceptions\BusinessException;

class CsgoLogFormatBase
{

    //battle
    const EVENT_TYPE_BATTLE_UP_COMING = "battle_up_coming";//funspark数据源比赛开始
    const EVENT_TYPE_BATTLE_GAME_COMMENCING = "battle_game_commencing";//
    const EVENT_TYPE_BATTLE_START = "battle_start";//对局开始（关键信息：时间√、对局开始√、地图√、起始阵营（子事件×）√）
    const EVENT_TYPE_BATTLE_BINDING_CURRENT_IDENTIFICATION = "battle_binding_current_identification";//绑定当前对局标识
    const EVENT_TYPE_ROUND_TIME_FROZEN_START = "round_time_frozen_start";//冻结时间开始：（初始化回合选手回合统计数据和HP（身上的装备道具、金钱不初始化））
    const EVENT_TYPE_BATTLE_TEAM_PLAYING_CT = "battle_team_playing_ct";//起始为反恐精英的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_BATTLE_TEAM_PLAYING_T = "battle_team_playing_t";//起始为恐怖分子的战队信息（关键信息：起始阵营√）
    const EVENT_TYPE_BATTLE_END = "battle_end";//对局结束（应该用World triggered \"Match_End\"更为准确）（关键信息：时间√、对局结束√、地图（子事件×）×、结束时比分（子事件×）×、比赛时长（子事件×）×）
    const EVENT_TYPE_BATTLE_SWITCH_TEAM = "battle_switch_team";//常规换边（关键信息：战队当前阵营更换√）
    //Match_SwitchTeamOvertime//加时换边（关键信息：战队当前阵营更换×）

    //round
    const EVENT_TYPE_ROUND_START = "round_start";//回合开始（关键信息：时间√、回合开始√）
    const EVENT_TYPE_ROUND_END = "round_end";//回合结束（关键信息：时间√、回合结束√、获胜方式（子事件√）√、回合结束后的当前比分（子事件√）×、存活人员（子事件√）×、存活人数（子事件√）×）
    const EVENT_TYPE_ROUND_WIN_TYPE = 'round_win_type';//获胜类型
    const EVENT_TYPE_TERRORISTS_WIN = 'terrorists_win';//反恐精英全部阵亡（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_CTS_WIN = 'cts_win';//恐怖分子被全歼（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_TARGET_BOMBED = 'target_bombed';//炸弹爆炸（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_TARGET_SAVED = 'target_saved';//时间耗尽（关键信息：获胜方式、回合结束后当前比分）
    const EVENT_TYPE_ROUND_ALIVE_PLAYERS = 'round_alive_players';//存活人员（关键信息：存活人员、存活人数）
    const EVENT_TYPE_LEFT_BUY_ZONE_WITH = "left_buy_zone_with";//离开购买区（5E：应该用Starting Freeze period初始化更准确）（关键信息：时间√、回合统计数据初始化√）
    const EVENT_TYPE_PLAYER_KILL = "player_kill";//击杀（关键信息：时间√、杀手信息√、受害者信息√、武器√、是否爆头√、致死伤害（子事件√）×、命中部位（子事件√）×、助攻者信息（子事件√）×、闪光弹助攻者信息（子事件√）×、）
    const EVENT_TYPE_CHICKEN_KILL = "chicken_kill";//杀鸡
    const EVENT_TYPE_ATTACKED = "attacked";//攻击（关键信息：致死伤害、命中部位）
    const EVENT_TYPE_ASSIST = "assist";//助攻（关键信息：助攻者信息）
    const EVENT_TYPE_FLASHASSIST = "flashassist";//闪光弹助攻（关键信息：闪光弹助攻者信息）

    const EVENT_TYPE_BOMB_PLANTED = "bomb_planted";//安放炸弹（关键信息：安放炸弹、安放炸弹的选手）
    const EVENT_TYPE_BOMB_DEFUSED = "bomb_defused";//拆除炸弹（关键信息：拆除炸弹、拆除炸弹的选手）
    const EVENT_TYPE_GET_C4 = "get_c4";//获得C4
    const EVENT_TYPE_LOST_C4 = "lost_c4";//失去C4
    const EVENT_TYPE_GET_GOODS = "get_goods";//获得物品
    const EVENT_TYPE_PURCHASE_GOODS = "purchase_goods";//购买物品
    const EVENT_TYPE_LOST_GOODS = "lost_goods";//失去物品
    const EVENT_TYPE_THROW_GOODS = "throw_goods";//投掷物品
    const EVENT_TYPE_MONEY_CHANGE = "money_change";//金钱变化
    const EVENT_TYPE_FLASH_Blindness = "flash_blindness";//闪光弹致盲：（这里要记录被闪到的人恢复正常的时间和闪他的人（自杀判定助攻时候用），裁判和未定义通过正则滤掉）
    const EVENT_TYPE_C4_KILL = "c4_kill";//c4炸死
    // 特殊事件
    const EVENT_TYPE_ROUND_DRAW = "round_draw";//比赛平局
    const EVENT_TYPE_BATTLE_RELOADED = "battle_reloaded";//重新加载
    // funspark数据源用事件
    const EVENT_TYPE_PLAYER_SWITCH_TEAM = "player_switch_team";//player更换阵营
    const EVENT_TYPE_SUICIDE = 'suicide';//自杀
    const EVENT_TYPE_ROUND_TO = 'round_to';//回档位置
    const EVENT_TYPE_LOG_STARTED = 'log_started';//Log开始
    const EVENT_TYPE_LOG_CLOSED = 'log_closed';//Log关闭
    const EVENT_TYPE_SERVER_CVARS_INIT = 'server_cvars_init';//服务器配置初始化
    const EVENT_TYPE_SERVER_CVARS_ASSIGN = 'server_cvars_assign';//服务器配置赋值
    const EVENT_TYPE_SERVER_CVARS_END = 'server_cvars_end';//服务器配置结束
    const EVENT_TYPE_SERVER_CHANGED_CVARS = 'server_changed_cvars';//服务器配置修改 配置文件
    const EVENT_TYPE_SERVER_CHANGED_CONSOLE = 'server_changed_console';//服务器配置修改 控制台
    const EVENT_TYPE_SERVER_BATTLE_RESTART = 'battle_restart';//对局重开
    const EVENT_TYPE_BATTLE_REL_IDENTITY = 'battle_rel_identity';
    const EVENT_TYPE_BATTLE_PAUSE = "battle_pause";//暂停指令
    const EVENT_TYPE_BATTLE_UNPAUSE = "battle_unpause";//暂停结束
    const EVENT_TYPE_LOADING_MAP = "Loading map";//加载地图
    const EVENT_TYPE_MATCH_HAS_END = "match_has_end";//比赛结束
    const EVENT_TYPE_MAP_START = "map_start";//地图开启
    // const EVENT_TYPE_ROUND_DRAW = "SFUI_Notice_Round_Draw";//比赛平局(重复抛弃)
    const EVENT_TYPE_DETAIL_CHOOSE_TEAM_SWITCH = "detail_choose_team_switch"; //加时换边，找到之'Match_SwitchTeamOvertime'
    const EVENT_TYPE_KNIFE_ROUND_START = "knife_round_start";//刀局开始
    const EVENT_TYPE_ROUND_SCORES = 'round_score';
    const EVENT_TYPE_BATTLE_SCORES = 'battle_scores';
    const EVENT_TYPE_MP_PAUSE_MATCH = "Match pause is enabled - mp_pause_match";//地图指令比赛暂停（关键信息：暂停√）
    const EVENT_TYPE_MP_UNPAUSE_MATCH = "Match pause is disabled - mp_unpause_match";//地图指令比赛暂停取消（关键信息：取消暂停√）
    const EVENT_TYPE_PLAYER_QUIT = "player_quit";//退出游戏
    const EVENT_TYPE_PLAYER_JOINED = "player_joined";//加入游戏

    /**
     * 查询转化后的事件
     * $logBeginTime        开始时间
     * $logEndTime          结束时间
     * $ipAddress           IP地址
     * $logType             日志类型
     */
    public static function getHandleEvent($logBeginTime, $logEndTime, $ipAddress, $logType)
    {
        // $logBeginTime = '2020-07-23 16:07:35';
        // $logEndTime = '2020-07-23 17:20:00';
        // $ipAddress = '47.244.77.240';
        $find = 'info,id,parent_id';
        // 约束条件
        $map = ['>=', 'created_at', $logBeginTime];
        $where = ['<=', 'created_at', $logEndTime];
        $ip = ['in', 'ip_address', explode(';',$ipAddress)];
        $logType = ['=', 'log_type', $logType];
        $q = ReceiveData5eplayFormatEvents::find()
            ->select($find)
            ->orderBy('parent_id asc,id asc')
            ->where($map)
            ->andWhere($ip)
            ->andWhere($where)
            ->andWhere($logType)
            ->asArray();
        $sql = $q->createCommand()->getRawSql();
        $data = $q->all();
        $info = [];
        foreach ($data as $key => &$value) {
            $tmp = json_decode($value['info'], true);
            $tmp['log_event_id'] = $value['id'];
            $tmp['log_event_parent_id'] = $value['parent_id'];
            $info[] = $tmp;
        }
        return $info;
    }
    public static function getReceiveDataFunsparkListAndDoByTime($s_time, $e_time, $is_handle = 2, $ipAddress)
    {
        // 这里循环整个表 按照顺序取出待处理的数据
        $where = [
            'and',
            ['=', 'is_handle', $is_handle],
            ['>=', 'created_at', $s_time],
            ['<=', 'created_at', $e_time],
            ['=', 'ip_address', $ipAddress]
        ];
        $q = ReceiveDataFunspark::find()
            ->orderBy('id')
            ->where($where);
        $perPage = 1000;
        $count = $q->count();
        for ($page = 1; $page <= $count / $perPage + 1; $page++) {
            $list = $q->limit($perPage)->offset(0)
                ->asArray()->all();
            $sql = $q->createCommand()->getRawSql();
            if ($list) {
                foreach ($list as $line) {
                    self::dealWithLine($line);
                }
            } else {
                break;
            }
        }
    }

    public static function mergeEvent($eventListTimeKey,$eventAddingMapping)
    {
        foreach ($eventListTimeKey as $key => $timeDetail) {
            // 单位事件内的所有事件
            if (count($timeDetail) <= 1) {
                continue;
            }
            $tmpTimeDetail = $timeDetail;
            foreach ($timeDetail as $event) {
                // 如果是子事件
                if (isset($eventAddingMapping[$event['log_event_type']])) {
                    if (is_array($eventAddingMapping[$event['log_event_type']]['main_event'])) {
                        // 循环配置里面的主事件列表，一个子事件会属于多个主事件
                        foreach ($eventAddingMapping[$event['log_event_type']]['main_event'] as $tmpEvent) {
                            foreach ($tmpTimeDetail as &$tmpDetail) {
                                if ($tmpDetail['log_event_type'] == $tmpEvent) {
                                    $tmpDetail['ext_log'][$event['log_event_type']][] = $event;
                                }
                            }
                        }
                    } else {
                        // 遍历这个单位时间的事件列表，找到主事件，添加到['ext_log']
                        foreach ($tmpTimeDetail as &$tmpDetail) {
                            if ($tmpDetail['log_event_type'] == $eventAddingMapping[$event['log_event_type']]['main_event']) {
                                $tmpDetail['ext_log'][$event['log_event_type']][] = $event;
                            }
                        }
                    }
                }
            }
            $eventListTimeKey[$key] = $tmpTimeDetail;
        }
        return $eventListTimeKey;
    }

    public static function formatEventByLineAndMapConfig($log, $eventMap)
    {
        $event = [
            'log_time' => substr($log, 0, 25),
            'event_type' => '',
            'log_map_name' => "",
            'log_event_type' => "",
            'log_team_name' => '',
            'log_team_camp' => "",
            'log_duration' => '',
        ];
        $matchMap = $eventMap;
        $subLine = substr($log, 28);
        foreach ($matchMap as $key => $val) {
            // 截取需要获取的line
            $tmpPregTest = preg_match_all($val['reg'], $subLine, $tmpPregMatchAll);
            if ($tmpPregTest) {
                $event['event_type'] = $val['event_type'];
                $event['log_event_type'] = $val['event_type'];
                if(isset($val['deal_func'])){
                    $dealInfo = $val['deal_func']($tmpPregMatchAll, $log);
                    $event = array_merge($event, $dealInfo);
                }
                return $event;
            }
        }
        return null;
    }
}
