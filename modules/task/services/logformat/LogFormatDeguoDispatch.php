<?php
/**
 *
 */

namespace app\modules\task\services\logformat;

use app\modules\task\models\ReceiveDataFunsparkFormatEvents;

class LogFormatDeguoDispatch
{
    public static function run()
    {
         //读取日志
        $log_data = self::read('F:\phpstudy_pro\WWW\game_center_server\public\123456.log');
        $new_array = [];
        foreach ($log_data as $key => $item){
            $time = substr($item, 0, 25);
            $timeFormat = self::changeTime($time);
            $new_array[$timeFormat][]= $item;
        }
        $insert_array = $new_array;
        foreach ($insert_array as $keyi => $itemi){
            $receiveDataFunspark = new ReceiveDataFunsparkFormatEvents();
            $receiveDataFunspark->setAttributes(
                [
                    'log' => implode($itemi,"\n"),
                    'ip_address' => '43.224.44.10',
                    'created_at' => $keyi,
                    'modified_at' =>null,
                    'is_handle' => null
                ]
            );
            $res = $receiveDataFunspark->save();
        }
        return '成功啦';
    }

    public static function read($path){
        $file = fopen($path, "r");
        $filedata=array();
        $i=0;
        //输出文本中所有的行，直到文件结束为止。
        while(! feof($file))
            {
                $filedata[$i]= trim(fgets($file));//fgets()函数从文件指针中读取一行
            $i++;
        }
        fclose($file);
        $filedata=array_filter($filedata);
        return $filedata;
    }

    /**
     * 时间转换csgo专用  By  王傲渊
     * $time    要转换的时间(字符串)
     */
    public static function changeTime($time)
    {
        $time = substr($time, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }
}