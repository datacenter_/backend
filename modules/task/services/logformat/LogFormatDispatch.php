<?php
/**
 *
 */

namespace app\modules\task\services\logformat;


use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardCommon;

class LogFormatDispatch
{
    public static function run($tagInfo,$taskInfo)
    {
        $resourceType=$tagInfo["resource_type"];
        $originType=$tagInfo["origin_type"];
        $extType=$tagInfo['ext_type'];
        switch ($originType){
            case QueueServer::QUEUE_ORIGIN_FIVE://赛事
                switch ($extType){
                    case QueueServer::QUEUE_ORIGIN_FIVE_EXT_FUNSPARK:
                        $tasks= CsgoLogUCC::run($tagInfo,$taskInfo);
                        break;
                    case QueueServer::QUEUE_ORIGIN_FIVE_EXT_5E:
                        $tasks= CsgoLogFive::run($tagInfo,$taskInfo);
                        break;
                }
                break;
            default:
                throw new TaskException("no StandardIncrementDispatcher type for :".$resourceType);
        }
        return $tasks;
    }

}