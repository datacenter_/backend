<?php
/**
 *
 */

namespace app\modules\task\services\logformat;


use app\modules\data\models\ReceiveData5eplay;
use app\modules\data\models\ReceiveDataFunspark;

class LogGenerator
{
    /**
     *  这个任务会生成log，供测试使用
     * @param string $type
     * @param int $startId
     * @param int $endId
     * @param string $ipAddress
     * @param string $virtualIpAddress
     * @param int $speed
     * @param string $url
     */
    public function addInfo($type = '5eplay', $startId = 0,
                            $endId = 0, $ipAddress = '',
                            $afterIpAddress = '', $url = "http://api.gc.veryapi.com/v1/receive/receive/debug")
    {
        if (!$startId||!$endId||!$ipAddress){
            echo 'error 参数';
        }
        if ($type == '5eplay') {
            $q    = ReceiveData5eplay::find()->where(
                ["and",
                    ['>=', 'id', $startId],
                    ['<=', 'id', $endId],
                    ['ip_address' => $ipAddress],
                ])->asArray();
            $list = $q->all();
        } else {
            $q    = ReceiveDataFunspark::find()->where(
                ["and",
                    ['>=', 'id', $startId],
                    ['<=', 'id', $endId],
                    ['ip_address' => $ipAddress],
                ])->asArray();
            $list = $q->all();
        }
        if ($list) {
            foreach ($list as $v) {
                // 推送给player
                $data = [
                    'log_type'   => $type == '5eplay' ? 1 : 2,
                    'ip_address' => $afterIpAddress,
                    'log'        => $v['log'],
                ];
//                \app\modules\task\services\logformat\CsgoLogFive::dealWithRecord($v);
                $this->sentToDebug($url, $data);
            }
        }
    }

    /**
     * 过滤每一行中的"L "
     * @param $row
     * @return false|string
     */
    public function filterLog($row)
    {
        return substr($row, 0, 2) == 'L ' ? substr($row, 2,21).'.000 -'.substr($row, 24,-1) : $row;
    }

    /**
     *读取文件生成日志回放
     * @param string $areaAddress
     * @param string $file  "D:\test\logs\481\New England Whalers VS chaos Dust2.log"
     * @param string $url
     * @return bool
     */

    public function addFileInfo($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug",$speed=1,$total=100)
    {

        if (!$file) {
            return false;
        }

        if (file_exists($file)) {
            $list        = file($file);
            $info        = [];
            $new_arr = [];
            $filter_list = array_map(array(__CLASS__, 'filterLog'), $list);
            if (!empty($filter_list)) {
                foreach ($filter_list as $v) {
                    $v = trim($v, "\r");
                    $new_arr[substr($v, 0, 28)][] = $v;
                }
            }
            $offset =0;
            $count = count($new_arr);
            for ($i = 0; $i <= 99999999; $i++) {

                $filter_list_new = array_slice($new_arr,$offset,$speed);
                if (!empty($filter_list_new)) {

                }
                $offset =  $i * $speed+$speed;
                foreach ($filter_list_new as $val) {
                    $data = [
                        'log_type'   => 2,
                        'ip_address' => $areaAddress,
                        'log'        => implode("\n", $val),
                    ];
                    $this->sentToDebug($url, $data);
                }
                if ($offset>=$count){
                    exit('complete!');
                }
                echo "offset ".$offset.PHP_EOL;
                echo "speed ".$speed.PHP_EOL;
                sleep(1);
            }

        }else{
            echo 'file no exist!';
        }

    }

    public function addFileInfoFast($areaAddress = '127.0.0.1', $file = '', $url = "http://admin.gc.comingtrue.cn/v1/receive/receive/debug",$log_type=1)
    {

        if (!$file) {
            return false;
        }
        if (file_exists($file)) {
            $list        = file($file);
            $info        = [];
            $filter_list = array_map(array(__CLASS__, 'filterLog'), $list);
//            $filter_list = array_splice($filter_list,0,500);
            if (!empty($filter_list)) {
                foreach ($filter_list as $v) {
                    $info[substr($v, 0, 28)][] = $v;
                }
            }
            $count = count($info);
            $i =0;
            foreach ($info as  $val) {
                $i++;
                echo "count--".$count." k--".$i.PHP_EOL;
                $data = [
                    'log_type'   => $log_type,
                    'ip_address' => $areaAddress,
                    'log'        => implode("\n", $val),
                ];
                $this->sentToDebug($url, $data);
            }
        }else{
            echo 'file no exist!';
        }

    }

    public function sentToDebug($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:' . $ipAddress, 'CLIENT-IP:' . $ipAddress));
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }


    public function sendToReceive($url, $data, $ipAddress)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:' . $ipAddress, 'CLIENT-IP:' . $ipAddress));
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }


}