<?php


namespace app\modules\task\services\mainIncrement;


use app\modules\common\services\Consts;
use app\modules\data\models\DbShuai;
use app\modules\match\models\Match;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchPushLog;
use app\modules\task\services\api\ApiBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;

class MainBattle
{
    public static function run($tagInfo,$taskInfo)
    {
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        $changeType=$tagInfo["change_type"];
        $ext_type=$tagInfo["ext_type"];
        $battleId = $mainInfo['id'];
        $matchId = $mainInfo['match'];
        if (!empty($matchId)) {
            if ($changeType == QueueServer::QUEUE_TYPE_ADD){
                $event = Consts::BATTLE_CREATION;
            }elseif ($changeType == QueueServer::QUEUE_TYPE_UPDATE && $ext_type == 'battle_status'){
                $event = Consts::BATTLE_UPDATE;
            }elseif ($changeType == QueueServer::CHANGE_TYPE_DELETE){
                $event = Consts::BATTLE_DELETION;
            }elseif ($changeType == QueueServer::CHANGE_TYPE_RECOVERY){
                $event = Consts::BATTLE_RECOVERY;
            }
            if (!empty($event)){
                $oldDb = \Yii::$app->get('db');
                $gyDb  = \Yii::$app->get('db_gy');
                \Yii::$app->set('db',$gyDb);

                $matchCore = Match::find()
                    ->select([])
                    ->where(["id" => $matchId])->asArray()->one();
                if (!empty($matchCore)) {
                    //一周前不推送
                    $aWeekAgo = date("Y-m-d H:i:s",strtotime( "-1 week"));
                    if ($matchCore['scheduled_begin_at'] < $aWeekAgo) {
                        return [];
                    }

                    $getBattle = MatchService::getBattle(null, $matchCore['game'], $matchCore['tournament_id'], $matchCore['group_id'], 'battle', $battleId);
                    $values = ApiBase::convertNull($getBattle);
                    if ($values) {
                        $data = $values[0];
                        $newdata['event_type'] = $event;
                        $newdata['battle_id'] = $data['battle_id'];
                        $newdata['match_id'] = $matchCore['id'];
                        $newdata['game'] = $data['game'];
                        $newdata['order'] = $data['order'];
                        $newdata['teams'] = $data['teams'];
                        $newdata['begin_at'] = $data['begin_at'];
                        $newdata['end_at'] = $data['end_at'];
                        $newdata['status'] = $data['status'];
                        $newdata['is_draw'] = $data['is_draw'];
                        $newdata['is_forfeit'] = $data['is_forfeit'];
                        $newdata['is_default_advantage'] = $data['is_default_advantage'];
                        $newdata['duration'] = $data['duration'];
                        $newdata['scores'] = $data['scores'];
                        $newdata['winner'] = $data['winner'];
                        $newdata['is_battle_detailed'] = $data['is_battle_detailed'];
                        $newdata['modified_at'] = $data['modified_at'];
                        $newdata['created_at'] = $data['created_at'];
                        $newdata['deleted_at'] = $data['deleted_at'];
                        $frames_data = [
                            'match_data' => json_encode($newdata, 320),
                            'type'       => 'battle-incre',
                            'match_id'   => $matchId
                        ];

                        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_MATCH_INSTANCE'),$frames_data);

                        $pushMatch = new MatchPushLog;
                        $pushMatchParam['match_data'] =  $frames_data['match_data'];
                        $pushMatchParam['event_type'] =  $event;
                        $pushMatchParam['type'] =  'battle-incre';
                        $pushMatchParam['match_id'] =  $matchId;
                        $pushMatchParam['battle_id'] =  $battleId;
                        $pushMatchParam['task_info_id'] =  $taskInfo['id'];
                        $pushMatch->setAttributes($pushMatchParam);
                        $pushMatch->save();
                    }
                }

                \Yii::$app->set('db',$oldDb);
            }
        }
        return [];
    }

}