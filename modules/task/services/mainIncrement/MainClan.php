<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;


use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;

class MainClan
{
    public static function run($tagInfo,$taskInfo)
    {
        // 账号更新，如果账号名字发生变化，刷新关联关系
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        if(isset($mainInfo['base']['id'])){
            RenovateService::refurbishApi('organization',$mainInfo['base']['id']);
        }

        if(isset($mainInfo['id'])){
            RenovateService::refurbishApi('organization',$mainInfo['id']);
        }
        return;
    }
}