<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;

use app\modules\common\services\Consts;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class MainIncrementDispatcher
{
    public static function run($tagInfo,$taskInfo)
    {
        $resourceType=$tagInfo["resource_type"];
        switch ($resourceType){
            case Consts::RESOURCE_TYPE_CLAN:
                $tasks= MainClan::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:
                $tasks= MainlolChampion::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:
                $tasks= MainlolItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_RUNE:
                $tasks= MainlolRune::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
                $tasks= MainlolSummonerSpell::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ABILITY:
                $tasks= MainlolAbility::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_HERO:
                $tasks= MainDotaHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_TALENT:
                $tasks= MainDotaTalent::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
                $tasks= MainDotaAbility::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:
                $tasks= MainDotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_CSGO_WEAPON:
                $tasks= MainCSgoWeapon::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_CSGO_MAP:
                $tasks= MainCSgoMap::run($tagInfo,$taskInfo);
                break;

            case QueueServer::QUEUE_RESOURCE_TEAM:
                $tasks= MainTeam::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
                $tasks= MainTournament::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks= MainPlayer::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                $tasks= MainMatch::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_BATTLE:
                $tasks= MainBattle::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_SERIES:
//            case QueueServer::QUEUE_RESOURCE_MATCH:
            case QueueServer::QUEUE_RESOURCE_METADATA:
            case QueueServer::QUEUE_RESOURCE_TO_DO:
                $tasks= [];
                break;
            //绑定类型处理
            case Consts::RESOURCE_TYPE_BING_MATCH:
            case Consts::RESOURCE_TYPE_BING_TOURNAMENT:
            case Consts::RESOURCE_TYPE_BING_PLAYER:
            case Consts::RESOURCE_TYPE_BING_TEAM:
            case Consts::RESOURCE_TYPE_SYS_DATA_RESOURCE_UPDATE_CONFIG:
            case Consts::RESOURCE_TYPE_SYS_CONFIG_UPDATE:
            case Consts::RESOURCE_TYPE_SYS_DATA_RESOURCE_TO_DO:
            case Consts::RESOURCE_TYPE_BING_LOL_CHAMPION:
            case Consts::RESOURCE_TYPE_BING_LOL_ITEM:
            case Consts::RESOURCE_TYPE_BIND_DOTA2_HERO:
            case Consts::RESOURCE_TYPE_BIND_DOTA2_ABILITY:
            case Consts::RESOURCE_TYPE_BIND_DOTA2_ITEM:
            case Consts::RESOURCE_TYPE_ERROR_MANY_TEAM:
            case Consts::RESOURCE_TYPE_BIND_LOL_ABILITY:
                $tasks = [];
                break;
            default:
                throw new TaskException("no MainIncrementDispatcher type for :".$resourceType);
        }
        return $tasks;
    }
}