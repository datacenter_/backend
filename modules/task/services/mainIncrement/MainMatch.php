<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;


use app\modules\common\services\Consts;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DbShuai;
use app\modules\data\models\DbSocket;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchPushLog;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\UpdateConfig;
use app\modules\task\models\StandardDataMatch;

class MainMatch
{
    public static function run($tagInfo,$taskInfo)
    {
        // 如果是新增的账号，刷新账号关联关系
        $changeType = $tagInfo["change_type"];
        $ext_type = $tagInfo["ext_type"];
        $ext2_type = $tagInfo["ext2_type"];
        // 账号更新，如果账号名字发生变化，刷新关联关系
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        $matchDiffInfo = $params["diff"];
//        unset($matchDiffInfo['begin_at']);
        unset($matchDiffInfo['duration']);
        if(!count($matchDiffInfo) && $changeType=='update'){
            return ;
        }

        //刷refurbishApi
        self::refurbishApi($mainInfo['match_id'],$mainInfo['battle_id'],$mainInfo['id']);

        self::refresh($changeType,$params,$mainInfo['id']);

        self::battleByMatch($changeType,$tagInfo,$mainInfo);

        self::matchPush($mainInfo,$changeType,$ext_type,$ext2_type,$taskInfo);


        if (isset($matchDiffInfo['scheduled_begin_at']) && $matchDiffInfo['scheduled_begin_at']['after'] ) {
            self::matchPush($mainInfo,$changeType,$ext_type,Consts::MATCH_FIXTURE_CHANGE,$taskInfo);
        }

        if (!empty($matchDiffInfo['team_version']['before']) && $matchDiffInfo['team_version']['before'] >= 1) {
            if (isset($matchDiffInfo['team_version']) && $matchDiffInfo['team_version']['after'] ) {
                self::matchPush($mainInfo,$changeType,$ext_type,Consts::MATCH_FIXTURE_TEAMCHANGE,$taskInfo);

            }
        }

        return [];
    }


    public static function refurbishApi ($match_id,$battle_id,$id) {

        if ($match_id && $battle_id) {
            RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_BATTLE_SINGLE,$battle_id);
            RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_MATCH,$match_id);

        }else{
            if($id){
                if(isset($match_id) && $battle_id){
                    $matchId = $match_id;
                }else{
                    $matchId = $battle_id;
                }
                RenovateService::refurbishApi(['match','battle'],$matchId);
            }

        }
    }


    public static function refresh($changeType,$params,$id) {
        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_MATCH,null, [$id]);
        }else{
            // 如果账号名字发生变化，则刷新关联关系
            if(isset($params["diff"]["scheduled_begin_at"]) || isset($params["diff"]["team_1_id"]) || isset($params["diff"]["team_2_id"]) || isset($params["diff"]["deleted"])){
                StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_MATCH,null, [$id]);
            }
        }
    }

    public static function battleByMatch($changeType,$tagInfo,$mainInfo) {

        if ($changeType == 'add' && $tagInfo['ext_type'] == 'core_data') {
            $master_id = $mainInfo['match_id'] ?: $mainInfo['id'];
            $masterId = DataStandardMasterRelation::find()->where(['resource_type' => 'match', 'master_id' => $master_id])->asArray()->one();
            if (!empty($masterId)) {
                $infoFromStandard = StandardDataMatch::find()->select([])->where(['id' => $masterId['standard_id']])->asArray()->one();
                //单图弃权
                if ($infoFromStandard['battle_forfeit'] && isset($infoFromStandard['battle_forfeit'])) {
                    $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
                    if (!empty($infoFromStandard['battle_forfeit'])) {
                        $BaorderArr = json_decode($infoFromStandard['battle_forfeit'], true);
                        foreach ($BaorderArr as $BkK => $Bvv) {
                            if ($Bvv['rel_team_id'] == $infoFromStandard['team_1_id']) {
                                $battleWinnerid = $infoFromStandard['team_2_id'];
                            }
                            if ($Bvv['rel_team_id'] == $infoFromStandard['team_2_id']) {
                                $battleWinnerid = $infoFromStandard['team_1_id'];
                            }
                            $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $battleWinnerid, $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
                            //判断更新配置 是不是自动更新   ->2改  新增不判断
//                            $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//                            if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
                            if ($Bvv['battle_order'] == 1 && !empty($infoFromStandard['default_advantage'])) {
                                continue;
                            }
                            if (empty($bWinnerid)) {
                                continue;
                            }

                            $battleType = 'forfeit';//'advantage' 或者 forfeit
                            $dealType = 'create';//create 或者 delete
//                            $originId = 3;//数据源ID
//                            $matchId = 789;//比赛ID
                            $battleOrder = $Bvv['battle_order'];// 具体orderID 或者 传空 是新建
                            $battleId = '';//如果传值就按照battleID 不按照battle order
                            $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                            $winnerId = $bWinnerid;//4317  1
                            $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
                            $rel_battleId = 1;//数据源对局ID
                            HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//                            }
                        }
                    }
                }
                //默认领先
                if ($infoFromStandard['default_advantage'] && isset($infoFromStandard['default_advantage'])) {
                    $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
                    $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $infoFromStandard['default_advantage'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);

                    //判断更新配置 是不是自动更新
//                    $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//                    if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
                    $battleType = 'advantage';//'advantage' 或者 forfeit
                    $dealType = 'create';//create 或者 delete
//                    $originId = 3;//数据源ID
//                    $matchId = 789;//比赛ID
                    $battleOrder = '1';// 具体orderID 或者 传空 是新建
                    $battleId = '';//如果传值就按照battleID 不按照battle order
                    $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                    $winnerId = $bWinnerid;//4317  1
                    $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
                    $rel_battleId = 1;//数据源对局ID
                    HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//                    }
                }
                if ($infoFromStandard['forfeit'] && isset($infoFromStandard['forfeit']) && $infoFromStandard['forfeit'] == 1) {
                    $matchId = HotBase::getMainIdByRelIdentityId('match', $infoFromStandard['rel_identity_id'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
                    $bWinnerid = HotBase::getMainIdByRelIdentityId('team', $infoFromStandard['winner'], $infoFromStandard['origin_id'], $infoFromStandard['game_id']);
                    //判断更新配置 是不是自动更新
//                    $updateInfo = DataResourceUpdateConfig::find()->select('origin_id')->where(['resource_type' => 'match'])->andWhere(['resource_id' => $matchId])->andWhere(['tag_type' => 'update_data'])->andWhere(['update_type' => 3])->asArray()->one();
//                    if (@$updateInfo['origin_id'] == $infoFromStandard['origin_id']) {
                    $battleType = 'forfeit';//'advantage' 或者 forfeit
                    $dealType = 'create';//create 或者 delete
//                    $originId = 3;//数据源ID
//                    $matchId = 789;//比赛ID
                    $battleOrder = '1';// 具体orderID 或者 传空 是新建
                    $battleId = '';//如果传值就按照battleID 不按照battle order
                    $winnerType = 'team_id';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
                    $winnerId = $bWinnerid;//4317  1
                    $rel_matchId = $infoFromStandard['rel_identity_id'];//数据源比赛ID
                    $rel_battleId = 1;//数据源对局ID
                    HotBase::dealMatchSpecialBattle($battleType, $dealType, $infoFromStandard['origin_id'], $matchId, $battleOrder, $battleId, $winnerType, $winnerId, $rel_matchId, $rel_battleId);
//                    }
                }
            }
        }
    }

    public static function matchPush($mainInfo,$changeType,$ext_type,$ext2_type,$taskInfo)  {
        if ($mainInfo){
            $matchId = $mainInfo['match_id'] ?: $mainInfo['id'];
            if ($changeType == QueueServer::QUEUE_TYPE_ADD && in_array($ext_type,['core_data','real_time'])){
                $event = Consts::MATCH_CREATION;
            }elseif ($changeType == QueueServer::QUEUE_TYPE_UPDATE && in_array($ext_type,['core_data','real_time'])){
                if ($ext2_type == Consts::MATCH_FIXTURE_CHANGE) {
                    $event = Consts::MATCH_FIXTURE_CHANGE;
                }elseif ($ext2_type == Consts::MATCH_FIXTURE_TEAMCHANGE){
                    $event = Consts::MATCH_FIXTURE_TEAMCHANGE;
                } else {
                    $event = Consts::MATCH_UPDATE;
                }
            }elseif ($changeType == QueueServer::CHANGE_TYPE_DELETE){
                $event = Consts::MATCH_DELETION;
            }elseif ($changeType == QueueServer::CHANGE_TYPE_RECOVERY){
                $event = Consts::MATCH_RECOVERY;
            }
            if (!empty($event)) {
                $oldDb = \Yii::$app->get('db');
                $gyDb  = \Yii::$app->get('db_gy');
                \Yii::$app->set('db',$gyDb);

                $data_frame =  MatchService::matchLog($matchId,$event);
                if ($data_frame) {
                    $frames_data = [
                        'match_data' => json_encode($data_frame, 320),
                        'type'       => 'match-incre',
                        'match_id'   => $matchId
                    ];
                    KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_MATCH_INSTANCE'),$frames_data);
                    $pushMatch = new MatchPushLog;
                    $pushMatchParam['match_data'] =  $frames_data['match_data'];
                    $pushMatchParam['type'] =  'match-incre';
                    $pushMatchParam['event_type'] =  $event;
                    $pushMatchParam['match_id'] =  $matchId;
                    $pushMatchParam['task_info_id'] =  $taskInfo['id'];
                    $pushMatch->setAttributes($pushMatchParam);
                    $pushMatch->save();
                }

                \Yii::$app->set('db',$oldDb);
            }

        }
    }

}