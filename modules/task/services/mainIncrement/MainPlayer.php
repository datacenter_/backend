<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;


use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\UpdateConfig;

class MainPlayer
{
    public static function run($tagInfo,$taskInfo)
    {
        // 账号更新，如果账号名字发生变化，刷新关联关系
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        //核心内容
        $mainInfoId = $mainInfo['player_id'] ?? $mainInfo['id'];
        if($mainInfoId) {
            RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_PLAYER,$mainInfoId);
        }
        $teamByPlayerInfos = $teams = [];
        if (!empty($mainInfoId)){
            $teamByPlayerInfo = TeamPlayerRelation::find()
                ->select("team_player_relation.team_id as team_id")
                ->leftJoin('team', 'team.id=team_player_relation.team_id')
                ->where(["player_id" => $mainInfoId])->asArray()->all();
            if (!empty($teamByPlayerInfo)){
                $teamByPlayerInfos = array_column($teamByPlayerInfo,'team_id');
            }

        }
        //增量更新（战队）：核心
        if (isset($mainInfo['team_by_playerIds']) && $mainInfo['team_by_playerIds'] != "[]"){
            $teams = json_decode($mainInfo['team_by_playerIds'], true);
        }

        if ($teamByPlayerInfos || $teams) {
            $teamMerge = array_merge($teamByPlayerInfos,$teams);
            $teamsArrData = array_unique($teamMerge);
            RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_TEAM, $teamsArrData);

        }

        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_PLAYER,null, [$mainInfo['id']]);
            return [];
        }else{
            // 如果账号名字发生变化，则刷新关联关系
            if(isset($params["diff"]["nick_name"]) || isset($params["diff"]["deleted"])){
                StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_PLAYER,null, [$mainInfo['id']]);
                return [];
            }
        }

        return [];

    }
}