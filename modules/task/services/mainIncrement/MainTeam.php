<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;


use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\UpdateConfig;

class MainTeam
{
    public static function run($tagInfo,$taskInfo)
    {
        // 账号更新，如果账号名字发生变化，刷新关联关系
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        if(isset($mainInfo['team_id'])){
            RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_TEAM,$mainInfo['team_id']);
        }else {
            if (isset($mainInfo['id']) && $mainInfo['id']) {
                RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_TEAM, $mainInfo['id']);
            }
            if (isset($mainInfo['organization']) && $mainInfo['organization']) {
                RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_ORGANIZATION, $mainInfo['organization']);
            }
        }

        $teamId = $mainInfo['team_id'] ?? $mainInfo['id'];
        $teamPlayerArr = $playerbeforeId = $playersIds = $coreDataPlayer = [];
        if ($teamId) {
            $teamPlayer = TeamPlayerRelation::find()
                ->alias('tr')
                ->select(['tr.*','p.*','country.name as country_name','country.e_name as country_e_name'])
                ->leftJoin('player as p', 'p.id = tr.player_id')
                ->leftJoin('enum_country as country', 'country.id = p.player_country')
                ->where(['tr.team_id' => $teamId])->asArray()->all();
            $teamPlayerArr = array_column($teamPlayer,'player_id');
        }

        if (!empty($mainInfo['playes']) && $mainInfo['playes'] != '[]') {
            $coreDataPlayer = json_decode($mainInfo['playes'], true);
        }



        //关联关系（选手before）
        if (isset($params["diff"]['rel_player_info']['before']) && $params["diff"]['rel_player_info']['before'] != '[]') {
            $playerbefore  =    $params["diff"]['rel_player_info']['before'];
            $playerbeforeId = json_decode($playerbefore, true);
        }
       if (isset($params["diff"]['rel_player_info']['after']) && $params["diff"]['rel_player_info']['after'] != '[]') {
            $playersJson = $params["diff"]['rel_player_info']['after'];
            $playersIds = json_decode($playersJson, true);

       }

       if ($teamPlayerArr || $playerbeforeId || $playersIds || $coreDataPlayer) {
           $playersAlls = array_merge($teamPlayerArr,$playerbeforeId,$playersIds,$coreDataPlayer);
           $playersAll = array_unique($playersAlls);
           RenovateService::refurbishApi(RenovateService::INTERFACE_TYPE_PLAYER, $playersAll);
       }


        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_TEAM,null, [$mainInfo['id']]);
            return [];
        }else{
            // 如果发生变化，则刷新关联关系
            if(isset($params["diff"]["name"]) || isset($params["diff"]["full_name"]) || isset($params["diff"]["short_name"]) || isset($params["diff"]["alias"]) || isset($params["diff"]["deleted"])){
                StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_TEAM,null, [$mainInfo['id']]);
                return [];
            }
        }
        
        return [];

    }
}