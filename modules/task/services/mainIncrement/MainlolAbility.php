<?php
/**
 *
 */

namespace app\modules\task\services\mainIncrement;

use app\modules\common\services\Consts;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;

class MainlolAbility
{
    public static function run($tagInfo,$taskInfo)
    {
        // 账号更新，如果账号名字发生变化，刷新关联关系
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);
        $mainInfo=$params["new"];
        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        if($mainInfo['champion_id']){
            RenovateService::refurbishApi('lol_champion',$mainInfo['champion_id']);
        }
        //英雄before
        $beforeDiff = $params['diff'];
        $championBeforeId = $beforeDiff['champion_id']['before'];
        if (isset($championBeforeId)) {
            RenovateService::refurbishApi('lol_champion',$championBeforeId);
        }

        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_METADATA,null, [$mainInfo['id']],Consts::METADATA_TYPE_LOL_ABILITY);
            return [];
        }else{
        // 如果账号名字发生变化，则刷新关联关系
            if(isset($params["diff"]["name"]) || isset($params["diff"]["external_id"]) || isset($params["diff"]["name_cn"]) || isset($params["diff"]["slug"]) || isset($params["diff"]["external_name"]) || isset($params["diff"]["deleted"])){
                StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_METADATA,null, [$mainInfo['id']],Consts::METADATA_TYPE_LOL_ABILITY);
                return [];
            }
        }
        return [];

    }
}