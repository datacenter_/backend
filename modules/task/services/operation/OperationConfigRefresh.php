<?php
/**
 *
 */

namespace app\modules\task\services\operation;


use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataResourceUpdateConfigOperation;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\tournament\models\Tournament;

class OperationConfigRefresh
{
    public static function run($tagInfo, $taskInfo)
    {
        // taskInfo中 有对应id
        $params = $taskInfo["params"];
        $paramsInfo = json_decode($params, true);
        $operationId = $paramsInfo['id'];
        $operationInfo = DataResourceUpdateConfigOperation::find()->where(['id' => $operationId])->asArray()->one();
        $gameId = $operationInfo['game_id'];
        $resourceType = $operationInfo['resource_type'];
        $configInfo = json_decode($operationInfo['config_info'], true);
        $operator = $operationInfo['operator'];
        // 获取所有资源, 遍历配置
        $ids = DataResourceUpdateConfig::find()->select('distinct(resource_id) as resource_id')->where(['resource_type' => $resourceType, 'game_id' => $gameId])->asArray()->all();
        $resourceIds = array_column($ids, 'resource_id');
        foreach ($resourceIds as $key => $resourceId) {
            UpdateConfigService::updateResourceUpdateConfig($resourceId, $configInfo, OperationLogService::USER_TYPE_ADMIN, $operator);
        }
        // 更新处理状态
        return [];
    }

    public static function initAll()
    {

        $info = Team::find()->all();
        foreach ($info as $val) {
            UpdateConfigService::initResourceUpdateConfig($val['id'], 'team', $val['game']);
        }

        $info = Player::find()->all();
        foreach ($info as $val) {
            UpdateConfigService::initResourceUpdateConfig($val['id'], 'player', $val['game_id']);
        }

        $info = Match::find()->all();
        foreach ($info as $val) {
            UpdateConfigService::initResourceUpdateConfig($val['id'], 'match', $val['game']);
        }

        $info=Tournament::find()->all();
        foreach ($info as $val) {
            UpdateConfigService::initResourceUpdateConfig($val['id'], 'tournament', $val['game']);
        }

    }
}