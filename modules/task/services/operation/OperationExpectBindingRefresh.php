<?php
/**
 *
 */

namespace app\modules\task\services\operation;


use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\models\DataBindTime;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataResourceUpdateConfigOperation;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\UpdateConfigService;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;

class OperationExpectBindingRefresh
{
    public static function run($tagInfo, $taskInfo)
    {
        // 查询绑定,更新绑定关系
        self::refreshPlayer();
    }

    public static function refreshPlayer()
    {
        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        $pageCount=200;
        for($i=0;$i<10000000;$i++){
            $resourceIds=StandardDataPlayer::find()->select("id")->where(['and',
                ['<>','deal_status','4'],
                ['>=','modified_at',$queryTime],
            ])->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_PLAYER,$ids);
        }
    }

    public static function refreshTeam()
    {
        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $resourceIds=StandardDataTeam::find()->select("id")->where(['and',
                ['<>','deal_status','4'],
                ['>=','modified_at',$queryTime],
            ])->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_TEAM,$ids);
        }
    }

    public static function refreshMetadata()
    {
        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        // 刷新元数据
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $resourceIds=StandardDataMetadata::find()->select(['id','metadata_type'])->where(['and',
                ['<>','deal_status',4],
                ['>=','modified_at',$queryTime],
            ])->limit($pageCount)->offset($i*$pageCount)->asArray()->all();
            if(!$resourceIds){
                break;
            }
            foreach($resourceIds as $val){
                StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_METADATA,[$val['id']],[],$val['metadata_type']);
            }
        }

    }
    public static function refreshMatch()
    {
        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        print_r("开始处理");
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $resourceIds=StandardDataMatch::find()->select("id")->where(['and',
                ['<>','deal_status','4'],
                ['>=','modified_at',$queryTime],
            ])->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            print_r("ids:".implode(",",$ids));
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_MATCH,$ids);
        }
    }
    //TODO  后续换这个
    public static function refreshMatchNew()
    {
        //查询限制条件
        $DataBindTimeInfo = DataBindTime::find()->where(['bd_type' => 'match'])->asArray()->one();
        if ($DataBindTimeInfo){
            $queryOriginIds = $DataBindTimeInfo['bd_origin_ids'];
            $queryTime = $DataBindTimeInfo['bd_start_time'];
        }else{
            $queryOriginIds = null;
            $queryTime = date("Y-m-d H:i:s",strtotime("-1 week"));
        }
        //$queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));//原来的
        print_r("开始处理");
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $where = [
                'and',
                ['<>','deal_status','4'],
                ['>=','scheduled_begin_at',$queryTime]
            ];
            if ($queryOriginIds){
                $where[] = ['in', 'origin_id', $queryOriginIds];
            }
            $resourceIds=StandardDataMatch::find()->select("id")->where($where)->limit($pageCount)->offset($i*$pageCount)->all();
//            $resourceIds=StandardDataMatch::find()->select("id")->where(['and',
//                ['<>','deal_status','4'],
//                ['>=','modified_at',$queryTime]
//            ])->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            print_r("ids:".implode(",",$ids));
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_MATCH,$ids);
        }
    }
    public static function refreshBdMatch()
    {
        //查询限制条件
        $queryEndTime = date("Y-m-d H:i:s",strtotime("+3 day"));
        $queryStartTime = date("Y-m-d H:i:s",strtotime("-1 day"));
        print_r("开始处理");
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $where = [
                'and',
                ['=','deal_status','4'],
                ['<','status',3],
                ['<=','scheduled_begin_at',$queryEndTime],
                ['>=','scheduled_begin_at',$queryStartTime]
            ];
//            if ($queryOriginIds){
//                $where[] = ['in', 'origin_id', $queryOriginIds];
//            }
            $resourceIds=StandardDataMatch::find()->select("id")->where($where)->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            print_r("ids:".implode(",",$ids));
            StandardMasterRelationExpectService::refreshBdMatch($ids);
        }
    }
    public static function refreshTournament()
    {
        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        $pageCount=200;
        for($i=0;$i<10000000;$i++) {
            $resourceIds=StandardDataTournament::find()->select("id")->where(['and',
                ['<>','deal_status','4'],
                ['>=','modified_at',$queryTime],
            ])->limit($pageCount)->offset($i*$pageCount)->all();
            if(!$resourceIds){
                break;
            }
            $ids=array_column($resourceIds,'id');
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_TOURNAMENT,$ids);
        }
    }

}