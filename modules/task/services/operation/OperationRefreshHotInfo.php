<?php
/**
 *
 */
namespace app\modules\task\services\operation;
use app\modules\common\services\Consts;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\match\models\Match;
use app\modules\match\models\SortingLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Redis;
class OperationRefreshHotInfo
{
    const CYCLE_WAITING = 1800;
    const LOL_CYCLE_WAITING = 300;
    const CYCLE_GOING = 2;

    public static function refreshListForLog()
    {
        $list = self::getRefreshListForLog();
        foreach ($list as $match) {
            self::refreshInfoForLog($match);
        }
    }
    public static function refreshListForLogWs()
    {
        $list = self::getRefreshListForLog();
        foreach ($list as $match) {
            self::refreshInfoForLogWs($match);
        }
    }
    public static function getRefreshListForLog()
    {
        $list = HotDataRunningMatch::find()->where(['and', ['<>', 'deal_status', '3'], ['=', 'origin_id', '5']])->asArray()->all();
        return $list;
    }
    public static function refreshInfoForLog($hotMatch)
    {
        // 判断任务的开启状态
        if ($hotMatch['auto_status'] == 2) {
            return;
        }

        // 是否处理完成
        if ($hotMatch['deal_status'] == 3) {
            return;
        }
        // 检查上次更新到的id，如果有新的id，则添加任务
        $sortingLog=SortingLog::find()->where(['id'=>$hotMatch['standard_id']])->one();
        $ipAdd=$sortingLog['service_ip'];
        $ipArray=explode(';',$ipAdd);
        $where = ['in', 'ip_address', $ipArray];
        $lastLine=ReceiveData5eplayFormatEvents::find()->where($where)->orderBy("id desc")->one();

        if((!$hotMatch['to_event_id'])||$lastLine['id']>$hotMatch['to_event_id']){
            HotDataRunningMatch::updateAll(['to_event_id'=>$lastLine['id'],'from_event_id'=>$hotMatch['to_event_id']],['id'=>$hotMatch['id']]);
            // 记录事件开始结束id，用来限定更新周期
            $hotMatchNew=HotDataRunningMatch::find()->where(['id'=>$hotMatch['id']])->asArray()->one();
            $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH,
                    "", "", "", ""),
                "type" => '',
                "batch_id" => date("YmdHis"),
                "params" => $hotMatchNew,
            ];
            TaskRunner::addTask($task, 1);
        }
        return;
    }
    public static function refreshInfoForLogWs($hotMatch)
    {
        // 判断任务的开启状态
        if ($hotMatch['auto_status'] == 2) {
            return;
        }

        // 是否处理完成
        if ($hotMatch['deal_status'] == 3) {
            return;
        }
        // 检查上次更新到的id，如果有新的id，则添加任务
        $sortingLog=SortingLog::find()->where(['id'=>$hotMatch['standard_id']])->one();
        $ipAdd=$sortingLog['service_ip'];
        $ipArray=explode(';',$ipAdd);
        $where = ['in', 'ip_address', $ipArray];
        $lastLine=ReceiveData5eplayFormatEvents::find()->where($where)->orderBy("id desc")->one();

        if((!$hotMatch['to_event_id'])||$lastLine['id']>$hotMatch['to_event_id']){
            HotDataRunningMatch::updateAll(['to_event_id'=>$lastLine['id'],'from_event_id'=>$hotMatch['to_event_id']],['id'=>$hotMatch['id']]);
            // 记录事件开始结束id，用来限定更新周期
            $hotMatchNew=HotDataRunningMatch::find()->where(['id'=>$hotMatch['id']])->asArray()->one();
            $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH_WS,
                    "", "", "", ""),
                "type" => '',
                "batch_id" => date("YmdHis"),
                "params" => $hotMatchNew,
            ];
            TaskRunner::addTask($task, 1);
        }
        return;
    }

    public static function refreshList()
    {
        $list = self::getRefreshList();
        foreach ($list as $match) {
            //获取 standard_id  rel_identity_id origin_id
            $standard_id = '';
            $rel_identity_id = '';
            $matchId = $match['match_id'];
            $updateInfo = DataResourceUpdateConfig::find()->select(['origin_id','update_type'])->where(['resource_type'=>'match'])->andWhere(['resource_id'=>$matchId])->andWhere(['tag_type'=>'score_data'])->asArray()->one();
            $updateInfo_origin_id = @$updateInfo['origin_id'];
            //日志录入跳过
            if ($updateInfo_origin_id == 5){
                continue;
            }
            $match['origin_id'] = $updateInfo_origin_id;
            //外面的录入状态
            $match['update_type'] = @$updateInfo['update_type'];
            //所有的relation
            $relationInfos = DataStandardMasterRelation::find()->where(['resource_type'=>'match'])->andWhere(['master_id'=>$matchId])->asArray()->all();
            if ($relationInfos){
                foreach ($relationInfos as $item){
                    $standard_id_check = $item['standard_id'];
                    //查询指定的standard match 数据
                    $standardDataMatchInfo = StandardDataMatch::find()->where(['id'=>$standard_id_check])->asArray()->one();
                    if (@$standardDataMatchInfo['origin_id'] == $updateInfo_origin_id && @$standardDataMatchInfo['origin_id']){
                        $rel_identity_id = @$standardDataMatchInfo['rel_identity_id'];
                        $standard_id = @$standardDataMatchInfo['id'];
                    }
                }
            }
            $match['standard_id'] = $standard_id;
            $match['rel_identity_id'] = $rel_identity_id;
            //插入任务
            self::refreshInfo($match);
        }
    }

    public static function getRefreshList()
    {
        $start_date = date("Y-m-d",strtotime("-1 day"));
        $end_date = date("Y-m-d",strtotime("+1 day"));
        $list = Match::find()->alias('m')->select([
                'h.id',
                'm.game as game_id',
                'h.match_id',
                'h.auto_status',
                'h.auto_add_type',
                'mrti.status as match_status',
                'h.deal_status',
                'h.cycle',
                'h.last_deal_status',
                'h.from_event_id',
                'h.to_event_id',
                'm.scheduled_begin_at as match_start_time',
                'mrti.end_at as match_end_time'
            ])
            ->leftJoin('match_real_time_info as mrti','m.id = mrti.id')
            ->leftJoin('hot_data_running_match as h','m.id=h.match_id')
            ->where(['and',
                ['>=','m.scheduled_begin_at',$start_date],
                ['<=','m.scheduled_begin_at',$end_date],
//                ['<=','m.game',3],//只刷 1,2,3
                ['=','h.deal_status',1],
                ['=','h.auto_status',1],
                //['=','mrti.is_battle_detailed',1],
                ['=','m.deleted',2]
            ])
            ->asArray()->all();
        return $list;

    }

    public static function refreshInfo($match)
    {
        $now_date = time();
        // 录入配置是否是自动
        if ($match['update_type'] != 3) {
            return;
        }
        //比赛自动录入状态
        if ($match['auto_add_type'] == 'socket'){
            return;
        }
        // 判断比赛状态
        if ($match['match_status'] == 3){
            $match_end_time_num = strtotime($match['match_end_time']) + 3600;
            if ($now_date > $match_end_time_num){
                return;
            }
        }
        if($match['match_status'] == 4 || $match['match_status'] == 5) {
            return;
        }
        // 是否处理完成
        if ($match['deal_status'] == 3) {
            return;
        }
        //处理时间
        if ($match['last_deal_status']){
            $lastDealTime = strtotime($match['last_deal_status']);
            if ($match['auto_add_type'] == 'restapi'){
                $cycle = self::LOL_CYCLE_WAITING;
                //判断时间周期  秒数
                if ($match['cycle']){
                    $cycle = $match['cycle'];
                }
                //判断他在不在这个时间内
                if ($now_date - $lastDealTime < $cycle) {
                    return;
                }
            }
        }else{
            $end_time = $now_date + 1000;
            $match_start_time_time = strtotime($match['match_start_time']);
            if($match_start_time_time < $end_time ){}else{
                return;
            }
        }
        //task内容
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH,
                "", "", "", ""),
            "type" => '',
            "batch_id" => date("YmdHis"),
            "params" => $match,
        ];

        //插入taskinfo
        TaskRunner::addTask($task, 1);
        //更新hot表的处理时间
        HotDataRunningMatch::updateAll(['last_deal_status'=>date("Y-m-d H:i:s"),'cycle' =>self::LOL_CYCLE_WAITING ],['id'=>$match['id']]);
        //这个不知道怎么原因return 了
        return;
//        $matchStartTimeStamp = strtotime($match['match_start_time']);
//        $matchEndTimeStamp = strtotime($match['match_end_time']);
//        $lastDealTime = strtotime($match['modified_time']);
//        // 判断比赛状态，和开始时间
//        if(!$match['match_status']){
//            $match['match_status']=1;
//        }
//        if ($match['match_status'] == 1) {
//            if ($matchStartTimeStamp - time() > 20) { // 比赛开始前20s之前
//                if (time() - $lastDealTime > self::CYCLE_WAITING) {  // 上次更新距离现在的时间大于更新周期
//                    TaskRunner::addTask($task, 1);
//                }
//            } else { // 比赛开始前20s之内
//                if (time() - $lastDealTime > self::CYCLE_GOING) {// 上次更新距离现在的时间大于更新周期
//                    TaskRunner::addTask($task, 1);
//                }
//            }
//        }
//        if ($match['match_status'] == 2) {
//            if (time() - $lastDealTime > self::CYCLE_GOING) {// 上次更新距离现在的时间大于更新周期
//                TaskRunner::addTask($task, 1);
//            }
//        }
//        if ($match['match_status'] == 3) {
//            if (time()-$matchEndTimeStamp > 86400) { // 比赛结束后一天
//                // 更新处理状态为完成
//                return;
//            }
//
//
//            if (time()-$matchEndTimeStamp > 600) { // 比赛结束后10分钟之后
//                if (time() - $lastDealTime > self::CYCLE_WAITING) {  // 上次更新距离现在的时间大于更新周期
//                    TaskRunner::addTask($task, 1);
//                }
//            } else { // 比赛开始前20s之内
//                if (time() - $lastDealTime > self::CYCLE_GOING) {// 上次更新距离现在的时间大于更新周期
//                    TaskRunner::addTask($task, 1);
//                }
//            }
//        }
    }
    //rest api 重刷
    public static function refreshMatchInfoByMatchId($match_id,$origin_id,$type='',$gameType='lol',$rel_match_id=null)
    {
//        if ($origin_id == 3 && $type=='after_api'){//在RefreshWebsocketApi 里添加
//            return true;
//        }
        if ($origin_id == 4 && $type=='after_api'){//在RefreshWebsocketApi 里添加
            return true;
        }
        if (($origin_id == 9 || $origin_id == 10) && $type=='after_api'){//在RefreshWebsocketApi 里添加
            return true;
        }
        //根据matchid获取hot表中的信息
        $match_info = HotDataRunningMatch::find()->select('id,match_id,game_id')->where(["match_id"=>$match_id])->asArray()->one();
        $match_info['origin_id'] = $origin_id;
        //task内容
        $task = [
            "tag" => QueueServer::getTag(
                QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH,
                "",
                "",
                "",
                "",
                'refresh'
            ),
            "type" => '',
            "batch_id" => date("YmdHis"),
            "params" => $match_info,
        ];
        if ($type == 'after_api'){
            $task["redis_server"]="order_{$gameType}_{$rel_match_id}";
            $taskNum = 9002;
        }else{
            $taskNum = 1;
        }
        //插入taskinfo
        $taskRes = TaskRunner::addTask($task, $taskNum);
        //更新hot表的处理时间
        HotDataRunningMatch::updateAll(['last_deal_status' => date("Y-m-d H:i:s")], ['id' => $match_info['id']]);
        //返回task id
        return $taskRes['id'];
    }
    //socket 重刷
    public static function refreshMatchInfoBySocket($origin_id,$params,$gameType='')
    {
        switch ($gameType){
            case 'csgo':
                //task内容
                if ($origin_id == 9 || $origin_id == 10){
                    $rel_match_id_string = HotBase::getPerIdByRelMatchId($params['rel_match_id'],$origin_id);
                    //task内容
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            1,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$rel_match_id_string}"
                    ];
                }else{
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                            $params['origin_id'],
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                            "add",
                            "1",
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server"=>"order_{$gameType}_{$params['rel_match_id']}"
                    ];
                }
            break;
            case 'dota':
                if ($origin_id == 3){
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            3,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$params['rel_match_id']}"
                    ];
                }

                if ($origin_id == 9 || $origin_id == 10){
                    $rel_match_id_string = HotBase::getPerIdByRelMatchId($params['rel_match_id'],$origin_id);
                    //task内容
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            3,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$rel_match_id_string}"
                    ];
                }

                break;
            case 'lol':
                if ($origin_id == 3){
//                    //task内容
//                    $task = [
//                        "tag" => QueueServer::getTag(
//                            QueueServer::QUEUE_REFRESH_MATCH_SOCKET,
//                            "",
//                            "",
//                            "",
//                            "",
//                            'refresh'
//                        ),
//                        "type" => QueueServer::QUEUE_REFRESH_MATCH_SOCKET,
//                        "batch_id" => date("YmdHis"),
//                        "params" => $params,
//                        "redis_server" => "order_{$gameType}_{$params['rel_match_id']}"
//                    ];
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            2,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$params['rel_match_id']}"
                    ];
                }
                if ($origin_id == 4){
                    //task内容
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            2,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$params['rel_match_id']}"
                    ];
                }
                if ($origin_id == 9 || $origin_id == 10){
                    $rel_match_id_string = HotBase::getPerIdByRelMatchId($params['rel_match_id'],$origin_id);
                    //task内容
                    $task = [
                        "tag" => QueueServer::getTag(
                            QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id,
                            Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                            "add",
                            2,
                            'refresh'
                        ),
                        "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id" => date("YmdHis"),
                        "params" => $params,
                        "redis_server" => "order_{$gameType}_{$rel_match_id_string}"
                    ];
                }
                break;
            case 'hltvcsgo':
                //task内容
                $task = [
                    "tag" => QueueServer::getTag(
                        QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                        $params['origin_id'],
                        Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "add",
                        "1",
                        'refresh'
                    ),
                    "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                    "batch_id" => date("YmdHis"),
                    "params" => $params,
                    "redis_server"=>"order_{$gameType}_{$params['rel_match_id']}"
                ];
                break;
        }

        //插入taskinfo
        $taskRes = TaskRunner::addTask($task, 9002);
        //删除完成 添加一条任务
        $match_id = $params['match_id'];
        if($gameType == 'csgo' && $origin_id==3){
            $match_id = $params['db_match_id'];
        }
        $rel_match_id = $params['rel_match_id'];
        $origin_id = $params['origin_id'];


        $result = OperationRefreshHotInfo::refreshMatchInfoByMatchId($match_id,$origin_id,'after_api',$gameType,$rel_match_id);
        //返回task id
        return $taskRes['id']."_".$result;





    }

}