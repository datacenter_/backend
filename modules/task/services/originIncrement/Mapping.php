<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;


class Mapping
{
    public static function transformation($config,$params)
    {
        $info=[];
        foreach($config as $key=>$val){
            if(is_callable($val)){
                $info[$key]=$val($params);
                continue;
            }
            if(isset($params[$val])){
                $info[$key]=isset($params[$val])?$params[$val]:"";
                continue;
            }
        }
        return $info;
    }

    /**
     * 获取数组总对应的key值，如果不存在返回defalutValue
     * @param $source
     * @param $keyString
     * @param null $defaultValue
     * @param string $keySplit
     * @return mixed|null
     */
    public static function getOrNull($source,$keyString,$defaultValue=null,$keySplit='.'){
        $keyArray=explode($keySplit,$keyString);
        $value=$source;
        foreach($keyArray as $v){
            if(isset($value[$v])){
                $value=$value[$v];
            }else{
                return $defaultValue;
            }
        }
        return $value;
    }
}