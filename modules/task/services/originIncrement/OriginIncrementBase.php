<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;


use app\modules\common\services\Consts;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataHotInfo;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\base\Exception;

class OriginIncrementBase
{
    public static function setStandardData($originId, $resourceType, $gameId, $identityId, $info)
    {
        $standardDataActiveTable=self::getStandardActiveTable($resourceType);
        $standardInfo=$standardDataActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
        ])->one();
        $newInfo = $info;
        $oldInfo = '';
        if ($standardInfo) {
            $oldInfo = $standardInfo->toArray();
        }
        if ($newInfo == $oldInfo) {
            // 数据相同，不用比较
            return ["changed" => false,
                'change_type' => QueueServer::CHANGE_TYPE_EQUAL,
                'diff' => [],
                'old' => $oldInfo,
                'new' => $newInfo];
        }

        if (!$standardInfo) {
            $standardInfo=new $standardDataActiveTable();
            $standardInfo->setAttributes(
                [
                    'origin_id' => $originId,
                    'game_id' => $gameId,
                    'rel_identity_id' => (string)$identityId
                ]
            );
        }
        $standardInfo->setAttributes($info);
        if (!$standardInfo->save()) {
            throw new BusinessException($standardInfo->getErrors(), '保存log失败');
        }
        return Common::getDiffInfo($oldInfo, $newInfo);
    }

    public static function getInfoFromStandard($identityId, $originId ='',$resourceType ='',$gameId ='')
    {
        $standardActiveTable=self::getStandardActiveTable($resourceType);
        $standardInfo=$standardActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
        ])->one();
        if($standardInfo){
            return $standardInfo->toArray();
        }
    }

    /**
     * @param $resourceType
     * @return \yii\db\ActiveRecord
     * @throws Exception
     */
    public static function getStandardActiveTable($resourceType)
    {
        $mapInfo=[
            Consts::RESOURCE_TYPE_PLAYER=>StandardDataPlayer::class,
            Consts::RESOURCE_TYPE_TEAM=>StandardDataTeam::class,
        ];
        if(isset($mapInfo[$resourceType])){
            return $mapInfo[$resourceType];
        }
        throw new Exception('获取对象失败');
    }
}