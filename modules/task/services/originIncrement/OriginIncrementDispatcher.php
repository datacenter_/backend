<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;


use app\modules\common\services\Consts;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\grab\hltv\HltvPlayer;
use app\modules\task\services\grab\radarpurple\RadarPurplePlayer;
use app\modules\task\services\originIncrement\abios\v3\AbiosDispatcher;
use app\modules\task\services\originIncrement\feijing\FeijingDispatcher;
use app\modules\task\services\originIncrement\hltv\HltvDispatcher;
use app\modules\task\services\originIncrement\orange\OrangeDispatcher;
use app\modules\task\services\originIncrement\pandascore\PandascoreDispatcher;
use app\modules\task\services\originIncrement\radarpurple\RadarpurpleDispatcher;
use app\modules\task\services\originIncrement\riotgames\RiotgamesDispatcher;
use app\modules\task\services\originIncrement\valve\ValveDispatcher;
use app\modules\task\services\QueueServer;

class OriginIncrementDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $originType=$tagInfo["origin_type"];
        switch ($originType){
            case QueueServer::QUEUE_ORIGIN_FEIJING:
                $tasks= FeijingDispatcher::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_ABIOS:
//                $tasks= AbiosDispatcher::run($tagInfo,$taskInfo); //abios v2老接口（暂保留）
                $tasks= AbiosDispatcher::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_PANDASCORE:
                $tasks= PandascoreDispatcher::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_RIOTGAMES:  //拳头
                $tasks= RiotgamesDispatcher::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_ORIGIN_ORANGE:  //玩家
                $tasks= OrangeDispatcher::run($tagInfo,$taskInfo);
                break;
            case Consts::ORIGIN_HLTV:
                $tasks= HltvDispatcher::run($tagInfo,$taskInfo);
                break;
            case Consts::ORIGIN_VALVE:
                $tasks = ValveDispatcher::run($tagInfo,$taskInfo);
                break;
            case Consts::ORIGIN_BAYES:
                $tasks =\app\modules\task\services\originIncrement\bayes\BayesDispatcher::run($tagInfo,$taskInfo);
                break;
            case Consts::ORIGIN_BAYES2:
                $tasks =\app\modules\task\services\originIncrement\bayes2\BayesDispatcher::run($tagInfo,$taskInfo);
                break;
            case Consts::ORIGIN_RADAR_PURPLE:
                $tasks = RadarpurpleDispatcher::run($tagInfo,$taskInfo);
                break;
            default:
                throw new TaskException("can not deal with this type :".$originType);
                break;
        }
        return $tasks;
    }
}