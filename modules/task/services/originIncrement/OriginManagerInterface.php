<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;


interface OriginManagerInterface
{
    public static function getInfoFromStandard($playerId, $originId ='',$resourceType ='',$gameId ='');

    public static function getInfoFromSource($playerId, $originId ='',$resourceType ='',$gameId ='');

    public static function getInfoFromCurl($playerId);
}