<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;

use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\base\Exception;

/**
 * Class OriginMetadataRunBase
 * @package app\modules\task\services\originIncrement
 * 这个类给源数据用，源数据的standard表是一个，里面用metadata_type做区分
 * 所以查询和更新的时候 要带着metadata_type
 */
class OriginMetadataRunBase extends OriginRunBase
{

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @param $info
     * @return array
     * @throws BusinessException
     * @throws Exception
     * 这个是set函数，把数据放到对应的standard表，返回diff函数，可以当做基础方法用
     */
    public static function setStandardData($originId, $resourceType, $gameId, $identityId, $info)
    {
        $standardDataActiveTable = self::getStandardActiveTable($resourceType);
        $standardInfo = $standardDataActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
            'metadata_type' => $resourceType,
        ])->one();
        $oldInfo = '';
        if ($standardInfo) {
            $oldInfo = $standardInfo->toArray();
        }
        if (!$standardInfo) {
            $standardInfo = new $standardDataActiveTable();
            $standardInfo->setAttributes(
                [
                    'origin_id' => $originId,
                    'game_id' => $gameId,
                    'rel_identity_id' => (string)$identityId,
                    'metadata_type' => $resourceType,
                ]
            );
        }
        if (isset($info['rel_identity_id'])) {
            $info['rel_identity_id'] = (string)$info['rel_identity_id'];
        }
        if(isset($info['external_id'])){
           $info['external_id'] = (string)$info['external_id'];
        }
        if(isset($info['total_cost'])){
            $info['total_cost'] = (string)$info['total_cost'];
        }
        //csgo武器的一些数据类型处理
        if(isset($info['ammunition'])){
            $info['ammunition'] = (string)$info['ammunition'];
        }
        if(isset($info['capacity'])){
            $info['capacity'] = (string)$info['capacity'];
        }
        if(isset($info['cost'])){
            $info['cost'] = (string)$info['cost'];
        }
        if(isset($info['kill_award'])){
            $info['kill_award'] = (string)$info['kill_award'];
        }
        if(isset($info['kind'])){
            $info['kind'] = (string)$info['kind'];
        }
        $standardInfo->setAttributes($info);
        if (!$standardInfo->save()) {
            throw new BusinessException($standardInfo->getErrors(), '保存log失败');
        }
        $newInfo = $standardInfo->toArray();

        return Common::getDiffInfo($oldInfo, $newInfo);
    }

    /**
     * @param $identityId
     * @param string $originId
     * @param string $resourceType
     * @param string $gameId
     * @return array|object|string
     * @throws Exception
     * 获取对应的standard数据，可以当做基础方法用
     */
    public static function getInfoFromStandard($identityId, $originId = '', $resourceType = '', $gameId = '')
    {
        $standardActiveTable = self::getStandardActiveTable($resourceType);
        $standardInfo = $standardActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
            'metadata_type' => $resourceType,
        ])->one();
        if ($standardInfo) {
            return $standardInfo->toArray();
        }
    }
}