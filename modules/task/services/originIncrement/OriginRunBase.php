<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\StandardDataService;
use app\modules\task\services\TaskInterface;
use app\rest\exceptions\BusinessException;
use yii\base\Exception;

class OriginRunBase implements TaskInterface
{
    protected static $tag;

    /**
     * @param $tagInfo
     * @param $taskInfo
     * @return array
     * @throws BusinessException
     * @throws Exception
     * 这个是示例文件，需要重写
     */
    public static function run($tagInfo, $taskInfo)
    {
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        $gameId = $tagInfo['ext_type'];

        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);
        if ($diffInfo["changed"]) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                $originType,
                $resourceType,
                $diffInfo["change_type"],
                "");
            $changeBody = $diffInfo;
            $queueInfos = [];
            $queueInfos[] = [
                "tag" => $tag,
                "params" => $changeBody,
            ];
            return $queueInfos;
        }
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
//        $infoFormHotInfo = Common::getHotLog($originId, $resourceType, $gameId, $identityId);
        $keyMap = [
            'nick_name' => 'name',
            'player_id' => 'id',
            'name' => function ($params) {
                return $params['first_name'] . " " . $params['last_name'];
            },
            'birthday' => 'birthday',
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $params['current_videogame']['id']);
                return $gameInfo['id'];
            },
            'rel_identity_id' => 'id',
            'image' => 'image_url',
            'nationality' => function ($params) {
                $country = EnumCountry::getNationalityByKey('two', $params['nationality']);
                return $country['id'];
            },
            'role' => function ($params) {
                // 根据对应关系取id
            },
            'slug' => 'slug',
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @param $info
     * @return array
     * @throws BusinessException
     * @throws Exception
     * 这个是set函数，把数据放到对应的standard表，返回diff函数，可以当做基础方法用
     */
    public static function setStandardData($originId, $resourceType, $gameId, $identityId, $info)
    {
        $standardDataActiveTable = self::getStandardActiveTable($resourceType);
        $standardInfo = $standardDataActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
        ])->one();
        $oldInfo = '';
        if ($standardInfo) {
            $oldInfo = $standardInfo->toArray();
        }

        if (!$standardInfo) {
            $standardInfo = new $standardDataActiveTable();
            $standardInfo->setAttributes(
                [
                    'origin_id' => $originId,
                    'game_id' => $gameId,
                    'rel_identity_id' => (string)$identityId
                ]
            );
        }
        if ($info['rel_identity_id']) {
            $info['rel_identity_id'] = (string)$info['rel_identity_id'];
        }
        if (isset($info['number_of_games'])) {
            $info['number_of_games'] = (string)$info['number_of_games'];
        }
        if (isset($info['team_1_score'])) {
            $info['team_1_score'] = (string)$info['team_1_score'];
        }
        if (isset($info['team_2_score'])) {
            $info['team_2_score'] = (string)$info['team_2_score'];
        }
        $standardInfo->setAttributes($info);
        if (!$standardInfo->save()) {
            throw new BusinessException($standardInfo->getErrors(), '保存log失败');
        }
        return Common::getDiffInfo($oldInfo, $standardInfo->toArray());
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @param $info
     * @return array
     * @throws BusinessException
     * @throws Exception
     * 设置standard_data_tournament
     */
    public static function setStandardDataTournament($originId, $resourceType, $gameId, $identityId, $info)
    {
        $tournamentType=isset($info['type'])?$info['type']:1;
        $standardDataActiveTable = self::getStandardActiveTable($resourceType);
        $standardInfo = $standardDataActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
            'type' => $tournamentType,
        ])->one();
        $newInfo = $info;
        $oldInfo = '';
        if ($standardInfo) {
            $oldInfo = $standardInfo->toArray();
        }

        if (!$standardInfo) {
            $standardInfo = new $standardDataActiveTable();
            $standardInfo->setAttributes(
                [
                    'origin_id' => $originId,
                    'game_id' => $gameId,
                    'rel_identity_id' => (string)$identityId,
                    'type' => $tournamentType,
                ]
            );
        }
        if ($info['rel_identity_id']) {
            $info['rel_identity_id'] = (string)$info['rel_identity_id'];
        }
        if (isset($info['number_of_games'])) {
            $info['number_of_games'] = (string)$info['number_of_games'];
        }
        if (isset($info['team_1_score'])) {
            $info['team_1_score'] = (string)$info['team_1_score'];
        }
        if (isset($info['team_2_score'])) {
            $info['team_2_score'] = (string)$info['team_2_score'];
        }
        $standardInfo->setAttributes($info);
        if (!$standardInfo->save()) {
            throw new BusinessException($standardInfo->getErrors(), '保存log失败');
        }
        return Common::getDiffInfo($oldInfo, $standardInfo->toArray());
    }
    /**
     * @param $identityId
     * @param string $originId
     * @param string $resourceType
     * @param string $gameId
     * @return array|object|string
     * @throws Exception
     * 获取对应的standard数据，可以当做基础方法用
     */
    public static function getInfoFromStandard($identityId, $originId = '', $resourceType = '', $gameId = '')
    {
        $standardActiveTable = self::getStandardActiveTable($resourceType);
        $standardInfo = $standardActiveTable::find()->where([
            'origin_id' => $originId,
            'rel_identity_id' => $identityId,
            'game_id' => $gameId,
        ])->one();
        if ($standardInfo) {
            return $standardInfo->toArray();
        }
    }

    /**
     * @param $identityId
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @return mixed
     * 获取数据源数据，可以当做基础方法用
     */
    public static function getOriginInfo($identityId, $originId, $resourceType, $gameId)
    {
        return Common::getHotLog($originId, $resourceType, $gameId, $identityId);
    }

    /**
     * @param $resourceType
     * @return \yii\db\ActiveRecord
     * @throws Exception
     * 这里是根据资源类型获取对应的对象，可以当做基础方法被调用
     */
    public static function getStandardActiveTable($resourceType)
    {
        return StandardDataService::getStandardActiveTable($resourceType);
    }

    public static function getSubStep($diffInfo, $originType, $resourceType, $gameId = '',$batchId = "")
    {
        if ($diffInfo["changed"]) {
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                $originType,
                $resourceType,
                $diffInfo["change_type"],
                "");
            $changeBody = $diffInfo;
            $queueInfos = [];
            $queueInfos[] = [
                "tag" => $tag,
                "batch_id"=>$batchId,
                "params" => $changeBody,
            ];
            return $queueInfos;
        }
    }
}