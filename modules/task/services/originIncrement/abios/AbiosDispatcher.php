<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;

use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;
use app\modules\task\services\QueueServer;
use app\modules\common\services\Consts;
class AbiosDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:  //赛事
            case Consts::RESOURCE_TYPE_SON_TOURNAMENT:
            case Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT:
                $tasks = Tournament::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_AB_STAGE:
                $tasks = Stage::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_SUBSTAGE:
                $tasks = SubStage::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                $tasks = Match::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks = Player::run($tagInfo, $taskInfo);
                break;

                // 暂时没有
//            case QueueServer::QUEUE_RESOURCE_TALENT:
//                $tasks = MetadataTalent::run($tagInfo,$taskInfo);
//                break;
            case QueueServer::QUEUE_RESOURCE_TEAM:  //战队
                $tasks = Team::run($tagInfo, $taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_HERO:     //dota2_hero
                $tasks = DotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ABILITY:     //dota2_ability
                $tasks = DotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:     //dota2_item
                $tasks = DotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:     //lol_champion
                $tasks = LOLItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:     //lol_item
                $tasks = LOLItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_CSGO_WEAPON:     //cs武器
                $tasks = CSWeapon::run($tagInfo,$taskInfo);
                break;
//            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:     //lol召唤师技能
//                $tasks = DotaItem::run($tagInfo,$taskInfo);
//                break;
            default:
                return [];
//                break;
                throw new TaskUndefinedHandleException('AbiosDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}