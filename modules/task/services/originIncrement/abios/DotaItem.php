<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;

use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginMetadataRunBase;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\rest\exceptions\BusinessException;
use yii\base\Exception;

class DotaItem extends OriginMetadataRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        if($resourceType==Consts::METADATA_TYPE_DOTA2_ITEM){  //dota道具数据转换
            $keyMap = [
                'rel_identity_id' => 'id',
                'name' => 'name',
                'image' => function($params){
                    return $params['image']['default'];
                },
                'external_id' =>'external_id',
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['metadata_type'] = $resourceType;
        }
        if($resourceType==Consts::METADATA_TYPE_DOTA2_ABILITY){  //dota技能
            $keyMap = [
                'rel_identity_id' => 'id',
                'name' => 'name',
                'image' => function($params){
                    return $params['images']['default'];
                },
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['metadata_type'] = $resourceType;
        }
        if($resourceType==Consts::METADATA_TYPE_DOTA2_TALENT){  //dota天赋
            $keyMap = [
                'rel_identity_id' => 'id',
                'external_name' => 'name',
                'image' => function($params){
                    return $params['images']['default'];
                },
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['metadata_type'] = $resourceType;
        }
        if($resourceType==Consts::METADATA_TYPE_DOTA2_HERO){  //dota英雄
            $keyMap = [
                'rel_identity_id' => 'id',
                'title' => 'name',
                'name' => 'name',
                'external_id' => 'external_id',
                'image' => function($params){
                    return $params['images']['default'];
                },
                'small_image' => function($params){
                    return $params['images']['small'];
                },
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['metadata_type'] = $resourceType;
        }
        return $info;
    }
}