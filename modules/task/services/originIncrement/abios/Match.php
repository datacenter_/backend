<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;

use app\modules\common\services\Consts;
use app\modules\task\models\abios\TaskDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'id',
            'description' => 'title',
            'name' => 'title',
            'number_of_games'=>function($params){
                return (string)$params['bestOf'];
            },
            'scheduled_begin_at'=>'start',
            'begin_at'=>'start',
            'end_at'=>function($params){
                return $params['end'];
            },
            'is_rescheduled'=>function($params){
                return $params['postponed_from']?1:2;
            },
            'status'=>function($params){
                return $params['deleted_at']?7:-1;
            },
            'is_battle_detailed'=>function($params){
                return $params['pbp_status']=='expected'?1:2;
            },
            'is_pbpdata_supported'=>function($params){
                return $params['pbp_status']=='expected'?1:2;
            },
            'team_1_score'=>function($params){
                $teamId=$params['seeding']['1'];
                return $params['scores'][$teamId];
            },
            'team_2_score'=>function($params){
                $teamId=$params['seeding']['2'];
                return $params['scores'][$teamId];
            },
            'is_forfeit'=>function($params){
                foreach($params['forfeit'] as $v){
                    if($v){
                        return 1;
                    }
                }
                return 2;
            },
            'team_1_id'=>function($params){
                return (string)$params['seeding']['1'];
            },
            'team_2_id'=>function($params){
                return (string)$params['seeding']['2'];
            },
            'game'=>function($params){
                $gameInfo=Common::getGameByMapperId(Consts::ORIGIN_ABIOS,$params['game']['id']);
                return $gameInfo['id'];
            },
            'game_id'=>function($params){
                $gameInfo=Common::getGameByMapperId(Consts::ORIGIN_ABIOS,$params['game']['id']);
                return $gameInfo['id'];
            },
            'group_id'=>'substage_id',
            'bracket_pos'=>function($params){
                if($params['bracket_pos']){
                    return json_encode($params['bracket_pos']);
                }
            },
            'tournament_id'=>'tournament_id',
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }


//    public static function getInfoFromSource($leagueId)
//    {
//        //这里需要转义，映射，改字段名字
//        $info = TaskDataMatch::find()->where(["id" => $leagueId])->asArray()->one();
//
//        $matchInfo = [];
//        $matchInfo['rel_identity_id'] = $info['id'];
//        $matchInfo['game'] = $info['game'];
//        $matchInfo['begin_at'] = $info['start'];
//        $matchInfo['end_at'] = $info['end'];
//        $matchInfo['original_scheduled_begin_at'] = $info['postponed_from'];
//        $matchInfo['name'] = $info['title'];
//        $matchInfo['number_of_games'] = $info['best_of'];
//        $matchInfo['tournament_id'] = $info['tournament_id'];
//        $matchInfo['deleted_at'] = $info['deleted_at'];
//        $matchInfo['group_id'] = $info['substage_id'];
//        $matchInfo['team_1_id'] = $info['team_1_id'];
//        $matchInfo['team_2_id'] = $info['team_2_id'];
//
//        return $matchInfo;
//    }
}