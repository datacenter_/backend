<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\abios\TaskDataPlayer;
use app\modules\task\models\abios\TaskDataTeam;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;

class RelTeamPlayer extends OriginRunBase
{
    protected static $tag=[
        'origin'=>QueueServer::QUEUE_ORIGIN_ABIOS,
        'resource'=>QueueServer::QUEUE_RESOURCE_PLAYER,
    ];

    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        $playerInfoFromStandard = self::getInfoFromStandard($sourceIdentityId);
        $playerInfoFromSource = self::getInfoFromSource($sourceIdentityId);
        $changeInfo = Common::getDiffInfo($playerInfoFromStandard, $playerInfoFromSource, array_diff(array_keys($playerInfoFromSource),['player_id']));

        if ($playerInfoFromStandard) {
            if (!$changeInfo['changed']) {
                return [];
            }
            $newInfo=self::updateInfo($playerInfoFromSource, $sourceIdentityId);
        } else {
            $newInfo=self::addInfo($playerInfoFromSource);
        }

        $changeType = $changeInfo["change_type"];
        //添加队列
        if (isset(self::$tag['origin'])) {
            $originName = self::$tag['origin'];
        } else {
            throw new TaskException("未定义origin");
        }
        if (isset(self::$tag['resource'])) {
            $resourceName = self::$tag['resource'];
        } else {
            throw new TaskException("未定义resource");
        }

        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originName,
            $resourceName,
            $changeType,
            "");
        $changeBody = [
            "new" => $newInfo,
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos = [];
        $queueInfos[] = [
            "tag" => $tag,
            "params" => $changeBody,
        ];
        return $queueInfos;
    }

    protected static function getInfoFromStandard($playerId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        return StandardDataTeam::find()->where(["rel_identity_id" => $playerId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    protected static function getInfoFromSource($id)
    {
        $keyMap=[
            'id'=>'id',
            'name'=>'name',
            'short_name'=>'short_name',
            'deleted_at'=>'deleted_at',
            'game_id'=>function($params){
                // 根据对应关系取id
                return $params["game"]?$params["game"]:0;
            },
            'rel_identity_id'=>'id',
            'logo'=>function($params){
                $imgJson = $params["images"];
                $image=json_decode($imgJson,true);
                return $image["default"]?$image["default"]:"";
            },
            'country'=>function($params){
                $country = EnumCountry::getNationalityByKey('e_name',$params['country']);
                return $country?$country['id']:0;
            },
            'slug'=>'slug',
        ];
        //这里需要转义，映射，改字段名字
        $playerFromOrigin = TaskDataTeam::find()->where(["id" => $id])->asArray()->one();
        $playerInfo=Mapping::transformation($keyMap,$playerFromOrigin);

        return $playerInfo;
    }

    protected static function updateInfo($info,$identityId)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $player = StandardDataTeam::find()->where(["rel_identity_id" => $identityId, "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_identity_id"] = (string)$identityId;
        $atts["origin_id"] = (string)$originInfo["id"];
        $player->setAttributes($atts);
        if(!$player->save()){
            print_r($player->getErrors());
            throw new BusinessException($player->getErrors(),"添加失败");
        }
        return $player->toArray();
    }

    protected static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $atts = $info;
        $atts["rel_identity_id"] = $info['id'];
        $atts["origin_id"] = $originInfo["id"];
        $player = new StandardDataTeam();
        $player->setAttributes($atts);
        if(!$player->save()){
            throw new BusinessException($player->getErrors(),"添加失败");
        }
        return $player->toArray();
    }
}