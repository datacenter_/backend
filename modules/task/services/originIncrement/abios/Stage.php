<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\abios\TaskDataStage;
use app\modules\task\models\abios\TaskDataTournament;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataStage;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\models\TaskDataFeijingTournament;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class Stage implements TaskInterface
{
    protected static $tag=[
        'origin'=>QueueServer::QUEUE_ORIGIN_ABIOS,
        'resource'=>QueueServer::QUEUE_RESOURCE_AB_STAGE,
    ];
    public static function run($tagInfo, $taskInfo)
    {

        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $stageId = $bodyInfo["new"]["id"];

        $stageInfoFromStandard = self::getInfoFromStandard($stageId);
        $stageInfoFromSource = self::getInfoFromSource($stageId);
        $changeInfo = Common::getDiffInfo($stageInfoFromStandard, $stageInfoFromSource, array_diff(array_keys($stageInfoFromSource),['id']));
        if ($stageInfoFromStandard) {
            if (!$changeInfo['changed']) {
                return [];
            }
            $newInfo=self::updateInfo($stageInfoFromSource, $stageId);
        } else {
            $newInfo=self::addInfo($stageInfoFromSource);
        }

        $changeType = $changeInfo["change_type"];
        //添加队列
        if (isset(self::$tag['origin'])) {
            $originName = self::$tag['origin'];
        } else {
            throw new TaskException("未定义origin");
        }
        if (isset(self::$tag['resource'])) {
            $resourceName = self::$tag['resource'];
        } else {
            throw new TaskException("未定义resource");
        }

        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originName,
            $resourceName,
            $changeType,
            "");
        $changeBody = [
            "new" => $newInfo,
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos = [];
        $queueInfos[] = [
            "tag" => $tag,
            "params" => $changeBody,
        ];

        return $queueInfos;
    }

    public static function getInfoFromStandard($leagueId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        return StandardDataStage::find()->where(["rel_identity_id" => $leagueId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    public static function getInfoFromSource($leagueId)
    {
        //这里需要转义，映射，改字段名字
        $Info = TaskDataStage::find()->where(["id" => $leagueId])->asArray()->one();

        $stageInfo = [];
        $stageInfo['rel_identity_id'] = $Info['id'];
        $stageInfo['tournament_id'] = $Info['tournament_id'];
        $stageInfo['name'] = $Info['title'];
        return $stageInfo;
    }
    public static function msecdate($time)
    {
        $tag='Y-m-d H:i:s';
        $a = substr($time,0,10);
        $date = date($tag,$a);
        return $date;
    }
    public static function updateInfo($info,$id)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $stage = StandardDataStage::find()->where(["rel_identity_id" => $id, "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_identity_id"] = $id;
        $atts["origin_id"] = $originInfo["id"];
        $stage->setAttributes($atts);
        $stage->save();
        return $stage->toArray();
    }

    public static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $atts = $info;
        $atts["origin_id"] = $originInfo["id"];
        $stage = new StandardDataStage();
        $stage->setAttributes($atts);
        $re = $stage->save();
        if ($stage->getErrors()) {
            print_r($stage->getErrors());
        }
        return $stage->toArray();
    }
}