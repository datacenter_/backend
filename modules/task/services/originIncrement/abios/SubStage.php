<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\abios\TaskDataSubstages;
use app\modules\task\models\abios\TaskDataTournament;
use app\modules\task\models\StandardDataGroup;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\models\TaskDataFeijingTournament;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class SubStage implements TaskInterface
{
    protected static $tag=[
        'origin'=>QueueServer::QUEUE_ORIGIN_ABIOS,
        'resource'=>QueueServer::QUEUE_RESOURCE_SUBSTAGE,
    ];
    public static function run($tagInfo, $taskInfo)
    {

        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $groupId = $bodyInfo["new"]["id"];

        $groupInfoFromStandard = self::getInfoFromStandard($groupId);
        $groupInfoFromSource = self::getInfoFromSource($groupId);
        $changeInfo = Common::getDiffInfo($groupInfoFromStandard, $groupInfoFromSource, array_diff(array_keys($groupInfoFromSource),['id']));
        if ($groupInfoFromStandard) {
            if (!$changeInfo['changed']) {
                return [];
            }
            $newInfo=self::updateInfo($groupInfoFromSource, $groupId);
        } else {
            $newInfo=self::addInfo($groupInfoFromSource);
        }

        $changeType = $changeInfo["change_type"];
        //添加队列
        if (isset(self::$tag['origin'])) {
            $originName = self::$tag['origin'];
        } else {
            throw new TaskException("未定义origin");
        }
        if (isset(self::$tag['resource'])) {
            $resourceName = self::$tag['resource'];
        } else {
            throw new TaskException("未定义resource");
        }

        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originName,
            $resourceName,
            $changeType,
            "");
        $changeBody = [
            "new" => $newInfo,
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos = [];
        $queueInfos[] = [
            "tag" => $tag,
            "params" => $changeBody,
        ];

        return $queueInfos;
    }

    public static function getInfoFromStandard($leagueId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        return StandardDataGroup::find()->where(["rel_identity_id" => $leagueId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    public static function getInfoFromSource($leagueId)
    {
        //这里需要转义，映射，改字段名字
        $Info = TaskDataSubstages::find()->where(["id" => $leagueId])->asArray()->one();

        $tournamentInfo = [];
        $tournamentInfo['rel_identity_id'] = $Info['id'];
        $tournamentInfo['name'] = $Info['title'];
        $tournamentInfo['begin_at'] = $Info['start'];
        $tournamentInfo['end_at'] = $Info['end'];
        $tournamentInfo['tournament_id'] = $Info['tournament_id'];
        $tournamentInfo['stage_id'] = $Info['stage_id'];

        return $tournamentInfo;
    }
    public static function msecdate($time)
    {
        $tag='Y-m-d H:i:s';
        $a = substr($time,0,10);
        $date = date($tag,$a);
        return $date;
    }
    public static function updateInfo($info,$id)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $group = StandardDataGroup::find()->where(["rel_identity_id" => $id, "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_identity_id"] = $id;
        $atts["origin_id"] = $originInfo["id"];
        $group->setAttributes($atts);
        $group->save();
        return $group->toArray();
    }

    public static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $atts = $info;
        $atts["origin_id"] = $originInfo["id"];
        $group = new StandardDataGroup();
        $group->setAttributes($atts);
        $re = $group->save();
        if ($group->getErrors()) {
            print_r($group->getErrors());
        }
        return $group->toArray();
    }
}