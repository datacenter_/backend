<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\abios\TaskDataPlayer;
use app\modules\task\models\abios\TaskDataTeam;
use app\modules\task\models\abios\TeamPlayerRelation;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataRelTeamPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;

class Team extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'name'=>'name',
            'short_name'=>'short_name',
            'rel_identity_id'=>'id',
            'deleted_at'=>function($params){
                if($params['deleted_at']){
                    return date('Y-m-d H:i:s',strtotime($params['deleted_at']));
                }
            },
            'logo'=>function($params){
                return $params['images']['default'];
            },
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $params['game']['id']);
                return $gameInfo['id'];
            },
            'country' => function ($params) {
               if (isset($params['country']['short_name'])){
                   $countryInfo = EnumService::getCountryByTwo($params['country']['short_name']);
                   return $countryInfo['id'];
               }
            },
            'players'=>function($params){
                $result =[];
                foreach ($params['players'] as $value){
                    $result[] = $value['id'];
                }
                return json_encode($result);
            },
            'history_players'=>function($params){
               if(isset($params['rosters'])){
                   $roster_players=[];
                   foreach ($params['rosters'] as $values){
                       foreach ($values['roster']['players'] as $val){
                           $roster_players[$val['id']] = $val['id'];
                       }
                   }
                   $result = [];
                   foreach ($params['players'] as $value){
                         $result[$value['id']] = $value['id'];
                     }
                   $history_players = [];
                   foreach ($roster_players as $key => $val)
                   {
                       if(!in_array($val,$result))
                       {
                           $history_players[] = $val;
                       }
                   }
//                   return implode(",",$history_players);
                   return json_encode($history_players);
               }
            }
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if($playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }
        return $playerInfo;
    }
}