<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;

use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\org\services\TeamService;
use app\modules\task\models\abios\TaskDataTournament;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\RequestParamsUnvalidatedException;

class Tournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        if ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT) {
            $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

            // 设置标准表，把顶层的赛事信息存入stand表
            $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

            // 返回子任务
            $substep[] = self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
            //循环处理二级子赛事
            if (isset($bodyInfo['new']['stages'])) {
                foreach ($bodyInfo['new']['stages'] as $stages) {
                    // 获取数据源数据并且转成标准化数据

                    $transInfo = self::getsonFromSource($sourceIdentityId, $stages);  //$sourceIdentityId为parent_id
                    $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $transInfo['rel_identity_id'], $transInfo);

                    // diffInfo是格式化数据，这个方法根据返回子任务
                    $substep[] = self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
                }
            }
        }
        if ($resourceType == Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT) {
            $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

            $diffInfo = self::setStandardDataTournament($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);
            // diffInfo是格式化数据，这个方法根据返回子任务
            $substep[] = self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
        }
        return $substep;
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        if ($resourceType == Consts::RESOURCE_TYPE_TOURNAMENT) {
            $keyMap = [
                'rel_identity_id' => 'id',
                'begin_match_date' => function ($params) {
                    if (isset($params['start'])) {
                        return date('Y-m-d H:i:s', strtotime($params['start']));
                    }
                },
                'end_match_date' => function ($params) {
                    if (isset($params['end'])) {
                        return date('Y-m-d H:i:s', strtotime($params['end']));
                    }
                },
                'introduction' => 'description',
                'name' => 'title',
                'match_short_name' => 'short_title',
                'country' => function ($params) {
                   if(isset($params['country']['short_name'])){
                       $countryInfo = EnumService::getCountryByTwo($params['country']['short_name']);
                       return $countryInfo['id'];
                   }
                },
                'format' => 'format',
                'tournament_status' =>'deleted_at',
                'prize_bonus' => function ($params) {
                    if (isset($params['prizepool_string']['total'])) {
                        return $params['prizepool_string']['total'];
                    }
                },
                'logo' => function ($params) {
                    return $params['images']['default'];
                },
//                'game_id' => function ($params) {
//                    // 根据对应关系取id
//                    $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $params['game']['id']);
//                    return $gameInfo['id'];
//                },
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['type']='1';
        }
        if ($resourceType == Consts::RESOURCE_TYPE_SON_TOURNAMENT) {
            return;
        }
        if ($resourceType == Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT) {
            $keyMap = [
                'rel_identity_id' => 'id',
                'begin_match_date' => function ($params) {
                    if (isset($params['start'])) {
                        return date('Y-m-d H:i:s', strtotime($params['start']));
                    }
                },
                'end_match_date' => function ($params) {
                    if (isset($params['end'])) {
                        return date('Y-m-d H:i:s', strtotime($params['end']));
                    }
                },
                'parent_id' => 'stage_id',
                'introduction' => 'phase',
                'name' => 'title',
                'tournament_status' =>'deleted_at',
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['type']='3';
        }
        return $info;
    }

    public static function getsonFromSource($identityId, $stages)
    {
        $info['rel_identity_id'] = $stages['id'];
        $info['name'] = $stages['title'];
        $info['parent_id'] = $identityId;
        $info['type']='2';
        return $info;
    }
}