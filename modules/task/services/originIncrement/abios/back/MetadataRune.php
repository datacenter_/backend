<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\data\models\StandardDataMetadata;
use app\modules\task\models\abios\TaskDataRune;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class MetadataRune implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $teamId = $bodyInfo["new"]["id"];
        $type = $tagInfo['ext_type'];
        $teamInfoFromStandard = self::getInfoFromStandard($teamId,$type);
        $teamInfoFromSource = self::getInfoFromSource($teamId);

        if ($teamInfoFromStandard) {
            $new=self::updateInfo($teamInfoFromSource);
        } else {
            $new=self::addInfo($teamInfoFromSource);
        }

        $changeInfo=Common::getDiffInfo($teamInfoFromStandard,$teamInfoFromSource,array_keys($teamInfoFromSource),['id']);
        $changeType=$changeInfo["change_type"];
        //添加队列
        $queueInfos=[];
        if($changeInfo["changed"]){
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                QueueServer::QUEUE_RESOURCE_METADATA,
                $changeType,
                Consts::METADATA_TYPE_LOL_CHAMPION);
            $changeBody = [
                "new" => $new,
                "old" => $changeInfo['old'],
                "diff" => $changeInfo['diff']
            ];

            $queueInfos[]=[
                "tag"=>$tag,
                "params"=>$changeBody,
            ];
        }
        return $queueInfos;
    }

    public static function getInfoFromStandard($id,$type)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $originId=$originInfo['id'];
        return StandardDataMetadata::find()->where(['rel_identity_id'=>$id,'metadata_type'=>$type,'origin_id'=>$originId])->asArray()->one();
    }

    public static function getInfoFromSource($id)
    {
        //这里需要转义，映射，改字段名字
        $infoFormDb= TaskDataRune::find()->where(["id" => $id])->asArray()->one();
        $keyMap=[
            'rel_identity_id'=>'id',
//            'image'=>'image_url',
//            'slug'=>function($params){
//                return str_replace(" ","_",$params['name']);
//            },
            'name'=>'name',
            'game_id'=>'game_id',
            'metadata_type'=>function($params){
                return Consts::METADATA_TYPE_LOL_RUNE;
            },
            'origin_id'=>function($params){
                $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
                return $originInfo['id'];
            }
        ];
        $map=Mapping::transformation($keyMap,$infoFormDb);

        return $map;

    }

    public static function updateInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("ABIOS");
        $team = StandardDataMetadata::find()->where(["rel_identity_id" => $info["rel_identity_id"], "origin_id" => $info["origin_id"], 'metadata_type'=>Consts::METADATA_TYPE_LOL_CHAMPION])->one();
        $team->setAttributes($info);
        $team->save();
        return $team->toArray();
    }

    public static function addInfo($info)
    {
        $atts = $info;
        $team = new StandardDataMetadata();
        $team->setAttributes($atts);
        $re = $team->save();
        if ($team->getErrors()) {
            print_r($team->getErrors());
        }
        return $team->toArray();
    }
}