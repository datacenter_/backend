<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\data\models\StandardDataMetadata;
use app\modules\task\models\pandascore\TaskDataHero;
use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\pandascore\TaskDataSkill;
use app\modules\task\models\pandascore\TaskDataTalent;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class MetadataTalent implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $Id = $bodyInfo["new"]["id"];
        $InfoFromStandard = self::getInfoFromStandard($Id);
        $InfoFromSource = self::getInfoFromSource($Id);

        if ($InfoFromStandard) {
            $new=self::updateInfo($InfoFromSource);
        } else {
            $new=self::addInfo($InfoFromSource);
        }

        $changeInfo=Common::getDiffInfo($InfoFromStandard,$InfoFromSource,array_keys($InfoFromSource),['id']);
        $changeType=$changeInfo["change_type"];
        //添加队列
        $queueInfos=[];
        if($changeInfo["changed"]){
            $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
                QueueServer::QUEUE_ORIGIN_PANDASCORE,
                QueueServer::QUEUE_RESOURCE_METADATA,
                $changeType,
                Consts::METADATA_TYPE_DOTA2_TALENT);
            $changeBody = [
                "new" => $new,
                "old" => $changeInfo['old'],
                "diff" => $changeInfo['diff']
            ];

            $queueInfos[]=[
                "tag"=>$tag,
                "params"=>$changeBody,
            ];
        }
        return $queueInfos;
    }

    public static function getInfoFromStandard($id)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("pandascore");
        $originId=$originInfo['id'];
        return StandardDataMetadata::find()
            ->select('metadata_type,rel_identity_id,origin_id,game_id,name,image')
            ->where(['rel_identity_id'=>$id,'metadata_type'=>Consts::METADATA_TYPE_DOTA2_TALENT,'origin_id'=>$originId])->asArray()->one();
    }

    public static function getInfoFromSource($id)
    {
        //这里需要转义，映射，改字段名字
        $infoFormDb= TaskDataTalent::find()->where(["id" => $id])->asArray()->one();
        $keyMap=[
            'rel_identity_id'=>'id',
            'image'=>'image_url',

            'name'=>'name',
            'game_id'=>'game_id',
            'metadata_type'=>function($params){
                if ($params['type'] == 1) {
                    return Consts::METADATA_TYPE_DOTA2_TALENT;
                }
                return Consts::METADATA_TYPE_DOTA2_ABILITY;
            },
            'origin_id'=>function($params){
                $originInfo = TaskTypes::getInstance()->getOriginByName("pandascore");
                return $originInfo['id'];
            }
        ];
        $map=Mapping::transformation($keyMap,$infoFormDb);

        return $map;

    }

    public static function updateInfo($info)
    {
        $data = StandardDataMetadata::find()->where(["rel_identity_id" => $info["rel_identity_id"], "origin_id" => $info["origin_id"], 'metadata_type'=>Consts::METADATA_TYPE_DOTA2_TALENT])->one();
        $data->setAttributes($info);
        $data->save();
        return $data->toArray();
    }

    public static function addInfo($info)
    {
        $atts = $info;
        $data = new StandardDataMetadata();
        $data->setAttributes($atts);
        $re = $data->save();
        if ($data->getErrors()) {
            print_r($data->getErrors());
        }
        return $data->toArray();
    }
}