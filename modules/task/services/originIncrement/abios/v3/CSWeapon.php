<?php


namespace app\modules\task\services\originIncrement\abios\v3;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginMetadataRunBase;

class CSWeapon extends OriginMetadataRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);
        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);

        if($resourceType==Consts::METADATA_TYPE_CSGO_WEAPON){  //Csgo数据转换
            $keyMap = [
                'rel_identity_id' => 'id',
                'name' => 'name',
                'game_id' => function ($params) {
                    // 根据对应关系取id
                    $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $params['game']['id']);
                    return $gameInfo['id'];
                },
                'kind' => function($params){
                    if (!empty($params['subcategory'])){
                        $kind_cn = \app\modules\metadata\models\EnumCsgoWeapon::find()->select(['kind_cn','id'])->where( ['like','abios',$params['subcategory']])->asArray()->one();
                        if (!empty($kind_cn)){
                            return (string)$kind_cn['id'];
                        }


                    }
                    return  null;
                },
                'deleted'=>function(){
                    return (int)2;
                },
                'image' => function($params){
                    if (!empty($params['images'][1]['url'])) {
                        $exname = basename($params['images'][1]['url']);
                        if ($exname != 'fallback.png') {
                            return $params['images'][1]['url'];

                        }
                    }
                    return null;
                },
            ];
            $info = Mapping::transformation($keyMap, $infoFormHotInfo);
            $info['metadata_type'] = $resourceType;
        }

        return $info;
    }



}