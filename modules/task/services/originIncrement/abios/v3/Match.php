<?php


namespace app\modules\task\services\originIncrement\abios\v3;


use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId, $batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);

        if (!empty($infoFormHotInfo['casters'])){
            foreach ($infoFormHotInfo['casters'] as $castk => $castv){
                if (!empty($castv['casterContent'])){
                    foreach ($castv['casterContent'] as $castKK => $castVV){
                        if ($castVV['platform']['name'] == 'Twitch') {
                            $liveUrl = 'https://www.twitch.tv/'.$castVV['username'];
                            $data['streamer'] = $castVV['display_name'] ?: null;
                            $data['title'] = $castVV['status_text'] ?: null;
                            $data['viewers'] = $castVV['viewer_count'] ?: null;
                            if ($castv['primary'] === true) {
                              Common::addStandardDataMatchVideo($liveUrl,'',true,$originId, $gameId, $identityId,Consts::ORIGIN_ABIOS,$data);

                            }else{
                                Common::addStandardDataMatchVideo($liveUrl,'',false,$originId, $gameId, $identityId,Consts::ORIGIN_ABIOS,$data);
                            }


                        }

                    }

                }

            }

        }

        $keyMap = [
            'rel_identity_id' => 'id',
            'description' => 'title',
//            'name' => 'title',
            'number_of_games' => function ($params) {
                return (string)$params['best_of'];
            },
            'group_id' => function ($params) {
                return $params['substage']['id'];
            },
            'tournament_id' => function ($params) {
                return (string)$params['tournament']['id'];
            },
            'status' => function ($params) {
                return Common::getAbiosMatchStatusV3($params['lifecycle']) ?: 1;

            },

            'team_1_id' => function ($params) {
                return (string)$params['participants'][0]['teamId'];
            },
            'team_2_id' => function ($params) {
                return (string)$params['participants'][1]['teamId'];
            },

            'team_1_score' => function ($params) {
                if ($params['lifecycle'] == 'over-forfeited') {

                    if ($params['participants'][0]['forfeit'] === true){
                        return (string)0;
                    }else{
                        return (string)1;
                    }

                }

                return (string)$params['participants'][0]['score'];
            },
            'team_2_score' => function ($params) {
                if ($params['lifecycle'] == 'over-forfeited') {
                    if ($params['participants'][1]['forfeit'] === true){
                        return (string)0;
                    }else{
                        return (string)1;
                    }
                }
                return (string)$params['participants'][1]['score'];
            },

            'winner' => function ($params) {
                if ($params['lifecycle'] == 'over-forfeited') {
                    if ($params['participants'][0]['forfeit']) {
                        return (string)$params['participants'][1]['teamId'];

                    }
                    if ($params['participants'][1]['forfeit']) {
                        return (string)$params['participants'][0]['teamId'];
                    }
                }

                if ($params['participants'][0]['winner']) {
                    return (string)$params['participants'][0]['teamId'];

                }
                if ($params['participants'][1]['winner']) {
                    return (string)$params['participants'][1]['teamId'];

                }

                return null;

            },
            'forfeit' => function ($params) {

//                if ($params['participants'][0]['forfeit']) {
//                    return 1;
//
//                }
//                if ($params['participants'][1]['forfeit']) {
//                    return 1;
//                }

                if ($params['lifecycle'] == 'over-forfeited') {
                    return 1;
                } else {
                    return 2;
                }

            },

            'bracket_pos' => function ($params) {
                if (!empty($params['bracket_position'])) {
                    return json_encode($params['bracket_position']);
                }
                return null;
            },
            'begin_at' => function ($params) {
                //3,2
                $beginNum = Common::getAbiosMatchStatusV3($params['lifecycle']);
                if ($beginNum == 3 || $beginNum == 2) {
                    if (isset($params['start']) && $params['start']) {
                        return date('Y-m-d H:i:s', strtotime($params['start']));
                    }

                }
                return null;
            },
            'scheduled_begin_at' => function ($params) {
//              1,2,3,4,6
                $statusNum = Common::getAbiosMatchStatusV3($params['lifecycle']);
                if ($statusNum == 1 || $statusNum == 4 || $statusNum == 2 || $statusNum == 3 || $params['lifecycle'] == 'deleted') {
                    if (isset($params['start']) && $params['start']) {
                        return date('Y-m-d H:i:s', strtotime($params['start']));
                    }

                }
                return null;
            },

            'original_scheduled_begin_at' => function ($params) {
//                $params['lifecycle'] == 'postponed' && !
                if (!empty($params['postponed_from'])) {
                    return date('Y-m-d H:i:s', strtotime($params['postponed_from']));
                }
                return null;

            },

            'is_rescheduled' => function ($params) {
                return $params['postponed_from'] ? 1 : 2;
            },
            'end_at' => function ($params) {
                if (isset($params['end']) && $params['end']) {
                    return date('Y-m-d H:i:s', strtotime($params['end']));
                }
            },
            'draw' => function ($params) {
                if ($params['lifecycle'] == 'over-draw') {
                    return 1;
                } else {
                    return 2;
                }
            },
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $params['game']['id']);
                return $gameInfo['id'];
            },
            'is_streams_supported' => function ($params) {
                if ($params['streamed']) {
                    return 1;
                }
                return 2;

            },
            'battle_forfeit' => function ($params) {
                $success = [];
                foreach ($params['matches'] as $matchKey => $matchval) {
                    if (!empty($matchval['matchesd']) && $matchval['matchesd']['lifecycle'] == 'over-forfeited') {
                        if ($matchval['matchesd']['participants'][0]['forfeit'] === true) {
                            $matchesTeamId = $matchval['matchesd']['participants'][0]['teamId'];
                        }
                        if ($matchval['matchesd']['participants'][1]['forfeit'] === true) {
                            $matchesTeamId = $matchval['matchesd']['participants'][1]['teamId'];
                        }
                        $success[] = ['battle_order' => $matchval['matchesd']['order'], 'rel_team_id' => $matchesTeamId];

                    }

                }

                return !empty($success) ? json_encode($success) : null;

            },
            'has_incident_report' => function($params){
               if ($params['has_incident_report']) {
                   return 1;
               }else{
                   return 2;
               }

            },
            'default_advantage' => function ($params) {
                foreach ($params['matches'] as $matchKey => $matchval) {
                    if (!empty($matchval['matchesd']) && $matchval['matchesd']['lifecycle'] == 'advantage') {
                        if ($matchval['matchesd']['participants'][0]['winner'] === true) {
                            $matchesTeamId = $matchval['matchesd']['participants'][0]['teamId'];
                        }
                        if ($matchval['matchesd']['participants'][1]['winner'] === true) {
                            $matchesTeamId = $matchval['matchesd']['participants'][1]['teamId'];
                        }
                        return (string)$matchesTeamId;

                    }
                }
                return null;
            },

            'battlesIds' => function ($params) {
                if (!empty($params['matches'])){
                    $num = count($params['matches']);
                    if ($num == 1){
                        return (string)$params['matches'][0]['id'];
                    }
                    $matcheIds = array_column($params['matches'],'id');
                    return implode(',',$matcheIds);
                }
                return null;
            },
            'coverage' => function ($params) {

                if (!empty($params['matches'])){
                    $newCoverage = [];
                    foreach ($params['matches'] as $coverageK => $coverageV){
                        $newCoverage[$coverageV['id']] = $coverageV['coverage'];
                    }
                    return json_encode($newCoverage,320);

                }
                return null;

            },
            'is_battle_detailed'=>function($params){
                if (!empty($params['matches'])){
                    $newCoverage = [];
                    foreach ($params['matches'] as $coverageK => $coverageV){
                        $strCoverage =  json_decode($coverageV['coverage'],true);
                        $battleDetailed = $strCoverage['data']['realtime']['api']['status'];

                        if ($battleDetailed == 'unknown' || $battleDetailed == 'supported' || $battleDetailed == 'available' || $battleDetailed == 'expected' || $battleDetailed == 'possible') {
                            return 1;
                        }
                    }
                }
                return 2;
            },

            'is_pbpdata_supported'=>function($params){
                if (!empty($params['matches'])){
                    $newCoverage = [];
                    foreach ($params['matches'] as $coverageKs => $coverageVs){
                        $strCoverages =  json_decode($coverageVs['coverage'],true);
                        $pbpdataSupported = $strCoverages['data']['realtime']['api']['status'];
                        if ($pbpdataSupported == 'unknown' || $pbpdataSupported == 'supported' || $pbpdataSupported == 'available' || $pbpdataSupported == 'expected' || $pbpdataSupported == 'possible') {
                            return 1;
                        }
                    }
                }
                return 2;
            },


            'deleted_at' => function ($params) {
                if (isset($params['deleted_at']) && $params['deleted_at']) {
                    return date('Y-m-d H:i:s', strtotime($params['deleted_at']));
                }
                return null;
            },
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if (isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']) {
            $playerInfo['deleted'] = 1;
        } else {
            $playerInfo['deleted'] = 2;
        }
        return $playerInfo;
    }

}