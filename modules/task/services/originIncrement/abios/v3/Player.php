<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\abios\v3;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Player extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap=[
            'nick_name'=>'nick_name',
            'name'=>function($params){
                return trim($params['first_name']." ".$params['last_name']);
            },
            'rel_identity_id'=>'id',
            'image' => function($params){
               if(isset($params['images'][0]['url']) && $params['images'][0]['url']){
                   $exname = basename($params['images'][0]['url']);
                   if ($exname != 'fallback.png') {
                       return $params['images'][0]['url'];

                   }
               }
            },
            'nationality'=>function($params){
               if(isset($params['region']['name']) && $params['region']['name']){
                   $countryInfo = EnumCountry::getNationalityByKey('abios',$params['region']['country']['abbreviation']);
                   if($countryInfo){
                       return $countryInfo['id'];
                   }
               }
            },
            'deleted_at'=>function($params){
                if(isset($params['deleted_at']) && $params['deleted_at']){
                    return date('Y-m-d H:i:s',strtotime($params['deleted_at']));
                }
            },
            'role'=>function($params) use ($gameId){
                if(isset($params['role']['id']) && $params['role']['id']){
                    $position = Common::getAbiosPositionByRelId($gameId,$params['role']['id']);
                    if($position){
                        return (string)$position;
                    }
                }
            },
            'team'=>function($params){
               if(isset($params['teams']['0']['id']) && $params['teams']['0']['id']){
                   return (string)$params['teams']['0']['id'];
               }
            },
            'race'=>'race', //待修改
            'game'=>function($params){
                $gameInfo=Common::getGameByMapperId(Consts::ORIGIN_ABIOS,$params['game']['id']);
                return $gameInfo['id'];
            },
            'game_id'=>function($params){
                $gameInfo=Common::getGameByMapperId(Consts::ORIGIN_ABIOS,$params['game']['id']);
                return $gameInfo['id'];
            },
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }else{
            $playerInfo['deleted'] = 2;
            $playerInfo['deleted_at'] = null;
        }
        return $playerInfo;
    }
}