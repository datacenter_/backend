<?php
/**
 *abios赛事增量处理
 */

namespace app\modules\task\services\originIncrement\abios\v3;

use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class SonTournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'name'=>'title',
            'rel_identity_id'=>'id',
            'son_sort'=>function($params){
              return (string)$params['order'];
            },
            'parent_id'=> function($params){
                return (int)$params['tournament']['id'];
            },
            'game_id' =>function() use($gameId){
               return (int)$gameId;
            },
            'deleted_at'=>function($params){
                if(isset($params['deleted_at']) && $params['deleted_at']){
                    return date('Y-m-d H:i:s',strtotime($params['deleted_at']));
                }
            },
            'type'=>function(){
                return (int)2;
            },
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }else{
            $playerInfo['deleted'] = 2;
            $playerInfo['deleted_at'] = null;
        }
        return $playerInfo;
    }
}