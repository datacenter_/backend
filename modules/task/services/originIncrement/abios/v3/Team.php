<?php
/**
 *abios战队origin增量处理
 */

namespace app\modules\task\services\originIncrement\abios\v3;

use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Team extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $refresh2 = $tagInfo['ext2_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId,$refresh2);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId,$refresh2)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'name'=>'name',
            'short_name'=>'abbreviation',
            'rel_identity_id'=>'id',
            'logo'=>function($params){
               if(isset($params['images'][0]['url']) && $params['images'][0]['url'])
                $exname = basename($params['images'][0]['url']);
                if ($exname != 'fallback.png') {
                    return $params['images'][0]['url'];
                }
            },
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_ABIOS, $params['game']['id']);
                return $gameInfo['id'];
            },
            'country' => function ($params) {
               if (isset($params['region']['country']['abbreviation']) && $params['region']['country']['abbreviation']){
                   $countryInfo = EnumCountry::getNationalityByKey('abios',$params['region']['country']['abbreviation']);
                   if($countryInfo){
                       return $countryInfo['id'];
                   }
               }elseif (isset($params['region']['name']) && $params['region']['name']){
                   $countryInfo = EnumCountry::getNationalityByKey('abios',$params['region']['name']);
                   if($countryInfo){
                       return $countryInfo['id'];
                   }
               }
            },
            'region' => function ($params){
                if (isset($params['region']['name']) && $params['region']['name']){
                    $countryInfo = EnumCountry::getNationalityByKey('abios',$params['region']['name']);
                    if($countryInfo){
                        return $countryInfo['id'];
                    }
                }
            },
            'players' => function ($params){
              if(isset($params['players']) && $params['players']){
                  return json_encode($params['players']);
              }
            },
            'history_players' => function ($params){
                if(isset($params['history_players'])){
                    $history_players = null;
                    if($params['history_players']){
                        $history_players = json_encode($params['history_players']);
                    }
                    return $history_players;
                }
            },
            'clan_id' => function($params){
              if(isset($params['organisation']) && $params['organisation']){
                  return (int)$params['organisation']['id'];
              }
            },
            'deleted_at'=>function($params){
                if(isset($params['deleted_at']) && $params['deleted_at']){
                    return date('Y-m-d H:i:s',strtotime($params['deleted_at']));
                }
            },
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
//        if($refresh2 != "history_players"){
//            unset($playerInfo['history_players']);
//        }
        if(isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }else{
            $playerInfo['deleted'] = 2;
            $playerInfo['deleted_at'] = null;
        }
        return $playerInfo;
    }
}