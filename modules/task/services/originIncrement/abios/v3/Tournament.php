<?php
/**
 *abios赛事增量处理
 */

namespace app\modules\task\services\originIncrement\abios\v3;

use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\BusinessException;

class Tournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $batchId = $taskInfo['batch_id'];
        $changeType = $tagInfo['change_type'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardDataTournament($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);
        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'id',
            'scheduled_begin_at' => function ($params) {
                if (isset($params['start']) && $params['start']) {
                    return date('Y-m-d H:i:s', strtotime($params['start']));
                }
            },
            'scheduled_end_at' => function ($params) {
                if (isset($params['end']) && $params['end']) {
                    return date('Y-m-d H:i:s', strtotime($params['end']));
                }
            },
            'name' => 'title',
            'short_name' => 'short_title',
            'country' => function ($params) {
                if(isset($params['location']['host']['country']['abbreviation'])){
                    $countryInfo = EnumCountry::getNationalityByKey('abios',$params['location']['host']['country']['abbreviation']);
                    if($countryInfo){
                        return $countryInfo['id'];
                    }
                } elseif (isset($params['location']['host']['name']) && $params['location']['host']['name']){
                    $countryInfo = EnumCountry::getNationalityByKey('abios',$params['location']['host']['name']);
                    if($countryInfo){
                        return $countryInfo['id'];
                    }
                }
            },
//            'region' => function ($params) {
//                if(isset($params['location']['host']['name'])){
//                    $countryInfo = EnumCountry::getNationalityByKey('abios',$params['location']['host']['name']);
//                    if($countryInfo){
//                        return $countryInfo['id'];
//                    }
//                }
//            },
            'parent_id'=> function(){
                return (int)0;
            },
            'game_id' => function($params){
               if(isset($params['game']['id']) && $params['game']['id']){
                   $gameInfo=Common::getGameByMapperId(Consts::ORIGIN_ABIOS,$params['game']['id']);
                   if($gameInfo){
                       return $gameInfo['id'];
                   }
               }
            },
            'introduction'=>function($params){
               if(isset($params['copy']['general_description']) && $params['copy']['general_description']){
                   return $params['copy']['general_description'];
               }
            },
            'format' => function($params){
               if(isset($params['copy']['format_description']) && $params['copy']['format_description']){
                   return $params['copy']['format_description'];
               }
            },
            'deleted_at' => function($params){
               if(isset($params['deleted_at']) && $params['deleted_at']){
                   return date('Y-m-d H:i:s', strtotime($params['deleted_at']));
               }
            },
            'prize_bonus' => function ($params) {
                if (isset($params['string_prize_pool']['total'])) {
                    preg_match("/(points|spot)/",$params['string_prize_pool']['total'],$pregResult);
                    if(!$pregResult){
                        return $params['string_prize_pool']['total'];
                    }
                }
            },
            'image' => function ($params) use ($gameId){
                if(isset($params['images'][2]['url']) && $params['images'][2]['url']){
                    $image = null;
                    $exname = basename($params['images'][2]['url']);
                    if($exname == 'fallback.png'){
                        $image = null;
                    }
                    elseif($params['images'][2]['url'] == "https://img.abiosgaming.com/events/No-Logo-Abios-Tournament-Square.png"){
                        $image = null;
                    }else{
                        $image = $params['images'][2]['url'];
                    }
                    return $image;
                }
            },
            'tier' => function($params){
               if(isset($params['tier']) && $params['tier']){
                   return (string)$params['tier'];
               }
            },
            'prize_distribution' => function($params){
               if(isset($params['string_prize_pool']) && $params['string_prize_pool']){
                   $result = [];
                   foreach ($params['string_prize_pool'] as $key=>$value){
                       if($key == "first"){
                           preg_match("/(points|spot)/",$value,$pregResult);
                           if(!$pregResult){
                               $trans['rank'] = "first";
                               $trans['price'] = $value;
                               $trans['num'] = null;
                               $trans['team_id'] = null;
                               $result[] = $trans;
                           }
                       }
                       if($key == "second"){
                           preg_match("/(points|spot)/",$value,$pregResult);
                           if(!$pregResult) {
                               $trans['rank'] = "second";
                               $trans['price'] = $value;
                               $trans['num'] = null;
                               $trans['team_id'] = null;
                               $result[] = $trans;
                           }
                       }
                       if($key == "third"){
                           preg_match("/(points|spot)/",$value,$pregResult);
                           if(!$pregResult) {
                               $trans['rank'] = "third";
                               $trans['price'] = $value;
                               $trans['num'] = null;
                               $trans['team_id'] = null;
                               $result[] = $trans;
                           }
                       }
                   }
                   if($result){
                       return json_encode($result);
                   }
               }
            },
            'type'=> function(){
               return (int)1;
            },
            'status' => function($params){
                if(isset($params['start'])){
                    if(date("Y-m-d H:i:s") < date('Y-m-d H:i:s',strtotime($params['start']))){
                        return (int)1;
                    }
                    if( (date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['start'])) && empty($params['end'])) ||
                        (date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['start'])) && date("Y-m-d H:i:s") < date('Y-m-d H:i:s',strtotime($params['end'])) ) ){
                        return (int)2;
                    }
                    if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['end_at']))){
                        return (int)3;
                    }
                }
            },
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }else{
            $playerInfo['deleted'] = 2;
            $playerInfo['deleted_at'] = null;
        }
        return $playerInfo;
    }
}