<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\bayes;

use app\modules\common\services\Consts;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;

class BayesDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case Consts::RESOURCE_TYPE_PLAYER:
                $tasks = Player::run($tagInfo, $taskInfo);
                break;
            case Consts::RESOURCE_TYPE_TEAM:
                $tasks = Team::run($tagInfo, $taskInfo);
                break;
            case Consts::RESOURCE_TYPE_MATCH:
                $tasks = Match::run($tagInfo, $taskInfo);
                break;
            case Consts::RESOURCE_TYPE_TOURNAMENT:
                $tasks = Tournament::run($tagInfo, $taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('PandascoreDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}