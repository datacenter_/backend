<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\bayes;


use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\models\BayesIdentifiers;

class Team extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        $gameId = $tagInfo['ext_type'];
        // 获取tag相关信息
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId,$sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId,$sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType,$gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap=[
            'rel_identity_id'=>'id',
            'perid' => function($params){
                if(isset($params['perid']) && $params['perid']){
                    return (string)$params['perid'];
                }
            },
            'name'=>function($params){
               if(isset($params['name']) && $params['name']){
                   return $params['name'];
               }
            },
            'short_name'=>function($params){
                if(isset($params['abbreviation']) && $params['abbreviation']){
                    return $params['abbreviation'];
                }
            },
            'origin_modified_at' => function($params){
                if(isset($params['last_modified']) && $params['last_modified']){
                    return date('Y-m-d H:i:s',(int)$params['last_modified']);
                }
            },
            'logo' => function($params){
               if(isset($params['media']['logo']) && $params['media']['logo']){
                   return $params['media']['logo'];
               }
            },
            'game_id' => function() use ($gameId){
                if(isset($gameId) && $gameId){
                    return (int)$gameId;
                }
            },
            'deleted'=>function(){
                return (int)2;
            }
        ];
        $transInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($infoFormHotInfo['identifiers']) && $infoFormHotInfo['identifiers']){
            foreach ($infoFormHotInfo['identifiers'] as $value){
                $identifierInfo = BayesIdentifiers::find()->where([
                    'resource_type'=>Consts::RESOURCE_TYPE_TEAM,
                    'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                    'type'=>$value['type'],
                    'value'=> $value['value'],
                ])->one();
                if ($identifierInfo) {
                    continue;
                }else{
                    $identifierInfo = new BayesIdentifiers();
                    $identifierInfo->setAttributes(
                        [
                            'resource_type'=>Consts::RESOURCE_TYPE_TEAM,
                            'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                            'type'=>$value['type'],
                            'value'=> $value['value'],
                        ]
                    );
                    $identifierInfo->save();
                }
            }
        }
        return $transInfo;
    }
}