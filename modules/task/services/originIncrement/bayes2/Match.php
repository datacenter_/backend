<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\bayes2;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\task\models\BayesCoverages;
use app\modules\task\models\BayesIdentifiers;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        $gameId = $tagInfo['ext_type'];
        // 获取tag相关信息
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId,$sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId,$sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType,$gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap=[
            'rel_identity_id'=>'id',
            'perid' => function($params){
                if(isset($params['perid']) && $params['perid']){
                    return (string)$params['perid'];
                }
            },
            'game_id' => function($params){
                 if(isset($params['title']['id']) && $params['title']['id']){
                     $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_BAYES2, $params['title']['id']);
                     if($gameInfo){
                         return $gameInfo['id'];
                     }
                 }
            },
            'game' => function() use ($gameId){
                if($gameId){
                    return (int)$gameId;
                }
            },
            'tournament_id' => function($params){
                 if(isset($params['tournament']['id']) && $params['tournament']['id']){
                     return (string)$params['tournament']['id'];
                 }
            },
            'stage_id' => function($params){
                if(isset($params['stage']['id']) && $params['stage']['id']){
                    return (string)$params['stage']['id'];
                }
            },
            "match_type"=> function($params){
               if(isset($params['match_format']['type']) && $params['match_format']['type']){
                   if($params['match_format']['type'] == "bestof" || $params['match_format']['type'] == "bestOf"){
                       return (string)1;
                   }else{
                       return (string)1;
//                       return $params['match_format']['type'];
                   }
               }
            },
            "number_of_games" => function($params){
                if(isset($params['match_format']['value']) && $params['match_format']['value']){
                    return (string)$params['match_format']['value'];
                }
            },
            'end_at'=>function($params){
                if(isset($params['date_end']) && $params['date_end']){
                    return date('Y-m-d H:i:s',strtotime($params['date_end']));
                }
            },
            'status' => function($params){
                 if(isset($params['state_label']) && $params['state_label']){
                    return Common::getBayesMatchStatus($params['state_label']);
                 }
            },
            'team_1_id'=>function($params){
                 if(isset($params['teams']['0']['id']) && $params['teams']['0']['id']){
                     return (string)$params['teams']['0']['id'];
                 }
            },
            'draw'=>function(){
                return (int)2;
            },
            'forfeit'=>function(){
                return (int)2;
            },
            'team_2_id'=>function($params){
                if(isset($params['teams']['1']['id'])){
                    return (string)$params['teams']['1']['id'];
                }
            },
            'team_1_score'=>function($params){
                if(isset($params['results']['0']) && $params['results']['0']){
                    return (string)$params['results']['0']['score'];
                }
            },
            'team_2_score'=>function($params){
                if(isset($params['results']['1']) && $params['results']['1']){
                    return (string)$params['results']['1']['score'];
                }
            },
            'winner' =>function($params){
               if($params['results']){
                   foreach ($params['results'] as $key=>$value){
                       if($value['winner'] == true){
                           return (string)$value['team_id'];
                       }
                   }
               }
            },
            'origin_modified_at' => function($params){
                if(isset($params['last_modified']) && $params['last_modified']){
                    return date('Y-m-d H:i:s',(int)$params['last_modified']);
                }
            },
            'is_battle_detailed' => function($params){
                if(!empty($params['coverages'])){
                    return 1;
                }else{
                    return 2;
                }
            },
            'is_pbpdata_supported' => function($params){
                if(!empty($params['coverages'])){
                    return 1;
                }else{
                    return 2;
                }
            },
            'deleted'=>function(){
                return (int)2;
            }
        ];
        $info = Mapping::transformation($keyMap, $infoFormHotInfo);
        if($info['status'] == 1){ //未开始
            if(isset($infoFormHotInfo['date_start']) && $infoFormHotInfo['date_start']){
                $info['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
            }
        }
        elseif($info['status'] == 2){  //进行中
            if(isset($infoFormHotInfo['date_start']) && $infoFormHotInfo['date_start']){
                if($info['scheduled_begin_at']){  //比赛未开始的时候抓过该比赛
                    $info['begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                }else{ //比赛进行中第一次抓取
                    $info['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                    $info['begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                }
            }
        }
        elseif($info['status'] == 3){  //已完成
            if(isset($infoFormHotInfo['date_start']) && $infoFormHotInfo['date_start']){
                if($info['begin_at']){  //已完成，并且有实际开始时间（经历过比赛中）
                    $info['begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                }elseif($info['scheduled_begin_at'] && !$info['begin_at']){  //没有经历过进行中的比赛,比赛开始前抓取过
                    $info['begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                }else{
                    $info['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                    $info['begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
                }
            }
        }
        elseif($info['status'] == 4){ //已推迟（只有计划开始时间，没有实际开始时间）
            if(isset($infoFormHotInfo['date_start']) && $infoFormHotInfo['date_start']){
                $info['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
            }
        }
        elseif ($info['status'] == 5){ //已取消(只存计划开始时间)
            if(isset($infoFormHotInfo['date_start']) && $infoFormHotInfo['date_start']){
                $info['scheduled_begin_at'] = date('Y-m-d H:i:s',strtotime($infoFormHotInfo['date_start']));
            }
        }

        if(isset($infoFormHotInfo['identifiers']) && $infoFormHotInfo['identifiers']){
            foreach ($infoFormHotInfo['identifiers'] as $value){
                $identifierInfo = BayesIdentifiers::find()->where([
                    'origin_id'=>10,
                    'resource_type'=>Consts::RESOURCE_TYPE_MATCH,
                    'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                    'type'=>$value['type'],
                    'value'=> $value['value'],
                ])->one();
                if ($identifierInfo) {
                    continue;
                }else{
                    $identifierInfo = new BayesIdentifiers();
                    $identifierInfo->setAttributes(
                        [
                            'origin_id'=>10,
                            'resource_type'=>Consts::RESOURCE_TYPE_MATCH,
                            'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                            'type'=>$value['type'],
                            'value'=> $value['value'],
                        ]
                    );
                    $identifierInfo->save();
                }
            }
        }
        if(isset($infoFormHotInfo['coverages']) && $infoFormHotInfo['coverages']){
            foreach ($infoFormHotInfo['coverages'] as $value){
                $typesString = null;
                if($value['types']){
                    $typesString = implode(",",$value['types']);
                }
                if(!$typesString){
                    continue;
                }
                $coveragesInfo = BayesCoverages::find()->where([
                    'origin_id'=>10,
                    'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                    'provider'=>$value['provider'],
                ])->one();
                if ($coveragesInfo) {
                    $coveragesInfo->setAttributes([
                        'types'=> $typesString
                    ]);
                }else{
                    $coveragesInfo = new BayesCoverages();
                    $coveragesInfo->setAttributes(
                        [
                            'origin_id'=>10,
                            'rel_identity_id'=>(string)$infoFormHotInfo['id'],
                            'provider'=>$value['provider'],
                            'types'=> $typesString,
                        ]
                    );
                }
                $coveragesInfo->save();
            }
        }
        return $info;
    }
}