<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\feijing;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\models\TaskDataFeijingTournament;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class Match implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        // TODO  还没做
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $tournamentId = $bodyInfo["new"]["league_id"];

        $tournamentInfoFromStandard = self::getInfoFromStandard($tournamentId);
        $tournamentInfoFromSource = self::getInfoFromSource($tournamentId);

        if ($tournamentInfoFromStandard) {
            self::updateInfo($tournamentInfoFromSource);
        } else {
            self::addInfo($tournamentInfoFromSource);
        }

        $changeInfo=Common::getDiffInfo($tournamentInfoFromStandard,$tournamentInfoFromSource);

        $changeType=$changeInfo["change_type"];
        //添加队列
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TOURNAMENT,
            $changeType,
            "");
        $changeBody = [
            "new" => $changeInfo['new'],
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos=[];
        if($changeType==QueueServer::QUEUE_TYPE_ADD || !empty($changeBody['diff']) && $changeType == QueueServer::QUEUE_TYPE_CHANGE){
            $queueInfos[]=[
                "tag"=>$tag,
                "params"=>$changeBody,
            ];
        }
        return $queueInfos;
    }

    public static function getInfoFromStandard($leagueId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        return StandardDataTournament::find()->where(["rel_league_id" => $leagueId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    public static function getInfoFromSource($leagueId)
    {
        //这里需要转义，映射，改字段名字
        $FeiJingInfo = TaskDataFeijingTournament::find()->where(["league_id" => $leagueId])->asArray()->one();

        $tournamentInfo = [];
        $tournamentInfo['rel_league_id'] = $FeiJingInfo['league_id'];
        $tournamentInfo['match_name'] = $FeiJingInfo['name_en'];
        $tournamentInfo['match_name_cn'] = $FeiJingInfo['name'];
        $tournamentInfo['tournament_status'] = (integer)$FeiJingInfo['status'];
        $tournamentInfo['begin_match_date'] = self::msecdate($FeiJingInfo['start_time']);
        $tournamentInfo['end_match_date'] = self::msecdate($FeiJingInfo['end_time']);
        $tournamentInfo['match_short_name_cn'] = $FeiJingInfo['short_name'];
        $tournamentInfo['logo'] = $FeiJingInfo['logo'];
        $tournamentInfo['organizer'] = $FeiJingInfo['organizer'];
        $tournamentInfo['address'] = $FeiJingInfo['address'];
        $tournamentInfo['type'] = 1;
        $tournamentInfo['game_id'] = 2;

        return $tournamentInfo;
    }
    public static function msecdate($time)
    {
        $tag='Y-m-d H:i:s';
        $a = substr($time,0,10);
        $date = date($tag,$a);
        return $date;
    }
    public static function updateInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $tournament = StandardDataTournament::find()->where(["rel_league_id" => $info['rel_league_id'], "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_league_id"] = $info['rel_league_id'];
        $atts["origin_id"] = $originInfo["id"];
        $tournament->setAttributes($atts);
        return $tournament->save();
    }

    public static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $atts = $info;
        $atts["origin_id"] = $originInfo["id"];
        $tournament = new StandardDataTournament();
        $tournament->setAttributes($atts);
        $re = $tournament->save();
        if ($tournament->getErrors()) {
            print_r($tournament->getErrors());
        }
        return $re;
    }
}