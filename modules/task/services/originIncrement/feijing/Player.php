<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\feijing;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class Player implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $teamId = $bodyInfo["new"]["team_id"];
        $playerInfoFromStandard = self::getInfoFromStandard($teamId);
        $playerInfoFromSource = self::getInfoFromSource($teamId);

        if ($playerInfoFromStandard) {
            self::updateInfo($playerInfoFromSource,$playerInfoFromSource['team_id']);
        } else {
            self::addInfo($playerInfoFromSource);
        }

        $changeInfo=Common::getDiffInfo($playerInfoFromStandard,$playerInfoFromSource);

        $changeType=$changeInfo["change_type"];
        //添加队列
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TEAM,
            $changeType,
            "");
        $changeBody = [
            "new" => $changeInfo['new'],
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos=[];
        if($changeType==QueueServer::QUEUE_TYPE_ADD){
            $queueInfos[]=[
                "tag"=>$tag,
                "params"=>$changeBody,
            ];
        }
        return $queueInfos;
    }

    public static function getInfoFromStandard($teamId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        return StandardDataPlayer::find()->where(["rel_identity_id" => $teamId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    public static function getInfoFromSource($teamId)
    {
        //这里需要转义，映射，改字段名字
        $FeiJingPlayer = TaskDataFeijingPlayer::find()->where(["team_id" => $teamId])->asArray()->one();

        $playerInfo = [];
        $playerInfo['player_name'] = $FeiJingPlayer['nick_name'];
        $playerInfo['team_id'] = $FeiJingPlayer['team_id'];
        $playerInfo['real_name'] = $FeiJingPlayer['real_name'];
        $playerInfo['logo'] = $FeiJingPlayer['avatar'];
        $playerInfo['introduction'] = $FeiJingPlayer['introduction'];
        // TODO 先暂时都是英雄联盟 查询位置得时候需要游戏id 有的游戏位置名称一样
        $playerInfo['game_id'] = 2;
        // 获取国籍
        $country = EnumCountry::getCountry($FeiJingPlayer['country']);
        $playerInfo['player_country'] = $country['id'] ?? '';
        // 获取选手位置 && 游戏id
        $position = EnumPosition::getPosition($FeiJingPlayer['position'],$playerInfo['game_id']);
        $playerInfo['position_id'] = $position['id'] ?? '';


        return $playerInfo;
    }

    public static function updateInfo($info,$teamId)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $team = StandardDataPlayer::find()->where(["rel_identity_id" => $teamId, "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_identity_id"] = $teamId;
        $atts["origin_id"] = $originInfo["id"];
        $team->setAttributes($atts);
        return $team->save();
    }

    public static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $atts = $info;
        $atts["rel_identity_id"] = $info['team_id'];
        $atts["origin_id"] = $originInfo["id"];
        $player = new StandardDataPlayer();
        $player->setAttributes($atts);
        $re = $player->save();
        if ($player->getErrors()) {
            print_r($player->getErrors());
        }
        return $re;
    }
}