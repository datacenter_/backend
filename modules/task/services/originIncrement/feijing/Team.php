<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\feijing;


use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;

class Team implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $teamId = $bodyInfo["new"]["team_id"];
        $teamInfoFromStandard = self::getInfoFromStandard($teamId);
        $teamInfoFromSource = self::getInfoFromSource($teamId);

        if ($teamInfoFromStandard) {
            self::updateInfo($teamInfoFromSource);
        } else {
            self::addInfo($teamInfoFromSource);
        }

        $changeInfo=Common::getDiffInfo($teamInfoFromStandard,$teamInfoFromSource);
        $changeType=$changeInfo["change_type"];
        //添加队列
        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            QueueServer::QUEUE_ORIGIN_FEIJING,
            QueueServer::QUEUE_RESOURCE_TEAM,
            $changeType,
            "");
        $changeBody = [
            "new" => $changeInfo['new'],
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos=[];
        if($changeType==QueueServer::QUEUE_TYPE_ADD){
            $queueInfos[]=[
                "tag"=>$tag,
                "params"=>$changeBody,
            ];
        }
        return $queueInfos;
    }

    public static function getInfoFromStandard($teamId)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        return StandardDataTeam::find()->where(["rel_identity_id" => $teamId, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    public static function getInfoFromSource($teamId)
    {
        //这里需要转义，映射，改字段名字
        return TaskDataFeijingTeam::find()->where(["team_id" => $teamId])->asArray()->one();
    }

    public static function updateInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $team = StandardDataTeam::find()->where(["rel_identity_id" => $info["team_id"], "origin_id" => $originInfo["id"]])->one();
        $team->setAttributes($info);
        return $team->save();
    }

    public static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("feijing");
        $atts = $info;
        $atts["rel_identity_id"] = $info["team_id"];
        $atts["origin_id"] = $originInfo["id"];
        $team = new StandardDataTeam();
        $team->setAttributes($atts);
        $re = $team->save();
        if ($team->getErrors()) {
            print_r($team->getErrors());
        }
        return $re;
    }
}