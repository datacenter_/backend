<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\hltv;
use app\modules\task\services\originIncrement\hltv\Team;
use app\modules\task\services\QueueServer;

class HltvDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType){
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks = Player::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TEAM:
                $tasks = Team::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                $tasks = Match::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
                $tasks = Tournament::run($tagInfo,$taskInfo);
                break;
        }
        return $tasks;
    }
}