<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\hltv;

use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["matche_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            //bug编号494
            'rel_identity_id' => 'matche_id',
            'match_type' => function(){
               return "1";
            },
            'origin_id' => function(){
                return "7";
            },
            //Common::getMatchType(Consts::Hltv,$params['match_type']);
            'game_id' => function ($params) {
                // 根据对应关系取id
                return 1;
            },
//            'game' => function ($params) {
//                // 根据对应关系取id
//                return 1;
//            },
            'status' => function($params){
              return Common::getHLTVMatchStatus($params);
            },
            'begin_at' => function($params){
                if(isset($params['match_time']) && $params['match_time']){
                    return date("Y-m-d H:i:s",$params['match_time']);
                }
            },
            'end_at' => function($params){
                if(isset($params['end_time']) && $params['end_time']){
                    return date("Y-m-d H:i:s",$params['end_time']);
                }
            },

            'default_advantage' => function ($params) {
                if (empty($params['description'])) {
                    return null;
                }
                $descriptionLtrim = ltrim($params['description'], "* ");
                $descriptionstr = preg_replace("/(>>>\*+)/", " * ", $descriptionLtrim);
                $descriptionArr = explode('*', $descriptionstr);
                $advantage = '';
                foreach ($descriptionArr as $key => $val) {
                    if (strpos($val, 'advantage') !== false) {
                        $advantage = $val;
                    }
                }
                if (!empty($advantage)) {
                    $team_one = StandardDataTeam::find()->select(['name'])->where(['rel_identity_id' => $params['team_one']])->asArray()->one();
                    $team_two = StandardDataTeam::find()->select(['name'])->where(['rel_identity_id' => $params['team_two']])->asArray()->one();
                    if (strpos($advantage, $team_one['name'])!== false && strpos($params['maps'],'Default') === 0) {
                        return $default_advantage = $params['team_one'];
                    }

                    if (strpos($advantage, $team_two['name']) !== false && strpos($params['maps'],'Default') === 0) {
                        return $default_advantage = $params['team_two'];
                    }
                }

                return null;

            },

            "deleted" => function($params) {
               if ($params['status'] == "Match deleted") {
                return 1;
              }else{
                   return  2;
               }
            },

            'forfeit' => function($params) {
            //比赛级弃权

                if (empty($params['description'])) {
                    return 2;
                }
                $descriptionLtrim = ltrim($params['description'], "* ");
                $descriptionstr = preg_replace("/(>>>\*+)/", " * ", $descriptionLtrim);
                $descriptionArr = explode('*', $descriptionstr);
                $forfeitarrCC = '';
                foreach ($descriptionArr as $key => $val) {
                    $patterns = "/.*forfeit.*the\smatch.*/";
                    preg_match_all($patterns,$val,$arr);
                    if (!empty($arr[0]) && $params['status'] == "Match over"){
                        return 1;
                    }

                    $patterns2 = "/.*withdrew from the event.*/";
                    preg_match_all($patterns2,$val,$arr2);
                    if (!empty($arr2[0]) && $params['status'] == "Match over"){
                        return 1;
                    }

                    $forfeitPreg = "/.*forfeit.*/";
                    preg_match_all($forfeitPreg,$val,$forfeitarr);
                    if (!empty($forfeitarr[0]) && $params['status'] == "Match over" &&  $params['number_of_games'] == '1'){
                        return 1;
                    }
                    $forfeitarrCC = $forfeitarr[0];
                }

                if (!empty($forfeitarrCC)) {
                    $mapsArr = explode('--',$params['maps']);
                    $uniquemaps= array_unique($mapsArr);
                    foreach ($uniquemaps as $mapKey => $mapVal) {
                        $forfeitMap = strpos($forfeitarrCC[0], 'map');
                        if ($forfeitMap === false && $params['number_of_games'] != '1' && $params['status'] == "Match over") {
                            return  1;
                        }
                    }
                }

                return 2;
            },

            //单图弃权
            'battle_forfeit' => function($params) {
                //对局

                if (empty($params['description'])) {
                    return null;
                }
                $descriptionLtrim = ltrim($params['description'], "* ");
                $descriptionstr = preg_replace("/(>>>\*+)/", " * ", $descriptionLtrim);
                $descriptionArr = explode('*', $descriptionstr);
                $mapsArr = explode('--',$params['maps']);
                $newMapArr = [];
                foreach ($mapsArr as $k => $v) {
                    $num =  $k+1;
                    $newMapArr[$num] = $v;
                }
                $success = [];
                foreach ($descriptionArr as $key => $val) {
                    $patterns = "/.*forfeit.*map.*/";
                    preg_match_all($patterns,$val,$arr);

                    if (!empty($arr[0]) && strpos($params['maps'], 'Default') !== false){
                        $team_one = StandardDataTeam::find()->select(['name'])->where(['rel_identity_id' => $params['team_one']])->asArray()->one();
                        $team_two = StandardDataTeam::find()->select(['name'])->where(['rel_identity_id' => $params['team_two']])->asArray()->one();
                        if (strpos($arr[0][0], $team_one['name']) !== false){

                            foreach ($newMapArr as $keyMap => $valMap) {
                                if ($valMap == 'Default') {
                                    $success[] = ['battle_order'=>$keyMap,'rel_team_id'=>$params['team_one']];
                                }
                            };

                        }
                        if (strpos($arr[0][0], $team_two['name']) !== false){
                            foreach ($newMapArr as $keyMap => $valMap) {
                                if ($valMap == 'Default') {
                                    $success[] = ['battle_order'=>$keyMap,'rel_team_id'=>$params['team_two']];
                                }

                            };

                        }

                    }

                }
                if (!empty($success)) {
                    return json_encode($success);
                }
                return null;
            },
            'map_info' => function($params) {
                $expArr = explode('--',$params['maps']);
                foreach ($expArr as  $k => $val) {
                    $newArr[]['map'] = $val;
                }
                $maps = json_encode($newArr);
                return $maps;
            },

            'winner' => function($params) {
            if ($params['status'] == 'Match over') {
                if ($params['score_one'] < $params['score_two']) {
                    return $params['team_two'];
                }

                if ($params['score_one'] > $params['score_two']) {
                    return $params['team_one'];
                }
            }

                return null;
            },

            //是否平局
            'draw' => function ($params) {
                if ($params['status'] == 'Match over') {
                    if ($params['score_one'] == $params['score_two']) {
                        return 1;

                    }
                }

                return 2;

            },

            'is_battle_detailed' => function($params) {
               return 1;
            },
            'is_pbpdata_supported' => function($params) {
                return 1;
            },

            'is_streams_supported' => function($params) {
                if (!empty($params['live_video'])){
                    return 1;
                }
                return  2;

            },

            'scheduled_begin_at' => function ($params) {
                if(isset($params['match_time']) && $params['match_time']){
                    return date("Y-m-d H:i:s",$params['match_time']);
                }
            },

            'description' => function($params) {
             if (!empty($params['description'])){
                 $descriptionstr = preg_replace("/(>>>)/", "\r", $params['description']);
                 return $descriptionstr;
             }
             return null;
            },
            'map_ban_pick' => function($params){
            if (!empty($params['map_ban_pick'])) {
                $newarr = json_decode($params['map_ban_pick'],true);
                $types = ['removed'=>'1','picked'=>'2','was left over'=>'3'];
                foreach ($newarr as $key => &$val) {
                    $val['type'] = $types[$val['type']];

                }
                $BpEncode = json_encode($newarr);
            }else{
                $BpEncode = null;

            }
             return $BpEncode;
            },

            'team_1_score' => function ($params) {

                if ($params['status'] == 'LIVE' && $params['number_of_games'] == "1") {
                    return "0";
                }
                if ($params['score_one'] < $params['score_two'] && $params['status'] == 'Match over' && $params['number_of_games'] == "1") {
                    return "0";
                }

                if ($params['score_one'] > $params['score_two'] && $params['status'] == 'Match over' && $params['number_of_games'] == "1") {
                    return "1";
                }

                return $params['score_one'];


            },

            'team_2_score' => function ($params) {
                if ($params['status'] == 'LIVE' && $params['number_of_games'] == "1") {
                    return "0";
                }

                if ($params['score_two'] < $params['score_one']  && $params['status'] == 'Match over' && $params['number_of_games'] == "1") {
                    return "0";
                }

                if ($params['score_two'] > $params['score_one'] && $params['status'] == 'Match over' && $params['number_of_games'] == "1") {
                    return "1";
                }

                return $params['score_two'];

            },

            'logo' => 'image',
            'number_of_games'=>'number_of_games',
//            'map_info'=>'maps',
//            'map_ban_pick'=>'map_ban_pick',
//            'description'=>'description',
            'tournament_id'=>'tournament_id',
//            'scheduled_begin_at'=>'scheduled_begin_at',
            'team_1_id'=>'team_one',
            'team_2_id'=>'team_two',
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}