<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\hltv;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginManagerInterface;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;

class Team extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["team_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'team_id',
            'origin_id' => 7,
            'name' => 'name',
            'short_name' => '',
            'full_name' => '',
            'alias' => '',
//            clan_id
            'game_id' => function ($params) {
                // 根据对应关系取id
                return 1;
            },
            'logo' => 'image',
            'country' => function ($params) {
                $country = EnumService::getCountryInfoByHltvEName($params['country']);
                return $country['id'];
            },
            'players' =>function($params) {
                $now_players = "[" . implode(',', explode('+', $params['player_now'])) . "]" ?: "[]";
                return $now_players;
            },
            'history_players' =>function($params) {
                $old_players = "[" . implode(',', explode('+', $params['player_old'])) . "]" ?: "[]";
                return $old_players;
            },
            'world_ranking' => 'world_ranking',
            'ago_30' => 'weeks_in_top30_for_core',
            'average_player_age' => 'average_player_age',


        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}