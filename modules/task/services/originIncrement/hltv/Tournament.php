<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\hltv;

use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;

class Tournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["tournament_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardDataTournament($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'tournament_id',
            'name' => 'name',
            'game_id' => function ($params) {
                // 根据对应关系取id
                return (int)1;
            },
            'country' => function ($params) {
                $country = EnumService::getCountryInfoByHltvEName($params['country']);
                return $country['id'];
            },

            'prize_bonus' => function($params){
                if($params['prize_pool']){
                    $a = explode("+",$params['prize_pool']);
                    foreach ($a as $value){
                        if(stristr($value,"$")){
                            return trim($value);
                        }
                    }
                }
            },
            'prize_seed' => function($params){
              if($params['prize_pool']){
                  $a = explode("+",$params['prize_pool']);
                  foreach ($a as $value){
                      if(stristr($value,"spot")){
                         return trim($value);
                      }
                  }
              }
            },
            'prize_points' => function($params){
                if($params['prize_pool']){
                    $a = explode("+",$params['prize_pool']);
                    foreach ($a as $value){
                        if(stristr($value,"points")){
                            return trim($value);
                        }
                    }
                }
            },
            'number_of_teams' => 'number_of_teams',
            'image'=>'image',
            'scheduled_begin_at'=>'scheduled_begin_at',
            'scheduled_end_at'=>'scheduled_end_at',
            'address'=>'location',
            'format' => function($params){
                $group = "";
                $playoffs = "";
                $format = "";
                if(isset($params['group_stage_format']) && $params['group_stage_format']){
                    $group =  "Group Stage:\r".$params['group_stage_format'];
                }
                if(isset($params['playoffs_format']) && $params['playoffs_format']){
                    $playoffs =  "Playoffs:\r".$params['playoffs_format'];
                }
                if($group && !$playoffs){
                    $format = $group;
                }
                if(!$group && $playoffs){
                    $format = $playoffs;
                }
                if($group && $playoffs){
                    $format = $group."\r".$playoffs;
                }
                if(!$group && !$playoffs){
                    $format = "";
                }
                return $format;
            },
            'status' => function($params){
               if($params['scheduled_begin_at']){
                   $status = '';
                   if(date("Y-m-d H:i:s") < $params['scheduled_begin_at']){
                       $status = 1;
                   }
                   if(date("Y-m-d H:i:s") >= $params['scheduled_begin_at'] && date("Y-m-d H:i:s") < $params['scheduled_end_at']){
                       $status = 2;
                   }
                   if(isset($params['scheduled_end_at']) && $params['scheduled_end_at']){
                       if(date("Y-m-d H:i:s") > $params['scheduled_end_at']){
                           $status = 3;
                       }
                   }
                   return $status;
               }
            },
            'map_pool' => function($params){
              if($params['map_pool']){
                  $result = str_replace("--",",",$params['map_pool']);
                  if($result){
                      return trim($result);
                  }
              }
            },
            'teams_condition' => function($params){  //参赛条件
               $result = [];
               if(isset($params['teams']) && $params['teams']){
                   foreach ($params['teams'] as $key=>$value){
                       $result[$key]['team_id'] =  @$value["team_id"];
                       $result[$key]["type"] = 1;
                       $result[$key]['match_condition'] =  @$value["match_condition"];
                       $result[$key]['match_condition_cn'] = null;
                       $result[$key]['team_sort'] =  @$value["team_sort"];
                   }
                   return json_encode($result);
               }
            },
            'teams' => function($params){
               if(isset($params['teams']) && $params['teams']){
                   if(is_array($params['teams'])){
                       $a = array_column($params['teams'],'team_id');
                       return json_encode($a);
                   }
               }
            },
            'prize_distribution' => function($params){
                if (isset($params['prize_distribution']) && $params['prize_distribution']){
                    foreach($params['prize_distribution'] as $key=>$value){
                        if($value['points_seed']){
                            if(stristr($value['points_seed'],"points")){
                                $params['prize_distribution'][$key]['score'] = $value['points_seed'];
                                $params['prize_distribution'][$key]['num'] = null;
                            }else{
                                $params['prize_distribution'][$key]['num'] =  $value['points_seed'];
                                $params['prize_distribution'][$key]['score'] =  null;
                            }
                        }else{
                            $params['prize_distribution'][$key]['num'] = null;
                            $params['prize_distribution'][$key]['score'] =  null;
                        }
                        unset($params['prize_distribution'][$key]['points_seed']);
                        unset($params['prize_distribution'][$key]['tournament_id']);
                    }
                    $result = [];
                    foreach ($params['prize_distribution'] as $pkey=>$pval){
                        $transArray = ['rank'=>'','price'=>'','score'=>'','num'=>'','team_id'=>''];
                        $transArray['rank'] = $pval['rank'];
                        $transArray['price'] = $pval['price'];
                        $transArray['score'] = $pval['score'];
                        $transArray['num'] = $pval['num'];
                        $transArray['team_id'] = $pval['team_id'];
                        $result[]= $transArray;
                    }
                    if($result){
                        return  json_encode($result);
                    }
                }
            },
            'type'=>function(){
               return (int)1;
            },
            'parent_id'=>function(){
               return (int)0;
            }
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}