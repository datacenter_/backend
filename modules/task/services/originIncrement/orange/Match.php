<?php

namespace app\modules\task\services\originIncrement\orange;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginMetadataRunBase;
use app\modules\task\services\originIncrement\OriginRunBase;

class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["scheduleid"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $reason = $tagInfo['ext2_type'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId,$reason);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId, $batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId, $reason ="")
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'game' =>function(){
              return (int)2;
            },
            'game_id' =>function(){
                return (int)2;
            },
            'draw'=>function(){
                return (int)2;
            },
            'forfeit'=>function(){
                return (int)2;
            },
            'tournament_id' =>function($params){
               if(isset($params['tournament_id']) && $params['tournament_id']){
                   return (string)$params['tournament_id'];
               }
            },
            'rel_identity_id'=> 'scheduleid',
            'match_type'=>function(){
                return (string)1;
            },
            'number_of_games'=>function($params){
                if(isset($params['bonum']) && $params['bonum']){
                    return (string)$params['bonum'];
                }
            },
            'round_order'=>function($params){
                if(isset($params['roundnum']) && $params['roundnum']){
                    return (string)$params['roundnum'];
                }
            },
            'team_1_id' =>'oneseedid',
            'team_2_id' =>'twoseedid',
            'team_1_score'=> function($params){
              if(isset($params['onewin'])){
                  return (string)$params['onewin'];
              }
            },
            'team_2_score'=> function($params){
                if(isset($params['twowin'])){
                    return (string)$params['twowin'];
                }
            },
            'winner' =>function($params){
               if(isset($params['onewin']) && isset($params['twowin'])){
                   $winner = null;
                   if((int)$params['onewin'] + (int)$params['twowin'] == $params['bonum']){
                       if($params['onewin'] > $params['twowin']){
                           $winner = (string)$params['oneseedid'];
                       }elseif ($params['onewin'] < $params['twowin']){
                           $winner = (string)$params['twoseedid'];
                       }
                   }
                   elseif ($params['onewin'] > $params['bonum']/2 || $params['twowin'] > $params['bonum']/2){
                       if($params['onewin'] > $params['twowin']){
                           $winner = (string)$params['oneseedid'];
                       }elseif ($params['onewin'] < $params['twowin']){
                           $winner = (string)$params['twoseedid'];
                       }
                   }
                   return $winner;
               }
            },
            'status' =>function($params){
              $status = null;
              if(time() < $params['starttime']){
                  $status = 1;
              }
              elseif(array_key_exists('onewin',$params) && array_key_exists('twowin',$params)){
                  if((int)$params['onewin'] + (int)$params['twowin'] == $params['bonum']){
                      $status = 3;
                  }elseif ($params['onewin'] > $params['bonum']/2 || $params['twowin'] > $params['bonum']/2){
                      $status = 3;
                  }elseif(time() < $params['starttime'] || !$params['starttime']){
                      $status = 1;
                  }else{
                      $status = 2;
                  }
              }
              elseif(time() > $params['starttime'] && $status ==2){
                  $status = 2;
              }else{
                  $status = 1;
              }
              return (int)$status;
            },
            'is_pbpdata_supported' => function(){
              return 1;
            },
            'is_battle_detailed' => function(){
               return 1;
            },
            'deleted' =>function(){
               return 2;
            }
        ];

        $info = Mapping::transformation($keyMap, $infoFormHotInfo);
        if($reason == "todo"){
            unset($info['tournament_id']);
        }

        if($info['status'] == 1){ //未开始
            if(isset($infoFormHotInfo['starttime']) && $infoFormHotInfo['starttime']){
                $info['scheduled_begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
            }
        }
        elseif($info['status'] == 2){  //进行中
            if(isset($infoFormHotInfo['starttime']) && $infoFormHotInfo['starttime']){
                if($info['scheduled_begin_at']){  //比赛未开始的时候抓过该比赛
                    $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                }else{ //比赛进行中第一次抓取
                    $info['scheduled_begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                    $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                }
            }
        }
        elseif($info['status'] == 3){  //已完成
            if(isset($infoFormHotInfo['starttime']) && $infoFormHotInfo['starttime']){
                if($info['begin_at']){  //已完成，并且有实际开始时间（经历过比赛中）
                    $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                }elseif($info['scheduled_begin_at'] && !$info['begin_at']){  //没有经历过进行中的比赛,比赛开始前抓取过
                    $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                }else{
                    $info['scheduled_begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                    $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
                }
            }
        }

//        if($info['status'] == 1 ||  $info['status'] == 2 || $info['status'] == 3){
//            if(isset($infoFormHotInfo['starttime']) && $infoFormHotInfo['starttime']){
//                $info['scheduled_begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
//            }
//        }
//        if($info['status'] == 2 || $info['status'] == 3){
//            if(isset($infoFormHotInfo['starttime']) && $infoFormHotInfo['starttime']){
//                $info['begin_at'] = date('Y-m-d H:i:s',$infoFormHotInfo['starttime']);
//            }
//        }
        return $info;
    }
}