<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\orange;

use app\modules\common\services\Consts;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;
use app\modules\task\services\QueueServer;

class OrangeDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case QueueServer::QUEUE_RESOURCE_TEAM:
                $tasks = Team::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
                $tasks = Tournament::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks = Player::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                $tasks = Match::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_HERO:
                $tasks = MetadataHero::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_PROP:
                $tasks = MetadataProp::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_RUNE:
                $tasks = MetadataRune::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_SKILL:
                $tasks = MetadataSkill::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TALENT:
                $tasks = MetadataTalent::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:     //lol_champion
                $tasks = LOLHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:     //lol_item
                $tasks = LOLItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:     //lol召唤师技能
                $tasks = LOLSummonerSpell::run($tagInfo,$taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('PandascoreDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}