<?php

namespace app\modules\task\services\originIncrement\orange;

use app\modules\common\models\EnumCountry;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;

class Tournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["eid"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'eid',
            'name' => 'alias',
            'name_cn' => 'name',
            'country' => function($params){
                $countryInfo = EnumCountry::getNationalityByKey('two',"CN");
                if($countryInfo){
                    return $countryInfo['id'];
                }
            },
            'scheduled_begin_at'=>function($params){
                if(isset($params['starttime']) && $params['starttime']){
                    return date('Y-m-d H:i:s',$params['starttime']);
                }
            },
            'scheduled_end_at'=>function($params){
                if(isset($params['endtime']) && $params['endtime']){
                    return date('Y-m-d H:i:s',$params['endtime']);
                }
            },
            'status' => function($params){
                if(isset($params['starttime'])){
                    if(date("Y-m-d H:i:s") < date('Y-m-d H:i:s',$params['starttime'])){
                        return (int)1;
                    }
                    if((date("Y-m-d H:i:s") > date('Y-m-d H:i:s',$params['starttime']) && date('Y-m-d H:i:s',$params['endtime']) > date("Y-m-d H:i:s")) ||
                        (date("Y-m-d H:i:s") > date('Y-m-d H:i:s',$params['starttime']) && empty($params['endtime']))){
                        return (int)2;
                    }
                    if(date("Y-m-d H:i:s") >= date('Y-m-d H:i:s',$params['endtime'])){
                        return (int)3;
                    }
                }
            },
            'number_of_teams'=> function($params){
               if(isset($params['seednum'])){
                   return (string)$params['seednum'];
               }
            },
            'version'=> function($params){
               if(isset($params['gameversion'])){
                   return (string)$params['gameversion'];
               }
            },
            'parent_id'=> function(){
                return (int)0;
            },
            'type'=> function(){
                return (int)1;
            },
            'deleted' =>function(){
                return 2;
            }
        ];
        $info = Mapping::transformation($keyMap, $infoFormHotInfo);
        return $info;
    }
}