<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\pandascore;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\pandascore\TaskDataTournament;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournamentGroup;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;

class Group extends OriginRunBase
{
    protected static $tag=[
        'origin'=>QueueServer::QUEUE_ORIGIN_PANDASCORE,
        'resource'=>QueueServer::QUEUE_RESOURCE_TOURNAMENT_GROUP,
    ];

    public static function run($tagInfo, $taskInfo)
    {
        // 更新数据 ，增加standard变化量
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];

        $InfoFromStandard = self::getInfoFromStandard($sourceIdentityId);
        $InfoFromSource = self::getInfoFromSource($sourceIdentityId);
        $changeInfo = Common::getDiffInfo($InfoFromStandard, $InfoFromSource, array_diff(array_keys($InfoFromSource),['id']));

        if ($InfoFromStandard) {
            if (!$changeInfo['changed']) {
                return [];
            }
            $newInfo=self::updateInfo($InfoFromSource, $sourceIdentityId);
        } else {
            $newInfo=self::addInfo($InfoFromSource);
        }

        $changeType = $changeInfo["change_type"];
        //添加队列
        if (isset(self::$tag['origin'])) {
            $originName = self::$tag['origin'];
        } else {
            throw new TaskException("未定义origin");
        }
        if (isset(self::$tag['resource'])) {
            $resourceName = self::$tag['resource'];
        } else {
            throw new TaskException("未定义resource");
        }

        $tag = QueueServer::getTag(QueueServer::QUEUE_MAJOR_STANDARD_INCREMENT,
            $originName,
            $resourceName,
            $changeType,
            "");
        $changeBody = [
            "new" => $newInfo,
            "old" => $changeInfo['old'],
            "diff" => $changeInfo['diff']
        ];
        $queueInfos = [];
        $queueInfos[] = [
            "tag" => $tag,
            "params" => $changeBody,
        ];
        return $queueInfos;
    }

    protected static function getInfoFromStandard($Id)
    {
        //从一个或者多个地方获取数据，合并用，比如分base，detail，rel
        $originInfo = TaskTypes::getInstance()->getOriginByName("pandascore");
        return StandardDataTournamentGroup::find()->where(["rel_identity_id" => $Id, "origin_id" => $originInfo["id"]])->asArray()->one();
    }

    protected static function getInfoFromSource($Id)
    {
        //这里需要转义，映射，改字段名字
        $fromOrigin = TaskDataTournament::find()->where(["id" => $Id])->asArray()->one();

        $info['begin_at'] = $fromOrigin['begin_at'];
        $info['end_at'] = $fromOrigin['end_at'];
        $info['tournament_id'] = $fromOrigin['serie_id'];
        $info['name'] = $fromOrigin['name'];
        $info['slug'] = $fromOrigin['slug'];
        $info['game'] = $fromOrigin['game_id'];

        return $info;
    }

    protected static function updateInfo($info,$identityId)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("pandascore");
        $group = StandardDataTournamentGroup::find()->where(["rel_identity_id" => $identityId, "origin_id" => $originInfo["id"]])->one();
        $atts = $info;
        $atts["rel_identity_id"] = $identityId;
        $atts["origin_id"] = $originInfo["id"];
        $group->setAttributes($atts);
        if(!$group->save()){
            print_r($group->getErrors());
            throw new BusinessException($group->getErrors(),"添加失败");
        }
        return $group->toArray();
    }

    protected static function addInfo($info)
    {
        $originInfo = TaskTypes::getInstance()->getOriginByName("pandascore");
        $atts = $info;
        $atts["rel_identity_id"] = $info['player_id'];
        $atts["origin_id"] = $originInfo["id"];
        $group = new StandardDataTournamentGroup();
        $group->setAttributes($atts);
        if(!$group->save()){
            throw new BusinessException($group->getErrors(),"添加失败");
        }
        return $group->toArray();
    }
}