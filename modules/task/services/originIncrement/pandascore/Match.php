<?php


namespace app\modules\task\services\originIncrement\pandascore;


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\task\models\MatchComingScoket;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\BusinessException;
use function GuzzleHttp\Psr7\str;

class Match  extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $reason = $tagInfo['ext2_type'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);
        //存入达雷那的websocket要用的数据
        if(($reason == "incidents" || $reason == "upcoming") && ($transInfo['is_pbpdata_supported'] == 1)){
            self::toMatchComingSocket($originId,$gameId, $sourceIdentityId,$transInfo);
        }
//        if(isset($transInfo['live_url']) && $transInfo['live_url']){
//            Common::addStandardDataMatchVideo($transInfo['live_url'], $transInfo['embed_url'],$transInfo['official_stream_url'], $originId, $gameId, $sourceIdentityId);
//        }
        // 如果比赛数据源给的弃权+取消+winner+0:0，的特殊处理逻辑（如果比赛数据源给的弃权+取消+winner+0:0，的特殊处理逻辑）
        if($transInfo['forfeit'] == 1 && $transInfo['status'] == 5 && $transInfo['winner'] && $transInfo['team_1_score'] == 0 && $transInfo['team_2_score'] == 0){
            $transInfo['status'] = 3; //状态改为已结束
            if($transInfo['winner'] == $transInfo['team_1_id']){
                $transInfo['team_1_score'] = (string)1;
                $transInfo['team_2_score'] = (string)0;
            }
            if($transInfo['winner'] == $transInfo['team_2_id']){
                $transInfo['team_1_score'] = (string)0;
                $transInfo['team_2_score'] = (string)1;
            }
        }
        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        //存入视频表
        if(isset($infoFormHotInfo['streams']) && $infoFormHotInfo['streams']){
            foreach ($infoFormHotInfo['streams'] as $key=>$value){
                $embed_url = '';
                $raw_url = '';
                if(isset($value['embed_url']) && $value['embed_url']){
                    $embed_url = $value['embed_url'];
                }
                if(isset($value['raw_url']) && $value['raw_url']){
                    $raw_url = $value['raw_url'];
                }
                if($embed_url || $raw_url){
                    $isOfficial = false;
                    if($key == "official"){
                        $isOfficial = true;
                    }
                    Common::addStandardDataMatchVideo($raw_url,$embed_url,$isOfficial, $originId, $gameId, $identityId,Consts::ORIGIN_PANDASCORE);
                }
            }
        }
        $keyMap = [

            'begin_at'=>function($params){
                if(isset($params['begin_at']) && $params['begin_at']){
                    return date('Y-m-d H:i:s',strtotime($params['begin_at']));
                }
            },
            'is_battle_detailed'=>function($params){  //是否有对局详情true，false
                return $params['detailed_stats'] ? 1:2;
            },
            'game_version'=> function($params){
                $game_version = null;
                if(isset($params['videogame_version']['name']) && $params['videogame_version']['name']){
                    $game_version = (string)$params['videogame_version']['name'];
                }
                return $game_version;
            },
            'embed_url' => function($params){
              return $params['live_embed_url'] ? $params['live_embed_url'] : null;
            },
//            'embed_url'=>'live_embed_url',
            'description' => 'name',
            'name' => 'name',
            'end_at' => function($params){
                if(isset($params['end_at']) && $params['end_at']){
                    return date('Y-m-d H:i:s',strtotime($params['end_at']));
                }
            },
//            'name' => 'full_name',
            'rel_identity_id' => 'id',
            'draw' => function($params){   //是否平局
               return $params['draw'] ? 1:2;
            },
            'forfeit' => function($params){  //是否弃权
               return $params['forfeit'] ? 1:2;
            },
            'default_advantage' => function($params){
              if(isset($params['game_advantage']) && $params['game_advantage']){
                return $params['game_advantage'] ? (string)$params['game_advantage'] : null;
              }
            },
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $params['videogame']['id']);
                return $gameInfo['id'];
            },
            'game' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $params['videogame']['id']);
                return $gameInfo['id'];
            },
            'team_1_id' => function ($params) {
               if(isset($params['opponents']['0']['opponent']['id'])){
                   return (string)$params['opponents']['0']['opponent']['id'];
               }
            },
            'team_2_id' => function ($params) {
               if(isset($params['opponents']['1']['opponent']['id'])){
                   return (string)$params['opponents']['1']['opponent']['id'];
               }
            },
            'team_1_score'=>function($params){
               if(isset($params['results']) && count($params['results']) > 0){
                   foreach ($params['results'] as $value){
                       if(isset($params['opponents']['0']['opponent']['id']) && $params['opponents']['0']['opponent']['id'])
                           if($value['team_id'] == $params['opponents']['0']['opponent']['id']){
                               return $value['score'];
                           }
                   }
               }
            },
            'team_2_score'=>function($params){
               if(isset($params['results']) && count($params['results']) > 0){
                   foreach ($params['results'] as $value){  //可能有另一个战队没有产生
                       if(isset($params['opponents']['1']['opponent']['id']) && $params['opponents']['1']['opponent']['id']){
                           if($value['team_id'] == $params['opponents']['1']['opponent']['id']){
                               return $value['score'];
                           }
                       }
                   }
               }
            },
            'winner' => function($params){
               if(isset($params['winner_id']) && $params['winner_id']){
                   return (string)$params['winner_id'];
               }
            },
            'tournament_id' => 'serie_id',
            'group_id' => 'tournament_id',
            'status' => function($params){
                return Common::getPandaScoreMatchStatus($params['status']);
            },
//            'live_url' => 'live_url',
            'number_of_games' => 'number_of_games',
            'original_scheduled_begin_at' => function($params){
               if(isset($params['original_scheduled_at']) && $params['original_scheduled_at']){
                   return $params['original_scheduled_at'] ? date('Y-m-d H:i:s',strtotime($params['original_scheduled_at'])) :'';

               }
            },
            'scheduled_begin_at' => function($params){
                if(isset($params['scheduled_at']) && $params['scheduled_at']){
                    return $params['scheduled_at'] ? date('Y-m-d H:i:s',strtotime($params['scheduled_at'])) :'';
                }
            },
            'match_type' => function($params){
                return Common::getMatchType(Consts::ORIGIN_PANDASCORE,$params['match_type']);
            },
            'is_rescheduled' => function($params){
               if(isset($params['rescheduled']) && $params['rescheduled']){
                   return 1;
               }else{
                   return 2;
               }
            },
            'is_pbpdata_supported' => function($params){
                if(isset($params['live']['supported']) && $params['live']['supported']){
                    return 1;
                }else{
                    return 2;
                }
            },
            'is_streams_supported' => function($params){
                if(isset($params['live']['url']) && $params['live']['url']){
                    return 1;
                }else{
                    return 2;
                }
            },
            'game_rules' => function(){
               return "1";
            }, //暂写默认为1
            'official_stream_url' => 'official_stream_url',
            'slug' => 'slug',
            //battle弃权
            'battle_forfeit' => function($params){
                if(isset($params['games'])){
                    if(count($params['games'])){
                        $result = [];
                        foreach ($params['games'] as $key=>$value){
                            if($value['forfeit'] == true && isset($value['winner']['id'])){
                                if(isset($params['opponents']['0']['opponent']['id']) && isset($params['opponents']['1']['opponent']['id'])){
                                    if($value['winner']['id'] == $params['opponents']['0']['opponent']['id']){
                                        $trans['rel_team_id'] = $params['opponents']['1']['opponent']['id'];
                                    }
                                    if($value['winner']['id'] == $params['opponents']['1']['opponent']['id']){
                                        $trans['rel_team_id'] = $params['opponents']['0']['opponent']['id'];
                                    }
                                }
                                $trans['battle_order'] = $key+1;

                                $result[] = $trans;
                            }
                        }
                        if($result){
                            return json_encode($result);
                        }
                    }
                }
            },
            'deleted'=>function(){
                return (int)2;
            }
        ];
        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($playerInfo['tournament_id'])){
            $playerInfo['tournament_id'] = (string)$playerInfo['tournament_id'];
        }
        return $playerInfo;
    }

    /**
     * @param $originId
     * @param $gameId
     * @param $sourceIdentityId
     * @param $transInfo
     * @return array|\yii\db\ActiveRecord
     * @throws BusinessException
     * 存入达雷websocket
     */
    public static function toMatchComingSocket($originId,$gameId, $sourceIdentityId,$transInfo)
    {
        $info['begin_at'] = $transInfo['begin_at'];
        $info['end_at'] = $transInfo['end_at'];
        $info['status'] = $transInfo['status'];

        $standardInfo = MatchComingScoket::find()->where([
            'origin_id' => $originId,
            'match_id' => (string)$sourceIdentityId,
            'game_id' => $gameId,
        ])->one();
        if (!$standardInfo) {
            $standardInfo = new MatchComingScoket();
            $standardInfo->setAttributes(
                [
                    'origin_id' => $originId,
                    'match_id' => (string)$sourceIdentityId,
                    'game_id' => $gameId,
                ]
            );
        }
        $standardInfo->setAttributes($info);
        if (!$standardInfo->save()) {
            throw new BusinessException($standardInfo->getErrors(), '保存失败');
        }
//        return Common::getDiffInfo($oldInfo, $standardInfo->toArray());
    }
}