<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;
use app\modules\task\services\QueueServer;

class PandascoreDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case QueueServer::QUEUE_RESOURCE_TEAM:
                $tasks = Team::run($tagInfo, $taskInfo);
                break;
            case Consts::RESOURCE_TYPE_SON_TOURNAMENT:  //赛事
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
                $tasks = Tournaments::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks = Player::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT_GROUP:
                $tasks = Group::run($tagInfo, $taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:  //比赛
                $tasks = Match::run($tagInfo, $taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_HERO:     //dota2_hero
                $tasks = DotaHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ABILITY:     //dota2_ability
            case Consts::METADATA_TYPE_DOTA2_TALENT:     //dota2_talent
                $tasks = DotaAbilities::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:     //dota2_item
                $tasks = DotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:     //lol_champion
                $tasks = LOLHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:     //lol_item
                $tasks = LOLItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:     //lol召唤师技能
                $tasks = LOLSummonerSpell::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_CSGO_MAP:     //csgo地图
                $tasks = CSMap::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_CSGO_WEAPON:     //csgo武器
                $tasks = CSWeapon::run($tagInfo,$taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('PandascoreDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}