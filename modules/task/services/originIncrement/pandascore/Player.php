<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\pandascore;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginManagerInterface;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;
use function GuzzleHttp\Psr7\str;

class Player extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'nick_name' => 'name',
            'rel_identity_id' => 'id',
            'name' => function ($params) {
                 return trim($params['first_name'] . " " . $params['last_name']);
            },
            'birthday'=> function($params){
               if(isset($params['birthday']) && $params['birthday']){
                   return date("Y-m-d",strtotime($params['birthday']));
                }
            },
            'game_id' => function ($params) {
                // 根据对应关系取id
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $params['current_videogame']['id']);
                return $gameInfo['id'];
            },
            'image' => function($params){
               if(isset($params['image_url'])){
                  return $params['image_url'];
               }
            },
            'team' => function($params){
                if(isset($params['current_team']['id']) && $params['current_team']['id']){
                    return (string)$params['current_team']['id'];
                }
            },
            'nationality' => function ($params) {
              if(isset($params['nationality'])){
                  $country = EnumService::getCountryByTwo($params['nationality']);
                  if($country){
                      return $country['id'];
                  }
              }
            },
            'role'=>function($params) use ($gameId){
                if(isset($params['role'])){
                    if($gameId == 2){
                        return (string)Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE,$params['role']);
                    }else if($gameId == 3){
                        return (string)Common::getDota2PositionByString(Consts::ORIGIN_PANDASCORE,$params['role']);
                    }
                    else{
                        return (string)$params['role'];
                    }
                }
            },
            'deleted'=>function(){
                return (int)2;
            }
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}