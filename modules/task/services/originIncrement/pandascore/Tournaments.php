<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\pandascore;

use app\modules\common\services\Consts;
use app\modules\task\models\EnumOrigin;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\rest\exceptions\BusinessException;
use yii\base\Exception;

class Tournaments extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);
        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardDataTournament($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $seriesFromRest = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        if(isset($seriesFromRest['serie_id'])){
            //todo 子任务的
            $keyMap = [
                'scheduled_begin_at'=>function($params){
                   if(isset($params['begin_at'])){
                       return date('Y-m-d H:i:s',strtotime($params['begin_at']));
                   }
                },
                'scheduled_end_at'=>function($params){
                    if(isset($params['end_at'])){
                        return date('Y-m-d H:i:s',strtotime($params['end_at']));
                    }
                },
                'name' => function($params){
                   if (isset($params['name'])){
                       return $params['name'];
                   }
                },
                'parent_id' => 'serie_id',
                'status' => function($params){
                    if(isset($params['begin_at'])){
                        if(date("Y-m-d H:i:s") < date('Y-m-d H:i:s',strtotime($params['begin_at']))){
                            return (int)1;
                        }
                        if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['begin_at'])) && empty($params['end_at'])){
                            return (int)2;
                        }
                        if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['end_at']))){
                            return (int)3;
                        }
                    }
                },
                'rel_identity_id' => 'id',
                'rel_league_id' => 'league_id',
                'deleted'=>function(){
                    return (int)2;
                }
            ];

            $info = Mapping::transformation($keyMap, $seriesFromRest);
            $info['game_id'] = $gameId;
            $info['type']='2';
        }else{
            $keyMap = [
                'scheduled_begin_at'=>function($params){
                   if(isset($params['begin_at']) && $params['begin_at']){
                       return date('Y-m-d H:i:s',strtotime($params['begin_at']));
                   }
                },
                'scheduled_end_at'=>function($params){
                    if(isset($params['end_at']) && $params['end_at']){
                        return date('Y-m-d H:i:s',strtotime($params['end_at']));
                    }
                },
                'introduction' => 'description',
                'name' => function($params){
                    if(isset($params['full_name']) && isset($params['league']['name'])){
                        $name = $params['league']['name']." ".$params['full_name'];
                    }elseif(empty($params['full_name']) && isset($params['league']['name'])){
                        $name = $params['league']['name'];
                    }elseif(isset($params['full_name']) && empty($params['league']['name'])){
                        $name = $params['full_name'];
                    }else{
                        $name = $params['name'];
                    }
                    return $name;
                },
                'rel_identity_id' => 'id',
                'rel_league_id' => 'league_id',
                'image' => function ($params){
                   if($params['league']['image_url']){
                       return $params['league']['image_url'];
                   }
                },
                'parent_id'=>function(){
                  return (int)0;
                },
                'status' => function($params){
                    if(isset($params['begin_at'])){
                        if(date("Y-m-d H:i:s") < date('Y-m-d H:i:s',strtotime($params['begin_at']))){
                            return (int)1;
                        }
                        if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['begin_at'])) && empty($params['end_at'])){
                            return (int)2;
                        }
                        if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',strtotime($params['end_at']))){
                            return (int)3;
                        }
                    }
                },
                'tier' => 'tier',
                'game_id' => function ($params) {
                    // 根据对应关系取id
                    $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_PANDASCORE, $params['videogame']['id']);
                    return $gameInfo['id'];
                },
                'deleted'=>function(){
                    return (int)2;
                }
//            'tournaments' => 'tournaments',
            ];

            $info = Mapping::transformation($keyMap, $seriesFromRest);
            $info['type']='1';
        }

        $info['rel_league_id']=(string)$info['rel_league_id'];
        return $info;
    }

}