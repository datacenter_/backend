<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\radarpurple;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;
use function GuzzleHttp\Psr7\str;


class Match extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["match_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'match_id',
            'game_id' => function ($params) {
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $params['game_id']);
                return $gameInfo['id'];
            },
            'game' => function ($params) {
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $params['game_id']);
                return $gameInfo['id'];
            },
            'tournament_id' => function($params){
                return (string)$params['tournament_id'];
            },
            'scheduled_begin_at' => function($params){
                return $params['match_time'];
            },
            'begin_at' => function($params){
                return $params['match_time'];
            },
            'end_at' => function($params){
               return $params['source_updated_at'];
            },
            'forfeit' => function(){
                return (int)2;
            },
            'team_1_id' => function($params){
                return (string)$params['home_team'];
            },
            'team_2_id' => function($params){
                return (string)$params['away_team'];
            },
            'team_1_score' => function($params){
                return (string)$params['home_score'];
            },
            'team_2_score' => function($params){
                return (string)$params['away_score'];
            },
            'is_battle_detailed' => function() {
                return 1;
            },
            'is_pbpdata_supported' => function() {
                return 1;
            },
            'status' => function($params){
                return Common::getRadarpurpleMatchStatus($params['status_id']);
            },
            'game_rules' => function(){
                return "1";
            }, //暂写默认为1
            "winner" => function($params) {
               if($params['status_id'] == 3){
                   if($params['home_score'] > $params['away_score']){
                       return (string)$params['home_score'];
                   }elseif($params['home_score'] < $params['away_score']){
                       return (string)$params['away_score'];
                   }
               }
            },
            "deleted" => function() {
               return 2;
            },
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}