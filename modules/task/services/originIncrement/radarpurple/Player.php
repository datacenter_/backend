<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\radarpurple;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;


class Player extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["player_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'player_id',
            'game_id' => function ($params) {
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $params['game_id']);
                return $gameInfo['id'];
            },
            'team' => function($params){
               if($params['team_id']){
                   return (string)$params['team_id'];
               }
            },
            'nick_name' => function($params){
               return $params['name_en'];
            },
            'image' => function($params){
               if($params['logo']){
                   $image = Common::radarpurpleTransImageUrl($params['logo']);
                   return $image;
               }
            },
            'role' => function($params){
               if($params['position']){
                   if($params['game_id'] == 2){
                       $position = Common::getDota2PositionByString(Consts::ORIGIN_RADAR_PURPLE,$params['position']);
                       return $position;
                   }
               }
            },
            'name' => function($params){
                if($params['real_name']){
                    if(!Common::isChinese($params['real_name'])){
                        return $params['real_name'];
                    }
                }
            },
            'name_cn' => function($params){
                if($params['real_name']){
                    if(Common::isChinese($params['real_name'])){
                        return $params['real_name'];
                    }
                }
            },
            'birthday' => function($params){
              if($params['birthday']){
                  return date("Y-m-d",$params['birthday']);
              }
            },
//            国籍没有转
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}