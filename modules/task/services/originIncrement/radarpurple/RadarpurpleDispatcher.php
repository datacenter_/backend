<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\radarpurple;

use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;

class RadarpurpleDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType){
            case QueueServer::QUEUE_RESOURCE_PLAYER:
                $tasks = Player::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TEAM:
                $tasks = Team::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                $tasks = Match::run($tagInfo,$taskInfo);
                break;
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
                $tasks = Tournament::run($tagInfo,$taskInfo);
                break;
            case  Consts::METADATA_TYPE_DOTA2_HERO:
            $tasks = DotaHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:
                $tasks = DotaItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_TALENT:
                $tasks = DotaTalent::run($tagInfo,$taskInfo);
                break;
        }
        return $tasks;
    }
}