<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\radarpurple;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumPosition;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\models\pandascore\TaskDataPlayer;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskDataFeijingPlayer;
use app\modules\task\models\TaskDataFeijingTeam;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginManagerInterface;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\TaskTypes;
use app\rest\exceptions\BusinessException;

class Team extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["team_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];
        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'team_id',
            'origin_id' => 11,
            'name' => 'name_en',
            'short_name' => 'abbr_en',
            'full_name' => '',
            'alias' => '',
            'game_id' => function ($params) {
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $params['game_id']);
                return $gameInfo['id'];
            },
            'logo' => function ($params) {
                if($params['logo']){
                    $image = Common::radarpurpleTransImageUrl($params['logo']);
                    return $image;
                }
                return $params['logo'];
            },
            'country' => function ($params) {
                $country = EnumService::getCountryInfoByHltvEName($params['country_name_en']);
                return $country['id'];
            },
            'players' =>function($params) {
               $playerall = str_replace('__',',',$params['playerall']);

                $now_players = "[" . $playerall . "]" ;
                return $now_players;
            },
//            'history_players' =>function($params) {
//                $old_players = "[" . implode(',', explode('+', $params['player_old'])) . "]" ?: "[]";
//                return $old_players;
//            },
            'world_ranking' => 'rank',
            'total_earnings' => 'total_earnings',
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);
        if(isset($playerInfo['deleted_at']) && $playerInfo['deleted_at']){
            $playerInfo['deleted'] = 1;
        }else{
            $playerInfo['deleted'] = 2;
            $playerInfo['deleted_at'] = null;
        }

        return $playerInfo;
    }
}