<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\radarpurple;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\common\services\EnumService;
use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginRunBase;


class Tournament extends OriginRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["tournament_id"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];
        $batchId = $taskInfo['batch_id'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId,$batchId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => 'tournament_id',
            'game_id' => function ($params) {
                $gameInfo = Common::getGameByMapperId(Consts::ORIGIN_RADAR_PURPLE, $params['game_id']);
                return $gameInfo['id'];
            },
            'scheduled_begin_at'=>function($params){
                if($params['start_time']){
                    return date('Y-m-d H:i:s',$params['start_time']);
                }
            },
            'scheduled_end_at'=>function($params){
                if($params['end_time']){
                    return date('Y-m-d H:i:s',$params['end_time']);
                }
            },
            'name' => 'name_en',
            'name_cn' => 'name_zh',
            'status' => function($params){
                $status = "";
                if($params['start_time']){
                    if(time() < $params['start_time']){
                        $status = 1;
                    }
                    elseif(time() > $params['start_time'] && empty($params['end_time'])){
                        $status = 2;
                    }
                    elseif(time() > $params['end_time'] && !empty($params['end_time'])){
                        $status = 3;
                    }
                }elseif(!$params['start_time']){
                    $status = 1;
                }
                return (int)$status;
            },
            'image' => function($params){
               if($params['logo']){
                   $image = Common::radarpurpleTransImageUrl($params['logo']);
                   return $image;
               }
            },
            'address_cn' => 'city_name',
            'tier' => 'types',
            //奖金
            'prize_bonus' =>function($params){
              if(stristr($params['price_pool'],'$') || stristr($params['price_pool'],'¥') || stristr($params['price_pool'],'₹')
              || stristr($params['price_pool'],'€') || stristr($params['price_pool'],'￡') || stristr($params['price_pool'],'₩')
              || is_numeric($params['price_pool'])){
                  return $params['price_pool'];
              }
            },
//            //积分
//            'prize_points' =>'',
            //名额
            'prize_seed' => function($params){
                if(Common::isChinese($params['price_pool'])){
                    return $params['price_pool'];
                }
            },
            'teams' => function($params){
                if(isset($params['team_all']) && $params['team_all']){
                    $a = explode("__",$params);
                    if(is_array($a)){
                        return json_encode($a);
                    }
                }
            },
            'type'=>function(){
                return (int)1;
            },
            'parent_id'=>function(){
                return (int)0;
            }
        ];

        $playerInfo = Mapping::transformation($keyMap, $infoFormHotInfo);

        return $playerInfo;
    }
}