<?php


namespace app\modules\task\services\originIncrement\riotgames;


use app\modules\task\services\Common;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\originIncrement\OriginMetadataRunBase;
use app\modules\task\services\originIncrement\OriginRunBase;

class LOLSummoner extends OriginMetadataRunBase
{
    public static function run($tagInfo, $taskInfo)
    {
        // 获取body
        $bodyInfo = json_decode($taskInfo["params"], true);
        $sourceIdentityId = $bodyInfo["new"]["key"];
        // 获取tag相关信息
        $gameId = $tagInfo['ext_type'];
        $originType = $tagInfo['origin_type'];
        $resourceType = $tagInfo['resource_type'];
        $changeType = $tagInfo['change_type'];

        $originInfo = Common::getOriginInfoByType($originType);
        $originId = $originInfo['id'];
        // 获取数据源数据并且转成标准化数据
        $transInfo = self::getInfoFromSource($originId, $resourceType, $gameId, $sourceIdentityId);

        // 设置标准表，并且返回diff信息，这个方法，根据事先逻辑不一样，可能需要做改动或者修改
        $diffInfo = self::setStandardData($originId, $resourceType, $gameId, $sourceIdentityId, $transInfo);

        // diffInfo是格式化数据，这个方法根据返回子任务
        return self::getSubStep($diffInfo, $originType, $resourceType, $gameId);
    }

    /**
     * @param $originId
     * @param $resourceType
     * @param $gameId
     * @param $identityId
     * @return array
     * 这个是从把对应的源数据转化成标准数据，然后格式化，这个是示例文件，需要重写
     */
    public static function getInfoFromSource($originId, $resourceType, $gameId, $identityId)
    {
        $infoFormHotInfo = self::getOriginInfo($identityId, $originId, $resourceType, $gameId);
        $keyMap = [
            'rel_identity_id' => function($params){
               return (string)$params['key'];
            },
            'name' => 'name',
            'name_cn' => 'name_cn',
            'image' => function($params){
                $aliOriginUrl = "http://".env('OSS_Bucket').".".env('OSS_Endpoint')."/riotgames/spell/".$params['id'].".png";
                $aliUrl = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
                $is = strstr($aliUrl, 'http');
                if($is){
                    $aliUrl = str_replace("http","https",$aliUrl);
                }
                return $aliUrl;

            },
            'external_id'=>function($params){
               return (string)$params['key'];
            },
            'external_name'=>function($params){
                return $params['id'];
            },
            'description' =>'description',
            'description_cn' =>'description_cn',
        ];

        $info = Mapping::transformation($keyMap, $infoFormHotInfo);
        $info['metadata_type'] = $resourceType;
        return $info;
    }
}