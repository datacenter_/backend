<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\riotgames;

use app\modules\common\services\Consts;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;
use app\modules\task\services\QueueServer;

class RiotgamesDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case Consts::METADATA_TYPE_LOL_ABILITY:
                $tasks = LOLAbility::run($tagInfo, $taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_RUNE:  //lol_rune
                $tasks = LOLRunes::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:     //lol_champion
                $tasks = LOLHero::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_ITEM:     //lol_item
                $tasks = LOLItem::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:     //lol召唤师技能
                $tasks = LOLSummoner::run($tagInfo,$taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('PandascoreDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}