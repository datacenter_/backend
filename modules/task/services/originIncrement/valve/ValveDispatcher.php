<?php
/**
 *
 */

namespace app\modules\task\services\originIncrement\valve;

use app\modules\common\services\Consts;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;

class ValveDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        $resourceType = $tagInfo['resource_type'];
        switch ($resourceType) {
            case Consts::METADATA_TYPE_DOTA2_HERO:
                $tasks = DotaHero::run($tagInfo, $taskInfo);
                break;
            case Consts::METADATA_TYPE_DOTA2_ITEM:
                $tasks = DotaItem::run($tagInfo, $taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('PandascoreDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}