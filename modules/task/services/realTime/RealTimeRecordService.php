<?php
/**
 *
 */

namespace app\modules\task\services\realTime;


use app\modules\task\models\TaskRealTimeBattleLogs;
use app\rest\exceptions\BusinessException;

class RealTimeRecordService
{
    public static function setBattleLog($originId,$gameId,$type,$identityId,$version,$info)
    {
        $log=new TaskRealTimeBattleLogs();
        $info=[
            'game_id'=>$gameId,
            'origin_id'=>(string)$originId,
            'identity_id'=>(string)$identityId,
            'version'=>(string)$version,
            'info'=>$info,
            'type'=>(string)$type,
        ];
        $log->setAttributes($info);
        if(!$log->save()){
            print_r($log->getErrors());
            exit;
            throw new BusinessException($log->getErrors(),"添加任务失败");
        }
        return $log->toArray();
    }
}