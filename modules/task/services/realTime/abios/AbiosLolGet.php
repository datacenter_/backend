<?php
/**
 *
 */

namespace app\modules\task\services\realTime\abios;


use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\AbiosBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\realTime\RealTimeRecordService;

class AbiosLolGet
{
    public static function getLiveMatchList()
    {
        // 获取进行中的比赛
        $token = AbiosBase::getAuthKey();
        $action = '/v2/series';
        $params['access_token'] = $token;
        $params['games'][] = 2;
        $params['is_over'] = 'false';
        $params['starts_after'] = '2020-05-22T09:49:31Z';
        $params['starts_before'] = '2020-05-24T09:49:31Z';
        $params['with'][] = 'matches';
        $params['order'] = 'start';
        $params['sort'] = 'ASC';
        $url = "https://" . 'api.abiosgaming.com' . $action;
        $header = [
            "Content-Type" => 'application/json;charset=utf-8',
        ];
        return Common::requestGet($url, $header, $params);
    }

    public static function getMatchInfo($matchId)
    {
        // 获取进行中的比赛
        $token = AbiosBase::getAuthKey();
        $params['with'] = ['matches', 'tournament', 'casters', 'summary'];
        $action = '/v2/series/' . $matchId;
        $params['access_token'] = $token;
        $url = "https://" . 'api.abiosgaming.com' . $action;
        $header = [
            "Content-Type" => 'application/json;charset=utf-8',
        ];
        return Common::requestGet($url, $header, $params);
    }

    public static function getGameList()
    {
        // 获取进行中的比赛
        $token = AbiosBase::getAuthKey();
        $action = '/v2/games';
        $params['access_token'] = $token;
        $params['games'][] = 1;
        $url = "https://" . 'api.abiosgaming.com' . $action;
        $header = [
            "Content-Type" => 'application/json;charset=utf-8',
        ];
        return Common::requestGet($url, $header, $params);
    }

    public static function getInfo($battleId)
    {
        // 获取信息，记录进来
        $info = self::getBattleInfoCurl($battleId);
        // 插入数据
        $infoArray = json_decode($info, true);
        $logInfo = RealTimeRecordService::setBattleLog(2, 2, 'lol', $battleId, '', $info);
        $put = self::putInfo($logInfo);
        // 更新操作结果
        // 刷新表
        return $info;
    }

    public static function putInfo($logInfo)
    {
        // 刷新对应信息
        // 基础信息
        $baseInfoConfig = [
            'is_confirmed' => 'is_confirmed',
            'duration' => 'length',
            'is_finished' => 'ended',
        ];
        // 战队信息
        $teamInfoConfig = [
            'id' => function ($params) {
                return Mapping::getOrNull($params, 'roster.id');
            },
            'winner' => 'is_winner',
            'kill' => 'score',
            'gold' => function ($params) {
                return Mapping::getOrNull($params, 'gold.earned');
            },
            'turret_kills' => function ($params) {
                return Mapping::getOrNull($params, 'turrets.destroyed');
            },
            'inhibitor_kills' => function ($params) {
                return Mapping::getOrNull($params, 'inhibitor.kills');
            },
            'baron_nashor_kills' => function ($params) {
                return Mapping::getOrNull($params, 'barons.killed');
            },
            'dragon_kills' => function ($params) {
                return Mapping::getOrNull($params, 'dragons.killed');
            }
        ];
        // players信息
        $playerInfoConfig = [
            'id' => function ($params) {
                return $params['player']['id'];
            },
            'kills' => 'kills',
            'deaths' => 'deaths',
            'assists' => 'assists',
            'items' => function ($params) {
                //需要转义成主表的道具id
            },
        ];
        // 更新baseinfo
        $info = json_decode($logInfo["info"], true);
        $baseInfo = Mapping::transformation($baseInfoConfig, $info);
        // 战队信息
        $teamAInfo = Mapping::transformation($teamInfoConfig, $info['blue']);
        $teamBInfo = Mapping::transformation($teamInfoConfig, $info['red']);
        // 队员信息
        // 遍历队员
        foreach ($info['blue']['players'] as $player) {
            $playerInfoBase = Mapping::transformation($playerInfoConfig, $player);
            // 附加战队属性
            // 存储数据

        }

    }

    private static function setBattleLol($info)
    {
        // set到对应的match

    }

    private static function trans($data)
    {

    }

    private static function getBattleInfoCurl($battleId)
    {
        $token = AbiosBase::getAuthKey();
        $action = sprintf('/v2/matches/%s/light_summary', $battleId);
        $params['access_token'] = $token;
        $url = "https://" . 'api.abiosgaming.com' . $action;
        $header = [
            "Content-Type" => 'application/json;charset=utf-8',
        ];
        return Common::requestGet($url, $header, $params);
    }

}