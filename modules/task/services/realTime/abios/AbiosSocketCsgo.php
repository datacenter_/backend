<?php
/**
 *
 */

namespace app\modules\task\services\realTime\abios;


use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\AbiosBase;
use app\modules\task\services\realTime\RealTimeRecordService;
use app\rest\exceptions\BusinessException;

class AbiosSocketCsgo
{
    const CHANNEL_LIVE_DOTA='live_dota';
    const CHANNEL_LIVE_LOL='live_lol';
    const CHANNEL_LIVE_CSGO='live_csgo';

    private $token;
    private $channel;
    private $subscriptionKey;
    private $webSocket;
    private $wsUrl;
    public function __construct($channel)
    {
        $this->setUrl($channel);
    }

    public function socketConnect()
    {
        self::log('start socketConnect');
        $url=sprintf('wss://ws.abiosgaming.com/v0?access_token=%s&subscription_id=%s',$this->getAuthKey(),$this->subscriptionKey);
        self::log('url: '.$url);
        $this->webSocket = \Swlib\SaberGM::websocket($url);
        return $this->webSocket;
    }

    public function setUrl($channel)
    {
        self::log("start set url");
        $this->setChannel($channel);
        $this->setSubKey($channel);
        $this->wsUrl=sprintf('wss://ws.abiosgaming.com/v0?access_token=%s&subscription_id=%s',$this->getAuthKey(),$this->subscriptionKey);
    }

    public function reConnect()
    {
        return $this->socketConnect();
    }

    public function ping($info)
    {
        try {
            if($this->webSocket){
                self::log("ping....".$info);
                $this->webSocket->push("9");
            }
        }catch (\Exception $e){
            self::log($e->getMessage());
        }

    }

    public function getRecv($time)
    {
        $res= $this->webSocket->recv($time) . "\n";
        $code=$this->webSocket->client->statusCode;
        if($code<=0){
            self::log('reConnect.....');
            $this->reConnect();
        }
        if($res){
            return $res;
        }
    }

    public static function run($channel)
    {
        // 抓取，这里一个获取，一个
        $service=new self($channel);
        go(function ()use($service) {
            $service->socketConnect();
            while(true){
                $re=$service->getRecv(1);
                RealTimeRecordService::setBattleLog(2,1,1,1,1,$re);
                print_r($re);
            };
        });
//        go(function ()use($service) {
//            while(true){
//                $re=$service->ping(9);
//                sleep(1);
//            };
//        });
    }

    private function setChannel($channel)
    {
        $this->channel=$channel;
    }

    private function setSubKey($channel)
    {
        self::log("start set setSubKey");
        $key=$this->getSubscription($channel);
        $this->subscriptionKey=$key;
    }

    private function getAuthKey()
    {
        return AbiosBase::getAuthKey();
    }

    private function getSubscription($channel)
    {
        self::log('start socketConnect');
        $url='https://ws.abiosgaming.com/v0/subscription';
        $token=AbiosBase::getAuthKey();
        $params['access_token']=$token;
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
        ];
        $jsonInfo = Common::requestGet($url,$header,$params);
        $infoArray=json_decode($jsonInfo,true);
        foreach($infoArray as $sub){
            if($sub['name']==$channel){
                return $sub['id'];
            }
        }
        //如果没有，添加订阅
        $info=$this->setSubscription($channel);
        return $info['id'];
    }

    private function setSubscription($channel)
    {
        self::log('start setSubscription');
        $token=AbiosBase::getAuthKey();
        $url=sprintf('https://ws.abiosgaming.com/v0/subscription?access_token=%s',$token);
        $postArray=[
            "name"=>$channel,
            "filters"=>[[
                "channel"=>$channel,
            ]],
        ];
        $postStr=json_encode($postArray);
        $info=Common::requestPostJsonString($url,[],$postStr,'POST');
        return $info;
    }

    public function delSubscription($id)
    {
        $token=AbiosBase::getAuthKey();
        $url=sprintf('https://ws.abiosgaming.com/v0/subscription/%s?access_token=%s',$id,$token);
        $info=Common::requestPostJsonString($url,[],'','DELETE');
        return $info;
    }

    public static function log($info,$type="default",$line=null)
    {
        print_r([date("YmdHis"),$info,$type,$line]);
    }

}