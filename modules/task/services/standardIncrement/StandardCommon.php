<?php
/**
 *
 */

namespace app\modules\task\services\standardIncrement;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\org\services\PlayerService;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\UpdateConfig;
use app\modules\common\services\ImageConversionHelper;

class StandardCommon implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);

        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        $resourceType=$tagInfo['resource_type'];
        $standardInfoId=$params["new"]["id"];
        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh($resourceType,[$standardInfoId]);
            return [];
        }
        // 账号更新，此时获取账号关联关系，与对应的配置，决定下一步操作,此时团队已丢失
        $diff=$params["diff"];//差异
        //图片相关转存
        if(isset($diff['image']) && !empty($diff['image']['after'])){
            if($diff['image']['after'] == "https://img.elementsdata.cn/hltv_img/csgo.png"){  //hltv默认图片改为自己平台图片
                $enumGame = EnumGame::find()->select('simple_image')->where(['slug'=>'csgo'])->one();
                if($enumGame){
                    $diff['image']['after'] = $enumGame['simple_image'];
                }
            }else{
                $diff['image']['after'] = ImageConversionHelper::mvImageToAli($diff['image']['after']);
            }
        }
        if(isset($diff['logo']) && !empty($diff['logo']['after'])){
            $diff['logo']['after'] = ImageConversionHelper::mvImageToAli($diff['logo']['after']);
        }
        //数据源为hltv的情况
//        if($diff['image']['after'] == "https://static.hltv.org/images/playerprofile/blankplayer.svg"){
//            $enumGame = EnumGame::find()->select('simple_image')->where(['slug'=>'csgo'])->one();
//            if($enumGame){
//                $diff['image']['after'] = $enumGame['simple_image'];
//            }
//        }
        // 获取对应的表名
        $standardActiveTable=OriginRunBase::getStandardActiveTable($resourceType);
        $standardInfo=$standardActiveTable::find()->where(['id'=>$standardInfoId])->asArray()->one();
//        $gameId=$standardInfo["game_id"];
        if($standardInfo['deal_status']==4){
            self::addTodoByDiffInfo($standardInfo, $resourceType, $diff, $taskInfo);
        }
        // 标志状态，主动修改，忽略，等待修改
        return [];
    }

    /**
     * @param $standardInfo
     * @param $resourceType
     * @param $diff
     * @param $taskInfo
     * @param $forceUpdateStatus
     * @throws \app\rest\exceptions\BusinessException
     */
    public static function addTodoByDiffInfo($standardInfo, $resourceType, $diff, $taskInfo, $forceUpdateStatus=false): void
    {
        $relInfo = DataStandardMasterRelation::find()->where(["standard_id" => $standardInfo['id'], 'resource_type' => $resourceType])->one();
        $majorId = $relInfo["master_id"];
        if (!$majorId) {
            throw new \Exception('逻辑错误，找不到对应的主资源');
        }
        // 获取对应资源的更新配置
        $updateConfig = UpdateConfigService::getResourceUpdateConfig($majorId, $resourceType);

        // 这个方法现在做的东西比较多
        //1.转化，过滤，验证（包括unset，新加值的时候，需要看）
        //2.根据keyName分割到对应的tag（core_data,base_data等），新加值的时候需要改配置
        $changeTagInfo = UpdateConfig::getInstance()->getChangeTagByDiffInfo($resourceType, $diff, $standardInfo['origin_id'], $standardInfo['game_id'],$standardInfo['id']);
        foreach ($changeTagInfo as $tagKey => $tabValue) {
            switch ($resourceType) {
                // 如果是match，所有配置都走一个
                case Consts::RESOURCE_TYPE_MATCH:
                    $tagConfig = $updateConfig[Consts::TAG_TYPE_UPDATE_DATA];
                    break;
                // 如果是TYPE_TOURNAMENT，所有配置都走一个
                case Consts::RESOURCE_TYPE_TOURNAMENT:
                    switch ($tagKey) {
                        case Consts::TAG_TYPE_ATTEND_TEAM:
                        case Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION:
                            $tagConfig = $updateConfig[Consts::TAG_TYPE_ATTEND_TEAM];
                            break;
                        case Consts::TAG_TYPE_CORE_DATA:
                            $tagConfig = $updateConfig[Consts::TAG_TYPE_CORE_DATA];
                            break;
                        case Consts::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE:
                        case Consts::TAG_TYPE_BASE_DATA:
                            $tagConfig = $updateConfig[Consts::TAG_TYPE_BASE_DATA];
                            break;
                    }
                    break;
                default:
                    // 根据更新值（diff里面分割后的结果，获取对应的配置）
                    $tagConfig = $updateConfig[$tagKey];
                    break;
            }
            //标准表与关联表的资源ID相同

            if ($tagConfig["origin_id"] == $standardInfo['origin_id']) {
                $updateType=$forceUpdateStatus?$forceUpdateStatus:$tagConfig['update_type'];
                    // 添加待处理任务,如果需要自动执行，会在添加任务后自动执行，并标注执行结果
                    DataTodoService::addToDo($resourceType, $majorId, $tagKey, $tabValue, $standardInfo['id'], $taskInfo['id'], $updateType);
            } else {
                // 没有绑定这个数据源，不需要处理
            }
        }
    }
}