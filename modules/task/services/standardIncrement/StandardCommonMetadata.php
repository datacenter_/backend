<?php
/**
 *
 */

namespace app\modules\task\services\standardIncrement;

use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskInterface;
use app\modules\task\services\UpdateConfig;
use app\modules\common\services\ImageConversionHelper;

class StandardCommonMetadata implements TaskInterface
{
    public static function run($tagInfo, $taskInfo)
    {
        //标准表发生变化，这里检查修改来源，
        $params=json_decode($taskInfo['params'],true);

        // 如果是新增的账号，刷新账号关联关系
        $changeType=$tagInfo["change_type"];
        $resourceType=$tagInfo['resource_type'];
        $standardInfoId=$params["new"]["id"];
        if($changeType===QueueServer::QUEUE_TYPE_ADD){
            StandardMasterRelationExpectService::refresh($resourceType,[$standardInfoId]);
            return [];
        }
        // 账号更新，此时获取账号关联关系，与对应的配置，决定下一步操作,此时团队已丢失
        $diff=$params["diff"];//差异
        if(isset($diff['image']) && !empty($diff['image']['after'])){
            $diff['image']['after'] = ImageConversionHelper::mvImageToAli($diff['image']['after']);
        }
        if(isset($diff['small_image']) && !empty($diff['small_image']['after'])){
            $diff['small_image']['after'] = ImageConversionHelper::mvImageToAli($diff['small_image']['after']);
        }
        if(isset($diff['square_image']) && !empty($diff['square_image']['after'])){
            $diff['square_image']['after'] = ImageConversionHelper::mvImageToAli($diff['square_image']['after']);
        }
        if(isset($diff['rectangle_image']) && !empty($diff['rectangle_image']['after'])){
            $diff['rectangle_image']['after'] = ImageConversionHelper::mvImageToAli($diff['rectangle_image']['after']);
        }
        if(isset($diff['thumbnail']) && !empty($diff['thumbnail']['after'])){
            $diff['thumbnail']['after'] = ImageConversionHelper::mvImageToAli($diff['thumbnail']['after']);
        }
        // 获取对应的表名
        $standardActiveTable=OriginRunBase::getStandardActiveTable($resourceType);
        $standardInfo=$standardActiveTable::find()->where(['id'=>$standardInfoId])->asArray()->one();
        if($standardInfo['deal_status']==4){
            $relInfo=DataStandardMasterRelation::find()->where(["standard_id"=>$standardInfo['id'],'resource_type'=>$resourceType])->one();
            $majorId=$relInfo["master_id"];
            if(!$majorId){
                return [];
            }
            // 获取对应资源的更新配置
            $updateConfig=UpdateConfigService::getResourceUpdateConfig($majorId,$resourceType);

            // 这个方法现在做的东西比较多
            //1.转化，过滤，验证（包括unset，新加值的时候，需要看）
            //2.根据keyName分割到对应的tag（core_data,base_data等），新加值的时候需要改配置
            $changeTagInfo=UpdateConfig::getInstance()->getChangeTagByDiffInfo($resourceType,$diff,$standardInfo['origin_id'],$standardInfo['game_id']);
            foreach($changeTagInfo as $tagKey=>$tabValue){
                switch($resourceType){
                    case Consts::METADATA_TYPE_DOTA2_HERO:
                        switch ($tagKey) {
                            case Consts::TAG_TYPE_CORE_DATA:
                            case Consts::TAG_TYPE_BASE_DATA:
                                $tagConfig = $updateConfig[Consts::TAG_TYPE_CORE_DATA];
                                break;
                        }
                        break;
                    default:
                        // 根据更新值（diff里面分割后的结果，获取对应的配置）
                        $tagConfig=$updateConfig[$tagKey];
                        break;
                }
                //标准表与关联表的资源ID相同
                if($tagConfig["origin_id"]==$standardInfo['origin_id']){
                        // 添加待处理任务,如果需要自动执行，会在添加任务后自动执行，并标注执行结果
                        DataTodoService::addToDo($resourceType,$majorId,$tagKey,$tabValue,$standardInfo['id'],$taskInfo['id'],$tagConfig['update_type'],$standardInfo['game_id']);
                }else{
                    return false;
                    // 没有绑定这个数据源，不需要处理
                }
            }
        }
        // 标志状态，主动修改，忽略，等待修改
        return [];
    }
}