<?php
/**
 *
 */

namespace app\modules\task\services\standardIncrement;

use app\modules\common\services\Consts;
use app\modules\data\models\StandardDataMetadata;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\QueueServer;

class StandardIncrementDispatcher
{
    public static function run($tagInfo,$taskInfo)
    {
        $resourceType=$tagInfo["resource_type"];
        switch ($resourceType){
            case QueueServer::QUEUE_RESOURCE_PLAYER://选手
            case QueueServer::QUEUE_RESOURCE_TEAM://战队
            case QueueServer::QUEUE_RESOURCE_MATCH://比赛
            case QueueServer::QUEUE_RESOURCE_TOURNAMENT://赛事
            case Consts::RESOURCE_TYPE_SON_TOURNAMENT://子赛事
            case Consts::RESOURCE_TYPE_GRANDSON_TOURNAMENT;//孙赛事
                $tasks= StandardCommon::run($tagInfo,$taskInfo);
                break;
            case Consts::METADATA_TYPE_LOL_CHAMPION:
            case Consts::METADATA_TYPE_LOL_ITEM:
            case Consts::METADATA_TYPE_LOL_ABILITY:
            case Consts::METADATA_TYPE_LOL_RUNE:
            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:

            case Consts::METADATA_TYPE_DOTA2_HERO:
            case Consts::METADATA_TYPE_DOTA2_ITEM:
            case Consts::METADATA_TYPE_DOTA2_ABILITY:
            case Consts::METADATA_TYPE_DOTA2_TALENT:

            case Consts::METADATA_TYPE_CSGO_MAP:
            case Consts::METADATA_TYPE_CSGO_WEAPON:
                $tasks= StandardCommonMetadata::run($tagInfo,$taskInfo);
                break;
//            case QueueServer::QUEUE_RESOURCE_TOURNAMENT:
//                $tasks= StandardTournament::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_RESOURCE_SERIES:
//                $tasks= StandardSeries::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_RESOURCE_METADATA:
//                $tasks= StandardMetadata::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_RESOURCE_AB_STAGE:
//                $tasks= StandardStage::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_RESOURCE_SUBSTAGE:
//                $tasks= StandardSubStage::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_RESOURCE_MATCH:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_LOL_ABILITY:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_LOL_CHAMPION:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_LOL_ITEM:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_LOL_RUNE:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_LOL_SUMMONER_SPELL:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_DOTA2_HERO:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_DOTA2_ITEM:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_DOTA2_ABILITY:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
//            case Consts::METADATA_TYPE_DOTA2_TALENT:
//                $tasks= StandardMatch::run($tagInfo,$taskInfo);
//                break;
            default:
                throw new TaskException("no StandardIncrementDispatcher type for :".$resourceType);
        }
        return $tasks;
    }
}