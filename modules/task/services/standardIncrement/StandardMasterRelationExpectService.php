<?php
/**
 *
 */

namespace app\modules\task\services\standardIncrement;


use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\DataStandardMasterRelationExpect;
use app\modules\data\models\StandardDataMetadata;
use app\modules\match\models\Match;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\StandardDataGroup;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataStage;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\autoMatch\AutoMatchService;
use app\modules\task\services\crontabTask\AutoCreateMatch;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\StandardDataService;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentStage;
use yii\db\Exception;

class StandardMasterRelationExpectService
{
    public static function refresh($resourceType, $standardIds = [], $majorIds = [], $typeExt = "")
    {
        switch ($resourceType) {
            case Consts::RESOURCE_TYPE_TEAM:
                self::refreshTeam($standardIds, $majorIds, $typeExt);
                break;
            case Consts::RESOURCE_TYPE_SERIES:
                self::refreshSeries($standardIds, $majorIds);
                break;
            case Consts::RESOURCE_TYPE_PLAYER:
                self::refreshPlayer($standardIds, $majorIds);
                break;
            case Consts::RESOURCE_TYPE_METADATA:
                self::refreshMetadata($standardIds, $majorIds, $typeExt);
                break;
            case Consts::RESOURCE_TYPE_TOURNAMENT:
                self::refreshTournament($standardIds, $majorIds, $typeExt);
                break;
            case QueueServer::QUEUE_RESOURCE_AB_STAGE:
                self::refreshStage($standardIds, $majorIds, $typeExt);
                break;
            case QueueServer::QUEUE_RESOURCE_SUBSTAGE:
                self::refreshSubStage($standardIds, $majorIds, $typeExt);
                break;
            case QueueServer::QUEUE_RESOURCE_MATCH:
                self::refreshMatch($standardIds, $majorIds, $typeExt);
                break;
            default:
                return [];
                throw new TaskException("no StandardMasterRelationExpectService for: " . $resourceType);
                break;
        }
    }

    private static function refreshPlayer($standardIds, $majorIds = [])
    {
        if ($standardIds && count($standardIds)) {
            $standardPlayers = StandardDataPlayer::find()->where(['in', 'id', $standardIds])->all();
            foreach ($standardPlayers as $key => $stp) {
                // 只能刷新未绑定的
                print_r($stp->toArray());
                if ($stp["deal_status"] == 4) {
                    continue;
                }
                $nickName = $stp["nick_name"];
                if(!$nickName){
                    continue;
                }
                if(mb_strlen($nickName) > 1){
                    $q = Player::find()->select("id,nick_name,name,name_cn")->Where(
                        ['and',
                            ["=", "game_id", $stp["game_id"]],
                            ["=", "deleted", 2],
                            ["like", "nick_name", $nickName],
                        ]
                    );
                    $sql=$q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                    self::refreshRelationExpectDb($temRelInfo, $stp, Consts::RESOURCE_TYPE_PLAYER);
                }
            }
        }

        if ($majorIds && count($majorIds)) {
            $majorPlayers = Player::find()->where(['in', 'id', $majorIds])->all();
            $tmpstandardIds = [];
            foreach ($majorPlayers as $key => $mp) {
                $q = StandardDataPlayer::find()->select("id,nick_name,name,name_cn")->Where(
                    ['and',
                        ["=", "game_id", $mp["game_id"]],
                        ["<>", "deal_status", 4],
                        ['or',
                            ["like", "nick_name", sprintf("%s", addslashes($mp["nick_name"]))],
                            'locate(nick_name,"'.addslashes($mp["nick_name"]).'")'
                        ]
                    ]
                );
                $sql = $q->createCommand()->getRawSql();
                $temRelInfo = $q->asArray()->all();
                if ($temRelInfo) {
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($temRelInfo, "id"));
                }
                $res = DataStandardMasterRelationExpect::find()->select('standard_id')->where(['resource_type'=>'player'])->andWhere(['in','master_id',$majorIds])->all();
                if($res){
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($res, "standard_id"));
                }
            }
            if (count($tmpstandardIds)) {
                self::refreshPlayer($tmpstandardIds);
            }
        }
    }

    private static function refreshTeam($standardIds, $majorIds = [], $typeExt)
    {
        if ($standardIds && count($standardIds)) {
            $standardTeam = StandardDataTeam::find()->where(['in', 'id', $standardIds])->all();
            foreach ($standardTeam as $key => $stp) {
                print_r($stp->toArray());
                if ($stp["deal_status"] == 4) {
                    continue;
                }
                $name = $stp["name"];
                if(mb_strlen($name) > 1){
                    $q = Team::find()->select("id,name,short_name,name")->Where(
                        ['and',
                            ["=", "game", $stp["game_id"]],
                            ["=", "deleted", 2],
                            ['or',
                                ["like", "name", $name],
                                ['like', 'full_name', $name],
                                ['like', 'short_name', $name],
                                ['like', 'alias', $name]

                            ],
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->limit(20)->asArray()->all();
                    self::refreshRelationExpectDb($temRelInfo, $stp, Consts::RESOURCE_TYPE_TEAM);
                }
            }
        }

        if ($majorIds && count($majorIds)) {
            $majorPlayers = Team::find()->where(['in', 'id', $majorIds])->all();
            $tmpstandardIds = [];
            foreach ($majorPlayers as $key => $mp) {
                $q = StandardDataTeam::find()->select("id,name")->Where(
                    ['and',
                        ["=", "game_id", $mp["game"]],
                        ["<>", "deal_status", 4],
                        ['or',
                            ["like", "name", $mp["name"]],
                            'locate(name,"'.$mp["name"].'")'
                        ]
                    ]
                );
//                $sql=$q->createCommand()->getRawSql();
                $temRelInfo = $q->limit(20)->asArray()->all();
                if ($temRelInfo) {
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($temRelInfo, "id"));
                }
                $res = DataStandardMasterRelationExpect::find()->select('standard_id')->where(['resource_type'=>'team'])->andWhere(['in','master_id',$majorIds])->all();
                if($res){
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($res, "standard_id"));
                }
            }
            if (count($tmpstandardIds)) {
                self::refreshTeam($tmpstandardIds, [], '');
            }
        }
    }

    private static function refreshMetadata($standardIds, $majorIds = [], $typeExt='')
    {
        if ($standardIds && count($standardIds)) {
            $standardTeam = StandardDataMetadata::find()->where(['in', 'id', $standardIds])->all();

            foreach ($standardTeam as $key => $stp) {
                print_r($stp->toArray());
                if ($stp["deal_status"] == 4) {
                    continue;
                }

                if (!$typeExt) {
                    continue;
                }
                $temRelInfo = null;
                $className = MetadataService::getObjByType($typeExt);
                if ($typeExt == Consts::METADATA_TYPE_DOTA2_TALENT) {  //dota2的天赋
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['talent']){
                            $where_or[] = ["like", "talent", (string)$stp["talent"]];
                            $matching = true;
                        }
                        if($stp['talent_cn']){
                            $where_or[] = ["like", "talent_cn", (string)$stp["talent_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] =  ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching ==false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            //改变推荐状态为1
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,external_id,talent,slug")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                } else if ($typeExt == Consts::METADATA_TYPE_DOTA2_HERO || $typeExt == Consts::METADATA_TYPE_LOL_CHAMPION) {   //dota2,lol英雄
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] = ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['title']){
                            $where_or[] =  ["like", "title", (string)$stp["title"]];
                            $matching = true;
                        }
                        if($stp['title_cn']){
                            $where_or[] =  ["like", "title_cn", (string)$stp["title_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,external_id,name,name_cn,slug")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                    $where_or
                            ]
                        )->asArray()->all();
                    }
                }  else if ($typeExt == Consts::METADATA_TYPE_DOTA2_ABILITY || $typeExt == Consts::METADATA_TYPE_LOL_ABILITY) {  //lol,dota2技能
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,name,name_cn")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                }else if ($typeExt == Consts::METADATA_TYPE_DOTA2_ITEM || $typeExt == Consts::METADATA_TYPE_LOL_ITEM) { //dota2道具，lol道具
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] = ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($stp['external_name']) {
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }

                        $q = $className::find()->select("id,external_id,external_name,name")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                 $where_or
                            ]
                        );
                        $sql = $q->createCommand()->getRawSql();
                        $temRelInfo = $q->asArray()->all();
                    }
                }else if ($typeExt == Consts::METADATA_TYPE_LOL_SUMMONER_SPELL) { //lol召唤师技能
                    if($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,external_id,external_name,name,name_cn,slug")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                }else if ($typeExt == Consts::METADATA_TYPE_LOL_RUNE) { //lol符文
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,external_id,external_name,name,name_cn,slug")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                }else if ($typeExt == Consts::METADATA_TYPE_CSGO_MAP) { //csgo地图
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['name_cn']){
                            $where_or[] = ["like", "name_cn", (string)$stp["name_cn"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }
                        $temRelInfo = $className::find()->select("id,external_id,external_name,name,name_cn,slug")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                }else if ($typeExt == Consts::METADATA_TYPE_CSGO_WEAPON) { //csgo武器
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_id']){
                            $where_or[] = ["=", "external_id", (string)$stp["external_id"]];
                            $matching = true;
                        }
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $where_or[] = ["like", "external_name", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }

                        $temRelInfo = $className::find()->select("id,external_id,external_name,name")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
//                    )->createCommand()->getRawSql();
                    }
                }else {
                    if ($stp) {
                        $matching = false;
                        $where_or =['or',
                        ];
                        if($stp['external_name']){
                            $where_or[] = ["like", "external_name", (string)$stp["external_name"]];
                            $matching = true;
                        }
                        if($stp['name']){
                            $where_or[] =  ["like", "name", (string)$stp["name"]];
                            $matching = true;
                        }
                        if($stp['slug']){
                            $where_or[] = ["like", "slug", (string)$stp["slug"]];
                            $matching = true;
                        }
                        if($matching == false){
                            //查询该条数据是否有推荐绑定的关系
                            $expect = DataStandardMasterRelationExpect::find()->where(['and',
                                ['=','resource_type',$typeExt],
                                ['=','standard_id',$stp['id']],
                            ])->one();
                            if($expect){
                                $expect->delete();
                                $stp['deal_status'] = 1;
                                $stp->save();
                            }
                            continue;
                        }

                        $temRelInfo = $className::find()->select("id,name")->Where(
                            ['and',
                                ["<>", "flag", 2],
                                ["=", "deleted", 2],
                                $where_or
                            ]
                        )->asArray()->all();
                    }
                }
                self::refreshRelationExpectDb($temRelInfo, $stp, $typeExt);
            }
        }

        if ($majorIds && count($majorIds)) {
            $className = MetadataService::getObjByType($typeExt);
            $majorPlayers = $className::find()->where(['in', 'id', $majorIds])->all();
            $tmpstandardIds = [];
            foreach ($majorPlayers as $key => $mp) {
                if ($typeExt == Consts::METADATA_TYPE_DOTA2_TALENT) {
                    $where_or =['or',
                    ];
                    if($mp['talent']){
                        $where_or[] = ["like", "talent", sprintf("%s", $mp["talent"])];
                        $where_or[] = "locate(talent,'".addslashes($mp["talent"])."')";
                    }
                    if($mp['talent_cn']){
                        $where_or[] = ["like", "talent_cn", sprintf("%s", $mp["talent_cn"])];
                        $where_or[] = "locate(talent_cn,'".addslashes($mp["talent_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else if ($typeExt == Consts::METADATA_TYPE_DOTA2_HERO || $typeExt == Consts::METADATA_TYPE_LOL_CHAMPION){ //英雄
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['title']){
                        $where_or[] = ["like", "title", sprintf("%s", $mp["title"])];
                        $where_or[] = "locate(title,'".addslashes($mp["title"])."')";
                    }
                    if($mp['title_cn']){
                        $where_or[] = ["like", "title_cn", sprintf("%s", $mp["title_cn"])];
                        $where_or[] = "locate(title_cn,'".addslashes($mp["title_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else if ($typeExt == Consts::METADATA_TYPE_DOTA2_ABILITY || $typeExt == Consts::METADATA_TYPE_LOL_ABILITY){ //技能
                    $where_or[] ='or';
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else if ($typeExt == Consts::METADATA_TYPE_DOTA2_ITEM || $typeExt == Consts::METADATA_TYPE_LOL_ITEM){  //道具
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else if ($typeExt == Consts::METADATA_TYPE_LOL_SUMMONER_SPELL){  //召唤师技能
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".$mp["external_name"]."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else if ($typeExt == Consts::METADATA_TYPE_LOL_RUNE){
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                } elseif($typeExt == Consts::METADATA_TYPE_CSGO_WEAPON) {
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }else{
                    $where_or =['or',
                    ];
                    if($mp['name']){
                        $where_or[] = ["like", "name", sprintf("%s", $mp["name"])];
                        $where_or[] = "locate(name,'".addslashes($mp["name"])."')";
                    }
                    if($mp['name_cn']){
                        $where_or[] = ["like", "name_cn", sprintf("%s", $mp["name_cn"])];
                        $where_or[] = "locate(name_cn,'".addslashes($mp["name_cn"])."')";
                    }
                    if($mp['external_name']){
                        $where_or[] = ["like", "external_name", sprintf("%s", $mp["external_name"])];
                        $where_or[] = "locate(external_name,'".addslashes($mp["external_name"])."')";
                    }
                    if($mp['external_id']){
                        $where_or[] = ["like", "external_id", sprintf("%s", $mp["external_id"])];
                        $where_or[] = "locate(external_id,'".addslashes($mp["external_id"])."')";
                    }
                    if($mp['slug']){
                        $where_or[] = ["like", "slug", sprintf("%s", $mp["slug"])];
                        $where_or[] = "locate(slug,'".addslashes($mp["slug"])."')";
                    }

                    $q = StandardDataMetadata::find()->select("*")->Where(
                        ['and',
                            ["=","metadata_type",$typeExt],
                            ["=", "deleted", 2],
                            $where_or
                        ]
                    );
                    $sql = $q->createCommand()->getRawSql();
                    $temRelInfo = $q->asArray()->all();
                }


                if ($temRelInfo) {
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($temRelInfo, "id"));
                }
                $res = DataStandardMasterRelationExpect::find()->select('standard_id')->where(['resource_type'=>$typeExt])->andWhere(['in','master_id',$majorIds])->all();
                if($res){
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($res, "standard_id"));
                }
//
//                if ($temRelInfo) {
//                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($temRelInfo, "id"));
//                }
            }
            if (count($tmpstandardIds)) {
                self::refreshMetadata($tmpstandardIds, [], $typeExt);
            }
        }
    }

    private static function refreshTournament($standardIds, $majorIds,$typeExt='')
    {
        if ($standardIds && count($standardIds)) {
            $standardTeam = StandardDataTournament::find()->where(['in', 'id', $standardIds])->andWhere(['type'=>1])->all();

            foreach ($standardTeam as $key => $stp) {
                print_r($stp->toArray());
                if ($stp["deal_status"] == 4) {
                    continue;
                }

                if(mb_strlen($stp['name']) >1){
                    $temRelInfo = Tournament::find()->select("id,name,name_cn,short_name")
                        ->where(['deleted'=>2])
                        ->andWhere(['game'=>$stp['game_id']])
                        ->andWhere(["like","name",trim($stp["name"])])
                        ->limit(20)->asArray()->all();
                    self::refreshRelationExpectDb($temRelInfo,$stp, Consts::RESOURCE_TYPE_TOURNAMENT);
                }
            }
        }

        if ($majorIds && count($majorIds)) {
            $majorPlayers = Tournament::find()->where(['in', 'id', $majorIds])->all();
            $tmpstandardIds = [];
            foreach ($majorPlayers as $key => $mp) {
                $temRelInfo = StandardDataTournament::find()->select("id,name,name_cn,short_name")->Where(
                    ["or",
                        ["like", "name", trim($mp["name"])],
                        "locate(name,'".$mp["name"]."')"
                    ]
                )->limit(20)->asArray()->all();
                if ($temRelInfo) {
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($temRelInfo, "id"));
                }
                $res = DataStandardMasterRelationExpect::find()->select('standard_id')->where(['resource_type'=>'tournament'])->andWhere(['in','master_id',$majorIds])->all();
                if($res){
                    $tmpstandardIds = array_merge_recursive($tmpstandardIds, array_column($res, "standard_id"));
                }
            }
            if (count($tmpstandardIds)) {
                self::refreshTournament($tmpstandardIds, []);
            }
        }
    }

    // 处理相似绑定
    private static function refreshMatch($standardIds, $majorIds = [])
    {
        if ($standardIds && count($standardIds)) {
            $matchInfos=self::getMatchInfoByIds($standardIds,[]);
            foreach($matchInfos as $key=>$matchInfo){
                // 如果有值没绑定，提示没绑定
                $needs=[
                    'tm1_master_id',
                    'tm2_master_id',
                    'scheduled_begin_at',
                    'trmt_master_id'
                ];
                $missingKeys=[];
                foreach($needs as $v){
                    if(!$matchInfo[$v]){
                        $missingKeys[]=$v;
                    }
                }
                $standardModel = StandardDataMatch::find()->where(['in', 'id', $matchInfo['std_match_id']])->one();
                $relInfo=[];
                if(count($missingKeys)){
                    // 有缺失数据
                    $standardModel->setAttribute('missing_field_for_binding',json_encode($missingKeys));
                    $standardModel->save();
                }else{
                    $standardModel->setAttribute('missing_field_for_binding',null);
                    $standardModel->save();
                    // 没有缺失数据，查看likeids
                    $likeIdsString=$matchInfo['like_ids'];
                    $likeIds=[];
                    if($likeIdsString){
                        $likeIds=explode(',',$likeIdsString);
                    }
                    foreach ($likeIds as $id){
                        $relInfo[]=['id'=>$id];
                    }
                }
                self::refreshRelationExpectDb($relInfo, $standardModel, Consts::RESOURCE_TYPE_MATCH);
            }
        }
        if ($majorIds && count($majorIds)) {
            $matchInfos=self::getMatchInfoByIds([],$majorIds);
            $standardIds=array_column($matchInfos,'std_match_id');
            $res = DataStandardMasterRelationExpect::find()->select('standard_id')->where(['resource_type'=>'match'])->andWhere(['in','master_id',$majorIds])->all();
            if($res){
                $standardIds = array_merge_recursive($standardIds, array_column($res, "standard_id"));
            }
            if (count($standardIds)) {
                self::refreshMatch($standardIds, []);
            }
        }
    }

    public static function refreshBdMatch($standardIds){
        if ($standardIds && count($standardIds)) {
            $matchInfos=self::getBdMatchInfoByIds($standardIds,[]);
            foreach($matchInfos as $key=>$matchInfo){
                // 如果有值没绑定，提示没绑定
                $needs=[
                    'tm1_master_id',
                    'tm2_master_id',
                    'scheduled_begin_at',
                    'trmt_master_id'
                ];
                $missingKeys=[];
                foreach($needs as $v){
                    if(!$matchInfo[$v]){
                        $missingKeys[]=$v;
                    }
                }
                $standardModel = StandardDataMatch::find()->where(['in', 'id', $matchInfo['std_match_id']])->one();
                if(count($missingKeys)){
                    // 有缺失数据
                    $standardModel->setAttribute('missing_field_for_binding',json_encode($missingKeys));
                    $standardModel->save();
                }else{
                    $standardModel->setAttribute('missing_field_for_binding',null);
                    $standardModel->save();
                }
            }
        }
    }
    //绑定的比赛
    public static function getBdMatchInfoByIds($standardIds)
    {
        $sqlTpl = AutoCreateMatch::getBdMatchSqlTemplate();
        if ($standardIds) {
            // 根据ids获取关联关系
            $sql = $sqlTpl.sprintf(" and std_match.id in (%s) \n group by std_match.id", implode(",", $standardIds));
            $matchList = \Yii::$app->getDb()->createCommand($sql)->queryAll();
            return $matchList;
        }
    }

    public static function getMatchInfoByIds($standardIds,$matchIds)
    {
        $sqlTpl=AutoCreateMatch::getSqlTemplate();
        if($standardIds){
            // 根据ids获取关联关系
            $sql = $sqlTpl . sprintf(" and std_match.id in (%s) \n group by std_match.id", implode(",",$standardIds));
            $matchList = \Yii::$app->getDb()->createCommand($sql)->queryAll();
            return $matchList;
        }
        if($matchIds){
            // 根据ids获取关联关系
            $sql = $sqlTpl . sprintf(" and main_match_like.id in (%s) \n group by std_match.id", implode(",",$matchIds));
            $matchList = \Yii::$app->getDb()->createCommand($sql)->queryAll();
            return $matchList;
        }
    }

    /**
     * @param $temRelInfo
     * @param \yii\db\ActiveRecord $stp
     * @throws \yii\db\Exception
     */
    private static function refreshRelationExpectDb($temRelInfo, \yii\db\ActiveRecord $stp, $resourceType): void
    {
        if ($temRelInfo) {
            $count = count($temRelInfo);
            if ($count === 1) {
                $statusInfo = [
                    'deal_status' => 2, // 有一个推荐待绑定
                ];
            } else {
                $statusInfo = [
                    'deal_status' => 3, // 有多个推荐待绑定
                ];
            }
        } else {
            $statusInfo = [
                'deal_status' => 1, // 没有推荐的
            ];
        }
        //开启事务，防止推荐绑定的时候正好在进行绑定操作
        $transaction = \Yii::$app->db->beginTransaction();
        try {
//            $stp->updateAll($statusInfo,['<>','deal_status',4]);
//            $stp->setAttributes($statusInfo);
//            $stp->save();
            // 插入预期关联关系
            $userkey = ['login', 'password', 'nicename', 'email', 'create_time'];//测试数据键
            $standardClass = StandardDataService::getStandardActiveTable($resourceType);
            $standard = $standardClass::find()->where(['id'=>$stp['id']])->one();
            if($standard['deal_status'] != 4){
                $standard->setAttributes($statusInfo);
                $standard->save();
                $transaction->commit();
                $insertDt = [];
                foreach ($temRelInfo as $key => $val) {
                    $insertDt[] = [$resourceType, $stp['id'], $val['id']];
                }
                // 删除关系
                \Yii::$app->db->createCommand('delete from data_standard_master_relation_expect where standard_id=' . $stp['id'] . ' and resource_type="' . $resourceType . '"')->execute();
                // 重建关系
                $res = \Yii::$app->db->createCommand()->batchInsert('data_standard_master_relation_expect',
                    ['resource_type', 'standard_id', 'master_id'], $insertDt)->execute();//执行批量添加
            }else{
                throw new Exception("绑定状态错误");
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
