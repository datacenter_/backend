<?php


namespace app\modules\task\services\wsdata;


use app\modules\data\models\StandardDataMetadata;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\metadata\services\MetadataService;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\StandardDataService;
use app\rest\exceptions\BusinessException;

class LivedMatchBase extends PandascoreHotBase
{
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据identityId和originId获取对应的主表id，有可能是空
     */
    public static function getMainIdByRelIdentityId($resourceType,$relIdentityId,$originId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.rel_identity_id',(int)$relIdentityId],
//            ['=','rel.resource_type',$resourceType]
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->asArray()
            ->where($where);
//        $sql=$q->createCommand()->getRawSql();
        $info=$q->one();
        if($info){
            return $info['master_id'];
        }
    }

    public static function getExternalIdByExternalName($resourceType,$externalName,$originId)
    {
        $metadataClass = MetadataService::getObjByType($resourceType);

//        if($standardClass == StandardDataMetadata::class){
//            $where[] =[
//                ['=','std.resource_type',$resourceType]
//            ];
//        }
        $info=$metadataClass::find()->where(['external_name'=>$externalName])->one();
        if($info){
            return $info['id'];
        }
    }
}