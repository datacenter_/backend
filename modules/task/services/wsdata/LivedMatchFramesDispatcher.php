<?php
/**
 *
 */

namespace app\modules\task\services\wsdata;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataLog;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\pandascore\PandascoreBase;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\task\services\QueueServer;
use PandascoreLolws;
use yii\db\Query;

class LivedMatchFramesDispatcher extends LivedMatchBase
{
//    public static function run($tagInfo,$taskInfo){
//        $originType=$tagInfo["origin_type"];
//        switch ($originType){
//            case QueueServer::QUEUE_ORIGIN_PANDASCORE:
//                $tasks= PandascoreLolws::run($tagInfo,$taskInfo);
//                break;
//            case QueueServer::QUEUE_ORIGIN_ABIOS:
//                $tasks= AbiosDispatcher::run($tagInfo,$taskInfo);
//                break;
//            default:
//                throw new TaskException("can not deal with this type :".$originType);
//                break;
//        }
//        return $tasks;
//    }

    public static function run($tagInfo, $taskInfo)
    {
        $timeStart = date('Y-m-d H:i:s');
        $info = json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $game_id = $info['game_id'];
        $match_data = $info['match_data'];
        $match_data['frame_type'] = $info['type'];
        $type = $info['type'];
        $rel_battle_id = $match_data['game']['id'];
        $rel_match_id = $match_data['match']['id'];
        //panda events  TODO 需要修改 另一个源数据
        if ($originId != 3) {
            return;
        }
        //找到对应的平台的match_id
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        //比赛信息
        $matchInfo = Match::find()->where(['id' => $matchId])->one();
//        self::setStatus($hotInfo, 2);
        $restInfos = self::getInfos($rel_match_id);
        //        $restInfos = self::getInfosTest();
        // 这里传matchInfo，用来判断主队客队
        $infoFormat = self::conversion($restInfos, $matchInfo,$match_data);
        $infoFormatAddBindingInfo = self::addBindingInfo($infoFormat);
        $timeEnd = date('Y-m-d H:i:s');
        $extInfo = [
            'time_begin' => $timeStart,
            'time_end' => $timeEnd,
            'task_id' => $taskInfo['id'],
            'match_id' => $matchId,
        ];
        // 添加绑定信息，转义,主要出现在battle的详情中
//        self::addHotLog($hotId, $restInfos,$infoFormatAddBindingInfo,$extInfo);
        // 更新到数据库
        self::refreshInfo($matchId, $infoFormatAddBindingInfo);
        // 更新hotdata状态
//        self::refreshHotDataStatusByMatchInfo($hotId);
    }

    public static function addBindingInfo($infoFormat)
    {
        $match = $infoFormat['match'];
        $default_advantage = self::getMainIdByRelIdentityId('team', $infoFormat['match']['default_advantage'], '3');
        if (!$default_advantage) {
            $default_advantage = -1;
        }
        $infoFormat['match']['default_advantage'] = $default_advantage;
        $infoFormat['match']['winner'] = self::getMainIdByRelIdentityId('team', $infoFormat['match']['winner'], '3');
        $infoFormat['match']['map_info'] = json_encode([1]);
        foreach ($infoFormat['battles'] as &$battle) {

            // 更新选手中的选手id，战队id，item中的装备
            $pick_array = [];
            foreach ($battle['players'] as $key => &$player) {
                //base
                $battle['base']['winner'] = (int)$infoFormat['match']['winner'];
                $battle['base']['map'] = 1;  //数据源没该数据，取值为metadata_lol_map表的召唤师峡谷
                $player['team_id'] = self::getMainIdByRelIdentityId('team', $player['rel_team_id'], '3');
                $player['champion'] = self::getMainIdByRelIdentityId('lol_champion', $player['champion'], '3');
                $player['player_id'] = self::getMainIdByRelIdentityId('player', $player['player_id'], '3');
                $spell1 = self::getMainIdByRelIdentityId('lol_summoner_spell', $player['spells'][0]['id'], '3');
                $spell2 = self::getMainIdByRelIdentityId('lol_summoner_spell', $player['spells'][1]['id'], '3');
                $player['summoner_spells'] = json_encode([$spell1, $spell2]);  //召唤师技能
                $originPlayerRole = $player['role'];
                $player['role'] = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE, $originPlayerRole);  //位置
                $player['seed'] = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE, $originPlayerRole);  //位置
                $player['order'] = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE, $originPlayerRole);  //位置
                $player['lane'] = Common::getLaneIdByRole($player['role']);  //分路id
                $item_array = [];
                foreach ($player['items'] as $item) {
                    $item_array[] = self::getMainIdByRelIdentityId('lol_item', $item['id'], 3);
                }
                $player['items'] = json_encode($item_array);
            }

            foreach ($battle['static'] as &$static) {
                $static['team_id'] = self::getMainIdByRelIdentityId('team', $static['rel_team_id'], '3');
            }
            foreach ($battle['events'] as &$event) {
                $event['event_type'] = Common::geGoalIdLOLEventTypeByEvent(
                    Consts::ORIGIN_PANDASCORE,
                    $event['event_type']
                );
                if ($event['killer_ingame_obj_type'] == "player") {  //击杀者id
                    $event['killer'] = self::getMainIdByRelIdentityId('player', $event['killer'], '3');
                }
                $event['killer_ingame_obj_type'] = self::getGoalIdByPandascoreName($event['killer_ingame_obj_type']);
                if ($event['victim_name']) {  //被击杀的为野怪
                    $event['victim'] = self::getGoalIdByPandascoreName($event['victim_name']);
                } elseif ($event['victim_player_name']) {  //被击杀的为选手
                    $event['victim'] = self::getMainIdByRelIdentityId('player', $event['victim_player_id'], '3');
                }
                $event['victim_ingame_obj_type'] = self::getGoalIdBySubType($event['victim_ingame_obj_type']);
                $event['victim_sub_type'] = self::getGoalIdByPandascoreName($event['victim_sub_type']);
//                $event['killer_sub_type']='';
                //助攻
                $assist_array = [];
                if (isset($event['assist']) && $event['assist']) {
                    foreach ($event['assist'] as $value) {
                        $assist_array[] = self::getMainIdByRelIdentityId('player', $value, '3');
                    }
                }
                $event['assist'] = json_encode($assist_array);
                //助攻类型
                $assist_type_array = [];
//                if(isset($event['assist_ingame_obj_type']) && $event['assist_ingame_obj_type']){
//                    foreach ($event['assist_ingame_obj_type'] as $value){
//                       $assist_type_array[] =  $event['assist_ingame_obj_type'] = self::getGoalIdBySubType($value);
//                    }
//                    $event['assist_ingame_obj_type'] = json_encode($assist_type_array);
//                }
                $event['assist_ingame_obj_type'] = self::getGoalIdBySubType($event['assist_ingame_obj_type']);

            }
            if (count($battle['ban_pick']) > 0) {
                foreach ($battle['ban_pick'] as &$ban_pick_val) {
                    $ban_pick_val['hero'] = self::getMainIdByRelIdentityId('lol_champion', $ban_pick_val['hero'], '3');
                    $ban_pick_val['team'] = self::getMainIdByRelIdentityId('team', $ban_pick_val['team'], '3');
                }
            }
        }

        return $infoFormat;
    }

    public static function getInfosTest()
    {
        //
        $info = HotDataLog::find()->where(['id' => 17])->one();
        $infoRest = json_decode($info['info_rest'], true);

        return $infoRest;
    }

    public static function refreshInfo($matchId, $info)
    {
        self::setMatch($matchId, $info['match']);
        foreach ($info['battles'] as $key => $battle) {
            // 根据battleId刷新对应的battle信息
            self::refreshBattle($matchId, $battle, $key);
        }
    }

    private static function refreshBattle($matchId, $battle, $key)
    {
        // 找到对应的battleId，更新battle
        $battleBase = MatchBattle::find()->where(
            [
                'match' => $matchId,
                'order' => $battle['base']['order'],
            ]
        )->one();
        if (!$battleBase) {
            $battleBase = new MatchBattle();
            $battleBase->setAttributes(
                [
                    'match' => $matchId,
                    'order' => $battle['base']['order'],
                ]
            );
            $battleBase->save();
        }
        $battleId = $battleBase['id'];

        BattleService::setBattleInfo($battleId, 'lol', $battle['base'], BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0) {
            BattleService::setBattleInfo($battleId, 'lol', $battle['ban_pick'], BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId, 'lol', $battle['detail'], BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId, 'lol', $battle['static'], BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId, 'lol', $battle['players'], BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId, 'lol', $battle['events'], BattleService::DATA_TYPE_EVENTS);
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    public static function getStandardMatchInfo($standardId)
    {
        return StandardDataMatch::find()->where(['id' => $standardId])->one();
    }

    public static function getBindingInfo($standardId)
    {
        $q = (new Query())
            ->select('match.*')
            ->from('data_standard_master_relation as rel')
            ->leftJoin('match', 'rel.master_id=match.id')
            ->andWhere(['standard_id' => $standardId, 'resource_type' => Consts::RESOURCE_TYPE_MATCH]);

        return $q->one();
    }

    public static function getInfos($relMatchId)
    {
        $matchInfo = self::getMatch($relMatchId);
        $battles = $matchInfo['games'];
        $battleInfos = [];
        foreach ($battles as $key => $battle) {
            $battleId = $battle['id'];
            if ($battle['status'] == 'not start') {
                continue;
            }
            $battleBase = self::getBattle($battleId);
            $events = self::getEvents($battleId);
            $frames = self::getFrames($battleId);
            $battleInfos[] = [
                'base' => $battleBase,
                'events' => $events,
                'frames' => $frames,
            ];
        }

        return [
            'match' => $matchInfo,
            'battles' => $battleInfos
        ];
    }

    private static function getMatchMapInfo($matchFromRest, $matchId)
    {
        // 主队客队绑定,
        // 取match信息中的战队信息，与rest中对比，如果不对，报警
        // 根据主队中的映射关系，获取对应关系
        $matchInfo = Match::find()->where(['id' => $matchId])->one();
        $team1Id = $matchInfo['team_1_id'];
        $team2Id = $matchInfo['team_2_id'];

        $teamFromRestMapTeamId = [];
//        $teamMap = [];
        $hasOrder = true;
        foreach ($matchFromRest['results'] as $key => $val) {
            $info = self::getTeamFromRel($val['team_id']);
            $order = $info && $matchInfo ? ($matchInfo['team_1_id'] == $info['id'] ? 1 : ($matchInfo['team_2_id'] == $info['id'] ? 2 : 0)) : 0;
            $teamFromRestMapTeamId[$val['team_id']] = [
                'team_id' => $info['id'],
                'order' => $order,
                'origin_order' => $key
            ];
//            $teamFromRestMapOrder[$key] = [
//                'team_id' => $info['id'],
//                'order' => $order];
            if (!$order) {
                $hasOrder = false;
            }
        }
        // 如果是false,表示不能找到所有战队的映射关系，用原版的排序
        if (!$hasOrder) {
            foreach ($teamFromRestMapTeamId as $key => $val) {
                $teamFromRestMapTeamId[$key]['order'] = $teamFromRestMapTeamId[$key]['origin_order'] + 1;
            }
        }

        return $teamFromRestMapTeamId;
    }

    private static function getTeamFromRel($teamId)
    {
        $teamInfo = Team::find()
            ->leftJoin('data_standard_master_relation rel', 'team.id=rel.master_id and rel.resource_type="team"')
            ->leftJoin('standard_data_team std', 'std.id=rel.standard_id')
            ->where(['std.rel_identity_id' => $teamId])->one();

        return $teamInfo;
    }

    /**
     * @param $restInfo
     * @param $matchInfo 比赛基础信息这里有主队客队id
     * @return array
     *
     */
    public static function conversion($restInfo, $matchInfo,$frame="")
    {
        // 转化match
        $formatData = [
            'match' => '',
            'battles' => [],
        ];

        // 这里记录了关联的team
        $teamInitInfo = [];

        // 遍历result，这里面有主队客队对应关系

        $detailInfoConfig = [
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['end_at']));
            },
            'team_1_score' => function ($info) {
                return $info['results'][0]['score'];
            },
            'team_2_score' => function ($info) {
                return $info['results'][1]['score'];
            },
            'is_battle_detailed' => function ($info) {
                return $info['detailed_stats'] ? 1 : 2;
            },
            'is_draw' => function ($info) {
                return $info['draw'] ? 1 : 2;
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            'default_advantage' => function ($info) {
                return $info['game_advantage'] ? $info['game_advantage'] : null;
            },
            'status' => function ($info) {
                return Common::getPandaScoreMatchStatus($info['status']);
            },
            'winner' => 'winner_id',
        ];

        $matchDetailInfo = Mapping::transformation($detailInfoConfig, $restInfo['match']);
        $formatData['match'] = $matchDetailInfo;

        foreach ($restInfo['battles'] as $key => $restBattle) {
            // 格式化battle信息，这里需要一些team的信息
            $battleInfo = self::conversionBattle($restBattle, $restInfo, $matchInfo,$frame);
            $battleInfoEvent = self::conversionBattleEvent($restBattle, $restInfo, $matchInfo,$frame);

            $formatData['battles'][] = array_merge($battleInfo, $battleInfoEvent);
        }

        return $formatData;
    }

    private static function conversionBattleFrames($restBattleInfo, $restInfo, $matchInfo, $order_key)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleFramesInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];
        //battles->frames
        //组合数据
        $battleFramesInfoConfig = [
            'begin_at' => date('Y-m-d H:i:s', strtotime($restInfo['match']['begin_at'])),
            'end_at' => date('Y-m-d H:i:s', strtotime($restInfo['match']['end_at'])),
            'is_forfeit' => $restInfo['match']['forfeit'] ? 1 : 2,
            'status' => $restInfo['match']['finished'] == 'finished' ? 3 : 2,
            'order' => $restInfo['match']['games'][$order_key]['position'],
            'duation' => (string)$restInfo['match']['games'][$order_key]['length'],
            'rel_battle_id' => (string)$restBattleInfo['base']['id'],
        ];
        // 获取battle详情,这里用来设置id
        $battleFramesInfo['base'] = $battleFramesInfoConfig;
        //定义frame内容
        $lastFramesInfo = count($restBattleInfo['frames']) > 0 ? end($restBattleInfo['frames']) : '';
        $game_id = $lastFramesInfo['game_id'];
//        $game_finished = $lastFramesInfo['game']['finished'];
//        $winner_id = $lastFramesInfo['game']['winner_id'];
        $battleDetail = $teamStatic = [];
        //battleDetail
        $battleDetail['first_blood_p_tid'] = 0;
        $battleDetail['first_baron_nashor_p_tid'] = 0;
        $battleDetail['first_dragon_p_tid'] = 0;
        $battleDetail['first_inhibitor_p_tid'] = 0;
        $battleDetail['first_tower_p_tid'] = 0;
        $battleFramesInfo['detail'] = $battleDetail;
        //$teamStatic
        $teamInfosRest = [$lastFramesInfo['blue'], $lastFramesInfo['red']];
        foreach ($teamInfosRest as $key => $team) {
            $teamStatic[$key]['rel_team_id'] = @$team['id'];
            $teamStatic[$key]['rel_identity_id'] = $team['id'];
            $teamStatic[$key]['kills'] = $team['kills'];
            $teamStatic[$key]['gold'] = $team['gold'];
            $teamStatic[$key]['faction'] = $key == 0 ? 'blue' : 'red';
            $teamStatic[$key]['inhibitor_kills'] = $team['inhibitors'];
            $teamStatic[$key]['turret_kills'] = $team['towers'];
            $teamStatic[$key]['baron_nashor_kills'] = $team['nashors'];

            $teamStatic[$key]['dragon_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['cloud_drake_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['elder_drake_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['infernal_drake_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['mountain_drake_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['ocean_drake_kills'] = '';
            $teamStatic[$key]['dragon_kills_detail']['cloud_drake_kills'] = '';
//            $teamStatic[$key]['order']=self::getTeamOrderByTeamRelId(@$team['id'], $matchInfo);
        }
        $battleFramesInfo['static'] = $teamStatic;
        //pick
        $pick_array = [];
        //ban
        $banArray = [];
        $battleFramesInfo['ban_pick'] = array_merge($banArray, $pick_array);
        $players = [];
        //玩家
        $playersRest = array_merge([$lastFramesInfo['blue']], [$lastFramesInfo['red']]);
        //循环玩家列表
        foreach ($playersRest as $keyp => $itemp) {
            $players_key = $itemp['players'];
            $keypp_key = 1;
            foreach ($players_key as $keypp => $itempp) {
                $player = [];
                $player['player_id'] = $itempp['id'];
                $player['game'] = $game_id;
                $player['spells'] = $itempp['summoner_spells'];
                $player['role'] = $keypp;
                $player['champion'] = $itempp['champion']['id'];
                $player['level'] = $itempp['level'];
                $player['kills'] = $itempp['kills'];
                $player['double_kill'] = '';
                $player['triple_kill'] = '';
                $player['quadra_kill'] = '';
                $player['penta_kill'] = '';
                $player['largest_multi_kill'] = '';
                $player['largest_killing_spree'] = '';
                $player['deaths'] = $itempp['deaths'];
                $player['assists'] = $itempp['assists'];
                $player['cs'] = $itempp['cs'];
                $player['minions_killed'] = '';
                $player['gold_earned'] = '';
                $player['gold_spent'] = '';
                $player['damage_to_champions'] = '';
                $player['damage_to_champions_physical'] = '';
                $player['damage_to_champions_magic'] = '';
                $player['damage_to_champions_true'] = '';
                $player['damage_taken'] = '';
                $player['damage_taken_physical'] = '';
                $player['damage_taken_true'] = '';
                $player['total_damage'] = '';
                $player['total_damage_magic'] = '';
                $player['total_heal'] = '';
                $player['total_crowd_control_time'] = '';
                $player['wards_placed'] = '';
                $player['wards_kills'] = '';
                $player['turrets_kills'] = '';
                $player['total_neutral_minion_kills'] = '';
                $player['neutral_minion_team_jungle_kills'] = '';
                $player['neutral_minion_enemy_jungle_kills'] = '';
                $player['items'] = [];//$itempp['items'];
                $player['rel_team_id'] = $itemp['id'];
                $player['rel_player'] = '';
                $player['rel_identity_id'] = (string)$itempp['id'];
                $player['rel_id'] = $itempp['id'];
//                $player['team_order']=self::getTeamOrderByTeamRelId($itemp['id'],$matchInfo);
                $player['order'] = $keypp_key;

                // 需要计算得属性
                $deaths = $player['deaths'] ? $player['deaths'] : 1;
                $player['kda'] = ($player['kills'] + $player['assists']) / $deaths * 3;

                $player['participation'] = ($player['kills'] + $player['assists']) / $itemp['kills'];
                $player['gold_remaining'] = '';//$player['gold_earned'] - $player['gold_spent'];
                $player['gold_earned_percent'] = '';//$player['gold_earned']/self::getTeamKills($itemp['id'],$restBattleInfo);
                $player['gpm'] = '';//$player['gold_earned']/$restBattleInfo['base']['length'];
                $player['dpm_to_champions'] = '';//$player['damage_to_champions']/$restBattleInfo['base']['length'];
                $player['damage_percent_to_champions'] = '';//$player['damage_to_champions']/self::getTeamKills($itemp['id'],$restBattleInfo);
                $player['dtpm'] = '';//$player['damage_taken']/$restBattleInfo['base']['length'];
                $player['damage_conversion_rate'] = '';//$player['damage_to_champions']/$player['gold_earned'];
                $players[] = $player;
                $keypp_key++;
            }
        }
        $battleFramesInfo['players'] = $players;
        $battleFramesInfo['events'] = [];

        return $battleFramesInfo;
    }

    private static function conversionBattleEvent($restBattleInfo, $restInfo, $matchInfo,$frame)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'events' => ''
        ];
        //事件
        $eventListRst = $restBattleInfo['events'];
        if($frame['frame_type'] == "events"){
            $eventConfig =[
                "event_type" =>"type",
                "match" =>function($params){
                   return $params['match']['id'];
                },
                "ingame_timestamp"=> "ingame_timestamp",
                "killer" => function ($params) {
                    if (isset($params['payload']['killer']['object']['id']) && $params['payload']['killer']['object']['id']) {
                        return $params['payload']['killer']['object']['id'];
                    }
                },//killer
                "killer_object_name" => function ($params) {
                    return $params['payload']['killer']['name'];
                },
                "killer_ingame_obj_type" => function ($params) {
                    return $params['payload']['killer']['type'];
                },//击杀者游戏内对象类型⇥
//                "killer_sub_type" => function($params){
//                   return $params['payload']['killer']['type'];
//                },//击杀者子类型⇥(暂时没有出现)
                "victim_name" => function ($params) {
                    if (isset($params['payload']['killed']['object']['name']) && $params['payload']['killed']['object']['name']) {
                        return $params['payload']['killed']['object']['name'];
                    }
                },//受害者(为野怪等)
                "victim_player_id" => function ($params) {
                    if (isset($params['payload']['killed']['object']['id']) && $params['payload']['killed']['object']['player_id']) {
                        return $params['payload']['killed']['object']['id'];
                    }
                },
                "victim_player_name" => function ($params) {
                    if (isset($params['payload']['victim']['object']['player_name']) && $params['payload']['victim']['object']['player_name']) {
                        return $params['payload']['victim']['object']['player_name'];
                    }
                },
                "victim_sub_type" => function ($params) {
                    if (isset($params['payload']['killed']['object']['type']) && $params['payload']['killed']['object']['type']) {
                        return $params['payload']['killed']['object']['type'];
                    }
                },  //受害者子类型sub_type
                "victim_ingame_obj_type" => function ($params) {
                    if (isset($params['payload']['killed']['type']) && $params['payload']['killed']['type']) {
                        return $params['payload']['killed']['type'];
                    }
                },//受害者游戏内对象类型
            ];
            $eventInfo = Mapping::transformation($eventConfig, $frame);
            $events[] = $eventInfo;
            $battleInfo['events'] = $events;
        }

        return $battleInfo;
    }

    private static function conversionBattle($restBattleInfo, $restInfo, $matchInfo,$frame)
    {
        // 获取战队映射情况，battle详情里面有两个战队
        $battleInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => ''
        ];

        $battleInfoConfig = [
            'begin_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['begin_at']));
            },
            'end_at' => function ($info) {
                return date('Y-m-d H:i:s', strtotime($info['end_at']));
            },
            'is_forfeit' => function ($info) {
                return $info['forfeit'] ? 1 : 2;
            },
            'status' => function ($info) {
                return $info['status'] == 'finished' ? 3 : 2;
            },
            'order' => 'position',
            'duation' => 'length'
        ];
        $battleBaseAtt = Mapping::transformation($battleInfoConfig, $restBattleInfo['base']);
        $battleBaseAtt['duation'] = (string)$battleBaseAtt['duation'];
        // 获取battle详情,这里用来设置id
        $battleInfo['base'] = $battleBaseAtt;


        $teamInfosRest = $restBattleInfo['base']['teams'];
        $battleDetail = [];
        foreach ($teamInfosRest as $key => $team) {
            // 一血
            if ($team['first_blood']) {
                $battleDetail['first_blood_p_tid'] = $key + 1;
            }
            if ($team['first_baron']) {
                $battleDetail['first_baron_nashor_p_tid'] = $key + 1;
            }
            if ($team['first_dragon']) {
                $battleDetail['first_dragon_p_tid'] = $key + 1;
            }

            if ($team['first_inhibitor']) {
                $battleDetail['first_inhibitor_p_tid'] = $key + 1;
            }
            if ($team['first_tower']) {
                $battleDetail['first_tower_p_tid'] = $key + 1;
            }
            $teamStatic[$key]['rel_team_id'] = @$team['team']['id'];
            $teamStatic[$key]['rel_identity_id'] = $team['team']['id'];
            $teamStatic[$key]['kills'] = $team['kills'];
            $teamStatic[$key]['gold'] = $team['gold_earned'];
            $teamStatic[$key]['faction'] = $team['color'];
            $teamStatic[$key]['inhibitor_kills'] = $team['inhibitor_kills'];
            $teamStatic[$key]['gold'] = $team['gold_earned'];
            $teamStatic[$key]['turret_kills'] = $team['tower_kills'];
            $teamStatic[$key]['baron_nashor_kills'] = $team['baron_kills'];

            $teamStatic[$key]['dragon_kills'] = $team['dragon_kills'];
            $teamStatic[$key]['dragon_kills_detail']['cloud_drake_kills'] = $team['cloud_drake_kills'];
            $teamStatic[$key]['dragon_kills_detail']['elder_drake_kills'] = $team['elder_drake_kills'];
            $teamStatic[$key]['dragon_kills_detail']['infernal_drake_kills'] = $team['infernal_drake_kills'];
            $teamStatic[$key]['dragon_kills_detail']['mountain_drake_kills'] = $team['mountain_drake_kills'];
            $teamStatic[$key]['dragon_kills_detail']['ocean_drake_kills'] = $team['ocean_drake_kills'];
            $teamStatic[$key]['dragon_kills_detail']['cloud_drake_kills'] = $team['cloud_drake_kills'];
//            $teamStatic[$key]['order']=self::getTeamOrderByTeamRelId(@$team['team']['id'], $matchInfo);
            // 队员信息转化
        }
        $battleInfo['detail'] = $battleDetail;
        $battleInfo['static'] = $teamStatic;
        //pick_array
        $pick_array = [];
        foreach ($restBattleInfo['base']['players'] as $key => $player) {
            $pick_array[$key]['hero'] = $player['champion']['id'];
            $pick_array[$key]['team'] = $player['team']['id'];
            $pick_array[$key]['type'] = 2;
        }
        //ban
        $banArray = [];
        foreach ($restBattleInfo['base']['teams'] as $playerTeams) {
            foreach ($playerTeams['bans'] as $banKey => $ban) {
                $tmp = [
                    'hero' => $ban,
                    'team' => $playerTeams['team']['id'],
                    'type' => 1,
                ];
                $banArray[] = $tmp;
            }
        }
        $battleInfo['ban_pick'] = array_merge($banArray, $pick_array);

        $palyersRest = $restBattleInfo['base']['players'];
        foreach ($palyersRest as $playerRest) {
            $palyerConfig = [
//                "rel_team_id" => "",//
//                'rel_player_id' => "",
//                "order" => "",//排序
//                "seed" => "",//选手排序
                "player_id" => function ($params) {
                    return $params['player']['id'];
                },//选手id
                "game" => "game_id",//游戏
                "spells" => "spells",//游戏
//                "match" => "",//比赛
//                "team_id" => "",//战队ID
//                "faction" => "",//阵营
                "role" => "role",//角色
//                "lane" => "",//分路
//                "player_id" => "",//选手id
//                "nick_name" => "",//选手昵称
                "champion" => function ($params) {
                    return $params['champion']['id'];
                },//英雄
                "level" => "level",//等级
//                "alive" => "",//存活状态
//                "ultimate_cd" => "",//大招状态
//                "coordinate" => "",//坐标
                "kills" => "kills",//击杀
                "double_kill" => function ($params) {
                    return $params['kills_series']['double_kills'];
                },//双杀
                "triple_kill" => function ($params) {
                    return $params['kills_series']['triple_kills'];
                },//三杀
                "quadra_kill" => function ($params) {
                    return $params['kills_series']['quadra_kills'];
                },//四杀
                "penta_kill" => function ($params) {
                    return $params['kills_series']['penta_kills'];
                },//五杀
                "largest_multi_kill" => "largest_multi_kill",//最大多杀
                "largest_killing_spree" => "largest_killing_spree",//最大连杀
                "deaths" => "deaths",//死亡
                "assists" => "assists",//助攻
//                "kda" => "",//KDA
//                "participation" => "",//参团率
                "cs" => "cs",//补刀得分
                "minion_kills" => "minions_killed",//小兵击击杀数
//                "total_neutral_kills" => "",//野怪击杀总数
//                "neutral_team_jungle_kills" => "",//本方野怪击杀数
//                "neutral_enemy_jungle_kills" => "",//敌方野怪击杀数
//                "cspm" => "",//分均补刀
                "gold_earned" => "gold_earned",//金币获取
                "gold_spent" => "gold_spent",//已花费金币
//                "gold_remaining" => "",//剩余金币
//                "gpm" => "",//分均金钱
//                "gold_earned_percent" => "",//经济占比
//                "experience" => "",//总经验
//                "xpm" => "",//分均经验
                "damage_to_champions" => function ($params) {
                    return $params['total_damage']['dealt_to_champions'];
                },//对英雄伤害
                "damage_to_champions_physical" => function ($params) {
                    return $params['physical_damage']['dealt_to_champions'];
                },//对英雄物理伤害
                "damage_to_champions_magic" => function ($params) {
                    return $params['magic_damage']['dealt_to_champions'];
                },//对英雄魔法伤害
                "damage_to_champions_true" => function ($params) {
                    return $params['true_damage']['dealt_to_champions'];
                },//对英雄真实伤害
//                "dpm_to_heroes" => "",//对英雄分均伤害
//                "damage_percent_to_heroes" => "",//对英雄伤害占比
                "damage_taken" => function ($params) {
                    return $params['total_damage']['taken'];
                },//承受伤害
                "damage_taken_physical" => function ($params) {
                    return $params['physical_damage']['taken'];
                },//承受物理伤害
//                "damage_taken_magic" => function ($params) {
//                },//承受魔法伤害
                "damage_taken_true" => function ($params) {
                    return $params['true_damage']['taken'];
                },//承受真实伤害
//                "dtpm" => "",//分均承受伤害
//                "damage_taken_percent" => "",//承伤占比
                "total_damage" => function ($params) {
                    return $params['total_damage']['dealt'];
                },//总伤害
//                "total_damage_physical" => "",//总物理伤害
                "total_damage_magic" => function ($params) {
                    return $params['magic_damage']['dealt'];
                },//总魔法伤害
//                "total_damage_true" => "",//总真实伤害
//                "damage_conversion_rate" => "",//伤害转化率
                "total_heal" => "total_heal",//总治疗量
                "total_crowd_control_time" => "total_time_crowd_control_dealt",//总控制时长
//                "wards_purchased" => "",//买眼
                "wards_placed" => function ($params) {
                    return $params['wards']['placed'];
                },//插眼
                "wards_kills" => function ($params) {
                    return $params['kills_counters']['wards'];
                },//排眼
                "turrets_kills" => function ($params) {
                    return $params['kills_counters']['turrets'];
                },
                "total_neutral_minion_kills" => function ($params) {
                    return $params['kills_counters']['neutral_minions'];
                },//野怪总数
                "neutral_minion_team_jungle_kills" => function ($params) {
                    return $params['kills_counters']['neutral_minions_team_jungle'];
                },
                "neutral_minion_enemy_jungle_kills" => function ($params) {
                    return $params['kills_counters']['neutral_minions_enemy_jungle'];
                },
//                "neutral_minions_kills" => function ($params) {
//                    return $params['kills_counters']['neutral_minions'];
//                },
//                "inhibitors_kills" => function ($params) {
//                    return $params['kills_counters']['inhibitors'];
//                },
//                'damage_to_champions_magic' => function ($params) {
//                    return $params['magic_damage']['dealt_to_champions'];
//                },
//                'damage_taken_magic' => function ($params) {
//                    return $params['magic_damage']['dealt_to_champions'];
//                },
//                "warding_totems_purchased" => "",//购买监视图腾
                "items" => "items",
                "rel_team_id" => function ($params) {
                    return $params['team']['id'];
                },
                'rel_player' => 'player',
                'rel_identity_id' => function ($params) {
                    return (string)$params['player']['id'];
                },
                'rel_id' => function ($params) {
                    return $params['player']['id'];
                },
                'team_order' => function ($params) {
                    return 1;
                }
            ];
            $player = Mapping::transformation($palyerConfig, $playerRest);
            $player['order'] = $key;
//            $player['team_order']=self::getTeamOrderByTeamRelId($playerRest['team']['id'],$matchInfo);
            // 需要计算得属性
            $deaths = $player['deaths'] ? $player['deaths'] : 1;
            $player['kda'] = ($player['kills'] + $player['assists']) / $deaths * 3;

            $player['participation'] = ($player['kills'] + $player['assists']) / self::getTeamKills(
                    $playerRest['team']['id'],
                    $restBattleInfo
                );
            $player['gold_remaining'] = $player['gold_earned'] - $player['gold_spent'];
            $player['gold_earned_percent'] = $player['gold_earned'] / self::getTeamKills(
                    $playerRest['team']['id'],
                    $restBattleInfo
                );
            $player['gpm'] = $player['gold_earned'] / $restBattleInfo['base']['length'];
            $player['dpm_to_champions'] = $player['damage_to_champions'] / $restBattleInfo['base']['length'];
            $player['damage_percent_to_champions'] = $player['damage_to_champions'] / self::getTeamKills(
                    $playerRest['team']['id'],
                    $restBattleInfo
                );
            $player['dtpm'] = $player['damage_taken'] / $restBattleInfo['base']['length'];
//            $player['damage_taken_percent'] = $player['damage_taken']/$restBattleInfo['base']['length'];
            $player['damage_conversion_rate'] = $player['damage_to_champions'] / $player['gold_earned'];


            $players[] = $player;
        }
        $battleInfo['players'] = $players;


        return $battleInfo;
    }

    private static function getMatch($matchId)
    {
        $url = '/lol/matches';
        $params = [
            'filter[id]' => $matchId,
        ];
        $jsonInfo = self::getRestInfo($url, $params);
        $info = json_decode($jsonInfo, true);

        // 获取当前battle的数据
        return $info[0];
    }

    private static function getBattle($battleId)
    {
        $url = '/lol/games/'.$battleId;

        $jsonInfo = PandascoreBase::getInfo($url, []);
        $info = json_decode($jsonInfo, true);

        return $info;
    }

    private static function getEvents($battleId)
    {
        // 这里会翻页
        $url = '/lol/games/'.$battleId.'/events';
        $infoAll = [];
        for ($i = 1; $i < 100; $i++) {
            $page = $i;
            $perPage = 100;
            $jsonInfo = PandascoreBase::getInfo($url, ['page' => $page, 'per_page' => $perPage]);
            $infoList = json_decode($jsonInfo, true);
            $infoAll = array_merge($infoAll, $infoList);
            if (count($infoList) < $perPage) {
                break;
            }
        }

        return $infoAll;
    }

    private static function getFrames($battleId)
    {
        $url = '/lol/games/'.$battleId.'/frames';
        $infoAll = [];
        for ($i = 1; $i < 100; $i++) {
            $page = $i;
            $perPage = 100;
            $jsonInfo = PandascoreBase::getInfo($url, ['page' => $page, 'per_page' => $perPage]);
            $infoList = json_decode($jsonInfo, true);
            $infoAll = array_merge($infoAll, $infoList);
            if (count($infoList) < $perPage) {
                break;
            }
        }

        return $infoAll;
    }

    public static function getTeamKills($teamId, $restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val) {
            if ($val['team']['id'] == $teamId) {
                return $val['kills'];
            }
        }
    }

    /**
     * @param $sub_type (子类型)
     * @param $ingame_obj_name (游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if (isset($name) && $name) {
            $goal = EnumIngameGoal::find()->where(['like', 'pandascore_name', $name])->asArray()->one();
            if ($goal) {
                return $goal['id'];
            }
        }
    }

    public static function getGoalIdBySubType($subType)
    {
        $goal = EnumIngameGoal::find()->where(['=', 'sub_type', $subType])
            ->asArray()->one();
        if ($goal) {
            return $goal['id'];
        }
    }

    public static function getTeamToDamage($teamId, $restBattleInfo)
    {
        foreach ($restBattleInfo['base']['teams'] as $val) {
            if ($val['team']['id'] == $teamId) {
                return $val['kills'];
            }
        }
    }
}