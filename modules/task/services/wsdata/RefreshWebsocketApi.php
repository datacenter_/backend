<?php
/**
 *
 */

namespace app\modules\task\services\wsdata;

use app\modules\common\services\Consts;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchLived;
use app\modules\match\services\BattleService;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\wsdata\pandascore\PandascoreLolApi;
use app\modules\task\services\wsdata\pandascore\PandascoreLolws;

class RefreshWebsocketApi
{
    public static function run($tagDataInfo,$taskDataInfo){
        $info = @json_decode($taskDataInfo['params'], true);
        $match_id = @$info['match_id'];
        $origin_id = @$info['origin_id'];
        $rel_match_id = @$info['rel_match_id'];
        $type = @$info['event_type'];
        //查询当前比赛的所有socket数据
        switch ($origin_id){
            case 3:
                //self::refreshPs($origin_id,$rel_match_id,$type);
                self::refreshPandaScoreToApi($match_id,$origin_id,$rel_match_id,$type);
                break;
            case 4:
                self::refreshOrange($origin_id,$rel_match_id,$type);
                break;
        }
        return true;
    }
    //pandascore
    public static function refreshPs($origin_id,$rel_match_id,$type){
        //redis初始化
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $itemsList = "lol:items:list:".$origin_id;

        $tagInfo = $taskInfo = [];
        $battle_refresh_info = [];
        //查询所有道具
        $lolItems = HotBase::getMainDataArrayMasterIdAndRelIdentityId('lol_item',3,2);
        //获取当前比赛所有的数据
        $resData = self::getMatchLivedDatas($origin_id,$rel_match_id,$type);
        //所有数据数量
        $socketDataNum = count($resData);
        $socket_now_num = 1;
        //玩家的上一个道具列表
        $battle_player_items_before = [];
        $match_battles_list = [];
        $item_battle = 0;
        //首先循环 比赛所有的数据
        foreach ($resData as $key => $value){
            $tagInfo['ext_type'] = $value['game_id'];
            //继续后面的rest
            $match_data = json_decode($value['match_data'],true);
            if (isset($match_data['type']) && $match_data['type'] =='hello'){
                //增量
                $socket_now_num ++;
                continue;
            }
            //对局ID
            $rel_battle_id = $match_data['game']['id'];
            $item_battle = $rel_battle_id;
            $gold_diff_timeline_teams = [];
            //判断是哪个对局 匹配当前对局的数据
            //原始值
            //处理好item line
            $insertData = [
                'origin_id' => $value['origin_id'],
                'game_id' => $value['game_id'],
                'match_id' => $value['match_id'],
                'round' => $value['round'],
                'match_data' => $match_data,
                'type' => $value['type'],
                'socket_time' => isset($value['socket_time']) ? $value['socket_time']: $value['created_at'],
                'socket_id' => $value['id']
            ];
            //类型
            $type_value = $value['type'];
            if ($type_value == 'no'){
                $teamInfosRest = [$match_data['blue'],$match_data['red']];
                foreach ($teamInfosRest as $key =>$team){
                    //经济
                    $gold_diff_timeline_teams[] = $team['gold'];
                    //玩家
                    $players_key = $team['players'];
                    foreach ($players_key as $keypp => $itempp){
                        $battle_player_items_before_array = isset($battle_player_items_before[$item_battle][$itempp['id']]) ? @end($battle_player_items_before[$item_battle][$itempp['id']])['items'] : [];
                        $need_change = 0;
                        $playerOrnamentsInfo = [];
                        $playerItemsNull = [];
                        $playerItemsInfo = [];
                        foreach ($itempp['items'] as $key_item => $value_item){
                            if ($value_item){
                                $itemInfoId = $lolItems[$value_item['id']];
                                if($itemInfoId){
                                    if (!in_array($itemInfoId,$battle_player_items_before_array)){
                                        $need_change = 1;
                                    }
                                    $itemInfoId_val = (Int)$itemInfoId;
                                    //获取当前
                                    $itemInfoResRedis = $Redis->hget($itemsList,$itemInfoId_val);
                                    if ($itemInfoResRedis){
                                        $itemInfoRes = @json_decode($itemInfoResRedis,true);
                                        if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                                            $playerOrnamentsInfo[$key_item] = $itemInfoId_val;
                                        }else{//道具
                                            $playerItemsInfo[$key_item]['total_cost'] = @$itemInfoRes['total_cost'];
                                            $playerItemsInfo[$key_item]['item_id'] = $itemInfoId_val;
                                        }
                                    }else{//道具
                                        $playerItemsInfo[$key_item]['total_cost'] = 1;
                                        $playerItemsInfo[$key_item]['item_id'] = @HotBase::getMetaDataUnknownId('lol_items');
                                    }
                                }else{
                                    $playerItemsInfo[$key_item]['total_cost'] = 1;
                                    $playerItemsInfo[$key_item]['item_id'] = @HotBase::getMetaDataUnknownId('lol_items');
                                }
                            }else{
                                $playerItemsNull[$key_item] = null;
                            }
                        }
                        $total_cost_array = array_column($playerItemsInfo,'total_cost');
                        array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
                        $playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
                        $playerItemsInfoResult = array_merge($playerItemsInfoRet,$playerItemsNull,$playerOrnamentsInfo);
                        if ($need_change == 1){
                            $playerItemsInfoArray = [
                                'ingame_timestamp'=>$match_data['current_timestamp'],
                                'items'=>$playerItemsInfoResult
                            ];
                            $battle_player_items_before[$item_battle][$itempp['id']][] = $playerItemsInfoArray;
                            if ($key == 0){
                                $match_data['blue']['players'][$keypp]['items_timeline'] = @$battle_player_items_before[$item_battle][$itempp['id']];
                            }else{
                                $match_data['red']['players'][$keypp]['items_timeline'] = @$battle_player_items_before[$item_battle][$itempp['id']];
                            }
                        }else{
                            if ($key == 0){
                                $match_data['blue']['players'][$keypp]['items_timeline'] = @$battle_player_items_before[$item_battle][$itempp['id']];
                            }else{
                                $match_data['red']['players'][$keypp]['items_timeline'] = @$battle_player_items_before[$item_battle][$itempp['id']];
                            }
                        }
                    }
                }
                //是否结束
                if ($match_data['game']['finished']){
                    $insertData['match_data'] = $match_data;
                    $match_battles_list[$item_battle]['frames']['params'] = json_encode($insertData,320);
                }else{
                    //判断最后一针
                    if ($socketDataNum == $socket_now_num){
                        $insertData['match_data'] = $match_data;
                        $match_battles_list[$item_battle]['frames']['params'] = json_encode($insertData,320);
                    }
                }
                //计算团队经济差
                $gold_diff_num = 0;
                if ($gold_diff_timeline_teams){
                    $gold_diff_num = $gold_diff_timeline_teams[0] - $gold_diff_timeline_teams[1];
                }
                $gold_diff_timeline_array = [
                    'ingame_timestamp' => $match_data['current_timestamp'],
                    'gold_diff' => $gold_diff_num
                ];
                $match_battles_list[$item_battle]['gold_diff_timeline'][] = $gold_diff_timeline_array;
            }else{
                $rel_event_id = $match_data['id'];
                $match_battles_list[$item_battle]['events'][$rel_event_id]['params'] = json_encode($insertData,320);
                //刷事件
            }
            //刷新socket TODO 暂时去掉推送WS
//            $lol_ws = json_encode($insertData,320);
//            $lol_ws_array['params'] = $lol_ws;
//            PandascoreLolws::run($tagInfo,$lol_ws_array);
            //增量
            $socket_now_num ++;
        }
        //判断有没有battle
        if ($match_battles_list){
            foreach ($match_battles_list as $key => $item){
                $battle_id = PandascoreLolApi::run($tagInfo,$item['frames']);
                //刷新经济差时间线
                HotBase::setGoldDiffTimeLineByRefresh($battle_id,$item['gold_diff_timeline']);
                //
                //刷新events
                foreach ($item['events'] as $key1 => $val){
                    PandascoreLolApi::run($tagInfo,$val);
                }
            }
        }
        return true;
    }
    //Orange
    public static function refreshPandaScoreToApi($match_id,$origin_id,$rel_match_id,$type){
        $all_socket_info = self::getMatchLivedDatas($origin_id,$rel_match_id,'all');
        if ($all_socket_info){
            foreach ($all_socket_info as $value){
                $insertData = [
                    'origin_id' => $value['origin_id'],
                    'game_id' => $value['game_id'],
                    'match_id' => $value['match_id'],
                    'round' => $value['round'],
                    'match_data' => $value['match_data'],
                    'type' => $value['type'],
                    'socket_time' => isset($value['socket_time']) ? $value['socket_time']: $value['created_at'],
                    'socket_id' => $value['id']
                ];
                if(!isset($insertData['socket_time'])){
                    $insertData['socket_time'] = date('Y-m-d H:i:s');
                }
                $newInsertData = @$insertData;
                $newInsertData['match_data']= @json_decode($value['match_data'],true);
                $taskType = '9002';
                $item = [
                    "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                        $value['origin_id'], Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "", $value['game_id']),
                    "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                    "batch_id" => date("YmdHis"),
                    "params" => $newInsertData,
                    "redis_server"=>'order_lol_'.$rel_match_id
                ];
                TaskRunner::addTask($item, $taskType);
            }
        }
        //根据matchid获取hot表中的信息
        $match_info = HotDataRunningMatch::find()->select('id,match_id,game_id')->where(["match_id"=>$match_id])->asArray()->one();
        $match_info['origin_id'] = $origin_id;
        //task内容
        $task = [
            "tag" => QueueServer::getTag(
                QueueServer::QUEUE_MAJOR_OPERATE_HOT_REFRESH,
                "",
                "",
                "",
                "",
                'refresh'
            ),
            "type" => '',
            "batch_id" => date("YmdHis"),
            "params" => $match_info,
            "redis_server" => "order_lol_".$rel_match_id
        ];
        //插入taskinfo
        TaskRunner::addTask($task, 9002);
        return true;
    }
    //Orange
    public static function refreshOrange($origin_id,$rel_match_id,$type){
        $all_socket_info = self::getMatchLivedDatas($origin_id,$rel_match_id,'all');
        if ($all_socket_info){
            foreach ($all_socket_info as $value){
                $insertData = [
                    'origin_id' => $value['origin_id'],
                    'game_id' => $value['game_id'],
                    'match_id' => $value['match_id'],
                    'round' => $value['round'],
                    'match_data' => $value['match_data'],
                    'type' => $value['type'],
                    'socket_time' => isset($value['socket_time']) ? $value['socket_time']: $value['created_at'],
                    'socket_id' => $value['id']
                ];
                if(!isset($insertData['socket_time'])){
                    $insertData['socket_time'] = date('Y-m-d H:i:s');
                }
                $newInsertData = @$insertData;
                $newInsertData['match_data']= json_decode($value['match_data'],true);
                $taskType = '9002';
                $item = [
                    "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                        $value['origin_id'], Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $value['game_id']),
                    "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                    "batch_id" => date("YmdHis"),
                    "params" => $newInsertData,
                    "redis_server"=>'order_lol_'.$rel_match_id
                ];
                TaskRunner::addTask($item, $taskType);
            }
        }
        return true;
    }
    //获取当前比赛的所有socket数据
    public static function getMatchLivedDatas($origin_id,$rel_match_id,$type='all'){
        $map = [
            'origin_id' => $origin_id,
            'match_id' => $rel_match_id,
//            'game_id' => $game_id,
//            'type' => 'events',
        ];
        if ($type != 'all'){
            $map['type'] = $type;
        }
        $data = MatchLived::find()
            ->where($map)
//            ->andwhere(['<','id','797771'])
//            ->andwhere(['>=','id','797645'])
//            ->andwhere(['=','id','797950'])
            ->select('id,match_data,type,round,match_id,game_id,origin_id,socket_time,created_at')
            ->orderBy('id asc')
            ->asArray()
            ->all();
        return $data;
    }
}