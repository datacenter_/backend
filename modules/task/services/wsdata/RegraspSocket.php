<?php
/**
 *
 */

namespace app\modules\task\services\wsdata;


use app\modules\match\models\Match;
use app\modules\match\models\MatchLived;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class RegraspSocket
{
    public static function run($tagDataInfo,$taskDataInfo){
        $info = @json_decode($taskDataInfo['params'], true);
        $match_id = @$info['match_id'];
        $origin_id = @$info['origin_id'];
        $game_id = @$info['game_id'];
        if (empty($match_id) || empty($origin_id) || empty($game_id)){
            throw new BusinessException([],'参数错误！');
        }
        //查询当前比赛的所有socket数据
        switch ($origin_id){
            case 3:
                return true;
                break;
            case 4:
                self::regraspOrangeSocket($origin_id,$match_id,$game_id);
                break;
        }
        return true;
    }
    //玩家 重抓
    public static function regraspOrangeSocket($origin_id,$match_id,$game_id)
    {
        $matchInfo = Match::find()->where(['id'=>$match_id])->asArray()->one();
        if ($matchInfo){
            $rel_match_id = HotBase::getRelIdByMasterId('match',$match_id,4);
            if ($rel_match_id){
                //删除原有的Socket 数据
                MatchLived::deleteAll(['match_id' => $rel_match_id,'origin_id'=>$origin_id]);
                //循环获取
                $number_of_games = $matchInfo['number_of_games'];
                if ($origin_id == 4){
                    for ($i=1;$i<=$number_of_games;$i++){
                        self::regraspSocketByOrange($origin_id,$game_id,$rel_match_id,-1000,$i);
                    }
                }else{
                    throw new BusinessException([],'暂无处理该数据源');
                }
            }
        }
    }
    //Orange
    private static function regraspSocketByOrange($origin_id,$game_id,$rel_match_id,$start_time,$match_order){
        $socket_time = date("Y-m-d H:i:s");
        $action='/lol/schedule/matchlog';
        $params=[
            'schedule_id'=> $rel_match_id,
            'match_order'=> $match_order,
            'start_time' =>$start_time
        ];
        $infoJson=OrangeBase::curlGet($action,'GET',$params);
        $info=@json_decode($infoJson,true);
        $res = @$info['data']['list'];
        if ($res){
            foreach ($res as $val){
                $match_data = json_encode($val,320);
                $insertData = [
                    'origin_id' => (Int)$origin_id,
                    'game_id' => (Int)$game_id,
                    'match_id' => (String)$rel_match_id,
                    'round' => (String)$match_order,
                    'match_data' => $match_data,
                    'type' => 'frames',
                    'socket_time' => $socket_time
                ];
                $MatchLivedModel = new MatchLived();
                $MatchLivedModel->setAttributes($insertData);
                $MatchLivedModel->save();
            }
            $event_time = end($res)['event_time'];
            $start_time = $event_time + 1; 
            self::regraspSocketByOrange($origin_id,$game_id,$rel_match_id,$start_time,$match_order);
        }else{
            return;
        }
    }
}