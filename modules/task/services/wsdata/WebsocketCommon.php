<?php

namespace app\modules\task\services\wsdata;

use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DbSocket;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\models\SortingLog;
use app\modules\match\services\battle\BattleServiceBase;
use app\modules\metadata\models\EnumCsgoWeapon;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\match\models\MatchBattle;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\models\TeamPlayerRelation;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\WsToApiSynchron;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\logformat\CsgoLogFormatBase;
use app\modules\org\models\Team;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\services\BattleService;
use app\modules\common\services\SteamHelper;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocket;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketBattleService;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketPlayerService;
use app\rest\exceptions\BusinessException;
use Throwable;
use Redis;
/**
 * Csgo Event Ws
 * @uses 结构层次: 一级分类:current history
 *                二级分类:match battle round player team
 *                三级分类:相关元素 status,begin_at,end_at,order,upcoming
 * @example ws:current:player:313:status => 'offline'
 * Class FiveHotCsgoMatchWs
 * @package app\modules\task\services\hot\five
 */
class WebsocketCommon extends HotBase
{

    static private $instance;
    public $matchId = "";
    public $match = [];
    public $sortingLog = "";
    public $weaponList = "";
    public $event = [];
    public $event_id = 0;
    public $event_type = '';
    public $currentPre = "ws";
    public $refresh = false;//重刷结束标志
    public $info = [];
    public $log_time = '';
    public $sec = '';

    //in_round_timestamp round_time is_bomb_planted time_since_plant
    public $battle_timestamp = 0;
    public $in_round_timestamp = 0;
    public $round_time = 0;
    public $is_bomb_planted = false;
    public $time_since_plant = 0;
    public $temp_battle_id = '';
    public $temp_battle_order = '';
    public $temp_round_ordinal = '';
    public $temp_time_since_plant = '';
    public $temp_live = false;
    public $matchEndFlag = false;
    public $muti = '';
    //refresh
    public $refresh_task = false;
    public $refresh_task_str = [];
    public $refresh_first_record_flag= false;
    public $bigReload= false;

    public $battle_duration = 0;
    public $battleId = null;
    public $battleOrder = null;

    public $roundId = null;
    public $roundOrder = null;
    public $battleUrn = '';
    public $duration = 0;
    public $tableMapList = '';  //地图
    public $tableWeaponList = '';//武器
    //record Event list
    public $record_first_kill= false;
    public $record_bomb_planted= false;
    public $record_knife_kill= false;
    public $record_taser_kill= false;
    public $record_ace_kill= false;
    public $record_team_kill= false;
    const SPECIAL_EVENT_KEY_ARR = [
        'terrorist_first_kill','ct_first_kill',
        'terrorist_bomb_planted','ct__bomb_planted',
        'terrorist_knife_kill','ct_knife_kill',
        'terrorist_taser_kill','ct_taser_kill',
        'terrorist_ace_kill','ct__ace_kill',
        'terrorist_team_kill','ct__team_kill',
        'main_events',
        'kill_events',
        'events',
    ];





    public function doCommonData($model,$data_action,$data_payload){
        if (!$model){
            $model = new self();
        }
        $updateBattleArr= [];
        $battleDetails = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model,$model->battleUrn);
        $model->battle_duration = $model->getCurrentBattleTimestamp($model->battle_timestamp,$model->info['socket_time']);
        //roundOrder
        $roundOrderEventNoArr = [
            'ANNOUNCE'
        ];
        if (!in_array($data_action, $roundOrderEventNoArr)) {
            $model->setCurrentRoundOrder($model, $data_payload['currentRoundNumber']);
        } else {
            $model->roundOrder = $battleDetails['current_round'] ? $battleDetails['current_round'] : 1;
        }
        //in_round_timestamp
        $inRoundTimestampEventNoAr = [
            'ANNOUNCE'
        ];
        if (!in_array($data_action,$inRoundTimestampEventNoAr)){
            $model->in_round_timestamp = $updateBattleArr['in_round_timestamp']= (int)substr($data_payload['gameTime'],0,-3);
        }else{
            $model->in_round_timestamp = $battleDetails['in_round_timestamp']?$battleDetails['in_round_timestamp']:0;
        }

        //round_time
        $roundTimeEventNoArr = [
            'ANNOUNCE'
        ];
        if (!in_array($data_action, $roundTimeEventNoArr)) {
            if ($data_action == 'START_MAP' || ($data_payload['currentRoundNumber'] == 1 && $data_action == 'FREEZE_TIME_STARTED')) {
                $model->round_time = $updateBattleArr['round_time'] = 115;
            } else {
                $model->round_time = $updateBattleArr['round_time'] = (int)substr($data_payload['timeRemaining'], 0, -3);
            }
        }else{
            $model->round_time = $battleDetails['round_time']?$battleDetails['round_time']:0;
        }
        //is_bomb_planted
        $bombInfo =  $model->getBombState($model,$model->battleId,$model->roundOrder);
        $model->is_bomb_planted = (bool)$bombInfo['is_bomb_planted'];
        if (!$bombInfo['is_bomb_planted']){
            $model->time_since_plant = $updateBattleArr['round_time'] =  0;
        }else{
            $model->time_since_plant = $updateBattleArr['round_time'] =substr($data_payload['timeRemaining'],0,-3);
        }
        if (!empty($updateBattleArr)){
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$updateBattleArr);
        }
    }


    /**
     * 获取sortingLog（缓存在redis中）查询信息完善标识和查询match信息,不缓存
     * @param $ip_address
     * @param int $refresh
     * @return string
     */
    public function getSortingLog($ip_address,$refresh=0)
    {
        $sortingLog =[];
        $logTimeBefore =$this->info['log_time'];
        $time = str_replace(' - ', ' ', $logTimeBefore);
        list($beforeTime, $sec) = explode(".", $time);
        $this->sec = $sec;
        $log_time = date('Y-m-d H:i:s',strtotime($time));
        $this->log_time = $log_time;
        $sortingLogObj = SortingLog::find()->alias('s');
        $sortingLogObj->select('s.id,s.match_id,s.log_type,s.team_1_log_name,s.team_2_log_name,s.service_ip,s.sleeps,s.timezone,s.flag,s.prefect_logo,s.refresh_time,m.scheduled_begin_at,m.number_of_games');
        $sortingLogObj->leftJoin('match as m','m.id = s.match_id');

        //每分钟取一次sortingLog
        $nowTime     = time();
        $redisKey    = $this->currentPre.':tag_csgo_frame_sortingLog_time:' . $this->matchId;
        $refreshTime = $this->redis->get($redisKey);
        if ($refreshTime&&!$this->refresh_first_record_flag) {
            $diff_time = $nowTime - $refreshTime;
            if ($diff_time > 60 ) {
                $this->redis->set($redisKey, $nowTime, 3600);
            }else{
                if (!empty($this->sortingLog)){
                    return $this->sortingLog;
                }
            }
        }else{
            $this->redis->set($redisKey, $nowTime, 3600);
        }
        //正常event寻找sortingLog
        if ($refresh == 0){
            $sortingLogObj->leftJoin('match_real_time_info as m_info', 'm.id = m_info.id');
            $sortMap = ['like', 's.service_ip', $ip_address];
            $sortingLogObj->andwhere($sortMap);
            $current_time        = date('Y-m-d H:i:s', time());//当前时间
            $current_time_decr_5 = date('Y-m-d H:i:s', time() - 300);//当前时间减5分钟
            //sql
            $where   = ['or'];
            $where[] = [
                'and',
                ['>=', 'm_info.end_at', $current_time_decr_5],
                ['<=', 'm_info.end_at', $current_time]
            ];
            $where[] = [
                'and',
                ['in', 'm_info.status', [1, 2]]
            ];
            $sortingLogObj->andWhere($where);
            $sql            = $sortingLogObj->createCommand()->getRawSql();
            $sortingLogList = $sortingLogObj->asArray()->orderBy('id asc')->all();
            if (!empty($sortingLogList)) {
                foreach ($sortingLogList as $k => $v) {
                    if (!empty($sortingLog)) {
                        break;
                    }
                    if ($v['timezone'] != 8) {
                        $log_time = date("Y-m-d H:i:s", strtotime($log_time) - (8 - $v['timezone']) * 3600);
                    }
                    $this->log_time = $log_time;
                    $vIpAddress     = json_decode($v['service_ip'], true);
                    foreach ($vIpAddress as $ipKey => $ipValue) {
                        if (!empty($ipValue['refresh_start_time']) && !empty($ipValue['refresh_end_time']) && $log_time >= $ipValue['refresh_start_time'] && $log_time <= $ipValue['refresh_end_time']) {
                            $sortingLog = $v;
                            break;
                        }
                    }

                }
                if (empty($sortingLog)) {
                    foreach ($sortingLogList as $k => $v) {
                        if (!empty($sortingLog)) {
                            break;
                        }
                        if ($v['timezone'] != 8) {
                            $log_time = date("Y-m-d H:i:s", strtotime($log_time) - (8 - $v['timezone']) * 3600);
                        }
                        $this->log_time         = $log_time;
                        $end_scheduled_begin_at = date('Y-m-d H:i:s', strtotime($v['scheduled_begin_at']) + $v['number_of_games'] * 5400);
                        $vIpAddress             = json_decode($v['service_ip'], true);
                        foreach ($vIpAddress as $ipKey => $ipValue) {
                            if (!empty($ipValue['refresh_start_time']) && empty($ipValue['refresh_end_time']) && $log_time >= $ipValue['refresh_start_time'] && $log_time <= $end_scheduled_begin_at) {
                                $sortingLog = $v;
                                break;
                            }
                        }
                    }
                }
                if (empty($sortingLog)){
                    foreach ($sortingLogList as $k=>$v){
                        if (!empty($sortingLog)){
                            break;
                        }
                        if ($v['timezone']!=8){
                            $log_time = date("Y-m-d H:i:s",strtotime($log_time)-(8-$v['timezone'])*3600);
                        }
                        $this->log_time = $log_time;
                        $start_scheduled_begin_at = date('Y-m-d H:i:s',strtotime($v['scheduled_begin_at'])-1800);
                        $vIpAddress = json_decode($v['service_ip'],true);
                        foreach ($vIpAddress as $ipKey => $ipValue){
                            if (empty($ipValue['refresh_start_time'])&&!empty($ipValue['refresh_end_time'])&&$log_time>=$start_scheduled_begin_at&&$log_time<=$ipValue['refresh_end_time']){
                                $sortingLog = $v;
                                break;
                            }
                        }

                    }
                }
                if (empty($sortingLog)) {
                    foreach ($sortingLogList as $k => $v) {
                        if (!empty($sortingLog)) {
                            break;
                        }
                        if ($v['timezone'] != 8) {
                            $log_time = date("Y-m-d H:i:s", strtotime($log_time) - (8 - $v['timezone']) * 3600);
                        }
                        $this->log_time           = $log_time;
                        $start_scheduled_begin_at = date('Y-m-d H:i:s', strtotime($v['scheduled_begin_at']) - 1800);
                        $end_scheduled_begin_at   = date('Y-m-d H:i:s', strtotime($v['scheduled_begin_at']) + $v['number_of_games'] * 5400);
                        $vIpAddress               = json_decode($v['service_ip'], true);
                        foreach ($vIpAddress as $ipKey => $ipValue) {
                            if (empty($ipValue['refresh_start_time']) && empty($ipValue['refresh_end_time']) && $log_time >= $start_scheduled_begin_at && $log_time <= $end_scheduled_begin_at) {
                                $sortingLog = $v;
                                break;
                            }
                        }
                    }
                }
            }else{
                return [];
            }
        }

        //重刷
        if ($refresh != 0) {
            $sortingLogObj->andwhere(['s.match_id' => $refresh]);
            $sortingLog = $sortingLogObj->asArray()->orderBy('id asc')->one();
        }

        if (empty($sortingLog) || $sortingLog == null) {
            $this->error_catch_log(['error_msg' => 'sorting_log is empty', 'sorting_log' => $sortingLog]);
            return [];
        }
        if ($sortingLog['prefect_logo'] != 1) {
            $this->error_catch_log(['error_msg' => 'prefect_logo not is 1', 'sorting_log' => $sortingLog]);
            return [];
        }
        $data_resource_update_config = DataResourceUpdateConfig::find()->where(['resource_id' => $sortingLog['match_id'], 'resource_type' => 'match', 'tag_type' => 'score_data'])->asArray()->one();
        if (!empty($data_resource_update_config) && $data_resource_update_config['origin_id'] == 5 && $data_resource_update_config['update_type'] == 3) {
            $this->sortingLog = $sortingLog;
            return $sortingLog;
        } else {
            $this->error_catch_log(['error_msg' => 'data_resource_update_config error', 'data_resource_update_config' => $data_resource_update_config]);
            return [];
        }

    }

    /**
     *
     * 单例
     * @return FiveHotCsgoMatchWs
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * getMatchInfo
     * @param $id
     * @return array|bool|\yii\db\ActiveRecord|null
     */
    public function getMatchInfo($id)
    {
        if ($id == 0) {
            return false;
        }
        $redis_res = $this->hGetAll(['table','match' ,$id]);
        if ($redis_res) {
            return $redis_res;
        } else {
            $match = MatchModel::find()->where(['id' => $id])->asArray()->one();
            $this->hMSet(['table','match' ,$id], $match);
            return $match;
        }
    }

    /**
     * @param $log_time
     * @return int
     */
    public function getInRoundTimestamp($log_time)
    {
        if ($this->isFreezeTime()){
            return 0;
        }
        $round_start_time = strtotime($this->getCurrentRoundBeginAt());
        $time             = strtotime($log_time) - $round_start_time;
        if (empty($round_start_time)) {
            $time = 0;
        }
        return $time > 0 ? (int)$time : 0;
    }

    /**
     * @return mixed
     */
    public function getCurrentRoundBeginAt()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT]);
    }

    public function matchIsTrue($match)
    {
        if (is_array($match) && array_key_exists('scheduled_begin_at', $match) && $match['scheduled_begin_at']) {
            //10 min
            $matchStatus = $this->getMatchStatus();
            $matchEndAt  = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
            if ($matchStatus && $matchEndAt) {
                if ($matchStatus == 3 || $matchEndAt) {
                    if ((time() - strtotime($matchEndAt) < 600) && (time() - strtotime($matchEndAt) >= 0)) {
                        return true;
                    } else {
                        $this->error_hash_log('matchIsTrue', ['time' => date('Y-m-d H:i:s')]);
                        return false;
                    }
                }
            }

            if (time() < strtotime($match['scheduled_begin_at']) && strtotime($match['scheduled_begin_at']) - time() < 3600) {
                return true;
            } elseif (time() >= strtotime($match['scheduled_begin_at']) && time() - strtotime($match['scheduled_begin_at']) < 3600 * 5) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    /**
     * battle_restart
     * @param $data
     * @param $event
     */
    public function battle_restart($data,$event){
        $second = $this->event['info']['second'];
        $data['battle_restart_in'] = (int)$second;
        return $data;
    }

    public function getServerCvarsInit(){
        $key = [self::KEY_TAG_LOG,  self::KEY_TAG_SERVER_CVARS_INIT, self::KEY_TAG_LIST];
        $res = $this->getValue($key);
        return json_decode($res,true);
    }

    public function setServerCvarsInit($value){
        $key = [self::KEY_TAG_LOG,  self::KEY_TAG_SERVER_CVARS_INIT, self::KEY_TAG_LIST];
        $res = $this->setValue($key,$value);
        return json_decode($res,true);
    }



    /**
     * @return string
     */
    public function getBattles()
    {
        return $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST]);
    }

    /**
     * 根据队名判断主客队
     * @param $teamName
     * @return int
     */
    public function getTeamOrderByTeamName($teamName)
    {
        $teamLongName1 = array_filter(explode(";", @$this->sortingLog['team_1_log_name']));
        $teamLongName2 = array_filter(explode(";", @$this->sortingLog['team_2_log_name']));
        for ($i = 0; $i < count($teamLongName1); $i++) {
            if (strtolower(trim($teamLongName1[$i]))== strtolower(trim($teamName))) {
                return 1;
                break;
            }
        }
        for ($i = 0; $i < count($teamLongName2); $i++) {
            if (strtolower(trim($teamLongName2[$i])) == strtolower(trim($teamName))) {
                return 2;
                break;
            }
        }
        $this->error_hash_log('getTeamOrderByTeamName', ['name'=>$teamName,'event'=>$this->event]);
        return 1;//default
    }

    /**
     * 通过名称获取team
     * @param $teamName
     * @return string
     */
    public function getTeamByName($teamName)
    {
        $order = $this->getTeamOrderByTeamName($teamName);
        if ($order == 1) {
            $teamId = $this->match['team_1_id'];
        }
        if ($order == 2) {
            $teamId = $this->match['team_2_id'];
        }
        //缓存
        $redisTeam = $this->getValue(['table','team' , $teamId]);
        if ($redisTeam) {
            return json_decode($redisTeam, true);
        } else {
            $team                   = Team::find()->select(['id', 'name', 'image'])->where(['id' => $teamId])->asArray()->one();
            $team['team_id']        = $team['id'];
            $team['opponent_order'] = $order;
            $this->setValue(['table','team' , $teamId], json_encode($team,320));
            //获取frame和events teams 信息
            $teamRes['team_id']        = $team['id']?$team['id']:null;
            $teamRes['name']           = $team['name'] ?? $teamName;
            $teamRes['log_name']       = $teamName;
            $teamRes['image']          = $team['image'];
            $teamRes['opponent_order'] = $order;
            return $teamRes;
        }
    }


    public function getGameRules(){
        $match = $this->match;
        $arr = [
            1=>'normal',
            2=>'brawl',
            3=>'battle_royale',
        ];
        if ($arr[$match['game_rules']]){
            return $arr[$match['game_rules']];
        }else{
            return 'normal';
        }
    }
    public function getMatchType(){
        $arr = [
            1=>'best_of',
            2=>'first_to',
            3=>'ow_best_of',
            4=>'best_ranking',
        ];
        $match = $this->match;
        if ($arr[$match['match_type']]){
            return $arr[$match['match_type']];
        }else{
            return 'best_of';
        }
    }
    public function getNumberOfGames(){
        $match = $this->match;
        return (int)$match['number_of_games'];
    }
    public function getServerConfigFrames(){
        $serverConfig                       = $this->getServerCvarsInit();
        $configArr['version']               = $this->getGameVersion();
        $configArr['mp_maxrounds']          = $serverConfig['mp_maxrounds']??null;
        $configArr['mp_overtime_enable']    = $serverConfig['mp_overtime_enable']??null;
        $configArr['mp_overtime_maxrounds'] = $serverConfig['mp_overtime_maxrounds']??null;
        $configArr['mp_roundtime_defuse']   = $serverConfig['mp_roundtime_defuse']??null;
        $configArr['mp_freezetime']         = $serverConfig['mp_freezetime']??null;
        $configArr['mp_c4timer']            = $serverConfig['mp_c4timer']??null;
        return $configArr;
    }

    public function getGameVersion(){
        $currentVersionKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_GAME_VERSION];
        $res = $this->getValue($currentVersionKey);
        return $res?$res:null;
    }
    /**
     * @param $team
     * @return array|null
     */
    public function outPutTeam($team){
        if (!empty($team['team_id'])){
            $newMap = $team;
            $newMap['team_id'] = (int)$team['team_id'];
            $newMap['image'] = ImageConversionHelper::showFixedSizeConversion(@$team['image'],200,200);
            $newMap['opponent_order'] = (int)$team['opponent_order'];

        }else{
            $newMap = null;
        }
        return $newMap;
    }

    /**
     * @param $winner
     * @return array|null
     */
    public function outPutMatchWinner($winner){
        if (!empty($winner['team_id'])){
            $newMap = $winner;
            $newMap['team_id'] = (int)$winner['team_id'];
            $newMap['opponent_order'] = (int)$winner['opponent_order'];
        }else{
            $newMap = null;
        }
        return $newMap;
    }
    /**
     * @param $winner
     * @return array|null
     */
    public function outPutBattleWinner($winner){
        if (!empty($winner['team_id'])){
            $newMap = $winner;
            $newMap['team_id'] = (int)$winner['team_id'];
            $newMap['opponent_order'] = (int)$winner['opponent_order'];
        }else{
            $newMap = null;
        }
        return $newMap;
    }

    /**
     * @param $map
     * @return array|null
     */
    public function outPutMap($map){
        if (!empty($map['map_id'])){
            $newMap = $map;
            $newMap['map_id'] = (int)$map['map_id'];
            $newMap['external_id'] = $map['external_id']?$map['external_id']:null;
        }else{
            $newMap = null;
        }
        return $newMap;
    }

    /**
     * @param $match_scores
     * @return array
     */
    public function outPutMatchScores($match_scores){
        $data = [];
        if (!empty($match_scores[0]['team_id'])){
            $data[0] = $match_scores[0];
            $data[0]['team_id'] = (int)$match_scores[0]['team_id'];
            $data[0]['image'] = ImageConversionHelper::showFixedSizeConversion($match_scores[0]['image'],200,200);
            $data[0]['score'] = (int)$match_scores[0]['score'];
        }

        if (!empty($match_scores[1]['team_id'])){
            $data[1] = $match_scores[1];
            $data[1]['team_id'] = (int)$match_scores[1]['team_id'];
            $data[1]['image'] = ImageConversionHelper::showFixedSizeConversion($match_scores[1]['image'],200,200);
            $data[1]['score'] = (int)$match_scores[1]['score'];
        }
        return array_values($data);
    }

    /**
     * getCtScore
     */
    public function getCtScore()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_CT_SCORE]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_CT_SCORE]) : 0;
    }

    /**
     * getCtScore
     */
    public function getTScore()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_T_SCORE]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_T_SCORE]) : 0;
    }

    /**
     * getRoundPlayerWeaponBySteamId
     * @param $steamId
     * @return array
     */
    public function getRoundPlayerWeaponInfoBySteamId($steamId)
    {
        $weapon_list = $this->getRoundPlayerWeaponBySteamId($steamId);
        $list        = [];
        if ($weapon_list) {
            foreach ($weapon_list as $k => $v) {
                if ($v!='vesthelm'){
                    $list[$k] = $this->getWeaponByName($v);
                }

            }
        }
        return $list;
    }

    /**
     * get round player weapon by steam id
     * @param $steamId
     * @return mixed
     */
    public function getRoundPlayerWeaponBySteamId($steamId)
    {
        $weapons_json_list = $this->hGet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId);
        return json_decode($weapons_json_list, true);
    }

    /**
     * getRoundPlayerGrenadesBySteamId
     * @param $steamId
     * @return array
     */
    public function getRoundPlayerGrenadesBySteamId($steamId)
    {
        $weapons_json_list = $this->hGet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId);
        return json_decode($weapons_json_list, true);

    }

    /**
     * getRoundPlayerGrenadesBySteamId
     * @param $steamId
     * @return array
     */
    public function getRoundPlayerGrenadesInfoBySteamId($steamId)
    {
        $weapon_list = $this->getRoundPlayerGrenadesBySteamId($steamId);
        $list        = [];
        if ($weapon_list) {
            foreach ($weapon_list as $k => $v) {
                $list[$k] = $this->getWeaponByName($v);
            }
        }
        $list = array_slice($list,0,4);//投掷物最多四个限制容错
        return $list;
    }


    /**
     * getRoundsByBattleId
     * @param $battleId
     * @return array
     */
    public function getRoundsByBattleId($battleId)
    {
        $list     = $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, self::KEY_TAG_LIST]);
        $new_list = array_map([__CLASS__, 'mapJson'], $list);
        foreach ($new_list as $k => $v) {
            if (!$v['is_use']) {
                unset($new_list[$k]);
            }
        }
//        return $new_list;
        $data = $this->sortArrayByField($new_list, 'round_ordinal');
        return $data;
    }

    /**
     * getRoundsByBattleId
     * @param $battleId
     * @return array
     */
    public function getNoUseRoundsByBattleId($battleId)
    {
        $count    = 0;
        $list     = $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, self::KEY_TAG_LIST]);
        $new_list = array_map([__CLASS__, 'mapJson'], $list);
        foreach ($new_list as $k => $v) {
            if (!$v['is_use']) {
                $count++;
            }
        }
        return $count;
    }

    public function sortArrayByField($data, $field, $order = SORT_ASC)
    {
        $keys         = array_keys($data);
        $array_column = array_column($data, $field);
        array_multisort(
            $array_column, $order, SORT_NUMERIC, $data, $keys
        );
        $data = array_combine($keys, $data);
        return $data;
    }

    public function getLiveRoundsId($battleId)
    {
        $rounds     = $this->getRoundsByBattleId($battleId);
        $array_keys = array_keys($rounds);
        sort($array_keys);
        return json_encode($array_keys,320);
    }

    public function getWeaponCast($weaponList,$grenadesList,$has_kevlar,$has_helmet,$has_defusekit){
        $weaponMoney = 0;
        if (!empty($weaponList)) {
            foreach ($weaponList as $k => $v) {
                $weapon      = $this->getWeaponByName($v['external_name']);
                $weaponMoney += $weapon['cost'];
            }
        }
        if (!empty($grenadesList)) {
            foreach ($grenadesList as $k => $v) {
                $weapon      = $this->getWeaponByName($v['external_name']);
                $weaponMoney += $weapon['cost'];
            }
        }
        if (!empty($has_kevlar) && empty($has_helmet)) {
            $weapon      = $this->getWeaponByName('item_kevlar');
            $weaponMoney += $weapon['cost'];
        }
        if (!empty($has_kevlar) && !empty($has_helmet)) {
            $weapon      = $this->getWeaponByName('item_assaultsuit');
            $weaponMoney += $weapon['cost'];
        }
        if (!empty($has_defusekit)) {
            $weapon      = $this->getWeaponByName('item_defuser');
            $weaponMoney += $weapon['cost'];
        }

        return $weaponMoney;
    }

    /**
     * currentRound 当前回合
     */
    public function currentRound()
    {
        if (!empty($this->temp_round_ordinal)) {
            $temp                     = $this->temp_round_ordinal;
            $this->temp_round_ordinal = '';
            return $temp;
        }
        $count = $this->getIsUseCountRoundsByBattleId();
        return $count ?$count: 1;
    }

    /**
     * 是否是冻结时间
     */
    public function isFreezeTime()
    {
        $match_status = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_FREEZE_TIME]);
        return $match_status ? (bool)$match_status : false;
    }

    /**
     * 对局开始时间
     */
    public function currentBattleBeginAt($model)
    {
        $battleBeginAt = null;
        $battleDetail  = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        if (!empty($battleDetail)) {
            $battleBeginAt = $battleDetail['begin_at'];
        }
        return $battleBeginAt;
    }

    /**
     * set FreezeTime
     * @param $value
     * @return mixed
     */
    public function setFreezeTime($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_FREEZE_TIME], $value);

    }
    //设置暂停状态
    public function setPause($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND,self::KEY_TAG_IS_PAUSE], $value);
    }
    //是否为暂停状态
    public function isPause()
    {
        $pause_status = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, 'is_pause']);
        return $pause_status ? (bool)$pause_status : false;
    }

    /**
     * @param $model
     * @param $battleId
     * @param $roundOrder
     * @param $itemData
     * @return bool
     * 设置炸弹信息
     */
    public function setBombState($model,$battleId,$roundOrder,$itemData)
    {
        $key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,"bombState"];
        $model->hSet($key,$roundOrder,$itemData);
        return true;
    }
    public function getBombState($model,$battleId,$roundOrder)
    {
        $key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,"bombState"];
        $bombStateJson = $model->hGet($key,$roundOrder);
        if($bombStateJson){
            $bombState = json_decode($bombStateJson,true);
            return $bombState;
        }else{
            return false;
        }
    }

    /**
     * getMatchWinner
     * @return mixed
     */
    public function getMatchWinner()
    {
        $win                    = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_WINNER]), true);
        $data                   = [];
        $data['team_id']        = @$win['team_id'] ?? null;
        $data['name']           = @$win['name'] ?? '';
        $data['image']          = @$win['image'] ?ImageConversionHelper::showFixedSizeConversion($win['image'],200,200): '';
        $data['opponent_order'] = @$win['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getBattleWinner
     * @return mixed
     */
    public function getBattleWinner()
    {
        $win                    = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_WINNER]), true);
        $data                   = [];
        $data['team_id']        = @$win['team_id'] ?? null;
        $data['name']           = @$win['name'] ?? '';
        $data['image']          = @$win['image'] ?ImageConversionHelper::showFixedSizeConversion($win['image'],200,200): '';
        $data['opponent_order'] = @$win['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getStartingCt
     * @return mixed
     */
    public function getCurrentCt()
    {
        $team                   = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_CT, self::KEY_TAG_DETAILS]), true);
        $data                   = [];
        $data['team_id']        = @$team['team_id'] ?? null;
        $data['name']           = @$team['name'] ?? '';
        $data['image']          = @$team['image'] ?? '';
        $data['opponent_order'] = @$team['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getStartingCt
     * @return mixed
     */
    public function getStartingCt()
    {
        $team                   = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_START_TEAM_CT, self::KEY_TAG_DETAILS]), true);
        $data                   = [];
        $data['team_id']        = @$team['team_id'] ?? null;
        $data['name']           = @$team['name'] ?? '';
        $data['image']          = @$team['image'] ?? '';
        $data['opponent_order'] = @$team['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getStartingCt
     * @return mixed
     */
    public function setStartingCt($detalis)
    {
        $baseArr = [];
        $baseArr['start_ct']['team_id']                     = $detalis['team_id'];
        $baseArr['start_ct']['opponent_order']              = $detalis['opponent_order'];
        $baseArr['start_ct']['log_name']                    = @$detalis['log_name'];
        $baseArr['start_ct']['name']                        = $detalis['name'];
        $baseArr['start_ct']['image']                       = $detalis['image'];
        $this->setCurrentBattleDetails($baseArr);
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_START_TEAM_CT, self::KEY_TAG_DETAILS], json_encode($detalis,320));
    }

    /**
     * getStartingT
     * @return mixed
     */
    public function getStartingT()
    {
        $team                   = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_START_TEAM_T, self::KEY_TAG_DETAILS]), true);
        $data                   = [];
        $data['team_id']        = @$team['team_id'] ?? null;
        $data['name']           = @$team['name'] ?? '';
        $data['image']          = @$team['image'] ?? '';
        $data['opponent_order'] = @$team['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getStartingCt
     * @param $detalis
     * @return mixed
     */
    public function setStartingT($detalis)
    {
        $baseArr = [];
        $baseArr['start_t']['team_id']                      = $detalis['team_id'];
        $baseArr['start_t']['opponent_order']               = $detalis['opponent_order'];
        $baseArr['start_t']['log_name']                     = @$detalis['log_name'];
        $baseArr['start_t']['name']                         = $detalis['name'];
        $baseArr['start_t']['image']                        = $detalis['image'];
        $this->setCurrentBattleDetails($baseArr);
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_START_TEAM_T, self::KEY_TAG_DETAILS], json_encode($detalis,320));
    }

    /**
     * getStartingT
     * @return mixed
     */
    public function getCurrentT()
    {
        $team                   = json_decode($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_T, self::KEY_TAG_DETAILS]), true);
        $data                   = [];
        $data['team_id']        = @$team['team_id'] ?? null;
        $data['name']           = @$team['name'] ?? '';
        $data['image']          = @$team['image'] ?? '';
        $data['opponent_order'] = @$team['opponent_order'] ?? '';
        return $data;
    }

    /**
     * getMatchScores
     * @return mixed
     */
    public function getMatchScores()
    {
        $team_t                       = $this->getCurrentT();
        $team_ct                      = $this->getCurrentCt();
        //判断主客队
        if ($team_t['opponent_order'] == 1) {
            $res_arr[0]['team_id']        = @$team_t['team_id'] ? $team_t['team_id'] : null;
            $res_arr[0]['name']           = @$team_t['name'];
            $res_arr[0]['image']          = @$team_t['image'];
            $res_arr[0]['opponent_order'] = @$team_t['opponent_order'];
            $res_arr[0]['score']          = $this->getMatch1Score();
            $res_arr[1]['team_id']        = @$team_ct['team_id'] ? $team_ct['team_id'] : null;
            $res_arr[1]['name']           = @$team_ct['name'];
            $res_arr[1]['image']          = @$team_ct['image'];
            $res_arr[1]['opponent_order'] = @$team_ct['opponent_order'];
            $res_arr[1]['score']          = $this->getMatch2Score();
        } else {
            $res_arr[0]['team_id']        = @$team_ct['team_id'] ? $team_ct['team_id'] : null;
            $res_arr[0]['name']           = @$team_ct['name'];
            $res_arr[0]['image']          = @$team_ct['image'];
            $res_arr[0]['opponent_order'] = @$team_ct['opponent_order'];
            $res_arr[0]['score']          = $this->getMatch1Score();
            $res_arr[1]['team_id']        = @$team_t['team_id'] ? $team_t['team_id'] : null;
            $res_arr[1]['name']           = @$team_t['name'];
            $res_arr[1]['image']          = @$team_t['image'];
            $res_arr[1]['opponent_order'] = @$team_t['opponent_order'];
            $res_arr[1]['score']          = $this->getMatch2Score();
        }
        return $res_arr;
    }

    public function getMatch1Score()
    {
        $battle1score = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE]);
        return  $battle1score? $battle1score : 0;
    }

    public function getMatch2Score()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]) : 0;
    }

    public function setCurrentMap($value)
    {
        $mapKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_MAP, self::KEY_TAG_DETAILS];
        $this->setValue($mapKey, json_encode($value,320));
    }

    /**
     * getCurrentMap
     */
    public function getCurrentMap()
    {
        $res                             = [];
        $map                             = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_MAP, self::KEY_TAG_DETAILS]);
        $mapInfo                         = json_decode($map, true);
        $res['map_id']                   = $mapInfo['map_id'];
        $res['name']                     = $mapInfo['name'];
        $res['name_cn']                  = $mapInfo['name_cn'];
        $res['external_id']              = $mapInfo['external_id'];
        $res['external_name']            = $mapInfo['external_name'];
        $res['short_name']               = $mapInfo['short_name'];
        $res['map_type']                 = $mapInfo['map_type'];
        $res['map_type_cn']              = $mapInfo['map_type_cn'];
        $res['is_default']               = $mapInfo['is_default'];
        $res['slug']               = $mapInfo['slug'];
        $res['image']['square_image']    = $mapInfo['image']['square_image'];
        $res['image']['rectangle_image'] = $mapInfo['image']['rectangle_image'];
        $res['image']['thumbnail']       = $mapInfo['image']['thumbnail'];
        return $res;
    }

    /**
     * getDuration 对局时长
     */
    public function getDuration($event)
    {
        $match_start_time = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT]);
        $match_start_time = !$match_start_time ? 0 : strtotime($match_start_time);
        $duration         = $match_start_time == 0 ? 0 : (strtotime($event['info']['log_time']) - $match_start_time);
        return $duration < 0 ? 0 : $duration;
    }

    /**
     * isFinished match是否结束
     */
    public function isFinished()
    {
        $match_status = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS]);
        return $match_status == self::STATUS_END;
    }

    /**
     * isFinished match是否结束
     */
    public function isBattleFinished()
    {
        $match_status = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_STATUS]);
        return $match_status == self::STATUS_END;
    }

    /**
     * isLive match是否是正式数据
     */
    public function isLive()
    {
        if ($this->temp_live) {
            $this->temp_live = false;
            return true;
        }
        if ($this->event_type == 'battle_end') {
            return true;
        }
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_LIVE]) ? true : false;
    }

    /**
     * set match is_live
     * @param $value
     * @return mixed
     */
    public function setIsLive($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_LIVE], $value);
    }


    /**
     * 获取round_time 回合内时间 = 当前event的log_time-回合开始时间(通过round_start时间可以获取回合开始时间)
     * @param $event
     * @return false|string
     */
    public function getRoundTime($event)
    {
        $mpRoundTimeDefuse = $this->getMpRoundTimeDefuse();
        if ($this->isFreezeTime()){
            return $mpRoundTimeDefuse;
        }
        $log_time         = $event['info']['log_time'];
        $round_start_time = strtotime($this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT]));
        $time             = $mpRoundTimeDefuse - (strtotime($log_time) - $round_start_time);
        return $time > 0 ? $time : 0;
    }

    public function getMpRoundTimeDefuse(){
        $serverInit = $this->getServerCvarsInit();
        if (!empty($serverInit)&&array_key_exists('mp_roundtime_defuse',$serverInit)&&is_int($serverInit['mp_roundtime_defuse'])){
            return intval(($serverInit['mp_roundtime_defuse'])*60);
        }else{
            return 115;
        }
    }

    /**
     * 获取battleOrder
     * @return mixed
     */
    public function getBattleOrder()
    {
        return $this->getRealOrder();
    }

    /**
     * 获取roundOrder
     * @return mixed
     */
    public function getRoundOrdinal()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL];
        return $this->getValue($key) ? (int)$this->getValue($key) : 1;
    }

    /**
     * getTimeSincePlant
     * @return mixed
     */
    public function getTimeSincePlant($event)
    {

        if ($this->getIsBombPlanted()) {
            //安放炸弹后时间 40秒倒计时
            $time_since_plant = 0;
            //存的是事件时间 需要转化
            $time_since_plant_time = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_TIME_SINCE_PLANT]);
            if ($time_since_plant_time) {
                $time_since_plant = $this->getMpC4timer() - (strtotime($event['info']['log_time']) - strtotime($time_since_plant_time));
            }
            $time_since_plant = $time_since_plant > 0 ? $time_since_plant : 0;
            return $time_since_plant;
        } else {

            return 0;
        }

    }

    public function getMpC4timer(){
        $serverInit = $this->getServerCvarsInit();
        if (!empty($serverInit)&&array_key_exists('mp_c4timer',$serverInit)&&is_int($serverInit['mp_c4timer'])){
            return $serverInit['mp_c4timer'];
        }else{
            return 40;
        }
    }

    public function hasTempBombTime(){
        //设置临时时间
        if ($this->temp_time_since_plant) {
            $temp_time                   = $this->temp_time_since_plant;
            $this->temp_time_since_plant = '';
            return $temp_time;
        }else{
            return 0;
        }

    }

    /**
     * getIsBombPlanted
     * @return mixed
     */
    public function getIsBombPlanted()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_BOMB_PLANTED];
        return $this->getValue($key) ? true : false;
    }

    /**
     * getBattleOrder
     * @return mixed
     */
    public function getCurrentBattleTimestamp($battleBeginAt,$socketAt)
    {
        $battle_timestamp = 0;
        if($battleBeginAt){
            $currentBattleTime = strtotime($battleBeginAt);
            $battle_timestamp = strtotime($socketAt) - $currentBattleTime;
        }
        return $battle_timestamp;
    }

    /**
     * 根据名称查找map
     * @param $name
     * @return array
     */
    public function findOneMapByName($name)
    {
        $arr =  explode('/',$name);
        $name = end($arr);
        //查询出缓存中csgo地图
        $map_redis = $this->getValue(['table','metadata_csgo_map',$name]);
        if (!empty($map_redis)&&$map_redis!=null){
            return json_decode($map_redis,true);
        }else{
            $dbMap = MetadataCsgoMap::find()->alias('map')
                ->select([
                    'map.id','map.name','map.name_cn','map.external_id','map.external_name','map.short_name','map.is_default'
                    ,'map.square_image','map.rectangle_image','map.thumbnail','map.slug','type.c_name','type.e_name'
                ])
                ->leftJoin("enum_csgo_map as type", "type.id = map.map_type")->where(['map.external_name'=>$name])->asArray()->one();
            if (empty($dbMap)||$dbMap==null){
                $this->error_hash_log('findOneMapByName',['name'=>$name,'event'=>$this->event]);
            }else{
                $this->setValue(['table','metadata_csgo_map',$name],json_encode($dbMap,320));
                return $dbMap;
            }
            return [];

        }
    }


    /**
     * @return mixed|string
     */
    public function getBattleId()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER]) : 'warmUp_1';
    }

    /**
     * @return mixed|null
     */
    public function getRealBattleId()
    {
        $battleId = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID]);
        return $battleId? $battleId: null;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setBattleId($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID], $value);
    }

    /**
     * 对局即将开始
     * 设置battle_up_coming为true,在battle_start设置is_live为true,在battle_end设置battle_up_coming为false,设置is_live为false
     * is_live是显示字段,battle_up_coming是判断字段
     * @param $data
     * @return mixed
     */
    public function battle_up_coming($data)
    {
        //设置比赛状态
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], true);
        return $data;
    }

    /**
     * 获取up_coming状态
     */
    public function getUpComing()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING]);
    }

    /**
     * set current match status
     * @param $status
     * @return mixed
     */
    public function setMatchStatus($status)
    {
        $nowStatus = $this->getMatchStatus();
        if ($nowStatus == 3) {
            return false;
        }
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS], $status);//match 状态
    }

    /**
     * set current match begin_at
     * @param $time
     * @return mixed
     */
    public function setBattleBeginAt($time)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT], $time);//match 开始时间
    }

    /**
     * setBattleIdByRealOrder
     * @return mixed
     */
    public function setBattleIdByRealOrder()
    {
        $battle = BattleServiceBase::getBattleByMatchIdOnWs($this->matchId, $this->getRealOrder());
        if (is_array($battle) && array_key_exists('id', $battle)) {
            $battleObj = MatchBattle::find()->where(['id'=>$battle['id']])->one();
            $map = [
                'deleted'              => 2,
                'deleted_at'           => null,
                'status'               => 1,
                'is_draw'              => 2,
                'is_forfeit'           => 2,
                'is_default_advantage' => 2,
                'is_battle_detailed'   => 1,
            ];
            $battleObj->setAttributes($map);
            $battleObj->save();
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID], $battle['id']);
            return $battle['id'];
        } else {
            $battleId = $this->createMatchBattleByMatchId();
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_ID], $battleId);
        }
    }

    /**
     * getBattleBase
     */
    public function getBattleBase()
    {
        $baseArr['battle_id']      = $this->getBattleId() ? $this->getBattleId() : '';
        $baseArr['real_battle_id'] = $this->getRealBattleId() ? $this->getRealBattleId() : '';
        $baseArr['begin_at']       = '';
        $baseArr['end_at']         = '';
        $baseArr['is_pause']       = false;
        if (strstr($this->getBattleId(), 'live')) {
            $baseArr['is_live'] = true;
        } else {
            $baseArr['is_live'] = false;
        }
        if (strstr($this->getBattleId(), 'live')) {
            $baseArr['is_use'] = true;
        } else {
            $baseArr['is_use'] = false;
        }
//        $baseArr['is_use']                                  = true;
        $baseArr['is_draw']                                 = false;
        $baseArr['is_forfeit']                              = false;
        $baseArr['is_default_advantage']                    = false;
        $baseArr['real_order']                              = $this->getRealOrder()?$this->getRealOrder():'';
        $baseArr['order']                                   = $this->getRealOrder()?$this->getRealOrder():'';
        $baseArr['identification']                          = '';
        $baseArr['number']                                  = 0;
        $baseArr['status']                                  = self::STATUS_GOING;
        $baseArr['is_bomb_planted']                         = false;
        $baseArr['duration']                                = 0;
        $baseArr['first_to_5_rounds_wins_side']             = '';
        $baseArr['first_to_5_rounds_wins_team']             = '';
        $baseArr['first_to_5_rounds_wins_detail']           = '';
        $baseArr['first_to_5_rounds_wins_team_name']        = '';
        $baseArr['win_round_1_side']                        = '';
        $baseArr['win_round_1_team']                        = '';
        $baseArr['win_round_1_team_name']                   = '';
        $baseArr['win_round_1_detail']                      = '';
        $baseArr['win_round_16_side']                       = '';
        $baseArr['win_round_16_team']                       = '';
        $baseArr['win_round_16_team_name']                  = '';
        $baseArr['win_round_16_detail']                     = '';
        $baseArr['map_details']['map_id']                   = '';
        $baseArr['map_details']['name']                     = '';
        $baseArr['map_details']['name_cn']                  = '';
        $baseArr['map_details']['external_id']              = '';
        $baseArr['map_details']['external_name']            = '';
        $baseArr['map_details']['short_name']               = '';
        $baseArr['map_details']['map_type']                 = '';
        $baseArr['map_details']['map_type_cn']              = '';
        $baseArr['map_details']['is_default']               = '';
        $baseArr['map_details']['image']['square_image']    = '';
        $baseArr['map_details']['image']['rectangle_image'] = '';
        $baseArr['map_details']['image']['thumbnail']       = '';
        //start_team_ct
        $baseArr['start_ct']['team_id']        = '';
        $baseArr['start_ct']['opponent_order'] = '';
        $baseArr['start_ct']['log_name']       = '';
        $baseArr['start_ct']['name']           = '';
        $baseArr['start_ct']['image']          = '';
        $baseArr['start_ct']['score']          = '';
        $baseArr['start_ct']['1st_half_score'] = '';
        $baseArr['start_ct']['2nd_half_score'] = '';
        $baseArr['start_ct']['ot_score']       = '';
        //start_team_t
        $baseArr['start_t']['team_id']        = '';
        $baseArr['start_t']['opponent_order'] = '';
        $baseArr['start_t']['log_name']       = '';
        $baseArr['start_t']['name']           = '';
        $baseArr['start_t']['image']          = '';
        $baseArr['start_t']['score']          = '';
        $baseArr['start_t']['1st_half_score'] = '';
        $baseArr['start_t']['2nd_half_score'] = '';
        $baseArr['start_t']['ot_score']       = '';

        //team ct
        $baseArr['ct']['team_id']               = '';
        $baseArr['ct']['opponent_order']        = '';
        $baseArr['ct']['log_name']              = '';
        $baseArr['ct']['name']                  = '';
        $baseArr['ct']['image']                 = '';
        $baseArr['ct']['starting_faction_side'] = '';
        //team t
        $baseArr['terrorist']['team_id']               = '';
        $baseArr['terrorist']['opponent_order']        = '';
        $baseArr['terrorist']['log_name']              = '';
        $baseArr['terrorist']['name']                  = '';
        $baseArr['terrorist']['image']                 = '';
        $baseArr['terrorist']['starting_faction_side'] = '';
        //winner info
        $baseArr['winner']['side']           = '';
        $baseArr['winner']['opponent_order'] = '';
        return $baseArr;
    }

    public function getRoundBase()
    {
        //round details
        $baseArr['round_ordinal']                = $this->getRoundOrdinal();
        $baseArr['battle_id']                    = $this->getBattleId();
        $baseArr['real_battle_id']               = $this->getRealBattleId();
        $baseArr['begin_at']                     = '';
        $baseArr['end_at']                       = '';
        $baseArr['status']                       = 2;
        $baseArr['is_bomb_planted']              = false;
        $baseArr['is_freeze_time']               = false;
        $baseArr['is_use']                       = true;
        $baseArr['bomb_planted_at']              = '';
        $baseArr['kills']                        = 0;
        $baseArr['deaths']                       = 0;
        $baseArr['first_kills_side']             = '';
        $baseArr['first_death_side']             = '';
        $baseArr['is_first_kills_headshot_side'] = '';
        $baseArr['ace_kill_side']                = '';
        //score
        $baseArr['round_ct_score'] = 0;
        $baseArr['round_t_score']  = 0;
        //team ct
        $baseArr['ct']['team_id']        = '';
        $baseArr['ct']['opponent_order'] = '';
        $baseArr['ct']['log_name']       = '';
        $baseArr['ct']['name']           = '';
        $baseArr['ct']['image']          = '';

        //team t
        $baseArr['terrorist']['team_id']        = '';
        $baseArr['terrorist']['opponent_order'] = '';
        $baseArr['terrorist']['log_name']       = '';
        $baseArr['terrorist']['name']           = '';
        $baseArr['terrorist']['image']          = '';
        $baseArr['terrorist']['image']          = '';

        //winner info
        $baseArr['winner_side']    = '';
        $baseArr['round_win_type'] = '';
        //ct statistics
        $baseArr['survived_players_ct'] = 0;
        $baseArr['ct_kills']            = 0;
        $baseArr['ct_headshot_kills']   = 0;
        $baseArr['ct_is_knife_kill']    = false;
        //t statistics
        $baseArr['survived_players_t']       = 0;
        $baseArr['terrorist_kills']          = 0;
        $baseArr['terrorist_headshot_kills'] = 0;
        $baseArr['terrorist_is_knife_kill']  = false;
        //1vn
        $baseArr['1vn_ct_player']       = [];
        $baseArr['1vn_ct_player_final']       = [];
        $baseArr['1vn_t_player']       = [];
        $baseArr['1vn_t_player_final']       = [];
        return $baseArr;
    }


    public function setWarmUpBattleId()
    {
        return true;
    }

    public function setBattleOrder($value)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER];
        return $this->setValue($key, $value);
    }

    public function createMatchBattleByMatchId()
    {
        $matchBattle         = new MatchBattle();
        $attributes['game']  = 1;
        $attributes['match'] = $this->matchId;
        $attributes['order'] = $this->getBattleOrder();
        $matchBattle->setAttributes($attributes);
        $matchBattle->save();
        return $matchBattle->id;
    }


    /**
     *set battle List
     * @param array $updateArr
     * @param string $battleId
     * @return string|void
     */
    public function setCurrentBattleDetails($updateArr = [], $battleId='')
    {
        if ($battleId){
            $res = $this->getBattleDetailsById($battleId);
        }else{
            $res = $this->getCurrentBattleDetails();
        }
        if (!$res) {
            $res = $this->getBattleBase();
        }
        if (array_key_exists('map_details', $updateArr)) {
            if (is_array($res['map_details'])){
                $updateArr['map_details'] = array_merge($res['map_details'], $updateArr['map_details']);
            }
        }
        if (array_key_exists('start_ct', $updateArr)) {
            if (is_array($res['start_ct'])){
                $updateArr['start_ct'] = array_merge($res['start_ct'], $updateArr['start_ct']);
            }
        }
        if (array_key_exists('start_t', $updateArr)) {
            if (is_array($res['start_t'])){
                $updateArr['start_t'] = array_merge($res['start_t'], $updateArr['start_t']);
            }
        }
        if (array_key_exists('ct', $updateArr)) {
            if (is_array($res['ct'])){
                $updateArr['ct'] = array_merge($res['ct'], $updateArr['ct']);
            }
        }
        if (array_key_exists('terrorist', $updateArr)) {
            if (is_array($res['terrorist'])){
                $updateArr['terrorist'] = array_merge($res['terrorist'], $updateArr['terrorist']);
            }
        }
        if (array_key_exists('winner', $updateArr)) {
            if (is_array($res['winner'])){
                $updateArr['winner'] = array_merge($res['winner'], $updateArr['winner']);
            }
        }
        $new_res = $res ? array_merge($res, $updateArr) : $updateArr;
        if ($res['status'] == 3) {
            $new_res['status'] = 3;
        }
        if (!$battleId){
            $battleId = $this->getBattleId();
        }
        return $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST], $battleId, json_encode($new_res,320));
    }

    public function doArrayMerge($arr1,$arr2){

        return array_merge($arr1,$arr2);
    }

    /**
     * @return string
     */
    public function getBattleList()
    {
        $battle_key = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST];
        return $this->hGetAll($battle_key);
    }

    /**
     * @return array
     */
    public function getCurrentBattleDetails()
    {
        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST];
        $json = $this->hGet($key, $this->getBattleId());;
        return json_decode($json, true);
    }

    /**
     * @return string
     */
    public function getBattleDetailsById($id)
    {
        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST];
        $json = $this->hGet($key, $id);;
        return json_decode($json, true);
    }

    /**
     * @return string
     */
    public function getCurrentRoundDetails()
    {
        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
        $json = $this->hGet($key, $this->getRoundOrdinal());
        return json_decode($json, true);
    }

    /**
     * set round order
     */
    public function setRoundOrder($order)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL];
        return $this->setValue($key, $order);
    }
    //设置当前round order
    public function setCurrentRoundOrder($model,$order)
    {
        if (!$model){
            $model = new self();
        }
        BayesCsgoSocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,['current_round'=>$order]);
        $model->roundOrder = $order;
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ORDER];

        return $this->setValue($key, $order);
    }
    //获取当前round order
    public function getCurrentRoundOrder()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ORDER];
        return $this->getValue($key);
    }
    /**
     * 设置当前round id
     */
    public function setCurrentRoundId($id)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ID];
        return $this->setValue($key, $id);
    }

    /**
     * 获取当前round id
     */
    public function getCurrentRoundId()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ID];
        return $this->getValue($key);
    }

    public function clearRoundScore()
    {
        $this->setRoundCtScore(0);
        $this->setRoundTScore(0);
        $this->setStartingCtScore(0);
        $this->setStartingTScore(0);
    }

    public function getRealOrder()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_REAL_ORDER];
        return $this->getValue($key);
    }

    public function getNumber()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_NUMBER];
        return $this->getValue($key)?$this->getValue($key):0;
    }


    public function incrNumber()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_NUMBER];
        return $this->incr($key);
    }

    public function incrRealOrder()
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_REAL_ORDER];
        return $this->incr($key);
    }

    public function setRealOrder($value)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_REAL_ORDER];
        return $this->setValue($key, $value);
    }

    public function setNumber($value)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_NUMBER];
        return $this->setValue($key, $value);
    }

    public function getMatchRealTimeInfoById($id)
    {
        return MatchRealTimeInfo::find()->where(['id' => $id])->asArray()->one();
    }

    public function is_default_advantage($matchId,$realOrder)
    {
        $battle = BattleServiceBase::getBattleByMatchIdOnWs($matchId, $realOrder);
        if ($battle && $battle['is_default_advantage'] == 1 && $battle['winner']) {
            return $battle['winner'];
        } else {
            return false;
        }
    }

    public function is_forfeit($matchId, $realOrder)
    {
        $battle = BattleServiceBase::getBattleByMatchIdOnWs($matchId, $realOrder);
        if ($battle && $battle['is_forfeit'] == 1 && $battle['winner']) {
            return $battle['winner'];
        } else {
            return false;
        }
    }

    public function createBattleTagOnBattleStart()
    {
        $real_order = $this->getRealOrder();
        $number     = $this->getNumber();
        $upComing   = $this->getUpComing();
        $battleId   = $this->getRealBattleId();
        //return tag
        if (empty($battleId) && empty($real_order) && empty($upComing)) {
            //warmUp number+1
            $number = $this->incrNumber();
            return 'warmUp_' . $number;
        } elseif (empty($battleId) && empty($real_order) && $number && $upComing) {
            //1-live-1  real_order=1or2 number=1

            //is_default_advantage
            $default_advantage_order = $this->is_default_advantage($this->matchId,1);
            if ($default_advantage_order) {
                //set match score
                if ($default_advantage_order == 1) {
                    $this->setMatch1Score(1);
                    $this->setMatch2Score(0);
                } elseif ($default_advantage_order == 2) {
                    $this->setMatch1Score(0);
                    $this->setMatch2Score(1);
                }
                $this->setRealOrder(2);
                //update battle is_default_advantage
                $baseBattleArr                         = [];
                $baseBattleArr['is_default_advantage'] = true;
                $this->setCurrentBattleDetails($baseBattleArr);
            }
            //is_forfeit
            $forfeit_order = $this->is_forfeit($this->matchId, $this->getRealOrder());
            if ($forfeit_order) {
                //set match score
                if ($forfeit_order == 1) {
                    $score = $this->getMatch1Score();
                    $this->setMatch1Score($score + 1);
//                    $this->setMatch2Score(0);
                } elseif ($forfeit_order == 2) {
                    $score = $this->getMatch2Score();
                    $this->setMatch2Score($score + 1);
                }
                //incr order
                $this->incrRealOrder();
                //update battle is_forfeit
                $baseBattleArr               = [];
                $baseBattleArr['is_forfeit'] = true;
                $this->setCurrentBattleDetails($baseBattleArr);
            }
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT], '');//match 结束时间
            $this->setnx([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BEGIN_AT], $this->event['info']['log_time']);//match 开始时间
            if (!$default_advantage_order && !$forfeit_order) {
                $this->setMatch1Score(0);
                $this->setMatch2Score(0);
                $this->setRealOrder(1);
            }
            //set number
            $this->setNumber(1);
            $this->setBattleIdByRealOrder();
            $new_real_order = $this->getRealOrder();
            return $new_real_order . '_live_1';
        } elseif ($battleId && $real_order && $number && $upComing) {
            //1-live-2 number+1
            $number = $this->incrNumber();
            $this->setCurrentBattleDetails(['is_use' => false]);
            return $real_order . '_live_' . $number;
        } elseif ($battleId && $real_order && $number && empty($upComing)) {
            //set battle details
//            $this->setbattleDetais();
            //1-warmUp-1 number=1
            $this->setNumber(1);
            $this->setBattleId(0);
            return $real_order . '_warmUp_1';
        } elseif (empty($battleId) && $real_order && $number && empty($upComing)) {
            //1-warmUp-2 number=1
            $number = $this->incrNumber();
            return $real_order . '_warmUp_' . $number;
        } elseif (empty($battleId) && $real_order && $number && $upComing) {
            //is_forfeit
            $forfeit_order = $this->is_forfeit($this->matchId, $this->getRealOrder()+1);
            if ($forfeit_order) {
                //set match score
                if ($forfeit_order == 1) {
                    $score = $this->getMatch1Score();
                    $this->setMatch1Score($score + 1);
                } elseif ($forfeit_order == 2) {
                    $score = $this->getMatch2Score();
                    $this->setMatch2Score($score + 1);
                }
                //incr order
                $this->incrRealOrder();
            }
            $real_order = $this->incrRealOrder();
            $this->setNumber(1);
            //2-live-1 real_order+1 number=1
            return $real_order . '_live_1';
        }
    }

    /**
     * setCurrentOrder
     * @param $value
     * @return mixed
     */
    public function setCurrentOrder($value)
    {
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER];
        return $this->setValue($key, $value);
    }

    public function getMatchStatus()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS])?$this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS]):2;
    }

    /**
     * @return mixed
     */
    public function getBattleBeginAt()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT]) ? $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT]) : '';
    }


    public function clearNoUseEvents(){
        $battleId = $this->getRealBattleId();
        if (is_numeric($battleId)){
            $this->error_catch_log(['delete_events_by_batle_id'=>$battleId]);
            MatchBattleRoundEventCsgo::deleteAll(['battle_id'=>$battleId]);
        }
    }

    public function clearNoUseEventsByRound(){
        $battleId = $this->getRealBattleId();
        $roundOrdinal = $this->getIsUseCountRoundsByBattleId();
        if (is_numeric($battleId)&&is_numeric($roundOrdinal)){
            $this->error_catch_log(['delete_events_by_batle_id'=>$battleId]);
            MatchBattleRoundEventCsgo::deleteAll(['battle_id'=>$battleId,'round_ordinal'=>$roundOrdinal]);
        }
    }

    /**
     * 对局开始
     * 创建自定义order(1-live-1)
     * clear round order && set battle begin_at && clear round score
     * @param $data
     * @param $event
     * @return mixed
     */
    public function battle_start($data, $event)
    {
        //假的round
        if (empty($roundDetails['end_at'])) {
            $this->setCurrentRoundDetails(['is_use' => false]);
        }
        //create battle tag
        $current_order = $this->createBattleTagOnBattleStart();
        $this->setCurrentOrder($current_order);
        //clear round order && set battle begin_at
        $this->setRoundOrder(1);
        $this->setBattleBeginAt($event['info']['log_time']);//battle 开始时间
        $this->setBattleWinner('');//battle winner 重置
        //clear round score
        $this->clearRoundScore();
        //isUpComing
        $upComing = $this->getUpComing();
        if ($upComing) {
            //set is_live true
            $this->setIsLive(true);
            //set match status
            $this->setMatchStatus(self::STATUS_GOING);//match 状态
            //battle_order
            $this->setBattleIdByRealOrder();
            $now_battle_order = $this->getBattleOrder();

        } else {
            //设置试抢局的battle
            $this->setWarmUpBattleId();
        }
        //clear events
//        $this->clearNoUseEvents();
        //清除选手身上的武器
        $this->clearPlayerWeapons();

        //处理子事件get_goods
        $getGoodsList           = @$event['info']['ext_log']['get_goods'];
        $battle_game_commencing = @$event['info']['ext_log']['battle_game_commencing'];
        if ($getGoodsList) {
            $type = '';
            if ($battle_game_commencing) {
                $type = 'battle_game_commencing';
            }
            foreach ($getGoodsList as $k => $v) {
                $this->addRoundPlayerWeaponsOrGrenadesBySteamId($v['SteamID'], $v['goods'], $type);
            }
        }

        //大初始化
        $this->createSnapshot('battle');
        //查询战队信息
        if ($event['info']['ext_log']) {
            $ct_order     = $this->getTeamOrderByTeamName(@$event['info']['ext_log']['battle_team_playing_ct'][0]['log_team_name']);
            $t_order      = $this->getTeamOrderByTeamName(@$event['info']['ext_log']['battle_team_playing_t'][0]['log_team_name']);
            $team_ct      = $this->getTeamByName(@$event['info']['ext_log']['battle_team_playing_ct'][0]['log_team_name']);
            $team_t       = $this->getTeamByName(@$event['info']['ext_log']['battle_team_playing_t'][0]['log_team_name']);
            $battle_order = $this->getBattleOrder();

            //设置战队信息
            if ($team_t['team_id']) {
                $this->setTeamT($team_t);
                $this->setStartingT($team_t);
            }
            if ($team_ct['team_id']) {
                $this->setTeamCt($team_ct);
                $this->setStartingCt($team_ct);
            }

            $list = $this->getPlayerListByType('all');
            foreach ($list as $k => $v) {
                if ($v['side'] == 'ct') {
                    $team = $this->getCurrentCt();
                }
                if ($v['side'] == 'terrorist') {
                    $team = $this->getCurrentT();
                }
                $playerArr['team_log_name'] = @$team['log_name'];
                $playerArr['team_name']     = @$team['name'];
                $playerArr['team_id']       = @$team['team_id']?$team['team_id']:null;
                $playerArr['team_order']    = @$team['opponent_order'];
                $this->setPlayerBySteamId($v['steam_id'], $playerArr);
            }

        }
        //处理子事件round_time_frozen_start
        $round_time_frozen_start = @$event['info']['ext_log']['round_time_frozen_start'];
        if ($round_time_frozen_start) {
            //do first round details on battle'freeze exist
            $this->setRoundDetailsOnRoundStart();
        }
        //根据地图名称查询缓存中的地图信息
        $map = $this->findOneMapByName($event['info']['log_map_name']);
        //设置地图信息
        if (!empty($map)) {
            $data['map']['map_id']                   = (int)$map['id'];
            $data['map']['name']                     = $map['name'];
            $data['map']['name_cn']                  = $map['name_cn'];
            $data['map']['external_id']              = $map['external_id']?$map['external_id']:null;
            $data['map']['external_name']            = $map['external_name'];
            $data['map']['short_name']               = $map['short_name'];
            $data['map']['map_type']                 = $map['e_name'];
            $data['map']['map_type_cn']              = $map['c_name'];
            $data['map']['is_default']               = $map['is_default'] == 1;
            $data['map']['slug']                     = $map['slug'];
            $data['map']['image']['square_image']    = $map['square_image'];
            $data['map']['image']['rectangle_image'] = $map['rectangle_image'];
            $data['map']['image']['thumbnail']       = $map['thumbnail'];
        } else {
            $data['map']['map_id']                   = 0;
            $data['map']['name']                     = '';
            $data['map']['name_cn']                  = '';
            $data['map']['external_id']              = '';
            $data['map']['external_name']            = '';
            $data['map']['short_name']               = '';
            $data['map']['map_type']                 = '';
            $data['map']['map_type_cn']              = '';
            $data['map']['is_default']               = false;
            $data['map']['slug']                     = '';
            $data['map']['image']['square_image']    = '';
            $data['map']['image']['rectangle_image'] = '';
            $data['map']['image']['thumbnail']       = '';
        }
        //设置当前地图
        $this->setCurrentMap($data['map']);
        if (!$data['map']['map_id']){
            $data['map'] = null;
        }
        if ($team_ct['id']){
            //开局作为反恐精英
            $data['starting_ct']['team_id']        = @$team_ct['id']?(int)$team_ct['id']:null;
            $data['starting_ct']['name']           = $team_ct['name'];
            $data['starting_ct']['image']          = ImageConversionHelper::showFixedSizeConversion($team_ct['image'],200,200);
            $data['starting_ct']['opponent_order'] = @$ct_order;
        }else{
            $data['starting_ct'] = null;
        }

        if ($team_t['id']){
            //开局作为恐怖分子
            $data['starting_t']['team_id']        = @$team_t['id']?(int)$team_t['id']:null;
            $data['starting_t']['name']           = @$team_t['name'];
            $data['starting_t']['image']          = ImageConversionHelper::showFixedSizeConversion(@$team_t['image'],200,200);
            $data['starting_t']['opponent_order'] = @$t_order;
        }else{
            $data['starting_t'] = null;
        }

        //battle details
        $this->updateBattleDetailsOnBattleStart();

        return $data;
    }

    public function updateBattleDetailsOnBattleStart()
    {
        //set battle list
        $baseArr              = [];
        $baseArr['battle_id'] = $this->getBattleId();
        if (strstr($this->getBattleId(), 'live')) {
            $baseArr['is_use'] = true;
        } else {
            $baseArr['is_use'] = false;
        }
        $battleDetails = $this->getCurrentBattleDetails();
        if (empty($battleDetails) || empty($battleDetails['begin_at'])) {
            $baseArr['begin_at'] = $this->getBattleBeginAt();
        }
        $baseArr['real_battle_id']                          = $this->getRealBattleId();
        if (strstr($this->getBattleId(), 'live')) {
            $baseArr['is_live'] = true;
        } else {
            $baseArr['is_live'] = false;
        }

        $baseArr['real_order']                              = $this->getRealOrder();
        $baseArr['order']                                   = $this->getRealOrder();
        $baseArr['number']                                  = $this->getNumber();
        if (empty($battleDetails) || empty($battleDetails['begin_at'])) {
            $baseArr['status'] = $this->getMatchStatus();
        } else {
            if (!$battleDetails['begin_at']) {
                $baseArr['status'] = 2;
            }
        }
        $mapInfo                                            = $this->getCurrentMap();
        $baseArr['map_details']['map_id']                   = $mapInfo['map_id'];
        $baseArr['map_details']['name']                     = $mapInfo['name'];
        $baseArr['map_details']['name_cn']                  = $mapInfo['name_cn'];
        $baseArr['map_details']['external_id']              = $mapInfo['external_id'];
        $baseArr['map_details']['external_name']            = $mapInfo['external_name'];
        $baseArr['map_details']['short_name']               = $mapInfo['short_name'];
        $baseArr['map_details']['map_type']                 = $mapInfo['map_type'];
        $baseArr['map_details']['map_type_cn']              = $mapInfo['map_type_cn'];
        $baseArr['map_details']['is_default']               = $mapInfo['is_default'];
        $baseArr['map_details']['image']['square_image']    = $mapInfo['image']['square_image'];
        $baseArr['map_details']['image']['rectangle_image'] = $mapInfo['image']['rectangle_image'];
        $baseArr['map_details']['image']['thumbnail']       = $mapInfo['image']['thumbnail'];
        $start_ct                                           = $this->getStartingCt();
        $baseArr['start_ct']['team_id']                     = $start_ct['team_id'];
        $baseArr['start_ct']['opponent_order']              = $start_ct['opponent_order'];
        $baseArr['start_ct']['log_name']                    = @$start_ct['log_name'];
        $baseArr['start_ct']['name']                        = $start_ct['name'];
        $baseArr['start_ct']['image']                       = $start_ct['image'];
        $baseArr['start_ct']['score']                       = $this->getStartingCtScore();
        if (empty($battleDetails) || empty($battleDetails['start_ct']['1st_half_score'])) {
            $baseArr['start_ct']['1st_half_score'] = null;
        }
        if (empty($battleDetails) || empty($battleDetails['start_ct']['2nd_half_score'])) {
            $baseArr['start_ct']['2nd_half_score'] = null;
        }
        if (empty($battleDetails) || empty($battleDetails['start_ct']['ot_score'])) {
            $baseArr['start_ct']['ot_score'] = null;
        }
        $start_t                                            = $this->getStartingT();
        $baseArr['start_t']['team_id']                      = $start_t['team_id'];
        $baseArr['start_t']['opponent_order']               = $start_t['opponent_order'];
        $baseArr['start_t']['log_name']                     = @$start_t['log_name'];
        $baseArr['start_t']['name']                         = $start_t['name'];
        $baseArr['start_t']['image']                        = $start_t['image'];
        $baseArr['start_t']['score']                        = $this->getStartingTScore();
        if (empty($battleDetails) || empty($battleDetails['start_t']['1st_half_score'])) {
            $baseArr['start_t']['1st_half_score'] = null;
        }
        if (empty($battleDetails) || empty($battleDetails['start_t']['2nd_half_score'])) {
            $baseArr['start_t']['2nd_half_score'] = null;
        }
        if (empty($battleDetails) || empty($battleDetails['start_t']['ot_score'])) {
            $baseArr['start_t']['ot_score'] = null;
        }
        //team ct
        $team_ct                         = $this->getCurrentCt();
        $baseArr['ct']['team_id']        = $team_ct['team_id'];
        $baseArr['ct']['opponent_order'] = $team_ct['opponent_order'];
        $baseArr['ct']['log_name']       = @$team_ct['log_name'];
        $baseArr['ct']['name']           = $team_ct['name'];
        $baseArr['ct']['image']          = $team_ct['image'];
        //team t
        $team_t                                 = $this->getCurrentT();
        $baseArr['terrorist']['team_id']        = $team_t['team_id'];
        $baseArr['terrorist']['opponent_order'] = $team_t['opponent_order'];
        $baseArr['terrorist']['log_name']       = @$team_t['log_name'];
        $baseArr['terrorist']['name']           = $team_t['name'];
        $baseArr['terrorist']['image']          = $team_t['image'];
        //init in_round_timestamp round_time is_bomb_planted time_since_plant
        $this->setRoundBeginAt($this->event['info']['log_time']);
        $this->setBombPlanted(false);
        return $this->setCurrentBattleDetails($baseArr);
    }

    /**
     * clearPlayerWeapons
     */
    public function clearPlayerWeapons()
    {
        $weaponKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $weapons   = $this->hGetAll($weaponKey);
        foreach ($weapons as $k => $v) {
            $this->hSet($weaponKey, $k, json_encode([],320));
        }
        $grenadeKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];
        $grenades   = $this->hGetAll($grenadeKey);
        foreach ($grenades as $k => $v) {
            $this->hSet($grenadeKey, $k, json_encode([],320));
        }
        $weaponRoundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $weaponsRound   = $this->hGetAll($weaponRoundKey);
        foreach ($weaponsRound as $k => $v) {
            $this->hSet($weaponRoundKey, $k, json_encode([],320));
        }
        $grenadeRoundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];;
        $grenadeRound = $this->hGetAll($grenadeRoundKey);
        foreach ($grenadeRound as $k => $v) {
            $this->hSet($grenadeRoundKey, $k, json_encode([],320));
        }
        return true;
    }
    /**
     * 设置ct
     * @param $detalis
     */
    public function setTeamCt($detalis)
    {
        //update battle starting_faction_side
        $startingCt = $this->getStartingCt();
        if ($startingCt) {
            $baseArr = [];
            if ($startingCt['opponent_order'] == $detalis['opponent_order']) {
                $baseArr['ct']['starting_faction_side'] = 'ct';
            } else {
                $baseArr['ct']['starting_faction_side'] = 'terrorist';
            }
            $this->setCurrentBattleDetails($baseArr);
        }
        $baseArr = [];
        $baseArr['ct']['team_id']        = $detalis['team_id'];
        $baseArr['ct']['opponent_order'] = $detalis['opponent_order'];
        $baseArr['ct']['log_name']       = @$detalis['log_name'];
        $baseArr['ct']['name']           = $detalis['name'];
        $baseArr['ct']['image']          = $detalis['image'];
        $this->setCurrentBattleDetails($baseArr);
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_CT, self::KEY_TAG_DETAILS], json_encode($detalis,320));
    }

    /**
     * 设置ct
     * @param $detalis
     */
    public function setTeamT($detalis)
    {
        //update battle starting_faction_side
        $startingT = $this->getStartingT();
        if ($startingT) {
            $baseArr = [];
            if ($startingT['opponent_order'] == $detalis['opponent_order']) {
                $baseArr['terrorist']['starting_faction_side'] = 'terrorist';
            } else {
                $baseArr['terrorist']['starting_faction_side'] = 'ct';
            }
            $this->setCurrentBattleDetails($baseArr);
        }
        $baseArr = [];
        $baseArr['terrorist']['team_id']        = $detalis['team_id'];
        $baseArr['terrorist']['opponent_order'] = $detalis['opponent_order'];
        $baseArr['terrorist']['log_name']       = @$detalis['log_name'];
        $baseArr['terrorist']['name']           = $detalis['name'];
        $baseArr['terrorist']['image']          = $detalis['image'];
        $this->setCurrentBattleDetails($baseArr);
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_T, self::KEY_TAG_DETAILS], json_encode($detalis,320));
    }

    public function setBattleEndAt($time)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_END_AT], $time);//battle 结束时间
        $baseArr['end_at'] = $time;
        return $this->setCurrentBattleDetails($baseArr);
    }

    public function setBattleStatus($value)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_STATUS], $value);//battle 结束状态
        $baseArr['status'] = $value;
        return $this->setCurrentBattleDetails($baseArr);
    }

    public function setBattleWinner($value, $side = '')
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_WINNER], json_encode($value,320));
        if ($side){
            if ($side == 'ct') {
                $team = $this->getCurrentCt();
            } else {
                $team = $this->getCurrentT();
            }
            //set battle details
            $baseArr['winner']['side']           = $side;
            $baseArr['winner']['opponent_order'] = $team['opponent_order'];
            return $this->setCurrentBattleDetails($baseArr);
        }

    }

    public function setBattleUpComing($value){
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], $value);
    }

    /**
     * 对局结束
     * set live=false upcoming=false status
     * set battle_winner
     * set match_score
     * @param $data
     * @param $event
     * @return mixed
     */
    public function battle_end($data, $event)
    {
        $this->temp_battle_id = $this->getOutPutBattleId();
        $this->temp_battle_order = $this->getOutPutBattleOrder();
        $this->temp_round_ordinal = $this->getIsUseCountRoundsByBattleId();

        //update kills
        $this->updateKillsOnFrozen();
        //update kast
        $this->updateKastOnRoundEnd();
        //update rating
        $this->updateRatingOnRoundEnd();
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct');
        $this->setBattlePlayerStatisOnBattleEnd('t');
        $this->temp_live = true;
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_LIVE], false);
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BATTLE_UP_COMING], false);
        $this->setBattleStatus(self::STATUS_END);
        $this->setBattleEndAt($event['info']['log_time']);
        //重置当前round order
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL], 1);
        //根据event info ext_log round_win_type 0 log_win_camp获得阵营(ct/t),再获取阵营中的team信息
        $currentCtScore = $this->getCtScore();
        $currentTerroristScore = $this->getTScore();
        if ($currentCtScore > $currentTerroristScore) {
            $winnerTeamCamp = 'ct';
        } elseif ($currentTerroristScore > $currentCtScore) {
            $winnerTeamCamp = 'terrorist';
        }
//        $winnerTeamCamp = $event['info']['ext_log']['round_win_type'][0]['log_win_camp'];
        $winnerTeamInfo = $this->getTeamInfoBySide($winnerTeamCamp);
        //is_draw
        if ($currentTerroristScore == $currentCtScore) {
            $baseBattleArr            = [];
            $baseBattleArr['is_draw'] = true;
            $this->setCurrentBattleDetails($baseBattleArr);
        }
        //save battle winner
        $data['winner'] = $winnerTeamInfo;
        $data['winner']['team_id'] = (int)$winnerTeamInfo['team_id'];
        $data['winner']['image'] = ImageConversionHelper::showFixedSizeConversion(@$winnerTeamInfo['image'],200,200);
        $this->setBattleWinner($winnerTeamInfo, $winnerTeamCamp);
        //获取比赛的大比分
        $match_team_1_score = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE]);//比分
        $match_team_2_score = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]);//比分
        if ($winnerTeamInfo['opponent_order'] == 1) {//队伍1
            $match_team_1_score += 1;
        } elseif ($winnerTeamInfo['opponent_order'] == 2) {//队伍2
            $match_team_2_score += 1;
        }
        //获取比赛信息
        $battleCount = $this->match['number_of_games'];
        //$battleCount = $MatchInfo['number_of_games'];
        $winnerInfo = WinnerHelper::csgoWinnerInfo($match_team_1_score, $match_team_2_score, $battleCount);
        //计算比赛结束
        if ($winnerInfo['is_finish'] == 1) {
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_STATUS], self::STATUS_END);//match 状态
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT], $event['info']['log_time']);//match 开始时间
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_WINNER], json_encode($winnerTeamInfo,320));
        } elseif ($winnerInfo['is_draw'] == 1) {
            $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_IS_DRAW], true);
        }
        $this->setMatch1Score($match_team_1_score);
        $this->setMatch2Score($match_team_2_score);

        return $data;
    }

    public function setMatchBeginAt($value){
        $this->setnx([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BEGIN_AT],$value);
    }

    public function setMatchEndAt($value){
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT],$value);
    }

    public function setMatchWinner($winner){
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_WINNER], json_encode($winner,320));
    }

    /**
     * 回合结束方式：（正常）
     * 时间、获胜方、获胜方式、比分
     * @param $data
     * @return mixed
     */
    public static $firstTo5RoundsScore = [
        'ct'        => 0,
        'terrorist' => 0
    ];

    /**
     * round_win_type
     * update round details
     * win_round_1_side first_to_5_rounds_wins win_round_16_side
     * 1st_half_score  2nd_half_score ot_score
     * @param $data
     * @param $event
     * @return mixed
     */
    public function round_win_type($data, $event)
    {
        $round_order = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL]);
        //更新回合结束方式
        return $data;
    }


    public function setRoundWinType($value)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_WIN_TYPE], $value);
        //update round details
        $baseArr['round_win_type'] = $value;
        return $this->setCurrentRoundDetails($baseArr);
    }

    public function setRoundWinSide($value)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_WIN_SIDE], $value);
        //update round details
        $baseArr['winner_side'] = $value;
        return $this->setCurrentRoundDetails($baseArr);
    }

    public function setCurrentRoundDetails($updateArr = [])
    {
        $res = $this->getCurrentRoundDetails();
        if (!$res) {
            $res = $this->getRoundBase();
        }
        if (array_key_exists('ct', $updateArr)) {
            $updateArr['ct'] = array_merge($res['ct'], $updateArr['ct']);
        }
        if (array_key_exists('terrorist', $updateArr)) {
            $updateArr['terrorist'] = array_merge($res['terrorist'], $updateArr['terrorist']);
        }
        $new_res = $res ? array_merge($res, $updateArr) : $updateArr;
        $key     = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
        return $this->hSet($key, $this->getRoundOrdinal(), json_encode($new_res,320));
    }

    public function setRoundDetails($updateArr = [],$roundOrder)
    {
        if (!$roundOrder){
            return false;
        }else{
            $res = $this->getRoundDetailsByOrder($roundOrder);
        }


        if (!$res) {
            $res = $this->getRoundBase();
        }
        if (array_key_exists('ct', $updateArr)) {
            $updateArr['ct'] = array_merge($res['ct'], $updateArr['ct']);
        }
        if (array_key_exists('terrorist', $updateArr)) {
            $updateArr['terrorist'] = array_merge($res['terrorist'], $updateArr['terrorist']);
        }
        if (array_key_exists('1vn_ct_player', $updateArr)) {
            if (empty($res['1vn_ct_player'])){
                $res['1vn_ct_player'] = [];
            }
            $updateArr['1vn_ct_player'] = array_merge($res['1vn_ct_player'], $updateArr['1vn_ct_player']);
        }
        if (array_key_exists('1vn_t_player', $updateArr)) {
            if (empty($res['1vn_t_player'])){
                $res['1vn_t_player'] = [];
            }
            $updateArr['1vn_t_player'] = array_merge($res['1vn_t_player'], $updateArr['1vn_t_player']);
        }
        if (array_key_exists('1vn_ct_player_final', $updateArr)) {
            if (empty($res['1vn_ct_player_final'])){
                $res['1vn_ct_player_final'] = [];
            }
            $updateArr['1vn_ct_player_final'] = array_merge($res['1vn_ct_player_final'], $updateArr['1vn_ct_player_final']);
        }
        if (array_key_exists('1vn_t_player_final', $updateArr)) {
            if (empty($res['1vn_t_player_final'])){
                $res['1vn_t_player_final'] = [];
            }
            $updateArr['1vn_t_player_final'] = array_merge($res['1vn_t_player_final'], $updateArr['1vn_t_player_final']);
        }
        $new_res = $res ? array_merge($res, $updateArr) : $updateArr;
        $key     = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
        return $this->hSet($key, $roundOrder, json_encode($new_res,320));
    }


    /**
     * 暂停
     * @param $data
     * @return mixed
     */
    public function battle_pause($data,$event)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_PAUSE], true);
        return $data;
    }

    public function setBattlePause($value){
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_PAUSE], $value);
    }

    /**
     * 解除暂停
     * @param $data
     * @return mixed
     */
    public function battle_unpause($data, $event)
    {
        //暂停结束,设置状态为going
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_IS_PAUSE], false);
        return $data;
    }

    /**
     * 回合结束方式：（平局）
     * 1.10个人集体掉线
     * 2.回档会发生
     * 这里不做处理,在round_end处理
     * @param $data
     * @param $event
     * @return mixed
     */
    public function round_draw($data, $event)
    {
        return $data;
    }

    /**
     * 冻结时间开始
     * incr round_order
     * set freeze true
     * init player
     * add round details
     * @param $data
     * @return mixed
     */
    public function round_time_frozen_start($data, $event)
    {
        //update kills
        $this->updateKillsOnFrozen();
        //update rating
        $this->updateRatingOnRoundEnd();
        //update kast
        $this->updateKastOnRoundEnd();
        //statis
        $this->setRoundPlayerStatisOnRoundEnd('ct');
        $this->setRoundPlayerStatisOnRoundEnd('t');
        //statis
        $this->setBattlePlayerStatisOnBattleEnd('ct');
        $this->setBattlePlayerStatisOnBattleEnd('t');
        $roundDetails = $this->getCurrentRoundDetails();
        //假的round
        if (empty($roundDetails['end_at'])) {
            $this->setCurrentRoundDetails(['is_use' => false]);
        }
        $this->incr([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL]);//回合序号
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT], $event['info']['log_time']);//回合开始时间
        $this->setFreezeTime(true);
        //初始化
        $this->createSnapshot('round');
        //update round survived_players
        $this->updateRoundSurvived();
        //update round details
        $this->setRoundDetailsOnRoundStart();
        //set round weapon
        $this->setRoundWeaponOnFrozenStart();
        //flush adr
        try {
            $this->flushPlayerAdrOnFrozenStart();
        }catch(Throwable $e){
            $this->error_catch_log(['action'=>'round_time_frozen_start','msg'=>$e->getMessage()]);
        }
        //set is_bomb_planted false
        $this->setBombPlanted(false);
        $this->setTimeSincePlant('');
        return $data;
    }

    public function updateKillsOnFrozen(){
        $roundPlayerList = $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER]);
        if (!empty($roundPlayerList)) {
            foreach ($roundPlayerList as $k => $v) {
                $roundPlayer = json_decode($v,true);
//                $roundPlayer = $this->getRoundPlayerBySteamId($k);
                if (!empty($roundPlayer)) {
                    if ($roundPlayer['kills'] == 1){
                        $updateArrStatistics= [];
                        $updateArrStatistics[] = 'one_kills';
                        $this->setPlayerBySteamId($k, $updateArrStatistics, 'incr');
                        continue;
                    }
                    if ($roundPlayer['kills'] >= 2 && $roundPlayer['kills'] <= 4) {
                        $updateArrStatistics= [];
                        //status fields two_kills
                        $updateArrStatistics[] = 'multi_kills';
                        if ($roundPlayer['kills'] == 2) {
                            $updateArrStatistics[] = 'two_kills';
                            $this->setPlayerBySteamId($k, $updateArrStatistics, 'incr');
                            continue;
                        } elseif ($roundPlayer['kills'] == 3) {
                            $updateArrStatistics[] = 'three_kills';
                            $this->setPlayerBySteamId($k, $updateArrStatistics, 'incr');
                            continue;
                        } elseif ($roundPlayer['kills'] == 4) {
                            $updateArrStatistics[] = 'four_kills';
                            $this->setPlayerBySteamId($k, $updateArrStatistics, 'incr');
                            continue;
                        }
                    }
                }
            }
        }
    }


    public function setRoundWeaponOnFrozenStart(){
        $weapon_list                          = $this->getCurrentPlayerWeaponList();
        $this->hMset([self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$this->getBattleId(),self::KEY_TAG_ROUND,$this->getRoundOrdinal(),self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS],$weapon_list);
    }

    public function getCurrentPlayerWeaponList(){
        return $this->hGetAll([self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS]);
    }

    public function flushPlayerAdrOnFrozenStart()
    {

        $battlePlayerList = $this->getBattlePlayerListByType('all');
        if (!empty($battlePlayerList)) {
            foreach ($battlePlayerList as $k => $v) {
                if (!empty($playerInfoArr)||$this->getIsUseCountRoundsByBattleId()!=0) {
                    $playerInfoArr = $this->getBattlePlayerBySteamId($v['steam_id']);
                    $playerInfoArr['adr'] = round(@$playerInfoArr['real_damage'] / $this->getIsUseCountRoundsByBattleId(), 2);
                    $this->setBattlePlayerBySteamId($v['steam_id'], $playerInfoArr);
                }
            }
        }
        return true;
    }

    public function updateRoundSurvived()
    {
        $updateArr['survived_players_ct'] = $this->getSurvivedPlayersCountByType('ct');;
        $updateArr['survived_players_t'] = $this->getSurvivedPlayersCountByType('t');;
        $this->setCurrentRoundDetails($updateArr);
        return $updateArr;
    }

    public function createSnapshot($type)
    {
        //初始化
        $ct_player_list = $this->getPlayerListByType('ct');
        foreach ($ct_player_list as $k => $v) {
            $v = $this->initPlayerBase($v);
            $this->setRoundPlayerBySteamId($v['steam_id'], $v);
            if ($type == 'round') {
                $battleStatusArr['hp'] = 100;
//                $battleStatusArr['has_kevlar']    = false;//
//                $battleStatusArr['has_helmet']    = false;//
//                $battleStatusArr['has_defusekit'] = false;//
                $battleStatusArr['has_bomb']      = false;//
                $battleStatusArr['is_alive']     = true;//
                $battleStatusArr['blinded_time'] = 0;//
                $this->setBattlePlayerBySteamId($v['steam_id'], $battleStatusArr);
                $this->setCurrentPlayerBySteamId($v['steam_id'], $battleStatusArr);
            }
            if ($type == 'battle') {
                $this->setBattlePlayerBySteamId($v['steam_id'], $v);
            }

        }
        $t_player_list = $this->getPlayerListByType('t');
        foreach ($t_player_list as $k => $v) {
            $v = $this->initPlayerBase($v);
            $this->setRoundPlayerBySteamId($v['steam_id'], $v);
            if ($type == 'round') {
                $battleStatusArr['hp']            = 100;
//                $battleStatusArr['has_kevlar']    = false;//
//                $battleStatusArr['has_helmet']    = false;//
//                $battleStatusArr['has_defusekit'] = false;//
                $battleStatusArr['has_bomb']      = false;//
                $battleStatusArr['is_alive']      = true;//
                $battleStatusArr['blinded_time']  = 0;//
                $this->setBattlePlayerBySteamId($v['steam_id'], $battleStatusArr);
                $this->setCurrentPlayerBySteamId($v['steam_id'], $battleStatusArr);
            }
            if ($type == 'battle') {
                $this->setBattlePlayerBySteamId($v['steam_id'], $v);
            }
        }
    }

    /**
     * 获得C4
     * @param $data
     * @return mixed
     */
    public function get_c4($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['SteamID']);
        //更新这个用户拥有c4
        $this->setPlayerBySteamId($event['info']['SteamID'], ['has_bomb' => true]);
        return $data;
    }

    /**
     * 失去C4
     * @param $data
     * @return mixed
     */
    public function lost_c4($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['steamId']);
        //更新这个用户失去c4
        $this->setPlayerBySteamId($event['info']['steamId'] ?? $event['info']['SteamID'], ['has_bomb' => false]);
        return $data;
    }

    /**
     * 更新选手武器
     * @param $steamId
     * @param $weapon
     * @param string $event_type
     * @return bool
     */
    public function addRoundPlayerWeaponsBySteamId($steamId, $weapon, $event_type = '')
    {
        $weapons = $this->getRoundPlayerWeaponBySteamId($steamId);
        if (!empty($weapons)) {
            $flag = 0;
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    $flag++;
                }
            }
            if ($flag == 0 || ($flag < 2 && strstr($weapon, 'flashbang'))) {
                $weapons[] = $weapon;
            }
        } else {
            $weapons[] = $weapon;
        }
        $weapons_json = json_encode($weapons,320);
        //更新选手武器(加)
        $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId, $weapons_json);
        //current weapon
        $this->addCurrentPlayerWeaponsBySteamId($steamId, $weapon);
        return true;
    }

    /**
     * 更新选手武器
     * @param $steamId
     * @param $weapon
     * @param string $event_type
     */
    public function addCurrentPlayerWeaponsBySteamId($steamId, $weapon, $event_type = '')
    {
        //更新选手武器(加)
        $weapons = $this->getCurrentPlayerWeaponsBySteamId($steamId);
        if (!empty($weapons)) {
            $flag = 0;
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    $flag++;
                }
            }
            if ($flag == 0 || ($flag < 2 && strstr($weapon, 'flashbang'))) {
                $weapons[] = $weapon;
            }
        } else {
            $weapons[] = $weapon;
        }
        $weapons_json = json_encode($weapons,320);
        //更新选手武器(加)
        $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId, $weapons_json);
    }

    /**
     * 更新选手投掷物
     * @param $steamId
     * @param $weapon
     * @param string $event_type
     */
    public function addCurrentPlayerGrenadesBySteamId($steamId, $weapon, $event_type = '')
    {
        $weapons = $this->getCurrentPlayerGrenadesBySteamId($steamId);
        if (!empty($weapons)) {
            $flag = 0;
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    $flag++;
                }
            }
            if ($flag == 0 || ($flag < 2 && strstr($weapon, 'flashbang'))) {
                $weapons[] = $weapon;
            }
        } else {
            $weapons[] = $weapon;
        }
        $weapons_json = json_encode($weapons,320);
        //更新选手武器(加)
        $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId, $weapons_json);
    }

    /**
     * 更新选手投掷物
     * @param $steamId
     * @param $weapon
     * @return mixed
     */
    public function getCurrentPlayerGrenadesBySteamId($steamId)
    {
        $grenades = $this->hGet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId);
        return json_decode($grenades, true);
    }

    /**
     * 更新选手投掷物
     * @param $steamId
     * @param $weapon
     * @return mixed
     */
    public function getCurrentPlayerWeaponsBySteamId($steamId)
    {
        $grenades = $this->hGet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId);
        return json_decode($grenades, true);
    }

    /**
     * 更新选手投掷物
     * @param $steamId
     * @param $weapon
     * @param string $event_type
     * @return string|void
     */
    public function addRoundPlayerGrenadesBySteamId($steamId, $weapon, $event_type = '')
    {
        $this->addCurrentPlayerGrenadesBySteamId($steamId, $weapon);
        $weapons = $this->getRoundPlayerGrenadesBySteamId($steamId);
        if (!empty($weapons)) {
            $flag = 0;
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    $flag++;
                }
            }
            if ($flag == 0 || ($flag < 2 && strstr($weapon, 'flashbang'))) {
                $weapons[] = $weapon;
            }
        } else {
            $weapons[] = $weapon;
        }
        $weapons_json = json_encode($weapons,320);
        return $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId, $weapons_json);
    }

    /**
     * 更新Order选手武器或者投掷物(加)
     * @param $steamId
     * @param $weapon
     * @param string $event_type
     * @return string|void
     */
    public function addRoundPlayerWeaponsOrGrenadesBySteamId($steamId, $weapon, $event_type = '')
    {
        $weapon_info = $this->getWeaponByName($weapon);
        if ($weapon_info || strstr($weapon,'kevlar')||strstr($weapon,'helmet')||strstr($weapon,'defuser')||strstr($weapon,'vesthelm')) {
            if ($weapon_info){
                if ($weapon_info['kind'] == 1 || $weapon_info['kind'] == 2) {
                    //武器
                    return $this->addRoundPlayerWeaponsBySteamId($steamId, $weapon, $event_type);
                } elseif ($weapon_info['kind'] == 4) {
                    //投掷物
                    return $this->addRoundPlayerGrenadesBySteamId($steamId, $weapon, $event_type);
                }
            }

            //防弹衣
            if (strstr($weapon, 'kevlar')) {
                $this->setPlayerBySteamId($steamId, ['has_kevlar' => true]);
            }
            // 头盔
            if (strstr($weapon, 'helmet')) {
                $this->setPlayerBySteamId($steamId, ['has_helmet' => true]);
            }
            // 拆弹器
            if (strstr($weapon, 'defuser')) {
                $this->setPlayerBySteamId($steamId, ['has_defusekit' => true]);
            }
            //头盔+防弹衣
            if (strstr($weapon, 'vesthelm')) {
                $this->setPlayerBySteamId($steamId, ['has_kevlar' => true]);
                $this->setPlayerBySteamId($steamId, ['has_helmet' => true]);
            }
        }

    }

    /**
     * 更新当前选手武器或者投掷物(加)
     * @param $steamId
     * @param $weapon
     * @return string|void
     */
    public function addCurrentPlayerWeaponsOrGrenadesBySteamId($steamId, $weapon)
    {
        $weapon_info = $this->getWeaponByName($weapon);
        if ($weapon_info) {
            if ($weapon_info['kind'] == 1 || $weapon_info['kind'] == 2) {
                //武器
                return $this->addCurrentPlayerWeaponsBySteamId($steamId, $weapon);
            } elseif ($weapon_info['kind'] == 4) {
                //投掷物
                return $this->addCurrentPlayerGrenadesBySteamId($steamId, $weapon);
            }

        }

    }

    /**
     * 更新选手武器或者投掷物(减)
     * @param $steamId
     * @param $weapo
     * @return string|void
     */
    public function remRoundPlayerWeaponsOrGrenadesBySteamId($steamId, $weapon)
    {
        $weapon_info = $this->getWeaponByName($weapon);
        if ($weapon_info) {
            if ($weapon_info['kind'] == 1 || $weapon_info['kind'] == 2) {
                //武器
                return $this->remRoundPlayerWeaponsBySteamId($steamId, $weapon);
            } elseif ($weapon_info['kind'] == 4) {
                //投掷物
                return $this->remRoundPlayerGrenadesBySteamId($steamId, $weapon);
            }
            //防弹衣
            if (strstr($weapon, 'kevlar')) {
                $this->setPlayerBySteamId($steamId, ['has_kevlar' => false]);
            }
            // 头盔
            if (strstr($weapon, 'helmet')) {
                $this->setPlayerBySteamId($steamId, ['has_helmet' => false]);
            }
            // 拆弹器
            if (strstr($weapon, 'defuser')) {
                $this->setPlayerBySteamId($steamId, ['has_defusekit' => false]);
            }
            //头盔+防弹衣
            if (strstr($weapon, 'vesthelm')) {
                $this->setPlayerBySteamId($steamId, ['has_kevlar' => false]);
                $this->setPlayerBySteamId($steamId, ['has_helmet' => false]);
            }
        }

    }

    /**
     *减武器
     * @param $steamId
     * @param $weapon
     * @return string|void
     */
    public function remRoundPlayerWeaponsBySteamId($steamId, $weapon)
    {
        //current
        $this->remCurrentPlayerWeaponsBySteamId($steamId, $weapon);
        //更新选手武器(减)
        $weapons = $this->getCurrentPlayerWeaponsBySteamId($steamId);
        if ($weapons) {
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    unset($weapons[$k]);
                    break;
                }
            }
            $weapons = array_values($weapons);
        }

        $weapons_json = json_encode($weapons,320);
        return $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId, $weapons_json);

    }

    /**
     *减武器(current)
     * @param $steamId
     * @param $weapon
     * @return string|void
     */
    public function remCurrentPlayerWeaponsBySteamId($steamId, $weapon)
    {
        $weapons = $this->getCurrentPlayerWeaponsBySteamId($steamId);
        if ($weapons) {
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    unset($weapons[$k]);
                    break;
                }
            }
            $weapons = array_values($weapons);
        }

        $weapons_json = json_encode($weapons,320);
        return $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS], $steamId, $weapons_json);
    }

    /**
     *(current)
     * @param $steamId
     * @param $weapon
     * @return string|void
     */
    public function remCurrentPlayerGrenadesBySteamId($steamId, $weapon)
    {
        $weapons = $this->getCurrentPlayerGrenadesBySteamId($steamId);
        if (!empty($weapons)) {
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    unset($weapons[$k]);
                    break;
                }
            }
            $weapons = array_values($weapons);
        }

        $weapons_json = json_encode($weapons,320);
        return $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId, $weapons_json);
    }

    /**
     *投掷物
     * @param $steamId
     * @param $weapon
     * @return string|void
     */
    public function remRoundPlayerGrenadesBySteamId($steamId, $weapon)
    {
        //current
        $this->remCurrentPlayerGrenadesBySteamId($steamId, $weapon);
        //更新选手武器(减)
        $weapons = $this->getRoundPlayerGrenadesBySteamId($steamId);

        if (!empty($weapons)) {
            foreach ($weapons as $k => $v) {
                if (strstr($v, $weapon) || strstr($weapon, $v)) {
                    unset($weapons[$k]);
                    break;
                }
            }
            $weapons = array_values($weapons);
        }

        $weapons_json = json_encode($weapons,320);
        return $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES], $steamId, $weapons_json);

    }


    /**
     * 获得物品
     * update warmUp player'is_alive ture && hp up to 100
     * @param $data
     * @param $event
     * @return mixed
     */
    public function get_goods($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['SteamID']);
        //update warmUp player'is_alive ture && hp up to 100
        if (!$this->isLive()) {
            $this->setPlayerBySteamId($event['info']['SteamID'], ['is_alive' => true, 'hp' => 100]);
        }
        //更新选手装备(加)
        $this->addRoundPlayerWeaponsOrGrenadesBySteamId($event['info']['SteamID'], $event['info']['goods']);
        return $data;
    }

    /**
     * 购买物品 （如果存在子事件，则不处理购买物品主事件的武器装备新增）
     * boot 按照 name加物品
     * 非boot 按照steamId加
     * @param $data
     * @return mixed
     */
    public function purchase_goods($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['SteamID']);
        $flag = false;
        if (empty($event['info']['ext_log']['get_goods'])) {
            //update player goods(add)
            $this->addRoundPlayerWeaponsOrGrenadesBySteamId($event['info']['SteamID'], $event['info']['goods']);
        }

        return $data;
    }

    /**
     * 失去物品
     * @param $data
     * @return mixed
     */
    public function lost_goods($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['SteamID']);
        //更新选手武器(减)
        $this->remRoundPlayerWeaponsOrGrenadesBySteamId($event['info']['SteamID'], $event['info']['goods']);
        return $data;
    }

    /**
     * 投掷物品
     * @param $data
     * @return mixed
     */
    public function throw_goods($data, $event)
    {
        //不处理此事件,处理lost_goods
        return $data;
    }

    /**
     * 金钱变化
     * 事件类型：主事件（时间、选手昵称、Ping、Steam ID、阵营、变化前的金钱、增/减、金钱变化量、变化后的金钱、购买的武器道具（不一定存在））
     * @param $data
     * @return mixed
     */
    public function money_change($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['SteamID']);
        $this->setPlayerBySteamId($event['info']['SteamID'], ['money' => $event['info']['now_money']]);
        return $data;
    }

    /**
     * 小回档
     * @param null $origin
     */
    public function littleReload($origin=null)
    {
        //clear events
        $this->clearNoUseEventsByRound();
        if ($origin!='hltv'){
            $this->setCurrentRoundDetails(['is_use' => false]);
        }

        if ($origin=='hltv'){

            //清除hltv多余events
            //hltv_ws:47640:history:battle:1_live_1:round:3:main_events
            $roundEventKey = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$this->getBattleId(),self::KEY_TAG_ROUND,$this->getRoundOrdinal(),self::KEY_TAG_MAIN_EVENTS];
            $events = $this->lRange($roundEventKey,0,-1);
            if (!empty($events)){
                $revEvents = array_reverse($events);
                $trim_flag = false;
                foreach ($revEvents as $k=>$v){
                    $vArr = json_decode($v,true);
                    if ($vArr['event_type']=='round_end'&&empty($trim_flag)){
                        $trim_end = $k;
                        $trim_flag = true;
                    }
                    if($trim_flag){
                        $removeEventKey  = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$this->getBattleId(),self::KEY_TAG_ROUND,$this->getRoundOrdinal(),self::KEY_TAG_REMOVE_EVENTS];
                        $this->lPush($removeEventKey,$v);
                    }

                }
                $trimKey = $this->keyString($roundEventKey);
                if ($trim_end){
                    $len = $this->redis->lLen($trimKey);
                    $real_trim_end = $len-$trim_end-1;
                    $this->redis->lTrim($trimKey,$real_trim_end,-1);
                }
                $killEventsKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_KILL_EVENTS];
                $killEventList = $this->lRange($killEventsKey,0,-1);

                //重新计算hltv小回档的击杀数,死亡数,和存活状态
                if (!empty($killEventList)){
                    $updateArr['kills'] = 0;
                    $updateArr['death'] = 0;
                    $updateArr['ct']['kills'] = 0;
                    $updateArr['ct']['deaths'] = 0;
                    $updateArr['terrorist']['kills'] = 0;
                    $updateArr['terrorist']['deaths'] = 0;
                    $this->setCurrentRoundDetails($updateArr);
                    $roundPlayer = $this->getRoundPlayersByRoundOrderAndBattleId($this->getBattleId(),$this->getRoundOrdinal());
                    foreach ($roundPlayer as $k=>$v){
                        $this->setPlayerBySteamId($k,['is_alive'=>true]);
                    }
                    $updateRound = [];
                    $updateRound['kills'] =0;
                    $updateRound['ct_kills'] =0;
                    $updateRound['ct']['kills'] =0;
                    $updateRound['terrorist_kills'] =0;
                    $updateRound['terrorist']['kills'] =0;

                    $updateRound['deaths'] =0;
                    $updateRound['ct']['deaths'] =0;
                    $updateRound['terrorist']['deaths'] =0;
                    foreach ($killEventList as $k=>$v){
                        $v = json_decode($v,true);
                        $player_kill_details = $this->getRoundPlayerBySteamId($v['killer_steam_id']);
                        $killer_side                          = $player_kill_details['side'];
                        $updateRound[$killer_side . '_kills'] ++ ;
                        $updateRound['kills']                 ++ ;
                        $updateRound[$killer_side]['kills']   ++ ;
                        $player_dead_details = $this->getRoundPlayerBySteamId($v['victim_steam_id']);
                        $dead_side                          = $player_dead_details['side'];
                        $updateRound['deaths']                 ++;
                        $updateRound[$dead_side]['deaths']      ++;
                        $this->setPlayerBySteamId($v['victim_steam_id'],['is_alive'=>false]);
                        $this->setCurrentRoundDetails($updateRound);

                    }

                }

            }


        }

        $this->incr([self::KEY_TAG_RELOAD,self::KEY_TAG_LITTLE_ORDER]);
        try {
            $this->reSetBattleDataOnReload();
        }catch(Throwable $e){
            $this->error_catch_log(['msg'=>$e->getMessage(),'action'=>'littleReload']);
        }

    }

    public function reSetBattleDataOnReload()
    {
        $id               = $this->getBattleId();
        $jsonIds          = $this->getLiveRoundsId($id);
        $newUpdate        = [];
        $liveRounds       = json_decode($jsonIds, true);
        $liveRoundsPlayer = [];
        if (!empty($liveRounds)) {
            foreach ($liveRounds as $k => $v) {
                $liveRoundsPlayer[] = $this->getRoundPlayersByRoundOrderAndBattleId($id, $v);
            }
        }
        if (!empty($liveRoundsPlayer)) {
            foreach ($liveRoundsPlayer as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    if (!array_key_exists($k1, $newUpdate)) {
                        $newUpdate[$k1]['kills']             = 0;
                        $newUpdate[$k1]['headshot_kills']    = 0;
                        $newUpdate[$k1]['deaths']            = 0;
                        $newUpdate[$k1]['k_d_diff']          = 0;
                        $newUpdate[$k1]['assists']           = 0;
                        $newUpdate[$k1]['flash_assist']      = 0;
                        $newUpdate[$k1]['first_kills']       = 0;
                        $newUpdate[$k1]['first_deaths']      = 0;
                        $newUpdate[$k1]['first_kills_diff']  = 0;
                        $newUpdate[$k1]['multi_kills']       = 0;
                        $newUpdate[$k1]['one_on_x_clutches'] = 0;
                        $newUpdate[$k1]['knife_kill']        = 0;
                        $newUpdate[$k1]['ace_kill']          = 0;
                        $newUpdate[$k1]['damage']            = 0;
                        $newUpdate[$k1]['real_damage']       = 0;

                        $newUpdate[$k1]['kast']       = 0;
                        $newUpdate[$k1]['team_damage']       = 0;
                        $newUpdate[$k1]['damage_taken']       = 0;
                        $newUpdate[$k1]['hegrenade_damage_taken']       = 0;
                        $newUpdate[$k1]['inferno_damage_taken']       = 0;
                        $newUpdate[$k1]['planted_bomb']       = 0;
                        $newUpdate[$k1]['defused_bomb']       = 0;
                        $newUpdate[$k1]['chicken_kills']       = 0;
                        $newUpdate[$k1]['blind_enemy_time']       = 0;
                        $newUpdate[$k1]['blind_teammate_time']       = 0;
                        $newUpdate[$k1]['team_kills']       = 0;
                        $newUpdate[$k1]['multi_kills']       = 0;
                        $newUpdate[$k1]['two_kills']       = 0;
                        $newUpdate[$k1]['three_kills']       = 0;
                        $newUpdate[$k1]['four_kills']       = 0;
                        $newUpdate[$k1]['five_kills']       = 0;
                        $newUpdate[$k1]['one_on_x_clutches']       = 0;
                        $newUpdate[$k1]['one_on_one_clutches']       = 0;
                        $newUpdate[$k1]['one_on_two_clutches']       = 0;
                        $newUpdate[$k1]['one_on_three_clutches']       = 0;
                        $newUpdate[$k1]['one_on_four_clutches']       = 0;
                        $newUpdate[$k1]['one_on_five_clutches']       = 0;
                        $newUpdate[$k1]['hit_generic']       = 0;
                        $newUpdate[$k1]['hit_head']       = 0;
                        $newUpdate[$k1]['hit_chest']       = 0;
                        $newUpdate[$k1]['hit_stomach']       = 0;
                        $newUpdate[$k1]['hit_left_arm']       = 0;
                        $newUpdate[$k1]['hit_right_arm']       = 0;
                        $newUpdate[$k1]['hit_left_leg']       = 0;
                        $newUpdate[$k1]['hit_right_leg']       = 0;
                        $newUpdate[$k1]['awp_kills']       = 0;
                        $newUpdate[$k1]['knife_kills']       = 0;
                        $newUpdate[$k1]['taser_kills']       = 0;
                        $newUpdate[$k1]['shotgun_kills']       = 0;
                        $newUpdate[$k1]['kast_number']       = 0;

                    }
                    $newUpdate[$k1]['has_kevlar']             = $v1['has_kevlar'];
                    $newUpdate[$k1]['has_helmet']             = $v1['has_helmet'];
                    $newUpdate[$k1]['kills']             += $v1['kills'];
                    $newUpdate[$k1]['headshot_kills']    += $v1['headshot_kills'];
                    $newUpdate[$k1]['deaths']            += $v1['deaths'];
                    $newUpdate[$k1]['k_d_diff']          += $v1['k_d_diff'];
                    $newUpdate[$k1]['assists']           += $v1['assists'];
                    $newUpdate[$k1]['flash_assist']      += $v1['flash_assist'];
                    $newUpdate[$k1]['first_kills']       += $v1['first_kills'];
                    $newUpdate[$k1]['first_deaths']      += $v1['first_deaths'];
                    $newUpdate[$k1]['first_kills_diff']  += $v1['first_kills_diff'];
                    $newUpdate[$k1]['kast']              += $v1['kast'];
                    $newUpdate[$k1]['multi_kills']              += $v1['multi_kills'];
                    $newUpdate[$k1]['knife_kill']        += $v1['knife_kill'];
                    $newUpdate[$k1]['ace_kill']          += $v1['ace_kill'];
                    $newUpdate[$k1]['damage']            += $v1['damage'];
                    $newUpdate[$k1]['real_damage']       += $v1['real_damage'];
                    $newUpdate[$k1]['one_on_x_clutches']              += $v1['one_on_x_clutches'];
                    $newUpdate[$k1]['team_damage']            += $v1['team_damage'];
                    $newUpdate[$k1]['damage_taken']           += $v1['damage_taken'];
                    $newUpdate[$k1]['hegrenade_damage_taken'] += $v1['hegrenade_damage_taken'];
                    $newUpdate[$k1]['inferno_damage_taken']   += $v1['inferno_damage_taken'];
                    $newUpdate[$k1]['planted_bomb']           += $v1['planted_bomb'];
                    $newUpdate[$k1]['defused_bomb']           += $v1['defused_bomb'];
                    $newUpdate[$k1]['chicken_kills']          += $v1['chicken_kills'];
                    $newUpdate[$k1]['blind_enemy_time']       += $v1['blind_enemy_time'];
                    $newUpdate[$k1]['blind_teammate_time']    += $v1['blind_teammate_time'];
                    $newUpdate[$k1]['team_kills']             += $v1['team_kills'];
                    $newUpdate[$k1]['one_kills']              += $v1['one_kills'];
                    $newUpdate[$k1]['two_kills']              += $v1['two_kills'];
                    $newUpdate[$k1]['three_kills']            += $v1['three_kills'];
                    $newUpdate[$k1]['four_kills']             += $v1['four_kills'];
                    $newUpdate[$k1]['five_kills']             += $v1['five_kills'];

                    $newUpdate[$k1]['one_on_one_clutches']    += $v1['one_on_one_clutches'];
                    $newUpdate[$k1]['one_on_two_clutches']    += $v1['one_on_two_clutches'];
                    $newUpdate[$k1]['one_on_three_clutches']  += $v1['one_on_three_clutches'];
                    $newUpdate[$k1]['one_on_four_clutches']   += $v1['one_on_four_clutches'];
                    $newUpdate[$k1]['one_on_five_clutches']   += $v1['one_on_five_clutches'];
                    $newUpdate[$k1]['hit_generic']            += $v1['hit_generic'];
                    $newUpdate[$k1]['hit_head']               += $v1['hit_head'];
                    $newUpdate[$k1]['hit_chest']              += $v1['hit_chest'];
                    $newUpdate[$k1]['hit_stomach']            += $v1['hit_stomach'];
                    $newUpdate[$k1]['hit_left_arm']           += $v1['hit_left_arm'];
                    $newUpdate[$k1]['hit_right_arm']          += $v1['hit_right_arm'];
                    $newUpdate[$k1]['hit_left_leg']           += $v1['hit_left_leg'];
                    $newUpdate[$k1]['hit_right_leg']          += $v1['hit_right_leg'];
                    $newUpdate[$k1]['awp_kills']              += $v1['awp_kills'];
                    $newUpdate[$k1]['knife_kills']            += $v1['knife_kills'];
                    $newUpdate[$k1]['taser_kills']            += $v1['taser_kills'];
                    $newUpdate[$k1]['shotgun_kills']          += $v1['shotgun_kills'];
                    $newUpdate[$k1]['kast_number']            += $v1['kast_number'];
                }
            }
            if (!empty($newUpdate)) {
                foreach ($newUpdate as $k => $v) {
                    //adr(battle)
                    $count = $this->getIsUseCountRoundsByBattleId();
                    if ($count != 0) {
                        $v['adr'] = round(@$v['real_damage'] / $count, 2);
                    }else{
                        $v['adr'] =0;
                    }
                    if ($count != 0) {
                        $v['kast'] = round(@$v['kast_number'] / $count, 4);
                    }else{
                        $v['kast'] =0;
                    }
//                    if ($count != 0) {
//                        $v['rating'] = round(@$v['rating'] / $count, 2);
//                    }else{
//                        $v['rating'] =0;
//                    }

                    $this->setBattlePlayerBySteamId($k, $v);
                }
            }
            return true;
        }else{
            return false;
        }

    }

    public function getReloadToBattle()
    {
        $battles = $this->getBattles();
        $battleOrder = $this->getRealOrder();
        foreach ($battles as $k => $v) {
            $arr_v = json_decode($v,true);
            if ($arr_v['order']==$battleOrder&&$arr_v['identification']) {
                return $arr_v;
            }
        }
        return false;
    }

    /**
     * @param $matchId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getMatchBattleByMatchId($matchId)
    {
        return MatchBattle::find()->where(['match' => $matchId])->all();
    }


    public function clearBattleByBattleId($battleId){
        if ($battleId) {
            BattleService::deleteBattle('csgo', $battleId);
        } else {
            return false;
        }
        $vBattle =  MatchBattle::find()->where(['id' => $battleId])->one();
        if ($vBattle['is_forfeit'] != 1 && $vBattle['is_default_advantage'] != 1) {
            //set battle delete deleted
            $map = [
                'deleted'              => 2,
                'deleted_at'           => null,
                'status'               => 1,
                'is_draw'              => 2,
                'is_forfeit'           => 2,
                'is_default_advantage' => 2,
                'is_battle_detailed'   => 1,
            ];
            $vBattle->setAttributes($map);
            $vBattle->save();
        }
    }

    /**
     * clear api Battle
     * @param $matchId
     */
    public function clearBattleMatchId($matchId,$type='')
    {
        //error matchId
        if (empty($matchId)){
            return false;
        }
//        //update match status
//        $matchReal = MatchRealTimeInfo::find()->where(['id' => $matchId])->asArray()->one();
//        $matchReal->setAttribute('status', self::STATUS_GOING);
//        $matchReal->save();
        //battles
        $battles = $this->getMatchBattleByMatchId($matchId);
        //clear api Battle
        foreach ($battles as $kBattle => $vBattle) {
            if ($vBattle['id']){
                //调用jianqi的清除battle方法
                BattleService::deleteBattle('csgo', $vBattle['id']);
            }
            if ($type == 'battle_reloaded'&& $vBattle['is_forfeit']!=1&&$vBattle['is_default_advantage']!=1){
                //set battle delete deleted
                $map = [
                    'deleted'=>2,
                    'deleted_at'=>null,
                    'status'=>1,
                    'is_draw'=>2,
                    'is_forfeit'=>2,
                    'is_default_advantage'=>2,
                    'is_battle_detailed'=>1,
                ];
                $vBattle->setAttributes($map);
                $vBattle->save();
            }

//            //is_forfeit is_default_advantage
//            if (  $vBattle['is_forfeit'] != 1 && $vBattle['is_default_advantage'] != 1) {
//                if ($type!='battle_reloaded'){
//                    //set battle delete deleted
//                    $vBattle->setAttribute('deleted', 1);
//                    $vBattle->setAttribute('flag', 2);
//                    $vBattle->save();
//                }
//                if ($vBattle['id']){
//                    $map = ['battle_id' => $vBattle['id']];
//                    //clear battle round
//                    MatchBattleRoundCsgo::deleteAll($map);
//                    //clear battle round side
//                    MatchBattleRoundSideCsgo::deleteAll($map);
//                    //clear battle player
//                    MatchBattlePlayerCsgo::deleteAll($map);
//                    //clear round event
//                    MatchBattleRoundEventCsgo::deleteAll($map);
//                    //clear round player
//                    MatchBattleRoundPlayerCsgo::deleteAll($map);
//                    $mapTeamExt = ['id' => $vBattle['id']];
//                    //clear battle team ext csgo
//                    MatchBattleTeamExtCsgo::deleteAll($mapTeamExt);
//                }
//            }
        }
    }


    public function saveBigArray($battle){
        if (empty($battle['battle_id'])){
            return false;
        }
        //select all redis about the match
        $preHistory   = $this->currentPre.self::Symbol.$this->matchId.self::Symbol.self::KEY_TAG_HISTORY.self::Symbol.self::KEY_TAG_BATTLE.self::Symbol.$battle['battle_id'];
        $preCurrent   = $this->currentPre.self::Symbol.$this->matchId.self::Symbol.self::KEY_TAG_CURRENT;
        // Have scan retry
        $this->redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        $bigArr = [];
        $bigArrKey = [];
        //ws:508:history:battle:list
        $battle_list_key = 'hltv_ws:'.$this->matchId.':history:battle:list';
        $bigArr[$battle_list_key] = $this->redis->hGetAll($battle_list_key);
        $specialEventKeyArr = self::SPECIAL_EVENT_KEY_ARR;
        $this->error_catch_log([$this->matchId.'_big_reload'=>'存history大数组开始']);

        while ($arr_keys = $this->redis->scan($it, "$preHistory*", 100)) {
            if (!empty($arr_keys)){
                $muti       = $this->redis->multi(Redis::PIPELINE);//pipe
                foreach ($arr_keys as $k=>$v){
                    $arr_redis_keys = explode(':',$v);
                    if (!empty($arr_redis_keys)){
                        $length_redis_key = count($arr_redis_keys);
                        if ($length_redis_key >= 5 && $arr_redis_keys[2]=='history'&& strstr($arr_redis_keys[4], 'live')) {
                            if ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'player' && array_key_exists(6, $arr_redis_keys) && $arr_redis_keys[6] == 'list') {
                                $bigArrKey[] = $v;
                                //ws:508:history:battle:1_live_1:player:list
                                 $this->redis->hGetAll($v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round'&& array_key_exists(7, $arr_redis_keys) &&  $arr_redis_keys[7] == 'player') {
                                $bigArrKey[] = $v;
                                //ws:508:history:battle:1_live_1:round:10:player
                                 $this->redis->hGetAll($v);
                            }elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round'&& array_key_exists(7, $arr_redis_keys) &&  in_array($arr_redis_keys[7],$specialEventKeyArr) ) {
                                $bigArrKey[] = $v;
                                //ws:551:history:battle:2_live_2:round:17:kill_events
                                $this->redis->lRange($v,0,-1);
                            }elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round'&& array_key_exists(7, $arr_redis_keys) &&  $arr_redis_keys[7] == 'record_player') {
                                $bigArrKey[] = $v;
                                //ws:508:history:battle:1_live_1:round:10:record_player
                                 $this->redis->hGetAll($v);
                            }elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && array_key_exists(6, $arr_redis_keys) && $arr_redis_keys[6] == 'list') {
                                $bigArrKey[] = $v;
                                //ws:508:history:battle:1_live_1:round:list
                                 $this->redis->hGetAll($v);
                            }
                        }
                    }
                }
                $bigArrMuti = $muti->exec();
                foreach ($bigArrKey as $k=>$v){
                    $bigArr[$v] = $bigArrMuti[$k];
                }
                $bigArrKey =[];
            }
        }
        $this->error_catch_log([$this->matchId.'_big_reload'=>'存current大数组开始']);
        $bigArrKey = [];
        $match_key_is_list = self::SPECIAL_EVENT_MATCH_RELOAD_ARR;
        while ($arr_keys = $this->redis->scan($itCurrent, "$preCurrent*", 100)) {
            if (!empty($arr_keys)){
                $muti       = $this->redis->multi(Redis::PIPELINE);//pipe
                foreach ($arr_keys as $k=>$v){
                    $arr_redis_keys = explode(':',$v);
                    if (!empty($arr_redis_keys)){
                        $length_redis_key = count($arr_redis_keys);
                        if ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'battle') {
                            $bigArrKey[] = $v;
                            //ws:2737:current:battle:battle_id
                             $this->redis->get($v);
                        }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'match'&&in_array($arr_redis_keys[4],$match_key_is_list)) {
                            $bigArrKey[] = $v;
                            //ws:2737:current:match:battle_1_score
                             $this->redis->lRange($v,0,-1);
                        }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'match') {
                            $bigArrKey[] = $v;
                            //ws:2737:current:match:battle_1_score
                            $this->redis->get($v);
                        }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'round') {
                            $bigArrKey[] = $v;
                            //ws:2737:current:round:status
                             $this->redis->get($v);
                        }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'player') {
                            $bigArrKey[] = $v;
                            //ws:2737:current:player:list
                             $this->redis->hGetAll($v);
                        }
                    }
                }
                $bigArrMuti = $muti->exec();
                foreach ($bigArrKey as $k=>$v){
                    $bigArr[$v] = $bigArrMuti[$k];
                }
                $bigArrKey =[];
            }
        }
        $reload_order = $this->incr([self::KEY_TAG_RELOAD,self::KEY_TAG_ORDER]);
        $battle['count_use_round'] = count($this->getRoundsByBattleId($battle['battle_id']));
        $battle['reload_order'] = $reload_order;
        $this->lPush([self::KEY_TAG_RELOAD,self::KEY_TAG_LIST],json_encode($battle,320));
        $this->setValue([self::KEY_TAG_RELOAD,self::KEY_TAG_BATTLE_ID],$battle['battle_id']);
        return $this->setValue(['reload',$reload_order],json_encode($bigArr,320));
    }

    public function getReloadList(){
        return $this->lRange([self::KEY_TAG_RELOAD,self::KEY_TAG_LIST],0,-1);
    }

    public function getShouldReloadOrder($battleId,$roundTo){
        $list = $this->getReloadList();
        if (!empty($list)){
            $reloadOrderArr = [];
            foreach ($list as $k=>$v){
                $vArr = json_decode($v,true);
                if ($vArr['battle_id']==$battleId&&$roundTo<=$v['count_use_round']){
                    $reloadOrderArr[] = $v['reload_order'];
                }
            }
            if (!empty($reloadOrderArr)){
                return max($reloadOrderArr)?max($reloadOrderArr):$this->getValue([self::KEY_TAG_RELOAD,self::KEY_TAG_ORDER]);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param $battleTo
     * @param $roundTo
     * @param $battleId
     * @return bool
     */
    public function reSetOnReload($battleTo,$roundTo,$battleId)
    {
//        $battleTo      = 1;//test
//        $battleId       = '1_live_2';//test
        $battleArr    = explode('_',$battleId);
//        $this->matchId = 508;//test
        $currentBattleId = $this->getBattleId();
        $preHistory   = $this->currentPre.self::Symbol.$this->matchId.self::Symbol.self::KEY_TAG_HISTORY.self::Symbol.self::KEY_TAG_BATTLE.self::Symbol.$battleArr[0].'_live';
        //delete history battle
        $this->batchDelRedisKey($preHistory);
//        $reload_order = $this->getValue([self::KEY_TAG_RELOAD,self::KEY_TAG_ORDER]);
//        $reload_order = $this->getShouldReloadOrder($battleId, $roundTo);
        $reload_order = $this->getValue([self::KEY_TAG_RELOAD, self::KEY_TAG_RELOAD_ORDER]);
        $bigArrJson   = $this->getValue(['reload', $reload_order]);
        $bigArr     = json_decode($bigArrJson, true);
        $specialEventKeyArr = self::SPECIAL_EVENT_KEY_SET_ARR;
        $match_key_is_list = self::SPECIAL_EVENT_MATCH_RELOAD_ARR;
        if (!empty($bigArr)) {
            $muti       = $this->redis->multi(Redis::PIPELINE);//pipe
            foreach ($bigArr as $k => $v) {
                $arr_redis_keys = explode(':', $k);
                if (!empty($arr_redis_keys)) {
                    $length_redis_key = count($arr_redis_keys);
                    if ($length_redis_key >= 5 && strstr($arr_redis_keys[4], 'live')) {
                        $arr_battle_id = explode('_', $arr_redis_keys[4]);
                        //battleId < $battleTo flush all round
                        if (!empty($arr_battle_id) && count($arr_battle_id) == 3 && $arr_battle_id[0] < $battleTo) {
                            if ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'player' &&array_key_exists(6, $arr_redis_keys)&& $arr_redis_keys[6] == 'list') {
                                //ws:508:history:battle:1_live_1:player:list
                                $muti->hMSet($k, $v);
                            }elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' &&array_key_exists(7, $arr_redis_keys)&& $arr_redis_keys[7] == 'record_player') {
                                //ws:551:history:battle:2_live_2:round:10:record_player
                                $muti->hMSet($k, $v);
                            }  elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' &&array_key_exists(7, $arr_redis_keys)&& $arr_redis_keys[7] == 'player' ) {
                                //ws:508:history:battle:1_live_1:round:10:player
                                $muti->hMSet($k, $v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' &&array_key_exists(7, $arr_redis_keys)&& in_array($arr_redis_keys[7],$specialEventKeyArr)) {
                                //ws:508:history:battle:1_live_1:round:10:events
                                if (!empty($v)) {
                                    foreach ($v as $k1 => $v1) {
                                        $muti->rPush($k, $v1);
                                    }
                                }
                            }  elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round'&&array_key_exists(6, $arr_redis_keys) && $arr_redis_keys[6] == 'list') {
                                //ws:508:history:battle:1_live_1:round:list
                                $muti->hMSet($k, $v);
                            }
                            //battleId == $battleTo flush part round
                        }elseif (!empty($arr_battle_id) && count($arr_battle_id) == 3 && $arr_battle_id[0] == $battleTo && $arr_redis_keys[4] == $battleId) {
                            if ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'player' && $arr_redis_keys[6] == 'list') {
                                //ws:508:history:battle:1_live_1:player:list
                                $muti->hMSet($k, $v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round'  &&array_key_exists(7, $arr_redis_keys)&& $arr_redis_keys[7] == 'record_player' && $arr_redis_keys[6] <= $roundTo) {
                                //ws:551:history:battle:2_live_2:round:10:record_player
                                $muti->hMSet($k, $v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && $arr_redis_keys[7] == 'player' && $arr_redis_keys[6] <= $roundTo && !array_key_exists(8,$arr_redis_keys)) {
                                //ws:508:history:battle:1_live_1:round:10:player
                                $muti->hMSet($k, $v);
                            }  elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && $arr_redis_keys[7] == 'player' && $arr_redis_keys[6] <= $roundTo && array_key_exists(8,$arr_redis_keys) && $arr_redis_keys[8] == 'grenades' ) {
                                //ws:551:history:battle:2_live_2:round:2:player:grenades
                                $muti->hMSet($k, $v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && $arr_redis_keys[7] == 'player' && $arr_redis_keys[6] <= $roundTo && array_key_exists(8,$arr_redis_keys) && $arr_redis_keys[8] == 'weapons' ) {
                                //ws:551:history:battle:2_live_2:round:2:player:grenades
                                $muti->hMSet($k, $v);
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && array_key_exists(7, $arr_redis_keys) && in_array($arr_redis_keys[7],$specialEventKeyArr)) {
                                //ws:508:history:battle:1_live_1:round:10:events
                                if (!empty($v)) {
                                    foreach ($v as $k1 => $v1) {
                                        $muti->rPush($k, $v1);
                                    }
                                }
                            } elseif ($arr_redis_keys[2] == 'history' && $arr_redis_keys[5] == 'round' && $arr_redis_keys[6] == 'list' ) {
                                $newRoundListArr = [];
                                //ws:508:history:battle:1_live_1:round:list
                                if (!empty($v)) {
                                    foreach ($v as $k1 => $v1) {
                                        if ($k1 <= $roundTo) {
                                            $newRoundListArr[$k1] = $v1;
                                        }
                                    }
                                }
                                //update round list
                                $muti->hMSet($k, $newRoundListArr);
                            }
                        }
                    } elseif ($length_redis_key >= 5 && $arr_redis_keys[4] == 'list' && $arr_redis_keys[2]=='history'&& $arr_redis_keys[3]=='battle') {
                        $newBattleListArr = [];
                        //ws:508:history:battle:list
//                        $this->redis->hMSet($k.'copy', $v);
                        if (!empty($v)) {
                            foreach ($v as $k1 => $v1) {
                                $arr_battle_id = explode('_', $k1);
                                if (strstr($k1,'live')&&!empty($arr_battle_id) && count($arr_battle_id) == 3&&$arr_battle_id[0]<$battleTo) {
                                    $newBattleListArr[$k1] = $v1;
                                }elseif (strstr($k1,'live')&&!empty($arr_battle_id) && count($arr_battle_id) == 3&&$arr_battle_id[0]==$battleTo&&$arr_battle_id[2]<=$battleArr[2]) {
                                    $newBattleListArr[$k1] = $v1;
                                }
                            }
                        }
                        //del battle list
//                        $this->redis->del($k);
                        //add new battle list
                        $muti->hMSet($k, $newBattleListArr);

                    }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'battle') {
                        //ws:2737:current:battle:battle_id
                        if ($arr_redis_keys[4]=='number'){
                            $muti->set($k, $battleArr[2]);
                        }elseif ($arr_redis_keys[4]=='order'){
                            $muti->set($k, $battleId);
                        }else{
                            $muti->set($k, $v);
                        }

                    } elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'match'&&in_array($arr_redis_keys[4],$match_key_is_list)) {
                        //ws:2737:current:match:battle_1_score
                        if (!empty($v)) {
                            foreach ($v as $k1 => $v1) {
                                $muti->rPush($k, $v1);
                            }
                        }
                    }elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'match') {
                        //ws:2737:current:match:battle_1_score
                        $muti->set($k, $v);
                    } elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'round') {
                        //ws:2737:current:round:status
                        if ($arr_redis_keys[4]=='round_ordinal'){
                            $muti->set($k, $roundTo);
                        }else{
                            $muti->set($k, $v);
                        }

                    } elseif ($arr_redis_keys[2] == 'current' && $arr_redis_keys[3] == 'player') {
                        //ws:2737:current:player:list
                        $muti->hMSet($k, $v);
                    }
                }
            }
             $exec_res = $muti->exec();
        }
        $this->error_catch_log([$this->matchId.'_big_reload'=>'重新set结束']);
        $this->error_catch_log([$this->matchId.'_big_reload'=>json_encode($exec_res,320)]);
        //set  battle true
        $this->setCurrentBattleDetails(['is_use'=>true],$battleId);
        return true;
    }

    public function getRealReloadRoundTo($roundTo, $battleId)
    {
        $counts = $this->getNoUseRoundsByBattleId($battleId);
        $res    = $roundTo + $counts;
        return $res >= 0 ? $res : 0;
    }

    /**
     * 回档
     * @param $data
     * @return mixed
     */
    public function battle_reloaded($data, $event)
    {
        $event_round_to = $event['info']['ext_log']['round_to'][0]['round_to'];
        if (@!isset($event_round_to)) {
            $this->littleReload();
        } else {
            $this->bigReload=true;
            //big Reload
            $battle       = $this->getReloadToBattle();
            $battleTo     = $battle['real_order'];
            $battleId     = $battle['battle_id'];
            $realBattleId = $battle['real_battle_id'];
            if ($battleTo && $battleId) {
                $this->setCurrentBattleDetails(['is_use' => false]);

                $roundTo = $this->getRealReloadRoundTo($event['info']['ext_log']['round_to'][0]['round_to'], $battleId);
                $roundTo = intval($roundTo);

                if ($this->refresh_task){
                    $temp_refresh_task = true;
                }else{
                    $temp_refresh_task = false;
                }
                $this->refresh_task_str = [];
                $this->refresh_task = false;
                //save array to redis
                $this->saveBigArray($battle);
                //set $battleId
                //reset battle and round
                $this->reSetOnReload($battleTo, $roundTo, $battleId);

                //incr round order
                $this->incr([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ORDINAL]);//回合序号
                //update round details
                $this->setRoundDetailsOnRoundStart();
                $this->setCurrentRoundDetails(['is_use' => false]);
                //重新统计
                $this->reSetBattleDataOnReload();
                $this->refresh_task = true;
                if ($temp_refresh_task){
                    $this->refresh_task = true;
                }

                //clear api Battle
                $this->clearBattleByBattleId($realBattleId);
            } else {
                return false;
            }
        }

        $map = $this->findOneMapByName($event['info']['log_map_name']);
        $data['map']['map_id']                   = (int)$map['id'];
        $data['map']['name']                     = @$map['name'];
        $data['map']['name_cn']                  = @$map['name_cn'];
        $data['map']['external_id']              = @$map['external_id'];
        $data['map']['external_name']            = @$map['external_name'];
        $data['map']['short_name']               = @$map['short_name'];
        $data['map']['map_type']                 = @$map['e_name'];
        $data['map']['map_type_cn']              = @$map['c_name'];
        $data['map']['is_default']               = @$map['is_default'] == 1;
        $data['map']['slug']               = @$map['slug'];
        $data['map']['image']['square_image']    = @$map['square_image'];
        $data['map']['image']['rectangle_image'] = @$map['rectangle_image'];
        $data['map']['image']['thumbnail']       = @$map['thumbnail'];
        $ct_order     = $this->getTeamOrderByTeamName(@$event['info']['ext_log']['battle_team_playing_ct'][0]['log_team_name']);
        $t_order      = $this->getTeamOrderByTeamName(@$event['info']['ext_log']['battle_team_playing_t'][0]['log_team_name']);
        $team_ct      = $this->getTeamByName(@$event['info']['ext_log']['battle_team_playing_ct'][0]['log_team_name']);
        $team_t       = $this->getTeamByName(@$event['info']['ext_log']['battle_team_playing_t'][0]['log_team_name']);
        //开局作为反恐精英
        $data['current_ct']['team_id']        = $team_ct['id']?(int)$team_ct['id']:null;
        $data['current_ct']['name']           = $team_ct['name'];
        $data['current_ct']['image']          = ImageConversionHelper::showFixedSizeConversion(@$team_ct['image'],200,200);
        $data['current_ct']['opponent_order'] = $ct_order;
        //开局作为恐怖分子
        $data['current_t']['team_id']        = (int)$team_t['id'];
        $data['current_t']['name']           = $team_t['name'];
        $data['current_t']['image']          = ImageConversionHelper::showFixedSizeConversion(@$team_t['image'],200,200);
        $data['current_t']['opponent_order'] = $t_order;
        $data['restore_to'] = intval($event_round_to)?intval($event_round_to):$this->currentRound();
        return $data;
    }


    /**
     * @param $steamId
     * @return array|bool|\yii\db\ActiveRecord|null
     */
    public function getPlayerInfoBySteamId($steamId)
    {
        if (!$steamId) {
            return false;
        }
        // 解析steam_id
        $id        = SteamHelper::playerConversionSteamId($steamId);
//        $redis_res = $this->redis->hGetAll($this->currentPre.':'.$this->matchId.':table:player:' . $id);
        $redis_res = $this->hGetAll(['table','player' ,$id]);
        if ($redis_res) {
            return $redis_res;
        } else {
            $player = \app\modules\org\models\Player::find()->where(['like','steam_id' , $id])->asArray()->one();
            if (!empty($player)) {
                $this->hMSet(['table','player' ,$id], $player);
            }
            return $player;
        }
    }



    /**
     * 选手进入游戏
     * @param $data
     * @return mixed
     */
    public function player_joined($data, $event)
    {
        //初始化基本信息
        $this->initPlayer($event);
        $player            = $this->getPlayerInfoBySteamId($event['info']['playID']);
        //设置player list
        $data['player_id'] = !empty($player)?@(int)$player['id']:null;
        $data['nick_name'] = !empty($player)?@$player['nick_name']:$event['info']['playerName'];
        $data['steam_id'] = SteamHelper::playerConversionSteamId($event['info']['playID']);;
        return $data;
    }

    /**
     * @param $playerId
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getTeamByPlayerId($playerId)
    {
        return TeamPlayerRelation::find()->where(['id' => $playerId])->asArray()->one();
    }

    /**
     * 初始化选手
     * @param $event
     */
    public function initPlayer($event)
    {
        if ($event['info']['event_type'] == 'player_joined') {
            $event['info']['steamId'] = $event['info']['playID'];
        }
        //init player
        $playerInfo              = $this->getPlayerInfoBySteamId($event['info']['steamId']);
        $playerArr['playerName'] = @$event['info']['playerName'];
        $playerArr['nick_name']  = @$playerInfo ? @$playerInfo['nick_name'] : @$event['info']['playerName'];
        $playerArr['side']       = @$event['info']['afterSide'] ? @$event['info']['afterSide'] : @$event['info']['side'];
//        if ($playerArr['side'] == 'ct') {
//            $team = $this->getCurrentCt();
//        } else {
//            $team = $this->getCurrentT();
//        }
//        $playerArr['team_log_name'] = @$team['log_name'];
//        $playerArr['team_name']     = @$team['name'];
//        $playerArr['team_id']       = @$team['team_id'];
//        $playerArr['team_order']    = @$team['opponent_order'];

        $playerArr['steam_id']  = @$event['info']['steamId'];
        $playerArr['player_id'] = @$playerInfo['id'];
        $playerArr['status']    = self::PLAYER_STATUS_ONLINE;
        $playerArr['ping']      = @$event['info']['ping'];

        $playerArr       = $this->initPlayerBase($playerArr);
        $battlePlayerKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        if ($event['info']['event_type'] == 'player_switch_team') {
            if ($this->getCurrentPlayer($event['info']['steamId'])) {
                $playerUpdateArr         = [];
                $playerUpdateArr['side'] = $event['info']['afterSide'];
                $this->setPlayerBySteamId($event['info']['steamId'], $playerUpdateArr);
            } else {
//                $playerArr['side'] = @$event['info']['afterSide'] ? $event['info']['afterSide'] : @$event['info']['side'];
            }

        }
        if ($event['info']['event_type'] == 'player_joined') {
            if ($this->getCurrentPlayer($event['info']['steamId'])) {
                $playerUpdateArr           = [];
                $playerUpdateArr['status'] = self::PLAYER_STATUS_ONLINE;
                $this->setPlayerBySteamId($event['info']['steamId'], $playerUpdateArr);
            } else {
//                $playerArr['status'] = self::PLAYER_STATUS_ONLINE;
            }

        }
        $this->hSetNx($battlePlayerKey, $event['info']['steamId'], json_encode($playerArr,320));
        $this->hSetNx([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER], $event['info']['steamId'], json_encode($playerArr,320));
        $this->hSetNx([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST], $event['info']['steamId'], json_encode($playerArr,320));
//        if ($event['info']['event_type'] == 'player_switch_team') {
//            $playerArr['side'] = $event['info']['afterSide'];
//            $this->setRoundPlayerBySteamId($event['info']['steamId'], $playerArr);
//            $this->setBattlePlayerBySteamId($event['info']['steamId'], $playerArr);
//            $this->setCurrentPlayerBySteamId($event['info']['steamId'], $playerArr);
//        }
//        if ($event['info']['event_type'] == 'player_joined') {
//            $playerArr['status'] = self::PLAYER_STATUS_ONLINE;
//            $this->setRoundPlayerBySteamId($event['info']['playID'], $playerArr);
//            $this->setBattlePlayerBySteamId($event['info']['playID'], $playerArr);
//            $this->setCurrentPlayerBySteamId($event['info']['playID'], $playerArr);
//        }
    }

    public function initPlayerBase($playerArr)
    {
        $playerArr['hp']                = 100;
        $playerArr['kills']             = 0;
        $playerArr['headshot_kills']    = 0;
        $playerArr['deaths']            = 0;
        $playerArr['k_d_diff']          = 0;
        $playerArr['assists']           = 0;
        $playerArr['flash_assist']      = 0;
        $playerArr['adr']               = 0;
        $playerArr['first_kills']       = 0;
        $playerArr['first_deaths']      = 0;
        $playerArr['first_kills_diff']  = 0;
        $playerArr['multi_kills']       = 0;
        $playerArr['one_on_x_clutches'] = 0;
        $playerArr['knife_kill']        = 0;
        $playerArr['ace_kill']          = 0;
        $playerArr['money']             = 0;
        $playerArr['coordinate']        = 0;
        $playerArr['damage']            = 0;
        $playerArr['real_damage']       = 0;
        $playerArr['blinded_time']      = 0;//致盲时间
        $playerArr['has_kevlar']        = false;//
        $playerArr['has_helmet']        = false;//
        $playerArr['has_defusekit']     = false;//
        $playerArr['has_bomb']          = false;//
        $playerArr['is_alive']          = true;//
        $playerArr['kast']              = 0;//
        $playerArr['knife_kill']        = 0;//
        //5e单独需要的数据
        $playerArr['team_damage']            = 0;//误伤队友
        $playerArr['damage_taken']           = 0;//承受总伤害
        $playerArr['hegrenade_damage_taken'] = 0;//承受手雷伤害
        $playerArr['inferno_damage_taken']   = 0;//承受燃烧弹伤害
        $playerArr['planted_bomb']           = 0;//安放炸弹
        $playerArr['defused_bomb']           = 0;//拆除炸弹
        $playerArr['chicken_kills']          = 0;//杀鸡
        $playerArr['blind_enemy_time']       = 0;//致盲敌人时长
        $playerArr['blind_teammate_time']    = 0;//致盲敌人时长
        $playerArr['team_kills']             = 0;//误杀队友
        $playerArr['one_kills']              = 0;//单杀
        $playerArr['two_kills']              = 0;//双杀
        $playerArr['three_kills']            = 0;//三杀
        $playerArr['four_kills']             = 0;//四杀
        $playerArr['five_kills']             = 0;//五杀
        $playerArr['one_on_one_clutches']    = 0;//1V1残局获胜
        $playerArr['one_on_two_clutches']    = 0;//1V2残局获胜
        $playerArr['one_on_three_clutches']  = 0;//1V3残局获胜
        $playerArr['one_on_four_clutches']   = 0;//1V4残局获胜
        $playerArr['one_on_five_clutches']   = 0;//1V5残局获胜
        $playerArr['hit_generic']            = 0;//击中敌人通用部位
        $playerArr['hit_head']               = 0;//击中敌人头部
        $playerArr['hit_chest']              = 0;//击中敌人胸部
        $playerArr['hit_stomach']            = 0;//击中敌人腹部
        $playerArr['hit_left_arm']           = 0;//击中敌人左臂
        $playerArr['hit_right_arm']          = 0;//击中敌人右臂
        $playerArr['hit_left_leg']           = 0;//击中敌人左腿
        $playerArr['hit_right_leg']          = 0;//击中敌人右腿
        $playerArr['awp_kills']              = 0;//AWP击杀
        $playerArr['knife_kills']            = 0;//刀杀
        $playerArr['taser_kills']            = 0;//电击枪击杀
        $playerArr['shotgun_kills']          = 0;//散弹枪击杀
        $playerArr['rating']                 = 0;//
        $playerArr['trade']                  = 0;//
        $playerArr['kast_number']            = 0;//
        $playerArr['position']            = '';//
        return $playerArr;
    }

    public function playerBase()
    {
        //player
        $playerArr['player_log_name'] = '';
        $playerArr['steam_id']        = '';
        $playerArr['nick_name']       = '';
        $playerArr['player_id']       = '';
        //team
        $playerArr['team_log_name'] = '';
        $playerArr['team_name']     = '';
        $playerArr['team_id']       = '';
        $playerArr['team_order']    = '';
        //status
        $playerArr['status'] = 0;
        $playerArr['ping']   = 0;
        $playerArr['side']   = 0;
        //details
        $playerArr['hp']                = 100;
        $playerArr['kills']             = 0;
        $playerArr['headshot_kills']    = 0;
        $playerArr['deaths']            = 0;
        $playerArr['k_d_diff']          = 0;
        $playerArr['assists']           = 0;
        $playerArr['flash_assist']      = 0;
        $playerArr['adr']               = 0;
        $playerArr['first_kills']       = 0;
        $playerArr['first_deaths']      = 0;
        $playerArr['first_kills_diff']  = 0;
        $playerArr['multi_kills']       = 0;
        $playerArr['one_on_x_clutches'] = 0;
        $playerArr['knife_kill']        = 0;
        $playerArr['ace_kill']          = 0;
        $playerArr['money']             = 0;
        $playerArr['coordinate']        = 0;
        $playerArr['damage']            = 0;
        $playerArr['real_damage']       = 0;
        $playerArr['blinded_time']      = 0;//致盲时间
        $playerArr['has_kevlar']        = false;//
        $playerArr['has_helmet']        = false;//
        $playerArr['has_defusekit']     = false;//
        $playerArr['has_bomb']          = false;//
        $playerArr['is_alive']          = true;//
        $playerArr['kast']              = 0;//
        $playerArr['knife_kill']        = 0;//
    }

    /**
     * set current player
     * @param $steamId
     * @param $updateArr
     * @param $updateType
     * @param int $changNum
     * @return string|void
     */
    public function setCurrentPlayerBySteamId($steamId, $updateArr, $updateType = '', $changNum = 1)
    {
        if (empty($updateArr)||$updateArr==null){
            $this->error_hash_log('setCurrentPlayerBySteamId',['updateArr'=>$updateArr]);
            return false;
        }
        $res = $this->getCurrentPlayer($steamId);
        if (empty($res)){
            $this->error_hash_log('setCurrentPlayerBySteamId',['steamId'=>$steamId]);
            $this->setCurrentPlayerIfHaveSteamId($steamId);
            return false;
        }
        if (!empty($res)){
            if ($updateType == 'incr') {

                foreach ($updateArr as $k => $v) {
                    if (!empty($v)){
                        $updateArrNew[$v] = $res[$v] + $changNum;
                    }
                }

            } elseif ($updateType == 'decr') {

                foreach ($updateArr as $k => $v) {
                    if (!empty($v)){
                        $updateArrNew[$v] = $res[$v] - $changNum > 0 ? $res[$v] - $changNum : 0;
                    }
                }
            }
        }

        $updateArr = $updateArrNew ?? $updateArr;
        if (empty($updateArr)) {
            return false;
        }

        $new_res = $res ? array_merge($res, $updateArr) : $updateArr;
        //首杀差更新
        @$new_res['first_kills_diff'] = @$new_res['first_kills'] - @$new_res['first_deaths'];
        @$new_res['k_d_diff'] = @$new_res['kills'] - @$new_res['deaths'];
        return $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST], $steamId, json_encode($new_res,320));
    }

    /**
     * get current player
     * @param $steamId
     * @return string|void
     */
    public function getCurrentPlayer($steamId)
    {
        $k = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        return json_decode($this->hGet($k, $steamId), true);
    }


    /**
     * 选手退出游戏
     * @param $data
     * @param $event
     * @return mixed
     */
    public function player_quit($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['playID']);
        //更新选手信息-状态-离线
        $updateArr['status'] = self::PLAYER_STATUS_OFFLINE;
        $this->setPlayerBySteamId($event['info']['playID'], $updateArr);
        $player            = $this->getPlayerInfoBySteamId($event['info']['playID']);
        $data['player_id'] = !empty($player)?@(int)$player['id']:null;
        $data['nick_name'] = !empty($player)?$player['nick_name']:$event['info']['playerName'];
        $data['steam_id'] = SteamHelper::playerConversionSteamId($event['info']['playID']);;
        $data['side']      = $event['info']['player_camp'];
        return $data;
    }

    /**
     * 根据武器名称查询redis表中的武器信息
     * @param $name
     * @return array|mixed|\yii\db\ActiveRecord|null
     */
    public function getWeaponByName($name)
    {
        if ($name == 'vesthelm' || $name == 'item_kevlar' || $name == 'vest') {
            return null;
        }
        if (strstr($name, 'knife') || strstr($name, 'bayonet') || strstr($name, 'karambit') || strstr($name, 'm9') || strstr($name, 'shadow') || strstr($name, 'daggers')) {
            $name = 'weapon_knife';
        }
        $redisWeapon = $this->getValue(['table','metadata_csgo_weapon', strtolower($name)]);
        if (!empty($redisWeapon)&&$redisWeapon!=null){
            return json_decode($redisWeapon,true);
        }else{
            $dbWeapon = MetadataCsgoWeapon::find()->andWhere(['like','external_name',strtolower($name)])->asArray()->one();
            if ($dbWeapon){
                $enum_csgo_weapon = EnumCsgoWeapon::find()->andWhere(['id'=>strtolower($dbWeapon['kind'])])->asArray()->one();
                $dbWeapon['kind_name'] = @$enum_csgo_weapon['kind'];
                $dbWeapon['kind_name_cn'] = @$enum_csgo_weapon['kind_cn'];
                $this->setValue(['table','metadata_csgo_weapon', strtolower($name)],json_encode($dbWeapon,320));
                return $dbWeapon;
            }else{
                $dbWeapon2 = MetadataCsgoWeapon::find()->andWhere(['like','external_name',strtolower($name)])->asArray()->one();
                if (empty($dbWeapon2)||$dbWeapon2==null){
                    $dbWeapon3 = MetadataCsgoWeapon::find()->andWhere(['slug'=>"unknown"])->asArray()->one();
                    $enum_csgo_weapon = EnumCsgoWeapon::find()->andWhere(['id'=>strtolower($dbWeapon['kind'])])->asArray()->one();
                    $dbWeapon3['kind_name'] = @$enum_csgo_weapon['kind'];
                    $dbWeapon3['kind_name_cn'] = @$enum_csgo_weapon['kind_cn'];
                    $this->setValue(['table','metadata_csgo_weapon', strtolower($name)],json_encode($dbWeapon3,320));
                    $this->error_hash_log('getWeaponByName',['name'=>$name,'event'=>$this->event]);
                }else{
                    $enum_csgo_weapon = EnumCsgoWeapon::find()->andWhere(['id'=>strtolower($dbWeapon['kind'])])->asArray()->one();
                    $dbWeapon2['kind_name'] = @$enum_csgo_weapon['kind'];
                    $dbWeapon2['kind_name_cn'] = @$enum_csgo_weapon['kind_cn'];
                    $this->setValue(['table','metadata_csgo_weapon', strtolower($name)],json_encode($dbWeapon2,320));
                }
                return $dbWeapon2;
            }
        }
    }

    public function getRoundBeginAt()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT]);
    }

    /**
     * 回合开始
     * freeze false
     * set round begin_at survived_players
     * @param $data
     * @param $event
     * @return mixed
     */
    public function round_start($data, $event)
    {
        $this->setBattlePause(false);
        //update round survived_players
        $this->updateRoundSurvived();
        $this->setFreezeTime(false);
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT], $event['info']['log_time']);//回合开始时间
        $this->setNx([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_BEGIN_AT], $event['info']['log_time']);
        $this->setRoundDetailsOnRoundStart();
        return $data;
    }

    public function setRoundDetailsOnRoundStart()
    {
        //create round details
        $baseArr['round_ordinal']  = $this->getRoundOrdinal();
        $baseArr['battle_id']      = $this->getBattleId();
        $baseArr['real_battle_id'] = $this->getRealBattleId();
        $baseArr['begin_at']       = $this->getRoundBeginAt();
        $ct_team                   = $this->getCurrentCt();
        //team ct
        $baseArr['ct']['team_id']        = $ct_team['team_id'];
        $baseArr['ct']['opponent_order'] = $ct_team['opponent_order'];
        $baseArr['ct']['log_name']       = @$ct_team['log_name'];
        $baseArr['ct']['name']           = $ct_team['name'];
        $baseArr['ct']['image']          = $ct_team['image'];
        $t_team                          = $this->getCurrentT();
        //team t
        $baseArr['terrorist']['team_id']        = $t_team['team_id'];
        $baseArr['terrorist']['opponent_order'] = $t_team['opponent_order'];
        $baseArr['terrorist']['log_name']       = @$t_team['log_name'];
        $baseArr['terrorist']['name']           = $t_team['name'];
        $baseArr['terrorist']['image']          = $t_team['image'];

        $this->setCurrentRoundDetails($baseArr);
    }



    public function setRoundEndAt($time)
    {
        //set round details
        $baseArr['end_at'] = $time;
        $this->setCurrentRoundDetails($baseArr);
    }

    public function setRoundBeginAt($time){
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_BEGIN_AT],$time);
    }


    /**
     * 回合结束
     * update round endAt,survived_players,status
     * @param $data
     * @param $event
     * @return mixed
     */
    public function round_end($data, $event)
    {

        $ct_score = 0;
        $t_score  = 0;
        $win_side = '';
        $win_type = '';
        //update round endAt
        $this->setRoundEndAt($event['info']['log_time']);

        //update round survived_players
        $this->updateRoundSurvived();
        //status end
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_STATUS], self::STATUS_END);//回合结束状态
        //round_win_type
        if (isset($event['info']['ext_log']['round_win_type'])) {
            $ct_score = @$event['info']['ext_log']['round_win_type'][0]['ct_score'];
            $t_score  = @$event['info']['ext_log']['round_win_type'][0]['t_score'];
            $win_type = @$event['info']['ext_log']['round_win_type'][0]['log_win_type'];
            $win_side = @$event['info']['ext_log']['round_win_type'][0]['log_win_camp'];
            $this->setRoundDetailsOnRoundEnd($event['info']['log_time'], $ct_score, $t_score, $win_type, $win_side);
        }
        //round_draw
        $round_draw = @$event['info']['ext_log']['round_draw'][0];
        if (!empty($round_draw) && is_array($round_draw)) {
            $ct_score = @$event['info']['ext_log']['round_draw'][0]['ct_score'];
            $t_score  = @$event['info']['ext_log']['round_draw'][0]['t_score'];
            $win_type = 'round_draw';
            $win_side = null;

//            $this->setRoundDetailsOnRoundEnd($event, $ct_score, $t_score, $win_type, $win_side);
        }
        //set battle score
        $baseArr   = [];
        $currentCt = $this->getCurrentCt();
        $startCt   = $this->getStartingCt();
        if ($currentCt['opponent_order'] == $startCt['opponent_order']) {
            $baseArr['start_ct']['score'] = @$ct_score;
            $baseArr['start_t']['score']  = @$t_score;
        } else {
            $baseArr['start_ct']['score'] = @$t_score;
            $baseArr['start_t']['score']  = @$ct_score;
        }
        $this->setStartingCtScore($baseArr['start_ct']['score']);
        $this->setStartingTScore($baseArr['start_t']['score']);
        $this->setCurrentBattleDetails($baseArr);
        //record battle details
        $this->recordBattleDetailsOnRoundEnd();
        //set round score
        $this->setRoundCtScore(@$ct_score);
        $this->setRoundTScore(@$t_score);
        //update 1vn
        $this->update1vnOnRoundEnd($win_side);
        //return data
        $data['round_end_type'] = $win_type;
        $data['winner_side']    = @$win_side;
        $data['ct_score']       = @(int)$ct_score;
        $data['t_score']        = @(int)$t_score;
        return $data;
    }

    /**
     * Rating = (KillRating + 0.7*SurvivalRating + RoundsWithMultipleKillsRating)/2.7
     * KillRating = Kills/Rounds/AverageKPR (平局每局击杀)
     * SurvivalRating = (Rounds-Deaths)/Rounds/AverageSPR (平局每局存活百分比)
     * RoundsWithMultipleKillsRating = (1K + 4*2K + 9*3K + 16*4K +25*5K)/Rounds/AverageRMK
     * AverageKPR = 0.679 (average kills per round)
     * AverageSPR = 0.317 (average survived rounds per round)
     * AverageRMK = 1.277 (average value calculated from rounds with multiple kills:
     * (1K + 4*2K + 9*3K + 16*4K + 25*5K)/Rounds)
     */
    public function updateRatingOnRoundEnd()
    {
        $playList = $this->getBattlePlayerListByType('all');
        $roundCount = $this->getIsUseCountRoundsByBattleId();
        if ($roundCount){
            foreach ($playList as $k=>$v){
                $killRating = $v['kills']/$roundCount/0.679;
                $survivalRating = ($roundCount-$v['deaths'])/$roundCount/0.317;
                $roundsWithMultipleKillsRating = ($v['one_kills'] + 4*$v['two_kills'] + 9*$v['three_kills'] + 16*$v['four_kills'] + 25*$v['five_kills'])/$roundCount/1.277;
                $rating = ($killRating + 0.7*$survivalRating + $roundsWithMultipleKillsRating)/2.7;
                $this->setPlayerBySteamId($v['steam_id'],['rating'=>$rating]);
            }
        }

    }

    public function updateKastOnRoundEnd()
    {
        $roundPlayerList = $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER]);
        if (!empty($roundPlayerList)) {
            //kast
            $roundDetails = $this->getCurrentRoundDetails();
            if ($roundDetails['is_use']==false){
                    return false;
            }
            foreach ($roundPlayerList as $k => $v) {
                $roundPlayer = $this->getRoundPlayerBySteamId($k);
                if (!empty($roundPlayer)) {
                    if ($roundPlayer['kills'] >= 1 || $roundPlayer['deaths'] == 0 ||  $roundPlayer['assists'] >= 1 || $roundPlayer['trade'] >= 1 ) {
                        //status fields
                        $updateArrStatistics= [];
                        $updateArrStatistics[] = 'kast_number';
                        $this->setPlayerBySteamId($k, $updateArrStatistics, 'incr');
                    }
                    $battlePlayer = $this->getBattlePlayerBySteamId($k);
                    $roundCount = $this->getIsUseCountRoundsByBattleId();
                    if ($roundCount){
                        $kast = $battlePlayer['kast_number']/$roundCount;
                        $this->setBattlePlayerBySteamId($k, ['kast'=>$kast]);
                    }
                }
            }
        }
    }

    public function update1vnOnRoundEnd($win_side){
        $playerCTInfo = '';
        $playerTInfo  = '';
        $nCTPlayers  = 0;
        $nTPlayers  = 0;
        $roundDetails = $this->getCurrentRoundDetails();
        if (count($roundDetails['1vn_ct_player']) == 1) {
            $playerCTInfo = $this->getRoundPlayerBySteamId($roundDetails['1vn_ct_player'][0]);
            $nCTPlayers     = count($roundDetails['1vn_t_player']);
        }
        if (count($roundDetails['1vn_t_player']) == 1) {
            $playerTInfo = $this->getRoundPlayerBySteamId($roundDetails['1vn_t_player'][0]);
            $nTPlayers    = count($roundDetails['1vn_ct_player']);
        }
        if ($playerCTInfo && $playerCTInfo['side'] == $win_side) {
            $playerInfo = $playerCTInfo;
            $nPlayers = $nCTPlayers;
        }
        if ($playerTInfo && $playerTInfo['side'] == $win_side) {
            $playerInfo = $playerTInfo;
            $nPlayers = $nTPlayers;
        }

        //do 1vn
        if ($playerInfo) {
            $updateArr   = [];
            $arr         = [
                1 => 'one',
                2 => 'two',
                3 => 'three',
                4 => 'four',
                5 => 'five',
            ];
            $num         = $arr[$nPlayers];
            $updateArr[] = 'one_on_' . $num . '_clutches';
            $updateArr[] = 'one_on_x_clutches';
            $this->setPlayerBySteamId($playerInfo['steam_id'], $updateArr, 'incr');
            return true;
        }
        $playerCTInfo = '';
        $playerTInfo  = '';
        $nCTPlayers  = 0;
        $nTPlayers  = 0;
        if (count($roundDetails['1vn_ct_player_final']) == 1) {
            $playerCTInfo = $this->getRoundPlayerBySteamId($roundDetails['1vn_ct_player_final'][0]);
            $nCTPlayers     = count($roundDetails['1vn_t_player_final']);
        }
        if (count($roundDetails['1vn_t_player_final']) == 1) {
            $playerTInfo = $this->getRoundPlayerBySteamId($roundDetails['1vn_t_player_final'][0]);
            $nTPlayers    = count($roundDetails['1vn_ct_player_final']);
        }

        if ($playerCTInfo && $playerCTInfo['side'] == $win_side) {
            $playerInfo = $playerCTInfo;
            $nPlayers = $nCTPlayers;
        }
        if ($playerTInfo && $playerTInfo['side'] == $win_side) {
            $playerInfo = $playerTInfo;
            $nPlayers = $nTPlayers;
        }

        //do 1vn
        if ($playerInfo) {
            $updateArr   = [];
            $arr         = [
                1 => 'one',
                2 => 'two',
                3 => 'three',
                4 => 'four',
                5 => 'five',
            ];
            $num         = $arr[$nPlayers];
            $updateArr[] = 'one_on_' . $num . '_clutches';
            $updateArr[] = 'one_on_x_clutches';
            $this->setPlayerBySteamId($playerInfo['steam_id'], $updateArr, 'incr');
            return true;
        }



    }

    public function setStartingCtScore($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_CT_SCORE], $value);
    }
    public function setStartingTScore($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_T_SCORE], $value);
    }


    public function getStartingCtScore()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_CT_SCORE])?$this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_CT_SCORE]):0;
    }
    public function getStartingTScore()
    {
        return $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_T_SCORE])?$this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_START_T_SCORE]):0;
    }



    /**
     * @return array|string
     */
    public function recordBattleDetailsOnRoundEnd(){
        $battlePlayerKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        $battlePlayer = $this->hGetAll($battlePlayerKey);
        if (!empty($battlePlayer)&&is_array($battlePlayer)){
            $recordKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_RECORD_PLAYER];
            return $this->hMset($recordKey,$battlePlayer);
        }
        return false;
    }

    /**
     * setRoundDetailsOnRoundEnd
     * @param $event
     * @param $ct_score
     * @param $t_score
     * @param $win_type
     * @param $win_side
     * @return bool
     */
    public function setRoundDetailsOnRoundEnd($log_time,$ct_score,$t_score,$win_type,$win_side)
    {
        $log_win_camp   = $win_side;
        $log_win_type   = $win_type;
        $round_ct_score = $ct_score;
        $round_t_score  = $t_score;

        if ($log_win_camp == 'ct') {
            $win_team = $this->getCurrentCt();
        } else {
            $win_team = $this->getCurrentT();
        }

        //update round details
        $rounds_arr['battle_id']           = $this->getBattleId();
        $rounds_arr['round_ordinal']       = $this->getRoundOrdinal();
        $rounds_arr['end_at']              = $log_time;
        if ($log_win_type != 'round_draw') {
            $rounds_arr['round_win_type'] = @$log_win_type;
        }

        $rounds_arr['round_ct_score']      = @$round_ct_score;
        $rounds_arr['round_t_score']       = @$round_t_score;
        $rounds_arr['survived_players_ct'] = $this->getSurvivedPlayersCountByType('ct');
        $rounds_arr['survived_players_t']  = $this->getSurvivedPlayersCountByType('t');
        if ($log_win_type != 'round_draw') {
            $rounds_arr['log_win_type'] = $log_win_type;
        }
        $rounds_arr['round_order']         = $this->getRoundOrdinal();
        $rounds_arr['winner_side']         = $log_win_camp;
        $rounds_arr['winner_order']        = $win_team['opponent_order'];
        $rounds_arr['winner_name']         = $win_team['name'];
        $this->setCurrentRoundDetails($rounds_arr);

        //update battle details
        $total = $round_ct_score + $round_t_score;
        if ($total == 1) {
            $baseBattleArr                          = [];
            $baseBattleArr['win_round_1_side']      = $log_win_camp;
            $baseBattleArr['win_round_1_team']      = $win_team['opponent_order'];
            $baseBattleArr['win_round_1_team_name'] = $win_team['name'];
            $baseBattleArr['win_round_1_detail']    = "";
            $this->setCurrentBattleDetails($baseBattleArr);
        }
        if (($round_ct_score == 5 && $round_t_score < 5) || ($round_t_score == 5 && $round_ct_score < 5)) {
            if ($round_ct_score == 5) {
                $firstFiveTeam                                     = $this->getCurrentCt();
                $baseBattleArr['first_to_5_rounds_wins_side']      = 'ct';
                $baseBattleArr['first_to_5_rounds_wins_team']      = $firstFiveTeam['opponent_order'];
                $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                $baseBattleArr['first_to_5_rounds_wins_team_name'] = $firstFiveTeam['name'];
                $this->setCurrentBattleDetails($baseBattleArr);
            } else {
                $firstFiveTeam                                     = $this->getCurrentT();
                $baseBattleArr['first_to_5_rounds_wins_side']      = 'terrorist';
                $baseBattleArr['first_to_5_rounds_wins_team']      = $firstFiveTeam['opponent_order'];
                $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                $baseBattleArr['first_to_5_rounds_wins_team_name'] = $firstFiveTeam['name'];
                $this->setCurrentBattleDetails($baseBattleArr);
            }


        }
        if ($total == 16) {
            $baseBattleArr                           = [];
            $baseBattleArr['win_round_16_side']      = $log_win_camp;
            $baseBattleArr['win_round_16_team']      = $win_team['opponent_order'];
            $baseBattleArr['win_round_16_team_name'] = $win_team['name'];
            $baseBattleArr['win_round_16_detail']    = "";
            $this->setCurrentBattleDetails($baseBattleArr);
        }
        $currentDetail = $this->getCurrentBattleDetails();

        if ($total <= 15) {
            $baseBattleArr                               = [];
            $baseBattleArr['start_ct']['1st_half_score'] = (int)$round_ct_score;
            $baseBattleArr['start_t']['1st_half_score']  = (int)$round_t_score;
            $this->setCurrentBattleDetails($baseBattleArr);
        } elseif ($total > 15 && $total <= 30) {
            $baseBattleArr                               = [];
            $baseBattleArr['start_ct']['2nd_half_score'] = $round_t_score - $currentDetail['start_ct']['1st_half_score'];
            $baseBattleArr['start_t']['2nd_half_score']  = $round_ct_score - $currentDetail['start_t']['1st_half_score'];
            $baseBattleArr['start_ct']['2nd_half_score'] = $baseBattleArr['start_ct']['2nd_half_score'] > 0 ? $baseBattleArr['start_ct']['2nd_half_score'] : 0;
            $baseBattleArr['start_t']['2nd_half_score']  = $baseBattleArr['start_t']['2nd_half_score'] > 0 ? $baseBattleArr['start_t']['2nd_half_score'] : 0;
            $this->setCurrentBattleDetails($baseBattleArr);
        } elseif ($total > 30) {
            $baseBattleArr = [];
            $currentCt     = $this->getCurrentCt();
            $startCt       = $this->getStartingCt();
            if ($currentCt['opponent_order'] == $startCt['opponent_order']) {
                $baseBattleArr['start_ct']['ot_score'] = $round_ct_score - 15;
                $baseBattleArr['start_t']['ot_score']  = $round_t_score - 15;
                $baseBattleArr['start_ct']['ot_score'] = $baseBattleArr['start_ct']['ot_score'] > 0 ? $baseBattleArr['start_ct']['ot_score'] : 0;
                $baseBattleArr['start_t']['ot_score']  = $baseBattleArr['start_t']['ot_score'] > 0 ? $baseBattleArr['start_t']['ot_score'] : 0;
            } else {
                $baseBattleArr['start_ct']['ot_score'] = $round_t_score - 15;
                $baseBattleArr['start_t']['ot_score']  = $round_ct_score - 15;
                $baseBattleArr['start_ct']['ot_score'] = $baseBattleArr['start_ct']['ot_score'] > 0 ? $baseBattleArr['start_ct']['ot_score'] : 0;
                $baseBattleArr['start_t']['ot_score']  = $baseBattleArr['start_t']['ot_score'] > 0 ? $baseBattleArr['start_t']['ot_score'] : 0;
            }
            $this->setCurrentBattleDetails($baseBattleArr);
        }




        return true;
    }

    /**
     * 计算存活人数
     * @param $type
     * @return int
     */
    public function getSurvivedPlayersCountByType($type)
    {
        $player_list = $this->getPlayerListByType($type, 'round');
        $count       = 0;
        foreach ($player_list as $k => $v) {
            if ($v['is_alive']) {
                $count++;
            }
        }
        return $count;
    }
    /**
     * 计算存活人数
     * @param $type
     * @return int
     */
    public function updateSurvivedPlayersSteamIdCountByType($type,$survivedArr)
    {
        $player_list = $this->getPlayerListByType($type, 'round');
        $res = [];
        foreach ($player_list as $k => $v) {
            if ($v['is_alive']) {
                $res[] =$v['steam_id'];
            }
        }
        $arr =[
            'ct'=>'t',
            't'=>'ct',
        ];
        $diffSide = $arr[$type];
        if ($res) {
            $roundDetails = $this->getCurrentRoundDetails();
            if (empty($roundDetails['1vn_' . $type . '_player'])) {
                $updateArr                             = [];
                $updateArr['1vn_' . $type . '_player'] = $res;
                $this->setRoundDetails($updateArr, $this->getRoundOrdinal());
            } elseif ($survivedArr['survived_players_'.$type] == 1 && $survivedArr['survived_players_'.$diffSide]  != 1) {
                return $res;
            } elseif ($survivedArr['survived_players_'.$type] == 1 && $survivedArr['survived_players_'.$diffSide]  == 1) {
                if (empty($roundDetails['1vn_' . $type . '_player_final'])) {
                    $updateArr                                   = [];
                    $updateArr['1vn_' . $type . '_player_final'] = $res;
                    $this->setRoundDetails($updateArr, $this->getRoundOrdinal());
                }

            }

        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function getRoundDetails()
    {
        $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
        return json_decode($this->hGet($roundKey, $this->getRoundOrdinal()), true);
    }

    /**
     * @param $order
     * @return mixed
     */
    public function getRoundDetailsByOrder($order)
    {
        $roundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, self::KEY_TAG_LIST];
        return json_decode($this->hGet($roundKey, $order), true);
    }


    /**
     * 获取Battle选手
     * @param $steamId
     * @return mixed
     */
    public function getBattlePlayerBySteamId($steamId)
    {
        $key = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        return json_decode($this->hGet($key, $steamId), true);
    }

    /**
     * 获取Round选手
     * @param $steamId
     * @return mixed
     */
    public function getRoundPlayerBySteamId($steamId)
    {
        $k = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER];
        return json_decode($this->hGet($k, $steamId), true);
    }

    public function getRoundPlayersByRoundOrderAndBattleId($battleId, $roundOrder)
    {
        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundOrder, self::KEY_TAG_PLAYER];
        $list = $this->hGetAll($key);
        return array_map([__CLASS__, 'mapJson'], $list);
    }
    public function getRoundPlayersByRoundOrderAndBattleIdAndSide($battleId, $roundOrder,$side)
    {
        $returnCtList = [];
        $returnTList  = [];
        $key          = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundOrder, self::KEY_TAG_PLAYER];
        $list         = $this->hGetAll($key);
        $new_list     = array_map([__CLASS__, 'mapJson'], $list);
        foreach ($new_list as $k => $v) {
            if ($v['side'] == 'ct') {
                $returnCtList[$k] = $v;
            }
            if ($v['side'] == 'terrorist') {
                $returnTList[$k] = $v;
            }
        }
        if ($side == 'ct') {
            return $returnCtList;
        }
        if ($side == 'terrorist') {
            return $returnTList;
        }

    }

    public function mapJson($json)
    {
        return json_decode($json, true);
    }

    /**
     * 设置RoundPlayer选手信息
     * @param $steamId
     * @param $updateArr
     * @param string $updateType
     * @param int $changNum
     * @return mixed
     */
    public function setRoundPlayerBySteamId($steamId, $updateArr, $updateType = '', $changNum = 1)
    {
        if (empty($steamId)) {
            return false;
        }

        $res        = $this->getRoundPlayerBySteamId($steamId);
        $resCurrent = $this->getCurrentPlayer($steamId);
        if (empty($updateArr)||$updateArr==null){
            $this->error_hash_log('setRoundPlayerBySteamId',['updateArr'=>$updateArr,'event'=>$this->event]);
            return false;
        }
        if (empty($res)&&empty($resCurrent)){
            $this->error_hash_log('setRoundPlayerBySteamId',['steamId'=>$steamId,'event'=>$this->event]);
            $this->setCurrentPlayerIfHaveSteamId($steamId);
        }
        if (empty($res)&&!empty($resCurrent)){
            $res = $resCurrent;
        }

        if ($updateType == 'incr') {
            foreach ($updateArr as $k => $v) {
                $updateArrNew[$v] = $res[$v] + $changNum;
            }
        } elseif ($updateType == 'decr') {
            foreach ($updateArr as $k => $v) {
                $updateArrNew[$v] = $res[$v] - $changNum > 0 ? $res[$v] - $changNum : 0;
            }
        }
        $updateArr = $updateArrNew ?? $updateArr;
        if (empty($updateArr)) {
            return false;
        }

        $new_res = $res ? array_merge($res, $updateArr) : array_merge($resCurrent, $updateArr);
        //首杀差更新
        $new_res['first_kills_diff'] = @$new_res['first_kills'] - @$new_res['first_deaths'];
        @$new_res['k_d_diff'] = @$new_res['kills'] - @$new_res['deaths'];
        $roundPlayerKey              = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER];
        $info                        = $this->hSet($roundPlayerKey, $steamId, json_encode($new_res,320));
        if ($this->isLive()){
            if ($new_res['side']=='ct' || $new_res['side']=='terrorist' ){


//            $this->hSet($roundPlayerKeyTeam, $this->getRoundOrdinal(), json_encode($new_res_team));
            }
        }

        return $info;
    }

    /**
     * @param $steamId
     * @param $updateArr
     * @param string $updateType
     * @param int $changNum
     */
    public function setPlayerBySteamId($steamId, $updateArr, $updateType = '', $changNum = 1)
    {
        if (!$steamId) {//异常日志
            @$this->event['error_log_time'];
            @$this->error_hash_log('no steamId',$this->event);
            return false;
        }
        $this->setCurrentPlayerBySteamId($steamId, $updateArr, $updateType, $changNum);
        $this->setRoundPlayerBySteamId($steamId, $updateArr, $updateType, $changNum);
        $this->setBattlePlayerBySteamId($steamId, $updateArr, $updateType, $changNum);

    }

    /**
     * 容错初始化player
     * @param $steamId
     */
    public function setCurrentPlayerIfHaveSteamId($steamId)
    {

//        if ($this->isLive()){
//            return false;
//        }
        $playerInfo              = $this->getPlayerInfoBySteamId($steamId);
        $playerArr['playerName'] = $playerInfo ? @$playerInfo['nick_name'] : '';
        $playerArr['nick_name']  = @$playerInfo ? @$playerInfo['nick_name'] : '';
//        if ($playerArr['side'] == 'ct') {
//            $team = $this->getCurrentCt();
//        } else {
//            $team = $this->getCurrentT();
//        }
//        $playerArr['team_log_name'] = @$team['log_name'];
//        $playerArr['team_name']     = @$team['name'];
//        $playerArr['team_id']       = @$team['team_id'];
//        $playerArr['team_order']    = @$team['opponent_order'];

        $playerArr['steam_id']  = @$steamId;
        $playerArr['player_id'] = @$playerInfo['id'];
        $playerArr['status']    = self::PLAYER_STATUS_ONLINE;
        $playerArr              = $this->initPlayerBase($playerArr);

        $this->hSetNx([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST], $steamId, json_encode($playerArr,320));
        $this->hSetNx([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_PLAYER, self::KEY_TAG_LIST], $steamId, json_encode($playerArr,320));
    }

    /**
     * set error_info
     */
    public function error_log($info)
    {
        $this->redis->lPush('error_log', json_encode($info,320));
    }

    /**
     * set error_info
     * @param $key
     * @param $info
     * @return bool|int
     */
    public function error_hash_log($key, $info)
    {
        if (is_array($info)) {
            $info['error_msg']  = $key;
            $info['error_time'] = date('Y-m-d-H:i:s');
            return $this->redis->lPush($this->currentPre.':error_common_log', json_encode($info,320));
        } elseif (is_string($info)) {
            $newInfo               = [];
            $newInfo['error_msg']  = $key;
            $newInfo['info']       = $info;
            $newInfo['error_time'] = date('Y-m-d-H:i:s');
            return $this->redis->lPush($this->currentPre.':error_common_log', json_encode($newInfo,320));
        }
    }
    /**
     * set error_info
     * @param $key
     * @param $info
     * @return bool|int
     */
    public function error_debug_log($key, $info)
    {
        if (is_array($info)) {
            $info['error_msg']  = $key;
            $info['error_time'] = date('Y-m-d-H:i:s');
            return $this->redis->lPush($this->currentPre.':error_debug_log', json_encode($info,320));
        } elseif (is_string($info)) {
            $newInfo               = [];
            $newInfo['error_msg']  = $key;
            $newInfo['info']       = $info;
            $newInfo['error_time'] = date('Y-m-d-H:i:s');
            return $this->redis->lPush($this->currentPre.':error_debug_log', json_encode($newInfo,320));
        }
    }
    /**
     * set error_info
     * @param $key
     * @param $info
     */
    public function error_catch_log($info)
    {
        if (is_array($info)) {
            $info['error_time'] = date('Y-m-d-H:i:s');
        }
        $this->redis->lPush($this->currentPre.':error_catch_log', json_encode($info,320));
    }

    /**
     * set battle player info
     * @param $steamId
     * @param $updateArr ['name'=>'xx','player_id'=>'xx'] special: if $updateType=='incr' ['kills','assist']
     * @param string $updateType incr: statistical data +1
     * @return mixed
     */
    public function setBattlePlayerBySteamId($steamId, $updateArr, $updateType = '', $changNum = 1)
    {
        if (empty($steamId)) {
            return false;
        }
        $res        = $this->getBattlePlayerBySteamId($steamId);
        $resCurrent = $this->getCurrentPlayer($steamId);
        if (empty($updateArr)||$updateArr==null){
            $this->error_hash_log('setBattlePlayerBySteamId',['updateArr'=>$updateArr]);
            return false;
        }
        if (empty($res)&&empty($resCurrent)){
            $this->error_hash_log('setBattlePlayerBySteamId',['steamId'=>$steamId]);
            $this->setCurrentPlayerIfHaveSteamId($steamId);
        }
        if ($updateType == 'incr') {
            foreach ($updateArr as $k => $v) {
                $updateArrNew[$v] = $res[$v] + $changNum;
            }
        } elseif ($updateType == 'decr') {
            foreach ($updateArr as $k => $v) {
                $updateArrNew[$v] = $res[$v] - $changNum > 0 ? $res[$v] - $changNum : 0;
            }
        }
        $updateArr = $updateArrNew ?? $updateArr;
        if (empty($updateArr)) {
            return false;
        }
        if ($res) {
            $new_res = array_merge($res, $updateArr);
        } else {
            $new_res = array_merge($resCurrent, $updateArr);
        }
        //首杀差更新
        $new_res['first_kills_diff'] = @$new_res['first_kills'] - @$new_res['first_deaths'];
        @$new_res['k_d_diff'] = @$new_res['kills'] - @$new_res['deaths'];
        return $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_PLAYER, self::KEY_TAG_LIST], $steamId, json_encode($new_res,320));
    }

    public function setRoundPlayerStatisOnRoundEnd($type){
        $roundDetails                         = $this->getCurrentRoundDetails();
        $new_res = [];
        $new_res['side'] = $type;
        if ($new_res['side'] == 'ct') {
            $roundPlayerKeyTeam = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_TEAM_CT, self::KEY_TAG_LIST];
            $side               = 'ct';
            $team_id            = $roundDetails['ct']['team_id'];
            $survived_players   = $roundDetails['survived_players_ct'];
            $roundAllPlayer     = $this->getRoundPlayersByRoundOrderAndBattleIdAndSide($this->getBattleId(), $this->getRoundOrdinal(),'ct');
        } else {
            $roundPlayerKeyTeam = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_TEAM_T, self::KEY_TAG_LIST];
            $side               = 'terrorist';
            $team_id            = $roundDetails['terrorist']['team_id'];
            $survived_players   = $roundDetails['survived_players_t'];;
            $roundAllPlayer = $this->getRoundPlayersByRoundOrderAndBattleIdAndSide($this->getBattleId(), $this->getRoundOrdinal(),'terrorist');
        }

        $new_res_team                           = [];
        $new_res_team['is_first_kill']          = false;
        $new_res_team['is_first_death']         = false;
        $new_res_team['is_planted_bomb']        = false;
        $new_res_team['is_defused_bomb']        = false;
        $new_res_team['is_one_on_x_clutch']     = false;
        $new_res_team['is_one_on_one_clutch']   = false;
        $new_res_team['is_one_on_two_clutch']   = false;
        $new_res_team['is_one_on_three_clutch'] = false;
        $new_res_team['is_one_on_four_clutch']  = false;
        $new_res_team['is_one_on_five_clutch']  = false;

        foreach ($roundAllPlayer as $k => $v) {

            $new_res_team['kills']            += $v['kills'];
            $new_res_team['headshot_kills']   += $v['headshot_kills'];
            $new_res_team['deaths']           += $v['deaths'];
            $new_res_team['assists']          += $v['assists'];
            $new_res_team['flash_assists']    += $v['flash_assist'];
            if ($v['first_kills']) {
                $new_res_team['is_first_kill'] = true;
            }
            if ($v['first_deaths']) {
                $new_res_team['is_first_death'] = true;
            }
            $new_res_team['damage']                 += $v['real_damage'];
            $new_res_team['team_damage']            += $v['team_damage'];
            $new_res_team['damage_taken']           += $v['damage_taken'];
            $new_res_team['hegrenade_damage_taken'] += $v['hegrenade_damage_taken'];
            $new_res_team['inferno_damage_taken']   += $v['inferno_damage_taken'];
            if ($v['planted_bomb']) {
                $new_res_team['is_planted_bomb'] = true;
            }
            if ($v['defused_bomb']) {
                $new_res_team['is_defused_bomb'] = true;
            }
            $new_res_team['chicken_kills']       += $v['chicken_kills'];
            $new_res_team['blind_enemy_time']    += $v['blind_enemy_time'];
            $new_res_team['blind_teammate_time'] += $v['blind_teammate_time'];
            $new_res_team['team_kills']          += $v['team_kills'];
            $new_res_team['multi_kills']         += $v['multi_kills'];
            $new_res_team['two_kills']           += $v['two_kills'];
            $new_res_team['three_kills']         += $v['three_kills'];
            $new_res_team['four_kills']          += $v['four_kills'];
            $new_res_team['five_kills']          += $v['five_kills'];
            if ($v['one_on_x_clutches']) {
                $new_res_team['is_one_on_x_clutch'] = true;
            }
            if ($v['one_on_one_clutches']) {
                $new_res_team['is_one_on_one_clutch'] = true;
            }
            if ($v['one_on_two_clutches']) {
                $new_res_team['is_one_on_two_clutch'] = true;
            }
            if ($v['one_on_three_clutches']) {
                $new_res_team['is_one_on_three_clutch'] = true;
            }
            if ($v['one_on_four_clutches']) {
                $new_res_team['is_one_on_four_clutch'] = true;
            }
            if ($v['one_on_five_clutches']) {
                $new_res_team['is_one_on_five_clutch'] = true;
            }
            $new_res_team['hit_generic']   += $v['hit_generic'];
            $new_res_team['hit_head']      += $v['hit_head'];
            $new_res_team['hit_chest']     += $v['hit_chest'];
            $new_res_team['hit_stomach']   += $v['hit_stomach'];
            $new_res_team['hit_left_arm']  += $v['hit_left_arm'];
            $new_res_team['hit_right_arm'] += $v['hit_right_arm'];
            $new_res_team['hit_left_leg']  += $v['hit_left_leg'];
            $new_res_team['hit_right_leg'] += $v['hit_right_leg'];
            $new_res_team['awp_kills']     += $v['awp_kills'];
            $new_res_team['knife_kills']   += $v['knife_kills'];
            $new_res_team['taser_kills']   += $v['taser_kills'];
            $new_res_team['shotgun_kills'] += $v['shotgun_kills'];
        }
        $roundUpdateDetails =[];
        if ($new_res['side'] == 'ct') {
            if (!is_array($roundDetails['ct'])){
                $roundDetails['ct'] = [];
            }
            $roundUpdateDetails['ct'] = array_merge($roundDetails['ct'],$new_res_team);
        }else{
            if (!is_array($roundDetails['terrorist'])){
                $roundDetails['terrorist'] = [];
            }
            $roundUpdateDetails['terrorist'] = array_merge($roundDetails['terrorist'],$new_res_team);
        }
        $this->setCurrentRoundDetails($roundUpdateDetails);
    }
    public function setBattlePlayerStatisOnBattleEnd($type){
        $new_res = [];
        $currentCt = $this->getCurrentCt();
        $startingCt = $this->getStartingCt();
        $new_res['side'] = $type;
        if ($currentCt['team_id']!=$startingCt['team_id']){
            //反转阵营
            $arrRev=['ct'=>'t','t'=>'ct'];
            $searchSide = $arrRev[$type];
        }else{
            $searchSide = $type;
        }
        $battleDetails = $this->getCurrentBattleDetails();
        if ($new_res['side'] == 'ct') {
            $battleAllPlayer    = $this->getBattlePlayerListByType($searchSide);
            $countPlayer    = count($battleAllPlayer);
        } else {
            $battleAllPlayer    = $this->getBattlePlayerListByType($searchSide);
            $countPlayer    = count($battleAllPlayer);
        }

        $new_res_team = [];
        foreach ($battleAllPlayer as $k => $v) {
            $new_res_team['kills']                  += $v['kills'];
            $new_res_team['headshot_kills']         += $v['headshot_kills'];
            $new_res_team['deaths']                 += $v['deaths'];
            $new_res_team['kd_diff']                += $v['k_d_diff'];
            $new_res_team['assists']                += $v['assists'];
            $new_res_team['flash_assists']          += $v['flash_assist'];
            $new_res_team['adr']                    += $v['adr'];
            $new_res_team['first_kills']            += $v['first_kills'];
            $new_res_team['first_deaths']           += $v['first_deaths'];
            $new_res_team['first_kills_diff']       += $v['first_kills_diff'];
            $new_res_team['kast']                   += $v['kast'];
            $new_res_team['rating']                 += $v['rating'];
            $new_res_team['damage']                 += $v['real_damage'];
            $new_res_team['team_damage']            += $v['team_damage'];
            $new_res_team['damage_taken']           += $v['damage_taken'];
            $new_res_team['hegrenade_damage_taken'] += $v['hegrenade_damage_taken'];
            $new_res_team['inferno_damage_taken']   += $v['inferno_damage_taken'];
            $new_res_team['planted_bomb']           += $v['planted_bomb'];
            $new_res_team['defused_bomb']           += $v['defused_bomb'];
            $new_res_team['chicken_kills']          += $v['chicken_kills'];
            $new_res_team['blind_enemy_time']       += $v['blind_enemy_time'];
            $new_res_team['blind_teammate_time']    += $v['blind_teammate_time'];
            $new_res_team['team_kills']             += $v['team_kills'];
            $new_res_team['multi_kills']            += $v['multi_kills'];
            $new_res_team['two_kills']              += $v['two_kills'];
            $new_res_team['three_kills']            += $v['three_kills'];
            $new_res_team['four_kills']             += $v['four_kills'];
            $new_res_team['five_kills']             += $v['five_kills'];
            $new_res_team['one_on_x_clutches']      += $v['one_on_x_clutches'];
            $new_res_team['one_on_one_clutches']    += $v['one_on_one_clutches'];
            $new_res_team['one_on_two_clutches']    += $v['one_on_two_clutches'];
            $new_res_team['one_on_three_clutches']  += $v['one_on_three_clutches'];
            $new_res_team['one_on_four_clutches']   += $v['one_on_four_clutches'];
            $new_res_team['one_on_five_clutches']   += $v['one_on_five_clutches'];
            $new_res_team['hit_generic']            += $v['hit_generic'];
            $new_res_team['hit_head']               += $v['hit_head'];
            $new_res_team['hit_chest']              += $v['hit_chest'];
            $new_res_team['hit_stomach']            += $v['hit_stomach'];
            $new_res_team['hit_left_arm']           += $v['hit_left_arm'];
            $new_res_team['hit_right_arm']          += $v['hit_right_arm'];
            $new_res_team['hit_left_leg']           += $v['hit_left_leg'];
            $new_res_team['hit_right_leg']          += $v['hit_right_leg'];
            $new_res_team['awp_kills']              += $v['awp_kills'];
            $new_res_team['knife_kills']            += $v['knife_kills'];
            $new_res_team['taser_kills']            += $v['taser_kills'];
            $new_res_team['shotgun_kills']          += $v['shotgun_kills'];
        }
        if ($countPlayer){
            $new_res_team['adr'] =  round(@$new_res_team['adr'] / $countPlayer, 2);
        }else{
            $new_res_team['adr'] = 0;
        }
        if ($countPlayer){
            $new_res_team['kast'] =  round(@$new_res_team['kast'] / $countPlayer, 4);
        }else{
            $new_res_team['kast'] = 0;
        }
        if ($countPlayer){
            $new_res_team['rating'] =  round(@$new_res_team['rating'] / $countPlayer, 2);
        }else{
            $new_res_team['rating'] = 0;
        }



        $battleUpdateDetails =[];
        if ($new_res['side'] == 'ct') {
            if (!is_array($battleDetails['start_ct'])){
                $battleDetails['start_ct'] = [];
            }
            $battleUpdateDetails['start_ct'] = array_merge($battleDetails['start_ct'],$new_res_team);
        }else{
            if (!is_array($battleDetails['start_t'])){
                $battleDetails['start_t'] = [];
            }
            $battleUpdateDetails['start_t'] = array_merge($battleDetails['start_t'],$new_res_team);
        }
        $this->setCurrentBattleDetails($battleUpdateDetails);
    }


    /**
     * 获取round下所有的选手
     * @param string $type
     * @param string $source
     * @param string $customKey
     * @return array|array[]|bool
     */
    public function getPlayerListByType($type = 'all', $source = '',$customKey='')
    {
        $list = $this->hGetAll([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST]);
        if ($source == 'round') {
            $roundPlayerKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_PLAYER];
            $list           = $this->hGetAll($roundPlayerKey);
        }
        $part_list_ct = [];
        $part_list_t  = [];
        foreach ($list as $k => $v) {
            $v_arr = json_decode($v, true);
            if ($v_arr['side'] == 'ct') {
                $part_list_ct[] = $v_arr;
            } elseif ($v_arr['side'] == 'terrorist') {
                $part_list_t[] = $v_arr;
            }
        }
        if ($type == 'all') {
            $all_list = array_merge($part_list_t, $part_list_ct);
            if ($customKey == 'steam_id') {
                $newAllList = [];
                if (!empty($all_list)) {
                    foreach ($all_list as $k => $v) {
                        $newAllList[$v['steam_id']] = $v;
                    }
                }
                return $newAllList;
            } else {
                return $all_list;
            }
        } elseif ($type == 'ct') {
            return $part_list_ct;
        } elseif ($type == 't') {
            return $part_list_t;
        } else {
            return false;
        }
    }

    public function getEventsByBattleIdAndRoundOrder($battleId, $roundOrder)
    {
//        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundOrder, self::KEY_TAG_EVENTS];
        $key  = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_ROUND, $roundOrder, self::KEY_TAG_MAIN_EVENTS];
        $list = $this->lRange($key, 0, -1);
        return $list ? array_reverse(array_map([__CLASS__, 'mapJson'], $list)) : null;
    }

    /**
     * 获取battle下所有的选手
     * @param string $type
     * @param string $battleId
     * @param string $customKey
     * @return array|bool
     */
    public function getBattlePlayerListByType($type = 'all', $battleId = '',$customKey='')
    {
        if (!$battleId) {
            $battleId = $this->getBattleId();
        }
        $key      = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $battleId, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        $list     = $this->hGetAll($key);
        $list_ct  = [];
        $list_t   = [];
        $list_all = [];
        foreach ($list as $k => $v) {
            $v_arr = json_decode($v, true);
            if ($v_arr) {
                if ($v_arr['steam_id'] != 'BOT') {
                    if (@$v_arr['side'] == 'ct') {
                        $list_ct[] = $v_arr;
                    } elseif (@$v_arr['side'] == 'terrorist') {
                        $list_t[] = $v_arr;
                    }
                }

            }

        }
        $list_ct = $this->sortArrayByField($list_ct, 'rating', SORT_DESC);
        $list_t  = $this->sortArrayByField($list_t, 'rating', SORT_DESC);
        if ($type == 'all') {
            foreach ($list as $k => $v) {
                $v_arr = json_decode($v, true);
                if ($v_arr) {
                    if ($v_arr['steam_id'] != 'BOT') {
                        if ($customKey){
                            $list_all[$v_arr['steam_id']] = $v_arr;
                        }else{
                            if (@$v_arr['team_id']) {
                                $list_all[] = $v_arr;
                            }
                        }

                    }
                }
            }
        }

        $part_list_t  = $list_t;
        $part_list_ct = $list_ct;
//        $arrayPlayerIdAll = array_column($list_all,'player_id');
//        if (!empty($arrayPlayerIdAll)){
//            $playerUn = array_unique($arrayPlayerIdAll);
//            $repeat_arr = array_diff_assoc ( $arrayPlayerIdAll, $playerUn );
//            if (!empty($repeat_arr)){
//                foreach ($list_all as $k=>$v){
//                    if (in_array($v['player_id'],$repeat_arr)){
//                        unset($list_all[$k]);
//                        break;
//                    }
//                }
//            }
//        }

//        $part_list_t  = array_slice($list_t, 0, 5);
//        $part_list_ct = array_slice($list_ct, 0, 5);
        if ($type == 'all') {
            if (!$customKey){
                $list_all = array_values($list_all);
            }
            return $list_all;
        } elseif ($type == 'ct') {
            $part_list_ct = array_values($part_list_ct);
            return $part_list_ct;
        } elseif ($type == 't') {
            $part_list_t = array_values($part_list_t);
            return $part_list_t;
        } else {
            return false;
        }

    }


    /**
     * 击杀：（这里要更新死亡的选手身上的道具装备为空）
     * 事件类型：主事件（时间、击杀、击杀者选手昵称、击杀者Ping、击杀者Steam ID、击杀者阵营、击杀者坐标、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被击杀者阵营、被击杀者坐标、武器、击杀特殊描述）
     * 选手：
     * 击杀：击杀敌人，击杀+1；击杀队友，击杀-1
     * 爆头击杀：击杀敌人的情况下，攻击命中的部位是头，爆头击杀+1；击杀队友，不处理
     * 是否死亡：被击杀、自杀，置为死亡
     * 助攻：助攻队友击杀敌人，助攻+1；助攻队友击杀队友，不处理；助攻敌人自杀，助攻+1；助攻队友自杀，不处理
     * 闪光弹助攻：闪光弹助攻队友击杀敌人，闪光弹助攻+1；闪光弹助攻队友击杀队友，不处理；闪光弹助攻敌人自杀，闪光弹助攻+1；闪光弹助攻队友自杀，不处理
     * 伤害：攻击敌人，以敌人当前血量上限为伤害上限，累加造成的伤害；攻击队友，不处理
     * 首杀：首先击杀
     * 首死：首先被击杀或自杀
     * 多杀：击杀>=2
     * 1onX clutch：后放
     * 刀杀：击杀的攻击子事件，使用的武器包含knife字样
     * ace：击杀>=5
     * 战队：
     * 存活人数：未死亡选手合计
     * 击杀：选手合计
     * 爆头击杀：选手合计
     * @param $data
     * @param $event
     * @return mixed
     */
    public function player_kill($data, $event)
    {
        $this->setBattlePause(false);
        //容错round不存在
        $this->setCurrentRoundDetails([]);
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['killer']['player_id']);
        $this->setCurrentPlayerIfHaveSteamId($event['info']['deader']['player_id']);
        //is diff side
        $diff_side = $event['info']['killer']['camp'] != $event['info']['deader']['camp'];
        //round details
        $round               = $this->getRoundDetails();
        $realPlayerName      = null;
        $realFlashPlayerSide = null;
        $realFlashPlayerName = null;
        /**
         * killer
         */
        $killerPlayerSteamId = $event['info']['killer']['player_id'];
        if ($diff_side) {
            //statistics fields
            $updateArrStatistics = [];
            $killer_side         = $event['info']['killer']['camp'];
            if ($event['info']['is_headshot'] == 1) {
                $updateArrStatistics[] = 'headshot_kills';
                //update round details
                $baseArr                                   = [];
                $baseArr[$killer_side . '_headshot_kills'] = $round[$killer_side . '_headshot_kills'] + 1;
                $this->setCurrentRoundDetails($baseArr);
            }
            if (!$round['kills']) {
                $this->record_first_kill = true;
                $updateArrStatistics[] = 'first_kills';
                //update round details first_kills_side,first_death_side
                $baseRoundArr                     = [];
                $baseRoundArr['first_kills_side'] = $killer_side;
                $baseRoundArr['first_death_side'] = $event['info']['deader']['camp'];
                if ($event['info']['is_headshot'] == 1) {
                    $baseRoundArr['is_first_kills_headshot_side'] = $killer_side;
                }
                $this->setCurrentRoundDetails($baseRoundArr);
            }else{
                $this->record_first_kill = false;
            }
            //knife kill
            $withWeapon = $event['info']['with'];
            if (strstr($withWeapon, 'knife')) {
                $updateArrStatistics[] = 'knife_kill';
                //update round details
                $baseArr[$killer_side . '_is_knife_kill'] = true;
                $this->setCurrentRoundDetails($baseArr);
            }
            //position
            $this->setPlayerBySteamId($killerPlayerSteamId, ['position'=>@$event['info']['killer']['coordinate']]);
            //ping
            $this->setPlayerBySteamId($killerPlayerSteamId, ['ping'=>@$event['info']['killer']['ping']]);
            //kills
            $updateArrStatistics[] = 'kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');

            //multi_kills (every round do once)
            $roundPlayer = $this->getRoundPlayerBySteamId($killerPlayerSteamId);
            if (!empty($roundPlayer)){
                //ace_kills (every round do once)
                if ($roundPlayer['kills'] == 5) {
                    $this->record_ace_kill = true;
                    //status fields
                    $updateArrStatus             = [];
                    $updateArrStatus['ace_kill'] = 1;
                    $this->setRoundPlayerBySteamId($killerPlayerSteamId, $updateArrStatus);
                    $updateArrStatistics   = [];
                    $updateArrStatistics[] = 'ace_kill';
                    $updateArrStatistics[] = 'five_kills';

                    $this->setBattlePlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
                    $this->setCurrentPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
                    $baseArr                  = [];
                    $baseArr['ace_kill_side'] = $killer_side;
                    $this->setCurrentRoundDetails($baseArr);
                }
            }

        } else {
            //statistics fields
            $updateArrStatistics = ['kills'];
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'decr');
            //team_kills
            $this->record_team_kill = true;
            $updateArrStatistics2 = [];
            $updateArrStatistics2[] = 'team_kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics2, 'incr');
        }

        /**
         * deader
         */
        $deaderPlayerSteamId = $event['info']['deader']['player_id'];
        //statistics fields
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'deaths';
        if (!$round['deaths']) {
            $updateArrStatistics[] = 'first_deaths';
        }
        $this->setPlayerBySteamId($deaderPlayerSteamId, $updateArrStatistics, 'incr');
        //update round deaths
        $baseArr           = [];
        $baseArr['deaths'] = $round['deaths'] + 1;
        $this->setCurrentRoundDetails($baseArr);
        //status fields
        $updateArrStatus             = [];
        $updateArrStatus['is_alive'] = false;
        $updateArrStatus['ping']     = @$event['info']['deader']['ping'];
        $this->setPlayerBySteamId($deaderPlayerSteamId, $updateArrStatus);
        //clear All
        $this->clearAllIfPlayerDeadBySteamId($deaderPlayerSteamId);

        if ($diff_side){
            $updatePlayerLogArr = [];
            $updatePlayerLogArr['killer_steam_id'] = $killerPlayerSteamId;
            $updatePlayerLogArr['victim_steam_id'] = $deaderPlayerSteamId;
            $updatePlayerLogArr['log_time'] = $event['info']['log_time'];
            $killEventsKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(), self::KEY_TAG_KILL_EVENTS];
            $this->lPush($killEventsKey, json_encode($updatePlayerLogArr,320));
            $listKillEvents = $this->lRange($killEventsKey,0,-1);
            if (!empty($listKillEvents)){
                foreach ($listKillEvents as $k=>$v){
                    $arrV = json_decode($v,true);
                    if ((strtotime($event['info']['log_time']) - strtotime($arrV['log_time']) < 4) && $arrV['killer_steam_id'] == $deaderPlayerSteamId) {
                        $update = [];
                        $update['trade'] = 1;
                        $this->setRoundPlayerBySteamId($arrV['victim_steam_id'],$update);
                    }
                }
            }
        }
        //position
        $this->setPlayerBySteamId($deaderPlayerSteamId, ['position'=>@$event['info']['deader']['coordinate']]);

        /**
         * assist
         */
        if (isset($event['info']['ext_log']['assist'])) {
            $assist_list = $event['info']['ext_log']['assist'];
            foreach ($assist_list as $k => $v) {
                //find real assist by steamId
                if ($v['victimSteamId'] == $event['info']['deader']['player_id']) {
                    //is diff side
                    $diff_side = @$event['info']['ext_log']['assist'][$k]['assistsPlayerCamp'] != $event['info']['deader']['camp'];
                    if ($diff_side) {
                        $assistPlayerSteamId = $v['assistsPlayerSteamId'];
                        //statistics fields
                        $updateArrStatistics   = [];
                        $updateArrStatistics[] = 'assists';
                        $this->setPlayerBySteamId($assistPlayerSteamId, $updateArrStatistics, 'incr');
                        $realPlayerSteamId = $v['assistsPlayerSteamId'];
                        $realPlayerSide    = $v['assistsPlayerCamp'];
                        $realPlayerName    = $v['assistsPlayerNickName'];
                    }

                }
            }

        }

        /**
         * flash assist
         */
        if (isset($event['info']['ext_log']['flashassist'])) {
            $flash_assist_list = $event['info']['ext_log']['flashassist'];

            foreach ($flash_assist_list as $k => $v) {
                //find real flash assist by steamId
                if ($v['victimSteamId'] == $event['info']['deader']['player_id']) {
                    //is diff side
                    $diff_side = @$event['info']['ext_log']['flashassist'][$k]['assistsPlayerCamp'] != $event['info']['deader']['camp'];
                    if ($diff_side) {
                        $flashAssistPlayerSteamId = $v['assistsSteamId'];
                        //statistics fields
                        $updateArrStatistics   = [];
                        $updateArrStatistics[] = 'flash_assist';
                        $this->setPlayerBySteamId($flashAssistPlayerSteamId, $updateArrStatistics, 'incr');
                        $realFlashPlayerSteamId = $v['assistsSteamId'];
                        $realFlashPlayerSide    = $v['assistsPlayerCamp'];
                        $realFlashPlayerName    = $v['assistsPlayerNickName'];
                    }
                }

            }

        }

        $real_damage =0;
        $real_body ='';
        /**
         * attacked
         */
        if (isset($event['info']['ext_log']['attacked'])) {
            $attacked_list = $event['info']['ext_log']['attacked'];
            foreach ($attacked_list as $k => $v) {
                //find real attacked by steamId
                if ($v['attackSteamId'] == $event['info']['killer']['player_id'] && $v['victimSteamId'] == $event['info']['deader']['player_id']) {
                    $real_damage = @$v['damage'] ?? null;
                    $real_body   = @$v['body'] ?? null;
                }
            }
        }

        /**
         * round
         */
        $killer_side                          = $event['info']['killer']['camp'];
        $updateRound[$killer_side . '_kills'] = $round[$killer_side . '_kills'] + 1;
        $updateRound['kills']                 = $round['kills'] + 1;
        $this->setCurrentRoundDetails($updateRound);
        //update round survived_players
        $survivedArr = $this->updateRoundSurvived();
        if ($survivedArr['survived_players_ct'] == 1 || $survivedArr['survived_players_t'] == 1) {
            $this->updateSurvivedPlayersSteamIdCountByType('ct',$survivedArr);
            $this->updateSurvivedPlayersSteamIdCountByType('t',$survivedArr);
        }
        $killerInfo            = $this->getPlayerInfoBySteamId($killerPlayerSteamId);
        $deaderInfo            = $this->getPlayerInfoBySteamId($deaderPlayerSteamId);
        $assistPlayerInfo      = $this->getPlayerInfoBySteamId(@$realPlayerSteamId);
        $flashAssistPlayerInfo = $this->getPlayerInfoBySteamId(@$realFlashPlayerSteamId);

        //data
        //killer
        $data['killer']['player_id'] = @$killerInfo['id'] ?(int)$killerInfo['id']: null;
        $data['killer']['nick_name'] = @$killerInfo['nick_name'] ?? $event['info']['killer']['name'];
        $data['killer']['steam_id'] = SteamHelper::playerConversionSteamId($killerPlayerSteamId);
        $data['killer']['side']      = @$event['info']['killer']['camp'];
        $data['killer']['position']  = @$event['info']['killer']['coordinate'];
        //victim
        $data['victim']['player_id'] = @$deaderInfo['id']?(int)$deaderInfo['id']:null;
        $data['victim']['nick_name'] = @$deaderInfo['nick_name'] ?? $event['info']['deader']['name'];
        $data['victim']['steam_id'] = SteamHelper::playerConversionSteamId($deaderPlayerSteamId);
        $data['victim']['side']      = @$event['info']['deader']['camp'];
        $data['victim']['position']  = @$event['info']['deader']['coordinate'];
        //assist
        if ($assistPlayerInfo['id']){
            $data['assist']['player_id'] = isset($assistPlayerInfo['id']) ? (int)$assistPlayerInfo['id'] : null;
            $data['assist']['nick_name'] = isset($assistPlayerInfo['nick_name']) ? $assistPlayerInfo['nick_name'] : (isset($event['info']['ext_log']['assist']) ? $realPlayerName : null);
            $data['assist']['steam_id'] = SteamHelper::playerConversionSteamId($realPlayerSteamId);
            $data['assist']['side'] = @$realPlayerSide ? @$realPlayerSide : null;
        }else{
            $data['assist'] = null;
        }

        //flashassist
        if ($flashAssistPlayerInfo['id']){
            $data['flashassist']['player_id'] = isset($flashAssistPlayerInfo['id']) ? (int)$flashAssistPlayerInfo['id'] : null;
            $data['flashassist']['nick_name'] = isset($flashAssistPlayerInfo['nick_name']) ? $flashAssistPlayerInfo['nick_name'] : (isset($event['info']['ext_log']['flashassist']) ? $realFlashPlayerName : null);
            $data['flashassist']['steam_id'] = SteamHelper::playerConversionSteamId($realFlashPlayerSteamId);
            $data['flashassist']['side']      = $realFlashPlayerSide ? $realFlashPlayerSide : null;
        }else{
            $data['flashassist'] = null;
        }
        //weapon
        $with = $event['info']['with'];
        if ($with == 'awp') {
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'awp_kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
        }elseif (strstr($with,'knife')){
            $this->record_knife_kill = true;
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'knife_kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
        }elseif ($with == 'taser'){
            $this->record_taser_kill = true;
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'taser_kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
        }
        elseif ($with == 'mag7' || $with == 'sawedoff' || $with == 'xm1014'){
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'shotgun_kills';
            $this->setPlayerBySteamId($killerPlayerSteamId, $updateArrStatistics, 'incr');
        }
        $weaponInfo                       = $this->getWeaponByName($with);
        if ($weaponInfo['id']){
            $data['weapon']['weapon_id']     = @(int)$weaponInfo['id'];
            $data['weapon']['name']          = @$weaponInfo['name'];

            $data['weapon']['external_id']   = !empty($weaponInfo['external_id']) ? $weaponInfo['external_id'] : null;
            $data['weapon']['external_name'] = @$weaponInfo['external_name'];
            $data['weapon']['kind']     = @$weaponInfo['kind_name'];
            $data['weapon']['slug'] = @$weaponInfo['slug'];

            $data['weapon']['image']         = ImageConversionHelper::showMfitSizeConversion(@$weaponInfo['image'],28,0);
        }else{
            $data['weapon'] = null;
        }

        //other

        $data['damage']              = (int)$real_damage;
        $data['hit_group']           = $real_body;
        $data['is_headshot']         = $event['info']['is_headshot'] ? ($event['info']['is_headshot'] == 1 ? true : false) : null;
        $data['special_description'] = @$event['info']['special_description']?$event['info']['special_description']:null;


        return $data;
    }


    /**
     * 选手更换阵营
     * @param $data
     * @param $event
     * @return mixed
     */
    public function player_switch_team($data, $event)
    {
        $steamId = $event['info']['steamId'];
        //初始化选手
        $this->initPlayer($event);
        return $data;
    }

    /**
     * 助攻
     * 时间、助攻、助攻者选手昵称、助攻者Ping、助攻者Steam ID、助攻者阵营、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被攻击者阵营
     * @param $data
     * @param $event
     * @return mixed
     */
    public function assist($data, $event)
    {
        //只在子事件中处理,这里不需要处理
        return $data;
    }

    /**
     * 攻击
     * 这里要更新攻击选手身上的武器，更新被攻击选手血量、护甲值，更新被攻击选手受到总伤害大于40排名第1的选手（自杀判定助攻时候用）
     * 字段:时间、攻击、攻击者选手昵称、攻击者Ping、攻击者Steam ID、攻击者阵营、被攻击者选手昵称、被攻击者Ping、被攻击者Steam ID、
     *      被攻击者阵营、武器、武器伤害、造成护甲损伤、被攻击者剩余血量、被攻击者剩余护甲、命中部位
     * @param $data
     * @param $event
     * @return mixed
     */
    public function attacked($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['victimSteamId']);
        $this->setCurrentPlayerIfHaveSteamId($event['info']['attackSteamId']);
        // victim(battle)
        $victimPlayerSteamId = $event['info']['victimSteamId'];
        $victimPlayerInfoArr = $this->getBattlePlayerBySteamId($victimPlayerSteamId);

        // attacked(battle)
        $attackedPlayerSteamId = $event['info']['attackSteamId'];

        //hp real_damage
        $hp             = $event['info']['health'];
        $victimLocation = $event['info']['victimLocation'];
        $attackLocation = $event['info']['attackLocation'];
        $real_damage = $victimPlayerInfoArr['hp'] - $event['info']['health'];
        $real_damage = $real_damage <= $event['info']['damage'] ? $real_damage : $event['info']['damage'];
        $diff_side   = $event['info']['victimCamp'] != $event['info']['attackCamp'];

        //attacked
        if ($diff_side) {
            //real_damage
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'real_damage';
            $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr', $real_damage);

            //damage
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'damage';
            $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr', $event['info']['damage']);

            //adr(battle)
            if ($this->getIsUseCountRoundsByBattleId()!=0){
                $playerInfoArr        = $this->getBattlePlayerBySteamId($attackedPlayerSteamId);
                $playerInfoArr['adr'] = round(@$playerInfoArr['real_damage'] / $this->getIsUseCountRoundsByBattleId(), 2);
                $this->setBattlePlayerBySteamId($attackedPlayerSteamId, $playerInfoArr);
            }

        }else{
            //team_damage
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'team_damage';
            $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr', $real_damage);

        }
        //position
        $updateArrStatistics = [];
        $updateArrStatistics['position'] = $attackLocation;
        $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics);


        //victim
        $updateArrStatus       = [];
        $updateArrStatus['hp'] = $hp;
        $updateArrStatus['position'] = $victimLocation;
        $this->setPlayerBySteamId($victimPlayerSteamId, $updateArrStatus);
        //damage_taken
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'damage_taken';
        $this->setPlayerBySteamId($victimPlayerSteamId, $updateArrStatistics, 'incr', $real_damage);
        //with
        $with = $event['info']['with'];
        if ($with){
            if ($with == 'hegrenade') {
                //incgrenade_damage_taken
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hegrenade_damage_taken';
                $this->setPlayerBySteamId($victimPlayerSteamId, $updateArrStatistics, 'incr', $real_damage);
            }
            if ($with == 'inferno') {
                //incgrenade_damage_taken
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'inferno_damage_taken';
                $this->setPlayerBySteamId($victimPlayerSteamId, $updateArrStatistics, 'incr', $real_damage);
            }
        }
        //body
        $body = $event['info']['body'];
        if ($body){
            if ($body == 'generic') {
                //hit_generic
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_generic';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'head') {
                //hit_head
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_head';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'chest') {
                //hit_chest
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_chest';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'stomach') {
                //hit_stomach
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_stomach';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'left arm') {
                //hit_left_arm
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_left_arm';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'right arm') {
                //hit_right_arm
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_right_arm';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'left leg') {
                //hit_left_leg
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_left_leg';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
            if ($body == 'right leg') {
                //hit_right_leg
                $updateArrStatistics   = [];
                $updateArrStatistics[] = 'hit_right_leg';
                $this->setPlayerBySteamId($attackedPlayerSteamId, $updateArrStatistics, 'incr');
            }
        }
        return $data;
    }

    public function getIsUseCountRoundsByBattleId()
    {
        $rounds = $this->getRoundsByBattleId($this->getBattleId());
        $count  = 0;
        foreach ($rounds as $k => $v) {
            if ($v['is_use']) {
                $count++;
            }
        }
        return $count;
    }


    /**
     * 闪光弹助攻
     * 时间、闪光弹助攻、闪光弹助攻者选手昵称、闪光弹助攻者Ping、闪光弹助攻者Steam ID、闪光弹助攻者阵营、被击杀者选手昵称、被击杀者Ping、被击杀者Steam ID、被攻击者阵营
     * @param $data
     * @param $event
     * @return mixed
     */
    public function flashassist($data, $event)
    {
        //只在作为子事件时处理,这里不处理
        return $data;
    }

    /**
     * 闪光弹致盲
     * @param $data
     * @param $event
     * @return mixed
     */
    public function flash_blindness($data, $event)
    {
        $assistsSteamId = $event['info']['assistsSteamId'];
        //is diff side
        $diff_side = $event['info']['assistsPlayerCamp'] != $event['info']['victimPlayerCamp'];
        if ($diff_side){
            $updateArr = [];
            $updateArr[] = 'blind_enemy_time';
            $this->setPlayerBySteamId($assistsSteamId, $updateArr,'incr', $event['info']['flash_time']);
        }else{
            $updateArr = [];
            $updateArr[] = 'blind_teammate_time';
            $this->setPlayerBySteamId($assistsSteamId, $updateArr,'incr', $event['info']['flash_time']);
        }

        $this->setFlashBlindnessTime($event);
        return $data;
    }

    public function setFlashBlindnessTime($event)
    {
        $updateArr['victimSteamId']     = $event['info']['victimSteamId'];
        $updateArr['flash_time']        = $event['info']['flash_time'];
        $updateArr['log_time']          = $event['info']['log_time'];
        $updateArr['assistsSteamId']    = $event['info']['assistsSteamId'];
        $updateArr['assistsPlayerCamp'] = $event['info']['assistsPlayerCamp'];
        $updateArr['victimPlayerCamp']  = $event['info']['victimPlayerCamp'];
        $log_time_m                     = substr($event['info']['log_time'], -3, 3);
        $a                              = substr($event['info']['log_time'], 0, -4);
        $updateArr['expire_time']       = strtotime(substr($event['info']['log_time'], 0, -4)) * 1000 + ($event['info']['flash_time'] * 1000 + $log_time_m);
        $c                              = strtotime($updateArr['expire_time']);
        return $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_FLASH_BLINDNESS], $event['info']['victimSteamId'], json_encode($updateArr,320));
    }

    public function getFlashBlindnessTime($steamId)
    {
        $blindness = $this->hGet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_FLASH_BLINDNESS], $steamId);
        if ($blindness) {
            $blindnessArr = json_decode($blindness, true);
            if ($blindnessArr['flash_time'] >= 0) {
//                $log_time_m = strtotime(substr($this->event['info']['log_time'],0,-4));
//                $log_time_end = substr($this->event['info']['log_time'],-3,3);
////                $expire = substr($blindnessArr['expire_time'],0,-3);
//                $expire = substr($blindnessArr['expire_time'],-3,3);
//                $time = round(substr($blindnessArr['expire_time'],0,-3)-$log_time_m +($expire-$log_time_end)/1000,2);
                $log_time_m   = strtotime(substr($this->event['info']['log_time'], 0, -4));
                $log_time_end = substr($this->event['info']['log_time'], -3, 3);
                $b            = $blindnessArr['expire_time'] - ($log_time_m . $log_time_end);
                $time         = round(($blindnessArr['expire_time'] - ($log_time_m . $log_time_end)) / 1000, 2);
                if ($time <= 0) {
                    $blindnessArr['flash_time'] = 0;
                    $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_FLASH_BLINDNESS], $steamId, json_encode($blindnessArr,320));
                } else {
                    $blindnessArr['flash_time'] = round($time, 2);
                    $this->hSet([self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_FLASH_BLINDNESS], $steamId, json_encode($blindnessArr,320));
                }

                return $blindnessArr['flash_time'];
            }
        } else {
            return 0;
        }


    }

    /**
     * C4炸死
     * @param $data
     * @return mixed
     */
    public function c4_kill($data, $event)
    {
        $this->setPlayerBySteamId($event['info']['playID'], ['is_alive' => false, 'position' => $event['info']['coordinate']]);
        return $data;
    }

    /**
     * 回档位置
     * @param $data
     * @return mixed
     */
    public function round_to($data,$event)
    {
        return $data;
    }

    /**
     * switch
     * @param $data
     * @return mixed
     */
    public function battle_switch_team($data,$event)
    {
        //switch team
        $team_ct = $this->getCurrentCt();
        $team_t  = $this->getCurrentT();
        $this->setTeamCt($team_t);
        $this->setTeamt($team_ct);
        //switch score
        $score_ct = $this->getCtScore();
        $score_t  = $this->getTScore();
        $this->setRoundCtScore($score_t);
        $this->setRoundTScore($score_ct);
        //data
        $data['map'] = $this->getCurrentMap();
        $currentCt =      $this->getCurrentCt();
        $currentT =      $this->getCurrentT();
        $data['current_ct'] = $this->outPutTeam($currentCt);
        $data['current_t'] = $this->outPutTeam($currentT);
        return $data;
    }

    public function setRoundCtScore($value)
    {
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_CT_SCORE], $value);
    }

    public function setRoundTScore($value)
    {
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_T_SCORE], $value);
    }

    public function incrMatch1Score()
    {
        return $this->incr([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE]);
    }
    public function incrMatch2Score()
    {
        return $this->incr([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE]);
    }

    public function setMatch1Score($value)
    {
//        if ($value>3){
//            $this->error_hash_log('setMatch1Score',['value'=>$value,'event'=>$this->event]);
//            return  false;
//        }
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_1_SCORE], $value);
    }

    public function setMatch2Score($value)
    {
//        if ($value>3){
//            $this->error_hash_log('setMatch2Score',['value'=>$value,'event'=>$this->event]);
//            return false;
//        }
        return $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_BATTLE_2_SCORE], $value);
    }

    public function setBombPlanted($value)
    {
        if ($value == false && $this->event['event_type'] == 'bomb_defused') {
            $this->temp_time_since_plant = $this->getTimeSincePlant($this->event);
        }
        //更新炸弹是否已安放(frame使用)
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_BOMB_PLANTED], $value);
        if ($this->event['event_type'] == 'bomb_planted'){
            //update round details
            $baseArr                    = [];
            $baseArr['is_bomb_planted'] = $value;
            $this->setCurrentBattleDetails($baseArr);
            return $this->setCurrentRoundDetails($baseArr);
        }

    }

    public function setTimeSincePlant($value)
    {
        //更新炸弹是否已安放(frame使用)
        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_TIME_SINCE_PLANT], $value);
        //update round details
        $baseArr['bomb_planted_at'] = $value;
        return $this->setCurrentRoundDetails($baseArr);
    }

    /**
     * 炸弹安放
     * 时间、安放炸弹、安放炸弹者昵称、安放炸弹者ping、安放炸弹者Steam ID、安放炸弹者阵营
     * @param $data
     * @param $event
     * @return mixed
     */
    public function bomb_planted($data, $event)
    {
        //set round bomb_planted_at,is_bomb_planted
        $this->setTimeSincePlant($event['info']['log_time']);
        $this->setBombPlanted(true);
        //bomb_planted
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'planted_bomb';
        //steamId
        $steamId = $event['info']['playID'];
        $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        $playerInfo                  = $this->getRoundPlayerBySteamId($steamId);
        $data['player_id']           = @$playerInfo['player_id']?(int)$playerInfo['player_id']:null;
        $data['nick_name']           = isset($playerInfo['nick_name']) ? $playerInfo['nick_name'] : $event['info']['playerName'];
        $data['steam_id']           = SteamHelper::playerConversionSteamId($steamId);
        $data['side']                = $event['info']['camp'];
        $data['position']            = '';
        $data['survived_players_ct'] = $this->getSurvivedPlayersCountByType('ct');
        $data['survived_players_t']  = $this->getSurvivedPlayersCountByType('t');
        $this->record_bomb_planted = true;
        return $data;
    }

    /**
     * 炸弹拆除
     * @param $data
     * @return mixed
     */
    public function bomb_defused($data, $event)
    {
        $this->setBombPlanted(false);
//        $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_IS_BOMB_PLANTED], false);
        //bomb_planted
        $updateArrStatistics   = [];
        $updateArrStatistics[] = 'defused_bomb';
        //steamId
        $steamId = $event['info']['playID'];
        $this->setPlayerBySteamId($steamId, $updateArrStatistics, 'incr');
        $player            = $this->getPlayerInfoBySteamId($event['info']['playID']);
        $data['player_id'] = !empty($player)?@(int)$player['id']:null;
        $data['nick_name'] = !empty($player)?@$player['nick_name']:@$event['info']['playerName'];
        $data['steam_id']           = SteamHelper::playerConversionSteamId($steamId);
        $data['side']      = $event['info']['camp'];
        $data['position']  = '';//没有值可以取
        return $data;
    }

    /**
     * 离开购买区域：（这里要更新选手身上的道具装备）
     * 时间、选手昵称、Ping、Steam ID、阵营、离开购买区、身上的武器道具
     * @param $data
     * @param $event
     * @return mixed
     */
    public function left_buy_zone_with($data, $event)
    {
        //暂时不更新装备
//        foreach ($event['info']['detail']['arms'] as $val) {
//            $this->addRoundPlayerWeaponsOrGrenadesBySteamId($event['info']['playID'], $val,$event['event_type']);
//        }
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['playID']);
        return $data;
    }


    /**
     * 选手自杀
     * update hp,is_alive...
     * update round survived_players
     * @param $data
     * @return mixed
     */
    public function player_suicide($data, $event)
    {
        //容错current player
        $this->setCurrentPlayerIfHaveSteamId($event['info']['playID']);
        $player            = $this->getPlayerInfoBySteamId($event['info']['playID']);
        $data['event_type'] = 'player_suicide';
        $data['player_id'] = !empty($player)?@(int)$player['id']:null;
        $data['nick_name'] =!empty($player)?@$player['nick_name']:$event['info']['playerName'];
        $data['steam_id']           = SteamHelper::playerConversionSteamId($event['info']['playID']);
        $data['side']      = $event['info']['player_camp'];
        $data['position']  = $event['info']['coordinate'];
        $weaponData        = $this->getWeaponByName($event['info']['with']);
        if (empty($event['info']['ext_log']['c4_kill'])) {
            //position
            $updateArrStatistics   = [];
            $updateArrStatistics['position'] = $event['info']['coordinate'];
            $this->setPlayerBySteamId($event['info']['playID'], $updateArrStatistics);
            //statistics fields
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'deaths';
            $this->setPlayerBySteamId($event['info']['playID'], $updateArrStatistics, 'incr');
            //statistics fields
            $updateArrStatistics   = [];
            $updateArrStatistics[] = 'kills';
            $this->setPlayerBySteamId($event['info']['playID'], $updateArrStatistics, 'decr');
        } else {
            //if c4_kill replace weapon name,id..
            $weaponData = $this->getWeaponByName('bomb');
        }
        //update hp,is_alive...
        $this->clearAllIfPlayerDeadBySteamId($event['info']['playID']);
        //update round survived_players
        $this->updateRoundSurvived();
        if ($weaponData['id']){
            $data['weapon']['weapon_id']     = @(int)$weaponData['id'];
            $data['weapon']['name']          = @$weaponData['name'];

            $data['weapon']['external_id']   = @$weaponData['external_id'];
            $data['weapon']['external_name'] = @$weaponData['external_name'];
            $data['weapon']['kind']     = @$weaponData['kind_name'];
            $data['weapon']['slug']     = @$weaponData['slug'];
            $data['weapon']['image']         = ImageConversionHelper::showMfitSizeConversion(@$weaponData['image'],28,0);
        }else{
            $data['weapon'] = null;
        }

        return $data;
    }

    public function clearAllIfPlayerDeadBySteamId($steamId)
    {
        $this->clearPlayerStatusBySteamId($steamId);
        $this->clearPlayerWeaponsBySteamId($steamId);
    }

    public function clearPlayerStatusBySteamId($steamId)
    {
        $updateArrStatus                  = [];
        $updateArrStatus['hp']            = 0;
        $updateArrStatus['is_alive']      = false;
        $updateArrStatus['has_kevlar']    = false;
        $updateArrStatus['has_helmet']    = false;
        $updateArrStatus['has_defusekit'] = false;
        $updateArrStatus['has_bomb']      = false;
        $this->setPlayerBySteamId($steamId, $updateArrStatus);
    }

    public function clearPlayerWeaponsBySteamId($steamId)
    {

        $weaponKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $this->hSet($weaponKey, $steamId, json_encode([],320));

        $grenadeKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];
        $this->hSet($grenadeKey, $steamId, json_encode([],320));

        $weaponKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $this->hSet($weaponKey, $steamId, json_encode([],320));

        $grenadeRoundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];;
        $this->hSet($grenadeRoundKey, $steamId, json_encode([],320));

        return true;
    }

    public function setValue($keys, $value, $timeout = null)
    {
        if ($this->refresh_task) {
            $keyString                          = implode("_", $keys);
            $this->refresh_task_str[$keyString] = $value;
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::setKey($keyString, $value, $timeout);
    }

    public function setnx($keys, $value)
    {
        if ($this->refresh_task) {
            $keyString = implode("_", $keys);
            if ((!array_key_exists($keyString, $this->refresh_task_str))
                || (empty($this->refresh_task_str[$keyString]) && array_key_exists($keyString, $this->refresh_task_str))) {
                $this->refresh_task_str[$keyString] = $value;
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::setnx($keyString, $value);
    }


    public function diffRedisMermorygetValue($keys){
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $redisValue = parent::getKey($keyString);

        $keyString = implode("_", $keys);
        $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_REFRESH_TASK_STR];
        if ($this->refresh_task_str[$keyString]!=$redisValue){

            $event['id'] = $this->event_id;
            $event['event_type'] = $this->event_type;
            $event['key'] = $keyString;
            $event['value'] = $this->refresh_task_str[$keyString];
            $event['redisValue'] = $redisValue;
            if (!empty($event['value'])){
                $res = json_encode($event,320);
                $this->lPush($key,$res);
            }

        }else{
            $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_SAME_REFRESH_TASK_STR];
            $event['value'] = $this->refresh_task_str[$keyString];
            $res = json_encode($event,320);
            $this->lPush($key,$res);
        }
    }

    public function getValue($keys)
    {

        $reloadKey = [self::KEY_TAG_RELOAD,self::KEY_TAG_STATUS];
        $endKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT];
        if ($this->refresh_task && $keys != $reloadKey && $keys != $endKey) {
//            $this->diffRedisMermorygetValue($keys);//test
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                return $this->refresh_task_str[$keyString];
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $redisValue = parent::getKey($keyString);
        if ($this->refresh_task && $keys != $reloadKey && $keys != $endKey) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                return $this->refresh_task_str[$keyString];
            }else{
                $this->refresh_task_str[$keyString] = $redisValue;
            }
        }
        return $redisValue;
    }

    public function incr($key)
    {
        if ($this->refresh_task) {
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString,$this->refresh_task_str)){
                $this->refresh_task_str[$keyString] = $this->refresh_task_str[$keyString]+1;
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::incr($keyString);
    }

    public function hMset($key, $value)
    {
        if (!is_array($value)){
            return false;
        }
        if ($this->refresh_task) {
            if (!empty($value)){
                $keyString                          = implode("_", $key);
                $this->refresh_task_str[$keyString] = $value;
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::hMset($keyString, $value);
    }

    public function diffRedisMermoryhGetAll($keys){
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $redisValue = parent::hGetAll($keyString);

        $keyString = implode("_", $keys);
        $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_REFRESH_TASK_STR];

        if ($this->refresh_task_str[$keyString]!=$redisValue){

            $event['id'] = $this->event_id;
            $event['event_type'] = $this->event_type;
            $event['key'] = $keyString;
            $event['value'] = $this->refresh_task_str[$keyString];
            $event['redisValue'] = $redisValue;
            $res = json_encode($event,320);
            if ($keyString=="current_player_list"){
                $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_REFRESH_TASK_STR];
                return $this->lPush($key,$res);
            }
            if (!empty($event['value'])){
                $this->lPush($key,$res);
            }

//            $this->lPush($key,$keyString);
        }else{
            $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_SAME_REFRESH_TASK_STR];
            $event['value'] = $this->refresh_task_str[$keyString];
            $res = json_encode($event,320);
            $this->lPush($key,$res);
        }
    }

    public function hGetAll($key)
    {

        if ($this->refresh_task) {
//            $this->diffRedisMermoryhGetAll($key);//test
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                $strValue = $this->refresh_task_str[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $returnValue = parent::hGetAll($keyString);
        if ($this->refresh_task) {
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                return $this->refresh_task_str[$keyString];
            }else{
                if (!empty($returnValue)){
                    $this->refresh_task_str[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }

    public function lPush($key, $value)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::lPush($keyString, $value);
    }

    public function lRange($key, $offset, $length)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::lRange($keyString, $offset, $length);
    }

    public function sAdd($key, $value)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::sAdd($keyString, $value);
    }

    public function sRem($key, $value)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::sRem($keyString, $value);
    }



    /**
     * hSet
     * @param $key
     * @param $field
     * @param $value
     * @return string|void
     */
    public function hSet($key, $field, $value)
    {
        if ($this->refresh_task) {
            $keyString = implode("_", $key);
            if (empty($this->refresh_task_str[$keyString])) {
                $matchId     = $this->matchId;
                $keyStringNew   = implode(":", $key);
                $keyStringNew   = sprintf("%s:%s", $matchId, $keyStringNew);
                $returnValue = parent::hGetAll($keyStringNew);
                if (is_array($returnValue) && $returnValue) {
//                    $arrValue = json_decode($value,true);
                    $this->refresh_task_str[$keyString] = $returnValue;
//                    if (is_array($arrValue)) {
////                        $this->refresh_task_str[$keyString] = array_merge($returnValue, $arrValue);
//                    } else {
//                        $this->refresh_task_str[$keyString] = $returnValue;
//                    }

                }
            }
            $this->refresh_task_str[$keyString][$field] = $value;

        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::hSet($keyString, $field, $value);
    }

    /**
     * hSet
     * @param $key
     * @param $field
     * @param $value
     * @return string|void
     */
    public function hSetNx($key, $field, $value)
    {
        if ($this->refresh_task) {
            $keyString                          = implode("_", $key);
            if (empty($this->refresh_task_str[$keyString][$field])&&!array_key_exists($field,$this->refresh_task_str[$keyString])){
                if (empty($this->refresh_task_str[$keyString])) {
                    $matchId     = $this->matchId;
                    $keyStringNew   = implode(":", $key);
                    $keyStringNew   = sprintf("%s:%s", $matchId, $keyStringNew);
                    $returnValue = parent::hGetAll($keyStringNew);
                    if (is_array($returnValue) && $returnValue) {
//                    $arrValue = json_decode($value,true);
                        $this->refresh_task_str[$keyString] = $returnValue;
                    }
                    if (!array_key_exists($field,$returnValue)){
                        $this->refresh_task_str[$keyString][$field] = $value;
                    }
                }else{
                    $this->refresh_task_str[$keyString][$field] = $value;
                }


            }

        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::hSetNx($keyString, $field, $value);
    }

    /**
     * hIncrBy
     * @param $key
     * @param $value
     * @return string|void
     */
    public function hIncrBy($key, $field, $value)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::hIncrBy($keyString, $field, $value);
    }

    public function diffRedisMermoryhGet($keys, $field){
        $matchId   = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $redisValue = parent::hGet($keyString,$field);

        $keyString = implode("_", $keys);
        $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_REFRESH_TASK_STR];
        if ($this->refresh_task_str[$keyString][$field]!=$redisValue){

            $event['id'] = $this->event_id;
            $event['event_type'] = $this->event_type;
            $event['key'] = $keyString;
            $event['value'] = $this->refresh_task_str[$keyString][$field];
            if (!empty($event['value'])){
                $event['redisValue'] = $redisValue;
                $res = json_encode($event,320);
                $this->lPush($key,$res);
            }

//            $this->lPush($key,$keyString);
        }else{
            $key =[self::KEY_TAG_MATCH,self::KEY_TAG_MATCH,self::KEY_TAG_SAME_REFRESH_TASK_STR];
            $event['value'] = $this->refresh_task_str[$keyString];
            $res = json_encode($event,320);
            $this->lPush($key,$res);
        }
    }
    /**
     * hGet
     * @param $key
     * @param $field
     * @return string|void
     */
    public function hGet($key, $field)
    {

        if ($this->refresh_task) {
//            $this->diffRedisMermoryhGet($key, $field);//test
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                $strValue = $this->refresh_task_str[$keyString][$field];
                if ($strValue){
                    return $strValue;
                }
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $returnValue = parent::hGet($keyString, $field);;

        return $returnValue;
    }

    /**
     * hGet
     * @param $key
     * @param $field
     * @return string|void
     */
    public function hDel($key, $field)
    {

        if ($this->refresh_task) {
//            $this->diffRedisMermoryhGet($key, $field);//test
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                 unset($this->refresh_task_str[$keyString][$field]);
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $returnValue = parent::hDel($keyString, $field);;
        return $returnValue;
    }

    public function delAll($key)
    {
        if ($this->refresh_task) {
//            $this->diffRedisMermoryhGet($key, $field);//test
            $keyString                          = implode("_", $key);
            if (array_key_exists($keyString, $this->refresh_task_str)){
                unset($this->refresh_task_str[$keyString]);
            }
        }
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $returnValue = parent::del($keyString);;
        return $returnValue;
    }
    //round结束后的需要进行的操作
    public function roundEndOperation($model,$data_payload)
    {
        if(!$model){
            $model = new BayesCsgoSocket();
        }
        foreach ($data_payload['playerBalances'] as $k=>$v){
            BayesCsgoSocketPlayerService::setPlayerInfo($model,$v['playerUrn'],['is_alive'=>true]);
        }
        return true;
    }
    public function battleEndClear($model)
    {
        if(!$model){
            $model = new BayesCsgoSocket();
        }
        $model->delAll([self::KEY_TAG_CURRENT,'battle','battle_id']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','details']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','frame']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','team_ct','details']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','team_t','details']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','map','details']);
        $model->delAll([self::KEY_TAG_CURRENT,'battle','order']);
        $model->delAll([self::KEY_TAG_CURRENT,'player','list']);
        $model->delAll([self::KEY_TAG_CURRENT,'player','weapons']);
        $model->delAll([self::KEY_TAG_CURRENT,'round','is_freeze_time']);
        $model->delAll([self::KEY_TAG_CURRENT,'round','is_pause']);
        $model->delAll([self::KEY_TAG_CURRENT,'round','order']);
        $model->delAll([self::KEY_TAG_CURRENT,'round','round_id']);
        $model->delAll([self::KEY_TAG_CURRENT,self::CSGO_EVENT_INFO_TYPE_EVENTS,'type']);
        return true;
    }


    /**
     * sMembers
     * @param $key
     * @param $field
     * @return array
     */
    public function sMembers($key)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return parent::sMembers($keyString);
    }

    public function handleKeyPrefix($key)
    {
        $matchId   = $this->matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        return $keyString;
    }

    public function dataDefaultValue($data)
    {
//        $this->muti = $this->redis->multi(Redis::PIPELINE);//pipe
//        $this->redis = $this->muti;
        $newData['match_id']           = $this->matchId ? (int)$this->matchId : 0;
        $newData['battle_id']          = $this->getOutPutBattleId();
        $newData['battle_order']       = $this->getOutPutBattleOrder();
        $newData['battle_timestamp']   = $this->getDuration($this->event);
        $newData['event_id']           = $this->event_id ? (int)$this->event_id : 0;
        if ($this->event_type == 'battle_up_coming') {
            $event_type = 'battle_upcoming';
        } elseif ($this->event_type == 'battle_switch_team') {
            $event_type = 'team_switchside';
        } elseif ($this->event_type == 'round_time_frozen_start') {
            $event_type = 'round_freezetime_start';
        } else {
            $event_type = $this->event_type;
        }
        $newData['event_type']         = $event_type;
        $newData['round_ordinal']      = $this->currentRound();
        $newData['in_round_timestamp'] = $this->getInRoundTimestamp($this->event['info']['log_time']);
        $newData['round_time']         = $this->getRoundTime($this->event);
        $newData['is_bomb_planted']    = $this->getIsBombPlanted() ? true : false;
        if ($newData['is_bomb_planted'] && $this->getTimeSincePlant($this->event) === 0) {
            $newData['time_since_plant'] = 0;
        } else {
            $tempTime = $this->hasTempBombTime();
            if ($tempTime){
                $newData['time_since_plant'] = $tempTime;
            }else{
                $newData['time_since_plant'] = $this->getTimeSincePlant($this->event) ? $this->getTimeSincePlant($this->event) : 0;
            }
        }
//        $muti_res = $this->muti->exec();

        $baseBattleArr             = [];
        $baseBattleArr['duration'] = $newData['battle_timestamp'];
        $this->setCurrentBattleDetails($baseBattleArr);
        $newData = array_merge($newData,$data);
        return $newData;
    }

    public function getOutPutBattleId()
    {
        if (!empty($this->temp_battle_id)) {
            $temp                 = $this->temp_battle_id;
            $this->temp_battle_id = '';
            return $temp;
        }

        $battleId = $this->getRealBattleId();
        if (!$battleId) {
            return null;
        } else {
            return (int)$battleId;
        }
    }

    public function getOutPutBattleOrder()
    {
        if (!empty($this->temp_battle_order)) {
            $temp                 = $this->temp_battle_order;
            $this->temp_battle_order = '';
            return $temp;
        }
        $battleId    = $this->getBattleId();
        $battleOrder = $this->getBattleOrder();
        if (strstr($battleId, 'warmUp')) {
            return null;
        } else {
            return (int)$battleOrder;
        }
    }

    //根据t或者ct 获取队伍信息
    public function getTeamInfoBySide($side)
    {
        $teamInfo = [];
        $side     = strtolower($side);
        $side     = $side == 'terrorist' ? 't' : $side;
        switch ($side) {
            case 'ct':
                $teamInfo = $this->getCurrentCt();
                break;
            case 't':
                $teamInfo = $this->getCurrentT();
                break;
        }
        return $teamInfo ? $teamInfo : null;
    }

    public function batchDelRedisKey($redis_key)
    {
        if (!$redis_key || $redis_key == '*') {
            return false;
        }
        $redis = $this->redis;
        $pre   = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 100)) {
//            foreach ($arr_keys as $k=>$v){
//                $keyString                          = implode("_", $v);
//                if (array_key_exists($keyString,$this->refresh_task_str)){
//                    unset($this->refresh_task_str[$keyString]);
//                }
//            }

            call_user_func_array([$redis, 'del'], $arr_keys);
//            echo var_export($arr_keys, true) . PHP_EOL;
        }
        return true;
    }

    public function batchExpireRedisKey($redis_key)
    {
        if (!$redis_key || $redis_key == '*' || (!strstr($redis_key,'ws')&&!strstr($redis_key,'hltv_ws'))) {
            return false;
        }
        $redis = $this->redis;
        $pre   = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 200)) {
            if (!empty($arr_keys)){
                foreach ($arr_keys as $k=>$v){
                    $redis->expire($v,172800);
                }
            }
        }
        return true;
    }

    public function saveSocketToDb($data_ws)
    {
        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'), $data_ws);
    }
}
