<?php
/**
 *
 */

namespace app\modules\task\services\wsdata;

use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\hot\hltv\HltvMatchWs;
use app\modules\task\services\wsdata\abios\AbiosDota2Ws;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocket;
use app\modules\task\services\wsdata\bayes\BayesLolWs;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
use app\modules\task\services\wsdata\radarPurple\RadarPurpleDotaWs;
use app\modules\task\services\wsdata\orange\OrangeLolWs;
use app\modules\task\services\wsdata\pandascore\PandascoreLolws;
use app\modules\task\services\wsdata\pandascore\PandascoreCsgoWs;
use app\modules\task\services\wsdata\pandascore\PsDota2Ws;
use app\modules\task\services\wsdata\pandascore\PsLolWs;

class WebsocketDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $tasks = [];
        $originType=$tagInfo["origin_type"];
        $extType=$tagInfo["ext_type"];
        switch ($originType){
            case 2:
                switch ($extType){
                    case 1:
                        return [];
                        break;
                    case 2:
                        return null;
                        break;
                    case 3:
                        $tasks= AbiosDota2Ws::run($tagInfo,$taskInfo);
                        break;
                }
                break;
            case 3:  //pandascore lol origin_id
                switch ($extType){
                    case 1:
                        return [];
                        $tasks= PandascoreCsgoWs::run($tagInfo,$taskInfo);
                        break;
                    case 2:
//                        $tasks= PandascoreLolws::run($tagInfo,$taskInfo);
                        $tasks= PsLolWs::run($tagInfo,$taskInfo);
                        break;
                    case 3:
                        $tasks= PsDota2Ws::run($tagInfo,$taskInfo);
                        break;
                }
                break;
            case 4:  //玩家lol origin_id
                switch ($extType){
                    case 2:
                        $tasks= OrangeLolWs::run($tagInfo,$taskInfo);
                        break;
                }
                break;
            case 7:  //hltv csgo websocket
                $tasks= HltvMatchWs::run($tagInfo,$taskInfo);
                break;
            case 9://bayes
            case 10:
                switch ($extType){
                    case 1:
                        $tasks = BayesCsgoSocket::run($tagInfo,$taskInfo);
                        break;
                    case 2:
                        $tasks = BayesLolWs::run($tagInfo,$taskInfo);
                        break;
                    case 3:
                        $tasks = Dota2Socket::run($tagInfo,$taskInfo);
                        break;
                }
                break;
            case 11://测试流表信息录入
                $tasks= RadarPurpleDotaWs::run($tagInfo,$taskInfo);
                break;
            case 999://重新 获取比赛的 socket 所有信息
                $tasks= RegraspSocket::run($tagInfo,$taskInfo);
                break;
            default:
                throw new TaskException("can not deal with this type :".$originType);
                break;
        }
        return $tasks;
    }
}