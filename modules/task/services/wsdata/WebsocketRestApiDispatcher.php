<?php
/**
 *
 */

namespace app\modules\task\services\wsdata;

use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\wsdata\abios\AbiosDota2Api;
use app\modules\task\services\wsdata\bayes\LolApi;
use app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi;
use app\modules\task\services\wsdata\pandascore\PandascoreLolApi;


class WebsocketRestApiDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $originType=$tagInfo["origin_type"];
        $extType=$tagInfo["ext_type"];
        switch ($originType){
            case 2:
                switch ($extType){
                    case 1:
                        return [];
                        break;
                    case 2:
                        return [];
                        break;
                    case 3:
                        $tasks= AbiosDota2Api::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
            case 3:  //pandascore lol origin_id
                switch ($extType){
                    case 1:
                        $tasks= PandascoreCsgoApi::run($tagInfo,$taskInfo);
                        break;
                    case 2:
                        $tasks= PandascoreLolApi::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
            case 4:  //玩家lol origin_id
//                $tasks= OrangeDispatcher::run($tagInfo,$taskInfo);
                break;
            case 9:  //bayes
            case 10:  //bayes plus
                switch ($extType){
                    case 2:
                        $tasks= LolApi::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
                break;
            default:
                throw new TaskException("can not deal with this type :".$originType);
                break;
        }
        return $tasks;
    }
}