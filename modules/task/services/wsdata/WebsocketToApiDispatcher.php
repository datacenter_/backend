<?php
namespace app\modules\task\services\wsdata;

use app\modules\task\services\exceptions\TaskException;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketToApi;
use app\modules\task\services\wsdata\bayes\BayesLolWsToApi;
use app\modules\task\services\wsdata\bayes\Dota2SocketToApi;
use app\modules\task\services\wsdata\orange\OrangeLolWsToApi;
use app\modules\task\services\wsdata\pandascore\PsDota2WsToApi;
use app\modules\task\services\wsdata\pandascore\PsLolWsToApi;

class WebsocketToApiDispatcher
{
    public static function run($tagInfo,$taskInfo){
        $originType=$tagInfo["origin_type"];//数据源ID
        $extType=$tagInfo["ext_type"];//游戏ID
        switch ($originType){
            case 3:  //Pandascore
                switch ($extType){
                    case 2://lol
                        $tasks= PsLolWsToApi::run($tagInfo,$taskInfo);
                        break;
                    case 3://DOTA2
                        $tasks= PsDota2WsToApi::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
                break;
            case 4:  //玩家
                switch ($extType){
                    case 2://lol
                        $tasks= OrangeLolWsToApi::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
                break;
            case 9:  //Bayes
            case 10:  //Bayes
                switch ($extType){
                    case 2://lol
                        $tasks= BayesLolWsToApi::run($tagInfo,$taskInfo);
                        break;
                    case 3://dota
                        $tasks= Dota2SocketToApi::run($tagInfo,$taskInfo);
                        break;
                    case 1://csgo
                        $tasks = BayesCsgoSocketToApi::run($tagInfo,$taskInfo);
                        break;
                    default:
                        return [];
                        break;
                }
                break;
            default:
                throw new TaskException("can not deal with this type :".$originType);
                break;
        }
        return $tasks;
    }
}