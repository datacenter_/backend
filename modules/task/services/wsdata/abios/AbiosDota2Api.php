<?php
namespace app\modules\task\services\wsdata\abios;

use app\modules\common\models\EnumIngameGoal;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtDota2;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\AbiosIngameData;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\services\Common;
use app\modules\task\services\hot\abios\AbiosHotBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class AbiosDota2Api extends HotBase
{
    //处理socket
    public static function run($tagInfo, $taskInfo)
    {
        $conversionRes = null;
        $gameId = $tagInfo['ext_type'];
        $info = json_decode($taskInfo['params'], true);
        $originId = 2 ;//Abios
        $rel_match_id = $info['match_id'];
        $socket_id = (Int)$info['socket_id'];
        //查询主表比赛ID
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        if (empty($matchId)){
            return true;
        }
        //获取比赛信息
        $matchInfo = self::getMatchAllInfoByMatchId($matchId);
        //判断事件的类型
        $type = $info['type'];
        switch ($type)   //websocket类型
        {
            case "no":
                $conversionRes = self::transFrames($info,$matchInfo,$rel_match_id);
                break;
            case "events":
                $conversionRes = self::transEvents($info,$matchInfo,$rel_match_id,$socket_id);
                break;
        }
        return $conversionRes;
    }
    //处理Frames
    public static function transFrames($info,$matchInfo,$rel_match_id){
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if (@$match_data['channel'] == 'system'){
            return $match_data_info;
        }
        $gameId = 3;
        $matchId = $matchInfo['id'];
        $matchTeams = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
        if (isset($match_data['created_timestamp']) && $match_data['created_timestamp']){
            $rel_battle_now_time = (Int)@round($match_data['created_timestamp']/1000);
            $socket_time = @date('Y-m-d H:i:s',$rel_battle_now_time);
        }else{
            $socket_time = date('Y-m-d H:i:s');
            $rel_battle_now_time = time();
        }
        $rel_battle_id = $match_data['payload']['match']['id'];
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchId,$rel_match_id,$rel_battle_id,$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $match_start = $battle_Info['match_start'];
        $battle_begin_at = @strtotime($battle_Info['begin_at']);
        //事件
        $event_data = $match_data['payload']['event_data'];
        //判断事件类型
        $rel_event_type = $match_data['payload']['event_type'];
        if ($rel_event_type == 'state-updated'){
            //队伍信息
            $teamStatic = [];
            $teamsInfo = $event_data['teams'];
            foreach ($teamsInfo as $key => $val){
                $roster_id = $val['roster']['id'];
                //根据roster_id获取rel_team_id
                $rel_team_id = self::getRelTeamIdByRosterId($roster_id,$matchId,$gameId);
                $teamId = self::getMainIdByRelIdentityId('team',$rel_team_id,2);
                $teamStatic[$teamId]['score']= $val['deaths'];
                $teamStatic[$teamId]['team_id']= $teamId;
                //$towers_status = $val['structures_status']['towers'];
            }
            if ($teamStatic){
                foreach ($teamStatic as $key_c => $item_c){
                    if($matchInfo['team_1_id']==$item_c['team_id']){
                        $killNum = $teamStatic[$matchInfo['team_2_id']]['score'];
                        self::setTeamKill($battleId,$item_c['team_id'],$killNum);
                    }
                    if($matchInfo['team_2_id']==$item_c['team_id']){
                        $killNum = $teamStatic[$matchInfo['team_1_id']]['score'];
                        self::setTeamKill($battleId,$item_c['team_id'],$killNum);
                    }
                }
            }
            //self::refreshBattleTeam($battleId,$teamStatic,'statics');
        }
        return $battleId;
    }
    //处理事件
    public static function transEvents($info,$matchInfo,$rel_match_id,$socket_id)
    {
        $result = null;//执行成功的返回值
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if ($match_data['channel'] =='system'){
            return $match_data_info;
        }
        $gameId = 3;
        $matchId = $matchInfo['id'];
        $matchTeams = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
        //Match_RealTimeInfo
        $winner_id = null;
        if (isset($match_data['created_timestamp']) && $match_data['created_timestamp']){
            $rel_battle_now_time = (Int)@round($match_data['created_timestamp']/1000);
            $socket_time = @date('Y-m-d H:i:s',$rel_battle_now_time);
        }else{
            $socket_time = date('Y-m-d H:i:s');
            $rel_battle_now_time = time();
        }
        $rel_battle_id = $match_data['payload']['match']['id'];
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchId,$rel_match_id,$rel_battle_id,$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $match_start = $battle_Info['match_start'];
        $battle_begin_at = @strtotime($battle_Info['begin_at']);
        //对局时间
        $duration = $rel_battle_now_time - $battle_begin_at;
        //事件
        $event_data = $match_data['payload']['event_data'];
        //判断事件类型
        $rel_event_type = $match_data['payload']['event_type'];
        switch ($rel_event_type){
            case 'match-started':
                //更新
                $RealTimeInfo = [
                    'team_1_score' => 0,
                    'team_2_score' => 0,
                    'is_battle_detailed' => 1,
                    'is_draw' => 2,
                    'is_forfeit' => 2,
                    'finished' => 1,
                    'status' => 2,
                ];
                if ($match_start == 1){
                    $RealTimeInfo['begin_at'] = $socket_time;
                }
                self::setMatch($matchId, $RealTimeInfo);
                //对局
                $battleFramesInfoConfig = [
                    'game' => 3,
                    'is_draw' => 2,
                    'is_forfeit' => 2,
                    'is_default_advantage' => 2,
                    'status' => 2,
                    'order' => $battle_order,
                    'duration' => (String)$duration,
                    'map' => 1,
                    'winner' => null,
                    'is_battle_detailed' => 1,
                    'flag' => 1,
                    'deleted' => 2,
                    'deleted_at' => null
                ];
                self::refreshBattle($battleId,$battleFramesInfoConfig,'base');
                //队伍信息
                $teamStatic = [];
                $teamsInfo = $event_data['teams'];
                foreach ($teamsInfo as $key => $val){
                    $roster_id = $val['roster']['id'];
                    //根据roster_id获取rel_team_id
                    $rel_team_id = self::getRelTeamIdByRosterId($roster_id,$matchId,$gameId);
                    $teamId = self::getMainIdByRelIdentityId('team',$rel_team_id,2);
                    $teamOrder = self::getTeamOrderByTeamId($teamId,$matchInfo);
                    $teamStatic[$teamOrder]['order']= $teamOrder;
                    $teamStatic[$teamOrder]['rel_identity_id'] = $rel_team_id;
                    $teamStatic[$teamOrder]['score']= 0;
                    $teamStatic[$teamOrder]['team_id']= $teamId;
                    //判断阵营
                    if ($val['faction']['id'] == 5448){
                        $teamStatic[$teamOrder]['faction'] = 'dire';
                    }
                    if ($val['faction']['id'] == 5449){
                        $teamStatic[$teamOrder]['faction'] = 'radiant';
                    }
                }
                self::refreshBattle($battleId,$teamStatic,'statics');
                $result = true;
                break;
            case 'hero-selected':
                $pick_array_res = $player_info_res = [];
                $teamId = $teamOrder = null;
                $rel_player_id = @$event_data['player']['id'];
                $rel_hero_id = @$event_data['hero']['id'];
                $player_id = self::getMainIdByRelIdentityId('player',$rel_player_id,2);
                if ($player_id){
                    //根据playerID 获取他的TEAM_ID
                    $teamsInfo = TeamPlayerRelation::find()->where(['player_id'=>$player_id])->asArray()->all();
                    foreach ($teamsInfo as $team_item){
                        $team_id = $team_item['team_id'];
                        if (in_array($team_id,$matchTeams)){
                            $teamId = $team_id;
                        }
                    }
                }
                $hero_id = @self::getMainIdByRelIdentityIdOrUnknown('dota2_hero',$rel_hero_id,2);
                $pick_array['hero'] = $hero_id;
                $pick_array['team'] = $teamId;
                $pick_array['type'] = 2;
                $pick_array['order'] = null;
                $pick_array_res[0] = $pick_array;
                self::conversionBanPick($battleId,$pick_array_res);
                //获取team_order
                if ($teamId){
                    $teamOrder = self::getTeamOrderByTeamId($teamId,$matchInfo);
                }
                //再更新player信息
                $player_info = [
                    'rel_identity_id'=>(String)$rel_player_id,
                    'team_order'=>$teamOrder,
                    'battle_id'=>$battleId,
                    'game'=>3,
                    'match'=>$matchId,
                    'team_id'=>$teamId,
                    'player_id'=>$player_id,
                    'hero'=>$hero_id
                ];
                $playerInfoUs = self::getPlayInfoById($player_id);
                if ($playerInfoUs){
                    $player_info['nick_name'] = $playerInfoUs['nick_name'];
                }
                $player_info_res[0] = $player_info;
                self::refreshBattle($battleId,$player_info_res,'players');
                $result = true;
                break;
            case 'structure-destroyed':
                //对局时间
                $battleFramesInfoConfig = [
                    'duration' => (String)$duration,
                ];
                self::refreshBattle($battleId,$battleFramesInfoConfig,'base');
                //添加事件
                $eventOrder = self::getEventOrderByGameType($battleId,'dota');
                $eventData = [
                    'battle_id'=>$battleId,
                    'game'=>3,
                    'match'=>$matchId,
                    'order'=>$eventOrder,
                    'event_id'=>$socket_id,
                    'p_tid'=>null,
                    'event_type'=>'building_kill',
                    'ingame_timestamp'=>$duration,
                    'position'=>null,
                    'killer'=>null,
                    'killer_champion_id'=>null,
                    'killer_faction'=>null,
                    'killer_ingame_obj_type'=>null,
                    'killer_sub_type'=>null,
                    'victim'=>null,
                    'victim_champion_id'=>null,
                    'victim_faction'=>null,
                    'victim_ingame_obj_type'=>null,
                    'victim_sub_type'=>null,
                    'assist'=>null,
                    'assist_ingame_obj_type'=>null,
                    'assist_sub_type'=>null,
                    'is_first_event'=>null,
                    'first_event_type'=>null
                ];
                //判断victim信息
                $first_tower_p_tid_faction = null;
                //处理推塔事件
                $structure_id = @$event_data['structure']['id'];
                //获取塔的名称
                $structureData = AbiosIngameData::find()->where(['game_id'=>1])->andWhere(['rel_id'=>$structure_id])->asArray()->one();
                if ($structureData){
                    $eventData['victim'] = @$structureData['ingame_id'];
                    $eventData['victim_ingame_obj_type'] = @$structureData['ingame_id'];
                    $eventData['victim_sub_type'] = @$structureData['ingame_id'];
                    //计算faction
                    $structure_name = $structureData['rel_name'];
                    if (strstr($structure_name,'dire')){
                        $eventData['victim_faction'] = 'dire';
                        $first_tower_p_tid_faction = 'radiant';
                    }
                    if (strstr($structure_name,'radiant')){
                        $eventData['victim_faction'] = 'radiant';
                        $first_tower_p_tid_faction = 'dire';
                    }
                }
                //判断第一次事件
                $first_event_Info = self::getAbiosDotaFirstEventInfo('match_battle_event_info_api',$matchId,$battleId,$match_data,'api');
                if (empty($first_event_Info)){
                    return null;
                }
                if ($first_event_Info['is_first_event'] == true){
                    $is_first_event = 1;
                    $first_tower_p_tid = null;
                    $first_tower_time = (Int)$duration;
                    $teamId = self::getTeamIdByFaction($battleId,$first_tower_p_tid_faction);
                    if ($teamId) {
                        $first_tower_p_tid = @self::getTeamOrderByTeamId($teamId,$matchInfo);
                    }
                    $first_blood_data = [
                        'first_tower_p_tid'=>$first_tower_p_tid,
                        'first_tower_time'=>$first_tower_time
                    ];
                    self::refreshBattle($battleId,$first_blood_data,'detail');
                }else{
                    $is_first_event = 2;
                }
                $eventData['is_first_event'] = $is_first_event;
                $eventData['first_event_type'] = empty($first_event_Info['first_event_type']) ? 0 : Common::getGameDota2EventNum($first_event_Info['first_event_type']);;
                $eventDataRes[] = $eventData;
                //更新事件
                BattleService::setBattleInfo($battleId,'dota',$eventDataRes,BattleService::DATA_TYPE_EVENTS);
                //处理推塔事件
                //获取塔的名称
                $structureData = AbiosIngameData::find()->where(['game_id'=>1])->andWhere(['rel_id'=>$structure_id])->asArray()->one();
                if ($structureData){
                    $structure_name = $structureData['rel_name'];
                    //查找对应的信息
                    self::getStructureName($structure_name,$battleId);
                }
                $result = true;
                break;
            case 'deaths-updated':
                //对局时间
                $battleFramesInfoConfig = [
                    'duration' => (String)$duration,
                ];
                self::refreshBattle($battleId,$battleFramesInfoConfig,'base');
                $teamKillId = null;
                $first_blood_p_tid = null;
                //这个是队伍死亡数量，应该加到对立的队伍上面的分数
                $roster_id = $event_data['roster']['id'];
                $rel_team_id = self::getRelTeamIdByRosterId($roster_id,$matchId,$gameId);
                $teamId = self::getMainIdByRelIdentityId('team',$rel_team_id,2);
                if ($teamId){
                    $teamKey = array_search($teamId,$matchTeams);
                    if ($teamKey == 0){
                        $teamKillId = $matchTeams[1];
                        $first_blood_p_tid = 2;
                    }
                    if ($teamKey == 1){
                        $teamKillId = $matchTeams[0];
                        $first_blood_p_tid = 1;
                    }
                    if (empty($teamKillId)){
                        throw new BusinessException([], "TeamID:".$teamId."在比赛队伍中未匹配到");
                    }else{
                        $killNum = $event_data['to'];
                        $killNumFrom = $event_data['from'];
                        if ($killNumFrom == 0){//一血
                            $first_blood_time = $rel_battle_now_time - $battle_begin_at;
                            $first_blood_data = [
                                'first_blood_p_tid'=>$first_blood_p_tid,
                                'first_blood_time'=>$first_blood_time
                            ];
                            self::refreshBattle($battleId,$first_blood_data,'detail');
                        }
                        self::setTeamKill($battleId,$teamKillId,$killNum);
                    }
                }else{
                    throw new BusinessException([], "roster id:".$roster_id.",rel_team_id:".$rel_team_id.",未匹配到主表的队伍ID");
                }
                $result = true;
                break;
            case 'net-worth-lead-updated':
                //对局时间
                $battleFramesInfoConfig = [
                    'duration' => (String)$duration,
                ];
                self::refreshBattle($battleId,$battleFramesInfoConfig,'base');
                $net_worth_diff_timeline_res = [];
                $ingame_timestamp = $rel_battle_now_time - $battle_begin_at;
                $net_worth_diff = $event_data['home']['net_worth_lead'];
                $net_worth_diff_timeline = [
                    'ingame_timestamp'=>$ingame_timestamp,
                    'net_worth_diff'=>$net_worth_diff
                ];
                $net_worth_diff_timeline_res[0] = $net_worth_diff_timeline;
                self::setNetWorthDiffTimeLine($battleId,$net_worth_diff_timeline_res);
                break;
            case 'match-ended':
                //对局时间
                $battleFramesInfoConfig = [
                    'duration' => (String)$duration,
                    'end_at' => (String)$socket_time,
                    'status' => 3
                ];
                self::refreshBattle($battleId,$battleFramesInfoConfig,'base');
                //队伍信息
                $teamStatic = [];
                $teamsInfo = $event_data['teams'];
                foreach ($teamsInfo as $key => $val){
                    $roster_id = $val['roster']['id'];
                    //根据roster_id获取rel_team_id
                    $rel_team_id = self::getRelTeamIdByRosterId($roster_id,$matchId,$gameId);
                    $teamId = self::getMainIdByRelIdentityId('team',$rel_team_id,2);
                    $teamStatic[$teamId]['score']= $val['deaths'];
                    $teamStatic[$teamId]['team_id']= $teamId;
                    //$towers_status = $val['structures_status']['towers'];
                }
                if ($teamStatic){
                    foreach ($teamStatic as $key_c => $item_c){
                        if($matchInfo['team_1_id']==$item_c['team_id']){
                            $killNum = $teamStatic[$matchInfo['team_2_id']]['score'];
                            self::setTeamKill($battleId,$item_c['team_id'],$killNum);
                        }
                        if($matchInfo['team_2_id']==$item_c['team_id']){
                            $killNum = $teamStatic[$matchInfo['team_1_id']]['score'];
                            self::setTeamKill($battleId,$item_c['team_id'],$killNum);
                        }
                    }
                }
                //判断比赛是否结束
                if (false){
                    $RealTimeInfo['end_at'] = $socket_time;
                    self::setMatch($matchId, $RealTimeInfo);
                    self::changeHotData(2,$gameId,$matchId);
                }
                $result = true;
                break;
            default:
                $result = null;
                break;
        }
        return $result;
    }
    //更新 match 信息
    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail_by_scoket_frames');
    }
    //更新battle 信息
    private static function refreshBattle($battleId, $battle_info,$type)
    {
        switch ($type){
            case 'base':
                BattleService::setBattleInfo($battleId,'dota',$battle_info,BattleService::DATA_TYPE_BASE);
                break;
            case 'detail':
                BattleService::setBattleInfo($battleId,'dota',$battle_info,BattleService::DATA_TYPE_DETAILS);
                break;
            case 'statics':
                BattleService::setBattleInfo($battleId,'dota',$battle_info,BattleService::DATA_TYPE_STATICS);
                break;
            case 'players':
                BattleService::setBattleInfo($battleId,'dota',$battle_info,BattleService::DATA_TYPE_PLAYERS);
                break;
        }
        return true;
    }
    //处理banPick
    private static function conversionBanPick($battleId,$pick_array_res){
        $matchBattleBanPickInfo = MatchBattleExtDota2::find()->where(['id'=>$battleId])->one();
        if ($matchBattleBanPickInfo){
            $oldBan_pick = @$matchBattleBanPickInfo->ban_pick;
            if ($oldBan_pick){
                $oldBan_pick_array = @json_decode($oldBan_pick,true);
                $new_ban_pick = array_merge($oldBan_pick_array,$pick_array_res);
            }else{
                $new_ban_pick = $pick_array_res;
            }
            $matchBattleBanPickInfo->setAttribute('ban_pick',json_encode($new_ban_pick,320));
            if (!$matchBattleBanPickInfo->save()){
                throw new BusinessException($matchBattleBanPickInfo->getErrors(), "保存MatchBattleExtDota2New失败");
            }
        }else{
            $MatchBattleExtDota2Model = new MatchBattleExtDota2();
            $battleBanpick = [
                'ban_pick'=>json_encode($pick_array_res,320),
                'is_confirmed'=>1,
                'id'=>$battleId
            ];
            $MatchBattleExtDota2Model->setAttributes($battleBanpick);
            if (!$MatchBattleExtDota2Model->save()){
                throw new BusinessException($MatchBattleExtDota2Model->getErrors(), "保存MatchBattleExtDota2失败");
            }
        }
    }
    //根据原始ID 更新信息
    public static function setTeamKill($battle_id,$team_id,$killNum){
        $matchBattleTeam = MatchBattleTeam::find()->where(['battle_id' => $battle_id])->andWhere(['team_id'=>$team_id])->one();
        if ($matchBattleTeam){
            $rel_id = $matchBattleTeam->id;
            $info_ext = MatchBattleTeamExtDota2::find()->where(['id' => $rel_id])->one();
            if ($info_ext){
                $info_ext->setAttribute('kills',$killNum);
                if (!$info_ext->save()) {
                    throw new BusinessException($info_ext->getErrors(), '保存失败');
                }
            }
        }
    }
    //设置财产差时间线 net_worth_diff_timeline
    private static function setNetWorthDiffTimeLine($battleId, $data)
    {
        //设置阵容
        $battleExtDota = MatchBattleExtDota2::find()->where(['id' => $battleId])->one();
        if (!$battleExtDota) {
            $battleExtDota = new MatchBattleExtDota2();
            $battleExtDota->setAttribute('id', $battleId);
        }
        if (isset($battleExtDota['net_worth_diff_timeline']) && $battleExtDota['net_worth_diff_timeline']){
            $net_worth_diff_timeline = json_decode($battleExtDota['net_worth_diff_timeline'],true);
            $data = array_merge($net_worth_diff_timeline,$data);
        }
        $battleExtDota->setAttribute('net_worth_diff_timeline', json_encode($data,320));
        if (!$battleExtDota->save()) {
            throw new BusinessException($battleExtDota->getErrors(), '保存财产差时间线失败');
        }
    }
    private static function getStructureName($structure_name,$battleId){
        switch ($structure_name){
            case 'dire_ancient'://遗迹
                //查找对应的队伍信息
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_ancient'=> 1
                    ];
                    self::setBuildingStatus($id,$data,'building_status_ancient');
                }
                break;
            case 'radiant_ancient'://遗迹
                //查找对应的队伍信息
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_ancient'=> 1
                    ];
                    self::setBuildingStatus($id,$data,'building_status_ancient');
                }
                break;
            case 'dire_tower_bot_1'://下路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_bot_2'://下路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_bot_3'://下路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_bot_4'://下路4塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_4_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_mid_1'://中路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_mid_2'://中路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_mid_3'://中路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_top_1'://上路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_top_2'://上路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_top_3'://上路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_tower_top_4'://上路4塔
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_4_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_bot_1'://下路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_bot_2'://下路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_bot_3'://下路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_bot_4'://下路4塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['bot_tier_4_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_mid_1'://中路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_mid_2'://中路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_mid_3'://中路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['mid_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_top_1'://上路1塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_1_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_top_2'://上路2塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_2_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_top_3'://上路3塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_3_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'radiant_tower_top_4'://上路4塔
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_towers']['top_tier_4_tower'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_towers'=>json_encode($ext_res['building_status_towers'],320)
                    ];
                    self::setBuildingStatus($id,$data,'tower_kills');
                }
                break;
            case 'dire_rangeed_rax_top'://上路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['top_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'dire_melee_rax_top'://上路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['top_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            case 'dire_rangeed_rax_mid'://中路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['mid_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'dire_melee_rax_mid'://中路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['mid_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            case 'dire_rangeed_rax_bot'://下路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['bot_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'dire_melee_rax_bot'://下路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'dire');
                if ($ext_res){
                    $ext_res['building_status_barracks']['bot_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            case 'radiant_rangeed_rax_top'://上路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['top_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'radiant_melee_rax_top'://上路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['top_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            case 'radiant_rangeed_rax_mid'://中路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['mid_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'radiant_melee_rax_mid'://中路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['mid_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            case 'radiant_rangeed_rax_bot'://下路远程兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['bot_ranged_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'ranged_barrack_kills');
                }
                break;
            case 'radiant_melee_rax_bot'://下路近战兵营
                $ext_res = self::getTeamInfoByFaction($battleId,'radiant');
                if ($ext_res){
                    $ext_res['building_status_barracks']['bot_melee_barrack'] = 1;
                    $id = @$ext_res['id'];
                    $data = [
                        'building_status_barracks'=>json_encode($ext_res['building_status_barracks'],320)
                    ];
                    self::setBuildingStatus($id,$data,'melee_barrack_kills');
                }
                break;
            default:
                break;
        }
        return true;
    }
    //查找对应的队伍信息 根据阵营 dire radiant
    private static function getTeamInfoByFaction($battleId,$faction){
        $MatchBattleTeamInfoRes = null;
        $MatchBattleTeamInfo = MatchBattleTeam::find()->alias('mbt')
            ->leftJoin('match_battle_team_ext_dota2 as ext','ext.id = mbt.id')
            ->select('ext.id as ext_id,ext.building_status_towers,ext.building_status_barracks,ext.building_status_ancient')
            ->where(['mbt.battle_id'=>$battleId])
            ->andWhere(['ext.faction'=>$faction])
//            ->createCommand()->getRawSql();
            ->asArray()->one();
        if ($MatchBattleTeamInfo){
            if ($MatchBattleTeamInfo['building_status_towers']){
                $MatchBattleTeamInfoRes['building_status_towers'] = json_decode($MatchBattleTeamInfo['building_status_towers'],true);
            }else{
                $MatchBattleTeamInfoRes['building_status_towers'] = [
                    'top_tier_1_tower'=> 2,
                    'top_tier_2_tower'=> 2,
                    'top_tier_3_tower'=> 2,
                    'mid_tier_1_tower'=> 2,
                    'mid_tier_2_tower'=> 2,
                    'mid_tier_3_tower'=> 2,
                    'bot_tier_1_tower'=> 2,
                    'bot_tier_2_tower'=> 2,
                    'bot_tier_3_tower'=> 2,
                    'top_tier_4_tower'=> 2,
                    'bot_tier_4_tower'=> 2
                ];
            }
            if ($MatchBattleTeamInfo['building_status_barracks']){
                $MatchBattleTeamInfoRes['building_status_barracks'] = @json_decode($MatchBattleTeamInfo['building_status_barracks'],true);
            }else{
                $MatchBattleTeamInfoRes['building_status_barracks'] = [
                    'top_ranged_barrack'=>2,
                    'top_melee_barrack'=>2,
                    'mid_ranged_barrack'=>2,
                    'mid_melee_barrack'=>2,
                    'bot_ranged_barrack'=>2,
                    'bot_melee_barrack'=>2
                ];
            }
            $MatchBattleTeamInfoRes['building_status_ancient'] = empty($MatchBattleTeamInfo['building_status_ancient']) ? 2 : $MatchBattleTeamInfo['building_status_ancient'];
            $MatchBattleTeamInfoRes['id'] = $MatchBattleTeamInfo['ext_id'];
        }
        return $MatchBattleTeamInfoRes;
    }
    //查找对应的队伍信息 根据阵营 dire radiant
    private static function getTeamIdByFaction($battleId,$faction)
    {
        $teamId = null;
        $MatchBattleTeamInfo = MatchBattleTeam::find()->alias('mbt')
            ->leftJoin('match_battle_team_ext_dota2 as ext', 'ext.id = mbt.id')
            ->select('mbt.team_id')
            ->where(['mbt.battle_id' => $battleId])
            ->andWhere(['ext.faction' => $faction])
            ->asArray()->one();
        if ($MatchBattleTeamInfo){
            $teamId = $MatchBattleTeamInfo['team_id'];
        }
        return $teamId;
    }
    //更新塔的信息
    private static function setBuildingStatus($id,$info,$type){
        $MatchBattleTeamExtDota2 = MatchBattleTeamExtDota2::find()->where(['id'=>$id])->one();
        if ($MatchBattleTeamExtDota2){
            //摧毁防御塔
            if ($type=='tower_kills'){
                $tower_kills = $MatchBattleTeamExtDota2->tower_kills + 1;
                $info['tower_kills'] = $tower_kills;
            }
            //摧毁近战兵营
            if ($type=='melee_barrack_kills'){
                $melee_barrack_kills = $MatchBattleTeamExtDota2->melee_barrack_kills + 1;
                $info['melee_barrack_kills'] = $melee_barrack_kills;
            }
            //摧毁远程兵营
            if ($type=='ranged_barrack_kills'){
                $ranged_barrack_kills = $MatchBattleTeamExtDota2->ranged_barrack_kills + 1;
                $info['ranged_barrack_kills'] = $ranged_barrack_kills;
            }
            $MatchBattleTeamExtDota2->setAttributes($info);
            if (!$MatchBattleTeamExtDota2->save()) {
                throw new BusinessException($MatchBattleTeamExtDota2->getErrors(), '更新队伍塔信息失败');
            }
        }
    }
    //根据$roster_id 获取 rel team ID
    public static function getRelTeamIdByRosterId($roster_id,$matchId,$gameId){
        //设置redis 关系
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redis_name = 'match_info_abios_team_relation:'.$matchId."_".$gameId;
        $rel_team_id = $Redis->hget($redis_name,$roster_id);
        if (empty($rel_team_id)){
            $roster_action = "/v3/rosters/{$roster_id}";
            $rosterInfo = AbiosHotBase::getRestInfo($roster_action, []);
            $rel_team_id = @$rosterInfo['team']['id'];
            $Redis->hset($redis_name,$roster_id,@$rosterInfo['team']['id']);
        }
        return $rel_team_id;
    }
}