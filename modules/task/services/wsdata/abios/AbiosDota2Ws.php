<?php
namespace app\modules\task\services\wsdata\abios;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\ImageConversionHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtDota2;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\AbiosIngameData;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Map;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\models\MatchSocketData;
use app\modules\task\services\hot\abios\AbiosHotBase;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;

class AbiosDota2Ws extends HotBase
{
    //处理socket
    public static function run($tagInfo, $taskInfo)
    {
        $conversionRes = null;
        $gameId = $tagInfo['ext_type'];
        $info = json_decode($taskInfo['params'], true);
        $originId = 2 ;//Abios
        $rel_match_id = $info['match_id'];
        $socket_id = (Int)$info['socket_id'];
        //查询主表比赛ID
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        if (empty($matchId)){
            return true;
        }
        //获取比赛信息
        $matchInfo = self::getMatchAllInfoByMatchId($matchId);
        //判断事件的类型
        $type = $info['type'];
        switch ($type)   //websocket类型
        {
            case "no":
                $conversionRes = self::transFrames($info,$matchInfo,$rel_match_id);
                $type = 'frames';
                break;
            case "events":
                $conversionRes = self::transEvents($info,$matchInfo,$rel_match_id,$socket_id);
                $type = 'events';
                break;
        }
        if (empty($conversionRes)){
            return ;
        }
        //接下来存数据库
        $insertData = [
            'match_data' =>$conversionRes['trans_data'],
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => $type,
            'battle_order' => $conversionRes['battle_order']
        ];
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $insertData);
        $db_websoket->execute();
        //插入到自己的库
        $matchSocketData = new MatchSocketData();
        $matchSocketData->setAttributes([
            'game_id' => $gameId,
            'origin_id' => $originId,
            'match_id' => $matchId,
            'battle_order' => $conversionRes['battle_order'],
            'match_data' =>$conversionRes['trans_data'],
            'type' => $type
        ]);
        return $matchSocketData->save();
    }
    //处理Frames
    public static function transFrames($info,$matchInfo,$rel_match_id){
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if (@$match_data['channel'] == 'system'){
            return $match_data_info;
        }
        $gameId = 3;
        $matchId = $matchInfo['id'];
        $matchTeams = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
        if (isset($match_data['created_timestamp']) && $match_data['created_timestamp']){
            $rel_battle_now_time = (Int)@round($match_data['created_timestamp']/1000);
            $socket_time = @date('Y-m-d H:i:s',$rel_battle_now_time);
        }else{
            $socket_time = date('Y-m-d H:i:s');
            $rel_battle_now_time = time();
        }
        $rel_battle_id = $match_data['payload']['match']['id'];
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchId,$rel_match_id,$rel_battle_id,$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $match_start = $battle_Info['match_start'];
        $battle_begin_at = @strtotime($battle_Info['begin_at']);
        //定义building_status
        $towers = [
            'top_tier_1_tower'=> null,
            'top_tier_2_tower'=> null,
            'top_tier_3_tower'=> null,
            'mid_tier_1_tower'=> null,
            'mid_tier_2_tower'=> null,
            'mid_tier_3_tower'=> null,
            'bot_tier_1_tower'=> null,
            'bot_tier_2_tower'=> null,
            'bot_tier_3_tower'=> null,
            'top_tier_4_tower'=> null,
            'bot_tier_4_tower'=> null
        ];
        $barracks = [
            'top_ranged_barrack'=> null,
            'top_melee_barrack'=> null,
            'mid_ranged_barrack'=> null,
            'mid_melee_barrack'=> null,
            'bot_ranged_barrack'=> null,
            'bot_melee_barrack'=> null
        ];
        $building_status = [
            'towers' => $towers,
            'barracks' => $barracks,
            'ancient' => null
        ];
        $home_building_status = $away_building_status = $building_status;
        $teams = $match_data['payload']['event_data']['teams'];
        //默认地图类型
        $mapModelResult = MetadataDota2Map::find()
            ->select(['id as map_id','name','name_cn','external_id','external_name','short_name','map_type','map_type_cn','is_default','image as thumbnail'])
            ->where(['is_default' => 1])->asArray()->one();
        if($mapModelResult){
            $map = [];
            $map['map_id'] = self::checkAndChangeInt($mapModelResult['map_id']);
            $map['name'] = self::checkAndChangeString($mapModelResult['name']);
            $map['name_cn'] = self::checkAndChangeString($mapModelResult['name_cn']);
            $map['external_id'] = self::checkAndChangeString($mapModelResult['external_id']);
            $map['external_name'] = self::checkAndChangeString($mapModelResult['external_name']);
            $map['short_name'] = self::checkAndChangeString($mapModelResult['short_name']);
            $map['map_type'] = self::checkAndChangeString($mapModelResult['map_type']);
            $map['map_type_cn'] = self::checkAndChangeString($mapModelResult['map_type_cn']);
            $map['is_default'] = $mapModelResult['is_default'] == 1 ? true : false;
            $map['image']['square_image'] = null;
            $map['image']['rectangle_image'] = null;
            $map['image']['thumbnail'] = self::checkAndChangeString($mapModelResult['thumbnail']);
        }else{
            $map = null;
        }
        //获取比赛的大比分
        $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
        //scores
        $match_scores_res = [];
        $match_scores = $match_winner = $battle_winner = null;
        $home_team_image_res = $away_team_image_res = null;
        //主队
        $home_faction = null;
        $home_faction_rel = $teams['home']['faction']['id'];
        //判断阵营
        if ($home_faction_rel == 5448){
            $home_faction = 'dire';
        }
        if ($home_faction_rel == 5449){
            $home_faction = 'radiant';
        }
        $home['faction'] = $home_faction;
        $home_roster_id = $teams['home']['roster']['id'];
        //根据roster_id获取rel_team_id
        $rel_home_team_id = self::getRelTeamIdByRosterId($home_roster_id,$matchId,$gameId);
        $homeTeamId = @self::getMainIdByRelIdentityId('team',$rel_home_team_id,2);
        $homeTeamOrder = @self::getTeamOrderByTeamId($homeTeamId,$matchInfo);
        $home_team = Team::find()->select('id,name,image')->where(['id' => $homeTeamId])->asArray()->one();
        if($home_team){
            $home['team']['team_id'] = (Int)$home_team['id'];
            $home['team']['name'] = @$home_team['name'];
            if (!empty($home_team['image'])){
                $home_team_image_res = ImageConversionHelper::showFixedSizeConversion($home_team['image'],200,200);
            }
            $home['team']['image'] = $home_team_image_res;
            $home['team']['opponent_order'] = $homeTeamOrder;
        }else{
            $home['team'] = null;
        }
        $home['deaths'] = $teams['home']['deaths'];
        $home['kills'] = 0;
        $home['net_worth'] = null;
        $home['net_worth_diff'] = $teams['home']['net_worth_lead'];
        $home['gold_earned'] = null;
        $home['experience'] = null;
        $home['tower_kills'] = null;
        $home['melee_barrack_kills'] = null;
        $home['ranged_barrack_kills'] = null;
        $home['roshan_kills'] = null;
        //处理building_status
        $home_structures_status = $teams['home']['structures_status']['towers'];
        if ($home_structures_status){
            $home_building_status_new = [];
            foreach ($home_structures_status as $key_bs => $val_bs){
                $standing_val = $val_bs['standing'] == true ? true : false;
                $home_building_status_new['towers'][$key_bs."_tower"] = $standing_val;
            }
            $home_building_status_new['towers']['top_tier_4_tower'] = null;
            $home_building_status_new['towers']['bot_tier_4_tower'] = null;
            $home['building_status'] = array_merge($home_building_status,$home_building_status_new);
        }else{
            $home['building_status'] = $home_building_status;
        }
        //players
        $home['players'] = self::conversionPlayers($teams['home']['players'],$home_faction);
        //主队比分
        $match_scores[$homeTeamOrder]['team_id'] = (Int)$homeTeamId;
        $match_scores[$homeTeamOrder]['name'] = @$home['team']['name'];
        $match_scores[$homeTeamOrder]['image'] = $home_team_image_res;
        $match_scores[$homeTeamOrder]['opponent_order'] = (Int)$homeTeamOrder;
        $match_scores[$homeTeamOrder]['score'] = 0;
        //客队
        $away_faction = null;
        $away_faction_rel = $teams['away']['faction']['id'];
        //判断阵营
        if ($away_faction_rel == 5448){
            $away_faction = 'dire';
        }
        if ($away_faction_rel == 5449){
            $away_faction = 'radiant';
        }
        $away['faction'] = $away_faction;
        $away_roster_id = $teams['away']['roster']['id'];
        //根据roster_id获取rel_team_id
        $rel_away_team_id = self::getRelTeamIdByRosterId($away_roster_id,$matchId,$gameId);
        $awayTeamId = @self::getMainIdByRelIdentityId('team',$rel_away_team_id,2);
        $awayTeamOrder = @self::getTeamOrderByTeamId($awayTeamId,$matchInfo);
        $away_team = Team::find()->select('id,name,image')->where(['id' => $awayTeamId])->asArray()->one();
        if($away_team){
            $away['team']['team_id'] = (Int)$away_team['id'];
            $away['team']['name'] = @$away_team['name'];
            if (!empty($away_team['image'])){
                $away_team_image_res = ImageConversionHelper::showFixedSizeConversion($away_team['image'],200,200);
            }
            $away['team']['image'] = $away_team_image_res;
            $away['team']['opponent_order'] = $awayTeamOrder;
        }else{
            $away['team'] = null;
        }
        $away['deaths'] = $teams['away']['deaths'];
        $away['kills'] = 0;
        $away['net_worth'] = null;
        $away['net_worth_diff'] = $teams['away']['net_worth_lead'];
        $away['gold_earned'] = null;
        $away['experience'] = null;
        $away['tower_kills'] = null;
        $away['melee_barrack_kills'] = null;
        $away['ranged_barrack_kills'] = null;
        $away['roshan_kills'] = null;
        //处理building_status
        $away_structures_status = $teams['away']['structures_status']['towers'];
        if ($away_structures_status){
            $away_building_status_new = [];
            foreach ($away_structures_status as $key_bs => $val_bs){
                $away_standing_val = $val_bs['standing'] == true ? true : false;
                $away_building_status_new['towers'][$key_bs."_tower"] = $away_standing_val;
            }
            $away_building_status_new['towers']['top_tier_4_tower'] = null;
            $away_building_status_new['towers']['bot_tier_4_tower'] = null;
            $away['building_status'] = array_merge($away_building_status,$away_building_status_new);
        }else{
            $away['building_status'] = $away_building_status;
        }
        //players
        $away['players'] = self::conversionPlayers($teams['away']['players'],$away_faction);
        //主队比分
        $match_scores[$awayTeamOrder]['team_id'] = (Int)$awayTeamId;
        $match_scores[$awayTeamOrder]['name'] = @$away['team']['name'];
        $match_scores[$awayTeamOrder]['image'] = $away_team_image_res;
        $match_scores[$awayTeamOrder]['opponent_order'] = (Int)$awayTeamOrder;
        $match_scores[$awayTeamOrder]['score'] = 0;
        //循环修改death为对方的信息
        $tmp_deaths = $home['deaths'];
        $home['kills'] = $away['deaths'];
        $away['kills'] = $tmp_deaths;
        unset($home['deaths']);
        unset($away['deaths']);
        //修改双方的比分
        if ($homeTeamOrder == 1){
            $match_scores[$homeTeamOrder]['score'] = (Int)$MatchRealTimeInfo['team_1_score'];
            $match_scores[$awayTeamOrder]['score'] = (Int)$MatchRealTimeInfo['team_2_score'];
        }
        if ($homeTeamOrder == 2){
            $match_scores[$homeTeamOrder]['score'] = (Int)$MatchRealTimeInfo['team_2_score'];
            $match_scores[$awayTeamOrder]['score'] = (Int)$MatchRealTimeInfo['team_1_score'];
        }
        //组合factions
        $factions[] = $home;
        $factions[] = $away;
        $duration = (Int)@round($match_data['payload']['match']['clock']['milliseconds']/1000);
        $is_finished = false;
        if ($match_data['payload']['match']['phase'] == 'game-over'){
            $is_finished = true;
        }
        $is_pause = false;
        $match_scores_res[] = @$match_scores[1];
        $match_scores_res[] = @$match_scores[2];
        //组合数据
        $transData = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'map' =>$map,
            'duration' => $duration,
            'is_finished' => $is_finished,
            'match_scores' => $match_scores_res,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => $is_pause,
            'factions' =>$factions
        ];
        $trans_data = json_encode($transData,320);
        $frames_data = [
            'battle_order' => $battle_order,
            'trans_data' => $trans_data
        ];
        return $frames_data;
    }
    //处理事件
    public static function transEvents($info,$matchInfo,$rel_match_id,$socket_id)
    {
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if ($match_data['channel'] =='system'){
            return $match_data_info;
        }
        if ($match_data['payload']['event_type'] != 'structure-destroyed'){
            return $match_data_info;
        }
        $event_type = 'building_kill';
        $position = null;
        $killer = $victim = $assist = null;
        $matchId = $matchInfo['id'];
        if (isset($match_data['created_timestamp']) && $match_data['created_timestamp']){
            $rel_battle_now_time = (Int)@round($match_data['created_timestamp']/1000);
            $socket_time = @date('Y-m-d H:i:s',$rel_battle_now_time);
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        $rel_battle_id = $match_data['payload']['match']['id'];
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchId,$rel_match_id,$rel_battle_id,$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        //处理推塔事件
        $structure_id = @$match_data['payload']['event_data']['structure']['id'];
        //获取塔的名称
        $structureData = AbiosIngameData::find()->where(['game_id'=>1])->andWhere(['rel_id'=>$structure_id])->asArray()->one();
        if ($structureData){
            //判断事件类型
            $victim = [
                'lane' => null,
                'ingame_obj_name' => null,
                'ingame_obj_name_cn' => null,
                'faction' => null,
                'ingame_obj_type' => null,
                'ingame_obj_sub_type' => null
            ];
            $ingame_info = EnumIngameGoal::find()->where(['id'=>$structureData['ingame_id']])->asArray()->one();
            $victim['lane'] = @$ingame_info['lane'];
            $victim['ingame_obj_name'] = @$ingame_info['ingame_obj_name'];
            $victim['ingame_obj_name_cn'] = @$ingame_info['ingame_obj_name_cn'];
            //计算faction
            $structure_name = $structureData['rel_name'];
            if (strstr($structure_name,'dire')){
                $victim['faction'] = 'dire';
            }
            if (strstr($structure_name,'radiant')){
                $victim['faction'] = 'radiant';
            }
            $victim['ingame_obj_type'] = @$ingame_info['ingame_obj_type'];
            $victim['ingame_obj_sub_type'] = @$ingame_info['sub_type'];
        }
        //第一次事件  只有tower
        $first_event_Info = self::getAbiosDotaFirstEventInfo('match_battle_event_info',$matchId,$battleId,$match_data,'ws');
        if (empty($first_event_Info)){
            return null;
        }
        $is_first_event = $first_event_Info['is_first_event'];
        $first_event_type = $first_event_Info['first_event_type'];
        //时间
        $ingame_timestamp = (Int)@round($match_data['payload']['match']['clock']['milliseconds']/1000);
        //组合数据
        $transData = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,  //待完善
            'battle_order' => (Int)$battle_order,
            'event_id' =>$socket_id,
            'ingame_timestamp' => $ingame_timestamp,
            'event_type' => $event_type,
            'position'=> $position,
            'killer' => $killer,
            'victim' => $victim,
            'assist' => $assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $trans_data = json_encode($transData,320);
        $frames_data = [
            'battle_order' => $battle_order,
            'trans_data' => $trans_data
        ];
        return $frames_data;
    }
    //转化player
    private static function conversionPlayers($players,$faction)
    {
        $players_res = [];
        foreach ($players as $key => $items){
            $players_items = [];
            $players_items['seed'] = null;
            $players_items['faction'] = $faction;
            $players_items['role'] = null;
            $players_items['lane'] = null;
            $player_id = self::getMainIdByRelIdentityId('player',$items['id'],2);
            if ($player_id){
                $players_items['player']['player_id'] = (Int)$player_id;
                $playerInfo = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                $players_items['player']['nick_name'] = (String)@$playerInfo['nick_name'];
            }else{
                $players_items['player']['player_id'] = null;
                $players_items['player']['nick_name'] = null;
            }
            $tempHeroId = self::getMainIdByRelIdentityIdOrUnknown('dota2_hero',$items['hero']['id'],2);
            $tempHero = MetadataDota2Hero::find()->select('id,external_id,external_name,name,name_cn,title,title_cn,slug,image,small_image')->where(['id' => $tempHeroId])->asArray()->one();
            if ($tempHero){
                $players_items['hero']['hero_id'] = (Int)@$tempHero['id'];
                $players_items['hero']['name'] = @$tempHero['name'];
                $players_items['hero']['name_cn'] = @$tempHero['name_cn'];
                $players_items['hero']['external_id'] = @$tempHero['external_id'];
                $players_items['hero']['external_name'] = @$tempHero['external_name'];
                $players_items['hero']['title'] = @$tempHero['title'];
                $players_items['hero']['title_cn'] = @$tempHero['title_cn'];
                $players_items['hero']['slug'] = @$tempHero['slug'];
                $players_items['hero']['image']['image'] = @ImageConversionHelper::showFixedSizeConversion(@$tempHero['image'],144,256);
                $players_items['hero']['image']['small_image'] = @ImageConversionHelper::showFixedSizeConversion(@$tempHero['image'],32,32);
            }else{
                $players_items['hero']=null;
            }
            $players_items['level'] = null;
            $players_items['is_alive'] = null;
            $players_items['ultimate_cd'] = null;
            $players_items['coordinate'] = null;
            $players_items['kills'] = null;
            $players_items['deaths'] = null;
            $players_items['assists'] = null;
            $players_items['kda'] = null;
            $players_items['participation'] = null;
            $players_items['last_hits'] = null;
            $players_items['lhpm'] = null;
            $players_items['denies'] = null;
            $players_items['net_worth'] = null;
            $players_items['gold_earned'] = null;
            $players_items['gpm'] = null;
            $players_items['experience'] = null;
            $players_items['xpm'] = null;
            $players_items['items']['inventory'] = null;
            $players_items['items']['backpack'] = null;
            $players_items['items']['stash'] = null;
            $players_items['items']['buffs'] = null;
            //组合数组
            $players_res[] = $players_items;
        }
        return $players_res;
    }
    //根据$roster_id 获取 rel team ID
    public static function getRelTeamIdByRosterId($roster_id,$matchId,$gameId){
        //设置redis 关系
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        $redis_name = 'match_info_abios_team_relation:'.$matchId."_".$gameId;
        $rel_team_id = $Redis->hget($redis_name,$roster_id);
        if (empty($rel_team_id)){
            $roster_action = "/v3/rosters/{$roster_id}";
            $rosterInfo = AbiosHotBase::getRestInfo($roster_action, []);
            $rel_team_id = @$rosterInfo['team']['id'];
            $Redis->hset($redis_name,$roster_id,@$rosterInfo['team']['id']);
        }
        return $rel_team_id;
    }
}