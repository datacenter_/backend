<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\MatchLived;
use app\modules\org\models\Team;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\wsdata\WebsocketCommon;
use Throwable;
class BayesCsgoSocket extends WebsocketCommon
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $killEvent = '';
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $ctTeam = [];
    public $battle_duration = 0;
    public $muti = '';
    public $battleId = null;
    public $battleOrder = null;

    public $roundId = null;
    public $roundOrder = null;
    public $battleUrn = '';
    public $duration = 0;
    public $tableMapList = '';  //地图
    public $tableWeaponList = '';//武器
    public $playerList = [];
    public $CurrentPlayerList = [];
    public $currentRoundDetails = [];
    public $relMatchId = '';
    public $socketTime = '';
    public $needOutPutBattleEndEvent = '';
    public $needOutPutReloadEvent = '';
    public $needOutPutKillEvent = '';
    public $needOutPutUpComingEvent = '';
    public $needOutPutBanPickEvent = [];
    public $needOutPutFrame = '';
    public $needToApi = false;
    public $assistDbList = [];
    public $battlePlayerList = [];
    public $map = [];
    public $refreshType = '';
    public $isPause = false; //是否暂停
    public $origin_id = 10;

    public $perId = 0;
    const ORIGIN_ID = 10;
    const GAME_ID = 1;
    // 是否冻结时间
    public $is_freeze_time = false;
    // 对局内时间戳
    public $battle_timestamp = 0;

    const CSGO_BATTLE_UPCOMING = 'battle_upcoming';
    const CSGO_BATTLE_START = 'battle_start';
    const CSGO_BATTLE_END = 'battle_end';
    const CSGO_BATTLE_PAUSE = 'battle_pause';
    Const CSGO_ROUND_FREEZETIME_START = "round_freezetime_start";
    const CSGO_BATTLE_UNPAUSE = 'battle_unpause';
    const CSGO_BATTLE_RELOADED = 'battle_reloaded';
    const CSGO_PLAYER_JOINED = 'player_joined';
    const CSGO_PLAYER_QUIT = 'player_quit';
    const CSGO_BOMB_PLANTED = 'bomb_planted';
    const CSGO_BOMB_DEFUSED = 'bomb_defused';
    const CSGO_PLAYER_SUICIDE = 'player_suicide';
    const CSGO_ROUND_START = 'round_start';
    const CSGO_EVENT_KILL = 'kill';
    const CSGO_PLAYER_KILL = 'player_kill';
    const CSGO_ROUND_END = 'round_end';

    const CSGO_EVENT_INFO_TYPE_NO = 'no';
    const CSGO_EVENT_INFO_TYPE_EVENTS = 'events';
    const ROUND_START_ONE = 1;
    // 主/客 战队数组
    public static $teamIdsArray = [];
    // 本库战队 主/客 Order
    public static $teamIdsOrderArray = [];
    // 主/客 战队信息数组
    public static $teamIdsDataArray = [];

    public static $conversionRes = [
        'battle_order' => null,
        'trans_data' => 'hello'
    ];

    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function run($tagInfo, $taskInfo)
    {
        $self               = self::getInstance();
        $self->refresh_task = true;
        $refreshType        = @$tagInfo['ext2_type'];
        $refreshType        = $refreshType ? $refreshType : "one";
        $info               = json_decode($taskInfo['params'], true);
        $self->info         = $info;
        $originId           = $info['origin_id'];
        $self->origin_id = $originId;
        //match
        $self->perId = @$info['match_id'];
        $self->refreshType = $refreshType;
        if ($refreshType == 'refresh') {
            $self->matchId       = $info['match_id'];
            $rel_match_id        = $info['rel_match_id'];
            $rel_match_id_string = self::getPerIdByRelMatchId($rel_match_id, $originId);
        } else {
            $match_id_list   = self::getMatchIdByPerId($self->perId, $originId);
            $self->relMatchId = @$match_id_list['rel_identity_id'];
            $self->matchId   = @$match_id_list['matchId'];
        }

        if (empty($self->matchId)) {
            return false;
        }

        if ($refreshType == 'refresh') {
//            $relMatchId = $info['rel_match_id'];
            try {
                //删除
                $redisDelKey = $self->currentPre . ':' . $self->matchId . ':';
                $self->batchDelRedisKey($redisDelKey);
                $self->clearBattleMatchId($self->matchId);
                $self->match = BayesCsgoSocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
                if (empty($self->match) || $self->matchfg == 'no_match_info') {
                    return false;
                }
            } catch (Throwable $e) {
                $self->error_catch_log(['msg' => $e->getMessage()]);
            }

            $livedObj = MatchLived::find()->where(['and', ['game_id' => 1], ['match_id' => (string)$rel_match_id_string]]);
            $count    = $livedObj->count();
            $limit    = 1000;
            if ($count > $limit) {
                $countSlice = (int)ceil($count / $limit);
                for ($i = 0; $i < $countSlice; $i++) {
                    $offset    = 0 + $limit * $i;
                    $list      = $livedObj->orderBy('id')->offset($offset)->limit($limit)->asArray()->all();
                    $countList = count($list);
                    foreach ($list as $k => $v) {
//                        $start_time = date('Y-m-d H:i:s');//test
                        $matchDetail = json_decode($self->getValue(['current','match','details']),true);
                        if($matchDetail['end_at'] && $matchDetail['status'] == 3){
                            $redisExpireKey = $self->currentPre .':' . $self->matchId . ':';
                            $self->error_catch_log(['match_end' => true]);
                            $self->batchExpireRedisKey($redisExpireKey);
                            return false;
                        }
//                        $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
//                        if ($matchEnd) {
//                            self::changeHotData(7,1,$self->matchId);//调用hotRefresh
//                            $self->error_catch_log(['match_end' => true]);
//                            return false;
//                        }

                        if ($k + 1 == $countList) {
                            $self->refresh = true;
                            $self->error_catch_log(['end_refresh' => true]);
                        }
                        $self->info              = $v;
                        $self->info['socket_id'] = $v['id'];
                        $self->match_data        = json_decode($v['match_data'], true);
                        $self->doRun();
//                        $end_time = date('Y-m-d H:i:s');//test
//                        echo 'startTime--'.$start_time.'--endTime--'.$end_time.'--id--'.$self->info['socket_id'].PHP_EOL;//test
                    }
                }
            } else {
                $list      = $livedObj->orderBy('socket_time')->limit($limit)->asArray()->all();
                $countList = count($list);
                foreach ($list as $k => $v) {
                    $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                    if ($matchEnd) {
//                        self::changeHotData(7,1,$self->matchId);//调用hotRefresh
                        $self->error_catch_log(['match_end1' => true]);
                        return false;
                    }
                    if ($k + 1 == $countList) {
                        $self->refresh = true;
                        $self->error_catch_log(['end_refresh' => true]);

                    }
                    $self->info              = $v;
                    $self->info['socket_id'] = $v['id'];
                    $self->match_data        = json_decode($v['match_data'], true);
                    $self->doRun();
                }
            }
        } else {

            $self->match = BayesCsgoSocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
            if (empty($self->match) || $self->match == 'no_match_info') {
                return false;
            }
            $self->match_data = $info['match_data'];
            if (empty($info['match_data'])) {
                return false;
            }
            $self->doRun();
        }
    }

    public function doRun()
    {
        try {
            //初始化
            $this->needToApi=false;
            //结束后的操作
            $matchDetail = json_decode($this->getValue(['current','match','details']),true);
            if($matchDetail['end_at'] && $matchDetail['status'] == 3){
//                $has_expire = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE]);
//                if (!$has_expire){
//                    $redisDelKey = $this->currentPre .':' . $this->matchId . ':';
//                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE],1);
//                    $this->batchExpireRedisKey($redisDelKey);
//                }
//                $this->error_hash_log('match end after events',['event_id'=>$this->info['socket_id']]);
//                return  false;
            }

            $this->redis->incr('speed:' . $this->matchId);
            $match_data = $this->match_data;
            //原始数据解析
            $seqIdx       = $match_data['seqIdx'];
            $payload_one  = $match_data['payload'];
            $data_type    = $payload_one['payload']['type'];
            $data_subject = $payload_one['payload']['subject'];
            $data_action  = $payload_one['payload']['action'];
            $data_payload = $payload_one['payload']['payload'];
            $sourceUpdatedAt = $payload_one['payload']['sourceUpdatedAt'];
            //获取battle和order
            $liveDataMatchUrn = $payload_one['payload']['liveDataMatchUrn'];

            $bayesRedisBattleId     = BayesCsgoSocketBattleService::getBayesBattleId($this);
            $battleId               = BayesCsgoSocketBattleService::getBayesRealBattleId($this);
            $battleOrder            = BayesCsgoSocketBattleService::getBayesBattleOrder($this);
            $battleList             = BayesCsgoSocketBattleService::getBayesBattleList($this);

            $roundId                = BayesCsgoSocketBattleService::getBayesBattleRoundId($this);
            $roundOrder             = BayesCsgoSocketBattleService::getBayesBattleRoundOrder($this);
//            $roundList             = BayesCsgoSocketBattleService::getBayesBattleList($this);

            $tableMapList           = BayesCsgoSocketMatchService::getTableCsgoMapList($this);
            $tableWeaponList        = BayesCsgoSocketMatchService::getTableCsgoWeaponList($this);
            // 对局内时间戳
            $this->battle_timestamp = $this->currentBattleBeginAt($this);
            //是否是冻结时间
            $this->is_freeze_time   = $this->isFreezeTime();
            //是否是暂停时间
            $this->isPause          = $this->isPause();
            $this->battleOrder      = $battleOrder;
            $this->battleId         = $battleId;
            $this->roundId          = $roundId;
            $this->roundOrder       = $roundOrder;
            $this->battleUrn        = $liveDataMatchUrn;
            $this->duration         = $battleList[$liveDataMatchUrn] ? (json_decode($battleList[$liveDataMatchUrn], true) ? json_decode($battleList[$liveDataMatchUrn], true)['duration'] : 0) : 0;
            $this->tableMapList     = $tableMapList;
            $this->tableWeaponList  = $tableWeaponList;
            $this->doCommonData($this,$data_action,$data_payload);
            //判断事件类型
            switch ($data_type) {
                case 'INFO':
                    //判断事件子类型
                    if ($data_subject == 'MATCH') {
                        //判断事件 action
                        //ANNOUNCE 只有一次
                        if ($data_action == 'ANNOUNCE') {
                            BayesCsgoSocketBattleService::createBayesCsgoBattle($this,$this->matchId,$data_payload['fixture']['seriesCurrent'],$this->info['socket_time']);
                            BayesCsgoSocketEventService::matchAnnounceEvent($this, $this->matchId, $data_action, $data_payload, $battleId, $this->match);
                        }
                        //ROLLBACK 回档  gameTime 回档到何时
                        if ($data_action == 'ROLLBACK') {

                        }
                    }
                    break;
                case 'GAME_EVENT':
                    switch ($data_subject) {
                        case 'MATCH':
                            switch ($data_action) {
                                case 'START_MAP':
                                    BayesCsgoSocketBattleService::createBayesCsgoBattle($this,$this->matchId,$match_data['payload']['payload']['payload']['matchCurrent'],$this->info['socket_time']);
                                    // start_map事件处理
                                    BayesCsgoSocketEventService::startMapEvent($this,$this->matchId,$data_payload,$sourceUpdatedAt);
                                    break;
                                case 'FREEZE_TIME_STARTED': //冻结时间开始事件
                                    //创建该battle下的round
                                    $handle_data = BayesCsgoSocketEventService::freezeTimeStarted($this,$this->battleId,$data_payload);
                                    break;
                                case 'FREEZE_TIME_ENDED': //冻结时间结束事件
                                   BayesCsgoSocketEventService::freezeTimeEnded($this,$data_payload);
                                    break;
                                case 'START_ROUND':
                                    // round开始
                                    $handle_data = BayesCsgoSocketEventService::startRoundEvent($this,$data_payload);
                                    break;
                                case 'END_ROUND':
                                    // round结束
                                    $handle_data = BayesCsgoSocketEventService::endRoundEvent($this,$data_payload);
                                    break;
                                case 'END_MAP':
                                    // battle结束
                                    $handle_data = BayesCsgoSocketEventService::battleEndEvent($this,$data_payload,$sourceUpdatedAt);
                                    $this->setValue([self::KEY_TAG_CURRENT,self::CSGO_EVENT_INFO_TYPE_EVENTS,'type'],"END_MAP");
                                    break;
                                case 'START_PAUSE':
                                    //开始暂停
                                    $handle_data = BayesCsgoSocketEventService::startPauseEvent($this, $data_payload);
                                    break;
                                case 'END_PAUSE':
                                    //暂停结束
                                    $handle_data = BayesCsgoSocketEventService::endPauseEvent($this, $data_payload);
                                    break;
                            }
                            break;
                        case 'PLAYER':
                            switch ($data_action) {
                                case 'PLANTED_BOMB':  //安放炸弹事件
                                    $handle_data = BayesCsgoSocketEventService::plantedBomb($this, $data_payload);
                                    break;
                                case 'DEFUSED_BOMB': //拆除炸弹
                                    $handle_data = BayesCsgoSocketEventService::defusedBomb($this, $data_payload);
                                    break;
                                case 'KILL': //选手击杀事件
                                    $handle_data = BayesCsgoSocketEventService::KillEvent($this, $data_payload);
                                    break;
                                case 'PICKED_UP_ITEM':
                                    // 拾取道具
                                    $handle_data = BayesCsgoSocketEventService::ItemPickupEvent($this, $data_payload);
                                    break;
                                case 'PURCHASED_ITEM':
                                    // 购买物品
                                    $handle_data = BayesCsgoSocketEventService::purchasedItemEvent($this, $data_payload);
                                    break;
                                case 'UPDATE_SCORE':
                                    // 积分版更新
                                    $handle_data = BayesCsgoSocketEventService::updateScoreEvent($this, $data_payload);
                                    break;
                                case 'DROPPED_ITEM':
                                    // 掉落物品
                                    $handle_data = BayesCsgoSocketEventService::droppedItemEvent($this, $data_payload);
                                    break;
                                case 'THREW_ITEM': //投掷物品
                                    BayesCsgoSocketEventService::threwItemEvent($this, $data_payload);
                                    break;
                                case 'DEALT_DAMAGE': //造成伤害
                                    $handle_data = BayesCsgoSocketEventService::dealtDamage($this, $data_payload);
                                    break;
                                case 'DIED': //选手死亡
                                    if($data_payload['suicide'] == true){
                                        $handle_data = BayesCsgoSocketEventService::playerDied($this, $data_payload);
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case 'SNAPSHOT':
                    if ($data_subject == 'MATCH') {
                        if ($data_action == 'UPDATE') { //frame
//                            $battleDetail = $this->hGet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST],$liveDataMatchUrn);
                            $handle_data = BayesCsgoSocketMatchService::transFrames($this, $this->matchId, $battleId, $data_payload);
                        }
                    }
                    break;
                case 'UNKNOWN':
                    break;
            }
            //空内容不返回
            if (empty($handle_data)) {
                return false;
            }
            if ($this->needOutPutFrame) {
                $this->lPush([self::KEY_TAG_HISTORY, 'battle',$this->battleId,self::KEY_TAG_FRAMES], json_encode($this->needOutPutFrame, 320));
                //存入socket
                $data_ws = [
                    'match_data' => json_encode($this->needOutPutFrame, 320),
                    'type'       => 'frames',
                    'game_id'    => 1,
                    'match_id'   => $this->matchId
                ];
                $this->saveSocketToDb($data_ws);
                $this->needOutPutFrame = '';
            }

            if ($handle_data['type'] == 'frame') {
                $this->event_type ='frame';
                $data = $handle_data['data'];
                $this->lPush([self::KEY_TAG_HISTORY, 'battle', $this->battleId, self::KEY_TAG_FRAMES], json_encode($data, 320));
                //存入socket
                $data_ws     = [
                    'match_data' => json_encode($data, 320),
                    'type'       => 'frame',
                    'game_id'    => 1,
                    'match_id'   => $this->matchId
                ];
                $this->event = $handle_data['data'];
                $this->saveSocketToDb($data_ws);
            } else {
                if(isset($handle_data['data']) && $handle_data['data']){
                    $data = $handle_data['data'];
                    $this->event_type =$data['event_type'];
                    $this->lPush(['history', 'battle', $this->battleId, 'events'], json_encode($data, 320));
                    $this->lPush(['history', 'battle', $this->battleId, 'round',$this->roundOrder,'events'], json_encode($data, 320));
                    //存入socket
                    $data_ws = [
                        'match_data' => json_encode($data, 320),
                        'type'       => 'events',
                        'game_id'    => 1,
                        'match_id'   => $this->matchId
                    ];
                    $this->saveSocketToDb($data_ws);
                }else{ //二维数组
                    foreach ($handle_data as $value){
                        $data = $value['data'];
                        $this->event_type =$data['event_type'];
                        $this->lPush(['history', 'battle', $this->battleId, 'events'], json_encode($data, 320));
                        $this->lPush(['history', 'battle', $this->battleId, 'round',$this->roundOrder,'events'], json_encode($data, 320));
                        //存入socket
                        $data_ws = [
                            'match_data' => json_encode($data, 320),
                            'type'       => 'events',
                            'game_id'    => 1,
                            'match_id'   => $this->matchId
                        ];
                        $this->saveSocketToDb($data_ws);
                    }
                }
            }

            $this->refreshToApi();


        } catch (Throwable $e) {

            $this->error_catch_log(['msg' => $e->getMessage()]);
        }

    }

    /**
     * 从Ws插入队列去SetApi
     * @return bool
     * @throws \yii\db\Exception
     */
    public function refreshToApi()
    {

        //task refresh api
        $contentArr  = [];
        $nowTime     = time();
        $origin      = $this->currentPre;
        $matchId     = $this->matchId;
        $battle_is_end  = $this->needToApi;
        $eventId     = $this->info['socket_id'];
        $eventType   = $this->event_type;
        $key         = ':tag_csgo_api_refresh_time:';
        $redisKey    = $this->currentPre . $key . $this->matchId;
        $refreshTime = $this->redis->get($redisKey);
        if ($refreshTime && $this->matchId) {
            $diff_time = $nowTime - $refreshTime;
            if ($diff_time > 2  || $this->matchEndFlag || $battle_is_end) {
                if ($this->matchEndFlag) {
                    $this->matchEndFlag = false;
                }
                if ($battle_is_end){
                    $this->needToApi=false;
                }
                $updateType = 2;
                $paramArr   = [
                    'updateType' => $updateType,
                    'match_id'   => $this->matchId,
                    'event_id'   => @$eventId,
                    'event_type' => @$eventType,
                    'battleUrn'   => @$this->battleUrn,
                    'currentPre' => $this->currentPre,
                    'origin_id' => @$this->origin_id,
                    'refreshType' => @$this->refreshType,
                ];
                $this->lPush(['ws_to_api'],json_encode($paramArr,320));
//                    WsToApiSynchron::rPushSynchron($origin, $matchId, $paramArr);
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        9, "", "", 1),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => $paramArr,
                ];
                TaskRunner::addTask($item, 4);
                $this->redis->set($redisKey, $nowTime, 3600);
            }


        } else {
            $this->redis->set($redisKey, $nowTime, 3600);
        }
    }

    // match比赛比分和获胜战队
    public static function matchWinnerAndScore($matchInfo,$terroristScore,$teamTid,$ctScore,$teamCtid,$originId)
    {
        if($terroristScore>$ctScore){
            $winnerTeamId = self::teamIdChangeOwnTeamId($teamTid,$originId);
            $matchWinner = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId);
            $matchWinnerSoure = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId,$terroristScore);
        }
        if($terroristScore<$ctScore){
            $winnerTeamId = self::teamIdChangeOwnTeamId($teamCtid,$originId);
            $matchWinner = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId);
            $matchWinnerSoure = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId,$ctScore);
        }
        if($terroristScore==$ctScore){
            $matchWinner = [];
            $matchWinnerSoure = [];
        }
        return [
            'match_winner' => $matchWinner,
            'match_winner_soure' => $matchWinnerSoure,
        ];
    }
    // 数据源teamId转换本库teamId
    public static function teamIdChangeOwnTeamId($relTeamId,$originId)
    {
        if($relTeamId) {
            if (isset(self::$teamIdsArray[$relTeamId])) {
            } else {
                self::$teamIdsArray[$relTeamId] =
                    intval(self::getMainIdByRelIdentityId('team', $relTeamId, $originId));
            }
            return self::$teamIdsArray[$relTeamId];
        }else{
            return null;
        }
    }
    // 本库teamId查询战队信息
    public static function teamIdSelectTeamsData($matchInfo,$teamId,$score=null)
    {
        if (isset(self::$teamIdsDataArray[$teamId])) {
        } else {
            self::$teamIdsDataArray[$teamId] = Team::find()->select([
                    'id as team_id','name','image'
                ])->where(['id' => $teamId])->asArray()->one();
            self::$teamIdsDataArray[$teamId]['opponent_order'] =
                self::teamIdChangeTeamOrder($teamId,$matchInfo);
        }
        if($score){
            self::$teamIdsDataArray[$teamId]['score'] = $score;
        }
        return self::$teamIdsDataArray[$teamId];
    }
    // 主客队order
    public static function teamIdChangeTeamOrder($teamIdResult,$matchInfo)
    {
        if (isset(self::$teamIdsOrderArray[$teamIdResult])) {
        } else {
            self::$teamIdsOrderArray[$teamIdResult] = self::getTeamOrderByTeamId($teamIdResult,$matchInfo);
        }
        return self::$teamIdsOrderArray[$teamIdResult];
    }
}
