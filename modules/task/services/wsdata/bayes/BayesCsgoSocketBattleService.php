<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\wsdata\WebsocketCommon;
use app\rest\exceptions\BusinessException;
use Throwable;
use yii\base\Exception;

class BayesCsgoSocketBattleService extends WebsocketCommon implements SocketBattleInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;

    //获取bayes的battleId
    public static function getBayesBattleId($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BAYES_BATTLE_ID];
        return $model->getValue($key);
    }
    //设置bayes的battleId
    public static function setBayesBattleId($model,$value)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BAYES_BATTLE_ID];
        return $model->setValue($key,$value);
    }
    //获取真实battleId
    public static function getBayesRealBattleId($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BATTLE_ID];
        return $model->getValue($key);
    }
    public static function getBayesBattleOrder($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_ORDER];
        return $model->getValue($key);
    }
    // 获取round回合Id
    public static function getBayesBattleRoundId($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_ROUND,'round_id'];
        return $model->getValue($key);
    }
    // 获取round回合序号
    public static function getBayesBattleRoundOrder($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_ROUND,self::KEY_TAG_ORDER];
        return $model->getValue($key);
    }
    public static function getBayesBattleList($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,self::KEY_TAG_LIST];
        return $model->hGetAll($key);
    }

    public static function createBattle($model, $matchId, $matchInfo, $newBattleOrder, $socketTime)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        if ($newBattleOrder > $matchInfo['number_of_games']) {
            //说明是错误的order
            return false;
        }
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $newBattleOrder,
        ])->one();
        if (empty($battleBase)) {
            //插入battle
            $battleBase = new MatchBattle();
            $battleBase->setAttributes([
                'match'      => $matchId,
                'order'      => $newBattleOrder,
                'deleted'  => 2,
                'deleted_at' => null,
            ]);
            $battleBase->save();
            $model->setBattleBeginAt($socketTime);
        }
        $battleBase->setAttributes([
            'deleted'  => 2,
            'deleted_at' => null,
        ]);
        $battleBase->save();
        $model->battleId = $battleBase->id;
        $model->setBattleId($battleBase->id);
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER];
        return $battleBase->id;
    }

    public static function incrCurrentBattleBattleOrder($model)
    {
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_ORDER];
        return $model->incr($key);
    }


    public static function getBattleDetailsByUrn($model, $battleUrn)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $battleList = self::getBayesBattleList($model);
        if (!empty($battleList)) {
            return json_decode($battleList[$battleUrn], true);
        } else {
            return null;
        }
    }

    public static function setBattleDetailsByUrn($model,$battleUrn,$updateData){
        if (empty($model)){
            $model = new BayesCsgoSocket();
        }
        if (!is_array($updateData)){
            return false;
        }
        $battleList = self::getBayesBattleList($model);
        $battle_details =  json_decode($battleList[$battleUrn],true);
        if ($battle_details&&is_array($battle_details)){
            $battleUpdate  = array_merge($battle_details,$updateData);
        }else{
            $battleUpdate = $updateData;
        }
        $battleUpdateJson = json_encode($battleUpdate,320);
        $key =  ['history','battle','list'];
        $model->hSet($key,$battleUrn,$battleUpdateJson);
        return $battleUpdate;
    }





    public static function getMap($model,$map_name)
    {
        if (!$model){
            $model= new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$model->battleId,self::KEY_TAG_MAP];
        $redisBattleMapJson = $model->getValue($key);
        if($redisBattleMapJson){
            $dBMap = json_decode($redisBattleMapJson,true);
        } else {
            $dBMap = MetadataCsgoMap::find()->alias('m')
                ->select(['m.id as map_id', 'm.name', 'm.name_cn', 'm.external_id', 'm.external_name', 'm.short_name', 'enum.e_name as map_type', 'enum.c_name as map_type_cn', 'm.is_default', 'm.slug',
                    'm.square_image', 'm.rectangle_image', 'm.thumbnail'])
                ->leftJoin('enum_csgo_map as enum', 'm.map_type = enum.id')
                ->where(['m.external_name' => $map_name, 'm.state' => 1])
                ->asArray()->one();
            if (!empty($dBMap)) {
                $dBMap['image'] = [
                    'square_image' => $dBMap['square_image'],
                    'rectangle_image' => $dBMap['rectangle_image'],
                    'thumbnail' => $dBMap['thumbnail'],
                ];
                $dBMap['map_id'] = (int)$dBMap['map_id'];
                $dBMap['is_default'] = $dBMap['is_default'] == 1 ? true : false;
                unset($dBMap['square_image']);
                unset($dBMap['rectangle_image']);
                unset($dBMap['thumbnail']);
                $model->setValue($key, json_encode($dBMap));
            }
        }
        return $dBMap;
    }
    /**
     * @param $matchId
     * @param $battle_order
     * @param $socket_time
     * @param $rel_matchId
     * @param $rel_battleId
     * @return mixed
     * @throws BusinessException
     * 创建battle
     */
    public static function createBayesCsgoBattle($model,$matchId,$battle_order,$socket_time)
    {
        if(!$model){
            $model = new BayesCsgoSocket();
        }


        if (empty($battle_order)){
            $currentBattleOrder = $model->getBattleOrder();
            $battle_order = !empty($currentBattleOrder)?$currentBattleOrder:1;
        }
        //创建battle，battle状态改为进行中
        $matchBattle=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle_order,
        ])->one();

        if(!$matchBattle){
            $matchBattle = new MatchBattle();
            $insertData = [
                'game'=>1,
                'match'=>$matchId,
                'order'=>(int)$battle_order,
                'begin_at'=>$socket_time,
                'status'=>2,
            ];
        }else{
            $insertData = [
                'begin_at'=>$socket_time,
                'status'=>2,
            ];
        }
        $matchBattle->setAttributes($insertData);
        //redis存当前battleId和battleOrder
        $model->setBattleId($matchBattle['id']);
        $model->setBattleOrder($matchBattle['order']);
        $model->battleId = $matchBattle->id;
        $model->battleOrder = $matchBattle->order;
        $battleUpdateArr = ['battle_id'=> $model->battleId ,'order'=>$model->battleOrder,'status'=>2];
        BayesCsgoSocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$battleUpdateArr);
        return $matchBattle->id;

    }

    public static function createCsgoRound($model, $battleId, $roundOrder)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        //存储当前roundid和order
        $model->setCurrentRoundOrder($model, $roundOrder);
        $model->roundOrder            = $roundOrder;
        $eventArr                     = BayesCsgoSocketEventService::getEventCommon($model, BayesCsgoSocket::CSGO_ROUND_FREEZETIME_START);
        $eventArr['is_bomb_planted']  = false;
        $eventArr['time_since_plant'] = 0;
        return $eventArr;
    }



}
