<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\WinnerHelper;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\org\models\Player;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\wsdata\WebsocketCommon;
use PHPUnit\phpDocumentor\Reflection\Types\Self_;

class BayesCsgoSocketEventService extends WebsocketCommon implements SocketEventInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;




    /**
     * 第一条数据
     * @param $model
     * @param $matchId
     * @param $type
     * @param $data_payload
     * @param $battleId
     * @param $matchInfo
     * @throws \app\rest\exceptions\BusinessException
     */
    public static function matchAnnounceEvent($model, $matchId, $type, $data_payload, $battleId, $matchInfo)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $model->setCurrentRoundOrder($model,1);
        $teamList = BayesCsgoSocketTeamService::getTeamList($model);
        if (!empty($data_payload) && empty($teamList)) {
            $teams                = $data_payload['teams'];
            $team_1 = $matchInfo['team_1_id'];
            $team_2 = $matchInfo['team_2_id'];
            $per1 = $teams[0]['urn'];
            $per2 = $teams[1]['urn'];
            $matching_teams = HotBase::socketMatchingTeam($team_1,$team_2,$per1,$per2,$model->origin_id,BayesCsgoSocket::GAME_ID);
            $matchDetails = BayesCsgoSocketMatchService::getMatchDetails($model);
            //循环匹配
            foreach ($teams as $key => $item) {
                $team_urn_id = $item['urn'];
                //查询队伍ID
                $team_id = $matching_teams[$team_urn_id];
                //获取$team_order
                if(empty($team_id)){
                    $team_order = null;
                }else{
                    $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
                }                //处理玩家
                $team_participants = $item['participants'];
                foreach ($team_participants as $key_p => $item_p) {
                    $player_urn_id = $item_p['urn'];
//                    $player_rel_name_array = @explode(' ',$item_p['name']);//空格 分割
                    $player_rel_name = @$item_p['name'];

                    $nick_name = $item_p['name'];
                    $steam_id  = $item_p['references']['STEAM_ID'];
                    $player_id = @self::getMainIdOfBayesByUrn('player', $model->origin_id, $player_urn_id, 1);
                    if ($player_id) {
                        $Player_Info =  Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                        $nick_name   = @$Player_Info['nick_name'];
                    }
                    $player_Info_base = $model->initPlayerBase([]);
                    $player_Info_extra = [
                        'order'           => $key_p + 1,
                        'rel_identity_id' => $player_urn_id,
                        'rel_team_id'     => $team_urn_id,
                        'team_order'      => $team_order,
                        'game'            => 1,
                        'match'           => $matchId,
                        'team_id'         => $team_id,
                        'side'         => null,
                        'player_id'       => $player_id,
                        'nick_name'       => $nick_name,
                        'rel_nick_name'   => $player_rel_name,
                        'steam_id'        => $steam_id,

                    ];
                    $player_Info = array_merge($player_Info_base,$player_Info_extra);
                    BayesCsgoSocketPlayerService::setPlayerInfo($model, $player_urn_id, $player_Info);
                }
                if (!empty($matchDetails)){
                    $score =(int)$matchDetails["team_".$team_order."_score"];
                }else{
                    $score = 0;
                }
                $teamDbInfo = BayesCsgoSocketTeamService::getTableTeamByTeamId($model,$team_id);
                $team_info = [
                    'team_id'         => $team_id,
                    'opponent_order'           => $team_order,
                    'side'         => null,
                    'score'           => $score,
                    'rel_identity_id' => $team_urn_id,
                    'identity_id'     => $team_urn_id,
                ];
                if (!empty($teamDbInfo)){
                    $team_info['name']= $teamDbInfo['name'];
                    $team_info['image']= $teamDbInfo['image_200x200'];
                }
                BayesCsgoSocketTeamService::setTeamInfo($model, $team_urn_id, $team_info);
            }
        }

        return true;
    }

    /**
     * 冻结时间开始
     */
    public static function freezeTimeStarted($model, $battleId, $dataPayload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $customBattleStartJson = $model->getValue(['current', 'custom', 'battleStart']);
        $customUpComingJson    = $model->getValue(['current', 'custom', 'upComing']);
        if ($customBattleStartJson & $customUpComingJson) {
            $customBattleStartArr           = json_decode($customBattleStartJson, true);
            $customUpComingArr              = json_decode($customUpComingJson, true);
            $customUpComingArrEvent['type'] = 'events';
            $customUpComingArrEvent['data'] = $customUpComingArr;
            foreach ($dataPayload['teams'] as $k => $v) {
                $side     = strtolower($v['side']);
                $teamInfo = BayesCsgoSocketTeamService::getTeamInfo($model, $v['teamUrn']);
                if (in_array($side, ['ct', 'terrorist'])) {
                    $updateArr['side'] = $side;
                    if ($model->roundOrder == 1) {
                        $updateArr['starting_side'] = $side;
                    }
                    BayesCsgoSocketTeamService::setTeamInfo($model, $v['teamUrn'], $updateArr);
                    $roundTeamArr = $updateArr;
                    $roundTeamArr['survived_players'] = 5;
                    BayesCsgoSocketRoundService::setTeamInfo($model, $v['teamUrn'], $updateArr);
                    if ($model->roundOrder == 1) {
                        if ($side == 'terrorist') {
                            $side = 't';
                        }
                        $customBattleStartArr["starting_$side"]['team_id']        = $teamInfo['team_id'];
                        $customBattleStartArr["starting_$side"]['name']           = $teamInfo['name'];
                        $customBattleStartArr["starting_$side"]['image']          = $teamInfo['image'];
                        $customBattleStartArr["starting_$side"]['opponent_order'] = $teamInfo['opponent_order'];
                        $customBattleStartArrEvent['type'] = 'events';
                        $customBattleStartArrEvent['data'] = $customBattleStartArr;
                    }

                }
            }


        }
        //清除上一个round数据(清除选手武器，清除roundId,roundOrder)
        $model->clearPlayerWeapons();
        $model->delAll([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ROUND_ID]);
        $model->delAll([self::KEY_TAG_CURRENT, self::KEY_TAG_ROUND, self::KEY_TAG_ORDER]);
        //是否是冻结时间
        $model->is_freeze_time = true;
        $model->setFreezeTime(true);
        $roundFreezetimeStartField = BayesCsgoSocketBattleService::createCsgoRound($model, $battleId, $dataPayload['currentRoundNumber']);
        //删除历史round的炸弹状态
//        $model->setBombState($model,$model->battleId,$model->roundOrder,null);
        //给每个选手加金钱
        foreach ($dataPayload['playerBalances'] as $data_payload_value) {
            $dataBalance = [
                'money' => $data_payload_value['balance']
            ];
            BayesCsgoSocketPlayerService::setPlayerInfo($model, $data_payload_value['playerUrn'], $dataBalance);
        }
        $outData = [
            'type' => 'events',
            'data' => $roundFreezetimeStartField
        ];
        if (!empty($customBattleStartArrEvent) && !empty($customUpComingArrEvent)) {
            $mutiEvents[] = $customUpComingArrEvent;
            $mutiEvents[] = $customBattleStartArrEvent;
        }
        $mutiEvents[] = $outData;
        return $mutiEvents;

    }
    public static function freezeTimeEnded($model,$data_payload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $model->setFreezeTime(false);
        $model->is_freeze_time = false;
        return ;
    }

    /**
     * @param $model
     * @param $data_payload
     * @return array
     */
    public static function startRoundEvent($model, $data_payload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $updateArr = [];
        $updateArr['begin_at'] = $model->info['socket_time'];
        $updateArr['status'] = 2;
        BayesCsgoSocketRoundService::setRoundDetailsByRoundOrder($model,$model->roundOrder,$updateArr);
        // 对局内时间戳
        $eventArr = BayesCsgoSocketEventService::getEventCommon($model, BayesCsgoSocket::CSGO_ROUND_START);
        //输出round_start事件
        return [
            'type' => 'events',
            'data' => $eventArr
        ];
    }

    //炸弹安放事件
    public static function plantedBomb($model, $data_payload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $plantBombPlayer = BayesCsgoSocketPlayerService::getPlayerInfo($model,$data_payload['playerUrn']);
        //输出炸弹安放事件
        $bombPlantedFieldCommon = BayesCsgoSocketEventService::getEventCommon($model, 'bomb_planted');
        $bombPlantedFieldExtra  = [
            'is_bomb_planted' =>true,
            'time_since_plant' =>$model->round_time,
            'player_id'           => $plantBombPlayer['player_id'],
            'nick_name'           => $plantBombPlayer['nick_name'],
            'steam_id'            => $plantBombPlayer['steam_id'],
            'side'                => $plantBombPlayer['side'],
            'position'            => $plantBombPlayer['position'],
            'survived_players_ct' => 4,
            'survived_players_t'  => 1,
        ];
        $bombPlantedField       = array_merge($bombPlantedFieldCommon, $bombPlantedFieldExtra);
        // c4 去除装备
        $itemArray = [
            'c4' => []
        ];
        BayesCsgoSocketPlayerService::setPlayerWeapon($model, $data_payload['playerUrn'], $itemArray, $isDel = true);
        return [
            'type' => 'events',
            'data' => $bombPlantedField
        ];
    }

    /**
     * @param $model
     * @return array
     * 炸弹拆除
     */
    public static function defusedBomb($model, $data_payload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $bombState = $model->getBombState($model, $model->battleId, $model->roundOrder);
        if ($bombState['is_bomb_planted'] == true) { //是否安装
            $bombStateData = [
                'is_bomb_planted'  => true,
                'time_since_plant' => substr($data_payload['timeRemaining'], 0, -3),
                'player_urn'       => $bombState['player_urn'],
                'defused'          => true,
            ];
            $model->setBombState($model, $model->battleId, $model->roundOrder, json_encode($bombStateData, 320));
        }
        $bombPlayer = BayesCsgoSocketPlayerService::getPlayerInfo($model, $data_payload['playerUrn']);
        $side       = BayesCsgoSocketTeamService::getTorCtByTeamUrn($model, $data_payload['teamUrn']);
        //输出炸弹拆除事件
        $bombDefusedFieldCommon = BayesCsgoSocketEventService::getEventCommon($model, 'bomb_defused');
        $bombDefusedFieldExtra  = [
            'is_bomb_planted'     => false,
            'time_since_plant'    => 0,
            'player_id'           => $bombPlayer['player_id'],
            'nick_name'           => $bombPlayer['nick_name'],
            'steam_id'            => $bombPlayer['steam_id'],
            'side'                => $side,
            'position'            => $bombPlayer['position'],
            'survived_players_ct' => 4,
            'survived_players_t'  => 1,
        ];
        $bombDefusedField       = array_merge($bombDefusedFieldCommon, $bombDefusedFieldExtra);
        return [
            'type' => 'events',
            'data' => $bombDefusedField
        ];
    }
    // 获取当前战队阵营信息数据
    public static function getCurrentRoundTeamsSide($model){
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $teamCtJson = $model->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_CT, self::KEY_TAG_DETAILS]);
        $teamTerroristJson = $model->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_T, self::KEY_TAG_DETAILS]);
        return [
            'current_ct' => json_decode($teamCtJson,true),
            'current_t'  => json_decode($teamTerroristJson,true),
        ];
    }

    // battle结束事件(更新match比分)
    public static function battleEndEvent($model,$data_payload,$sourceUpdatedAt)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        // 获取当前战队阵营
        $currentTeamsSide = self::getCurrentRoundTeamsSide($model);
        // 获胜方
        $winnerTeam = null;
        if($currentTeamsSide['current_ct']['teamUrn'] == $data_payload['winningTeamUrn']){
            unset($currentTeamsSide['current_ct']['teamUrn']);
            $winnerTeam = $currentTeamsSide['current_ct'];
        } elseif ($currentTeamsSide['current_t']['teamUrn'] == $data_payload['winningTeamUrn']){
            unset($currentTeamsSide['current_t']['teamUrn']);
            $winnerTeam = $currentTeamsSide['current_t'];
        }
        //更新battleDetail信息
        $winningBattleTeamOne = BayesCsgoSocketTeamService::getTeamInfo($model, $data_payload['winningTeamUrn']);
        $battleDetails        = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        $battleDetailData     = [
            'status' => 3,
            'end_at' => $model->info['socket_time'],
            'winner' => $winningBattleTeamOne['opponent_order'],
        ];
        $updateBattleDetail   = array_merge($battleDetails, $battleDetailData);
        BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $updateBattleDetail);
        // 更新current:match:details （比分），current:team:list比分
        $bayesDetail = BayesCsgoSocketMatchService::getMatchDetails($model);
        $tableMatch = BayesCsgoSocketMatchService::getMatchInfoOrInitMatch($model,$model->matchId);
        if($tableMatch['team_1_id'] == $winnerTeam['team_id']){
            $bayesDetail['team_1_score'] = $bayesDetail['team_1_score']+1;
            BayesCsgoSocketMatchService::setMatchDetails($model,$bayesDetail);
        }elseif ($tableMatch['team_2_id'] == $winnerTeam['team_id']){
            $bayesDetail['team_2_score'] = $bayesDetail['team_2_score']+1;
            BayesCsgoSocketMatchService::setMatchDetails($model,$bayesDetail);
        }
        BayesCsgoSocketTeamService::setTeamInfo($model,$data_payload['winningTeamUrn'],['score'=>$winningBattleTeamOne['score'] + 1]);
        // 比赛信息
        $matchInfo = BayesCsgoSocketMatchService::getMatchInfoOrInitMatch($model, $model->matchId);
        // 判断比赛是否结束
        $winnerInfo = WinnerHelper::csgoWinnerInfo($bayesDetail['team_1_score'], $bayesDetail['team_2_score'], $matchInfo['number_of_games']);
        //比赛结束
        if($winnerInfo['is_finish'] == 1){
            // 记录结束时间，比赛状态
            BayesCsgoSocketMatchService::setMatchDetails($model,[
                'status' => 3,
                'end_at' => date('Y-m-d H:i:s',strtotime($sourceUpdatedAt)),
                'winner'=> $winnerInfo['winner_team'],
            ]);
        }
        // 获取安放炸弹信息
        $bombInfoJson = $model->hGet([self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$model->battleId,self::KEY_TAG_ROUND,"bombState"],$data_payload['roundNumber']);
        if ($bombInfoJson) {
            $bombInfo = json_decode($bombInfoJson, true);
        }
        // battle_end事件字段
        $eventInfo = BayesCsgoSocketEventService::getEventCommon($model,'battle_end');
        $eventInfo['winner'] = $winnerTeam;
        $winnerMatchTeam = null;
        $teamListFormat = BayesCsgoSocketTeamService::getTeamListFormat($model,'urn');
        if($winnerInfo['is_finish'] == 1){
            if($winnerInfo['winner_team'] == 1){
                $winnerTeamId = $matchInfo['team_1_id'];
            }
            if($winnerInfo['winner_team'] == 2){
                $winnerTeamId = $matchInfo['team_2_id'];
            }
            foreach ($teamListFormat as $v){
                if($v['team_id'] == $winnerTeamId){
                    $winnerMatchTeam = $v;
                }
            }
        }

        // 取出frame
        $currentFrameJson = $model->getValue([self::KEY_TAG_CURRENT,'battle',"frame"]);
        if ($currentFrameJson) {
            $currentFrame = json_decode($currentFrameJson, true);
            $currentFrame['is_battle_finished'] = true;
            $currentFrame['is_match_finished'] = $winnerInfo['is_finish']==1;
            $currentFrame['match_scores'] = BayesCsgoSocketTeamService::getTeamListFormat($model,'',['score']);
            $currentFrame['match_winner'] = $winnerMatchTeam;
            $currentFrame['battle_winner'] = $winnerTeam;
            // 获取当前战队阵营
            $currentTeamsSide = self::getCurrentRoundTeamsSide($model);
            $current_t_teamUrn = $currentTeamsSide['current_t']['teamUrn'];
            $current_ct_teamUrn = $currentTeamsSide['current_ct']['teamUrn'];
            foreach ($currentFrame['side'] as $k => $side) {
                if ($battleDetails[$current_t_teamUrn]['team_id'] == $side['team']['team_id']) {
                    $currentFrame['side'][$k]['score'] = $battleDetails[$current_t_teamUrn]['score'];
                } elseif ($battleDetails[$current_ct_teamUrn]['team_id'] == $side['team']['team_id']) {
                    $currentFrame['side'][$k]['score'] = $battleDetails[$current_ct_teamUrn]['score'];
                }
            }
        }
            // 现刷新frame
            $model->needOutPutFrame = $currentFrame;
            $model->needToApi = true;
        // 清除当前redis - key
//        $model->battleEndClear($model);
        // 更新redis-key : csgo_ws:3114:current:player:list
        // 更新redis-key : csgo_ws:3114:current:battle:map:details
        // 更新redis-key : csgo_ws:3114:current:battle:details （删除）
        return [
            'type' => 'events',
            'data' => $eventInfo
        ];
    }



    public static function getWinTypeOnRoundEnd($model,$data_payload,$current_t_teamUrn,$current_ct_teamUrn){
        if ($data_payload['winReason'] == "TEAM_ELIMINATION") {
            if ($current_t_teamUrn == $data_payload['winningTeamUrn']) {
                $winnerBayes = "T_TEAM_ELIMINATION";
            } elseif ($current_ct_teamUrn == $data_payload['winningTeamUrn']) {
                $winnerBayes = "CT_TEAM_ELIMINATION";
            } else {
                $winnerBayes = "";
            }
        } else {
            $winnerBayes = $data_payload['winReason'];
        }
        $bombState = $model->getBombState($model,$model->battleId,$model->roundOrder);
        if($data_payload['winReason'] == "BOMB_DEFUSED") {  //获胜方式为被拆除时，更新炸弹状态
            $bombStateData = [
                'is_bomb_planted' => $bombState['is_bomb_planted'],
                'time_since_plant' => substr($data_payload['timeRemaining'],0,-3),
                'player_urn' => $bombState['player_urn'],
                'defused' => true,
            ];
            $model->setBombState($model, $model->battleId, $model->roundOrder, json_encode($bombStateData,320));
        }
        // 获胜方式
        return Common::getBayesWinReason($winnerBayes);
    }


    /**
     * 记录是否有1vn
     * @param $win_side
     * @param string $source
     * @return bool
     */
    public static function do1vnOnRoundEnd($model, $winTeamUrn,$failTeamUrn){
        $winTeamInfo = BayesCsgoSocketRoundService::getTeamInfo($model,$winTeamUrn);
        $failTeamInfo = BayesCsgoSocketRoundService::getTeamInfo($model,$failTeamUrn);
        if ($winTeamInfo['1vn_player_count'] == 1) {
            $playerUrn =$winTeamInfo['1vn_player_urn'];
            //1vn成功
            $nPlayers   = $failTeamInfo['1vn_player_count'];
            //do 1vn
            if ($nPlayers) {
                $updateArr   = [];
                $arr         = [
                    1 => 'one',
                    2 => 'two',
                    3 => 'three',
                    4 => 'four',
                    5 => 'five',
                ];
                $num         = $arr[$nPlayers];
                $updateArr['one_on_' . $num . '_clutches'] = 1;
                $updateArr['one_on_x_clutches'] = 1;
                if ($playerUrn){
                    BayesCsgoSocketPlayerService::setPlayerByUrn($model,$playerUrn,$updateArr,'incr');
                }
                return true;
            }else{
                return false;
            }
        }else{
            //1vn失败
            return false;
        }
    }


    public static function doKastOnRoundEnd($model)
    {

        $roundPlayerList = BayesCsgoSocketRoundService::getPlayerList($model,$model->roundOrder);
        if (!empty($roundPlayerList)) {
            $teamKast =[];
            //kast
            foreach ($roundPlayerList as $k => $roundPlayer) {
                if (!empty($roundPlayer)) {
                    if ($roundPlayer['kills'] >= 1 || $roundPlayer['deaths'] == 0 || $roundPlayer['assists'] >= 1 || $roundPlayer['trade'] >= 1) {
                        //status fields
                        $updateArrStatistics   = [];
                        $updateArrStatistics['kast_number'] = 1;
                        BayesCsgoSocketPlayerService::setPlayerInfo($model,$roundPlayer['rel_identity_id'],$updateArrStatistics,'incr');
                    }
                    $battlePlayer = BayesCsgoSocketPlayerService::getPlayerInfo($model,$k);
                    $roundCount   = $model->roundOrder;
                    if ($roundCount) {
                        $kast = $battlePlayer['kast_number'] / $roundCount;
                        $playerKast = round($kast,4);
                        $teamKast[$roundPlayer['teamUrn']]['kast'] += $playerKast;
                        $teamKast[$roundPlayer['teamUrn']]['playerCount'] += 1;
                        BayesCsgoSocketPlayerService::setPlayerInfo($model,$k, ['kast' => $playerKast]);
                    }
                }
            }
            foreach ($teamKast as $k=>$v){
                if($v['playerCount']){
                    $teamKastValue = round($v['kast']/$v['playerCount'],4);
                    BayesCsgoSocketTeamService::setTeamInfo($model,$k,['kast'=>$teamKastValue]);
                }

            }

        }
    }


    /**
     * @param $model
     * @param $data_payload
     * @return array
     * 回合结束
     */
    public static function endRoundEvent($model,$data_payload)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        // 获取当前战队阵营
        $ct = BayesCsgoSocketTeamService::getTeamBySide($model, 'ct');
        $t  = BayesCsgoSocketTeamService::getTeamBySide($model, 'terrorist');
        //urn
        $current_t_teamUrn  = $t['teamUrn'];
        $current_ct_teamUrn = $ct['teamUrn'];
        //获胜方式
        $winnerType = self::getWinTypeOnRoundEnd($model, $data_payload, $current_t_teamUrn, $current_ct_teamUrn);
        $winnerTeam = BayesCsgoSocketTeamService::getTeamInfo($model, $data_payload['winningTeamUrn']);
        $teamList  = BayesCsgoSocketTeamService::getTeamList($model);
        foreach ($teamList as $k=>$v){
            if ($v['rel_identity_id']!=$data_payload['winningTeamUrn']){
                $failTeamInfo = $v;
            }
        }
        //设置1vn
        self::do1vnOnRoundEnd($model,$data_payload['winningTeamUrn'],$failTeamInfo['rel_identity_id']);
        // 当前回合记录
        $roundsHistory = [
            'round_ordinal'    => $data_payload['currentRoundNumber'],
            'survived_players' => null, //待完善
            'round_end_type'   => $winnerType,
            'winner_order'     => $winnerTeam['opponent_order'],
            'end_at'     => $model->info['socket_time'],
            'status'     => 3,
        ];
        BayesCsgoSocketRoundService::setRoundDetailsByRoundOrder($model, $data_payload['currentRoundNumber'], $roundsHistory);
        //在roundEnd的时候设置小比分
        $battleDetails = self::setScoreOnRoundEnd($model, $current_ct_teamUrn, $current_t_teamUrn, $data_payload, $t, $ct);
        // 判断获胜阵营
        $winnerSide = $data_payload['winningTeamUrn'] == $current_t_teamUrn ? "terrorist" : "ct";
        //记录特殊胜局
        $ctScore = $battleDetails[$current_ct_teamUrn]['score'];
        $tScore  = $battleDetails[$current_t_teamUrn]['score'];
        self::doScoreOnRoundEnd($model, $ctScore, $tScore, $winnerSide, $winnerTeam ,$t,$ct);
        //记录特殊击杀事件
        self::doSpecialEventByRoundEndEvent($model);
        //记录round的存活人数
        BayesCsgoSocketRoundService::setSurvivedPlayer($model,$model->roundOrder);
        //rating
        self::doRatingOnRoundEnd($model);
        //kast
        self::doKastOnRoundEnd($model);
        //输出round_end事件
        $roundEndFieldCommon = BayesCsgoSocketEventService::getEventCommon($model, 'round_end');
        $roundEndFieldExtra  = [
            'round_end_type' => $winnerType, //回合结束方式
            'winner_side'    => $winnerSide, //获胜方阵营
            'ct_score'       => $ctScore, // CT比分
            't_score'        => $tScore // T比分
        ];
        $roundEndField       = array_merge($roundEndFieldCommon, $roundEndFieldExtra);
        // 取出frame
        $currentFrameJson = $model->getValue([self::KEY_TAG_CURRENT, 'battle', "frame"]);
        if ($currentFrameJson) {
            $currentFrame = json_decode($currentFrameJson, true);
            foreach ($currentFrame['side'] as $k => $side) {
                if ($battleDetails[$current_t_teamUrn]['team_id'] == $side['team']['team_id']) {
                    $currentFrame['side'][$k]['score'] = $battleDetails[$current_t_teamUrn]['score'];
                } elseif ($battleDetails[$current_ct_teamUrn]['team_id'] == $side['team']['team_id']) {
                    $currentFrame['side'][$k]['score'] = $battleDetails[$current_ct_teamUrn]['score'];
                }
            }
            // 现刷新frame
            $model->needOutPutFrame = $currentFrame;
        }
        //round_end之后需要的操作
        // current:player:list 的数据需要重置
        $model->roundEndOperation($model, $data_payload);
        return [
            'type' => 'events',
            'data' => $roundEndField
        ];
    }

    /**
     * 在roundEnd的时候设置小比分
     * @param $model
     * @param $battleDetails
     * @param $current_ct_teamUrn
     * @param $current_t_teamUrn
     * @param $data_payload
     * @param $t
     * @param $ct
     * @return array|bool
     */
    public static function setScoreOnRoundEnd($model,$current_ct_teamUrn,$current_t_teamUrn,$data_payload,$t,$ct){
        //先查询是否有battle比分
        $battleDetails = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        if (!empty($battleDetails)) {
            if (isset($battleDetails[$current_t_teamUrn]) && isset($battleDetails[$current_ct_teamUrn])) {
                if ($battleDetails['round_number'] != $data_payload['roundNumber']) {
                    $battleDetails['round_number']                           = $data_payload['roundNumber'];
                    $battleDetails[$data_payload['winningTeamUrn']]['score'] = $battleDetails[$data_payload['winningTeamUrn']]['score'] + 1;
                    // 存入
                    $battleDetails = BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleDetails);
                }
            } else {
                // battle开始时间
                $battleBeginAt = $battleDetails['begin_at'];
                $battleDetails = [
                    'begin_at'          => $battleBeginAt,
                    $current_t_teamUrn  => [
                        'team_id' => $t['team_id'],
                        'score'   => $data_payload['winningTeamUrn'] == $current_t_teamUrn ? 1 : 0
                    ],
                    $current_ct_teamUrn => [
                        'team_id' => $ct['team_id'],
                        'score'   => $data_payload['winningTeamUrn'] == $current_ct_teamUrn ? 1 : 0
                    ],
                    'round_number'      => $data_payload['roundNumber'],
                ];
                // 存入
                $battleDetails = BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleDetails);
            }
        } else {
            $battleDetails = [
                $current_t_teamUrn  => [
                    'team_id' => $t['team_id'],
                    'score'   => $data_payload['winningTeamUrn'] == $current_t_teamUrn ? 1 : 0
                ],
                $current_ct_teamUrn => [
                    'team_id' => $ct['team_id'],
                    'score'   => $data_payload['winningTeamUrn'] == $current_ct_teamUrn ? 1 : 0
                ],
                'round_number'      => $data_payload['roundNumber'],
            ];
            // 存入
            $battleDetails = BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleDetails);
        }
        return $battleDetails;
    }
    /**
     * startPauseEvent
     * @param $model
     * @param $match_id
     * @param $data_payload
     */
    public static function startPauseEvent($model, $data_payload)
    {
        //当前是否为暂停存储redis
        $model->setPause(true);
        $model->isPause = true;
        $bombState = $model->getBombState($model,$model->battleId,$model->roundOrder);
        if(isset($bombState['is_bomb_planted'])){
            $is_bomb_planted = $bombState['is_bomb_planted'];
        }else{
            $is_bomb_planted = false;
        }
        if($is_bomb_planted == true){
            $time_since_plant = substr($data_payload['timeRemaining'],0,-3);
        }else{
            $time_since_plant =  null;
        }
        // 输出battle_pause事件
        $battlePauseField = BayesCsgoSocketEventService::getEventCommon($model,'battle_pause');
        return [
            'type' => 'events',
            'data' => $battlePauseField
        ];
    }
    /**
     * battle_unpause
     * @param $model
     * @param $match_id
     * @param $data_payload
     */
    public static function endPauseEvent($model, $data_payload)
    {
        //当前是否为暂停存储redis
        $model->setPause(false);
        $model->isPause = false;
        $bombState = $model->getBombState($model,$model->battleId,$model->roundOrder);
        if(isset($bombState['is_bomb_planted'])){
            $is_bomb_planted = $bombState['is_bomb_planted'];
        }else{
            $is_bomb_planted = false;
        }
        if($is_bomb_planted == true){
            $time_since_plant = substr($data_payload['timeRemaining'],0,-3);
        }else{
            $time_since_plant =  null;
        }
        // 输出battle_unpause事件
        $battlePauseField = BayesCsgoSocketEventService::getEventCommon($model,'battle_pause');
        return [
            'type' => 'events',
            'data' => $battlePauseField
        ];
    }

    // 购买物品
    public static function purchasedItemEvent($model, $data_payload)
    {
        // 更新选手金钱
        $dataBalance = [
            'money'=> $data_payload['balance'],
            'equipment_value' => $data_payload['equipmentValue']
        ];
        BayesCsgoSocketPlayerService::setPlayerInfo($model,$data_payload['playerUrn'],$dataBalance);
        // 更新选手武器
        self::ItemPickupEvent($model, $data_payload);

        return ;
    }
    // 积分版更新
    public static function updateScoreEvent($model, $data_payload)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        // 更新选手 存活状态，击杀，助攻，金钱
        $dataBalance = [
            'is_alive'=> $data_payload['alive'],
            'kills'=> $data_payload['kills'],
            'assists'=> $data_payload['assists'],
            'money'=> $data_payload['balance'],
        ];
        // 刷新当前人的信息
        BayesCsgoSocketPlayerService::setPlayerInfo($model,$data_payload['playerUrn'],$dataBalance);
        // 存入历史回合人的记录
        $playerInfo = BayesCsgoSocketPlayerService::getPlayerInfo($model,$data_payload['playerUrn']);
        $currentPlayerInfoJson = json_encode($playerInfo);
        return ;
    }
    // 掉落物品
    public static function droppedItemEvent($model, $data_payload)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $playerInfo = [
            'position'=> implode(' ',$data_payload['position']),
        ];
        // 刷新当前人的信息
        BayesCsgoSocketPlayerService::setPlayerInfo($model,$data_payload['playerUrn'],$playerInfo);
        // 所有武器
        $itemInfo = $model->hGet([self::KEY_TAG_TABLE, 'weapon', self::KEY_TAG_LIST], $data_payload['item']);
        $itemInfo = json_decode($itemInfo, true);
        $itemArray = [
            $data_payload['item']=>[
                'weapon_id'=>$itemInfo['id'],
                'name'=>$itemInfo['name'],
                'external_id'=>$itemInfo['external_id'],
                'external_name'=>$itemInfo['external_name'],
                'kind'=>$itemInfo['kind'],
                'slug'=>$itemInfo['slug'],
                'image'=>$itemInfo['image'],
            ]
        ];
        // 删除物品
        BayesCsgoSocketPlayerService::setPlayerWeapon($model,$data_payload['playerUrn'],$itemArray,true);
        return ;
    }
    // 拾取物品
    public static function ItemPickupEvent($model, $data_payload)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        //team
        $tableTeamsInfo = [];
        $tableTeams     = $model->hGetAll(['table', 'team', 'list']);
        foreach ($tableTeams as $tk => $tableTeamInfo) {
            $tableTeamsInfo[$tk] = json_decode($tableTeamInfo, true);
        }
        $playerUrn       = $data_payload['playerUrn'];
        $teamUrn         = $data_payload['teamUrn'];
        $currentTeamList = BayesCsgoSocketTeamService::getTeamList($model);
        // 所有武器
        $itemInfo = $model->hGet([self::KEY_TAG_TABLE, 'weapon', self::KEY_TAG_LIST], $data_payload['item']);
        $itemInfo = json_decode($itemInfo, true);
        // 判断c4
        if ($data_payload['item'] == "c4") {
            foreach ($currentTeamList as $key => $team) {
                $info = [
                    'team_id' => $team['team_id'],
                    'name' => $tableTeamsInfo[$team['team_id']]['name'],
                    'image' => $tableTeamsInfo[$team['team_id']]['image_200x200'],
                    'opponent_order' => $team['opponent_order'],
                    'teamUrn' => $key
                ];
                $infoJson = json_encode($info);
                $updateTeamArr = [];
                if ($key == $teamUrn) {
                    $updateTeamArr['side']='terrorist';
                    BayesCsgoSocketTeamService::setTeamInfo($model,$key,$updateTeamArr);
                    $model->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, 'team_t', 'details'], $infoJson);
                } else {
                    $updateTeamArr['side']='ct';
                    BayesCsgoSocketTeamService::setTeamInfo($model,$key,$updateTeamArr);
                    $model->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, 'team_ct', 'details'], $infoJson);
                }
            }
        }
        $itemArray = [
            $data_payload['item']=>[
                'weapon_id'=>$itemInfo['id'],
                'name'=>$itemInfo['name'] ? $itemInfo['name'] : $data_payload['item'], //转换失败就用数据源的名称
                'external_id'=>$itemInfo['external_id'],
                'external_name'=>$itemInfo['external_name'],
                'kind'=>$itemInfo['kind'],
                'slug'=>$itemInfo['slug'],
                'image'=>$itemInfo['image'],
            ]
        ];
        // 记录武器
        BayesCsgoSocketPlayerService::setPlayerWeapon($model,$playerUrn,$itemArray);
        return ;
    }


    /*
    * 在回合结束事件记录其他特殊事件
    */
    public static function doSpecialEventByRoundEndEvent($model)
    {
        $roundPlayerList  = BayesCsgoSocketRoundService::getPlayerList($model, $model->roundOrder);
        $battlePlayerList = BayesCsgoSocketPlayerService::getPlayerList($model);
        $battleTeamList   = BayesCsgoSocketTeamService::getTeamList($model);
        $roundKillTeamArr = BayesCsgoSocketRoundService::getTeamList($model, $model->roundOrder);
        foreach ($roundPlayerList as $k => $roundKillPlayer) {
            $teamUrn               = $roundKillPlayer['teamUrn'];
            $playerUrn             = $roundKillPlayer['rel_identity_id'];
            $roundTeamUpdateArr    = $roundKillTeamArr[$teamUrn];
            $battleTeamUpdateArr   = $battleTeamList[$teamUrn];
            $battlePlayerUpdateArr = $battlePlayerList[$playerUrn];
            if ($roundKillPlayer['kills'] >= 2) {
                $roundKillPlayer['multi_kills']       += 1;
                $roundTeamUpdateArr['multi_kills']    += 1;
                $battlePlayerUpdateArr['multi_kills'] += 1;
                $battleTeamUpdateArr['multi_kills']   += 1;
            }
            if ($roundKillPlayer['kills'] == 2) {
                $roundKillPlayer['two_kills']       += 1;
                $roundTeamUpdateArr['two_kills']    += 1;
                $battlePlayerUpdateArr['two_kills'] += 1;
                $battleTeamUpdateArr['two_kills']   += 1;
            }
            if ($roundKillPlayer['kills'] == 3) {
                $roundKillPlayer['three_kills']       += 1;
                $roundTeamUpdateArr['three_kills']    += 1;
                $battlePlayerUpdateArr['three_kills'] += 1;
                $battleTeamUpdateArr['three_kills']   += 1;
            }
            if ($roundKillPlayer['kills'] == 4) {
                $roundKillPlayer['four_kills']       += 1;
                $roundTeamUpdateArr['four_kills']    += 1;
                $battlePlayerUpdateArr['four_kills'] += 1;
                $battleTeamUpdateArr['four_kills']   += 1;
            }
            if ($roundKillPlayer['kills'] == 5) {
                $roundKillPlayer['five_kills']       += 1;
                $roundTeamUpdateArr['five_kills']    += 1;
                $battlePlayerUpdateArr['five_kills'] += 1;
                $battleTeamUpdateArr['five_kills']   += 1;
            }
            BayesCsgoSocketRoundService::setPlayerInfo($model, $playerUrn, $roundKillPlayer);
            BayesCsgoSocketPlayerService::setPlayerInfo($model, $playerUrn, $battlePlayerUpdateArr);
        }
        if ($teamUrn&&$roundTeamUpdateArr){
            BayesCsgoSocketRoundService::setTeamInfo($model, $teamUrn, $roundTeamUpdateArr);
        }
        if ($teamUrn&&$battleTeamUpdateArr){
            BayesCsgoSocketTeamService::setTeamInfo($model, $teamUrn, $battleTeamUpdateArr);
        }
        return true;
    }


    /*
     * 通过击杀事件记录其他特殊事件
     */
    public static function doSpecialEventByKillEvent($model, $killerPlayerInfo, $victimPlayerInfo, $killerTeam, $victimTeam, $data_payload)
    {
        if (!$model){
            $model= new BayesCsgoSocket();
        }
        $killPlayerUpdate   = [];
        $victimPlayerUpdate = [];
        $victimPlayerNeedUpdate = [];
        $roundKillPlayer    = BayesCsgoSocketRoundService::getPlayerInfo($model, $killerPlayerInfo['rel_identity_id']);
        $roundKillTeamArr   = BayesCsgoSocketRoundService::getTeamInfo($model, $killerTeam['rel_identity_id']);
        if (empty($roundKillTeamArr['first_kills'])) {
            $killPlayerUpdate['first_kills'] = 1;
        }
        $roundVictimTeamArr = BayesCsgoSocketRoundService::getTeamInfo($model, $victimTeam['rel_identity_id']);
        //更新死亡玩家的队伍存活人数
        BayesCsgoSocketRoundService::setSurvivedPlayer($model, $model->roundOrder);
        //update 1vn
        self::do1vnRecordOnKillEvent($model, $killerPlayerInfo['rel_identity_id']);
        if (empty($roundVictimTeamArr['first_deaths'])) {
            $victimPlayerUpdate['first_deaths'] = 1;
        }
        if ($data_payload['headshot'] && empty($roundKillPlayer['headshot_kills'])) {
            $killPlayerUpdate['is_first_kills_headshot'] = 1;
        }
        //weapon kill
        $weapon = $data_payload['weapon'];
        if ($weapon == 'knife') {
            $killPlayerUpdate['knife_kills'] = 1;
        } elseif ($weapon == 'awp') {
            $killPlayerUpdate['awp_kills'] = 1;
        } elseif ($weapon == 'taser') {
            $killPlayerUpdate['taser_kills'] = 1;
        } elseif ($weapon == 'mag7' || $weapon == 'sawedoff' || $weapon == 'xm1014') {
            $killPlayerUpdate['shotgun_kills'] = 1;
        }

        //kill
        if (!empty($killPlayerUpdate)){
            BayesCsgoSocketPlayerService::setPlayerByUrn($model, $killerPlayerInfo['rel_identity_id'], $killPlayerUpdate,'incr');
        }

        //victim
        if (!empty($victimPlayerUpdate)){
            BayesCsgoSocketPlayerService::setPlayerByUrn($model, $victimPlayerInfo['rel_identity_id'], $victimPlayerUpdate,'incr');
        }
        $victimPlayerNeedUpdate['kills'] =1;
        BayesCsgoSocketRoundService::setPlayerInfo($model, $victimPlayerInfo['rel_identity_id'], $victimPlayerNeedUpdate,'incr');



        //trade
        $updatePlayerLogArr = [];
        $updatePlayerLogArr['killerUrn'] = $killerPlayerInfo['rel_identity_id'];
        $updatePlayerLogArr['victimUrn'] = $victimPlayerInfo['rel_identity_id'];
        $updatePlayerLogArr['log_time'] = $model->info['socket_time'];
        $killEventsKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $model->battleId, self::KEY_TAG_ROUND, $model->roundOrder, self::KEY_TAG_KILL_EVENTS];
        $model->lPush($killEventsKey, json_encode($updatePlayerLogArr,320));
        $listKillEvents = $model->lRange($killEventsKey,0,-1);
        if (!empty($listKillEvents)){
            foreach ($listKillEvents as $k=>$v){
                $arrV = json_decode($v,true);
                if ((strtotime($model->info['socket_time']) - strtotime($arrV['log_time']) < 4) && $arrV['killer_steam_id'] == $victimPlayerInfo['rel_identity_id']) {
                    $update = [];
                    $update['trade'] = 1;
                    BayesCsgoSocketRoundService::setPlayerInfo($model,$victimPlayerInfo['rel_identity_id'],$update);
                }
            }
        }
        return true;
    }


    public static function do1vnRecordOnKillEvent($model,$killPlayerUrn){
        $teamList = BayesCsgoSocketRoundService::getTeamList($model,$model->roundOrder);
        foreach ($teamList as $k=>$v){
            if ($v['survived_players'] == 1) {
                $updateArr['1vn_player_count'] = 1;
                $updateArr['1vn_player_urn'] = $killPlayerUrn;
                BayesCsgoSocketRoundService::setTeamInfo($model,$k,$updateArr);
                $team1vnUrn = $k;
            }
        }
        if ($updateArr['1vn_player_urn']){
            foreach ($teamList as $k=>$v){
                if ($k != $team1vnUrn) {
                    $updateArotherArr['1vn_player_count'] = $v['survived_players'];
                    BayesCsgoSocketRoundService::setTeamInfo($model, $k, $updateArotherArr);
                }
            }
        }
    }

    /**
     * 击杀事件
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function KillEvent($model,$data_payload)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $bombState = $model->getBombState($model,$model->battleId,$model->roundOrder);
        if(isset($bombState['is_bomb_planted'])){
            $is_bomb_planted = $bombState['is_bomb_planted'];
        }else{
            $is_bomb_planted = false;
        }
        if($is_bomb_planted == true){
            $time_since_plant = substr($data_payload['timeRemaining'],0,-3);
        }else{
            $time_since_plant =  null;
        }
        $killerPlayerInfo = BayesCsgoSocketPlayerService::getPlayerInfo($model, $data_payload['killerUrn']);
        $killerTeam       = BayesCsgoSocketTeamService::getTeamInfo($model, $data_payload['killerTeamUrn']);
        $victimPlayerInfo = BayesCsgoSocketPlayerService::getPlayerInfo($model, $data_payload['victimUrn']);
        $victimTeam       = BayesCsgoSocketTeamService::getTeamInfo($model, $data_payload['victimTeamUrn']);
        $weaponJson       = $model->hGet([self::KEY_TAG_TABLE, 'weapon', self::KEY_TAG_LIST], $data_payload['weapon']);
        if($weaponJson){
            $weaponInfo = json_decode($weaponJson,true);
            $weaponKind = Common::getCsgoWeaponKindByKind($weaponInfo['kind']);
        }else{
            $weaponInfo = null;
        }
        //处理特殊事件
        self::doSpecialEventByKillEvent($model,$killerPlayerInfo,$victimPlayerInfo,$killerTeam,$victimTeam,$data_payload);
        $eventCommon = BayesCsgoSocketEventService::getEventCommon($model,'player_kill');
        $killFieldCommon = $eventCommon;

        //输出击杀事件
        $killFieldExtra = [
            'killer' => [
                'player_id' => $killerPlayerInfo['player_id'],
                'nick_name' => $killerPlayerInfo['nick_name'],
                'steam_id'  => $killerPlayerInfo['steam_id'],
                'side'      => $killerTeam['side'],
                'position'  => implode(',', $data_payload['position']),
            ],
            'victim' => [
                'player_id' => $victimPlayerInfo['player_id'],
                'nick_name' => $victimPlayerInfo['nick_name'],
                'steam_id'  => $victimPlayerInfo['steam_id'],
                'side'      => $victimTeam['side'],
                'position'  => implode(',', $data_payload['position']),
            ],
            'weapon' => [
                'weapon_id'     => (int)$weaponInfo['id'],
                'name'          => $weaponInfo['name'],
                'external_id'   => $weaponInfo['external_id'],
                'external_name' => $weaponInfo['external_name'],
                'kind'          => $weaponKind,
                'slug'          => $weaponInfo['slug'],
                'image'         => $weaponInfo['image'],
                'is_headshot'   => $data_payload['headshot'],
            ],
        ];
        $killField = array_merge($killFieldCommon,$killFieldExtra);
        return [
            'type' => 'events',
            'data' => $killField
        ];
    }
     public static function threwItemEvent($model, $data_payload)
     {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
         $itemArray = [
             $data_payload['item']=>[]
         ];
        BayesCsgoSocketPlayerService::setPlayerWeapon($model,$data_payload['playerUrn'],$itemArray,$isDel=true);
        return true;
     }

    /**
     * Rating = (KillRating + 0.7*SurvivalRating + RoundsWithMultipleKillsRating)/2.7
     * KillRating = Kills/Rounds/AverageKPR (平局每局击杀)
     * SurvivalRating = (Rounds-Deaths)/Rounds/AverageSPR (平局每局存活百分比)
     * RoundsWithMultipleKillsRating = (1K + 4*2K + 9*3K + 16*4K +25*5K)/Rounds/AverageRMK
     * AverageKPR = 0.679 (average kills per round)
     * AverageSPR = 0.317 (average survived rounds per round)
     * AverageRMK = 1.277 (average value calculated from rounds with multiple kills:
     * (1K + 4*2K + 9*3K + 16*4K + 25*5K)/Rounds)
     */
    public static function doRatingOnRoundEnd($model)
    {
        $playList   = BayesCsgoSocketPlayerService::getPlayerList($model);
        $roundCount = $model->roundOrder;
        if ($roundCount) {
            $teamRating = [];
            foreach ($playList as $k => $v) {
                $killRating                    = $v['kills'] / $roundCount / 0.679;
                $survivalRating                = ($roundCount - $v['deaths']) / $roundCount / 0.317;
                $roundsWithMultipleKillsRating = ($v['one_kills'] + 4 * $v['two_kills'] + 9 * $v['three_kills'] + 16 * $v['four_kills'] + 25 * $v['five_kills']) / $roundCount / 1.277;
                $rating                        = ($killRating + 0.7 * $survivalRating + $roundsWithMultipleKillsRating) / 2.7;
                BayesCsgoSocketPlayerService::setPlayerInfo($model, $v['rel_identity_id'], ['rating' => round($rating, 2)]);
                $teamRating[$v['rel_team_id']]['rating']      += $rating;
                $teamRating[$v['rel_team_id']]['playerCount'] += 1;
            }
            if (!empty($teamRating)) {
                foreach ($teamRating as $k => $v) {
                    if (!empty($v['playerCount'])) {
                        $new_res_team['rating'] = round($v['rating'] / $v['playerCount'], 2);
                        BayesCsgoSocketTeamService::setTeamInfo($model, $k, $new_res_team);
                    }
                }
            }
        }
    }

    public static function dealtDamage($model, $data_payload)
     {
         if (!$model){
             $model = new BayesCsgoSocket();
         }
         //kill
         $updateKillArr                    = [];
         $updateKillArr['position']        = explode(' ',$data_payload['position']);
         $updateKillArr['damage']          = $data_payload['health'];
         $updateKillArrIncr['real_damage'] = $data_payload['healthDeducted'];
         BayesCsgoSocketPlayerService::setPlayerByUrn($model, $data_payload['killerUrn'], $updateKillArr);
         BayesCsgoSocketPlayerService::setPlayerByUrn($model, $data_payload['killerUrn'], $updateKillArrIncr, 'incr');
         //victim
         $updateVictimArr['position'] = explode(' ',$data_payload['targetPosition']);
         $updateVictimArr['health']   = $data_payload['healthRemaining'];
         BayesCsgoSocketPlayerService::setPlayerByUrn($model, $data_payload['victimUrn'], $updateVictimArr);
         $roundOrder = $model->roundOrder;
         //adr(battle)
         if ($roundOrder != 0) {
             $killPlayerInfo        = BayesCsgoSocketPlayerService::getPlayerInfo($model, $data_payload['killerUrn']);
             $killPlayerUpdateInfo['adr'] = round(@$killPlayerInfo['real_damage'] / $roundOrder, 2);
             BayesCsgoSocketPlayerService::setPlayerInfo($model, $data_payload['killerUrn'], $killPlayerUpdateInfo);
             $playerList = BayesCsgoSocketPlayerService::getPlayerListByTeamUrn($model,$model->battleId,$data_payload['killerTeamUrn']);
             $countPlayer = count($playerList);
             if (!empty($playerList)&&$countPlayer>0){
                 $teamArr=[];
                 $teamAdrValue=0;
                 foreach ($playerList as $k=>$v){
                     $teamAdrValue +=$v['adr'];
                 }
                 $teamArr['adr'] = round($teamAdrValue/$countPlayer,2);
                 if (!empty($teamArr)){
                     BayesCsgoSocketTeamService::setTeamInfo($model,$data_payload['killerTeamUrn'],$teamArr);
                 }
             }
         }
         //更新击中部位的特殊事件
         $body = $data_payload['hitgroup'];
         if ($body){
             if ($body == 'generic') {
                 //hit_generic
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_generic'] = 1;
             }
             if ($body == 'head') {
                 //hit_head
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_head'] = 1;
             }
             if ($body == 'chest') {
                 //hit_chest
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_chest'] = 1;
             }
             if ($body == 'stomach') {
                 //hit_stomach
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_stomach'] = 1;
             }
             if ($body == 'left arm') {
                 //hit_left_arm
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_left_arm'] = 1;
             }
             if ($body == 'right arm') {
                 //hit_right_arm
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_right_arm'] = 1;
             }
             if ($body == 'left leg') {
                 //hit_left_leg
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_left_leg'] = 1;
             }
             if ($body == 'right leg') {
                 //hit_right_leg
                 $updateArrStatistics   = [];
                 $updateArrStatistics['hit_right_leg'] = 1;
             }
             if (!empty($updateArrStatistics)){
                 BayesCsgoSocketPlayerService::setPlayerByUrn($model,$data_payload['killerUrn'], $updateArrStatistics, 'incr');
             }
         }
         return false;

     }

    /**
     * @param $model
     * @param $data_payload
     * 自杀事件
     */
     public static function playerDied($model,$data_payload)
     {
         if (!$model) {
             $model = new BayesCsgoSocket();
         }
         $bombState = $model->getBombState($model,$model->battleId,$model->roundOrder);
         if(isset($bombState['is_bomb_planted'])){
             $is_bomb_planted = $bombState['is_bomb_planted'];
         }else{
             $is_bomb_planted = false;
         }
         if($is_bomb_planted == true){
             $time_since_plant = substr($data_payload['timeRemaining'],0,-3);
         }else{
             $time_since_plant =  null;
         }

         $killerPlayerInfo = BayesCsgoSocketPlayerService::getPlayerInfo($model,$data_payload['playerUrn']);
         $killerTeam = BayesCsgoSocketTeamService::getTorCtByTeamUrn($model,$data_payload['teamUrn']);
         // 对局内时间戳
         $battle_timestamp = $model->getCurrentBattleTimestamp($model->battle_timestamp,$model->info['socket_time']);
         //输出杀人事件
         $killField = [
             'match_id' => $model->matchId,
             'battle_id' => $model->battleId,
             'battle_order' => $model->battleOrder,
             'battle_timestamp' => $battle_timestamp, //
             'event_id' => $model->info['id'], //事件id
             'event_type' => BayesCsgoSocket::CSGO_PLAYER_KILL, //事件类型
             'round_ordinal' => $data_payload['currentRoundNumber'], //回合序号
             'in_round_timestamp' => substr($data_payload['gameTime'],0,-3), //回合内时间戳
             'round_time' => substr($data_payload['timeRemaining'],0,-3), //回合内时间
             'is_bomb_planted'=> $is_bomb_planted, //炸弹是否已安放
             'time_since_plant'=> $time_since_plant, //安放炸弹后时间
             'killer'=>[
                 'player_id'=> $killerPlayerInfo['player_id'],
                 'nick_name'=> $killerPlayerInfo['nick_name'],
                 'steam_id'=> $killerPlayerInfo['steam_id'],
                 'side'=> $killerTeam,
                 'position'=> implode(',',$data_payload['position']),
             ],
             'victim'=>[
                 'player_id'=> $killerPlayerInfo['player_id'],
                 'nick_name'=> $killerPlayerInfo['nick_name'],
                 'steam_id'=> $killerPlayerInfo['steam_id'],
                 'side'=> $killerTeam,
                 'position'=> implode(',',$data_payload['position']),
             ],
         ];
     }

    /**
     * battle_start
     * @param $model
     * @param $match_id
     * @param $data_payload
     * @return array[]
     */
    public static function startMapEvent($model, $match_id, $data_payload,$sourceUpdatedAt)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        if ($data_payload) {
            $startTime = date('Y-m-d H:i:s', strtotime($sourceUpdatedAt));
            if ($data_payload['matchCurrent'] == 1) {
                //更新match开始时间
                $matchData['begin_at'] = $startTime;
                $matchData['status']   = 2;
                //更新match开始时间
                BayesCsgoSocketMatchService::setMatchDetails($model, $matchData);
            }

            //map eventCommon
            $map                    = BayesCsgoSocketBattleService::getMap($model, $data_payload['mapName']);
            //记录battle开始时间和地图信息
            $battleDetailsUpdate = [
                'begin_at' => $startTime,
                'map' => $map['map_id'],
            ];
            $model->setCurrentRoundOrder($model,1);
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$battleDetailsUpdate);
            $eventCommon = BayesCsgoSocketEventService::getEventCommon($model,BayesCsgoSocket::CSGO_BATTLE_START);
            //battle_upcoming
            $battleUpcomingFieldConfig = $eventCommon;
            $battleUpcomingFieldConfig['in_round_timestamp'] = 0;
            $battleUpcomingFieldConfig['event_type'] = BayesCsgoSocket::CSGO_BATTLE_UPCOMING;
            //battle_start
            $battleStartFieldConfig                       = $eventCommon;
            $battleStartFieldConfig['in_round_timestamp'] = 0;
            $battleStartFieldConfig['map']                = $map;
            $battleStartFieldConfig['starting_ct']        = null;
            $battleStartFieldConfig['starting_t']         = null;
            $customBattleStartKey = ['current','custom','battleStart'];
            $customUpComingKey = ['current','custom','upComing'];
            $model->setValue($customBattleStartKey,json_encode($battleStartFieldConfig,320));
            $model->setValue($customUpComingKey,json_encode($battleUpcomingFieldConfig,320));
            $outData1 = [
                'type'=>'events',
                'data'=> $battleUpcomingFieldConfig
            ];
            $outData2 = [
                'type'=>'events',
                'data'=> $battleStartFieldConfig
            ];
            return [$outData1,$outData2];
        }
    }



    public static function getNeedOutPutFrame($model, $eventType)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $key           = $model->currentPre . $model->matchId . ':history:frames';
        $lastFrameJson = $model->redis->lIndex($key, 1);
        $lastFrameArr  = json_decode($lastFrameJson, true);
        if ($eventType == 'battle_end') {
            $lastFrameArr['is_battle_finished'] = true;
        } elseif ($eventType == 'match_end') {
            $lastFrameArr['is_battle_finished'] = true;
            $lastFrameArr['is_match_finished']  = true;
        }
        $matchDetails = BayesCsgoSocketMatchService::getMatchDetails($model);
        if ($lastFrameArr['match_scores'][0]['opponent_order'] == 1) {
            $lastFrameArr['match_scores'][0]['score'] = $matchDetails['team_1_score'];
            $lastFrameArr['match_scores'][1]['score'] = $matchDetails['team_2_score'];
        } else {
            $lastFrameArr['match_scores'][1]['score'] = $matchDetails['team_1_score'];
            $lastFrameArr['match_scores'][0]['score'] = $matchDetails['team_2_score'];
        }
        $battleDetails                 = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        $lastFrameArr['match_winner']  = $matchDetails['winner_details'] ? $matchDetails['winner_details'] : null;
        $lastFrameArr['battle_winner'] = $battleDetails['winner_details'] ? $battleDetails['winner_details'] : null;
        $model->needOutPutFrame        = $lastFrameArr;

    }

    /**
     * @param $model
     * @param $battleId
     * @return array
     */
    public static function getBattleEventByBattleId($model, $battleId)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $key  = ['history', 'battle', $battleId, 'events'];
        $list = $model->lRange($key, 0, -1);
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $newList[$k] = json_decode($v, true);
            }
            return $newList;
        } else {
            return $list;
        }
    }
    /**
     * @param $model
     * @param $battleId
     * @return array
     */
    public static function getBattleEventByRoundOrder($model, $battleId,$roundOrder)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $key  = ['history', 'battle', $battleId, 'round',$roundOrder,'events'];
        $list = $model->lRange($key, 0, -1);
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $newList[$k] = json_decode($v, true);
            }
            return $newList;
        } else {
            return $list;
        }
    }


    public static function getEventCommon($model,$realEventType='')
    {
        if (!$model){
            $model  = new self();
        }
        $commonArr                       = [];
        $commonArr['match_id']           = (int)$model->matchId;
        $commonArr['battle_id']          = (int)$model->battleId;
        $commonArr['battle_order']       = (int)$model->battleOrder;
        $commonArr['battle_timestamp']   = (int)$model->battle_duration;
        $commonArr['event_id']           = (int)$model->info['socket_id'];
        $commonArr['event_type']         = $realEventType;
        $commonArr['round_ordinal']      = (int)$model->roundOrder;
        $commonArr['in_round_timestamp'] = (int)$model->in_round_timestamp;
        $commonArr['round_time']         = (int)$model->round_time;
        $commonArr['is_bomb_planted']    = $model->is_bomb_planted;
        $commonArr['time_since_plant']   = (int)$model->time_since_plant;
        return $commonArr;
    }

    /**
     * 在roundEnd的时候处理特殊比分
     * @param $model
     * @param $ctScore
     * @param $tScore
     * @param $log_win_camp
     * @param $win_team
     * @param $terroristTeam
     * @param $ctTeam
     */
    public static function doScoreOnRoundEnd($model, $ctScore, $tScore, $log_win_camp, $win_team, $terroristTeam, $ctTeam)
    {
        $baseBattleArr = [];
        $total         = $ctScore + $tScore;
        if ($total == 1) {
            $baseBattleArr['win_round_1_side']      = $log_win_camp;
            $baseBattleArr['win_round_1_team']      = $win_team['opponent_order'];
            $baseBattleArr['win_round_1_team_name'] = $win_team['name'];
            $baseBattleArr['win_round_1_detail']    = "";
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $baseBattleArr);
        }
        if (($ctScore == 5 && $tScore < 5) || ($tScore == 5 && $ctScore < 5)) {
            if ($ctScore == 5) {
                $baseBattleArr['first_to_5_rounds_wins_side']      = 'ct';
                $baseBattleArr['first_to_5_rounds_wins_team']      = $ctTeam['opponent_order'];
                $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                $baseBattleArr['first_to_5_rounds_wins_team_name'] = $ctTeam['name'];
                BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $baseBattleArr);
            } else {
                $baseBattleArr['first_to_5_rounds_wins_side']      = 'terrorist';
                $baseBattleArr['first_to_5_rounds_wins_team']      = $terroristTeam['opponent_order'];
                $baseBattleArr['first_to_5_rounds_wins_detail']    = '';
                $baseBattleArr['first_to_5_rounds_wins_team_name'] = $terroristTeam['name'];
            }
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $baseBattleArr);
        }
        if ($total == 16) {
            $baseBattleArr                           = [];
            $baseBattleArr['win_round_16_side']      = $log_win_camp;
            $baseBattleArr['win_round_16_team']      = $win_team['opponent_order'];
            $baseBattleArr['win_round_16_team_name'] = $win_team['name'];
            $baseBattleArr['win_round_16_detail']    = "";
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $baseBattleArr);
        }

        //set start_ct 1st_half_score,2nd_half_score,ot_score
        if ($total <= 15) {
            $baseBattleCtArr['1st_half_score']        = (int)$ctScore;
            $baseBattleTerroristArr['1st_half_score'] = (int)$tScore;
            BayesCsgoSocketTeamService::setTeamInfo($model, $ctTeam['rel_identity_id'], $baseBattleCtArr);
            BayesCsgoSocketTeamService::setTeamInfo($model, $terroristTeam['rel_identity_id'], $baseBattleTerroristArr);
        } elseif ($total > 15 && $total <= 30) {
            $baseBattleTerroristArr['2nd_half_score'] = $tScore - $terroristTeam['1st_half_score'];
            $baseBattleCtArr['2nd_half_score']        = $ctScore - $ctTeam['1st_half_score'];
            $baseBattleTerroristArr['2nd_half_score'] = $baseBattleTerroristArr['2nd_half_score'] ? $baseBattleTerroristArr['2nd_half_score'] : 0;
            $baseBattleCtArr['2nd_half_score']        = $baseBattleCtArr['2nd_half_score'] ? $baseBattleCtArr['2nd_half_score'] : 0;
            BayesCsgoSocketTeamService::setTeamInfo($model, $terroristTeam['rel_identity_id'], $baseBattleTerroristArr);
            BayesCsgoSocketTeamService::setTeamInfo($model, $ctTeam['rel_identity_id'], $baseBattleCtArr);
        } elseif ($total > 30) {
            $baseBattleTerroristArr['ot_score'] = $tScore - 15;
            $baseBattleCtArr['ot_score']        = $ctScore - 15;
            $baseBattleTerroristArr['ot_score'] = $baseBattleTerroristArr['ot_score'] ? $baseBattleTerroristArr['ot_score'] : 0;
            $baseBattleCtArr['ot_score']        = $baseBattleCtArr['ot_score'] ? $baseBattleCtArr['ot_score'] : 0;
            BayesCsgoSocketTeamService::setTeamInfo($model, $terroristTeam['rel_identity_id'], $baseBattleTerroristArr);
            BayesCsgoSocketTeamService::setTeamInfo($model, $ctTeam['rel_identity_id'], $baseBattleCtArr);
        }
    }


}
