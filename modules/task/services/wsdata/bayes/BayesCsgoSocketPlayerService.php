<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\wsdata\WebsocketCommon;
use Throwable;
use yii\base\Exception;

class BayesCsgoSocketPlayerService extends WebsocketCommon implements SocketPlayerInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    public static function getPlayerList($model, $battleId = null)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        if (empty($battleId)) {
            $battleId = $model->battleId;
        }
        $key  = ['history', 'battle', $battleId, 'player', 'list'];
        $list = $model->hGetAll($key);
        if (!empty($list)) {
            $newList = [];
            foreach ($list as $k => $v) {
                $newList[$k] = json_decode($v, true);
            }
            return $newList;
        } else {
            return null;
        }
    }

    /**
     * @param $model
     * @param null $battleId
     * @param $teamUrn
     * @return array|null
     */
    public static function getPlayerListByTeamUrn($model, $battleId = null, $teamUrn = '')
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        if (empty($battleId)) {
            $battleId = $model->battleId;
        }
        if (empty($teamUrn)) {
            return null;
        }
        $key  = ['history', 'battle', $battleId, 'player', 'list'];
        $list = $model->hGetAll($key);
        if (!empty($list)) {
            $newList = [];
            foreach ($list as $k => $v) {
                $vArr = json_decode($v, true);;
                if ($vArr['rel_team_id']==$teamUrn){
                    $newList[$k] = $vArr;
                }
            }
            return $newList;
        } else {
            return null;
        }
    }

    /**
     * @param $model
     * @param $liveDataPlayerUrn
     * @param $playerDetails
     * @param $specialOperation
     * @return array
     */
    public static function setPlayerInfo($model, $liveDataPlayerUrn, $playerDetails, $specialOperation='')
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $oldData = BayesCsgoSocketPlayerService::getPlayerInfo($model, $liveDataPlayerUrn);
        if ($oldData && is_array($oldData)) {
            if ($specialOperation == 'incr') {
                foreach ($playerDetails as $k => $v) {
                    $oldData[$k] += $v;
                }
            } else {
                $oldData = array_merge($oldData, $playerDetails);
            }
        } else {
            $oldData = $playerDetails;
        }
        $key  = ['history', 'battle', $model->battleId, 'player', 'list'];
        $json = json_encode($oldData, 320);
        $model->hSet($key, $liveDataPlayerUrn, $json);
        return $oldData;
    }

    public static function getPlayerInfo($model,$liveDataPlayerUrn){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['history', 'battle', $model->battleId, 'player', 'list'];
        $playerJson =   $model->hGet($key,$liveDataPlayerUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }

    /**
     * @param $model
     * @return mixed
     * 获取battle下的选手列表，没有的话从当前选手中获取
     */
    public static function getBattlePlayerList($model)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key  =[self::KEY_TAG_HISTORY,$model->battleId,'player','list'];
        $playerJson =   $model->hGetAll($key);
        if (empty($playerJson)){ //把当前的选手复制到battle下
            //redis存储该battle下的选手
            $battlePlayers = $model->hGetAll([self::KEY_TAG_CURRENT,'player','list']);
            if($battlePlayers){
                foreach ($battlePlayers as $battlePlayerKey => $battlePlayerValue){
                    $model->hSet([self::KEY_TAG_HISTORY,$model->battleId,'player','list'],$battlePlayerKey,$battlePlayerValue);
                }
            }
            $playerJson =   $model->hGetAll($key);
            return json_decode($playerJson,true);
        }else{
            return json_decode($playerJson,true);
        }
    }

    /**
     * @param $model
     * @param $playerUrn
     * @param $updatePlayerArr
     * @param $specialOperation
     * @param string $isUpdateTeam
     */
    public static function setPlayerByUrn($model, $playerUrn, $updatePlayerArr, $specialOperation = '', $isUpdateTeam = 'true')
    {
        BayesCsgoSocketRoundService::setPlayerInfo($model, $playerUrn, $updatePlayerArr, $specialOperation);
        $battlePlayer = BayesCsgoSocketPlayerService::setPlayerInfo($model, $playerUrn, $updatePlayerArr, $specialOperation);
        $teamUrn = $battlePlayer['rel_team_id'];
        if ($isUpdateTeam){
            BayesCsgoSocketRoundService::setTeamInfo($model, $teamUrn, $updatePlayerArr, $specialOperation);
            BayesCsgoSocketTeamService::setTeamInfo($model, $teamUrn, $updatePlayerArr, $specialOperation);
        }
        return true;
    }

    public static function setPlayerWeapon($model,$liveDataPlayerUrn,$itemArray,$isDel=false) //武器暂时不存
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $oldData=BayesCsgoSocketPlayerService::getPlayerWeapon($model,$liveDataPlayerUrn);
        if ($oldData) {
            foreach ($oldData as $k => $v) {
                if($isDel){
                    if(isset($itemArray[$k])){
                        unset($oldData[$k]);
                    }
                } else {
                    if (isset($itemArray[$k])) {
                        continue;
                    } else {
                        foreach ($itemArray as $itemKey => $val) {
                            $oldData[$itemKey] = $val;
                        }
                    }
                }
            }
            $updateArr = $oldData;
        }else{
            $updateArr = $itemArray;
        }
        $key  = [self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS];
        $json = json_encode($updateArr,320);
        $model->hSet($key,$liveDataPlayerUrn,$json);
        return $updateArr;
    }
    public static function getPlayerWeapon($model,$liveDataPlayerUrn)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key  = [self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS];
        $weaponJson =   $model->hGet($key,$liveDataPlayerUrn);
        if (empty($weaponJson)){
            return false;
        }else{
            $weaponArr = json_decode($weaponJson,true);
            return $weaponArr;
        }
    }

    public static function getPlayerWeaponList($model,$liveDataPlayerUrn)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key  = [self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS];
        $weaponJson =   $model->hGetAll($key);
        if (empty($weaponJson)){
            return false;
        }else{
            return json_decode($weaponJson,true);
        }
    }
    public function getCurrentPlayerWeaponList($model)
    {
        return $model->hGetAll([self::KEY_TAG_CURRENT,self::KEY_TAG_PLAYER,self::KEY_TAG_WEAPONS]);
    }
    /**
     * clearPlayerWeapons
     */
    public function clearPlayerWeapons()
    {
        $weaponKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $weapons   = $this->hGetAll($weaponKey);
        foreach ($weapons as $k => $v) {
            $this->hSet($weaponKey, $k, json_encode([],320));
        }
        $grenadeKey = [self::KEY_TAG_CURRENT, self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];
        $grenades   = $this->hGetAll($grenadeKey);
        foreach ($grenades as $k => $v) {
            $this->hSet($grenadeKey, $k, json_encode([],320));
        }
        $weaponRoundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_WEAPONS];
        $weaponsRound   = $this->hGetAll($weaponRoundKey);
        foreach ($weaponsRound as $k => $v) {
            $this->hSet($weaponRoundKey, $k, json_encode([],320));
        }
        $grenadeRoundKey = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $this->getBattleId(), self::KEY_TAG_ROUND, $this->getRoundOrdinal(),
            self::KEY_TAG_PLAYER, self::KEY_TAG_GRENADES];;
        $grenadeRound = $this->hGetAll($grenadeRoundKey);
        foreach ($grenadeRound as $k => $v) {
            $this->hSet($grenadeRoundKey, $k, json_encode([],320));
        }
        return true;
    }
}
