<?php
namespace app\modules\task\services\wsdata\bayes;


use app\modules\task\services\wsdata\WebsocketCommon;

class BayesCsgoSocketRoundService extends WebsocketCommon implements SocketRoundInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;

    /**
     * 设置round详情
     * @param $model
     * @param $roundOrder
     * @param $updateArr
     * @return string|void
     */
    public static function setRoundDetailsByRoundOrder($model, $roundOrder, $updateArr)
    {
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $details = self::getRoundDetailsByRoundOrder($model,$roundOrder);
        if (is_array($details)&&is_array($updateArr)){
            $updateArr = array_merge($details,$updateArr);
        }
        $updateArr['is_use'] = true;
        $roundsHistoryJson = json_encode($updateArr, 320);
        //设置round历史
        $key = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $model->battleId, "round", "list"];
        return $model->hSet($key, $roundOrder, $roundsHistoryJson);
    }

    /**
     * 获取round详情
     * @param $model
     * @param $roundOrder
     * @return mixed|null
     */
    public static function getRoundDetailsByRoundOrder($model,$roundOrder){
        if (empty($model)) {
            $model = new BayesCsgoSocket();
        }
        $key = [self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, $model->battleId, "round", "list"];
        $details = $model->hGet($key,$roundOrder);
        if (!empty($details)){
            return json_decode($details,true);
        }else{
            return null;
        }
    }

    /**
     * @param $model
     * @param $liveDataPlayerUrn
     * @param $playerDetails
     * @param string $specialOperation
     * @return array
     */
    public static function setPlayerInfo($model, $liveDataPlayerUrn, $playerDetails, $specialOperation='')
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $oldData = self::getPlayerInfo($model, $liveDataPlayerUrn);
        if ($oldData && is_array($oldData)) {
            if ($specialOperation == 'incr') {
                foreach ($playerDetails as $k => $v) {
                    $oldData[$k] += $v;
                }
            } else {
                $oldData = array_merge($oldData, $playerDetails);
            }
        } else {
            $oldData = $playerDetails;
        }
        $key  = ['history', 'battle', $model->battleId, 'round', $model->roundOrder, 'player'];
        $json = json_encode($oldData, 320);
        $model->hSet($key, $liveDataPlayerUrn, $json);
        return $oldData;
    }

    public static function getPlayerInfo($model,$liveDataPlayerUrn){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['history', 'battle', $model->battleId, 'round',$model->roundOrder, 'player'];
        $playerJson =   $model->hGet($key,$liveDataPlayerUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }


    /**
     * @param $model
     * @param $teamUrn
     * @param $upDateTeamArr
     * @param $specialOperation
     * @return string|void
     * 更新current team list的数据
     */
    public static function setTeamInfo($model, $teamUrn, $upDateTeamArr, $specialOperation='')
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $oldData = self::getTeamInfo($model, $teamUrn);
        if ($oldData && is_array($oldData)) {
            if ($specialOperation == 'incr') {
                foreach ($upDateTeamArr as $k => $v) {
                    $oldData[$k] += $v;
                }
            } else {
                $oldData = array_merge($oldData, $upDateTeamArr);
            }
        } else {
            $oldData = $upDateTeamArr;
        }
        $json = json_encode($oldData, true);
        $key  = ['history', 'battle', $model->battleId, 'round', $model->roundOrder, 'team'];
        return $model->hSet($key, $teamUrn, $json);
    }

    /**
     * @param $model
     * @param $liveDataTeamUrn
     * @return bool|mixed
     */
    public static function getTeamInfo($model,$liveDataTeamUrn){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['history', 'battle', $model->battleId, 'round',$model->roundOrder, 'team'];
        $playerJson =   $model->hGet($key,$liveDataTeamUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }

    /**
     * @param $model
     * @param $roundOrder
     * @return bool|mixed
     */
    public static function getTeamList($model, $roundOrder)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $roundOrderKey = $roundOrder ? $roundOrder : $model->roundOrder;
        $key           = ['history', 'battle', $model->battleId, 'round', $roundOrderKey, 'team'];
        $teamList      = $model->hGetAll($key);
        if (empty($teamList)) {
            return false;
        } else {
            $newTeamList = [];
            foreach ($teamList as $k => $v) {
                $newTeamList[$k] = json_decode($v, true);
            }
            return $newTeamList;
        }
    }

    public static function setSurvivedPlayer($model, $roundOrder)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $roundPlayerList = BayesCsgoSocketRoundService::getPlayerList($model,$roundOrder);
        if (!empty($roundPlayerList)){
            $countSurvivedPlayerArr = [];
            foreach ($roundPlayerList as $k=>$v){
                if ($v['is_alive']&&$v['teamUrn']){
                    $countSurvivedPlayerArr[$v['teamUrn']]++;
                }
            }
            if (!empty($countSurvivedPlayerArr)){
                foreach ($countSurvivedPlayerArr as $k=>$v){
                    $updateArr =[];
                    $updateArr['survived_players'] =  $v;
                    $updateTeamArr[$k]['survived_players'] =  $v;
                    self::setTeamInfo($model,$k,$updateArr);
                    self::setRoundDetailsByRoundOrder($model,$roundOrder,$updateTeamArr);
                }
            }


        }
    }

    /**
     * 获取round player list
     * @param $model
     * @param $roundOrder
     * @return array|bool
     */
    public static function getPlayerList($model, $roundOrder)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $key        = ['history', 'battle', $model->battleId, 'round', $model->roundOrder, 'player'];
        $playerJson = $model->hGetAll($key);
        if (empty($playerJson)) {
            return false;
        } else {
            $playerArr = [];
            foreach ($playerJson as $k => $v) {
                $playerArr[$k] = json_decode($v, true);
            }
            return $playerArr;
        }
    }

}
