<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\task\services\wsdata\WebsocketCommon;
use Throwable;
use yii\base\Exception;
use function MongoDB\BSON\fromJSON;

class BayesCsgoSocketTeamService extends WebsocketCommon
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    /**
     * @param $model
     * @return array|null
     */
    public static function getTeamList($model)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $teamList = $model->hGetAll(['history', 'battle', $model->battleId, 'team', 'list']);
        if (empty($teamList)) {
            return null;
        } else {
            $newTeam = [];
            foreach ($teamList as $k => $v) {
                $newTeam[$k] = json_decode($v, true);
            }
            return $newTeam;
        }
    }

    /**
     * @param $model
     * @param null $customField
     * @param array $moreField
     * @return array|bool
     */
    public static function getTeamListFormat($model, $customField = null, $moreField = [])
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $redisValueList = BayesCsgoSocketTeamService::getTeamList($model);
        if (!empty($redisValueList)) {
            $newArr = [];
            $i = 0;
            foreach ($redisValueList as $k_team => $v_team_arr) {
                $teamId = $v_team_arr['team_id'];
                if ($customField == 'teamId') {
                    $key = $teamId;
                } elseif ($customField == 'urn') {
                    $key = $k_team;
                } else {
                    $key = $i;
                }
                $i++;
                $newArr[$key]['team_id']        = (int)$v_team_arr['team_id'];
                $newArr[$key]['name']           = $v_team_arr['name'];
                $newArr[$key]['image']          = $v_team_arr['image'];
                $newArr[$key]['opponent_order'] = $v_team_arr['opponent_order'];
                if (!empty($moreField)){
                    foreach ($moreField as $k=>$v){
                            $newArr[$key][$v] = $v_team_arr[$v];
                    }
                }

            }
            return $newArr;
        } else {
            return false;
        }
    }



    public static function formatTeam($model,$team){
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $newTeam = [];
        $newTeam['team_id'] = $team['team_id'];
        $newTeam['name'] = $team['name'];
        $newTeam['image'] = $team['image'];
        $newTeam['opponent_order'] = $team['opponent_order'];
        return $newTeam;
    }

    /**
     * @param $model
     * @param $side
     * @param string $extra
     * @return array|mixed|null
     */
    public static function getTeamBySide($model, $side, $extra = '')
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $teamList = BayesCsgoSocketTeamService::getTeamList($model);
        foreach ($teamList as $k => $v) {
            if ($side == $v['side']) {
                $v['teamUrn'] = $k;
                if ($extra == 'format') {
                    $newArr                   = [];
                    $newArr['team_id']        = $v['team_id'];
                    $newArr['name']           = $v['name'];
                    $newArr['image']          = $v['image'];
                    $newArr['opponent_order'] = $v['opponent_order'];
                    return $newArr;
                } else {
                    return $v;
                }
            }
        }
        return null;
    }

    /**
     * @param $model
     * @return array|bool|mixed
     */
    public static function getTableTeam($model){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['table','team','list'];
        $tableTeam = $model->hGetAll($key);
        if (!empty($tableTeam)){
            return $tableTeam;
        }else{
            return false;
        }
    }


    /**
     * @param $model
     * @param $teamId
     * @return mixed|null
     */
    public static function getTableTeamByTeamId($model,$teamId){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['table','team','list'];
        $tableTeam = $model->hGetAll($key);
        if (!empty($tableTeam)){
            return $tableTeam[$teamId] ? json_decode($tableTeam[$teamId], true) : null;
        }else{
            return null;
        }
    }

    /**
     * @param $model
     * @param $teamUrn
     * @param $upDateTeamArr
     * @return string|void
     * 更新current team list的数据
     */
    public static function setTeamInfo($model,$teamUrn,$upDateTeamArr,$specialOperation=''){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $oldData=BayesCsgoSocketTeamService::getTeamInfo($model,$teamUrn);
        if ($oldData && is_array($oldData)) {
            if ($specialOperation == 'incr') {
                foreach ($upDateTeamArr as $k => $v) {
                    $oldData[$k] += $v;
                }
            } else {
                $oldData = array_merge($oldData, $upDateTeamArr);
            }
        } else {
            $oldData = $upDateTeamArr;
        }
        $json = json_encode($oldData,true);
        return $model->hSet(['history','battle',$model->battleId,'team','list'],$teamUrn,$json);
    }

    /**
     * @param $model
     * @param $liveDataTeamUrn
     * @return bool|mixed
     */
    public static function getTeamInfo($model,$liveDataTeamUrn){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key  = ['history','battle',$model->battleId,'team','list'];
        $playerJson =   $model->hGet($key,$liveDataTeamUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }

    /**
     * @param $model
     * @param $battleId
     * @return bool
     */
    public static function getHistoryBattleTeamByBattleId($model,$battleId){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
//        $key  = ['history','battle',$battleId,'team'];
        $key  = ['history','battle',$model->battleId,'team','list'];
        $teamJson =   $model->hGetAll($key);
        if (empty($teamJson)){
            return false;
        }else{
            foreach ($teamJson as $k=>$v){
                $teamNewArr[$k]= json_decode($v,true);
            }
            return $teamNewArr;
        }
    }


    /**
     * @param $model
     * @param string $type
     * @return array|bool
     */
    public static function getTeamPlayerByType($model,$type='all'){
        if (!$model){
            $model  = new BayesCsgoSocket();
        }
        $playList = BayesCsgoSocketPlayerService::getPlayerList($model);
        if (!empty($playList)){
            $radiantArr = [];
            $direArr = [];
            $allArr = [];
            foreach($playList as $k=>$vArr){
                if ($vArr['faction']=='radiant'){
                    $radiantArr[] = $vArr;
                }else{
                    $direArr[] = $vArr;
                }
                $allArr[] = $vArr;
            }
            if ($type=='radiant'){
                return $radiantArr;
            }elseif ($type=='dire'){
                return $direArr;
            }else{
                return $allArr;
            }

        }else{
            return false;
        }

    }

    /**
     * @param $model
     * @param $teamUrn
     * @return mixed
     * 通过bayes 的teamid获取是t还是ct
     */
    public static function getTorCtByTeamUrn($model,$teamUrn)
    {
        $ctJson = $model->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_CT, self::KEY_TAG_DETAILS]);
        $ct = json_decode($ctJson,true);
        $tJson = $model->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_TEAM_T, self::KEY_TAG_DETAILS]);
        $t = json_decode($tJson,true);
        if($teamUrn == $ct['teamUrn']){
            return "ct";
        }elseif ($teamUrn == $t['teamUrn']){
            return "terrorist";
        }
    }
}
