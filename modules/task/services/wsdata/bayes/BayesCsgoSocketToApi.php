<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\SteamHelper;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Throwable;

/**
 * Bayes csgo Ws To Api
 **/
class BayesCsgoSocketToApi extends BayesCsgoSocket
{
    static private $instance;
    const CSGO_GAME_ID = 1;
    public $updateType = 1;
    public $info = [];
    public $teamList = [];

    /**
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     */
    public static function run($tagInfo, $taskInfo)
    {
        //task info
        $self = self::getInstance();
        $info = json_decode($taskInfo['params'], true);
        if (!$info['match_id']) {
            return false;
        }
        //match info
        $updateType       = $info['updateType'];
        $self->matchId    = $info['match_id'];
        $self->currentPre = $info['currentPre'] ? $info['currentPre'] : 'bayes_csgo_ws';
        if (array_key_exists('event_id', $info) && $info['event_id']) {
            $self->event_id = $info['event_id'];
        }
        if (array_key_exists('battleUrn', $info) && $info['battleUrn']) {
            $self->battleUrn = $info['battleUrn'];
        }else{
            return 'no battleUrn';
        }
        if (array_key_exists('refreshType', $info) && $info['refreshType']) {
            $self->refreshType = $info['refreshType'];
        }
        $self->info = $info;
        if (!$self->matchId) {
            $infoFormat = [
                'match'   => null,
                'battles' => []
            ];
        } else {
            $matchConversionInfo   = $self->conversionMatch($self, $self->matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battlesConversionInfo = $self->getMatchBattles($self, $self->matchId, $updateType);
            $infoFormat['battles'] = $battlesConversionInfo;
        }
        //refresh
        self::refresh($self->matchId, $infoFormat);
        //判断是不是结束了
        if ($infoFormat['match']['status'] == 3) {
            self::refreshApi($self->matchId, 9);
        }
        return true;

    }


    public function conversionMatch($self, $matchId)
    {
        $this->matchId = $matchId;
        //读取redis 组出来infoFormat
        $matchDetails = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_DETAILS]);
        if (!empty($matchDetails)) {
            $matchDetailsArr          = json_decode($matchDetails, true);
            $match_begin_at_value     = $matchDetailsArr['begin_at'];
            $match_end_at_value       = $matchDetailsArr['end_at'];
            $match_status_value       = $matchDetailsArr['status'];
            $match_team_1_score_value = $matchDetailsArr['team_1_score'];
            $match_team_2_score_value = $matchDetailsArr['team_2_score'];
            $match_game_version       = $matchDetailsArr['game_version'];
            $match_winner_value       = $matchDetailsArr['winner'];

            $matchinfoFormat = [
                'begin_at'             => $match_begin_at_value == 'null' ? '' : (string)$match_begin_at_value,
                'end_at'               => $match_end_at_value == 'null' ? '' : (string)$match_end_at_value,
                'status'               => $match_status_value ? intval($match_status_value) : 1,
                'team_1_score'         => intval($match_team_1_score_value),
                'team_2_score'         => intval($match_team_2_score_value),
                'winner'               => intval($match_winner_value),
                'is_battle_detailed'   => 1,
                'is_pbpdata_supported' => 1,
                'game_version'         => $match_game_version,
            ];
            return $matchinfoFormat;
        } else {
            return false;
        }

    }


    private function getMatchBattles($model,$matchId, $updateType = 1)
    {
        if (!$model){
            $model =  new self();
        }
        $model->updateType = $updateType;
        $battlesRes       = [];
        //查找所有battle
        $battle = BayesCsgoSocketBattleService::getBattleDetailsByUrn($model,$model->battleUrn);
        if (!empty($battle)){
            $battleId = $model->battleId= $battle['battle_id'];

            $battleInfo = $this->conversionBattles($model,$matchId, $battleId, $battle);
            if ($battleInfo['order']) {
                $battlesRes[$battle['battle_id']] = $battleInfo;
            }
            return $battlesRes;
        }else{
            return [];
        }

    }

    // base 数据格式(结果)
    private static $battleFieldConfig = [];
    // static 转换完数组(结果)
    private static $battleStaticFieldArray = [];
    // static 数据格式设置
    private static $battleStaticFieldConfig = [];
    // detail 数据格式设置(结果)
    private static $battleDetailFieldConfig = [];
    // battle下 players转换完数组(结果)
    private static $battlePlayersFieldArray = [];
    // rounds下 players转换完数组(结果)
    private static $roundPlayersFieldArray = [];
    // match下 teams转换完数组(结果)
    private static $teamsFieldArray = [];
    // battle下 rounds转换完数组(结果)
    private static $roundsFieldArray = [];
    // rounds下 events转换完数组(结果)
    private static $roundEventsFieldArray = [];
    private static $WinEndTypeArray = [
        'bomb_defused'   => 1,
        'cts_win'        => 2,
        'target_saved'   => 3,
        'target_bombed'  => 4,
        'terrorists_win' => 5,
        'round_draw'     => 6,
    ];

    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        // 初始化
        self::$battleFieldConfig = [
//            'status' => 3,
            'is_draw'            => 2,
            'is_forfeit'         => 2,
            'is_battle_detailed' => 1 //是否有对局详情数据
        ];
        self::$battleFieldConfig = array_merge(self::$battleFieldConfig, $baseResultData);
    }

    // set static数据
    public static function setBattleStatic($staticResultData)
    {
        // 初始化
        self::$battleStaticFieldConfig  = [];
        self::$battleStaticFieldArray[] = array_merge(self::$battleStaticFieldConfig, $staticResultData);
    }


    /**
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $battle
     * @return array|null[]
     */
    private function conversionBattles($model,$matchId, $battleId, $battle)
    {
        // 初始化
        self::initializationBattleInfo();
        // base

        //extra
        $this->setBayesCsgoBattleDetails($model,$battle);
        //base
        self::setBattleBase([
            'game'                 => self::CSGO_GAME_ID,
            'match'                => $matchId,
            'order'                => @$battle['order'] ? $battle['order'] : 0,
            'begin_at'             => @$battle['begin_at'],
            'end_at'               => @$battle['end_at'],
            'status'               => @$battle['status'], //battle状态
            'is_draw'              => @$battle['is_draw'] ? 1 : 2,
            'is_forfeit'           => @$battle['is_forfeit'] ? 1 : 2,
            'is_default_advantage' => @$battle['is_default_advantage'] ? 1 : 2, //是否是默认领先获胜
            'map'                  => @$battle['map'],
            'duration'             => @$battle['battle_duration'], //对局时长
            'winner'               => @$battle['winner'] ? (int)$battle['winner'] : null, //获胜战队order
            'deleted'               => 2, //
            'deleted_at'               => null, //
        ]);


        $model->teamList = $teamList= BayesCsgoSocketTeamService::getTeamList($model);

        // battle_events
        $this->getBattleEventByRedis($model,$battleId);
        // round_details
        $this->getRoundDetailsByRedis($model,$battleId,$battle,$model->teamList);
        // battle-players
        $this->getBattlePlayersByBattleId($model,$battle);
        // round-players
        $this->getAllRoundPlayerByBattleId($model,$battleId);
        // static(teams)
        $this->getBattleTeams($model,$battle);

        return [
            'order'         => @intval($battle['order']),
            'base'          => self::$battleFieldConfig,
            'static'        => self::$teamsFieldArray,
            'players'       => self::$battlePlayersFieldArray,
            'detail'        => self::$battleDetailFieldConfig,
            'rounds'        => self::$roundsFieldArray,
            'round_players' => self::$roundPlayersFieldArray,
            'events'        => self::$roundEventsFieldArray
        ];
    }

    public function getRoundDetailsByRedis($model,$battleId,$battle,$teamList){
        $rounds = $this->getRoundsByBattleIdOnApi($battleId);
        foreach ($rounds as $key => $round) {
            $j                         = 0;
            $roundArr                  = [];
            $roundTeamList = BayesCsgoSocketRoundService::getTeamList($model,$round['round_ordinal']);
            $roundArr['battle_id']     = $battleId;
            $roundArr['round_ordinal'] = @$round['round_ordinal'];
            //winner_team
            if ($round['winner_order']) {
                $roundArr['winner_team_id']                   = @$round['winner_order'];
                $roundArr['winner_team']['team_id']           = @$round['winner_order'];
            } else {
                $roundArr['winner_team_id']    = null;
                $roundArr['winner_team']       = [];
            }
            //winner_end_type
            $roundArr['winner_end_type'] = array_key_exists($round['round_end_type'], self::$WinEndTypeArray) ? self::$WinEndTypeArray[$round['round_end_type']] : null;
            //info
            $roundArr['begin_at'] = @(string)$round['begin_at'];
            $roundArr['end_at']   = @(string)$round['end_at'];
            $roundArr['status']   = @$round['status'];
            $roundArr['end_time'] = @(string)$round['end_at'];
            $j++;

            if (!empty($roundTeamList)){
                foreach ($roundTeamList as $k=>$v){
                    $sideArr['order']                    = $j;
                    $sideArr['game']                     = self::CSGO_GAME_ID;
                    $sideArr['identity_id']              = @$teamList[$k]['identity_id'];
                    $sideArr['rel_identity_id']          = @$teamList[$k]['rel_identity_id'];
                    $sideArr['team_id']                  = @$teamList[$k]['team_id'];
                    $sideArr['name']                     = @$teamList[$k]['name'];
                    $sideArr['starting_faction_side']    = @$teamList[$k]['starting_side'];;
                    $sideArr['camp']                     = $v['side'];
                    $sideArr['survived_players']         = @$v['survived_players'];
                    $sideArr['is_opening_kill_side']     = @$v['first_kills'] == 1 ? 1 : 2;
                    $sideArr['is_opening_kill_headshot'] = @$v['is_first_kills_headshot'] == 1 ? 1 : 2;
                    $sideArr['is_knife_kill']            = @null;
                    $sideArr['is_ace_kill']              = @$v['ace_kill'] == 1 ? 1 : 2;
                    $sideArr['kills']                    = @$v['kills']?$v['ct_kills']:0;
                    $sideArr['headshot_kills']           = @$v['headshot_kills']?$v['headshot_kills']:0;
                    $sideArr['side']                     = $v['side'];
                    $sideArr['round_ordinal']            = @(int)$round['round_ordinal'];
                    $sideArr['side_order']               = @$v['opponent_order'];

                    $sideArr['is_first_kill']          = @$v['first_kills']?1:2;
                    $sideArr['is_first_death']         = @$v['first_deaths']?1:2;
                    $sideArr['is_planted_bomb']        = @$v['is_planted_bomb']?1:2;
                    $sideArr['is_bomb_planted']        = @$v['is_planted_bomb']?1:2;
                    $sideArr['is_defused_bomb']        = @$v['is_defused_bomb']?1:2;
                    $sideArr['is_one_on_x_clutch']     = @$v['one_on_x_clutch']?1:2;
                    $sideArr['is_one_on_one_clutch']   = @$v['one_on_one_clutch']?1:2;
                    $sideArr['is_one_on_two_clutch']   = @$v['one_on_two_clutch']?1:2;
                    $sideArr['is_one_on_three_clutch'] = @$v['one_on_three_clutch']?1:2;
                    $sideArr['is_one_on_four_clutch']  = @$v['one_on_four_clutch']?1:2;
                    $sideArr['is_one_on_five_clutch']  = @$v['one_on_five_clutch']?1:2;
                    $sideArr['kills']                  = @$v['kills']?$v['kills']:0;
                    $sideArr['headshot_kills']         = @$v['headshot_kills']?$v['headshot_kills']:0;
                    $sideArr['deaths']                 = @$v['deaths']?$v['deaths']:0;
                    $sideArr['assists']                = @$v['assists']?$v['assists']:0;
                    $sideArr['flash_assists']          = null;

                    $sideArr['damage']                 = @$v['damage'];
                    $sideArr['team_damage']            = @$v['team_damage'];
                    $sideArr['damage_taken']           = @$v['damage_taken'];
                    $sideArr['hegrenade_damage_taken'] = @$v['hegrenade_damage_taken'];
                    $sideArr['inferno_damage_taken']   = @$v['inferno_damage_taken'];
                    $sideArr['chicken_kills']          = @$v['chicken_kills'];
                    $sideArr['blind_enemy_time']       = @(string)$v['blind_enemy_time'];
                    $sideArr['blind_teammate_time']    = @(string)$v['blind_teammate_time'];

                    $sideArr['team_kills']    = @$v['team_kills'] ? $v['team_kills'] : 0;
                    $sideArr['multi_kills']   = @$v['multi_kills'] ? $v['multi_kills'] : 0;
                    $sideArr['two_kills']     = @$v['two_kills'] ? $v['two_kills'] : 0;
                    $sideArr['three_kills']   = @$v['three_kills'] ? $v['three_kills'] : 0;
                    $sideArr['four_kills']    = @$v['four_kills'] ? $v['four_kills'] : 0;
                    $sideArr['five_kills']    = @$v['five_kills'] ? $v['five_kills'] : 0;
                    $sideArr['hit_generic']   = @$v['hit_generic'] ? $v['hit_generic'] : 0;
                    $sideArr['hit_head']      = @$v['hit_head'] ? $v['hit_head'] : 0;
                    $sideArr['hit_chest']     = @$v['hit_chest'] ? $v['hit_chest'] : 0;
                    $sideArr['hit_stomach']   = @$v['hit_stomach'] ? $v['hit_stomach'] : 0;
                    $sideArr['hit_left_arm']  = @$v['hit_left_arm'] ? $v['hit_left_arm'] : 0;
                    $sideArr['hit_right_arm'] = @$v['hit_right_arm'] ? $v['hit_right_arm'] : 0;
                    $sideArr['hit_left_leg']  = @$v['hit_left_leg'] ? $v['hit_left_leg'] : 0;
                    $sideArr['hit_right_leg'] = @$v['hit_right_leg'] ? $v['hit_right_leg'] : 0;
                    $sideArr['awp_kills']     = @$v['awp_kills'] ? $v['awp_kills'] : 0;
                    $sideArr['knife_kills']   = @$v['knife_kills'] ? $v['knife_kills'] : 0;
                    $sideArr['taser_kills']   = @$v['taser_kills'] ? $v['taser_kills'] : 0;
                    $sideArr['shotgun_kills'] = @$v['shotgun_kills'] ? $v['shotgun_kills'] : 0;

                    $first_kill_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_first_kill'];
                    $first_details_arr = $this->lRange($first_kill_key,0,-1);
                    if (!empty($first_details_arr)){
                        $first_details_arr=$this->unSetSpecialEvent($first_details_arr,'opening_kill_details');
                        $sideArr['opening_kill_details'] = $first_details_arr[0];
                    }

                    $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_bomb_planted'];
                    $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
                    if (!empty($bomb_planted_details_arr)){
                        $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'bomb_planted_details');
                        $sideArr['bomb_planted_details'] = $bomb_planted_details_arr[0];
                    }

                    $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_knife_kill'];
                    $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
                    if (!empty($bomb_planted_details_arr)){
                        $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'knife_kill_details');
                        foreach ($bomb_planted_details_arr as $k6=>$v6){
                            $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                        }
                        $json = json_encode($bomb_planted_details_arr,320);
                        $sideArr['knife_kill_details'] = $json;
                    }

                    $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_taser_kill'];
                    $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
                    if (!empty($bomb_planted_details_arr)){
                        $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'taser_kill_details');
                        foreach ($bomb_planted_details_arr as $k6=>$v6){
                            $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                        }
                        $json = json_encode($bomb_planted_details_arr,320);
                        $sideArr['taser_kill_details'] = $json;
                    }

                    $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_ace_kill'];
                    $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
                    if (!empty($bomb_planted_details_arr)){
                        $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'ace_kill_details');
                        $sideArr['ace_kill_details'] = $bomb_planted_details_arr[0];
                    }

                    $bomb_planted_key =  [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,$battleId,self::KEY_TAG_ROUND,$roundArr['round_ordinal'],'ct_team_kill'];
                    $bomb_planted_details_arr = $this->lRange($bomb_planted_key,0,-1);
                    if (!empty($bomb_planted_details_arr)){
                        $bomb_planted_details_arr=$this->unSetSpecialEvent($bomb_planted_details_arr,'team_kill_details');
                        foreach ($bomb_planted_details_arr as $k6=>$v6){
                            $bomb_planted_details_arr[$k6] = json_decode($v6,true);
                        }
                        $json = json_encode($bomb_planted_details_arr,320);
                        $sideArr['team_kill_details'] = $json;
                    }
                    $roundArr['side'][$v['side']] = $sideArr ;
                }

                self::$roundsFieldArray[]    = $roundArr;

            }


        }

        return true;
    }

    /**
     * set battle
     * @param $model
     * @param $battle
     */
    public function setBayesCsgoBattleDetails($model,$battle)
    {

        self::$battleDetailFieldConfig = [
            'is_confirmed'                  => 1,
            'is_finished'                   => @$battle['status'] == 3 ? 1 : 2,
            'is_pause'                      => intval($battle['is_pause'] ? 1 : 2),
            'is_live'                       => @intval($battle['is_live'] ? 1 : 2),
            'current_round'                 => @intval($battle['current_round']),
            'is_freeze_time'                => @intval($battle['is_freeze_time'] ? 1 : 2),
            'in_round_timestamp'            => @intval($battle['in_round_timestamp']),
            'round_time'                    => @intval($battle['round_time']),
            'time_since_plant'              => @intval($battle['time_since_plant']),
            'is_bomb_planted'               => @intval($battle['is_bomb_planted'] ? 1 : 2),
            'win_round_1_side'              => @$battle['win_round_1_side'],
            'win_round_1_team'              => @$battle['win_round_1_team'] ? intval($battle['win_round_1_team']) : null,
            'win_round_1_detail'            => @$battle['win_round_1_detail'],
            'win_round_16_side'             => @$battle['win_round_16_side'],
            'win_round_16_team'             => @$battle['win_round_16_team'] ? @intval($battle['win_round_16_team']) : null,
            'win_round_16_detail'           => @$battle['win_round_16_detail'],
            'first_to_5_rounds_wins_side'   => @$battle['first_to_5_rounds_wins_side'],
            'first_to_5_rounds_wins_team'   => @$battle['first_to_5_rounds_wins_team'] ? intval($battle['first_to_5_rounds_wins_team']) : null,
            'first_to_5_rounds_wins_detail' => @$battle['first_to_5_rounds_wins_detail'],
            'live_rounds'                   => @$this->getLiveRoundsId($battle['battle_id'])
        ];

    }


    public function getBattleTeams($model, $battle)
    {
        $teamList = BayesCsgoSocketTeamService::getTeamList($model);
        $teamArr  = [];
        if (!empty($teamList)) {
            foreach ($teamList as $k => $v) {
                $teamArr[$k]['order']                  = $v['opponent_order'];
                $teamArr[$k]['score']                  = $battle[$k]['score'];
                $teamArr[$k]['game']                   = self::CSGO_GAME_ID;
                $teamArr[$k]['identity_id']            = $v['name'];
                $teamArr[$k]['rel_identity_id']        = $v['name'];
                $teamArr[$k]['team_id']                = $v['team_id'];
                $teamArr[$k]['name']                   = $v['name'];
                $teamArr[$k]['starting_faction_side']  = $v['starting_side'];
                $teamArr[$k]['1st_half_score']         = $v['1st_half_score'];
                $teamArr[$k]['2nd_half_score']         = $v['2nd_half_score'];
                $teamArr[$k]['ot_score']               = $v['ot_score'];
                $teamArr[$k]['kills']                  = @$v['kills'];
                $teamArr[$k]['headshot_kills']         = @$v['headshot_kills'];
                $teamArr[$k]['deaths']                 = @$v['deaths'];
                $teamArr[$k]['kd_diff']                = @$v['kd_diff'];
                $teamArr[$k]['assists']                = @$v['assists'];
                $teamArr[$k]['flash_assists']          = @$v['flash_assists'];
                $teamArr[$k]['adr']                    = @(string)$v['adr'];
                $teamArr[$k]['first_kills']            = @$v['first_kills'];
                $teamArr[$k]['first_deaths']           = @$v['first_deaths'];
                $teamArr[$k]['first_kills_diff']       = @$v['first_kills_diff'];
                $teamArr[$k]['kast']                   = @(string)$v['kast'];
                $teamArr[$k]['rating']                 = @(string)$v['rating'];
                $teamArr[$k]['damage']                 = @$v['damage'];
                $teamArr[$k]['team_damage']            = @$v['team_damage'];
                $teamArr[$k]['damage_taken']           = @$v['damage_taken'];
                $teamArr[$k]['hegrenade_damage_taken'] = @$v['hegrenade_damage_taken'];
                $teamArr[$k]['inferno_damage_taken']   = @$v['inferno_damage_taken'];
                $teamArr[$k]['chicken_kills']          = @$v['chicken_kills'];
                $teamArr[$k]['blind_enemy_time']       = @(string)$v['blind_enemy_time'];
                $teamArr[$k]['blind_teammate_time']    = @(string)$v['blind_teammate_time'];
                $teamArr[$k]['planted_bomb']           = @$v['planted_bomb'];
                $teamArr[$k]['defused_bomb']           = @$v['defused_bomb'];
                $teamArr[$k]['team_kills']             = @$v['team_kills'];
                $teamArr[$k]['one_kills']              = @(int)$v['one_kills'];
                $teamArr[$k]['multi_kills']            = @$v['multi_kills'];
                $teamArr[$k]['two_kills']              = @$v['two_kills'];
                $teamArr[$k]['three_kills']            = @$v['three_kills'];
                $teamArr[$k]['four_kills']             = @$v['four_kills'];
                $teamArr[$k]['five_kills']             = @$v['five_kills'];
                $teamArr[$k]['one_on_x_clutches']      = @$v['one_on_x_clutches'];
                $teamArr[$k]['one_on_one_clutches']    = @$v['one_on_one_clutches'];
                $teamArr[$k]['one_on_two_clutches']    = @$v['one_on_two_clutches'];
                $teamArr[$k]['one_on_three_clutches']  = @$v['one_on_three_clutches'];
                $teamArr[$k]['one_on_four_clutches']   = @$v['one_on_four_clutches'];
                $teamArr[$k]['one_on_five_clutches']   = @$v['one_on_five_clutches'];
                $teamArr[$k]['hit_generic']            = @$v['hit_generic'];
                $teamArr[$k]['hit_head']               = @$v['hit_head'];
                $teamArr[$k]['hit_chest']              = @$v['hit_chest'];
                $teamArr[$k]['hit_stomach']            = @$v['hit_stomach'];
                $teamArr[$k]['hit_left_arm']           = @$v['hit_left_arm'];
                $teamArr[$k]['hit_right_arm']          = @$v['hit_right_arm'];
                $teamArr[$k]['hit_left_leg']           = @$v['hit_left_leg'];
                $teamArr[$k]['hit_right_leg']          = @$v['hit_right_leg'];
                $teamArr[$k]['awp_kills']              = @$v['awp_kills'];
                $teamArr[$k]['knife_kills']            = @$v['knife_kills'];
                $teamArr[$k]['taser_kills']            = @$v['taser_kills'];
                $teamArr[$k]['shotgun_kills']          = @$v['shotgun_kills'];
            }
            self::$teamsFieldArray = $teamArr;
        }
    }

    public function getBattlePlayersByBattleId($model,$battle)
    {
        $battleId        = $battle['battle_id'];
        $realBattleOrder = @$battle['real_order'];
        if ($this->currentPre == 'hltv_ws') {
            $playerList      = $this->getBattlePlayerListByType('all', $battleId,'steam_id');
        }else{
            $playerList      = $this->getBattlePlayerListByType('all', $battleId);
        }
        $i               = 0;
        foreach ($playerList as $k => $v) {
            $i++;
            $playerArr                       = [];
            $playerArr['game']               = self::CSGO_GAME_ID;
            $playerArr['order']              = $i;
            $playerArr['team_order']         = @$v['team_order'];
            $playerArr['side']         = @$v['side'];
            $weaponArr = [];
            $weapon_list                          = $this->getRoundPlayerWeaponInfoBySteamId($v['steam_id']);
            if (!empty($weapon_list)) {
                foreach ($weapon_list as $k_weapon => $v_weapon) {
                    if ($v_weapon['id']){
                        $weaponArr[] = (int)$v_weapon['id'];
                    }
                }
                if (!empty($weaponArr)){
                    $playerArr['weapon']         = @json_encode($weaponArr,320);
                }else{
                    $playerArr['weapon'] = json_encode([],320);
                }
            }else{
                $playerArr['weapon'] = json_encode([],320);
            }
            $playerArr['has_kevlar']    = @$v['has_kevlar']?1:2;
            $playerArr['has_helmet']    = @$v['has_helmet']?1:2;
            $playerArr['has_defusekit'] = @$v['has_defusekit']?1:2;
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['has_bomb'] = null;
            }else{
                $playerArr['has_bomb'] = @$v['has_bomb']?1:2;
            }

            $grenades_list                  = $this->getRoundPlayerGrenadesInfoBySteamId($v['steam_id']);
            $grenadesArr =[];
            if (!empty($grenades_list)) {
                foreach ($grenades_list as $k_grenades => $v_grenade) {
                    $grenadesArr[] = (int)$v_grenade['id'];
                }
                if (!empty($grenadesArr)){
                    $playerArr['grenades']         = @json_encode($grenadesArr,320);
                }else{
                    $playerArr['grenades'] = json_encode([],320);
                }
            }else{
                $playerArr['grenades'] = json_encode([],320);
            }

            $playerArr['hp']              = @(int)$v['hp'];
            $playerArr['is_alive']        = @$v['is_alive'] ? 1 : 2;
            $playerArr['money']           = @(int)$v['money'];
            $playerArr['position']        = @(string)$v['position'];

            if ($this->currentPre == 'hltv_ws') {
                $playerArr['equipment_value'] = null;
            }else{
                $weaponMoney = $this->getWeaponCast($weapon_list, $grenades_list,$v['has_kevlar'],$v['has_helmet'],$v['has_defusekit']);
                $playerArr['equipment_value'] = $weaponMoney ? (int)$weaponMoney : 0;
            }

            $playerArr['blinded_time']    = @$v['steam_id'] ? (string)$this->getFlashBlindnessTime($v['steam_id']) : 0;
            if ($playerArr['blinded_time'] > 100) {
                $playerArr['blinded_time'] = (string)0;
            }
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['blinded_time'] = null;
                $playerArr['ping']            = null;
            }else{
                $playerArr['ping']            = @(int)$v['ping'];
            }



            $playerArr['team_id']            = @$v['team_id'];
            $playerArr['player_id']          = @$v['player_id'];
            $playerArr['nick_name']          = @$v['nick_name'];
            $playerArr['kills']              = @$v['kills'];
            $playerArr['headshot_kills']     = @$v['headshot_kills'];
            $playerArr['deaths']             = @$v['deaths'];
            $playerArr['k_d_diff']           = @$v['k_d_diff'];
            $playerArr['assists']            = @$v['assists'];
            $playerArr['flash_assists']      = @$v['flash_assist'];
            $playerArr['adr']                = @$v['adr'];
            $playerArr['first_kills']        = @$v['first_kills'];
            $playerArr['first_deaths']       = @$v['first_deaths'];
            $playerArr['first_kills_diff']   = @$v['first_kills_diff'];
            $playerArr['multi_kills']        = @$v['multi_kills'];
            $playerArr['1_v_n_clutches']     = @$v['one_on_x_clutches'];
            $playerArr['knife_kill']         = @$v['knife_kill'];
            $playerArr['ace_kill']           = @$v['ace_kill'];
            $playerArr['money']              = @$v['money'];
            $playerArr['coordinate']         = (string)$v['position'];
            $playerArr['team_name']          = @$v['team_name'];

            $playerArr['steam_id']           = @$v['steam_id'];
            //5e需要的额外字段
            $playerArr['kast']            = @(string)round($v['kast'],4);
            $playerArr['rating']            = @(string)round($v['rating'],2);
            if ($this->currentPre == 'hltv_ws') {
                $playerArr['damage']            = null;
                $playerArr['team_damage']            = null;
                $playerArr['damage_taken']           = null;
                $playerArr['hegrenade_damage_taken'] = null;
                $playerArr['inferno_damage_taken']   = null;
                $playerArr['chicken_kills']          = null;
                $playerArr['blind_enemy_time']       = null;
                $playerArr['blind_teammate_time']    = null;
            }else{
                $playerArr['damage']             = @$v['real_damage'];
                $playerArr['team_damage']            = @$v['team_damage'];
                $playerArr['damage_taken']           = @$v['damage_taken'];
                $playerArr['hegrenade_damage_taken'] = @$v['hegrenade_damage_taken'];
                $playerArr['inferno_damage_taken']   = @$v['inferno_damage_taken'];
                $playerArr['chicken_kills']          = @$v['chicken_kills'];
                $playerArr['blind_enemy_time']       = @(string)$v['blind_enemy_time'];
                $playerArr['blind_teammate_time']    = @(string)$v['blind_teammate_time'];
            }

            $playerArr['planted_bomb']           = @$v['planted_bomb'];
            $playerArr['defused_bomb']           = @$v['defused_bomb'];
            $playerArr['team_kills']             = @$v['team_kills'];
            $playerArr['one_kills']              = @$v['one_kills'];
            $playerArr['two_kills']              = @$v['two_kills'];
            $playerArr['three_kills']            = @$v['three_kills'];
            $playerArr['four_kills']             = @$v['four_kills'];
            $playerArr['five_kills']             = @$v['five_kills'];
            $playerArr['one_on_one_clutches']    = @$v['one_on_one_clutches'];
            $playerArr['one_on_two_clutches']    = @$v['one_on_two_clutches'];
            $playerArr['one_on_three_clutches']  = @$v['one_on_three_clutches'];
            $playerArr['one_on_four_clutches']   = @$v['one_on_four_clutches'];
            $playerArr['one_on_five_clutches']   = @$v['one_on_five_clutches'];
            $playerArr['hit_generic']            = @$v['hit_generic'];
            $playerArr['hit_head']               = @$v['hit_head'];
            $playerArr['hit_chest']              = @$v['hit_chest'];
            $playerArr['hit_stomach']            = @$v['hit_stomach'];
            $playerArr['hit_left_arm']           = @$v['hit_left_arm'];
            $playerArr['hit_right_arm']          = @$v['hit_right_arm'];
            $playerArr['hit_left_leg']           = @$v['hit_left_leg'];
            $playerArr['hit_right_leg']          = @$v['hit_right_leg'];
            $playerArr['awp_kills']              = @$v['awp_kills'];
            $playerArr['knife_kills']            = @$v['knife_kills'];
            $playerArr['taser_kills']            = @$v['taser_kills'];
            $playerArr['shotgun_kills']          = @$v['shotgun_kills'];
            $playerArr['steam_id']               = @(string)SteamHelper::playerConversionSteamId($v['steam_id']);
            self::$battlePlayersFieldArray[] = $playerArr;
        }
    }


    public function getRoundsByBattleIdOnApi($battleId)
    {
        $newRounds = $this->getRoundsByBattleId($battleId);
        if ($this->updateType == 2||$this->updateType == 3) {
            $roundsLength = count($newRounds);
            ksort($newRounds);
            if ($this->refreshType=='refresh'){
                $sliceRoundsLength =6;
            }else{
                $sliceRoundsLength =2;
            }
            //default refresh 2 rounds
            if ($roundsLength >= $sliceRoundsLength) {
                $newRounds = array_slice($newRounds, -$sliceRoundsLength, $sliceRoundsLength, true);
            }
        }
        return $newRounds;
    }
    public function getAllRoundPlayerByBattleId($model,$battleId)
    {
        if (!$model){
            $model = new  self();
        }
        $rounds = $this->getRoundsByBattleIdOnApi($battleId);
        foreach ($rounds as $k => $v) {
            $roundPlayers = $this->getRoundPlayersByRoundOrderAndBattleId($battleId, $v['round_ordinal']);
            if ($roundPlayers) {

//                $playerOrder = 0;
                $i = 0;
                foreach ($roundPlayers as $k_player => $v_player) {

                    $i++;
                    $playerArr                  = [];
                    $playerArr['game']          = self::CSGO_GAME_ID;
                    $playerArr['order']         = $i;
                    $playerArr['player_order']  = @$v_player['team_order'];
                    $playerArr['round_ordinal'] = @$v['round_ordinal'];
                    $playerArr['side']              = @$v_player['side'];
                    $playerArr['team_id']           = @$v_player['team_id'];
                    $playerArr['player_id']         = @$v_player['player_id'];
                    $playerArr['nick_name']         = @$v_player['nick_name'];
                    $playerArr['kills']             = @$v_player['kills']?$v_player['kills']:0;
                    $playerArr['headshot_kills']    = @$v_player['headshot_kills']?$v_player['headshot_kills']:0;
                    $playerArr['is_died']           = @$v_player['is_alive'] ? 2 : 1;
                    $playerArr['assists']           = @$v_player['assists']?$v_player['assists']:0;
                    $playerArr['flash_assists']     = @null;

                    $playerArr['is_first_kill']     = @$v_player['first_kills'] ? 1 : 2;
                    $playerArr['is_first_death']    = @$v_player['first_deaths'] ? 1 : 2;
                    $playerArr['is_multi_kill']     = @$v_player['multi_kills'] ? 1 : 2;
                    $playerArr['is_1_v_n_clutche']  = @$v_player['one_on_x_clutches'] ? 1 : 2;
                    $playerArr['is_knife_kill']     = @$v_player['knife_kill'] ? 1 : 2;
                    $playerArr['is_ace_kill']       = @$v_player['five_kills'] ? 1 : 2;
                    $playerArr['steam_id']          = @$v_player['steam_id'];
                    $playerArr['camp']              = @$v_player['side'];
                    $playerArr['player_name']       = @$v_player['nick_name'];
                    $playerArr['team_name']         = $model->teamList[$v_player['teamUrn']]['name'];

                    $playerArr['damage']                 = @$v_player['real_damage'];
                    $playerArr['team_damage']            = @$v_player['team_damage'];
                    $playerArr['damage_taken']           = @$v_player['damage_taken'];
                    $playerArr['hegrenade_damage_taken'] = @$v_player['hegrenade_damage_taken'];
                    $playerArr['inferno_damage_taken']   = @$v_player['inferno_damage_taken'];
                    $playerArr['chicken_kills']          = @$v_player['chicken_kills'];
                    $playerArr['blind_enemy_time']       = @(string)$v_player['blind_enemy_time'];
                    $playerArr['blind_teammate_time']    = @(string)$v_player['blind_teammate_time'];

                    $playerArr['is_planted_bomb']        = @$v_player['planted_bomb']?1:2;
                    $playerArr['is_defused_bomb']        = @$v_player['defused_bomb']?1:2;

                    $playerArr['team_kills']             = @$v_player['team_kills']?$v_player['team_kills']:0;
                    $playerArr['one_kills']              = @$v_player['one_kills']?$v_player['one_kills']:0;
                    $playerArr['two_kills']              = @$v_player['two_kills']?$v_player['two_kills']:0;
                    $playerArr['three_kills']            = @$v_player['three_kills']?$v_player['three_kills']:0;
                    $playerArr['four_kills']             = @$v_player['four_kills']?$v_player['four_kills']:0;
                    $playerArr['five_kills']             = @$v_player['five_kills']?$v_player['five_kills']:0;
                    $playerArr['is_one_on_one_clutch']   = @$v_player['one_on_one_clutches']?1:2;
                    $playerArr['is_one_on_two_clutch']   = @$v_player['one_on_two_clutches']?1:2;
                    $playerArr['is_one_on_three_clutch'] = @$v_player['one_on_three_clutches']?1:2;
                    $playerArr['is_one_on_four_clutch']  = @$v_player['one_on_four_clutches']?1:2;
                    $playerArr['is_one_on_five_clutch']  = @$v_player['one_on_five_clutches']?1:2;
                    $playerArr['hit_generic']            = @$v_player['hit_generic']?$v_player['hit_generic']:0;
                    $playerArr['hit_head']               = @$v_player['hit_head']?$v_player['hit_head']:0;
                    $playerArr['hit_chest']              = @$v_player['hit_chest']?$v_player['hit_chest']:0;
                    $playerArr['hit_stomach']            = @$v_player['hit_stomach']?$v_player['hit_stomach']:0;
                    $playerArr['hit_left_arm']           = @$v_player['hit_left_arm']?$v_player['hit_left_arm']:0;
                    $playerArr['hit_right_arm']          = @$v_player['hit_right_arm']?$v_player['hit_right_arm']:0;
                    $playerArr['awp_kills']              = @$v_player['awp_kills']?$v_player['awp_kills']:0;
                    $playerArr['knife_kills']            = @$v_player['knife_kills']?$v_player['knife_kills']:0;
                    $playerArr['taser_kills']            = @$v_player['taser_kills']?$v_player['taser_kills']:0;
                    $playerArr['shotgun_kills']          = @$v_player['shotgun_kills']?$v_player['shotgun_kills']:0;
                    $playerArr['steam_id']               = @(string)SteamHelper::playerConversionSteamId($v_player['steam_id']);
                    self::$roundPlayersFieldArray[] = $playerArr;
                }
            }
        }
        return true;
    }

    public function getEventByBattleId($model,$battleId)
    {
//        $events = BayesCsgoSocketEventService::getBattleEventByBattleId($model,$battleId);
//        return $events;
        $roundEventsArr = [];
        $rounds= $this->getRoundsByBattleIdOnApi($battleId);
        foreach ($rounds as $k=>$v){
            $roundEvents = BayesCsgoSocketEventService::getBattleEventByRoundOrder($model,$battleId,$v['round_ordinal']);
            foreach ($roundEvents as $k1=>$v1){
                $roundEventsArr[] =$v1;
            }
        }
        return $roundEventsArr;
    }

    /**
     * 获取battle events
     * @param $model
     * @param $battleId
     * @return bool
     */
    public function getBattleEventByRedis($model,$battleId)
    {
        if (!$model){
            $model = new self();
        }
        $events = $this->getEventByBattleId($model,$battleId);
        if ($events) {
            $doTypeArray = [
                'round_start',
                'round_end',
                'player_kill',
                'bomb_planted',
                'bomb_defused',
                'player_suicide',
            ];
            $i           = 0;
            foreach ($events as $k => $v) {
                if (in_array($v['event_type'], $doTypeArray)) {
                    $i++;
                    $v['game']            = self::CSGO_GAME_ID;
                    $v['match']           = $model->matchId;
                    $v['order']           = $i;
                    $v['is_bomb_planted'] = @$v['is_bomb_planted'] ? 1 : 2;
                    if ($v['event_type'] == 'round_end') {
                        if (array_key_exists(@$v['round_end_type'], self::$WinEndTypeArray)) {
                            @$v['round_end_type'] = @(int)self::$WinEndTypeArray[@$v['round_end_type']];
                            $v['winner_side'] = @$v['winner_side'] ?? '';
                        } else {
                            $v['round_end_type'] = null;
                            $v['winner_side']    = '';
                        }
                    } elseif ($v['event_type'] == 'player_kill') {
                        $v['killer_nick_name'] = @$v['killer']['nick_name'];
                        $v['killer_side']      = @$v['killer']['side'];
                        $v['killer_position']  = @$v['killer']['position'];
                        $v['killer_steam_id']  = @(string)$v['killer']['steam_id'];
                        $v['killer']           = @(int)$v['killer']['player_id'];

                        $v['victim_nick_name'] = @$v['victim']['nick_name'];
                        $v['victim_side']      = @$v['victim']['side'];
                        $v['victim_position']  = @$v['victim']['position'];
                        $v['victim_steam_id']  = @(string)$v['victim']['steam_id'];
                        $v['victim']           = @(int)$v['victim']['player_id'];

                        $v['assist_nick_name'] = @$v['assist']['nick_name'];
                        $v['assist_side']      = @$v['assist']['side'];
                        $v['assist_steam_id']  = @(string)$v['assist']['steam_id'];
                        $v['assist']           = @(int)$v['assist']['player_id'];

                        $v['flashassistt_nick_name'] = @$v['flashassist']['nick_name'];
                        $v['flashassist_side']       = @$v['flashassist']['side'];
                        $v['flashassist_steam_id']   = @(string)$v['flashassist']['steam_id'];
                        $v['flashassist']            = @(int)$v['flashassist']['player_id'];

                        $v['weapon_name'] = @$v['weapon']['weapon_name'];
                        $v['weapon']      = @(int)$v['weapon']['weapon_id'];
                        $v['is_headshot'] = @$v['is_headshot'] ? 1 : 2;
                    } elseif ($v['event_type'] == 'bomb_planted') {
                        $v['bomb_planted_player_id'] = @(int)$v['player_id'];
                        $v['bomb_planted_nick_name'] = @$v['nick_name'];
                        $v['steam_id']               = @(string)$v['steam_id'];
                        $v['bomb_planted_side']      = @$v['side'];
                        $v['bomb_planted_position']  = @$v['position'];

                    } elseif ($v['event_type'] == 'bomb_defused') {
                        $v['bomb_defused_player_id'] = @(int)$v['player_id'];
                        $v['bomb_defused_nick_name'] = @$v['nick_name'];
                        $v['steam_id']               = @(string)$v['steam_id'];
                        $v['bomb_defused_side']      = @$v['side'];
                        $v['bomb_defused_position']  = @$v['position'];
                    } elseif ($v['event_type'] == 'player_suicide') {
                        $v['weapon_name']       = @$v['weapon']['weapon_name'];
                        $v['weapon']            = @(int)$v['weapon']['weapon_id'];
                        $v['suicide_player_id'] = @(int)$v['player_id'];
                        $v['suicide_nick_name'] = @$v['nick_name'];
                        $v['steam_id']          = @(string)$v['steam_id'];
                        $v['suicide_side']      = @$v['side'];
                        $v['suicide_position']  = @$v['position'];
                    }else{
                        continue;
                    }
                    if (!empty($v)){
                        self::$roundEventsFieldArray[] = $v;
                    }
                }
            }
        }
        return true;
    }



    public function unSetSpecialEvent($first_details_arr, $type)
    {
        $newArr = [];
        foreach ($first_details_arr as $k => $v) {
            $vNewArr = [];
            $vArr    = json_decode($v, true);
            if ($type == 'opening_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'bomb_planted_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['player_id']           = $vArr['player_id'];
                $vNewArr['nick_name']           = $vArr['nick_name'];
                $vNewArr['steam_id']            = $vArr['steam_id'];
                $vNewArr['side']                = $vArr['side'];
                $vNewArr['position']            = $vArr['position'];
                $vNewArr['survived_players_ct'] = $vArr['survived_players_ct'];
                $vNewArr['survived_players_t']  = $vArr['survived_players_t'];
            } elseif ($type == 'knife_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'taser_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'ace_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'team_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }
            $newArr[$k] = json_encode($vNewArr, 320);
        }
        return $newArr;

    }




    // 初始化
    public function initializationBattleInfo()
    {
        self::$battlePlayersFieldArray = [];
        self::$roundPlayersFieldArray  = [];
        self::$teamsFieldArray         = [];
        self::$battleFieldConfig       = [];
        self::$battleDetailFieldConfig = [];
        self::$roundsFieldArray        = [];
        self::$roundEventsFieldArray   = [];
    }


    private static function refresh($matchId, $formatInfo)
    {
        // todo getMountedId,有可能会找不到，找不到的留空
        self::setMatch($matchId, $formatInfo['match']);
        foreach ($formatInfo['battles'] as $battle) {
            self::setBattle($matchId, $battle);
        }
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $battle['order'],
        ])->one();
        if (!$battleBase) {
            $battleBase = new MatchBattle();
            $battleBase->setAttributes([
                'match' => $matchId,
                'order' => $battle['order'],
            ]);
            $battleBase->save();
        }
        $battleId                = $battleBase['id'];
        $battle['base']['match'] = $matchId;
        BattleService::setBattleInfo($battleId, 'csgo', $battle['base'], BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['detail'], BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['static'], BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['players'], BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['rounds'], BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['events'], BattleService::DATA_TYPE_EVENTS);
        BattleService::setBattleInfo($battleId, 'csgo', $battle['round_players'], BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

    public static function getBattleFormat()
    {

    }


    /**
     * @return string
     */
    public function getBattles()
    {
        return $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST]);
    }
    //刷API
    private static function refreshApi($matchId,$originId){
        //battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId,   RenovateService::INTERFACE_TYPE_BATTLE, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //match
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId, RenovateService::INTERFACE_TYPE_MATCH, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        return true;
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}