<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class BayesLolWs extends HotBase
{
    private static $instance;
    public $battle_order_relation_id = [];
    public $matchId = null;
    public $origin_id = null;
    public $rel_matchId = null;
    public $map = null;
    public $matchInfo = null;
    public $matchRealTimeInfo = null;
    public $rel_order_id = null;
    public $now_battle_order = null;
    public $socket_id = null;
    public $socket_time = null;
    public $match_status = null;
    public $battleId = null;
    public $battle_order = null;
    public $team_name_id_relation = null;
    public $teams_info = null;
    public $player_name_id_relation = null;
    public $player_info = null;
    public $current = [];
    public $battles = null;
    public $memory_mode = false;
    public $is_reload = false;
    public $match_all_champion_websocket = [];
    //其它
    public $lol_champion_relation_id_list = [];
    public $lol_items_relation = [];
    public $lol_items_list = [];
    public $lol_item_unknown_id = null;
    public $lol_champion_list = [];
    public $lol_runes_list = [];
    public $lol_runes_relation_list = [];
    public $lol_summoner_spell_name_relation_id_list = [];
    public $lol_summoner_spell_list = [];
    public $enum_ingame_goal_info = [];
    public $enum_socket_game_configure = [];
    public $match_socket_sub_type = null;
    public $operate_type = null;

    //程序入口
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $BayesLolWs = self::getInstance();
        $redis = $BayesLolWs->redis;
        //内存模式
        $BayesLolWs->memory_mode = true;
        //解析
        $res = [];
        $runTag = @$tagInfo['ext2_type'];
        if ($runTag == 'refresh') {
            $info = @json_decode($taskInfo['params'], true);
            $rel_match_id = @$info['rel_match_id'];
            $match_id = @$info['match_id'];
            $origin_id = @$info['origin_id'];
            $refresh_sub_type = @$info['refresh_sub_type'];
            $BayesLolWs->operate_type = @$info['operate_type'];
            $socket_sub_type = Common::getSocketSubTypeByOriginId($origin_id,$refresh_sub_type);
            $rel_match_id_string = self::getPerIdByRelMatchId($rel_match_id,$origin_id);
            if ($rel_match_id_string){
                $BayesLolWs->delRedisAndMemory($redis,$origin_id,$rel_match_id_string,$match_id);
                //查询所有
                $match_list = MatchLived::find()->where(['match_id'=>$rel_match_id_string])->asArray()->all();
                if ($match_list){
                    foreach ($match_list as $key => $value){
                        if (!empty($socket_sub_type) && strpos($value['match_data'],$socket_sub_type)){
                            $BayesLolWs->runWithInfo($redis,$value,'refresh');
                        }
                    }
                }
            }
            $res = 'success';
        } else {
            $res = $BayesLolWs->runWithInfo($redis, $taskInfo,'socket');
        }
        return $res;
    }
    //正式处理
    public function runWithInfo($redis,$taskInfo,$run_type){
        $conversionRes = null;
        $gameId = 2;
        //获取元数据的数据源ID 拳头元数据
        $metadata_origin_id = 6;
        if ($run_type == 'socket'){
            $info = @json_decode($taskInfo['params'], true);
            $match_data = $info['match_data'];
            $socket_id = (Int)$info['socket_id'];
        }else{
            $info = $taskInfo;
            $match_data = @json_decode($taskInfo['match_data'], true);
            $socket_id = (Int)$info['id'];
        }
        $originId = (Int)@$info['origin_id'];
        $this->origin_id = $originId;
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        //判断是不是空
        if (empty($match_data)){
            return [];
        }
        //判断是不是回档
        $roll_back = false;
        if ($run_type == 'roll_back'){
            $roll_back = true;
        }
        //判断是不是正式数据
        if(!isset($match_data['seqIdx'])){
            return 'no seqIdx';
        }
        //原始数据解析
        $seqIdx = $match_data['seqIdx'];
        $payload_one = $match_data['payload'];
        $data_type = $payload_one['payload']['type'];
        $data_subject = $payload_one['payload']['subject'];
        $data_action = $payload_one['payload']['action'];
        $data_payload = $payload_one['payload']['payload'];
        $sourceUpdatedAt = @$payload_one['payload']['sourceUpdatedAt'];
        //$rel_battleId = @$data_payload['seriesStatus']['seriesCurrent'];
        $liveDataMatchUrn = $payload_one['payload']['liveDataMatchUrn'];
        $BattleArray = explode('-',$liveDataMatchUrn);
        $rel_battleId = @$BattleArray[1];
        //perId
        $perId = $payload_one['payload']['additionalProperties']['perId'];
        //根据数据源的比赛ID 获取平台的ID  在这之后就可以使用内存中的matchId了
        $matchId = $this->matchId;
        $rel_matchId = $this->rel_matchId;
        if (empty($matchId) || empty($rel_matchId)){
            $matchIdInfo = $this->getMatchIdAndRelMatchId($redis,$originId,$perId);
            $matchId = @$matchIdInfo['matchId'];
            $rel_matchId = @$matchIdInfo['rel_matchId'];
        }
        //判断有没有数据
        if (empty($matchId) || empty($rel_matchId)){
            return 'no-match';
        }
        if ($run_type == 'socket'){
            //判断是不是录入的socket sub type
            $socket_origin_data_type = @$match_data['payload']['type'];
            $match_socket_sub_type = $this->getMatchHotDataRunningInfo($redis,$matchId);
            $socket_sub_type_res = Common::getSocketSubTypeByOriginId($originId,$match_socket_sub_type);
            if ($socket_sub_type_res != $socket_origin_data_type){
                return 'socket-sub-type-error';
            }
        }
        $this->socket_id = $socket_id;
        $this->socket_time = $socket_time;
        //基础redisName
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId.":current:";
        //获取比赛数据
        $matchInfo = $this->getMatchInfo($redis,$matchId,$socket_time);
        if ($matchInfo == 'no_match_info'){
            return 'no_match_info';
        }
        //更改比赛状态 为进行中
        if ($this->operate_type != 'xiufu') {
            $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
            if ($match_status_check == 3) {
                return 'match is over';
            }
            $this->redis_hSet_And_Memory($redis, ['match_real_time_info'], 'status',2);
            //$this->setMatchStatus($redis, $matchId);
        }
        //获取battle和order
        $battleId = $battle_order = null;
        $battleInfoMemory = $this->battle_order_relation_id[$rel_battleId];
        if ($battleInfoMemory){
            $battle_res_explode = explode('-',$battleInfoMemory);
            $battleId = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }
        if (empty($battleId) || empty($battle_order)){
            //获取battle信息
            $isSetBattleStartTime = false;
            $battle_Info = self::getbattleIdByMatchIdAndOrderNew($redis,$redis_base_name,$matchId,$rel_matchId,$rel_battleId,$socket_time,0,$isSetBattleStartTime);
            if (empty($battle_Info['battle_id'])){
                return 'no-battleId';
            }
            $battleId = $battle_Info['battle_id'];
            $battle_order = $battle_Info['battle_order'];
            $this->battle_order_relation_id[$rel_battleId] = $battleId.'-'.$battle_order;
        }
        //查询时间是否已经处理过了
        $rel_event = $redis->hGet($redis_base_name."battles:".$battleId.":rel_events_list",$seqIdx);
        if ($rel_event){
            return $battleId.'-already-have:'.$seqIdx;
        }
        //保存
        $redis->hSet($redis_base_name."battles:".$battleId.":rel_events_list",$seqIdx,1);
        //设置 socket录入服务器配置
        $this->setSocketGameConfigure('lol');
        //查询是否已经设置了battle的第一针ID 暂时用于回档
        $battle_start_event_id = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'battle_start_event_id']);
        if (!$battle_start_event_id){
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'battle_start_event_id'],$socket_id);
        }
        $handle_data = [];
        //判断事件类型
        switch ($data_type){
            case 'INFO':
                //判断事件子类型
                if($data_subject == 'MATCH'){
                    //判断事件 action
                    //ANNOUNCE 只有一次
                    if ($data_action == 'ANNOUNCE'){
                        $this->conversionMatchAnnounce($redis,$matchId,$battleId,$data_payload,$matchInfo);
                    }
                    //UPDATE 做补充的玩家的英雄
                    if ($data_action == 'UPDATE'){
                        $this->conversionMatchUpdate($redis,$matchId,$battleId,$data_payload,$metadata_origin_id);
                    }
                    //ROLLBACK 回档  gameTime 回档到何时
                    if ($data_action == 'ROLLBACK'){
                        $handle_data = $this->conversionMatchRollBack($redis,$matchId,$battleId,$data_payload,$originId,$socket_id,$perId);
                    }
                }
                break;
            case 'GAME_EVENT':
                switch ($data_subject){
                    case 'MATCH':
                        switch ($data_action){
                            case 'ANNOUNCED_ANCIENT'://远古资源即将刷新公告
                                $handle_data = $this->conversionEliteAnnounced($redis,$matchId,$battleId,$data_payload,$socket_id);
                                break;
                            case 'SPAWNED_ANCIENT'://远古资源刷新公告
                                $handle_data = $this->conversionEliteSpawned($redis,$matchId,$battleId,$data_payload,$socket_id);
                                break;
                            case 'START_PAUSE':
                                $handle_data = $this->conversionStartPause($redis,$matchId,$battleId,$data_payload,$socket_id);
                                break;
                            case 'END_PAUSE':
                                $handle_data = $this->conversionEndPause($redis,$matchId,$battleId,$data_payload,$socket_id);
                                break;
                            case 'START_MAP':
                                $handle_data = $this->conversionBattleStart($redis,$matchId,$battleId,$data_payload,$socket_id,$sourceUpdatedAt);
                                break;
                            case 'END_MAP':
                                $handle_data = $this->conversionEndMap($redis,$matchInfo,$matchId,$battleId,$data_payload,$socket_id,$sourceUpdatedAt);
                                break;
                        }
                        break;
                    case 'PLAYER':
                        switch ($data_action){
                            case 'SELECTED_HERO'://选英雄
                                $handle_data = $this->conversionTeamPick($redis,$matchId,$battleId,$data_payload,$metadata_origin_id);
                                break;
                            case 'PURCHASED_ITEM'://购买道具
                                $this->conversionPlayerHandleItems($redis,$battleId,$data_payload,$metadata_origin_id);
                                break;
                            case 'SOLD_ITEM'://卖道具
                                break;
                            case 'UNDO_ITEM':
                                break;
                            case 'CONSUMED_ITEM'://消耗了道具
                                break;
                            case 'LEVEL_UP'://升级
                                break;
                            case 'PLACED_WARD'://插眼
                                $handle_data = $this->conversionPlayerPlacedWard($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                                break;
                            case 'KILLED_WARD'://排眼
                                $handle_data = $this->conversionPlayerKilledWard($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                                break;
                            case 'KILL'://击杀
                                $handle_data = $this->conversionPlayerKill($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                                break;
                            case 'SPECIAL_KILL'://最大多杀  ace  multi firstBlood
                                $this->conversionPlayerMultiKill($redis,$battleId,$data_payload);
                                break;
                            case 'DIED'://死亡
                                $this->conversionPlayerDied($redis,$battleId,$data_payload);
                                break;
                            case 'SPAWNED'://玩家复活
                                $handle_data = $this->conversionPlayerSpawned($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                                break;
                        }
                        break;
                    case 'TEAM':
                        //BANNED  HERO
                        //if ($data_action == 'BANNED_HERO'){}
                        if ($data_action == 'UPDATE_SCORE'){
                            //$this->conversionTeamScore($redis,$matchId,$battleId,$data_payload);
                        }
                        if ($data_action == 'KILLED_ANCIENT'){//击杀龙
                            $handle_data = $this->conversionDragonKill($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                        }
                        if ($data_action == 'TOOK_OBJECTIVE'){//推塔
                            $handle_data = $this->conversionBuildingKill($redis,$matchId,$battleId,$data_payload,$socket_id,$metadata_origin_id);
                        }
                        break;
                }
                break;
            case 'SNAPSHOT':
                if ($data_subject == 'TEAM'){
                    if ($data_action =='UPDATE'){//ban英雄
                        $handle_data = $this->bayesConversionBanPick($redis,$matchInfo,$battleId,$data_payload,$metadata_origin_id);
                    }
                }
                if ($data_subject == 'MATCH'){
                    if ($data_action =='UPDATE'){//frames
                        $handle_data = $this->conversionFrames($redis,$matchInfo,$battleId,$data_payload,$metadata_origin_id,$sourceUpdatedAt);
                    }
                }
                if ($data_subject == 'PLAYER'){
                    if ($data_action = 'UPDATE_POSITIONS'){//选手位置更新  并产生frames
                        $handle_data = $this->conversionPlayerPositions($redis,$matchId,$battleId,$data_payload,$metadata_origin_id);
                    }
                }
                break;
            case 'UNKNOWN':
                break;
        }
        //不正确返回
        if (empty($handle_data)){
            return $battleId.'no-data';
        }
        $is_battle_finished = false;
        $event_order = 0;
        if ($handle_data['type'] == 'events'){
            $socket_type = 'events';
            $event_type = $handle_data['event_data']['event_type'];
            $event_order = @$handle_data['event_order'];
            //事件的时间 单位 秒
            $ingame_timestamp = 0;
            $timestamp = @$data_payload['gameTime'];
            if ($timestamp){
                $ingame_timestamp = intval($timestamp/1000);
            }
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'ingame_timestamp' => (Int)$ingame_timestamp,
                'event_id' => (Int)$socket_id
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$handle_data['event_data']);
        }else{
            $is_battle_finished = $handle_data['event_data']['is_battle_finished'];
            if (!$is_battle_finished){
                $now_time = time();
                $out_frames_time_before = $this->redis_Get_Or_Memory($redis,['out_frames_time']);
                if ($out_frames_time_before){
                    $out_frames_time_cha = $now_time - $out_frames_time_before;
                    if ($out_frames_time_cha < 1){
                        return 'no-handle';
                    }
                }
                $this->redis_Set_And_Memory($redis,['out_frames_time'],$now_time);
            }else{
                //给redis 设置有效期
                $redis->expire($redis_base_name."battles:".$battleId.":ws_out_put_data",86400);
                $redis->expire($redis_base_name."battles:".$battleId.":event_list",86400);
            }
            $socket_type = 'frames';
            $event_type = 'frames';
            $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
            $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
            $number_of_games = $matchInfo['number_of_games'];
            //获取地图
            $map = $this->getMapInfoByRedisOrMemory($redis,1);
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'game_rules' => (String)$game_rules,
                'match_type' => (String)$match_type,
                'number_of_games' => (Int)$number_of_games,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'map' => $map
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$handle_data['event_data']);
        }
        if ($event_data_ws_res){
            //插入到redis
            $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
            if ($roll_back){
                return $battleId;
            }
            //保存 api
            $this->refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order);
            //如果是回档事件那么更新一下frame
            if($event_type == 'battle_reloaded'){
                //保存 api
                $this->refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,'frames',0);
            }
            //接下来存数据库
            $insertData = [
                'match_data' =>@json_encode($event_data_ws_res,320),
                'match_id' => $matchId,
                'game_id' => $gameId,
                'origin_id' => $originId,
                'type' => $socket_type,
                'battle_order' => $battle_order
            ];
            //当前数据库连接
//            $this->saveDataBase($insertData);
            $this->saveKafka($insertData);
        }
        return $battleId;
    }
    //处理所有的队伍ban
    private function bayesConversionBanPick($redis,$matchInfo,$battleId,$data_payload,$metadata_origin_id){
        $event_type = 'champion_banned';
        $teams_Info_Array = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        if (count($teams_Info_Array) < 2){
            $teams_Info_Array = $this->supplementTeamInfo($redis,$matchInfo,$battleId,$data_payload);
        }
        $playerInfoRedis = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        if (count($playerInfoRedis) < 10){
            $this->supplementTeamAndPlayersInfo($redis,$matchInfo,$battleId,$data_payload,$teams_Info_Array);
        }
        //获取英雄relation
        $bannedChampions = $data_payload['bannedChampions'];
        $relBannedChampions = array_column($bannedChampions, null, 'championId');
        $rel_ban_redis = $this->redis_hKeys_Or_Memory($redis,['current','battles',$battleId,'rel_ban']);
        if ($rel_ban_redis){
            $bannedChampionsIds_Array = array_column($bannedChampions, 'championId');
            $diff_array = array_diff($bannedChampionsIds_Array,$rel_ban_redis);
        }else{
            $diff_array = array_keys($relBannedChampions);
        }
        //没有变化
        if (empty($diff_array)){
            return null;
        }
        $championRelationIdList = $this->getChampionRelationIdList($redis,$metadata_origin_id);
        //首先获取ban pick的数量
        $ban_pick_len = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_len){
            $ban_pick_order = $ban_pick_len + 1;
        }else{
            $ban_pick_order = 1;
        }
        $team = null;
        $team_faction = null;
        $champions_id = null;
        $teams_info_ret = [];
        //获取到diff info 英雄ID
        foreach ($diff_array as $bc_key => $bc_championID){
            $bc_item = $relBannedChampions[$bc_championID];
            if ($bc_item){
                $rel_championID = $bc_item['championId'];
                //$champions_id = $redis->hGet('lol:champions:relation:'.$metadata_origin_id,$rel_championID);
                $champions_id = @$championRelationIdList[$rel_championID];
                if (!$champions_id){
                    //查询更为准确的信息（mysql查询）
                    $champions_id = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$rel_championID,$metadata_origin_id);
                }
                $rel_team_id = $bc_item['teamID'];
                $rel_team_urn = $bc_item['liveDataTeamUrn'];
                $teams_Info = @json_decode(@$teams_Info_Array[$rel_team_urn],true);
                $team_id = @$teams_Info['team_id'];
                $teamRes = [
                    "hero" => $champions_id,
                    "team" => $team_id,
                    "type" => 1,
                    "order" => $ban_pick_order
                ];
                if (empty($teams_Info['faction'])){
                    if ($rel_team_id == 100){
                        $team_faction = 'blue';
                        $team_another_faction = 'red';
                    }else{
                        $team_faction = 'red';
                        $team_another_faction = 'blue';
                    }
                    $teams_Info['faction'] = $team_faction;
                    //给另外一个队伍设置 阵营
                    $teams_Info_Another_urn = str_replace($rel_team_urn,'',implode(array_keys($teams_Info_Array)));
                    $teams_Info_Another = @$teams_Info_Array[$teams_Info_Another_urn];
                    if ($teams_Info_Another){
                        $teams_Info_Another_Array = @json_decode($teams_Info_Another,true);
                        $teams_Info_Another_Array['faction'] = $team_another_faction;
                        $teams_info_ret[$teams_Info_Another_urn] = @json_encode($teams_Info_Another_Array,320);
                    }
                    $teams_info_ret[$rel_team_urn] = @json_encode($teams_Info,320);
                    $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$teams_info_ret);
                }else{
                    $team_faction = $teams_Info['faction'];
                }
                //ban_pick
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_order,json_encode($teamRes,320));
                //rel_ban
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_ban'],$rel_championID,$rel_championID);
                $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_id);
                $team_info = @json_decode($match_team_info_redis,true);
                $team = [
                    'team_id' => self::checkAndChangeInt(@$teams_Info['team_id']),
                    'name' => self::checkAndChangeString(@$team_info['name']),
                    'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                    'opponent_order' => self::checkAndChangeInt(@$teams_Info['order'])
                ];
                $ban_pick_order += 1;
            }
        }
        //获取英雄信息
        $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id);
        //因为循环的原因 要减一
        $bp_order = $ban_pick_order - 1;
        $event_data = [
            'event_type' => (String)$event_type,
            'bp_order' => (Int)$bp_order,
            'faction' => (String)$team_faction,
            'team' => $team,
            'champion' => $champion
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data
        ];
        return $result;
    }
    //处理队伍pick 英雄
    private function conversionTeamPick($redis,$matchId,$battleId,$data_payload,$metadata_origin_id){
        $event_type = 'champion_picked';
        $rel_pick_championID = $data_payload['championId'];
        if ($rel_pick_championID < 1 ){
            return null;
        }
        $rel_pick_redis = $this->redis_hKeys_Or_Memory($redis,['current','battles',$battleId,'rel_pick']);
        if (in_array($rel_pick_championID,$rel_pick_redis)){
            return null;
        }
        $championRelationIdList = $this->getChampionRelationIdList($redis,$metadata_origin_id);
        //第几个了
        $ban_pick_len = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_len){
            $bp_order = $ban_pick_len + 1;
        }else{
            $bp_order = 1;
        }
        //获取平台英雄ID
        //$champions_id = $redis->hGet('lol:champions:relation:'.$metadata_origin_id,$rel_pick_championID);
        $champions_id = @$championRelationIdList[$rel_pick_championID];
        if (!$champions_id){
            $champions_id = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$rel_pick_championID,6);
        }
        $faction = null;
        $team = null;
        $rel_team_id = $data_payload['teamUrn'];
        $team_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_team_id);
        if ($team_info_redis){
            $team_info_battle = @json_decode($team_info_redis,true);
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_battle['team_id']);
            $team_info = @json_decode($match_team_info_redis,true);
            $team_id = @$team_info_battle['team_id'];
            $team = [
                'team_id' => self::checkAndChangeInt($team_id),
                'name' => self::checkAndChangeString(@$team_info['name']),
                'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                'opponent_order' => self::checkAndChangeInt(@$team_info_battle['order'])
            ];
            $faction = @$team_info_battle['faction'];
            //数组
            $teamRes = [
                "hero" => $champions_id,
                "team" => $team_id,
                "type" => 2,
                "order" => $bp_order
            ];
            //ban_pick
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$bp_order,json_encode($teamRes,320));
            //插入原始的
            //rel_pick
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_pick'],$rel_pick_championID,$rel_pick_championID);
        }
        //获取英雄信息
        $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id);
        $event_data = [
            'event_type' => (String)$event_type,
            'bp_order' => (Int)$bp_order,
            'faction' => (String)$faction,
            'team' => $team,
            'champion' => $champion
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data
        ];
        return $result;
    }
    //处理队伍的score
    private function conversionTeamScore($redis,$matchId,$battleId,$data_payload,$matchInfo){
        //队伍信息
        //推塔数
        $towersKilled = $data_payload['towersKilled'];
        //英雄击杀数
        $championsKilled = $data_payload['championsKilled'];
        //队伍经济
        $totalGold = $data_payload['totalGold'];
        //队伍ID
        $teamUrnId = $data_payload['teamUrn'];
        $teamInfo = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$teamUrnId);
        if ($teamInfo){
            $team_info = @json_decode($teamInfo,true);
            $team_info['kills'] = $championsKilled;
            $team_info['gold'] = $totalGold;
            $team_info['turret_kills'] = $towersKilled;
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$teamUrnId,@json_encode($team_info,320));
        }
        return true;
    }
    //处理第一针数据 队伍信息
    private function conversionMatchAnnounce($redis,$matchId,$battleId,$data_payload,$matchInfo){
        //首先查询出所有的玩家
        //$playerIdAndNameArray = $redis->hGetAll(Consts::MATCH_ALL_INFO.":".$matchId.":player_info");
        $player_order = 1;
        //获取队伍信息
        $turrets = [
            'top_outer_turret'=> ['is_alive' => true],
            'top_inner_turret'=> ['is_alive' => true],
            'top_inhibitor_turret'=> ['is_alive' => true],
            'mid_outer_turret'=> ['is_alive' => true],
            'mid_inner_turret'=> ['is_alive' => true],
            'mid_inhibitor_turret'=> ['is_alive' => true],
            'bot_outer_turret'=> ['is_alive' => true],
            'bot_inner_turret'=> ['is_alive' => true],
            'bot_inhibitor_turret'=> ['is_alive' => true],
            'top_nexus_turret'=> ['is_alive' => true],
            'bot_nexus_turret'=> ['is_alive' => true]
        ];
        $inhibitors = [
            'top_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ],
            'mid_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ],
            'bot_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ]
        ];
        $nexus = [
            'is_alive' => true
        ];
        $building_status = [
            'turrets' => $turrets,
            'inhibitors' => $inhibitors,
            'nexus' => $nexus
        ];
        $faction = null;
        $team_info_res = $playerRes = [];
        $team_1 = $matchInfo['team_1_id'];
        $team_2 = $matchInfo['team_2_id'];
        $teams = $data_payload['teams'];
        $per1 = $teams[0]['urn'];
        $per2 = $teams[1]['urn'];
        $matching_teams = HotBase::socketMatchingTeam($team_1,$team_2,$per1,$per2,$this->origin_id,2);
        //循环匹配
        foreach ($teams as $key => $item) {
            $team_urn_id = $item['urn'];
            //查询队伍ID
            $team_id = $matching_teams[$team_urn_id];
            //@self::getMainIdOfBayesByUrn('team', $this->origin_id, $team_urn_id, 2);
            //获取$team_order
            if(empty($team_id)){
                $team_order = null;
            }else{
                $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
            }
            //处理玩家
            $team_participants = $item['participants'];
            foreach ($team_participants as $key_p => $item_p) {
                $player_urn_id = $item_p['urn'];
                $player_rel_name_array = @explode(' ',$item_p['name']);//空格 分割
                array_shift($player_rel_name_array);
                $player_rel_name = implode(' ',$player_rel_name_array);
                //$player_rel_name = @$player_rel_name_array[1];
                $nick_name = $player_rel_name;
                $player_id = @self::getMainIdOfBayesByUrn('player',$this->origin_id, $player_urn_id, 2);
                if ($player_id) {
                    $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                    //$Player_Info = @json_decode(@$playerIdAndNameArray[$player_id],true);
                    $nick_name = @$Player_Info['nick_name'];
                }
                $player_seed = $key_p + 1;
                $role = Common::getLolPositionByString('bayes',$player_seed);
                $lane = Common::getLolLaneId('bayes',$role);
                $player_Info = [
                    'order' => $player_order,
                    'rel_identity_id' => $player_urn_id,
                    'rel_team_id' => $team_urn_id,
                    'team_order' => $team_order,
                    'seed' => $player_seed,
                    'battle_id' => $battleId,
                    'game' => 2,
                    'match' => $matchId,
                    'team_id' => $team_id,
                    'faction' => null,
                    'role' => $role,
                    'lane' => $lane,
                    'player_id' => $player_id,
                    'nick_name' => $nick_name,
                    'rel_nick_name' => $player_rel_name,
                    'champion' => null,
                    'turret_kills' => 0,
                    'inhibitor_kills' => 0,
                    'rift_herald_kills' => 0,
                    'dragon_kills' => 0,
                    'baron_nashor_kills' => 0,
                    'double_kill' => 0,
                    'triple_kill' => 0,
                    'quadra_kill' => 0,
                    'penta_kill' => 0,
                    'largest_multi_kill' => 0,
                    'largest_killing_spree' => 0,
                    'wards_purchased' => 0,
                    'respawntimer' => 0,
                    'died_time' => 0,
                    'abilities_timeline' => null,
                    'runes' => null,
                    'keystone' => null
                ];
                $playerRes[$player_urn_id] = @json_encode($player_Info, 320);
                $player_order ++;
            }
            $team_info = [
                'team_id' => $team_id,
                'order' => $team_order,
                'faction' => null,
                'score' => 0,
                'building_status' => $building_status,
                'dragon_kills_detail' => null,
                'rift_herald_kills' => 0,
                'rel_identity_id' => $team_urn_id,
                'identity_id' => $team_urn_id
            ];
            $team_info_res[$team_urn_id] = @json_encode($team_info, 320);
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_info_res);
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerRes);
        return true;
    }
    //处理没有第一针时候的队伍 增加队伍信息
    private function supplementTeamInfo($redis,$matchInfo,$battleId,$data_payload){
        //获取队伍信息
        $turrets = [
            'top_outer_turret'=> ['is_alive' => true],
            'top_inner_turret'=> ['is_alive' => true],
            'top_inhibitor_turret'=> ['is_alive' => true],
            'mid_outer_turret'=> ['is_alive' => true],
            'mid_inner_turret'=> ['is_alive' => true],
            'mid_inhibitor_turret'=> ['is_alive' => true],
            'bot_outer_turret'=> ['is_alive' => true],
            'bot_inner_turret'=> ['is_alive' => true],
            'bot_inhibitor_turret'=> ['is_alive' => true],
            'top_nexus_turret'=> ['is_alive' => true],
            'bot_nexus_turret'=> ['is_alive' => true]
        ];
        $inhibitors = [
            'top_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ],
            'mid_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ],
            'bot_inhibitor'=> [
                'is_alive' => true,
                'respawntimer' => null
            ]
        ];
        $nexus = [
            'is_alive' => true
        ];
        $building_status = [
            'turrets' => $turrets,
            'inhibitors' => $inhibitors,
            'nexus' => $nexus
        ];
        $team_info_res = [];
        $team_1 = $matchInfo['team_1_id'];
        $team_2 = $matchInfo['team_2_id'];
        $teamOne_liveDataTeamUrn = $data_payload['teamOne']['liveDataTeamUrn'];
        $teamTwo_liveDataTeamUrn = $data_payload['teamTwo']['liveDataTeamUrn'];
        $per1 = $teamOne_liveDataTeamUrn;
        $per2 = $teamTwo_liveDataTeamUrn;
        $matching_teams = HotBase::socketMatchingTeam($team_1,$team_2,$per1,$per2,$this->origin_id,2);
        $teams = [$teamOne_liveDataTeamUrn,$teamTwo_liveDataTeamUrn];
        foreach ($teams as $key => $item) {
            $team_urn_id = $item;
            //查询队伍ID  TODO
//            $team_id = @self::getMainIdOfBayesByUrn('team', $this->origin_id, $team_urn_id, 2);
//            //获取$team_order
//            $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
            //查询队伍ID
            $team_id = $matching_teams[$team_urn_id];
            //@self::getMainIdOfBayesByUrn('team', $this->origin_id, $team_urn_id, 2);
            //获取$team_order
            if (empty($team_id)) {
                $team_order = null;
            } else {
                $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
            }

            $team_info = [
                'team_id' => $team_id,
                'order' => $team_order,
                'faction' => null,
                'score' => 0,
                'building_status' => $building_status,
                'dragon_kills_detail' => null,
                'rift_herald_kills' => 0,
                'rel_identity_id' => $team_urn_id,
                'identity_id' => $team_urn_id
            ];
            $team_info_res[$team_urn_id] = @json_encode($team_info, 320);
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_info_res);
        return $team_info_res;
    }
    //处理没有第一针的时候数据
    private function supplementTeamAndPlayersInfo($redis,$matchInfo,$battleId,$data_payload,$teams_Info_Array){
        //首先查询出所有的玩家
        $teamOne_liveDataTeamUrn = $data_payload['teamOne']['liveDataTeamUrn'];
        $teamOne_players = $data_payload['teamOne']['players'];
        $teamTwo_liveDataTeamUrn = $data_payload['teamTwo']['liveDataTeamUrn'];
        $teamTwo_players = $data_payload['teamTwo']['players'];
        if (count($teamOne_players) < 5 || count($teamTwo_players) < 5){
            return false;
        }
        $team_rel_players = [
            $teamOne_liveDataTeamUrn => $teamOne_players,
            $teamTwo_liveDataTeamUrn => $teamTwo_players
        ];
        $matchId = @$matchInfo['id'];
        $player_order = 1;
        $playerRes = [];
        $team = [$teamOne_liveDataTeamUrn,$teamTwo_liveDataTeamUrn];
        foreach ($team as $key => $item) {
            $team_urn_id = $item;
            $teamInfo = @json_decode(@$teams_Info_Array[$team_urn_id],true);
            $team_order = @$teamInfo['order'];
            $team_id = @$teamInfo['team_id'];
            //处理玩家
            $team_players = $team_rel_players[$team_urn_id];
            foreach ($team_players as $key_p => $item_p) {
                $player_urn_id = $item_p['liveDataPlayerUrn'];
                $player_rel_name_array = @explode(' ',$item_p['summonerName']);//空格 分割
                array_shift($player_rel_name_array);
                $player_rel_name = implode(' ',$player_rel_name_array);
                $nick_name = $player_rel_name;
                $player_id = @self::getMainIdOfBayesByUrn('player',$this->origin_id, $player_urn_id, 2);
                if ($player_id) {
                    $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                    $nick_name = @$Player_Info['nick_name'];
                }
                $player_seed = $key_p + 1;
                $role = Common::getLolPositionByString('bayes',$player_seed);
                $lane = Common::getLolLaneId('bayes',$role);
                $player_Info = [
                    'order' => $player_order,
                    'rel_identity_id' => $player_urn_id,
                    'rel_team_id' => $team_urn_id,
                    'team_order' => $team_order,
                    'seed' => $player_seed,
                    'battle_id' => $battleId,
                    'game' => 2,
                    'match' => $matchId,
                    'team_id' => $team_id,
                    'faction' => null,
                    'role' => $role,
                    'lane' => $lane,
                    'player_id' => $player_id,
                    'nick_name' => $nick_name,
                    'rel_nick_name' => $player_rel_name,
                    'champion' => null,
                    'turret_kills' => 0,
                    'inhibitor_kills' => 0,
                    'rift_herald_kills' => 0,
                    'dragon_kills' => 0,
                    'baron_nashor_kills' => 0,
                    'double_kill' => 0,
                    'triple_kill' => 0,
                    'quadra_kill' => 0,
                    'penta_kill' => 0,
                    'largest_multi_kill' => 0,
                    'largest_killing_spree' => 0,
                    'wards_purchased' => 0,
                    'respawntimer' => 0,
                    'died_time' => 0,
                    'abilities_timeline' => null,
                    'runes' => null,
                    'keystone' => null
                ];
                $playerRes[$player_urn_id] = @json_encode($player_Info, 320);
                $player_order ++;
            }
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerRes);
        return $playerRes;
    }
    //做补充的玩家的英雄
    private function conversionMatchUpdate($redis,$matchId,$battleId,$data_payload,$metadata_origin_id){
        $playerInfoRedis = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        if(count($playerInfoRedis) < 10){
            return true;
        }
        $teams = $data_payload['teams'];
        foreach ($teams as $item){
            $rel_players_list = $item['participants'];
            foreach ($rel_players_list as $player_item){
                $player_urn = $player_item['urn'];
                $playerInfoRes = $playerInfoRedis[$player_urn];
                $playerInfo = @json_decode($playerInfoRes,true);
                //是不是需要补pick
                $playerInfo['champion'] = $this->getLolChampionId($redis,$metadata_origin_id,$player_item['championId']);
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$player_urn,@json_encode($playerInfo,320));
            }
        }
        return true;
    }
    //BattleStart
    private function conversionBattleStart($redis,$matchId,$battleId,$data_payload,$event_id,$sourceUpdatedAt){
        $map_id = 1;//可以做查询
        $begin_at_time = @date("Y-m-d H:i:s",strtotime($sourceUpdatedAt));
        $event_type = 'battle_start';
        $duration = intval($data_payload['gameTime']/1000);
        $battlesBase['game'] = 2;
        $battlesBase['match'] = $matchId;
        $battlesBase['status'] = 2;
        $battlesBase['begin_at'] = $begin_at_time;
        $battlesBase['is_draw'] = 2;
        $battlesBase['is_forfeit'] = 2;
        $battlesBase['is_default_advantage'] = 2;
        $battlesBase['map'] = $map_id;
        $battlesBase['duration'] = $duration;
        $battlesBase['is_battle_detailed'] = 1;
        $battlesBase['flag'] = 1;
        $battlesBase['deleted_at'] = null;
        $battlesBase['deleted'] = 2;
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battlesBase);
        //比赛开始 更改比赛的实际开始时间
        if ($this->operate_type != 'xiufu'){
            $match_isset_time_and_status = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'isset_time_and_status');
            if (!$match_isset_time_and_status){
                $match_real_time_info['begin_at'] = $begin_at_time;
                $match_real_time_info['status'] = 2;
                $match_real_time_info['isset_time_and_status'] = 'isset';
                $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
            }
        }
        //detail
        $battlesDetail = [
            'is_confirmed' => 1,
            'is_pause' => 2,
            'is_live' => 1
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$battlesDetail);
        //获取队伍信息
        $blue = null;
        $red = null;
        $teamsRedis = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        foreach ($teamsRedis as $key => $teamItem){
            $teamInfo = @json_decode($teamItem,true);
            if ($teamInfo['faction'] == 'blue'){
                $blue_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],@$teamInfo['team_id']);
                $blue_team_info = @json_decode($blue_team_info_redis,true);
                $blue = [
                    'team_id' => self::checkAndChangeInt(@$teamInfo['team_id']),
                    'name' => self::checkAndChangeString(@$blue_team_info['name']),
                    'image' => self::checkAndChangeString(@$blue_team_info['image_200x200']),
                    'opponent_order' => self::checkAndChangeInt(@$teamInfo['order'])
                ];
            }
            if ($teamInfo['faction'] == 'red'){
                $red_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],@$teamInfo['team_id']);
                $red_team_info = @json_decode($red_team_info_redis,true);
                $red = [
                    'team_id' => self::checkAndChangeInt(@$teamInfo['team_id']),
                    'name' => self::checkAndChangeString(@$red_team_info['name']),
                    'image' => self::checkAndChangeString(@$red_team_info['image_200x200']),
                    'opponent_order' => self::checkAndChangeInt(@$teamInfo['order'])
                ];
            }
        }
        //地图
        $map = $this->getMapInfoByRedisOrMemory($redis,$map_id);
        //获取事件的order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $duration,
            'position' => null,
            'killer' => (Int)$blue['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => (Int)$red['team_id'],
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type,
            'map' => $map,
            'blue' => $blue,
            'red' => $red,
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理龙刷新事件
    private function conversionEliteAnnounced($redis,$matchId,$battleId,$data_payload,$event_id){
        $event_type = 'elite_announced';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $elite = null;
        $spawn_time = intval($data_payload['spawnGameTime']/1000);
        $elite_id = null;
        $monsterType = $data_payload['monsterType'];
        $dragonType = $data_payload['dragonType'];
        switch ($monsterType){
            case 'dragon':
                switch ($dragonType){
                    case 'elder':
                        $elite_id = @Common::getEliteId('elder_dragon');
                        break;
                    case 'earth':
                        $elite_id = @Common::getEliteId('elite_mountain');
                        break;
                    case 'fire':
                        $elite_id = @Common::getEliteId('elite_infernal');
                        break;
                    case 'water':
                        $elite_id = @Common::getEliteId('elite_ocean');
                        break;
                    case 'air':
                        $elite_id = @Common::getEliteId('elite_cloud');
                        break;
                }
                break;
        }
        if ($elite_id){
            $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
            $drakeInfoRedis = @$drakeInfoList[$elite_id];
            $drakeInfo = @json_decode($drakeInfoRedis,true);
            $elite = [
                'ingame_obj_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                'ingame_obj_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn']),
                'ingame_obj_type' => self::checkAndChangeString(@$drakeInfo['ingame_obj_type']),
                'ingame_obj_sub_type' => self::checkAndChangeString(@$drakeInfo['sub_type']),
            ];
            //保存是哪只龙 即将刷新
            $elite_ancient_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'elite_ancient'],$elite_id);
            if ($elite_ancient_redis){
                $elite_ancient_info_old = @json_decode($elite_ancient_redis,true);
                $elite_ancient_new[] = '0-'.$spawn_time;
                $elite_ancient_info = array_merge($elite_ancient_info_old,$elite_ancient_new);
            }else{
                $elite_ancient_info[] = '0-'.$spawn_time;
            }
            $elite_ancient_res = @json_encode($elite_ancient_info,320);
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'elite_ancient'],$elite_id,$elite_ancient_res);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'elite_ancient_now_id'],$elite_id);
        }
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => $elite_id,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'spawn_time' => (string)$spawn_time,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type,
            'elite' => $elite,
            'spawn_time' => (Int)$spawn_time
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理龙刷新事件
    private function conversionEliteSpawned($redis,$matchId,$battleId,$data_payload,$event_id){
        $event_type = 'elite_spawned';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $elite = null;
        $elite_id = null;
        $monsterType = $data_payload['monsterType'];
        $dragonType = $data_payload['dragonType'];
        switch ($monsterType){
            case 'dragon':
                switch ($dragonType){
                    case 'elder':
                        $elite_id = @Common::getEliteId('elder_dragon');
                        break;
                    case 'earth':
                        $elite_id = @Common::getEliteId('elite_mountain');
                        break;
                    case 'fire':
                        $elite_id = @Common::getEliteId('elite_infernal');
                        break;
                    case 'water':
                        $elite_id = @Common::getEliteId('elite_ocean');
                        break;
                    case 'air':
                        $elite_id = @Common::getEliteId('elite_cloud');
                        break;
                }
                break;
        }
        if ($elite_id){
            $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
            $drakeInfoRedis = @$drakeInfoList[$elite_id];
            $drakeInfo = @json_decode($drakeInfoRedis,true);
            $elite = [
                'ingame_obj_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                'ingame_obj_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn']),
                'ingame_obj_type' => self::checkAndChangeString(@$drakeInfo['ingame_obj_type']),
                'ingame_obj_sub_type' => self::checkAndChangeString(@$drakeInfo['sub_type']),
            ];
            //保存是哪只龙 刷新了
            $elite_ancient_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'elite_ancient'],$elite_id);
            if ($elite_ancient_redis){
                $elite_ancient_array = @json_decode($elite_ancient_redis,true);
                $elite_ancient_keys = array_keys($elite_ancient_array);
                $elite_ancient_end = end($elite_ancient_keys);
                $elite_ancient_info = $elite_ancient_array[$elite_ancient_end];
                $elite_ancient_info_array = explode('-',$elite_ancient_info);
                $elite_ancient_info_array[0] = 1;
                $elite_ancient_info_res = implode('-',$elite_ancient_info_array);
                $elite_ancient_array[$elite_ancient_end] = $elite_ancient_info_res;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'elite_ancient'],$elite_id,@json_encode($elite_ancient_array,320));
                $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'elite_ancient_now_id'],$elite_id);
            }
        }
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => $elite_id,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type,
            'elite' => $elite
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理  玩家处理道具
    private function conversionPlayerHandleItems($redis,$battleId,$data_payload,$originId){
        //道具
        $rel_itemId = $data_payload['item'];
        $itemsInfoList = $this->getLolItemsList($redis,$originId);
        $LolItemsRelationList = $this->getLolItemsRelationList($redis,$originId);
        $itemInfoId = $LolItemsRelationList[$rel_itemId];
        if($itemInfoId){
            //获取原始数据
            $itemInfoResRedis = $itemsInfoList[$itemInfoId];
            if ($itemInfoResRedis) {
                $itemInfoRes = @json_decode($itemInfoResRedis, true);
                //如果买的是控制守卫
                if ($itemInfoRes['name'] == 'Control Ward' || $itemInfoRes['name'] == 'Stealth Ward'){
                    //人物
                    $playerUrn = $data_payload['playerUrn'];
                    $playerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrn);
                    if ($playerInfoRedis){
                        $playerInfo = @json_decode($playerInfoRedis,true);
                        $playerInfo['wards_purchased'] = (Int)$playerInfo['wards_purchased'] + 1;
                        $playerInfoRes = @json_encode($playerInfo,320);
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerUrn,$playerInfoRes);
                    }
                }
            }
        }
        return true;
    }
    //处理杀人事件
    private function conversionPlayerKill($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $event_type = 'player_kill';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        //原始数据
        $killer_rel_order_id = $data_payload['killerUrn'];
        $victim_rel_order_id = $data_payload['victimUrn'];
        //计算最大连杀
        $this->conversionPlayerLargestKillingSpree($redis,$battleId,$killer_rel_order_id,$victim_rel_order_id);
        //击杀者
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $first_event_team_order = null;
        $killer_player_info = [];
        if (empty($killer_rel_order_id)){//自杀
            $event_type = 'player_suicide';
            $killer = 51;
            $killer_player_name = null;
            $killer_player_rel_name = 'unknown';
            $killer_champion_id = null;
            $killer_faction = null;
            $killer_ingame_obj_type = 51;
            $killer_sub_type = 51;
            //websocket
            $websocket_killer = [
                'ingame_obj_name' => 'unknown',
                'ingame_obj_name_cn' => '未知',
                'ingame_obj_type' => 'unknown',
                'ingame_obj_sub_type' => 'unknown'
            ];
        }else{
            $killer_ingame_obj_type = $killer_sub_type = 50;
            //查询
            $killer_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
            if ($killer_player_info_redis){
                $killer_player_info = @json_decode($killer_player_info_redis,true);
                $killer = @$killer_player_info['player_id'];
                $killer_player_name = @$killer_player_info['nick_name'];
                $killer_player_rel_name = @$killer_player_info['rel_nick_name'];
                $killer_champion_id = @$killer_player_info['champion'];
                $killer_faction = @$killer_player_info['faction'];
                $first_event_team_order = @$killer_player_info['team_order'];
            }
            //这里处理killer和victim 和assist  方便存detail 和socket
            //获取killer英雄信息
            $killer_championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
            //killer
            $websocket_killer = [
                'player_id' => self::checkAndChangeInt($killer),
                'nick_name' => self::checkAndChangeString($killer_player_name),
                'faction' => self::checkAndChangeString($killer_faction),
                'champion' => $killer_championRes,
                'ingame_obj_type' => (string)'player',
                'ingame_obj_sub_type' => (string)'player'
            ];
        }
        //受害者 victim
        $victim = $victim_player_name = $victim_player_rel_name = $victim_champion_id = $victim_faction = null;
        //查询
        $victim_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$victim_rel_order_id);
        if ($victim_player_info_redis){
            $victim_player_info = @json_decode($victim_player_info_redis,true);
            $victim = @$victim_player_info['player_id'];
            $victim_player_name = @$victim_player_info['nick_name'];
            $victim_player_rel_name = @$victim_player_info['rel_nick_name'];
            $victim_champion_id = @$victim_player_info['champion'];
            $victim_faction = @$victim_player_info['faction'];
        }
        //$assist 助攻
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        $assist_rel_list = $data_payload['assistants'];
        if (count($assist_rel_list) > 0){
            $assist = [];
            foreach ($assist_rel_list as $a_key =>$a_val){
                $assist_player_info_array = [];
                $assist_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$a_val);
                if ($assist_player_info_redis){
                    $assist_player_info = @json_decode($assist_player_info_redis,true);
                    $assist_player_info_array['player_id'] = @$assist_player_info['player_id'];
                    $assist_player_info_array['player_nick_name'] = @$assist_player_info['nick_name'];
                    $assist_player_info_array['assist_player_name'] = @$assist_player_info['rel_nick_name'];
                    $assist_player_info_array['faction'] = @$assist_player_info['faction'];
                    $assist_player_info_array['champion_id'] = @$assist_player_info['champion'];
                }
                $assist[] = $assist_player_info_array;
            }
            $assist_ingame_obj_type = 50;
            $assist_sub_type = 50;
        }
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //获取victim英雄信息
        $victim_championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$victim_champion_id);
        //victim
        $websocket_victim = [
            'player_id' => (Int)$victim,
            'nick_name' => self::checkAndChangeString($victim_player_name),
            'faction' => self::checkAndChangeString($victim_faction),
            'champion' => $victim_championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //$websocket_assist
        $websocket_assist = null;
        if ($assist){
            foreach ($assist as $ws_a_key => $ws_a_item){
                $websocket_assist_info = [];
                $assist_champion_info = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$ws_a_item['champion_id']);
                $websocket_assist_info['player_id'] = self::checkAndChangeInt($ws_a_item['player_id']);
                $websocket_assist_info['nick_name'] = self::checkAndChangeString($ws_a_item['player_nick_name']);
                $websocket_assist_info['faction'] = self::checkAndChangeString($ws_a_item['faction']);
                $websocket_assist_info['champion'] = $assist_champion_info;
                $websocket_assist_info['ingame_obj_type'] = (string)'player';
                $websocket_assist_info['ingame_obj_sub_type'] = (string)'player';
                $websocket_assist[] = $websocket_assist_info;
            }
        }
        //获取第一次事件
        $killed_type = 'player';
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $is_first_event = false;
        if (!empty($killer_rel_order_id)) {//非自杀
            $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
            $is_first_event = $first_event_Info['is_first_event'];//true false
            if ($is_first_event) {
                $is_first_event_res = 1;
                $first_event_type = $first_event_Info['first_event_type'];
                $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
                //保存一血 五杀 十杀
                if ($first_event_type == 'first_blood') {
                    $first_blood_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_blood_p_tid' => $first_event_team_order,
                        'first_blood_time' => $ingame_timestamp,
                        'first_blood_detail' => @json_encode($first_blood_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
                if ($first_event_type == 'first_to_5_kills') {
                    $first_to_5_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_to_5_kills_p_tid' => $first_event_team_order,
                        'first_to_5_kills_time' => $ingame_timestamp,
                        'first_to_5_detail' => @json_encode($first_to_5_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
                if ($first_event_type == 'first_to_10_kills') {
                    $first_to_10_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_to_10_kills_p_tid' => $first_event_team_order,
                        'first_to_10_kills_time' => $ingame_timestamp,
                        'first_to_10_detail' => @json_encode($first_to_10_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => $victim,
            'victim_player_name' => $victim_player_name,
            'victim_player_rel_name' => $victim_player_rel_name,
            'victim_champion_id' => $victim_champion_id,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => 50,
            'victim_sub_type' => 50,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理玩家死亡 复活时间
    private function conversionPlayerDied($redis,$battleId,$data_payload){
        $respawnTime = $data_payload['respawnTime'];
        $playerUrn = $data_payload['playerUrn'];
        $died_time = intval($data_payload['gameTime']/1000);
        //获取玩家
        $playerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrn);
        if ($playerInfoRedis){
            $playerInfo = @json_decode($playerInfoRedis,true);
            if ($playerInfo){
                $playerInfo['respawntimer'] = intval($respawnTime/1000);
                $playerInfo['died_time'] = $died_time;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerUrn,@json_encode($playerInfo,320));
            }
        }
        return true;
    }
    //玩家复活
    private function conversionPlayerSpawned($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $event_type = 'player_spawned';
        $player_ws = null;
        $killer = null;
        $player_id = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = $killer_ingame_obj_type = $killer_sub_type = null;
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        $playerUrn = $data_payload['playerUrn'];
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrn);
        if ($player_info_redis) {
            $killer_ingame_obj_type = 50;
            $killer_sub_type = 50;
            $player_info = @json_decode($player_info_redis, true);
            $player_id = @$player_info['player_id'];
            $killer_player_name = @$player_info['nick_name'];
            $killer_player_rel_name = @$player_info['rel_nick_name'];
            $killer_faction = @$player_info['faction'];
            $champion_id = @$player_info['champion'];
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champion_id);
            $killer_champion_id = $champion_id;
            $player_ws = [
                'player_id' => self::checkAndChangeInt($player_id),
                'nick_name' => self::checkAndChangeString(@$player_info['nick_name']),
                'faction' => self::checkAndChangeString(@$player_info['faction']),
                'champion' => $champion,
                'ingame_obj_type' => 'player',
                'ingame_obj_sub_type' => 'player'
            ];
            //保存选手复活时间为0
            $player_info['respawntimer'] = 0;
            $player_info['died_time'] = 0;
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerUrn,@json_encode($player_info,320));
            $killer = self::checkAndChangeInt($player_id);
        }
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'player' => $player_ws
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //玩家插眼
    private function conversionPlayerPlacedWard($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $event_type = 'ward_placed';
        $owner_ws = null;
        $player_id = null;
        $killer = null;
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        $playerUrn = $data_payload['placerUrn'];
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrn);
        if ($player_info_redis) {
            $player_info = @json_decode($player_info_redis, true);
            $player_id = @$player_info['player_id'];
            $champion_id = @$player_info['champion'];
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champion_id);
            $owner_ws = [
                'player_id' => self::checkAndChangeInt($player_id),
                'nick_name' => self::checkAndChangeString(@$player_info['nick_name']),
                'faction' => self::checkAndChangeString(@$player_info['faction']),
                'champion' => $champion,
                'ingame_obj_type' => 'player',
                'ingame_obj_sub_type' => 'player'
            ];
            $killer = self::checkAndChangeInt($player_id);
        }
        $rel_ward_type = $data_payload['wardType'];
        $ward_type = Common::getWardType(Consts::ORIGIN_BAYES,$rel_ward_type);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        $other_info = [
            'ward_type' => $ward_type,
            'owner' => $owner_ws
        ];
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'other_info' => $other_info,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'ward_type' => $ward_type,
            'owner' => $owner_ws
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //玩家排眼
    private function conversionPlayerKilledWard($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $event_type = 'ward_kill';
        $owner_ws = null;
        $websocket_killer = null;
        $killer_player_id = null;
        $killer = null;
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        $playerUrn = $data_payload['placerUrn'];
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrn);
        if ($player_info_redis) {
            $player_info = @json_decode($player_info_redis, true);
            $player_id = @$player_info['player_id'];
            $champion_id = @$player_info['champion'];
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champion_id);
            $owner_ws = [
                'player_id' => self::checkAndChangeInt($player_id),
                'nick_name' => self::checkAndChangeString(@$player_info['nick_name']),
                'faction' => self::checkAndChangeString(@$player_info['faction']),
                'champion' => $champion,
                'ingame_obj_type' => 'player',
                'ingame_obj_sub_type' => 'player'
            ];
        }
        //排眼者
        $killerUrn = $data_payload['killerUrn'];
        $killer_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killerUrn);
        if ($killer_player_info_redis) {
            $killer_player_info = @json_decode($killer_player_info_redis, true);
            $killer_player_id = @$killer_player_info['player_id'];
            $killer_champion_id = @$killer_player_info['champion'];
            $killer_champion = $this->getChampionInfoByRedisOrMemory($redis, $metadata_origin_id, $killer_champion_id);
            $websocket_killer = [
                'player_id' => self::checkAndChangeInt($killer_player_id),
                'nick_name' => self::checkAndChangeString(@$killer_player_info['nick_name']),
                'faction' => self::checkAndChangeString(@$killer_player_info['faction']),
                'champion' => $killer_champion,
                'ingame_obj_type' => (string)'player',
                'ingame_obj_sub_type' => (string)'player'
            ];
            $killer = self::checkAndChangeInt($killer_player_id);
        }
        $rel_ward_type = $data_payload['wardType'];
        $ward_type = Common::getWardType(Consts::ORIGIN_BAYES,$rel_ward_type);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        $other_info = [
            'ward_type' => $ward_type,
            'killer' => $websocket_killer,
            'owner' => $owner_ws
        ];
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'other_info' => $other_info,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'ward_type' => $ward_type,
            'killer' => $websocket_killer,
            'owner' => $owner_ws
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理杀龙信息
    public function conversionDragonKill($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $drake_id = null;
        $event_type = 'elite_kill';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        //击杀者
        $killer_player_info_array = [];
        $killerTeamUrn = $data_payload['killerTeamUrn'];
        $killer_rel_order_id = $data_payload['killerUrn'];
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $first_event_team_order = null;
        //查询
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
        if ($player_info_redis){
            $player_info = @json_decode($player_info_redis,true);
            $killer_player_info_array = $player_info;
            $killer = @$player_info['player_id'];
            $killer_player_name = @$player_info['nick_name'];
            $killer_player_rel_name = @$player_info['rel_nick_name'];
            $killer_champion_id = @$player_info['champion'];
            $killer_faction = @$player_info['faction'];
            $first_event_team_order = @$player_info['team_order'];
        }
        //被杀者
        $killed_type = null;
        $victim = null;
        $monsterType = $data_payload['monsterType'];
        $dragonType = $data_payload['dragonType'];
        switch ($monsterType){
            case 'dragon':
                switch ($dragonType){
                    case 'elder':
                        $killed_type = 'elder_dragon';
                        $victim = @Common::getEliteId('elder_dragon');
                        $drake_id = $victim;
                        break;
                    case 'water':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_ocean');
                        $drake_id = $victim;
                        break;
                    case 'fire':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_infernal');
                        $drake_id = $victim;
                        break;
                    case 'earth':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_mountain');
                        $drake_id = $victim;
                        break;
                    case 'air':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_cloud');
                        $drake_id = $victim;
                        break;
                }
                //记录当前这个人的杀龙信息
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['dragon_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    if ($killed_type == 'elder_dragon'){
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'elder_dragon_kill_time',$ingame_timestamp);
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'kill_time',$ingame_timestamp);
                    }
                }
                break;
            case 'riftHerald':
                $killed_type = 'rift_herald';
                $victim = @Common::getEliteId('rift_herald');
                //保存队伍击杀
                $team_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$killerTeamUrn);
                if ($team_info_redis){
                    $team_info = @json_decode($team_info_redis,true);
                    if (@$team_info['rift_herald_kills']){
                        (int)$team_info['rift_herald_kills'] += 1;
                    }else{
                        $team_info['rift_herald_kills'] = 1;
                    }
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$killerTeamUrn,@json_encode($team_info,320));
                }
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['rift_herald_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //记录 击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time',$ingame_timestamp);
                    $rift_herald_kill_num = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num');
                    if ($rift_herald_kill_num == 1){
                        $rift_herald_kill_num += 1;
                        $enum_socket_game_configure_res = $this->enum_socket_game_configure;
                        if ($rift_herald_kill_num == $enum_socket_game_configure_res['rift_herald_max_num']){
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',2);
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
                        }
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',1);
                    }
                }
                break;
            case 'baron':
                $killed_type = 'baron_nashor';
                $victim = @Common::getEliteId('baron_nashor');
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['baron_nashor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time',$ingame_timestamp);
                }
                break;
        }
        if (empty($victim)){
            return null;
        }
        //$assist 助攻
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        $assist_rel_list = $data_payload['assistants'];
        if (count($assist_rel_list) > 0){
            $assist = [];
            foreach ($assist_rel_list as $a_key =>$a_val){
                $assist_player_info_array = [];
                $assist_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$a_val);
                if ($assist_player_info_redis){
                    $assist_player_info = @json_decode($assist_player_info_redis,true);
                    $assist_player_info_array['player_id'] = @$assist_player_info['player_id'];
                    $assist_player_info_array['player_nick_name'] = @$assist_player_info['nick_name'];
                    $assist_player_info_array['assist_player_name'] = @$assist_player_info['rel_nick_name'];
                    $assist_player_info_array['faction'] = @$assist_player_info['faction'];
                    $assist_player_info_array['champion_id'] = @$assist_player_info['champion'];
                }
                $assist[] = $assist_player_info_array;
            }
            $assist_ingame_obj_type = 50;
            $assist_sub_type = 50;
        }
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //这里处理killer和victim 和assist  方便存detail 和socket
        //获取英雄信息
        $championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
        //killer
        $websocket_killer = [
            'player_id' => self::checkAndChangeInt($killer),
            'nick_name' => (string)$killer_player_name,
            'faction' => (string)$killer_faction,
            'champion' => $championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //victim
        $victim_ingame_obj_name = $victim_ingame_obj_name_cn = $victim_ingame_obj_type = $victim_ingame_obj_sub_type = null;
        if ($victim){
            $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
            $victimInfoRedis = $drakeInfoList[$victim];
            $victimInfo = @json_decode($victimInfoRedis,true);
            $victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        //$websocket_assist
        $websocket_assist = null;
        if ($assist){
            foreach ($assist as $ws_a_key => $ws_a_item){
                $websocket_assist_info = [];
                $assist_champion_info = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$ws_a_item['champion_id']);
                $websocket_assist_info['player_id'] = self::checkAndChangeInt($ws_a_item['player_id']);
                $websocket_assist_info['nick_name'] = self::checkAndChangeString($ws_a_item['player_nick_name']);
                $websocket_assist_info['faction'] = self::checkAndChangeString($ws_a_item['faction']);
                $websocket_assist_info['champion'] = $assist_champion_info;
                $websocket_assist_info['ingame_obj_type'] = (string)'player';
                $websocket_assist_info['ingame_obj_sub_type'] = (string)'player';
                $websocket_assist[] = $websocket_assist_info;
            }
        }
        $websocket_victim = [
            'ingame_obj_name' => $victim_ingame_obj_name,
            'ingame_obj_name_cn' => $victim_ingame_obj_name_cn,
            'ingame_obj_type' => $victim_ingame_obj_type,
            'ingame_obj_sub_type' => $victim_ingame_obj_sub_type
        ];
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = @Common::getPandaScoreEventNum($first_event_type);
            //第一次事件 保存内容
            if ($first_event_type == 'first_rift_herald'){
                $first_rift_herald_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_rift_herald_p_tid' => $first_event_team_order,
                    'first_rift_herald_time' => $ingame_timestamp,
                    'first_rift_herald_detail' => @json_encode($first_rift_herald_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_dragon'){
                $first_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_dragon_p_tid' => $first_event_team_order,
                    'first_dragon_time' => $ingame_timestamp,
                    'first_dragon_detail' =>json_encode($first_dragon_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_baron_nashor'){
                $first_baron_nashor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_baron_nashor_p_tid' => $first_event_team_order,
                    'first_baron_nashor_time' => $ingame_timestamp,
                    'first_baron_nashor_detail' => @json_encode($first_baron_nashor_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_elder_dragon'){
                $first_elder_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_elder_dragon_p_tid' => $first_event_team_order,
                    'first_elder_dragon_time' => $ingame_timestamp,
                    'first_elder_dragon_detail' => @json_encode($first_elder_dragon_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => 50,
            'killer_sub_type' => 50,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => $victim,
            'victim_sub_type' => $victim,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //设置队伍杀龙信息
        if ($drake_id){
            //组成二维数组
            $dragon_kills_detail_now[] = [
                'date'=>$ingame_timestamp,
                'drake'=>(string)$drake_id
            ];
            $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$killerTeamUrn);
            if ($teamInfoRedis) {
                $teamInfo = @json_decode($teamInfoRedis, true);
                $dragon_kills_detail = $teamInfo['dragon_kills_detail'];
                if ($dragon_kills_detail){
                    $teamInfo['dragon_kills_detail'] = array_merge($dragon_kills_detail,$dragon_kills_detail_now);
                }else{
                    $teamInfo['dragon_kills_detail'] = $dragon_kills_detail_now;
                }
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$killerTeamUrn,@json_encode($teamInfo,320));
            }
        }
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理推塔信息
    public function conversionBuildingKill($redis,$matchId,$battleId,$data_payload,$event_id,$metadata_origin_id){
        $event_type = 'building_kill';
        $building_lane = null;
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $position = @implode(',',$data_payload['position']);
        //击杀者
        $is_player = 0;//击杀者是不是玩家
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $killer_ingame_obj_type = $killer_sub_type = null;
        $first_event_team_order = null;
        $killer_player_info_array = [];
        $killer_rel_order_id = $data_payload['killerUrn'];//killerTeamUrn
        if (!empty($killer_rel_order_id)){
            $is_player = 1;
            $killer_ingame_obj_type = 50;
            $killer_sub_type = 50;
            $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
            if ($player_info_redis){
                $player_info = @json_decode($player_info_redis,true);
                $killer_player_info_array = $player_info;
                $killer = @$player_info['player_id'];
                $killer_player_name = @$player_info['nick_name'];
                $killer_player_rel_name = @$player_info['rel_nick_name'];
                $killer_champion_id = @$player_info['champion'];
                $killer_faction = @$player_info['faction'];
                $first_event_team_order = @$player_info['team_order'];
            }
        }else{
            $killer = 51;
            $killer_ingame_obj_type = $killer;
            $killer_sub_type = $killer;
        }
        //塔的阵营
        $victim_faction = $websocket_victim_faction = null;
        $rel_teamId = $data_payload['buildingTeamUrn'];//塔的阵营
        //获取队伍信息
        $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_teamId);
        if ($teamInfoRedis){
            $teamInfo = @json_decode($teamInfoRedis,true);
            $victim_faction = @$teamInfo['faction'];
            $websocket_victim_faction = @$teamInfo['faction'];
            $victim_team_order = @$teamInfo['order'];
            //判断killer 是不是unknown
            if (empty($killer_rel_order_id)){
                if ($victim_team_order == 1){
                    $first_event_team_order = 2;
                }
                if ($victim_team_order == 2){
                    $first_event_team_order = 1;
                }
            }
        }
        //被杀者
        $killed_type = null;
        $victim = $victim_ingame_obj_type = $victim_sub_type = $victim_lane = $victim_ingame_obj_name = $victim_ingame_obj_name_cn = $victim_ingame_obj_sub_type = null;
        //原始数据
        $buildingType = $data_payload['buildingType'];
        $laneType = $data_payload['lane'];
        $towerType = $data_payload['turretTier'];
        switch ($buildingType){
            case 'turret':
                $killed_type = 'tower';
                switch ($laneType){
                    case 'bot':
                        switch ($towerType){
                            case 'outer':
                                $victim = 17;
                                $building_lane = 'bot_outer_turret';
                                break;
                            case 'inner':
                                $victim = 18;
                                $building_lane = 'bot_inner_turret';
                                break;
                            case 'base':
                                $victim = 19;
                                $building_lane = 'bot_inhibitor_turret';
                                break;
                        }
                        break;
                    case 'top':
                        switch ($towerType){
                            case 'outer':
                                $victim = 10;
                                $building_lane = 'top_outer_turret';
                                break;
                            case 'inner':
                                $victim = 11;
                                $building_lane = 'top_inner_turret';
                                break;
                            case 'base':
                                $victim = 12;
                                $building_lane = 'top_inhibitor_turret';
                                break;
                        }
                        break;
                    case 'mid':
                        switch ($towerType){
                            case 'outer':
                                $victim = 14;
                                $building_lane = 'mid_outer_turret';
                                break;
                            case 'inner':
                                $victim = 15;
                                $building_lane = 'mid_inner_turret';
                                break;
                            case 'base':
                                $victim = 16;
                                $building_lane = 'mid_inhibitor_turret';
                                break;
                            case 'nexus'://基地塔  平台分为上路基地塔和下路基地塔 13 ，20   蓝色方：上路1748,2270  下路2177,1807  红方：下路13052,12612  上路12611,13084
                                if ($position == '1748,2270' || $position =='12611,13084'){//上路
                                    $victim = 13;
                                    $building_lane = 'top_nexus_turret';
                                }
                                if ($position == '2177,1807' || $position =='13052,12612'){//上路
                                    $victim = 20;
                                    $building_lane = 'bot_nexus_turret';
                                }
                                break;
                        }
                        break;
                }
                //记录当前这个人的摧毁防御塔信息
                if ($is_player == 1 && $killer_player_info_array){
                    (int)$killer_player_info_array['turret_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                }
                break;
            case 'inhibitor':
                $killed_type = 'inhibitor';
                switch ($laneType){
                    case 'top':
                        $victim = 22;
                        $building_lane = 'top_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_top_inhibitor_kill_time',$ingame_timestamp);
                        break;
                    case 'mid':
                        $victim = 23;
                        $building_lane = 'mid_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_mid_inhibitor_kill_time',$ingame_timestamp);
                        break;
                    case 'bot':
                        $victim = 24;
                        $building_lane = 'bot_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_bot_inhibitor_kill_time',$ingame_timestamp);
                        break;
                }
                //记录当前这个人的摧毁水晶信息
                if ($is_player == 1 && $killer_player_info_array){
                    (int)$killer_player_info_array['inhibitor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                }
                break;
            default:
                $victim = null;
                break;
        }
        if (empty($victim)){
            return null;
        }
        $victim_ingame_obj_type = $victim;
        $victim_sub_type = $victim;
        //$assist 助攻
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        $assist_rel_list = $data_payload['assistingParticipantIds'];
        if (count($assist_rel_list) > 0){
            $assist = [];
            foreach ($assist_rel_list as $a_key =>$a_val){
                $assist_player_info_array = [];
                $assist_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$a_val);
                if ($assist_player_info_redis){
                    $assist_player_info = @json_decode($assist_player_info_redis,true);
                    $assist_player_info_array['player_id'] = @$assist_player_info['player_id'];
                    $assist_player_info_array['player_nick_name'] = @$assist_player_info['nick_name'];
                    $assist_player_info_array['assist_player_name'] = @$assist_player_info['rel_nick_name'];
                    $assist_player_info_array['faction'] = @$assist_player_info['faction'];
                    $assist_player_info_array['champion_id'] = @$assist_player_info['champion'];
                }
                $assist[] = $assist_player_info_array;
            }
            $assist_ingame_obj_type = 50;
            $assist_sub_type = 50;
        }
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //这里处理killer和victim 和assist  方便存detail 和socket
        //判断是不是玩家
        if ($is_player){
            //获取英雄信息
            $championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
            //killer
            $websocket_killer = [
                'player_id' => self::checkAndChangeInt($killer),
                'nick_name' => self::checkAndChangeString($killer_player_name),
                'faction' => self::checkAndChangeString($killer_faction),
                'champion' => $championRes,
                'ingame_obj_type' => (string)'player',
                'ingame_obj_sub_type' => (string)'player'
            ];
        }else{
            $websocket_killer = [
                'ingame_obj_name' => 'unknown',
                'ingame_obj_name_cn' => '未知',
                'ingame_obj_type' => 'unknown',
                'ingame_obj_sub_type' => 'unknown'
            ];
        }
        //victim 在上面计算出来的 再通过victim的ID 获取数据
        $websocket_victim_lane = $websocket_victim_ingame_obj_name = $websocket_victim_ingame_obj_name_cn = $websocket_victim_ingame_obj_type = $websocket_victim_ingame_obj_sub_type = null;
        $buildingInfoList = $this->getEnumIngameGoalInfo($redis);
        if ($victim){
            $victimInfo = @json_decode($buildingInfoList[$victim],true);
            $websocket_victim_lane = self::checkAndChangeString(@$victimInfo['lane']);
            $websocket_victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $websocket_victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $websocket_victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $websocket_victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        $websocket_victim = [
            'lane' => $websocket_victim_lane,
            'ingame_obj_name' => $websocket_victim_ingame_obj_name,
            'ingame_obj_name_cn' => $websocket_victim_ingame_obj_name_cn,
            'faction' => $websocket_victim_faction,
            'ingame_obj_type' => $websocket_victim_ingame_obj_type,
            'ingame_obj_sub_type' => $websocket_victim_ingame_obj_sub_type
        ];
        //$websocket_assist
        $websocket_assist = [];
        if ($assist){
            foreach ($assist as $ws_a_key => $ws_a_item){
                $websocket_assist_info = [];
                //获取英雄信息
                $assist_champion_info = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$ws_a_item['champion_id']);
                $websocket_assist_info['player_id'] = self::checkAndChangeInt($ws_a_item['player_id']);
                $websocket_assist_info['nick_name'] = self::checkAndChangeString(@$ws_a_item['player_nick_name']);
                $websocket_assist_info['faction'] = self::checkAndChangeString(@$ws_a_item['faction']);
                $websocket_assist_info['champion'] = $assist_champion_info;
                $websocket_assist_info['ingame_obj_type'] = (string)'player';
                $websocket_assist_info['ingame_obj_sub_type'] = (string)'player';
                $websocket_assist[] = $websocket_assist_info;
            }
        }
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
            //保存首塔 首水晶
            if ($first_event_type == 'first_turret'){
                $first_turret_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_turret_p_tid' => $first_event_team_order,
                    'first_turret_time' => $ingame_timestamp,
                    'first_turret_detail' => @json_encode($first_turret_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_inhibitor'){
                $first_inhibitor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_inhibitor_p_tid' => $first_event_team_order,
                    'first_inhibitor_time' => $ingame_timestamp,
                    'first_inhibitor_detail' => @json_encode($first_inhibitor_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => $victim_ingame_obj_type,
            'victim_sub_type' => $victim_sub_type,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //设置队伍的塔信息
        if ($killed_type && $building_lane){
            //$rel_teamId
            $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_teamId);
            if ($teamInfoRedis) {
                $teamInfo = @json_decode($teamInfoRedis, true);
                $team_building_status = @$teamInfo['building_status'];
                if ($killed_type == 'tower'){
                    $team_building_status['turrets'][$building_lane]['is_alive'] = false;
                }
                if ($killed_type == 'inhibitor'){
                    $team_building_status['inhibitors'][$building_lane]['is_alive'] = false;
                }
                $teamInfo['building_status'] = $team_building_status;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$rel_teamId,@json_encode($teamInfo,320));
            }
        }
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理frames
    public function conversionFrames($redis,$matchInfo,$battleId,$data_payload,$metadata_origin_id,$sourceUpdatedAt)
    {
        $matchId = @$matchInfo['id'];
        //保存gameVersion
        $gameVersion = $this->matchInfo['game_version'];
        if (empty($gameVersion)){
            $gameVersion = $data_payload['gameVersion'];
            $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
            $redis->hSet($matchInfoRedisName,'game_version',$gameVersion);
            $this->matchInfo['game_version'] = $gameVersion;
        }
        $battle_status = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'base'],'status');
        if (empty($battle_status)){
            $battlesBase['game'] = 2;
            $battlesBase['match'] = $matchId;
            $battlesBase['status'] = 2;
            $battlesBase['is_draw'] = 2;
            $battlesBase['is_forfeit'] = 2;
            $battlesBase['is_default_advantage'] = 2;
            $battlesBase['map'] = 1;
            $battlesBase['is_battle_detailed'] = 1;
            $battlesBase['flag'] = 1;
            $battlesBase['deleted_at'] = null;
            $battlesBase['deleted'] = 2;
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battlesBase);
        }
        $match_battle_duration = intval($data_payload['gameTime']/1000);
        $duration = $match_battle_duration > 0 ? $match_battle_duration : 1;
        $server_config = [
            'version' => (String)$gameVersion
        ];
        $factions = [];
        //处理原始值
        $gameOver = $data_payload['gameOver'];
        $winningTeamUrnId = $data_payload['winningTeamUrn'];
        $is_pause = $data_payload['paused'] == true ? true : false;
        $playersInfo = [];
        $rel_teamsInfo = [$data_payload['teamOne'],$data_payload['teamTwo']];
        $winner_teams_info = [];
        //获取所有的item
        $lolItemsList = $this->getLolItemsList($redis,$metadata_origin_id);
        $LolItemsRelationList = $this->getLolItemsRelationList($redis,$metadata_origin_id);
        //符文列表
        //$relKeystoneRelationArray = $redis->hGetAll("lol:rune:relation:".$metadata_origin_id);
        $relKeystoneRelationArray = $this->getRunesRelationList($redis,$metadata_origin_id);
        //获取所有的召唤师技能
        $summonerSpellInfoList = $this->getSummonerSpellInfoList($redis,$metadata_origin_id);
        //获取原来team的数据
        $battleTeamInfo_old = $this->getBattleTeamsInfoFromMemory($redis,$battleId,'all_info');
        if (count($battleTeamInfo_old) == 0){
            $teams_Info_Array = $this->supplementTeamInfo($redis,$matchInfo,$battleId,$data_payload);
            foreach ($teams_Info_Array as $key_team => $item_team){
                $battleTeamInfo_old[$key_team] = @json_decode($item_team,true);
            }
        }
        $winning_team_id = 0;
        $websocket_team_players = [];
        $winner_teams_info_res = [];
        $match_scores = [];
        $team_info_res = $team_info_new = $team_elites_Kills = $playerResVal = [];
        $team1_damage_to_champions = $team2_damage_to_champions = $team1_damage_taken = $team2_damage_taken = 0;
        $team_player_wards_purchased_all = $team_player_wards_placed_all = $team_player_wards_kills_all = 0;
        //原来的player
        $player_array_new = [];
        $battlePlayersInfo_old = $this->redis_hGetPlayersAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        if (count($battlePlayersInfo_old) == 0){
            $teams_Info_Array_Json = [];
            foreach ($battleTeamInfo_old as $key_team => $item_team) {
                $teams_Info_Array_Json[$key_team] = @json_encode($item_team,320);
            }
            $battlePlayersInfo_old = $this->supplementTeamAndPlayersInfo($redis,$matchInfo,$battleId,$data_payload,$teams_Info_Array_Json);
        }
        //循环原来的
        foreach ($rel_teamsInfo as $key => $item){
            $team_player_damage_to_champions_all = 0;
            $team_player_damage_taken_all = 0;
            $liveDataTeamUrn = $item['liveDataTeamUrn'];
            $battleTeamInfo = $battleTeamInfo_old[$liveDataTeamUrn];
            //初始化里面已经有了， 统计数据来源于 事件统计
            //$match_score = (Int)$data_payload['seriesStatus']['seriesScores'][$liveDataTeamUrn];
            $team_score = 0;
            $team_order = $battleTeamInfo['order'];
            $team_id = $battleTeamInfo['team_id'];
            if ($item['teamID'] == 100){//100蓝 200红
                $team_faction = 'blue';
            }else{//200
                $team_faction = 'red';
            }
            $championsKills = $item['championsKills'];
            $deaths = $item['deaths'];
            $assists = $item['assists'];
            $totalGold = $item['totalGold'];
            $towerKills = $item['towerKills'];
            $inhibitorKills = $item['inhibKills'];
            $baronKills = $item['baronKills'];
            $dragonKills = $item['dragonKills'];
            //杀龙信息
            $team_elites_Kills[] = (int)$dragonKills;
            //组合数据
            $team_info = [
                'battle_id' => $battleId,
                'faction' => $team_faction,
                'rel_identity_id' => $liveDataTeamUrn,
                'score' => $team_score,
                //'match_score' => $match_score,
                'kills' => $championsKills,
                'deaths' => $deaths,
                'assists' => $assists,
                'gold' => $totalGold,
                'gold_diff' => 0,//最下面计算
                'experience' => 0,//选手计算
                'experience_diff' => 0,//最下面计算
                'turret_kills' => $towerKills,
                'inhibitor_kills' => $inhibitorKills,
                //'rift_herald_kills' => $rift_herald_kills,//通过计算的不用再这里取
                'dragon_kills' => $dragonKills,
                //'dragon_kills_detail' => null,
                'baron_nashor_kills' => $baronKills,
            ];
            //循环选手
            foreach ($item['players'] as $key_p => $val_p){
                $player = $old_player = [];
                $liveDataPlayerUrn = $val_p['liveDataPlayerUrn'];
                $player_res_item = $battlePlayersInfo_old[$liveDataPlayerUrn];
                $old_player = @json_decode($player_res_item,true);
                //整理玩家数据
                $player_id = @$old_player['player_id'];
                //新数据
                $player['faction'] = $team_faction;
                $player['team_id'] = $team_id;
                $player['team_order'] = $team_order;
                //判断玩家是否有英雄
                if (empty($old_player['champion'])){
                    $player['champion'] = $this->getLolChampionId($redis,$metadata_origin_id,$val_p['championID']);
                }
                $player['level'] = $val_p['level'];
                $player['alive'] = $val_p['alive'] == true ? 1 : 2;
                $player['ultimate_cd'] = intval($val_p['ultimate']['cooldownRemaining']);
                $player['coordinate'] = @implode(',',$val_p['position']);
                $player['kills'] = $val_p['stats']['championsKilled'];//击杀
//                $player['double_kill'] = null;//通过计算的 old 里面
//                $player['triple_kill'] = null;//通过计算的 old 里面
//                $player['quadra_kill'] = null;//通过计算的 old 里面
//                $player['penta_kill'] = null;//通过计算的 old 里面
//                $player['largest_multi_kill'] = null;//通过计算的 old 里面
//                $player['largest_killing_spree'] = null;//通过计算的 old 里面
                $player['deaths'] = $val_p['stats']['numDeaths'];
                $player['assists'] = $val_p['stats']['assists'];
                //计算kda
                $deaths = $player['deaths'] ? $player['deaths'] : 1;
                $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
                $player['participation'] = null;
                //团队击杀 $championsKills
                if ($championsKills > 0) {
                    $player['participation'] = (String)round((($player['kills'] + $player['assists'])/$championsKills),4);
                }
                $player['cs'] = (Int)$val_p['stats']['minionsKilled'] + (Int)$val_p['stats']['neutralMinionsKilled'];
                $player['minion_kills'] = $val_p['stats']['minionsKilled'];
                $player['total_neutral_minion_kills'] = $val_p['stats']['neutralMinionsKilled'];
                $player['neutral_minion_team_jungle_kills'] = $val_p['stats']['neutralMinionsKilledYourJungle'];
                $player['neutral_minion_enemy_jungle_kills'] = $val_p['stats']['neutralMinionsKilledEnemyJungle'];
                //cspm
                $player['cspm'] = (String)round(($player['cs']/($duration/60)),2);
                $player['gold_earned'] = $val_p['totalGold'];
                $player['gold_spent'] = (Int)($val_p['totalGold'] - $val_p['currentGold']);
                $player['gold_remaining'] = $val_p['currentGold'];
                $player['gpm'] = (String)round(($player['gold_earned']/($duration/60)),2);
                $player['gold_earned_percent'] = (String)round(($player['gold_earned'] / $totalGold),4) ;
                $player['experience'] = (Int)$val_p['experience'];
                //计算团队经验
                $team_info['experience'] += (Int)$val_p['experience'];
                $player['xpm'] = (String)round(($player['experience']/($duration/60)),2);
                $player['damage_to_champions'] = $val_p['stats']['totalDamageDealtChampions'];
                $player['damage_to_champions_physical'] = $val_p['stats']['physicalDamageDealtChampions'];
                $player['damage_to_champions_magic'] = $val_p['stats']['magicDamageDealtChampions'];
                $player['damage_to_champions_true'] = $val_p['stats']['trueDamageDealtChampions'];
                $player['dpm_to_champions'] = (String)round(($player['damage_to_champions']/($duration/60)),2);
                $player['damage_percent_to_champions'] = null;//下面计算
                $player['damage_to_towers']= (String)$val_p['stats']['totalDamageDealtToTurrets'];
                $player['damage_taken'] = $val_p['stats']['totalDamageTaken'];
                $player['damage_taken_physical'] = $val_p['stats']['physicalDamageTaken'];
                $player['damage_taken_magic'] = $val_p['stats']['magicDamageTaken'];
                $player['damage_taken_true'] = $val_p['stats']['trueDamageTaken'];
                $player['dtpm'] = (String)round(($player['damage_taken']/($duration/60)),2);
                $player['damage_taken_percent'] = null;//下面计算
                $player['damage_conversion_rate'] = (String)round(($player['damage_to_champions'] / $player['gold_earned']),4);//伤害转化率
                $player['total_heal'] = $val_p['stats']['totalHealOnTeammates'];//总治疗量
                $player['total_crowd_control_time'] = $val_p['stats']['totalTimeCrowdControlDealt'];//总控制时长
                $player['wards_purchased'] = empty($old_player['wards_purchased']) ? 0 : $old_player['wards_purchased'];
                $player['wards_placed'] = (Int)$val_p['stats']['wardPlaced'];//插眼
                $player['wards_kills'] = (Int)$val_p['stats']['wardKilled'];//排眼
                $player['warding_totems_purchased'] = null;//购买监视图腾
                $player['warding_totems_placed'] = null;//放置监视图腾
                $player['warding_totems_kills'] = null;//摧毁监视图腾
                $player['control_wards_purchased'] = null;//购买控制守卫
                $player['control_wards_placed'] = null;//放置控制守卫
                $player['control_wards_kills'] = null;//摧毁控制守卫
                $player['total_damage'] = $val_p['stats']['totalDamageDealt'];
                $player['total_damage_physical'] = $val_p['stats']['physicalDamageDealtPlayer'];
                $player['total_damage_magic'] = $val_p['stats']['magicDamageDealtPlayer'];
                $player['total_damage_true'] = $val_p['stats']['trueDamageDealtPlayer'];
                //items
                $player_items = $this->conversionBayesPlayerItemsIdsFromRedis($redis,$match_battle_duration,$val_p['items'],$liveDataPlayerUrn,$battleId,$lolItemsList,$LolItemsRelationList);
                $player['items'] = @$player_items['items'];
                $player['items_timeline'] = @$player_items['items_timeline'];
                //为空在处理 召唤师技能
                $player_spell_list = [$val_p['spell1'],$val_p['spell2']];
                $rel_summoner_spells = $this->conversionBayesPlayerSpellsIds($redis,$player_spell_list,$metadata_origin_id);
                $player['rel_summoner_spells'] = $rel_summoner_spells;
                $summoner_spells = [];
                foreach ($rel_summoner_spells as $ss_item){
                    $summoner_spells[] = [
                        'spell_id' => $ss_item['spell_id'],
                        'cooldown' => $ss_item['cooldown']
                    ];
                }
                $player['summoner_spells'] = $summoner_spells;
                //符文 会变化的
                $player['runes'] = $this->conversionPlayerRunes($redis,$metadata_origin_id,$val_p['stats']['perks'],$relKeystoneRelationArray);
                //abilities_timeline 技能升级时间线  old 里面
                $player['hp'] = $val_p['health'];
                if (empty($old_player['keystone'])){
                    //符文
                    $rel_player_keystoneId = $val_p['keystoneID'];
                    $keystone_relation_id = $relKeystoneRelationArray[$rel_player_keystoneId];
                    if ($keystone_relation_id){
                        $player['keystone'] = $keystone_relation_id;
                    }else{
                        $player['keystone'] = $this->getKeystoneUnknownId($redis);
                    }
                }
                //活着就不计算 复活时间
                if ($val_p['alive']){
                    $player['respawntimer'] = 0;
                    $player['died_time'] = 0;
                }else{
                    $player_died_time = (Int)@$old_player['died_time'];
                    $player_respawntimer = $match_battle_duration - $player_died_time;
                    if ($player_died_time > 0 && $player_respawntimer >0){
                        $player['respawntimer'] = $player_respawntimer;
                    }else{
                        $player['respawntimer'] = 0;
                        $player['died_time'] = 0;
                    }
                }
                $player['health'] = (Int)$val_p['health'];
                $player['health_max'] = (Int)$val_p['healthMax'];
                $player['damage_selfmitigated'] = $val_p['stats']['totalDamageSelfMitigated'];
                $player['damage_shielded_on_teammates'] = $val_p['stats']['totalDamageShieldedOnTeammates'];
                $player['damage_to_buildings'] = $val_p['stats']['totalDamageDealtToBuildings'];
                $player['damage_to_objectives'] = $val_p['stats']['totalDamageDealtToObjectives'];
                $player['total_crowd_control_time_others'] = $val_p['stats']['totalTimeCCOthers'];
                $player['vision_score'] = $val_p['stats']['visionScore'];
                //组合要处理的数据
                $team_player_damage_to_champions_all += $player['damage_to_champions'];
                $team_player_damage_taken_all += $player['damage_taken'];
                $team_player_wards_purchased_all += $player['wards_purchased'];
                $team_player_wards_placed_all += $player['wards_placed'];
                $team_player_wards_kills_all += $player['wards_kills'];
                //合并新数组
                $player_array_new[$liveDataPlayerUrn] = @array_merge($old_player,$player);
            }
            //计算总数
            if ($team_order == 1){
                $team1_damage_to_champions = $team_player_damage_to_champions_all;
                $team1_damage_taken = $team_player_damage_taken_all;
            }
            if ($team_order == 2){
                $team2_damage_to_champions = $team_player_damage_to_champions_all;
                $team2_damage_taken = $team_player_damage_taken_all;
            }
            $team_info['wards_purchased'] = $team_player_wards_purchased_all;
            $team_info['wards_placed'] = $team_player_wards_placed_all;
            $team_info['wards_kills'] = $team_player_wards_kills_all;
            //设置team信息
            $team_info_new[$item['teamID']] = @array_merge($battleTeamInfo,$team_info);
            $winner_teams_info_res[$liveDataTeamUrn] = $team_order;
        }
        //计算damage_percent_to_champions 和damage_taken_percent  再出websocket
        $team1_damage_to_champions_res = $team1_damage_to_champions == 0 ? 1 : $team1_damage_to_champions;
        $team2_damage_to_champions_res = $team2_damage_to_champions == 0 ? 1 : $team2_damage_to_champions;
        $team1_damage_taken_res = $team1_damage_taken == 0 ? 1 : $team1_damage_taken;
        $team2_damage_taken_res = $team2_damage_taken == 0 ? 1 : $team2_damage_taken;
        foreach ($player_array_new as $player_key => &$player_res) {
            if ($player_res['team_order'] == 1){
                $player_res['damage_percent_to_champions'] = (string)round(($player_res['damage_to_champions'] / $team1_damage_to_champions_res),4);
                $player_res['damage_taken_percent'] = (string)round(($player_res['damage_taken'] / $team1_damage_taken_res),4);
            }
            if ($player_res['team_order'] == 2){
                $player_res['damage_percent_to_champions'] = (string)round(($player_res['damage_to_champions'] / $team2_damage_to_champions_res),4);
                $player_res['damage_taken_percent'] = (string)round(($player_res['damage_taken'] / $team2_damage_taken_res),4);
            }
            //组合websocket
            $websocket_player = [];
            $websocket_player['seed'] = (int)$player_res['seed'];
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = @Common::getLolRoleStringByRoleId('orange',$player_res['role']);
            $websocket_player['lane'] = @Common::getLolLaneStringByLaneId('orange',$player_res['lane']);
            $websocket_player['player']['player_id'] = self::checkAndChangeInt($player_res['player_id']);
            $websocket_player['player']['nick_name'] = self::checkAndChangeString($player_res['nick_name']);
            //获取英雄信息
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$player_res['champion']);
            $websocket_player['champion'] = $champion;
            $websocket_player['level'] = (int)$player_res['level'];
            $websocket_player['is_alive'] = $player_res['alive'] == 1 ? true : false;
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['cs'] = (Int)$player_res['cs'];
            $websocket_player['cspm'] = (String)$player_res['cspm'];
            //summoner_spells
            $websocket_player['summoner_spells'] = $this->getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_res['summoner_spells']);
            //道具
            $websocket_player['items'] = $this->getWebSocketPlayerItemsList($lolItemsList,$player_res['items']);
            $keystone = $this->getWebSocketPlayerKeystoneInfo($redis,$metadata_origin_id,$player_res['player_id'],$player_res['keystone']);
            $advanced = [
                'keystone' => $keystone,
                'ultimate_cd' => (Int)$player_res['ultimate_cd'],
                'coordinate' => (String)$player_res['coordinate'],
                'respawntimer' => (Int)$player_res['respawntimer'],
                'health' => (Int)$player_res['health'],
                'health_max' => (Int)$player_res['health_max'],
                'turret_kills' => (Int)$player_res['turret_kills'],
                'inhibitor_kills' => (Int)$player_res['inhibitor_kills'],
                'rift_herald_kills' => (Int)$player_res['rift_herald_kills'],
                'dragon_kills' => (Int)$player_res['dragon_kills'],
                'baron_nashor_kills' => (Int)$player_res['baron_nashor_kills'],
                'double_kill' => (Int)$player_res['double_kill'],
                'triple_kill' => (Int)$player_res['triple_kill'],
                'quadra_kill' => (Int)$player_res['quadra_kill'],
                'penta_kill' => (Int)$player_res['penta_kill'],
                'largest_multi_kill' => (Int)$player_res['largest_multi_kill'],
                'largest_killing_spree' => (int)$player_res['largest_killing_spree'],
                'minion_kills' => (String)$player_res['minion_kills'],
                'total_neutral_minion_kills' => (String)$player_res['total_neutral_minion_kills'],
                'neutral_minion_team_jungle_kills' => (String)$player_res['neutral_minion_team_jungle_kills'],
                'neutral_minion_enemy_jungle_kills' => (String)$player_res['neutral_minion_enemy_jungle_kills'],
                'gold_earned' => (Int)$player_res['gold_earned'],
                'gold_spent' => (Int)$player_res['gold_spent'],
                'gold_remaining' => (Int)$player_res['gold_remaining'],
                'gpm' => (String)$player_res['gpm'],
                'gold_earned_percent' => (String)$player_res['gold_earned_percent'],
                'experience' => (Int)$player_res['experience'],
                'xpm' => (String)$player_res['xpm'],
                'damage_to_champions' => (String)$player_res['damage_to_champions'],
                'damage_to_champions_physical' => (String)$player_res['damage_to_champions_physical'],
                'damage_to_champions_magic' => (String)$player_res['damage_to_champions_magic'],
                'damage_to_champions_true' => (String)$player_res['damage_to_champions_true'],
                'dpm_to_champions' => (String)$player_res['dpm_to_champions'],
                'damage_percent_to_champions' => (String)$player_res['damage_percent_to_champions'],
                'total_damage' => (String)$player_res['total_damage'],
                'total_damage_physical' => (String)$player_res['total_damage_physical'],
                'total_damage_magic' => (String)$player_res['total_damage_magic'],
                'total_damage_true' => (String)$player_res['total_damage_true'],
                'damage_taken' => (String)$player_res['damage_taken'],
                'damage_taken_physical' => (String)$player_res['damage_taken_physical'],
                'damage_taken_magic' => (String)$player_res['damage_taken_magic'],
                'damage_taken_true' => (String)$player_res['damage_taken_true'],
                'dtpm' => (String)$player_res['dtpm'],
                'damage_taken_percent' => (String)$player_res['damage_taken_percent'],
                'damage_conversion_rate' => (String)$player_res['damage_conversion_rate'],
                'damage_selfmitigated' => (String)$player_res['damage_selfmitigated'],
                'damage_shielded_on_teammates' => (String)$player_res['damage_shielded_on_teammates'],
                'damage_to_buildings' => (String)$player_res['damage_to_buildings'],
                'damage_to_towers' => (String)$player_res['damage_to_towers'],
                'damage_to_objectives' => (String)$player_res['damage_to_objectives'],
                'total_crowd_control_time' => (String)$player_res['total_crowd_control_time'],
                'total_crowd_control_time_others' => (String)$player_res['total_crowd_control_time_others'],
                'total_heal' => (String)$player_res['total_heal'],
                'wards_purchased' => (Int)$player_res['wards_purchased'],
                'wards_placed' => (Int)$player_res['wards_placed'],
                'wards_kills' => (Int)$player_res['wards_kills'],
                'vision_score' => (String)$player_res['vision_score']
            ];
            $websocket_player['advanced'] = $advanced;
            $websocket_team_players[$player_res['team_order']][] = $websocket_player;
            //组成最终player数组
            $playerResVal[$player_key] = @json_encode($player_res,320);
        }
        //最终保存player
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerResVal);

        //计算队伍的diff 信息
        //计算gold_diff  100 蓝  200红  蓝减红
        $gold_diff = $team_info_new[100]['gold'] - $team_info_new[200]['gold'];
        if ($gold_diff != 0){
            $team_info_new[100]['gold_diff'] = $gold_diff;
            $team_info_new[200]['gold_diff'] = -$gold_diff;
        }
        //计算experience_diff 100 蓝  200红  蓝减红
        $experience_diff = $team_info_new[100]['experience'] - $team_info_new[200]['experience'];
        if ($experience_diff != 0){
            $team_info_new[100]['experience_diff'] = $experience_diff;
            $team_info_new[200]['experience_diff'] = -$experience_diff;
        }
        //首先获取时间是否可以保存了
        $gold_or_experience_diff_time = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff']);
        if ($gold_or_experience_diff_time){
            $gold_diff_time_cha = $match_battle_duration - $gold_or_experience_diff_time;
            //间隔60秒保存
            if ($gold_diff_time_cha >= 30 || $gameOver == true){
                //gold_diff
                $gold_diff_timeline_now[] = [
                    'ingame_timestamp' => $match_battle_duration,
                    'gold_diff' => $gold_diff
                ];
                //获取原来的信息
                $gold_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_diff_timeline']);
                if ($gold_diff_timeline_redis){
                    $gold_diff_timeline_old = @json_decode($gold_diff_timeline_redis,true);
                    $gold_diff_timeline_new = @array_merge($gold_diff_timeline_old,$gold_diff_timeline_now);
                }else{
                    $gold_diff_timeline_new = $gold_diff_timeline_now;
                }
                $gold_diff_timeline_res = @json_encode($gold_diff_timeline_new,320);
                $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_diff_timeline'],$gold_diff_timeline_res);
                //experience_diff
                $experience_diff_timeline_now[] = [
                    'ingame_timestamp' => $match_battle_duration,
                    'experience_diff' => $experience_diff
                ];
                //获取原来的信息
                $experience_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'experience_diff_timeline']);
                if ($experience_diff_timeline_redis){
                    $experience_diff_timeline_old = @json_decode($experience_diff_timeline_redis,true);
                    $experience_diff_timeline_new = @array_merge($experience_diff_timeline_old,$experience_diff_timeline_now);
                }else{
                    $experience_diff_timeline_new = $experience_diff_timeline_now;
                }
                $experience_diff_timeline_res = @json_encode($experience_diff_timeline_new,320);
                $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'experience_diff_timeline'],$experience_diff_timeline_res);
                //设置时间间隔
                $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff'],$match_battle_duration);
            }
        }else{
            //gold_diff
            $gold_diff_timeline_now[] = [
                'ingame_timestamp' => $match_battle_duration,
                'gold_diff' => $gold_diff
            ];
            $gold_diff_timeline_res = @json_encode($gold_diff_timeline_now,320);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_diff_timeline'],$gold_diff_timeline_res);
            //experience_diff
            $experience_diff_timeline_now[] = [
                'ingame_timestamp' => $match_battle_duration,
                'experience_diff' => $experience_diff
            ];
            $experience_diff_timeline_res = @json_encode($experience_diff_timeline_now,320);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'experience_diff_timeline'],$experience_diff_timeline_res);
            //设置时间间隔
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff'],$match_battle_duration);
        }

        //获取龙信息
        $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
        //获取比赛的大比分 match_real_time_info
        $match_real_time_info = $this->redis_hGetAll_Or_Memory($redis,['match_real_time_info']);
        //计算winner
        $winner_id = null;
        $winningTeamUrnId = $data_payload['winningTeamUrn'];
        if ($winningTeamUrnId){
            $winner_id = @$winner_teams_info_res[$winningTeamUrnId];
        }
        //循环新的team 和 websocket
        foreach ($team_info_new as &$team_info_item){
            $team_key = $team_info_item['rel_identity_id'];
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_item['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info_item['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt(@$team_info_item['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            $winner_teams_info[$team_opponent_order] = $teamInfo;
            //处理龙的信息
            $dragon_kills_detail_res = $this->conversionTeamDragonKills($drakeInfoList,$team_info_item['dragon_kills_detail']);
            //处理塔的信息  加上复活时间
            //计算胜利的队伍
            $team_building_status_nexus = true;
            if ($winningTeamUrnId && $winningTeamUrnId != $team_key){
                $team_building_status_nexus = false;
            }
            //计算一下复活时间
            $team_building_status_res = $this->conversionTeamBuildingStatus($redis,$battleId,$match_battle_duration,$team_key,$team_info_item['building_status'],$team_building_status_nexus);
            //计算之后 再做保存
            $team_info_item['building_status'] = $team_building_status_res;
            //advanced
            $team_advanced = [
                'wards_purchased' => (int)@$team_info_item['wards_purchased'],
                'wards_placed' => (int)@$team_info_item['wards_placed'],
                'wards_kills' => (int)@$team_info_item['wards_kills']
            ];
            //factions team的信息
            $faction_team = [
                'faction' => (String)$team_info_item['faction'],
                'team' => $teamInfo,
                'kills' => (int)$team_info_item['kills'],
                'deaths' => (int)@$team_info_item['deaths'],
                'assists' => (int)@$team_info_item['assists'],
                'gold' => (int)@$team_info_item['gold'],
                'gold_diff' => (int)@$team_info_item['gold_diff'],
                'experience' => (int)@$team_info_item['experience'],
                'experience_diff' => (int)@$team_info_item['experience_diff'],
                'turret_kills' => (int)@$team_info_item['turret_kills'],
                'inhibitor_kills' => (int)@$team_info_item['inhibitor_kills'],
                'rift_herald_kills' => (int)@$team_info_item['rift_herald_kills'],
                'dragon_kills' => (int)@$team_info_item['dragon_kills'],
                'dragon_kills_detail' => $dragon_kills_detail_res,
                'baron_nashor_kills' => (int)@$team_info_item['baron_nashor_kills'],
                'building_status' => $team_building_status_res,
                'players' => $websocket_team_players[$team_opponent_order],
                'advanced' => $team_advanced
            ];
            //判断是否胜利
            if ($winner_id == $team_opponent_order){
                $team_info_item['score'] = 1;
            }
            $team_info_res[$team_info_item['rel_identity_id']] = @json_encode($team_info_item,320);
            $match_score_team = 0;
            //再设置外层 match_scores
            if($team_opponent_order == 1){
                $match_score_team = $match_real_time_info['team_1_score'] + $team_info_item['score'];
            }
            if($team_opponent_order == 2){
                $match_score_team = $match_real_time_info['team_2_score'] + $team_info_item['score'];
            }
            $teamInfo['score'] = (int)$match_score_team;
            $match_scores[] = $teamInfo;
            $factions[] = $faction_team;
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_info_res);
//        //重置array key
//        $factions = @array_values($factions);
        //battle 是否结束
        $is_battle_finished = false;
        $is_match_finished = false;
        $match_winner = null;
        $battle_winner = null;
        if ($gameOver){
            $is_battle_finished = true;
            //上面计算
            switch ($winner_id){
                case 1:
                    $match_real_time_info['team_1_score'] = $match_real_time_info['team_1_score'] + 1;
                    break;
                case 2:
                    $match_real_time_info['team_2_score'] = $match_real_time_info['team_2_score'] + 1;
                    break;
            }
            $battle_winner = $winner_teams_info[$winner_id];
            //battle的分数
            $team_info_res[$winningTeamUrnId]['score'] = 1;
            //计算结束时间
            //$end_at = date("Y-m-d H:i:s",strtotime($data_payload['lastUpdateTime']));
            $battle_base = [
                'duration'=>$duration,
                //'end_at' => $end_at,
                'status' => 3,
                'winner' => $winner_id
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_base);
            //计算比赛的胜算
            $statusInfo = WinnerHelper::lolWinnerInfo($match_real_time_info['team_1_score'],$match_real_time_info['team_2_score'],$matchInfo['number_of_games']);
            if($statusInfo['is_finish']==1){
                $is_match_finished = true;
                $match_real_time_info['status'] = 3;
                //$match_real_time_info['end_at']= $end_at;
                if ($statusInfo['is_draw'] == 1){//判断是不是平局
                    $match_real_time_info['winner'] = null;
                    $match_real_time_info['is_draw'] = 1;
                }else{
                    $match_real_time_info['winner'] = $statusInfo['winner_team'];
                    $match_winner = $winner_teams_info[$statusInfo['winner_team']];
                }
            }
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
        }else{
            $base_data = [
                'duration'=>$duration
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$base_data);
        }
        //精英怪状态
        $elites_status = $this->getElitesStatusNew($redis,$battleId,$match_battle_duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'elites_status',@json_encode($elites_status,320));
        //最终结果
        $event_data = [
            'duration' => $match_battle_duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => $is_pause,
            'is_live' => true,
            'elites_status' => $elites_status,
            'factions' => $factions,
            'server_config' => $server_config
        ];
        $result = [
            'type' => 'frames',
            'event_data' => $event_data
        ];
        return $result;
    }
    //处理装备
    public function conversionBayesPlayerItemsIdsFromRedis($redis,$ingame_timestamp,$playerItems,$playerId,$battleId,$lolItemsList,$lolItemsRelationList){
        //获取未知的装备ID
        $item_unknown_id = $this->getLolItemsUnknownId($redis);
        $need_change = 0;
        //当前这个player的装备信息 然后获取上一个装备信息列表，判断有没有变化
        $playersItemsRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$playerId);
        if ($playersItemsRedis){
            $playersItemsBeforeLine = @json_decode($playersItemsRedis,true);
            $playersItemsBefore = @end($playersItemsBeforeLine)['items'];
        }else{
            $playersItemsBeforeLine = $playersItemsBefore = [];
        }
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        if ($playerItems){
            foreach ($playerItems as $key => $item_val){
                if ($item_val['itemID']){
                    $itemInfoId = $lolItemsRelationList[$item_val['itemID']];
                    if($itemInfoId){
                        if (!in_array($itemInfoId,$playersItemsBefore)){
                            $need_change = 1;
                        }
                        $itemInfoId_val = (Int)$itemInfoId;
                        //获取当前
                        $itemInfoResRedis = $lolItemsList[$itemInfoId_val];
                        if ($itemInfoResRedis){
                            $itemInfoRes = @json_decode($itemInfoResRedis,true);
                            if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                                $playerOrnamentsInfo[$key]['item_id'] = $itemInfoId_val;
                                $playerOrnamentsInfo[$key]['purchase_time'] = intval($item_val['purchaseGameTime']/1000);
                                $playerOrnamentsInfo[$key]['cooldown'] = intval($item_val['cooldownRemaining']);
                            }else{//道具
                                $playerItemsInfo[$key]['total_cost'] = @$itemInfoRes['total_cost'];
                                $playerItemsInfo[$key]['item_id'] = $itemInfoId_val;
                                $playerItemsInfo[$key]['purchase_time'] = intval($item_val['purchaseGameTime']/1000);
                                $playerItemsInfo[$key]['cooldown'] = intval($item_val['cooldownRemaining']);
                            }
                        }else{//道具
                            $playerItemsInfo[$key]['total_cost'] = 1;
                            $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                            $playerItemsInfo[$key]['purchase_time'] = intval($item_val['purchaseGameTime']/1000);
                            $playerItemsInfo[$key]['cooldown'] = intval($item_val['cooldownRemaining']);
                        }
                    }else{
                        $playerItemsInfo[$key]['total_cost'] = 1;
                        $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                        $playerItemsInfo[$key]['purchase_time'] = intval($item_val['purchaseGameTime']/1000);
                        $playerItemsInfo[$key]['cooldown'] = intval($item_val['cooldownRemaining']);
                    }
                }else{
                    $playerItemsNull[$key] = null;
                }
            }
            $total_cost_array = array_column($playerItemsInfo,'total_cost');
            array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
            //$playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
        }
        //判断是不是够6个
        $playerItemsInfoNum = count($playerItemsInfo);
        if ($playerItemsInfoNum < 6){
            $chaNum = 6 - $playerItemsInfoNum;
            for ($i=0;$i<$chaNum;$i++){
                $playerItemsNull[] = null;
            }
        }
        if (empty($playerOrnamentsInfo)){
            $playerOrnamentsInfo[] = null;
        }
        $playerItemsInfoResult = array_merge($playerItemsInfo,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        $playerItemsInfoIdResultEnd = [];
        //循环
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsId = null;
            if ($val_res){
                $ItemsId = @$val_res['item_id'];
                unset($val_res['total_cost']);
            }
            $playerItemsInfoResultEnd[] = $val_res;
            $playerItemsInfoIdResultEnd[] = $ItemsId;
        }
        //需要变化则更新redis
        if ($need_change == 1){
            $playerItemsInfoArray = [
                'ingame_timestamp'=>$ingame_timestamp,
                'items'=>$playerItemsInfoIdResultEnd
            ];
            $playersItemsBeforeLine[] = $playerItemsInfoArray;
            $playerItemsLine =  @json_encode($playersItemsBeforeLine,320);
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$playerId,$playerItemsLine);
        }else{
            $playerItemsLine = $playersItemsBeforeLine;
        }
        //设置返回数据
        $result = [
            'items' => $playerItemsInfoResultEnd,
            'items_timeline' => $playerItemsLine
        ];
        return $result;
    }
    //处理召唤师技能
    public function conversionBayesPlayerSpellsIds($redis,$player_spell_list,$originId){
        $player_spell = [];
        $UnknownInfo = null;
        //召唤师技能
        $summonerSpellNameRelationIdList = $this->getSummonerSpellNameRelationIdList($redis,$originId);
        foreach ($player_spell_list as $s_key => $s_val){
            $is_unknown = 0;
            $summoner_spell_id = $summonerSpellNameRelationIdList[$s_val['name']];
            if (!$summoner_spell_id && empty($UnknownInfo)){
                $redisNameList = "lol:summoner_spell:list:unknown";
                $UnknownInfoRedis = $redis->get($redisNameList);
                $UnknownInfo = @json_decode($UnknownInfoRedis,true);
                $summoner_spell_id = @$UnknownInfo['id'];
                $is_unknown = 1;
            }
            $player_spell[$s_key]['spell_id'] = $summoner_spell_id;
            $player_spell[$s_key]['cooldown'] = intval($s_val['cooldownRemaining']);
            $player_spell[$s_key]['is_unknown'] = $is_unknown;
            $player_spell[$s_key]['unknown_info'] = $UnknownInfo;
        }
        return $player_spell;
    }
    //处理符文
    private function conversionPlayerRunes($redis,$originId,$rel_player_runes,$relKeystoneRelationArray){
        $runes = [];
        if ($rel_player_runes){
            //符文列表
            //$runesRelationList = $this->getRunesRelationList($redis,$originId);
            foreach ($rel_player_runes as $item){
                //$runes_id = @$runesRelationList[$item['value']];
                $runes_id = @$relKeystoneRelationArray[$item['value']];
                if (empty($runes_id)){
                    $runes_id = $this->getRunesUnknownId($redis);
                }
                $runes[]=$runes_id;
            }
        }
        return $runes;
    }
    //第一次事件
    public function getCommonFirstEventInfo($redis,$battleId,$killed_type,$faction){
        //$event_first_redis_name = $redis_base_name."battles:".$battleId.":event_first";
        $event_first_redis_name = ['current','battles',$battleId,'event_first'];
        //记录事件
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
                //设置初始值
                $team_kills_key = 'team_kills_'.$faction;
                //查询是否是一血
                //$first_blood = $redis->hGet($event_first_redis_name,'first_blood');
                $first_blood = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_blood');
                if (!$first_blood){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_blood'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_blood',$faction);
                }
                //查询 first_to_5_kills first_to_10_kills
                //查询当前team击杀了多少人
                //查询是否五杀了
                $first_to_5_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_5_kills');
                $first_to_10_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_10_kills');
                $team_kills_res = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,$team_kills_key);//击杀队伍
                if ($team_kills_res){
                    $now_team_kills = $team_kills_res + 1;
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,$now_team_kills);
                    if ($now_team_kills == 5 && empty($first_to_5_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_5_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_5_kills',$faction);
                    }
                    if ($now_team_kills == 10 && empty($first_to_10_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_10_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_10_kills',$faction);
                    }
                }else{
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,1);
                }
                break;
            case 'rift_herald':
                $rift_herald = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_rift_herald',$faction);
                }
                break;
            case 'drake':
                $first_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_dragon');
                if (!$first_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_dragon',$faction);
                }
                break;
            case 'elder_dragon':
                $first_elder_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_elder_dragon',$faction);
                }
                break;
            case 'baron_nashor':
                $first_baron_nashor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_baron_nashor',$faction);
                }
                break;
            case 'tower':
                $first_turret = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_turret',$faction);
                }
                break;
            case 'inhibitor':
                $first_inhibitor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_inhibitor',$faction);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null
                ];
        }
        return $eventInfoRes;
    }
    //处理玩家 position
    private function conversionPlayerPositions($redis,$matchInfo,$battleId,$data_payload,$metadata_origin_id){
        $match_battle_duration = intval($data_payload['gameTime']/1000);
        $matchId = @$matchInfo['id'];
        //保存gameVersion
        $gameVersion = $this->matchInfo['game_version'];
        if (empty($gameVersion)){
            $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
            $gameVersion = $redis->hGet($matchInfoRedisName,'game_version');
            $this->matchInfo['game_version'] = $gameVersion;
        }
        $server_config = [
            'version' => (String)$gameVersion
        ];
        $base_data = [
            'duration'=>$match_battle_duration
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$base_data);
        //获取所有玩家
        $playersPositionsInfo = [];
        $players_positions = $data_payload['positions'];
        foreach ($players_positions as $item){
            $playersPositionsInfo[$item['playerUrn']] = @implode(',' , $item['position']);
        }
        //web socket
        $factions = [];
        //获取原来team的数据
        $battleTeamInfo = $this->getBattleTeamsInfoFromMemory($redis,$battleId,'all_info');
        if (empty($battleTeamInfo)){
            return null;
        }
        //获取所有的装备
        $lolItemsList = $this->getLolItemsList($redis,$metadata_origin_id);
        //获取所有的召唤师技能
        $summonerSpellInfoList = $this->getSummonerSpellInfoList($redis,$metadata_origin_id);
        $websocket_team_players = [];
        $match_scores = [];
        $playerResVal = [];
        //原来的player
        $player_array_new = $this->redis_hGetPlayersAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        foreach ($player_array_new as $player_key => $player_item) {
            $player_res = @json_decode($player_item,true);
            $rel_identity_id = $player_res['rel_identity_id'];
            $player_res['coordinate'] = @$playersPositionsInfo[$rel_identity_id];
            //组合websocket
            $websocket_player = [];
            $websocket_player['seed'] = (int)$player_res['seed'];
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = @Common::getLolRoleStringByRoleId('orange',$player_res['role']);
            $websocket_player['lane'] = @Common::getLolLaneStringByLaneId('orange',$player_res['lane']);
            $websocket_player['player']['player_id'] = self::checkAndChangeInt($player_res['player_id']);
            $websocket_player['player']['nick_name'] = self::checkAndChangeString($player_res['nick_name']);
            //获取英雄信息
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$player_res['champion']);
            $websocket_player['champion'] = $champion;
            $websocket_player['level'] = (int)$player_res['level'];
            $websocket_player['is_alive'] = $player_res['alive'] == 1 ? true : false;
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['cs'] = (Int)$player_res['cs'];
            $websocket_player['cspm'] = (String)$player_res['cspm'];
            //summoner_spells
            $websocket_player['summoner_spells'] = $this->getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_res['summoner_spells']);
            //道具
            $websocket_player['items'] = $this->getWebSocketPlayerItemsList($lolItemsList,$player_res['items']);
            $keystone = $this->getWebSocketPlayerKeystoneInfo($redis,$metadata_origin_id,$player_res['player_id'],$player_res['keystone']);
            //活着就不计算 复活时间
            if ($player_res['alive'] == 1){
                $player_respawntimer = 0;
            }else{
                $player_died_time = (Int)@$player_res['died_time'];
                $player_respawntimer_cha = $match_battle_duration - $player_died_time;
                if ($player_died_time > 0 && $player_respawntimer_cha >0){
                    $player_respawntimer = $player_respawntimer_cha;
                }else{
                    $player_respawntimer = 0;
                }
            }
            $advanced = [
                'keystone' => $keystone,
                'ultimate_cd' => (Int)$player_res['ultimate_cd'],
                'coordinate' => (String)$player_res['coordinate'],
                'respawntimer' => (Int)$player_respawntimer,
                'health' => (Int)$player_res['health'],
                'health_max' => (Int)$player_res['health_max'],
                'turret_kills' => (Int)$player_res['turret_kills'],
                'inhibitor_kills' => (Int)$player_res['inhibitor_kills'],
                'rift_herald_kills' => (Int)$player_res['rift_herald_kills'],
                'dragon_kills' => (Int)$player_res['dragon_kills'],
                'baron_nashor_kills' => (Int)$player_res['baron_nashor_kills'],
                'double_kill' => (Int)$player_res['double_kill'],
                'triple_kill' => (Int)$player_res['triple_kill'],
                'quadra_kill' => (Int)$player_res['quadra_kill'],
                'penta_kill' => (Int)$player_res['penta_kill'],
                'largest_multi_kill' => (Int)$player_res['largest_multi_kill'],
                'largest_killing_spree' => (int)$player_res['largest_killing_spree'],
                'minion_kills' => (String)$player_res['minion_kills'],
                'total_neutral_minion_kills' => (String)$player_res['total_neutral_minion_kills'],
                'neutral_minion_team_jungle_kills' => (String)$player_res['neutral_minion_team_jungle_kills'],
                'neutral_minion_enemy_jungle_kills' => (String)$player_res['neutral_minion_enemy_jungle_kills'],
                'gold_earned' => (Int)$player_res['gold_earned'],
                'gold_spent' => (Int)$player_res['gold_spent'],
                'gold_remaining' => (Int)$player_res['gold_remaining'],
                'gpm' => (String)$player_res['gpm'],
                'gold_earned_percent' => (String)$player_res['gold_earned_percent'],
                'experience' => (Int)$player_res['experience'],
                'xpm' => (String)$player_res['xpm'],
                'damage_to_champions' => (String)$player_res['damage_to_champions'],
                'damage_to_champions_physical' => (String)$player_res['damage_to_champions_physical'],
                'damage_to_champions_magic' => (String)$player_res['damage_to_champions_magic'],
                'damage_to_champions_true' => (String)$player_res['damage_to_champions_true'],
                'dpm_to_champions' => (String)$player_res['dpm_to_champions'],
                'damage_percent_to_champions' => (String)$player_res['damage_percent_to_champions'],
                'total_damage' => (String)$player_res['total_damage'],
                'total_damage_physical' => (String)$player_res['total_damage_physical'],
                'total_damage_magic' => (String)$player_res['total_damage_magic'],
                'total_damage_true' => (String)$player_res['total_damage_true'],
                'damage_taken' => (String)$player_res['damage_taken'],
                'damage_taken_physical' => (String)$player_res['damage_taken_physical'],
                'damage_taken_magic' => (String)$player_res['damage_taken_magic'],
                'damage_taken_true' => (String)$player_res['damage_taken_true'],
                'dtpm' => (String)$player_res['dtpm'],
                'damage_taken_percent' => (String)$player_res['damage_taken_percent'],
                'damage_conversion_rate' => (String)$player_res['damage_conversion_rate'],
                'damage_selfmitigated' => (String)$player_res['damage_selfmitigated'],
                'damage_shielded_on_teammates' => (String)$player_res['damage_shielded_on_teammates'],
                'damage_to_buildings' => (String)$player_res['damage_to_buildings'],
                'damage_to_towers' => (String)$player_res['damage_to_towers'],
                'damage_to_objectives' => (String)$player_res['damage_to_objectives'],
                'total_crowd_control_time' => (String)$player_res['total_crowd_control_time'],
                'total_crowd_control_time_others' => (String)$player_res['total_crowd_control_time_others'],
                'total_heal' => (String)$player_res['total_heal'],
                'wards_purchased' => (Int)$player_res['wards_purchased'],
                'wards_placed' => (Int)$player_res['wards_placed'],
                'wards_kills' => (Int)$player_res['wards_kills'],
                'vision_score' => (String)$player_res['vision_score']
            ];
            $websocket_player['advanced'] = $advanced;
            $player_seed_res = $websocket_player['seed']-1;
            $websocket_team_players[$player_res['team_order']][$player_seed_res] = $websocket_player;
            //组成最终player数组
            $playerResVal[$player_key] = @json_encode($player_res,320);
        }
        //最终保存player
        if ($playerResVal){
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerResVal);
        }
        //获取比赛的大比分 match_real_time_info
        $match_real_time_info = $this->redis_hGetAll_Or_Memory($redis,['match_real_time_info']);
        //获取龙信息
        $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
        //获取队伍信息
        $factions_faction = [];
        $team_info_new = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        $team_elites_Kills = [];
        //循环新的team 和 websocket
        foreach ($team_info_new as $team_info_val){
            $team_info_item = @json_decode($team_info_val,true);
            $team_key = $team_info_item['rel_identity_id'];
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_item['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info_item['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt(@$team_info_item['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            //处理龙的信息
            $dragon_kills_detail_res = $this->conversionTeamDragonKills($drakeInfoList,$team_info_item['dragon_kills_detail']);
            //处理塔的信息  加上复活时间
            //计算胜利的队伍
            $team_building_status_nexus = true;
            //计算一下复活时间
            $team_building_status_res = $this->conversionTeamBuildingStatus($redis,$battleId,$match_battle_duration,$team_key,$team_info_item['building_status'],$team_building_status_nexus);
            //计算之后 再做保存
            $team_info_item['building_status'] = $team_building_status_res;
            //advanced
            $team_advanced = [
                'wards_purchased' => (int)@$team_info_item['wards_purchased'],
                'wards_placed' => (int)@$team_info_item['wards_placed'],
                'wards_kills' => (int)@$team_info_item['wards_kills']
            ];
            $player_res_data = $websocket_team_players[$team_opponent_order];
            ksort($player_res_data);
            //factions team的信息
            $faction_team = [
                'faction' => (String)$team_info_item['faction'],
                'team' => $teamInfo,
                'kills' => (int)$team_info_item['kills'],
                'deaths' => (int)@$team_info_item['deaths'],
                'assists' => (int)@$team_info_item['assists'],
                'gold' => (int)@$team_info_item['gold'],
                'gold_diff' => (int)@$team_info_item['gold_diff'],
                'experience' => (int)@$team_info_item['experience'],
                'experience_diff' => (int)@$team_info_item['experience_diff'],
                'turret_kills' => (int)@$team_info_item['turret_kills'],
                'inhibitor_kills' => (int)@$team_info_item['inhibitor_kills'],
                'rift_herald_kills' => (int)@$team_info_item['rift_herald_kills'],
                'dragon_kills' => (int)@$team_info_item['dragon_kills'],
                'dragon_kills_detail' => $dragon_kills_detail_res,
                'baron_nashor_kills' => (int)@$team_info_item['baron_nashor_kills'],
                'building_status' => $team_building_status_res,
                'players' => $player_res_data,
                'advanced' => $team_advanced
            ];
            //杀龙信息
            $team_elites_Kills[] = (int)@$team_info_item['dragon_kills'];
            $match_score_team = 0;
            //再设置外层 match_scores
            if($team_opponent_order == 1){
                $match_score_team = $match_real_time_info['team_1_score'];
            }
            if($team_opponent_order == 2){
                $match_score_team = $match_real_time_info['team_2_score'];
            }
            $teamInfo['score'] = (int)$match_score_team;
            $match_scores[] = $teamInfo;
            $factions_faction[$team_info_item['faction']] = $faction_team;
        }
        //先蓝 后红
        $factions = [$factions_faction['blue'],$factions_faction['red']];
        $is_battle_finished = false;
        $is_match_finished = false;
        $match_winner = null;
        $battle_winner = null;
        //精英怪状态
        $elites_status = $this->getElitesStatusNew($redis,$battleId,$match_battle_duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'elites_status',@json_encode($elites_status,320));
        //最终结果
        $event_data = [
            'duration' => $match_battle_duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => false,
            'is_live' => true,
            'elites_status' => $elites_status,
            'factions' => $factions,
            'server_config' => $server_config
        ];
        $result = [
            'type' => 'frames',
            'event_data' => $event_data
        ];
        return $result;
    }
    //暂停
    private function conversionStartPause($redis,$matchId,$battleId,$data_payload,$event_id){
        $event_type = 'battle_pause';
        $duration = round($data_payload['gameTime']/1000);
        $pause_data = [
            'duration'=>$duration
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$pause_data);
        $pause_data = [
            'is_pause'=>1
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$pause_data);
        //获取 event_order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => (Int)$duration,
            'position' => null,
            'killer' => null,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //解除暂停
    private function conversionEndPause($redis,$matchId,$battleId,$data_payload,$event_id){
        $event_type = 'battle_unpause';
        $duration = round($data_payload['gameTime']/1000);
        $pause_data = [
            'duration'=>$duration
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$pause_data);
        $pause_data = [
            'is_pause'=>2
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$pause_data);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => (Int)$duration,
            'position' => null,
            'killer' => null,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //回档
    private function conversionMatchRollBack($redis,$matchId,$battleId,$data_payload,$origin_id,$event_id,$perId){
        $event_type = 'battle_reloaded';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $RollBackTime = $data_payload['gameTime'];
        //获取当前battle开始的event_id
        $battle_start_event_id = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'battle_start_event_id']);
        if (!$battle_start_event_id){
            return null;
        }
        $roll_back_event_data_array = [];
        //查询是否已经回过档
        $roll_back_info = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'roll_back_info']);
        if ($roll_back_info){
            $roll_back_array = $roll_back_info;
            $roll_back_info = array_flip($roll_back_info);
        }else{
            $roll_back_info = [];
            $roll_back_array = [];
        }
        //删除缓存 当前对局的缓存
        $this->delRedisAndMemoryByRollBack($redis,$matchId,$battleId,$RollBackTime);
        //
        $redis->set('match_all_info:'.$matchId.':reloaded_start_time',time());
        //查询所有
        $event_time_res = null;
        $match_list = MatchLived::find()->where(
            ['and',
                ['=','match_id',$perId],
                ['<=','id',$event_id],
                ['>=','id',(Int)$battle_start_event_id],
            ])->asArray()->all();
        if ($match_list){
            $redis->set('match_all_info:'.$matchId.':reloaded_search_time',time());
            foreach ($match_list as $key => $value){
                $rel_event_id = (Int)$value['id'];
                if (isset($roll_back_info[$rel_event_id])){
                    continue;
                }
                $event_item = @json_decode($value['match_data'],true);
                $event_time = @$event_item['payload']['payload']['payload']['gameTime'];
                if ($event_time){
                    if ($event_time >= $RollBackTime){
                        $roll_back_event_data_array[$rel_event_id] = $rel_event_id;
                        continue;
                    }
                }
                $this->runWithInfo($redis,$value,'roll_back');
            }
            $roll_back_event_data_array_res = array_merge($roll_back_array,$roll_back_event_data_array);
            //保存回档的开始结束 无效事件阶段
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'roll_back_info'],$roll_back_event_data_array_res);
        }
        $redis->set('match_all_info:'.$matchId.':reloaded_end_time',time());
        //设置battle 时间
        $battle_data = [
            'duration'=>$ingame_timestamp
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_data);
        //查询事件的order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'position' => null,
            'killer' => null,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'restore_to' => (string)$ingame_timestamp,//回档到何时
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type,
            'restore_to' => (Int)$ingame_timestamp,
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理 玩家 多杀
    private function conversionPlayerMultiKill($redis,$battleId,$data_payload){
        $killType = $data_payload['killType'];
        if ($killType == 'multi'){
            $playerUrnId = $data_payload['killerUrn'];
            $playerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$playerUrnId);
            if ($playerInfoRedis){
                $playerInfo = @json_decode($playerInfoRedis,true);
                $largest_multi_kill = (Int)$playerInfo['largest_multi_kill'];
                $killStreakLength = $data_payload['killStreak'];
                switch ($killStreakLength){
                    case 2://2杀
                        $playerInfo['double_kill'] = (Int)$playerInfo['double_kill'] + 1;
                        break;
                    case 3://3杀
                        $double_kill = (Int)$playerInfo['double_kill'] - 1; //取消2杀次数
                        if ($double_kill < 0){
                            $double_kill = 0;
                        }
                        $playerInfo['double_kill'] = (Int)$double_kill;
                        $playerInfo['triple_kill'] = (Int)$playerInfo['triple_kill'] + 1;
                        break;
                    case 4://4杀
                        $triple_kill = (Int)$playerInfo['triple_kill'] - 1; //取消3杀次数
                        if ($triple_kill < 0){
                            $triple_kill = 0;
                        }
                        $playerInfo['triple_kill'] = (Int)$triple_kill;
                        $playerInfo['quadra_kill'] = (Int)$playerInfo['quadra_kill'] + 1;
                        break;
                    case 5://5杀
                        $quadra_kill = (Int)$playerInfo['quadra_kill'] - 1; //取消4杀次数
                        if ($quadra_kill < 0){
                            $quadra_kill = 0;
                        }
                        $playerInfo['quadra_kill'] = (Int)$quadra_kill;
                        $playerInfo['penta_kill'] = (Int)$playerInfo['penta_kill'] + 1;
                        break;
                }
                //大于才替换
                if ($killStreakLength > $largest_multi_kill ){
                    $playerInfo['largest_multi_kill'] = (Int)$killStreakLength;
                }
                //最终结果
                $playerInfoRes = @json_encode($playerInfo,320);
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerUrnId,$playerInfoRes);
            }
        }
        return true;
    }
    //处理玩家最大连杀
    private function conversionPlayerLargestKillingSpree($redis,$battleId,$killer_rel_order_id,$victim_rel_order_id){
        if (!empty($killer_rel_order_id)){
            $killerPlayerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
            if ($killerPlayerInfoRedis) {
                $killerPlayerInfo = @json_decode($killerPlayerInfoRedis, true);
                //当前击杀数量
                $player_current_kill_num = $this->redis_hGet_Or_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num');
                if ($player_current_kill_num) {
                    //最大连杀数量
                    $player_largest_killing_spree_num = $this->redis_hGet_Or_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num');
                    if ($player_largest_killing_spree_num > 0){
                        $player_current_kill_num_now = (Int)$player_current_kill_num + 1;
                        if ($player_current_kill_num_now > $player_largest_killing_spree_num){
                            $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num',$player_current_kill_num_now);
                            $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num',$player_current_kill_num_now);
                            //保存
                            $killerPlayerInfo['largest_killing_spree'] = $player_current_kill_num_now;
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killerPlayerInfo,320));
                        }
                    }
                } else {
                    $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num',1);
                    if ($player_current_kill_num !== 0){
                        $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num',1);
                        $killerPlayerInfo['largest_killing_spree'] = 1;
                        //保存
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killerPlayerInfo,320));
                    }
                }
            }
        }
        //被杀者
        if ($victim_rel_order_id > 0){
            $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$victim_rel_order_id.'_current_kill_num',0);
        }
        return true;
    }
    //执行ws to rest api
    private function refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order){
        //判断是不是修复 并且比赛是不是结束
        if ($this->operate_type == 'xiufu') {
            if ($event_type == 'frames'){
                $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
                if ($match_status_check == 3) {
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'  => $rel_matchId,
                            'battleId'     => $battleId,
                            'battleOrder'  => $battle_order,
                            'event_type'   => 'refresh_match_xiufu',
                            'event_order'  => 0
                        ],
                    ];
                    TaskRunner::addTask($item, 4);
                }
            }
            return true;
        }
        $nowTime = time();
        $refreshTime = $this->redis_Get_Or_Memory($redis,['current', 'battles', $battleId, 'ws_to_api']);
        if ($refreshTime){
            //判断类型
            if ($event_type == 'frames'){
                if (!$is_battle_finished){
                    $diff_time = $nowTime - $refreshTime;
                    if ($diff_time < 5) {
                        return true;
                    }
                }
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        $originId, "", "", 2),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => [
                        'matchId'      => $matchId,
                        'rel_matchId'      => $rel_matchId,
                        'battleId'      => $battleId,
                        'battleOrder'      => $battle_order,
                        'event_type'      => $event_type,
                        'event_order'      => 0
                    ],
                ];
                TaskRunner::addTask($item, 4);
                $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'ws_to_api'],$nowTime);
            }else{
                $event_type_array = ['battle_start','battle_end','battle_pause','battle_unpause','battle_reloaded','elite_announced','elite_spawned','player_spawned','player_kill','player_suicide','elite_kill','building_kill','ward_placed','ward_kill','ward_expired'];
                if (in_array($event_type,$event_type_array)){
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'      => $rel_matchId,
                            'battleId'      => $battleId,
                            'battleOrder'      => $battle_order,
                            'event_type'      => $event_type,
                            'event_order'      => $event_order
                        ],
                    ];
                    TaskRunner::addTask($item, 4);
                }
            }
        }else {
            $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'ws_to_api'],$nowTime);
        }
        return true;
    }
    //获取battle的队伍信息
    private function getBattleTeamsInfoFromMemory($redis,$battleId,$type='all_info'){
        $teams_Id_Info_Array = [];
        $teams_Info_Redis = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        if ($teams_Info_Redis){
            foreach ($teams_Info_Redis as $key => $val){
                $val_array = @json_decode($val,true);
                if ($type == 'id_relation'){
                    $teams_Id_Info_Array[$key] = @$val_array['team_id'];
                }else{
                    $teams_Id_Info_Array[$key] = @$val_array;
                }
            }
        }
        return $teams_Id_Info_Array;
    }
    //处理battle_end
    private function conversionEndMap($redis,$matchInfo,$matchId,$battleId,$data_payload,$event_id,$sourceUpdatedAt){
        $event_type = 'battle_end';
        $ingame_timestamp = intval($data_payload['gameTime']/1000);
        $team = $team_order = $winner_id = null;
        $rel_winningTeam = $data_payload['winningTeamUrn'];
        //获取赢的队伍信息
        $winnerTeamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_winningTeam);
        if ($winnerTeamInfoRedis){
            $winnerTeamInfo = @json_decode($winnerTeamInfoRedis,true);
            $team_order = @$winnerTeamInfo['order'];
            //根据ID 获取team信息
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$winnerTeamInfo['team_id']);
            $team_info = @json_decode($match_team_info_redis,true);
            $team = [
                'team_id' => self::checkAndChangeInt(@$winnerTeamInfo['team_id']),
                'name' => self::checkAndChangeString(@$team_info['name']),
                'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_order)
            ];
            $winner_id = $team_order;
        }
        //结束时间
        //计算结束时间
        $battle_end_at = date("Y-m-d H:i:s",strtotime($sourceUpdatedAt));
        $battle_base = [
            'end_at' => $battle_end_at
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_base);
        //比赛信息
        $match_real_time_info = $this->redis_hGetAll_Or_Memory($redis,['match_real_time_info']);
        //上面计算
        switch ($winner_id){
            case 1:
                $match_real_time_info['team_1_score'] = $match_real_time_info['team_1_score'] + 1;
                break;
            case 2:
                $match_real_time_info['team_2_score'] = $match_real_time_info['team_2_score'] + 1;
                break;
        }
        //计算比赛的胜算
        $statusInfo = WinnerHelper::lolWinnerInfo($match_real_time_info['team_1_score'],$match_real_time_info['team_2_score'],$matchInfo['number_of_games']);
        if($statusInfo['is_finish']==1){
            //$match_real_time_info['status'] = 3;
            $match_real_time_info_update['end_at']= $battle_end_at;
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info_update);
        }
        //event_order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => @$team['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'winner' => $team
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //////////////////////////  获取比赛信息  /////////////////////////////////////
    //获取比赛ID
    public function getMatchIdAndRelMatchId($redis,$originId,$perId){
        $match_id_list = self::getMatchIdByPerId($perId,$originId);
        $rel_identity_id = @$match_id_list['rel_identity_id'];
        $matchId = @$match_id_list['matchId'];
        if ($this->memory_mode){
            $this->matchId = $matchId;
            $this->rel_matchId = $rel_identity_id;
        }
        return [
            'rel_matchId' => $rel_identity_id,
            'matchId' => $matchId
        ];
    }
    //设置比赛状态
    public function setMatchStatus($redis,$matchId){
        if ($this->memory_mode){
            $match_status = $this->match_status;
            if ($match_status){
                return true;
            }
            $this->match_status = 2;
        }
        $matchStatusRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_status";
        $match_status = $redis->get($matchStatusRedisName);
        if (!$match_status){
            $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
            if ($MatchRealTimeInfo){
                $MatchRealTimeInfo->setAttribute('status',2);
                $MatchRealTimeInfo->save();
                $redis->set($matchStatusRedisName,2);
            }
        }
        return true;
    }
    //获取比赛信息
    public function getMatchInfo($redis,$matchId,$socket_time){
        if ($this->memory_mode && !empty($this->matchInfo)){
            return $this->matchInfo;
        }
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $matchInfo = $redis->hGetAll($matchInfoRedisName);
        if (!$matchInfo){
            //获取比赛信息
            $matchInfo = self::getMatchAllInfoByMatchId($matchId);
            if (empty($matchInfo)){
                return 'no_match_info';
            }
            $redis->hMSet($matchInfoRedisName,$matchInfo);
            //setMatchRealTimeInfo
            $this->getMatchRealTimeInfo($redis,$matchId,$socket_time);
            $this->getInitTeamInfo($redis,$matchId,$matchInfo);
            //$this->getInitPlayerInfo($redis,$matchId,$matchInfo);
        }
        //设置比赛
        if ($this->memory_mode){
            $this->matchInfo = $matchInfo;
        }
        return $matchInfo;
    }
    //获取比赛信息
    public function getMatchRealTimeInfo($redis,$matchId,$socket_time)
    {
//        if ($this->memory_mode && !empty($this->matchRealTimeInfo)) {
//            return $this->matchRealTimeInfo;
//        }
        //match_real_time_info
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":".Consts::MATCH_REAL_TIME_INFO;
        $match_real_time_info = $redis->hGetAll($matchRealTimeInfoRedisName);
        if (!$match_real_time_info){
            $match_real_time_info = MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
//            $match_real_time_info['status'] = 2;
//            if (empty(@$match_real_time_info['begin_at'])){
//                $match_real_time_info['begin_at'] = $socket_time;
//            }
            if ($this->operate_type == 'xiufu') {
                $match_real_time_info['status'] = 2;
                $match_real_time_info['team_1_score'] = 0;
                $match_real_time_info['team_2_score'] = 0;
                $match_real_time_info['winner'] = null;
            }
            $redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
        }
        //存内存
        if ($this->memory_mode){
            $this->matchRealTimeInfo = $match_real_time_info;
        }
        return $match_real_time_info;
    }
    //获取比赛信息
    public function getMatchHotDataRunningInfo($redis,$matchId){
        if ($this->memory_mode && !empty($this->match_socket_sub_type)) {
            return $this->match_socket_sub_type;
        }
        $match_socket_sub_typeRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_socket_sub_type";
        $match_socket_sub_type = $redis->get($match_socket_sub_typeRedisName);
        if (empty($match_socket_sub_type)){
            $hotInfo = HotDataRunningMatch::find()->where(['match_id' => $matchId])->asArray()->one();
            $match_socket_sub_type = @$hotInfo['auto_add_sub_type'];
            if (empty($match_socket_sub_type)){
                $match_socket_sub_type = 'auto_add_sub_type-no-data';
            }
            $redis->set($match_socket_sub_typeRedisName,$match_socket_sub_type);
        }
        //存内存
        if ($this->memory_mode){
            $this->match_socket_sub_type = $match_socket_sub_type;
        }
        return $match_socket_sub_type;
    }
    //设置team 各种信息
    public function getInitTeamInfo($redis,$matchId,$matchInfo){
//        if ($this->memory_mode && !empty($this->teams_info)){
//            return $this->teams_info;
//        }
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $teams_info = $redis->hGetAll($matchTeamInfoRedisName);
        if (!$teams_info){
            $Team1Info = Team::find()->where(['id'=>$matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])){
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team1Info['id'],@json_encode($Team1Info,320));
            $Team2Info = Team::find()->where(['id'=>$matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])){
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team2Info['id'],@json_encode($Team2Info,320));
            $teams_info = [
                $Team1Info['id'] =>@json_encode($Team1Info,320),
                $Team2Info['id'] =>@json_encode($Team2Info,320)
            ];
        }
        if ($this->memory_mode){
            $this->teams_info = $teams_info;
            $this->setDataToMemory(['teams_info'],$teams_info);
        }
        return $teams_info;
    }
    //初始化玩家
    public function getInitPlayerInfo($redis,$matchId,$matchInfo){
//        if ($this->memory_mode && !empty($this->player_info)){
//            return $this->player_info;
//        }
        //根据队伍ID 获取player 战队选手快照   选手表
        $playerInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_info";
        $player_info = $redis->hGetAll($playerInfoRedisName);
        if (!$player_info){
            $team_ids = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
            $playersInfo = Team::find()->alias('ts')->select('p.id,p.nick_name')
                ->leftJoin('team_player_relation as tpr','ts.id = tpr.team_id')
                ->leftJoin('player as p','p.id = tpr.player_id')
                ->andWhere(['in', 'ts.id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val){
                $player_info[@$val['id']] = @json_encode($val, 320);
            }
            $redis->hMSet($playerInfoRedisName,$player_info);
        }
        if ($this->memory_mode){
            $this->player_info = $player_info;
        }
        return $player_info;
    }
    //元素龙 刷新
    private function getElitesStatusNew($redis,$battleId,$duration){
        //计算 elites_status
        $enumIngameGoalInfoList = $this->getEnumIngameGoalInfo($redis);
        //enum_socket_game_configure
        $enum_socket_game_configure_info = $this->enum_socket_game_configure;
        //rift_herald
        $rift_herald_first_time = $enum_socket_game_configure_info['rift_herald_first_time'];
        $rift_herald_quit_time = $enum_socket_game_configure_info['rift_herald_quit_time'];
        $rift_herald_reborn_time = $enum_socket_game_configure_info['rift_herald_reborn_time'];
        $rift_herald_exited = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited');
        if ($rift_herald_exited){
            $rift_herald_status_val = 'exited';
            $rift_herald_spawntimer = 0;
        }else{
            $rift_herald_status_val = 'alive';
            $rift_herald_spawntimer = 0;
            if ($duration < $rift_herald_first_time){
                $rift_herald_status_val = 'not_spawned';
                $rift_herald_spawntimer = $rift_herald_first_time - $duration;
            }elseif ($duration > $rift_herald_quit_time){
                $rift_herald_status_val = 'exited';
                $rift_herald_spawntimer = 0;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
            }else{
                $rift_herald_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time');
                if ($rift_herald_kill_time){
                    $rift_herald_spawntimer_time = $duration - $rift_herald_kill_time;
                    if ($rift_herald_spawntimer_time < $rift_herald_reborn_time){
                        $rift_herald_status_val = 'not_spawned';
                        $rift_herald_spawntimer = $rift_herald_reborn_time - $rift_herald_spawntimer_time;
                    }
                }
            }
        }
        //rift_herald Info   1
        $rift_herald_array = @json_decode($enumIngameGoalInfoList[1],true);
        $rift_herald_name_val = @$rift_herald_array['ingame_obj_name'];
        $rift_herald_name_cn_val = @$rift_herald_array['ingame_obj_name_cn'];
        $rift_herald_status = [
            'status' => $rift_herald_status_val,
            'name' => self::checkAndChangeString($rift_herald_name_val),
            'name_cn' => self::checkAndChangeString($rift_herald_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($rift_herald_spawntimer)
        ];
        //dragon_status
        $dragon_status_status_val = 'alive';
        $dragon_status_spawntimer = 0;
        $dragon_status_name = null;
        $dragon_status_name_cn= null;
        //查询第一只是哪只龙
        $elite_id_now = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'elite_ancient_now_id']);
        if ($elite_id_now){
            $elite_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'elite_ancient'],$elite_id_now);
            if ($elite_info_redis){
                $elite_info_array = @json_decode($elite_info_redis,true);
                $elite_no_spawned_keys = @array_keys($elite_info_array);
                $elite_no_spawned_duration = @end($elite_no_spawned_keys);//最后一个Key
                $elite_no_spawned_info_array = @explode('-',$elite_info_array[$elite_no_spawned_duration]);
                $elite_no_spawned_elite_status = @$elite_no_spawned_info_array[0];
                if ($elite_no_spawned_elite_status == 0){//0 是预计刷新 1已结刷新 复活了 2死亡
                    $spawn_time = (Int)@$elite_no_spawned_info_array[1] - $duration;
                    if ($spawn_time > 0){
                        $dragon_status_status_val = 'not_spawned';
                        $dragon_status_spawntimer = $spawn_time;
                    }
                }
                $elite_info = @json_decode($enumIngameGoalInfoList[$elite_id_now],true);
                $dragon_status_name = self::checkAndChangeString(@$elite_info['ingame_obj_name']);
                $dragon_status_name_cn= self::checkAndChangeString(@$elite_info['ingame_obj_name_cn']);
            }
        }
        $dragon_status = [
            'status' => $dragon_status_status_val,
            'name' => $dragon_status_name,
            'name_cn' => $dragon_status_name_cn,
            'spawntimer' => self::checkAndChangeInt($dragon_status_spawntimer)
        ];
        //baron_nashor_status  7
        $baron_nashor_first_time = $enum_socket_game_configure_info['baron_nashor_first_time'];
        $baron_nashor_reborn_time = $enum_socket_game_configure_info['baron_nashor_reborn_time'];
        $baron_nashor_spawntimer = 0;
        $baron_nashor_array = @json_decode($enumIngameGoalInfoList[7],true);
        $baron_nashor_name_val = @$baron_nashor_array['ingame_obj_name'];
        $baron_nashor_name_cn_val = @$baron_nashor_array['ingame_obj_name_cn'];
        $baron_nashor_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time');
        if ($baron_nashor_kill_time){
            $baron_nashor_spawntimer_time = $duration - $baron_nashor_kill_time;
            if ($baron_nashor_spawntimer_time > $baron_nashor_reborn_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = 0;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_reborn_time - $baron_nashor_spawntimer_time;
            }
        }else{
            if ($duration > $baron_nashor_first_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = 0;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_first_time - $duration;
            }
        }
        $baron_nashor_status = [
            'status' => $baron_nashor_status_val,
            'name' => self::checkAndChangeString($baron_nashor_name_val),
            'name_cn' => self::checkAndChangeString($baron_nashor_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($baron_nashor_spawntimer)
        ];
        $elites_status = [
            'rift_herald_status' => $rift_herald_status,
            'dragon_status' => $dragon_status,
            'baron_nashor_status' => $baron_nashor_status
        ];
        return $elites_status;
    }
    //处理队伍的杀龙信息
    private function conversionTeamDragonKills($drakeInfoList,$team_dragon_kills_detail){
        $dragon_kills_detail_res = null;
        if ($team_dragon_kills_detail){
            foreach ($team_dragon_kills_detail as $val_d){
                $drakeInfoRedis = $drakeInfoList[$val_d['drake']];
                if ($drakeInfoRedis){
                    $drakeInfo = @json_decode($drakeInfoRedis,true);
                    $drakeInfoRes = [
                        'ingame_timestamp' => self::checkAndChangeInt($val_d['date']),
                        'dragon_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                        'dragon_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn'])
                    ];
                    $dragon_kills_detail_res[] = $drakeInfoRes;
                }
            }
        }
        return $dragon_kills_detail_res;
    }
    //处理队伍推塔信息
    private function conversionTeamBuildingStatus($redis,$battleId,$match_battle_duration,$team_urn,$building_status,$team_building_status_nexus){
        $enum_socket_game_configure_res = $this->enum_socket_game_configure;
        $inhibitor_reborn_time = $enum_socket_game_configure_res['inhibitor_reborn_time'];
        $team_building_status_res = [];
        foreach ($building_status as $key_one => $val_one){
            if ($key_one == 'nexus'){ //最后一针 nexus  为 false
                $team_building_status_res['nexus']['is_alive'] = $team_building_status_nexus;
            }
            if ($key_one == 'turrets'){
                foreach ($val_one as $key_two => $val_two){
                    $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                }
            }
            if ($key_one == 'inhibitors'){
                foreach ($val_one as $key_two => $val_two){
                    $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                    //设置初始值 后面修改
                    $team_building_status_res[$key_one][$key_two]['respawntimer'] = null;
                    //查询这个是否已经被摧毁了
                    $inhibitor_killed_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$team_urn.'_'.$key_two.'_kill_time');
                    if ($inhibitor_killed_time){
                        $inhibitor_respawntimer = $match_battle_duration - (Int)$inhibitor_killed_time;
                        if ($inhibitor_respawntimer >= $inhibitor_reborn_time){
                            //说明已经复活了, 去掉击杀的时间 respawntimer = null;
                            $team_building_status_res[$key_one][$key_two]['is_alive'] = true;
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$team_urn.'_'.$key_two.'_kill_time',null);
                        }else{
                            $team_building_status_res[$key_one][$key_two]['respawntimer'] = (Int)($inhibitor_reborn_time - $inhibitor_respawntimer);
                        }
                    }
                }
            }
        }
        return $team_building_status_res;
    }
    /////////////////  基础信息   /////////////////////
    //获取英雄信息
    private function getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id){
        if ($this->memory_mode && !empty($this->match_all_champion_websocket[$champions_id])){
            return $this->match_all_champion_websocket[$champions_id];
        }
        $champion = null;
        $championList = $this->getChampionList($redis);
        //$championInfoRedis = $redis->hGet('lol:champions:list:'.$metadata_origin_id,$champions_id);
        $championInfoRedis = @$championList[$champions_id];
        if ($championInfoRedis){
            $championInfo = @json_decode($championInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $redis->get("lol:champions:list:unknown");
            $championInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }
        if ($this->memory_mode){
            $this->match_all_champion_websocket[$champions_id] = $champion;
        }
        return $champion;
    }
    //获取英雄 列表 信息
    public function getChampionList($redis){
        if ($this->memory_mode) {
            $strValue = $this->lol_champion_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('lol:champions:list');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->lol_champion_list = $returnValue;
            }
        }
        return $returnValue;
    }
    //获取英雄 relation id 信息
    public function getChampionRelationIdList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->lol_champion_relation_id_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll('lol:champions:relation:'.$originId);
        $returnValue = $redis->hGetAll('lol:champions:external_id_relation_list');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->lol_champion_relation_id_list = $returnValue;
            }
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getChampionUnknownId($redis){
        $champion_unknown_redis = $redis->get('lol:champions:list:unknown');
        $champion_unknown_info = @json_decode($champion_unknown_redis,true);
        return @$champion_unknown_info['id'];
    }
    //获取Lol 英雄ID
    private function getLolChampionId($redis,$metadata_origin_id,$rel_championId){
        $championRelationIdList = $this->getChampionRelationIdList($redis,$metadata_origin_id);
        $champions_id = @$championRelationIdList[$rel_championId];
        if (!$champions_id){
            $champions_id = $this->getChampionUnknownId($redis);
        }
        return (Int)$champions_id;
    }
    //获取LOL Items Relation
    public function getLolItemsRelationList($redis,$originId)
    {
        if ($this->memory_mode) {
            //$strValue = $this->lol_items_relation[$originId];
            $strValue = $this->lol_items_relation;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:items:relation:".$originId);
        $returnValue = $redis->hGetAll("lol:items:external_id_relation_list");
        if ($this->memory_mode) {
            //return $this->lol_items_relation[$originId] = $returnValue;
            $this->lol_items_relation = $returnValue;
        }
        return $returnValue;
    }
    //获取LOL Items Relation
    public function getLolItemsList($redis,$originId)
    {
        if ($this->memory_mode) {
            $strValue = $this->lol_items_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:items:list:".$originId);
        $returnValue = $redis->hGetAll("lol:items:list");
        if ($this->memory_mode) {
            $this->lol_items_list = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getLolItemsUnknownId($redis){
        if ($this->memory_mode) {
            $strValue = $this->lol_item_unknown_id;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $item_unknown_id = null;
        $item_unknown_redis = $redis->get('lol:items:list:unknown');
        $item_unknown_info = @json_decode($item_unknown_redis,true);
        $item_unknown_id = @$item_unknown_info['id'];
        if ($this->memory_mode) {
            $this->lol_item_unknown_id = $item_unknown_id;
        }
        return $item_unknown_id;
    }
    //获取未知的符文ID
    private function getKeystoneUnknownId($redis){
        $rune_unknown_redis = $redis->get('lol:rune:list:unknown');
        $rune_unknown_info = @json_decode($rune_unknown_redis,true);
        return @$rune_unknown_info['id'];
    }
    //召唤师技能 name relation id
    public function getSummonerSpellNameRelationIdList($redis,$originId){

        if ($this->memory_mode) {
            //$strValue = $this->lol_summoner_spell_name_relation_id_list[$originId];
            $strValue = $this->lol_summoner_spell_name_relation_id_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll('lol:summoner_spell:name_relation_id:'.$originId);
        $returnValue = $redis->hGetAll('lol:summoner_spell:external_name_relation_list');
        if ($this->memory_mode) {
            //return $this->lol_summoner_spell_name_relation_id_list[$originId] = $returnValue;
            $this->lol_summoner_spell_name_relation_id_list = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 列表
    public function getSummonerSpellInfoList($redis,$originId){
        if ($this->memory_mode) {
            //$strValue = $this->lol_summoner_spell_list[$originId];
            $strValue = $this->lol_summoner_spell_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll('lol:summoner_spell:list:'.$originId);
        $returnValue = $redis->hGetAll('lol:summoner_spell:list');
        if ($this->memory_mode) {
            //return $this->lol_summoner_spell_list[$originId] = $returnValue;
            $this->lol_summoner_spell_list = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 webscoket 信息
    public function getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_summoner_spells){
        $player_summoner_spells_res = [];
        foreach ($player_summoner_spells as $val_s){
            $player_summoner_spell_info = [];
            $summoner_spell_id_is_unknown = $val_s['is_unknown'];
            if ($summoner_spell_id_is_unknown == 1){
                $player_summoner_spell_info['summoner_spell_id'] = (int)@$val_s['unknown_info']['id'];
                $player_summoner_spell_info['name'] = (String)@$val_s['unknown_info']['name'];
                $player_summoner_spell_info['name_cn'] = (String)@$val_s['unknown_info']['name_cn'];
                $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$val_s['unknown_info']['external_id']);
                $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$val_s['unknown_info']['external_name']);
                $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$val_s['unknown_info']['slug']);
                $summoner_spell_info_image = !empty(@$val_s['unknown_info']['image']) ? (string)@$val_s['unknown_info']['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                $player_summoner_spell_info['cooldown'] = (Int)$val_s['cooldown'];
            }else{
                $summoner_spell_info_redis = @$summonerSpellInfoList[$val_s['spell_id']];
                if ($summoner_spell_info_redis){
                    $summoner_spell_info = @json_decode($summoner_spell_info_redis,320);
                    $player_summoner_spell_info['summoner_spell_id'] = (int)@$summoner_spell_info['id'];
                    $player_summoner_spell_info['name'] = (String)@$summoner_spell_info['name'];
                    $player_summoner_spell_info['name_cn'] = (String)@$summoner_spell_info['name_cn'];
                    $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$summoner_spell_info['external_id']);
                    $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$summoner_spell_info['external_name']);
                    $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$summoner_spell_info['slug']);
                    $summoner_spell_info_image = !empty(@$summoner_spell_info['image']) ? (string)@$summoner_spell_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                    $player_summoner_spell_info['cooldown'] = (Int)$val_s['cooldown'];
                }
            }
            $player_summoner_spells_res[] = $player_summoner_spell_info;
        }
        return $player_summoner_spells_res;
    }
    //item websocket 信息
    public function getWebSocketPlayerItemsList($lolItemsList,$player_items_rel){
        $player_items = [];
        foreach ($player_items_rel as $key_item => $val_item){
            $player_items_val = [];
            if ($val_item){
                $item_info_data = @$lolItemsList[$val_item['item_id']];
                if ($item_info_data){
                    $item_info = @json_decode($item_info_data,true);
                    $player_items_val['item_id'] = (int)$item_info['id'];
                    $player_items_val['name'] = (string)$item_info['name'];
                    $player_items_val['name_cn'] = (string)$item_info['name_cn'];
                    $player_items_val['external_id'] = self::checkAndChangeString($item_info['external_id']);
                    $player_items_val['external_name'] = self::checkAndChangeString($item_info['external_name']);
                    $player_items_val['total_cost'] = (int)$item_info['total_cost'];
                    $player_items_val['is_trinket'] = @$item_info['is_trinket'] == 1 ? true : false;
                    $player_items_val['is_purchasable'] = @$item_info['is_purchasable'] == 1 ? true : false;
                    $player_items_val['slug'] = (string)$item_info['slug'];
                    $player_item_image = @$item_info['image'] ? @$item_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_items_val['image'] = $player_item_image;
                    $player_items_val['purchase_time'] = (Int)$val_item['purchase_time'];
                    $player_items_val['cooldown'] = (Int)$val_item['cooldown'];
                }
            }
            $key_name_item = $key_item + 1;
            $player_items['slot_'.$key_name_item] = $player_items_val;
        }
        return $player_items;
    }
    //获取符文列表
    private function getRunesList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->lol_runes_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:rune:list:".$originId);
        $returnValue = $redis->hGetAll("lol:rune:list");
        if ($this->memory_mode) {
            $this->lol_runes_list = $returnValue;
        }
        return $returnValue;
    }
    //获取符文关系列表
    private function getRunesRelationList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->lol_runes_relation_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:rune:relation:".$originId);
        $returnValue = $redis->hGetAll("lol:rune:external_id_relation_list");
        if ($this->memory_mode) {
            $this->lol_runes_relation_list = $returnValue;
        }
        return $returnValue;
    }
    //获取未知符文
    private function getRunesUnknownId($redis){
        $rune_unknown_redis = $redis->get('lol:rune:list:unknown');
        $rune_unknown_info = @json_decode($rune_unknown_redis,true);
        return @$rune_unknown_info['id'];
    }
    //获取地图信息
    private function getMapInfoByRedisOrMemory($redis,$map_id){
        $map = null;
        if ($this->memory_mode && !empty($this->map)){
            return $this->map;
        }
        $mapInfoRedis = $redis->hGet(Consts::GAME_MAPS.":lol",$map_id);
        if ($mapInfoRedis){
            $mapInfo = @json_decode($mapInfoRedis,true);
            $map_is_default = @$mapInfo['is_default'] == 1 ? true : false;
            $map_image = [
                'square_image' => null,
                'rectangle_image' => null,
                'thumbnail' => (String)@$mapInfo['image']
            ];
            $map = [
                'map_id' => (Int)@$mapInfo['id'],
                'name' => (String)@$mapInfo['name'],
                'name_cn' => self::checkAndChangeString(@$mapInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$mapInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$mapInfo['external_name']),
                'short_name' => self::checkAndChangeString(@$mapInfo['short_name']),
                'map_type' => self::checkAndChangeString(@$mapInfo['map_type']),
                'map_type_cn' => self::checkAndChangeString(@$mapInfo['map_type_cn']),
                'is_default' => $map_is_default,
                'slug' => self::checkAndChangeString(@$mapInfo['slug']),
                'image' => $map_image
            ];
        }
        if ($this->memory_mode){
            $this->map = $map;
        }
        return $map;
    }
    //获取符文信息
    private function getWebSocketPlayerKeystoneInfo($redis,$originId,$player_id,$keystone_id){
        $keyString = 'player_keystone_websocket';
        if ($this->memory_mode) {
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$player_id];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $keystone = null;
        //$keystone_info_redis = $redis->hGet("lol:rune:list:".$originId,$keystone_id);
        $runesList = $this->getRunesList($redis,$originId);
        $keystone_info_redis = @$runesList[$keystone_id];
        if ($keystone_info_redis){
            $keystone_image = null;
            $keystone_info = @json_decode($keystone_info_redis,true);
            if ($keystone_info['image']){
                $keystone_image = $keystone_info['image']."?x-oss-process=image/resize,m_fixed,h_50,w_50";
            }
            $keystone = [
                'rune_id' => (int)$keystone_info['id'],
                'name' => self::checkAndChangeString(@$keystone_info['name']),
                'name_cn' => self::checkAndChangeString(@$keystone_info['name_cn']),
                'external_id' => self::checkAndChangeString(@$keystone_info['external_id']),
                'external_name' => self::checkAndChangeString(@$keystone_info['external_name']),
                'path_name' => self::checkAndChangeString(@$keystone_info['path_name']),
                'path_name_cn' => self::checkAndChangeString(@$keystone_info['path_name_cn']),
                'slug' => self::checkAndChangeString(@$keystone_info['slug']),
                'image' => $keystone_image
            ];
        }
        if ($this->memory_mode) {
            if (!empty($keystone)){
                $this->current[$keyString][$player_id] = $keystone;
            }
        }
        return $keystone;
    }
    //获取EnumIngameGoal信息
    private function getEnumIngameGoalInfo($redis){
        if ($this->memory_mode) {
            $strValue = $this->enum_ingame_goal_info;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('enum:ingame_goal');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->enum_ingame_goal_info = $returnValue;
            }
        }
        return $returnValue;
    }
    //socket录入服务器配置
    private function setSocketGameConfigure($game_type){
        $enum_socket_lol = $this->enum_socket_game_configure;
        if (empty($enum_socket_lol)){
            $this->enum_socket_game_configure = $this->getSocketGameConfigure($game_type);
        }
        return true;
    }
    //////////////////////////    redis 和 内存      ///////////////////////////////////
    //获取数组 key
    public function redis_hKeys_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_keys($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hKeys($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组 key
    public function redis_hVals_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_values($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hVals($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetTeamAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 2){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetPlayersAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 10 ){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取hash 单条
    public function redis_hGet_Or_Memory($redis,$keys,$field)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$field];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGet($keyString,$field);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
//            if (array_key_exists($keyString, $this->current)){
//                $this->current[$keyString][$field] = $returnValue;
//            }else{
//                if (!empty($returnValue)){
//                    $this->current[$keyString][$field] = $returnValue;
//                }
//            }
            //重新赋值所有
            $allData = $redis->hGetAll($keyString);
            if ($allData){
                $this->current[$keyString] = $allData;
            }
        }
        return $returnValue;
    }
    //获取 get 单条
    public function redis_Get_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->get($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取长度
    public function redis_hLen_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return count($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hLen($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue){
                $this->setDataToMemory($keys,$returnAllValue);
            }
        }
        return $returnValue;
    }
    //设置缓存 内存
    public function redis_hMSet_And_Memory($redis,$keys,$values)
    {
        if ($values){
            $matchId = $this->matchId;
            $keyString = implode(":", $keys);
            $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
            $redis->hMSet($keyString,$values);
            if ($this->memory_mode) {
                $memory_values = $redis->hGetAll($keyString);
                $keyString = implode("_", $keys);
                $this->current[$keyString] = $memory_values;
            }
        }
        return true;
    }
    //设置缓存 内存
    public function redis_hSet_And_Memory($redis,$keys,$filedKey,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->hSet($keyString,$filedKey,$values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString][$filedKey] = $values;
        }
        return true;
    }
    //设置缓存 内存
    public function redis_Set_And_Memory($redis,$keys,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->set($keyString, $values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $values;
        }
        return true;
    }
    //设置缓存 单独设置 内存
    public function setDataToMemory($keys,$value){
        if ($this->memory_mode && $value) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $value;
        }
        return true;
    }
    //保存到DB
    public function saveDataBase($data){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $data);
        $db_websoket->execute();
        return true;
    }
    //保存到Kafka
    public function saveKafka($data){
        if ($this->operate_type == 'xiufu'){
            return true;
        }
        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_LOL'),$data);
    }
    //根据名字获取 ID
    private static function getGoalIdByBayesName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','bayes_name',$name])->asArray()->one();
            if($goal){
                return $goal['id'];
            }
        }
    }
    //清空redis和内存
    public function delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id){
        //内存清空
        $this->current = [];
        //清空redis
        //删除关联关系
        $redis_match_lol_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":lol:".$origin_id;
        $redis->hdel($redis_match_lol_relation,$rel_match_id);
        //新的结构删除
        $match_all_info_list = $redis->Keys('match_all_info:'.$match_id."*");
        if ($match_all_info_list) {
            foreach ($match_all_info_list as $item_mai) {
                $redis->del($item_mai);
            }
        }
        return true;
    }
    //回档删除
    private function delRedisAndMemoryByRollBack($redis,$match_id,$battleId,$RollBackTime){
        $ingame_timestamp = intval($RollBackTime/1000);
        //内存清空
        $this->current = [];
        //新的结构删除
        $keysName = Consts::MATCH_ALL_INFO.':'.$match_id.':current:battles:'.$battleId.'*';
        $battle_now_keys = $redis->Keys($keysName);
        if ($battle_now_keys) {
            foreach ($battle_now_keys as $item_keys) {
                if (strstr($item_keys,'roll_back_info')){
                    continue;
                }
                if (strstr($item_keys,'base')){
                    continue;
                }
                if (strstr($item_keys,'ws_to_api')){
                    continue;
                }
                $redis->del($item_keys);
            }
        }
        //删除数据库中的回档事件之后的事件
        MatchBattleEventLol::deleteAll(['and',['=','battle_id',(Int)$battleId],['>=','ingame_timestamp',$ingame_timestamp]]);
        $MatchBattleExtLol = MatchBattleExtLol::findOne(['id' => $battleId]);
        if($MatchBattleExtLol){
            $MatchBattleExtLol->setAttributes([
                'first_blood_p_tid'=>null,
                'first_blood_time'=>null,
                'first_to_5_kills_p_tid'=>null,
                'first_to_5_kills_time'=>null,
                'first_to_10_kills_p_tid'=>null,
                'first_to_10_kills_time'=>null,
                'first_turret_p_tid'=>null,
                'first_turret_time'=>null,
                'first_inhibitor_p_tid'=>null,
                'first_inhibitor_time'=>null,
                'first_rift_herald_p_tid'=>null,
                'first_rift_herald_time'=>null,
                'first_dragon_p_tid'=>null,
                'first_dragon_time'=>null,
                'first_baron_nashor_p_tid'=>null,
                'first_baron_nashor_time'=>null,
                'first_elder_dragon_p_tid'=>null,
                'first_elder_dragon_time'=>null,
                'gold_diff_timeline'=>null,
                'experience_diff_timeline'=>null,
                'faction'=>null,
                'first_blood_detail'=>null,
                'first_to_5_detail'=>null,
                'first_to_10_detail'=>null,
                'first_turret_detail'=>null,
                'first_inhibitor_detail'=>null,
                'first_rift_herald_detail'=>null,
                'first_dragon_detail'=>null,
                'first_baron_nashor_detail'=>null,
                'first_elder_dragon_detail'=>null,
                'elites_status'=>null
            ]);
            $MatchBattleExtLol->save();
        }
        return true;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
