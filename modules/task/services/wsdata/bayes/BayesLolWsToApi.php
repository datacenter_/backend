<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchComingSocketOrder;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class BayesLolWsToApi extends HotBase
{
    private static $instance;
    public static function run($tagInfo, $taskInfo)
    {
        $bayesLolWsToApi = self::getInstance();
        $redis = $bayesLolWsToApi->redis;
        $info       = json_decode($taskInfo['params'], true);
        $matchId = @$info['matchId'];
        $rel_matchId = @$info['rel_matchId'];
        $battleId = @$info['battleId'];
        $battleOrder = @$info['battleOrder'];
        $event_type = @$info['event_type'];
        $event_order = @$info['event_order'];
        if (!$matchId || !$battleId) {
            return false;
        }
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId;
        $redis_base_name_current = $redis_base_name.":current:";
        //判断是不是 重抓，并且统一覆盖
        if ($event_type == 'refresh_match_xiufu'){
            $matchConversionInfo   = $bayesLolWsToApi->conversionMatch($redis,$redis_base_name,$matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battles = [];
            //获取所有的battle
            $match_battles = $redis->hGetAll($redis_base_name_current."match_battles");
            if ($match_battles){
                foreach ($match_battles as $key => $item){
                    $battle_info_array = explode('-',$item);
                    $battle_id_ref = $battle_info_array[0];
                    $battlesConversionInfoFrames = $bayesLolWsToApi->conversionBattlesFrames($redis,$redis_base_name_current,$battle_id_ref);
                    $battlesConversionInfoFrames['battle_id'] = $battle_id_ref;
                    //获取events
                    $battlesConversionInfoEvents = $bayesLolWsToApi->conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battle_id_ref,$battleOrder,$event_type,$event_order);
                    $battlesConversionInfoFrames['events'] = $battlesConversionInfoEvents;
                    $battles[] = $battlesConversionInfoFrames;
                }
            }
            $infoFormat['battles'] = $battles;
            //保存
            $bayesLolWsToApi->refreshMatchAndBattle($matchId, $battleId,$infoFormat,'refresh_match_xiufu');
        }elseif ($event_type == 'frames'){
            $save_match_version = $redis->get($redis_base_name.":save_match_version");
            if (!$save_match_version){
                $gameVersion = $redis->hGet($redis_base_name.":base_info",'game_version');
                if ($gameVersion){
                    $MatchBase = Match::find()->where(['id'=>$matchId])->one();
                    $MatchBase->setAttribute('game_version',$gameVersion);
                    $MatchBase->save();
                    $redis->set($redis_base_name.":save_match_version",$gameVersion);
                }
            }
            $matchConversionInfo   = $bayesLolWsToApi->conversionMatch($redis,$redis_base_name,$matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battlesConversionInfoFrames = $bayesLolWsToApi->conversionBattlesFrames($redis,$redis_base_name_current,$battleId);
            $infoFormat['battle'] = $battlesConversionInfoFrames;
            //保存
            $bayesLolWsToApi->refreshMatchAndBattle($matchId, $battleId,$infoFormat);
            //判断是不是结束了
            if($infoFormat['match']['status'] == 3){
                self::refreshApi($matchId,4);
            }
        }else{//events 事件
            $battlesConversionInfoEvents = $bayesLolWsToApi->conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battleId,$battleOrder,$event_type,$event_order);
            $battle['events'] = $battlesConversionInfoEvents;
            //保存指定的events
            if(isset($battle['events']) && count($battle['events']) > 0){
                BattleService::setBattleInfo($battleId,'lol',$battle['events'],BattleService::DATA_TYPE_EVENTS);
            }
            //如果是battle_end 保存一下
            if ($event_type == 'battle_end'){
                self::refreshApi($matchId,4);
            }
        }
        return $battleId;
    }
    //match 的信息
    public function conversionMatch($redis,$redis_base_name)
    {
        $matchinfoFormat = [];
        $match_real_time_info_redis = $redis->hGetAll($redis_base_name.':match_real_time_info');
        if ($match_real_time_info_redis){
            $matchinfoFormat           = [
                'begin_at'             => @$match_real_time_info_redis['begin_at'],
                'end_at'               => @$match_real_time_info_redis['end_at'],
                'status'               => @$match_real_time_info_redis['status'],
                'team_1_score'         => @$match_real_time_info_redis['team_1_score'],
                'team_2_score'         => @$match_real_time_info_redis['team_2_score'],
                'winner'               => @$match_real_time_info_redis['winner'],
                'is_battle_detailed'   => 1
            ];
        }
        return $matchinfoFormat;
    }
    //battle  frames 信息
    public function conversionBattlesFrames($redis,$redis_base_name_current,$battleId){
        $battleFramesInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => '',
            'ban_pick' => ''
        ];
        //base
        $battleFramesInfoBase = [];
        //获取base数据
        $battleRedisInfoBase = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":base");
        if ($battleRedisInfoBase){
            $battleFramesInfoBase = [
                'game' => @$battleRedisInfoBase['game'],
                'is_draw' => @$battleRedisInfoBase['is_draw'],
                'is_forfeit' => @$battleRedisInfoBase['is_forfeit'],
                'is_default_advantage' => @$battleRedisInfoBase['is_default_advantage'],
                'status' => @$battleRedisInfoBase['status'],
                'order' => @$battleRedisInfoBase['order'],
                'begin_at' => @$battleRedisInfoBase['begin_at'],
                'end_at' => @$battleRedisInfoBase['end_at'],
                'duration' => (string)@$battleRedisInfoBase['duration'],
                'map' => @$battleRedisInfoBase['map'],
                'winner' => @$battleRedisInfoBase['winner'],
                'is_battle_detailed' => @$battleRedisInfoBase['is_battle_detailed'],
                'flag' => 1,
                'deleted' => 2,
                'deleted_at' => null
            ];
        }
        $battleFramesInfo['base'] = $battleFramesInfoBase;
        //detail
        $battleFramesInfoDetail = [];
        //获取detail信息
        $battleRedisInfoDetail = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":detail");
        if ($battleRedisInfoDetail){
            //$gold_diff_timeline
            $gold_diff_timeline = $redis->get($redis_base_name_current."battles:".$battleId.":gold_diff_timeline");
            if (!$gold_diff_timeline){
                $gold_diff_timeline = null;
            }
            //$experience_diff_timeline
            $experience_diff_timeline = $redis->get($redis_base_name_current."battles:".$battleId.":experience_diff_timeline");
            if (!$experience_diff_timeline){
                $experience_diff_timeline = null;
            }
            //组合数据
            $battleFramesInfoDetail = [
                'is_confirmed' => @$battleRedisInfoDetail['is_confirmed'],
                'is_pause' => @$battleRedisInfoDetail['is_pause'],
                'is_live' => @$battleRedisInfoDetail['is_live'],
                'first_blood_p_tid' => @$battleRedisInfoDetail['first_blood_p_tid'],
                'first_blood_time' => @$battleRedisInfoDetail['first_blood_time'],
                'first_to_5_kills_p_tid' => @$battleRedisInfoDetail['first_to_5_kills_p_tid'],
                'first_to_5_kills_time' => @$battleRedisInfoDetail['first_to_5_kills_time'],
                'first_to_10_kills_p_tid' => @$battleRedisInfoDetail['first_to_10_kills_p_tid'],
                'first_to_10_kills_time' => @$battleRedisInfoDetail['first_to_10_kills_time'],
                'first_turret_p_tid' => @$battleRedisInfoDetail['first_turret_p_tid'],
                'first_turret_time' => @$battleRedisInfoDetail['first_turret_time'],
                'first_inhibitor_p_tid' => @$battleRedisInfoDetail['first_inhibitor_p_tid'],
                'first_inhibitor_time' => @$battleRedisInfoDetail['first_inhibitor_time'],
                'first_rift_herald_p_tid' => @$battleRedisInfoDetail['first_rift_herald_p_tid'],
                'first_rift_herald_time' => @$battleRedisInfoDetail['first_rift_herald_time'],
                'first_dragon_p_tid' => @$battleRedisInfoDetail['first_dragon_p_tid'],
                'first_dragon_time' => @$battleRedisInfoDetail['first_dragon_time'],
                'first_baron_nashor_p_tid' => @$battleRedisInfoDetail['first_baron_nashor_p_tid'],
                'first_baron_nashor_time' => @$battleRedisInfoDetail['first_baron_nashor_time'],
                'first_elder_dragon_p_tid' => @$battleRedisInfoDetail['first_elder_dragon_p_tid'],
                'first_elder_dragon_time' => @$battleRedisInfoDetail['first_elder_dragon_time'],
                'gold_diff_timeline' => $gold_diff_timeline,
                'experience_diff_timeline' => $experience_diff_timeline,
                'first_blood_detail' => @$battleRedisInfoDetail['first_blood_detail'],
                'first_to_5_detail' => @$battleRedisInfoDetail['first_to_5_detail'],
                'first_to_10_detail' => @$battleRedisInfoDetail['first_to_10_detail'],
                'first_turret_detail' => @$battleRedisInfoDetail['first_turret_detail'],
                'first_inhibitor_detail' => @$battleRedisInfoDetail['first_inhibitor_detail'],
                'first_rift_herald_detail' => @$battleRedisInfoDetail['first_rift_herald_detail'],
                'first_dragon_detail' => @$battleRedisInfoDetail['first_dragon_detail'],
                'first_baron_nashor_detail' => @$battleRedisInfoDetail['first_baron_nashor_detail'],
                'first_elder_dragon_detail' => @$battleRedisInfoDetail['first_elder_dragon_detail'],
                'elites_status' => @$battleRedisInfoDetail['elites_status']
            ];
        }
        $battleFramesInfo['detail'] = $battleFramesInfoDetail;
        //static
        $battleFramesInfoStatic = [];
        $battleRedisInfoStatic = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":teams");
        if ($battleRedisInfoStatic){
            foreach ($battleRedisInfoStatic as $team_val){
                $team_info_array = @json_decode($team_val,true);
                if (!empty(@$team_info_array['team_id']) && !empty(@$team_info_array['order']) && !empty(@$team_info_array['battle_id'])){
                    $dragon_kills_detail_res = null;
                    if ($team_info_array['dragon_kills_detail']){
                        $dragon_kills_detail_res = @json_encode($team_info_array['dragon_kills_detail'],320);
                    }
                    $team_info = [
                        'order' => @$team_info_array['order'],
                        'battle_id' => @$team_info_array['battle_id'],
                        'score' => @$team_info_array['score'],
                        'identity_id' => @$team_info_array['identity_id'],
                        'rel_identity_id' => (string)@$team_info_array['rel_identity_id'],
                        'team_id' => @$team_info_array['team_id'],
                        'name' => @$team_info_array['name'],
                        'faction' => @$team_info_array['faction'],
                        'kills' => @$team_info_array['kills'],
                        'deaths' => @$team_info_array['deaths'],
                        'assists' => @$team_info_array['assists'],
                        'wards_purchased' => @$team_info_array['wards_purchased'],
                        'wards_placed' => @$team_info_array['wards_placed'],
                        'wards_kills' => @$team_info_array['wards_kills'],
                        'gold' => @$team_info_array['gold'],
                        'gold_diff' => @$team_info_array['gold_diff'],
                        'experience' => @$team_info_array['experience'],
                        'experience_diff' => @$team_info_array['experience_diff'],
                        'turret_kills' => @$team_info_array['turret_kills'],
                        'inhibitor_kills' => @$team_info_array['inhibitor_kills'],
                        'rift_herald_kills' => @$team_info_array['rift_herald_kills'],
                        'dragon_kills' => @$team_info_array['dragon_kills'],
                        'dragon_kills_detail' => $dragon_kills_detail_res,
                        'baron_nashor_kills' => @$team_info_array['baron_nashor_kills'],
                        'building_status' => @json_encode($team_info_array['building_status'],320)
                    ];
                    $battleFramesInfoStatic[] = $team_info;
                }
            }
        }
        $battleFramesInfo['static'] = $battleFramesInfoStatic;
        //player
        $battleFramesInfoPlayers = null;
        $battleRedisInfoPlayers = $redis->hVals($redis_base_name_current."battles:".$battleId.":players");
        $battlePlayersAbilitiesTimeline = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":players_abilities_timeline");
        if ($battleRedisInfoPlayers){
            foreach ($battleRedisInfoPlayers as $player_item){
                $player_info_array = @json_decode($player_item,true);
                $player_info = [
                    'order' => @$player_info_array['order'],
                    'rel_identity_id' => (string)@$player_info_array['rel_identity_id'],
                    'rel_team_id' => (string)@$player_info_array['rel_team_id'],
                    'team_order' => (int)@$player_info_array['team_order'],
                    'seed' => @$player_info_array['seed'],
                    'battle_id' => @$player_info_array['battle_id'],
                    'game' => @$player_info_array['game'],
                    'match' => @$player_info_array['match'],
                    'team_id' => @$player_info_array['team_id'],
                    'faction' => @$player_info_array['faction'],
                    'role' => @$player_info_array['role'],
                    'lane' => @$player_info_array['lane'],
                    'player_id' => @$player_info_array['player_id'],
                    'nick_name' => @$player_info_array['nick_name'],
                    'rel_nick_name' => @$player_info_array['rel_nick_name'],
                    'champion' => @$player_info_array['champion'],
                    'level' => @$player_info_array['level'],
                    'alive' => @$player_info_array['alive'],
                    'ultimate_cd' => (string)@$player_info_array['ultimate_cd'],
                    'coordinate' => @$player_info_array['coordinate'],
                    'kills' => @$player_info_array['kills'],
                    'double_kill' => @$player_info_array['double_kill'],
                    'triple_kill' => @$player_info_array['triple_kill'],
                    'quadra_kill' => @$player_info_array['quadra_kill'],
                    'penta_kill' => @$player_info_array['penta_kill'],
                    'largest_multi_kill' => @$player_info_array['largest_multi_kill'],
                    'largest_killing_spree' => @$player_info_array['largest_killing_spree'],
                    'deaths' => @$player_info_array['deaths'],
                    'assists' => @$player_info_array['assists'],
                    'kda' => @$player_info_array['kda'],
                    'participation' => @$player_info_array['participation'],
                    'cs' => (Int)@$player_info_array['cs'],
                    'minion_kills' => (string)@$player_info_array['minion_kills'],
                    'total_neutral_minion_kills' => (string)@$player_info_array['total_neutral_minion_kills'],
                    'neutral_minion_team_jungle_kills' => (string)@$player_info_array['neutral_minion_team_jungle_kills'],
                    'neutral_minion_enemy_jungle_kills' => (string)@$player_info_array['neutral_minion_enemy_jungle_kills'],
                    'cspm' => @$player_info_array['cspm'],
                    'gold_earned' => @$player_info_array['gold_earned'],
                    'gold_spent' => @$player_info_array['gold_spent'],
                    'gold_remaining' => @$player_info_array['gold_remaining'],
                    'gpm' => @$player_info_array['gpm'],
                    'gold_earned_percent' => @$player_info_array['gold_earned_percent'],
                    'experience' => @$player_info_array['experience'],
                    'xpm' => @$player_info_array['xpm'],
                    'damage_to_champions' => (string)@$player_info_array['damage_to_champions'],
                    'damage_to_champions_physical' => (string)@$player_info_array['damage_to_champions_physical'],
                    'damage_to_champions_magic' => (string)@$player_info_array['damage_to_champions_magic'],
                    'damage_to_champions_true' => (string)@$player_info_array['damage_to_champions_true'],
                    'dpm_to_champions' => (string)@$player_info_array['dpm_to_champions'],
                    'damage_percent_to_champions' => (string)@$player_info_array['damage_percent_to_champions'],
                    'damage_to_towers' => (string)@$player_info_array['damage_to_towers'],
                    'damage_taken' => (string)@$player_info_array['damage_taken'],
                    'damage_taken_physical' => (string)@$player_info_array['damage_taken_physical'],
                    'damage_taken_magic' => (string)@$player_info_array['damage_taken_magic'],
                    'damage_taken_true' => (string)@$player_info_array['damage_taken_true'],
                    'dtpm' => @$player_info_array['dtpm'],
                    'damage_taken_percent' => (string)@$player_info_array['damage_taken_percent'],
                    'damage_conversion_rate' => (string)@$player_info_array['damage_conversion_rate'],
                    'total_heal' => (string)@$player_info_array['total_heal'],
                    'total_crowd_control_time' => (string)@$player_info_array['total_crowd_control_time'],
                    'wards_purchased' => @$player_info_array['wards_purchased'],
                    'wards_placed' => @$player_info_array['wards_placed'],
                    'wards_kills' => @$player_info_array['wards_kills'],
                    'warding_totems_purchased' => @$player_info_array['warding_totems_purchased'],
                    'warding_totems_placed' => @$player_info_array['warding_totems_placed'],
                    'warding_totems_kills' => @$player_info_array['warding_totems_kills'],
                    'control_wards_purchased' => @$player_info_array['control_wards_purchased'],
                    'control_wards_placed' => @$player_info_array['control_wards_placed'],
                    'control_wards_kills' => @$player_info_array['control_wards_kills'],
                    'total_damage' => (string)@$player_info_array['total_damage'],
                    'total_damage_physical' => (string)@$player_info_array['total_damage_physical'],
                    'total_damage_magic' => (string)@$player_info_array['total_damage_magic'],
                    'total_damage_true' => (string)@$player_info_array['total_damage_true'],
                    'items' => @json_encode($player_info_array['items'],320),
                    'summoner_spells' => @json_encode($player_info_array['summoner_spells'],320),
                    'runes' => @json_encode($player_info_array['runes'],320),
                    'abilities_timeline' => @$battlePlayersAbilitiesTimeline[@$player_info_array['rel_identity_id']],
                    'items_timeline' => @json_encode($player_info_array['items_timeline'],320),
                    'hp' => (string)@$player_info_array['hp'],
                    'keystone' => @$player_info_array['keystone'],
                    'respawntimer' => (string)@$player_info_array['respawntimer'],
                    'health' => @$player_info_array['health'],
                    'health_max' => @$player_info_array['health_max'],
                    'damage_selfmitigated' => (string)@$player_info_array['damage_selfmitigated'],
                    'damage_shielded_on_teammates' => (string)@$player_info_array['damage_shielded_on_teammates'],
                    'damage_to_buildings' => (string)@$player_info_array['damage_to_buildings'],
                    'damage_to_objectives' => (string)@$player_info_array['damage_to_objectives'],
                    'total_crowd_control_time_others' =>(string) @$player_info_array['total_crowd_control_time_others'],
                    'vision_score' => (string)@$player_info_array['vision_score'],
                    'turret_kills' => @$player_info_array['turret_kills'],
                    'inhibitor_kills' => @$player_info_array['inhibitor_kills'],
                    'rift_herald_kills' => @$player_info_array['rift_herald_kills'],
                    'dragon_kills' => @$player_info_array['dragon_kills'],
                    'baron_nashor_kills' => @$player_info_array['baron_nashor_kills']
                ];
                $battleFramesInfoPlayers[] = $player_info;
            }
        }
        $battleFramesInfo['players'] = $battleFramesInfoPlayers;
        //ban pick
        $battleDetailInfoBanPick = null;
        $battleRedisInfoBanPick = $redis->hVals($redis_base_name_current."battles:".$battleId.":ban_pick");
        if ($battleRedisInfoBanPick){
            foreach ($battleRedisInfoBanPick as $ban_pick_item){
                $battleDetailInfoBanPick[] = @json_decode($ban_pick_item,true);
            }
        }
        $battleFramesInfo['ban_pick'] = $battleDetailInfoBanPick;
        return $battleFramesInfo;
    }
    //battle 事件 信息
    public function conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battleId,$battleOrder,$event_type,$event_order){
        $event_list = [];
        //如果是结束做个大覆盖
        if ($event_type == 'refresh_match_xiufu'){
            $battleEventsInfo = $redis->hVals($redis_base_name_current."battles:".$battleId.":event_list");
            if ($battleEventsInfo){
                foreach ($battleEventsInfo as $item){
                    $event_info = @json_decode($item,true);
                    if ($event_info['assist']){
                        $event_info['assist'] = @json_encode($event_info['assist'],320);
                    }
                    if ($event_info['other_info']){
                        $event_info['other_info'] = @json_encode($event_info['other_info'],320);
                    }
                    $event_list[] = $event_info;
                }
                if ($event_list){
                    //ingame_timestamp 按照这个排序
                    $event_list_ingame_timestamp = array_column($event_list,'ingame_timestamp');
                    array_multisort($event_list_ingame_timestamp,SORT_ASC,$event_list);
                }
            }
        }elseif ($event_type == 'battle_end'){
            $battleEventsInfo = $redis->hVals($redis_base_name_current."battles:".$battleId.":event_list");
            if ($battleEventsInfo){
                foreach ($battleEventsInfo as $item){
                    $event_info = @json_decode($item,true);
                    if ($event_info['assist']){
                        $event_info['assist'] = @json_encode($event_info['assist'],320);
                    }
                    if ($event_info['other_info']){
                        $event_info['other_info'] = @json_encode($event_info['other_info'],320);
                    }
                    $event_list[] = $event_info;
                }
            }
            //修改达雷的 MatchComingSocketOrder
            self::changeMatchComingSocketOrder($redis,$matchId,$rel_matchId,$battleOrder);
        }else{
            $event_info_redis = $redis->hGet($redis_base_name_current."battles:".$battleId.":event_list",$event_order);
            if ($event_info_redis){
                $event_info = @json_decode($event_info_redis,true);
                if ($event_info['assist']){
                    $event_info['assist'] = @json_encode($event_info['assist'],320);
                }
                if ($event_info['other_info']){
                    $event_info['other_info'] = @json_encode($event_info['other_info'],320);
                }
                $event_list[] = $event_info;
            }
        }
        return $event_list;
    }
    //保存match 和 battle
    public function refreshMatchAndBattle($matchId, $battleId, $info,$refresh_match_xiufu = null){
        if ($refresh_match_xiufu == 'refresh_match_xiufu'){
            foreach ($info['battles'] as $battle){
                $battle_id = $battle['battle_id'];
                if (!empty($battle_id)){
                    //更新battle
                    self::refreshBattle($battle_id,$battle,$refresh_match_xiufu);
                }
            }
        }else{
            //更新battle
            self::refreshBattle($battleId,$info['battle']);
        }
        //更新Match
        self::refreshMatch($matchId,$info['match']);
    }
    private static function refreshBattle($battleId, $battle,$refresh_match_xiufu = null)
    {
        //更新数据battle
        BattleService::setBattleInfo($battleId,'lol',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (isset($battle['static']) && count($battle['static'])) {
            BattleService::setBattleInfo($battleId, 'lol', $battle['static'], BattleService::DATA_TYPE_STATICS);
        }
        BattleService::setBattleInfo($battleId,'lol',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'lol',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'lol',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        //判断是不是 修复
        if ($refresh_match_xiufu == 'refresh_match_xiufu'){
            if(isset($battle['events']) && count($battle['events']) > 0){
                BattleService::setBattleInfo($battleId,'lol',$battle['events'],BattleService::DATA_TYPE_EVENTS);
            }
        }
        return true;
    }
    private static function refreshMatch($matchId, $matchInfo)
    {
        MatchService::setRealTimeInfo($matchId, $matchInfo, 'detail_by_order_scoket_frames');
        return true;
    }
    //修改MatchComingSocketOrder 表 比赛order信息
    private static function changeMatchComingSocketOrder($redis,$matchId,$rel_matchId,$battleOrder){
        $insert_order_id = 0;
        $order_begin_at = date("Y-m-d H:i:s");
        $MatchComingSocketOrder = MatchComingSocketOrder::find()->where(['match_id'=>$rel_matchId])->all();
        if ($MatchComingSocketOrder){
            $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
            $match_winner_id = $redis->hGet($matchRealTimeInfoRedisName,'winner');
            if ($match_winner_id){
                foreach ($MatchComingSocketOrder as $socket_item){
                    $socket_item->setAttribute('status',3);
                    $socket_item->save();
                }
            }else{
                foreach ($MatchComingSocketOrder as $socket_item){
                    $insert_order_id = $socket_item->order;
                    if ($battleOrder == $insert_order_id){
                        $socket_item->setAttribute('status',3);
                        $socket_item->save();
                    }else{
                        $changeData = [
                            'begin_at' => $order_begin_at,
                            'fpid' => null,
                            'spid' => null,
                            'status_refresh' => null,
                            'connection_status' => null,
                            'connection_error' => null,
                            'server_status' => null
                        ];
                        $socket_item->setAttributes($changeData);
                        $socket_item->save();
                    }
                }
            }
        }
        if($insert_order_id > 0){
            //插入新的order
            $MatchComingSocketOrderInsert = new MatchComingSocketOrder();
            $insert_order_data = [
                'match_id' => $rel_matchId,
                'order' => $insert_order_id + 1,
                'begin_at' => $order_begin_at,
                'status' => 1
            ];
            $MatchComingSocketOrderInsert->setAttributes($insert_order_data);
            $MatchComingSocketOrderInsert->save();
        }
        return true;
    }
    //刷API
    private static function refreshApi($matchId,$originId){
        //battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId,   RenovateService::INTERFACE_TYPE_BATTLE, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //match
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId, RenovateService::INTERFACE_TYPE_MATCH, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        return true;
    }

    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}