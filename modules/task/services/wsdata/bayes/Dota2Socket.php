<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\hot\WsToApiSynchron;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Redis;
use yii\base\Exception;
use Throwable;
use app\modules\match\models\MatchLived;


class Dota2Socket extends FiveHotCsgoMatchWs
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $killEvent = '';
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $ctTeam = [];
    public $battle_duration = 0;
    public $muti = '';
    public $battleId = '';
    public $battle_is_end = false;
    public $battleUrn = '';
    public $duration = 0;
    public $tableHeroList = '';
    public $tableItemList = '';
    public $tableTalentList = '';
    public $tableAbilityList = '';
    public $tableIngameGoalList = '';
    public $battleDetails = '';
    public $battleOrder = '';
    public $playerList = [];
    public $CurrentPlayerList = [];
    public $currentRoundDetails = [];
    public $relMatchId = '';
    public $socketTime = '';
    public $needOutPutBattleEndEvent = '';
    public $needOutPutReloadEvent = '';
    public $needOutPutKillEvent = '';
    public $needOutPutUpComingEvent = '';
    public $needOutPutBanPickEvent = [];
    public $needOutPutFrame = '';
    public $needOutPutEvents = '';
    public $roundOrder = '';
    public $assistDbList = [];
    public $battlePlayerList = [];
    public $map = [];
    public $perId = 0;
    const ORIGIN_ID = 9;
    public $operate_type = '';
    public $origin_id = '';
    const GAME_ID = 3;

    /**
     * 消费 event queue
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function run($tagInfo, $taskInfo)
    {
        //info
        $self               = self::getInstance();
        $self->refresh_task = true;
        $refreshType        = @$tagInfo['ext2_type'];
        $refreshType        = $refreshType ? $refreshType : "one";
        $info               = json_decode($taskInfo['params'], true);
        $self->info         = $info;
        $originId           = $info['origin_id'];
        $self->origin_id         = $originId;
        //match
        $self->perId = @$info['match_id'];
        $socket_sub_type = '';
        if ($refreshType == 'refresh') {
            $self->matchId       = $info['match_id'];
            $rel_match_id        = $info['rel_match_id'];
            $refresh_sub_type = @$info['refresh_sub_type'];
            $self->operate_type = @$info['operate_type'];
            $socket_sub_type = Common::getSocketSubTypeByOriginId($originId,$refresh_sub_type);
            if (empty($socket_sub_type)){
                $socket_sub_type = 'DOTA2_BAYES_CARP';
            }
            $rel_match_id_string = self::getPerIdByRelMatchId($rel_match_id, $originId);
        } else {
            $match_id_list   = self::getMatchIdByPerId($self->perId, $originId);
            $rel_identity_id = @$match_id_list['rel_identity_id'];
            $self->matchId   = @$match_id_list['matchId'];
        }

        if (empty($self->matchId) ) {
            return false;
        }

        if ($refreshType == 'refresh') {
//            $relMatchId = $info['rel_match_id'];
            try {
                //删除
                $redisDelKey = $self->currentPre . ':' . $self->matchId . ':';
                $self->batchDelRedisKey($redisDelKey);
                $self->clearBattleMatchId($self->matchId);
                $self->match = Dota2SocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
                if (empty($self->match) || $self->match == 'no_match_info') {
                    return false;
                }
            } catch (Throwable $e) {
                $self->error_catch_log(['msg' => $e->getMessage()]);
            }

            $livedObj = MatchLived::find()->where(['match_id' => (string)$rel_match_id_string]);
            $count    = $livedObj->count();
            $limit    = 1000;
            if ($count > $limit) {
                $countSlice = (int)ceil($count / $limit);
                for ($i = 0; $i < $countSlice; $i++) {
                    $offset    = 0 + $limit * $i;
                    $list      = $livedObj->orderBy('id asc')->offset($offset)->limit($limit)->asArray()->all();
                    $countList = count($list);
                    foreach ($list as $k => $v) {
                        if (!empty($socket_sub_type) && strpos($v['match_data'],$socket_sub_type)){
                            //                        $start_time = date('Y-m-d H:i:s');//test
                            $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                            if ($matchEnd) {
//                            self::changeHotData(7,1,$self->matchId);//调用hotRefresh
                                $self->error_catch_log(['match_end' => true]);
                                return false;
                            }

                            if ($k + 1 == $countList) {
                                $self->refresh = true;
                                $self->error_catch_log(['end_refresh' => true]);
                            }
                            $self->info              = $v;
                            $self->info['socket_id'] = $v['id'];
                            $self->match_data        = json_decode($v['match_data'], true);
                            $self->doRun();
                        }

//                        $end_time = date('Y-m-d H:i:s');//test
//                        echo 'startTime--'.$start_time.'--endTime--'.$end_time.'--id--'.$self->info['socket_id'].PHP_EOL;//test
                    }
                }
            } else {
                $list      = $livedObj->orderBy('socket_time')->limit($limit)->asArray()->all();
                $countList = count($list);
                foreach ($list as $k => $v) {
                    $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                    if ($matchEnd) {
//                        self::changeHotData(7,1,$self->matchId);//调用hotRefresh
                        $self->error_catch_log(['match_end1' => true]);
                        return false;
                    }
                    if ($k + 1 == $countList) {
                        $self->refresh = true;
                        $self->error_catch_log(['end_refresh' => true]);
                    }
                    $self->info              = $v;
                    $self->info['socket_id'] = $v['id'];
                    $self->match_data        = json_decode($v['match_data'], true);
                    $self->doRun();
                }
            }
        } else {
            //判断是不是录入的socket sub type
            $socket_origin_data_type = @$info['match_data']['payload']['type'];
            $db_socket_sub_type = self::getMatchHotDataRunningInfo($self);
            if ($socket_origin_data_type!=$db_socket_sub_type){
                return 'socketSubType no handle';
            }
            $self->match = Dota2SocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
            if (empty($self->match) || $self->match == 'no_match_info') {
                return false;
            }
            $self->match_data = $info['match_data'];
            if (empty($info['match_data'])) {
                return false;
            }
            $self->doRun();
        }
    }
    public static function getMatchHotDataRunningInfo($self){
        $matchDetails = Dota2SocketMatchService::getMatchDetails($self);
        if ($matchDetails['socketSubType']){
            return $matchDetails['socketSubType'];
        }else{
            $hotInfo =  HotDataRunningMatch::find()->where(['match_id' => $self->matchId])->asArray()->one();

            $socket_sub_type_res = Common::getSocketSubTypeByOriginId($self->origin_id,$hotInfo['auto_add_sub_type']);
            $updateArr['socketSubType'] = $socket_sub_type_res;
            Dota2SocketMatchService::setMatchDetails($self,$updateArr);
            return $updateArr['socketSubType'];
        }
    }



    public function doRun()
    {
        try {
            $countKey = 'speed:bayes:dota2:' . $this->matchId;
            $this->redis->incr($countKey);
            $this->redis->expire($countKey,3600);
            $match_data = $this->match_data;
            //原始数据解析
            $seqIdx       = $match_data['seqIdx'];
            $payload_one  = $match_data['payload'];
            $data_type    = $payload_one['payload']['type'];
            $data_subject = $payload_one['payload']['subject'];
            $data_action  = $payload_one['payload']['action'];
            $data_payload = $payload_one['payload']['payload'];
            $sourceUpdatedAt = @$payload_one['payload']['sourceUpdatedAt'];
            //获取battle和order
            $battleId         = $battle_order = null;
            $liveDataMatchUrn = $payload_one['payload']['liveDataMatchUrn'];
            $eventGameTime = $payload_one['payload']['payload']['gameTime'];
//            $BattleArray         = explode('-', $liveDataMatchUrn);
//            $bayesSocketBattleId = end($BattleArray);
            $bayesRedisBattleId     = Dota2SocketBattleService::getBayesBattleId($this);
            $battleId               = Dota2SocketBattleService::getBayesRealBattleId($this);
            $battleOrder            = Dota2SocketBattleService::getBayesBattleOrder($this);
            $battleList             = Dota2SocketBattleService::getBayesBattleList($this);
            $tableHeroList          = Dota2SocketMatchService::getTableHeroList($this);
            $tableItemList          = Dota2SocketMatchService::getTableItemList($this);
            $tableTalentList        = Dota2SocketMatchService::getTableTalentList($this);
            $tableAbilityList       = Dota2SocketMatchService::getTableAbilityList($this);
            $tableIngameGoalList       = Dota2SocketMatchService::getTableIngameGoalList($this);
            $this->battleOrder      = $battleOrder;
            $this->battleId         = $battleId;
            $this->battleUrn        = $liveDataMatchUrn;
            $this->battle_duration        = $eventGameTime?substr($eventGameTime,0,-3):null;
            $this->duration         = $eventGameTime?substr($eventGameTime,0,-3):null;
            $this->tableHeroList    = $tableHeroList;
            $this->tableItemList    = $tableItemList;
            $this->tableTalentList  = $tableTalentList;
            $this->tableAbilityList = $tableAbilityList;
            $this->tableIngameGoalList = $tableIngameGoalList;
            $handle_data = [];
            //处理battle
            if (array_key_exists($liveDataMatchUrn, $battleList)) {
                //当前回合
                $battleDetails = json_decode($battleList[$liveDataMatchUrn], true);
            } else {
                //创建新Battle
                Dota2SocketBattleService::setBayesBattleId($this, $liveDataMatchUrn);
                $newBattleOrder = Dota2SocketBattleService::incrCurrentBattleBattleOrder($this);

                $battleId            = Dota2SocketBattleService::createBattle($this, $this->matchId, $this->match, $newBattleOrder, $this->info['socket_time']);
                $upDate['order']     = $newBattleOrder;
                $upDate['battle_id'] = $battleId;
                $upDateJson          = json_encode($upDate, 320);
                $this->hSet([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST], $liveDataMatchUrn, $upDateJson);
                $battleDetails = $upDate;
            }
            $indexRes =$this->hGet(['history','battle',$this->battleId,'index'],$seqIdx);
            if ($indexRes){
                return $this->battleId.'重复'.$seqIdx;
            }
            $this->hSet(['history','battle',$this->battleId,'index'],$seqIdx,1);
            $this->battleDetails = $battleDetails;
            //判断事件类型
            switch ($data_type) {
                case 'INFO':
                    //判断事件子类型
                    if ($data_subject == 'MATCH') {
                        //判断事件 action
                        //ANNOUNCE 只有一次
                        if ($data_action == 'ANNOUNCE') {
                            Dota2SocketEventService::matchAnnounceEvent($this, $this->matchId, $data_action, $data_payload, $battleId, $this->match);
                        }

                    }
                    break;
                case 'GAME_EVENT':
                    switch ($data_subject) {
                        case 'MATCH':
                            switch ($data_action) {
                                case 'ANNOUNCED_ANCIENT'://远古资源即将刷新公告
                                    break;
                                case 'UPDATE_BUILDINGS'://建筑状态更新
                                    //建筑状态更新
                                    $handle_data = Dota2SocketEventService::BuildingsUpdateEvent($this, $data_payload);
                                    break;
                                case 'SPAWNED_ITEM'://道具重生(刷新)
                                    //道具重生
                                    $handle_data = Dota2SocketEventService::ItemSpawnEvent($this, $data_payload);
                                    break;
                                case 'DROPPED_ITEM':
                                    //道具掉落
                                    $handle_data = Dota2SocketEventService::ItemDropEvent($this, $data_payload);
                                    break;
                                case 'SPAWNED_ANCIENT'://远古资源刷新公告(击杀精英怪,肉山)
                                    //开始暂停
                                    $handle_data = Dota2SocketEventService::AncientSpawnEvent($this, $data_payload);
                                    break;
                                case 'START_PAUSE':
                                    //开始暂停
                                    $handle_data = Dota2SocketEventService::startPauseEvent($this, $data_payload);
                                    break;
                                case 'END_PAUSE':
                                    //END_PAUSE
                                    $handle_data = Dota2SocketEventService::endPauseEvent($this, $data_payload);
                                    break;
                                case 'START_MAP':
                                    //battle_start
                                    $handle_data = Dota2SocketEventService::startMapEvent($this, $this->matchId, $data_payload,$sourceUpdatedAt);
                                    break;
                                case 'END_MAP':
                                    //battle_end
                                    $handle_data = Dota2SocketEventService::endMapEvent($this, $this->matchId, $data_payload,$sourceUpdatedAt);
                                    break;
                            }
                            break;
                        case 'PLAYER':
                            switch ($data_action) {
                                case 'PICKED_UP_ITEM':
                                    //拾取道具
                                    $handle_data = Dota2SocketEventService::ItemPickupEvent($this, $data_payload);
                                    break;
                                case 'DENIED_ITEM':
                                    //摧毁道具
                                    $handle_data = Dota2SocketEventService::ItemDenyEvent($this, $data_payload);
                                    break;
                                case 'CONSUMED_ITEM':
                                    //使用道具
                                    $handle_data = Dota2SocketEventService::ItemUseEvent($this, $data_payload);
                                    break;
                                case 'PLACED_WARD':
                                    //插眼
                                    $handle_data = Dota2SocketEventService::WardPlacedEvent($this, $data_payload);
                                    break;
                                case 'KILLED_WARD':
                                    //拆眼
                                    $handle_data = Dota2SocketEventService::WardDestroyedEvent($this, $data_payload);
                                    break;
                                case 'EXPIRED_WARD':
                                    //眼过期
                                    $handle_data = Dota2SocketEventService::WardExpiredEvent($this, $data_payload);
                                    break;
                                case 'SPAWNED':
                                    //选手复活
                                    $handle_data = Dota2SocketEventService::HeroSpawnEvent($this, $data_payload);
                                    break;
                                case 'KILLED_MINION':
                                    //击杀信使
                                    $handle_data = Dota2SocketEventService::MinionDeathEvent($this, $data_payload);
                                    break;
                                case 'USED_ABILITY':
                                    //使用特殊技能
                                    $handle_data = Dota2SocketEventService::AbilityUsedEvent($this, $data_payload);
                                    break;
                                case 'SPECIAL_KILL':
                                    //特殊击杀
                                    $handle_data = Dota2SocketEventService::SpecialKillEvent($this, $data_payload);
                                    break;
                                case 'USED_CLOAK':
                                    //开雾
                                    $handle_data = Dota2SocketEventService::PlayersCloakedEvent($this, $data_payload);
                                    break;
                                case 'BROKE_CLOAK':
                                    //破雾
                                    $handle_data = Dota2SocketEventService::PlayerUncloakedEvent($this, $data_payload);
                                    break;
                                case 'KILL'://击杀
                                    $handle_data = Dota2SocketEventService::KillEvent($this, $this->matchId, $battleId, $data_payload);
                                    break;
                                case 'KILLED_ANCIENT'://击杀肉山
                                    $handle_data = Dota2SocketEventService::AncientKillEvent($this,$data_payload);
                                    break;

                            }
                            break;
                        case 'TEAM':
                            if ($data_action == 'UPDATE_SCORE') {
//                                $this->conversionTeamScore($redis,$matchId,$battleId,$data_payload);
                            }

                            if ($data_action == 'TOOK_OBJECTIVE') {//推塔(摧毁防御塔)
                                $handle_data = Dota2SocketEventService::ObjectiveTakenEvent($this, $this->matchId, $battleId, $data_payload);
                            }
                            break;
                    }
                    break;
                case 'SNAPSHOT':
                    if ($data_subject == 'TEAM') {
                        if ($data_action == 'UPDATE') {//ban英雄
                            Dota2SocketEventService::teamUpdateBanPickEvent($this, $this->matchId, $battleId, $data_payload);
                        }
                    }
                    if ($data_subject == 'MATCH') {
                        if ($data_action == 'UPDATE') {//frames
                            $handle_data = Dota2SocketMatchService::transFrames($this, $this->matchId, $battleId, $data_payload, $battleDetails);
                        }
                    }
                    if ($data_subject == 'PLAYER') {
                        if ($data_action = 'UPDATE_POSITIONS') {//选手位置更新  并产生frames

                        }
                    }
                    break;
                case 'UNKNOWN':
                    break;
            }
            if (!empty($this->needOutPutBanPickEvent)) {

                foreach ($this->needOutPutBanPickEvent as $k => $vArr) {
                    if (!empty($vArr)) {
//                        $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_MAIN_EVENTS], json_encode($vArr, 320));
                        $this->lPush(['history', 'battle', $battleId, 'events'], json_encode($vArr, 320));
                    }
                    //存入socket
                    $data_ws = [
                        'match_data' => json_encode($vArr, 320),
                        'type'       => 'events',
                        'game_id'    => 3,
                        'match_id'   => $this->matchId,
                        'origin_id'   => $this->origin_id,
                        'battle_order'   => $this->battleOrder
                    ];
                    $this->saveSocketToDb($data_ws);
                }

                $this->needOutPutBanPickEvent = [];
            }
            //空内容不返回
            if (empty($handle_data)) {
                return false;
            }
            //空内容不返回
            if (empty($handle_data['data'])) {
                return false;
            }
            if ($this->needOutPutFrame) {
                $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_FRAMES], json_encode($this->needOutPutFrame, 320));
                //存入socket
                $data_ws = [
                    'match_data' => json_encode($this->needOutPutFrame, 320),
                    'type'       => 'frames',
                    'game_id'    => 3,
                    'match_id'   => $this->matchId,
                    'origin_id'   => $this->origin_id,
                    'battle_order'   => $this->battleOrder
                ];
                $this->saveSocketToDb($data_ws);
                $this->needOutPutFrame = '';
            }
            if ($this->needOutPutEvents) {
                $this->lPush(['history', 'battle', $battleId, 'events'], $this->needOutPutEvents);
                //存入socket
                $data_ws = [
                    'match_data' => $this->needOutPutEvents,
                    'type'       => 'events',
                    'game_id'    => 3,
                    'match_id'   => $this->matchId,
                    'origin_id'   => $this->origin_id,
                    'battle_order'   => $this->battleOrder
                ];
                $this->saveSocketToDb($data_ws);
                $this->needOutPutEvents = '';
            }

            if ($handle_data['type'] == 'events') {
                $data = $handle_data['data'];
//                $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_MAIN_EVENTS], json_encode($data, 320));
                $this->lPush(['history', 'battle', $battleId, 'events'], json_encode($data, 320));
                if ($data['event_type'] == 'building_kill'){
                    unset($data['victim']['ingame_id']);
                }
                //存入socket
                $data_ws     = [
                    'match_data' => json_encode($data, 320),
                    'type'       => 'events',
                    'game_id'    => 3,
                    'match_id'   => $this->matchId,
                    'origin_id'   => $this->origin_id,
                    'battle_order'   => $this->battleOrder
                ];
                $this->event = $handle_data['data'];
                $this->saveSocketToDb($data_ws);
            } else {
                $data = $handle_data['data'];
                $is_battle_finished = $data['is_battle_finished'];
                $this->battle_is_end = $is_battle_finished;
                if (!$is_battle_finished){
                    $nowTime     = time();
                    $redisKey    = $this->currentPre . ':tag_csgo_frame_refresh_time:' . $this->matchId;
                    $refreshTime = $this->redis->get($redisKey);
//                $matchStatus = $this->matchEndFlag;
                    if ($refreshTime) {
                        $diff_time = $nowTime - $refreshTime;
                        if ($diff_time >= 1 || $this->matchEndFlag) {
//                        $this->matchEndFlag = false;
                            $this->redis->set($redisKey, $nowTime, 3600);
                        } else {
                            return true;
                        }
                    } else {
                        $this->redis->set($redisKey, $nowTime, 3600);
                    }
                }
                $this->lPush([self::KEY_TAG_HISTORY, self::KEY_TAG_FRAMES], json_encode($data, 320));
                //存入socket
                $data_ws = [
                    'match_data' => json_encode($data, 320),
                    'type'       => 'frames',
                    'game_id'    => 3,
                    'match_id'   => $this->matchId,
                    'origin_id'   => $this->origin_id,
                    'battle_order'   => $this->battleOrder
                ];
                $this->saveSocketToDb($data_ws);
            }

            $this->refreshToApi();


        } catch (Throwable $e) {

            $this->error_catch_log(['msg' => $e->getMessage()]);
        }

    }


    /**
     * 从Ws插入队列去SetApi
     * @return bool
     * @throws \yii\db\Exception
     */
    public function refreshToApi()
    {
        //task refresh api
        $contentArr  = [];
        $nowTime     = time();
        $origin      = $this->currentPre;
        $matchId     = $this->matchId;
        $battle_is_end  = $this->battle_is_end;
        $eventId     = $this->event['event_id'];
        $eventType   = $this->event['event_type'];
        $key         = ':tag_csgo_api_refresh_time:';
        $redisKey    = $this->currentPre . $key . $this->matchId;
        $refreshTime = $this->redis->get($redisKey);
        if ($refreshTime && $this->matchId) {

            $specialEventArr = [
                'battle_end',
            ];

            $diff_time = $nowTime - $refreshTime;
            if ($diff_time > 2 || in_array($eventType, $specialEventArr) || $eventType == 'battle_start' || $this->matchEndFlag || $battle_is_end) {
                if ($this->matchEndFlag) {
                    $this->matchEndFlag = false;
                }
                $updateType = 3;
                $paramArr   = [
                    'updateType' => $updateType,
                    'match_id'   => $this->matchId,
                    'event_id'   => @$eventId,
                    'battleUrn'   => @$this->battleUrn,
                    'currentPre' => $this->currentPre,
                    'event_type' => @$eventType,
                    'origin_id' => @$this->origin_id,
                ];
//                    WsToApiSynchron::rPushSynchron($origin, $matchId, $paramArr);
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        $this->origin_id, "", "", 3),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => $paramArr,
                ];
                TaskRunner::addTask($item, 4);
                $this->redis->set($redisKey, $nowTime, 3600);
            }


        } else {
            $this->redis->set($redisKey, $nowTime, 3600);
        }
    }

    /**
     * 把socket存到db
     * @param $data_ws
     * @return int
     * @throws \yii\db\Exception
     */
    public function saveSocketToDb($data_ws)
    {
//        $this->saveDataBase($data_ws);//TODO 正式去掉
        if ($this->operate_type != 'xiufu'){
            KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'), $data_ws);
        }

    }

    //保存到DB
    public function saveDataBase($data){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $data);
        $db_websoket->execute();
        return true;
    }
    /**
     * 单例
     * @return HltvMatchWs
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }


}