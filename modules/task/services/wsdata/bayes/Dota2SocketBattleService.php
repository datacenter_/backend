<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\TaskService;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Throwable;
use yii\base\Exception;

class Dota2SocketBattleService extends FiveHotCsgoMatchWs implements SocketBattleInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    public static function getBayesBattleId($model)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BAYES_BATTLE_ID];
        return $model->getValue($key);
    }
    public static function setBayesBattleId($model,$value)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BAYES_BATTLE_ID];
        return $model->setValue($key,$value);
    }
    public static function getBayesRealBattleId($model)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_BATTLE_ID];
        return $model->getValue($key);
    }
    public static function getBayesBattleOrder($model)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_ORDER];
        return $model->getValue($key);
    }
    public static function getBayesBattleList($model)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_HISTORY,self::KEY_TAG_BATTLE,self::KEY_TAG_LIST];
        return $model->hGetAll($key);
    }

    public static function createBattle($model, $matchId, $matchInfo, $newBattleOrder, $socketTime)
    {
        if (empty($model)) {
            $model = new Dota2Socket();
        }
        if ($newBattleOrder > $matchInfo['number_of_games']) {
            //说明是错误的order
            return false;
        }
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $newBattleOrder,
        ])->one();
        if (empty($battleBase)) {
            $battleId = $model->createMatchBattleByMatchId($matchId,$newBattleOrder,3);
            $model->setBattleBeginAt($socketTime);
            $battleBase = MatchBattle::find()->where([
                'id' => $battleId,
            ])->one();
        }
//        if ($battleBase['deleted']==1){
//            $infoMainIncrement  = ["diff" => [], "new" => ['id'=>$battleBase->id,'match'=>$matchId]];
//            TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_BATTLE,QueueServer::CHANGE_TYPE_RECOVERY,$infoMainIncrement,'');
//        }
//        $battleBase->setAttributes([
//            'deleted'  => 2,
//            'deleted_at' => null,
//        ]);
        $battleBase->save();
        $model->battleId = $battleBase->id;
        $model->setBattleId($battleBase->id);
        $key = [self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER];
        return $battleBase->id;
    }

    public static function incrCurrentBattleBattleOrder($model)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = [self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,self::KEY_TAG_ORDER];
        return $model->incr($key);
    }


    public static function getBattleDetailsByUrn($model,$battleUrn){
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $battleList = self::getBayesBattleList($model);
        return json_decode($battleList[$battleUrn],true);
    }

    public static function setBattleDetailsByUrn($model,$battleUrn,$updateData){
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $battleList = self::getBayesBattleList($model);
        $battle_details =  json_decode($battleList[$battleUrn],true);
        if ($battle_details&&is_array($battle_details)){
            $battleUpdate  = array_merge($battle_details,$updateData);
        }else{
            $battleUpdate = $updateData;
        }
        $battleUpdateJson = json_encode($battleUpdate,320);
        $key =  ['history','battle','list'];
        $model->hSet($key,$battleUrn,$battleUpdateJson);
    }


    public static function getMap($model,$map_id){
        if (!$model){
            $model= new Dota2Socket();
        }
        if ($model->refresh_task && !empty($model->map)){
            return $model->map;
        }
        $map = null;
        $key = Consts::GAME_MAPS.":dota";
        $mapInfoRedis = $model->redis->hGet($key,$map_id);
        if ($mapInfoRedis){
            $mapInfo = @json_decode($mapInfoRedis,true);
            $map_is_default = @$mapInfo['is_default'] == 1 ? true : false;
            $map_image = [
                'square_image' => null,
                'rectangle_image' => null,
                'thumbnail' => (String)@$mapInfo['image']
            ];
            $map = [
                'map_id' => (Int)@$mapInfo['id'],
                'name' => (String)@$mapInfo['name'],
                'name_cn' => self::checkAndChangeString(@$mapInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$mapInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$mapInfo['external_name']),
                'short_name' => self::checkAndChangeString(@$mapInfo['short_name']),
                'map_type' => self::checkAndChangeString(@$mapInfo['map_type']),
                'map_type_cn' => self::checkAndChangeString(@$mapInfo['map_type_cn']),
                'is_default' => $map_is_default,
                'slug' => self::checkAndChangeString(@$mapInfo['slug']),
                'image' => $map_image
            ];
        }
        if ($model->refresh_task ){
            $model->map = $map;
        }
        return $map;
    }









}