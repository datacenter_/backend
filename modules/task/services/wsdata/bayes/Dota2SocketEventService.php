<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\services\WinnerHelper;
use app\modules\org\models\Player;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;

class Dota2SocketEventService extends FiveHotCsgoMatchWs implements SocketEventInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    /**
     * 第一条数据
     * @param $model
     * @param $matchId
     * @param $type
     * @param $data_payload
     * @param $battleId
     * @param $matchInfo
     * @throws \app\rest\exceptions\BusinessException
     */
    public static function matchAnnounceEvent($model, $matchId, $type, $data_payload, $battleId, $matchInfo)
    {
        if (empty($model)) {
            $model = new Dota2Socket();
        }
        $teamList = Dota2SocketTeamService::getTeamList($model);
        if (!empty($data_payload) && empty($teamList)) {
            $teams                = $data_payload['teams'];
            $key                  = [self::KEY_TAG_TABLE, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
            $playerIdAndNameArray = $model->hGetAll($key);


            $team_1 = $matchInfo['team_1_id'];
            $team_2 = $matchInfo['team_2_id'];
            $per1 = $teams[0]['urn'];
            $per2 = $teams[1]['urn'];
            $matching_teams = HotBase::socketMatchingTeam($team_1,$team_2,$per1,$per2,$model->origin_id,3);
            //循环匹配
            foreach ($teams as $key => $item) {
                $team_urn_id = $item['urn'];
                //查询队伍ID
                $team_id = $matching_teams[$team_urn_id];
                //@self::getMainIdOfBayesByUrn('team', $this->origin_id, $team_urn_id, 2);
                //获取$team_order
                if(empty($team_id)){
                    $team_order = null;
                }else{
                    $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
                }


                //处理玩家
                $team_participants = $item['participants'];
                foreach ($team_participants as $key_p => $item_p) {
                    $player_urn_id = $item_p['urn'];
//                    $player_rel_name_array = @explode(' ',$item_p['name']);//空格 分割
                    $player_rel_name = @$item_p['name'];

                    $nick_name = $item_p['name'];
                    $steam_id  = $item_p['references']['STEAM_ID_64'];
                    $player_id = @self::getMainIdOfBayesByUrn('player', $model->origin_id, $player_urn_id, 3);
                    if ($player_id) {
                        $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                        $nick_name   = @$Player_Info['nick_name'];
                    }
                    $player_seed = $key_p + 1;
//                    $role = Common::getDota2LaneByPositionId('bayes',$player_seed);
//                    $lane = Common::getDota2LaneId('bayes',$role);

                    $player_Info = [
                        'order'           => $key + 1,
                        'rel_identity_id' => $player_urn_id,
                        'rel_team_id'     => $team_urn_id,
                        'team_order'      => $team_order,
                        'seed'            => $key_p + 1,
                        'battle_id'       => $battleId,
                        'game'            => 3,
                        'match'           => $matchId,
                        'team_id'         => $team_id,
                        'faction'         => null,
                        'role'            => null,
                        'lane'            => null,
                        'player_id'       => $player_id,
                        'nick_name'       => $nick_name,
                        'rel_nick_name'   => $player_rel_name,
                        'steam_id'        => $steam_id,
                        'champion'        => null,
                        'heroId' => null,
                        'double_kill' => 0,
                        'triple_kill' => 0,
                        'ultra_kill' => 0,
                        'rampage' => 0,
                        'largest_multi_kill' => 0,
                        'largest_killing_spree' => 0,
                        'bounty_runes_pickedup' => null,
                        'double_damage_runes_pickedup' => null,
                        'haste_runes_pickedup' => null,
                        'illusion_runes_pickedup' => null,
                        'invisibility_runes_pickedup' => null,
                        'regeneration_runes_pickedup' => null,
                        'arcane_runes_pickedup' => null,
                        'smoke_used' => 0,
                        'dust_used' => 0,
                        'observer_wards_placed' => 0,
                        'observer_wards_kills' => 0,
                        'sentry_wards_placed' => 0,
                        'sentry_wards_kills' => 0,
                        'barrack_kills' => 0
                    ];
                    Dota2SocketPlayerService::setPlayerInfo($model, $player_urn_id, $player_Info);
                }
                $team_info = [
                    'team_id'         => $team_id,
                    'order'           => $team_order,
                    'faction'         => null,
                    'score'           => 0,
                    'rel_identity_id' => $team_urn_id,
                    'identity_id'     => $team_urn_id,
                    'smoke_used' => 0,
                    'dust_used' => 0,
                    'bounty_runes_pickedup' => null,
                    'double_damage_runes_pickedup' => null,
                    'haste_runes_pickedup' => null,
                    'illusion_runes_pickedup' => null,
                    'invisibility_runes_pickedup' => null,
                    'regeneration_runes_pickedup' => null,
                    'arcane_runes_pickedup' => null,
                    'observer_wards_kills' => 0,
                    'observer_wards_placed' => 0,
                    'sentry_wards_kills' => 0,
                    'sentry_wards_placed' => 0
                ];
                Dota2SocketTeamService::setTeamInfo($model, $team_urn_id, $team_info);
            }
        }
        return true;
    }

    /**
     * startPauseEvent
     * @param $model
     * @param $match_id
     * @param $data_payload
     */
    public static function startPauseEvent($model, $data_payload)
    {
        if ($data_payload) {
            $event_type = 'battle_pause';
            $duration = intval($data_payload['gameTime']/1000);
            $update['is_pause'] = true;
            $update['duration'] = $duration;
            Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     * 精英怪刷新（肉山）
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function AncientSpawnEvent($model, $data_payload)
    {
        if ($data_payload) {
            $ancientPosition = $data_payload['ancientPosition'];
            $event_type = 'elite_spawned';//精英怪刷新（肉山）
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['elite'] = Dota2SocketEventService::getEventBuildingFormat($model,$data_payload,'ROSHAN',null);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }
    /**
     * 建筑状态更新
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function BuildingsUpdateEvent($model, $data_payload)
    {
        //更新 outposts towers barracks ancient
        if ($data_payload) {
            $buildingStatusArr = $buildingTeamsInfo = [];
            $buildingUpdates =  $data_payload['buildingUpdates'];
            foreach ($buildingUpdates as $k=>$v){
                $buildingTeamsInfo[$v['teamUrn']] = $v['teamUrn'];
                $buildingType= '';
                $buildingPosition= '';
                //OUTPOST
                if ($v['buildingType'] == 'OUTPOST') {
                    $buildingType = 'outposts';
                    if (strpos($v['name'],"bottom")){
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['bot_outpost']['is_captured'] = $v['alive'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['bot_outpost']['health'] = $v['healthCurrent'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['bot_outpost']['health_max'] = $v['healthMax'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['bot_outpost']['armor'] = $v['armor'];

                    }
                    if (strpos($v['name'],"top")){
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['top_outpost']['is_captured'] = $v['alive'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['top_outpost']['health'] = $v['healthCurrent'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['top_outpost']['health_max'] = $v['healthMax'];
                        $buildingStatusArr[$v['teamUrn']][$buildingType]['top_outpost']['armor'] = $v['armor'];
                    }
                }
                //TOWER
                if ($v['buildingType'] == 'TOWER') {
                    $buildingType = 'towers';
                    if ($v['lane'] == 'TOP' && $v['towerTier'] == 'TIER_1') {
                        $buildingPosition = 'top_tier_1_tower';
                    } elseif ($v['lane'] == 'TOP' && $v['towerTier'] == 'TIER_2') {
                        $buildingPosition = 'top_tier_2_tower';
                    } elseif ($v['lane'] == 'TOP' && $v['towerTier'] == 'TIER_3') {
                        $buildingPosition = 'top_tier_3_tower';
                    } elseif ($v['lane'] == 'MID' && $v['towerTier'] == 'TIER_1') {
                        $buildingPosition = 'mid_tier_1_tower';
                    } elseif ($v['lane'] == 'MID' && $v['towerTier'] == 'TIER_2') {
                        $buildingPosition = 'mid_tier_2_tower';
                    } elseif ($v['lane'] == 'MID' && $v['towerTier'] == 'TIER_3') {
                        $buildingPosition = 'mid_tier_3_tower';
                    } elseif ($v['lane'] == 'BOT' && $v['towerTier'] == 'TIER_1') {
                        $buildingPosition = 'bot_tier_1_tower';
                    } elseif ($v['lane'] == 'BOT' && $v['towerTier'] == 'TIER_2') {
                        $buildingPosition = 'bot_tier_2_tower';
                    } elseif ($v['lane'] == 'BOT' && $v['towerTier'] == 'TIER_3') {
                        $buildingPosition = 'bot_tier_3_tower';
                    } elseif ($v['lane'] == 'TOP' && $v['towerTier'] == 'TIER_4') {
                        $buildingPosition = 'top_tier_4_tower';
                    } elseif ($v['lane'] == 'BOT' && $v['towerTier'] == 'TIER_4') {
                        $buildingPosition = 'bot_tier_4_tower';
                    }
                }
                if ($v['buildingType'] == 'BARRACKS') {
                    $buildingType = 'barracks';
                    $barracksType = $v['barracksType'];
                    $barracksLine = $v['lane'];
                    switch ($barracksType){
                        case 'RANGED':
                            switch ($barracksLine){
                                case 'TOP':
                                    $buildingPosition = 'top_ranged_barrack';
                                    break;
                                case 'MID':
                                    $buildingPosition = 'mid_ranged_barrack';
                                    break;
                                case 'BOT':
                                    $buildingPosition = 'bot_ranged_barrack';
                                    break;
                            }
                            break;
                        case 'MELEE':
                            switch ($barracksLine){
                                case 'TOP':
                                    $buildingPosition = 'top_melee_barrack';
                                    break;
                                case 'MID':
                                    $buildingPosition = 'mid_melee_barrack';
                                    break;
                                case 'BOT':
                                    $buildingPosition = 'bot_melee_barrack';
                                    break;
                            }
                            break;
                    }
                }
                if ($v['buildingType'] == 'ANCIENT') {
                    $buildingType                                                   = 'ancient';
                    $buildingStatusArr[$v['teamUrn']][$buildingType]['is_alive'] = $v['alive'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType]['health']      = $v['healthCurrent'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType]['health_max']  = $v['healthMax'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType]['armor']       = $v['armor'];
                }
                if ($buildingType && $buildingPosition) {
                    $buildingStatusArr[$v['teamUrn']][$buildingType][$buildingPosition]['is_alive'] = $v['alive'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType][$buildingPosition]['health'] = $v['healthCurrent'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType][$buildingPosition]['health_max'] = $v['healthMax'];
                    $buildingStatusArr[$v['teamUrn']][$buildingType][$buildingPosition]['armor']      = $v['armor'];
                }
            }
            $teamList = Dota2SocketTeamService::getTeamList($model);
            if ($teamList&&$buildingStatusArr){
                foreach ($teamList as $k=>$v){
                    $team_info = @json_decode($v,true);
                    $rel_identity_id = $team_info['rel_identity_id'];
                    $team_building_status = $team_info['building_status'];
                    if ($team_building_status){
                        //outposts towers barracks ancient
                        //outposts
                        foreach ($team_building_status['outposts'] as $key => $item){
                            if (isset($buildingStatusArr[$rel_identity_id]['outposts'][$key])){
                                //更新血量等信息
                                $team_building_status['outposts'][$key]['is_captured'] = @$buildingStatusArr[$rel_identity_id]['outposts'][$key]['is_captured'];
                            }else{
                                $team_building_status['outposts'][$key]['is_captured'] = false;
                            }
                        }
                        //towers
                        foreach ($team_building_status['towers'] as $key => $item){
                            if (isset($buildingStatusArr[$rel_identity_id]['towers'][$key])){
                                //更新血量等信息
                                $team_building_status['towers'][$key]['is_alive'] = @$buildingStatusArr[$rel_identity_id]['towers'][$key]['is_alive'];
                                $team_building_status['towers'][$key]['health'] = @$buildingStatusArr[$rel_identity_id]['towers'][$key]['health'];
                                $team_building_status['towers'][$key]['armor'] = @$buildingStatusArr[$rel_identity_id]['towers'][$key]['armor'];
                            }else{
                                //更新状态
                                $team_building_status['towers'][$key]['is_alive'] = false;
                                $team_building_status['towers'][$key]['health'] = 0;
                                $team_building_status['towers'][$key]['armor'] = 0;
                            }
                        }
                        //barracks
                        foreach ($team_building_status['barracks'] as $key => $item){
                            if (isset($buildingStatusArr[$rel_identity_id]['barracks'][$key])){
                                //更新血量等信息
                                $team_building_status['barracks'][$key]['is_alive'] = @$buildingStatusArr[$rel_identity_id]['barracks'][$key]['is_alive'];
                                $team_building_status['barracks'][$key]['health'] = @$buildingStatusArr[$rel_identity_id]['barracks'][$key]['health'];
                                $team_building_status['barracks'][$key]['armor'] = @$buildingStatusArr[$rel_identity_id]['barracks'][$key]['armor'];
                            }else{
                                //更新状态
                                $team_building_status['barracks'][$key]['is_alive'] = false;
                                $team_building_status['barracks'][$key]['health'] = 0;
                                $team_building_status['barracks'][$key]['armor'] = 0;
                            }
                        }
                        //ancient
                        if (isset($buildingStatusArr[$rel_identity_id]['ancient'])){
                            //更新血量等信息
                            $team_building_status['ancient']['is_alive'] = @$buildingStatusArr[$rel_identity_id]['ancient']['is_alive'];
                            $team_building_status['ancient']['health'] = @$buildingStatusArr[$rel_identity_id]['ancient']['health'];
                            $team_building_status['ancient']['armor'] = @$buildingStatusArr[$rel_identity_id]['ancient']['armor'];
                        }else{
                            //更新状态
                            $team_building_status['ancient']['is_alive'] = false;
                            $team_building_status['ancient']['health'] = 0;
                            $team_building_status['ancient']['armor'] = 0;
                        }
                    }
                    $team_info['building_status'] = $team_building_status;
                    //保存
                    Dota2SocketTeamService::setTeamInfo($model,$rel_identity_id,$team_info);
                }
            }
        }
    }
    /**
     * 道具重生
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function ItemSpawnEvent($model, $data_payload)
    {
        if ($data_payload) {
            $itemType = $data_payload['itemType'];
            if ($itemType == 'RUNE'){
                $itemName = $data_payload['itemName'];
                $rune_name = Common::getDota2RuneByItemInfo('bayes',$itemName);
                if (empty($rune_name)){
                    return null;
                }
                $event_type = 'rune_spawned';//神符刷新
                $eventArr               = Dota2SocketEventService::getEventCommon($model);
                $eventArr['event_type'] = $event_type;
                $eventArr['rune_name'] = $rune_name;
                $arr['data']            = $eventArr;
                $arr['type']            = 'events';
                return $arr;
            }else{
                return null;
            }
        }
    }
    /**
     * 道具掉落
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function ItemDropEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $playerUrn = $data_payload['playerUrn'];
            $event_type = 'item_dropped';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['player'] = Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            $eventArr['item'] = Dota2SocketEventService::getEventFormatItemByExternalId($model,$data_payload['itemId']);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     * 道具拾取
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function ItemPickupEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $event_type    = 'item_picked_up';
            $playerUrn     = $data_payload['playerUrn'];
//            $playerTeamUrn = $data_payload['playerTeamUrn'];
            $itemType      = $data_payload['itemType'];
            $itemName      = $data_payload['itemName'];
//            $playerInfo    = Dota2SocketPlayerService::getPlayerInfo($model, $playerUrn);
//            $teamInfo      = Dota2SocketTeamService::getTeamInfo($model, $playerTeamUrn);
//            $playerUpdate  = [];
            //TODO  数据源有错 需要注销
//            if ($itemType == 'RUNE'){
//                return null;
//            }
            //赏金符文不需要输出,因为事件太多了
            if ($itemName == 'rune_bounty') {
                return false;
            }
            $rune_name = null;
            switch ($itemType) {
                case 'RUNE':
                    $event_type = 'rune_picked_up';//神符拾取（强化神符）
                    switch ($itemName) {
                        case 'rune_bounty':
                            //赏金神符
//                            $event_type                            = 'item_piciked_up';//赏金神符不需要输出拾取神符
//                            $playerUpdate['bounty_runes_pickedup'] = $playerInfo['bounty_runes_pickedup'] + 1;
//                            $teamUpdate['bounty_runes_pickedup']   = $teamInfo['bounty_runes_pickedup'] + 1;
                            break;
                        case 'rune_doubledamage':
                            $rune_name = 'double_damage';
                            //双倍伤害神符
//                            $playerUpdate['double_damage_runes_pickedup'] = $playerInfo['double_damage_runes_pickedup'] + 1;
//                            $teamUpdate['double_damage_runes_pickedup']   = $teamInfo['double_damage_runes_pickedup'] + 1;
                            break;
                        case 'rune_haste':
                            $rune_name = 'haste';
                            //拾取极速神符
//                            $playerUpdate['haste_runes_pickedup'] = $playerInfo['haste_runes_pickedup'] + 1;
//                            $teamUpdate['haste_runes_pickedup']   = $teamInfo['haste_runes_pickedup'] + 1;
                            break;
                        case 'rune_illusion':
                            $rune_name = 'illusion';
                            //拾取幻象神符
//                            $playerUpdate['illusion_runes_pickedup'] = $playerInfo['illusion_runes_pickedup'] + 1;
//                            $teamUpdate['illusion_runes_pickedup']   = $teamInfo['illusion_runes_pickedup'] + 1;
                            break;
                        case 'rune_invis':
                            $rune_name = 'invisibility';
                            //拾取隐身神符
//                            $playerUpdate['invisibility_runes_pickedup'] = $playerInfo['invisibility_runes_pickedup'] + 1;
//                            $teamUpdate['invisibility_runes_pickedup']   = $teamInfo['invisibility_runes_pickedup'] + 1;
                            break;
                        case 'rune_regen':
                            $rune_name = 'regeneration';
                            //拾取恢复神符
//                            $playerUpdate['regeneration_runes_pickedup'] = $playerInfo['regeneration_runes_pickedup'] + 1;
//                            $teamUpdate['regeneration_runes_pickedup']   = $teamInfo['regeneration_runes_pickedup'] + 1;
                            break;
                        case 'rune_arcane':
                            $rune_name = 'arcane';
//                            //拾取奥术神符
//                            $playerUpdate['arcane_runes_pickedup'] = $playerInfo['arcane_runes_pickedup'] + 1;
//                            $teamUpdate['arcane_runes_pickedup']   = $teamInfo['arcane_runes_pickedup'] + 1;
                            break;
                    }
//                    if (!empty($playerUpdate)) {
//                        //更新用户神符
//                        Dota2SocketPlayerService::setPlayerInfo($model, $playerUrn, $playerUpdate);
//                    }
//                    if (!empty($teamUpdate)) {
//                        //更新用户神符
//                        Dota2SocketTeamService::setTeamInfo($model, $playerTeamUrn, $teamUpdate);
//                    }
                    break;

            }

            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;

            if ($itemType == 'RUNE') {
                $eventArr['rune_name'] = $rune_name;
                $eventArr['player']     = Dota2SocketEventService::getEventPlayerFormatByUrn($model, $playerUrn);
            }
            if ($itemType != 'RUNE'){
                $eventArr['is_steal'] = $data_payload['steal'];
                $eventArr['player']     = Dota2SocketEventService::getEventPlayerFormatByUrn($model, $playerUrn);
                $eventArr['item']     = Dota2SocketEventService::getEventFormatItemByExternalId($model, $data_payload['itemId']);
            }
            $arr['data'] = $eventArr;
            $arr['type'] = 'events';
            return $arr;
        }
    }
    /**
     * 摧毁道具
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function ItemDenyEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $playerUrn = $data_payload['playerUrn'];
            $event_type = 'item_denied';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['player'] = Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            $eventArr['item'] = Dota2SocketEventService::getEventFormatItemByExternalId($model,$data_payload['itemId']);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     * 使用道具
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function ItemUseEvent($model, $data_payload)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $playerUrn     = $data_payload['playerUrn'];
            $playerTeamUrn = $data_payload['playerTeamUrn'];
            $itemName      = $data_payload['itemName'];
            $playerInfo    = Dota2SocketPlayerService::getPlayerInfo($model, $playerUrn);
            $teamInfo      = Dota2SocketTeamService::getTeamInfo($model, $playerTeamUrn);
            switch ($itemName) {
                case 'item_dust':
                    //使用显影之尘
                    $playerUpdate['dust_used'] = $playerInfo['dust_used'] + 1;
                    $teamUpdate['dust_used']   = $teamInfo['dust_used'] + 1;
                    break;
                case 'item_ward_observer':
                    //放置侦查守卫
                    $playerUpdate['observer_wards_placed'] = $playerInfo['observer_wards_placed'] + 1;
                    $teamUpdate['observer_wards_placed']   = $teamInfo['observer_wards_placed'] + 1;
                    break;
                case 'item_ward_sentry':
                    //放置侦查守卫
                    $playerUpdate['sentry_wards_placed'] = $playerInfo['sentry_wards_placed'] + 1;
                    $teamUpdate['sentry_wards_placed']   = $teamInfo['sentry_wards_placed'] + 1;
                    break;

            }
            if (!empty($playerUpdate)) {
                Dota2SocketPlayerService::setPlayerInfo($model, $playerUrn, $playerUpdate);
            }
            if (!empty($teamUpdate)) {
                Dota2SocketTeamService::setTeamInfo($model, $playerTeamUrn, $teamUpdate);
            }
        }
        //不输出的事件
        return false;

    }

    /**
     * ward_placed(放置守卫)
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function WardPlacedEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $wardPosition = $data_payload['wardPosition'];
            $wardType = strtolower($data_payload['wardType']);
            $wardOwnerUrn = $data_payload['wardOwnerUrn'];
            $event_type = 'ward_placed';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['position'] = implode(',',$wardPosition);
            $eventArr['ward_type'] = $wardType;
            $eventArr['owner'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$wardOwnerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     * 摧毁守卫
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function WardDestroyedEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $wardPosition = $data_payload['wardPosition'];
            $wardType = strtolower($data_payload['wardType']);
            $wardOwnerUrn = $data_payload['wardOwnerUrn'];
            $killerUrn = $data_payload['killerUrn'];
            $killerTeamUrn = $data_payload['killerTeamUrn'];
            $event_type = 'ward_kill';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['position'] = implode(',',$wardPosition);
            $eventArr['ward_type'] = $wardType;
            $eventArr['killer'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$killerUrn);
            $eventArr['owner'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$wardOwnerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            if (!empty($killerUrn) && !empty($killerTeamUrn)){
                $playerUpdate = $teamUpdate = [];
                //查询选手和队伍信息
                $playerInfo    = Dota2SocketPlayerService::getPlayerInfo($model, $killerUrn);
                $teamInfo      = Dota2SocketTeamService::getTeamInfo($model, $killerTeamUrn);
                switch ($wardType) {
                    case 'observer':
                        //使用显影之尘
                        $playerUpdate['observer_wards_kills'] = $playerInfo['observer_wards_kills'] + 1;
                        $teamUpdate['observer_wards_kills']   = $teamInfo['observer_wards_kills'] + 1;
                        break;
                    case 'sentry':
                        //放置侦查守卫
                        $playerUpdate['sentry_wards_kills'] = $playerInfo['sentry_wards_kills'] + 1;
                        $teamUpdate['sentry_wards_kills']   = $teamInfo['sentry_wards_kills'] + 1;
                        break;
                }
                if (!empty($playerUpdate)) {
                    Dota2SocketPlayerService::setPlayerInfo($model, $killerUrn, $playerUpdate);
                }
                if (!empty($teamUpdate)) {
                    Dota2SocketTeamService::setTeamInfo($model, $killerTeamUrn, $teamUpdate);
                }
            }
            return $arr;
        }
    }

    /**
     * 选手复活
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function HeroSpawnEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $position = $data_payload['position'];
            $playerUrn = $data_payload['playerUrn'];
            $event_type = 'player_spawned';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['position'] = implode(',',$position);
            $eventArr['player'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            //设置选手的respawntimer
            $playerUpdate = [
                'respawntimer' => 0,
                'is_alive' => true,
                'respawntimer_duration' => 0
            ];
            Dota2SocketPlayerService::setPlayerInfo($model,$playerUrn,$playerUpdate);

            return $arr;
        }
    }


    /**
     *  击杀信使
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function MinionDeathEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $position = $data_payload['minionPosition'];
            $killerUrn = $data_payload['killerUrn'];
            $ownerUrn = $data_payload['ownerUrn'];
            $event_type = 'courier_kill';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['position'] = implode(',',$position);
            $eventArr['killer'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$killerUrn);
            $eventArr['owner'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$ownerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     *  使用特殊技能
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function AbilityUsedEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $abilityName = $data_payload['abilityName'];
            $playerUrn = $data_payload['playerUrn'];
            $event_type = 'special_ability_used';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['special_ability_name'] =$abilityName;
            $eventArr['player'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }
    /**
     *  开雾
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function PlayersCloakedEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $playerUrn = $data_payload['playerUrn'];
            $playerTeamUrn = $data_payload['playerTeamUrn'];
            $cloakedPlayers = $data_payload['cloakedPlayers'];
            $playerInfo = Dota2SocketPlayerService::getPlayerInfo($model,$playerUrn);
            $teamInfo = Dota2SocketTeamService::getTeamInfo($model,$playerTeamUrn);
            $event_type = 'smoke_used';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['owner'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            //更新player
            $playerUpdate['smoke_used'] = $playerInfo['smoke_used']+1;
            $teamUpdate['smoke_used'] = $teamInfo['smoke_used']+1;
            Dota2SocketPlayerService::setPlayerInfo($model,$playerUrn,$playerUpdate);
            Dota2SocketTeamService::setTeamInfo($model, $playerTeamUrn, $teamUpdate);
            if (!empty($cloakedPlayers)){
                foreach ($cloakedPlayers as $k=>$v){
                    $eventArr['coverage'][] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$v);
                }
            }else{
                $eventArr['coverage'] = [];
            }

            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }
    /**
     *  破雾
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function PlayerUncloakedEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $playerUrn = $data_payload['playerUrn'];
            $cloakedPlayers = $data_payload['cloakedPlayers'];
            $event_type = 'smoke_broke';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['player'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$playerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }



    /**
     * 眼过期
     * @param $model
     * @param $data_payload
     * @return mixed
     */
    public static function WardExpiredEvent($model, $data_payload)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        if ($data_payload) {
            $wardPosition = $data_payload['wardPosition'];
            $wardType = strtolower($data_payload['wardType']);
            $wardOwnerUrn = $data_payload['wardOwnerUrn'];
            $event_type = 'ward_expired';
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $eventArr['position'] = implode(',',$wardPosition);
            $eventArr['ward_type'] = $wardType;
            $eventArr['owner'] =  Dota2SocketEventService::getEventPlayerFormatByUrn($model,$wardOwnerUrn);
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }


    public static function getEventFormatItemByExternalId($model,$external_id){

        $itemList = $model->tableItemList;
        $item = $itemList[$external_id];
        if (empty($item)){
            $item = Dota2SocketMatchService::getItemUnknownInfo($model);
        }
        $format['item_id'] = (int)$item['id'];
        $format['name'] = $item['name'];
        $format['name_cn'] = $item['name_cn'];
        $format['external_id'] = (string)$item['external_id'];
        $format['external_name'] = $item['external_name'];
        $format['total_cost'] = (int)$item['total_cost'];
        $format['is_recipe'] = $item['is_recipe']==1;
        $format['is_secret_shop'] = $item['is_secret_shop']==1;
        $format['is_home_shop'] = $item['is_home_shop']==1;
        $format['is_neutral_drop'] = $item['is_neutral_drop']==1;
        $format['slug'] = $item['slug'];
        $format['image'] = $item['image'];

        return $format;
    }
    /**
     * endPauseEvent
     * @param $model
     * @param $match_id
     * @param $data_payload
     */
    public static function endPauseEvent($model, $data_payload)
    {
        if ($data_payload) {
            if ($data_payload) {
                $event_type             = 'battle_unpause';
                $duration = intval($data_payload['gameTime']/1000);
                $update['is_pause'] = false;
                $update['duration'] = $duration;
                Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);
                $eventArr               = Dota2SocketEventService::getEventCommon($model);
                $eventArr['event_type'] = $event_type;
                $arr['data']            = $eventArr;
                $arr['type']            = 'events';
                return $arr;
            }
        }
    }


    /**
     * battle_start
     * @param $model
     * @param $match_id
     * @param $data_payload
     */
    public static function startMapEvent($model, $match_id, $data_payload,$sourceUpdatedAt)
    {
        if ($data_payload) {
            //$startTime    = $model->info['socket_time']; //改到用frames 里面的时间
            $matchCurrent = $data_payload['matchCurrent'];
            //更新battle开始时间
            if (empty($sourceUpdatedAt)){
                $begin_at_time = $model->info['socket_time'];
            }else{
                $begin_at_time = date("Y-m-d H:i:s",strtotime($sourceUpdatedAt));
            }
            $battleUpdate['begin_at'] = $begin_at_time;
            $battleUpdate['status']   = 2;
            Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleUpdate);
            if ($matchCurrent == 1) {
                if ($model->operate_type != 'xiufu'){
                    //更新match开始时间
                    Dota2SocketMatchService::setMatchDetails($model, $battleUpdate);
                }

            }
            $teamList               = Dota2SocketTeamService::getTeamListFormat($model, 'urnField');
            foreach ($teamList as $k=>$v){
                if (!empty($k)){
                    Dota2SocketTeamService::setTeamInfo($model, $k, ['score' => 0]);
                }
            }
            $event_type = 'battle_start';

            $map                    = Dota2SocketBattleService::getMap($model, 1);
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['ingame_timestamp'] = -90;
            $eventArr['event_type'] = $event_type;
            $eventArr['map']        = $map;
            $teamFormatList         = Dota2SocketTeamService::getTeamListFormat($model);
            $eventArr['radiant']    = $teamFormatList['radiant'];
            $eventArr['dire']       = $teamFormatList['dire'];
            $arr['data']            = $eventArr;
            $arr['type']            = 'events';
            return $arr;
        }
    }

    /**
     * battle_end
     * @param $model
     * @param $match_id
     * @param $data_payload
     * @return mixed
     */
    public static function endMapEvent($model, $match_id, $data_payload,$sourceUpdatedAt)
    {
        if (empty($model)) {
            $model = new Dota2Socket();
        }
        //更新battle开始时间
        if (empty($sourceUpdatedAt)){
            $end_at_time = $model->info['socket_time'];
        }else{
            $end_at_time = date("Y-m-d H:i:s",strtotime($sourceUpdatedAt));
        }
        if ($data_payload) {
            $event_type             = 'battle_end';
            $matchCurrent           = $data_payload['matchCurrent'];
            $winningTeamUrn         = $data_payload['winningTeamUrn'];
            $eventArr               = Dota2SocketEventService::getEventCommon($model);
            $eventArr['event_type'] = $event_type;
            $teamList               = Dota2SocketTeamService::getTeamListFormat($model, 'urnField');
            //更新match详情
            $matchDetails = Dota2SocketMatchService::getMatchDetails($model);
            $teamWinner   = $teamList[$winningTeamUrn];

            foreach ($teamList as $k=>$v){
                if (!empty($k)&&$k!=$winningTeamUrn){
                    $anotherTeam = $k;
                }
            }

            if ($teamWinner['opponent_order'] == 1) {
                $matchDetails['team_1_score'] += 1;
                Dota2SocketTeamService::setTeamInfo($model, $winningTeamUrn, ['score' => 1]);
                if ($anotherTeam){
                    Dota2SocketTeamService::setTeamInfo($model, $anotherTeam, ['score' => 0]);
                }

            }
            if ($teamWinner['opponent_order'] == 2) {
                $matchDetails['team_2_score'] += 1;
                Dota2SocketTeamService::setTeamInfo($model, $winningTeamUrn, ['score' => 1]);
                if ($anotherTeam){
                    Dota2SocketTeamService::setTeamInfo($model, $anotherTeam, ['score' => 0]);
                }
            }
            //$end_at                         = $model->info['socket_time'];
            $statusInfo                     = WinnerHelper::lolWinnerInfo($matchDetails['team_1_score'], $matchDetails['team_2_score'], $model->match['number_of_games']);
            $match_winner['team_id']        = $teamWinner['team_id'];
            $match_winner['name']           = $teamWinner['name'];
            $match_winner['image']          = $teamWinner['image'];
            $match_winner['opponent_order'] = $teamWinner['opponent_order'];
            if ($statusInfo['is_finish'] == 1) {
                $model->matchEndFlag    = true;
                $is_match_finished      = true;
                $matchDetails['status'] = 3;
                //$matchDetails['end_at'] = $end_at;
                if ($statusInfo['is_draw'] == 1) {//判断是不是平局
                    $matchDetails['winner']  = null;
                    $matchDetails['is_draw'] = 1;
                } else {
                    $matchDetails['winner'] = $statusInfo['winner_team'];
                    $winner_id              = $statusInfo['winner_team'];
                }
                $matchDetails['winner_details'] = $match_winner;
                $matchDetails['end_at'] = $end_at_time;
            }
            Dota2SocketMatchService::setMatchDetails($model, $matchDetails);
            //更新battle详情
            $battleDetails                   = Dota2SocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
            $battleDetails['status']         = 3;
            $battleDetails['winner_details'] = $match_winner;
            //$battleDetails['end_at']         = $end_at;
            $battleDetails['end_at'] = $end_at_time;
            Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleDetails);
//            if ($statusInfo['is_finish'] == 1) {
//                //自己创建一个必输出的frame
//                self::getNeedOutPutFrame($model, 'match_end');
//            } else {
//                //自己创建一个必输出的frame
//                self::getNeedOutPutFrame($model, 'battle_end');
//            }
            //events详情
            $eventArr['winner'] = $match_winner;
            $arr['data']        = $eventArr;
            $arr['type']        = 'events';
            return $arr;
        }
    }

    public static function getNeedOutPutFrame($model, $eventType)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $key           = 'bayes_dota_ws:' . $model->matchId . ':history:frames';
        $lastFrameJson = $model->redis->lIndex($key, 1);
        $lastFrameArr  = json_decode($lastFrameJson, true);
        if ($eventType == 'battle_end') {
            $lastFrameArr['is_battle_finished'] = true;
        } elseif ($eventType == 'match_end') {
            $lastFrameArr['is_battle_finished'] = true;
            $lastFrameArr['is_match_finished']  = true;
        }
        $matchDetails = Dota2SocketMatchService::getMatchDetails($model);
        if ($lastFrameArr['match_scores'][0]['opponent_order'] == 1) {
            $lastFrameArr['match_scores'][0]['score'] = $matchDetails['team_1_score'];
            $lastFrameArr['match_scores'][1]['score'] = $matchDetails['team_2_score'];
        } else {
            $lastFrameArr['match_scores'][1]['score'] = $matchDetails['team_1_score'];
            $lastFrameArr['match_scores'][0]['score'] = $matchDetails['team_2_score'];
        }
        $battleDetails                 = Dota2SocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        $lastFrameArr['match_winner']  = $matchDetails['winner_details'] ? $matchDetails['winner_details'] : null;
        $lastFrameArr['battle_winner'] = $battleDetails['winner_details'] ? $battleDetails['winner_details'] : null;
        $model->needOutPutFrame        = $lastFrameArr;

    }

    public static function getBattleEventByBattleId($model, $battleId)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $key  = ['history', 'battle', $battleId, 'events'];
        $list = $model->lRange($key, 0, -1);
        return array_reverse($list);
    }

    /**
     * 推塔事件或者占领建筑物事件
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function ObjectiveTakenEvent($model, $matchId, $battleId, $data_payload)
    {
        if (!empty($data_payload)) {
            $takenObjective = $data_payload['takenObjective'];
            if ($takenObjective=='DESTROYED'){
                //推塔事件
                return   Dota2SocketEventService::getDestroyedTower($model,$data_payload);
            }else{
                //占领建筑物事件
                return   Dota2SocketEventService::getTakenTower($model,$data_payload);
            }

        }
    }
    /**
     * 击杀肉山
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function AncientKillEvent($model, $data_payload)
    {
        if (!empty($data_payload)) {
            $killerUrn       = $data_payload['killerUrn'];
            $killerTeamUrn   = $data_payload['killerTeamUrn'];
            $killerFaction   = $data_payload['killerFaction'];
            $killerPosition  = $data_payload['killerPosition'];
            $ancientPosition = $data_payload['ancientPosition'];
            $eventArr                    = Dota2SocketEventService::getEventCommon($model);
            //选手推塔
            $killer = Dota2SocketEventService::getEventPlayerFormatByUrn($model, $killerUrn);
            $killerTeam = Dota2SocketTeamService::getTeamInfo($model,$data_payload['killerTeamUrn']);
            $team_order = $killerTeam['order'];
            $victim                      = Dota2SocketEventService::getEventBuildingFormat($model, $data_payload,'ROSHAN',$killerFaction);
            $event_type ='elite_kill';
            $eventArr['event_type']      = $event_type;
            $eventArr['killer_position'] = implode(',',$killerPosition);
            $eventArr['killer']          = $killer;
            $eventArr['victim_position'] = implode(',',$ancientPosition);
            $eventArr['victim']          = $victim;
            $eventFirst                  = self::getIsEventSpecial($model,$eventArr,'ROSHAN',$team_order);
            //判断是否特殊事件
            $eventArr['is_first_event']   = $eventFirst['is_first_event'];
            $eventArr['first_event_type'] = $eventFirst['first_event_type'];
            $returnArr['type']            = 'events';
            $returnArr['data']            = $eventArr;
            return $returnArr;
        }
    }

    /**
     * 推塔事件
     * @param $model
     * @param $data_payload
     */
    public static function getDestroyedTower($model, $data_payload)
    {
        $buildingTypeArray = ['BARRACKS','TOWER','OUTPOST','ANCIENT'];
        $buildingType = $data_payload['buildingType'];
        if (!in_array($buildingType,$buildingTypeArray)){
            return null;
        }
        //有选手是选手推塔,没选手有阵营是小兵推塔(军团推塔),没选手没阵营是基地自爆
        $attackUrn       = $data_payload['attackerUrn'];//有值则是player
        $attackerTeamUrn = $data_payload['attackerTeamUrn'];//Team
        $attackFaction   = $data_payload['attackerFaction'];//阵营RADIANT DIRE UNKNOWN(基地自爆)
        $buildingFaction = $data_payload['buildingFaction'];//塔的阵营
        if ($buildingFaction == $attackFaction){
            $event_type      = 'building_denied';
        }else{
            $event_type      = 'building_kill';
        }
        if ($attackUrn && $attackerTeamUrn) {
            //选手推塔
            $killer = Dota2SocketEventService::getEventPlayerFormatByUrn($model, $data_payload['attackerUrn']);
            $killerInfo = Dota2SocketPlayerService::getPlayerInfo($model, $data_payload['attackerUrn']);
            $killerTeam = Dota2SocketTeamService::getTeamInfo($model,$data_payload['attackerTeamUrn']);
            $team_order = $killerTeam['order'];
        } elseif (empty($attackUrn) && $attackerTeamUrn && $attackFaction != 'UNKNOWN') {
            //军团推塔
            $killer = Dota2SocketEventService::getEventFactionFormat($model, $data_payload['attackerFaction']);
            $killerTeam = Dota2SocketTeamService::getTeamInfo($model,$data_payload['attackerTeamUrn']);
            $team_order = $killerTeam['order'];
        } else {
            //基地自爆
            $killer = Dota2SocketEventService::getEventUnknownPlayerFormat($model, $data_payload['attackerUrn'],'unknown');
            $victimTeam = Dota2SocketTeamService::getTeamInfo($model,$data_payload['buildingTeamUrn']);
            $revArr=[1=>2,2=>1];
            $team_order = $revArr[$victimTeam['order']];
        }

        $victim                      = Dota2SocketEventService::getEventBuildingFormat($model, $data_payload,'',$data_payload['buildingFaction']);
        $eventArr                    = Dota2SocketEventService::getEventCommon($model);
        $eventArr['event_type']      = $event_type;
        $eventArr['killer_position'] = null;
        $eventArr['killer']          = $killer;
        $eventArr['victim_position'] = null;
        $eventArr['victim']          = $victim;
        $eventFirst                  = self::getIsEventSpecial($model,$eventArr,$data_payload['buildingType'],$team_order);
        if (!empty($killerInfo)){
            //选手击杀
            //kills
            switch ($data_payload['buildingType']){
                case 'BARRACKS':
                    $playerUpdate['barrack_kills'] = $killerInfo['barrack_kills']+1;
                    if ($data_payload['towerTier']=='Melee'){
                        $playerUpdate['melee_barrack_kills'] = $killerInfo['melee_barrack_kills']+1;
                    }
                    break;
            }
            if (!empty($playerUpdate)){
                Dota2SocketPlayerService::setPlayerInfo($model,$attackUrn,$playerUpdate);
            }
        }

        //判断是否特殊事件
        $eventArr['is_first_event']   = $eventFirst['is_first_event'];
        $eventArr['first_event_type'] = $eventFirst['first_event_type'];
        $returnArr['type']            = 'events';
        $returnArr['data']            = $eventArr;
        return $returnArr;
    }

    /**
     * OUTPOST(前哨)，BARRACKS(兵营)，TOWER(塔)
     * @param $model
     * @param $eventArr
     * @param $buildingType
     * @param $team_order
     * @return mixed
     */
    public static function getIsEventSpecial($model,$eventArr,$buildingType,$team_order){
        unset($eventArr['victim']['ingame_id']);
        //计算首塔,首兵营
        $battleDetails = Dota2SocketBattleService::getBattleDetailsByUrn($model,$model->battleUrn);
        if ($buildingType=='TOWER') {
            //首塔
            if (empty($battleDetails['first_tower'])){
                $eventArr['is_first_event']                        = true;
                $eventArr['first_event_type']                      = 'first_tower';
                $first_tower_details['ingame_timestamp']           = $model->duration;
                $first_tower_details['faction']                    = $eventArr['killer']['faction'];
                $first_tower_details['team_id']                    = (int)$team_order;
                $first_tower_details['details']['killer_position'] = $eventArr['killer_position'];
                $first_tower_details['details']['killer']          = $eventArr['killer'];
                $first_tower_details['details']['victim_position'] = $eventArr['victim_position'];
                $first_tower_details['details']['victim']          = $eventArr['victim'];
                $update['first_tower']                             = $first_tower_details;
            }else{
                $eventArr['is_first_event']                        = false;
                $eventArr['first_event_type']                      = null;
            }
            $update[$team_order . '_real_towers']             = $battleDetails[$team_order . '_real_towers'] + 1;
            Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $update);
        }elseif ($buildingType=='BARRACKS'){
            //首兵营
            if (empty($battleDetails['first_barrack'])){
                $eventArr['is_first_event']                        = true;
                $eventArr['first_event_type']                      = 'first_barrack';
                $first_details['ingame_timestamp']           = $model->duration;
                $first_details['faction']                    = $eventArr['killer']['faction'];
                $first_details['team_id']                    =  (int)$team_order;
                $first_details['details']['killer_position'] = $eventArr['killer_position'];
                $first_details['details']['killer']          = $eventArr['killer'];
                $first_details['details']['victim_position'] = $eventArr['victim_position'];
                $first_details['details']['victim']          = $eventArr['victim'];
                $update['first_barrack']                             = $first_details;
            }else{
                $eventArr['is_first_event']                        = false;
                $eventArr['first_event_type']                      = null;
            }
            $update[$team_order . '_real_barracks']             = $battleDetails[$team_order . '_real_barracks'] + 1;
            Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $update);
        }elseif ($buildingType=='ROSHAN'){
            //首肉山
            if (empty($battleDetails['first_roshan'])){
                $eventArr['is_first_event']                        = true;
                $eventArr['first_event_type']                      = 'first_roshan';
                $first_details['ingame_timestamp']           = $model->duration;
                $first_details['faction']                    = $eventArr['killer']['faction'];
                $first_details['team_id']                    =  (int)$team_order;
                $first_details['details']['killer_position'] = $eventArr['killer_position'];
                $first_details['details']['killer']          = $eventArr['killer'];
                $first_details['details']['victim_position'] = $eventArr['victim_position'];
                $first_details['details']['victim']        = $eventArr['victim'];
                $update['first_roshan']                             = $first_details;
            }else{
                $eventArr['is_first_event']                        = false;
                $eventArr['first_event_type']                      = null;
            }
            $update[$team_order . '_real_kill_roshans']             = $battleDetails[$team_order . '_real_kill_roshans'] + 1;
            $update['real_kill_roshans']             = $battleDetails['real_kill_roshans'] + 1;
            Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $update);
        }else{
            $eventArr['is_first_event']                        = false;
            $eventArr['first_event_type']                      = null;
        }
        return $eventArr;
    }


    public static function getEventFactionFormat($model, $faction)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $tableIngameList = $model->tableIngameGoalList;
        foreach ($tableIngameList as $k => $vArr) {
            if ($vArr['game_id'] == 3 && $vArr['sub_type'] == 'faction') {
                $dbIngame = $vArr;
            }
        }
        $formatPlayer['ingame_obj_name']     = $dbIngame['ingame_obj_name'];
        $formatPlayer['ingame_obj_name_cn']  = $dbIngame['ingame_obj_name_cn'];
        $formatPlayer['faction']             = strtolower($faction);
        $formatPlayer['ingame_obj_type']     = $dbIngame['ingame_obj_type'];
        $formatPlayer['ingame_obj_sub_type'] = $dbIngame['sub_type'];
        return $formatPlayer;
    }

    public static function getEventBuildingFormat($model, $data_payload,$type,$faction)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $dbId = $sub_type = null;
        $building_lane = null;
        //肉山
        if ($type=='ROSHAN'){
            $dbId=27;
        }else{
            //建筑
            $buildingType = $data_payload['buildingType'];
            $lane         = $data_payload['lane'];
            $towerTier    = $data_payload['towerTier'];
            switch ($buildingType) {
                case 'ANCIENT':
                    //基地
                    $sub_type = 'ancient';
                    $dbId     = 49;
                    break;
                case 'OUTPOST':
                    //前哨
                    $sub_type = 'outpost';
                    $dbId     = 61;
                    break;
                case 'TOWER':
                    //塔  lane : UNKNOWN 是 41
                    $sub_type = 'tower';
                    if ($lane == 'TOP' && $towerTier == 'TIER_1') {
                        $dbId = 30;
                        $building_lane = 'top_tier_1_tower';
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_2') {
                        $dbId = 31;
                        $building_lane = 'top_tier_2_tower';
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_3') {
                        $dbId = 32;
                        $building_lane = 'top_tier_3_tower';
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_4') {
                        $dbId = 33;
                        $building_lane = 'top_tier_4_tower';
                    }
                    if ($lane == 'MID' && $towerTier == 'TIER_1') {
                        $dbId = 34;
                        $building_lane = 'mid_tier_1_tower';
                    } elseif ($lane == 'MID' && $towerTier == 'TIER_2') {
                        $dbId = 35;
                        $building_lane = 'mid_tier_2_tower';
                    } elseif ($lane == 'MID' && $towerTier == 'TIER_3') {
                        $dbId = 36;
                        $building_lane = 'mid_tier_3_tower';
                    }
                    if ($lane == 'BOT' && $towerTier == 'TIER_1') {
                        $dbId = 37;
                        $building_lane = 'bot_tier_1_tower';
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_2') {
                        $dbId = 38;
                        $building_lane = 'bot_tier_2_tower';
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_3') {
                        $dbId = 39;
                        $building_lane = 'bot_tier_3_tower';
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_4') {
                        $dbId = 40;
                        $building_lane = 'bot_tier_4_tower';
                    }
                    if (!$dbId){
                        if ($towerTier == 'TIER_4'){
                            $dbId = 65;
                        }else{
                            $dbId = 41;
                        }
                    }
                    break;
                case 'BARRACKS'://计算  近战兵营数量 远程兵营数量
                    //兵营
                    $sub_type = 'barrack';
                    if ($lane == 'TOP'){
                        if ($towerTier == 'Melee'){
                            $dbId = 42;
                        }elseif ($towerTier == 'Ranged'){
                            $dbId = 43;
                        }else{
                            $dbId = 64;
                        }
                    }
                    if ($lane == 'MID'){
                        if ($towerTier == 'Melee'){
                            $dbId = 44;
                        }elseif ($towerTier == 'Ranged'){
                            $dbId = 45;
                        }else{
                            $dbId = 63;
                        }
                    }
                    if ($lane == 'BOT'){
                        if ($towerTier == 'Melee'){
                            $dbId = 46;
                        }elseif ($towerTier == 'Ranged'){
                            $dbId = 47;
                        }else{
                            $dbId = 62;
                        }
                    }
                    if (empty($dbId)){
                        $dbId=48;
                    }
                    break;
                default:
                    $sub_type = null;
                    break;
            }
            if (empty($sub_type)) {
                $format['lane']                = 'unknown';
                $format['ingame_obj_name']     = 'unknown';
                $format['ingame_obj_name_cn']  = '未知';
                $format['faction']             = strtolower($data_payload['buildingFaction']);
                $format['ingame_obj_type']     = 'unknown';
                $format['ingame_obj_sub_type'] = 'unknown';
                return $format;
            }
        }


        $tableIngameList = $model->tableIngameGoalList;
        if ($dbId) {
            $dbIngame = $tableIngameList[$dbId];
        }
        if (empty($dbIngame)) {
            $dbIngame['lane']                = 'unknown';
            $dbIngame['ingame_obj_name']     = 'unknown';
            $dbIngame['ingame_obj_name_cn']  = '未知';
            $dbIngame['faction']             = strtolower($faction);
            $dbIngame['ingame_obj_type']     = 'unknown';
            $dbIngame['ingame_obj_sub_type'] = 'unknown';
        }
        if ($type=='ROSHAN'){//肉山的格式
            $format['ingame_obj_name']     = $dbIngame['ingame_obj_name'];
            $format['ingame_obj_name_cn']  = $dbIngame['ingame_obj_name_cn'];
            $format['ingame_obj_type']     = $dbIngame['ingame_obj_type'];
            $format['ingame_obj_sub_type'] = $dbIngame['sub_type'];
        }else{
            $format['lane']                = $dbIngame['lane'] ? $dbIngame['lane'] : null;
            $format['ingame_obj_name']     = $dbIngame['ingame_obj_name'];
            $format['ingame_obj_name_cn']  = $dbIngame['ingame_obj_name_cn'];
            $format['faction']             = strtolower($faction);
            $format['ingame_obj_type']     = $dbIngame['ingame_obj_type'];
            $format['ingame_obj_sub_type'] = $dbIngame['sub_type'];
            $format['ingame_id'] = $dbId;
        }
        return $format;
    }
    //TakenTower
    public static function getTakenTower($model,$data_payload){
        $attackerFaction = $data_payload['attackerFaction'];//Team
        $event_type      = 'building_captured';
        $capturer = null;
        if ($attackerFaction){
            $capturer = [
                'ingame_obj_name' => 'faction',
                'ingame_obj_name_cn' => '军团',
                'faction' => strtolower($attackerFaction),
                'ingame_obj_type' => 'faction',
                'ingame_obj_sub_type' => 'faction'
            ];
        }else{
            $capturer = [
                'ingame_obj_name' => 'unknown',
                'ingame_obj_name_cn' => '未知',
                'ingame_obj_type' => 'unknown',
                'ingame_obj_sub_type' => 'unknown'
            ];
        }
        $victim                      = Dota2SocketEventService::getEventBuildingFormat($model, $data_payload,'',$data_payload['buildingFaction']);
        $eventArr                    = Dota2SocketEventService::getEventCommon($model);
        $eventArr['event_type']      = $event_type;
        $eventArr['capturer_position'] = null;
        $eventArr['capturer']          = $capturer;
        $eventArr['victim_position'] = null;
        $eventArr['victim']          = $victim;
        //判断是否特殊事件
        $eventArr['is_first_event']   = false;
        $eventArr['first_event_type'] = null;
        $returnArr['type']            = 'events';
        $returnArr['data']            = $eventArr;
        return $returnArr;
    }



    public static function getHeroInfoByExternalId($model, $externalId)
    {
        if (array_key_exists($externalId, $model->tableHeroList)) {
            $formatHero                         = [];
            $dbHero                             = $model->tableHeroList[$externalId];
            $formatHero['hero_id']              = (int)$dbHero['id'];
            $formatHero['name']                 = $dbHero['name'];
            $formatHero['name_cn']              = $dbHero['name_cn'];
            $formatHero['external_id']          = (string)$dbHero['external_id'];
            $formatHero['external_name']        = $dbHero['external_name'];
            $formatHero['title']                = $dbHero['title'];
            $formatHero['title_cn']             = $dbHero['title_cn'];
            $formatHero['slug']                 = $dbHero['slug'];
            $formatHero['image']['image']       = $dbHero['image'];
            $formatHero['image']['small_image'] = $dbHero['small_image'];
            return $formatHero;
        } else {
            return null;
        }
    }


    public static function getBanPickList($model, $battleId = null)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        if ($battleId) {
            $key = ['history', 'ban_pick', $battleId, 'list'];
        } else {
            $key = ['history', 'ban_pick', $model->battleId, 'list'];
        }

        return array_reverse($model->lRange($key, 0, -1));
    }

    public static function setBanPick($model, $json)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $key = ['history', 'ban_pick', $model->battleId, 'list'];
        return $model->lPush($key, $json);
    }

    /**
     * banPick事件
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function teamUpdateBanPickEvent($model, $matchId, $battleId, $data_payload)
    {
        if (!empty($data_payload)) {
            if (!$model) {
                $model = new Dota2Socket();
            }
            $teams = $data_payload['teams'];
            if (!empty($teams)) {
                $teamRedisList     = Dota2SocketTeamService::getTeamList($model);
                $playerRedisList   = Dota2SocketPlayerService::getPlayerList($model);
                $banPickRedisList  = self::getBanPickList($model);
                $banPickHeroColumn = [];
                foreach ($banPickRedisList as $k => $v) {
                    $vArr                = json_decode($v, true);
                    $banPickHeroColumn[] = $vArr['hero']['external_id'];
                }
                $countBanPick = count($banPickRedisList);
                $startOrder   = $countBanPick + 1;
                foreach ($teams as $k_team => $v_team) {
                    $faction         = strtolower($v_team['faction']);
                    $liveDataTeamUrn = $v_team['liveDataTeamUrn'];
                    if (array_key_exists($liveDataTeamUrn, $teamRedisList)) {
                        $teamDetails            = json_decode($teamRedisList[$liveDataTeamUrn], true);
                        $teamDetails = [];
                        $teamDetails['faction'] = $faction;
                        Dota2SocketTeamService::setTeamInfo($model, $liveDataTeamUrn, $teamDetails);
                    }else{
                        $matchInfo = $model->match;
                        $team_urn_id = $liveDataTeamUrn;
                        $team_id = @self::getMainIdOfBayesByUrnOnDota('team', $model->origin_id, $team_urn_id, 3,$matchInfo);
                        //获取$team_order
                        $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
                        $team_info = [
                            'team_id'         => $team_id,
                            'order'           => $team_order,
                            'faction'         => $faction,
                            'score'           => 0,
                            'rel_identity_id' => $team_urn_id,
                            'identity_id'     => $team_urn_id,
                            'smoke_used' => 0,
                            'dust_used' => 0,
                            'bounty_runes_pickedup' => null,
                            'double_damage_runes_pickedup' => null,
                            'haste_runes_pickedup' => null,
                            'illusion_runes_pickedup' => null,
                            'invisibility_runes_pickedup' => null,
                            'regeneration_runes_pickedup' => null,
                            'arcane_runes_pickedup' => null,
                            'observer_wards_kills' => 0,
                            'observer_wards_placed' => 0,
                            'sentry_wards_kills' => 0,
                            'sentry_wards_placed' => 0
                        ];
                        Dota2SocketTeamService::setTeamInfo($model, $team_urn_id, $team_info);
                    }
                    //ban_pick
                    $teamFormat = Dota2SocketTeamService::getTeamListFormat($model);
                    if (!empty($v_team['bans'])) {
                        $event_type = 'hero_banned';
                        foreach ($v_team['bans'] as $k => $v) {
                            if (!in_array($v['heroId'], $banPickHeroColumn)) {
                                $heroExternalId                  = $v['heroId'];
                                $eventArr                        = Dota2SocketEventService::getEventCommon($model);
                                $eventArr['ingame_timestamp'] = -90;
                                $eventArr['event_type']          = $event_type;
                                $eventArr['bp_order']            = $startOrder;
                                $eventArr['faction']             = $faction;
                                $eventArr['team']                = $teamFormat[$faction];
                                $eventArr['hero']                = self::getHeroInfoByExternalId($model, $heroExternalId);
                                $model->needOutPutBanPickEvent[] = $eventArr;
                                self::setBanPick($model, @json_encode($eventArr, 320));
                                $startOrder++;
                            }
                        }
                    }
                    if (!empty($v_team['bans'])) {
                        $event_type = 'hero_picked';
                        foreach ($v_team['picks'] as $k => $v) {
                            if (!in_array($v['heroId'], $banPickHeroColumn)) {
                                $heroExternalId                  = $v['heroId'];
                                $eventArr                        = Dota2SocketEventService::getEventCommon($model);
                                $eventArr['ingame_timestamp'] = -90;
                                $eventArr['event_type']          = $event_type;
                                $eventArr['bp_order']            = $startOrder;
                                $eventArr['faction']             = $faction;
                                $eventArr['team']                = $teamFormat[$faction];
                                $eventArr['hero']                = self::getHeroInfoByExternalId($model, $heroExternalId);
                                $model->needOutPutBanPickEvent[] = $eventArr;
                                self::setBanPick($model, @json_encode($eventArr, 320));
                                $startOrder++;
                            }
                        }
                    }
                    //players
                    if (!empty($v_team['players'])) {
                        foreach ($v_team['players'] as $k_player => $v_player) {
                            $liveDataPlayerUrn = $v_player['liveDataUrn'];
                            if (array_key_exists($liveDataPlayerUrn, $playerRedisList)) {
                                $playerDetails            = json_decode($playerRedisList[$liveDataPlayerUrn], true);
                                $playerDetails['faction'] = $faction;
                                Dota2SocketPlayerService::setPlayerInfo($model, $liveDataPlayerUrn, $playerDetails);
                            }else{
                                $player_rel_name = @$v_player['name'];
                                $nick_name = $v_player['name'];
                                $steam_id  = $v_player['references']['STEAM_ID_64'];
                                $player_id = @self::getMainIdOfBayesByUrn('player', $model->origin_id, $liveDataPlayerUrn, 3);
                                if ($player_id) {
                                    $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                                    $nick_name   = @$Player_Info['nick_name'];
                                }
                                $matchInfo = $model->match;
                                $team_urn_id = $liveDataTeamUrn;
                                $team_id = @self::getMainIdOfBayesByUrnOnDota('team', $model->origin_id, $team_urn_id, 3,$matchInfo);
                                //获取$team_order
                                $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
                                $player_Info = [
                                    'order'           => $k_player + 1,
                                    'rel_identity_id' => $liveDataPlayerUrn,
                                    'rel_team_id'     => $liveDataTeamUrn,
                                    'team_order'      => $team_order,
                                    'seed'            => $k_player + 1,
                                    'battle_id'       => $battleId,
                                    'game'            => 3,
                                    'match'           => $matchId,
                                    'team_id'         => $team_id,
                                    'faction'         => $faction,
                                    'role'            => null,
                                    'lane'            => null,
                                    'player_id'       => $player_id,
                                    'nick_name'       => $nick_name,
                                    'rel_nick_name'   => $player_rel_name,
                                    'steam_id'        => $steam_id,
                                    'champion'        => null,
                                    'heroId' => null,
                                    'double_kill' => 0,
                                    'triple_kill' => 0,
                                    'ultra_kill' => 0,
                                    'rampage' => 0,
                                    'largest_multi_kill' => 0,
                                    'largest_killing_spree' => 0,
                                    'bounty_runes_pickedup' => null,
                                    'double_damage_runes_pickedup' => null,
                                    'haste_runes_pickedup' => null,
                                    'illusion_runes_pickedup' => null,
                                    'invisibility_runes_pickedup' => null,
                                    'regeneration_runes_pickedup' => null,
                                    'arcane_runes_pickedup' => null,
                                    'smoke_used' => 0,
                                    'dust_used' => 0,
                                    'observer_wards_placed' => 0,
                                    'observer_wards_kills' => 0,
                                    'sentry_wards_placed' => 0,
                                    'sentry_wards_kills' => 0,
                                    'barrack_kills' => 0
                                ];
                                Dota2SocketPlayerService::setPlayerInfo($model, $liveDataPlayerUrn, $player_Info);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 击杀事件
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function KillEvent($model, $matchId, $battleId, $data_payload)
    {
        if (!empty($data_payload)) {
            if (!$model) {
                $model = new Dota2Socket();
            }
            if ($data_payload['suicide']) {
                //自杀
                $event_type             = 'player_suicide';
                $eventArr               = Dota2SocketEventService::getEventCommon($model);
                $eventArr['event_type'] = $event_type;
                $eventArr['killer_position'] = implode(',',$data_payload['killerPosition']);
                if ($data_payload['killerUrn']){
                    $eventArr['killer'] = self::getEventPlayerFormatByUrn($model,$data_payload['killerUrn']);
                }else{
                    if (!empty($data_payload['killerTeamUrn'])){
                        $eventArr['killer'] = self::getEventUnknownPlayerFormat($model,$data_payload['killerTeamUrn'],'faction');
                    }else{
                        $eventArr['killer'] = self::getEventUnknownPlayerFormat($model,$data_payload['killerUrn'],'unknown');
                    }
                }
                $eventArr['victim_position'] = implode(',',$data_payload['victimPosition']);
                $eventArr['victim'] = self::getEventPlayerFormatByUrn($model,$data_payload['victimUrn']);
                $eventArr['is_first_event'] = false;
                $eventArr['first_event_type'] = null;

                $returnArr['type'] ='events';
                $returnArr['data'] =$eventArr;
            } else {
                if ($data_payload['deny']) {
                    //反补
                    $event_type             = 'player_denied';
                }else{
                    //选手击杀
                    $event_type             = 'player_kill';
                }
                $eventArr               = Dota2SocketEventService::getEventCommon($model);
                $eventArr['event_type'] = $event_type;
                $eventArr['killer_position'] = implode(',',$data_payload['killerPosition']);
                $eventArr['killer'] = self::getEventPlayerFormatByUrn($model,$data_payload['killerUrn'],'order');
                $eventArr['victim_position'] = implode(',',$data_payload['victimPosition']);
                $eventArr['victim'] = self::getEventPlayerFormatByUrn($model,$data_payload['victimUrn']);
                $killerTeamId = (int)$eventArr['killer']['team_order'];
                unset($eventArr['killer']['team_order']);
                if ($data_payload['killerTeamUrn']!=$data_payload['victimTeamUrn']&&$event_type=='player_kill'){
                    //不同阵营可以计算1血,首五首十
                    $battleDetails = Dota2SocketBattleService::getBattleDetailsByUrn($model,$model->battleUrn);
                    if (empty($battleDetails['first_blood'])){
                        $eventArr['is_first_event'] = true;
                        $eventArr['first_event_type'] = 'first_blood';
                        $first_blood_details['ingame_timestamp'] = $model->duration;
                        $first_blood_details['faction'] = $eventArr['killer']['faction'];
                        $first_blood_details['team_id'] = $killerTeamId;
                        $first_blood_details['details']['killer_position'] = $eventArr['killer_position'];
                        $first_blood_details['details']['killer'] = $eventArr['killer'];
                        $first_blood_details['details']['victim_position'] = $eventArr['victim_position'];
                        $first_blood_details['details']['victim'] = $eventArr['victim'];
                        $update['first_blood'] = $first_blood_details;
                        $update[$killerTeamId.'_real_kills'] = $battleDetails[$killerTeamId.'_real_kills']+1;
                        Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);
                    } elseif (empty($battleDetails['first_to_5_kills']) && $battleDetails[$killerTeamId . '_real_kills'] == 4 && $battleDetails[$killerTeamId . '_real_kills'] < 5) {
                        $eventArr['is_first_event'] = true;
                        $eventArr['first_event_type'] = 'first_to_5_kills';
                        $first_blood_details['ingame_timestamp'] = $model->duration;
                        $first_blood_details['faction'] = $eventArr['killer']['faction'];
                        $first_blood_details['team_id'] = $killerTeamId;
                        $first_blood_details['details']['killer_position'] = $eventArr['killer_position'];
                        $first_blood_details['details']['killer'] = $eventArr['killer'];
                        $first_blood_details['details']['victim_position'] = $eventArr['victim_position'];
                        $first_blood_details['details']['victim'] = $eventArr['victim'];
                        $update['first_to_5_kills'] = $first_blood_details;
                        $update[$killerTeamId.'_real_kills'] = $battleDetails[$killerTeamId.'_real_kills']+1;
                        Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);
                        unset($killerTeamId);
                    }elseif (empty($battleDetails['first_to_10_kills'])&&$battleDetails[$killerTeamId.'_real_kills']==9&&$battleDetails[$killerTeamId.'_real_kills']<10){

                        $eventArr['is_first_event'] = true;
                        $eventArr['first_event_type'] = 'first_to_10_kills';
                        $first_blood_details['ingame_timestamp'] = $model->duration;
                        $first_blood_details['faction'] = $eventArr['killer']['faction'];
                        $first_blood_details['team_id'] = $killerTeamId;
                        $first_blood_details['details']['killer_position'] = $eventArr['killer_position'];
                        $first_blood_details['details']['killer'] = $eventArr['killer'];
                        $first_blood_details['details']['victim_position'] = $eventArr['victim_position'];
                        $first_blood_details['details']['victim'] = $eventArr['victim'];
                        $update['first_to_10_kills'] = $first_blood_details;
                        $update[$killerTeamId.'_real_kills'] = $battleDetails[$killerTeamId.'_real_kills']+1;
                        Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);

                    }else{
                        $eventArr['is_first_event'] = false;
                        $eventArr['first_event_type'] = null;
                        $update[$killerTeamId.'_real_kills'] = $battleDetails[$killerTeamId.'_real_kills']+1;
                        Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$update);
                    }
                }
                if ($event_type == 'player_denied'){
                    $eventArr['is_first_event'] = false;
                    $eventArr['first_event_type'] = null;
                }
                //更新复活市场
                $respawnTime = substr($data_payload['respawnTimeInMillis'],0,-3);
                $victimUpdate['respawntimer'] = $respawnTime;
                $victimUpdate['is_alive'] = false;
                $victimUpdate['respawntimer_duration'] = substr($data_payload['gameTime'],0,-3);
                Dota2SocketPlayerService::setPlayerInfo($model,$data_payload['victimUrn'],$victimUpdate);
                $returnArr['type'] ='events';
                $returnArr['data'] =$eventArr;

            }
            //更改用户的LargestKillingSpree
            self::conversionPlayerLargestKillingSpree($model,$data_payload['killerUrn'],$data_payload['victimUrn'],$data_payload['suicide']);
            return $returnArr;

        }
    }

    /**
     * 特殊击杀
     * @param $model
     * @param $matchId
     * @param $battleId
     * @param $data_payload
     */
    public static function SpecialKillEvent($model, $data_payload)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        if (!empty($data_payload)){
            $killType = $data_payload['killType'];
            if ($killType == 'MULTI'){
                $liveDataPlayerUrn = $data_payload['killerUrn'];
                $playerInfo = Dota2SocketPlayerService::getPlayerInfo($model,$liveDataPlayerUrn);
                if (!empty($playerInfo)){
                    $largest_multi_kill = (Int)$playerInfo['largest_multi_kill'];
                    $killStreakLength = $data_payload['killStreak'];
                    switch ($killStreakLength){
                        case 2://2杀
                            $playerInfo['double_kill'] = (Int)$playerInfo['double_kill'] + 1;
                            break;
                        case 3://3杀
                            $double_kill = (Int)$playerInfo['double_kill'] - 1; //取消2杀次数
                            if ($double_kill < 0){
                                $double_kill = 0;
                            }
                            $playerInfo['double_kill'] = (Int)$double_kill;
                            $playerInfo['triple_kill'] = (Int)$playerInfo['triple_kill'] + 1;
                            break;
                        case 4://4杀
                            $triple_kill = (Int)$playerInfo['triple_kill'] - 1; //取消3杀次数
                            if ($triple_kill < 0){
                                $triple_kill = 0;
                            }
                            $playerInfo['triple_kill'] = (Int)$triple_kill;
                            $playerInfo['ultra_kill'] = (Int)$playerInfo['ultra_kill'] + 1;
                            break;
                        case 5://5杀
                            $ultra_kill = (Int)$playerInfo['ultra_kill'] - 1; //取消4杀次数
                            if ($ultra_kill < 0){
                                $ultra_kill = 0;
                            }
                            $playerInfo['ultra_kill'] = (Int)$ultra_kill;
                            $playerInfo['rampage'] = (Int)$playerInfo['rampage'] + 1;
                            break;
                    }
                    //大于才替换
                    if ($killStreakLength > $largest_multi_kill ){
                        $playerInfo['largest_multi_kill'] = (Int)$killStreakLength;
                    }
                    Dota2SocketPlayerService::setPlayerInfo($model,$liveDataPlayerUrn,$playerInfo);
                }
            }
        }
        return true;
    }



    public static function getEventPlayerFormatByUrn($model, $urn, $extField=null){
        if (!$model) {
            $model = new Dota2Socket();
        }
        $playerInfo = Dota2SocketPlayerService::getPlayerInfo($model,$urn);
        if (!empty($playerInfo)){
            $formatPlayer['player_id'] = $playerInfo['player_id']?(int)$playerInfo['player_id']:null;
            $formatPlayer['nick_name'] = $playerInfo['nick_name'];
            $formatPlayer['steam_id'] = $playerInfo['steam_id'];
            $formatPlayer['faction'] = $playerInfo['faction'];
            $formatPlayer['hero'] = self::getHeroInfoByExternalId($model,$playerInfo['heroId']);
            $formatPlayer['ingame_obj_type'] = 'player';
            $formatPlayer['ingame_obj_sub_type'] = 'player';
            if ($extField=='order'){
                $formatPlayer['team_order'] = $playerInfo['team_order'];
            }
            return $formatPlayer;
        }else{
            return self::getEventUnknownPlayerFormat($model,'','unknown');
        }
    }

    public static function getEventUnknownPlayerFormat($model, $urn, $extField= 'unknown'){
        if (!$model) {
            $model = new Dota2Socket();
        }
        $tableIngameList = $model->tableIngameGoalList;
        foreach ($tableIngameList as $k => $vArr) {
            if ($vArr['game_id'] == 3 && $vArr['sub_type'] == $extField) {
                $dbIngame = $vArr;
            }
        }
        $formatPlayer['ingame_obj_name']    = $dbIngame['ingame_obj_name'];
        $formatPlayer['ingame_obj_name_cn'] = $dbIngame['ingame_obj_name_cn'];
        if ($extField =='faction') {
            $teamInfo        = Dota2SocketTeamService::getTeamInfo($model, $urn);
            $formatPlayer['faction'] = $teamInfo['faction'];
        }
        $formatPlayer['ingame_obj_type']     = $dbIngame['ingame_obj_type'];
        $formatPlayer['ingame_obj_sub_type'] = $dbIngame['sub_type'];
        return $formatPlayer;
    }

    public static function getEventCommon($model)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $battleDetails                 = Dota2SocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
        $commonArr                     = [];
        $commonArr['match_id']         = (int)$model->matchId;
        $commonArr['battle_id']        = (int)$model->battleId;
        $commonArr['battle_order']     = (int)$model->battleOrder;
        $commonArr['ingame_timestamp'] = (int)$model->battle_duration;
        $commonArr['event_id']         = (int)$model->info['socket_id'];
        return $commonArr;

    }

    //处理玩家最大连杀
    private static function conversionPlayerLargestKillingSpree($model,$killerUrn,$victimUrn,$is_suicide){
        if (!empty($killerUrn) && !$is_suicide){
            $killerPlayerInfo = Dota2SocketPlayerService::getPlayerInfo($model,$killerUrn);
            if ($killerPlayerInfo) {
                //当前击杀数量
                $player_current_kill_num_have = @$killerPlayerInfo['current_kill_num_have'];
                if ($player_current_kill_num_have){
                    $player_current_kill_num = @$killerPlayerInfo['current_kill_num'];
                    //最大连杀数量
                    $player_largest_killing_spree_num = $killerPlayerInfo['largest_killing_spree'];
                    $player_current_kill_num_now = (Int)$player_current_kill_num + 1;
                    if ($player_current_kill_num_now > $player_largest_killing_spree_num){
                        //保存
                        $killerPlayerInfo['largest_killing_spree'] = $player_current_kill_num_now;
                        $killerPlayerInfo['current_kill_num'] = $player_current_kill_num_now;
                        Dota2SocketPlayerService::setPlayerInfo($model,$killerUrn,$killerPlayerInfo);
                    }
                } else {
                    $killerPlayerInfo['current_kill_num'] = 1;
                    $killerPlayerInfo['current_kill_num_have'] = true;
                    $killerPlayerInfo['largest_killing_spree'] = 1;
                    Dota2SocketPlayerService::setPlayerInfo($model,$killerUrn,$killerPlayerInfo);
                }
            }
        }
        //被杀者 的current_kill_num 为0
        if (!empty($victimUrn)){
            $victimUpdate = [
                'current_kill_num' => 0
            ];
            Dota2SocketPlayerService::setPlayerInfo($model,$victimUrn,$victimUpdate);
        }
        return true;
    }
}
