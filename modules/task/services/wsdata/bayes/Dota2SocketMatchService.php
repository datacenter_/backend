<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\metadata\models\MetadataDota2Ability;
use app\modules\metadata\models\MetadataDota2Hero;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataDota2Talent;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Throwable;
use yii\base\Exception;

class Dota2SocketMatchService extends FiveHotCsgoMatchWs implements SocketMatchInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    public static function getFiveHot($matchId)
    {
        $hotMatch             = FiveHotCsgoMatchWs::getInstance();
        $hotMatch->currentPre = 'bayes_dota_ws';
        $hotMatch->matchId    = $matchId;
        $hotMatch->refresh_task    = true;
        return $hotMatch;
    }

    /**
     * getMatchInfo
     * @param $model
     * @param $matchId
     * @return array|bool|\yii\db\ActiveRecord|null
     */
    public static function getMatchInfoOrInitMatch($model,$matchId)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        if ($matchId == 0) {
            return false;
        }
        $redis_res = $model->hGetAll(['table','match' ,$matchId]);

        if ($redis_res) {
            return $redis_res;
        } else {
            $redis_match_real_res = $model->hGetAll(['table', 'match_real_time_info', $matchId]);
            if (empty($redis_match_real_res)) {
                $match_real = MatchRealTimeInfo::find()->where(['id' => $matchId])->asArray()->one();
                if (!empty($match)) {
                    $model->hMSet(['table', 'match_real_time_info', $matchId], $match_real);
                    Dota2SocketMatchService::setMatchDetails($model, $match_real);
                }
            }
            $match = MatchModel::find()->where(['id' => $matchId])->asArray()->one();
            if (empty($match)){
                return 'no_match_info';
            }
            $model->hMSet(['table','match' ,$matchId], $match);
            Dota2SocketMatchService::setMatchInfo($model,$matchId, 'init',$match);
            return $match;
        }

    }

    public static function getMatchDetails($model){
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $key = ['current','match','details'];
        return json_decode($model->getValue($key),true);
    }

    public static function setMatchDetails($model,$updateArr){
        if (!is_array($updateArr)){
            return false;
        }
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $oldDetails = self::getMatchDetails($model);
        if (!empty($oldDetails)){
            $update = array_merge($oldDetails,$updateArr);
        }else{
            $initDetails = Dota2SocketMatchService::getMatchBase($model);
            $update = array_merge($initDetails,$updateArr);
        }
        $key = ['current','match','details'];
        if (is_array($update)){
            $update = json_encode($update,320);
        }
        $model->setValue($key,$update);
    }

    public static function getMatchBase($model){
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $baseArr = [
            'team_1_score'=>0,
            'team_2_score'=>0,
        ];
        return $baseArr;
    }

    public static function setMatchInfo($model,$matchId, $type,$matchInfo)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        if ($type=='init'){
            Dota2SocketMatchService::initTeams($model, $matchInfo);
//            Dota2SocketMatchService::initPlayers($model, $matchInfo);
        }

    }





    /**
     * 初始化选手
     * @param $model
     * @param $matchInfo
     * @return array
     */
    public static function initPlayers($model, $matchInfo)
    {
        if (empty($model)){
            $model = new Dota2Socket();
        }
        $playerRedisKey = [self::KEY_TAG_TABLE, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        $player_info    = $model->hGetAll($playerRedisKey);
        if (empty($player_info)) {
            $team_ids    = [$matchInfo['team_1_id'], $matchInfo['team_2_id']];
            $playersInfo = TeamSnapshot::find()->alias('ts')->select('p.player_id,p.nick_name,ts.team_id')
                ->leftJoin('team_player_relation_snapshot as tpr','ts.id = tpr.team_id')
                ->leftJoin('player_snapshot as p','p.relation_id = tpr.id')
                ->where(['ts.relation_id'=>$matchInfo['tournament_id']])
                ->andWhere(['in', 'ts.team_id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val) {
                if ($val['player_id']){
                    $player_info[@$val['player_id']] = @json_encode($val, 320);
                }
            }
            $model->hMSet($playerRedisKey, $player_info);
        }
        return $player_info;
    }


    /**
     * 初始化team信息
     * @param $model
     * @param $matchInfo
     * @return array
     */
    public static function initTeams($model, $matchInfo)
    {
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $teamRedisKey = [self::KEY_TAG_TABLE, self::KEY_TAG_TEAM, self::KEY_TAG_LIST];
        $teams        = $model->hGetAll($teamRedisKey);
        $teams_info = null;
        if (empty($teams)) {
            $Team1Info = Team::find()->where(['id' => $matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])) {
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'], 200, 200);
            }
            $model->hSet($teamRedisKey, $Team1Info['id'], json_encode($Team1Info, 320));
            $Team2Info = Team::find()->where(['id' => $matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])) {
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'], 200, 200);
            }
            $model->hSet($teamRedisKey, @$Team2Info['id'], @json_encode($Team2Info, 320));
            $teams_info = [
                $Team1Info['id'] => @json_encode($Team1Info, 320),
                $Team2Info['id'] => @json_encode($Team2Info, 320)
            ];
        }
        return $teams_info;
    }

    public static function initTeamOnNoTeam($model,$team_urn_id,$faction){
        $matchInfo = $model->match;
        $team_id = @self::getMainIdOfBayesByUrnOnDota('team', $model->origin_id, $team_urn_id, 3,$matchInfo);
        //获取$team_order
        $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
        $team_info = [
            'team_id'         => $team_id,
            'order'           => $team_order,
            'faction'         => $faction,
            'score'           => 0,
            'rel_identity_id' => $team_urn_id,
            'identity_id'     => $team_urn_id,
            'smoke_used' => 0,
            'dust_used' => 0,
            'bounty_runes_pickedup' => null,
            'double_damage_runes_pickedup' => null,
            'haste_runes_pickedup' => null,
            'illusion_runes_pickedup' => null,
            'invisibility_runes_pickedup' => null,
            'regeneration_runes_pickedup' => null,
            'arcane_runes_pickedup' => null,
            'observer_wards_kills' => 0,
            'observer_wards_placed' => 0,
            'sentry_wards_kills' => 0,
            'sentry_wards_placed' => 0
        ];
        Dota2SocketTeamService::setTeamInfo($model, $team_urn_id, $team_info);
    }

    public static function transFrames($model,$matchId,$battleId,$data_payload,$battleDetails){
        if (!empty($data_payload)){
            if (empty($model)){
                $model = new Dota2Socket();
            }
//            $seriesCurrent = $data_payload['seriesStatus']['seriesCurrent'];
//            $battleUpdate = [];
//            $battle_begin_at = $battle_end_at = null;
//            $startTime = $data_payload['startedAt'];
//            $endTime = $data_payload['endedAt'];
//            if ($startTime){
//                $battle_begin_at = date('Y-m-d H:i:s',strtotime($startTime));
//                $battleUpdate['begin_at'] = $battle_begin_at;
//                if ($seriesCurrent == 1){//第一个对局 设置比赛的时间
//                    $MatchDetailsStartUpdate['begin_at'] = $battle_begin_at;
//                    Dota2SocketMatchService::setMatchDetails($model, $MatchDetailsStartUpdate);
//                }
//            }
//            if ($endTime){
//                $battle_end_at = date('Y-m-d H:i:s',strtotime($endTime));
//                $battleUpdate['end_at'] = $battle_end_at;
//            }
//            if ($battleUpdate){
//                Dota2SocketBattleService::setBattleDetailsByUrn($model, $model->battleUrn, $battleUpdate);
//            }
            $map_id = 1;
            $frameArr = [];
            $matchDetails = self::getMatchDetails($model);
            $matchInfo = $model->match;
            $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
            $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
            $number_of_games = $matchInfo['number_of_games'];
            $seriesStatus = $data_payload['seriesStatus'];
            $seriesScores = $data_payload['seriesStatus']['seriesScores'];
            $matchStatus = $data_payload['matchStatus'];
            $gameTime = $model->duration = intval($data_payload['gameTime']/1000);
            $dayTime = $data_payload['daytime']?'day':'night';
            $teamFormat = Dota2SocketTeamService::getTeamListFormat($model,'all');
            $teamList = Dota2SocketTeamService::getTeamList($model);
            if (empty($teamList)) {
                self::initTeamOnNoTeam($model, $data_payload['radiant']['liveDataUrn'], 'radiant');
                self::initTeamOnNoTeam($model, $data_payload['dire']['liveDataUrn'], 'dire');
            }
            if (!empty($seriesScores)&&!empty($teamList)){
                //更新比分
                foreach ($teamList as $k=>$v){
//                    $vArr = json_decode($v,true);
//                    $vArr['score'] = $seriesScores[$k];
//                    Dota2SocketTeamService::setTeamInfo($model,$k,$vArr);
                }
            }

            $frameArr['match_id']           = (int)$model->matchId;
            $frameArr['game_rules']         = $game_rules;
            $frameArr['match_type']         = $match_type;
            $frameArr['number_of_games']    = (int)$number_of_games;
            $frameArr['battle_id']          = (int)$model->battleId;
            $frameArr['battle_order']       = (int)$model->battleOrder;
            $frameArr['map']                = Dota2SocketBattleService::getMap($model, $map_id);
            $frameArr['duration']           = (int)$gameTime;
            $frameArr['is_battle_finished'] = $battleDetails['status'] == 3;
            $is_match_finished = $matchDetails['status'] == 3;
            $frameArr['is_match_finished']  = $is_match_finished;
//            if ($is_match_finished && !empty($battle_end_at)){
//                $MatchDetailsEndUpdate['end_at'] = $battle_end_at;
//                Dota2SocketMatchService::setMatchDetails($model, $MatchDetailsEndUpdate);
//            }
            $frameArr['match_scores']       = Dota2SocketMatchService::getScoreFormat($model, $teamFormat);
            $frameArr['match_winner']       = $matchDetails['winner_details'];
            $frameArr['battle_winner']      = $battleDetails['winner_details'];
            $frameArr['is_pause']           = $matchStatus['paused'];
            $frameArr['is_live']            = true;//$matchStatus['matchStatus']!='PRE_GAME';
            $frameArr['time_of_day']        = $dayTime;
            $frameArr['elites_status']['roshan_status']      = self::getRoShanStatusOnFrame($model,$data_payload['roshanStatus']);
            $frameArr['factions']           = self::getFramesFactions($model, $data_payload, $teamFormat);
            $frameArr['server_config']['version']           = null;
            $returnArr['type']              = 'frame';
            $returnArr['data']              = $frameArr;
            //更新historyBattle的详情
            $battleDetails                   = Dota2SocketBattleService::getBattleDetailsByUrn($model, $model->battleUrn);
            $is_pause_num = $frameArr['is_pause'] ? 1 : 2;
            $is_pause_old = @$battleDetails['is_pause_old'];
            if (empty($is_pause_old)){
                $battleUpdate['is_pause_old'] = $is_pause_num;
            }else{
                if ($is_pause_old == 1){//已经暂停
                    if ($is_pause_num == 2){
                        //解除暂停
                        $event_type             = 'battle_unpause';
                        $eventArr               = Dota2SocketEventService::getEventCommon($model);
                        $eventArr['event_type'] = $event_type;
                        $model->needOutPutEvents =  @json_encode($eventArr, 320);
                        $battleUpdate['is_pause_old'] = 2;
                    }
                }else{//为暂停
                    if ($is_pause_num == 1){
                        //暂停
                        $event_type             = 'battle_pause';
                        $eventArr               = Dota2SocketEventService::getEventCommon($model);
                        $eventArr['event_type'] = $event_type;
                        $model->needOutPutEvents =  @json_encode($eventArr, 320);
                        $battleUpdate['is_pause_old'] = 1;
                    }
                }
            }
            $battleUpdate['duration'] = (int)$gameTime;
            $battleUpdate['is_pause'] = $frameArr['is_pause'];
            $battleUpdate['is_live'] = $frameArr['is_live'];
            $battleUpdate['time_of_day'] = $frameArr['time_of_day'];
            $battleUpdate['elites_status'] = $frameArr['elites_status'];
            $battleUpdate['map'] = $map_id;
            Dota2SocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$battleUpdate);
            return $returnArr;
        }
    }

    public static function getRoShanStatusOnFrame($model,$roushanStatus){
        if (!$model) {
            $model = new Dota2Socket();
        }
        $battleDetails = $model->battleDetails;
        $returnArr['generation'] = $battleDetails['real_kill_roshans']+1<=4?$battleDetails['real_kill_roshans']+1:4;
        $returnArr['name'] = 'Roshan';
        $returnArr['name_cn'] = '肉山';
        $returnArr['status'] = strtolower($roushanStatus['roshanState']);
        $returnArr['spawntimer'] = (int)substr($roushanStatus['respawnTimeInMillis'],0,-3);
        return $returnArr;
    }


    public static function getScoreFormat($model, $teamFormat)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        //更新match详情
        $matchDetails = Dota2SocketMatchService::getMatchDetails($model);
        $data = [];
        foreach ($teamFormat as $k=>$v){
            $vNew['team_id']        = $v['team_id'];
            $vNew['name']           = $v['name'];
            $vNew['image']          = $v['image'];
            $vNew['opponent_order'] = $v['order'];
            if ($v['order'] == 1){
                $vNew['score'] = $matchDetails['team_1_score'];
            }
            if ($v['order'] == 2){
                $vNew['score'] = $matchDetails['team_2_score'];
            }
            //$vNew['score']          = $v['score'];
            $data[] = $vNew;
        }
        return $data;
    }


    /**
     * net_worth,experience的计算
     * @param $model
     * @param $toDoDireTeamData
     * @param $toDoRadiantTeamData
     */
    public static function dealToDoRadiantTeamData($model,$gameOver,$toDoDireTeamData,$toDoRadiantTeamData){
        if (!$model) {
            $model = new Dota2Socket();
        }
        $net_worth_diff = $toDoDireTeamData['net_worth']-$toDoRadiantTeamData['net_worth'];
        $experience_diff = $toDoDireTeamData['experience']-$toDoRadiantTeamData['experience'];

        $dire_net_worth_diff = $net_worth_diff;
        $radiant_net_worth_diff = -$net_worth_diff;
        $dire_experience_diff = $experience_diff;
        $radiant_experience_diff = -$experience_diff;
        $teamList = Dota2SocketTeamService::getTeamList($model);
        foreach ($teamList as $k => $v) {
            $vArr = json_decode($v, true);
            if ($vArr['faction'] == 'dire') {
                if (empty($vArr['building_status'])){
                    $update['building_status'] = self::getBuildingDefaultStatusOnFrame($model,'dire');
                }
                $update['net_worth_diff']  = $dire_net_worth_diff;
                $update['experience_diff'] = $dire_experience_diff;
                Dota2SocketTeamService::setTeamInfo($model, $k, $update);
            } else {
                if (empty($vArr['building_status'])){
                    $update['building_status'] = self::getBuildingDefaultStatusOnFrame($model,'radiant');
                }
                $update['net_worth_diff']  = $radiant_net_worth_diff;
                $update['experience_diff'] = $radiant_experience_diff;
                Dota2SocketTeamService::setTeamInfo($model, $k, $update);
            }
        }
        //设置时间线
        $team_timeline_time = $model->duration;
        if ($team_timeline_time > 0){
            $team_timeline_time_key  = ['history','battle',$model->battleId,'team_timeline_time'];
            $net_worth_diff_timeline_key  = ['history','battle',$model->battleId,'net_worth_diff_timeline'];
            $experience_diff_timeline_key  = ['history','battle',$model->battleId,'experience_diff_timeline'];
            $old_time = $model->getValue($team_timeline_time_key);
            if ($old_time){
                $time_cha = $team_timeline_time - $old_time;
                if ($time_cha >= 30 || $gameOver){
                    //获取原来的时间线
                    $net_worth_diff_timeline_new[] = [
                        'ingame_timestamp' => $team_timeline_time,
                        'net_worth_diff' => -$net_worth_diff
                    ];
                    $net_worth_diff_timeline_old_redis = $model->getValue($net_worth_diff_timeline_key);
                    $net_worth_diff_timeline_old_array = @json_decode($net_worth_diff_timeline_old_redis,true);
                    $net_worth_diff_timeline_res = @array_merge($net_worth_diff_timeline_old_array,$net_worth_diff_timeline_new);
                    $net_worth_diff_timeline_info = @json_encode($net_worth_diff_timeline_res,320);
                    $model->setValue($net_worth_diff_timeline_key,$net_worth_diff_timeline_info);
                    $experience_diff_timeline_new[] = [
                        'ingame_timestamp' => $team_timeline_time,
                        'experience_diff' => -$experience_diff
                    ];
                    $experience_diff_timeline_old_redis = $model->getValue($experience_diff_timeline_key);
                    $experience_diff_timeline_old_array = @json_decode($experience_diff_timeline_old_redis,true);
                    $experience_diff_timeline_res = @array_merge($experience_diff_timeline_old_array,$experience_diff_timeline_new);
                    $experience_diff_timeline_info = @json_encode($experience_diff_timeline_res,320);
                    $model->setValue($experience_diff_timeline_key,$experience_diff_timeline_info);
                    //保存时间
                    $model->setValue($team_timeline_time_key,$team_timeline_time);
                }
            }else{
                $net_worth_diff_timeline_res[] = [
                    'ingame_timestamp' => $team_timeline_time,
                    'net_worth_diff' => -$net_worth_diff
                ];
                $net_worth_diff_timeline_info = @json_encode($net_worth_diff_timeline_res,320);
                $model->setValue($net_worth_diff_timeline_key,$net_worth_diff_timeline_info);
                $experience_diff_timeline_res[] = [
                    'ingame_timestamp' => $team_timeline_time,
                    'experience_diff' => -$experience_diff
                ];
                $experience_diff_timeline_info = @json_encode($experience_diff_timeline_res,320);
                $model->setValue($experience_diff_timeline_key,$experience_diff_timeline_info);
                //保存时间
                $model->setValue($team_timeline_time_key,$team_timeline_time);
            }
        }
        return true;
    }

    /**
     * frame阵营内容
     * @param $model
     * @param $data_payload
     * @param $teamFormat
     * @return array
     */
    public static function getFramesFactions($model, $data_payload, $teamFormat)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $gameOver       = $data_payload['gameOver'];
        $direTeam       = $data_payload['dire'];
        $direPlayers    = $data_payload['dire']['players'];
        $radiantTeam    = $data_payload['radiant'];
        $radiantPlayers = $data_payload['radiant']['players'];
        Dota2SocketMatchService::dealFrameTeam($model, $direTeam);
        $toDoDireTeamData=Dota2SocketMatchService::dealFramePlayer($model, $direPlayers, 'dire', $direTeam['liveDataUrn']);
        Dota2SocketMatchService::dealFrameTeam($model, $radiantTeam);
        $toDoRadiantTeamData =Dota2SocketMatchService::dealFramePlayer($model, $radiantPlayers, 'radiant', $radiantTeam['liveDataUrn']);
        Dota2SocketMatchService::dealToDoRadiantTeamData($model,$gameOver,$toDoDireTeamData,$toDoRadiantTeamData);
        $playerList  = self::getFramesPlayers($model);
        $teamList    = Dota2SocketTeamService::getTeamListFormat($model, 'all');
        if (count($teamList)==2){
            $tempTeamList['radiant'] = $teamList['radiant'];
            $tempTeamList['dire'] = $teamList['dire'];
            $teamList =  $tempTeamList;
        }
        $factionsArr = [];
        $team_item = [];
        $i           = 0;
        foreach ($teamList as $k => $v) {
            $faction                                 = $v['faction'];
            $factionsArr[$i]['faction']              = $faction;
            $factionsArr[$i]['team']                 = self::formatFrameTeam($model, $teamFormat[$faction]);
            $factionsArr[$i]['kills']                = $v['kills'];
            $factionsArr[$i]['deaths']               = $v['deaths'];
            $factionsArr[$i]['assists']              = $v['assists'];
            $factionsArr[$i]['net_worth']            = $v['net_worth'];
            $factionsArr[$i]['net_worth_diff']       = $v['net_worth_diff'];
            $factionsArr[$i]['experience']           = $v['experience'];
            $factionsArr[$i]['experience_diff']      = $v['experience_diff'];
            $factionsArr[$i]['tower_kills']          = $v['tower_kills'];
            $factionsArr[$i]['barrack_kills']        = $v['barrack_kills'];
            $factionsArr[$i]['melee_barrack_kills']  = $v['melee_barrack_kills'];
            $factionsArr[$i]['ranged_barrack_kills'] = $v['ranged_barrack_kills'];
            $factionsArr[$i]['roshan_kills']         = $v['roshan_kills'];
            $factionsArr[$i]['building_status']      = $v['building_status'];//self::getBuildingDefaultStatusOnFrame($model);
            $factionsArr[$i]['players']              = $playerList[$faction];
            $factionsArr[$i]['advanced']             = self::getFramesTeamAdvanced($model, $v);
            $i++;
            $team_item[$faction] = $v['team_id'];
        }
        //补充pan_pick
        $banPickRedisList  = Dota2SocketEventService::getBanPickList($model);
        if (empty($banPickRedisList)){
            $playerBanPick = array_merge($playerList['dire'],$playerList['radiant']);
            foreach ($playerBanPick as $key => $item){
                $ban_pick = [];
                $ban_pick['hero']['hero_id'] = @$item['hero']['hero_id'];
                $ban_pick['team']['team_id'] = @$team_item[$item['faction']];
                $ban_pick['event_type'] = 'hero_picked';
                $ban_pick['bp_order'] = null;
                Dota2SocketEventService::setBanPick($model,@json_encode($ban_pick,320));
            }
        }
        return $factionsArr;
    }

    public static function formatFrameTeam($model,$team){
        $newTeam = [];
        $newTeam['team_id'] = $team['team_id'];
        $newTeam['name'] = $team['name'];
        $newTeam['image'] = $team['image'];
        $newTeam['opponent_order'] = $team['order'];
        return $newTeam;
    }

    public static function getBuildingDefaultStatusOnFrame($model,$faction){
        if (!$model) {
            $model = new Dota2Socket();
        }
        if ($faction == 'radiant'){
            $top_outpost['is_captured']               = false;
            $top_outpost['health']                    = 450;
            $top_outpost['health_max']                = 450;
            $top_outpost['armor']                     = 0;
            $bot_outpost['is_captured']               = true;
            $bot_outpost['health']                    = 450;
            $bot_outpost['health_max']                = 450;
            $bot_outpost['armor']                     = 0;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        if ($faction == 'dire'){
            $top_outpost['is_captured']               = true;
            $top_outpost['health']                    = 450;
            $top_outpost['health_max']                = 450;
            $top_outpost['armor']                     = 0;
            $bot_outpost['is_captured']               = false;
            $bot_outpost['health']                    = 450;
            $bot_outpost['health_max']                = 450;
            $bot_outpost['armor']                     = 0;
            $returnData['outposts']['top_outpost']    = $top_outpost;
            $returnData['outposts']['bot_outpost']    = $bot_outpost;
        }
        $top_tier_1_tower['is_alive']             = true;
        $top_tier_1_tower['health']               = 1800;
        $top_tier_1_tower['health_max']           = 1800;
        $top_tier_1_tower['armor']                = 12;
        $top_tier_2_tower['is_alive']             = true;
        $top_tier_2_tower['health']               = 2500;
        $top_tier_2_tower['health_max']           = 2500;
        $top_tier_2_tower['armor']                = 16;
        $top_tier_4_tower['is_alive']             = true;
        $top_tier_4_tower['health']               = 2600;
        $top_tier_4_tower['health_max']           = 2600;
        $top_tier_4_tower['armor']                = 21;

        $ranged_barrack['is_alive']             = true;
        $ranged_barrack['health']               = 1300;
        $ranged_barrack['health_max']           = 1300;
        $ranged_barrack['armor']                = 9;

        $melee_barrack['is_alive']             = true;
        $melee_barrack['health']               = 2200;
        $melee_barrack['health_max']           = 2200;
        $melee_barrack['armor']                = 15;

        $ancient['is_alive']             = true;
        $ancient['health']               = 4500;
        $ancient['health_max']           = 4500;
        $ancient['armor']                = 13;
        $returnData['towers']['top_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['mid_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_2_tower'] = $top_tier_2_tower;
        $returnData['towers']['bot_tier_3_tower'] = $top_tier_2_tower;
        $returnData['towers']['top_tier_4_tower'] = $top_tier_4_tower;
        $returnData['towers']['bot_tier_4_tower'] = $top_tier_4_tower;
        $returnData['barracks']['top_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['top_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['mid_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['mid_melee_barrack'] = $melee_barrack;
        $returnData['barracks']['bot_ranged_barrack'] = $ranged_barrack;
        $returnData['barracks']['bot_melee_barrack'] = $melee_barrack;
        $returnData['ancient'] = $ancient;
        return $returnData;

    }


    public static function dealFramePlayer($model, $framePlayer, $faction, $teamUrn)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $tableHeroList= $model->tableHeroList;
        $teamDetails = $teamTempDetails = [];
        $durationNum = $model->duration / 60;
        $durationNum = $durationNum > 0 ? $durationNum : 1;
        foreach ($framePlayer as $k => $v) {
            $teamTempDetails['kills']     += $v['kills'];
            $teamTempDetails['net_worth'] += $v['networth'];
        }
        $historyPlayerList = Dota2SocketPlayerService::getPlayerList($model);

        foreach ($framePlayer as $k => $v) {
            $playerDetails['faction']        = $faction;
            $playerDetails['rel_nick_name']  = $v['playerName'];
            $playerDetails['steam_id']       = $v['references']['STEAM_ID_64'];
            $playerDetails['heroId']         = $v['heroId'];
            if (empty($historyPlayerList)){
                $matchInfo = $model->match;
                $team_id = @self::getMainIdOfBayesByUrnOnDota('team', $model->origin_id, $teamUrn, 3,$matchInfo);
                //获取$team_order
                $team_order = self::getTeamOrderByTeamId($team_id, $matchInfo);
                $playerDetails['order'] = $k + 1;
                $playerDetails['rel_identity_id'] = $v['liveDataUrn'];
                $playerDetails['rel_team_id'] = $teamUrn;
                $playerDetails['team_order'] = $team_order;
                $playerDetails['seed'] = $k + 1;
                $playerDetails['battle_id'] = $model->battleId;
                $playerDetails['game'] = 3;
                $playerDetails['match'] = $model->matchId;
                $playerDetails['team_id'] = $team_id;
                $player_id = @self::getMainIdOfBayesByUrn('player', $model->origin_id,  $v['liveDataUrn'], 3);
                $nick_name = $v['playerName'];
                if ($player_id) {
                    $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                    $nick_name   = @$Player_Info['nick_name'];
                }
                $playerDetails['player_id'] = $player_id;
                $playerDetails['nick_name'] = $nick_name;
                $playerDetails['rel_nick_name'] = $v['playerName'];
                $playerDetails['steam_id'] = $v['references']['STEAM_ID_64'];
            }
            if ($v['heroId']){
                if (array_key_exists($v['heroId'],$tableHeroList)){
                    $heroDetails = @$tableHeroList[$v['heroId']];
                    if($heroDetails){
                        $playerDetails['hero'] = self::checkAndChangeInt(@$heroDetails['id']);
                    }else{
                        $playerDetails['hero']  =null;
                    }
                }else{
                    $playerDetails['hero']  =null;
                }
            }else{
                $playerDetails['hero']  =null;
            }

            $playerDetails['heroName']       = $v['heroName'];
            $playerDetails['itemsInventory'] = $v['itemsInventory'];//物品栏
            $playerDetails['itemsBackpack']  = $v['itemsBackpack'];//背包
            $playerDetails['itemsStash']     = $v['itemsStash'];//储藏处
            $playerDetails['itemsBear']      = $v['itemsBear'];//召唤物
            $playerDetails['itemNeutral']    = $v['itemNeutral'];//野怪掉落
            $playerDetails['abilities']      = $v['abilities'];//技能
            $playerDetails['coordinate']     = implode(',', $v['position']);//	坐标
            $playerDetails['level']          = $v['level'];//等级
            $playerDetails['is_alive']       = $v['alive'];//存活状态
            $playerDetails['kills']          = $v['kills'];//击杀
            $playerDetails['deaths']         = $v['deaths'];//死亡
            $playerDetails['assists']        = $v['assists'];//助攻
            //计算
            $death                              = $v['deaths'] > 0 ? $v['deaths'] : 1;
            $teamTempDetails['kills']           = $teamTempDetails['kills'] > 0 ? $teamTempDetails['kills'] : 1;
            $teamTempDetails['net_worth']       = $teamTempDetails['net_worth'] > 0 ? $teamTempDetails['net_worth'] : 1;
            $playerDetails['kda']               = (string)round(($v['kills'] + $v['assists']) / $death, 2);//kda
            $playerDetails['participation']     = (string)round(($v['kills'] + $v['assists']) / $teamTempDetails['kills'], 4);//participation
            $playerDetails['lhpm']              = (string)round(($v['lastHits'] / $durationNum), 2);//lhpm 选手补刀/（对局时长（秒）/60）
            $playerDetails['net_worth_percent'] = (string)round(($v['networth'] / $teamTempDetails['net_worth']), 4);
            $playerDetails['gpm']               = (string)round(($v['goldEarned'] / $durationNum), 2);//
            $playerDetails['xpm']               = (string)round(($v['xp'] / $durationNum), 2);//

            $playerDetails['last_hits']      = $v['lastHits'];//	补刀
            $playerDetails['denies']         = $v['denies'];//	反补
            $playerDetails['gold_remaining'] = $v['goldCurrent'];//	当前金钱
            $playerDetails['gold_earned']    = $v['goldEarned'];//	金钱获取
            $playerDetails['gold_spent']    = $v['goldEarned'] - $v['goldCurrent'];//	金钱花费

            $playerDetails['net_worth']             = $v['networth'];//	财产总和
            $playerDetails['experience']            = $v['xp'];//	经验
            $playerDetails['total_heal']            = $v['healingDone'];//	治疗量
            $playerDetails['total_runes_pickedup']  = $v['runesPickedUp'];//	拾取符文
            $playerDetails['buyback_cost']          = $v['buybackCost'];//买活花费
            $playerDetails['buyback_cooldown']      = null;//买活CD
            $playerDetails['roshan_kills']          = $v['roshanKills'];//击杀肉山
            $playerDetails['health']                = $v['health'];//	当前血量
            $playerDetails['health_max']            = $v['healthMax'];//总血量
            $playerDetails['is_visible']            = $v['visibleByEnemy'];//	敌人是否不可见
            $playerDetails['gold_reliable']         = $v['reliableGold'];//可靠金钱
            $playerDetails['gold_herokill']         = $v['heroKillGold'];//击杀英雄获取的金钱
            $playerDetails['gold_creepkill']        = $v['creepKillGold'];//补刀获取的金钱
            $playerDetails['gold_unreliable']       = $v['unreliableGold'];//不可靠金钱
            $playerDetails['tower_kills']           = $v['towerKills'];//摧毁防御塔
            //$playerDetails['largest_killing_spree'] = $v['bestKillStreak']?$v['bestKillStreak']:0;//最大连杀
            //保存选手数据
            Dota2SocketPlayerService::setPlayerInfo($model, $v['liveDataUrn'], $playerDetails);
            //处理team数据
            $teamDetails['deaths']                += $v['deaths'];//死亡
            $teamDetails['assists']               += $v['assists'];//助攻
            $teamDetails['last_hits']             += $v['lastHits'];//	补刀
            $teamDetails['denies']                += $v['denies'];//	反补
            $teamDetails['gold_remaining']        += $v['goldCurrent'];//	当前金钱
            $teamDetails['gold_earned']           += $v['goldEarned'];//	金钱获取
            $teamDetails['net_worth']             += $v['networth'];//	财产总和
            $teamDetails['experience']            += $v['xp'];//	经验
            $teamDetails['total_heal']            += $v['healingDone'];//	治疗量
            $teamDetails['total_runes_pickedup']  += $v['runesPickedUp'];//	拾取符文
            $teamDetails['buyback_cost']          += $v['buybackCost'];//买活花费
            $teamDetails['buyback_cooldown']      = null;//买活CD
            $teamDetails['health']                += $v['health'];//	当前血量
            $teamDetails['health_max']            += $v['healthMax'];//总血量
            $teamDetails['is_visible']            += $v['visibleByEnemy'];//	敌人是否不可见
            $teamDetails['gold_reliable']         += $v['reliableGold'];//可靠金钱
            $teamDetails['gold_herokill']         += $v['heroKillGold'];//击杀英雄获取的金钱
            $teamDetails['gold_creepkill']        += $v['creepKillGold'];//补刀获取的金钱
            $teamDetails['gold_unreliable']       += $v['unreliableGold'];//不可靠金钱
            $teamDetails['tower_kills']           += $v['towerKills'];//摧毁防御塔
            //$teamDetails['largest_killing_spree'] += $v['bestKillStreak'];//最大连杀
        }
        Dota2SocketTeamService::setTeamInfo($model, $teamUrn, $teamDetails);
        //返回待处理的team数据
        $teamNeedDealData['net_worth']  = $teamDetails['net_worth'];
        $teamNeedDealData['experience'] = $teamDetails['experience'];
        return $teamNeedDealData;
    }

    public static function dealFrameTeam($model,$team){

        $playerDetails['kills']  = $team['heroKills'];
        $playerDetails['tower_kills']       = $team['towerKills'];
        $playerDetails['barrack_kills']         = $team['barracksKills'];
        $playerDetails['roshan_kills']       = $team['roshanKills'];
        Dota2SocketTeamService::setTeamInfo($model, $team['liveDataUrn'], $playerDetails);

    }

    public static function getFramesPlayers($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $playerList = Dota2SocketPlayerService::getPlayerList($model);
        $players = [];
        if (!empty($playerList)) {
            $players_radiant = $players_dire = [];
            foreach ($playerList as $k => $v) {
                $vArr = json_decode($v, true);
                if ($vArr['faction'] == 'radiant') {
                    $vPlayer              = Dota2SocketMatchService::getFormatFramesFactionPlayer($model, $vArr);
                    $players_radiant[$vPlayer['seed']] = $vPlayer;
                }
                if ($vArr['faction'] == 'dire') {
                    $vPlayer           = Dota2SocketMatchService::getFormatFramesFactionPlayer($model, $vArr);
                    $players_dire[$vPlayer['seed']] = $vPlayer;
                }
            }
            ksort($players_radiant);
            ksort($players_dire);
            $players['radiant'] = array_values($players_radiant);
            $players['dire'] = array_values($players_dire);
        }
        return $players;

    }

    public static function getHeroInfoByRelId($model,$id){
        $redisKey = ['table','hero','list'];
        $redisResult = $model->hGet($redisKey,$id);
        if (!empty($redisResult)){
            return json_decode($redisResult,true);
        }else{
            $where['external_id'] = $id;
            $dbResult = MetadataDota2Hero::find()->where($where)->asArray()->one();
            if (!empty($dbResult)){
                $model->hSet($redisKey,$id,json_encode($dbResult,320));
                return $dbResult;
            }else{
                return null;
            }
        }
    }

    /**
     * @param $model
     * @return array
     */
    public static function getTableHeroList($model)
    {
        $redisKey    = ['table', 'hero', 'list'];
        $redisResult = $model->hGetAll($redisKey);
        if (!empty($redisResult)) {
            $newArr = [];
            foreach ($redisResult as $k => $v) {
                $newArr[$k] = json_decode($v, true);
            }
            return $newArr;
        } else {
            $where    = ['<>', 'external_id', ''];
            $andWhere = ['=', 'deleted', 2];
            $dbResult = MetadataDota2Hero::find()->where($where)->andWhere($andWhere)->asArray()->all();
            if (!empty($dbResult)) {
                foreach ($dbResult as $k => $v) {
                    $v['image']                   = ImageConversionHelper::showFixedSizeConversion($v['image'],144, 256);
                    $v['small_image']             = ImageConversionHelper::showFixedSizeConversion($v['small_image'], 32, 32);
                    $newArr[$v['external_id']]    = json_encode($v, 320);
                    $returnArr[$v['external_id']] = $v;
                }
                $model->hMset($redisKey, $newArr);
                return $returnArr;
            } else {
                return [];
            }
        }
    }


    public static function getFormatFramesHero($model,$heroArr){
        if (!$model){
            $model = new Dota2Socket();
        }
        $returnArr['hero_id'] = (int)$heroArr['id'];
        $returnArr['name'] = $heroArr['name'];
        $returnArr['name_cn'] = $heroArr['name_cn'];
        $returnArr['external_id'] = (string)$heroArr['external_id'];
        $returnArr['external_name'] = $heroArr['external_name'];
        $returnArr['title'] = $heroArr['title'];
        $returnArr['title_cn'] = $heroArr['title_cn'];
        $returnArr['slug'] = $heroArr['slug'];
        $returnArr['image']['image'] = $heroArr['image'];
        $returnArr['image']['small_image'] = $heroArr['small_image'];
        return $returnArr;
    }

    public static function getItemUnknownInfo($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','item','unknown'];
        $redisResult = $model->hGet($key,'unknown');
        if (empty($redisResult)){
            $MetadataDota2UnknownItem = MetadataDota2Item::find()->where(['name' => 'Unknown'])->asArray()->one();
            $model->hSet($key,'unknown',json_encode($MetadataDota2UnknownItem,320));
        }else{
            $MetadataDota2UnknownItem = json_decode($redisResult,true);
        }
        return $MetadataDota2UnknownItem;

    }

    public static function getTableItemList($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','item','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $where=  ['>','external_id',0];
            $andWhere=  ['=','deleted',2];
            $dbResult = MetadataDota2Item::find()->where($where)->asArray()->andWhere($andWhere)->all();
            foreach ($dbResult as $k=>$v){
                $v['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'],64 , 88);
                $newArr[$v['external_id']]= json_encode($v,320);
                $returnArr[$v['external_id']]= $v;
            }
            $model->hMset($key,$newArr);
            return $returnArr;
        }
    }

    public static function getTableTalentList($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','talent','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $returnArr = [];
            $where=  ['<>','external_name',''];
            $andWhere=  ['=','deleted',2];
            $dbResult = MetadataDota2Talent::find()->where($where)->andWhere($andWhere)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $returnArr[$v['external_name']]= $v;
                $newArr[$v['external_name']]= json_encode($v,320);
            }
            $model->hMset($key,$newArr);
            return $returnArr;
        }
    }
    public static function getTableAbilityList($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','ability','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $returnArr= [];
            $where=  ['>','external_id',0];
            $andWhere=  ['=','deleted',2];
            $dbResult = MetadataDota2Ability::find()->where($where)->andWhere($andWhere)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $returnArr[$v['external_id']]= $v;
                $newArr[$v['external_id']]= json_encode($v,320);
            }
            $model->hMset($key,$newArr);
            return $returnArr;
        }
    }
    public static function getTableIngameGoalList($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','ingameGoal','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $returnArr= [];
            $where=  ['>','id',0];
            $dbResult = EnumIngameGoal::find()->where($where)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $returnArr[$v['id']]= $v;
                $newArr[$v['id']]= json_encode($v,320);
            }
            $model->hMset($key,$newArr);
            return $returnArr;
        }
    }
    //天赋
    public static function getFromatFramesTalentsAndUltimateCd($model, $playerArr, $talentsJson)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $liveDataUrn = $playerArr['rel_identity_id'];
        $talentArr = [
            'ultimate_cd' => null,
            'talent'  => [],
            'abilities_old'  => []
        ];;
        $tableTalentList  = $model->tableTalentList;
        $tableAbilityList = $model->tableAbilityList;
        $abilities        = $playerArr['abilities'];
        if (empty($talentsJson)) {
            return [
                'ultimate_cd' => null,
                'talent'  => [],
                'abilities_old'  => []
            ];
        }
        $abilities_show = [1,2,3,4];
        $abilities_old = [];
        $abilities_timeline_res = $abilities_timeline_talent_res = $abilities_timeline_ability_res = [];
        $talentsJsonArr = json_decode($talentsJson, true);
        if (!empty($abilities)) {
            $ultimate_cd = 0;
            foreach ($abilities as $k => $v) {
                if ($v['level'] > 0) {
                    if (strstr($v['name'], 'special_bonus')) {
                        if (array_key_exists($v['name'], $tableTalentList)) {
                            $dbTalent                       = $tableTalentList[$v['name']];
                            $talentArr[$k]['talent_id']     = (int)$dbTalent['id'];
                            $talentArr[$k]['external_id']   = $dbTalent['external_id'];
                            $talentArr[$k]['external_name'] = $dbTalent['external_name'];
                            $left_right                     = 'left';
                            foreach ($talentsJsonArr as $k1 => $v1) {
                                if ($v1['left'] == $dbTalent['id']) {
                                    $left_right = 'left';
                                    $level      = $v1['id'];
                                }
                                if ($v1['right'] == $dbTalent['id']) {
                                    $left_right = 'right';
                                    $level      = $v1['id'];
                                }
                            }
                            $talentArr[$k]['level']      = $level;
                            $talentArr[$k]['left_right'] = $left_right;
                            $talentArr[$k]['talent']     = $dbTalent['talent'];
                            $talentArr[$k]['talent_cn']  = $dbTalent['talent_cn'];
                            $talentArr[$k]['slug']       = $dbTalent['slug'];
                            $talentArr[$k]['is_chosen']  = true;

                            $abilities_timeline_talent = [
                                'ingame_timestamp' => $model->duration,
                                'hero_level' => $playerArr['level'],
                                'type' => 'talent',
                                'ability_level' => $v['level'],
                                'talent_id' => $dbTalent['id'],
                            ];
                            $abilities_timeline_talent_res[$dbTalent['id']] = $abilities_timeline_talent;
                        }else{
                            $dbTalent                       = $tableTalentList['Unknown'];
                            $talentArr[$k]['talent_id']     = (int)$dbTalent['id'];
                            $talentArr[$k]['external_id']   = $dbTalent['external_id'];
                            $talentArr[$k]['external_name'] = $dbTalent['external_name'];
                            $talentArr[$k]['level']      = null;
                            $talentArr[$k]['left_right'] = null;
                            $talentArr[$k]['talent']     = $dbTalent['talent'];
                            $talentArr[$k]['talent_cn']  = $dbTalent['talent_cn'];
                            $talentArr[$k]['slug']       = $dbTalent['slug'];
                            $talentArr[$k]['is_chosen']  = true;
                        }
                    } else {
                        if (array_key_exists($v['abilId'], $tableAbilityList)) {
                            $dbAbility = $tableAbilityList[$v['abilId']];
                            if ($dbAbility['hotkey'] == 'R') {
                                $ultimate_cd = (int)$v['cooldownUntil'];
                            }
                            //判断是不是 在1,2,3,4 里面
                            if (in_array($dbAbility['ability_code'],$abilities_show)){
                                $abilities_timeline_ability = [
                                    'ingame_timestamp' => $model->duration,
                                    'hero_level' => $playerArr['level'],
                                    'type' => 'ability',
                                    'ability_level' => $v['level'],
                                    'ability_id' => $dbAbility['id'],
                                ];
                                $abilities_timeline_ability_res[$dbAbility['id']] = $abilities_timeline_ability;
                            }
                        }
                    }
                }
            }
            $abilities_timeline_end = @$playerArr['abilities_old'];
            $abilities_timeline_res = [];
            $ability_old = $talent_old = [];
            //$abilities_timeline = Dota2SocketPlayerService::getPlayerTimeLineInfo($model,$liveDataUrn,'abilities_timeline');
            if ($abilities_timeline_end){
                //$abilities_timeline_end = $playerArr['abilities'];
                //$abilities_timeline_end = end($abilities_timeline);
                foreach ($abilities_timeline_end as $abilities_key => $abilities_item){
                    if($abilities_item['type'] == 'ability'){
                        $ability_old[$abilities_item['ability_id']]['ability_id'] = $abilities_item['ability_id'];
                        $ability_old[$abilities_item['ability_id']]['ability_level'] = $abilities_item['ability_level'];
                    }
                    if($abilities_item['type'] == 'talent'){
                        $talent_old[$abilities_item['talent_id']]['talent_id'] = $abilities_item['talent_id'];
                        $talent_old[$abilities_item['talent_id']]['ability_level'] = $abilities_item['ability_level'];
                    }
                }
            }
            //循环 ability
            if ($abilities_timeline_ability_res){
               foreach ($abilities_timeline_ability_res as $k_ability => $val_ability){
                    $ability_level_new = $val_ability['ability_level'];
                    $ability_id = $val_ability['ability_id'];
                    $ability_old_info = @$ability_old[$ability_id];
                    if (!empty($ability_old_info)){
                        $ability_level_old = $ability_old_info['ability_level'];
                        if ($ability_level_new > $ability_level_old){
                            $abilities_timeline_res[] = $val_ability;
                        }
                    }else{
                        $abilities_timeline_res[] = $val_ability;
                    }
               }
            }
            //循环 talent
            if ($abilities_timeline_talent_res){
                foreach ($abilities_timeline_talent_res as $k_talent => $val_talent){
                    $talent_level_new = $val_talent['ability_level'];
                    $talent_id = $val_talent['talent_id'];
                    $talent_old_info = @$talent_old[$talent_id];
                    if ($talent_old_info){
                        $talent_level_old = $talent_old_info['ability_level'];
                        if ($talent_level_new > $talent_level_old){
                            $abilities_timeline_res[] = $val_talent;
                        }
                    }else{
                        $abilities_timeline_res[] = $val_talent;
                    }
                }
            }
            if (!empty($abilities_timeline_res)){
                $abilities_timeline_ability_result = [];
                if (!empty($abilities_timeline_ability_res)){
                    $abilities_timeline_ability_result = array_values($abilities_timeline_ability_res);
                }
                $abilities_timeline_talent_result = [];
                if (!empty($abilities_timeline_talent_res)){
                    $abilities_timeline_talent_result = array_values($abilities_timeline_talent_res);
                }
                $abilities_old = array_merge($abilities_timeline_ability_result,$abilities_timeline_talent_result);
                Dota2SocketPlayerService::setPlayerTimeLineInfo($model,$liveDataUrn,$abilities_timeline_res,'abilities_timeline');
            }

            $talentNewArr = [];
            foreach ($talentsJsonArr as $k => $v) {
                foreach ($talentArr as $k1 => $v1) {
                    if ($v1['talent_id'] == $v['left'] || $v1['talent_id'] == $v['right']) {
                        $talentNewArr[] = $v1;
                    }
                }
            }

            $returnArr =[
                'ultimate_cd' => $ultimate_cd,
                'talent'  => $talentNewArr,
                'abilities_old'  => $abilities_old
            ];
            return $returnArr;
        } else {
            return $talentArr;
        }

    }


    public static function getFromatFramesItems($model,$playerArr){
        if (!$model){
            $model = new Dota2Socket();
        }
        $liveDataUrn = $playerArr['rel_identity_id'];
        $playerInfo = Dota2SocketPlayerService::getPlayerInfo($model,$liveDataUrn);
        //转化item
        $inventoryArr = self::getFormatFrameItem($model,$playerArr['itemsInventory'],'itemsInventory');
        $backpackArr = self::getFormatFrameItem($model,$playerArr['itemsBackpack'],'itemsBackpack');
        $neutralArr = self::getFormatFrameItem($model,$playerArr['itemNeutral'],'itemNeutral');
        $stashArr = self::getFormatFrameItem($model,$playerArr['itemsStash'],'itemsStash');
        $mob_inventoryArr = self::getFormatFrameItem($model,$playerArr['itemsBear'],'itemsBear');
        $mob_backpackArr = self::getFormatFrameItem($model,$playerArr['mob_backpack'],'mob_backpack');

        $returnArr['inventory'] = $inventoryArr;//物品栏
        $returnArr['backpack'] = $backpackArr;//背包
        $returnArr['neutral'] = $neutralArr;//野外掉落
        $returnArr['stash'] = $stashArr;//储藏处
        $returnArr['buffs'] = null;//增益
        $returnArr['mob_inventory'] = $mob_inventoryArr;//召唤物物品栏
        $returnArr['mob_backpack'] = $mob_backpackArr;//召唤物背包
        $returnArr['mob_neutral'] = null;//召唤物野外掉落
        //更新时间线 和 装备信息
        if ($playerInfo){
            $updateInfo = [];
            $playerItemOld = @$playerInfo['items'];
            if (empty($playerItemOld)){
                $updateInfo['items'] = $returnArr;
                $items_modified_res = [];
                $item_array_key = 0;
                foreach ($returnArr as $key => $value){
                    if (in_array($key,['inventory','backpack','stash','mob_inventory','mob_backpack'])){
                        foreach ($value as $val){
                            if ($val){
                                $items_modified_res[$item_array_key]['item_id'] = $val['item_id'];
                                $items_modified_res[$item_array_key]['total_cost'] = $val['total_cost'];
                                $item_array_key ++;
                            }
                        }
                    }
                    if (in_array($key,['neutral','buffs','mob_neutral'])){
                        if ($value){
                            $items_modified_res[$item_array_key]['item_id'] = $value['item_id'];
                            $items_modified_res[$item_array_key]['total_cost'] = $value['total_cost'];
                            $item_array_key ++;
                        }
                    }
                }
                if ($items_modified_res){
                    $total_cost_array = array_column($items_modified_res,'total_cost');
                    array_multisort($total_cost_array,SORT_DESC,$items_modified_res);
                    $items_modified_res = array_column($items_modified_res,'item_id');
                    $items_modified = [
                        'ingame_timestamp' => $model->duration,
                        'items_modified' => $items_modified_res
                    ];
                    //保存time_line
                    Dota2SocketPlayerService::setPlayerTimeLineInfo($model,$liveDataUrn,$items_modified,'items_timeline');
                }
            }else{
                $returnArrRes = $returnArr;
                $item_array_key = 0;
                $items_modified_res = [];
                foreach ($returnArr as $key => $value){
                    if (in_array($key,['inventory','backpack','stash','mob_inventory','mob_backpack'])){
                        foreach ($value as $val){
                            if ($val){
                                $items_modified_res[$item_array_key]['item_id'] = $val['item_id'];
                                $items_modified_res[$item_array_key]['total_cost'] = $val['total_cost'];
                                $item_array_key ++;
                            }
                        }
                    }
                    if (in_array($key,['neutral','buffs','mob_neutral'])){
                        if ($value){
                            $items_modified_res[$item_array_key]['item_id'] = $value['item_id'];
                            $items_modified_res[$item_array_key]['total_cost'] = $value['total_cost'];
                            $item_array_key ++;
                        }
                    }
                }
                $items_timeline = Dota2SocketPlayerService::getPlayerTimeLineInfo($model,$liveDataUrn,'items_timeline');
                if (!empty($items_timeline)){
                    if (is_array($items_timeline) && !empty($items_modified_res)){
                        $total_cost_array = array_column($items_modified_res,'total_cost');
                        array_multisort($total_cost_array,SORT_DESC,$items_modified_res);
                        $items_modified_res = array_column($items_modified_res,'item_id');
                        $end_items_timeline = end($items_timeline);
                        $old_items_timeline_md5 = md5(json_encode($end_items_timeline['items_modified']));
                        $new_items_timeline_md5 = md5(json_encode($items_modified_res));
                        if ($old_items_timeline_md5 != $new_items_timeline_md5){
                            $updateInfo['items'] = $returnArrRes;
                            $items_modified = [
                                'ingame_timestamp' => $model->duration,
                                'items_modified' => $items_modified_res
                            ];
                            //保存time_line
                            Dota2SocketPlayerService::setPlayerTimeLineInfo($model,$liveDataUrn,$items_modified,'items_timeline');
                        }
                    }
                }else{
                    if ($items_modified_res){
                        $updateInfo['items'] = $returnArrRes;
                        $total_cost_array = array_column($items_modified_res,'total_cost');
                        array_multisort($total_cost_array,SORT_DESC,$items_modified_res);
                        $items_modified_res = array_column($items_modified_res,'item_id');
                        $items_modified = [
                            'ingame_timestamp' => $model->duration,
                            'items_modified' => $items_modified_res
                        ];
                        //保存time_line
                        Dota2SocketPlayerService::setPlayerTimeLineInfo($model,$liveDataUrn,$items_modified,'items_timeline');
                    }
                }
            }
            if (!empty($updateInfo)){
                //保存装备
                Dota2SocketPlayerService::setPlayerInfo($model,$liveDataUrn,$updateInfo);
            }

        }
        return $returnArr;
    }
    //道具
    public static function getFormatFrameItem($model, $item, $itemKey){
        $tableItemList=$model->tableItemList;

        $inventoryArr = [];

        $mutiArr = [
            'itemsInventory',
            'itemsBackpack',
            'itemsStash',
            'itemsBear',
            'mob_backpack',
        ];
        if (in_array($itemKey,$mutiArr)){
            if (empty($item)){
                if ($itemKey=='itemsInventory'||$itemKey=='itemsStash'||$itemKey=='itemsBear'){
                    for ($i=1;$i<=6;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }else{
                    for ($i=1;$i<=3;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }
            }else{
                $inventoryArrRes = [];
                $i = 1;
                foreach ($item as $k => $v) {
                    $i = $k + 1;
                    if (array_key_exists($v['itemId'], $tableItemList)) {
                        $inventoryArrData = [];

                        $dbItem                                    = $tableItemList[$v['itemId']];
//                        $dbItem = 254;
                        if ($dbItem['name_cn'] == '回城卷轴'){
                            continue;
                        }
                        $inventoryArrData['item_id']         = (int)$dbItem['id'];
                        $inventoryArrData['name']            = $dbItem['name'];
                        $inventoryArrData['name_cn']         = $dbItem['name_cn'];
                        $inventoryArrData['external_id']     = (string)$dbItem['external_id'];
                        $inventoryArrData['external_name']   = $dbItem['external_name'];
                        $inventoryArrData['total_cost']      = (int)$dbItem['total_cost'];
                        $inventoryArrData['is_recipe']       = $dbItem['is_recipe']==1;
                        $inventoryArrData['is_secret_shop']  = $dbItem['is_secret_shop']==1;
                        $inventoryArrData['is_home_shop']    = $dbItem['is_home_shop']==1;
                        $inventoryArrData['is_neutral_drop'] = $dbItem['is_neutral_drop']==1;
                        $inventoryArrData['slug']            = $dbItem['slug'];
                        $inventoryArrData['image']           = $dbItem['image'];
                        $inventoryArrData['purchase_time']   = null;
                        $inventoryArrData['cooldown']        = (int)$v['cooldownUntil'];

                        $inventoryArrRes[] = $inventoryArrData;
                    }
                }
                $total_cost_array = array_column($inventoryArrRes,'total_cost');
                array_multisort($total_cost_array,SORT_DESC,$inventoryArrRes);
                if ($itemKey == 'itemsInventory' || $itemKey == 'itemsStash' || $itemKey == 'itemsBear') {
                    if ($i < 6) {
                        for ($j = $i+1; $j <= 6; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                    if ($i > 6) {
                        $inventoryArrRes = array_slice($inventoryArrRes,0,6);
                    }
                }else{
                    if ($i < 3) {
                        for ($j = $i+1; $j <= 3; $j++) {
                            $inventoryArrRes[] = null;
                        }
                    }
                }

                $inventoryArrMiddle = [];
                foreach ($inventoryArrRes as $index_key => $val){
                    $inventoryKey = $index_key + 1;
                    $inventoryArrMiddle['slot_'.$inventoryKey] = $val;
                }
                $inventoryArr = $inventoryArrMiddle;
            }

        }else{
            if (empty($item)){
                return null;
            }
            $dbItem                          = $tableItemList[$item['itemId']];
            if (empty($dbItem)){
                return null;
            }
            $inventoryArr['item_id']         = (int)$dbItem['id'];
            $inventoryArr['name']            = $dbItem['name'];
            $inventoryArr['name_cn']         = $dbItem['name_cn'];
            $inventoryArr['external_id']     = (string)$dbItem['external_id'];
            $inventoryArr['external_name']   = $dbItem['external_name'];
            $inventoryArr['total_cost']      = (int)$dbItem['total_cost'];
            $inventoryArr['is_recipe']       = $dbItem['is_recipe']==1;
            $inventoryArr['is_secret_shop']  = $dbItem['is_secret_shop']==1;
            $inventoryArr['is_home_shop']    = $dbItem['is_home_shop']==1;
            $inventoryArr['is_neutral_drop'] = $dbItem['is_neutral_drop']==1;
            $inventoryArr['slug']            = $dbItem['slug'];
            $inventoryArr['image']           = $dbItem['image'];
            $inventoryArr['purchase_time']   = null;
            $inventoryArr['cooldown']        = (int)$item['cooldownUntil'];
        }
        return $inventoryArr;
    }
    public static function getFormatFramesFactionPlayer($model, $vArr){
        if (!$model){
            $model = new Dota2Socket();
        }
        $tableHeroList= $model->tableHeroList;

        $vPlayer = [];
        $vPlayer['seed']                                     = $vArr['seed'];
        $vPlayer['faction']                                  = $vArr['faction'];
        $vPlayer['role']                                     = $vArr['role'];
        $vPlayer['lane']                                     = $vArr['lane'];
        $vPlayer['player']['player_id']                      = $vArr['player_id'];
        $vPlayer['player']['nick_name']                      = $vArr['nick_name'];
        $vPlayer['player']['steam_id']                       = $vArr['steam_id'];
        $heroDetails = [];
        if ($vArr['heroId']){
            if (array_key_exists($vArr['heroId'],$tableHeroList)){
                $heroDetails = $tableHeroList[$vArr['heroId']];
                $vPlayer['hero']   = self::getFormatFramesHero($model,$tableHeroList[$vArr['heroId']]);
            }else{
                $vPlayer['hero']  =null;
            }
        }else{
            $vPlayer['hero']  =null;
        }

        $vPlayer['level']                                    = $vArr['level'];
        $vPlayer['is_alive']                                 = $vArr['is_alive'];
        $vPlayer['kills']                                    = $vArr['kills'];
        $vPlayer['deaths']                                   = $vArr['deaths'];
        $vPlayer['assists']                                  = $vArr['assists'];
        $vPlayer['kda']                                      = (string)$vArr['kda'];
        $vPlayer['participation']                            = $vArr['participation'];
        $vPlayer['last_hits']                                = $vArr['last_hits'];
        $vPlayer['lhpm']                                     = $vArr['lhpm'];
        $vPlayer['denies']                                   = $vArr['denies'];
        $vPlayer['items']                                    = self::getFromatFramesItems($model,$vArr);

        //advanced
        $talentsArr = self::getFromatFramesTalentsAndUltimateCd($model,$vArr,$heroDetails['talents']);
        $vPlayer_talent                                      = $talentsArr['talent'];
        $vPlayer_ultimate_cd                                 = $talentsArr['ultimate_cd'];
        $vPlayer_abilities_old                               = $talentsArr['abilities_old'];
        $vPlayer['advanced']['talents']                      = $vPlayer_talent;
        $vPlayer['advanced']['ultimate_cd']                  = $vPlayer_ultimate_cd;
        $vPlayer['advanced']['coordinate']                   = $vArr['coordinate'];
        $vPlayer['advanced']['is_visible']                   = $vArr['is_visible'];
        $vPlayer['advanced']['respawntimer']                 = $vArr['respawntimer_duration']?($model->duration-$vArr['respawntimer_duration']>0?$model->duration-$vArr['respawntimer_duration']:0):0;
        $vPlayer['advanced']['buyback_cooldown']             = null;//self::checkAndChangeInt($vArr['buyback_cooldown']);
        $vPlayer['advanced']['buyback_cost']                 = $vArr['buyback_cost'];
        $vPlayer['advanced']['health']                       = $vArr['health'];
        $vPlayer['advanced']['health_max']                   = $vArr['health_max'];
        $vPlayer['advanced']['tower_kills']                  = $vArr['tower_kills'];
        $vPlayer['advanced']['barrack_kills']                = $vArr['barrack_kills'];
        $vPlayer['advanced']['melee_barrack_kills']          = $vArr['melee_barrack_kills'];
        $vPlayer['advanced']['ranged_barrack_kills']         = $vArr['ranged_barrack_kills'];
        $vPlayer['advanced']['roshan_kills']                 = $vArr['roshan_kills'];
        $vPlayer['advanced']['double_kill']                  = $vArr['double_kill'];
        $vPlayer['advanced']['triple_kill']                  = $vArr['triple_kill'];
        $vPlayer['advanced']['ultra_kill']                   = $vArr['ultra_kill'];
        $vPlayer['advanced']['rampage']                      = $vArr['rampage'];
        $vPlayer['advanced']['largest_multi_kill']           = $vArr['largest_multi_kill'];
        $vPlayer['advanced']['largest_killing_spree']        = $vArr['largest_killing_spree'];
        $vPlayer['advanced']['lane_creep_kills']             = $vArr['lane_creep_kills'];
        $vPlayer['advanced']['neutral_creep_kills']          = $vArr['neutral_creep_kills'];
        $vPlayer['advanced']['net_worth']                    = $vArr['net_worth'];
        $vPlayer['advanced']['net_worth_percent']            = $vArr['net_worth_percent'];
        $vPlayer['advanced']['gold_earned']                  = $vArr['gold_earned'];
        $vPlayer['advanced']['gold_spent']                   = $vArr['gold_spent'];
        $vPlayer['advanced']['gold_remaining']               = $vArr['gold_remaining'];
        $vPlayer['advanced']['gold_reliable']                = $vArr['gold_reliable'];
        $vPlayer['advanced']['gold_unreliable']              = $vArr['gold_unreliable'];
        $vPlayer['advanced']['gold_herokill']                = $vArr['gold_herokill'];
        $vPlayer['advanced']['gold_creepkill']               = $vArr['gold_creepkill'];
        $vPlayer['advanced']['gpm']                          = $vArr['gpm'];
        $vPlayer['advanced']['experience']                   = $vArr['experience'];
        $vPlayer['advanced']['xpm']                          = $vArr['xpm'];
        $vPlayer['advanced']['damage_to_heroes']             = $vArr['damage_to_heroes'];
        $vPlayer['advanced']['damage_to_heroes_hp_removal']  = $vArr['damage_to_heroes_hp_removal'];
        $vPlayer['advanced']['damage_to_heroes_magical']     = $vArr['damage_to_heroes_magical'];
        $vPlayer['advanced']['damage_to_heroes_physical']    = $vArr['damage_to_heroes_physical'];
        $vPlayer['advanced']['damage_to_heroes_pure']        = $vArr['damage_to_heroes_pure'];
        $vPlayer['advanced']['dpm_to_heroes']                = $vArr['dpm_to_heroes'];
        $vPlayer['advanced']['damage_percent_to_heroes']     = $vArr['damage_percent_to_heroes'];
        $vPlayer['advanced']['damage_taken']                 = $vArr['damage_taken'];
        $vPlayer['advanced']['damage_taken_hp_removal']      = $vArr['damage_taken_hp_removal'];
        $vPlayer['advanced']['damage_taken_magical']         = $vArr['damage_taken_magical'];
        $vPlayer['advanced']['damage_taken_physical']        = $vArr['damage_taken_physical'];
        $vPlayer['advanced']['damage_taken_pure']            = $vArr['damage_taken_pure'];
        $vPlayer['advanced']['dtpm']                         = $vArr['dtpm'];
        $vPlayer['advanced']['damage_taken_percent']         = $vArr['damage_taken_percent'];
        $vPlayer['advanced']['damage_conversion_rate']       = $vArr['damage_conversion_rate'];
        $vPlayer['advanced']['damage_to_buildings']          = $vArr['damage_to_buildings'];
        $vPlayer['advanced']['damage_to_towers']             = $vArr['damage_to_towers'];
        $vPlayer['advanced']['damage_to_objectives']         = $vArr['damage_to_objectives'];
        $vPlayer['advanced']['total_crowd_control_time']     = $vArr['total_crowd_control_time'];
        $vPlayer['advanced']['total_heal']                   = $vArr['total_heal'];
        $vPlayer['advanced']['total_runes_pickedup']         = $vArr['total_runes_pickedup'];
        $vPlayer['advanced']['bounty_runes_pickedup']        = null;//$vArr['bounty_runes_pickedup'];
        $vPlayer['advanced']['double_damage_runes_pickedup'] = null;//$vArr['double_damage_runes_pickedup'];
        $vPlayer['advanced']['haste_runes_pickedup']         = null;//$vArr['haste_runes_pickedup'];
        $vPlayer['advanced']['illusion_runes_pickedup']      = null;//$vArr['illusion_runes_pickedup'];
        $vPlayer['advanced']['invisibility_runes_pickedup']  = null;//$vArr['invisibility_runes_pickedup'];
        $vPlayer['advanced']['regeneration_runes_pickedup']  = null;//$vArr['regeneration_runes_pickedup'];
        $vPlayer['advanced']['arcane_runes_pickedup']        = null;//$vArr['arcane_runes_pickedup'];
        $vPlayer['advanced']['smoke_purchased']              = $vArr['smoke_purchased'];
        $vPlayer['advanced']['smoke_used']                   = $vArr['smoke_used'];
        $vPlayer['advanced']['dust_purchased']               = $vArr['dust_purchased'];
        $vPlayer['advanced']['dust_used']                    = $vArr['dust_used'];
        $vPlayer['advanced']['observer_wards_purchased']     = $vArr['observer_wards_purchased'];
        $vPlayer['advanced']['observer_wards_placed']        = $vArr['observer_wards_placed'];
        $vPlayer['advanced']['observer_wards_kills']         = $vArr['observer_wards_kills'];
        $vPlayer['advanced']['sentry_wards_purchased']       = $vArr['sentry_wards_purchased'];
        $vPlayer['advanced']['sentry_wards_placed']          = $vArr['sentry_wards_placed'];
        $vPlayer['advanced']['sentry_wards_kills']           = $vArr['sentry_wards_kills'];

        //保存选手信息
        $player_talents_res = null;
        if (!empty($vPlayer_talent)){
            foreach ($vPlayer_talent as $key_talents => $val_talent){
                $player_talents_res[$key_talents]['talent_id'] = $val_talent['talent_id'];
                $player_talents_res[$key_talents]['is_chosen'] = $val_talent['is_chosen'];
            }
        }
        $updatePlayerInfo = [
            'talents' => @$player_talents_res,
            'ultimate_cd' => @$vPlayer_ultimate_cd
        ];
        if (!empty($vPlayer_abilities_old)){
            $updatePlayerInfo['abilities_old'] = $vPlayer_abilities_old;
        }
        Dota2SocketPlayerService::setPlayerInfo($model,$vArr['rel_identity_id'],$updatePlayerInfo);
        return $vPlayer;
    }


    public static function getFramesTeamAdvanced($model, $redisData)
    {
        $data['total_runes_pickedup']         = $redisData['total_runes_pickedup'];
        $data['bounty_runes_pickedup']        = null;//$redisData['bounty_runes_pickedup'];
        $data['double_damage_runes_pickedup'] = null;//$redisData['double_damage_runes_pickedup'];
        $data['haste_runes_pickedup']         = null;//$redisData['haste_runes_pickedup'];
        $data['illusion_runes_pickedup']      = null;//$redisData['illusion_runes_pickedup'];
        $data['invisibility_runes_pickedup']  = null;//$redisData['invisibility_runes_pickedup'];
        $data['regeneration_runes_pickedup']  = null;//$redisData['regeneration_runes_pickedup'];
        $data['arcane_runes_pickedup']        = null;//$redisData['arcane_runes_pickedup'];
        $data['smoke_purchased']              = $redisData['smoke_purchased'];
        $data['smoke_used']                   = $redisData['smoke_used'];
        $data['dust_purchased']               = $redisData['dust_purchased'];
        $data['dust_used']                    = $redisData['dust_used'];
        $data['observer_wards_purchased']     = $redisData['observer_wards_purchased'];
        $data['observer_wards_placed']        = $redisData['observer_wards_placed'];
        $data['observer_wards_kills']         = $redisData['observer_wards_kills'];
        $data['sentry_wards_purchased']       = $redisData['sentry_wards_purchased'];
        $data['sentry_wards_placed']          = $redisData['sentry_wards_placed'];
        $data['sentry_wards_kills']           = $redisData['sentry_wards_kills'];
        return $data;
    }





}