<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Throwable;
use yii\base\Exception;

class Dota2SocketPlayerService extends FiveHotCsgoMatchWs implements SocketPlayerInterface
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    public static function getPlayerList($model,$battleId=null)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        $key  = ['history','battle',$model->battleId,'player','list'];
        return  $model->hGetAll($key);
    }

    public static function setPlayerInfo($model,$liveDataPlayerUrn,$playerDetails){
        if (!$model){
            $model = new Dota2Socket();
        }
        if (empty($liveDataPlayerUrn)){
            return false;
        }
        $oldData=Dota2SocketPlayerService::getPlayerInfo($model,$liveDataPlayerUrn);
        if ($oldData&&is_array($oldData)){
            $updateArr = array_merge($oldData,$playerDetails);
        }else{
            $updateArr = $playerDetails;
        }
        $key  =  ['history','battle',$model->battleId,'player','list'];
        $json = json_encode($updateArr,320);
        $model->hSet($key,$liveDataPlayerUrn,$json);
        return $updateArr;
    }

    public static function getPlayerInfo($model,$liveDataPlayerUrn){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key  =  ['history','battle',$model->battleId,'player','list'];
        $playerJson =   $model->hGet($key,$liveDataPlayerUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }

    public static function setPlayerTimeLineInfo($model,$liveDataPlayerUrn,$playerDetails,$type){
        if (!$model){
            $model = new Dota2Socket();
        }
        //组成二维数组
        $playerDetailsRes[] = $playerDetails;
        $updateArr = [];
        $key  = ['history','battle',$model->battleId,'player_'.$type];
        $playerTimeLineInfo = $model->hGet($key,$liveDataPlayerUrn);
        if ($playerTimeLineInfo){
            $playerTimeLineInfoArray = @json_decode($playerTimeLineInfo,true);
            if ($playerTimeLineInfoArray && is_array($playerTimeLineInfoArray)){
                $updateArr = array_merge($playerTimeLineInfoArray,$playerDetailsRes);
            }
        }else{
            $updateArr = $playerDetailsRes;
        }
        if (!empty($updateArr)){
            $json = @json_encode($updateArr,320);
            $model->hSet($key,$liveDataPlayerUrn,$json);
        }
        return true;
    }
    public static function getPlayerTimeLineInfo($model,$liveDataPlayerUrn,$type){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key  = ['history','battle',$model->battleId,'player_'.$type];
        $playerTimeLineInfo = $model->hGet($key,$liveDataPlayerUrn);
        if ($playerTimeLineInfo){
            $playerTimeLineInfoArray = @json_decode($playerTimeLineInfo,true);
        }else{
            $playerTimeLineInfoArray = [];
        }
        return $playerTimeLineInfoArray;
    }
    public static function getPlayerTimeLineInfoList($model,$battleId,$type)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        $key  = ['history','battle',$battleId,'player_'.$type];
        return  $model->hGetAll($key);
    }

}