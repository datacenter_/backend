<?php
namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\SteamHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\match\models\Match as MatchModel;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\hot\pandascore\PandascoreHotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Throwable;
use yii\base\Exception;

class Dota2SocketTeamService extends FiveHotCsgoMatchWs
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "bayes_dota_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;


    public static function getTeamList($model)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        return $model->hGetAll(['history','battle',$model->battleId,'team','list']);
    }

    public static function getTeamListFormat($model, $moreField = null)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $tableTeamList  = Dota2SocketTeamService::getTableTeam($model);
        $redisValueList = $model->hGetAll(['history','battle',$model->battleId,'team','list']);
        if (!empty($redisValueList)) {
            $newArr = [];
            $newUrnArr = [];
            foreach ($redisValueList as $k_team => $v_team) {
                $v_team_arr          = json_decode($v_team, true);
                $tableTeamDetails    = $tableTeamList[$v_team_arr['team_id']];
                $tableTeamDetailsArr = json_decode($tableTeamDetails, true);
                $faction             = $v_team_arr['faction'];
                if ($moreField == 'all') {
                    $newArr[$faction] = $v_team_arr;
                }
                if ($moreField == 'urnField') {
                    $newUrnArr[$k_team]['team_id']        = (int)$v_team_arr['team_id'];
                    $newUrnArr[$k_team]['name']           = $tableTeamDetailsArr['name'];
                    $newUrnArr[$k_team]['image']          = $tableTeamDetailsArr['image_200x200'];
                    $newUrnArr[$k_team]['opponent_order'] = $v_team_arr['order'];
                }
                $newArr[$faction]['team_id']        = (int)$v_team_arr['team_id'];
                $newArr[$faction]['name']           = $tableTeamDetailsArr['name'];
                $newArr[$faction]['image']          = $tableTeamDetailsArr['image_200x200'];
                $newArr[$faction]['opponent_order'] = $v_team_arr['order'];
                if ($moreField == 'score') {
                    $newArr[$faction]['score'] = $v_team_arr['score'];
                }

            }
            if ($moreField == 'urnField') {
                return $newUrnArr;
            }
            return $newArr;
        } else {
            return false;
        }
        return $model->hGetAll(['history','battle',$model->battleId,'team','list']);
    }

    public static function getTableTeam($model){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','team','list'];
        $tableTeam = $model->hGetAll($key);
        if (!empty($tableTeam)){
            return $tableTeam;
        }else{
            return false;
        }
    }

    public static function setTeamInfo($model,$teamUrn,$upDateTeamArr){
        if (!$model){
            $model = new Dota2Socket();
        }
        if (empty($teamUrn)){
            return false;
        }
        $oldData=Dota2SocketTeamService::getTeamInfo($model,$teamUrn);
        if ($oldData&&is_array($oldData)){
            $updateArr = array_merge($oldData,$upDateTeamArr);
        }else{
            $updateArr = $upDateTeamArr;
        }
        $json = json_encode($updateArr,true);
        return $model->hSet(['history','battle',$model->battleId,'team','list'],$teamUrn,$json);
    }
    public static function getTeamInfo($model,$liveDataTeamUrn){
        if (!$model){
            $model = new Dota2Socket();
        }
        $key  = ['history','battle',$model->battleId,'team','list'];
        $playerJson =   $model->hGet($key,$liveDataTeamUrn);
        if (empty($playerJson)){
            return false;
        }else{
            return json_decode($playerJson,true);
        }
    }

    public static function getHistoryBattleTeamByBattleId($model,$battleId){
        if (!$model){
            $model = new Dota2Socket();
        }
//        $key  = ['history','battle',$battleId,'team'];
        $key  = ['history','battle',$model->battleId,'team','list'];
        $teamJson =   $model->hGetAll($key);
        if (empty($teamJson)){
            return false;
        }else{
            foreach ($teamJson as $k=>$v){
                $teamNewArr[$k]= json_decode($v,true);
            }
            return $teamNewArr;
        }
    }


    public static function getTeamPlayerByType($model,$type='all'){
        if (!$model){
            $model  = new Dota2Socket();
        }
        $playList = Dota2SocketPlayerService::getPlayerList($model);
        if (!empty($playList)){
            $radiantArr = [];
            $direArr = [];
            $allArr = [];
            foreach($playList as $k=>$v){
                $vArr = json_decode($v,true);
                if ($vArr['faction']=='radiant'){
                    $radiantArr[] = $vArr;
                }else{
                    $direArr[] = $vArr;
                }
                $allArr[] = $vArr;
            }
            if ($type=='radiant'){
                return $radiantArr;
            }elseif ($type=='dire'){
                return $direArr;
            }else{
                return $allArr;
            }

        }else{
            return false;
        }

    }


}