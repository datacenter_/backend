<?php

namespace app\modules\task\services\wsdata\bayes;

use app\modules\common\models\EnumFirstEvents;
use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\SteamHelper;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\WsToApiSynchron;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
use Throwable;

/**
 * Bayes Dota2 Ws To Api
 **/
class Dota2SocketToApi extends Dota2Socket
{
    static private $instance;
    const DOTA_GAME_ID = 3;
    public $updateType = 1;
    public $round_end_count = 0;
    public $info = [];

    /**
     * @param $tagInfo
     * @param $taskInfo
     * @return bool
     */
    public static function run($tagInfo, $taskInfo)
    {

        //info
        $self = self::getInstance();
        $info = json_decode($taskInfo['params'], true);
        if (!$info['match_id']) {
            return false;
        }

        $self->currentPre= 'bayes_dota_ws';
        //match info
        $updateType       = $info['updateType'];
        $matchId          = $info['match_id'];
        $currentPre       = $info['currentPre'];
        $origin_id       = $info['origin_id'];
        $self->matchId = $matchId;
        $self->currentPre = $currentPre ? $currentPre : 'bayes_dota_ws';
        if (array_key_exists('event_id', $info) && $info['event_id']) {
            $self->event_id = $info['event_id'];
        }
        if (array_key_exists('battleUrn', $info) && $info['battleUrn']) {
            $self->battleUrn = $info['battleUrn'];
        }
        $self->info = $info;
        if (!$matchId) {
            $infoFormat = [
                'match'   => null,
                'battles' => []
            ];
        } else {
            $matchConversionInfo   = $self->conversionMatch($self,$matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battlesConversionInfo = $self->getMatchBattles($self,$matchId, $updateType);
            $infoFormat['battles'] = $battlesConversionInfo;
        }

        //refresh
        self::refresh($matchId, $infoFormat);
        //判断是不是结束了
        if($infoFormat['match']['status'] == 3){
            self::refreshApi($matchId,$origin_id);
        }

    }


    public function conversionMatch($self,$matchId)
    {
        $this->matchId = $matchId;
        //读取redis 组出来infoFormat
        $matchDetails = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_DETAILS]);
        if (!empty($matchDetails)) {
            $matchDetailsArr = json_decode($matchDetails,true);
            $match_begin_at_value     = $matchDetailsArr['begin_at'];
            $match_end_at_value       = $matchDetailsArr['end_at'];
            $match_status_value       = $matchDetailsArr['status'];
            $match_team_1_score_value = $matchDetailsArr['team_1_score'];
            $match_team_2_score_value = $matchDetailsArr['team_2_score'];
            $match_game_version       = $matchDetailsArr['game_version'];
            $match_winner_value       = $matchDetailsArr['winner'];
            if (!empty($match_begin_at_value)) {
                $length = strlen($match_begin_at_value);
                if ($length == 23) {
                    $match_begin_at_value = substr($match_begin_at_value, 0, 19);
                }
            }
            if (!empty($match_end_at_value)) {
                $length = strlen($match_end_at_value);
                if ($length == 23) {
                    $match_end_at_value = substr($match_end_at_value, 0, 19);
                }
            }
            $matchinfoFormat = [
                'begin_at'             => $match_begin_at_value == 'null' ? '' : (string)$match_begin_at_value,
                'end_at'               => $match_end_at_value == 'null' ? '' : (string)$match_end_at_value,
                'status'               => $match_status_value ? intval($match_status_value) : 1,
                'team_1_score'         => intval($match_team_1_score_value),
                'team_2_score'         => intval($match_team_2_score_value),
                'winner'               => intval($match_winner_value),
                'is_battle_detailed'   => 1,
                'is_pbpdata_supported' => 1,
                'game_version'         => $match_game_version,
            ];
            return $matchinfoFormat;
        } else {
            return false;
        }

    }


    private function getMatchBattles($model,$matchId, $updateType = 1)
    {
        $this->updateType = $updateType;
        $battlesRes       = [];
        //查找所有battle
        $battles       = $this->getBattles();
//        $bayesBattleId = Dota2SocketBattleService::getBayesBattleId($model);
        $bayesBattleId = $model->battleUrn;
        if (!empty($battles) && $bayesBattleId) {
            $battlesArr = array_map([__CLASS__, 'mapJson'], $battles);
            foreach ($battlesArr as $key => $battle) {
                if ($key == $bayesBattleId) {
                    //刷新当前battle
                    $battle_id  = $battle['battle_id'];
                    $battleInfo = $this->conversionBattles($model,$matchId, $battle_id, $battle);
                    if ($battleInfo['order']) {
                        $battlesRes[$battle['battle_id']] = $battleInfo;
                    }
                }
            }
        }
        return $battlesRes;
    }

    // base 数据格式(结果)
    private static $battleFieldConfig = [];
    // static 转换完数组(结果)
    private static $battleStaticFieldArray = [];
    // static 数据格式设置
    private static $battleStaticFieldConfig = [];
    // detail 数据格式设置(结果)
    private static $battleDetailFieldConfig = [];
    // battle下 players转换完数组(结果)
    private static $battlePlayersFieldArray = [];
    // rounds下 players转换完数组(结果)
    private static $roundPlayersFieldArray = [];
    // match下 teams转换完数组(结果)
    private static $teamsFieldArray = [];
    // battle下 rounds转换完数组(结果)
    private static $roundsFieldArray = [];
    // rounds下 events转换完数组(结果)
    private static $roundEventsFieldArray = [];
    private static $WinEndTypeArray = [
        'bomb_defused'   => 1,
        'cts_win'        => 2,
        'target_saved'   => 3,
        'target_bombed'  => 4,
        'terrorists_win' => 5,
        'round_draw'     => 6,
    ];

    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        // 初始化
        self::$battleFieldConfig = [
//            'status' => 3,
            'is_draw'            => 2,
            'is_forfeit'         => 2,
            'is_battle_detailed' => 1 //是否有对局详情数据
        ];
        self::$battleFieldConfig = array_merge(self::$battleFieldConfig, $baseResultData);
    }

    // set static数据
    public static function setBattleStatic($staticResultData)
    {
        // 初始化
        self::$battleStaticFieldConfig  = [];
        self::$battleStaticFieldArray[] = array_merge(self::$battleStaticFieldConfig, $staticResultData);
    }


    private function conversionBattles($model,$matchId, $battleId, $battle)
    {
        $model->battleId = $battleId;
        // 初始化
        self::initializationBattleInfo();
        // base
        $orderId = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, self::KEY_TAG_ORDER]);
        if (!empty($orderId)) {
            // detail
            $this->setDotaBattleDetails($model,$battle);
            if (!empty($battle['begin_at'])) {
                $length = strlen($battle['begin_at']);
                if ($length == 23) {
                    $battle['begin_at'] = substr($battle['begin_at'], 0, 19);
                }
            }
            if (!empty($battle['end_at'])) {
                $length = strlen($battle['end_at']);
                if ($length == 23) {
                    $battle['end_at'] = substr($battle['end_at'], 0, 19);
                }
            }
            //base
            self::setBattleBase([
                'game'                 => self::DOTA_GAME_ID,
                'match'                => $matchId,
                'order'                => @$battle['order'] ? $battle['order'] : 0,
                'begin_at'             => @$battle['begin_at'],
                'end_at'               => @$battle['end_at'],
                'status'               => @$battle['status'], //battle状态
                'is_draw'              => @$battle['is_draw'] ? 1 : 2,
                'is_forfeit'           => @$battle['is_forfeit'] ? 1 : 2,
                'is_default_advantage' => @$battle['is_default_advantage'] ? 1 : 2, //是否是默认领先获胜
                'map'                  => @$battle['map'],
                //'map_name'             => @$battle['map_details']['name'],
                'duration'             => @$battle['duration'], //对局时长
                'winner'               => @$battle['winner_details']['opponent_order'] ? (int)$battle['winner_details']['opponent_order'] : null, //获胜战队order
                'deleted'               => 2, //获胜战队order
                'deleted_at'               => null, //获胜战队order
            ]);

        } else {
            return ['order' => null];
        }
        $realBattleId = $battle['real_battle_id'];
        // battle_events
        $this->getBattleEventByRedis($model,$battleId);
        // battle-players
        $this->getBattlePlayersByBattleId($model,$battle);
        // static(teams)
        $this->getBattleTeams($model,$battle);

        return [
            'order'         => @intval($battle['order']),
            'base'          => self::$battleFieldConfig,
            'static'        => self::$teamsFieldArray,
            'players'       => self::$battlePlayersFieldArray,
            'detail'        => self::$battleDetailFieldConfig,
            'events'        => self::$roundEventsFieldArray
        ];
    }


    /**
     * set match_battle_ext_dota2
     * @param $model
     * @param $battle
     */
    public function setDotaBattleDetails($model,$battle)
    {
        $banPickList = Dota2SocketEventService::getBanPickList($model,$battle['battle_id']);
        $banPickArr = [];
        foreach ($banPickList as $k=>$v){
            $vArr = json_decode($v,true);
            $banPickArr[$k]['hero'] =$vArr['hero']['hero_id'];
            $banPickArr[$k]['team'] =$vArr['team']['team_id'];
            $banPickArr[$k]['type'] =$vArr['event_type']=='hero_picked'?2:1;
            $banPickArr[$k]['order'] =$vArr['bp_order'];
        }

        if (!empty($battle['first_blood'])){
            $battle['first_blood_p_tid'] = $battle['first_blood']['team_id'];
            $battle['first_blood_time'] = $battle['first_blood']['ingame_timestamp'];
            $battle['first_blood_details'] = json_encode($battle['first_blood']['details'],true);
        }

        if (!empty($battle['first_to_5_kills'])){
            $battle['first_to_5_kills_p_tid'] = $battle['first_to_5_kills']['team_id'];
            $battle['first_to_5_kills_time'] = $battle['first_to_5_kills']['ingame_timestamp'];
            $battle['first_to_5_kills_details'] = json_encode($battle['first_to_5_kills']['details'],true);
        }

        if (!empty($battle['first_to_10_kills'])){
            $battle['first_to_10_kills_p_tid'] = $battle['first_to_10_kills']['team_id'];
            $battle['first_to_10_kills_time'] = $battle['first_to_10_kills']['ingame_timestamp'];
            $battle['first_to_10_kills'] = json_encode($battle['first_to_10_kills']['details'],true);
        }

        if (!empty($battle['first_barrack'])){
            $battle['first_barracks_p_tid'] = $battle['first_barrack']['team_id'];
            $battle['first_barracks_time'] = $battle['first_barrack']['ingame_timestamp'];
            $battle['first_barracks'] = json_encode($battle['first_barrack']['details'],true);
        }

        if (!empty($battle['first_tower'])){
            $battle['first_tower_p_tid'] = $battle['first_tower']['team_id'];
            $battle['first_tower_time'] = $battle['first_tower']['ingame_timestamp'];
            $battle['first_tower'] = json_encode($battle['first_tower']['details'],true);
        }
        if (!empty($battle['first_roshan'])){
            $battle['first_roshan_p_tid'] = $battle['first_roshan']['team_id'];
            $battle['first_roshan_time'] = $battle['first_roshan']['ingame_timestamp'];
            $battle['first_roshan'] = json_encode($battle['first_roshan']['details'],true);
        }
        //时间线
        $net_worth_diff_timeline_key  = ['history','battle',$battle['battle_id'],'net_worth_diff_timeline'];
        $experience_diff_timeline_key  = ['history','battle',$battle['battle_id'],'experience_diff_timeline'];
        $net_worth_diff_timeline = $model->getValue($net_worth_diff_timeline_key);
        $experience_diff_timeline = $model->getValue($experience_diff_timeline_key);
        //battle detail
        self::$battleDetailFieldConfig = [
            'is_confirmed'             => 1,
            'is_finished'              => @$battle['status'] == 3 ? 1 : 2,
            'is_pause'                 => @$battle['is_pause'] ? 1 : 2,
            'is_live'                  => @$battle['is_live'] ? 1 : 2,
            'ban_pick'                 => $banPickArr,
            'first_blood_p_tid'        => $battle['first_blood_p_tid'],
            'first_blood_time'         => $battle['first_blood_time'],
            'first_to_5_kills_p_tid'   => $battle['first_to_5_kills_p_tid'],
            'first_to_5_kills_time'    => $battle['first_to_5_kills_time'],
            'first_to_10_kills_p_tid'  => $battle['first_to_10_kills_p_tid'],
            'first_to_10_kills_time'   => $battle['first_to_10_kills_time'],
            'first_tower_p_tid'        => $battle['first_tower_p_tid'],
            'first_tower_time'         => $battle['first_tower_time'],
            'first_barracks_p_tid'     => $battle['first_barracks_p_tid'],
            'first_barracks_time'      => $battle['first_barracks_time'],
            'first_roshan_p_tid'       => $battle['first_roshan_p_tid'],
            'first_roshan_time'        => $battle['first_roshan_time'],
            'net_worth_diff_timeline'  => $net_worth_diff_timeline,
            'experience_diff_timeline' => $experience_diff_timeline,
            'time_of_day'              => $battle['time_of_day'],
            'elites_status'            => @json_encode($battle['elites_status'],320),
            'first_blood_details'      => $battle['first_blood_details'],
            'first_to_5_kills_details' => $battle['first_to_5_kills_details'],
            'first_to_10_kills'        => $battle['first_to_10_kills'],
            'first_tower'              => $battle['first_tower'],
            'first_barracks'           => $battle['first_barracks'],
            'first_roshan'             => $battle['first_roshan'],
        ];
    }


    public function getBattleTeams($model,$battle)
    {
        $teamList = Dota2SocketTeamService::getHistoryBattleTeamByBattleId($model,$battle['battle_id']);
        $teamArr                             = [];
        foreach ($teamList as $k=>$v){
            $teamOrder = $v['order'];
            $teamArr[$teamOrder]['order'] = $teamOrder;
            $teamArr[$teamOrder]['battle_id'] = $battle['battle_id'];
            $teamArr[$teamOrder]['team_id'] = (int)$v['team_id'];
            $teamArr[$teamOrder]['score'] = (int)$v['score'];

            $teamArr[$teamOrder]['faction'] = $v['faction'];
            $teamArr[$teamOrder]['kills'] = $v['kills'];
            $teamArr[$teamOrder]['net_worth'] = $v['net_worth'];
            $teamArr[$teamOrder]['gold_earned'] = $v['gold_earned'];
            $teamArr[$teamOrder]['experience'] = $v['experience'];
            $teamArr[$teamOrder]['tower_kills'] = $v['tower_kills'];
            $teamArr[$teamOrder]['melee_barrack_kills'] = $v['melee_barrack_kills'];
            $teamArr[$teamOrder]['ranged_barrack_kills'] = $v['ranged_barrack_kills'];
            $teamArr[$teamOrder]['roshan_kills'] = $v['roshan_kills'];
            $teamArr[$teamOrder]['building_status'] = @json_encode($v['building_status'],320);
            $teamArr[$teamOrder]['building_status_towers'] = $v['building_status_towers'];
            $teamArr[$teamOrder]['building_status_barracks'] = $v['building_status_barracks'];
            $teamArr[$teamOrder]['building_status_ancient'] = $v['building_status_ancient'];
            $teamArr[$teamOrder]['deaths'] = $v['deaths'];
            $teamArr[$teamOrder]['assists'] = $v['assists'];
            $teamArr[$teamOrder]['net_worth_diff'] = $v['net_worth_diff'];
            $teamArr[$teamOrder]['experience_diff'] = $v['experience_diff'];
            $teamArr[$teamOrder]['barrack_kills'] = $v['barrack_kills'];
            $teamArr[$teamOrder]['building_status_outposts'] = $v['building_status_outposts'];
            $teamArr[$teamOrder]['total_runes_pickedup'] = $v['total_runes_pickedup'];
            $teamArr[$teamOrder]['bounty_runes_pickedup'] = $v['bounty_runes_pickedup'];
            $teamArr[$teamOrder]['double_damage_runes_pickedup'] = $v['double_damage_runes_pickedup'];
            $teamArr[$teamOrder]['haste_runes_pickedup'] = $v['haste_runes_pickedup'];
            $teamArr[$teamOrder]['illusion_runes_pickedup'] = $v['illusion_runes_pickedup'];
            $teamArr[$teamOrder]['invisibility_runes_pickedup'] = $v['invisibility_runes_pickedup'];
            $teamArr[$teamOrder]['regeneration_runes_pickedup'] = $v['regeneration_runes_pickedup'];
            $teamArr[$teamOrder]['arcane_runes_pickedup'] = $v['arcane_runes_pickedup'];
            $teamArr[$teamOrder]['smoke_purchased'] = $v['smoke_purchased'];
            $teamArr[$teamOrder]['smoke_used'] = $v['smoke_used'];
            $teamArr[$teamOrder]['dust_purchased'] = $v['dust_purchased'];
            $teamArr[$teamOrder]['dust_used'] = $v['dust_used'];
            $teamArr[$teamOrder]['observer_wards_purchased'] = $v['observer_wards_purchased'];
            $teamArr[$teamOrder]['observer_wards_placed'] = $v['observer_wards_placed'];
            $teamArr[$teamOrder]['observer_wards_kills'] = $v['observer_wards_kills'];
            $teamArr[$teamOrder]['sentry_wards_purchased'] = $v['sentry_wards_purchased'];
            $teamArr[$teamOrder]['sentry_wards_placed'] = $v['sentry_wards_placed'];
            $teamArr[$teamOrder]['sentry_wards_kills'] = $v['sentry_wards_kills'];
        }
        self::$teamsFieldArray = $teamArr;
    }

    public function getBattlePlayersByBattleId($model,$battle)
    {
        $battleId        = $battle['battle_id'];
        $realBattleOrder = @$battle['order'];
        $playerList=Dota2SocketPlayerService::getPlayerList($model);
        $playerItemsTimeLineList = Dota2SocketPlayerService::getPlayerTimeLineInfoList($model,$battleId,'items_timeline');
        $playerAbilitiesTimeLineList = Dota2SocketPlayerService::getPlayerTimeLineInfoList($model,$battleId,'abilities_timeline');
        $i               = 0;
        foreach ($playerList as $k => $vJosn) {
            $v = json_decode($vJosn,true);
            $i++;
            $playerArr                                      = [];
            $playerArr['game']                              = self::DOTA_GAME_ID;
            $playerArr['order']                             = $i;
            $playerArr['rel_identity_id']                   = $k;
            $playerArr['rel_team_id']                       = $v['rel_team_id'];
            $playerArr['team_order']                        = @$v['team_order'];
            $playerArr['seed']                              = @$v['seed'];
            $playerArr['battle_id']                         = @$battleId;
            $playerArr['match']                         = @$model->matchId;
            $playerArr['team_id']                           = @$v['team_id'];
            $playerArr['faction']                           = @$v['faction'];
            $playerArr['role']                              = @$v['role'];
            $playerArr['lane']                              = @$v['lane'];
            $playerArr['player_id']                         = @$v['player_id'];
            $playerArr['nick_name']                         = @$v['nick_name'];
            $playerArr['rel_nick_name']                     = @$v['rel_nick_name'];
            $playerArr['hero']                              = @$v['hero'];
            $playerArr['level']                             = @$v['level'];
            $playerArr['is_alive']                          = @$v['is_alive']?1:2;
            $playerArr['ultimate_cd']                       = @$v['ultimate_cd'];
            $playerArr['coordinate']                        = @$v['coordinate'];
            $playerArr['kills']                             = @$v['kills'];
            $playerArr['double_kill']                       = @$v['double_kill'];
            $playerArr['triple_kill']                       = @$v['triple_kill'];
            $playerArr['ultra_kill']                        = @$v['ultra_kill'];
            $playerArr['rampage']                           = @$v['rampage'];
            $playerArr['largest_multi_kill']                = @$v['largest_multi_kill'];
            $playerArr['largest_killing_spree']             = @$v['largest_killing_spree'];
            $playerArr['deaths']                            = @$v['deaths'];
            $playerArr['assists']                           = @$v['assists'];
            $playerArr['kda']                               = @$v['kda'];
            $playerArr['participation']                     = @$v['participation'];
            $playerArr['last_hits']                         = @$v['last_hits'];
            $playerArr['lane_creep_kills']                  = @$v['lane_creep_kills'];
            $playerArr['neutral_creep_kills']               = @$v['neutral_creep_kills'];
            $playerArr['neutral_minion_team_jungle_kills']  = @$v['neutral_minion_team_jungle_kills'];
            $playerArr['neutral_minion_enemy_jungle_kills'] = @$v['neutral_minion_enemy_jungle_kills'];
            $playerArr['lhpm']                              = @$v['lhpm'];
            $playerArr['denies']                            = @$v['denies'];
            $playerArr['net_worth']                         = @$v['net_worth'];
            $playerArr['gold_earned']                       = @$v['gold_earned'];
            $playerArr['gold_spent']                        = @$v['gold_spent'];
            $playerArr['gold_remaining']                    = @$v['gold_remaining'];
            $playerArr['gpm']                               = @$v['gpm'];
            $playerArr['gold_earned_percent']               = @$v['gold_earned_percent'];
            $playerArr['experience']                        = @$v['experience'];
            $playerArr['xpm']                               = @$v['xpm'];
            $playerArr['damage_to_heroes']                  = @$v['damage_to_heroes'];
            $playerArr['dpm_to_heroes']                     = @$v['dpm_to_heroes'];
            $playerArr['damage_percent_to_heroes']          = @$v['damage_percent_to_heroes'];
            $playerArr['damage_taken']                      = @$v['damage_taken'];
            $playerArr['dtpm']                              = @$v['dtpm'];
            $playerArr['damage_taken_percent']              = @$v['damage_taken_percent'];
            $playerArr['damage_to_heroes_hp_removal']       = @$v['damage_to_heroes_hp_removal'];
            $playerArr['damage_to_heroes_magical']          = @$v['damage_to_heroes_magical'];
            $playerArr['damage_to_heroes_physical']         = @$v['damage_to_heroes_physical'];
            $playerArr['damage_to_heroes_pure']             = @$v['damage_to_heroes_pure'];
            $playerArr['damage_by_mobs_hp_removal']         = @$v['damage_by_mobs_hp_removal'];
            $playerArr['damage_by_mobs_magical_dmg']        = @$v['damage_by_mobs_magical_dmg'];
            $playerArr['damage_by_mobs_physical_dmg']       = @$v['damage_by_mobs_physical_dmg'];
            $playerArr['damage_by_mobs_pure_dmg']           = @$v['damage_by_mobs_pure_dmg'];
            $playerArr['damage_to_towers']                  = @$v['damage_to_towers'];
            $playerArr['damage_from_heroes_hp_removal']     = @$v['damage_from_heroes_hp_removal'];
            $playerArr['damage_from_heroes_magical_dmg']    = @$v['damage_from_heroes_magical_dmg'];
            $playerArr['damage_from_heroes_physical_dmg']   = @$v['damage_from_heroes_physical_dmg'];
            $playerArr['damage_from_heroes_pure_dmg']       = @$v['damage_from_heroes_pure_dmg'];
            $playerArr['damage_from_mobs_hp_removal']       = @$v['damage_from_mobs_hp_removal'];
            $playerArr['damage_from_mobs_magical_dmg']      = @$v['damage_from_mobs_magical_dmg'];
            $playerArr['damage_from_mobs_physical_dmg']     = @$v['damage_from_mobs_physical_dmg'];
            $playerArr['damage_from_mobs_pure_dmg']         = @$v['damage_from_mobs_pure_dmg'];
            $playerArr['damage_conversion_rate']            = @$v['damage_conversion_rate'];
            $playerArr['total_heal']                        = @$v['total_heal'];
            $playerArr['total_crowd_control_time']          = @$v['total_crowd_control_time'];
            $playerArr['wards_purchased']                   = @$v['wards_purchased'];
            $playerArr['wards_placed']                      = @$v['wards_placed'];
            $playerArr['wards_kills']                       = @$v['wards_kills'];
            $playerArr['observer_wards_purchased']          = @$v['observer_wards_purchased'];
            $playerArr['observer_wards_placed']             = @$v['observer_wards_placed'];
            $playerArr['observer_wards_kills']              = @$v['observer_wards_kills'];
            $playerArr['sentry_wards_purchased']            = @$v['sentry_wards_purchased'];
            $playerArr['sentry_wards_placed']               = @$v['sentry_wards_placed'];
            $playerArr['sentry_wards_kills']                = @$v['sentry_wards_kills'];
            $playerArr['camps_stacked']                     = @$v['camps_stacked'];
            $playerArr['runes_detail_double_damage_runes']  = @$v['runes_detail_double_damage_runes'];
            $playerArr['runes_detail_haste_runes']          = @$v['runes_detail_haste_runes'];
            $playerArr['runes_detail_illusion_runes']       = @$v['runes_detail_illusion_runes'];
            $playerArr['runes_detail_invisibility_runes']   = @$v['runes_detail_invisibility_runes'];
            $playerArr['runes_detail_regeneration_runes']   = @$v['runes_detail_regeneration_runes'];
            $playerArr['runes_detail_bounty_runes']         = @$v['runes_detail_bounty_runes'];
            $playerArr['runes_detail_arcane_runes']         = @$v['runes_detail_arcane_runes'];
            $playerItems = @$v['items'];
            $items_info = null;
            if ($playerItems){
                $items_modified_res = [];
                foreach ($playerItems as $key => &$value){
                    if (in_array($key,['inventory','backpack','stash','mob_inventory','mob_backpack'])){
                        foreach ($value as &$val){
                            if ($val){
                                unset($val['name']);
                                unset($val['name_cn']);
                                unset($val['external_id']);
                                unset($val['external_name']);
                                unset($val['total_cost']);
                                unset($val['is_recipe']);
                                unset($val['is_secret_shop']);
                                unset($val['is_home_shop']);
                                unset($val['is_neutral_drop']);
                                unset($val['slug']);
                                unset($val['image']);
                            }
                        }
                    }
                    if (in_array($key,['neutral','buffs','mob_neutral'])){
                        if ($value){
                            unset($value['name']);
                            unset($value['name_cn']);
                            unset($value['external_id']);
                            unset($value['external_name']);
                            unset($value['total_cost']);
                            unset($value['is_recipe']);
                            unset($value['is_secret_shop']);
                            unset($value['is_home_shop']);
                            unset($value['is_neutral_drop']);
                            unset($value['slug']);
                            unset($value['image']);
                        }
                    }
                    $items_modified_res[$key] = $value;
                }
                if ($items_modified_res){
                    $items_info = $items_modified_res;
                }
            }
            $playerArr['items']                             = @json_encode($items_info,320);
            $playerArr['abilities_timeline']                = @$playerAbilitiesTimeLineList[$v['rel_identity_id']];
            $playerArr['items_timeline']                    = @$playerItemsTimeLineList[$v['rel_identity_id']];

            //talents
            if (empty(@$v['talents'])){
                $player_talents = null;
            }else{
                $player_talents = @json_encode($v['talents'],320);
            }
            $playerArr['talents']                      = $player_talents;
            $playerArr['is_visible']                   = @$v['is_visible']?1:2;
            $playerArr['respawntimer']                 = @$v['respawntimer'];
            $playerArr['buyback_cooldown']             = null;//@(int)$v['buyback_cooldown'];
            $playerArr['buyback_cost']                 = @$v['buyback_cost'];
            $playerArr['health']                       = @$v['health'];
            $playerArr['health_max']                   = @$v['health_max'];
            $playerArr['tower_kills']                  = @$v['tower_kills'];
            $playerArr['barrack_kills']                = @$v['barrack_kills'];
            $playerArr['melee_barrack_kills']          = @$v['melee_barrack_kills'];
            $playerArr['ranged_barrack_kills']         = @$v['ranged_barrack_kills'];
            $playerArr['roshan_kills']                 = @$v['roshan_kills'];
            $playerArr['net_worth_percent']            = @(string)$v['net_worth_percent'];
            $playerArr['gold_reliable']                = @$v['gold_reliable'];
            $playerArr['gold_unreliable']              = @$v['gold_unreliable'];
            $playerArr['gold_herokill']                = @$v['gold_herokill'];
            $playerArr['gold_creepkill']               = @$v['gold_creepkill'];
            $playerArr['damage_taken_hp_removal']      = @$v['damage_taken_hp_removal'];
            $playerArr['damage_taken_magical']         = @$v['damage_taken_magical'];
            $playerArr['damage_taken_physical']        = @$v['damage_taken_physical'];
            $playerArr['damage_taken_pure']            = @$v['damage_taken_pure'];
            $playerArr['damage_to_buildings']          = @$v['damage_to_buildings'];
            $playerArr['damage_to_objectives']         = @$v['damage_to_objectives'];
            $playerArr['total_runes_pickedup']         = @$v['total_runes_pickedup'];
            $playerArr['bounty_runes_pickedup']        = @$v['bounty_runes_pickedup'];
            $playerArr['double_damage_runes_pickedup'] = @$v['double_damage_runes_pickedup'];
            $playerArr['haste_runes_pickedup']         = @$v['haste_runes_pickedup'];
            $playerArr['illusion_runes_pickedup']      = @$v['illusion_runes_pickedup'];
            $playerArr['invisibility_runes_pickedup']  = @$v['invisibility_runes_pickedup'];
            $playerArr['regeneration_runes_pickedup']  = @$v['regeneration_runes_pickedup'];
            $playerArr['arcane_runes_pickedup']        = @$v['arcane_runes_pickedup'];
            $playerArr['smoke_purchased']              = @$v['smoke_purchased'];
            $playerArr['smoke_used']                   = @$v['smoke_used'];
            $playerArr['dust_purchased']               = @$v['dust_purchased'];
            $playerArr['dust_used']                    = @$v['dust_used'];
            $playerArr['steam_id']                     = @$v['steam_id'];

            self::$battlePlayersFieldArray[]    = $playerArr;
        }
    }

    public function getEventByBattleId($model,$battleId)
    {
        $events = Dota2SocketEventService::getBattleEventByBattleId($model,$battleId);
        return $events;
    }

    public function getBattleEventByRedis($model,$battleId)
    {
        $events = $this->getEventByBattleId($model,$battleId);
        $tableFirstEventsNew = [];
        $tableEnumFirstEvents = EnumFirstEvents::find()->where(['game_id'=>3])->asArray()->all();
        foreach ($tableEnumFirstEvents as $k=>$v){
            $tableFirstEventsNew[$v['first_event']]=$v['id'];
        }
        $order =0;
        foreach ($events as $k => $v) {
            if (!empty($v)) {
                $vArr = json_decode($v,true);
                $eventArr=[
                    'battle_start',
                    'battle_end',
                    'battle_pause',
                    'battle_unpause',
                    'elite_spawned',
                    'player_spawned',
                    'player_kill',
                    'player_suicide',
                    'player_denied',
                    'building_kill',
                    'building_denied',
                    'building_captured',
                    'elite_kill',
                    'ward_placed',
                    'ward_kill',
                    'ward_expired',
                    'courier_kill',
                    'special_ability_used',
                    'smoke_used',
                    'smoke_broke',
                    'rune_spawned',
                    'rune_picked_up',
                    'item_dropped',
                    'item_picked_up',
                    'item_denied',
                    ];
                if (!in_array($vArr['event_type'],$eventArr)){
                    continue;
                }
                $order++;
                $newArr = [];
                $newArr['game'] = 3;
                $newArr['match'] = $this->matchId;
                $newArr['ingame_timestamp'] = $vArr['ingame_timestamp'];
                $newArr['event_id']         = $vArr['event_id'];
                $newArr['event_type']       = $vArr['event_type'];
                $newArr['order']       = $order;
                switch ($vArr['event_type']){//TODO
                    case  'battle_start':
                        $newArr['killer'] = $vArr['radiant']['team_id'];
                        $newArr['victim'] = $vArr['dire']['team_id'];
                        break;
                    case  'elite_spawned':
                        $dbId = 27;
                        $newArr['killer_ingame_obj_type'] = $dbId;
                        $newArr['killer_sub_type'] = $dbId;
                        break;
                    case 'player_spawned':
                        $dbId = 56;
                        $newArr['killer'] = $vArr['player']['player_id'];
                        $newArr['killer'] = $vArr['player']['player_id'];
                        $newArr['killer_player_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] =$vArr['player']['nick_name'];
                        $newArr['killer_champion_id'] = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction'] = $vArr['player']['faction'];
                        $newArr['killer_ingame_obj_type'] = $dbId;
                        $newArr['killer_sub_type'] = $dbId;
                        $newArr['killer_steam_id'] = $vArr['player']['steam_id'];
                        $newArr['killer_position'] = $vArr['position'];
                        $newArr['position'] = $vArr['position'];
                        break;
                    case 'player_suicide':
                        if ($vArr['killer']['ingame_obj_type'] == 'player') {
                            $killerDbId = 56;
                            $dbId = $vArr['killer']['player_id'];
                        } elseif ($vArr['killer']['ingame_obj_type']  == 'faction') {
                            $killerDbId = $dbId = 58;
                        }elseif ($vArr['killer']['ingame_obj_type']  == 'elite') {
                            $killerDbId = $dbId = 27;
                        }elseif ($vArr['killer']['ingame_obj_type']  == 'neutral_creep') {
                            $killerDbId = $dbId = 29;
                        } else {
                            $killerDbId = $dbId = 57;
                        }
                        $victimdbId = 56;
                        $newArr['killer'] = $dbId;
                        $newArr['killer_player_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] =$vArr['killer']['nick_name'];
                        $newArr['killer_champion_id'] = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction'] = $vArr['killer']['faction'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type'] = $killerDbId;
                        $newArr['killer_steam_id'] = $vArr['killer']['steam_id'];
                        $newArr['killer_position'] = $vArr['killer_position'];

                        $newArr['victim'] = $vArr['victim']['player_id'];
                        $newArr['victim_player_name'] = $vArr['victim']['nick_name'];
                        $newArr['victim_player_rel_name'] =$vArr['victim']['nick_name'];
                        $newArr['victim_champion_id'] = $vArr['victim']['hero']['hero_id'];
                        $newArr['victim_faction'] = $vArr['victim']['faction'];
                        $newArr['victim_ingame_obj_type'] = $victimdbId;
                        $newArr['victim_sub_type'] = $victimdbId;
                        $newArr['victim_steam_id'] = $vArr['victim']['steam_id'];
                        $newArr['victim_position'] = $vArr['victim_position'];
                        $newArr['is_first_event'] = $vArr['is_first_event']?1:2;
                        $newArr['first_event_type'] = $vArr['first_event_type']?$tableFirstEventsNew[$vArr['first_event_type']]:null;
                        break;
                    case 'player_kill':
                    case 'player_denied':
                        $dbId = 56;
                        $newArr['killer'] = $vArr['killer']['player_id'];
                        $newArr['killer_player_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] =$vArr['killer']['nick_name'];
                        $newArr['killer_champion_id'] = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction'] = $vArr['killer']['faction'];
                        $newArr['killer_ingame_obj_type'] = $dbId;
                        $newArr['killer_sub_type'] = $dbId;
                        $newArr['killer_steam_id'] = $vArr['killer']['steam_id'];
                        $newArr['killer_position'] = $vArr['killer_position'];

                        $newArr['victim'] = $vArr['victim']['player_id'];
                        $newArr['victim_player_name'] = $vArr['victim']['nick_name'];
                        $newArr['victim_player_rel_name'] =$vArr['victim']['nick_name'];
                        $newArr['victim_champion_id'] = $vArr['victim']['hero']['hero_id'];
                        $newArr['victim_faction'] = $vArr['victim']['faction'];
                        $newArr['victim_ingame_obj_type'] = $dbId;
                        $newArr['victim_sub_type'] = $dbId;
                        $newArr['victim_steam_id'] = $vArr['victim']['steam_id'];
                        $newArr['victim_position'] = $vArr['victim_position'];
                        $newArr['is_first_event'] = $vArr['is_first_event']?1:2;
                        $newArr['first_event_type'] = $vArr['first_event_type']?$tableFirstEventsNew[$vArr['first_event_type']]:null;
                        break;

                    case 'building_captured':
                        if ($vArr['capturer']['ingame_obj_type'] == 'faction'){
                            $killerDbId = 58;
                        }else{
                            $killerDbId = 57;
                        }
                        $killer_faction = @$vArr['capturer']['faction'];
                        $victim_faction = @$vArr['victim']['faction'];
                        $victimDbId = 61;
                        $newArr['killer'] = $killerDbId;
                        $newArr['killer_faction'] = $killer_faction;
                        $newArr['killer_steam_id'] = $vArr['killer']['steam_id'];
                        $newArr['killer_position'] = $vArr['capturer_position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type'] = $killerDbId;
                        $newArr['victim'] = $victimDbId;
                        $newArr['victim_faction'] = $victim_faction;
                        $newArr['victim_ingame_obj_type'] = $victimDbId;
                        $newArr['victim_sub_type'] = $victimDbId;
                        $newArr['victim_position'] = @$vArr['victim_position'];
                        $newArr['is_first_event'] = 2;
                        $newArr['first_event_type'] = null;
                        break;
                    case 'building_denied':
                    case 'building_kill':
                        if ($vArr['killer']['ingame_obj_type'] == 'player') {
                            $killerDbId = 56;
                        } elseif ($vArr['killer']['ingame_obj_type']  == 'faction') {
                            $killerDbId = 58;
                        } else {
                            $killerDbId = 57;
                        }
//                        $where = ['lane'=>$vArr['victim']['lane'],'ingame_obj_type'=>$vArr['victim']['ingame_obj_type'],'sub_type'=>$vArr['victim']['ingame_obj_sub_type']];
//                        $victimDbEnum = EnumIngameGoal::find()->where($where)->asArray()->one();
                        $victimDbId = @$vArr['victim']['ingame_id']?@$vArr['victim']['ingame_id']:57;

                        $newArr['killer'] = $vArr['killer']['player_id'];
                        $newArr['killer_player_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_champion_id'] = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction'] = $vArr['killer']['faction'];
                        $newArr['killer_steam_id'] = $vArr['killer']['steam_id'];
                        $newArr['killer_position'] = $vArr['killer_position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type'] = $killerDbId;
                        $newArr['victim_ingame_obj_type'] = $victimDbId;
                        $newArr['victim_sub_type'] = $victimDbId;
                        $newArr['victim_faction'] = $vArr['victim']['faction'];
                        $newArr['victim_position'] = $vArr['victim_position'];
                        $newArr['is_first_event'] = $vArr['is_first_event']?1:2;
                        $newArr['first_event_type'] = $vArr['first_event_type']?$tableFirstEventsNew[$vArr['first_event_type']]:null;
                        break;
                    case 'elite_kill'://击杀精英怪
                        if ($vArr['killer']['ingame_obj_type'] == 'player') {
                            $killerDbId = 56;
                        } elseif ($vArr['killer']['ingame_obj_type'] == 'faction') {
                            $killerDbId = 58;
                        } else {
                            $killerDbId = 57;
                        }
                        $victimDbId = 27;

                        $newArr['killer']                 = $vArr['killer']['player_id'];
                        $newArr['killer_player_name']     = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['killer']['faction'];
                        $newArr['killer_steam_id']        = $vArr['killer']['steam_id'];
                        $newArr['killer_position']        = $vArr['killer_position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['victim_ingame_obj_type'] = $victimDbId;
                        $newArr['victim_sub_type']        = $victimDbId;
                        $newArr['victim_position']        = $vArr['victim_position'];
                        $newArr['is_first_event'] = $vArr['is_first_event']?1:2;
                        $newArr['first_event_type'] = $vArr['first_event_type']?$tableFirstEventsNew[$vArr['first_event_type']]:null;
                        break;
                    case 'ward_placed':
                    case 'ward_expired':
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['owner']['player_id'];
                        $newArr['killer_player_name']     = $vArr['owner']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['owner']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['owner']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['owner']['faction'];
                        $newArr['killer_steam_id']        = $vArr['owner']['steam_id'];
                        $newArr['position']                 = $vArr['position'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['ward_type']              = $vArr['ward_type'];
                        break;
                    case 'ward_kill':
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['killer']['player_id'];
                        $newArr['killer_player_name']     = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['killer']['faction'];
                        $newArr['killer_steam_id']        = $vArr['killer']['steam_id'];
                        $newArr['position']                 = $vArr['position'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['ward_type']              = $vArr['ward_type'];
                        $newArr['victim']                 = $vArr['owner']['player_id'];
                        $newArr['victim_player_name']     = $vArr['owner']['nick_name'];
                        $newArr['victim_player_rel_name'] = $vArr['owner']['nick_name'];
                        $newArr['victim_champion_id']     = $vArr['owner']['hero']['hero_id'];
                        $newArr['victim_faction']         = $vArr['owner']['faction'];
                        $newArr['victim_ingame_obj_type'] = $killerDbId;
                        $newArr['victim_sub_type']        = $killerDbId;
                        $newArr['victim_steam_id']        = $vArr['owner']['steam_id'];
                        break;
                    case 'courier_kill'://信使击杀
                        $dbId                             = 56;
                        $newArr['killer']                 = $vArr['killer']['player_id'];
                        $newArr['killer']                 = $vArr['killer']['player_id'];
                        $newArr['killer_player_name']     = $vArr['killer']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['killer']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['killer']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['killer']['faction'];
                        $newArr['killer_ingame_obj_type'] = $dbId;
                        $newArr['killer_sub_type']        = $dbId;
                        $newArr['killer_steam_id']        = $vArr['killer']['steam_id'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['position']               = $vArr['position'];

                        $newArr['victim']                 = $vArr['owner']['player_id'];
                        $newArr['victim']                 = $vArr['owner']['player_id'];
                        $newArr['victim_player_name']     = $vArr['owner']['nick_name'];
                        $newArr['victim_player_rel_name'] = $vArr['owner']['nick_name'];
                        $newArr['victim_champion_id']     = $vArr['owner']['hero']['hero_id'];
                        $newArr['victim_faction']         = $vArr['owner']['faction'];
                        $newArr['victim_ingame_obj_type'] = $dbId;
                        $newArr['victim_sub_type']        = $dbId;
                        $newArr['victim_steam_id']        = $vArr['owner']['steam_id'];
                        $newArr['victim_position']        = $vArr['position'];
                        $newArr['is_first_event']         = $vArr['is_first_event'] ? 1 : 2;
                        $newArr['first_event_type']       = $vArr['first_event_type'] ? $tableFirstEventsNew[$vArr['first_event_type']] : null;
                        break;
                    case 'special_ability_used'://使用特殊技能
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['player']['player_id'];
                        $newArr['killer_player_name']     = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['player']['faction'];
                        $newArr['killer_steam_id']        = $vArr['player']['steam_id'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['special_ability_name']   = @$vArr['special_ability_name'];
                        break;
                    case 'smoke_used'://开雾
                        $dbId                             = 56;
                        $newArr['killer']                 = $vArr['owner']['player_id'];
                        $newArr['killer']                 = $vArr['owner']['player_id'];
                        $newArr['killer_player_name']     = $vArr['owner']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['owner']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['owner']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['owner']['faction'];
                        $newArr['killer_ingame_obj_type'] = $dbId;
                        $newArr['killer_sub_type']        = $dbId;
                        $newArr['killer_steam_id']        = $vArr['owner']['steam_id'];

                        $newArr['coverages']                 = !empty($vArr['coverage'])?json_encode($vArr['coverage']):null;

                        break;
                    case 'smoke_broke'://破雾
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['player']['player_id'];
                        $newArr['killer_player_name']     = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['player']['faction'];
                        $newArr['killer_steam_id']        = $vArr['player']['steam_id'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        break;
                    case 'rune_spawned'://
                        $newArr['rune_name'] = $vArr['rune_name'];
                        break;
                    case 'rune_picked_up'://神符拾取（强化神符）
                        $killerDbId                       = 56;
                        $newArr['rune_name']              = $vArr['rune_name'];
                        $newArr['killer']                 = $vArr['player']['player_id'];
                        $newArr['killer_player_name']     = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['player']['faction'];
                        $newArr['killer_steam_id']        = $vArr['player']['steam_id'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        break;
                    case 'item_dropped'://道具掉落
                    case 'item_denied'://摧毁道具
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['player']['player_id'];
                        $newArr['killer_player_name']     = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['player']['faction'];
                        $newArr['killer_steam_id']        = $vArr['player']['steam_id'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['item_id']     = $vArr['item']['item_id'];
                        break;
                    case 'item_picked_up'://道具拾取
                        $killerDbId                       = 56;
                        $newArr['killer']                 = $vArr['player']['player_id'];
                        $newArr['killer_player_name']     = $vArr['player']['nick_name'];
                        $newArr['killer_player_rel_name'] = $vArr['player']['nick_name'];
                        $newArr['killer_champion_id']     = $vArr['player']['hero']['hero_id'];
                        $newArr['killer_faction']         = $vArr['player']['faction'];
                        $newArr['killer_steam_id']        = $vArr['player']['steam_id'];
                        $newArr['killer_position']        = $vArr['position'];
                        $newArr['killer_ingame_obj_type'] = $killerDbId;
                        $newArr['killer_sub_type']        = $killerDbId;
                        $newArr['item_id']     = $vArr['item']['item_id'];
                        $newArr['is_steal']     = $vArr['is_steal']?1:2;
                        break;

                }

                self::$roundEventsFieldArray[] = $newArr;
            }

        }
        return true;
    }

    public static function getEventBuildingFormat($model, $data_payload,$type,$faction)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $dbId = 30;
        //肉山
        if ($type == 'ROSHAN') {
            $dbId = 27;
        } else {
            //建筑
            $buildingType = $data_payload['buildingType'];
            $lane         = $data_payload['lane'];
            $towerTier    = $data_payload['towerTier'];
            switch ($buildingType) {
                case 'ANCIENT':
                    //基地
                    $sub_type = 'ancient';
                    $dbId     = 49;
                    break;
                case 'OUTPOST':
                    //前哨
                    $sub_type = 'outpost';
                    $dbId     = 61;
                    break;
                case 'TOWER':
                    //塔
                    $sub_type = 'tower';
                    if ($lane == 'TOP' && $towerTier == 'TIER_1') {
                        $dbId = 30;
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_2') {
                        $dbId = 31;
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_3') {
                        $dbId = 32;
                    } elseif ($lane == 'TOP' && $towerTier == 'TIER_4') {
                        $dbId = 33;
                    }
                    if ($lane == 'MID' && $towerTier == 'TIER_1') {
                        $dbId = 34;
                    } elseif ($lane == 'MID' && $towerTier == 'TIER_2') {
                        $dbId = 35;
                    } elseif ($lane == 'MID' && $towerTier == 'TIER_3') {
                        $dbId = 36;
                    }
                    if ($lane == 'BOT' && $towerTier == 'TIER_1') {
                        $dbId = 37;
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_2') {
                        $dbId = 38;
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_3') {
                        $dbId = 39;
                    } elseif ($lane == 'BOT' && $towerTier == 'TIER_4') {
                        $dbId = 40;
                    }
                    break;
                case 'BARRACKS':
                    //兵营
                    $sub_type = 'barrack';
                    if ($lane == 'TOP' && $towerTier == 'Melee') {
                        $dbId = 42;
                    } elseif ($lane == 'TOP' && $towerTier == 'Ranged') {
                        $dbId = 43;
                    }

                    if ($lane == 'MID' && $towerTier == 'Melee') {
                        $dbId = 44;
                    } elseif ($lane == 'MID' && $towerTier == 'Ranged') {
                        $dbId = 45;
                    }

                    if ($lane == 'BOT' && $towerTier == 'Melee') {
                        $dbId = 46;
                    } elseif ($lane == 'BOT' && $towerTier == 'Ranged') {
                        $dbId = 47;
                    }
                    if (!$dbId) {
                        $dbId = 48;
                    }
                    break;
                default:
                    $sub_type = null;
                    break;
            }
            if (empty($sub_type)) {
                $format['lane']                = 'unknown';
                $format['ingame_obj_name']     = 'unknown';
                $format['ingame_obj_name_cn']  = '未知';
                $format['faction']             = strtolower($data_payload['buildingFaction']);
                $format['ingame_obj_type']     = 'unknown';
                $format['ingame_obj_sub_type'] = 'unknown';
                return $format;
            }
        }
        return $dbId;
    }


    public function unSetSpecialEvent($first_details_arr, $type)
    {
        $newArr = [];
        foreach ($first_details_arr as $k => $v) {
            $vNewArr = [];
            $vArr    = json_decode($v, true);
            if ($type == 'opening_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'bomb_planted_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['player_id']           = $vArr['player_id'];
                $vNewArr['nick_name']           = $vArr['nick_name'];
                $vNewArr['steam_id']            = $vArr['steam_id'];
                $vNewArr['side']                = $vArr['side'];
                $vNewArr['position']            = $vArr['position'];
                $vNewArr['survived_players_ct'] = $vArr['survived_players_ct'];
                $vNewArr['survived_players_t']  = $vArr['survived_players_t'];
            } elseif ($type == 'knife_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'taser_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'ace_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            } elseif ($type == 'team_kill_details') {
                $vNewArr['in_round_timestamp']  = $vArr['in_round_timestamp'];
                $vNewArr['round_time']          = $vArr['round_time'];
                $vNewArr['is_bomb_planted']     = $vArr['is_bomb_planted'];
                $vNewArr['time_since_plant']    = $vArr['time_since_plant'];
                $vNewArr['killer']              = $vArr['killer'];
                $vNewArr['victim']              = $vArr['victim'];
                $vNewArr['assist']              = $vArr['assist'];
                $vNewArr['flashassist']         = $vArr['flashassist'];
                $vNewArr['weapon']              = $vArr['weapon'];
                $vNewArr['damage']              = $vArr['damage'];
                $vNewArr['hit_group']           = $vArr['hit_group'];
                $vNewArr['is_headshot']         = $vArr['is_headshot'];
                $vNewArr['special_description'] = $vArr['special_description'];
            }
            $newArr[$k] = json_encode($vNewArr, 320);
        }
        return $newArr;

    }




    // 初始化
    public function initializationBattleInfo()
    {
        self::$battlePlayersFieldArray = [];
        self::$roundPlayersFieldArray  = [];
        self::$teamsFieldArray         = [];
        self::$battleFieldConfig       = [];
        self::$battleDetailFieldConfig = [];
        self::$roundsFieldArray        = [];
        self::$roundEventsFieldArray   = [];
    }


    private static function refresh($matchId, $formatInfo)
    {
        // todo getMountedId,有可能会找不到，找不到的留空
        self::setMatch($matchId, $formatInfo['match']);
        foreach ($formatInfo['battles'] as $battle) {
            self::setBattle($matchId, $battle);
        }
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }

    private static function setBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase = MatchBattle::find()->where([
            'match' => $matchId,
            'order' => $battle['order'],
        ])->one();
        if (!$battleBase) {
            $battleBase = new MatchBattle();
            $battleBase->setAttributes([
                'match' => $matchId,
                'order' => $battle['order'],
            ]);
            $battleBase->save();
        }
        $battleId                = $battleBase['id'];
        $battle['base']['match'] = $matchId;
        BattleService::setBattleInfo($battleId, 'dota', $battle['base'], BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId, 'dota', $battle['detail'], BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId, 'dota', $battle['static'], BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId, 'dota', $battle['players'], BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId, 'dota', $battle['events'], BattleService::DATA_TYPE_EVENTS);
    }

    public static function getBattleFormat()
    {

    }


    /**
     * @return string
     */
    public function getBattles()
    {
        return $this->hGetAll([self::KEY_TAG_HISTORY, self::KEY_TAG_BATTLE, self::KEY_TAG_LIST]);
    }
    //刷API
    private static function refreshApi($matchId,$originId){
        //battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId,   RenovateService::INTERFACE_TYPE_BATTLE, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //match
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId, RenovateService::INTERFACE_TYPE_MATCH, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        return true;
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}