<?php
/**
 *
 */

namespace app\modules\task\services\wsdata\bayes;


interface SocketBattleInterface
{
    public static function getBayesBattleId($model);
}