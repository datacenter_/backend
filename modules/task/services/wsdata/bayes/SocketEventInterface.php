<?php
/**
 *
 */

namespace app\modules\task\services\wsdata\bayes;


interface SocketEventInterface
{
    public static function matchAnnounceEvent($model,$matchId, $type, $data_payload,$battleId,$matchInfo);
}