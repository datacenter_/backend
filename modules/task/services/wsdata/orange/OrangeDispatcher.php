<?php

namespace  app\modules\task\services\wsdata\orange;
use app\modules\task\services\exceptions\TaskUndefinedHandleException;
class OrangeDispatcher
{
    public static function run($tagInfo, $taskInfo)
    {
        return [];
//        $resourceType = $tagInfo['resource_type'];
        $game_id = $tagInfo['ext_type'];
        switch ($game_id) {
            case 2:     //lol
                $tasks = OrangeLolWsToApi::run($tagInfo,$taskInfo);
                break;
            default:
                return [];
                throw new TaskUndefinedHandleException('OrangeDispatcher.'.$resourceType);
        }
        return $tasks;
    }
}