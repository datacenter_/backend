<?php

namespace app\modules\task\services\wsdata\orange;

use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Redis;

class OrangeLolWs extends HotBase
{
    private static $instance;
    public $battle_order_relation_id = null;
    public $matchId = null;
    public $map = null;
    public $matchInfo = null;
    public $matchRealTimeInfo = null;
    public $rel_match_id = null;
    public $rel_order_id = null;
    public $now_battle_order = null;
    public $socket_id = null;
    public $socket_time = null;
    public $match_status = null;
    public $battleId = null;
    public $battle_order = null;
    public $team_name_id_relation = null;
    public $teams_info = null;
    public $player_name_id_relation = null;
    public $player_info = null;
    public $current = [];
    public $battles = null;
    public $memory_mode = false;
    public $match_all_champion_websocket = [];
    //其它
    public $lol_items_relation = [];
    public $lol_items_list = [];
    public $lol_item_unknown_id = null;
    public $lol_summoner_spell_name_relation_id_list = [];
    public $lol_summoner_spell_list = [];
    public $enum_ingame_goal_info = [];
    public $enum_socket_game_configure = [];
    public $operate_type = null;
    //入口
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $OrangeLolWs = self::getInstance();
        $redis = $OrangeLolWs->redis;
        //内存模式
        $OrangeLolWs->memory_mode = true;
        //解析
        $runTag = @$tagInfo['ext2_type'];
        if ($runTag == 'refresh') {
            $info = @json_decode($taskInfo['params'], true);
            $rel_match_id = @$info['rel_match_id'];
            $match_id = @$info['match_id'];
            $origin_id = @$info['origin_id'];
            $OrangeLolWs->operate_type = @$info['operate_type'];
            $OrangeLolWs->delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id);
            //查询所有
            $match_list = MatchLived::find()->where(['match_id'=>$rel_match_id])->asArray()->all();
            if ($match_list){
                foreach ($match_list as $key => $value){
                    $value['match_data'] = @json_decode($value['match_data'],true);
                    $value['socket_id'] = @$value['id'];
                    $item['params'] = @json_encode($value,320);
                    $OrangeLolWs->runWithInfo($redis,$item,'refresh');
                }
            }
            $res = 'success';
        }else{
            $res = $OrangeLolWs->runWithInfo($redis,$taskInfo,'socket');
        }
        return $res;
    }
    //正式处理
    public function runWithInfo($redis,$taskInfo,$run_type){
        //lmcmqe71c15
        $event_data_ws = null;
        //数据源ID
        $originId = 4;
        $gameId = 2;
        //获取元数据的数据源ID 拳头元数据
        $metadata_origin_id = 6;
        $info = @json_decode($taskInfo['params'], true);
        $rel_match_id = $info['match_id'];
        $rel_order_id = @$info['round'];
        $socket_id = (Int)$info['socket_id'];
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        $match_data = $info['match_data'];
        if (empty($match_data)){
            return [];
        }
        //判断是不是正式数据
        if (!isset($match_data['event_type'])){
            return 'no event_type';
        }
        $this->rel_match_id = $rel_match_id;
        $this->rel_order_id = $rel_order_id;
        $this->socket_id = $socket_id;
        $this->socket_time = $socket_time;
        //根据数据源的比赛ID 获取平台的ID  在这之后就可以使用内存中的matchId了
        $matchId = $this->matchId;
        if (empty($matchId)){
            $matchId = $this->getMatchId($redis,$originId,$rel_match_id);
        }
        //获取比赛数据
        $matchInfo = $this->getMatchInfo($redis,$matchId,$socket_time);
        if ($matchInfo == 'no_match_info'){
            return 'no_match_info';
        }
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId.":current:";
        //查看比赛是否已经结束了
        if ($this->operate_type != 'xiufu') {
            $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
            if ($match_status_check == 3) {
                return 'match is over';
            }
            $this->redis_hSet_And_Memory($redis, ['match_real_time_info'], 'status',2);
            //$this->setMatchStatus($redis, $matchId);
        }
        $battleId = $battle_order = null;
        $battleInfoMemory = $this->battle_order_relation_id;
        if ($battleInfoMemory){
            $battle_res_explode = explode('-',$battleInfoMemory);
            $battleId = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }
        if (empty($battleId) || empty($battle_order)){
            //获取battle信息
            $isSetBattleStartTime = false;
            $battle_Info = self::getBattleIdByMatchIdAndNoOrder($redis,$redis_base_name,$matchId,$rel_match_id,$rel_order_id,$socket_time,$isSetBattleStartTime);
            if (empty($battle_Info)){
                return null;
            }
            $battleId = $battle_Info['battle_id'];
            $battle_order = $battle_Info['battle_order'];
            $this->battle_order_relation_id = $battleId.'-'.$battle_order;
        }
        $websocket_data = [
            'gameId' => $gameId,
            'battleId' => $battleId,
            'rel_match_id' => $rel_match_id,
            'battle_order' => $battle_order,
            'socket_id' => $socket_id,
            'originId' => $originId,
            'metadata_origin_id' => $metadata_origin_id
        ];
        //获得事件
        $rel_event_type = @$match_data['event_type'];
        if($rel_event_type == 'KILL_ACHIEVEMENT'){//因为这个没有 sequenceIndex 所以用 event_time
            $sequenceIndex = $match_data['event_time'];
        }else{
            $sequenceIndex = $match_data['sequenceIndex'];
        }
        //查询时间是否已经处理过了
        $rel_event = $redis->hGet($redis_base_name."battles:".$battleId.":rel_events_list",$sequenceIndex);
        if ($rel_event){
            return 'already have';
        }
        //保存
        $redis->hSet($redis_base_name."battles:".$battleId.":rel_events_list",$sequenceIndex,1);
        //设置 socket录入服务器配置
        $this->setSocketGameConfigure('lol');
        //处理返回的数据
        $handle_data = [];
        //判断事件
        switch ($rel_event_type){
            case 'BAN_PICK':
                $handle_data = $this->orangeConversionBanPick($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$metadata_origin_id,$websocket_data);
                break;
            case 'GAME_INFO':
                $handle_data = $this->orangeConversionBattleStart($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$socket_id,$metadata_origin_id,$websocket_data,$socket_time);
                break;
            case 'GAME_END':
                $handle_data = $this->orangeConversionBattleEnd($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$socket_id,$socket_time);
                break;
            case 'STATS_UPDATE':
                $handle_data = $this->orangeConversionBattleFrames($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$metadata_origin_id);
                break;
            case 'SKILL_LEVEL_UP'://没有技能顺序关系不能处理
                $handle_data = $this->orangeConversionPlayerSkill($redis,$battleId,$match_data,$metadata_origin_id);
                break;
            case 'ELITE_MONSTER_KILL':
                $handle_data = $this->orangeConversionDragonKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$socket_id,$metadata_origin_id);
                break;
            case 'BUILDING_KILL':
                $handle_data = $this->orangeConversionBuildingKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$socket_id,$metadata_origin_id);
                break;
            case 'CHAMPION_KILL':
                $handle_data = $this->orangeConversionPlayerKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$socket_id,$metadata_origin_id);
                break;
            case 'ITEM_PURCHASED':
                $this->conversionPlayerHandleItems($redis,$redis_base_name,$battleId,$match_data,$metadata_origin_id);
                break;
            case 'KILL_ACHIEVEMENT'://特殊事件 多杀
                $this->conversionPlayerMultiKill($redis,$battleId,$match_data);
                break;
        }
        //不正确返回
        if (empty($handle_data)){
            return $battleId.'no-data';
        }
        $battle_end = false;
        $event_order = 0;
        if ($handle_data['type'] == 'events'){
            $socket_type = 'events';
            $event_type = $handle_data['event_data']['event_type'];
            $event_order = @$handle_data['event_order'];
            //事件的时间 单位 秒
            $ingame_timestamp = 0;
            $timestamp = @$match_data['timestamp'];
            if ($timestamp){
                $ingame_timestamp = (Int)round($timestamp/1000);
            }
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'ingame_timestamp' => (Int)$ingame_timestamp,
                'event_id' => (Int)$socket_id
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$handle_data['event_data']);
            if ($event_type == 'battle_end'){
                $battle_end = true;
            }
        }else{
            $now_time = time();
            $out_frames_time_before = $this->redis_Get_Or_Memory($redis,['out_frames_time']);
            if ($out_frames_time_before){
                $out_frames_time_cha = $now_time - $out_frames_time_before;
                if ($out_frames_time_cha < 1){
                    return 'no-handle';
                }
            }
            $this->redis_Set_And_Memory($redis,['out_frames_time'],$now_time);
            $socket_type = 'frames';
            $event_type = 'frames';
            $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
            $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
            $number_of_games = $matchInfo['number_of_games'];
            //获取地图
            $map = $this->getMapInfoByRedisOrMemory($redis,1);
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'game_rules' => (String)$game_rules,
                'match_type' => (String)$match_type,
                'number_of_games' => (Int)$number_of_games,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'map' => $map
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$handle_data['event_data']);
        }
        if ($event_data_ws_res){
            $this->refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_match_id,$battleId,$battle_order,$event_type,$event_order);
            //插入到redis
            $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
            //接下来存数据库
            $insertData = [
                'match_data' =>@json_encode($event_data_ws_res,320),
                'match_id' => $matchId,
                'game_id' => $gameId,
                'origin_id' => $originId,
                'type' => $socket_type,
                'battle_order' => $battle_order
            ];
            //当前数据库连接
            //$this->saveDataBase($insertData);
            $this->saveKafka($insertData);
            if ($battle_end){
                if ($this->operate_type != 'xiufu') {
                    $this->getFramesInfo($redis,$redis_base_name,$battleId,$battle_order,$match_data,$matchInfo,$metadata_origin_id,$gameId,$originId);
                }
                //设置battle 为空 为了产生新的battle
                $this->battle_order_relation_id = null;
                $redis->del($redis_base_name."now_battle_num");
                $redis->hSet($redis_base_name."match_battles",$battle_order,$battleId.'-'.$battle_order.'-1');
                //给redis 设置有效期
                $redis->expire($redis_base_name."battles:".$battleId.":ws_out_put_data",86400);
                $redis->expire($redis_base_name."battles:".$battleId.":event_list",86400);
            }
        }
        return $battleId;
    }
    //处理ban pick  100 蓝  200红
    private function orangeConversionBanPick($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$metadata_origin_id,$websocket_data){
        $champions_id = null;
        $action_type = $match_data['action'];
        $event_type = $bp_order = $ban_pick_order = $faction = $rel_team_id = $team = $champion = $champion_id = null;
        $diff_array = [];
        if ($action_type == 'ban'){//type:1
            //优先判断有没有pick的信息
            $this->appendBanPickOfPick($redis,$redis_base_name,$battleId,$matchInfo,$match_data,$metadata_origin_id,$websocket_data);
            $event_type = 'champion_banned';
            //原始的ban 列表
            $relBannedChampions = array_column($match_data['bannedChampions'], null, 'championID');
            //$rel_ban_redis = $redis->hKeys($redis_base_name."battles:".$battleId.":rel_ban");
            $rel_ban_redis = $this->redis_hKeys_Or_Memory($redis,['current','battles',$battleId,'rel_ban']);
            if ($rel_ban_redis){
                $bannedChampionsIds_Array = array_column($match_data['bannedChampions'], 'championID');
                $diff_array = array_diff($bannedChampionsIds_Array,$rel_ban_redis);
            }else{
                $diff_array = array_keys($relBannedChampions);
            }
            //没有变化
            if (empty($diff_array)){
                return null;
            }
            //$teams_Id_Info_Array = self::getBattleTeamsInfoFromRedis($redis,$redis_base_name,$battleId,$match_data,$matchInfo,'id_relation');
            $teams_Id_Info_Array = $this->getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,'id_relation');
            //首先获取ban pick的数量
            //$ban_pick_len = $redis->hLen($redis_base_name."battles:".$battleId.":ban_pick");
            $ban_pick_len = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
            if ($ban_pick_len){
                $ban_pick_order = $ban_pick_len + 1;
            }else{
                $ban_pick_order = 1;
            }
            //获取到diff info 英雄ID
            foreach ($diff_array as $bc_key => $bc_championID){
                $bc_item = $relBannedChampions[$bc_championID];
                if ($bc_item){
                    $rel_championID = $bc_item['championID'];
                    //$champions_id = $redis->hGet('lol:champions:relation:'.$metadata_origin_id,$rel_championID);
                    $champions_id = $redis->hGet('lol:champions:external_id_relation_list',$rel_championID);
                    if (!$champions_id){
                        //查询更为准确的信息（mysql查询）
                        $champions_id = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$rel_championID,6);
                    }
                    $rel_team_id = $bc_item['teamID'];
                    $team_id = @$teams_Id_Info_Array[$rel_team_id];
                    $teamRes = [
                        "hero" => $champions_id,
                        "team" => $team_id,
                        "type" => 1,
                        "order" => $ban_pick_order
                    ];
                    //$redis->hSet($redis_base_name."battles:".$battleId.":ban_pick",$ban_pick_order,json_encode($teamRes,320));
                    //ban_pick
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_order,json_encode($teamRes,320));
                    //插入原始的
                    //$redis->hSet($redis_base_name."battles:".$battleId.":rel_ban",$rel_championID,json_encode($bc_item,320));
                    //rel_ban
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_ban'],$rel_championID,json_encode($bc_item,320));
                    $ban_pick_order += 1;
                }
            }
        }
        if ($action_type == 'pick'){//type:2
            $event_type = 'champion_picked';
            $rel_pick = [];
            $relParticipants = array_column($match_data['participants'], null, 'championId');
            //首先循环已经选择的英雄
            foreach ($relParticipants as $rp_key => $rp_val){
                if ($rp_val['pickStatus'] == 2 && $rp_val['championId'] > 0){
                    $rel_pick[] = $rp_val['championId'];
                }
            }
            //判断有没有 已经选择好了
            if (empty($rel_pick)){
                return null;
            }
            //原始的pick 列表
            //$rel_pick_redis = $redis->hKeys($redis_base_name."battles:".$battleId.":rel_pick");
            $rel_pick_redis = $this->redis_hKeys_Or_Memory($redis,['current','battles',$battleId,'rel_pick']);
            if ($rel_pick_redis){
                $diff_array = array_diff($rel_pick,$rel_pick_redis);
            }else{
                $diff_array = $rel_pick;
            }
            //没有变化
            if (empty($diff_array)){
                return null;
            }
            //$teams_Id_Info_Array = self::getBattleTeamsInfoFromRedis($redis,$redis_base_name,$battleId,$match_data,$matchInfo,'id_relation');
            $teams_Id_Info_Array = $this->getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,'id_relation');
            //首先获取ban pick的数量
            //$ban_pick_len = $redis->hLen($redis_base_name."battles:".$battleId.":ban_pick");
            $ban_pick_len = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
            if ($ban_pick_len){
                $ban_pick_order = $ban_pick_len + 1;
            }else{
                $ban_pick_order = 1;
            }
            //获取到diff info 英雄ID
            foreach ($diff_array as $pc_key => $pc_championId){
                $pc_item = $relParticipants[$pc_championId];
                if ($pc_item){
                    $rel_championID = $pc_item['championId'];
                    //$champions_id = $redis->hGet('lol:champions:relation:'.$metadata_origin_id,$rel_championID);
                    $champions_id = $redis->hGet('lol:champions:external_id_relation_list',$rel_championID);
                    if (!$champions_id){
                        $champions_id = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$rel_championID,6);
                    }
                    $rel_team_id = $pc_item['teamId'];
                    $team_id = @$teams_Id_Info_Array[$rel_team_id];
                    $teamRes = [
                        "hero" => $champions_id,
                        "team" => $team_id,
                        "type" => 2,
                        "order" => $ban_pick_order
                    ];
//                    $redis->hSet($redis_base_name."battles:".$battleId.":ban_pick",$ban_pick_order,json_encode($teamRes,320));
                    //ban_pick
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_order,json_encode($teamRes,320));
//                    //插入原始的
//                    $redis->hSet($redis_base_name."battles:".$battleId.":rel_pick",$rel_championID,json_encode($pc_item,320));
                    //rel_pick
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_pick'],$rel_championID,json_encode($pc_item,320));
                    $ban_pick_order += 1;
                }
            }
        }
        //获取队伍信息
        //$team_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":teams",$rel_team_id);
        $team_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_team_id);
        if ($team_info_redis){
            $team_info_battle = @json_decode($team_info_redis,true);
            //获取team
            //$matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchInfo['id'].":teams_info";
            //$match_team_info_redis = $redis->hGet($matchTeamInfoRedisName,$team_info_battle['team_id']);
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_battle['team_id']);
            $team_info = @json_decode($match_team_info_redis,true);
            $team = [
                'team_id' => self::checkAndChangeInt(@$team_info_battle['team_id']),
                'name' => self::checkAndChangeString(@$team_info['name']),
                'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                'opponent_order' => self::checkAndChangeInt(@$team_info_battle['order'])
            ];
            $faction = @$team_info_battle['faction'];
        }
        //获取英雄信息
        $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id);
        //因为循环的原因 要减一
        $bp_order = $ban_pick_order - 1;
        $event_data = [
            'event_type' => (String)$event_type,
            'bp_order' => (Int)$bp_order,
            'faction' => (String)$faction,
            'team' => $team,
            'champion' => $champion
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data
        ];
        return $result;
    }
    //补充Pick
    private function appendBanPickOfPick($redis,$redis_base_name,$battleId,$matchInfo,$match_data,$metadata_origin_id,$websocket_data){
        $faction = $team = $rel_team_id = $champion = $champions_id = null;
        $event_type = 'champion_picked';
        $rel_pick = [];
        $relParticipants = array_column($match_data['participants'], null, 'championId');
        //首先循环已经选择的英雄
        foreach ($relParticipants as $rp_key => $rp_val){
            if ($rp_val['pickStatus'] == 2 && $rp_val['championId'] > 0){
                $rel_pick[] = $rp_val['championId'];
            }
        }
        //判断有没有 已经选择好了
        if (empty($rel_pick)){
            return null;
        }
        //原始的pick 列表
        $rel_pick_redis = $this->redis_hKeys_Or_Memory($redis,['current','battles',$battleId,'rel_pick']);
        if ($rel_pick_redis){
            $diff_array = array_diff($rel_pick,$rel_pick_redis);
        }else{
            $diff_array = $rel_pick;
        }
        //没有变化
        if (empty($diff_array)){
            return null;
        }
        $teams_Id_Info_Array = $this->getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,'id_relation');
        //首先获取ban pick的数量
        $ban_pick_len = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_len){
            $ban_pick_order = $ban_pick_len + 1;
        }else{
            $ban_pick_order = 1;
        }
        //获取到diff info 英雄ID
        foreach ($diff_array as $pc_key => $pc_championId){
            $pc_item = $relParticipants[$pc_championId];
            if ($pc_item){
                $rel_championID = $pc_item['championId'];
                $champions_id = $redis->hGet('lol:champions:external_id_relation_list',$rel_championID);
                if (!$champions_id){
                    $champions_id = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$rel_championID,6);
                }
                $rel_team_id = $pc_item['teamId'];
                $team_id = @$teams_Id_Info_Array[$rel_team_id];
                $teamRes = [
                    "hero" => $champions_id,
                    "team" => $team_id,
                    "type" => 2,
                    "order" => $ban_pick_order
                ];
                //ban_pick
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_order,json_encode($teamRes,320));
                //rel_pick
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_pick'],$rel_championID,json_encode($pc_item,320));
                $ban_pick_order += 1;
            }
        }
        //获取队伍信息
        $team_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_team_id);
        if ($team_info_redis){
            $team_info_battle = @json_decode($team_info_redis,true);
            //获取team
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_battle['team_id']);
            $team_info = @json_decode($match_team_info_redis,true);
            $team = [
                'team_id' => self::checkAndChangeInt(@$team_info_battle['team_id']),
                'name' => self::checkAndChangeString(@$team_info['name']),
                'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                'opponent_order' => self::checkAndChangeInt(@$team_info_battle['order'])
            ];
            $faction = @$team_info_battle['faction'];
        }
        //因为循环的原因 要减一
        $bp_order = $ban_pick_order - 1;
//        $event_data = [
//            'event_type' => (String)$event_type,
//            'bp_order' => (Int)$bp_order,
//            'faction' => (String)$faction,
//            'team' => $team,
//            'champion' => $champion
//        ];
        $event_info = [
            'bp_order' => $bp_order,
            'faction' => $faction,
            'team' => $team,
            'champions_id' => $champions_id
        ];
        $this->supplementBattleEventsToWebSocket($redis,$redis_base_name,$match_data,$matchInfo,'champion_picked',$event_info,$websocket_data);
        return true;
    }
    //获取team 列表 内存
    public function getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,$type='id_relation'){
        $teams_Id_Info_Array = [];
        //第一针没有就走else 生成
        //$teams_Info_Redis = $redis->hGetAll($redis_base_name."battles:".$battleId.":teams");
        $teams_Info_Redis = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        if ($teams_Info_Redis){
            foreach ($teams_Info_Redis as $key => $val){
                $val_array = @json_decode($val,true);
                if ($type == 'id_relation'){
                    $teams_Id_Info_Array[$key] = @$val_array['team_id'];
                }else{
                    $teams_Id_Info_Array[$key] = @$val_array;
                }
            }
        }else{
            $participants = $match_data['participants'];
            $event_type = $match_data['event_type'];
            if ($event_type == 'STATS_UPDATE'){
                //去redis 里面查询
                $team_redis_name = Consts::MATCH_ALL_INFO.':'.$matchInfo['id'].':current:battles:'.$battleId.':teams';
                $team_redis_Info = $redis->hGetAll($team_redis_name);
                if ($team_redis_Info){
                    return $team_redis_Info;
                }
                return $teams_Id_Info_Array;
            }
            //队伍信息
            //获取队伍信息
            $turrets = [
                'top_outer_turret'=> ['is_alive' => true],
                'top_inner_turret'=> ['is_alive' => true],
                'top_inhibitor_turret'=> ['is_alive' => true],
                'mid_outer_turret'=> ['is_alive' => true],
                'mid_inner_turret'=> ['is_alive' => true],
                'mid_inhibitor_turret'=> ['is_alive' => true],
                'bot_outer_turret'=> ['is_alive' => true],
                'bot_inner_turret'=> ['is_alive' => true],
                'bot_inhibitor_turret'=> ['is_alive' => true],
                'top_nexus_turret'=> ['is_alive' => true],
                'bot_nexus_turret'=> ['is_alive' => true]
            ];
            $inhibitors = [
                'top_inhibitor'=> [
                    'is_alive' => true,
                    'respawntimer' => null
                ],
                'mid_inhibitor'=> [
                    'is_alive' => true,
                    'respawntimer' => null
                ],
                'bot_inhibitor'=> [
                    'is_alive' => true,
                    'respawntimer' => null
                ]
            ];
            $nexus = [
                'is_alive' => true
            ];
            $building_status = [
                'turrets' => $turrets,
                'inhibitors' => $inhibitors,
                'nexus' => $nexus
            ];
            $faction = null;
            $rel_team = $redisTeam = [];
            $rel_team[$participants[0]['summonerName']] = $participants[0]['teamId'];
            $rel_team[$participants[5]['summonerName']] = $participants[5]['teamId'];
            //$team_name_id_relation_info = $redis->hGetAll(Consts::MATCH_ALL_INFO.":".$matchInfo['id'].":team_name_id_relation");
            $team_name_id_relation_info = $this->redis_hGetAll_Or_Memory($redis,['team_name_id_relation']);
            $team_info = [];
            //$teamNames = array_column($participants,'summonerName');
            foreach ($rel_team as $key => $item){
                if ($item == 100){
                    $faction = 'blue';
                }
                if ($item == 200){
                    $faction = 'red';
                }
                foreach ($team_name_id_relation_info as $ti => $val){
                    $reg = '/'.$ti.'/';
                    $pi_pei = preg_match($reg,$key,$v);
                    if ($pi_pei == 1){
                        //要计算的放在这里初始化
                        $team_order = self::getTeamOrderByTeamId($val,$matchInfo);
                        $redisTeam['team_id'] = $val;
                        $redisTeam['order'] = $team_order;
                        $redisTeam['faction'] = $faction;
                        $redisTeam['score'] = 0;
                        $redisTeam['building_status'] = $building_status;
                        $redisTeam['dragon_kills_detail'] = null;
                        $redisTeam['rift_herald_kills'] = 0;
                        $redisTeam['rel_identity_id'] = @$item;
                        $redisTeam['identity_id'] = @$ti;
                        $redisTeam['name'] = @$ti;
                        $team_info[$item] = @json_encode($redisTeam,320);
                        //组合数组 banpickk 用到
                        $teams_Id_Info_Array[$item] = $val;
                    }
                }
            }
            if ($team_info){
                //$redis->hMSet($redis_base_name."battles:".$battleId.":teams",$team_info);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_info);
            }
        }
        return $teams_Id_Info_Array;
    }
    //battle 开始
    private function orangeConversionBattleStart($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$event_id,$metadata_origin_id,$websocket_data,$socket_time){
        $map_id = 1;//可以做查询
        $event_type = 'battle_start';
        $matchId = $this->matchId;
        $ingame_timestamp = (Int)$match_data['event_time'];
        $event_time = (string)$match_data['event_time'];
        $gameVersion = empty($match_data['gameVersion']) ? null : (string)$match_data['gameVersion'];
        //保存gameVersion
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $redis->hSet($matchInfoRedisName,'game_version',$gameVersion);
        $this->matchInfo['game_version'] = $gameVersion;
//        $this->redis_hSet_And_Memory($redis,['base_info'],'game_version',$gameVersion);
//        //设置比赛开始时间
//        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
//        $match_begin_at = $redis->hGet($matchRealTimeInfoRedisName,'begin_at');
//        if (!$match_begin_at){
//            $redis->hSet($matchRealTimeInfoRedisName,'begin_at',$socket_time);
//        }
        //更改比赛状态 为进行中
        //$this->setMatchStatus($redis,$matchId);
        //比赛开始 更改比赛的实际开始时间
        if ($this->operate_type != 'xiufu') {
            $match_isset_time_and_status = $this->redis_hGet_Or_Memory(
                $redis,
                ['match_real_time_info'],
                'isset_time_and_status'
            );
            if (!$match_isset_time_and_status) {
                $match_real_time_info['begin_at'] = $socket_time;
                $match_real_time_info['status'] = 2;
                $match_real_time_info['isset_time_and_status'] = 'isset';
                $this->redis_hMSet_And_Memory($redis, ['match_real_time_info'], $match_real_time_info);
            }
        }
        //base
        $battlesBase['game'] = 2;
        $battlesBase['match'] = $matchId;
        $battlesBase['begin_at'] = $socket_time;
        $battlesBase['status'] = 2;
        $battlesBase['is_draw'] = 2;
        $battlesBase['is_forfeit'] = 2;
        $battlesBase['is_default_advantage'] = 2;
        $battlesBase['map'] = $map_id;
        $battlesBase['duration'] = $event_time;
        $battlesBase['is_battle_detailed'] = 1;
        $battlesBase['flag'] = 1;
        $battlesBase['deleted_at'] = null;
        $battlesBase['deleted'] = 2;
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battlesBase);
        //detail
        $battlesDetail = [
            'is_confirmed' => 1,
            'is_pause' => null,
            'is_live' => 1
        ];
        //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$battlesDetail);
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$battlesDetail);
        //事件返回数据
        //map 默认是1
        //$map = self::getMapInfoByRedis($redis,$map_id);
        $map = $this->getMapInfoByRedisOrMemory($redis,$map_id);
        $team_order_array = [];
        $team_array_events = [];
        //$matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $blue = $red = [];
        //$rel_team_info = self::getBattleTeamsInfoFromRedis($redis,$redis_base_name,$battleId,$match_data,$matchInfo,'id_relation');
        $rel_team_info = $this->getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,'id_relation');
        foreach ($rel_team_info as $key => $val){
            if ($key == 100){
                //$blue_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":teams",$key);
                $blue_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$key);
                if ($blue_info_redis){
                    $blue_info = @json_decode($blue_info_redis,true);
                    //获取team
                    //$blue_team_info_redis = $redis->hGet($matchTeamInfoRedisName,$val);
                    $blue_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$val);
                    $blue_team_info = @json_decode($blue_team_info_redis,true);
                    $blue = [
                        'team_id' => self::checkAndChangeInt($val),
                        'name' => self::checkAndChangeString(@$blue_team_info['name']),
                        'image' => self::checkAndChangeString(@$blue_team_info['image_200x200']),
                        'opponent_order' => self::checkAndChangeInt(@$blue_info['order'])
                    ];
                    $team_order_array[$key] = @$blue_info['order'];
                    $team_array_events[$key] = $blue;
                }
            }
            if ($key == 200){
                //$red_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":teams",$key);
                $red_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$key);
                if ($red_info_redis){
                    $red_info = @json_decode($red_info_redis,true);
                    //获取team
                    //$red_team_info_redis = $redis->hGet($matchTeamInfoRedisName,$val);
                    $red_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$val);
                    $red_team_info = @json_decode($red_team_info_redis,true);
                    $red = [
                        'team_id' => self::checkAndChangeInt($val),
                        'name' => self::checkAndChangeString(@$red_team_info['name']),
                        'image' => self::checkAndChangeString(@$red_team_info['image_200x200']),
                        'opponent_order' => self::checkAndChangeInt(@$red_info['order'])
                    ];
                    $team_order_array[$key] = @$red_info['order'];
                    $team_array_events[$key] = $red;
                }
            }
        }
        //处理玩家
        //$team_name_info = $redis->hKeys(Consts::MATCH_ALL_INFO.":".$matchInfo['id'].":team_name_id_relation");
        $team_name_info = $this->redis_hKeys_Or_Memory($redis,['team_name_id_relation']);
        //玩家名字和ID 关系
        $playerInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_info";
        $playerInfoRedisNameArray = $redis->hGetAll($playerInfoRedisName);
        $playerNameIdRelationRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_name_id_relation";
        $playerNameIdRelationInfo = $redis->hGetAll($playerNameIdRelationRedisName);
        //查询已经pick的英雄数量
        $pick_data_old = [];
        $pick_data_order = null;
        //$ban_pick_data_redis = $redis->hVals($redis_base_name."battles:".$battleId.":ban_pick");
        $ban_pick_data_redis = $this->redis_hVals_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_data_redis){
            $pick_data_order = count($ban_pick_data_redis);
            foreach ($ban_pick_data_redis as $ban_pick_val){
                $ban_pick_info = @json_decode($ban_pick_val,true);
                if ($ban_pick_info['type'] == 2){
                    $pick_data_old[] = $ban_pick_info['hero'];
                }
            }
        }
        //玩家英雄
        //$redisNameIdRelationArray = $redis->hGetAll("lol:champions:name_relation_id:".$metadata_origin_id);
        $redisNameIdRelationArray = $redis->hGetAll("lol:champions:external_name_relation_list");
        //符文列表
        //$relKeystoneRelationArray = $redis->hGetAll("lol:rune:relation:".$metadata_origin_id);
        $relKeystoneRelationArray = $redis->hGetAll("lol:rune:external_id_relation_list");
        //lol:rune:list:unknown
        $rune_unknown_redis = $redis->get('lol:rune:list:unknown');
        $rune_unknown_info = @json_decode($rune_unknown_redis,true);
        $rune_unknown_id = @$rune_unknown_info['id'];
        //原始数据
        $playerRes = [];
        //补充ban pick信息
        $participants = $match_data['participants'];
        foreach ($participants as $key => $item){
            $rel_player_order = $item['participantId'];
            //计算 $player_seed
            if ($rel_player_order < 6){
                $player_seed = $rel_player_order;
            }else{
                $player_seed = $rel_player_order - 5;
            }
            $rel_player_keystoneId = $item['keystoneId'];
            $rel_teamId = $item['teamId'];
            $summonerName = $item['summonerName'];
            $rel_runes_list = $item['perks'][0]['perkIds'];
            $runes_list = [];
            //循环 只要前6个
            foreach ($rel_runes_list as $key => $val){
                if ($key < 6){
                    $runes_id = @$relKeystoneRelationArray[$val];
                    if (empty($runes_id)){
                        $runes_id = $rune_unknown_id;
                    }
                    $runes_list[] = (Int)$runes_id;
                }
            }
            //去掉队员的战队名称
            $rel_nick_name = str_replace($team_name_info,'',$summonerName);
            //$rel_championName = $item['championName'];
            $champion_id = $redisNameIdRelationArray[$item['championName']];
            if (!$champion_id){
                //获取unknown
                $ChampionInfoRedis = $redis->get('lol:champions:list:unknown');
                $ChampionInfo = @json_decode($ChampionInfoRedis,true);
                $champion_id = $ChampionInfo['id'];
            }
            //必少
            if (!in_array($champion_id,$pick_data_old)){
                if ($pick_data_order){
                    $pick_data_order = $pick_data_order + 1;
                }
                $pick_data_supplement = [
                    "hero" => (Int)$champion_id,
                    "team" => $rel_team_info[$rel_teamId],
                    "type" => 2,
                    "order" => $pick_data_order
                ];
                if ($pick_data_order < 21){
                    //$redis->hSet($redis_base_name."battles:".$battleId.":ban_pick",$pick_data_order,json_encode($pick_data_supplement,320));
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$pick_data_order,@json_encode($pick_data_supplement,320));
                    //100 蓝 200 红
                    if ($rel_teamId == 100){
                        $faction = 'blue';
                    }else{
                        $faction = 'red';
                    }
                    $event_info = [
                        'bp_order' => $pick_data_order,
                        'faction' => $faction,
                        'team' => @$team_array_events[$rel_teamId],
                        'champions_id' => $champion_id
                    ];
                    $this->supplementBattleEventsToWebSocket($redis,$redis_base_name,$match_data,$matchInfo,'champion_picked',$event_info,$websocket_data);
                }
            }
            $rel_team_order = $item['teamId'];
            $team_order = $team_order_array[$rel_team_order];
            $player = [];
            $player['order'] = $rel_player_order;
            $player['rel_identity_id'] = $rel_player_order;
            $player['rel_team_id'] = null;
            $player['rel_team_order'] = $rel_team_order;
            $player['team_order'] = $team_order;
            $player['seed'] = $player_seed;
            $player['battle_id'] = $battleId;
            $player['game'] = 2;
            $player['match'] = $matchId;
            $player['team_id'] = $rel_team_info[$rel_team_order];
            if ($rel_team_order == 100){
                $faction = 'blue';
            }else{
                $faction = 'red';
            }
            $player['faction'] = $faction;
            $player['role'] = Common::getLolPositionByString('orange',$player_seed);
            $player['lane'] = Common::getLolLaneId('orange',@$player['role']);
            $player_id = null;
            $nick_name = $rel_nick_name;
            //循环匹配nick_name 找到$player_id
            foreach ($playerNameIdRelationInfo as $p_key => $p_val){
                $haystack = stristr($p_key,$rel_nick_name);
                if ($haystack){
                    $player_id = $p_val;
                    //$playerInfoRedis = $redis->hGet($playerInfoRedisName,$player_id);
                    $matchPlayerInfo = $playerInfoRedisNameArray[$player_id];
                    if ($matchPlayerInfo){
                        $nick_name = @json_decode($matchPlayerInfo,true)['nick_name'];
                    }
                }
            }
            $player['player_id'] = $player_id;
            $player['nick_name'] = $nick_name;
            $player['rel_nick_name'] = $rel_nick_name;
            $player['champion'] = $champion_id;
            $player['turret_kills'] = 0;
            $player['inhibitor_kills'] = 0;
            $player['rift_herald_kills'] = 0;
            $player['dragon_kills'] = 0;
            $player['baron_nashor_kills'] = 0;
            $player['double_kill'] = 0;
            $player['triple_kill'] = 0;
            $player['quadra_kill'] = 0;
            $player['penta_kill'] = 0;
            $player['largest_multi_kill'] = 0;
            $player['largest_killing_spree'] = 0;
            $player['abilities_timeline'] = null;
            $player['runes'] = $runes_list;
            $player['keystone'] = null;
            //符文
            //$rune_relation_id_redis = $redis->hGet("lol:rune:relation:".$metadata_origin_id,$rel_player_keystoneId);
            $keystone_relation_id = $relKeystoneRelationArray[$rel_player_keystoneId];
            if ($keystone_relation_id){
                $player['keystone'] = $keystone_relation_id;
            }else{
                $player['keystone'] = $rune_unknown_id;
            }
            $playerRes[$rel_player_order] = @json_encode($player,320);
        }
        if ($playerRes){
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerRes);
            //设置到redis
            //$redis->hSet($redis_base_name."battles:".$battleId.":players",$rel_player_order,@json_encode($player,320));
        }
        //查询出已经有的事件
        //$eventListRedisName = $redis_base_name."battles:".$battleId.":event_list";
        //$eventNum = $redis->hLen($eventListRedisName);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => @$matchInfo['id'],
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => (Int)$blue['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => (Int)$red['team_id'],
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        //$redis->hSet($eventListRedisName,$event_order,json_encode($event_data_list,320));
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data = [
            'event_type' => $event_type,
            'map' => $map,
            'blue' => $blue,
            'red' => $red,
        ];
        $result = [
            'type' => 'events',
            'event_data' => $event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //frames
    private function orangeConversionBattleFrames($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$metadata_origin_id){
        $matchId = (Int)$matchInfo['id'];
        $match_battle_duration = (Int)round($match_data['timestamp']/1000);
        $duration = $match_battle_duration > 0 ? $match_battle_duration : 1;
        $game_version = $matchInfo['game_version'];
        $server_config = [
            'version' => (String)$game_version
        ];
        //获取防御塔的个数
        $team_turret_kills = $this->redis_hGetAll_Or_Memory($redis,['current','battles',$battleId,'team_turret_kills']);
        if(!$team_turret_kills){
            $team_turret_kills = [
                'blue' => 0,
                'red' => 0
            ];
        }
        //处理队伍信息
        $team_info_new = [];
        $team_info_count = [];
        //获取队伍的原始信息
        //$battleTeamInfo_old = self::getBattleTeamsInfoFromRedis($redis,$redis_base_name,$battleId,$match_data,$matchInfo,'all_info');
        $battleTeamInfo_old = $this->getBattleTeamsInfoFromMemory($redis,$battleId,$match_data,$matchInfo,'all_info');
        if ($battleTeamInfo_old){
            $rel_teams = $match_data['teams'];
            foreach ($rel_teams as $item){
                $rel_team_faction_id = $item['teamId'];
                $battleTeamInfo = $battleTeamInfo_old[$rel_team_faction_id];
                $team_order = $battleTeamInfo['order'];
                $team_faction = $battleTeamInfo['faction'];
                //队伍信息
                $baronKills = $item['baronKills'];
                $dragonKills = $item['dragonKills'];
                $championKills = $item['championKills'];
                $deaths = $item['deaths'];
                $assists = $item['assists'];
                $totalGold = $item['totalGold'];
                $towerKills = self::checkAndChangeInt(@$team_turret_kills[$team_faction]);//$item['towerKills'];
                $inhibitorKills = $item['inhibitorKills'];
                //$rift_herald_kills = $item['rift_herald_kills'];//通过计算的不用再这里取
                //组合平台的信息
                $team_info = [
                    'battle_id' => $battleId,
                    'kills' => $championKills,
                    'deaths' => $deaths,
                    'assists' => $assists,
                    'gold' => $totalGold,
                    'gold_diff' => 0,//最下面计算
                    'experience' => 0,//选手计算
                    'experience_diff' => 0,//最下面计算
                    'turret_kills' => $towerKills,
                    'inhibitor_kills' => $inhibitorKills,
                    //'rift_herald_kills' => $rift_herald_kills,//通过计算的不用再这里取
                    'dragon_kills' => $dragonKills,
                    //'dragon_kills_detail' => null,
                    'baron_nashor_kills' => $baronKills,
                    //'building_status' => null
                ];
                $team_info_res = @array_merge($battleTeamInfo,$team_info);
                $team_info_new[$rel_team_faction_id] = $team_info_res;
                $team_info_count[$team_order] = $team_info_res;
            }
        }else{
            return [];
        }
        //首先获取所有的items
        $lolItemsList = $this->getLolItemsList($redis,$metadata_origin_id);
        //循环玩家
        $player_damage_to_champions_all1 = $player_damage_to_champions_all2 = $player_damage_taken_all1 = $player_damage_taken_all2 = 0;
        $websocket_team_players = $player_array = $player_res_array = $websocket_team_players_wards = $websocket_team_players_rift_herald_kills = [];
        $rel_participants = $match_data['participants'];
        foreach ($rel_participants as $key => $val){
            $player = [];
            $old_player = [];
            $rel_player_order = $val['participantId'];
            //$playerRedis = $redis->hGet($redis_base_name."battles:".$battleId.":players",$rel_player_order);
            $playerRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_player_order);
            if ($playerRedis){
                $old_player = @json_decode($playerRedis,true);
            }else{
                $player['order'] = $rel_player_order;
            }
            $rel_team_order = @$old_player['rel_team_order'];
            $player_id = @$old_player['player_id'];
            $player_team_order = @$old_player['team_order'];
            $player['level'] = @$val['level'];
            $player['alive'] = null;
            $player['ultimate_cd'] = null;
            $player['coordinate'] = @implode(',',$val['position']);
            $player['kills'] = @$val['kills'];
//            $player['double_kill'] = null;//通过计算的
//            $player['triple_kill'] = null;//通过计算的
//            $player['quadra_kill'] = null;//通过计算的
//            $player['penta_kill'] = null;//通过计算的
//            $player['largest_multi_kill'] = null;//通过计算的
            //$player['largest_killing_spree'] = empty(@$old_player['largest_killing_spree']) ? 0 : (int)$old_player['largest_killing_spree'];
            $player['deaths'] = $val['deaths'];
            $player['assists'] = $val['assists'];
            //计算
            $deaths = $player['deaths'] ? $player['deaths'] : 1;
            $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
            $player['participation'] = null;
            //团队击杀
            $participation_team_kills = @$team_info_count[$player_team_order]['kills'];
            if ($participation_team_kills === 0){
                $participation_team_kills = 1;
                $player['participation'] = (String)round((($player['kills'] + $player['assists'])/$participation_team_kills),4);
            }
            if ($participation_team_kills > 0){
                $player['participation'] = (String)round((($player['kills'] + $player['assists'])/$participation_team_kills),4);
            }
            $player['cs'] = $val['lasthit'];
            $player['minion_kills'] = $val['minionsKilled'];
            $player['total_neutral_minion_kills'] = $val['neutralMinionsKilled'];
            $player['neutral_minion_team_jungle_kills'] = $val['neutralMinionsKilledTeamJungle'];
            $player['neutral_minion_enemy_jungle_kills'] = $val['neutralMinionsKilledEnemyJungle'];
            $player['cspm'] = (String)round(($player['cs']/($duration/60)),2);
            $player['gold_earned'] = $val['totalGold'];
            $player['gold_spent'] = (Int)($val['totalGold'] - $val['currentGold']);
            $player['gold_remaining'] = $val['currentGold'];
            $player['gpm'] = (String)round(($player['gold_earned']/($duration/60)),2);
            $team_gold = @$team_info_count[$player_team_order]['gold'];
            $team_gold_res = $team_gold ? $team_gold : 1;
            $player['gold_earned_percent'] = (String)round(($player['gold_earned'] / $team_gold_res),4) ;
            $player['experience'] = $val['experience'];
            //计算团队经验
            $team_info_new[$rel_team_order]['experience'] += $val['experience'];
            $player['xpm'] = (String)round(($player['experience']/($duration/60)),2);
            $player['damage_to_champions']= $val['damageToChampions'];
            $player['damage_to_champions_physical']= $val['physicalDamageDealtToChampions'];
            $player['damage_to_champions_magic']= $val['magicDamageDealtToChampions'];
            $player['damage_to_champions_true']= $val['trueDamageDealtToChampions'];
            $player['dpm_to_champions'] = (String)round(($player['damage_to_champions']/($duration/60)),2);
            $player['damage_percent_to_champions'] = null;//下面计算
            $player['damage_to_towers']= (String)$val['damageToTower'];
            $player['damage_taken']= $val['damageTaken'];
            $player['damage_taken_physical']= $val['physicalDamageTaken'];
            $player['damage_taken_magic']= $val['magicDamageTaken'];
            $player['damage_taken_true']= $val['trueDamageTaken'];
            $player['dtpm'] = (String)round(($player['damage_taken']/($duration/60)),2);
            $player['damage_taken_percent'] = null;//下面计算
            $player['damage_conversion_rate'] = (String)round(($player['damage_to_champions'] / $player['gold_earned']),4);;
            $player['total_heal'] = null;
            $player['total_crowd_control_time'] = null;
            $player['wards_purchased'] = empty($old_player['wards_purchased']) ? 0 : $old_player['wards_purchased'];
            $player['wards_placed'] = null;
            $player['wards_kills'] = null;
            $player['warding_totems_purchased'] = null;
            $player['warding_totems_placed'] = null;
            $player['warding_totems_kills'] = null;
            $player['control_wards_purchased'] = null;
            $player['control_wards_placed'] = null;
            $player['control_wards_kills'] = null;
            $player['total_damage']= null;
            $player['total_damage_physical']= null;
            $player['total_damage_magic']= null;
            $player['total_damage_true']= null;
            $player_items = $this->conversionOrangePlayerItemsIdsFromRedis($redis,$redis_base_name,$match_battle_duration,$val['items'],$player_id,$rel_player_order,$battleId,$matchId,$metadata_origin_id,$lolItemsList);
            $player['items'] = @$player_items['items'];
            $player['items_timeline'] = @$player_items['items_timeline'];
            $UnknownInfo = null;
            $player_spell_list = [$val['spell1'],$val['spell2']];
            $player_summoner_spells = $this->conversionOrangePlayerSpellsIds($redis,$player_spell_list,$metadata_origin_id);
            $player['summoner_spells'] = $player_summoner_spells;
            $player['keystone'] = $old_player['keystone'];
            $player['hp'] = null;
            $player['respawntimer'] = null;
            $player['health'] = null;
            $player['health_max'] = null;
            $player['damage_selfmitigated'] = null;
            $player['damage_shielded_on_teammates'] = null;
            $player['damage_to_buildings'] = null;
            $player['damage_to_objectives'] = null;
            $player['total_crowd_control_time_others'] = null;
            $player['vision_score'] = null;
            $player_merge_info = @array_merge($old_player,$player);
            $player_array[] = $player_merge_info;
            //计算总数
            if ($player_team_order == 1) {
                $player_damage_to_champions_all1 += $player['damage_to_champions'];
                $player_damage_taken_all1 += $player['damage_taken'];
            }
            if ($player_team_order == 2) {
                $player_damage_to_champions_all2 += $player['damage_to_champions'];
                $player_damage_taken_all2 += $player['damage_taken'];
            }
        }
        $player_damage_taken_all = $player_damage_to_champions_all = 1;
        //获取所有的召唤师技能
        $summonerSpellInfoList = $this->getSummonerSpellInfoList($redis,$metadata_origin_id);
        //计算
        foreach ($player_array as $key_a => &$player_res){
            $rel_team_order = @$player_res['rel_team_order'];
            if (@$player_res['team_order'] == 1) {
                $player_damage_to_champions_all = $player_damage_to_champions_all1 == 0 ? 1 : $player_damage_to_champions_all1;
                $player_damage_taken_all = $player_damage_taken_all1 == 0 ? 1 : $player_damage_taken_all1;
            }
            if (@$player_res['team_order'] == 2) {
                $player_damage_to_champions_all = $player_damage_to_champions_all2 == 0 ? 1 : $player_damage_to_champions_all2;
                $player_damage_taken_all = $player_damage_taken_all2 == 0 ? 1 : $player_damage_taken_all2;
            }
            $player_res['damage_percent_to_champions'] = (string)round(($player_res['damage_to_champions'] / $player_damage_to_champions_all),4);
            $player_res['damage_taken_percent'] = (string)round(($player_res['damage_taken'] / $player_damage_taken_all),4);
            //组合websocket
            $websocket_player = [];
            $websocket_player['seed'] = (int)$player_res['seed'];
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = @Common::getLolRoleStringByRoleId('orange',$player_res['role']);
            $websocket_player['lane'] = @Common::getLolLaneStringByLaneId('orange',$player_res['lane']);
            $websocket_player['player']['player_id'] = $player_res['player_id'];
            $websocket_player['player']['nick_name'] = $player_res['nick_name'];
            //获取英雄信息
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$player_res['champion']);
            $websocket_player['champion'] = $champion;
            $websocket_player['level'] = (int)$player_res['level'];
//            $is_alive = false;
//            if (empty(@$player_res['alive'])){
//                $is_alive = null;
//            }else{
//                if ($player_res['alive'] == 1){
//                    $is_alive = true;
//                }
//            }
            $websocket_player['is_alive'] = null;//$is_alive
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['cs'] = (String)$player_res['cs'];
            $websocket_player['cspm'] = (String)$player_res['cspm'];
            //summoner_spells
            $websocket_player['summoner_spells'] = $this->getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_res['summoner_spells']);
            //道具
            //$websocket_player['items'] = $player_items;
            $websocket_player['items'] = $this->getWebSocketPlayerItemsList($lolItemsList,$player_res['items']);
            $keystone = $this->getWebSocketPlayerKeystoneInfo($redis,$metadata_origin_id,$player_res['player_id'],$player_res['keystone']);
            $advanced = [
                'keystone' => $keystone,
                'ultimate_cd' => null,
                'coordinate' => (String)$player_res['coordinate'],
                'respawntimer' => null,
                'health' => null,
                'health_max' => null,
                'turret_kills' => (Int)$player_res['turret_kills'],
                'inhibitor_kills' => (Int)$player_res['inhibitor_kills'],
                'rift_herald_kills' => (Int)$player_res['rift_herald_kills'],
                'dragon_kills' => (Int)$player_res['dragon_kills'],
                'baron_nashor_kills' => (Int)$player_res['baron_nashor_kills'],
                'double_kill' => (Int)$player_res['double_kill'],
                'triple_kill' => (Int)$player_res['triple_kill'],
                'quadra_kill' => (Int)$player_res['quadra_kill'],
                'penta_kill' => (Int)$player_res['penta_kill'],
                'largest_multi_kill' => (Int)$player_res['largest_multi_kill'],
                'largest_killing_spree' => (int)$player_res['largest_killing_spree'],
                'minion_kills' => (String)$player_res['minion_kills'],
                'total_neutral_minion_kills' => (String)$player_res['total_neutral_minion_kills'],
                'neutral_minion_team_jungle_kills' => (String)$player_res['neutral_minion_team_jungle_kills'],
                'neutral_minion_enemy_jungle_kills' => (String)$player_res['neutral_minion_enemy_jungle_kills'],
                'gold_earned' => (Int)$player_res['gold_earned'],
                'gold_spent' => (Int)$player_res['gold_spent'],
                'gold_remaining' => (Int)$player_res['gold_remaining'],
                'gpm' => (String)$player_res['gpm'],
                'gold_earned_percent' => (String)$player_res['gold_earned_percent'],
                'experience' => (Int)$player_res['experience'],
                'xpm' => (String)$player_res['xpm'],
                'damage_to_champions' => (String)$player_res['damage_to_champions'],
                'damage_to_champions_physical' => (String)$player_res['damage_to_champions_physical'],
                'damage_to_champions_magic' => (String)$player_res['damage_to_champions_magic'],
                'damage_to_champions_true' => (String)$player_res['damage_to_champions_true'],
                'dpm_to_champions' => (String)$player_res['dpm_to_champions'],
                'damage_percent_to_champions' => (String)$player_res['damage_percent_to_champions'],
                'total_damage' => null,
                'total_damage_physical' => null,
                'total_damage_magic' => null,
                'total_damagel_true' => null,
                'damage_taken' => (String)$player_res['damage_taken'],
                'damage_taken_physical' => (String)$player_res['damage_taken_physical'],
                'damage_taken_magic' => (String)$player_res['damage_taken_magic'],
                'damage_taken_true' => (String)$player_res['damage_taken_true'],
                'dtpm' => (String)$player_res['dtpm'],
                'damage_taken_percent' => (String)$player_res['damage_taken_percent'],
                'damage_conversion_rate' => (String)$player_res['damage_conversion_rate'],
                'damage_selfmitigated' => null,
                'damage_shielded_on_teammates' => null,
                'damage_to_buildings' => null,
                'damage_to_towers' => (String)$player_res['damage_to_towers'],
                'damage_to_objectives' => null,
                'total_crowd_control_time' => null,
                'total_crowd_control_time_others' => null,
                'total_heal' => null,
                'wards_purchased' => (Int)$player_res['wards_purchased'],
                'wards_placed' => null,
                'wards_kills' => null,
                'vision_score' => null
            ];
            $websocket_player['advanced'] = $advanced;
            $websocket_team_players[$rel_team_order][] = $websocket_player;
            $websocket_team_players_wards[$rel_team_order]['wards_purchased'] += (Int)$player_res['wards_purchased'];
            $websocket_team_players_rift_herald_kills[$rel_team_order]['rift_herald_kills'] += (Int)$player_res['rift_herald_kills'];
            //插入redis
            //$redis->hSet($redis_base_name."battles:".$battleId.":players",@$player_res['order'],@json_encode($player_res,320));
            $player_res_array[@$player_res['order']] = @json_encode($player_res,320);
        }
        //保存 players 的 Memory
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$player_res_array);
        //计算队伍的diff 信息
        //计算gold_diff  100 蓝  200红  蓝减红
        $gold_diff = $team_info_new[100]['gold'] - $team_info_new[200]['gold'];
        if ($gold_diff != 0){
            $team_info_new[100]['gold_diff'] = $gold_diff;
            $team_info_new[200]['gold_diff'] = -$gold_diff;
        }
        //计算experience_diff 100 蓝  200红  蓝减红
        $experience_diff = $team_info_new[100]['experience'] - $team_info_new[200]['experience'];
        if ($experience_diff != 0){
            $team_info_new[100]['experience_diff'] = $experience_diff;
            $team_info_new[200]['experience_diff'] = -$experience_diff;
        }
        //首先获取时间是否可以保存了
        //$gold_or_experience_diff_time = $redis->get($redis_base_name."battles:".$battleId.":gold_or_experience_diff");
        $gold_or_experience_diff_time = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff']);
        $gold_diff_time_cha = $match_battle_duration - $gold_or_experience_diff_time;
        //间隔60秒保存
        if ($gold_diff_time_cha >= 30){
            //gold_diff
            $gold_diff_timeline_now[] = [
                'ingame_timestamp' => $match_battle_duration,
                'gold_diff' => $gold_diff
            ];
            //获取原来的信息
            //$gold_diff_timeline_redis = $redis->get($redis_base_name."battles:".$battleId.":gold_diff_timeline");
            $gold_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_diff_timeline']);
            if ($gold_diff_timeline_redis){
                $gold_diff_timeline_old = @json_decode($gold_diff_timeline_redis,true);
                $gold_diff_timeline_new = @array_merge($gold_diff_timeline_old,$gold_diff_timeline_now);
            }else{
                $gold_diff_timeline_new = $gold_diff_timeline_now;
            }
            $gold_diff_timeline_res = @json_encode($gold_diff_timeline_new,320);
            //$redis->set($redis_base_name."battles:".$battleId.":gold_diff_timeline",$gold_diff_timeline_res);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_diff_timeline'],$gold_diff_timeline_res);
            //experience_diff
            $experience_diff_timeline_now[] = [
                'ingame_timestamp' => $match_battle_duration,
                'experience_diff' => $experience_diff
            ];
            //获取原来的信息
            //$experience_diff_timeline_redis = $redis->get($redis_base_name."battles:".$battleId.":experience_diff_timeline");
            $experience_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'experience_diff_timeline']);
            if ($experience_diff_timeline_redis){
                $experience_diff_timeline_old = @json_decode($experience_diff_timeline_redis,true);
                $experience_diff_timeline_new = @array_merge($experience_diff_timeline_old,$experience_diff_timeline_now);
            }else{
                $experience_diff_timeline_new = $experience_diff_timeline_now;
            }
            $experience_diff_timeline_res = @json_encode($experience_diff_timeline_new,320);
            //$redis->set($redis_base_name."battles:".$battleId.":experience_diff_timeline",$experience_diff_timeline_res);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'experience_diff_timeline'],$experience_diff_timeline_res);
            //设置时间间隔
            //$redis->set($redis_base_name."battles:".$battleId.":gold_or_experience_diff",$match_battle_duration);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff'],$match_battle_duration);
        }
        //保存 base 的 duration
        //$redis->hSet($redis_base_name."battles:".$battleId.":base",'duration',$match_battle_duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'base'],'duration',$match_battle_duration);
        //生成websocket
        //$factions
        //获取team
        //$matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchInfo['id'].":teams_info";
        //$matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
        $factions = [];
        $match_scores = [];
        //获取龙信息
        $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
        //循环队伍保存到redis
        $team_info_res = [];
        $team_elites_Kills = [];
        foreach ($team_info_new as $rel_team_order_key => $team_info){
            $team_score = 0;
            $factions[$rel_team_order_key]['faction'] = (string)@$team_info['faction'];
            //$match_team_info_redis = $redis->hGet($matchTeamInfoRedisName,$team_info['team_id']);
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt($team_info['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            //获取比赛的分数
            if($team_opponent_order == 1){
                $team_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_1_score');
                //$team_score = $redis->hGet($matchRealTimeInfoRedisName,'team_1_score');
            }
            if($team_opponent_order == 2){
                $team_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_2_score');
                //$team_score = $redis->hGet($matchRealTimeInfoRedisName,'team_2_score');
            }
            //优先设置 factions 里面 team
            $factions[$rel_team_order_key]['team'] = $teamInfo;
            //再设置外层 match_scores
            $teamInfo['score'] = (int)$team_score;
            $match_scores[] = $teamInfo;
            //处理龙的信息
            $dragon_kills_detail_res = null;
            $dragon_kills_detail = $team_info['dragon_kills_detail'];
            if ($dragon_kills_detail){
                foreach ($dragon_kills_detail as $val_d){
                    //$drakeInfoRedis = $redis->hGet('enum:ingame_goal',$val_d['drake']);
                    $drakeInfoRedis = @$drakeInfoList[$val_d['drake']];
                    if ($drakeInfoRedis){
                        $drakeInfo = @json_decode($drakeInfoRedis,true);
                        $drakeInfoRes = [
                            'ingame_timestamp' => (int)$val_d['date'],
                            'dragon_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                            'dragon_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn'])
                        ];
                        $dragon_kills_detail_res[] = $drakeInfoRes;
                    }
                }
            }
            //处理塔的信息
            $building_status = $team_info['building_status'];
            $team_building_status_res = [];
            foreach ($building_status as $key_one => $val_one){
                if ($key_one == 'nexus'){
                    $team_building_status_res['nexus']['is_alive'] = $val_one['is_alive'];
                }
                if ($key_one == 'turrets'){
                    foreach ($val_one as $key_two => $val_two){
                        $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                    }
                }
                if ($key_one == 'inhibitors'){
                    $enum_socket_game_configure_res = $this->enum_socket_game_configure;
                    $inhibitor_reborn_time = $enum_socket_game_configure_res['inhibitor_reborn_time'];
                    foreach ($val_one as $key_two => $val_two){
                        $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                        //设置初始值 后面修改
                        $team_building_status_res[$key_one][$key_two]['respawntimer'] = null;
                        //查询这个是否已经被摧毁了
                        $inhibitor_killed_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_team_order_key.'_'.$key_two.'_kill_time');
                        if ($inhibitor_killed_time){
                            $inhibitor_respawntimer = $match_battle_duration - (Int)$inhibitor_killed_time;
                            if ($inhibitor_respawntimer >= $inhibitor_reborn_time){
                                //说明已经复活了, 去掉击杀的时间 respawntimer = null;  is_alive 为true
                                $team_building_status_res[$key_one][$key_two]['is_alive'] = true;
                                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_team_order_key.'_'.$key_two.'_kill_time',null);
                            }else{
                                $team_building_status_res[$key_one][$key_two]['respawntimer'] = (Int)($inhibitor_reborn_time - $inhibitor_respawntimer);
                            }
                        }
                    }
                }
            }
            $team_info['building_status'] = $team_building_status_res;
            //继续设置 factions team的信息
            $factions[$rel_team_order_key]['kills'] = (int)@$team_info['kills'];
            $factions[$rel_team_order_key]['deaths'] = (int)@$team_info['deaths'];
            $factions[$rel_team_order_key]['assists'] = (int)@$team_info['assists'];
            $factions[$rel_team_order_key]['gold'] = (int)@$team_info['gold'];
            $factions[$rel_team_order_key]['gold_diff'] = (int)@$team_info['gold_diff'];
            $factions[$rel_team_order_key]['experience'] = (int)@$team_info['experience'];
            $factions[$rel_team_order_key]['experience_diff'] = (int)@$team_info['experience_diff'];
            $factions[$rel_team_order_key]['turret_kills'] = (int)@$team_info['turret_kills'];
            $factions[$rel_team_order_key]['inhibitor_kills'] = (int)@$team_info['inhibitor_kills'];
            //从选手计算来的 rift_herald_kills
            $factions[$rel_team_order_key]['rift_herald_kills'] = (int)@$websocket_team_players_rift_herald_kills[$rel_team_order_key]['rift_herald_kills'];
            //$factions[$rel_team_order_key]['rift_herald_kills'] = (int)@$team_info['rift_herald_kills'];
            $factions[$rel_team_order_key]['dragon_kills'] = (int)@$team_info['dragon_kills'];
            $factions[$rel_team_order_key]['dragon_kills_detail'] = $dragon_kills_detail_res;
            $factions[$rel_team_order_key]['baron_nashor_kills'] = (int)@$team_info['baron_nashor_kills'];
            $factions[$rel_team_order_key]['building_status'] = $team_building_status_res;
            //上面循环选手已经有了
            $factions[$rel_team_order_key]['players'] = $websocket_team_players[$rel_team_order_key];
            $team_advanced = [
                'wards_purchased' => $websocket_team_players_wards[$rel_team_order_key]['wards_purchased'],
                'wards_placed' => null,
                'wards_kills' => null
            ];
            $factions[$rel_team_order_key]['advanced'] = $team_advanced;
            //$redis->hSet($redis_base_name."battles:".$battleId.":teams",$rel_team_order_key,json_encode($team_info,320));
            $team_elites_Kills[$rel_team_order_key]['dragon_kills'] = (int)@$team_info['dragon_kills'];
            //通过计算的 wards_purchased rift_herald_kills
            $team_info['wards_purchased'] = @$websocket_team_players_wards[$rel_team_order_key]['wards_purchased'];
            $team_info['rift_herald_kills'] = @$websocket_team_players_rift_herald_kills[$rel_team_order_key]['rift_herald_kills'];
            $team_info_res[$rel_team_order_key] = @json_encode($team_info,320);
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_info_res);
        //重置array key
        $factions = @array_values($factions);
        //设置默认值
        $is_match_finished = $is_battle_finished = false;
        $match_winner = null;
        $battle_winner = null;
        $is_pause = null;
        $is_live = true;
        //精英怪状态
        $elites_status = $this->getElitesStatus($redis,$battleId,$team_elites_Kills,$match_battle_duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'elites_status',@json_encode($elites_status,320));
        $event_data = [
            'duration' => $match_battle_duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => $is_pause,
            'is_live' => $is_live,
            'elites_status' => $elites_status,
            'factions' => $factions,
            'server_config' => $server_config
        ];
        $result = [
            'type' => 'frames',
            'event_data' => $event_data
        ];
        return $result;
    }
    //处理装备
    public function conversionOrangePlayerItemsIdsFromRedis($redis,$redis_base_name,$ingame_timestamp,$playerItems,$playerId,$rel_player_order,$battleId,$matchId,$originId,$lolItemsList){
        $need_change = 0;
        $LolExternalIdItemsRelationList = $this->getLolItemsRelationList($redis,$originId);
        $LolItemsUnknownId = $this->getLolItemsUnknownId($redis);
        //当前这个player的装备信息 然后获取上一个装备信息列表，判断有没有变化
        $playersItemsRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$playerId);
        if ($playersItemsRedis){
            $playersItemsBeforeLine = json_decode($playersItemsRedis,true);
            $playersItemsBefore = @end($playersItemsBeforeLine)['items'];
        }else{
            $playersItemsBeforeLine = $playersItemsBefore = [];
        }
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        $playerItemsInfoRet = [];
        if ($playerItems){
            foreach ($playerItems as $key => $rel_id){
                if ($rel_id){
                    $itemInfoId = $LolExternalIdItemsRelationList[$rel_id];
                    if($itemInfoId){
                        if (!in_array($itemInfoId,$playersItemsBefore)){
                            $need_change = 1;
                        }
                        $itemInfoId_val = (Int)$itemInfoId;
                        //获取当前
                        $itemInfoResRedis = $lolItemsList[$itemInfoId_val];
                        if ($itemInfoResRedis){
                            $itemInfoRes = @json_decode($itemInfoResRedis,true);
                            if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                                $playerOrnamentsInfo[$key] = $itemInfoId_val;
                            }else{//道具
                                $playerItemsInfo[$key]['total_cost'] = @$itemInfoRes['total_cost'];
                                $playerItemsInfo[$key]['item_id'] = $itemInfoId_val;
                            }
                        }else{//道具
                            $playerItemsInfo[$key]['total_cost'] = 1;
                            $playerItemsInfo[$key]['item_id'] = self::checkAndChangeInt($LolItemsUnknownId);
                        }
                    }else{
                        $playerItemsInfo[$key]['total_cost'] = 1;
                        $playerItemsInfo[$key]['item_id'] = self::checkAndChangeInt($LolItemsUnknownId);
                    }
                }else{
                    $playerItemsNull[$key] = null;
                }
            }
            $total_cost_array = array_column($playerItemsInfo,'total_cost');
            array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
            $playerItemsInfoRet = array_column($playerItemsInfo,'item_id');
        }
        //判断是不是够6个
        $playerItemsInfoNum = count($playerItemsInfoRet);
        if ($playerItemsInfoNum < 6){
            $chaNum = 6 - $playerItemsInfoNum;
            for ($i=0;$i<$chaNum;$i++){
                $playerItemsNull[] = null;
            }
        }
        if (empty($playerOrnamentsInfo)){
            $playerOrnamentsInfo[] = null;
        }
        $playerItemsInfoResult = array_merge($playerItemsInfoRet,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        //循环添加 purchase_time购买时间点 cooldown装备使用CD
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsInfo = null;
            if ($val_res){
                $players_handle_item_info_key = $rel_player_order.'_'.$val_res;
                $purchase_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_handle_item_info'],$players_handle_item_info_key);
                $ItemsInfo = [
                    'item_id' => $val_res,
                    'purchase_time' => $purchase_time ? $purchase_time : null,
                    'cooldown' => null
                ];
            }
            $playerItemsInfoResultEnd[] = $ItemsInfo;
        }
        //需要变化则更新redis
        if ($need_change == 1){
            $playerItemsInfoArray = [
                'ingame_timestamp'=>$ingame_timestamp,
                'items'=>$playerItemsInfoResult
            ];
            $playersItemsBeforeLine[] = $playerItemsInfoArray;
            $playerItemsLine =  @json_encode($playersItemsBeforeLine,320);
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$playerId,$playerItemsLine);
        }else{
            $playerItemsLine = $playersItemsBeforeLine;
        }
        //设置返回数据
        $result = [
            'items' => $playerItemsInfoResultEnd,
            'items_timeline' => $playerItemsLine
        ];
        return $result;
    }
    //处理召唤师技能
    public function conversionOrangePlayerSpellsIds($redis,$player_spell_list,$originId){
        $player_spell = [];
        $UnknownInfo = null;
        //召唤师技能
        $summonerSpellNameRelationIdList = $this->getSummonerSpellNameRelationIdList($redis,$originId);
        //$summoner_spell_redis_name = 'lol:summoner_spell:name_relation_id:'.$originId;
        foreach ($player_spell_list as $s_key => $s_val){
            $is_unknown = 0;
            //$summoner_spell_id = $redis->hGet($summoner_spell_redis_name,$s_val);
            $summoner_spell_id = $summonerSpellNameRelationIdList[$s_val];
            if (!$summoner_spell_id && empty($UnknownInfo)){
                $redisNameList = "lol:summoner_spell:list:unknown";
                $UnknownInfoRedis = $redis->get($redisNameList);
                $UnknownInfo = @json_decode($UnknownInfoRedis,true);
                $summoner_spell_id = @$UnknownInfo['id'];
                $is_unknown = 1;
            }
            $player_spell[$s_key]['spell_id'] = $summoner_spell_id;
            $player_spell[$s_key]['cooldown'] = null;
            $player_spell[$s_key]['is_unknown'] = $is_unknown;
            $player_spell[$s_key]['unknown_info'] = $UnknownInfo;
        }
        return $player_spell;
    }
    //处理技能
    public function orangeConversionPlayerSkill($redis,$battleId,$match_data,$metadata_origin_id){
        $ingame_timestamp = (Int)intval($match_data['timestamp']/1000);
        $player_order = $match_data['participantId'];
        $skillSlot = $match_data['skillSlot'];//技能ID 1,2,3,4
        //查找玩家信息
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$player_order);
        if ($player_info_redis){
            $player_info_array = @json_decode($player_info_redis,true);
            $champion_id = @$player_info_array['champion'];
            $champion_ability_and_level = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_champion_ability'],$player_order.'-'.$skillSlot);
            if ($champion_ability_and_level){
                $champion_ability_and_level_array = explode('-',$champion_ability_and_level);
                $ability = $champion_ability_and_level_array[0];
                $ability_level = (Int)$champion_ability_and_level_array[1] + 1;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_champion_ability'],$player_order.'-'.$skillSlot,$ability.'-'.$ability_level);
            }else{
                //查询对应的技能ID
                $championAbilityHotkeyRelation = "lol:ability:champion_ability_hotkey_relation_list";
                //$redisChampionAbilityRelation = "lol:ability:champion_ability_relation_id:".$metadata_origin_id;
                $redis_key_val = null;
                switch ($skillSlot){
                    case 1:
                        $redis_key_val = $champion_id.'_Q';
                        break;
                    case 2:
                        $redis_key_val = $champion_id.'_W';
                        break;
                    case 3:
                        $redis_key_val = $champion_id.'_E';
                        break;
                    case 4:
                        $redis_key_val = $champion_id.'_R';
                        break;
                }
                if (empty($redis_key_val)){//没有匹配上 返回
                    return true;
                }
                $ability_id = $redis->hGet($championAbilityHotkeyRelation,$redis_key_val);
                if (!$ability_id){//没有匹配上技能ID 也返回
                    return true;
                }
                $ability = $ability_id;
                $ability_level = 1;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_champion_ability'],$player_order.'-'.$skillSlot,$ability.'-'.$ability_level);
            }
            $abilities_timeline_new = [
                'ingame_timestamp' => $ingame_timestamp,
                'champion_level' => @$player_info_array['level'],
                'ability' => $ability,
                'ability_level' => $ability_level
            ];
            //查询有没有时间线了
            $abilities_timeline_old_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_abilities_timeline'],$player_order);
            if ($abilities_timeline_old_redis){
                $abilities_timeline_old = @json_decode($abilities_timeline_old_redis,true);
                $abilities_timeline_new_res[] = $abilities_timeline_new;
                $abilities_timeline_res = array_merge($abilities_timeline_old,$abilities_timeline_new_res);
            }else{
                $abilities_timeline_res[] = $abilities_timeline_new;
            }
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_abilities_timeline'],$player_order,@json_encode($abilities_timeline_res,320));
            //保存玩家
//            $player_info_array['abilities_timeline'] = @json_encode($abilities_timeline_res,320);
//            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$player_order,@json_encode($player_info_array,320));
        }
        return true;
    }
    //处理杀龙信息
    public function orangeConversionDragonKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$event_id,$metadata_origin_id){
        $drake_id = null;
        $event_type = 'elite_kill';
        $ingame_timestamp = (Int)round($match_data['timestamp']/1000);
        $position = @implode(',',$match_data['position']);
        //击杀者
        $killer_player_info_array = [];
        $killer_rel_order_id = $match_data['killerId'];
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $first_event_team_order = $player_rel_team_order = null;
        //查询
        //$player_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":players",$killer_rel_order_id);
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
        if ($player_info_redis){
            $player_info = @json_decode($player_info_redis,true);
            $killer_player_info_array = $player_info;
            $killer = @$player_info['player_id'];
            $killer_player_name = @$player_info['nick_name'];
            $killer_player_rel_name = @$player_info['rel_nick_name'];
            $killer_champion_id = @$player_info['champion'];
            $killer_faction = @$player_info['faction'];
            $first_event_team_order = @$player_info['team_order'];
            $player_rel_team_order = @$player_info['rel_team_order'];
        }
        //被杀者
        $killed_type = null;
        $victim = null;
        $monsterType = $match_data['monsterType'];
        $monsterSubType = $match_data['monsterSubType'];
        switch ($monsterType){
            case 'DRAGON':
                switch ($monsterSubType){
                    case 'ELDER_DRAGON':
                        $killed_type = 'elder_dragon';
                        $victim = @Common::getEliteId('elder_dragon');
                        $drake_id = $victim;
                        break;
                    case 'WATER_DRAGON':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_ocean');
                        $drake_id = $victim;
                        break;
                    case 'FIRE_DRAGON':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_infernal');
                        $drake_id = $victim;
                        break;
                    case 'EARTH_DRAGON':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_mountain');
                        $drake_id = $victim;
                        break;
                    case 'AIR_DRAGON':
                        $killed_type = 'drake';
                        $victim = @Common::getEliteId('elite_cloud');
                        $drake_id = $victim;
                        break;
                }
                //记录当前这个人的摧毁水晶信息
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['dragon_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    if ($killed_type == 'elder_dragon'){
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'elder_dragon_kill_time',$ingame_timestamp);
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'kill_time',$ingame_timestamp);
//                        $killer_team_faction = $killer_player_info_array['faction'];
//                        $killer_team_num_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'team_dragon_status'],$killer_team_faction.'_kill_num');
//                        $killer_team_num = $killer_team_num_redis ? ($killer_team_num_redis + 1) : 1;
//                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_dragon_status'],$killer_team_faction.'_kill_num',$killer_team_num);
                    }
                }
                break;
            case 'RIFTHERALD':
                $killed_type = 'rift_herald';
                $victim = @Common::getEliteId('rift_herald');
                //保存队伍击杀
                $team_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":teams",$player_rel_team_order);
                if ($team_info_redis){
                    $team_info = @json_decode($team_info_redis,true);
                    if (@$team_info['rift_herald_kills']){
                        (int)$team_info['rift_herald_kills'] += 1;
                    }else{
                        $team_info['rift_herald_kills'] = 1;
                    }
                    $redis->hSet($redis_base_name."battles:".$battleId.":teams",$player_rel_team_order,@json_encode($team_info,320));
                }
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['rift_herald_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //记录 击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time',$ingame_timestamp);
                    $rift_herald_kill_num = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num');
                    if ($rift_herald_kill_num == 1){
                        $rift_herald_kill_num += 1;
                        $enum_socket_game_configure_res = $this->enum_socket_game_configure;
                        if ($rift_herald_kill_num == $enum_socket_game_configure_res['rift_herald_max_num']){
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',2);
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
                        }
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',1);
                    }
                }
                break;
            case 'BARON_NASHOR':
                $killed_type = 'baron_nashor';
                $victim = @Common::getEliteId('baron_nashor');
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['baron_nashor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time',$ingame_timestamp);
                }
                break;
        }
        //查询出已经有的事件
//        $eventListRedisName = $redis_base_name."battles:".$battleId.":event_list";
//        $eventNum = $redis->hLen($eventListRedisName);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //这里处理killer和victim 和assist  方便存detail 和socket
        //获取英雄信息
        $championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
//        $championRes = $this->get
        //killer
        $websocket_killer = [
            'player_id' => (Int)$killer,
            'nick_name' => (string)$killer_player_name,
            'faction' => (string)$killer_faction,
            'champion' => $championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //victim
        $victim_ingame_obj_name = $victim_ingame_obj_name_cn = $victim_ingame_obj_type = $victim_ingame_obj_sub_type = null;
        if ($victim){
            $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
//            $redisEnumIngameGoalName = "enum:ingame_goal";
//            $victimInfoRedis = @$redis->hGet($redisEnumIngameGoalName,$victim);
            $victimInfoRedis = $drakeInfoList[$victim];
            $victimInfo = @json_decode($victimInfoRedis,true);
            $victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        $websocket_victim = [
            'ingame_obj_name' => $victim_ingame_obj_name,
            'ingame_obj_name_cn' => $victim_ingame_obj_name_cn,
            'ingame_obj_type' => $victim_ingame_obj_type,
            'ingame_obj_sub_type' => $victim_ingame_obj_sub_type
        ];
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$redis_base_name,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = @Common::getPandaScoreEventNum($first_event_type);
            //第一次事件 保存内容
            if ($first_event_type == 'first_rift_herald'){
                $first_rift_herald_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => []
                ];
                $match_battle_ext_lol = [
                    'first_rift_herald_p_tid' => $first_event_team_order,
                    'first_rift_herald_time' => $ingame_timestamp,
                    'first_rift_herald_detail' => @json_encode($first_rift_herald_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_dragon'){
                $first_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => []
                ];
                $match_battle_ext_lol = [
                    'first_dragon_p_tid' => $first_event_team_order,
                    'first_dragon_time' => $ingame_timestamp,
                    'first_dragon_detail' =>json_encode($first_dragon_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_baron_nashor'){
                $first_baron_nashor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => []
                ];
                $match_battle_ext_lol = [
                    'first_baron_nashor_p_tid' => $first_event_team_order,
                    'first_baron_nashor_time' => $ingame_timestamp,
                    'first_baron_nashor_detail' => @json_encode($first_baron_nashor_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_elder_dragon'){
                $first_elder_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => []
                ];
                $match_battle_ext_lol = [
                    'first_elder_dragon_p_tid' => $first_event_team_order,
                    'first_elder_dragon_time' => $ingame_timestamp,
                    'first_elder_dragon_detail' => @json_encode($first_elder_dragon_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => @$matchInfo['id'],
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => 50,
            'killer_sub_type' => 50,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => $victim,
            'victim_sub_type' => $victim,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        //$redis->hSet($eventListRedisName,$event_order,json_encode($event_data_list,320));
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //设置队伍杀龙信息  100 蓝  200红
        if ($drake_id){
            $team_faction_num = null;
            if ($killer_faction == 'blue'){
                $team_faction_num = 100;
            }
            if ($killer_faction == 'red'){
                $team_faction_num = 200;
            }
            //组成二维数组
            $dragon_kills_detail_now[] = [
                'date'=>$ingame_timestamp,
                'drake'=>(string)$drake_id
            ];
            $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$team_faction_num);
            //$teamInfoRedis = $redis->hGet($redis_base_name."battles:".$battleId.":teams",$team_faction_num);
            if ($teamInfoRedis) {
                $teamInfo = @json_decode($teamInfoRedis, true);
                $dragon_kills_detail = $teamInfo['dragon_kills_detail'];
                if ($dragon_kills_detail){
                    $teamInfo['dragon_kills_detail'] = array_merge($dragon_kills_detail,$dragon_kills_detail_now);
                }else{
                    $teamInfo['dragon_kills_detail'] = $dragon_kills_detail_now;
                }
                //$redis->hSet($redis_base_name."battles:".$battleId.":teams",$team_faction_num,@json_encode($teamInfo,320));
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$team_faction_num,@json_encode($teamInfo,320));
            }
        }
        //下面是websocket
        //websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => [],
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理杀人信息
    public function orangeConversionPlayerKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$event_id,$metadata_origin_id){
        $event_type = 'player_kill';
        $ingame_timestamp = (Int)round($match_data['timestamp']/1000);
        $position = @implode(',',$match_data['position']);
        //原始数据
        $killer_rel_order_id = $match_data['killerId'];
        $victim_rel_order_id = $match_data['victimId'];
        //计算最大连杀
        $this->conversionPlayerLargestKillingSpree($redis,$battleId,$killer_rel_order_id,$victim_rel_order_id);
        //击杀者
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $first_event_team_order = null;
        $killer_player_info = [];
        if ($killer_rel_order_id == 0){//自杀
            $event_type = 'player_suicide';
            $killer = 51;
            $killer_player_name = null;
            $killer_player_rel_name = 'unknown';
            $killer_champion_id = null;
            $killer_faction = null;
            $killer_ingame_obj_type = 51;
            $killer_sub_type = 51;
            //websocket
            $websocket_killer = [
                'ingame_obj_name' => 'unknown',
                'ingame_obj_name_cn' => '未知',
                'ingame_obj_type' => 'unknown',
                'ingame_obj_sub_type' => 'unknown'
            ];
        }else{
            $killer_ingame_obj_type = $killer_sub_type = 50;
            //查询
            //$killer_player_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":players",$killer_rel_order_id);
            $killer_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
            if ($killer_player_info_redis){
                $killer_player_info = @json_decode($killer_player_info_redis,true);
                $killer = @$killer_player_info['player_id'];
                $killer_player_name = @$killer_player_info['nick_name'];
                $killer_player_rel_name = @$killer_player_info['rel_nick_name'];
                $killer_champion_id = @$killer_player_info['champion'];
                $killer_faction = @$killer_player_info['faction'];
                $first_event_team_order = @$killer_player_info['team_order'];
            }
            //这里处理killer和victim 和assist  方便存detail 和socket
            //获取killer英雄信息
            $killer_championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
            //killer
            $websocket_killer = [
                'player_id' => (Int)$killer,
                'nick_name' => self::checkAndChangeString($killer_player_name),
                'faction' => self::checkAndChangeString($killer_faction),
                'champion' => $killer_championRes,
                'ingame_obj_type' => (string)'player',
                'ingame_obj_sub_type' => (string)'player'
            ];
            //计算
        }
        //受害者 victim
        $victim = $victim_player_name = $victim_player_rel_name = $victim_champion_id = $victim_faction = null;
        //查询
        //$victim_player_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":players",$victim_rel_order_id);
        $victim_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$victim_rel_order_id);
        if ($victim_player_info_redis){
            $victim_player_info = @json_decode($victim_player_info_redis,true);
            $victim = @$victim_player_info['player_id'];
            $victim_player_name = @$victim_player_info['nick_name'];
            $victim_player_rel_name = @$victim_player_info['rel_nick_name'];
            $victim_champion_id = @$victim_player_info['champion'];
            $victim_faction = @$victim_player_info['faction'];
        }
        //$assist 助攻
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        $assist_rel_list = $match_data['assistingParticipantIds'];
        if (count($assist_rel_list) > 0){
            $assist = [];
            foreach ($assist_rel_list as $a_key =>$a_val){
                $assist_player_info_array = [];
                $assist_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$a_val);
                if ($assist_player_info_redis){
                    $assist_player_info = @json_decode($assist_player_info_redis,true);
                    $assist_player_info_array['player_id'] = @$assist_player_info['player_id'];
                    $assist_player_info_array['player_nick_name'] = @$assist_player_info['nick_name'];
                    $assist_player_info_array['assist_player_name'] = @$assist_player_info['rel_nick_name'];
                    $assist_player_info_array['faction'] = @$assist_player_info['faction'];
                    $assist_player_info_array['champion_id'] = @$assist_player_info['champion'];
                }
                $assist[] = $assist_player_info_array;
            }
            $assist_ingame_obj_type = 50;
            $assist_sub_type = 50;
        }
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //获取victim英雄信息
        $victim_championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$victim_champion_id);
        //victim
        $websocket_victim = [
            'player_id' => (Int)$victim,
            'nick_name' => self::checkAndChangeString($victim_player_name),
            'faction' => self::checkAndChangeString($victim_faction),
            'champion' => $victim_championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //$websocket_assist
        $websocket_assist = null;
        if ($assist){
            foreach ($assist as $ws_a_key => $ws_a_item){
                $websocket_assist_info = [];
                $assist_champion_info = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$ws_a_item['champion_id']);
                $websocket_assist_info['player_id'] = (int)$ws_a_item['player_id'];
                $websocket_assist_info['nick_name'] = (string)$ws_a_item['player_nick_name'];
                $websocket_assist_info['faction'] = (string)$ws_a_item['faction'];
                $websocket_assist_info['champion'] = $assist_champion_info;
                $websocket_assist_info['ingame_obj_type'] = (string)'player';
                $websocket_assist_info['ingame_obj_sub_type'] = (string)'player';
                $websocket_assist[] = $websocket_assist_info;
            }
        }
        //获取第一次事件
        $killed_type = 'player';
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $is_first_event = false;
        if ($killer_rel_order_id > 0) {//非自杀
            $first_event_Info = $this->getCommonFirstEventInfo($redis,$redis_base_name,$battleId,$killed_type,$killer_faction);
            $is_first_event = $first_event_Info['is_first_event'];//true false
            if ($is_first_event) {
                $is_first_event_res = 1;
                $first_event_type = $first_event_Info['first_event_type'];
                $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
                //保存一血 五杀 十杀
                if ($first_event_type == 'first_blood') {
                    $first_blood_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_blood_p_tid' => $first_event_team_order,
                        'first_blood_time' => $ingame_timestamp,
                        'first_blood_detail' => @json_encode($first_blood_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
                if ($first_event_type == 'first_to_5_kills') {
                    $first_to_5_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_to_5_kills_p_tid' => $first_event_team_order,
                        'first_to_5_kills_time' => $ingame_timestamp,
                        'first_to_5_detail' => @json_encode($first_to_5_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
                if ($first_event_type == 'first_to_10_kills') {
                    $first_to_10_detail = [
                        'position' => $position,
                        'killer' => $websocket_killer,
                        'victim' => $websocket_victim,
                        'assist' => $websocket_assist
                    ];
                    $match_battle_ext_lol = [
                        'first_to_10_kills_p_tid' => $first_event_team_order,
                        'first_to_10_kills_time' => $ingame_timestamp,
                        'first_to_10_detail' => @json_encode($first_to_10_detail, 320)
                    ];
                    $this->redis_hMSet_And_Memory(
                        $redis,
                        ['current', 'battles', $battleId, 'detail'],
                        $match_battle_ext_lol
                    );
                }
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => @$matchInfo['id'],
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => $victim,
            'victim_player_name' => $victim_player_name,
            'victim_player_rel_name' => $victim_player_rel_name,
            'victim_champion_id' => $victim_champion_id,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => 50,
            'victim_sub_type' => 50,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理推塔信息
    public function orangeConversionBuildingKill($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$event_id,$metadata_origin_id){
        $event_type = 'building_kill';
        $building_lane = null;
        $ingame_timestamp = (Int)round($match_data['timestamp']/1000);
        $position = @implode(',',$match_data['position']);
        //击杀者
        $is_player = 0;//击杀者是不是玩家
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $killer_ingame_obj_type = $killer_sub_type = null;
        $first_event_team_order = null;
        $killer_player_info_array = [];
        $killer_rel_order_id = (Int)$match_data['killerId'];
        if ($killer_rel_order_id > 0){
            $is_player = 1;
            $killer_ingame_obj_type = 50;
            $killer_sub_type = 50;
            //$player_info_redis = $redis->hGet($redis_base_name."battles:".$battleId.":players",$killer_rel_order_id);
            $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
            if ($player_info_redis){
                $player_info = @json_decode($player_info_redis,true);
                $killer_player_info_array = $player_info;
                $killer = @$player_info['player_id'];
                $killer_player_name = @$player_info['nick_name'];
                $killer_player_rel_name = @$player_info['rel_nick_name'];
                $killer_champion_id = @$player_info['champion'];
                $killer_faction = @$player_info['faction'];
                $first_event_team_order = @$player_info['team_order'];
            }
        }else{
            $killer = 51;
            $killer_ingame_obj_type = $killer;
            $killer_sub_type = $killer;
        }
        //100 蓝  200红
        $rel_teamId = $match_data['teamId'];//塔的阵营
        if ($rel_teamId == 100){
            $victim_faction = 'blue';
            $websocket_victim_faction = 'blue';
            $team_killer_faction = 'red';
        }else{
            $victim_faction = 'red';
            $websocket_victim_faction = 'red';
            $team_killer_faction = 'blue';
        }
        //被杀者
        $killed_type = null;
        $victim = $victim_ingame_obj_type = $victim_sub_type = $victim_lane = $victim_ingame_obj_name = $victim_ingame_obj_name_cn = $victim_ingame_obj_sub_type = null;
        //原始数据
        $buildingType = $match_data['buildingType'];
        $laneType = $match_data['laneType'];
        $towerType = $match_data['towerType'];
        switch ($buildingType){
            case 'TOWER_BUILDING':
                $killed_type = 'tower';
                switch ($laneType){
                    case 'BOT_LANE':
                        switch ($towerType){
                            case 'OUTER_TURRET':
                                $victim = 17;
                                $building_lane = 'bot_outer_turret';
                                break;
                            case 'INNER_TURRET':
                                $victim = 18;
                                $building_lane = 'bot_inner_turret';
                                break;
                            case 'BASE_TURRET':
                                $victim = 19;
                                $building_lane = 'bot_inhibitor_turret';
                                break;
                        }
                        break;
                    case 'TOP_LANE':
                        switch ($towerType){
                            case 'OUTER_TURRET':
                                $victim = 10;
                                $building_lane = 'top_outer_turret';
                                break;
                            case 'INNER_TURRET':
                                $victim = 11;
                                $building_lane = 'top_inner_turret';
                                break;
                            case 'BASE_TURRET':
                                $victim = 12;
                                $building_lane = 'top_inhibitor_turret';
                                break;
                        }
                        break;
                    case 'MID_LANE':
                        switch ($towerType){
                            case 'OUTER_TURRET':
                                $victim = 14;
                                $building_lane = 'mid_outer_turret';
                                break;
                            case 'INNER_TURRET':
                                $victim = 15;
                                $building_lane = 'mid_inner_turret';
                                break;
                            case 'BASE_TURRET':
                                $victim = 16;
                                $building_lane = 'mid_inhibitor_turret';
                                break;
                            case 'NEXUS_TURRET'://基地塔  平台分为上路基地塔和下路基地塔 13 ，20   蓝色方：上路1748,2270  下路2177,1807  红方：下路13052,12612  上路12611,13084
                                if ($position == '1748,2270' || $position =='12611,13084'){//上路
                                    $victim = 13;
                                    $building_lane = 'top_nexus_turret';
                                }
                                if ($position == '2177,1807' || $position =='13052,12612'){//上路
                                    $victim = 20;
                                    $building_lane = 'bot_nexus_turret';
                                }
                                break;
                        }
                        break;
                }
                //记录当前这个人的摧毁防御塔信息
                if ($is_player == 1 && $killer_player_info_array){
                    (int)$killer_player_info_array['turret_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                }
                //记录队伍的击杀塔数量
                $team_turret_kills = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'team_turret_kills'],$team_killer_faction);
                if ($team_turret_kills){
                    $team_turret_kills_now = (Int)$team_turret_kills + 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_turret_kills'],$team_killer_faction,$team_turret_kills_now);
                }else{
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_turret_kills'],$team_killer_faction,1);
                }
                break;
            case 'INHIBITOR_BUILDING':
                $killed_type = 'inhibitor';
                switch ($laneType){
                    case 'TOP_LANE':
                        $victim = 22;
                        $building_lane = 'top_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_top_inhibitor_kill_time',$ingame_timestamp);
                        break;
                    case 'MID_LANE':
                        $victim = 23;
                        $building_lane = 'mid_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_mid_inhibitor_kill_time',$ingame_timestamp);
                        break;
                    case 'BOT_LANE':
                        $victim = 24;
                        $building_lane = 'bot_inhibitor';
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_teamId.'_bot_inhibitor_kill_time',$ingame_timestamp);
                        break;
                }
                //记录当前这个人的摧毁水晶信息
                if ($is_player == 1 && $killer_player_info_array){
                    (int)$killer_player_info_array['inhibitor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killer_player_info_array,320));
                }
                break;
            default:
                $victim = null;
                break;
        }
        if (empty($victim)){
            return null;
        }
        $victim_ingame_obj_type = $victim;
        $victim_sub_type = $victim;
        //$assist 助攻
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        $assist_rel_list = $match_data['assistingParticipantIds'];
        if (count($assist_rel_list) > 0){
            $assist = [];
            foreach ($assist_rel_list as $a_key =>$a_val){
                $assist_player_info_array = [];
                $assist_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$a_val);
                if ($assist_player_info_redis){
                    $assist_player_info = @json_decode($assist_player_info_redis,true);
                    $assist_player_info_array['player_id'] = @$assist_player_info['player_id'];
                    $assist_player_info_array['player_nick_name'] = @$assist_player_info['nick_name'];
                    $assist_player_info_array['assist_player_name'] = @$assist_player_info['rel_nick_name'];
                    $assist_player_info_array['faction'] = @$assist_player_info['faction'];
                    $assist_player_info_array['champion_id'] = @$assist_player_info['champion'];
                }
                $assist[] = $assist_player_info_array;
            }
            $assist_ingame_obj_type = 50;
            $assist_sub_type = 50;
        }
        //查询出已经有的事件
//        $eventListRedisName = $redis_base_name."battles:".$battleId.":event_list";
//        $eventNum = $redis->hLen($eventListRedisName);
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //这里处理killer和victim 和assist  方便存detail 和socket
        //判断是不是玩家
        if ($is_player){
            //获取英雄信息
            $championRes = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$killer_champion_id);
            //killer
            $websocket_killer = [
                'player_id' => (Int)$killer,
                'nick_name' => self::checkAndChangeString($killer_player_name),
                'faction' => self::checkAndChangeString($killer_faction),
                'champion' => $championRes,
                'ingame_obj_type' => (string)'player',
                'ingame_obj_sub_type' => (string)'player'
            ];
        }else{
            $websocket_killer = [
                'ingame_obj_name' => 'unknown',
                'ingame_obj_name_cn' => '未知',
                'ingame_obj_type' => 'unknown',
                'ingame_obj_sub_type' => 'unknown'
            ];
        }
        //victim 在上面计算出来的 再通过victim的ID 获取数据
        $websocket_victim_lane = $websocket_victim_ingame_obj_name = $websocket_victim_ingame_obj_name_cn = $websocket_victim_ingame_obj_type = $websocket_victim_ingame_obj_sub_type = null;
        $buildingInfoList = $this->getEnumIngameGoalInfo($redis);
        //$victimInfoRedis = $redis->hGet('enum:ingame_goal',$victim);
        if ($victim){
            $victimInfo = @json_decode($buildingInfoList[$victim],true);
            $websocket_victim_lane = self::checkAndChangeString(@$victimInfo['lane']);
            $websocket_victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $websocket_victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $websocket_victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $websocket_victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        $websocket_victim = [
            'lane' => $websocket_victim_lane,
            'ingame_obj_name' => $websocket_victim_ingame_obj_name,
            'ingame_obj_name_cn' => $websocket_victim_ingame_obj_name_cn,
            'faction' => $websocket_victim_faction,
            'ingame_obj_type' => $websocket_victim_ingame_obj_type,
            'ingame_obj_sub_type' => $websocket_victim_ingame_obj_sub_type
        ];
        //$websocket_assist
        $websocket_assist = [];
        if ($assist){
            foreach ($assist as $ws_a_key => $ws_a_item){
                $websocket_assist_info = [];
                //获取英雄信息
                $assist_champion_info = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$ws_a_item['champion_id']);
                $websocket_assist_info['player_id'] = (int)$ws_a_item['player_id'];
                $websocket_assist_info['nick_name'] = self::checkAndChangeString(@$ws_a_item['player_nick_name']);
                $websocket_assist_info['faction'] = self::checkAndChangeString(@$ws_a_item['faction']);
                $websocket_assist_info['champion'] = $assist_champion_info;
                $websocket_assist_info['ingame_obj_type'] = (string)'player';
                $websocket_assist_info['ingame_obj_sub_type'] = (string)'player';
                $websocket_assist[] = $websocket_assist_info;
            }
        }
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$redis_base_name,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
            //保存首塔 首水晶
            if ($first_event_type == 'first_turret'){
                $first_turret_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_turret_p_tid' => $first_event_team_order,
                    'first_turret_time' => $ingame_timestamp,
                    'first_turret_detail' => @json_encode($first_turret_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_inhibitor'){
                $first_inhibitor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_inhibitor_p_tid' => $first_event_team_order,
                    'first_inhibitor_time' => $ingame_timestamp,
                    'first_inhibitor_detail' => @json_encode($first_inhibitor_detail,320)
                ];
                //$redis->hMSet($redis_base_name."battles:".$battleId.":detail",$match_battle_ext_lol);
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => @$matchInfo['id'],
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => $victim_ingame_obj_type,
            'victim_sub_type' => $victim_sub_type,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        //$redis->hSet($eventListRedisName,$event_order,json_encode($event_data_list,320));
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //设置队伍的塔信息
        if ($killed_type && $building_lane){
            //$rel_teamId
            $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_teamId);
            if ($teamInfoRedis) {
                $teamInfo = @json_decode($teamInfoRedis, true);
                $team_building_status = @$teamInfo['building_status'];
                if ($killed_type == 'tower'){
                    $team_building_status['turrets'][$building_lane]['is_alive'] = false;
                }
                if ($killed_type == 'inhibitor'){
                    $team_building_status['inhibitors'][$building_lane]['is_alive'] = false;
                }
                $teamInfo['building_status'] = $team_building_status;
                //$redis->hSet($redis_base_name."battles:".$battleId.":teams",$rel_teamId,@json_encode($teamInfo,320));
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$rel_teamId,@json_encode($teamInfo,320));
            }
        }
        //下面是websocket
        //websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //第一次事件
    public function getCommonFirstEventInfo($redis,$redis_base_name,$battleId,$killed_type,$faction){
        //$event_first_redis_name = $redis_base_name."battles:".$battleId.":event_first";
        $event_first_redis_name = ['current','battles',$battleId,'event_first'];
        //记录事件
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
                //设置初始值
                $team_kills_key = 'team_kills_'.$faction;
                //查询是否是一血
                //$first_blood = $redis->hGet($event_first_redis_name,'first_blood');
                $first_blood = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_blood');
                if (!$first_blood){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_blood'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_blood',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_blood',$faction);
                }
                //查询 first_to_5_kills first_to_10_kills
                //查询当前team击杀了多少人
                //查询是否五杀了
                //$first_to_5_kills = $redis->hGet($event_first_redis_name,'first_to_5_kills');
                $first_to_5_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_5_kills');
                //$first_to_10_kills = $redis->hGet($event_first_redis_name,'first_to_10_kills');
                $first_to_10_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_10_kills');
                //$team_kills_res = $redis->hGet($event_first_redis_name,$team_kills_key);//击杀队伍
                $team_kills_res = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,$team_kills_key);//击杀队伍
                if ($team_kills_res){
                    $now_team_kills = $team_kills_res + 1;
                    //$redis->hSet($event_first_redis_name,$team_kills_key,$now_team_kills);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,$now_team_kills);
                    if ($now_team_kills == 5 && empty($first_to_5_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_5_kills'
                        ];
                        //$redis->hSet($event_first_redis_name,'first_to_5_kills', $faction);
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_5_kills',$faction);
                    }
                    if ($now_team_kills == 10 && empty($first_to_10_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_10_kills'
                        ];
                        //$redis->hSet($event_first_redis_name,'first_to_10_kills', $faction);
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_10_kills',$faction);
                    }
                }else{
                    //$redis->hSet($event_first_redis_name,$team_kills_key,1);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,1);
                }
                break;
            case 'rift_herald':
                //$rift_herald = $redis->hGet($event_first_redis_name,'first_rift_herald');
                $rift_herald = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_rift_herald',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_rift_herald',$faction);
                }
                break;
            case 'drake':
                //$first_dragon = $redis->hGet($event_first_redis_name,'first_dragon');
                $first_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_dragon');
                if (!$first_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_dragon'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_dragon',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_dragon',$faction);
                }
                break;
            case 'elder_dragon':
                //$first_elder_dragon = $redis->hGet($event_first_redis_name,'first_elder_dragon');
                $first_elder_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_elder_dragon',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_elder_dragon',$faction);
                }
                break;
            case 'baron_nashor':
                //$first_baron_nashor = $redis->hGet($event_first_redis_name,'first_baron_nashor');
                $first_baron_nashor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_baron_nashor',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_baron_nashor',$faction);
                }
                break;
            case 'tower':
                //$first_turret = $redis->hGet($event_first_redis_name,'first_turret');
                $first_turret = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_turret',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_turret',$faction);
                }
                break;
            case 'inhibitor':
                //$first_inhibitor = $redis->hGet($event_first_redis_name,'first_inhibitor');
                $first_inhibitor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor'
                    ];
                    //$redis->hSet($event_first_redis_name,'first_inhibitor',$faction);
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_inhibitor',$faction);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null
                ];
        }
        return $eventInfoRes;
    }
    //battle end
    private function orangeConversionBattleEnd($redis,$redis_base_name,$battleId,$match_data,$matchInfo,$event_id,$socket_time){
        $matchId = $matchInfo['id'];
        $event_type = 'battle_end';
        $ingame_timestamp = (Int)round($match_data['timestamp']/1000);
        //查询出已经有的事件
        //match_real_time_info 信息
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
        $team = $team_order = null;
        $rel_winningTeam = $match_data['winningTeam'];//100 或者200
        //修改 失败的一方 nexus 为false
        if ($rel_winningTeam == 100){
            $rel_failTeam = 200;
        }else{
            $rel_failTeam = 100;
        }
        $team_info_end = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        $failTeamInfoRedis = $team_info_end[$rel_failTeam];
        //$failTeamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_failTeam);
        if ($failTeamInfoRedis){
            $failTeamInfo = @json_decode($failTeamInfoRedis,true);
            $failTeamInfo['building_status']['nexus']['is_alive'] = false;
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$rel_failTeam,@json_encode($failTeamInfo,320));
        }
        //获取赢的队伍信息
        //$winnerTeamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_winningTeam);
        $winnerTeamInfoRedis = $team_info_end[$rel_winningTeam];
        if ($winnerTeamInfoRedis){
            $winnerTeamInfo = @json_decode($winnerTeamInfoRedis,true);
            $team_order = @$winnerTeamInfo['order'];
            //根据ID 获取team信息
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$winnerTeamInfo['team_id']);
            $team_info = @json_decode($match_team_info_redis,true);
            $team = [
                'team_id' => self::checkAndChangeInt(@$winnerTeamInfo['team_id']),
                'name' => self::checkAndChangeString(@$team_info['name']),
                'image' => self::checkAndChangeString(@$team_info['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_order)
            ];
            //更新battle队伍的分数
            $winnerTeamInfo['score'] = 1;
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$rel_winningTeam,@json_encode($winnerTeamInfo,320));
            //更新battle的结束信息
            $battle_base = [
                'duration'=>$ingame_timestamp,
                'end_at' => $socket_time,
                'status' => 3,
                'winner' => $team_order
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_base);
            //更新比赛的结束
            $team_1_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_1_score');
            $team_2_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_2_score');
            if ($team_order == 1){
                $team_1_score = $team_1_score + 1;
            }
            if ($team_order == 2){
                $team_2_score = $team_2_score + 1;
            }
        }else{
            return null;
        }
        //获取match winner
        $statusInfo = WinnerHelper::lolWinnerInfo($team_1_score,$team_2_score,$matchInfo['number_of_games']);
        if($statusInfo['is_finish']==1){
            $match_real_time_info['status'] = 3;
            $match_real_time_info['end_at'] = $socket_time;
            $match_real_time_info['team_1_score'] = $team_1_score;
            $match_real_time_info['team_2_score'] = $team_2_score;
            if ($statusInfo['is_draw'] == 1){//判断是不是平局
                $match_real_time_info['winner'] = null;
                $match_real_time_info['is_draw'] = 1;
            }else{
                $match_real_time_info['winner'] = $statusInfo['winner_team'];
            }
            //$redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
        }else{
            $match_real_time_info['team_1_score'] = $team_1_score;
            $match_real_time_info['team_2_score'] = $team_2_score;
            //$redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
        }
        //计算gold_diff_timeline experience_diff 100 蓝  200红  蓝减红
        $team_info_res = [];
        if ($team_info_end){
            foreach ($team_info_end as $key => $value){
                $team_info_new = @json_decode($value,true);
                $team_info_res[$key]['gold'] = $team_info_new['gold'];
                $team_info_res[$key]['experience'] = $team_info_new['experience'];
            }
            //保存时间线
            $gold_diff = $team_info_res[100]['gold'] - $team_info_res[200]['gold'];
            $experience_diff = $team_info_res[100]['experience'] - $team_info_res[200]['experience'];
            //gold_diff
            $gold_diff_timeline_now[] = [
                'ingame_timestamp' => $ingame_timestamp,
                'gold_diff' => $gold_diff
            ];
            $gold_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_diff_timeline']);
            if ($gold_diff_timeline_redis){
                $gold_diff_timeline_old = @json_decode($gold_diff_timeline_redis,true);
                $gold_diff_timeline_new = @array_merge($gold_diff_timeline_old,$gold_diff_timeline_now);
            }else{
                $gold_diff_timeline_new = $gold_diff_timeline_now;
            }
            $gold_diff_timeline_res = @json_encode($gold_diff_timeline_new,320);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_diff_timeline'],$gold_diff_timeline_res);
            //experience_diff
            $experience_diff_timeline_now[] = [
                'ingame_timestamp' => $ingame_timestamp,
                'experience_diff' => $experience_diff
            ];
            //获取原来的信息
            $experience_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'experience_diff_timeline']);
            if ($experience_diff_timeline_redis){
                $experience_diff_timeline_old = @json_decode($experience_diff_timeline_redis,true);
                $experience_diff_timeline_new = @array_merge($experience_diff_timeline_old,$experience_diff_timeline_now);
            }else{
                $experience_diff_timeline_new = $experience_diff_timeline_now;
            }
            $experience_diff_timeline_res = @json_encode($experience_diff_timeline_new,320);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'experience_diff_timeline'],$experience_diff_timeline_res);
        }
        //获取 event_order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => @$team['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        //$redis->hSet($eventListRedisName,$event_order,json_encode($event_data_list,320));
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //winner
        $websocket_winner = $team;
        //websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'winner' => $websocket_winner
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //直接获取frame
    private function getFramesInfo($redis,$redis_base_name,$battleId,$battle_order,$match_data,$matchInfo,$metadata_origin_id,$gameId,$originId){
        $matchId = $matchInfo['id'];
        $game_version = $matchInfo['game_version'];
        $server_config = [
            'version' => (String)$game_version
        ];
        $match_battle_duration = (Int)round($match_data['timestamp']/1000);
        //$factions
        //获取team
        $winner_teams_info = [];
        $matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $factions = [];
        $match_scores = [];
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
        //首先获取player
        $websocket_team_players = $websocket_team_players_wards = $websocket_team_players_rift_herald_kills = [];
        //首先获取所有的items
        $lolItemsList = $this->getLolItemsList($redis,$metadata_origin_id);
        //获取所有的召唤师技能
        $summonerSpellInfoList = $this->getSummonerSpellInfoList($redis,$metadata_origin_id);
        //$players_info_new = $redis->hGetAll($redis_base_name."battles:".$battleId.":players");
        $players_info_new = $this->redis_hGetPlayersAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        ksort($players_info_new);
        foreach ($players_info_new as $player_res_item){
            $player_res = @json_decode($player_res_item,true);
            $rel_team_order = @$player_res['rel_team_order'];
            //组合websocket
            $websocket_player = [];
            $websocket_player['seed'] = (int)$player_res['seed'];
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = @Common::getLolRoleStringByRoleId('orange',$player_res['role']);
            $websocket_player['lane'] = @Common::getLolLaneStringByLaneId('orange',$player_res['lane']);
            $websocket_player['player']['player_id'] = $player_res['player_id'];
            $websocket_player['player']['nick_name'] = $player_res['nick_name'];
            //获取英雄信息
            $champion = $this->getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$player_res['champion']);
            $websocket_player['champion'] = $champion;
            $websocket_player['level'] = (int)$player_res['level'];
//            $is_alive = false;
//            if (empty(@$player_res['alive'])){
//                $is_alive = null;
//            }else{
//                if ($player_res['alive'] == 1){
//                    $is_alive = true;
//                }
//            }
            $websocket_player['is_alive'] = null;//$is_alive
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['cs'] = (String)$player_res['cs'];
            $websocket_player['cspm'] = (String)$player_res['cspm'];
            //summoner_spells
            $websocket_player['summoner_spells'] = $this->getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_res['summoner_spells']);
            //$websocket_player['summoner_spells'] = $player_summoner_spells;
            //道具
            $websocket_player['items'] = $this->getWebSocketPlayerItemsList($lolItemsList,$player_res['items']);;
            //$keystone = self::getKeystoneInfoByRedis($redis,$metadata_origin_id,$player_res['keystone']);
            $keystone = $this->getWebSocketPlayerKeystoneInfo($redis,$metadata_origin_id,$player_res['player_id'],$player_res['keystone']);
            $advanced = [
                'keystone' => $keystone,
                'ultimate_cd' => null,
                'coordinate' => (String)$player_res['coordinate'],
                'respawntimer' => null,
                'health' => null,
                'health_max' => null,
                'turret_kills' => (Int)$player_res['turret_kills'],
                'inhibitor_kills' => (Int)$player_res['inhibitor_kills'],
                'rift_herald_kills' => (Int)$player_res['rift_herald_kills'],
                'dragon_kills' => (Int)$player_res['dragon_kills'],
                'baron_nashor_kills' => (Int)$player_res['baron_nashor_kills'],
                'double_kill' => (Int)$player_res['double_kill'],
                'triple_kill' => (Int)$player_res['triple_kill'],
                'quadra_kill' => (Int)$player_res['quadra_kill'],
                'penta_kill' => (Int)$player_res['penta_kill'],
                'largest_multi_kill' => (Int)$player_res['largest_multi_kill'],
                'largest_killing_spree' => (int)$player_res['largest_killing_spree'],
                'minion_kills' => (String)$player_res['minion_kills'],
                'total_neutral_minion_kills' => (String)$player_res['total_neutral_minion_kills'],
                'neutral_minion_team_jungle_kills' => (String)$player_res['neutral_minion_team_jungle_kills'],
                'neutral_minion_enemy_jungle_kills' => (String)$player_res['neutral_minion_enemy_jungle_kills'],
                'gold_earned' => (Int)$player_res['gold_earned'],
                'gold_spent' => (Int)$player_res['gold_spent'],
                'gold_remaining' => (Int)$player_res['gold_remaining'],
                'gpm' => (String)$player_res['gpm'],
                'gold_earned_percent' => (String)$player_res['gold_earned_percent'],
                'experience' => (Int)$player_res['experience'],
                'xpm' => (String)$player_res['xpm'],
                'damage_to_champions' => (String)$player_res['damage_to_champions'],
                'damage_to_champions_physical' => (String)$player_res['damage_to_champions_physical'],
                'damage_to_champions_magic' => (String)$player_res['damage_to_champions_magic'],
                'damage_to_champions_true' => (String)$player_res['damage_to_champions_true'],
                'dpm_to_champions' => (String)$player_res['dpm_to_champions'],
                'damage_percent_to_champions' => (String)$player_res['damage_percent_to_champions'],
                'total_damage' => null,
                'total_damage_physical' => null,
                'total_damage_magic' => null,
                'total_damagel_true' => null,
                'damage_taken' => (String)$player_res['damage_taken'],
                'damage_taken_physical' => (String)$player_res['damage_taken_physical'],
                'damage_taken_magic' => (String)$player_res['damage_taken_magic'],
                'damage_taken_true' => (String)$player_res['damage_taken_true'],
                'dtpm' => (String)$player_res['dtpm'],
                'damage_taken_percent' => (String)$player_res['damage_taken_percent'],
                'damage_conversion_rate' => (String)$player_res['damage_conversion_rate'],
                'damage_selfmitigated' => null,
                'damage_shielded_on_teammates' => null,
                'damage_to_buildings' => null,
                'damage_to_towers' => (String)$player_res['damage_to_towers'],
                'damage_to_objectives' => null,
                'total_crowd_control_time' => null,
                'total_crowd_control_time_others' => null,
                'total_heal' => null,
                'wards_purchased' => (int)$player_res['wards_purchased'],
                'wards_placed' => null,
                'wards_kills' => null,
                'vision_score' => null
            ];
            $websocket_player['advanced'] = $advanced;
            $websocket_team_players[$rel_team_order][] = $websocket_player;
            $websocket_team_players_wards[$rel_team_order]['wards_purchased'] += (Int)$player_res['wards_purchased'];
            $websocket_team_players_rift_herald_kills[$rel_team_order]['rift_herald_kills'] += (Int)$player_res['rift_herald_kills'];
        }
        //获取龙信息
        $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
        //获取battle胜者
        $is_battle_finished = false;
        //查询battle 是否结束
        //$battle_winner_id = $redis->hGet($redis_base_name."battles:".$battleId.":base","winner");
        $battle_winner_id = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'base'],'winner');
        if ($battle_winner_id){
            $is_battle_finished = true;
        }
        //$team_info_new = $redis->hGetAll($redis_base_name."battles:".$battleId.":teams");
        $team_info_new = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        //循环队伍保存到redis
        $team_elites_Kills = [];
        foreach ($team_info_new as $rel_team_order_key => $team_info_item){
            $team_info = @json_decode($team_info_item,true);
            $team_score = 0;
            $factions[$rel_team_order_key]['faction'] = (string)@$team_info['faction'];
//            $match_team_info_redis = $redis->hGet($matchTeamInfoRedisName,$team_info['team_id']);
//            $team_info_base = @json_decode($match_team_info_redis,true);
//            $team_opponent_order = @$team_info['order'];
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt(@$team_info['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            $winner_teams_info[$team_opponent_order] = $teamInfo;
            //获取比赛的分数
            if($team_opponent_order == 1){
                //$team_score = $redis->hGet($matchRealTimeInfoRedisName,'team_1_score');
                $team_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_1_score');
            }
            if($team_opponent_order == 2){
                //$team_score = $redis->hGet($matchRealTimeInfoRedisName,'team_2_score');
                $team_score = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'team_2_score');
            }
            //优先设置 factions 里面 team
            $factions[$rel_team_order_key]['team'] = $teamInfo;
            //再设置外层 match_scores
            $teamInfo['score'] = (int)$team_score;
            $match_scores[] = $teamInfo;
            //处理龙的信息
            $dragon_kills_detail_res = null;
            $dragon_kills_detail = $team_info['dragon_kills_detail'];
            if ($dragon_kills_detail){
                foreach ($dragon_kills_detail as $val_d){
                    //$drakeInfoRedis = $redis->hGet('enum:ingame_goal',$val_d['drake']);
                    $drakeInfoRedis = $drakeInfoList[$val_d['drake']];
                    if ($drakeInfoRedis){
                        $drakeInfo = @json_decode($drakeInfoRedis,true);
                        $drakeInfoRes = [
                            'ingame_timestamp' => self::checkAndChangeInt($val_d['date']),
                            'dragon_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                            'dragon_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn'])
                        ];
                        $dragon_kills_detail_res[] = $drakeInfoRes;
                    }
                }
            }
            //处理塔的信息
            $building_status = $team_info['building_status'];
            $team_building_status_res = [];
            foreach ($building_status as $key_one => $val_one){
                if ($key_one == 'nexus'){ //最后一针 nexus  为 false
                    $team_building_status_res['nexus']['is_alive'] = false;
                    if ($battle_winner_id == $team_opponent_order){
                        $team_building_status_res['nexus']['is_alive'] = true;
                    }
                }
                if ($key_one == 'turrets'){
                    foreach ($val_one as $key_two => $val_two){
                        $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                    }
                }
                if ($key_one == 'inhibitors'){
                    $enum_socket_game_configure_res = $this->enum_socket_game_configure;
                    $inhibitor_reborn_time = $enum_socket_game_configure_res['inhibitor_reborn_time'];
                    foreach ($val_one as $key_two => $val_two){
                        $team_building_status_res[$key_one][$key_two]['is_alive'] = $val_two['is_alive'];
                        //设置初始值 后面修改
                        $team_building_status_res[$key_one][$key_two]['respawntimer'] = null;
                        //查询这个是否已经被摧毁了
                        $inhibitor_killed_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_team_order_key.'_'.$key_two.'_kill_time');
                        if ($inhibitor_killed_time){
                            $inhibitor_respawntimer = $match_battle_duration - (Int)$inhibitor_killed_time;
                            if ($inhibitor_respawntimer >= $inhibitor_reborn_time){
                                //说明已经复活了, 去掉击杀的时间 respawntimer = null;
                                $team_building_status_res[$key_one][$key_two]['is_alive'] = true;
                                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'team_inhibitor_killed'],$rel_team_order_key.'_'.$key_two.'_kill_time',null);
                            }else{
                                $team_building_status_res[$key_one][$key_two]['respawntimer'] = (Int)($inhibitor_reborn_time - $inhibitor_respawntimer);
                            }
                        }
                    }
                }
            }
            //加上复活时间
            $team_info['building_status'] = $team_building_status_res;
            //继续设置 factions team的信息
            $factions[$rel_team_order_key]['kills'] = (int)@$team_info['kills'];
            $factions[$rel_team_order_key]['deaths'] = (int)@$team_info['deaths'];
            $factions[$rel_team_order_key]['assists'] = (int)@$team_info['assists'];
            $factions[$rel_team_order_key]['gold'] = (int)@$team_info['gold'];
            $factions[$rel_team_order_key]['gold_diff'] = (int)@$team_info['gold_diff'];
            $factions[$rel_team_order_key]['experience'] = (int)@$team_info['experience'];
            $factions[$rel_team_order_key]['experience_diff'] = (int)@$team_info['experience_diff'];
            $factions[$rel_team_order_key]['turret_kills'] = (int)@$team_info['turret_kills'];
            $factions[$rel_team_order_key]['inhibitor_kills'] = (int)@$team_info['inhibitor_kills'];
            //$factions[$rel_team_order_key]['rift_herald_kills'] = (int)@$team_info['rift_herald_kills'];
            //从选手计算来的 rift_herald_kills
            $factions[$rel_team_order_key]['rift_herald_kills'] = (int)@$websocket_team_players_rift_herald_kills[$rel_team_order_key]['rift_herald_kills'];
            $factions[$rel_team_order_key]['dragon_kills'] = (int)@$team_info['dragon_kills'];
            $factions[$rel_team_order_key]['dragon_kills_detail'] = $dragon_kills_detail_res;
            $factions[$rel_team_order_key]['baron_nashor_kills'] = (int)@$team_info['baron_nashor_kills'];
            $factions[$rel_team_order_key]['building_status'] = $team_building_status_res;
            //上面循环选手已经有了
            $factions[$rel_team_order_key]['players'] = $websocket_team_players[$rel_team_order_key];
            $team_advanced = [
                'wards_purchased' => $websocket_team_players_wards[$rel_team_order_key]['wards_purchased'],
                'wards_placed' => null,
                'wards_kills' => null
            ];
            $factions[$rel_team_order_key]['advanced'] = $team_advanced;
            //杀龙信息
            $team_elites_Kills[$rel_team_order_key]['dragon_kills'] = (int)@$team_info['dragon_kills'];
        }
        //重置array key
        $factions = @array_values($factions);
        //$battle_winner
        if ($battle_winner_id){
            $battle_winner = $winner_teams_info[$battle_winner_id];
        }else{
            $battle_winner = null;
        }
        //设置默认值
        $is_match_finished = false;
        //查询match是否结束
        //$match_winner_id = $redis->hGet($matchRealTimeInfoRedisName,'winner');
        $match_winner_id = $this->redis_hGet_Or_Memory($redis,['match_real_time_info'],'winner');
        if ($match_winner_id){
            $match_winner = $winner_teams_info[$match_winner_id];
            $is_match_finished = true;
        }else{
            $match_winner = null;
        }
        $is_pause = null;
        $is_live = true;
        //比赛的信息
        $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
        $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
        $number_of_games = $matchInfo['number_of_games'];
        //获取地图
        $map = $this->getMapInfoByRedisOrMemory($redis,1);
        //精英怪状态
        $elites_status = $this->getElitesStatus($redis,$battleId,$team_elites_Kills,$match_battle_duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'elites_status',@json_encode($elites_status,320));
        //最后结果
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'game_rules' => (String)$game_rules,
            'match_type' => (String)$match_type,
            'number_of_games' => (Int)$number_of_games,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'map' => $map,
            'duration' => $match_battle_duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => $is_pause,
            'is_live' => $is_live,
            'elites_status' => $elites_status,
            'factions' => $factions,
            'server_config' => $server_config
        ];
        //插入到redis
        $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
        //接下来存数据库
        $insertData = [
            'match_data' =>@json_encode($event_data_ws_res,320),
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => 'frames',
            'battle_order' => $battle_order
        ];
        //当前数据库连接
        //$this->saveDataBase($insertData);
        $this->saveKafka($insertData);
        return true;
    }
    //补充发 events
    private function supplementBattleEventsToWebSocket($redis,$redis_base_name,$match_data,$matchInfo,$event_type,$event_info,$websocket_data){
        $socket_type = 'events';
        $matchId = $matchInfo['id'];
        $gameId = $websocket_data['gameId'];
        $battleId = $websocket_data['battleId'];
        $rel_match_id = $websocket_data['rel_match_id'];
        $battle_order = $websocket_data['battle_order'];
        $socket_id = $websocket_data['socket_id'];
        $originId= $websocket_data['originId'];
        $metadata_origin_id= $websocket_data['metadata_origin_id'];
        $event_data = [];
        if ( $event_type == 'champion_picked'){
            //获取英雄信息
            $champions_id = $event_info['champions_id'];
            $champion = self::getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id);
            $event_data = [
                'event_type' => (String)$event_type,
                'bp_order' => (Int)$event_info['bp_order'],
                'faction' => (String)$event_info['faction'],
                'team' => $event_info['team'],
                'champion' => $champion
            ];
        }
        if ($event_data){
            //事件的时间 单位 秒
            $ingame_timestamp = 0;
            $timestamp = @$match_data['timestamp'];
            if ($timestamp){
                $ingame_timestamp = (Int)round($match_data['timestamp']/1000);
            }
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'ingame_timestamp' => (Int)$ingame_timestamp,
                'event_id' => (Int)$socket_id
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$event_data);
            if ($event_data_ws_res){
//                $eventListRedisName = $redis_base_name."battles:".$battleId.":event_list";
//                $eventNum = $redis->hLen($eventListRedisName);
                $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
                if ($eventNum){
                    $event_order = $eventNum + 1;
                }else{
                    $event_order = 1;
                }
                $this->refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_match_id,$battleId,$battle_order,$event_type,$event_order);
                //插入到redis
                $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
                //接下来存数据库
                $insertData = [
                    'match_data' =>@json_encode($event_data_ws_res,320),
                    'match_id' => $matchId,
                    'game_id' => $gameId,
                    'origin_id' => $originId,
                    'type' => $socket_type,
                    'battle_order' => $battle_order
                ];
                //当前数据库连接
                //$this->saveDataBase($insertData);
                $this->saveKafka($insertData);
            }
        }
        return true;
    }
    //获取英雄信息
    private function getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id){
        if ($this->memory_mode && !empty($this->match_all_champion_websocket[$champions_id])){
            return $this->match_all_champion_websocket[$champions_id];
        }
        $champion = null;
        //$championInfoRedis = $redis->hGet('lol:champions:list:'.$metadata_origin_id,$champions_id);
        $championInfoRedis = $redis->hGet('lol:champions:list',$champions_id);
        if ($championInfoRedis){
            $championInfo = @json_decode($championInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $redis->get("lol:champions:list:unknown");
            $championInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }
        if ($this->memory_mode){
            $this->match_all_champion_websocket[$champions_id] = $champion;
        }
        return $champion;
    }
    //处理  玩家处理道具
    private function conversionPlayerHandleItems($redis,$redis_base_name,$battleId,$match_data,$originId){
        $purchase_time = (string)round($match_data['timestamp']/1000);
        //道具
        $item_id = null;
        $is_wards = false;
        $rel_itemId = $match_data['itemId'];
        $itemsInfoList = $this->getLolItemsList($redis,$originId);
        $LolExternalIdItemsRelationList = $this->getLolItemsRelationList($redis,$originId);
        $itemInfoId = $LolExternalIdItemsRelationList[$rel_itemId];
        if($itemInfoId){
            //获取原始数据
            $itemInfoResRedis = $itemsInfoList[$itemInfoId];
            if ($itemInfoResRedis) {
                $item_id = $itemInfoId;
                $itemInfoRes = @json_decode($itemInfoResRedis, true);
                //如果买的是控制守卫
                if ($itemInfoRes['name'] == 'Control Ward' || $itemInfoRes['name'] == 'Stealth Ward'){
                    $is_wards = true;
                }
            }
        }
        //人物
        $player_order = $match_data['participantId'];
        $playerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$player_order);
        if ($playerInfoRedis){
            $playerInfo = @json_decode($playerInfoRedis,true);
            if ($is_wards){
                $playerInfo['wards_purchased'] = (Int)$playerInfo['wards_purchased'] + 1;
            }
            $playerInfoRes = @json_encode($playerInfo,320);
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$player_order,$playerInfoRes);
        }
        //记录购买记录
        $players_handle_item_info_key = $player_order.'_'.$item_id;
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_handle_item_info'],$players_handle_item_info_key,$purchase_time);
    }
    //处理 玩家 多杀
    private function conversionPlayerMultiKill($redis,$battleId,$match_data){
        $achievement = $match_data['achievement'];//类型
        if ($achievement == 'multi'){
            $player_order = $match_data['killer'];
            $playerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$player_order);
            if ($playerInfoRedis){
                $playerInfo = @json_decode($playerInfoRedis,true);
                $largest_multi_kill = (Int)$playerInfo['largest_multi_kill'];
                $killStreakLength = $match_data['killStreakLength'];
                switch ($killStreakLength){
                    case 2://2杀
                        $playerInfo['double_kill'] = (Int)$playerInfo['double_kill'] + 1;
                        break;
                    case 3://3杀
                        $double_kill = (Int)$playerInfo['double_kill'] - 1; //取消2杀次数
                        if ($double_kill < 0){
                            $double_kill = 0;
                        }
                        $playerInfo['double_kill'] = (Int)$double_kill;
                        $playerInfo['triple_kill'] = (Int)$playerInfo['triple_kill'] + 1;
                        break;
                    case 4://4杀
                        $triple_kill = (Int)$playerInfo['triple_kill'] - 1; //取消3杀次数
                        if ($triple_kill < 0){
                            $triple_kill = 0;
                        }
                        $playerInfo['triple_kill'] = (Int)$triple_kill;
                        $playerInfo['quadra_kill'] = (Int)$playerInfo['quadra_kill'] + 1;
                        break;
                    case 5://5杀
                        $quadra_kill = (Int)$playerInfo['quadra_kill'] - 1; //取消4杀次数
                        if ($quadra_kill < 0){
                            $quadra_kill = 0;
                        }
                        $playerInfo['quadra_kill'] = (Int)$quadra_kill;
                        $playerInfo['penta_kill'] = (Int)$playerInfo['penta_kill'] + 1;
                        break;
                }
                //大于才替换
                if ($killStreakLength > $largest_multi_kill ){
                    $playerInfo['largest_multi_kill'] = (Int)$killStreakLength;
                }
                //最终结果
                $playerInfoRes = @json_encode($playerInfo,320);
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$player_order,$playerInfoRes);
            }
        }
        return true;
    }
    //处理玩家最大连杀
    private function conversionPlayerLargestKillingSpree($redis,$battleId,$killer_rel_order_id,$victim_rel_order_id){
        $killerPlayerInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id);
        if ($killerPlayerInfoRedis) {
            $killerPlayerInfo = @json_decode($killerPlayerInfoRedis, true);
            //当前击杀数量
            $player_current_kill_num = $this->redis_hGet_Or_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num');
            if ($player_current_kill_num) {
                //最大连杀数量
                $player_largest_killing_spree_num = $this->redis_hGet_Or_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num');
                if ($player_largest_killing_spree_num > 0){
                    $player_current_kill_num_now = (Int)$player_current_kill_num + 1;
                    if ($player_current_kill_num_now > $player_largest_killing_spree_num){
                        $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num',$player_current_kill_num_now);
                        $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num',$player_current_kill_num_now);
                        //保存
                        $killerPlayerInfo['largest_killing_spree'] = $player_current_kill_num_now;
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killerPlayerInfo,320));
                    }
                }
            } else {
                $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_current_kill_num',1);
                if ($player_current_kill_num !== 0){
                    $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$killer_rel_order_id.'_largest_killing_spree_num',1);
                    $killerPlayerInfo['largest_killing_spree'] = 1;
                    //保存
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$killer_rel_order_id,@json_encode($killerPlayerInfo,320));
                }
            }
        }
        //被杀者
        if ($victim_rel_order_id > 0){
            $this->redis_hSet_And_Memory($redis,['current', 'battles', $battleId, 'players_largest_killing_spree'],$victim_rel_order_id.'_current_kill_num',0);
        }
        return true;
    }
    //执行ws to rest api
    private function refreshToApi($redis,$redis_base_name,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$event_type,$event_order){
        //判断是不是修复 并且比赛是不是结束
        if ($this->operate_type == 'xiufu') {
            $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
            if ($match_status_check == 3) {
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        $originId, "", "", 2),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => [
                        'matchId'      => $matchId,
                        'rel_matchId'  => $rel_matchId,
                        'battleId'     => $battleId,
                        'battleOrder'  => $battle_order,
                        'event_type'   => 'refresh_match_xiufu',
                        'event_order'  => 0
                    ],
                ];
                TaskRunner::addTask($item, 4);
            }
            return true;
        }
        $nowTime = time();
        $redisKey = $redis_base_name."ws_to_api";
        $refreshTime = $redis->get($redisKey);
        if ($refreshTime){
            //判断类型
            if ($event_type == 'frames'){
                $diff_time = $nowTime - $refreshTime;
                if ($diff_time >= 5){
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'      => $rel_matchId,
                            'battleId'      => $battleId,
                            'battleOrder'      => $battle_order,
                            'event_type'      => $event_type,
                            'event_order'      => 0
                        ],
                    ];
                    TaskRunner::addTask($item, 4);
                    $redis->set($redisKey, $nowTime);
                }
            }else{
                $event_type_array = ['battle_start','battle_end','battle_pause','battle_unpause','battle_reloaded','player_kill','player_suicide','building_kill','elite_kill'];
                if (in_array($event_type,$event_type_array)){
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'      => $rel_matchId,
                            'battleId'      => $battleId,
                            'battleOrder'      => $battle_order,
                            'event_type'      => $event_type,
                            'event_order'      => $event_order
                        ],
                    ];
                    TaskRunner::addTask($item, 4);
                    //判断最后一针刷新frames
                    if ($event_type == 'battle_end'){
                        $battle_end_item = [
                            "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                                $originId, "", "", 2),
                            "type"     => '',
                            "batch_id" => date("YmdHis"),
                            "params"   => [
                                'matchId'      => $matchId,
                                'rel_matchId'      => $rel_matchId,
                                'battleId'      => $battleId,
                                'battleOrder'      => $battle_order,
                                'event_type'      => 'frames',
                                'event_order'      => 0
                            ],
                        ];
                        TaskRunner::addTask($battle_end_item, 4);
                    }
                }
            }
        }else {
            $redis->set($redisKey, $nowTime);
        }
        return true;
    }
    //处理和获取
    private function getElitesStatus($redis,$battleId,$team_elites_Kills,$duration){
        //计算 elites_status
        $enumIngameGoalInfoList = $this->getEnumIngameGoalInfo($redis);
        //enum_socket_game_configure
        $enum_socket_game_configure_info = $this->enum_socket_game_configure;
        //rift_herald
        $rift_herald_first_time = $enum_socket_game_configure_info['rift_herald_first_time'];
        $rift_herald_quit_time = $enum_socket_game_configure_info['rift_herald_quit_time'];
        $rift_herald_reborn_time = $enum_socket_game_configure_info['rift_herald_reborn_time'];
        $rift_herald_exited = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited');
        if ($rift_herald_exited){
            $rift_herald_status_val = 'exited';
            $rift_herald_spawntimer = null;
        }else{
            $rift_herald_status_val = 'alive';
            $rift_herald_spawntimer = null;
            if ($duration < $rift_herald_first_time){
                $rift_herald_status_val = 'not_spawned';
                $rift_herald_spawntimer = $rift_herald_first_time - $duration;
            }elseif ($duration > $rift_herald_quit_time){
                $rift_herald_status_val = 'exited';
                $rift_herald_spawntimer = null;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
            }else{
                $rift_herald_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time');
                if ($rift_herald_kill_time){
                    $rift_herald_spawntimer_time = $duration - $rift_herald_kill_time;
                    if ($rift_herald_spawntimer_time < $rift_herald_reborn_time){
                        $rift_herald_status_val = 'not_spawned';
                        $rift_herald_spawntimer = $rift_herald_reborn_time - $rift_herald_spawntimer_time;
                    }
                }
            }
        }
        //rift_herald Info   1
        $rift_herald_array = @json_decode($enumIngameGoalInfoList[1],true);
        $rift_herald_name_val = @$rift_herald_array['ingame_obj_name'];
        $rift_herald_name_cn_val = @$rift_herald_array['ingame_obj_name_cn'];
        $rift_herald_status = [
            'status' => $rift_herald_status_val,
            'name' => self::checkAndChangeString($rift_herald_name_val),
            'name_cn' => self::checkAndChangeString($rift_herald_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($rift_herald_spawntimer)
        ];
        //dragon_status
        $dragon_first_time = $enum_socket_game_configure_info['dragon_first_time'];
        $dragon_reborn_time = $enum_socket_game_configure_info['dragon_reborn_time'];
        $dragon_quit_num = $enum_socket_game_configure_info['dragon_quit_num'];
        $elder_dragon_first_time = $enum_socket_game_configure_info['elder_dragon_first_time'];
        $elder_dragon_reborn_time = $enum_socket_game_configure_info['elder_dragon_reborn_time'];

        $dragon_status_status_val = 'alive';
        $dragon_status_spawntimer = null;
        $dragon_status_name = null;
        $dragon_status_name_cn= null;
        //判断大龙是不是被杀了
        $dragon_status_Elder_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'dragon_status'],'elder_dragon_kill_time');
        if ($dragon_status_Elder_kill_time) {
            $dragon_status_Elder_spawntimer_cha = $duration - $dragon_status_Elder_kill_time;
            if ($dragon_status_Elder_spawntimer_cha < $elder_dragon_reborn_time){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = $elder_dragon_reborn_time - $dragon_status_Elder_spawntimer_cha;
            }
            $dragon_status_name = 'Elder Dragon';
            $dragon_status_name_cn = '远古巨龙';
        }else{
            if ($duration < $dragon_first_time){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = $dragon_first_time - $duration;
            }else{
                $dragon_status_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'dragon_status'],'kill_time');
                if ($dragon_status_kill_time){
                    //首先判断两队是否累计杀了4个了  $dragon_quit_num 是4
                    $ElderDragonIsAlive = false;
                    foreach ($team_elites_Kills as $val_ek ){
                        if ($val_ek['dragon_kills'] >= $dragon_quit_num){
                            $ElderDragonIsAlive = true;
                        }
                    }
                    if ($ElderDragonIsAlive){
                        $dragon_status_Elder_spawned_cha = $duration - $dragon_status_kill_time;
                        $dragon_status_spawntimer = $elder_dragon_first_time - $dragon_status_Elder_spawned_cha;
                        if ($dragon_status_spawntimer > 0){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_status_spawntimer;
                        }else{
                            $dragon_status_status_val = 'alive';
                            $dragon_status_spawntimer = null;
                        }
                        $dragon_status_name = 'Elder Dragon';
                        $dragon_status_name_cn = '远古巨龙';
                    }else{
                        $dragon_status_spawntimer_cha = $duration - $dragon_status_kill_time;
                        if ($dragon_status_spawntimer_cha < $dragon_reborn_time){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_reborn_time - $dragon_status_spawntimer_cha;
                        }
                    }
                }
            }
        }
        $dragon_status = [
            'status' => $dragon_status_status_val,
            'name' => $dragon_status_name,
            'name_cn' => $dragon_status_name_cn,
            'spawntimer' => self::checkAndChangeInt($dragon_status_spawntimer)
        ];
        //baron_nashor_status  7
        $baron_nashor_first_time = $enum_socket_game_configure_info['baron_nashor_first_time'];
        $baron_nashor_reborn_time = $enum_socket_game_configure_info['baron_nashor_reborn_time'];
        $baron_nashor_spawntimer = null;
        $baron_nashor_array = @json_decode($enumIngameGoalInfoList[7],true);
        $baron_nashor_name_val = @$baron_nashor_array['ingame_obj_name'];
        $baron_nashor_name_cn_val = @$baron_nashor_array['ingame_obj_name_cn'];
        $baron_nashor_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time');
        if ($baron_nashor_kill_time){
            $baron_nashor_spawntimer_time = $duration - $baron_nashor_kill_time;
            if ($baron_nashor_spawntimer_time > $baron_nashor_reborn_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_reborn_time - $baron_nashor_spawntimer_time;
            }
        }else{
            if ($duration > $baron_nashor_first_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_first_time - $duration;
            }
        }
        $baron_nashor_status = [
            'status' => $baron_nashor_status_val,
            'name' => self::checkAndChangeString($baron_nashor_name_val),
            'name_cn' => self::checkAndChangeString($baron_nashor_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($baron_nashor_spawntimer)
        ];
        $elites_status = [
            'rift_herald_status' => $rift_herald_status,
            'dragon_status' => $dragon_status,
            'baron_nashor_status' => $baron_nashor_status
        ];
        return $elites_status;
    }
    //////////////////////////  获取比赛信息  /////////////////////////////////////
    //获取比赛ID
    public function getMatchId($redis,$originId,$rel_match_id){
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        if (empty($matchId)){
            return true;
        }
        if ($this->memory_mode){
            $this->matchId = $matchId;
        }
        return $matchId;
    }
    //设置比赛状态
    public function setMatchStatus($redis,$matchId){
        if ($this->memory_mode){
            $match_status = $this->match_status;
            if ($match_status){
                return true;
            }
            $this->match_status = 2;
        }
        $matchStatusRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_status";
        $match_status = $redis->get($matchStatusRedisName);
        if (!$match_status){
            $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
            if ($MatchRealTimeInfo){
                $MatchRealTimeInfo->setAttribute('status',2);
                $MatchRealTimeInfo->save();
                $redis->set($matchStatusRedisName,2);
            }
        }
        return true;
    }
    //获取比赛信息
    public function getMatchInfo($redis,$matchId,$socket_time){
        if ($this->memory_mode && !empty($this->matchInfo)){
            return $this->matchInfo;
        }
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $matchInfo = $redis->hGetAll($matchInfoRedisName);
        if (!$matchInfo){
            //获取比赛信息
            $matchInfo = self::getMatchAllInfoByMatchId($matchId);
            if (empty($matchInfo)){
                return 'no_match_info';
            }
            $redis->hMSet($matchInfoRedisName,$matchInfo);
            //setMatchRealTimeInfo
            $this->getMatchRealTimeInfo($redis,$matchId,$socket_time);
            $this->getInitTeamNameIdRelationInfo($redis,$matchId,$matchInfo);
            $this->getInitTeamInfo($redis,$matchId,$matchInfo);
            $this->getInitPlayerNameIdRelationInfo($redis,$matchId,$matchInfo);
            //$this->getInitPlayerInfo($redis,$matchId,$matchInfo);
        }
        //设置比赛
        if ($this->memory_mode){
            $this->matchInfo = $matchInfo;
        }
        return $matchInfo;
    }
    //获取比赛信息
    public function getMatchRealTimeInfo($redis,$matchId,$socket_time)
    {
        if ($this->memory_mode && !empty($this->matchRealTimeInfo)) {
            return $this->matchRealTimeInfo;
        }
        //match_real_time_info
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":".Consts::MATCH_REAL_TIME_INFO;
        $match_real_time_info = $redis->hGetAll($matchRealTimeInfoRedisName);
        if (!$match_real_time_info){
            $match_real_time_info = MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
            if ($this->operate_type == 'xiufu') {
                $match_real_time_info['team_1_score'] = 0;
                $match_real_time_info['team_2_score'] = 0;
                $match_real_time_info['winner'] = null;
                $match_real_time_info['status'] = 2;
            }
            $redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
        }
        //存内存
        if ($this->memory_mode){
            $this->matchRealTimeInfo = $match_real_time_info;
        }
        return $match_real_time_info;
    }
    //设置team relation各种信息
    public function getInitTeamNameIdRelationInfo($redis,$matchId,$matchInfo){
        if ($this->memory_mode && !empty($this->team_name_id_relation)) {
            return $this->team_name_id_relation;
        }
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $matchTeamIdRelationRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":team_name_id_relation";
        $team_name_id_relation = $redis->hGetAll($matchTeamIdRelationRedisName);
        if (!$team_name_id_relation){
            $Team1Info = Team::find()->where(['id'=>$matchInfo['team_1_id']])->asArray()->one();
            $Team1InfoName = @str_replace('.','',@$Team1Info['short_name']);
            $redis->hSet($matchTeamIdRelationRedisName,$Team1InfoName,$matchInfo['team_1_id']);
            $Team2Info = Team::find()->where(['id'=>$matchInfo['team_2_id']])->asArray()->one();
            $Team2InfoName = @str_replace('.','',@$Team2Info['short_name']);
            $redis->hSet($matchTeamIdRelationRedisName,$Team2InfoName,$matchInfo['team_2_id']);
            $team_name_id_relation = [
                $Team1Info['short_name'] =>$matchInfo['team_1_id'],
                $Team2Info['short_name'] =>$matchInfo['team_2_id']
            ];
        }
        if ($this->memory_mode){
            $this->team_name_id_relation = $team_name_id_relation;
        }
        return $team_name_id_relation;
    }
    //设置team 各种信息
    public function getInitTeamInfo($redis,$matchId,$matchInfo){
        if ($this->memory_mode && !empty($this->teams_info)){
            return $this->teams_info;
        }
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $teams_info = $redis->hGetAll($matchTeamInfoRedisName);
        if (!$teams_info){
            $Team1Info = Team::find()->where(['id'=>$matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])){
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team1Info['id'],@json_encode($Team1Info,320));
            $Team2Info = Team::find()->where(['id'=>$matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])){
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team2Info['id'],@json_encode($Team2Info,320));
            $teams_info = [
                $Team1Info['id'] =>@json_encode($Team1Info,320),
                $Team2Info['id'] =>@json_encode($Team2Info,320)
            ];
        }
        if ($this->memory_mode){
            $this->teams_info = $teams_info;
            $this->setDataToMemory(['teams_info'],$teams_info);
        }
        return $teams_info;
    }
    //初始化玩家 relation
    public function getInitPlayerNameIdRelationInfo($redis,$matchId,$matchInfo){
        if ($this->memory_mode && !empty($this->player_name_id_relation)){
            return $this->player_name_id_relation;
        }
        //根据队伍ID 获取player 战队选手快照   选手表
        $playerNameIdRelationRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_name_id_relation";
        $player_name_id_relation = $redis->hGetAll($playerNameIdRelationRedisName);
        if (!$player_name_id_relation){
            $player_info_save = false;
            $playerInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_info";
            $player_info = $redis->hGetAll($playerInfoRedisName);
            if (!$player_info){
                $player_info_save = true;
            }
            $team_ids = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
            $playersInfo = TeamSnapshot::find()->alias('ts')->select('p.player_id,p.nick_name')
                ->leftJoin('team_player_relation_snapshot as tpr','ts.id = tpr.team_id')
                ->leftJoin('player_snapshot as p','p.relation_id = tpr.id')
                ->where(['ts.relation_id'=>$matchInfo['tournament_id']])
                ->andWhere(['in', 'ts.team_id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val){
                $player_name_id_relation[@$val['nick_name']] = @$val['player_id'];
                if ($player_info_save) {
                    $player_info[@$val['player_id']] = @json_encode($val, 320);
                }
            }
            $redis->hMSet($playerNameIdRelationRedisName,$player_name_id_relation);
            if ($player_info_save){
                if ($this->memory_mode) {
                    $this->player_info = $player_info;
                }
                $redis->hMSet($playerInfoRedisName,$player_info);
            }
        }
        if ($this->memory_mode){
            $this->player_name_id_relation = $player_name_id_relation;
        }
        return $player_name_id_relation;
    }
    //初始化玩家
    public function getInitPlayerInfo($redis,$matchId,$matchInfo){
        if ($this->memory_mode && !empty($this->player_info)){
            return $this->player_info;
        }
        //根据队伍ID 获取player 战队选手快照   选手表
        $playerInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_info";
        $player_info = $redis->hGetAll($playerInfoRedisName);
        if (!$player_info){
            $player_name_id_relation_save = false;
            $playerNameIdRelationRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_name_id_relation";
            $player_name_id_relation = $redis->hGetAll($playerNameIdRelationRedisName);
            if (!$player_name_id_relation){
                $player_name_id_relation_save = true;
            }
            $team_ids = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
            $playersInfo = TeamSnapshot::find()->alias('ts')->select('p.id,p.nick_name')
                ->leftJoin('team_player_relation_snapshot as tpr','ts.id = tpr.team_id')
                ->leftJoin('player_snapshot as p','p.relation_id = tpr.id')
                ->where(['ts.relation_id'=>$matchInfo['tournament_id']])
                ->andWhere(['in', 'ts.team_id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val){
                if ($player_name_id_relation_save){
                    $player_name_id_relation[@$val['nick_name']] = @$val['id'];
                }
                $player_info[@$val['id']] = @json_encode($val, 320);
            }
            $redis->hMSet($playerInfoRedisName,$player_info);
            if ($player_name_id_relation_save){
                if ($this->memory_mode) {
                    $this->player_name_id_relation = $player_name_id_relation;
                }
                $redis->hMSet($playerNameIdRelationRedisName,$player_name_id_relation);
            }
        }
        if ($this->memory_mode){
            $this->player_info = $player_info;
        }
        return $player_info;
    }
    //获取LOL Items Relation
    public function getLolItemsRelationList($redis,$originId)
    {
        if ($this->memory_mode) {
            //$strValue = $this->lol_items_relation[$originId];
            $strValue = $this->lol_items_relation;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:items:relation:".$originId);
        $returnValue = $redis->hGetAll("lol:items:external_id_relation_list");
        if ($this->memory_mode) {
            //return $this->lol_items_relation[$originId] = $returnValue;
            $this->lol_items_relation = $returnValue;
        }
        return $returnValue;
    }
    //获取LOL Items Relation
    public function getLolItemsList($redis,$originId)
    {
        if ($this->memory_mode) {
            //$strValue = $this->lol_items_list[$originId];
            $strValue = $this->lol_items_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll("lol:items:list:".$originId);
        $returnValue = $redis->hGetAll("lol:items:list");
        if ($this->memory_mode) {
            //return $this->lol_items_list[$originId] = $returnValue;
            $this->lol_items_list = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getLolItemsUnknownId($redis){
        if ($this->memory_mode) {
            $strValue = $this->lol_item_unknown_id;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $item_unknown_id = null;
        $item_unknown_redis = $redis->get('lol:items:list:unknown');
        $item_unknown_info = @json_decode($item_unknown_redis,true);
        $item_unknown_id = @$item_unknown_info['id'];
        if ($this->memory_mode) {
            $this->lol_item_unknown_id = $item_unknown_id;
        }
        return $item_unknown_id;
    }
    //召唤师技能 name relation id
    public function getSummonerSpellNameRelationIdList($redis,$originId){

        if ($this->memory_mode) {
            //$strValue = $this->lol_summoner_spell_name_relation_id_list[$originId];
            $strValue = $this->lol_summoner_spell_name_relation_id_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll('lol:summoner_spell:name_relation_id:'.$originId);
        $returnValue = $redis->hGetAll('lol:summoner_spell:external_name_relation_list');
        if ($this->memory_mode) {
            //return $this->lol_summoner_spell_name_relation_id_list[$originId] = $returnValue;
            $this->lol_summoner_spell_name_relation_id_list = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 列表
    public function getSummonerSpellInfoList($redis,$originId){
        if ($this->memory_mode) {
            //$strValue = $this->lol_summoner_spell_list[$originId];
            $strValue = $this->lol_summoner_spell_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        //$returnValue = $redis->hGetAll('lol:summoner_spell:list:'.$originId);
        $returnValue = $redis->hGetAll('lol:summoner_spell:list');
        if ($this->memory_mode) {
            //return $this->lol_summoner_spell_list[$originId] = $returnValue;
            $this->lol_summoner_spell_list = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 webscoket 信息
    public function getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_summoner_spells){
        $player_summoner_spells_res = [];
        foreach ($player_summoner_spells as $val_s){
            $player_summoner_spell_info = [];
            $summoner_spell_id_is_unknown = $val_s['is_unknown'];
            if ($summoner_spell_id_is_unknown == 1){
                $player_summoner_spell_info['summoner_spell_id'] = (int)@$val_s['unknown_info']['id'];
                $player_summoner_spell_info['name'] = (String)@$val_s['unknown_info']['name'];
                $player_summoner_spell_info['name_cn'] = (String)@$val_s['unknown_info']['name_cn'];
                $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$val_s['unknown_info']['external_id']);
                $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$val_s['unknown_info']['external_name']);
                $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$val_s['unknown_info']['slug']);
                $summoner_spell_info_image = !empty(@$val_s['unknown_info']['image']) ? (string)@$val_s['unknown_info']['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                $player_summoner_spell_info['cooldown'] = null;
            }else{
                //$summoner_spell_info_redis = $redis->hGet("lol:summoner_spell:list:".$metadata_origin_id,$val_s['spell_id']);
                $summoner_spell_info_redis = @$summonerSpellInfoList[$val_s['spell_id']];
                if ($summoner_spell_info_redis){
                    $summoner_spell_info = @json_decode($summoner_spell_info_redis,320);
                    $player_summoner_spell_info['summoner_spell_id'] = (int)@$summoner_spell_info['id'];
                    $player_summoner_spell_info['name'] = (String)@$summoner_spell_info['name'];
                    $player_summoner_spell_info['name_cn'] = (String)@$summoner_spell_info['name_cn'];
                    $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$summoner_spell_info['external_id']);
                    $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$summoner_spell_info['external_name']);
                    $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$summoner_spell_info['slug']);
                    $summoner_spell_info_image = !empty(@$summoner_spell_info['image']) ? (string)@$summoner_spell_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                    $player_summoner_spell_info['cooldown'] = null;
                }
            }
            $player_summoner_spells_res[] = $player_summoner_spell_info;
        }
        return $player_summoner_spells_res;
    }
    //item websocket 信息
    public function getWebSocketPlayerItemsList($lolItemsList,$player_items_rel){
        $player_items = [];
        foreach ($player_items_rel as $key_item => $val_item){
            $player_items_val = [];
            if ($val_item){
                $item_info_data = @$lolItemsList[$val_item['item_id']];
                if ($item_info_data){
                    $item_info = @json_decode($item_info_data,true);
                    $player_items_val['item_id'] = (int)$item_info['id'];
                    $player_items_val['name'] = (string)$item_info['name'];
                    $player_items_val['name_cn'] = (string)$item_info['name_cn'];
                    $player_items_val['external_id'] = self::checkAndChangeString($item_info['external_id']);
                    $player_items_val['external_name'] = self::checkAndChangeString($item_info['external_name']);
                    $player_items_val['total_cost'] = (int)$item_info['total_cost'];
                    $player_items_val['is_trinket'] = @$item_info['is_trinket'] == 1 ? true : false;
                    $player_items_val['is_purchasable'] = @$item_info['is_purchasable'] == 1 ? true : false;
                    $player_items_val['slug'] = (string)$item_info['slug'];
                    $player_item_image = @$item_info['image'] ? @$item_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    //$player_item_image = $item_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50';
                    $player_items_val['image'] = $player_item_image;
                    $player_items_val['purchase_time'] = self::checkAndChangeString($val_item['purchase_time']);
                    $player_items_val['cooldown'] = null;
                }
            }
            $key_name_item = $key_item + 1;
            $player_items['slot_'.$key_name_item] = $player_items_val;
        }
        return $player_items;
    }
    //获取地图信息
    private function getMapInfoByRedisOrMemory($redis,$map_id){
        $map = null;
        if ($this->memory_mode && !empty($this->map)){
            return $this->map;
        }
        $mapInfoRedis = $redis->hGet(Consts::GAME_MAPS.":lol",$map_id);
        if ($mapInfoRedis){
            $mapInfo = @json_decode($mapInfoRedis,true);
            $map_is_default = @$mapInfo['is_default'] == 1 ? true : false;
            $map_image = [
                'square_image' => null,
                'rectangle_image' => null,
                'thumbnail' => (String)@$mapInfo['image']
            ];
            $map = [
                'map_id' => (Int)@$mapInfo['id'],
                'name' => (String)@$mapInfo['name'],
                'name_cn' => self::checkAndChangeString(@$mapInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$mapInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$mapInfo['external_name']),
                'short_name' => self::checkAndChangeString(@$mapInfo['short_name']),
                'map_type' => self::checkAndChangeString(@$mapInfo['map_type']),
                'map_type_cn' => self::checkAndChangeString(@$mapInfo['map_type_cn']),
                'is_default' => $map_is_default,
                'slug' => self::checkAndChangeString(@$mapInfo['slug']),
                'image' => $map_image
            ];
        }
        if ($this->memory_mode){
            $this->map = $map;
        }
        return $map;
    }
    //获取符文信息
    private function getWebSocketPlayerKeystoneInfo($redis,$originId,$player_id,$keystone_id){
        $keyString = 'player_keystone_websocket';
        if ($this->memory_mode) {
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$player_id];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $keystone = null;
        //$keystone_info_redis = $redis->hGet("lol:rune:list:".$originId,$keystone_id);
        $keystone_info_redis = $redis->hGet("lol:rune:list",$keystone_id);
        if ($keystone_info_redis){
            $keystone_image = null;
            $keystone_info = @json_decode($keystone_info_redis,true);
            if ($keystone_info['image']){
                $keystone_image = $keystone_info['image']."?x-oss-process=image/resize,m_fixed,h_50,w_50";
            }
            $keystone = [
                'rune_id' => (int)$keystone_info['id'],
                'name' => self::checkAndChangeString(@$keystone_info['name']),
                'name_cn' => self::checkAndChangeString(@$keystone_info['name_cn']),
                'external_id' => self::checkAndChangeString(@$keystone_info['external_id']),
                'external_name' => self::checkAndChangeString(@$keystone_info['external_name']),
                'path_name' => self::checkAndChangeString(@$keystone_info['path_name']),
                'path_name_cn' => self::checkAndChangeString(@$keystone_info['path_name_cn']),
                'slug' => self::checkAndChangeString(@$keystone_info['slug']),
                'image' => $keystone_image
            ];
        }
        if ($this->memory_mode) {
            if (!empty($keystone)){
                $this->current[$keyString][$player_id] = $keystone;
            }
        }
        return $keystone;
    }
    //获取EnumIngameGoal信息
    private function getEnumIngameGoalInfo($redis){
        if ($this->memory_mode) {
            $strValue = $this->enum_ingame_goal_info;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('enum:ingame_goal');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->enum_ingame_goal_info = $returnValue;
            }
        }
        return $returnValue;
    }
    //socket录入服务器配置
    private function setSocketGameConfigure($game_type){
        $enum_socket_lol = $this->enum_socket_game_configure;
        if (empty($enum_socket_lol)){
            $this->enum_socket_game_configure = $this->getSocketGameConfigure($game_type);
        }
        return true;
    }
    //////////////////////////    redis 和 内存      ///////////////////////////////////
    //获取数组 key
    public function redis_hKeys_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_keys($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hKeys($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组 key
    public function redis_hVals_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_values($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hVals($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetTeamAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 2){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetPlayersAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 10 ){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取hash 单条
    public function redis_hGet_Or_Memory($redis,$keys,$field)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$field];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGet($keyString,$field);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
//            if (array_key_exists($keyString, $this->current)){
//                $this->current[$keyString][$field] = $returnValue;
//            }else{
//                if (!empty($returnValue)){
//                    $this->current[$keyString][$field] = $returnValue;
//                }
//            }
            //重新赋值所有
            $allData = $redis->hGetAll($keyString);
            if ($allData){
                $this->current[$keyString] = $allData;
            }
        }
        return $returnValue;
    }
    //获取 get 单条
    public function redis_Get_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->get($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取长度
    public function redis_hLen_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return count($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hLen($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue){
                $this->setDataToMemory($keys,$returnAllValue);
            }
        }
        return $returnValue;
    }
    //设置缓存 内存
    public function redis_hMSet_And_Memory($redis,$keys,$values)
    {
        if ($values){
            $matchId = $this->matchId;
            $keyString = implode(":", $keys);
            $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
            $redis->hMSet($keyString,$values);
            if ($this->memory_mode) {
                $memory_values = $redis->hGetAll($keyString);
                $keyString = implode("_", $keys);
                $this->current[$keyString] = $memory_values;
            }
        }
        return true;
    }
    //设置缓存 内存
    public function redis_hSet_And_Memory($redis,$keys,$filedKey,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->hSet($keyString,$filedKey,$values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString][$filedKey] = $values;
        }
        return true;
    }
    //设置缓存 内存
    public function redis_Set_And_Memory($redis,$keys,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->set($keyString, $values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $values;
        }
        return true;
    }
    //设置缓存 单独设置 内存
    public function setDataToMemory($keys,$value){
        if ($this->memory_mode && $value) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $value;
        }
        return true;
    }
    //保存到DB
    public function saveDataBase($data){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $data);
        $db_websoket->execute();
    }
    //保存到Kafka
    public function saveKafka($data){
        if ($this->operate_type == 'xiufu'){
            return true;
        }
        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_LOL'),$data);
    }
    //清空redis和内存
    public function delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id){
        //内存清空
        $this->current = [];
        //清空redis
        //删除关联关系
        $redis_match_lol_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":lol:".$origin_id;
        $redis->hdel($redis_match_lol_relation,$rel_match_id);
        //新的结构删除
        $match_all_info_list = $redis->Keys('match_all_info:'.$match_id."*");
        if ($match_all_info_list) {
            foreach ($match_all_info_list as $item_mai) {
                $redis->del($item_mai);
            }
        }
        return true;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
