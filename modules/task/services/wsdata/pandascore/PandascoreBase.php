<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/9/9
 * Time: 22:13
 */
namespace app\modules\task\services\wsdata\pandascore;

use Redis;

class PandascoreBase
{
    // Api
    const CSGO_ROUND_START = 'round_start';
    const CSGO_EVENT_KILL = 'kill';
    const CSGO_PLAYER_KILL = 'player_kill';
    const CSGO_ROUND_END = 'round_end';
    const MATCH_FINISHED_STATUS = 1;
    const MATCH_START_STATUS = 1;
    const MATCH_END_STATUS = 3;
    const BATTLE_FINISHED_ONGOING = 2;
    const BATTLE_FINISHED_PAST = 3;
    const ROUND_START_ONE = 1;
    const CSGO_EVENT_INFO_TYPE_NO = 'no';
    const CSGO_EVENT_INFO_TYPE_EVENTS = 'events';

    // Match数据结构
    public static $matchFramesInfoConfig = [
        'team_1_score' => 0,
        'team_2_score' => 0,
//        'is_battle_detailed' => 1,
//        'is_draw' => 2,
//        'is_forfeit' => 2,
//        'finished' => 1,
//        'default_advantage' => null,
//        'status' => 2,
//        'winner' => null,
//        'map_info' => ''
    ];
    // match Map地图数组
    public static $matchMapArray = [];
    // battle数据结构
    public static $battleFramesInfoConfig = [
        'game' => 1,
        'match' => null,
        'end_at' => '',
        'is_forfeit' => 2,
        'status' => null,
        'order' => null,
        'duration' => '',
        'map' => null,
        'winner' => null,
        'is_battle_detailed' => 1,
        'is_draw' => 2,
        'flag' => 1
    ];
    // battle Csgo 地图
    public static $battleMap = [];
    // 武器Id
    public static $weaponNameCreId = [];
    // 主客 战队
    public static $teamIds = [];
    // playersId
    public static $playerIds = [];
    // 有效回合数组转Json
    public static $liveRounds = [];
    // teams数据结构
    public static $teamStaticConfig = [];
    public static $teamStaticConfigTpl = [
        'order' => null,
        'battle_id' => null,
        'score' => 0,
        'identity_id' => '',
        'rel_identity_id' => '',
        'team_id' => null,
        'name' => '',
//        'starting_faction_side' => '',
//        '1st_half_score' => 0,
//        '2nd_half_score' => 0,
//        'ot_score' => 0
    ];
    // battle下players数据结构
    public static $playersBattleConfig = [
        'battle_id' => null,
        'game' => 1,
        'match' => null,
        'order' => null,
        'team_order' => null,
        'team_id' => null,
        'player_id' => null,
        'nick_name' => '',
        'kills' => 0,
//        'headshot_kills' => null, //爆头击杀
        'deaths' => 0,
        'k_d_diff' => null, //击杀死亡差
        'first_kills' => 0, //首先击杀
        'first_deaths' => 0, //首先死亡
        'first_kills_diff' => 0, //首杀差
        'primary_weapon' => null, //主武器ID
        'hp' => null, //血量
        'is_alive' => null, //存活状态
    ];
    // 对局选手数组
    public static $battlePlayersArray = [];
    // 回合数据
    public static $battleRoundConfig = [
        'battle_id' => null, //对局id
        'round_ordinal' => null, //回合序号
//        'winner_team_id' => null, //获胜战队
//        'winner_end_type' => null, //结束方式
    ];
    // 回合阵营数组
    public static $roundSidesArray = [];
    // 回合阵营数据结构
    public static $roundSideFieldConfig = [
        'battle_id' => null, //对局id
        'game' => 1, //游戏项目
        'match' => null, //所属比赛
        'order' => null, //排序
        'side_order' => null, //主队 客队1或者2
        'round_ordinal' => null, //回合序号
        'side' => '', //阵营
        'team_id' => null, //战队ID
//        'winner_end_type' => null, //结束方式
        'survived_players' => 0, //存活人数
        'kills' => null, //击杀
        'is_winner' => null, //是否为获胜方
        'is_bomb_planted' => null, //是否安放了炸弹
    ];
    // 回合选手数据
    public static $roundPlayersFieldConfig = [
        'battle_id' => null, //对局id
        'game' => 1, //游戏项目
        'match' => null, //所属比赛
        'order' => null, //排序
        'player_order' => null, //主队 客队1或者2
        'round_ordinal' => null, //回合序号
        'side' => '', //阵营
        'team_id' => null, //战队ID
        'player_id' => null, //选手ID
        'nick_name' => '', //选手昵称 优先使用数据源给的昵称
        'kills' => null, //击杀
        'is_died' => null, //是否死亡
        'is_first_kill' => null, //
        'is_first_death' => null, //
        'is_multi_kill' => null, //
        'is_ace_kill' => null, //
    ];
    // 回合event事件数据结构
    public static $roundEventsConfig = [
        'battle_id' => null, //对局id
        'game' => 1, //游戏项目
        'match' => null, //所属比赛
        'order' => null, //排序
        'event_type' => '', //事件类型
        'round_ordinal' => null, //回合序号
        'in_round_timestamp' => '', //回合内时间戳
        'round_time' => '', //回合内时间
        'is_bomb_planted' => null, //炸弹是否已安放
        'time_since_plant' => '', //安放炸弹后时间
        'killer' => null, //击杀者ID
        'killer_nick_name' => null, //击杀者选手昵称
        'killer_side' => null, //击杀者阵营
        'victim' => null, //受害者ID
        'victim_nick_name' => null, //受害者ID
        'victim_side' => null, //受害者阵营
        'round_end_type' => null, //回合获胜方式
        'winner_side' => null, //获胜方阵营
    ];
    // 初始化battle players
    public static function playersBattleConfigInfo(){
        PandascoreBase::$playersBattleConfig = [
            'battle_id' => null,
            'game' => 1,
            'match' => null,
            'order' => null,
            'team_order' => null,
            'team_id' => null,
            'player_id' => null,
            'nick_name' => '',
            'kills' => 0,
//            'headshot_kills' => null, //爆头击杀
            'deaths' => 0,
            'k_d_diff' => null, //击杀死亡差
            'first_kills' => 0, //首先击杀
            'first_deaths' => 0, //首先死亡
            'first_kills_diff' => 0, //首杀差
            'primary_weapon' => null, //主武器ID
            'hp' => null, //血量
            'is_alive' => null, //存活状态
        ];
    }
    // 初始化round players
    public static function roundPlayersFieldConfigInfo(){
        PandascoreBase::$roundPlayersFieldConfig = [
            'battle_id' => null, //对局id
            'game' => 1, //游戏项目
            'match' => null, //所属比赛
            'order' => null, //排序
            'player_order' => null, //主队 客队1或者2
            'round_ordinal' => null, //回合序号
            'side' => '', //阵营
            'team_id' => null, //战队ID
            'player_id' => null, //选手ID
            'nick_name' => '', //选手昵称 优先使用数据源给的昵称
            'kills' => null, //击杀
            'is_died' => null, //是否死亡
            'is_first_kill' => null, //
            'is_first_death' => null, //
            'is_multi_kill' => null, //
            'is_ace_kill' => null, //
        ];
    }
    // 初始化round events
    public static function roundEventsConfigInfo(){
        PandascoreBase::$roundEventsConfig = [
            'battle_id' => null, //对局id
            'game' => 1, //游戏项目
            'match' => null, //所属比赛
            'order' => null, //排序
            'event_type' => '', //事件类型
            'round_ordinal' => null, //回合序号
            'in_round_timestamp' => '', //回合内时间戳
            'round_time' => '', //回合内时间
            'is_bomb_planted' => null, //炸弹是否已安放
            'time_since_plant' => '', //安放炸弹后时间
            'killer' => null, //击杀者ID
            'killer_nick_name' => null, //击杀者选手昵称
            'killer_side' => null, //击杀者阵营
            'victim' => null, //受害者ID
            'victim_nick_name' => null, //受害者ID
            'victim_side' => null, //受害者阵营
            'round_end_type' => null, //回合获胜方式
            'winner_side' => null, //获胜方阵营
        ];
    }

    // 回合事件数组
    public static $roundEventsArray = [];
    // 回合选手数组
    public static $roundPlayersArray = [];

    // Ws

    const PREFIX_PWS = 'Pws';
    const PREFIX_PAPI = 'Papi';
    const Symbol = ':';

    public static $matchId;
    public $redis;
    private static $instance;
    public function __construct()
    {
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(0);
        $this->redis = $redis;
    }

    public static function getInstance()
    {
        if(!self::$instance){
            self::$instance=new self();
        }
        return self::$instance;
    }
    /**
     *
     * Redis
     * 封装方法
     * 可理解为（数据库操作）
     *
     */
    // key-value 存值
    public function valueSet($key, $value)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->set($tableName,$value);
    }
    // key-value 取值
    public function valueGet($key)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->get($tableName);
    }
    // key-value 取值
    public function valueBeginAtGet($keyString)
    {
//        $matchId   = self::$matchId;
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->get($tableName);
    }
    // key-value 将指定key存储的数字值增加1。若key不存在会先初始化为0再增加1，若key存储的不是整数值则返回false。成功返回key新值
    public function valueIncr($key)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->incr($tableName);
    }
    // hash 存值多个字段值
    public function hashMset($key, $arrayValue){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hMset($tableName,$arrayValue);
    }
    // hash 获取某个表的多个字段值。其中不存在的字段值为false。
    public function hashMget($key, $fieldArray){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hMget($tableName,$fieldArray);
    }
    // hash 表中的字段赋值。成功返回1，失败返回0。若user表不存在会先创建表再赋值，若字段已存在会覆盖旧值。
    public function hashSet($key, $field, $value){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hSet($tableName,$field,$value);
    }
    // hash 获取表中指定字段的值。若表不存在则返回false。
    public function hashGet($key, $field){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hGet($tableName,$field);
    }
    // hash 获取某个表中所有的字段和值。
    public function hashGetAll($key){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hGetAll($tableName);
    }
    // hash 删除表的一个字段，不支持删除多个字段。成功返回1，否则返回0。
    public function hashDel($key, $field){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->hDel($tableName,$field);
    }
    // list 在列表头部插入一个值，当列表不存在时自动创建一个列表
    public function listLpush($key, $value){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->lpush($tableName,$value);
    }
    // list 在列表尾部插入一个值，当列表不存在时自动创建一个列表
    public function listRpush($key, $value){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->rpush($tableName,$value);
    }
    // list 将一个值插入已存在的列表头部，列表不存在时操作无效
    public function listrPushx($key, $value){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->rPushx($tableName,$value);
    }
    // list 删除列表的第一个元素并返回列表和列表的第一个元素，当 $tableName 不存在或不是列表则返回false
    public function listLpop($key){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->lPop($tableName);
    }
    // list 删除列表的最后一个元素并返回列表和列表的最后一个元素，当 $tableName 不存在或不是列表则返回false
    public function listRpop($key){
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->rPop($tableName);
    }
    // list 获取存储的数据并输出列表下标 $offset 到 $limit 的数据
    public function listLrange($key, $offset, $limit)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->lRange($tableName, $offset, $limit);
    }
    // Set 将一个元素加入集合，已经存在集合中的元素则忽略。若集合不存在则先创建，若key不是集合类型则返回false，插入成功返回1。
    public function setsAdd($key, $value)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->sAdd($tableName, $value);
    }
    // Set 返回集合中所有成员。
    public function setsMembers($key)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->sMembers($tableName);
    }
    // Set 判断集合里是否存在指定元素，是返回true，否则返回false。
    public function setsSismember($key, $value)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->sismember($tableName, $value);
    }
    // Set 删除集合中指定的一个元素，元素不存在返回0。删除成功返回1，否则返回0。
    public function setsSrem($key, $value)
    {
        $matchId   = self::$matchId;
        $keyString = implode(":", $key);
        $keyString = sprintf("%s:%s", $matchId, $keyString);
        $tableName = self::PREFIX_PWS.self::Symbol.$keyString;
        return $this->redis->srem($tableName, $value);
    }

    // 删除redis
    public function batchDelRedisKey($redis_key){
        if (!$redis_key||$redis_key=='*'){
            return false;
        }
        $redis = $this->redis;
        $pre = $redis_key;
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 5)) {
            call_user_func_array([$redis, 'del'], $arr_keys);
            echo var_export($arr_keys, true) . PHP_EOL;
        }
        return true;
    }
}