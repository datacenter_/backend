<?php
namespace app\modules\task\services\wsdata\pandascore;

use app\modules\common\services\WinnerHelper;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleRoundCsgo;
use app\modules\match\models\MatchBattleRoundSideCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\task\services\hot\HotBase;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\match\models\MatchBattleRoundEventCsgo;
use Redis;

class PandascoreCsgoApi extends HotBase
{
    public $operate_type = '';
    public static function run($tagInfo, $taskInfo)
    {
        $gameId = @$tagInfo['ext_type'];
        // 统计更新状态，如果是重刷，则取到当前比赛下所有的消息，批量更新
        $refreshType=@$tagInfo['ext2_type'];
        $refreshType=$refreshType?$refreshType:"one";
//        $refreshType='refresh';
        $info = json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $relMatchId = $info['match_id'];
        $socketTime = date('Y-m-d H:i:s');
        if (isset($info['socket_time']) && $info['socket_time']){
            $socketTime = $info['socket_time'];
        }
        $matchId = self::getMainIdByRelIdentityId('match', $relMatchId, $originId, 1);
        // 设置matchId
        PandascoreBase::$matchId = $matchId;
        //获取比赛信息
        $matchInfo = self::getMatchAllInfoByMatchId($matchId);
        if($refreshType=='refresh'){
            // 清除redis
            $PandascoreBase = new PandascoreBase();
            $PandascoreBase->batchDelRedisKey("Pws:{$matchId}");
            $PandascoreBase->batchDelRedisKey("match_battle_info:{$matchId}");
            $list=\app\modules\match\models\MatchLived::find()->where(['game_id'=>1,'match_id'=>$relMatchId])->orderBy('socket_time')->limit(500000)->asArray()->all();
            $lastRoundOrder=null;
            $currentRoundOrder=null;
            $lastInfo=null;
            foreach($list as $k=>$v){
                $info=$v;
                $info['match_data']=json_decode($info['match_data'],true);
                $socketTime = date('Y-m-d H:i:s');
                if (isset($info['socket_time']) && $info['socket_time']){
                    $socketTime = $info['socket_time'];
                }
                // 循环获取到每回合最后一条frame，刷新frame
                if(@$info['type']==PandascoreBase::CSGO_EVENT_INFO_TYPE_NO){
                    $currentRoundOrder=@$info['match_data']['round'];
                    if($currentRoundOrder==$lastRoundOrder){
                        $lastInfo=$info;
                        continue;
                    }else{
                        // 执行上次frame和当前frame
                        if($lastInfo){
                            $conversionRes = self::transFrames($lastInfo,$matchInfo,$socketTime);
                            if (!empty($conversionRes))
                                self::refreshInfo($matchId,$conversionRes);
                            if (isset($conversionRes['match']['status']) && $conversionRes['match']['status'] ==3){
                                self::changeHotData($originId,$gameId,$matchId);
                            }
                        }
                        $conversionRes = self::transFrames($info,$matchInfo,$socketTime);
                        if (!empty($conversionRes))
                            self::refreshInfo($matchId,$conversionRes);
                        $lastInfo=$info;
                        $lastRoundOrder=$currentRoundOrder;
                        if (isset($conversionRes['match']['status']) && $conversionRes['match']['status'] ==3){
                            self::changeHotData($originId,$gameId,$matchId);
                        }
                    }
                }
                if(@$info['type']==PandascoreBase::CSGO_EVENT_INFO_TYPE_EVENTS){
                    self::transEvents($info,$matchId,$socketTime);
                }
            }
            // 刷最后一条
            if($lastInfo){
                $conversionRes = self::transFrames($lastInfo,$matchInfo,$socketTime);
                if (!empty($conversionRes))
                    self::refreshInfo($matchId,$conversionRes);
                if (isset($conversionRes['match']['status']) && $conversionRes['match']['status'] ==3){
                    self::changeHotData($originId,$gameId,$matchId);
                }
            }
        }else{
            //判断事件的类型 websocket类型
            switch ($info['type'])
            {
                case PandascoreBase::CSGO_EVENT_INFO_TYPE_NO:
                    $conversionRes = self::transFrames($info,$matchInfo,$socketTime);
                    if (!empty($conversionRes))
                        self::refreshInfo($matchId,$conversionRes);
                    if (isset($conversionRes['match']['status']) && $conversionRes['match']['status'] ==3){
                        self::changeHotData($originId,$gameId,$matchId);
                    }
                    break;
                case PandascoreBase::CSGO_EVENT_INFO_TYPE_EVENTS:
                    self::transEvents($info,$matchId,$socketTime);
                    break;
            }
        }
        return ;
    }

    // 是否安放了炸弹数组
    public static $bombPlantedArray = [];
    /**
     * @param $info
     * @param $matchInfo
     * @param $socketTime
     * 处理frame事件
     */
    public static function transFrames($info,$matchInfo,$socketTime){
        $csgoMatchData = $info['match_data'];
        if (isset($csgoMatchData['type'])) return null;
        if(!$csgoMatchData['game']['id']) return null;
        //找到对应的平台的match_id
        $battleInfo = self::getbattleIdByMatchIdAndOrder($matchInfo['id'],$csgoMatchData['match']['id'],
            $csgoMatchData['game']['id'],$socketTime);
        $battleId = $battleInfo['battle_id'];
        // match初始化
        $matchConfigArray = [
            'is_battle_detailed' => 1,
            'is_draw' => 2,
            'is_forfeit' => 2,
            'finished' => 1,
            'default_advantage' => null,
            'status' => 2,
            'winner' => null,
            'map_info' => ''
        ];
        // 比赛开始
        $battleFinishedStatus = PandascoreBase::BATTLE_FINISHED_ONGOING;
        if($battleInfo['match_start']==PandascoreBase::MATCH_START_STATUS){
            $matchConfigArray['team_1_score'] = 0;
            $matchConfigArray['team_2_score'] = 0;
            $matchConfigArray['begin_at'] = $socketTime;
        }
        //查看当前battle是否结束
        if (isset($csgoMatchData['game']['finished']) && isset($csgoMatchData['game']['winner_id'])){
            $battleFinishedStatus = PandascoreBase::BATTLE_FINISHED_PAST;
//            $teamId = self::getMainIdByRelIdentityId('team',$csgoMatchData['game']['winner_id'],'3');
            $teamId = self::teamIdChangeOwnTeamId($csgoMatchData['game']['winner_id']);
            $winnerId = self::getTeamOrderByTeamId($teamId,$matchInfo);
        }
        $battleMapId = self::getNameConvertCsgoMapId($csgoMatchData['map']['name']);
//        if($battleMapId) self::pushMatchMapInfo($battleMapId);
        // redis取出battle开始时间
//        $battleBeginAt = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'begin_at']);
        $battleBeginAt = PandascoreBase::getInstance()->valueBeginAtGet("battle:{$battleId}:match:{$matchInfo['id']}:begin_at");
        // 算出对局时长
        if($battleBeginAt && $socketTime){
            $current_timestamp = strtotime($socketTime) - strtotime($battleBeginAt);
        }
        // battle数据结构
        $battleFieldArray = [
            'game' => 1,
            'is_forfeit' => 2,
            'is_battle_detailed' => 1,
            'is_draw' => 2,
            'flag' => 1,
            'match' => $matchInfo['id'],
            'status' => $battleFinishedStatus,
            'order' => isset($battleInfo['battle_order']) ? $battleInfo['battle_order']:null,
            'duration' => isset($current_timestamp) ? (String)$current_timestamp : null,
            'map' => $battleMapId,
            'winner' => isset($winnerId) ? $winnerId:null
        ];
        if(!$battleFieldArray['duration']){
            unset($battleFieldArray['duration']);
        }
        // 处理match地图数据 map_info
        if($battleFieldArray['map']){
            $mapsJson = PandascoreBase::getInstance()->valueGet(['maps']);
            if($mapsJson){
                $mapsArray = array();
                $mapsData = json_decode($mapsJson,true);
                array_push($mapsData,array('map'=>$battleMapId));
                foreach ($mapsData as $key=>$v) {
                    if(isset($mapsArray[$v['map']]) == false){
                        $mapsArray[$v['map']] = $v;
                    }
                }
                $mapsArray = array_merge($mapsArray);
                // redis 存入地图
                PandascoreBase::getInstance()->valueSet(['maps'],json_encode($mapsArray));
            }else{
                // redis 存入地图
                PandascoreBase::getInstance()->valueSet(['maps'],json_encode([['map'=>$battleMapId]]));
            }
        }else{
            unset($battleFieldArray['map']);
        }
        // battle数据
//        self::setBattleBase($battleFieldArray);
        // 是否安放了炸弹
        self::$bombPlantedArray = [
            'round' => $csgoMatchData['round'],
            'bomb_planted' => $csgoMatchData['bomb_planted']==null ? null:($csgoMatchData['bomb_planted']==false ? 2:1),
        ];
        // StaticTeam数据结构处理
        $teamsStatic = array_merge(
            array('terrorist'=>$csgoMatchData['terrorists']),
            array('ct'=>$csgoMatchData['counter_terrorists'])
        );
        $battleTeams = self::staticTeamStructuralData($teamsStatic,$battleId,$csgoMatchData['round'],
            $matchInfo,$csgoMatchData['game']['winner_id']);
        // 再次查询redis map数据
        $mapsJson = PandascoreBase::getInstance()->valueGet(['maps']);
        if($mapsJson){
            $matchConfigArray['map_info'] = $mapsJson;
        }
        //判断比赛是否结束
        if(isset($winnerId) && $winnerId) {
            // battle结束时间
            $battleFieldArray['end_at'] = $socketTime;
            switch ($winnerId) {
                case 1:
                    $team_1_score = PandascoreBase::getInstance()->valueIncr(['team_1_score']);
//                    $team_1_score = PandascoreBase::getInstance()->valueGet(['team_1_score']);
//                    PandascoreBase::$matchFramesInfoConfig['team_1_score'] = PandascoreBase::$matchFramesInfoConfig['team_1_score'] + 1;
//                    $matchConfigArray['team_1_score'] = PandascoreBase::$matchFramesInfoConfig['team_1_score'];
                    $matchConfigArray['team_1_score'] = $team_1_score;
                    break;
                case 2:
                    $team_2_score = PandascoreBase::getInstance()->valueIncr(['team_2_score']);
//                    PandascoreBase::$matchFramesInfoConfig['team_2_score'] = PandascoreBase::$matchFramesInfoConfig['team_2_score'] + 1;
//                    $matchConfigArray['team_2_score'] = PandascoreBase::$matchFramesInfoConfig['team_2_score'];
                    $matchConfigArray['team_2_score'] = $team_2_score;
                    break;
            }
            $team_1_score = PandascoreBase::getInstance()->valueGet(['team_1_score']) ?? 0;
            $team_2_score = PandascoreBase::getInstance()->valueGet(['team_2_score']) ?? 0;
            $winnerInfo = WinnerHelper::csgoWinnerInfo($team_1_score, $team_2_score, $matchInfo['number_of_games']);
            if ($winnerInfo['is_finish'] == PandascoreBase::MATCH_FINISHED_STATUS) {
                $matchConfigArray['is_draw'] = $winnerInfo['is_draw'];
                $matchConfigArray['winner'] = $winnerInfo['winner_team'];
                $matchConfigArray['status'] = PandascoreBase::MATCH_END_STATUS;
                $matchConfigArray['end_at'] = $socketTime;
            }
        }
//        $battleTeams['base'] = PandascoreBase::$battleFramesInfoConfig;
        $battleTeams['base'] = $battleFieldArray;
        $players = PandascoreBase::$battlePlayersArray;
        foreach($players as $key=>$val){
//            $val['order']=$key+1;
            $players[$key]['order']=$key+1;
        }
        $battleTeams['players'] = $players;

        $roundPlayers = PandascoreBase::$roundPlayersArray;
        foreach($roundPlayers as $key=>$val){
            $roundPlayers[$key]['order']=$key+1;
        }
        $battleTeams['round_players'] = $roundPlayers;
        $battleTeams['order'] = $battleTeams['base']['order'];
        return [
//            'match' => PandascoreBase::$matchFramesInfoConfig,
            'match' => $matchConfigArray,
            'battle' => $battleTeams
        ];
    }

    // StaticTeam数据结构处理
    public static function staticTeamStructuralData($teamsStaticData,$battleId,$round,$matchInfo,$winnerId)
    {
        // 初始化
        PandascoreBase::$battlePlayersArray = [];
        PandascoreBase::$roundPlayersArray = [];
        PandascoreBase::$roundSidesArray = [];
        if($round){
            // 有效回合存入redis
            PandascoreBase::getInstance()->setsAdd(['battle',$battleId,'liveRounds'],$round);
        }
        // 回合数据
        self::setBattleRound([
            'battle_id' => $battleId,
            'round_ordinal' => $round,
//            'winner_team_id' => $winnerId ? self::teamIdChangeOwnTeamId($winnerId):null,
        ]);
        $teamsStatic = [];
        $teamStaticConfig = [];
        foreach ($teamsStaticData as $csgoSide => $team){
            // 初始化 阵营
            PandascoreBase::$roundSideFieldConfig = [
                'battle_id' => null, //对局id
                'game' => 1, //游戏项目
                'match' => null, //所属比赛
                'order' => null, //排序
                'side_order' => null, //主队 客队1或者2
                'round_ordinal' => null, //回合序号
                'side' => '', //阵营
                'team_id' => null, //战队ID
//        'winner_end_type' => null, //结束方式
                'survived_players' => 0, //存活人数
                'kills' => null, //击杀
                'is_winner' => null, //是否为获胜方
                'is_bomb_planted' => null, //是否安放了炸弹
            ];
            $teamId = self::teamIdChangeOwnTeamId($team['id']);
            $teamOrder = $teamId ? self::getTeamOrderByTeamId($teamId,$matchInfo):null;
            $staticTeam = [
                'battle_id' => $battleId,
                'rel_identity_id' => $team['id'],
                'identity_id' => $team['name'],
                'name' => $team['name'],
                'team_id' => $teamId,
                'order' => $teamOrder,
                'score' => $team['round_score'],
            ];
            // 处理battlePlayers数据
            self::battlePlayersStructuralData($team['players'],$teamId,$battleId,$teamOrder,$csgoSide,$round);
            // 是否安放了炸弹
            if($csgoSide == 'ct'){
                $is_bomb_planted = 2;
            }
            if($csgoSide == 'terrorist'){
                if(self::$bombPlantedArray['round'] == $round){
                    $is_bomb_planted = self::$bombPlantedArray['bomb_planted'];
                }else{
                    $is_bomb_planted = null;
                }
            }
            // 取出获胜方式
            $winnerDataJson = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'round','eventOrder']);
            if($winnerDataJson) {
                $winnerData = json_decode($winnerDataJson, true);
                $winnerData['winner_side'] = $winnerData['winner_side']=='terrorists' ? 'terrorist':'ct';
                if($winnerData['round'] == $round && $winnerData['winner_side']==$csgoSide){
                    $winnerEndType = $winnerData['round_end_type'];
                    $isWinner = 1;
                }else{
                    $winnerEndType = null;
                    $isWinner = 2;
                }
            }else{
                $winnerEndType = null;
                $isWinner = null;
            }
            // 取出side-kills数量
            $sideKillCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'round_side','kills'],"round_{$round}_side_kill_{$teamOrder}");
            if(!$sideKillCount){
                $sideKillCount = 0;
            }
            // round下side数据
            self::setBattleRoundSide([
                'battle_id' => $battleId,
                'order' => $teamOrder,
                'side_order' => $teamOrder,
                'round_ordinal' => $round,
                'side' => $csgoSide,
                'team_id' => $teamId,
//                'is_winner' => $winnerId ? ($winnerId==$team['id'] ? 1:2):null,
                'is_winner' => $isWinner,
                'kills' => $sideKillCount,
                'round_end_type' => $winnerEndType,
                'is_bomb_planted' => $is_bomb_planted,
                'survived_players' => PandascoreBase::$roundSideFieldConfig['survived_players']
            ]);
            switch ($round){
                case $round<16:
                    // 开局阵营
                    $staticTeam['starting_faction_side']=$csgoSide;
                    // 进行存入redis上半场比分
                    PandascoreBase::getInstance()->valueSet(['battle',$battleId,'1st_half_score',$teamOrder],$team['round_score']);
                    $staticTeam['1st_half_score']=$team['round_score'];
                    break;
                case $round>15 && $round<=30:
                    // redis取出上半场比分
                    $_st_half_score = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'1st_half_score',$teamOrder]);
                    if($_st_half_score){
                        $staticTeam['2nd_half_score'] = $team['round_score']-$_st_half_score;
                    }
                    // 进行存入redis下半场比分
                    PandascoreBase::getInstance()->valueSet(['battle',$battleId,'2nd_half_score',$teamOrder],$team['round_score']);
                    break;
                case $round>30:
                    // redis取出下半场比分
                    $_nd_half_score = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'2nd_half_score',$teamOrder]);
                    if($_nd_half_score){
                        $staticTeam['ot_score'] = $team['round_score']-$_nd_half_score;
                    }
                    break;
                default:
                    break;
            }
            $teamStaticConfig[] = $staticTeam;
//            $teamsStatic['static'][] = PandascoreBase::$teamStaticConfig;
            $teamsStatic['rounds'][0] = PandascoreBase::$battleRoundConfig;
            self::roundSideStructuralData();
        }
        $teamsStatic['static'] = $teamStaticConfig;
        // 从redis取出有效回合
        $liveRounds = PandascoreBase::getInstance()->setsMembers(['battle',$battleId,'liveRounds']);
        if($liveRounds){
            $teamsStatic['detail']['live_rounds'] = json_encode($liveRounds);
        }
        $teamsStatic['detail']['is_confirmed'] = 1;
        if($round <= 5){
            // 获胜战队和获胜阵营 // 第1回合获胜战队和获胜阵营 从redis获取
            $win_round_1_team = PandascoreBase::getInstance()->hashGet(['win_round'],"win_round_1_team_{$battleId}");
            if($win_round_1_team){
                $winRoundTeam = explode('-', $win_round_1_team);
                $teamsStatic['detail']['win_round_1_side'] = isset($winRoundTeam[1]) ? $winRoundTeam[1]:'';
                $teamsStatic['detail']['win_round_1_team'] = isset($winRoundTeam[0]) ? $winRoundTeam[0]:null;
            }
        }
        if($round >= 5) {
            // 首先获胜五回合
            $first_to_five_team = PandascoreBase::getInstance()->hashGet(['win_round'], "first_to_five_team_{$battleId}_{$teamOrder}");
            if ($first_to_five_team) {
                $winRoundTeam = explode('-', $first_to_five_team);
                $teamsStatic['detail']['first_to_5_rounds_wins_side'] = isset($winRoundTeam[1]) ? $winRoundTeam[1] : '';
                $teamsStatic['detail']['first_to_5_rounds_wins_team'] = isset($winRoundTeam[0]) ? $winRoundTeam[0] : null;
            }
        }
        if($round>15){
            // 获胜战队和获胜阵营 // 第16回合获胜战队和获胜阵营 从redis获取
            $win_round_16_team = PandascoreBase::getInstance()->hashGet(['win_round'],"win_round_16_team_{$battleId}");
            if($win_round_16_team){
                $winRoundTeam = explode('-', $win_round_16_team);
                $teamsStatic['detail']['win_round_16_side'] = isset($winRoundTeam[1]) ? $winRoundTeam[1]:'';
                $teamsStatic['detail']['win_round_16_team'] = isset($winRoundTeam[0]) ? $winRoundTeam[0]:null;
            }
        }
        if(isset($teamsStatic['rounds'])){
            $teamsStatic['rounds'][0]['side'] = PandascoreBase::$roundSidesArray;
        }
        return $teamsStatic;
    }
    // 处理roundSide数据
    public static function roundSideStructuralData()
    {
        if(PandascoreBase::$roundSideFieldConfig['is_bomb_planted'] == 1){
            PandascoreBase::$roundSideFieldConfig['side'] == 'ct' ?
                PandascoreBase::$roundSideFieldConfig['is_bomb_planted']=2 :
                PandascoreBase::$roundSideFieldConfig['is_bomb_planted']=1;
        }
        PandascoreBase::$roundSidesArray[PandascoreBase::$roundSideFieldConfig['side']] = PandascoreBase::$roundSideFieldConfig;
    }
    // 处理battlePlayers数据
    public static function battlePlayersStructuralData($teamPlayersData,$teamId,$battleId,$teamOrder,$side,$round)
    {
        // 存活人数初始值
        PandascoreBase::$roundSideFieldConfig['survived_players'] = 0;
        foreach ($teamPlayersData as $key => $player){
//            $playerId = self::getMainIdByRelIdentityId('player',$player['id'],'3');
            $playerId = self::playerIdChangeOwnplayerId($player['id']);
            // 初始化Players数据结构
//            self::playersBattleConfig();
            PandascoreBase::playersBattleConfigInfo();

            $roundCountentData = PandascoreBase::getInstance()->hashGetAll(['battle',$battleId,'battle_round_players','content']);
            if($roundCountentData){
//                $roundFirstKill = isset($roundCountentData["battle_{$battleId}_first_kill_round_{$round}_{$player['id']}"]) ? 1:2;
//                $roundFirstDeath = isset($roundCountentData["battle_{$battleId}_first_died_round_{$round}_{$player['id']}"]) ? 1:2;
                // round 是否首杀
                if(isset($roundCountentData["battle_{$battleId}_first_kill_round_{$round}"])){
                    $firstKillPlayerId = $roundCountentData["battle_{$battleId}_first_kill_round_{$round}"];
                    if($firstKillPlayerId == $player['id']){
                        $roundFirstKill = 1;
                    }else{
                        $roundFirstKill = 2;
                    }
                }else{
                    $roundFirstKill = 2;
                }
                // round 是否首死
                if(isset($roundCountentData["battle_{$battleId}_first_died_round_{$round}"])){
                    $firstDiedPlayerId = $roundCountentData["battle_{$battleId}_first_kill_round_{$round}"];
                    if($firstDiedPlayerId == $player['id']){
                        $roundFirstDeath = 1;
                    }else{
                        $roundFirstDeath = 2;
                    }
                }else{
                    $roundFirstDeath = 2;
                }
                // round 击杀数
                if(isset($roundCountentData["battle_{$battleId}_round_{$round}_{$player['id']}"])){
                    $roundKills = $roundCountentData["battle_{$battleId}_round_{$round}_{$player['id']}"];
                }else{
                    $roundKills = 0;
                }
                // round 是否多杀
                $roundMultiKill = 2;
                if($roundKills >= 2){
                    $roundMultiKill = 1;
                }
                // round 是否ace
                $roundAceKill = 2;
                if($roundKills == 5){
                    $roundAceKill = 1;
                }
                // round 是否死亡
                $roundDide = isset($roundCountentData["battle_{$battleId}_round_{$round}_died_{$player['id']}"]) ? 1:2;

                // battle 首先击杀次数
                if(isset($roundCountentData["battle_{$battleId}_first_kill_battle_{$player['id']}"])){
                    $battleFirstKillCount = $roundCountentData["battle_{$battleId}_first_kill_battle_{$player['id']}"];
                }else{
                    $battleFirstKillCount = 0;
                }
                // battle 首先死亡次数
                if(isset($roundCountentData["battle_{$battleId}_first_died_battle_{$player['id']}"])){
                    $battleFirstDiedCount = $roundCountentData["battle_{$battleId}_first_died_battle_{$player['id']}"];
                }else{
                    $battleFirstDiedCount = 0;
                }
                // battle 首杀差
                if(isset($battleFirstKillCount) && isset($battleFirstDiedCount)){
                    $battleFirstKillsDiff = (int)$battleFirstKillCount-(int)$battleFirstDiedCount;
                }
                // battle 多杀次数
                if(isset($roundCountentData["battle_{$battleId}_battle_kill_count_{$player['id']}"])){
                    $battleMultiKillsCount = $roundCountentData["battle_{$battleId}_battle_kill_count_{$player['id']}"];
                }else{
                    $battleMultiKillsCount = 0;
                }
                // battle ace次数
                if(isset($roundCountentData["battle_{$battleId}_battle_ace_count_{$player['id']}"])){
                    $battleAceKillsCount = $roundCountentData["battle_{$battleId}_battle_ace_count_{$player['id']}"];
                }else{
                    $battleAceKillsCount = 0;
                }
            }
            // battle-Players数据结构
            self::setBattlePlayers([
                'battle_id' => $battleId,
//                'order' => $key + 1,
                'team_order' => $teamOrder,
                'team_id' => $teamId,
                'player_id' => $playerId,
                'nick_name' => $player['name'],
                'kills' => $player['kills'],
                'deaths' => $player['deaths'],
                'k_d_diff' => $player['kills']-$player['deaths'],
                'primary_weapon' => isset($player['primary_weapon']['name']) ?
                    self::weaponNameChangeId($player['primary_weapon']['name']):null,
                'hp' => $player['hp'],
                'is_alive' => $player['is_alive']==true ? 1:2,
                'first_kills' => isset($battleFirstKillCount) ? $battleFirstKillCount:null,
                'first_deaths' => isset($battleFirstDiedCount) ? $battleFirstDiedCount:null,
                'first_kills_diff' => isset($battleFirstKillsDiff) ? $battleFirstKillsDiff:null,
                'multi_kills' => isset($battleMultiKillsCount) ? $battleMultiKillsCount:null,
                'ace_kill' => isset($battleAceKillsCount) ? $battleAceKillsCount:null,
            ]);
            // 初始化
            PandascoreBase::roundPlayersFieldConfigInfo();
//            self::roundPlayersFieldConfig();

            // 取出round-kills
            // 取出round-is_died
            // 取出round-is_first_kill
            // 取出round-is_first_death
            // 取出round-is_multi_kill
            // 取出round-is_ace_kill
            // round-Players数据结构
            self::setRoundPlayers([
                'battle_id' => $battleId,
//                'order' => $key + 1,
                'player_order' => $teamOrder,
                'round_ordinal' => $round,
                'side' => $side,
                'team_id' => $teamId,
                'player_id' => $playerId,
                'nick_name' => $player['name'],
                'kills' => isset($roundKills) ? $roundKills:null,
                'is_first_kill' => isset($roundFirstKill) ? $roundFirstKill:null,
                'is_first_death' => isset($roundFirstDeath) ? $roundFirstDeath:null,
                'is_multi_kill' => isset($roundMultiKill) ? $roundMultiKill:null,
                'is_ace_kill' => isset($roundAceKill) ? $roundAceKill:null,
                'is_died' => isset($roundDide) ? $roundDide:null,
            ]);
            PandascoreBase::$battlePlayersArray[$player['id']] = PandascoreBase::$playersBattleConfig;
            PandascoreBase::$roundPlayersArray[$player['id']] = PandascoreBase::$roundPlayersFieldConfig;
            // 计算存活人数
            self::calculationSurvivedPlayer($player['is_alive']);
        }
    }
    // 存活人数
    public static function calculationSurvivedPlayer($isAlive)
    {
        if($isAlive){
            PandascoreBase::$roundSideFieldConfig['survived_players'] = PandascoreBase::$roundSideFieldConfig['survived_players']+1;
        }else{
            PandascoreBase::$roundSideFieldConfig['survived_players'] = PandascoreBase::$roundSideFieldConfig['survived_players']+0;
        }
    }
    public static function pushMatchMapInfo($mapId)
    {
        array_push(PandascoreBase::$matchMapArray,array('map'=>$mapId));
    }
    // set matchReal数据
    public static function setMatchTimeReal($matchResultData)
    {
        PandascoreBase::$matchFramesInfoConfig = array_merge(PandascoreBase::$matchFramesInfoConfig, $matchResultData);
    }
    // set battle数据
    public static function setBattleBase($baseResultData)
    {
        PandascoreBase::$battleFramesInfoConfig = array_merge(PandascoreBase::$battleFramesInfoConfig, $baseResultData);
    }
    // set rounds数据
    public static function setBattleRound($roundResultData)
    {
        PandascoreBase::$battleRoundConfig = array_merge(PandascoreBase::$battleRoundConfig, $roundResultData);
    }
    // set StaticTeam数据
    public static function setStaticTeam($teamResultData)
    {
        PandascoreBase::$teamStaticConfig = array_merge(PandascoreBase::$teamStaticConfigTpl, $teamResultData);
    }
    // set battlePlayers数据
    public static function setBattlePlayers($playersResultData)
    {
        PandascoreBase::$playersBattleConfig = array_merge(PandascoreBase::$playersBattleConfig, $playersResultData);
    }
    // set roundPlayers数据
    public static function setRoundPlayers($playersResultData)
    {
        PandascoreBase::$roundPlayersFieldConfig = array_merge(PandascoreBase::$roundPlayersFieldConfig, $playersResultData);
    }
    // set roundSide数据
    public static function setBattleRoundSide($sideResultData)
    {
        PandascoreBase::$roundSideFieldConfig = array_merge(PandascoreBase::$roundSideFieldConfig, $sideResultData);
    }
    // 数据源对局地图转换本库battle地图
    public static function getNameConvertCsgoMapId($mapName)
    {
        if(isset(PandascoreBase::$battleMap[$mapName])){
        }else{
            PandascoreBase::$battleMap[$mapName] =
                MetadataCsgoMap::find()->select('id')->where(['name'=>$mapName])->one();
        }
        return PandascoreBase::$battleMap[$mapName]['id'];
    }
    // 数据源武器名称转换本库武器Id
    public static function weaponNameChangeId($weaponName)
    {
        if(isset(PandascoreBase::$weaponNameCreId[$weaponName])){
        }else {
            PandascoreBase::$weaponNameCreId[$weaponName] =
                MetadataCsgoWeapon::find()->where(['name' => $weaponName])->select('id')->one();
        }
        return PandascoreBase::$weaponNameCreId[$weaponName]['id'];
    }
    // 数据源teamId转换本库teamId
    public static function teamIdChangeOwnTeamId($relTeamId)
    {
        if($relTeamId) {
            if (isset(PandascoreBase::$teamIds[$relTeamId])) {
            } else {
                PandascoreBase::$teamIds[$relTeamId] =
                    intval(self::getMainIdByRelIdentityId('team', $relTeamId, '3'));
            }
            return PandascoreBase::$teamIds[$relTeamId];
        }else{
            return null;
        }
    }
    // 数据源playerIds转换本库playerId
    public static function playerIdChangeOwnplayerId($relPlayerId)
    {
        if($relPlayerId) {
            if (isset(PandascoreBase::$playerIds[$relPlayerId])) {
            } else {
                PandascoreBase::$playerIds[$relPlayerId] = self::getMainIdByRelIdentityId('player',$relPlayerId,'3');
            }
            return PandascoreBase::$playerIds[$relPlayerId];
        }else{
            return null;
        }
    }
    // 处理events事件
    public static function transEvents($info,$matchId,$socketTime)
    {
        $matchData = $info['match_data'];
        if (@$matchData['type'] == 'hello'){
            return [];
        }
        if(!$matchData['game']['id']){
            return [];
        }
        $payloadArray = isset($matchData['payload']) ? $matchData['payload']:[];
        $battleInfo = self::getbattleIdByMatchIdAndOrder($matchId,$matchData['match']['id'],$matchData['game']['id'],$socketTime);
        $battleId = $battleInfo['battle_id'];
        $battleOrder = $battleInfo['battle_order'];
        if($payloadArray['type'] == PandascoreBase::CSGO_ROUND_START){
            // redis 存储事件回合序号
            if(isset($payloadArray['round_number'])){
                self::setCurrentRoundNum($payloadArray['round_number'],$battleId);
            }
            // redis 存储这时候时间  无法准确预估
//            PandascoreBase::getInstance()->valueSet(['battle',$battleId,'round_timestamp',$payloadArray['round_number']],0);
//            PandascoreBase::getInstance()->valueSet(['battle',$battleId,'round_time',$payloadArray['round_number']],115);
        }
        if(!isset($payloadArray['round_number'])){
            $payloadArray['round_number'] = null;
        }
        self::payloadTransEventType(@$payloadArray['type'],$battleId,$matchId,$payloadArray);
        $roundEventData = PandascoreBase::$roundEventsArray;
        BattleService::setBattleInfo($battleId,'csgo',$roundEventData,BattleService::DATA_TYPE_EVENTS);
    }

    public static function getCurrentRoundNum($battleId)
    {
        // roundorder 从redis中取
        return PandascoreBase::getInstance()->valueGet(['battle',$battleId,'round_ordinal']);
    }

    public static function setCurrentRoundNum($roundOrdinal,$battleId)
    {
        // roundorder 从redis中取
        PandascoreBase::getInstance()->valueSet(['battle',$battleId,'round_ordinal'],$roundOrdinal);
    }

    public static function payloadTransEventType($eventType,$battleId,$matchId,$payloadData)
    {
        if($eventType == PandascoreBase::CSGO_ROUND_END){
            if(!$payloadData['team_side']){
                return;
            }
        }
        // 初始化
//        self::roundEventsConfig();
        PandascoreBase::roundEventsConfigInfo();
        PandascoreBase::$roundEventsArray = [];
        // redis 默认event时间order
        $roundEventOrder = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'order','defult']);
        if($roundEventOrder){
            $roundEventOrder = $roundEventOrder+1;
            PandascoreBase::getInstance()->valueSet(['battle',$battleId,'order','defult'],$roundEventOrder);
        }
        if($roundEventOrder==0 || !$roundEventOrder){
            $roundEventOrder = 1;
            PandascoreBase::getInstance()->valueSet(['battle',$battleId,'order','defult'],$roundEventOrder);
        }
//        $roundEventOrder = MatchBattleRoundEventCsgo::find()->where(['battle_id'=>$battleId])->max('`order`');
        self::setRoundEvents([
            'battle_id' => $battleId,
            'match' => $matchId,
            'order' => $roundEventOrder ? $roundEventOrder:1,
//            'is_bomb_planted' => PandascoreBase::$roundSideFieldConfig['is_bomb_planted'],
            'round_ordinal' => isset($payloadData['round_number'])?$payloadData['round_number']:self::getCurrentRoundNum($battleId),
        ]);
        switch ($eventType){
            case PandascoreBase::CSGO_ROUND_START:
                self::setRoundEvents([
                    'event_type' => PandascoreBase::CSGO_ROUND_START,
                    'in_round_timestamp' => '0.00',
                    'round_time' => '115.00',
                ]);
                // redis取出获胜阵营
                $jsonData = PandascoreBase::getInstance()->valueGet(['battle',$battleId,'round','eventOrder']);
                if($jsonData){
                    $dataArray = json_decode($jsonData,true);
                    if($dataArray['winner_side'] == 'ct'){
                        $ct_score = $payloadData['score'][0]>$payloadData['score'][1] ? $payloadData['score'][0]:$payloadData['score'][1];
                        $t_score = $payloadData['score'][0]<$payloadData['score'][1] ? $payloadData['score'][0]:$payloadData['score'][1];
                    }
                    if($dataArray['winner_side'] == 'terrorists'){
                        $ct_score = $payloadData['score'][0]<$payloadData['score'][1] ? $payloadData['score'][0]:$payloadData['score'][1];
                        $t_score = $payloadData['score'][0]>$payloadData['score'][1] ? $payloadData['score'][0]:$payloadData['score'][1];
                    }
                    $event = [
                        'battle_id' => $battleId,
                        'order' => $dataArray['order'],
                        'round_ordinal' => $payloadData['round_number']-1,
                        'ct_score' => isset($ct_score) ? $ct_score:null,
                        't_score' => isset($t_score) ? $t_score:null,
                    ];
                    BattleService::setBattleInfo($battleId,'csgo',[$event],BattleService::DATA_TYPE_EVENTS);
                }
                break;
            case PandascoreBase::CSGO_EVENT_KILL:
                $round_number = isset($payloadData['round_number'])?$payloadData['round_number']:self::getCurrentRoundNum($battleId);
                // 击杀者
                $teamKillerId = @$payloadData['killer']['object']['id'];
                $killerData = self::getCsgoPlayerIdAndName($teamKillerId);
                $teamKillSide = @$payloadData['killer']['object']['team_side'];
                $teamKside = $teamKillSide=='counter_terrorists' ? 'ct':$teamKillSide;
                // 击杀者 team_order
                $teamOrder = self::getRoundTeamOrderByTeamFaction($battleId, @$round_number, $teamKside);
                // 受害者
                $teamKilledId = @$payloadData['killed']['object']['id'];
                $killedData = self::getCsgoPlayerIdAndName($teamKilledId);
                $teamVictimSide = @$payloadData['killed']['object']['team_side'];
                $teamVside = $teamVictimSide=='counter_terrorists' ? 'ct':$teamVictimSide;
                // 算出battle_player首杀 首死 首杀差 未做
                // 存入redis
                if(@$round_number) {
                    $payloadData['round_number'] = $round_number;
                    // side 击杀数量
                    if($teamOrder) {
                        $sideKillCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'round_side','kills'],"round_{$payloadData['round_number']}_side_kill_{$teamOrder}");
                        if ($sideKillCount) {
                            $sideKillCount = (int)$sideKillCount + 1;
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'round_side','kills'],"round_{$payloadData['round_number']}_side_kill_{$teamOrder}", $sideKillCount);
                        } else {
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'round_side','kills'],"round_{$payloadData['round_number']}_side_kill_{$teamOrder}",1);
                        }
                    }
                    // round 是否首先击杀
//                    $firstKillerCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_round_{$payloadData['round_number']}_{$teamKillerId}");
                    $firstKillerCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_round_{$payloadData['round_number']}");
                    if (!$firstKillerCount) {
                        // round
//                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_round_{$payloadData['round_number']}_{$teamKillerId}", 1);
                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_round_{$payloadData['round_number']}", $teamKillerId);
                        // battle
                        $battleFirstKillerCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_battle_{$teamKillerId}");
                        if($battleFirstKillerCount){
                            $battleFirstKillerCount = (int)$battleFirstKillerCount+1;
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_battle_{$teamKillerId}", $battleFirstKillerCount);
                        }else {
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_kill_battle_{$teamKillerId}", 1);
                        }
                    }
                    // round 是否首先死亡
//                    $firstKilledCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_round_{$payloadData['round_number']}_{$teamKilledId}");
                    $firstKilledCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_round_{$payloadData['round_number']}");
                    if (!$firstKilledCount) {
                        // round
//                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_round_{$payloadData['round_number']}_{$teamKilledId}", 1);
                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_round_{$payloadData['round_number']}", $teamKilledId);
                        // battle
                        $battleFirstKilledCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_battle_{$teamKilledId}");
                        if($battleFirstKilledCount){
                            $battleFirstKilledCount = (int)$battleFirstKilledCount+1;
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_battle_{$teamKilledId}", $battleFirstKilledCount);
                        }else {
                            PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_first_died_battle_{$teamKilledId}", 1);
                        }
                    }
                    // round 击杀数量 是否多杀
                    $killerCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_round_{$payloadData['round_number']}_{$teamKillerId}");
                    if ($killerCount) {
                        // round
                        $killerCount = (int)$killerCount+1;
                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_round_{$payloadData['round_number']}_{$teamKillerId}", $killerCount);
                        // battle
                        if($killerCount==2){
                            // 多杀次数
                            $battleKillerCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_kill_count_{$teamKillerId}");
                            if($battleKillerCount){
                                $battleKillerCount = (int)$battleKillerCount+1;
                                PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_kill_count_{$teamKillerId}",$battleKillerCount);
                            }else{
                                PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_kill_count_{$teamKillerId}",1);
                            }
                        }
                        if($killerCount==5){
                            // ace次数
                            $battleAceCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_ace_count_{$teamKillerId}");
                            if($battleAceCount){
                                $battleAceCount = (int)$battleAceCount+1;
                                PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_ace_count_{$teamKillerId}",$battleAceCount);
                            }else{
                                PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_battle_ace_count_{$teamKillerId}",1);
                            }
                        }
                    }else{
                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_round_{$payloadData['round_number']}_{$teamKillerId}", 1);
                    }
                    // round 是否死亡
                    $killedCount = PandascoreBase::getInstance()->hashGet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_round_{$payloadData['round_number']}_died_{$teamKilledId}");
                    if (!$killedCount) {
                        PandascoreBase::getInstance()->hashSet(['battle',$battleId,'battle_round_players', 'content'], "battle_{$battleId}_round_{$payloadData['round_number']}_died_{$teamKilledId}", 1);
                    }
                }
                // set events
                self::setRoundEvents([
                    'event_type' => PandascoreBase::CSGO_PLAYER_KILL,
                    'killer' => $killerData['player_id'],
                    'killer_nick_name' => $killerData['nick_name'] ? $killerData['nick_name']:$payloadData['killer']['object']['name'],
                    'killer_side' => $teamKside,
                    'victim' => $killedData['player_id'],
                    'victim_nick_name' => $killedData['nick_name'] ? $killedData['nick_name']:$payloadData['killed']['object']['name'],
                    'victim_side' => $teamVside,
                ]);
                break;
            case PandascoreBase::CSGO_ROUND_END:
                // 定义获胜方式
                $winnerEndType = [
                    "planted_eliminated-terrorists" => "5",
                    "timeout-counter_terrorists" => "3",
                    "defused-counter_terrorists" => "1",
                    "eliminated-terrorists" => "5",
                    "eliminated-counter_terrorists" => "2",
                    "exploded-terrorists" => "4",
                ];
                $roundEndType = $winnerEndType[$payloadData['outcome'].'-'.$payloadData['team_side']];
                // 去数据库查找，当前阵营的team_order
                $teamOrder = self::getRoundTeamOrderByTeamFaction($battleId, @$payloadData['round_number'], $payloadData['team_side']);
                // 判断第一回合逻辑
                if(@$payloadData['round_number'] == 1){
                    // 第1回合获胜方数据 存入redis
                    PandascoreBase::getInstance()->hashSet(['win_round'],"win_round_1_team_{$battleId}",$teamOrder."-".$payloadData['team_side']);
                }
                // 第16回合获胜
                if(@$payloadData['round_number'] == 16){
                    // 第16回合获胜方数据 存入redis
                    PandascoreBase::getInstance()->hashSet(['win_round'],"win_round_16_team_{$battleId}",$teamOrder."-".$payloadData['team_side']);
                }
                // 先获胜五回合
                $payloadData['team_side'] = $payloadData['team_side']=='counter_terrorists' ? 'ct':$payloadData['team_side'];
                if(@$payloadData['round_number'] <= 10){
                    $first_to_5_ct = PandascoreBase::getInstance()->hashGet(['win_round'],"first_to_5_score_{$battleId}_1");
                    if(!$first_to_5_ct){
                        // 初始值
                        PandascoreBase::getInstance()->hashSet(['win_round'],"first_to_5_score_{$battleId}_1",0);
                    }
                    $first_to_5_terrorists = PandascoreBase::getInstance()->hashGet(['win_round'],"first_to_5_score_{$battleId}_2");
                    if(!$first_to_5_terrorists){
                        // 初始值
                        PandascoreBase::getInstance()->hashSet(['win_round'],"first_to_5_score_{$battleId}_2",0);
                    }
                    if($first_to_5_ct<5 && $first_to_5_terrorists<5){
                        $num = PandascoreBase::getInstance()->hashGet(['win_round'],"first_to_5_score_{$battleId}_{$teamOrder}");
                        PandascoreBase::getInstance()->hashSet(['win_round'],"first_to_5_score_{$battleId}_{$teamOrder}",intval($num)+1);
                        if(intval($num)+1 == 5){
                            PandascoreBase::getInstance()->hashSet(['win_round'],"first_to_five_team_{$battleId}_{$teamOrder}",$teamOrder."-".$payloadData['team_side']);
                        }
                    }
                }
                // 获胜方式和阵营存入redis
                $order_sideWinner = [
                    'winner_side' => $payloadData['team_side'],
                    'order' => $roundEventOrder,
                    'round' => @$payloadData['round_number'],
                    'round_end_type' => $roundEndType,
                    'winner_team_id' => $teamOrder,
                ];
                PandascoreBase::getInstance()->valueSet(['battle',$battleId,'round','eventOrder'],json_encode($order_sideWinner));
                // round_events数据
                self::setRoundEvents([
                    'event_type' => PandascoreBase::CSGO_ROUND_END,
                    'round_end_type' => $roundEndType,
                    'winner_side' => $payloadData['team_side'],
                ]);
                // 更新对应battle的获胜方和获胜方式,这里的event影响frame的值
                $roundInfo=[
                    'battle_id' => $battleId,
                    'match' => $matchId,
                    'round_ordinal' => @$payloadData['round_number'],
                    'winner_team_id' => $teamOrder,
                    'winner_end_type' =>$roundEndType,
                ];
                // 更新对应的round
                BattleService::setBattleInfo($battleId,'csgo',[$roundInfo],BattleService::DATA_TYPE_ROUNDS);
                break;
        }
        PandascoreBase::$roundEventsArray[] = PandascoreBase::$roundEventsConfig;
    }
    public static $firstHalfTeamOrder = [];
    public static $secondHalfTeamOrder = [];
    public static $overTimeTeamOrder = [];
    public static function getRoundTeamOrderByTeamFaction($battle_id,$roundOrdinal,$team_faction)
    {
        if(!$roundOrdinal){
            return null;
        }
        if($team_faction=='counter_terrorists'){
            $team_faction='ct';
        }
        if($team_faction=='terrorists'){
            $team_faction='terrorist';
        }
        if($roundOrdinal<=15 && ($team_faction=='ct'||$team_faction=='terrorist')){
            if(isset(self::$firstHalfTeamOrder[$team_faction."-".$battle_id])){
            }else {
                $md = MatchBattleRoundSideCsgo::find()
                    ->where(['battle_id'=>$battle_id,'round_ordinal'=>$roundOrdinal,'side'=>$team_faction]);
                $info=$md->asArray()->one();
                if(!$info){
                    return null;
                }
                self::$firstHalfTeamOrder[$team_faction."-".$battle_id] = $info['order'];
            }
            return self::$firstHalfTeamOrder[$team_faction."-".$battle_id];
        }
        if($roundOrdinal>=16 && $roundOrdinal<=30 && ($team_faction=='ct'||$team_faction=='terrorist')){
            if(isset(self::$secondHalfTeamOrder[$team_faction."-".$battle_id])){
            }else {
                $md = MatchBattleRoundSideCsgo::find()
                    ->where(['battle_id'=>$battle_id,'round_ordinal'=>$roundOrdinal,'side'=>$team_faction]);
                $info=$md->asArray()->one();
                if(!$info){
                    return null;
                }
                self::$secondHalfTeamOrder[$team_faction."-".$battle_id] = $info['order'];
            }
            return self::$secondHalfTeamOrder[$team_faction."-".$battle_id];
        }
        if($roundOrdinal>30 && ($team_faction=='ct'||$team_faction=='terrorist')){
            if(isset(self::$overTimeTeamOrder[$team_faction."-".$battle_id])){
            }else {
                $md = MatchBattleRoundSideCsgo::find()
                    ->where(['battle_id'=>$battle_id,'round_ordinal'=>$roundOrdinal,'side'=>$team_faction]);
                $info=$md->asArray()->one();
                if(!$info){
                    return null;
                }
                self::$overTimeTeamOrder[$team_faction."-".$battle_id] = $info['order'];
            }
            return self::$overTimeTeamOrder[$team_faction."-".$battle_id];
        }

        return null;
    }

    public static function getTeamOrderByTeamFaction($battle_id, $team_faction)
    {
        return parent::getTeamOrderByTeamFaction($battle_id, $team_faction); // TODO: Change the autogenerated stub
    }

    // set roundEvents数据
    public static function setRoundEvents($eventResultData)
    {
        PandascoreBase::$roundEventsConfig = array_merge(PandascoreBase::$roundEventsConfig, $eventResultData);
    }
    public static $playerInfos = [];
    public static function getCsgoPlayerIdAndName($relPlayerId)
    {
        if($relPlayerId) {
//            $playerId = self::getMainIdByRelIdentityId('player', $relPlayerId, '3');
            $playerId = self::playerIdChangeOwnplayerId($relPlayerId);
            if ($playerId) {
                if (isset(self::$playerInfos[$playerId])) {
                } else {
                    $playerInfoUs = self::getPlayInfoById($playerId);
                    if(!empty($playerInfoUs)){
                        self::$playerInfos[$playerId] = $playerInfoUs['nick_name'];
                    }
                }
            }
        }else{
            $playerId = null;
        }
        return [
            'nick_name' => isset(self::$playerInfos[$playerId]) ? self::$playerInfos[$playerId]:'',
            'player_id' => $playerId,
        ];
    }

    public static function refreshInfo($matchId, $info)
    {
        self::setMatch($matchId, $info['match']);
        // 根据battleId刷新对应的battle信息
        self::refreshBattle($matchId, $info['battle']);
    }
    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail');
    }
    private static function refreshBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        // todo 数据库的battle数量>返回的数量
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
        }
        $battleId = $battleBase['id'];
        BattleService::setBattleInfo($battleId,'csgo',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'csgo',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['rounds'],BattleService::DATA_TYPE_ROUNDS);
        BattleService::setBattleInfo($battleId,'csgo',$battle['round_players'],BattleService::DATA_TYPE_ROUND_PLAYERS);
    }

}