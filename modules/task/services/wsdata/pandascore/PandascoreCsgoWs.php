<?php
namespace app\modules\task\services\wsdata\pandascore;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\org\models\Team;
use app\modules\task\models\MatchSocketData;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\Mapping;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\match\models\MatchBattleRoundEventCsgo;

class PandascoreCsgoWs extends HotBase
{
    const CSGO_BATTLE_START = 'battle_start';
    const CSGO_BATTLE_END = 'battle_end';
    const CSGO_BATTLE_PAUSE = 'battle_pause';
    const CSGO_BATTLE_UNPAUSE = 'battle_unpause';
    const CSGO_BATTLE_RELOADED = 'battle_reloaded';
    const CSGO_PLAYER_JOINED = 'player_joined';
    const CSGO_PLAYER_QUIT = 'player_quit';
    const CSGO_BOMB_PLANTED = 'bomb_planted';
    const CSGO_BOMB_DEFUSED = 'bomb_defused';
    const CSGO_PLAYER_SUICIDE = 'player_suicide';
    const CSGO_ROUND_START = 'round_start';
    const CSGO_EVENT_KILL = 'kill';
    const CSGO_PLAYER_KILL = 'player_kill';
    const CSGO_ROUND_END = 'round_end';

    const CSGO_EVENT_INFO_TYPE_NO = 'no';
    const CSGO_EVENT_INFO_TYPE_EVENTS = 'events';
    const ROUND_START_ONE = 1;
    public static function run($tagInfo, $taskInfo)
    {
        $gameId = $tagInfo['ext_type'];
        // 统计更新状态，如果是重刷，则取到当前比赛下所有的消息，批量更新
        $refreshType=@$tagInfo['ext2_type'];
        $refreshType=$refreshType?$refreshType:"one";
        $info = json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $relMatchId = $info['match_id'];
        $socketTime = date('Y-m-d H:i:s');
        if (isset($info['socket_time']) && $info['socket_time']){
            $socketTime = $info['socket_time'];
        }
        $matchId = self::getMainIdByRelIdentityId('match', $relMatchId, $originId);

        if($refreshType=='refresh'){
            $list=\app\modules\match\models\MatchLived::find()->where(['game_id'=>1,'match_id'=>$relMatchId])->orderBy('socket_time')->limit(500000)->asArray()->all();
            foreach ($list as $k=>$v){
                $info=$v;
                $info['match_data']=json_decode($info['match_data'],true);
                $socketTime = date('Y-m-d H:i:s');
                if (isset($info['socket_time']) && $info['socket_time']){
                    $socketTime = $info['socket_time'];
                }

                //判断事件的类型 websocket类型
                $csgoMatchData = $info['match_data'];
                $conversionRes = null;
                $type = $info['type'];
                if(isset($csgoMatchData['type']) && $csgoMatchData['type'] == 'hello') continue;
                switch ($info['type']) {
                    case self::CSGO_EVENT_INFO_TYPE_NO:
                        if(isset($csgoMatchData['type']))
                            $conversionRes = self::$conversionRes;
                        $conversionRes = self::transFramesWs($csgoMatchData,$matchId,$socketTime,$originId);
                        $type = 'frames';
                        break;
                    case self::CSGO_EVENT_INFO_TYPE_EVENTS:
                        if($csgoMatchData['type'] == 'hello')
                            $conversionRes = self::$conversionRes;
                        $conversionRes = self::transEventsWs($csgoMatchData,$matchId,$socketTime);
                        $type = 'events';
                        break;
                }

            }
        }else {
            //判断事件的类型 websocket类型
            $csgoMatchData = $info['match_data'];
            $conversionRes = null;
            $type = $info['type'];
            switch ($info['type']) {
                case self::CSGO_EVENT_INFO_TYPE_NO:
                    if (isset($csgoMatchData['type']))
                        $conversionRes = self::$conversionRes;
                    $conversionRes = self::transFramesWs($csgoMatchData, $matchId, $socketTime, $originId);
                    $type = 'frames';
                    break;
                case self::CSGO_EVENT_INFO_TYPE_EVENTS:
                    if ($csgoMatchData['type'] == 'hello')
                        $conversionRes = self::$conversionRes;
                    $conversionRes = self::transEventsWs($csgoMatchData, $matchId, $socketTime);
                    $type = 'events';
                    break;
            }
        }

        if (empty($conversionRes)){
            return ;
        }
        //接下来存数据库
        $insertData = [
            'match_data' =>$conversionRes['trans_data'],
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => $type,
            'battle_order' => $conversionRes['battle_order']
        ];
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $insertData);
        $db_websoket->execute();
        //插入到自己的库
        $matchSocketData = new MatchSocketData();
        $matchSocketData->setAttributes([
            'game_id' => $gameId,
            'origin_id' => $originId,
            'match_id' => $matchId,
            'battle_order' => $conversionRes['battle_order'],
            'match_data' =>$conversionRes['trans_data'],
            'type' => $type
        ]);
        return $matchSocketData->save();
    }
    public static function transFramesWs($csgoMatchData,$matchId,$socketTime,$originId)
    {
        // 获取比赛的信息
        $matchInfo = self::getMatchInfoByMatchId($matchId);
        $battleInfo = self::getbattleIdByMatchIdAndOrder($matchId,
            $csgoMatchData['match']['id'],$csgoMatchData['game']['id'],$socketTime);
        $battleId = isset($battleInfo['battle_id']) ? $battleInfo['battle_id']:null;
        $battleOrder = isset($battleInfo['battle_order']) ? $battleInfo['battle_order']:null;
        if($csgoMatchData['round'] == self::ROUND_START_ONE){
            // 开局作为反恐精英
            $ctTeamId = self::teamIdChangeOwnTeamId($csgoMatchData['counter_terrorists']['id'],$originId);
            $ctTeam = self::teamIdSelectTeamsData($matchInfo,$ctTeamId);
            // 开局作为恐怖分子
            $terroristTeamId = self::teamIdChangeOwnTeamId($csgoMatchData['terrorists']['id'],$originId);
            $terroristTeam = self::teamIdSelectTeamsData($matchInfo,$terroristTeamId);
        }
        if(isset($csgoMatchData['game']['finished'])&&$csgoMatchData['game']['finished']){
            $winnerTeamId = self::teamIdChangeOwnTeamId($csgoMatchData['game']['winner_id'],$originId);
            $battleWinner = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId);
        }
        $matchWinnerData = self::matchWinnerAndScore($matchInfo,
            $csgoMatchData['terrorists']['score'],$csgoMatchData['terrorists']['id'],
            $csgoMatchData['counter_terrorists']['score'],$csgoMatchData['counter_terrorists']['id'],$originId);
        // 战队阵营数据结构处理
        $sidesStatic = array_merge(
            array('terrorists'=>$csgoMatchData['terrorists']),
            array('ct'=>$csgoMatchData['counter_terrorists'])
        );
        $sides = self::staticSideStructuralData($sidesStatic,$matchInfo);
        $csgoFrameFieldConfig = [
            'match_id' => null,
            'battle_id' => null,
            'battle_order' => null,
            'map' => [],
            'duration' => null, //对局时长
            'is_finished' => null, //是否已结束
            'match_scores' => [], //比赛比分
            'match_winner' => [], //比赛获胜战队
            'battle_winner' => [], //对局获胜战队
            'starting_ct' => [], //开局作为反恐精英
            'starting_t' => [], //开局作为恐怖分子
            'is_pause' => null, //是否暂停
            'is_live' => null, //是否比赛正式数据
            'current_round' => null, //当前回合
            'is_freeze_time' => null, //是否冻结时间
            'in_round_timestamp' => '', //回合内时间戳
            'round_time' => '', //回合内时间
            'is_bomb_planted' => null, //炸弹是否已安放
            'time_since_plant' => '', //安放炸弹后时间
            'side' => [], //阵营
        ];
        $csgoFrameFieldConfig['match_id'] = $matchId;
        $csgoFrameFieldConfig['battle_id'] = $battleId;
        $csgoFrameFieldConfig['battle_order'] = $battleOrder;
        $csgoFrameFieldConfig['map'] = self::getNameConvertCsgoMapData($csgoMatchData['map']['name']);
        $csgoFrameFieldConfig['duration'] = $csgoMatchData['current_timestamp'];
        $csgoFrameFieldConfig['is_finished'] = $csgoMatchData['game']['finished'];
        $csgoFrameFieldConfig['match_scores'] = $matchWinnerData['match_winner_soure'];
        $csgoFrameFieldConfig['match_winner'] = $matchWinnerData['match_winner'];
        $csgoFrameFieldConfig['starting_ct'] = isset($ctTeam) ? $ctTeam:[];
        $csgoFrameFieldConfig['starting_t'] = isset($terroristTeam) ? $terroristTeam:[];
        $csgoFrameFieldConfig['is_pause'] = $csgoMatchData['paused'];
        $csgoFrameFieldConfig['is_live'] = true;
        $csgoFrameFieldConfig['current_round'] = $csgoMatchData['round'];
        $csgoFrameFieldConfig['is_bomb_planted'] = $csgoMatchData['bomb_planted'];
        $csgoFrameFieldConfig['time_since_plant'] = $csgoMatchData['bomb_planted']==true ? $socketTime:'';
        $csgoFrameFieldConfig['side'] = $sides;

        return [
            'battle_order' => $battleOrder,
            'trans_data' => $csgoFrameFieldConfig
        ];
    }
    // match比赛比分和获胜战队
    public static function matchWinnerAndScore($matchInfo,$terroristScore,$teamTid,$ctScore,$teamCtid,$originId)
    {
        if($terroristScore>$ctScore){
            $winnerTeamId = self::teamIdChangeOwnTeamId($teamTid,$originId);
            $matchWinner = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId);
            $matchWinnerSoure = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId,$terroristScore);
        }
        if($terroristScore<$ctScore){
            $winnerTeamId = self::teamIdChangeOwnTeamId($teamCtid,$originId);
            $matchWinner = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId);
            $matchWinnerSoure = self::teamIdSelectTeamsData($matchInfo,$winnerTeamId,$ctScore);
        }
        if($terroristScore==$ctScore){
            $matchWinner = [];
            $matchWinnerSoure = [];
        }
        return [
            'match_winner' => $matchWinner,
            'match_winner_soure' => $matchWinnerSoure,
        ];
    }
    // 战队阵营数据结构处理
    public static function staticSideStructuralData($teamsStaticData,$matchInfo)
    {
//        self::$sidesArray = [];
        $sidesArray = [];
        foreach ($teamsStaticData as $csgoSide => $team){
            $teamId = self::teamIdChangeOwnTeamId($team['id'],3);
            $side['side'] = $csgoSide;
            $side['team'] = self::teamIdSelectTeamsData($matchInfo,$teamId);
            $side['score'] = $team['round_score'];
            // 处理Players数据
            $side['players'] = self::sidePlayersStructuralData($team['players'],$csgoSide);
            // 反恐精英回合历史
            $side['rounds_history'] = [];
            $sidesArray[] = $side;
//            self::$sidesArray[] = $side;
        }
        return $sidesArray;
    }
    // 处理battlePlayers数据
    public static function sidePlayersStructuralData($teamPlayersData,$side)
    {
//        self::$playersArray = [];
        $playersArray = [];
        foreach ($teamPlayersData as $key => $player) {
            $playerData = self::getCsgoPlayerIdAndName($player['id']);
            // side-Players数据结构
            $sidePlayerFieldConfig = [
                'side' => '',
                'player' => [],
                'weapon' => [],
                'has_kevlar' => null, // 是否有防弹衣
                'has_helmet' => null, // 是否有头盔
                'has_defusekit' => null, // 是否有拆弹器
                'has_bomb' => null, // 是否有C4炸弹
                'grenades' => [], // 投掷物
                'hp' => null, // 血量
                'is_alive' => null, // 存活状态
                'blinded_time' => '', // 被致盲时长
                'money' => null, // 金钱
                'kills' => null, // 击杀
                'headshot_kills' => null, // 爆头击杀
                'deaths' => null, // 死亡
                'assists' => null, // 助攻
                'flash_assist' => null, // 闪光弹助攻
                'adr' => '', // 平均每局伤害
                'first_kills' => null, // 首先击杀
                'first_deaths' => null, // 首先死亡
                'first_kills_diff' => null, // 首杀差
                'multi_kills' => null, // 多杀
                'one_on_x_clutches' => null, // 1VN残局获胜
                'kast' => '', // KAST
                'ping' => null, // PING
            ];
            $sidePlayerFieldConfig['side'] = $side;
            $sidePlayerFieldConfig['player']['player_id'] = $playerData['player_id'];
            $sidePlayerFieldConfig['player']['nick_name'] = $playerData['nick_name'];
            $sidePlayerFieldConfig['weapon'] = isset($player['primary_weapon']['name']) ?
                self::weaponNameChangeData($player['primary_weapon']['name']) : [];
            $sidePlayerFieldConfig['hp'] = $player['hp'];
            $sidePlayerFieldConfig['is_alive'] = $player['is_alive'];
            $sidePlayerFieldConfig['kills'] = $player['kills'];
            $sidePlayerFieldConfig['deaths'] = $player['deaths'];
            $playersArray[] = $sidePlayerFieldConfig;
        }
        return $playersArray;
    }
    // 数据源武器名称转换本库武器数据
    public static function weaponNameChangeData($weaponName)
    {
        if(isset(self::$weaponNameCreArray[$weaponName])){
        }else {
            self::$weaponNameCreArray[$weaponName] = MetadataCsgoWeapon::find()
                ->select(['id as weapon_id','external_id','external_name','name','image'])
                ->where(['name' => $weaponName])->one()->toArray();
        }
        return self::$weaponNameCreArray[$weaponName];
    }
    // 数据源teamId转换本库teamId
    public static function teamIdChangeOwnTeamId($relTeamId,$originId)
    {
        if($relTeamId) {
            if (isset(self::$teamIdsArray[$relTeamId])) {
            } else {
                self::$teamIdsArray[$relTeamId] =
                    intval(self::getMainIdByRelIdentityId('team', $relTeamId, $originId));
            }
            return self::$teamIdsArray[$relTeamId];
        }else{
            return null;
        }
    }
    // 本库teamId查询战队信息
    public static function teamIdSelectTeamsData($matchInfo,$teamId,$score=null)
    {
        if (isset(self::$teamIdsDataArray[$teamId])) {
        } else {
            self::$teamIdsDataArray[$teamId] = Team::find()->select([
                    'id as team_id','name','image'
                ])->where(['id' => $teamId])->asArray()->one();
            self::$teamIdsDataArray[$teamId]['opponent_order'] =
                self::teamIdChangeTeamOrder($teamId,$matchInfo);
        }
        if($score){
            self::$teamIdsDataArray[$teamId]['score'] = $score;
        }
        return self::$teamIdsDataArray[$teamId];
    }
    // 主客队order
    public static function teamIdChangeTeamOrder($teamIdResult,$matchInfo)
    {
        if (isset(self::$teamIdsOrderArray[$teamIdResult])) {
        } else {
            self::$teamIdsOrderArray[$teamIdResult] = self::getTeamOrderByTeamId($teamIdResult,$matchInfo);
        }
        return self::$teamIdsOrderArray[$teamIdResult];
    }
    // set sidePlayers数据
    public static function setSidePlayers($playersResultData)
    {
        self::$sidePlayerFieldConfig = array_merge(self::$sidePlayerFieldConfig, $playersResultData);
    }
    // set Frames数据
    public static function setFrameFieldData($frameResultData)
    {
        self::$csgoFrameFieldConfig = [
            'match_id' => null,
            'battle_id' => null,
            'battle_order' => null,
            'map' => [],
            'duration' => null, //对局时长
            'is_finished' => null, //是否已结束
            'match_scores' => [], //比赛比分
            'match_winner' => [], //比赛获胜战队
            'battle_winner' => [], //对局获胜战队
            'starting_ct' => [], //开局作为反恐精英
            'starting_t' => [], //开局作为恐怖分子
            'is_pause' => null, //是否暂停
            'is_live' => null, //是否比赛正式数据
            'current_round' => null, //当前回合
            'is_freeze_time' => null, //是否冻结时间
            'in_round_timestamp' => '', //回合内时间戳
            'round_time' => '', //回合内时间
            'is_bomb_planted' => null, //炸弹是否已安放
            'time_since_plant' => '', //安放炸弹后时间
            'side' => [], //阵营
        ];
        self::$csgoFrameFieldConfig = array_merge(self::$csgoFrameFieldConfig, $frameResultData);
    }
    // set Events数据
    public static function setEventFieldData($eventResultData)
    {
        self::$csgoEventFieldConfig = array_merge(self::$csgoEventFieldConfig, $eventResultData);
    }
    // 数据源对局地图转换本库地图
    public static function getNameConvertCsgoMapData($mapName)
    {
        $mapData = MetadataCsgoMap::find()->alias('map')
            ->select([
                'map.id','map.name','map.name_cn','map.external_id','map.external_name','map.short_name','map.is_default'
                ,'map.square_image','map.rectangle_image','map.thumbnail','type.c_name','type.e_name'
            ])
            ->leftJoin("enum_csgo_map as type", "type.id = map.map_type")
            ->where(['map.name'=>$mapName])->asArray()->one();
        $mapStructureData = [];
        if(!empty($mapData))
            $mapStructureData = [
                'map_id' => $mapData['id'],
                'name' => $mapData['name'],
                'name_cn' => $mapData['name_cn'],
                'external_id' => $mapData['external_id'],
                'external_name' => $mapData['external_name'],
                'short_name' => $mapData['short_name'],
                'map_type' => $mapData['e_name'],
                'map_type_cn' => $mapData['c_name'],
                'is_default' => $mapData['is_default']==1 ? true:false,
                'image' => [
                    'square_image' => ImageConversionHelper::showFixedSizeConversion($mapData['square_image'],252,126),
                    'rectangle_image' => ImageConversionHelper::showFixedSizeConversion($mapData['rectangle_image'],675,84),
                    'thumbnail' => ImageConversionHelper::showFixedSizeConversion($mapData['thumbnail'],1380,930),
                ],
            ];
        return $mapStructureData;
    }
    public static function getCsgoPlayerIdAndName($relPlayerId)
    {
        if($relPlayerId) {
            $playerId = self::getMainIdByRelIdentityId('player', $relPlayerId, '3');
            if ($playerId) {
                $playerInfoUs = self::getPlayInfoById($playerId);
                if (!empty($playerInfoUs))
                    $playerNickName = $playerInfoUs['nick_name'];
            }
        }else{
            $playerId = null;
        }
        return [
            'nick_name' => isset($playerNickName) ? $playerNickName:'',
            'player_id' => $playerId,
        ];
    }
    public static function transEventsWs($csgoMatchData,$matchId,$socketTime)
    {
        $payloadData = $csgoMatchData['payload'];
        // 获取比赛的信息
//        $matchInfo = self::getMatchInfoByMatchId($matchId);
        $battleInfo = self::getbattleIdByMatchIdAndOrder($matchId,
            $csgoMatchData['match']['id'],$csgoMatchData['game']['id'],$socketTime);
        $battleId = isset($battleInfo['battle_id']) ? $battleInfo['battle_id']:null;
        $battleOrder = isset($battleInfo['battle_order']) ? $battleInfo['battle_order']:null;
        $eventId = Common::getGoalIdByPandascoreName($payloadData['type']);
        self::setEventFieldData([
            'match_id' => $matchId,
            'battle_id' => $battleId,
            'battle_order' => $battleOrder,
            'event_id' => $eventId,
            'ingame_timestamp' => self::$csgoFrameFieldConfig['in_round_timestamp'],
            'round_ordinal' => $payloadData['round_number'],
            'is_bomb_planted' => self::$csgoFrameFieldConfig['is_bomb_planted'],
            'time_since_plant' => self::$csgoFrameFieldConfig['time_since_plant'],
        ]);
        self::transCsgoEventType($payloadData['type'],$payloadData);
        return [
            'battle_order' => $battleOrder,
            'trans_data' => self::$csgoEventFieldConfig
        ];
    }
    public static function transCsgoEventType($eventType,$payloadData)
    {
        switch ($eventType){
            case self::CSGO_BATTLE_END:
                self::setEventFieldData([
                    'event_type' => self::CSGO_BATTLE_END,
                    'winner' => [],
                ]);
                break;
            case self::CSGO_ROUND_START:
            case self::CSGO_BATTLE_PAUSE:
            case self::CSGO_BATTLE_UNPAUSE:
                $cogoEventTypeArray = [];
                break;
            case self::CSGO_BATTLE_START:
                self::setEventFieldData([
                    'event_type' => self::CSGO_BATTLE_START,
                    'map' => [],
                    'starting_ct' => [],
                    'starting_t' => [],
                ]);
                break;
            case self::CSGO_BATTLE_RELOADED:
                self::setEventFieldData([
                    'event_type' => self::CSGO_BATTLE_RELOADED,
                    'map' => [],
                    'starting_ct' => [],
                    'starting_t' => [],
                ]);
                break;
            case self::CSGO_PLAYER_JOINED:
                self::setEventFieldData([
                    'event_type' => self::CSGO_PLAYER_JOINED,
                    'player_id' => null,
                    'nick_name' => ''
                ]);
                break;
            case self::CSGO_PLAYER_QUIT:
                self::setEventFieldData([
                    'event_type' => self::CSGO_PLAYER_QUIT,
                    'player_id' => null,
                    'nick_name' => '',
                    'side' => '',
                ]);
                break;
            case self::CSGO_ROUND_END:
                self::setEventFieldData([
                    'event_type' => self::CSGO_ROUND_END,
                    'round_end_type' => $payloadData['outcome'],
                    'winner_side' => isset($payloadData['team_side']) ? $payloadData['team_side']:'',
                    'ct_score' => null,
                    't_score' => null,
                ]);
                break;
            case self::CSGO_EVENT_KILL:
                if(isset($payloadData['killer']['object']['id'])){
                    $killerData = self::getCsgoPlayerIdAndName($payloadData['killer']['object']['id']);
                }
                if(isset($payloadData['killed']['object']['id'])){
                    $killedData = self::getCsgoPlayerIdAndName($payloadData['killed']['object']['id']);
                }
                self::setEventFieldData([
                    'event_type' => self::CSGO_PLAYER_KILL,
                    'killer' => [
                        'player_id' => @$killerData['player_id'],
                        'nick_name' => @$killerData['nick_name'],
                        'side' => $payloadData['killer']['object']['team_side']=='terrorists' ? 'terrorists':'ct',
                        'position' => ''
                    ],
                    'victim' => [
                        'player_id' => $killedData['player_id'],
                        'nick_name' => $killedData['nick_name'],
                        'side' => $payloadData['killed']['object']['team_side']=='terrorists' ? 'terrorists':'ct',
                        'position' => ''
                    ],
                    'assist' => [],
                    'flashassist' => [],
                    'weapon' => [],
                    'damage' => null,
                    'hit_group' => '',
                    'is_headshot' => null,
                    'special_description' => '' //特殊描述（headshot、penetrated、throughsmoke、attackerblind、noscope）
                ]);
                break;
            case self::CSGO_BOMB_PLANTED:
                self::setEventFieldData([
                    'event_type' => self::CSGO_BOMB_PLANTED,
                    'player_id' => null,
                    'nick_name' => '',
                    'side' => '',
                    'position' => '',
                    'survived_players_ct' => null,
                    'survived_players_t' => null
                ]);
                break;
            case self::CSGO_BOMB_DEFUSED:
                self::setEventFieldData([
                    'event_type' => self::CSGO_BOMB_DEFUSED,
                    'player_id' => null,
                    'nick_name' => '',
                    'side' => '',
                    'position' => ''
                ]);
                break;
            case self::CSGO_PLAYER_SUICIDE:
                self::setEventFieldData([
                    'event_type' => self::CSGO_PLAYER_SUICIDE,
                    'player_id' => null,
                    'nick_name' => '',
                    'side' => '',
                    'position' => '',
                    'weapon' => []
                ]);
                break;
            default:
                break;
        }
    }

    public static $csgoEventFieldConfig = [
        'match_id' => null,
        'battle_id' => null,
        'battle_order' => null, //对局排序
        'battle_timestamp' => null, //对局内时间戳
        'event_id' => null,
        'event_type' => '',
        'round_ordinal' => null, //回合序号
        'in_round_timestamp' => '', //回合内时间戳
        'round_time' => '', //回合内时间
        'is_bomb_planted' => null, //炸弹是否已安放
        'time_since_plant' => '', //安放炸弹后时间
    ];
    public static $csgoFrameFieldConfig = [
        'match_id' => null,
        'battle_id' => null,
        'battle_order' => null,
        'map' => [],
        'duration' => null, //对局时长
        'is_finished' => null, //是否已结束
        'match_scores' => [], //比赛比分
        'match_winner' => [], //比赛获胜战队
        'battle_winner' => [], //对局获胜战队
        'starting_ct' => [], //开局作为反恐精英
        'starting_t' => [], //开局作为恐怖分子
        'is_pause' => null, //是否暂停
        'is_live' => null, //是否比赛正式数据
        'current_round' => null, //当前回合
        'is_freeze_time' => null, //是否冻结时间
        'in_round_timestamp' => '', //回合内时间戳
        'round_time' => '', //回合内时间
        'is_bomb_planted' => null, //炸弹是否已安放
        'time_since_plant' => '', //安放炸弹后时间
        'side' => [], //阵营
    ];
    // 阵营下选手数据格式
    public static $sidePlayerFieldConfig = [
        'side' => '',
        'player' => [],
        'weapon' => [],
        'has_kevlar' => null, // 是否有防弹衣
        'has_helmet' => null, // 是否有头盔
        'has_defusekit' => null, // 是否有拆弹器
        'has_bomb' => null, // 是否有C4炸弹
        'grenades' => [], // 投掷物
        'hp' => null, // 血量
        'is_alive' => null, // 存活状态
        'blinded_time' => '', // 被致盲时长
        'money' => null, // 金钱
        'kills' => null, // 击杀
        'headshot_kills' => null, // 爆头击杀
        'deaths' => null, // 死亡
        'assists' => null, // 助攻
        'flash_assist' => null, // 闪光弹助攻
        'adr' => '', // 平均每局伤害
        'first_kills' => null, // 首先击杀
        'first_deaths' => null, // 首先死亡
        'first_kills_diff' => null, // 首杀差
        'multi_kills' => null, // 多杀
        'one_on_x_clutches' => null, // 1VN残局获胜
        'kast' => '', // KAST
        'ping' => null, // PING
    ];
    // 武器数组
    public static $weaponNameCreArray = [];
    // 选手数组
    public static $playersArray = [];
    // 两边阵营
    public static $sidesArray = [];
    // 主/客 战队数组
    public static $teamIdsArray = [];
    // 本库战队 主/客 Order
    public static $teamIdsOrderArray = [];
    // 主/客 战队信息数组
    public static $teamIdsDataArray = [];

    public static $conversionRes = [
        'battle_order' => null,
        'trans_data' => 'hello'
    ];
}