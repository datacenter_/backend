<?php
namespace app\modules\task\services\wsdata\pandascore;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\match\models\MatchBattle;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\Mapping;

class PandascoreLolApi extends HotBase
{
    private static $instance;
    public static function run($tagInfo, $taskInfo)
    {
        return true;
        $PandascoreLolApi = self::getInstance();
        $redis = $PandascoreLolApi->redis;
        $conversionRes = null;
        $gameId = $tagInfo['ext_type'];
        $info = @json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $rel_match_id = $info['match_id'];
        $socket_id = (Int)$info['socket_id'];
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        //根据数据源的比赛ID 获取平台的ID
        $redis_match_lol_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":lol:".$originId;
        $matchId = $redis->hGet($redis_match_lol_relation,$rel_match_id);
        if (!$matchId){
            $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
            if (empty($matchId)){
                return true;
            }
            $redis->hSet($redis_match_lol_relation,$rel_match_id,$matchId);
        }
        //判断事件的类型
        $type = $info['type'];
        if ($type == 'no'){
            if (!$info['match_data']['game']['finished']){
                $socket_time_num = strtotime($socket_time);
                $frames_time = $redis->get(Consts::MATCH_ALL_INFO.":".$matchId.":current:frames_time");
                if ($frames_time){
                    $frames_time_cha = $socket_time_num - $frames_time;
                    if ($frames_time_cha < 5){
                        return 'no-console';
                    }
                    $redis->set(Consts::MATCH_ALL_INFO.":".$matchId.":current:frames_time",$socket_time_num);
                }else{
                    $redis->set(Consts::MATCH_ALL_INFO.":".$matchId.":current:frames_time",$socket_time_num);
                }
            }
        }
        //获取比赛信息
        //获取比赛数据
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $matchInfo = $redis->hGetAll($matchInfoRedisName);
        if (!$matchInfo){
            //获取比赛信息
            $matchInfo = self::getMatchAllInfoByMatchId($matchId);
            if (empty($matchInfo)){
                return 'no match info';
            }
            $redis->hMSet($matchInfoRedisName,$matchInfo);
        }
        $battleId = null;
        switch ($type)   //websocket类型
        {
            case "no":
                $allEvents = self::getEventsByRelMatchId($originId,$rel_match_id);
                $conversionRes = self::transFrames($redis,$info,$matchInfo,$allEvents,$socket_time,$socket_id);
                if ($conversionRes){
                    self::refreshInfo($matchId,$conversionRes);
                    $battleId = $conversionRes['battleId'];
                    //首先获取时间是否可以保存了
                    $match_battle_duration = (Int)$info['match_data']['current_timestamp'];
                    $gold_or_experience_diff_time = $redis->get(Consts::MATCH_ALL_INFO.":".$matchId.':current:battles:'.$battleId.':gold_or_experience_diff_time');
                    if ($gold_or_experience_diff_time){
                        $gold_diff_time_cha = $match_battle_duration - (Int)$gold_or_experience_diff_time;
                        //间隔30秒保存
                        if ($gold_diff_time_cha >= 30) {
                            self::setGoldDiffTimeLine($battleId, $conversionRes['gold_diff_timeline']);
                            $redis->set(Consts::MATCH_ALL_INFO.":".$matchId.':current:battles:'.$battleId.':gold_or_experience_diff_time',$match_battle_duration);
                        }
                    }else{
                        self::setGoldDiffTimeLine($battleId, $conversionRes['gold_diff_timeline']);
                        $redis->set(Consts::MATCH_ALL_INFO.":".$matchId.':current:battles:'.$battleId.':gold_or_experience_diff_time',$match_battle_duration);
                    }
                    if (isset($conversionRes['match']['status']) && $conversionRes['match']['status'] ==3){
                        self::changeHotData($originId,$gameId,$matchId);
                    }
                }
                break;
            case "events":
                $conversionRes = self::transEvents($redis,$info,$matchId,$socket_time,$socket_id);
                $battleId = $conversionRes['battleId'];
                break;
        }
        return $battleId;
    }
    /**
     * @param $info
     * @param $matchInfo
     * @param $allEvents
     * 处理frame事件
     */
    public static function transFrames($redis,$info,$matchInfo,$allEvents,$socket_time,$socket_id){
        $matchId = $matchInfo['id'];
        $blue = $red = $players = [];
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if (isset($match_data['type'])){
            return $match_data_info;
        }
        $duration = $match_data['current_timestamp'];
        $winner_id = null;
        $RealTimeInfo = [
            'team_1_score' => 0,
            'team_2_score' => 0,
            'is_battle_detailed' => 1,
//            'is_draw' => 2,
//            'is_forfeit' => 2,
            'finished' => 1,
//            'default_advantage' => null,
            'status' => 2,
//            'winner' => null,
//            'map_info' => '[{"map":"1"}]'
        ];
        //找到对应的平台的match_id
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchInfo['id'],$match_data['match']['id'],$match_data['game']['id'],$socket_time,$match_data['current_timestamp']);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $match_start = $battle_Info['match_start'];
        if ($match_start == 1){
            $begin_at = strtotime($socket_time)-(Int)$match_data['current_timestamp'];
            $RealTimeInfo['begin_at'] = date("Y-m-d H:i:s",$begin_at);
        }
        $battle_is_finished = false;
        $battle_finished = 2;
        //查看当前battle是否完成
        if (isset($match_data['game']['finished']) && isset($match_data['game']['winner_id'])){
            $battle_finished = 3;
            $battle_is_finished = true;
            $teamId = self::getMainIdByRelIdentityId('team',$match_data['game']['winner_id'],'3');
            $winner_id = self::getTeamOrderByTeamId($teamId,$matchInfo);
            $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_real_time_info";
            $match_real_time_info['winner'] = $winner_id;
            $redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
        }
        // 获取战队映射情况，battle详情里面有两个战队
        $battleFramesInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => '',
        ];
        $is_pause = $match_data['paused'] == true ? 1 : 2;
        $detail = [
            'is_confirmed' => 1,
            'is_pause' => $is_pause,
            'is_live' => 1
        ];
        //battles->frames
        //组合数据
        $battleFramesInfoConfig = [
            'game' => 2,
            'is_draw' => 2,
            'is_forfeit' => 2,
            'is_default_advantage' => 2,
            'status' => $battle_finished,
            'order' => $battle_order,
            'duration' => (String)$duration,
            'map' => 1,
            'winner' => $winner_id,
            'is_battle_detailed' => 1,
            'flag' => 1,
            'deleted' => 2,
            'deleted_at' => null
        ];
        $teamStatic = [];
        //gold_diff_timeline
        $gold_diff_timeline_teams = [];
        //pick_array
        $pick_array = [];
        $pick_array_num = 0;
        //$teamStatic
        $turrets = [
            'top_outer_turret'=> null,
            'top_inner_turret'=> null,
            'top_inhibitor_turret'=> null,
            'mid_outer_turret'=> null,
            'mid_inner_turret'=> null,
            'mid_inhibitor_turret'=> null,
            'bot_outer_turret'=> null,
            'bot_inner_turret'=> null,
            'bot_inhibitor_turret'=> null,
            'top_nexus_turret'=> null,
            'bot_nexus_turret'=> null
        ];
        $inhibitors = [
            'top_inhibitor'=> null,
            'mid_inhibitor'=> null,
            'bot_inhibitor'=> null
        ];
        $nexus = null;
        $building_status = [
            'turrets' => $turrets,
            'inhibitors' => $inhibitors,
            'nexus' => $nexus
        ];
        //获取信息
        $ws_player_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_api:battles:'.$battleId.':players_kills_counters';
        $ws_player_array = $redis->hGetAll($ws_player_redis_name);
        $match_data['blue']['faction'] = 'blue';
        $match_data['red']['faction'] = 'red';
        $winner_team_and_order_relation = [];
        $playersRest = $teamInfosRest = [$match_data['blue'],$match_data['red']];
        foreach ($teamInfosRest as $key =>$team){
            $team_deaths = 0;
            $team_assists = 0;
            $teamStatic[$key]['rel_team_id']=@$team['id'];
            $teamStatic[$key]['rel_identity_id']=$team['id'];
            $teamStatic[$key]['kills'] = $team['kills'];
            $teamStatic[$key]['wards_purchased'] = null;
            $teamStatic[$key]['wards_placed'] = null;
            $teamStatic[$key]['wards_kills'] = null;
            $teamStatic[$key]['gold'] = $team['gold'];
            $teamStatic[$key]['experience'] = null;
            $teamStatic[$key]['experience_diff'] = null;
            $teamStatic[$key]['faction'] = $team['faction'];
            $teamStatic[$key]['inhibitor_kills'] = $team['inhibitors'];
            $teamStatic[$key]['turret_kills'] = $team['towers'];
            $teamStatic[$key]['baron_nashor_kills'] = $team['nashors'];
            $teamStatic[$key]['rift_herald_kills'] = $team['herald'];
            $teamStatic[$key]['dragon_kills'] = $team['drakes'];
            $teamStatic[$key]['building_status'] = json_encode($building_status,320);
            $dragon_kills_detail = self::conversionDragonKillsId($allEvents,$team['faction'],$match_data['match']['id'],$match_data['game']['id']);
            if (empty($dragon_kills_detail)){
                $teamStatic[$key]['dragon_kills_detail'] = null;
            }else{
                $teamStatic[$key]['dragon_kills_detail'] = json_encode($dragon_kills_detail,320);
            }
            $teamId = self::getMainIdByRelIdentityId('team',@$team['id'],'3');
            $teamOrder = self::getTeamOrderByTeamId($teamId,$matchInfo);
            $teamStatic[$key]['order']= $teamOrder;
            if (!empty($winner_id) && $winner_id == $teamOrder){
                $teamStatic[$key]['score']= 1;
            }else{
                $teamStatic[$key]['score']= 0;
            }
            $teamStatic[$key]['team_id']= $teamId;
            $winner_team_and_order_relation[$teamOrder] = $teamId;
            $teamscroekey = $teamOrder == 1 ? 'team_1_score' : 'team_2_score';
            $RealTimeInfo[$teamscroekey] =$team['score'];
            //经济
            $gold_diff_timeline_teams[] = $team['gold'];
            //玩家
            $players_key = $team['players'];
            $keypp_key = 1;
            foreach ($players_key as $keypp => $itempp){
                $seed = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE,$keypp);
                $player = [];
                $player['seed'] = $seed;
                $player['faction'] = $team['faction'];
                $player['role'] = $seed;
                $player['lane'] = Common::getLaneIDByRoleName($keypp);
                $player_id = self::getMainIdByRelIdentityId('player',$itempp['id'],'3');
                $player['player_id'] = $player_id;
                $player['nick_name'] = null;
                $playerInfoUs = self::getPlayInfoById($player_id);
                if ($playerInfoUs){
                    $player['nick_name'] = $playerInfoUs['nick_name'];
                }
                $player['rel_nick_name'] = $itempp['name'];
                $player['game'] = @$info['game_id'];
                $player['match'] = @$matchInfo['id'];
                $player['summoner_spells'] = json_encode(self::conversionPlayerSummonerSpellsIds($itempp['summoner_spells']));
                $player_champion = @self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $itempp['champion']['id'], '3');
                $player['champion'] = $player_champion;
                $player['level']= $itempp['level'];
                $player['alive'] = $itempp['hp']>0 ? 1:2;
                $player['ultimate_cd'] = null;
                $player['coordinate'] = null;
                $player['kills']= $itempp['kills'];
                $player['double_kill']= null;
                $player['triple_kill']= null;
                $player['quadra_kill']= null;
                $player['penta_kill']= null;
                $player['largest_multi_kill']= null;
                $player['largest_killing_spree']= null;
                $player_deaths = $itempp['deaths'];
                $team_deaths = $team_deaths + $player_deaths;
                $player['deaths'] = $player_deaths;
                $player_assists = $itempp['assists'];
                $team_assists = $team_assists + $player_assists;
                $player['assists']= $player_assists;
                // 需要计算得属性
                $deaths = $player['deaths']?$player['deaths']:1;
                $player['kda'] = (String)round((($player['kills'] + $player['assists'])/$deaths),2);
                $player['cs']= $itempp['cs'];
                $player['minion_kills']= null;
                $player['total_neutral_minion_kills']= null;
                $player['neutral_minion_team_jungle_kills']= null;
                $player['neutral_minion_enemy_jungle_kills']= null;
                $player['cspm']= (String)round(($itempp['cs']/($match_data['current_timestamp']/60)),2);
                $player['gold_earned']= null;
                $player['gold_spent']= null;
                $player['gold_remaining']= null;
                $player['gpm'] = null;
                $player['gold_earned_percent'] = null;
                $player['experience'] = null;
                $player['xpm'] = null;
                $player['damage_to_champions']= null;
                $player['dpm_to_champions'] = null;
                $player['damage_percent_to_champions'] = null;
                $player['damage_to_champions_physical']= null;
                $player['damage_to_champions_magic']= null;
                $player['damage_to_champions_true']= null;
                $player['damage_taken']= null;
                $player['dtpm'] = null;
                $player['damage_taken_percent'] = null;
                $player['damage_taken_physical']= null;
                $player['damage_taken_magic']= null;
                $player['damage_taken_true']= null;
                $player['damage_to_towers']= null;
                $player['total_damage']= null;
                $player['total_damage_physical']= null;
                $player['total_damage_magic']= null;
                $player['total_damage_true']= null;
                $player['total_heal']= null;
                $player['total_crowd_control_time']= null;
                $player['wards_purchased']= null;
                $player['wards_placed']= null;
                $player['wards_kills']= null;
                $player['turrets_kills']= null;
                $player['runes']= null;
                $player['abilities_timeline']= null;
                $player['keystone']= null;
                $player['respawntimer']= null;
                $player['health']= null;
                $player['health_max']= null;
                $player['damage_selfmitigated']= null;
                $player['damage_shielded_on_teammates']= null;
                $player['damage_to_buildings']= null;
                $player['damage_to_objectives']= null;
                $player['total_crowd_control_time_others']= null;
                $player['vision_score']= null;
//                $player['items']= json_encode(self::conversionPlayerItemsIds($itempp['items']));//以前查数据库
                //判断是不是重刷来的数据
                if (isset($itempp['items_timeline'])){
                    $playerItemsInfoResultEnd = [];
                    $items_timeline_item = @json_encode(end($itempp['items_timeline'])['items'],320);
                    if ($items_timeline_item){
                        foreach ($items_timeline_item as $key_res => $val_res){
                            $ItemsInfo = null;
                            if ($val_res){
                                $ItemsInfo = [
                                    'item_id' => $val_res,
                                    'purchase_time' => null,
                                    'cooldown' => null
                                ];
                            }
                            $playerItemsInfoResultEnd[] = $ItemsInfo;
                        }
                    }
                    $player['items'] = $playerItemsInfoResultEnd;
                    $player['items_timeline'] = @json_encode($itempp['items_timeline'],320);
                    //在这set一下redis
                    self::setPlayerItemsIds($player['items_timeline'],$itempp['id'],$battleId,$matchInfo['id']);
                }else{
                    $playerItems = self::conversionPlayerItemsIdsFromRedis($match_data['current_timestamp'],$itempp['items'],$itempp['id'],$battleId,$matchInfo['id'],3);//查redis
                    $player['items'] = @$playerItems['items'];
                    $player['items_timeline'] = @$playerItems['items_timeline'];
                }
                $player['rel_team_id']= @$team['id'];
                $player['rel_player']= '';
                $player['rel_identity_id']= (String)$itempp['id'];
                $player['rel_id']= $itempp['id'];
                $player['team_order']= $teamOrder;
                $player['order']= $keypp_key;
                $participation_team_kills = $team['kills'] ? $team['kills'] : 1;
                $player['participation'] = round((($player['kills'] + $player['assists'])/$participation_team_kills),4);
                $player['damage_conversion_rate'] = null;
                $player['team_id'] = $teamId;
                //计算
                if (empty(@$ws_player_array[$player_id])){
                    $turret_kills = 0;
                    $inhibitor_kills = 0;
                    $rift_herald_kills = 0;
                    $dragon_kills = 0;
                    $baron_nashor_kills = 0;
                }else{
                    $players_res_array = @json_decode(@$ws_player_array[$player_id],true);
                    $turret_kills = @$players_res_array['turret_kills'];
                    $inhibitor_kills = @$players_res_array['inhibitor_kills'];
                    $rift_herald_kills = @$players_res_array['rift_herald_kills'];
                    $dragon_kills = @$players_res_array['dragon_kills'];
                    $baron_nashor_kills = @$players_res_array['baron_nashor_kills'];
                }
                $player['turret_kills'] =$turret_kills;
                $player['inhibitor_kills'] =$inhibitor_kills;
                $player['rift_herald_kills'] =$rift_herald_kills;
                $player['dragon_kills'] =$dragon_kills;
                $player['baron_nashor_kills'] =$baron_nashor_kills;
                $players[]=$player;
                $pick_array[$pick_array_num]['hero'] = $player_champion;
                $pick_array[$pick_array_num]['team'] = $teamId;
                $pick_array[$pick_array_num]['type'] = 2;
                $pick_array[$pick_array_num]['order'] = null;
                $keypp_key ++;
                $pick_array_num ++;
            }
            $teamStatic[$key]['deaths'] = $team_deaths;
            $teamStatic[$key]['assists'] = $team_assists;
        }
        //蓝队减去红队
        $gold_diff = (Int)($teamInfosRest[0]['gold'] - $teamInfosRest[1]['gold']);
        $teamStatic[0]['gold_diff'] = $gold_diff;
        $teamStatic[1]['gold_diff'] = -$gold_diff;

        $battleFramesInfo['static'] = $teamStatic;
        $battleFramesInfo['players'] = $players;
        $battleFramesInfo['ban_pick'] = $pick_array;
        //判断比赛是否结束
        if ($winner_id){
            $battleFramesInfoConfig['end_at'] = $socket_time;
            switch ($winner_id){
                case 1:
                    $RealTimeInfo['team_1_score'] = $RealTimeInfo['team_1_score'] + 1;
                    break;
                case 2:
                    $RealTimeInfo['team_2_score'] = $RealTimeInfo['team_2_score'] + 1;
                    break;
            }
            $statusInfo = WinnerHelper::lolWinnerInfo($RealTimeInfo['team_1_score'],$RealTimeInfo['team_2_score'],$matchInfo['number_of_games']);
            if($statusInfo['is_finish']==1){
                $RealTimeInfo['is_draw'] = $statusInfo['is_draw'];
                $RealTimeInfo['winner'] = $statusInfo['winner_team'];
                $RealTimeInfo['status'] = 3;
                $RealTimeInfo['end_at']= $socket_time;
            }
        }
        //统计龙的信息
        //计算 elites_status
        $elites_status_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_api:battles:'.$battleId.':';
        $enumIngameGoalInfoList = $redis->hGetAll('enum:ingame_goal');
        //rift_herald
        $rift_herald_exited = $redis->hGet($elites_status_redis_name.'rift_herald','exited');
        if ($rift_herald_exited){
            $rift_herald_status_val = 'exited';
            $rift_herald_spawntimer = null;
        }else{
            $rift_herald_status_val = 'alive';
            $rift_herald_spawntimer = null;
            if ($duration < 480){
                $rift_herald_status_val = 'not_spawned';
                $rift_herald_spawntimer = 480 - $duration;
            }elseif ($duration > 1200){
                $rift_herald_status_val = 'exited';
                $rift_herald_spawntimer = null;
                $redis->hSet($elites_status_redis_name.'rift_herald','exited',1);
            }else{
                $rift_herald_kill_time = $redis->hGet($elites_status_redis_name.'rift_herald','kill_time');
                if ($rift_herald_kill_time){
                    $rift_herald_spawntimer_time = $duration - $rift_herald_kill_time;
                    if ($rift_herald_spawntimer_time < 360){
                        $rift_herald_status_val = 'not_spawned';
                        $rift_herald_spawntimer = 360 - $rift_herald_spawntimer_time;
                    }
                }
            }
        }
        $rift_herald_array = @json_decode($enumIngameGoalInfoList[1],true);
        $rift_herald_name_val = @$rift_herald_array['ingame_obj_name'];
        $rift_herald_name_cn_val = @$rift_herald_array['ingame_obj_name_cn'];
        $rift_herald_status = [
            'status' => $rift_herald_status_val,
            'name' => self::checkAndChangeString($rift_herald_name_val),
            'name_cn' => self::checkAndChangeString($rift_herald_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($rift_herald_spawntimer)
        ];
        //dragon_status
        $dragon_status_status_val = 'alive';
        $dragon_status_spawntimer = null;
        $dragon_status_name = null;
        $dragon_status_name_cn= null;
        $dragon_status_Elder_kill_time = $redis->hGet($elites_status_redis_name.'dragon_status','elder_dragon_kill_time');
        if ($dragon_status_Elder_kill_time) {
            $dragon_status_Elder_spawntimer_cha = $duration - $dragon_status_Elder_kill_time;
            if ($dragon_status_Elder_spawntimer_cha < 360){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = 360 - $dragon_status_Elder_spawntimer_cha;
            }
            $dragon_elder_array = @json_decode($enumIngameGoalInfoList[6],true);
            $dragon_status_name = @$dragon_elder_array['ingame_obj_name'];
            $dragon_status_name_cn = @$dragon_elder_array['ingame_obj_name_cn'];
        }else{
            if ($duration < 300){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = 300 - $duration;
            }else{
                $dragon_status_kill_time = $redis->hGet($elites_status_redis_name.'dragon_status','kill_time');
                if ($dragon_status_kill_time){
                    //首先判断两队是否累计杀了4个了
                    $team_dragon_status = $redis->hGetAll($elites_status_redis_name.'team_dragon_status');
                    $blue_kill_num = $team_dragon_status['blue_kill_num'];
                    $red_kill_num = $team_dragon_status['red_kill_num'];
                    if ($blue_kill_num > 3 || $red_kill_num > 3){
                        $dragon_status_Elder_spawned_cha = $duration - $dragon_status_kill_time;
                        $dragon_status_spawntimer = 360 - $dragon_status_Elder_spawned_cha;
                        if ($dragon_status_spawntimer > 0){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_status_spawntimer;
                        }else{
                            $dragon_status_status_val = 'alive';
                            $dragon_status_spawntimer = null;
                        }
                        $dragon_elder_array = @json_decode($enumIngameGoalInfoList[6],true);
                        $dragon_status_name = @$dragon_elder_array['ingame_obj_name'];
                        $dragon_status_name_cn = @$dragon_elder_array['ingame_obj_name_cn'];
                    }else{
                        $dragon_status_spawntimer_cha = $duration - $dragon_status_kill_time;
                        if ($dragon_status_spawntimer_cha < 300){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = 300 - $dragon_status_spawntimer_cha;
                        }
                    }
                }
            }
        }
        $dragon_status = [
            'status' => $dragon_status_status_val,
            'name' => $dragon_status_name,
            'name_cn' => $dragon_status_name_cn,
            'spawntimer' => self::checkAndChangeInt($dragon_status_spawntimer)
        ];
        //baron_nashor_status
        $baron_nashor_spawntimer = null;
        $baron_nashor_array = @json_decode($enumIngameGoalInfoList[7],true);
        $baron_nashor_name_val = @$baron_nashor_array['ingame_obj_name'];
        $baron_nashor_name_cn_val = @$baron_nashor_array['ingame_obj_name_cn'];
        $baron_nashor_kill_time = $redis->hGet($elites_status_redis_name.'baron_nashor','kill_time');
        if ($baron_nashor_kill_time){
            $baron_nashor_spawntimer_time = $duration - $baron_nashor_kill_time;
            if ($baron_nashor_spawntimer_time > 360){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = 360 - $baron_nashor_spawntimer_time;
            }
        }else{
            if ($duration > 1200){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = 1200 - $duration;
            }
        }
        $baron_nashor_status = [
            'status' => $baron_nashor_status_val,
            'name' => self::checkAndChangeString($baron_nashor_name_val),
            'name_cn' => self::checkAndChangeString($baron_nashor_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($baron_nashor_spawntimer)
        ];
        $elites_status = [
            'rift_herald_status' => $rift_herald_status,
            'dragon_status' => $dragon_status,
            'baron_nashor_status' => $baron_nashor_status
        ];
        $detail['elites_status'] = @json_encode($elites_status);
        $battleFramesInfo['detail'] = $detail;
        // 获取battle详情,这里用来设置id
        $battleFramesInfo['base'] = $battleFramesInfoConfig;
        //只更新经济差
        $gold_diff_num = 0;
        if ($gold_diff_timeline_teams){
            $gold_diff_num = $gold_diff_timeline_teams[0] - $gold_diff_timeline_teams[1];
        }
        $gold_diff_timeline_array = [
            'ingame_timestamp' => $match_data['current_timestamp'],
            'gold_diff' => $gold_diff_num
        ];
        //处理是不是第一条数据
        //获取是否有数据了
        $is_have_ws_data_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_api:battles:'.$battleId.':is_have_ws_data';
        $is_have_ws_data = $redis->get($is_have_ws_data_redis_name);
        if (!$is_have_ws_data){
            //保存battle_start
            $event_battle_start = [
                'battle_id' => $battleId,
                'game' => 2,
                'match' => $matchId,
                'order' => 1,
                'event_id' => $socket_id,
                'event_type' => 'battle_start',
                'ingame_timestamp' => 0,
                'killer' => $teamStatic[0]['team_id'],
                'killer_faction' => (string)$teamStatic[0]['order'],
                'victim' => $teamStatic[1]['team_id'],
                'victim_faction' => (string)$teamStatic[1]['order'],
                'is_first_event' => 2,
                'first_event_type' => 0
            ];
            $event_battle_start_Data = [$event_battle_start];
            BattleService::setBattleInfo($battleId,'lol',$event_battle_start_Data,BattleService::DATA_TYPE_EVENTS);
            $redis->set($is_have_ws_data_redis_name,1);
        }
        //处理是否完成了battle
        if ($battle_is_finished){
            //保存battle_end
            $event_battle_end_order_id = self::getEventOrder($battleId);
            $event_battle_start = [
                'battle_id' => $battleId,
                'game' => 2,
                'match' => $matchId,
                'order' => $event_battle_end_order_id,
                'event_id' => $socket_id,
                'event_type' => 'battle_end',
                'ingame_timestamp' => (Int)$duration,
                'killer' => (Int)$winner_team_and_order_relation[$winner_id],
                'killer_faction' => (string)$winner_id,
                'is_first_event' => 2,
                'first_event_type' => 0
            ];
            $event_battle_start_Data = [$event_battle_start];
            BattleService::setBattleInfo($battleId,'lol',$event_battle_start_Data,BattleService::DATA_TYPE_EVENTS);
        }
        $match_data_info = [
            'match' => $RealTimeInfo,
            'battleId' => $battleId,
            'battle' => $battleFramesInfo,
            'gold_diff_timeline' => $gold_diff_timeline_array
        ];
        return $match_data_info;
    }

    public static function transEvents($redis,$info,$matchId,$socket_time,$socket_id)
    {
        $is_first_event_val = 2;
        $match_data_info = []; //返回值
        $match_data = $info['match_data'];
        if ($match_data['type'] =='hello'){
            return $match_data_info;
        }
        $ingame_timestamp = $match_data['ingame_timestamp'];
        $battle_Info = self::getbattleIdByMatchIdAndOrder($matchId,$match_data['match']['id'],$match_data['game']['id'],$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $first_event_Info = self::getFirstEventInfo('current_api',$matchId,$battleId,$match_data,'api');
        if (empty($first_event_Info)){
            return null;
        }
        $is_first_event = $first_event_Info['is_first_event'];
        $first_event_type = $first_event_Info['first_event_type'];
        $battle_detail_data = [
            'is_confirmed' => 1
        ];
        $eventConfig = [
            "event_type" => function ($params) {
                if(isset($params['payload']['type']) && $params['payload']['type']){
                    return $params['payload']['type'];
                }
            },//事件类型
            "ingame_timestamp" => "ingame_timestamp",//游戏内时间戳
            "killer" => function ($params) {
                if(isset($params['payload']['killer']['object']['id']) && $params['payload']['killer']['object']['id']){
                    return $params['payload']['killer']['object']['id'];
                }
            },//killer
            "killer_player_rel_name" => function ($params) {
                if(isset($params['payload']['killer']['object']['name']) && $params['payload']['killer']['object']['name']){
                    return $params['payload']['killer']['object']['name'];
                }
            },//killer
            "killer_champion_id" => function ($params) {
                if(isset($params['payload']['killer']['object']['champion']['id']) && $params['payload']['killer']['object']['champion']['id']){
                    return $params['payload']['killer']['object']['champion']['id'];
                }
            },//killer_champion_id
            "killer_faction" => function ($params) {
                if(isset($params['payload']['killer']['object']['team']) && $params['payload']['killer']['object']['team']){
                    return $params['payload']['killer']['object']['team'];
                }
            },//killer_faction
            "killer_object_name" => function($params) {
                if(isset($params['payload']['killer']['object']['name']) && $params['payload']['killer']['object']['name']){
                    return $params['payload']['killer']['object']['name'];
                }
            },
            "killer_ingame_obj_type" => function ($params) {
                if(isset($params['payload']['killer']['type']) && $params['payload']['killer']['type']){
                    return $params['payload']['killer']['type'];
                }
            },
            "killer_sub_type" =>function($params) {
                if(isset($params['payload']['killer']['object']['type']) && $params['payload']['killer']['type']){
                    return $params['payload']['killer']['object']['type'];
                }
            },
            "victim_name" => function ($params) {
                if (isset($params['payload']['killed']['object']['name']) && $params['payload']['killed']['object']['name']){
                    return $params['payload']['killed']['object']['name'];
                }
            },
            "victim_player_id" => function($params){
                if(isset($params['payload']['killed']['object']['id']) && $params['payload']['killed']['object']['id']){
                    return $params['payload']['killed']['object']['id'];
                }
            },
            "victim_player_rel_name" => function($params){
                if(isset($params['payload']['killed']['object']['name']) && $params['payload']['killed']['object']['name']){
                    return $params['payload']['killed']['object']['name'];
                }
            },
            "victim_champion_id" => function($params){
                if(isset($params['payload']['killed']['object']['champion']['id']) && $params['payload']['killed']['object']['champion']['id']){
                    return $params['payload']['killed']['object']['champion']['id'];
                }
            },
            "victim_faction" => function($params){
                if(isset($params['payload']['killed']['object']['team']) && $params['payload']['killed']['object']['team']){
                    return $params['payload']['killed']['object']['team'];
                }
            },
            "victim_ingame_obj_type" => function ($params) {
                if(isset($params['payload']['killed']['type']) && $params['payload']['killed']['type']){
                    return $params['payload']['killed']['type'];
                }
            },//受害者游戏内对象类型
            "rel_victim_ingame_obj_type" => function ($params) {
                if(isset($params['payload']['killed']['type']) && $params['payload']['killed']['type']){
                    return $params['payload']['killed']['type'];
                }
            },//受害者游戏内对象类型
            "victim_sub_type" =>function($params) {
                if(isset($params['payload']['killed']['object']['type']) && $params['payload']['killed']['object']['type']){
                    return $params['payload']['killed']['object']['type'];
                }
            },  //受害者子类型sub_type
            "assist" => null,
            "assist_ingame_obj_type" => null,
            "assist_sub_type" => null,
        ];
        $eventInfo = Mapping::transformation($eventConfig, $match_data);
        $eventInfo['order'] = self::getEventOrder($battleId);
        $event = $eventInfo;
        $elites_status_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_api:battles:'.$battleId.':';
        $event["battle_id"] = $battleId;
        $event["game"] = 2;
        $event['event_type'] = Common::geGoalIdLOLEventTypeByEvent(Consts::ORIGIN_PANDASCORE,$event['event_type'].'_kill');
        //击杀者
        if($event['killer_ingame_obj_type'] == "player"){  //击杀者类型是player
            $event['killer'] = self::getMainIdByRelIdentityId('player',$event['killer'],'3');
            $player_detail_killer = Player::find()->select('nick_name')->where(['id'=>$event['killer']])->asArray()->one();
            $event['killer_player_name'] = @$player_detail_killer['nick_name'];
            $event['killer_champion_id'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $event['killer_champion_id'], '3');
            $killerType = 50;//self::getGoalIdByPandascoreName($event['killer_ingame_obj_type']);
        }else{
            $killerType= self::getGoalIdByPandascoreName($event['killer_object_name']);
            $event['killer'] = $killerType;
        }
        $event['killer_ingame_obj_type'] = $killerType;
        $event['killer_sub_type'] = $killerType;
        //受害者
        if($event['victim_ingame_obj_type'] == "player"){
            $event['victim'] = self::getMainIdByRelIdentityId('player',$event['victim_player_id'],'3');
            $player_detail_victim = Player::find()->select('nick_name')->where(['id'=>$event['victim']])->asArray()->one();
            $event['victim_player_name'] = @$player_detail_victim['nick_name'];
            $event['victim_champion_id'] = self::getMainIdByRelIdentityIdOrUnknown('lol_champion', $event['victim_champion_id'], '3');
            $VictimType = 50;//self::getGoalIdByPandascoreName($event['victim_ingame_obj_type']);
        }else{
            if($event['victim_name']){  //被击杀的为野怪
                $VictimType = self::getGoalIdByPandascoreName($event['victim_name']);
                $event['victim'] = $VictimType;
            }else{
                $VictimType = 51;//unknown
                $event['victim'] = $VictimType;
            }
        }
        $event['victim_ingame_obj_type'] = $VictimType;
        $event['victim_sub_type'] = $VictimType;
        //记录死亡时间
        switch ($event['rel_victim_ingame_obj_type']){
            case 'drake':
                if ($event['victim_sub_type'] == 'elder'){
                    if ($killerType == 'player' && $event['killer']){
                        self::setPlayerKillsCounters($redis,$matchId,$battleId,'dragon_kills',$event['killer']);
                        $redis->hSet($elites_status_redis_name.'dragon_status','elder_dragon_kill_time',$ingame_timestamp);
                    }
                }else{
                    if ($killerType == 50 && $event['killer']){
                        self::setPlayerKillsCounters($redis,$matchId,$battleId,'dragon_kills',$event['killer']);
                        $redis->hSet($elites_status_redis_name.'dragon_status','kill_time',$ingame_timestamp);
                        $killer_team_faction = $event['killer_faction'];
                        $killer_team_num_redis = $redis->hGet($elites_status_redis_name.'team_dragon_status',$killer_team_faction.'_kill_num');
                        $killer_team_num = $killer_team_num_redis ? ($killer_team_num_redis + 1) : 1;
                        $redis->hSet($elites_status_redis_name.'team_dragon_status',$killer_team_faction.'_kill_num',$killer_team_num);
                    }
                }
                break;
            case 'rift_herald':
                if ($killerType == 50 && $event['killer']){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'rift_herald_kills',$event['killer']);
                    $redis->hSet($elites_status_redis_name.'rift_herald','kill_time',$ingame_timestamp);
                    $rift_herald_kill_num = $redis->hGet($elites_status_redis_name.'rift_herald','kill_num');
                    if ($rift_herald_kill_num){
                        $redis->hSet($elites_status_redis_name.'rift_herald','kill_num',2);
                        $redis->hSet($elites_status_redis_name.'rift_herald','exited',1);
                    }else{
                        $redis->hSet($elites_status_redis_name.'rift_herald','kill_num',1);
                    }
                }
                break;
            case 'elder_dragon':
                if ($killerType == 50 && $event['killer']){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'dragon_kills',$event['killer']);
                    $redis->hSet($elites_status_redis_name.'dragon_status','elder_dragon_kill_time',$ingame_timestamp);
                }
                break;
            case 'baron_nashor':
                if ($killerType == 50 && $event['killer']){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'baron_nashor_kills',$event['killer']);
                    $redis->hSet($elites_status_redis_name.'baron_nashor','kill_time',$ingame_timestamp);
                }
                break;
            case 'tower':
                if ($killerType == 50 && $event['killer']){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'turret_kills',$event['killer']);
                }
                break;
            case 'inhibitor':
                if ($killerType == 50 && $event['killer']){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'inhibitor_kills',$event['killer']);
                }
                break;
        }
        //助攻
        $event['assist'] = null;
        $event['assist_ingame_obj_type'] = null;
        $event['assist_sub_type'] = null;
        //$event["is_confirmed"] = 1;
        //计算第一次事件
        if ($is_first_event){
            $killer_team_faction = @$match_data['payload']['killer']['object']['team'];
            if (empty($killer_team_faction)){
                $killed_team_faction = @$match_data['payload']['killed']['object']['team'];
                if ($killed_team_faction == 'blue'){
                    $killer_team_faction = 'red';
                }else{
                    $killer_team_faction = 'blue';
                }
            }
            $killer_team = self::getTeamOrderByTeamFaction($battleId,$killer_team_faction);
            $is_first_event_val = 1;
            switch ($first_event_type){
                case 'first_blood':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim_champion = self::getChampionInfoByRedis($redis,$event['victim_champion_id']);
                    $victim = [
                        'player_id' => (Int)$event['victim'],
                        'nick_name' => $event['victim_player_name'],
                        'faction' => $event['victim_faction'],
                        'champion' => $victim_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $first_blood_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_blood_p_tid'=>$killer_team,
                        'first_blood_time' =>(String)$ingame_timestamp,
                        'first_blood_detail' =>@json_encode($first_blood_detail,320)
                    ];
                    break;
                case 'first_to_5_kills':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim_champion = self::getChampionInfoByRedis($redis,$event['victim_champion_id']);
                    $victim = [
                        'player_id' => (Int)$event['victim'],
                        'nick_name' => $event['victim_player_name'],
                        'faction' => $event['victim_faction'],
                        'champion' => $victim_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $first_to_5_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_to_5_kills_p_tid'=>$killer_team,
                        'first_to_5_kills_time' =>(String)$ingame_timestamp,
                        'first_to_5_detail' => @json_encode($first_to_5_detail,320)
                    ];
                    break;
                case 'first_to_10_kills':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' =>(Int) $event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim_champion = self::getChampionInfoByRedis($redis,$event['victim_champion_id']);
                    $victim = [
                        'player_id' => (Int)$event['victim'],
                        'nick_name' => $event['victim_player_name'],
                        'faction' => $event['victim_faction'],
                        'champion' => $victim_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $first_to_10_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_to_10_kills_p_tid'=>$killer_team,
                        'first_to_10_kills_time' =>(String)$ingame_timestamp,
                        'first_to_10_detail' => @json_encode($first_to_10_detail,320)
                    ];
                    break;
                case 'first_rift_herald':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim = [
                        'ingame_obj_name' => 'Rift Herald',
                        'ingame_obj_name_cn' => '峡谷先锋',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'rift_herald'
                    ];
                    $first_rift_herald_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_rift_herald_p_tid'=>$killer_team,
                        'first_rift_herald_time' =>(String)$ingame_timestamp,
                        'first_rift_herald_detail' => @json_encode($first_rift_herald_detail,320)
                    ];
                    break;
                case 'first_dragon':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim = null;
                    //判断是哪个龙
                    if ($event['victim'] == 2){
                        $victim = [
                            'ingame_obj_name' => 'Infernal Drake',
                            'ingame_obj_name_cn' => '炼狱亚龙',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'drake'
                        ];
                    }
                    if ($event['victim'] == 3){
                        $victim = [
                            'ingame_obj_name' => 'Mountain Drake',
                            'ingame_obj_name_cn' => '山脉亚龙',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'drake'
                        ];
                    }
                    if ($event['victim'] == 4){
                        $victim = [
                            'ingame_obj_name' => 'Ocean Drake',
                            'ingame_obj_name_cn' => '海洋亚龙',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'drake'
                        ];
                    }
                    if ($event['victim'] == 5){
                        $victim = [
                            'ingame_obj_name' => 'Cloud Drake',
                            'ingame_obj_name_cn' => '云端亚龙',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'drake'
                        ];
                    }
                    $first_dragon_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_dragon_p_tid'=>$killer_team,
                        'first_dragon_time' =>(String)$ingame_timestamp,
                        'first_dragon_detail' =>@json_encode($first_dragon_detail,320)
                    ];
                    break;
                case 'first_baron_nashor':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim = [
                        'ingame_obj_name' => 'Baron Nashor',
                        'ingame_obj_name_cn' => '纳什男爵',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'baron_nashor'
                    ];
                    $first_baron_nashor_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_baron_nashor_p_tid'=>$killer_team,
                        'first_baron_nashor_time' =>(String)$ingame_timestamp,
                        'first_baron_nashor_detail' => @json_encode($first_baron_nashor_detail,320)
                    ];
                    break;
                case 'first_elder_dragon':
                    $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                    $killer = [
                        'player_id' => (Int)$event['killer'],
                        'nick_name' => $event['killer_player_name'],
                        'faction' => $event['killer_faction'],
                        'champion' => $killer_champion,
                        'ingame_obj_type' => 'player',
                        'ingame_obj_sub_type' => 'player'
                    ];
                    $victim = [
                        'ingame_obj_name' => 'Elder Dragon',
                        'ingame_obj_name_cn' => '远古巨龙',
                        'ingame_obj_type' => 'elite',
                        'ingame_obj_sub_type' => 'elder_dragon'
                    ];
                    $first_elder_dragon_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_elder_dragon_p_tid'=>$killer_team,
                        'first_elder_dragon_time' =>(String)$ingame_timestamp,
                        'first_elder_dragon_detail' => @json_encode($first_elder_dragon_detail,320)
                    ];
                    break;
                case 'first_turret':
                    $killer = [];
                    if ($killerType == 50){
                        $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                        $killer = [
                            'player_id' => (Int)$event['killer'],
                            'nick_name' => $event['killer_player_name'],
                            'faction' => $event['killer_faction'],
                            'champion' => $killer_champion,
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                    }
                    if($killerType == 1){
                        $killer = [
                            'ingame_obj_name' => 'Rift Herald',
                            'ingame_obj_name_cn' => '峡谷先锋',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'rift_herald'
                        ];
                    }
                    if($killerType == 8){//Minion
                        $killer = [
                            'ingame_obj_name' => 'Minion',
                            'ingame_obj_name_cn' => '小兵',
                            'faction' => $event['killer_faction'],
                            'ingame_obj_type' => 'minion',
                            'ingame_obj_sub_type' => 'minion'
                        ];
                    }
                    if($killerType == 51){//Minion
                        $killer = [
                            'ingame_obj_name' => 'unknown',
                            'ingame_obj_name_cn' => '未知',
                            'ingame_obj_type' => 'unknown',
                            'ingame_obj_sub_type' => 'unknown'
                        ];
                    }
                    $victim = [
                        'lane' => null,
                        'ingame_obj_name' => 'Turret',
                        'ingame_obj_name_cn' => '防御塔',
                        'faction' => $event['victim_faction'],
                        'ingame_obj_type' => 'building',
                        'ingame_obj_sub_type' => 'turret'
                    ];
                    $first_turret_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_turret_p_tid'=>$killer_team,
                        'first_turret_time' =>(String)$ingame_timestamp,
                        'first_turret_detail' => @json_encode($first_turret_detail,320)
                    ];
                    break;
                case 'first_inhibitor':
                    $killer = [];
                    if ($killerType == 50){
                        $killer_champion = self::getChampionInfoByRedis($redis,$event['killer_champion_id']);
                        $killer = [
                            'player_id' => (Int)$event['killer'],
                            'nick_name' => $event['killer_player_name'],
                            'faction' => $event['killer_faction'],
                            'champion' => $killer_champion,
                            'ingame_obj_type' => 'player',
                            'ingame_obj_sub_type' => 'player'
                        ];
                    }
                    if($killerType == 1){
                        $killer = [
                            'ingame_obj_name' => 'Rift Herald',
                            'ingame_obj_name_cn' => '峡谷先锋',
                            'ingame_obj_type' => 'elite',
                            'ingame_obj_sub_type' => 'rift_herald'
                        ];
                    }
                    if($killerType == 8){//Minion
                        $killer = [
                            'ingame_obj_name' => 'Minion',
                            'ingame_obj_name_cn' => '小兵',
                            'faction' => $event['killer_faction'],
                            'ingame_obj_type' => 'minion',
                            'ingame_obj_sub_type' => 'minion'
                        ];
                    }
                    if($killerType == 51){//Minion
                        $killer = [
                            'ingame_obj_name' => 'unknown',
                            'ingame_obj_name_cn' => '未知',
                            'ingame_obj_type' => 'unknown',
                            'ingame_obj_sub_type' => 'unknown'
                        ];
                    }
                    $victim = [
                        'lane' => null,
                        'ingame_obj_name' => 'Inhibitor',
                        'ingame_obj_name_cn' => '召唤水晶',
                        'faction' => $event['victim_faction'],
                        'ingame_obj_type' => 'building',
                        'ingame_obj_sub_type' => 'inhibitor'
                    ];
                    $first_inhibitor_detail = [
                        'position' => null,
                        'killer' => $killer,
                        'victim' => $victim,
                        'assist' => []
                    ];
                    $battle_detail_data = [
                        'first_inhibitor_p_tid'=>$killer_team,
                        'first_inhibitor_time' =>(String)$ingame_timestamp,
                        'first_inhibitor_detail' => @json_encode($first_inhibitor_detail,320)
                    ];
                    break;
            }
        }
        if ($battle_detail_data){
            self::setBattleDetails($battleId,$battle_detail_data);
        }
        $event["is_first_event"] = $is_first_event_val;
        $event["first_event_type"] = empty($first_event_type) ? 0 : Common::getPandaScoreEventNum($first_event_type);
        $event['event_id'] = $socket_id;
        $event['match'] = $matchId;
        $eventData = [$event];
        BattleService::setBattleInfo($battleId,'lol',$eventData,BattleService::DATA_TYPE_EVENTS);
        $match_data_info = [
            'battleId' => $battleId,
        ];
        return $match_data_info;
    }

    private static function setMatch($id, $atts)
    {
        MatchService::setRealTimeInfo($id, $atts, 'detail_by_scoket_frames');
    }

    public static function refreshInfo($matchId, $info)
    {
        self::setMatch($matchId, $info['match']);
        // 根据battleId刷新对应的battle信息
        self::refreshBattle($matchId, $info['battle']);
    }

    private static function refreshBattle($matchId, $battle)
    {
        // 找到对应的battleId，更新battle
        $battleBase=MatchBattle::find()->where([
            'match'=>$matchId,
            'order'=>$battle['base']['order'],
        ])->one();
        if(!$battleBase){
            $battleBase=new MatchBattle();
            $battleBase->setAttributes([
                'match'=>$matchId,
                'order'=>$battle['base']['order'],
            ]);
            $battleBase->save();
        }
        $battleId=$battleBase['id'];
        //查看原始的battle状态是否已经结束
        if ($battleBase->status ==3){
            return;
        }
        //查看当前数据来的 battle状态 是不是结束  不是结束才判断时间
        if($battle['base']['status'] != 3){
            if ($battle['base']['duration'] < $battleBase->duration){
                return;
            }
        }
        //更新数据battle
        BattleService::setBattleInfo($battleId,'lol',$battle['base'],BattleService::DATA_TYPE_BASE);
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'lol',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        BattleService::setBattleInfo($battleId,'lol',$battle['static'],BattleService::DATA_TYPE_STATICS);
        BattleService::setBattleInfo($battleId,'lol',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'lol',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        if(isset($battle['events']) && count($battle['events']) > 0){
            BattleService::setBattleInfo($battleId,'lol',$battle['events'],BattleService::DATA_TYPE_EVENTS);
        }
    }
    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal['id'];
            }
        }
    }

    public static function getGoalIdBySubType($subType)
    {
        $goal = EnumIngameGoal::find()->where(['=','sub_type',$subType])
            ->asArray()->one();
        if($goal){
            return $goal['id'];
        }
    }
    //设置玩家 KillsCounters
    public static function setPlayerKillsCounters($redis,$matchId,$battleId,$kills_type,$killer_player_id){
        $ws_player_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_api:battles:'.$battleId.':players_kills_counters';
        $playerKillsCountersRedis = $redis->hGet($ws_player_redis_name,$killer_player_id);
        if ($playerKillsCountersRedis){
            $playerKillsCountersArray = @json_decode($playerKillsCountersRedis,true);
            $playerKillsCountersArray[$kills_type] = (Int)$playerKillsCountersArray[$kills_type] + 1;
        }else{
            $playerKillsCountersArray = [
                'turret_kills' => 0,
                'inhibitor_kills' => 0,
                'rift_herald_kills' => 0,
                'dragon_kills' => 0,
                'baron_nashor_kills' => 0,
            ];
            $playerKillsCountersArray[$kills_type] = 1;
        }
        $redis->hSet($ws_player_redis_name,$killer_player_id,@json_encode($playerKillsCountersArray,320));
        return true;
    }
    //获取英雄信息
    private static function getChampionInfoByRedis($redis,$champions_id){
        $champion = null;
        $championInfoRedis = $redis->hGet('lol:champions:list:3',$champions_id);
        if ($championInfoRedis){
            $championInfo = @json_decode($championInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
                $small_image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $redis->get("lol:champions:list:unknown");
            $championInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
                $small_image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }
        return $champion;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}