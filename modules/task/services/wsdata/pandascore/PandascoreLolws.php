<?php

namespace app\modules\task\services\wsdata\pandascore;
use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBase;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\Team;
use app\modules\task\models\MatchSocketData;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;

class PandascoreLolws extends HotBase
{
    private static $instance;
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $PandascoreLolws = self::getInstance();
        $redis = $PandascoreLolws->redis;
        $conversionRes = null;
        $gameId = $tagInfo['ext_type'];
        $info = json_decode($taskInfo['params'], true);
        $originId = $info['origin_id'];
        $rel_match_id = $info['match_id'];
        $socket_id = (Int)$info['socket_id'];
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        $allEvents = self::getEventsByRelMatchId($originId,$rel_match_id);
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        //获取比赛信息
        $matchInfo = self::getMatchAllInfoByMatchId($matchId);
        $type = $info['type'];
        switch ($type)   //websocket类型
        {
            case "no":
                $conversionRes = self::transNoWs($redis,$info,$matchInfo,$allEvents,$socket_time,$socket_id);
                $type = 'frames';
            break;
            case "events":
                $conversionRes = self::transEventsWs($redis,$info,$matchId,$allEvents,$socket_time,$socket_id);
                $type = 'events';
            break;
        }
        if (empty($conversionRes)){
            return ;
        }
        //判断是否开始
        if ($conversionRes['is_battle_start']){
            $battle_start_trans_data = [
                'match_id' => $conversionRes['trans_data']['match_id'],
                'battle_id' => $conversionRes['trans_data']['battle_id'],
                'battle_order' => $conversionRes['trans_data']['battle_order'],
                'ingame_timestamp' => 0,
                'event_id' => (Int)$socket_id,
                'event_type' => 'battle_start',
                'map' => $conversionRes['trans_data']['map'],
                'blue' => $conversionRes['trans_data']['factions'][0]['team'],
                'red' => $conversionRes['trans_data']['factions'][1]['team']
            ];
            $battle_start_Data = [
                'match_data' =>@json_encode($battle_start_trans_data,320),
                'match_id' => $matchId,
                'game_id' => $gameId,
                'origin_id' => $originId,
                'type' => 'events',
                'battle_order' => $conversionRes['battle_order']
            ];
//            self::insertEventsFromPs($battle_start_Data);
            self::saveKafka($battle_start_Data);
        }
        //判断是否结束
        if ($conversionRes['is_battle_end']){
            $battle_end_trans_data = [
                'match_id' => $conversionRes['trans_data']['match_id'],
                'battle_id' => $conversionRes['trans_data']['battle_id'],
                'battle_order' => $conversionRes['trans_data']['battle_order'],
                'ingame_timestamp' => $conversionRes['trans_data']['duration'],
                'event_id' => (Int)$socket_id,
                'event_type' => 'battle_end',
                'winner' => $conversionRes['trans_data']['battle_winner']
            ];
            $battle_end_Data = [
                'match_data' =>@json_encode($battle_end_trans_data,320),
                'match_id' => $matchId,
                'game_id' => $gameId,
                'origin_id' => $originId,
                'type' => 'events',
                'battle_order' => $conversionRes['battle_order']
            ];
//            self::insertEventsFromPs($battle_end_Data);
            self::saveKafka($battle_end_Data);
        }
        //接下来存数据库
        $insertData = [
            'match_data' => @json_encode($conversionRes['trans_data'],320),
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => $type,
            'battle_order' => $conversionRes['battle_order']
        ];
//        //当前数据库连接
//        $db_websoket = DbSocket::getDb()->createCommand();
//        //单条插入
//        $db_websoket->insert('match_data', $insertData);
//        $db_websoket->execute();
        self::saveKafka($insertData);
        return true;
//        //插入到自己的库
//        $matchSocketData = new MatchSocketData();
//        $matchSocketData->setAttributes([
//            'game_id' => $gameId,
//            'origin_id' => $originId,
//            'match_id' => $matchId,
//            'battle_order' => $conversionRes['battle_order'],
//            'match_data' =>$conversionRes['trans_data'],
//            'type' => $type
//        ]);
//        return $matchSocketData->save();
    }
    //保存到Kafka
    public static function saveKafka($data){
        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_LOL'),$data);
    }
    public static function transNoWs($redis,$info,$matchInfo,$allEvents,$socket_time,$socket_id)
    {
        $is_battle_end = false;
        $matchId = $matchInfo['id'];
        //根据比赛规则的num获取字符串的值
        $game_rules_num = $matchInfo['game_rules'];
        $game_rules = Common::getGameRulesStringByNum('common',$game_rules_num);
        //根据比赛类型的num获取字符串的值
        $match_type_num = $matchInfo['match_type'];
        $match_type = Common::getMatchTypeStringByNum('common',$match_type_num);
        $number_of_games = (Int)$matchInfo['number_of_games'];
        //服务器设置游戏版本
        $MatchBaseInfo = MatchBase::find()->select('game_version')->where(['match_id'=>$matchId])->asArray()->one();
        $game_version = @$MatchBaseInfo['game_version'];
        $server_config = [
            'version' => $game_version
        ];
        $blue = $red = [];
        $timeStart = date('Y-m-d H:i:s');
        $match_data = $info['match_data'];
        if (isset($match_data['type'])){
//            $frames_data = [
//                'battle_order' => null,
//                'trans_data' => 'hello'
//            ];
//            return $frames_data;
            return '';
        }
        //获取比赛的信息
        //$matchInfo = self::getMatchInfoByMatchId($matchId);
        //定义building_status
        $turrets = [
            'top_outer_turret'=> null,
            'top_inner_turret'=> null,
            'top_inhibitor_turret'=> null,
            'mid_outer_turret'=> null,
            'mid_inner_turret'=> null,
            'mid_inhibitor_turret'=> null,
            'bot_outer_turret'=> null,
            'bot_inner_turret'=> null,
            'bot_inhibitor_turret'=> null,
            'top_nexus_turret'=> null,
            'bot_nexus_turret'=> null
        ];
        $inhibitors = [
            'top_inhibitor'=> null,
            'mid_inhibitor'=> null,
            'bot_inhibitor'=> null
        ];
        $building_status = [
            'turrets' => $turrets,
            'inhibitors' => $inhibitors,
            'nexus' => null
        ];
        $blue_building_status = $red_building_status = $building_status;
        //找到对应的平台的match_id
        $battle_Info = self::getbattleIdByMatchIdAndOrderByWs($matchId,$match_data['match']['id'],$match_data['game']['id'],$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $is_battle_start = $battle_Info['is_battle_start'];
        //默认地图类型
        $mapModelResult = MetadataLolMap::find()
            ->select(['id as map_id','name','name_cn','external_id','external_name','short_name','map_type','map_type_cn','is_default','slug','image as map_image'])
            ->where(['is_default' => 1])->asArray()->one();
        if($mapModelResult){
            $map = [];
            $map['map_id'] = self::checkAndChangeInt($mapModelResult['map_id']);
            $map['name'] = self::checkAndChangeString($mapModelResult['name']);
            $map['name_cn'] = self::checkAndChangeString($mapModelResult['name_cn']);
            $map['external_id'] = self::checkAndChangeString($mapModelResult['external_id']);
            $map['external_name'] = self::checkAndChangeString($mapModelResult['external_name']);
            $map['short_name'] = self::checkAndChangeString($mapModelResult['short_name']);
            $map['map_type'] = self::checkAndChangeString($mapModelResult['map_type']);
            $map['map_type_cn'] = self::checkAndChangeString($mapModelResult['map_type_cn']);
            $map['is_default'] = $mapModelResult['is_default'] == 1 ? true : false;
            $map['slug'] = self::checkAndChangeString($mapModelResult['slug']);
            $map['image']['square_image'] = null;
            $map['image']['rectangle_image'] = null;
            $map['image']['thumbnail'] = self::checkAndChangeString($mapModelResult['map_image']);
        }else{
            $map = null;
        }
        $team_rel_master = [];
        //scores
        $match_scores = [];
//        $blue[] = $info['match_data']['blue'];
        $blue['faction'] ='blue';
        //战队
        $blue_team_rel_id = $match_data['blue']['id'];
        $blue_team_image_res = null;
        $blue_teamId = self::getMainIdByRelIdentityId('team',$blue_team_rel_id,'3');
        $blue_opponent_order = self::getTeamOrderByTeamId($blue_teamId,$matchInfo);
        $blue_team = Team::find()->select('id,name,image')->where(['id' => $blue_teamId])->asArray()->one();
        if($blue_team){
            $team_rel_master[$blue_team_rel_id] = $blue_teamId;
            $blue['team']['team_id'] = (Int)$blue_team['id'];
            $blue['team']['name'] = $blue_team['name'];
            if (!empty($blue_team['image'])){
                $blue_team_image_res = ImageConversionHelper::showFixedSizeConversion($blue_team['image'],200,200);
            }
            $blue['team']['image'] = $blue_team_image_res;
            $blue['team']['opponent_order'] = $blue_opponent_order;
        }else{
            $blue['team'] = null;
        }
        //处理玩家
        $bluePlayersInfo = self::conversionPlayers($redis,$matchId,$battleId,$match_data['blue']['players'],$match_data,'blue');
        $blue['kills'] = (Int)$match_data['blue']['kills'];
        $blue['deaths'] = (Int)$bluePlayersInfo['deaths'];
        $blue['assists'] = (Int)$bluePlayersInfo['assists'];
        $blue['gold'] = (Int)$match_data['blue']['gold'];
        $blue['gold_diff'] = $match_data['blue']['gold'] - $match_data['red']['gold'];
        $blue['experience'] = null;
        $blue['experience_diff'] = null;
        $blue['turret_kills'] = (Int)$match_data['blue']['towers'];
        $blue['inhibitor_kills'] = (Int)$match_data['blue']['inhibitors'];
        $blue['rift_herald_kills'] = (Int)$match_data['blue']['herald'];
        $blue['dragon_kills'] = (Int)$match_data['blue']['drakes'];
        $blue['dragon_kills_detail'] = self::conversionDragonKills($allEvents,'blue',$match_data['match']['id'],$match_data['game']['id']);
        $blue['baron_nashor_kills'] = (Int)$match_data['blue']['nashors'];
        //获取$building_status
        $blue['building_status'] = $blue_building_status;
        //players转换
        $blue['players'] = $bluePlayersInfo['players'];
        $blue['advanced']['wards_purchased'] = null;
        $blue['advanced']['wards_placed'] = null;
        $blue['advanced']['wards_kills'] = null;
        //设置蓝队比分
        $match_scores[$blue_opponent_order]['team_id'] = (Int)$blue['team']['team_id'];
        $match_scores[$blue_opponent_order]['name'] = $blue['team']['name'];
        $match_scores[$blue_opponent_order]['image'] = $blue_team_image_res;
        $match_scores[$blue_opponent_order]['opponent_order'] = (Int)$blue_opponent_order;
        $match_scores[$blue_opponent_order]['score'] = (Int)$match_data['blue']['score'];
        //红队转换
        $red_team_image_res = null;
        $red_team_rel_id = $match_data['red']['id'];
        $red['faction'] ='red';
        $red_teamId = self::getMainIdByRelIdentityId('team',$red_team_rel_id,'3');
        $red_opponent_order = self::getTeamOrderByTeamId($red_teamId,$matchInfo);
        $red_team = Team::find()->select('id,name,image')->where(['id' => $red_teamId])->asArray()->one();
        if($red_team){
            $team_rel_master[$red_team_rel_id] = $red_teamId;
            $red['team']['team_id'] = (Int)$red_team['id'];
            $red['team']['name'] = $red_team['name'];
            if (!empty($red_team['image'])){
                $red_team_image_res = ImageConversionHelper::showFixedSizeConversion($red_team['image'],200,200);
            }
            $red['team']['image'] = $red_team_image_res;
            $red['team']['opponent_order'] = $red_opponent_order;
        }else{
            $red['team'] = null;
        }
        //处理玩家
        $redPlayersInfo = self::conversionPlayers($redis,$matchId,$battleId,$match_data['red']['players'],$match_data,'red');
        $red['kills'] = (Int)$match_data['red']['kills'];
        $red['deaths'] = (Int)$redPlayersInfo['deaths'];
        $red['assists'] = (Int)$redPlayersInfo['assists'];
        $red['gold'] = (Int)$match_data['red']['gold'];
        $red['gold_diff'] = $match_data['red']['gold'] - $match_data['blue']['gold'];
        $red['experience'] = null;
        $red['experience_diff'] = null;
        $red['turret_kills'] = (Int)$match_data['red']['towers'];
        $red['inhibitor_kills'] = (Int)$match_data['red']['inhibitors'];
        $red['rift_herald_kills'] = (Int)$match_data['red']['herald'];
        $red['dragon_kills'] = (Int)$match_data['red']['drakes'];
        $red['dragon_kills_detail'] = self::conversionDragonKills($allEvents,'red',$match_data['match']['id'],$match_data['game']['id']);
        $red['baron_nashor_kills'] = (Int)$match_data['red']['nashors'];
        $red['building_status'] = $red_building_status;
        //players转换
        $red['players'] = $redPlayersInfo['players'];
        $red['advanced']['wards_purchased'] = null;
        $red['advanced']['wards_placed'] = null;
        $red['advanced']['wards_kills'] = null;
        //设置红队比分
        $match_scores[$red_opponent_order]['team_id'] = (Int)$red['team']['team_id'];
        $match_scores[$red_opponent_order]['name'] = $red['team']['name'];
        $match_scores[$red_opponent_order]['image'] = $red_team_image_res;
        $match_scores[$red_opponent_order]['opponent_order'] = (Int)$red_opponent_order;
        $match_scores[$red_opponent_order]['score'] = (Int)$match_data['red']['score'];
        //battle是否已结束
        $is_battle_finished = $is_match_finished = false;
        $is_finished = self::checkAndChangeBoolean($match_data['game']['finished']);
        if($is_finished){
            $is_battle_end = true;
            $is_battle_finished = true;
            $battle_winner=[
                'team_id'=>null,
                'name'=>null,
                'image'=>null,
                'opponent_order'=>null
            ];
            $game_rel_winner_id = $match_data['game']['winner_id'];
            $winnerId =$team_rel_master[$game_rel_winner_id];
            if ($winnerId == $blue_teamId){
                $battle_winner = $blue['team'];
            }
            if ($winnerId == $red_teamId){
                $battle_winner = $red['team'];
            }
//            $winnerId = self::getMainIdByRelIdentityId('team',$match_data['game']['winner_id'],'3');
//            $winnerInfo = Team::find()->select('id,name,image')->where(['id' => $winnerId])->asArray()->one();
//            $battle_winner['team_id'] = (Int)$winnerInfo['id'];
//            $battle_winner['name'] = $winnerInfo['name'];
//            $battle_winner['image'] = $winnerInfo['image'];
//            $battle_winner['opponent_order'] = self::getTeamOrderByTeamId($winnerId,$matchInfo);
            //计算比赛是否结束
            switch ($battle_winner['opponent_order']){
                case 1:
                    $match_scores[1]['score'] = $match_scores[1]['score'] + 1;
                    break;
                case 2:
                    $match_scores[2]['score'] = $match_scores[2]['score'] + 1;
                    break;
            }
            //获取match winner
            $statusInfo = WinnerHelper::lolWinnerInfo($match_scores[1]['score'],$match_scores[2]['score'],$matchInfo['number_of_games']);
            if($statusInfo['is_finish']==1){
                $is_match_finished = true;
                $match_winner=[
                    'team_id'=>null,
                    'name'=>null,
                    'image'=>null,
                    'opponent_order'=>null
                ];
                $match_winner_id = '';
                switch ($statusInfo['winner_team']){
                    case 1:
                        $match_winner_id = $match_scores[1]['team_id'];
                        break;
                    case 2:
                        $match_winner_id = $match_scores[2]['team_id'];
                        break;
                }
                if ($match_winner_id == $blue_teamId){
                    $match_winner = $blue['team'];
                }
                if ($match_winner_id == $red_teamId){
                    $match_winner = $red['team'];
                }
//                $winnerInfo = Team::find()->select('id,name,image')->where(['id' => $match_winner_id])->asArray()->one();
//                $match_winner['team_id'] = (Int)$winnerInfo['id'];
//                $match_winner['name'] = $winnerInfo['name'];
//                $match_winner['image'] = $winnerInfo['image'];
//                $match_winner['opponent_order'] = self::getTeamOrderByTeamId($match_winner_id,$matchInfo);
            }else{
                $match_winner = null;
            }
        }else{
            $battle_winner = null;
            $match_winner = null;
        }
        //$elites_status
        $duration = $match_data['current_timestamp'];
        //计算 elites_status
        $elites_status_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_ws:battles:'.$battleId.':';
        $enumIngameGoalInfoList = $redis->hGetAll('enum:ingame_goal');
        //rift_herald
        $rift_herald_exited = $redis->hGet($elites_status_redis_name.'rift_herald','exited');
        if ($rift_herald_exited){
            $rift_herald_status_val = 'exited';
            $rift_herald_spawntimer = null;
        }else{
            $rift_herald_status_val = 'alive';
            $rift_herald_spawntimer = null;
            if ($duration < 480){
                $rift_herald_status_val = 'not_spawned';
                $rift_herald_spawntimer = 480 - $duration;
            }elseif ($duration > 1200){
                $rift_herald_status_val = 'exited';
                $rift_herald_spawntimer = null;
                $redis->hSet($elites_status_redis_name.'rift_herald','exited',1);
            }else{
                $rift_herald_kill_time = $redis->hGet($elites_status_redis_name.'rift_herald','kill_time');
                if ($rift_herald_kill_time){
                    $rift_herald_spawntimer_time = $duration - $rift_herald_kill_time;
                    if ($rift_herald_spawntimer_time < 360){
                        $rift_herald_status_val = 'not_spawned';
                        $rift_herald_spawntimer = 360 - $rift_herald_spawntimer_time;
                    }
                }
            }
        }
        $rift_herald_array = @json_decode($enumIngameGoalInfoList[1],true);
        $rift_herald_name_val = @$rift_herald_array['ingame_obj_name'];
        $rift_herald_name_cn_val = @$rift_herald_array['ingame_obj_name_cn'];
        $rift_herald_status = [
            'status' => $rift_herald_status_val,
            'name' => self::checkAndChangeString($rift_herald_name_val),
            'name_cn' => self::checkAndChangeString($rift_herald_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($rift_herald_spawntimer)
        ];
        //dragon_status
        $dragon_status_status_val = 'alive';
        $dragon_status_spawntimer = null;
        $dragon_status_name = null;
        $dragon_status_name_cn= null;
        $dragon_status_Elder_kill_time = $redis->hGet($elites_status_redis_name.'dragon_status','elder_dragon_kill_time');
        if ($dragon_status_Elder_kill_time) {
            $dragon_status_Elder_spawntimer_cha = $duration - $dragon_status_Elder_kill_time;
            if ($dragon_status_Elder_spawntimer_cha < 360){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = 360 - $dragon_status_Elder_spawntimer_cha;
            }
            $dragon_status_name = 'Elder Dragon';
            $dragon_status_name_cn = '远古巨龙';
        }else{
            if ($duration < 300){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = 300 - $duration;
            }else{
                $dragon_status_kill_time = $redis->hGet($elites_status_redis_name.'dragon_status','kill_time');
                if ($dragon_status_kill_time){
                    //首先判断两队是否累计杀了4个了
                    $team_dragon_status = $redis->hGetAll($elites_status_redis_name.'team_dragon_status');
                    $blue_kill_num = $team_dragon_status['blue_kill_num'];
                    $red_kill_num = $team_dragon_status['red_kill_num'];
                    if ($blue_kill_num > 3 || $red_kill_num > 3){
                        $dragon_status_Elder_spawned_cha = $duration - $dragon_status_kill_time;
                        $dragon_status_spawntimer = 360 - $dragon_status_Elder_spawned_cha;
                        if ($dragon_status_spawntimer > 0){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_status_spawntimer;
                        }else{
                            $dragon_status_status_val = 'alive';
                            $dragon_status_spawntimer = null;
                        }
                        $dragon_status_name = 'Elder Dragon';
                        $dragon_status_name_cn = '远古巨龙';
                    }else{
                        $dragon_status_spawntimer_cha = $duration - $dragon_status_kill_time;
                        if ($dragon_status_spawntimer_cha < 300){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = 300 - $dragon_status_spawntimer_cha;
                        }
                    }
                }
            }
        }
        $dragon_status = [
            'status' => $dragon_status_status_val,
            'name' => $dragon_status_name,
            'name_cn' => $dragon_status_name_cn,
            'spawntimer' => self::checkAndChangeInt($dragon_status_spawntimer)
        ];
        //baron_nashor_status
        $baron_nashor_spawntimer = null;
        $baron_nashor_array = @json_decode($enumIngameGoalInfoList[7],true);
        $baron_nashor_name_val = @$baron_nashor_array['ingame_obj_name'];
        $baron_nashor_name_cn_val = @$baron_nashor_array['ingame_obj_name_cn'];
        $baron_nashor_kill_time = $redis->hGet($elites_status_redis_name.'baron_nashor','kill_time');
        if ($baron_nashor_kill_time){
            $baron_nashor_spawntimer_time = $duration - $baron_nashor_kill_time;
            if ($baron_nashor_spawntimer_time > 360){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = 360 - $baron_nashor_spawntimer_time;
            }
        }else{
            if ($duration > 1200){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = 1200 - $duration;
            }
        }
        $baron_nashor_status = [
            'status' => $baron_nashor_status_val,
            'name' => self::checkAndChangeString($baron_nashor_name_val),
            'name_cn' => self::checkAndChangeString($baron_nashor_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($baron_nashor_spawntimer)
        ];
        $elites_status = [
            'rift_herald_status' => $rift_herald_status,
            'dragon_status' => $dragon_status,
            'baron_nashor_status' => $baron_nashor_status
        ];
        //组合数据
        $factions[] = $blue;
        $factions[] = $red;
        $match_scores_res[] = @$match_scores[1];
        $match_scores_res[] = @$match_scores[2];
        $transData = [
            'match_id' => (Int)$matchId,
            'game_rules' => $game_rules,
            'match_type' => $match_type,
            'number_of_games' => $number_of_games,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'map' =>$map,
            'duration' => (Int)$duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores_res,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => self::checkAndChangeBoolean($match_data['paused']),
            'is_live' => true,
            'elites_status' => $elites_status,
            'factions' =>$factions,
            'server_config' =>$server_config
        ];
        //$trans_data = json_encode($transData,320);
        $frames_data = [
            'battle_order' => $battle_order,
            'trans_data' => $transData,
            'is_battle_start' => $is_battle_start,
            'is_battle_end' => $is_battle_end
        ];
        return $frames_data;
    }

    public static function transEventsWs($redis,$info,$matchId,$allEvents,$socket_time,$socket_id)
    {
        $match_data = $info['match_data'];
        if ($match_data['type'] =='hello'){
//            $frames_data = [
//                'battle_order' => null,
//                'trans_data' => 'hello'
//            ];
//            return $frames_data;
            return '';
        }
        $event_type = null;
        $position = null;
        //找到对应的平台的match_id
        $battle_Info = self::getbattleIdByMatchIdAndOrderByWs($matchId,$match_data['match']['id'],$match_data['game']['id'],$socket_time);
        if (empty($battle_Info)){
            return null;
        }
        $ingame_timestamp = (Int)$match_data['ingame_timestamp'];
        $battleId = $battle_Info['battle_id'];
        $battle_order = $battle_Info['battle_order'];
        $elites_status_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_ws:battles:'.$battleId.':';
        $event_type = Common::geGoalIdLOLEventTypeByEvent(Consts::ORIGIN_PANDASCORE,$match_data['payload']['type'].'_kill');
        //击杀者
        $killer_ingame_obj_info = $killer_ingame_obj_type = $killer_ingame_obj_sub_type = null;
        $killer_type = $match_data['payload']['killer']['type'];
        $killer_player_id = @$match_data['payload']['killer']['object']['id'];
        switch ($killer_type){
            case 'player':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'player');
                $killer_ingame_obj_type = 'player';
                $killer_ingame_obj_sub_type = 'player';
                break;
            case 'rift_herald':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'elite');
                $killer_ingame_obj_type = 'elite';
                $killer_ingame_obj_sub_type = 'rift_herald';
                break;
            case 'drake':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'elite');
                $killer_ingame_obj_type = 'elite';
                $killer_ingame_obj_sub_type = 'drake';
                break;
            case 'elder_dragon':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'elite');
                $killer_ingame_obj_type = 'elite';
                $killer_ingame_obj_sub_type = 'elder_dragon';
                break;
            case 'baron_nashor':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'baron_nashor');
                $killer_ingame_obj_type = 'elite';
                $killer_ingame_obj_sub_type = 'baron_nashor';
                break;
            case 'minion':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'minion');
                $killer_ingame_obj_type = 'minion';
                $killer_ingame_obj_sub_type = 'minion';
                break;
            case 'neutral_minion':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'minion');
                $killer_ingame_obj_type = 'minion';
                $killer_ingame_obj_sub_type = 'neutral_minion';
                break;
            case 'tower':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killer'],'building');
                $killer_ingame_obj_type = 'building';
                $killer_ingame_obj_sub_type = 'turret';
                break;
            case 'inhibitor':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'building');
                $killer_ingame_obj_type = 'building';
                $killer_ingame_obj_sub_type = 'inhibitor';
                break;
            case 'nexus':
                $killer_ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'building');
                $killer_ingame_obj_type = 'building';
                $killer_ingame_obj_sub_type = 'nexus';
                break;
            default: //
                $killer_ingame_obj_type = 'unknown';
                $killer_ingame_obj_sub_type = 'unknown';
                break;
        }
        $killer = array_merge($killer_ingame_obj_info,[
            'ingame_obj_type' => $killer_ingame_obj_type,
            'ingame_obj_sub_type' => $killer_ingame_obj_sub_type
        ]);
        //死亡
        $ingame_obj_info = $victim_ingame_obj_type = $victim_ingame_obj_sub_type = null;
        $victim_type = $match_data['payload']['killed']['type'];
        switch ($victim_type){
            case 'player':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'player');
                $victim_ingame_obj_type = 'player';
                $victim_ingame_obj_sub_type = 'player';
                break;
            case 'drake':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'elite');
                if ($match_data['payload']['killed']['object']['type'] =='elder'){
                    $victim_ingame_obj_type = 'elite';
                    $victim_ingame_obj_sub_type = 'elder_dragon';
                    if ($killer_type == 'player' && $killer_player_id){
                        self::setPlayerKillsCounters($redis,$matchId,$battleId,'dragon_kills',$killer_player_id);
                        $redis->hSet($elites_status_redis_name.'dragon_status','elder_dragon_kill_time',$ingame_timestamp);
                    }
                }else{
                    $victim_ingame_obj_type = 'elite';
                    $victim_ingame_obj_sub_type = 'drake';
                    if ($killer_type == 'player' && $killer_player_id){
                        self::setPlayerKillsCounters($redis,$matchId,$battleId,'dragon_kills',$killer_player_id);
                        $redis->hSet($elites_status_redis_name.'dragon_status','kill_time',$ingame_timestamp);
                        $killer_team_faction = @$match_data['payload']['killer']['object']['team'];
                        $killer_team_num_redis = $redis->hGet($elites_status_redis_name.'team_dragon_status',$killer_team_faction.'_kill_num');
                        $killer_team_num = $killer_team_num_redis ? ($killer_team_num_redis + 1) : 1;
                        $redis->hSet($elites_status_redis_name.'team_dragon_status',$killer_team_faction.'_kill_num',$killer_team_num);
                    }
                }
                break;
            case 'rift_herald':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'elite');
                $victim_ingame_obj_type = 'elite';
                $victim_ingame_obj_sub_type = 'rift_herald';
                if ($killer_type == 'player' && $killer_player_id){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'rift_herald_kills',$killer_player_id);
                    $redis->hSet($elites_status_redis_name.'rift_herald','kill_time',$ingame_timestamp);
                    $rift_herald_kill_num = $redis->hGet($elites_status_redis_name.'rift_herald','kill_num');
                    if ($rift_herald_kill_num){
                        $redis->hSet($elites_status_redis_name.'rift_herald','kill_num',2);
                        $redis->hSet($elites_status_redis_name.'rift_herald','exited',1);
                    }else{
                        $redis->hSet($elites_status_redis_name.'rift_herald','kill_num',1);
                    }
                }
                break;
            case 'baron_nashor':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'elite');
                $victim_ingame_obj_type = 'elite';
                $victim_ingame_obj_sub_type = 'baron_nashor';
                if ($killer_type == 'player' && $killer_player_id){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'baron_nashor_kills',$killer_player_id);
                    $redis->hSet($elites_status_redis_name.'baron_nashor','kill_time',$ingame_timestamp);
                }
                break;
            case 'tower':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'building');
                $victim_ingame_obj_type = 'building';
                $victim_ingame_obj_sub_type = 'turret';
                if ($killer_type == 'player' && $killer_player_id){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'turret_kills',$killer_player_id);
                }
                break;
            case 'inhibitor':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'building');
                $victim_ingame_obj_type = 'building';
                $victim_ingame_obj_sub_type = 'inhibitor';
                if ($killer_type == 'player' && $killer_player_id){
                    self::setPlayerKillsCounters($redis,$matchId,$battleId,'inhibitor_kills',$killer_player_id);
                }
                break;
            case 'nexus':
                $ingame_obj_info = self::conversionIngameObj($match_data['payload']['killed'],'building');
                $victim_ingame_obj_type = 'building';
                $victim_ingame_obj_sub_type = 'nexus';
                break;
            default:
                $victim_ingame_obj_type = 'unknown';
                $victim_ingame_obj_sub_type = 'unknown';
                break;
        }
        $victim = array_merge((array)$ingame_obj_info,[
            'ingame_obj_type' => $victim_ingame_obj_type,
            'ingame_obj_sub_type' => $victim_ingame_obj_sub_type
        ]);
        $assist = [];
        $first_event_Info = self::getFirstEventInfo('current_ws',$matchId,$battleId,$match_data,'ws');
        if (empty($first_event_Info)){
            return null;
        }
        $is_first_event = $first_event_Info['is_first_event'];
        $first_event_type = $first_event_Info['first_event_type'];
        $transData = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,  //待完善
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'event_id' =>$socket_id,
            'event_type' => $event_type,
            'position'=> $position,
            'killer' => $killer,
            'victim' => $victim,
            'assist' => $assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        //$trans_data = json_encode($transData,320);
        $frames_data = [
            'battle_order' => $battle_order,
            'trans_data' => $transData
        ];
        return $frames_data;
    }
    //转化player
    public static function conversionPlayers($redis,$matchId,$battleId,$players,$match_data,$faction)
    {
        //获取
        $ws_player_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_ws:battles:'.$battleId.':ws_players_kills_counters';
        $ws_player_array = $redis->hGetAll($ws_player_redis_name);
        $deaths = 0;
        $assists = 0;
        $conversionPlayers = $conversionRedisPlayers = [];
        $players_key = 1;
        foreach ($players as $key=>$value){
            $conversionPlayersInfo = [];
            $position_blue_num = Common::getLolPositionByString(Consts::ORIGIN_PANDASCORE,$key);
            $conversionPlayersInfo['seed'] = (Int)$position_blue_num;
            $conversionPlayersInfo['faction'] = (String)$faction;
            $role = Common::getPositionCNameById($position_blue_num);
            if (empty($role)){
                $conversionPlayersInfo['role'] = null;
            }else{
                $conversionPlayersInfo['role'] = (String)$role;
            }
            $lane = Common::getLaneCNameByRole($position_blue_num);
            if (empty($lane)){
                $conversionPlayersInfo['lane'] = null;
            }else{
                $conversionPlayersInfo['lane'] = (String)$lane;
            }
            $player_id_master = @self::getMainIdByRelIdentityId('player',$value['id'],'3');
            if (empty($player_id_master)){
                $conversionPlayersInfo['player'] = null;
            }else{
                $conversionPlayersInfo['player']['player_id'] = (Int)$player_id_master;
                $conversionPlayersInfo['player']['nick_name'] = @$value['name'];
            }
            $tempChampionId = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$value['champion']['id'],'3');
            $tempChampion = MetadataLolChampion::find()->select('id,external_id,external_name,name,name_cn,title,title_cn,slug,image,small_image')->where(['id' => $tempChampionId])->asArray()->one();
            if ($tempChampion){
                $conversionPlayersInfo['champion']['champion_id'] = (Int)@$tempChampion['id'];
                $conversionPlayersInfo['champion']['name'] = @$tempChampion['name'];
                $conversionPlayersInfo['champion']['name_cn'] = @$tempChampion['name_cn'];
                $conversionPlayersInfo['champion']['external_id'] = @$tempChampion['external_id'];
                $conversionPlayersInfo['champion']['external_name'] = @$tempChampion['external_name'];
                $conversionPlayersInfo['champion']['title'] = @$tempChampion['title'];
                $conversionPlayersInfo['champion']['title_cn'] = @$tempChampion['title_cn'];
                $conversionPlayersInfo['champion']['slug'] = @$tempChampion['slug'];
                $conversionPlayersInfo['champion']['image']['image'] = ImageConversionHelper::showFixedSizeConversion(@$tempChampion['image'],120,120);
                $conversionPlayersInfo['champion']['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion(@$tempChampion['image'],32,32);
            }else{
                $conversionPlayersInfo['champion']=null;
            }
            $conversionPlayersInfo['level'] = (Int)$value['level'];
            $conversionPlayersInfo['is_alive'] = $value['hp']>0 ? true:false;
            $conversionPlayersInfo['kills'] = (Int)$value['kills'];
            $player_deaths = (Int)$value['deaths'];
            $conversionPlayersInfo['deaths'] = $player_deaths;
            //计算总的死亡数
            $deaths = $player_deaths + $deaths;
            $player_assists = (Int)$value['assists'];
            $conversionPlayersInfo['assists'] = $player_assists;
            //计算总的助攻数
            $assists = $player_assists + $assists;
            //计算KDA
            $players_deaths = 1;
            if ($value['deaths'] > 0 ){
                $players_deaths = $value['deaths'];
            }
            $conversionPlayersInfo['kda'] = (String)round((($value['kills'] + $value['assists'])/$players_deaths),2);
            if ($match_data[$faction]['kills'] == 0){
                $match_data[$faction]['kills'] = 1;
            }
            $participation_team_kills = $match_data[$faction]['kills'] ? $match_data[$faction]['kills'] : 1;
            $conversionPlayersInfo['participation'] = (String)round((($value['kills'] + $value['assists'])/$participation_team_kills),4);
            $conversionPlayersInfo['cs'] = (Int)$value['cs'];
            $conversionPlayersInfo['cspm'] = (String)round(($value['cs']/($match_data['current_timestamp']/60)),2);
            $conversionPlayersInfo['summoner_spells'] = self::conversionPlayerSummonerSpells($value['summoner_spells']);
            $conversionPlayersInfo['items'] = self::conversionPlayerItems($value['items']);
            if (empty(@$ws_player_array[@$value['id']])){
                $turret_kills = 0;
                $inhibitor_kills = 0;
                $rift_herald_kills = 0;
                $dragon_kills = 0;
                $baron_nashor_kills = 0;
            }else{
                $players_res_array = @json_decode(@$ws_player_array[@$value['id']],true);
                $turret_kills = @$players_res_array['turret_kills'];
                $inhibitor_kills = @$players_res_array['inhibitor_kills'];
                $rift_herald_kills = @$players_res_array['rift_herald_kills'];
                $dragon_kills = @$players_res_array['dragon_kills'];
                $baron_nashor_kills = @$players_res_array['baron_nashor_kills'];
            }
            //设置初始值
            $advanced = [
                'keystone' => null,
                'ultimate_cd' => null,
                'coordinate' => null,
                'respawntimer' => null,
                'health' => null,
                'health_max' => null,
                'turret_kills' => (Int)$turret_kills,
                'inhibitor_kills' => (Int)$inhibitor_kills,
                'rift_herald_kills' => (Int)$rift_herald_kills,
                'dragon_kills' => (Int)$dragon_kills,
                'baron_nashor_kills' => (Int)$baron_nashor_kills,
                'double_kill' => null,
                'triple_kill' => null,
                'quadra_kill' => null,
                'penta_kill' => null,
                'largest_multi_kill' => null,
                'largest_killing_spree' => null,
                'minion_kills' => null,
                'total_neutral_minion_kills' => null,
                'neutral_minion_team_jungle_kills' => null,
                'neutral_minion_enemy_jungle_kills' => null,
                'gold_earned' => null,
                'gold_spent' => null,
                'gold_remaining' => null,
                'gpm' => null,
                'gold_earned_percent' => null,
                'experience' => null,
                'xpm' => null,
                'damage_to_champions' => null,
                'damage_to_champions_physical' => null,
                'damage_to_champions_magic' => null,
                'damage_to_champions_true' => null,
                'dpm_to_champions' => null,
                'damage_percent_to_champions' => null,
                'total_damage' => null,
                'total_damage_physical' => null,
                'total_damage_magic' => null,
                'total_damagel_true' => null,
                'damage_taken' => null,
                'damage_taken_physical' => null,
                'damage_taken_magic' => null,
                'damage_taken_true' => null,
                'dtpm' => null,
                'damage_taken_percent' => null,
                'damage_conversion_rate' => null,
                'damage_selfmitigated' => null,
                'damage_shielded_on_teammates' => null,
                'damage_to_buildings' => null,
                'damage_to_towers' => null,
                'damage_to_objectives' => null,
                'total_crowd_control_time' => null,
                'total_crowd_control_time_others' => null,
                'total_heal' => null,
                'wards_purchased' => null,
                'wards_placed' => null,
                'wards_kills' => null,
                'vision_score' => null
            ];
            $conversionPlayersInfo['advanced'] = $advanced;
            $conversionPlayers[$players_key] = $conversionPlayersInfo;
            $players_key ++ ;
        }
        $total_cost_array = array_column($conversionPlayers,'seed');
        array_multisort($total_cost_array,SORT_ASC,$conversionPlayers);
        $result = [
            'deaths' => $deaths,
            'assists' => $assists,
            'players' => $conversionPlayers
        ];
        return $result;
    }
    //转化ingame_obj 游戏内对象
    public static function conversionIngameObj($ingame_obj_info,$ingame_obj_type){
        $ingame_obj_info_res = [];
        $player= [
            'player_id' => null,
            'nick_name' => null,
            'faction' => null,
            'champion' => [
                'champion_id' => null,
                'name' => null,
                'name_cn' => null,
                'external_id' => null,
                'external_name' => null,
                'title' => null,
                'title_cn' => null,
                'slug'=> null,
                'image' => [
                    'image' => null,
                    'small_image' => null
                ]
            ]
        ];
        $building = [
            'lane' => null,
            'ingame_obj_name' => null,
            'ingame_obj_name_cn' => null,
            'faction' => null
        ];
        $minion = [
            'ingame_obj_name' => null,
            'ingame_obj_name_cn' => null,
            'faction' => null
        ];
        //neutral_minion or elite or unknow
        $elite = $victim_other = [
            'ingame_obj_name' => null,
            'ingame_obj_name_cn' => null
        ];

        switch ($ingame_obj_type){
            case 'player':
                $player['player_id'] = (Int)@self::getMainIdByRelIdentityId('player',$ingame_obj_info['object']['id'],'3');
                $player['nick_name'] = $ingame_obj_info['object']['name'];
                $player['faction'] = $ingame_obj_info['object']['team'];
                $champion_id = $ingame_obj_info['object']['champion']['id'];
                $tempChampionId = self::getMainIdByRelIdentityIdOrUnknown('lol_champion',$champion_id,'3');
                $tempChampion = MetadataLolChampion::find()->select('id,external_id,external_name,name,name_cn,title,title_cn,slug,image,small_image')->where(['id' => $tempChampionId])->asArray()->one();
                if ($tempChampion){
                    $player['champion']['champion_id'] = (Int)$tempChampion['id'];
                    $player['champion']['name'] = $tempChampion['name'];
                    $player['champion']['name_cn'] = $tempChampion['name_cn'];
                    $player['champion']['external_id'] = $tempChampion['external_id'];
                    $player['champion']['external_name'] = $tempChampion['external_name'];
                    $player['champion']['title'] = $tempChampion['title'];
                    $player['champion']['title_cn'] = $tempChampion['title_cn'];
                    $player['champion']['slug'] = $tempChampion['slug'];
                    $player['champion']['image']['image'] = ImageConversionHelper::showFixedSizeConversion($tempChampion['image'],120,120);
                    $player['champion']['image']['small_image'] = ImageConversionHelper::showFixedSizeConversion($tempChampion['image'],32,32);
                }else{
                    $player['champion'] = null;
                }
                $ingame_obj_info_res = $player;
                break;
            case 'building':
                $ingame_obj_info_ingame = @self::getGoalIdByPandascoreName($ingame_obj_info['object']['name']);
                $building['lane'] = null;
                $building['ingame_obj_name'] = @$ingame_obj_info_ingame['ingame_obj_name'];
                $building['ingame_obj_name_cn'] = @$ingame_obj_info_ingame['ingame_obj_name_cn'];
                $building['faction'] = $ingame_obj_info['object']['team'];
                $ingame_obj_info_res = $building;
                break;
            case 'minion':
                $ingame_obj_info_ingame = @self::getGoalIdByPandascoreName($ingame_obj_info['object']['name']);
                $minion['ingame_obj_name'] = @$ingame_obj_info_ingame['ingame_obj_name'];
                $minion['ingame_obj_name_cn'] = @$ingame_obj_info_ingame['ingame_obj_name_cn'];
                $minion['faction'] = $ingame_obj_info['object']['team'];
                $ingame_obj_info_res = $minion;
                break;
            case 'elite':
                $ingame_obj_info_ingame = @self::getGoalIdByPandascoreName($ingame_obj_info['object']['name']);
                $elite['ingame_obj_name'] = @$ingame_obj_info_ingame['ingame_obj_name'];
                $elite['ingame_obj_name_cn'] = @$ingame_obj_info_ingame['ingame_obj_name_cn'];
                $ingame_obj_info_res = $elite;
                break;
            case 'baron_nashor':
                $ingame_obj_info_ingame = @self::getGoalIdByPandascoreName($ingame_obj_info['object']['name']);
                $elite['ingame_obj_name'] = @$ingame_obj_info_ingame['ingame_obj_name'];
                $elite['ingame_obj_name_cn'] = @$ingame_obj_info_ingame['ingame_obj_name_cn'];
                $ingame_obj_info_res = $elite;
                break;
            default:
                $ingame_obj_info_ingame = @self::getGoalIdByPandascoreName($ingame_obj_info['object']['name']);
                $other['ingame_obj_name'] = @$ingame_obj_info_ingame['ingame_obj_name'];
                $other['ingame_obj_name_cn'] = @$ingame_obj_info_ingame['ingame_obj_name_cn'];
                $ingame_obj_info_res = $other;
        }
        return $ingame_obj_info_res;
    }
    /**
     * @param $sub_type(子类型)
     * @param $ingame_obj_name(游戏内对象名称)
     */
    public static function getGoalIdByPandascoreName($name)
    {
        if(isset($name) && $name){
            $goal = EnumIngameGoal::find()->where(['like','pandascore_name',$name])->asArray()->one();
            if($goal){
                return $goal;
            }else{
                return null;
            }
        }
    }
    //插入 创造的事件
    public static function insertEventsFromPs($insertData){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $insertData);
        $db_websoket->execute();
        return true;
    }
    //设置玩家 KillsCounters
    public static function setPlayerKillsCounters($redis,$matchId,$battleId,$kills_type,$killer_player_id){
        $ws_player_redis_name = Consts::MATCH_ALL_INFO.':'.$matchId.':current_ws:battles:'.$battleId.':ws_players_kills_counters';
        $playerKillsCountersRedis = $redis->hGet($ws_player_redis_name,$killer_player_id);
        if ($playerKillsCountersRedis){
            $playerKillsCountersArray = @json_decode($playerKillsCountersRedis,true);
            $playerKillsCountersArray[$kills_type] = (Int)$playerKillsCountersArray[$kills_type] + 1;
        }else{
            $playerKillsCountersArray = [
                'turret_kills' => 0,
                'inhibitor_kills' => 0,
                'rift_herald_kills' => 0,
                'dragon_kills' => 0,
                'baron_nashor_kills' => 0,
            ];
            $playerKillsCountersArray[$kills_type] = 1;
        }
        $redis->hSet($ws_player_redis_name,$killer_player_id,@json_encode($playerKillsCountersArray,320));
        return true;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}