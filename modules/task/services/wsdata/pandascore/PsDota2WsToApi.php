<?php

namespace app\modules\task\services\wsdata\pandascore;

use app\modules\common\services\Consts;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchComingSocketOrder;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class PsDota2WsToApi extends HotBase
{
    private static $instance;
    public static function run($tagInfo, $taskInfo)
    {
        $PsDota2WsToApi = self::getInstance();
        $redis = $PsDota2WsToApi->redis;
        $info       = json_decode($taskInfo['params'], true);
        $matchId = @$info['matchId'];
        $rel_matchId = @$info['rel_matchId'];
        $battleId = @$info['battleId'];
        $battleOrder = @$info['battleOrder'];
        $event_type = @$info['event_type'];
        $event_order = @$info['event_order'];
        $run_type = @$info['run_type'];//refresh socket
        if (!$matchId || !$battleId) {
            return false;
        }
        if (empty($run_type)){
            $run_type = 'socket';
        }
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId;
        $redis_base_name_current = $redis_base_name.":current:";
        //判断是不是 重抓，并且统一覆盖
        if ($event_type == 'refresh_match_xiufu'){
            $matchConversionInfo   = $PsDota2WsToApi->conversionMatch($redis,$redis_base_name,$matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battles = [];
            //获取所有的battle
            $match_battles = $redis->hGetAll($redis_base_name_current."match_battles");
            if ($match_battles){
                foreach ($match_battles as $key => $item){
                    $battle_info_array = explode('-',$item);
                    $battle_id_ref = $battle_info_array[0];
                    $battlesConversionInfoFrames = $PsDota2WsToApi->conversionBattlesFrames($redis,$redis_base_name_current,$battle_id_ref);
                    $battlesConversionInfoFrames['battle_id'] = $battle_id_ref;
                    //获取events
                    $battlesConversionInfoEvents = $PsDota2WsToApi->conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battle_id_ref,$battleOrder,$event_type,$event_order);
                    $battlesConversionInfoFrames['events'] = $battlesConversionInfoEvents;
                    $battles[] = $battlesConversionInfoFrames;
                }
            }
            $infoFormat['battles'] = $battles;
            //保存
            $PsDota2WsToApi->refreshMatchAndBattle($matchId, $battleId,$infoFormat,'refresh_match_xiufu');
        }elseif ($event_type == 'frames'){
            $matchConversionInfo   = $PsDota2WsToApi->conversionMatch($redis,$redis_base_name,$matchId);
            $infoFormat['match']   = $matchConversionInfo;
            $battlesConversionInfoFrames = $PsDota2WsToApi->conversionBattlesFrames($redis,$redis_base_name_current,$battleId);
            $infoFormat['battle'] = $battlesConversionInfoFrames;
            //保存
            $PsDota2WsToApi->refreshMatchAndBattle($matchId, $battleId,$infoFormat);
            //判断是不是结束了
            if($infoFormat['match']['status'] == 3){
                self::refreshApi($matchId,4);
                if ($run_type == 'socket'){
                    //刷rest api
                    self::changeHotData(3,3,$matchId);
                }
            }
        }else{//events 事件
            $battlesConversionInfoEvents = $PsDota2WsToApi->conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battleId,$battleOrder,$event_type,$event_order);
            $battle['events'] = $battlesConversionInfoEvents;
            //保存指定的events
            if(isset($battle['events']) && count($battle['events']) > 0){
                BattleService::setBattleInfo($battleId,'dota',$battle['events'],BattleService::DATA_TYPE_EVENTS);
            }
        }
        return $battleId;
    }
    //match 的信息
    public function conversionMatch($redis,$redis_base_name)
    {
        $matchinfoFormat = [];
        $match_real_time_info_redis = $redis->hGetAll($redis_base_name.':match_real_time_info');
        if ($match_real_time_info_redis){
            $matchinfoFormat           = [
                'begin_at'             => @$match_real_time_info_redis['begin_at'],
                'end_at'               => @$match_real_time_info_redis['end_at'],
                'status'               => @$match_real_time_info_redis['status'],
                'team_1_score'         => @$match_real_time_info_redis['team_1_score'],
                'team_2_score'         => @$match_real_time_info_redis['team_2_score'],
                'winner'               => @$match_real_time_info_redis['winner'],
                'is_battle_detailed'   => 1
            ];
        }
        return $matchinfoFormat;
    }
    //battle  frames 信息
    public function conversionBattlesFrames($redis,$redis_base_name_current,$battleId){
        $battleFramesInfo = [
            'base' => '',
            'detail' => '',
            'static' => '',
            'players' => '',
            'ban_pick' => ''
        ];
        //base
        $battleFramesInfoBase = [];
        //获取base数据
        $battleRedisInfoBase = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":base");



        if ($battleRedisInfoBase){
            $battleFramesInfoBase = [
                'id' => @$battleRedisInfoBase['id'],
                'game' => @$battleRedisInfoBase['game'],
                'match' => @$battleRedisInfoBase['match'],
                'order' => @$battleRedisInfoBase['order'],
                'begin_at' => @$battleRedisInfoBase['begin_at'],
                'end_at' => @$battleRedisInfoBase['end_at'],
                'status' => @$battleRedisInfoBase['status'],
                'is_draw' => @$battleRedisInfoBase['is_draw'],
                'is_forfeit' => @$battleRedisInfoBase['is_forfeit'],
                'is_default_advantage' => @$battleRedisInfoBase['is_default_advantage'],
                'map' => @$battleRedisInfoBase['map'],
                'duration' => (string)@$battleRedisInfoBase['duration'],
                'winner' => @$battleRedisInfoBase['winner'],
                'is_battle_detailed' => @$battleRedisInfoBase['is_battle_detailed'],
                'flag' => 1,
                'deleted' => 2,
                'deleted_at' => null
            ];
        }
        $battleFramesInfo['base'] = $battleFramesInfoBase;
        //detail
        $battleFramesInfoDetail = [];
        //获取detail信息
        $battleRedisInfoDetail = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":detail");
        if ($battleRedisInfoDetail){
            //$gold_diff_timeline

            $gold_diff_timeline = $redis->get($redis_base_name_current."battles:".$battleId.":gold_diff_timeline");
            if (!$gold_diff_timeline){
                $gold_diff_timeline = null;
            }
            //组合数据
            $battleFramesInfoDetail = [

                'is_confirmed' => @$battleRedisInfoDetail['is_confirmed'],
                'is_finished' => @$battleRedisInfoDetail['is_finished'],
                'ban_pick' => @$battleRedisInfoDetail['ban_pick'],
                'first_blood_p_tid' => @$battleRedisInfoDetail['first_blood_p_tid'],
                'first_blood_time' => @$battleRedisInfoDetail['first_blood_time'],
                'first_to_5_kills_p_tid' => @$battleRedisInfoDetail['first_to_5_kills_p_tid'],
                'first_to_5_kills_time' => @$battleRedisInfoDetail['first_to_5_kills_time'],
                'first_to_10_kills_p_tid' => @$battleRedisInfoDetail['first_to_10_kills_p_tid'],
                'first_to_10_kills_time' => @$battleRedisInfoDetail['first_to_10_kills_time'],
                'first_tower_p_tid' => @$battleRedisInfoDetail['first_tower_p_tid'],
                'first_tower_time' => @$battleRedisInfoDetail['first_tower_time'],
                'first_barracks_p_tid' => @$battleRedisInfoDetail['first_barracks_p_tid'],
                'first_barracks_time' => @$battleRedisInfoDetail['first_barracks_time'],
                'first_roshan_p_tid' => @$battleRedisInfoDetail['first_roshan_p_tid'],
                'first_roshan_time' => @$battleRedisInfoDetail['first_roshan_time'],
                'net_worth_diff_timeline' => @$battleRedisInfoDetail['net_worth_diff_timeline'],
                'experience_diff_timeline' => @$battleRedisInfoDetail['experience_diff_timeline'],
                'is_pause' => @$battleRedisInfoDetail['is_pause'],
                'is_live' => @$battleRedisInfoDetail['is_live'],
                'time_of_day' => @$battleRedisInfoDetail['time_of_day'],
                'elites_status' => @$battleRedisInfoDetail['elites_status'],
                'first_blood_details' => @$battleRedisInfoDetail['first_blood_details'],
                'first_to_5_kills_details' => @$battleRedisInfoDetail['first_to_5_kills_details'],
                'first_to_10_kills' => @$battleRedisInfoDetail['first_to_10_kills'],
                'first_tower' => @$battleRedisInfoDetail['first_tower'],
                'first_barracks' => @$battleRedisInfoDetail['first_barracks'],
                'first_roshan' => @$battleRedisInfoDetail['first_roshan'],
            ];
        }
        $battleFramesInfo['detail'] = $battleFramesInfoDetail;
        //teams
        $battleFramesInfoStatic = [];
        $battleRedisInfoStatic = $redis->hGetAll($redis_base_name_current."battles:".$battleId.":teams");
        if ($battleRedisInfoStatic){
            foreach ($battleRedisInfoStatic as $team_val){
                $team_info_array = @json_decode($team_val,true);
                $dragon_kills_detail_res = null;
                if ($team_info_array['dragon_kills_detail']){
                    $dragon_kills_detail_res = @json_encode($team_info_array['dragon_kills_detail'],320);
                }
                $team_info = [
                    'order' => @$team_info_array['order'],
                    'battle_id' => @$team_info_array['battle_id'],
                    'score' => @$team_info_array['score'],
                    'identity_id' => @$team_info_array['identity_id'],
                    'rel_identity_id' => (string)@$team_info_array['rel_identity_id'],
                    'team_id' => @$team_info_array['team_id'],
                    'name' => @$team_info_array['name'],


                    'faction' => @$team_info_array['faction'],
                    'kills' => @$team_info_array['kills'],
                    'net_worth' => @$team_info_array['net_worth'],
                    'gold_earned' => @$team_info_array['gold_earned'],
                    'experience' => @$team_info_array['experience'] ?? null,
                    'tower_kills' => @$team_info_array['tower_kills'] ?? null,
                    'melee_barrack_kills' => @$team_info_array['melee_barrack_kills'] ?? null,
                    'ranged_barrack_kills' => @$team_info_array['ranged_barrack_kills'] ?? null,
                    'roshan_kills' => @$team_info_array['roshan_kills'] ?? null,
                    'building_status' => @json_encode($team_info_array['building_status'],320),
                    'building_status_towers' => @json_encode($team_info_array['building_status_towers'],320),
                    'building_status_barracks' => @json_encode($team_info_array['building_status_barracks'],320),
                    'building_status_ancient' => @json_encode($team_info_array['building_status_ancient'],320),
                    'deaths' => @$team_info_array['deaths'] ?? null,
                    'assists' => @$team_info_array['assists'] ?? null,
                    'net_worth_diff' => @$team_info_array['net_worth_diff'] ?? null,
                    'experience_diff' => @$team_info_array['experience_diff'] ?? null,
                    'barrack_kills' => @$team_info_array['barrack_kills'] ?? null,
                    'building_status_outposts' => @$team_info_array['building_status_outposts'] ?? null,
                    'total_runes_pickedup' => @$team_info_array['total_runes_pickedup'] ?? null,
                    'bounty_runes_pickedup' => @$team_info_array['bounty_runes_pickedup'] ?? null,
                    'double_damage_runes_pickedup' => @$team_info_array['double_damage_runes_pickedup'] ?? null,
                    'haste_runes_pickedup' => @$team_info_array['haste_runes_pickedup'] ?? null,
                    'illusion_runes_pickedup' => @$team_info_array['illusion_runes_pickedup'] ?? null,
                    'invisibility_runes_pickedup' => @$team_info_array['invisibility_runes_pickedup'] ?? null,
                    'regeneration_runes_pickedup' => @$team_info_array['regeneration_runes_pickedup'] ?? null,
                    'arcane_runes_pickedup' => @$team_info_array['arcane_runes_pickedup'] ?? null,
                    'smoke_purchased' => @$team_info_array['smoke_purchased'] ?? null,
                    'smoke_used' => @$team_info_array['smoke_used'] ?? null,
                    'dust_purchased' => @$team_info_array['dust_purchased'] ?? null,
                    'dust_used' => @$team_info_array['dust_used'] ?? null,
                    'observer_wards_purchased' => @$team_info_array['observer_wards_purchased'] ?? null,
                    'observer_wards_placed' => @$team_info_array['observer_wards_placed'] ?? null,
                    'observer_wards_kills' => @$team_info_array['observer_wards_kills'] ?? null,
                    'sentry_wards_purchased' => @$team_info_array['sentry_wards_purchased'] ?? null,
                    'sentry_wards_placed' => @$team_info_array['sentry_wards_placed'] ?? null,
                    'sentry_wards_kills' => @$team_info_array['sentry_wards_kills'] ?? null,
                ];
                $battleFramesInfoStatic[] = $team_info;
            }
        }
        $battleFramesInfo['static'] = $battleFramesInfoStatic;
        //player
        $battleFramesInfoPlayers = null;
        $battleRedisInfoPlayers = $redis->hVals($redis_base_name_current."battles:".$battleId.":players");
        if ($battleRedisInfoPlayers){
            foreach ($battleRedisInfoPlayers as $player_item){
                $player_info_array = @json_decode($player_item,true);
                $player_info = [

                    'order' => @$player_info_array['order'],
                    'rel_identity_id' => (string)@$player_info_array['rel_identity_id'],
                    'rel_team_id' => (string)@$player_info_array['rel_team_id'],
                    'team_order' => (int)@$player_info_array['team_order'],
                    'seed' => @$player_info_array['seed'],
                    'battle_id' => @$player_info_array['battle_id'],
                    'game' => @$player_info_array['game'],
                    'match' => @$player_info_array['match'],
                    'team_id' => @$player_info_array['team_id'],
                    'faction' => @$player_info_array['faction'],
                    'role' => @$player_info_array['role'],
                    'lane' => @$player_info_array['lane'],
                    'player_id' => @$player_info_array['player_id'],
                    'nick_name' => @$player_info_array['nick_name'],
                    'rel_nick_name' => @$player_info_array['rel_nick_name'],
                    'hero' => @$player_info_array['hero'],
                    'level' => @$player_info_array['level'],
                    'is_alive' => @$player_info_array['is_alive'],
                    'ultimate_cd' => null,
                    'coordinate' => null,
                    'kills' => @$player_info_array['kills'],
                    'double_kill' => @$player_info_array['double_kill'],
                    'triple_kill' => @$player_info_array['triple_kill'],
                    'ultra_kill' => @$player_info_array['ultra_kill'],
                    'rampage' => @$player_info_array['rampage'],
                    'largest_multi_kill' => @$player_info_array['largest_multi_kill'],
                    'largest_killing_spree' => @$player_info_array['largest_killing_spree'],
                    'deaths' => @$player_info_array['deaths'],
                    'assists' => @$player_info_array['assists'],
                    'kda' => @$player_info_array['kda'],
                    'participation' => @$player_info_array['participation'],
                    'last_hits' => @$player_info_array['last_hits'],
                    'lane_creep_kills' => @$player_info_array['lane_creep_kills'],
                    'neutral_creep_kills' => @$player_info_array['neutral_creep_kills'],
                    'neutral_minion_team_jungle_kills' => @$player_info_array['neutral_minion_team_jungle_kills'] ?? null,
                    'neutral_minion_enemy_jungle_kills' => @$player_info_array['neutral_minion_enemy_jungle_kills'] ?? null,
                    'lhpm' => @$player_info_array['lhpm'] ?? null,
                    'denies' => @$player_info_array['denies'] ?? null,
                    'net_worth' => @$player_info_array['net_worth'] ?? null,
                    'gold_earned' => @$player_info_array['gold_earned'] ?? null,
                    'gold_spent' => @$player_info_array['gold_spent'] ?? null,
                    'gold_remaining' => @$player_info_array['gold_remaining'] ?? null,
                    'gpm' => @$player_info_array['gpm'] ?? null,
                    'gold_earned_percent' => @$player_info_array['gold_earned_percent'] ?? null,
                    'experience' => @$player_info_array['experience'] ?? null,
                    'xpm' => @$player_info_array['xpm'] ?? null,
                    'damage_to_heroes' => @$player_info_array['damage_to_heroes'] ?? null,
                    'dpm_to_heroes' => @$player_info_array['dpm_to_heroes'] ?? null,
                    'damage_percent_to_heroes' => @$player_info_array['damage_percent_to_heroes'] ?? null,
                    'damage_taken' => @$player_info_array['damage_taken'] ?? null,
                    'dtpm' => @$player_info_array['dtpm'] ?? null,
                    'damage_taken_percent' => @$player_info_array['damage_taken_percent'] ?? null,
                    'damage_to_heroes_hp_removal' => @$player_info_array['damage_to_heroes_hp_removal'] ?? null,
                    'damage_to_heroes_magical' => @$player_info_array['damage_to_heroes_magical'] ?? null,
                    'damage_to_heroes_physical' => @$player_info_array['damage_to_heroes_physical'] ?? null,
                    'damage_to_heroes_pure' => @$player_info_array['damage_to_heroes_pure'] ?? null,
                    'damage_by_mobs_hp_removal' => @$player_info_array['damage_by_mobs_hp_removal'] ?? null,
                    'damage_by_mobs_magical_dmg' => @$player_info_array['damage_by_mobs_magical_dmg'] ?? null,
                    'damage_by_mobs_physical_dmg' => @$player_info_array['damage_by_mobs_physical_dmg'] ?? null,
                    'damage_by_mobs_pure_dmg' => @$player_info_array['damage_by_mobs_pure_dmg'] ?? null,
                    'damage_to_towers' => @$player_info_array['damage_to_towers'] ?? null,
                    'damage_from_heroes_hp_removal' => @$player_info_array['damage_from_heroes_hp_removal'] ?? null,
                    'damage_from_heroes_magical_dmg' => @$player_info_array['damage_from_heroes_magical_dmg'] ?? null,
                    'damage_from_heroes_physical_dmg' => @$player_info_array['damage_from_heroes_physical_dmg'] ?? null,
                    'damage_from_heroes_pure_dmg' => @$player_info_array['damage_from_heroes_pure_dmg'] ?? null,
                    'damage_from_mobs_hp_removal' => @$player_info_array['damage_from_mobs_hp_removal'] ?? null,
                    'damage_from_mobs_magical_dmg' => @$player_info_array['damage_from_mobs_magical_dmg'] ?? null,
                    'damage_from_mobs_physical_dmg' => @$player_info_array['damage_from_mobs_physical_dmg'] ?? null,
                    'damage_from_mobs_pure_dmg' => @$player_info_array['damage_from_mobs_pure_dmg'] ?? null,
                    'damage_conversion_rate' => @$player_info_array['damage_conversion_rate'] ?? null,
                    'total_heal' => @$player_info_array['total_heal'] ?? null,
                    'total_crowd_control_time' => @$player_info_array['total_crowd_control_time'] ?? null,
                    'wards_purchased' => @$player_info_array['wards_purchased'] ?? null,
                    'wards_placed' => @$player_info_array['wards_placed'] ?? null,
                    'wards_kills' => @$player_info_array['wards_kills'] ?? null,
                    'observer_wards_purchased' => @$player_info_array['observer_wards_purchased'] ?? null,
                    'observer_wards_placed' => @$player_info_array['observer_wards_placed'] ?? null,
                    'observer_wards_kills' => @$player_info_array['observer_wards_kills'] ?? null,
                    'sentry_wards_purchased' => @$player_info_array['sentry_wards_purchased'] ?? null,
                    'sentry_wards_placed' => @$player_info_array['sentry_wards_placed'] ?? null,
                    'sentry_wards_kills' => @$player_info_array['sentry_wards_kills'] ?? null,
                    'camps_stacked' => @$player_info_array['camps_stacked'] ?? null,
                    'runes_detail_double_damage_runes' => @$player_info_array['runes_detail_double_damage_runes'] ?? null,
                    'runes_detail_haste_runes' => @$player_info_array['runes_detail_haste_runes'] ?? null,
                    'runes_detail_illusion_runes' => @$player_info_array['runes_detail_illusion_runes'] ?? null,
                    'runes_detail_invisibility_runes' => @$player_info_array['runes_detail_invisibility_runes'] ?? null,
                    'runes_detail_regeneration_runes' => @$player_info_array['runes_detail_regeneration_runes'] ?? null,
                    'runes_detail_bounty_runes' => @$player_info_array['runes_detail_bounty_runes'] ?? null,
                    'runes_detail_arcane_runes' => @$player_info_array['runes_detail_arcane_runes'] ?? null,
                    'items' => @json_encode($player_info_array['items'],320),
                    'summoner_spells' => @json_encode($player_info_array['summoner_spells'],320),
                    'abilities_timeline' => @json_encode($player_info_array['abilities_timeline'],320),
                    'items_timeline' => @json_encode($player_info_array['items_timeline'],320),
                    'talents' => @json_encode($player_info_array['talents'],320),

                    'is_visible' => @$player_info_array['is_visible'],
                    'respawntimer' => @$player_info_array['respawntimer'],
                    'buyback_cooldown' => @$player_info_array['buyback_cooldown'],
                    'buyback_cost' => @$player_info_array['buyback_cost'],
                    'health' => @$player_info_array['health'],
                    'health_max' => @$player_info_array['health_max'],
                    'tower_kills' => @$player_info_array['tower_kills'],
                    'barrack_kills' => @$player_info_array['barrack_kills'],
                    'melee_barrack_kills' => @$player_info_array['melee_barrack_kills'],
                    'ranged_barrack_kills' => @$player_info_array['ranged_barrack_kills'],
                    'roshan_kills' => @$player_info_array['roshan_kills'],
                    'net_worth_percent' => @$player_info_array['net_worth_percent'],
                    'gold_reliable' => @$player_info_array['gold_reliable'],
                    'gold_unreliable' => @$player_info_array['gold_unreliable'],
                    'gold_herokill' => @$player_info_array['gold_herokill'],
                    'gold_creepkill' => @$player_info_array['gold_creepkill'],
                    'damage_taken_hp_removal' => @$player_info_array['damage_taken_hp_removal'],
                    'damage_taken_magical' => @$player_info_array['damage_taken_magical'],
                    'damage_taken_physical' => @$player_info_array['damage_taken_physical'],
                    'damage_taken_pure' => @$player_info_array['damage_taken_pure'],
                    'damage_to_buildings' => @$player_info_array['damage_to_buildings'],
                    'damage_to_objectives' => @$player_info_array['damage_to_objectives'],
                    'total_runes_pickedup' => @$player_info_array['total_runes_pickedup'],
                    'bounty_runes_pickedup' => @$player_info_array['bounty_runes_pickedup'],
                    'double_damage_runes_pickedup' => @$player_info_array['double_damage_runes_pickedup'],
                    'haste_runes_pickedup' => @$player_info_array['haste_runes_pickedup'],
                    'illusion_runes_pickedup' => @$player_info_array['illusion_runes_pickedup'],
                    'invisibility_runes_pickedup' => @$player_info_array['invisibility_runes_pickedup'],
                    'regeneration_runes_pickedup' => @$player_info_array['regeneration_runes_pickedup'],
                    'arcane_runes_pickedup' => @$player_info_array['arcane_runes_pickedup'],
                    'smoke_purchased' => @$player_info_array['smoke_purchased'],
                    'smoke_used' => @$player_info_array['smoke_used'],
                    'dust_purchased' => @$player_info_array['dust_purchased'],
                    'dust_used' => @$player_info_array['dust_used'],
                    'steam_id' => @$player_info_array['steam_id'],
                ];
                $battleFramesInfoPlayers[] = $player_info;
            }
        }
        $battleFramesInfo['players'] = $battleFramesInfoPlayers;


        //ban pick
        $battleDetailInfoBanPick = null;
        $battleRedisInfoBanPick = $redis->hVals($redis_base_name_current."battles:".$battleId.":ban_pick");
        if ($battleRedisInfoBanPick){
            foreach ($battleRedisInfoBanPick as $ban_pick_item){
                $battleDetailInfoBanPick[] = @json_decode($ban_pick_item,true);
            }
        }
        $battleFramesInfo['ban_pick'] = $battleDetailInfoBanPick;
        return $battleFramesInfo;
    }
    //battle 事件 信息
    public function conversionBattlesEvents($redis,$redis_base_name_current,$matchId,$rel_matchId,$battleId,$battleOrder,$event_type,$event_order){
        $event_list = [];
        //如果是结束做个大覆盖
        if ($event_type == 'refresh_match_xiufu'){
            $battleEventsInfo = $redis->hVals($redis_base_name_current."battles:".$battleId.":event_list");
            if ($battleEventsInfo){
                foreach ($battleEventsInfo as $item){
                    $event_info = @json_decode($item,true);
                    if ($event_info['assist']){
                        $event_info['assist'] = @json_encode($event_info['assist'],320);
                    }
                    $event_list[] = $event_info;
                }
                if ($event_list){
                    //ingame_timestamp 按照这个排序
                    $event_list_ingame_timestamp = array_column($event_list,'ingame_timestamp');
                    array_multisort($event_list_ingame_timestamp,SORT_ASC,$event_list);
                }
            }
        }elseif ($event_type == 'battle_end'){
            $battleEventsInfo = $redis->hVals($redis_base_name_current."battles:".$battleId.":event_list");
            if ($battleEventsInfo){
                foreach ($battleEventsInfo as $item){
                    $event_info = @json_decode($item,true);
                    if ($event_info['assist']){
                        $event_info['assist'] = @json_encode($event_info['assist'],320);
                    }
                    $event_list[] = $event_info;
                }
            }
        }else{
            $event_info_redis = $redis->hGet($redis_base_name_current."battles:".$battleId.":event_list",$event_order);
            if ($event_info_redis){
                $event_info = @json_decode($event_info_redis,true);
                if ($event_info['assist']){
                    $event_info['assist'] = @json_encode($event_info['assist'],320);
                }
                $event_list[] = $event_info;
            }
        }
        return $event_list;
    }
    //保存match 和 battle
    public function refreshMatchAndBattle($matchId, $battleId, $info,$refresh_match_xiufu = null){
        if ($refresh_match_xiufu == 'refresh_match_xiufu'){
            foreach ($info['battles'] as $battle){
                $battle_id = $battle['battle_id'];
                if (!empty($battle_id)){
                    //更新battle
                    self::refreshBattle($battle_id,$battle,$refresh_match_xiufu);
                }
            }
        }else{
            //更新battle
            self::refreshBattle($battleId,$info['battle']);
        }
        //更新Match
        self::refreshMatch($matchId,$info['match']);
    }
    private static function refreshBattle($battleId, $battle,$refresh_match_xiufu = null)
    {
        //更新数据battle
        BattleService::setBattleInfo($battleId,'dota',$battle['base'],BattleService::DATA_TYPE_BASE);
        BattleService::setBattleInfo($battleId,'dota',$battle['detail'],BattleService::DATA_TYPE_DETAILS);
        if (isset($battle['static']) && count($battle['static'])) {
            BattleService::setBattleInfo($battleId, 'dota', $battle['static'], BattleService::DATA_TYPE_STATICS);
        }
        if (isset($battle['players']) && count($battle['players'])){
            BattleService::setBattleInfo($battleId,'dota',$battle['players'],BattleService::DATA_TYPE_PLAYERS);
        }
        if (count($battle['ban_pick']) > 0){
            BattleService::setBattleInfo($battleId,'dota',$battle['ban_pick'],BattleService::DATA_TYPE_BAN_PICK);
        }
        //判断是不是 修复
        if ($refresh_match_xiufu == 'refresh_match_xiufu'){
            if(isset($battle['events']) && count($battle['events']) > 0){
                BattleService::setBattleInfo($battleId,'dota',$battle['events'],BattleService::DATA_TYPE_EVENTS);
            }
        }
        return true;
    }
    private static function refreshMatch($matchId, $matchInfo)
    {
        MatchService::setRealTimeInfo($matchId, $matchInfo, 'detail_by_order_scoket_frames');
        return true;
    }
    //刷API
    private static function refreshApi($matchId,$originId){
        //battle
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId,   RenovateService::INTERFACE_TYPE_BATTLE, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        //match
        $taskInfo = [
            "tag"      => QueueServer::getTag(QueueServer::QUEUE_REFRESH_ES_API,
                $originId, RenovateService::INTERFACE_TYPE_MATCH, "", $matchId),
            "type"     => '',
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($taskInfo, 1);
        return true;
    }

    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}