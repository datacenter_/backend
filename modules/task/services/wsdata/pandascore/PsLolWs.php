<?php

namespace app\modules\task\services\wsdata\pandascore;
use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtLol;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\metadata\models\MetadataLolMap;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\metadata\services\MetadataService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\MatchSocketData;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class PsLolWs extends HotBase
{
    private static $instance;
    public $battle_order_relation_id = [];
    public $matchId = null;
    public $rel_matchId = null;
    public $map = null;
    public $matchInfo = null;
    public $matchRealTimeInfo = null;
    public $rel_order_id = null;
    public $now_battle_order = null;
    public $socket_id = null;
    public $socket_time = null;
    public $match_status = null;
    public $battleId = null;
    public $battle_order = null;
    public $team_name_id_relation = null;
    public $teams_info = null;
    public $player_name_id_relation = null;
    public $player_info = null;
    public $current = [];
    public $battles = null;
    public $memory_mode = false;
    public $is_reload = false;
    public $match_all_champion_websocket = [];
    //其它
    public $lol_champion_relation_id_list = [];
    public $lol_items_relation = [];
    public $lol_items_list = [];
    public $lol_item_unknown_id = null;
    public $lol_runes_list = [];
    public $lol_runes_relation_list = [];
    public $lol_summoner_spell_relation_id_list = [];
    public $lol_summoner_spell_list = [];
    public $enum_ingame_goal_info = [];
    public $enum_socket_game_configure = [];
    public $run_type = null;
    public $operate_type = null;

    //程序入口
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $PsLolWs = self::getInstance();
        $redis = $PsLolWs->redis;
        //内存模式
        $PsLolWs->memory_mode = true;
        //解析
        $runTag = @$tagInfo['ext2_type'];
        if ($runTag == 'refresh') {
            $info = @json_decode($taskInfo['params'], true);
            $rel_match_id = @$info['rel_match_id'];
            $match_id = @$info['match_id'];
            $origin_id = @$info['origin_id'];
            $PsLolWs->operate_type = @$info['operate_type'];
            $PsLolWs->delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id);
            //查询所有
            $match_list = MatchLived::find()->where(['match_id'=>$rel_match_id])->asArray()->all();
            if ($match_list){
                foreach ($match_list as $key => $value){
                    $PsLolWs->runWithInfo($redis,$value,'refresh');
                }
            }
            $res = 'success';
        } else {
            $res = $PsLolWs->runWithInfo($redis, $taskInfo,'socket');
        }
        return $res;
    }

    public function runWithInfo($redis,$taskInfo,$run_type)
    {
        $this->run_type = $run_type;
        $conversionRes = null;
        $gameId = 2;
        if ($run_type == 'socket'){
            $info = @json_decode($taskInfo['params'], true);
            $match_data = $info['match_data'];
            $socket_id = (Int)$info['socket_id'];
        }else{
            $info = $taskInfo;
            $match_data = @json_decode($taskInfo['match_data'], true);
            $socket_id = (Int)$info['id'];
        }
        $originId = (Int)@$info['origin_id'];
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        //判断是不是空
        if (empty($match_data)){
            return [];
        }
        //判断是不是 hello
        if (isset($match_data['type']) && $match_data['type'] == 'hello'){
            return 'hello';
        }
        $rel_matchId = $info['match_id'];
        $this->rel_matchId = $rel_matchId;
        $matchId = $this->matchId;
        if (empty($matchId)){
            $matchId = $this->getMatchIdAndRelMatchId($redis,$originId,$rel_matchId);
        }
        //判断有没有数据
        if (empty($matchId)){
            return 'no-match';
        }
        $this->socket_id = $socket_id;
        $this->socket_time = $socket_time;
        //获取比赛数据
        $matchInfo = $this->getMatchInfo($redis,$matchId,$socket_time);
        if ($matchInfo == 'no_match_info') {
            return 'no_match_info';
        }
        //更改比赛状态 为进行中
        if ($this->operate_type != 'xiufu') {
            $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
            if ($match_status_check == 3) {
                return 'match is over';
            }
            $this->redis_hSet_And_Memory($redis, ['match_real_time_info'], 'status',2);
            //$this->setMatchStatus($redis, $matchId);
        }
        //基础redisName
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId.":current:";
        $rel_battleId = $match_data['game']['id'];
        //获取battle和order
        $battleId = $battle_order = $match_start = null;
        $battleInfoMemory = $this->battle_order_relation_id[$rel_battleId];
        if ($battleInfoMemory){
            $battle_res_explode = explode('-',$battleInfoMemory);
            $battleId = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }
        if (empty($battleId) || empty($battle_order)) {
            //找到对应的平台的match_id
            $isSetBattleStartTime = true;
            $battle_Info = self::getbattleIdByMatchIdAndOrderNew($redis,$redis_base_name,$matchId,$rel_matchId,$rel_battleId,$socket_time,$match_data['ingame_timestamp'],$isSetBattleStartTime);
            if (empty($battle_Info['battle_id'])){
                return 'no-battleId';
            }
            $battleId = $battle_Info['battle_id'];
            $battle_order = $battle_Info['battle_order'];
            $match_start = $battle_Info['match_start'];
            $this->battle_order_relation_id[$rel_battleId] = $battleId.'-'.$battle_order;
        }
        //比赛开始 更改比赛的实际开始时间
        if ($match_start == 1){
            if ($this->operate_type != 'xiufu') {
                $match_real_time_info['begin_at'] = $socket_time;
                $this->redis_hMSet_And_Memory($redis, ['match_real_time_info'], $match_real_time_info);
            }
        }
        $is_battle_finished = false;
        $event_type_type = null;
        //判断类型
        $type = $info['type'];//frames event
        if ($type == 'no') {//如果是frames 就判断创建battle_start事件
            $is_have_ws_data = $this->redis_Get_Or_Memory($redis,['current', 'battles', $battleId, 'is_have_ws_data']);
            if (!$is_have_ws_data) {
                $this->conversionBattleStart($redis,$redis_base_name,$matchInfo,$rel_matchId,$matchId,$battleId,$battle_order,$match_data,$socket_id);
                $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'is_have_ws_data'],1);
            }
        }
        //设置 socket录入服务器配置
        $this->setSocketGameConfigure('lol');
        //处理type
        switch ($type)   //websocket类型
        {
            case "no":
                $conversionRes = $this->transFramesWs($redis,$battleId,$match_data,$matchInfo,$socket_time);
                $event_type_type = 'frames';
            break;
            case "events":
                $rel_events_id = $match_data['id'];
                //查询时间是否已经处理过了
                $rel_event = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rel_events_list'],$rel_events_id);
                if ($rel_event){
                    return $battleId.'-events-id-already-have:'.$rel_events_id;
                }
                //保存
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rel_events_list'],$rel_events_id,1);
                $conversionRes = $this->transEventsWs($redis,$matchId,$battleId,$match_data,$socket_id);
                $event_type_type = 'events';
            break;
        }
        if (empty($conversionRes)){
            return $battleId.'no-data';
        }
        $event_order = 0;
        if ($event_type_type == 'frames'){
            //判断是不是暂停
            $battle_is_pause_data = $match_data['paused'];
            $battle_is_pause_old = false;
            $battle_is_pause = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'detail'],'is_pause');
            if ($battle_is_pause == 1){
                $battle_is_pause_old = true;
            }
            if ($battle_is_pause_old != $battle_is_pause_data){
                $this->conversionBattlePause($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$battle_is_pause_data,$match_data['current_timestamp'],$socket_id);
            }

            $is_battle_finished = $conversionRes['is_battle_end'];
            if (!$is_battle_finished){
                $now_time = time();
                $out_frames_time_before = $this->redis_Get_Or_Memory($redis,['out_frames_time']);
                if ($out_frames_time_before){
                    $out_frames_time_cha = $now_time - $out_frames_time_before;
                    if ($out_frames_time_cha < 1){
                        return 'no-handle';
                    }
                }
                $this->redis_Set_And_Memory($redis,['out_frames_time'],$now_time);
            }else{
                $this->conversionBattleEnd($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$conversionRes['trans_data'],$socket_id);
                //给redis 设置有效期
                $redis->expire($redis_base_name."battles:".$battleId.":ws_out_put_data",86400);
                $redis->expire($redis_base_name."battles:".$battleId.":event_list",86400);
            }
            $socket_type = 'frames';
            $event_type = 'frames';
            $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
            $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
            $number_of_games = $matchInfo['number_of_games'];
            //获取地图
            $map = $this->getMapInfoByRedisOrMemory($redis,1);
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'game_rules' => (String)$game_rules,
                'match_type' => (String)$match_type,
                'number_of_games' => (Int)$number_of_games,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'map' => $map
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$conversionRes['trans_data']);
        }else{
            $socket_type = 'events';
            $event_type = $conversionRes['event_data']['event_type'];
            $event_order = $conversionRes['event_order'];
            //事件的时间 单位 秒
            $ingame_timestamp = $match_data['ingame_timestamp'];
            $event_data_ws_common = [
                'match_id' => (Int)$matchId,
                'battle_id' => (Int)$battleId,
                'battle_order' => (Int)$battle_order,
                'ingame_timestamp' => (Int)$ingame_timestamp,
                'event_id' => (Int)$socket_id
            ];
            $event_data_ws_res = @array_merge($event_data_ws_common,$conversionRes['event_data']);
        }
        //最后保存
        if ($event_data_ws_res){
            $this->saveWsData($redis,$redis_base_name,3,2,$socket_type,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_data_ws_res,$event_order);
        }
        return $battleId;
    }
    public function transFramesWs($redis,$battleId,$match_data,$matchInfo,$socket_time)
    {
        $matchId = (Int)$matchInfo['id'];
        //获取游戏版本
        $game_version = $this->getGameVersion($redis,$matchId);
        $server_config = [
            'version' => $game_version
        ];
        $blue = $red = [];
        //定义building_status
        $turrets = [
            'top_outer_turret'=> null,
            'top_inner_turret'=> null,
            'top_inhibitor_turret'=> null,
            'mid_outer_turret'=> null,
            'mid_inner_turret'=> null,
            'mid_inhibitor_turret'=> null,
            'bot_outer_turret'=> null,
            'bot_inner_turret'=> null,
            'bot_inhibitor_turret'=> null,
            'top_nexus_turret'=> null,
            'bot_nexus_turret'=> null
        ];
        $inhibitors = [
            'top_inhibitor'=> null,
            'mid_inhibitor'=> null,
            'bot_inhibitor'=> null
        ];
        $building_status = [
            'turrets' => $turrets,
            'inhibitors' => $inhibitors,
            'nexus' => null
        ];
        $duration = $match_data['current_timestamp'];
        $factions = [];
        //获取所有的item
        $lolItemsList = $this->getLolItemsList($redis,3);
        $lolItemsRelationList = $this->getLolItemsRelationList($redis,3);
        //获取所有的召唤师技能
        $summonerSpellInfoList = $this->getSummonerSpellInfoList($redis,3);
        $summonerSpellRelationIdList = $this->getSummonerSpellRelationIdList($redis,3);
        $websocket_team_players = [];
        $winner_teams_info_res = [];
        $winner_teams_socket_info = [];
        $team_info_new = [];
        $player_array_new = [];
        $match_scores = [];
        $match_score_info = [];
        //获取原来team的数据
        $battleTeamInfo_old = $this->getBattleTeamsInfoFromMemory($redis,$battleId,'all_info');
        //获取原来的player数据
        $battlePlayersInfo_old = $this->redis_hGetPlayersAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        $ban_pick_array = [];
        $ban_pick_is_have = false;
        $ban_pick_info = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_info){
            $ban_pick_is_have = true;
        }
        //组合数据
        $rel_teamsInfo = [
            'blue' => $match_data['blue'],
            'red' => $match_data['red'],
        ];
        $player_order = 1;
        foreach ($rel_teamsInfo as $key_faction => $team_val){
            $rel_team_id = $team_val['id'];
            $team_info_old = @$battleTeamInfo_old[$rel_team_id];
            if (empty($team_info_old)){
                $team_id = self::getMainIdByRelIdentityId('team',$rel_team_id,3);
                $team_order = self::getTeamOrderByTeamId($team_id,$matchInfo);
                $dragon_kills_detail = null;
            }else{
                $team_id = $team_info_old['team_id'];
                $team_order = $team_info_old['order'];
                $dragon_kills_detail = $team_info_old['dragon_kills_detail'];
            }
            $team_faction = $key_faction;
            $team_kills = $team_val['kills'];
            $team_gold = (Int)$team_val['gold'];
            $team_info = [
                'battle_id' => $battleId,
                'order' => $team_order,
                'score' => 0,
                'identity_id' => $rel_team_id,
                'rel_identity_id' => $rel_team_id,
                'team_id' => $team_id,
                'faction' => $team_faction,
                'kills' => $team_kills,
                'wards_purchased' => null,
                'wards_placed' => null,
                'wards_kills' => null,
                'gold' => $team_gold,
                'gold_diff' => 0,
                'experience' => null,
                'experience_diff' => null,
                'turret_kills' => $team_val['towers'],
                'inhibitor_kills' => $team_val['inhibitors'],
                'rift_herald_kills' => $team_val['herald'],
                'dragon_kills' => $team_val['drakes'],
                'dragon_kills_detail' => $dragon_kills_detail,
                'baron_nashor_kills' => $team_val['nashors'],
                'building_status' => $building_status,
                'match_score' => $team_val['score'],
            ];
            $match_score_info[$team_order] = $team_val['score'];
            //初始化队伍一些计算信息
            $team_deaths = $team_assists = 0;
            //循环玩家
            foreach ($team_val['players'] as $player_key => $player_item){
                $player_info = [];
                $player_nick_name = $rel_player_nick_name = $player_item['name'];
                $rel_player_id = $player_item['id'];
                $player_old_res = @$battlePlayersInfo_old[$rel_player_id];
                $old_player = @json_decode($player_old_res,true);
                if (empty($old_player)){
                    $old_player = [];
                    $player_id = self::getMainIdByRelIdentityId('player',$rel_player_id,3);
                    if (empty($player_id)){
                        $player_id = null;
                    }else{
                        $player_id = (Int)$player_id;
                        $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                        $player_nick_name = @$Player_Info['nick_name'];
                    }
                }else{
                    $player_id = $old_player['player_id'];
                    $player_nick_name = $old_player['nick_name'];
                }
                //玩家信息
                $player_seed = Common::getLolPositionByString('pandascore',$player_key);
                $player_role = $player_seed;
                $player_lane = Common::getLolLaneId('pandascore',$player_seed);
                $player_info = [
                    'order' => $player_order,
                    'rel_identity_id' => $rel_player_id,
                    'rel_team_id' => $rel_team_id,
                    'team_order' => $team_order,
                    'seed' => $player_seed,
                    'battle_id' => $battleId,
                    'game' => 2,
                    'match' => $matchId,
                    'team_id' => $team_id,
                    'faction' => $team_faction,
                    'role' => $player_role,
                    'lane' => $player_lane,
                    'player_id' => $player_id,
                    'nick_name' => $player_nick_name,
                    'rel_nick_name' => $rel_player_nick_name,
                    'level' => $player_item['level'],
                    'alive' => $player_item['hp'] > 0 ? 1:2,
                    'ultimate_cd' => null,
                    'coordinate' => null,
                    'kills' => $player_item['kills'],
                    'double_kill' => null,
                    'triple_kill' => null,
                    'quadra_kill' => null,
                    'penta_kill' => null,
                    'largest_multi_kill' => null,
                    'largest_killing_spree' => null,
                    'deaths' => $player_item['deaths'],
                    'assists' => $player_item['assists'],
                    'cs' => $player_item['cs'],
                ];
                //判断玩家是否有英雄
                if (empty($old_player['champion'])){
                    $player_info['champion'] = $this->getLolChampionId($redis,3,$player_item['champion']['id']);
                }
                //计算选手信息
                $player_deaths = $player_info['deaths'] ? $player_info['deaths'] : 1;
                $player_info['kda'] = (String)round((($player_info['kills'] + $player_info['assists'])/$player_deaths),2);
                $player_info['participation'] = (String)round((($player_info['kills'] + $player_info['assists'])/$team_kills),4);
                $player_info['cspm'] = (String)round(($player_info['cs'] / ( $duration / 60 )),2);
                $player_items = $this->conversionPandascorePlayerItemsIds($redis,$battleId,$duration,$rel_player_id,$player_item['items'],$lolItemsList,$lolItemsRelationList);
                $player_info['items'] = @$player_items['items'];
                $player_info['items_timeline'] = @$player_items['items_timeline'];
                $player_info['summoner_spells'] = $this->conversionPandascorePlayerSummonerSpellsIds($redis,$player_item['summoner_spells'],$summonerSpellRelationIdList);
                //选手的一些击杀数据
                if (empty(@$old_player)){
                    $turret_kills = 0;
                    $inhibitor_kills = 0;
                    $rift_herald_kills = 0;
                    $dragon_kills = 0;
                    $baron_nashor_kills = 0;
                }else{
                    $turret_kills = @$old_player['turret_kills'];
                    $inhibitor_kills = @$old_player['inhibitor_kills'];
                    $rift_herald_kills = @$old_player['rift_herald_kills'];
                    $dragon_kills = @$old_player['dragon_kills'];
                    $baron_nashor_kills = @$old_player['baron_nashor_kills'];
                }
                $player_info['turret_kills'] =$turret_kills;
                $player_info['inhibitor_kills'] =$inhibitor_kills;
                $player_info['rift_herald_kills'] =$rift_herald_kills;
                $player_info['dragon_kills'] =$dragon_kills;
                $player_info['baron_nashor_kills'] =$baron_nashor_kills;
                //组合玩家的数据
                $player_new_array_res = array_merge($old_player,$player_info);
                $player_array_new[$rel_player_id] = $player_new_array_res;
                //计算队伍的信息
                $team_deaths += $player_item['deaths'];
                $team_assists += $player_item['assists'];
                //ban pick
                if (!$ban_pick_is_have){
                    $team_ban_pick = [
                        "hero" => @$player_new_array_res['champion'],
                        "team" => (Int)$team_id,
                        "type" => 2,
                        "order" => null
                    ];
                    $ban_pick_array[$player_order] = @json_encode($team_ban_pick,320);
                }
                //整体的玩家order
                $player_order ++;
            }
            //玩家的结果 到team
            $team_info['deaths'] = $team_deaths;
            $team_info['assists'] = $team_assists;
            //设置team信息
            $team_info_new[$team_faction] = $team_info;
            $winner_teams_info_res[$rel_team_id] = $team_order;
        }
        //保存ban_pick
        if (!empty($ban_pick_array) && !$ban_pick_is_have){
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_array);
        }
        //计算队伍的其他一些信息
        $team_gold_diff = $team_info_new['blue']['gold'] - $team_info_new['red']['gold'];
        $team_info_new['blue']['gold_diff'] = $team_gold_diff;
        $team_info_new['red']['gold_diff'] = -$team_gold_diff;
        //首先获取时间是否可以保存了
        $gold_or_experience_diff_time = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff']);
        $gold_diff_time_cha = $duration - $gold_or_experience_diff_time;
        //间隔60秒保存
        if ($gold_diff_time_cha >= 30){
            //gold_diff
            $gold_diff_timeline_now[] = [
                'ingame_timestamp' => $duration,
                'gold_diff' => $team_gold_diff
            ];
            //获取原来的信息
            $gold_diff_timeline_redis = $this->redis_Get_Or_Memory($redis,['current','battles',$battleId,'gold_diff_timeline']);
            if ($gold_diff_timeline_redis){
                $gold_diff_timeline_old = @json_decode($gold_diff_timeline_redis,true);
                $gold_diff_timeline_new = @array_merge($gold_diff_timeline_old,$gold_diff_timeline_now);
            }else{
                $gold_diff_timeline_new = $gold_diff_timeline_now;
            }
            $gold_diff_timeline_res = @json_encode($gold_diff_timeline_new,320);
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_diff_timeline'],$gold_diff_timeline_res);
            //设置时间间隔
            $this->redis_Set_And_Memory($redis,['current','battles',$battleId,'gold_or_experience_diff'],$duration);
        }
        //组合最新的数据
        $team_info_res[$team_info_new['blue']['rel_identity_id']] = $team_info_new['blue'];
        $team_info_res[$team_info_new['red']['rel_identity_id']] = $team_info_new['red'];
        //组合websocket数据
        $playerResVal = [];
        foreach ($player_array_new as $player_key => $player_res) {
            //组合websocket
            $websocket_player = [];
            $player_seed_id = (int)$player_res['seed'];
            $websocket_player['seed'] = $player_seed_id;
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = @Common::getLolRoleStringByRoleId('pandascore',$player_res['role']);
            $websocket_player['lane'] = @Common::getLolLaneStringByLaneId('pandascore',$player_res['lane']);
            $websocket_player['player']['player_id'] = self::checkAndChangeInt($player_res['player_id']);
            $websocket_player['player']['nick_name'] = self::checkAndChangeString($player_res['nick_name']);
            //获取英雄信息
            $champion = $this->getChampionInfoByRedisOrMemory($redis,3,$player_res['champion']);
            $websocket_player['champion'] = $champion;
            $websocket_player['level'] = (int)$player_res['level'];
            $websocket_player['is_alive'] = $player_res['alive'] == 1 ? true : false;
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['cs'] = (Int)$player_res['cs'];
            $websocket_player['cspm'] = (String)$player_res['cspm'];
            //summoner_spells
            $websocket_player['summoner_spells'] = $this->getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_res['summoner_spells']);
            //道具
            $websocket_player['items'] = $this->getWebSocketPlayerItemsList($lolItemsList,$player_res['items']);
            $advanced = [
                'keystone' => null,
                'ultimate_cd' => null,
                'coordinate' => null,
                'respawntimer' => null,
                'health' => null,
                'health_max' => null,
                'turret_kills' => (Int)$player_res['turret_kills'],
                'inhibitor_kills' => (Int)$player_res['inhibitor_kills'],
                'rift_herald_kills' => (Int)$player_res['rift_herald_kills'],
                'dragon_kills' => (Int)$player_res['dragon_kills'],
                'baron_nashor_kills' => (Int)$player_res['baron_nashor_kills'],
                'double_kill' => null,
                'triple_kill' => null,
                'quadra_kill' => null,
                'penta_kill' => null,
                'largest_multi_kill' => null,
                'largest_killing_spree' => null,
                'minion_kills' => null,
                'total_neutral_minion_kills' => null,
                'neutral_minion_team_jungle_kills' => null,
                'neutral_minion_enemy_jungle_kills' => null,
                'gold_earned' => null,
                'gold_spent' => null,
                'gold_remaining' => null,
                'gpm' => null,
                'gold_earned_percent' => null,
                'experience' => null,
                'xpm' => null,
                'damage_to_champions' => null,
                'damage_to_champions_physical' => null,
                'damage_to_champions_magic' => null,
                'damage_to_champions_true' => null,
                'dpm_to_champions' => null,
                'damage_percent_to_champions' => null,
                'total_damage' => null,
                'total_damage_physical' => null,
                'total_damage_magic' => null,
                'total_damagel_true' => null,
                'damage_taken' => null,
                'damage_taken_physical' => null,
                'damage_taken_magic' => null,
                'damage_taken_true' => null,
                'dtpm' => null,
                'damage_taken_percent' => null,
                'damage_conversion_rate' => null,
                'damage_selfmitigated' => null,
                'damage_shielded_on_teammates' => null,
                'damage_to_buildings' => null,
                'damage_to_towers' => null,
                'damage_to_objectives' => null,
                'total_crowd_control_time' => null,
                'total_crowd_control_time_others' => null,
                'total_heal' => null,
                'wards_purchased' => null,
                'wards_placed' => null,
                'wards_kills' => null,
                'vision_score' => null
            ];
            $websocket_player['advanced'] = $advanced;
            $websocket_team_players[$player_res['team_order']][$player_seed_id] = $websocket_player;
            //组成最终player数组
            $playerResVal[$player_key] = @json_encode($player_res,320);
        }
        //最终保存player
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerResVal);
        //获取一些信息
        //获取龙信息
        $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
        $teamResVal = [];
        //team新信息
        foreach ($team_info_res as $team_key => $team_info_item){
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_item['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info_item['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt(@$team_info_item['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            //处理龙的信息
            $dragon_kills_detail_res = $this->conversionTeamDragonKills($drakeInfoList,$team_info_item['dragon_kills_detail']);
            //advanced
            $team_advanced = [
                'wards_purchased' => null,
                'wards_placed' => null,
                'wards_kills' => null
            ];
            $player_res_data = $websocket_team_players[$team_opponent_order];
            ksort($player_res_data);
            $player_res_data_ws = @array_values($player_res_data);
            //factions team的信息
            $faction_team = [
                'faction' => (String)$team_info_item['faction'],
                'team' => $teamInfo,
                'kills' => (int)$team_info_item['kills'],
                'deaths' => (int)@$team_info_item['deaths'],
                'assists' => (int)@$team_info_item['assists'],
                'gold' => (int)@$team_info_item['gold'],
                'gold_diff' => (int)@$team_info_item['gold_diff'],
                'experience' => null,
                'experience_diff' => null,
                'turret_kills' => (int)@$team_info_item['turret_kills'],
                'inhibitor_kills' => (int)@$team_info_item['inhibitor_kills'],
                'rift_herald_kills' => (int)@$team_info_item['rift_herald_kills'],
                'dragon_kills' => (int)@$team_info_item['dragon_kills'],
                'dragon_kills_detail' => $dragon_kills_detail_res,
                'baron_nashor_kills' => (int)@$team_info_item['baron_nashor_kills'],
                'building_status' => $team_info_item['building_status'],
                'players' => $player_res_data_ws,
                'advanced' => $team_advanced
            ];
            $winner_teams_socket_info[$team_opponent_order] = $teamInfo;
            $match_scores[$team_opponent_order] = $teamInfo;
            $match_scores[$team_opponent_order]['score'] = $team_info_item['match_score'];
            $factions[] = $faction_team;
        }
        //battle 是否结束
        $is_battle_finished = false;
        $is_match_finished = false;
        $match_winner = null;
        $battle_winner = null;
        if ($match_data['game']['finished'] && !empty($match_data['game']['winner_id'])){
            $is_battle_finished = true;
            $battle_winner_id = $match_data['game']['winner_id'];
            $winner_id = $winner_teams_info_res[$battle_winner_id];
            $battle_winner = $winner_teams_socket_info[$winner_id];
            $match_real_time_info = $this->redis_hGetAll_Or_Memory($redis,['match_real_time_info']);
            //上面计算
            switch ($winner_id){
                case 1:
                    $match_real_time_info['team_1_score'] = $match_score_info[1] + 1;
                    $match_scores[1]['score'] = $match_scores[1]['score'] + 1;
                    break;
                case 2:
                    $match_real_time_info['team_2_score'] = $match_score_info[2] + 1;
                    $match_scores[2]['score'] = $match_scores[2]['score'] + 1;
                    break;
            }
            //battle的分数
            $team_info_res[$battle_winner_id]['score'] = 1;
            //计算结束时间
            $battle_base = [
                'duration'=>$duration,
                'end_at' => $socket_time,
                'status' => 3,
                'winner' => $winner_id
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_base);
            //计算比赛的胜算
            $statusInfo = WinnerHelper::lolWinnerInfo($match_real_time_info['team_1_score'],$match_real_time_info['team_2_score'],$matchInfo['number_of_games']);
            if($statusInfo['is_finish']==1){
                $is_match_finished = true;
                $match_real_time_info['status'] = 3;
                $match_real_time_info['end_at']= $socket_time;
                if ($statusInfo['is_draw'] == 1){//判断是不是平局
                    $match_real_time_info['winner'] = null;
                    $match_real_time_info['is_draw'] = 1;
                }else{
                    $match_real_time_info['winner'] = $statusInfo['winner_team'];
                    $match_winner = $winner_teams_socket_info[$statusInfo['winner_team']];
                }
            }
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
        }else{
            $base_data = [
                'duration'=>$duration
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$base_data);
        }
        $team_elites_Kills = [];
        //循环$team_info_res
        foreach ($team_info_res as $team_index => $team_val){
            //杀龙信息
            $team_elites_Kills[$team_index]['dragon_kills'] = (int)@$team_val['dragon_kills'];
            //组合队伍加密
            $teamResVal[$team_index] = @json_encode($team_val,320);
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$teamResVal);
        //计算 elites_status
        //精英怪状态
        $elites_status = $this->getElitesStatusNew($redis,$battleId,$team_elites_Kills,$duration);
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'elites_status',@json_encode($elites_status,320));
        $match_scores_res = array_values($match_scores);
        //组合数据
        $transData = [
            'duration' => (Int)$duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores_res,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => self::checkAndChangeBoolean($match_data['paused']),
            'is_live' => true,
            'elites_status' => $elites_status,
            'factions' =>$factions,
            'server_config' =>$server_config
        ];
        //$trans_data = json_encode($transData,320);
        $frames_data = [
            'trans_data' => $transData,
            'is_battle_end' => $is_battle_finished
        ];
        return $frames_data;
    }

    public function transEventsWs($redis,$matchId,$battleId,$match_data,$socket_id)
    {
        $handle_data = null;
        $event_type = null;
        $position = null;
        $ingame_timestamp = (Int)$match_data['ingame_timestamp'];
        $data_payload = $match_data['payload'];
        $payload_type = $match_data['payload']['type'];
        switch ($payload_type){
            case 'suicide':
                $handle_data = $this->conversionPlayerSuicide($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload);
                break;
            case 'player':
                $handle_data = $this->conversionPlayerKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload);
                break;
            case 'rift_herald':
                $handle_data = $this->conversionDragonKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload,'rift_herald');
                break;
            case 'drake':
                $handle_data = $this->conversionDragonKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload,'drake');
                break;
            case 'baron_nashor':
                $handle_data = $this->conversionDragonKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload,'baron_nashor');
                break;
            case 'tower':
                $handle_data = $this->conversionBuildingKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload);
                break;
            case 'inhibitor':
                $handle_data = $this->conversionBuildingKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload);
                break;
            case 'nexus':
                $handle_data = $this->conversionBuildingKill($redis,$matchId,$battleId,$socket_id,$ingame_timestamp,$data_payload);
                break;
        }
        return $handle_data;
    }
    ///////////////// 处理事件 ///////////
    //处理杀人事件
    private function conversionPlayerKill($redis,$matchId,$battleId,$event_id,$ingame_timestamp,$data_payload){
        $event_type = 'player_kill';
        $position = null;
        $first_event_team_order = null;
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $rel_killer_id = $data_payload['killer']['object']['id'];
        $killer_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id);
        if ($killer_player_info_redis){
            $killer_player_info = @json_decode($killer_player_info_redis,true);
            $killer = @$killer_player_info['player_id'];
            $killer_player_name = @$killer_player_info['nick_name'];
            $killer_player_rel_name = @$killer_player_info['rel_nick_name'];
            $killer_champion_id = @$killer_player_info['champion'];
            $killer_faction = @$killer_player_info['faction'];
            $first_event_team_order = @$killer_player_info['team_order'];
        }
        //$websocket_killer
        //获取killer英雄信息
        $killer_championRes = $this->getChampionInfoByRedisOrMemory($redis,3,$killer_champion_id);
        //killer
        $websocket_killer = [
            'player_id' => self::checkAndChangeInt($killer),
            'nick_name' => self::checkAndChangeString($killer_player_name),
            'faction' => self::checkAndChangeString($killer_faction),
            'champion' => $killer_championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //victim
        $victim = $victim_player_name = $victim_player_rel_name = $victim_champion_id = $victim_faction = null;
        $rel_victim_id = $data_payload['killed']['object']['id'];
        $victim_player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_victim_id);
        if ($victim_player_info_redis){
            $victim_player_info = @json_decode($victim_player_info_redis,true);
            $victim = @$victim_player_info['player_id'];
            $victim_player_name = @$victim_player_info['nick_name'];
            $victim_player_rel_name = @$victim_player_info['rel_nick_name'];
            $victim_champion_id = @$victim_player_info['champion'];
            $victim_faction = @$victim_player_info['faction'];
        }
        //$websocket_victim
        //获取victim英雄信息
        $victim_championRes = $this->getChampionInfoByRedisOrMemory($redis,3,$victim_champion_id);
        $websocket_victim = [
            'player_id' => (Int)$victim,
            'nick_name' => self::checkAndChangeString($victim_player_name),
            'faction' => self::checkAndChangeString($victim_faction),
            'champion' => $victim_championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //$websocket_assist
        $websocket_assist = null;
        //获取第一次事件
        $killed_type = 'player';
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $is_first_event = false;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event) {
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
            //保存一血 五杀 十杀
            if ($first_event_type == 'first_blood') {
                $first_blood_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_blood_p_tid' => $first_event_team_order,
                    'first_blood_time' => $ingame_timestamp,
                    'first_blood_detail' => @json_encode($first_blood_detail, 320)
                ];
                $this->redis_hMSet_And_Memory(
                    $redis,
                    ['current', 'battles', $battleId, 'detail'],
                    $match_battle_ext_lol
                );
            }
            if ($first_event_type == 'first_to_5_kills') {
                $first_to_5_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_to_5_kills_p_tid' => $first_event_team_order,
                    'first_to_5_kills_time' => $ingame_timestamp,
                    'first_to_5_detail' => @json_encode($first_to_5_detail, 320)
                ];
                $this->redis_hMSet_And_Memory(
                    $redis,
                    ['current', 'battles', $battleId, 'detail'],
                    $match_battle_ext_lol
                );
            }
            if ($first_event_type == 'first_to_10_kills') {
                $first_to_10_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_to_10_kills_p_tid' => $first_event_team_order,
                    'first_to_10_kills_time' => $ingame_timestamp,
                    'first_to_10_detail' => @json_encode($first_to_10_detail, 320)
                ];
                $this->redis_hMSet_And_Memory(
                    $redis,
                    ['current', 'battles', $battleId, 'detail'],
                    $match_battle_ext_lol
                );
            }
        }
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => 50,
            'killer_sub_type' => 50,
            'victim' => $victim,
            'victim_player_name' => $victim_player_name,
            'victim_player_rel_name' => $victim_player_rel_name,
            'victim_champion_id' => $victim_champion_id,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => 50,
            'victim_sub_type' => 50,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理自杀事件
    private function conversionPlayerSuicide($redis,$matchId,$battleId,$event_id,$ingame_timestamp,$data_payload){
        $event_type = 'player_suicide';
        $position = null;
        $websocket_killer = null;
        $enumIngameGoalInfoList = $this->getEnumIngameGoalInfo($redis);
        $killer = 25;
        $killer_type_check = null;
        $killer_type = $data_payload['killer']['type'];
        $killer_sub_type = @$data_payload['killer']['object']['type'];
        $killer_faction = @$data_payload['killer']['object']['team'];
        $killer_player_rel_name = @$data_payload['killer']['object']['name'];
        switch ($killer_type){
            case 'rift_herald':
                $killer = @Common::getEliteId('rift_herald');
                $killer_type_check = 'elite';
                break;
            case 'drake':
                switch ($killer_sub_type){
                    case 'cloud':
                        $killer = @Common::getEliteId('elite_cloud');
                        break;
                    case 'mountain':
                        $killer = @Common::getEliteId('elite_mountain');
                        break;
                    case 'ocean':
                        $killer = @Common::getEliteId('elite_ocean');
                        break;
                    case 'infernal':
                        $killer = @Common::getEliteId('elite_infernal');
                        break;
                    case 'elder':
                        $killer = @Common::getEliteId('elder_dragon');
                        break;
                }
                $killer_type_check = 'elite';
                break;
            case 'baron_nashor':
                $killer = @Common::getEliteId('baron_nashor');
                $killer_type_check = 'elite';
                break;
            case 'minion':
                $killer = @Common::getEliteId('minion');
                $killer_type_check = 'minion';
                break;
            case 'neutral_minion':
                $killer = @Common::getEliteId('neutral_minion');
                $killer_type_check = 'neutral_minion';
                break;
            case 'tower':
                $killer = 21;
                $killer_type_check = 'building';
                break;
            case 'inhibitor':
                $killer = 25;
                $killer_type_check = 'building';
                break;
            case 'nexus':
                $killer = 26;
                $killer_type_check = 'building';
                break;
            case 'unknown':
                $killer = 25;
                $killer_type_check = 'unknown';
                break;
        }
        // websocket_killer
        switch ($killer_type_check){
            case 'unknown':
            case 'elite':
            case 'neutral_minion':
                if ($killer){
                    $websocket_killerInfoRedis = $enumIngameGoalInfoList[$killer];
                    $websocket_killerInfo = @json_decode($websocket_killerInfoRedis,true);
                    $ingame_obj_name = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name']);
                    $ingame_obj_name_cn = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name_cn']);
                    $ingame_obj_type = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_type']);
                    $ingame_obj_sub_type = self::checkAndChangeString(@$websocket_killerInfo['sub_type']);
                    $websocket_killer = [
                        'ingame_obj_name' => $ingame_obj_name,
                        'ingame_obj_name_cn' => $ingame_obj_name_cn,
                        'ingame_obj_type' => $ingame_obj_type,
                        'ingame_obj_sub_type' => $ingame_obj_sub_type
                    ];
                }
                break;
            case 'minion':
                if ($killer){
                    $websocket_killerInfoRedis = $enumIngameGoalInfoList[$killer];
                    $websocket_killerInfo = @json_decode($websocket_killerInfoRedis,true);
                    $ingame_obj_name = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name']);
                    $ingame_obj_name_cn = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name_cn']);
                    $ingame_obj_type = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_type']);
                    $ingame_obj_sub_type = self::checkAndChangeString(@$websocket_killerInfo['sub_type']);
                    $websocket_killer = [
                        'ingame_obj_name' => $ingame_obj_name,
                        'ingame_obj_name_cn' => $ingame_obj_name_cn,
                        'faction' => $killer_faction,
                        'ingame_obj_type' => $ingame_obj_type,
                        'ingame_obj_sub_type' => $ingame_obj_sub_type
                    ];
                }
                break;
            case 'building':
                if ($killer){
                    $websocket_killerInfoRedis = $enumIngameGoalInfoList[$killer];
                    $websocket_killerInfo = @json_decode($websocket_killerInfoRedis,true);
                    $ingame_obj_name = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name']);
                    $ingame_obj_name_cn = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_name_cn']);
                    $ingame_obj_type = self::checkAndChangeString(@$websocket_killerInfo['ingame_obj_type']);
                    $ingame_obj_sub_type = self::checkAndChangeString(@$websocket_killerInfo['sub_type']);
                    $websocket_killer = [
                        'lane' => null,
                        'ingame_obj_name' => $ingame_obj_name,
                        'ingame_obj_name_cn' => $ingame_obj_name_cn,
                        'faction' => $killer_faction,
                        'ingame_obj_type' => $ingame_obj_type,
                        'ingame_obj_sub_type' => $ingame_obj_sub_type
                    ];
                }
                break;
        }
        //$victim
        $victim = $victim_player_name = $victim_player_rel_name = $victim_champion_id = $victim_faction = null;
        $rel_victim_id = $data_payload['killed']['object']['id'];
        //查询
        $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_victim_id);
        if ($player_info_redis){
            $player_info = @json_decode($player_info_redis,true);
            $victim = @$player_info['player_id'];
            $victim_player_name = @$player_info['nick_name'];
            $victim_player_rel_name = @$player_info['rel_nick_name'];
            $victim_champion_id = @$player_info['champion'];
            $victim_faction = @$player_info['faction'];
        }
        //websocket
        $victim_championRes = null;
        //获取英雄信息
        if ($victim_champion_id){
            $victim_championRes = $this->getChampionInfoByRedisOrMemory($redis,3,$victim_champion_id);
        }
        //victim
        $websocket_victim = [
            'player_id' => (Int)$victim,
            'nick_name' => self::checkAndChangeString($victim_player_name),
            'faction' => self::checkAndChangeString($victim_faction),
            'champion' => $victim_championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        $websocket_assist = null;
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => null,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => null,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer,
            'killer_sub_type' => $killer,
            'victim' => $victim,
            'victim_player_name' => $victim_player_name,
            'victim_player_rel_name' => $victim_player_rel_name,
            'victim_champion_id' => $victim_champion_id,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => 50,
            'victim_sub_type' => 50,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
//下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => false,
            'first_event_type' => null
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //处理峡谷先锋事件
    private function conversionDragonKill($redis,$matchId,$battleId,$event_id,$ingame_timestamp,$data_payload,$elite_type){
        $event_type = 'elite_kill';
        $position = null;
        $killer_player_info_array = [];
        $rel_killer_id = $rel_team_id = $first_event_team_order = null;
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = null;
        $rel_killer_type = $data_payload['killer']['type'];
        if ($rel_killer_type == 'player'){
            $rel_killer_id = $data_payload['killer']['object']['id'];
            //查询
            $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id);
            if ($player_info_redis){
                $player_info = @json_decode($player_info_redis,true);
                $killer_player_info_array = $player_info;
                $killer = @$player_info['player_id'];
                $killer_player_name = @$player_info['nick_name'];
                $killer_player_rel_name = @$player_info['rel_nick_name'];
                $killer_champion_id = @$player_info['champion'];
                $killer_faction = @$player_info['faction'];
                $first_event_team_order = @$player_info['team_order'];
                $rel_team_id = @$player_info['rel_team_id'];
            }
        }
        $drake_id = $killed_type =  null;
        $elite_sub_type = $data_payload['killed']['object']['type'];
        switch ($elite_type){
            case 'rift_herald':
                $killed_type = 'rift_herald';
                $drake_id = @Common::getEliteId('rift_herald');
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['rift_herald_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id,@json_encode($killer_player_info_array,320));
                    //记录 击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time',$ingame_timestamp);
                    $rift_herald_kill_num = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num');
                    if ($rift_herald_kill_num == 1){
                        $rift_herald_kill_num += 1;
                        $enum_socket_game_configure_res = $this->enum_socket_game_configure;
                        if ($rift_herald_kill_num == $enum_socket_game_configure_res['rift_herald_max_num']){
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',$rift_herald_kill_num);
                            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
                        }
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_num',1);
                    }
                }
                break;
            case 'drake':
                switch ($elite_sub_type){
                    case 'cloud':
                        $killed_type = 'drake';
                        $drake_id = @Common::getEliteId('elite_cloud');
                        break;
                    case 'mountain':
                        $killed_type = 'drake';
                        $drake_id = @Common::getEliteId('elite_mountain');
                        break;
                    case 'ocean':
                        $killed_type = 'drake';
                        $drake_id = @Common::getEliteId('elite_ocean');
                        break;
                    case 'infernal':
                        $killed_type = 'drake';
                        $drake_id = @Common::getEliteId('elite_infernal');
                        break;
                    case 'elder':
                        $killed_type = 'elder_dragon';
                        $drake_id = @Common::getEliteId('elder_dragon');
                        break;
                }
                //记录当前这个人的杀龙信息
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['dragon_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    if ($elite_sub_type == 'elder'){
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'elder_dragon_kill_time',$ingame_timestamp);
                    }else{
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'dragon_status'],'kill_time',$ingame_timestamp);
                    }
                }
                //设置队伍杀龙信息
                if ($drake_id && $rel_team_id){
                    //组成二维数组
                    $dragon_kills_detail_now[] = [
                        'date'=>$ingame_timestamp,
                        'drake'=>(string)$drake_id
                    ];
                    $teamInfoRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'teams'],$rel_team_id);
                    if ($teamInfoRedis) {
                        $teamInfo = @json_decode($teamInfoRedis, true);
                        $dragon_kills_detail = $teamInfo['dragon_kills_detail'];
                        if ($dragon_kills_detail){
                            $teamInfo['dragon_kills_detail'] = array_merge($dragon_kills_detail,$dragon_kills_detail_now);
                        }else{
                            $teamInfo['dragon_kills_detail'] = $dragon_kills_detail_now;
                        }
                        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'teams'],$rel_team_id,@json_encode($teamInfo,320));
                    }
                }
                break;
            case 'baron_nashor':
                $killed_type = 'baron_nashor';
                $drake_id = @Common::getEliteId('baron_nashor');
                //保存 个人击杀
                if ($killer_player_info_array){
                    (int)$killer_player_info_array['baron_nashor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id,@json_encode($killer_player_info_array,320));
                    //保存击杀时间
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time',$ingame_timestamp);
                }
                break;
        }
        $victim = $drake_id;
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //获取英雄信息
        $championRes = $this->getChampionInfoByRedisOrMemory($redis,3,$killer_champion_id);
        //killer
        $websocket_killer = [
            'player_id' => self::checkAndChangeInt($killer),
            'nick_name' => (string)$killer_player_name,
            'faction' => (string)$killer_faction,
            'champion' => $championRes,
            'ingame_obj_type' => (string)'player',
            'ingame_obj_sub_type' => (string)'player'
        ];
        //victim
        $victim_ingame_obj_name = $victim_ingame_obj_name_cn = $victim_ingame_obj_type = $victim_ingame_obj_sub_type = null;
        if ($victim){
            $drakeInfoList = $this->getEnumIngameGoalInfo($redis);
            $victimInfoRedis = $drakeInfoList[$victim];
            $victimInfo = @json_decode($victimInfoRedis,true);
            $victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        $websocket_victim = [
            'ingame_obj_name' => $victim_ingame_obj_name,
            'ingame_obj_name_cn' => $victim_ingame_obj_name_cn,
            'ingame_obj_type' => $victim_ingame_obj_type,
            'ingame_obj_sub_type' => $victim_ingame_obj_sub_type
        ];
        //$websocket_assist
        $websocket_assist = null;
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = @Common::getPandaScoreEventNum($first_event_type);
            //第一次事件 保存内容
            if ($first_event_type == 'first_rift_herald'){
                $first_rift_herald_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_rift_herald_p_tid' => $first_event_team_order,
                    'first_rift_herald_time' => $ingame_timestamp,
                    'first_rift_herald_detail' => @json_encode($first_rift_herald_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_dragon'){
                $first_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_dragon_p_tid' => $first_event_team_order,
                    'first_dragon_time' => $ingame_timestamp,
                    'first_dragon_detail' =>json_encode($first_dragon_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_baron_nashor'){
                $first_baron_nashor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_baron_nashor_p_tid' => $first_event_team_order,
                    'first_baron_nashor_time' => $ingame_timestamp,
                    'first_baron_nashor_detail' => @json_encode($first_baron_nashor_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_elder_dragon'){
                $first_elder_dragon_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_elder_dragon_p_tid' => $first_event_team_order,
                    'first_elder_dragon_time' => $ingame_timestamp,
                    'first_elder_dragon_detail' => @json_encode($first_elder_dragon_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => 50,
            'killer_sub_type' => 50,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => $victim,
            'victim_sub_type' => $victim,
            'assist' => null,
            'assist_ingame_obj_type' => null,
            'assist_sub_type' => null,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //conversionBuildingKill
    private function conversionBuildingKill($redis,$matchId,$battleId,$event_id,$ingame_timestamp,$data_payload){
        $enumIngameGoalInfoList = $this->getEnumIngameGoalInfo($redis);
        $event_type = 'building_kill';
        $position = null;
        //killer
        $is_player = false;
        $websocket_killer = null;
        $killer_player_info_array = [];
        $rel_killer_id = $rel_team_id = $first_event_team_order = null;
        $killer = $killer_player_name = $killer_player_rel_name = $killer_champion_id = $killer_faction = $killer_ingame_obj_type = $killer_sub_type = null;
        $rel_killer_type = $data_payload['killer']['type'];
        if ($rel_killer_type == 'player'){
            $is_player = true;
            $killer_ingame_obj_type = $killer_sub_type = 50;
            $rel_killer_id = $data_payload['killer']['object']['id'];
            //查询
            $player_info_redis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id);
            if ($player_info_redis){
                $player_info = @json_decode($player_info_redis,true);
                $killer_player_info_array = $player_info;
                $killer = @$player_info['player_id'];
                $killer_player_name = @$player_info['nick_name'];
                $killer_player_rel_name = @$player_info['rel_nick_name'];
                $killer_champion_id = @$player_info['champion'];
                $killer_faction = @$player_info['faction'];
                $first_event_team_order = @$player_info['team_order'];
                $rel_team_id = @$player_info['rel_team_id'];
                //获取英雄信息
                $championRes = $this->getChampionInfoByRedisOrMemory($redis,3,$killer_champion_id);
                //killer
                $websocket_killer = [
                    'player_id' => self::checkAndChangeInt($killer),
                    'nick_name' => (string)$killer_player_name,
                    'faction' => (string)$killer_faction,
                    'champion' => $championRes,
                    'ingame_obj_type' => (string)'player',
                    'ingame_obj_sub_type' => (string)'player'
                ];
            }
        }
        if ($rel_killer_type == 'minion'){
            $killer_player_rel_name = $data_payload['killer']['object']['name'];
            $killer_faction = $data_payload['killer']['object']['team'];
            $killer = $killer_ingame_obj_type = $killer_sub_type = 8;
            $killerInfo = @json_decode($enumIngameGoalInfoList[$killer],true);
            $websocket_killer = [
                'ingame_obj_name' => self::checkAndChangeString(@$killerInfo['ingame_obj_name']),
                'ingame_obj_name_cn' => self::checkAndChangeString(@$killerInfo['ingame_obj_name_cn']),
                'faction' => self::checkAndChangeString(@$killer_faction),
                'ingame_obj_type' => self::checkAndChangeString(@$killerInfo['ingame_obj_type']),
                'ingame_obj_sub_type' => self::checkAndChangeString(@$killerInfo['sub_type']),
            ];
        }
        if ($rel_killer_type == 'rift_herald'){
            $killer_player_rel_name = $data_payload['killer']['object']['name'];
            $killer_faction = null;
            $killer = $killer_ingame_obj_type = $killer_sub_type = 1;
            $websocket_killer = [
                'ingame_obj_name' => self::checkAndChangeString(@$killerInfo['ingame_obj_name']),
                'ingame_obj_name_cn' => self::checkAndChangeString(@$killerInfo['ingame_obj_name_cn']),
                'ingame_obj_type' => self::checkAndChangeString(@$killerInfo['ingame_obj_type']),
                'ingame_obj_sub_type' => self::checkAndChangeString(@$killerInfo['sub_type']),
            ];
        }
        if ($rel_killer_type == 'unknown'){
            $killer_player_rel_name = $data_payload['killer']['object']['name'];
            $killer_faction = null;
            $killer = $killer_ingame_obj_type = $killer_sub_type = 51;
            $websocket_killer = [
                'ingame_obj_name' => self::checkAndChangeString(@$killerInfo['ingame_obj_name']),
                'ingame_obj_name_cn' => self::checkAndChangeString(@$killerInfo['ingame_obj_name_cn']),
                'ingame_obj_type' => self::checkAndChangeString(@$killerInfo['ingame_obj_type']),
                'ingame_obj_sub_type' => self::checkAndChangeString(@$killerInfo['sub_type']),
            ];
        }
        //victim
        $killed_type = null;
        $victim = $victim_ingame_obj_type = $victim_sub_type = null;
        $victim_faction = @$data_payload['killed']['object']['team'];
        $rel_victim_type = $data_payload['killed']['type'];
        switch ($rel_victim_type){
            case 'tower':
                $killed_type = 'tower';
                $victim = $victim_ingame_obj_type = $victim_sub_type = 21;
                //记录当前这个人的摧毁水晶信息
                if ($is_player && $killer_player_info_array){
                    (int)$killer_player_info_array['turret_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id,@json_encode($killer_player_info_array,320));
                }
                break;
            case 'inhibitor':
                $killed_type = 'inhibitor';
                $victim = $victim_ingame_obj_type = $victim_sub_type = 25;
                //记录当前这个人的摧毁水晶信息
                if ($is_player && $killer_player_info_array){
                    (int)$killer_player_info_array['inhibitor_kills'] += 1;
                    $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players'],$rel_killer_id,@json_encode($killer_player_info_array,320));
                }
                break;
            case 'nexus':
                $victim = $victim_ingame_obj_type = $victim_sub_type = 26;
                break;
        }
        $websocket_victim_lane = $websocket_victim_ingame_obj_name = $websocket_victim_ingame_obj_name_cn = $websocket_victim_ingame_obj_type = $websocket_victim_ingame_obj_sub_type = null;
        if ($victim){
            $victimInfo = @json_decode($enumIngameGoalInfoList[$victim],true);
            $websocket_victim_lane = null;
            $websocket_victim_ingame_obj_name = self::checkAndChangeString(@$victimInfo['ingame_obj_name']);
            $websocket_victim_ingame_obj_name_cn = self::checkAndChangeString(@$victimInfo['ingame_obj_name_cn']);
            $websocket_victim_ingame_obj_type = self::checkAndChangeString(@$victimInfo['ingame_obj_type']);
            $websocket_victim_ingame_obj_sub_type = self::checkAndChangeString(@$victimInfo['sub_type']);
        }
        $websocket_victim = [
            'lane' => $websocket_victim_lane,
            'ingame_obj_name' => $websocket_victim_ingame_obj_name,
            'ingame_obj_name_cn' => $websocket_victim_ingame_obj_name_cn,
            'faction' => $victim_faction,
            'ingame_obj_type' => $websocket_victim_ingame_obj_type,
            'ingame_obj_sub_type' => $websocket_victim_ingame_obj_sub_type
        ];
        //assist
        $assist = $assist_ingame_obj_type = $assist_sub_type = null;
        //查询出已经有的事件
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //websocket
        $websocket_assist = null;
        //获取第一次事件
        $is_first_event_res = 2;
        $first_event_type_res = 0;
        $first_event_type = null;
        $first_event_Info = $this->getCommonFirstEventInfo($redis,$battleId,$killed_type,$killer_faction);
        $is_first_event = $first_event_Info['is_first_event'];//true false
        if ($is_first_event){
            $is_first_event_res = 1;
            $first_event_type = $first_event_Info['first_event_type'];
            $first_event_type_res = Common::getPandaScoreEventNum($first_event_type);
            //保存首塔 首水晶
            if ($first_event_type == 'first_turret'){
                $first_turret_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_turret_p_tid' => $first_event_team_order,
                    'first_turret_time' => $ingame_timestamp,
                    'first_turret_detail' => @json_encode($first_turret_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
            if ($first_event_type == 'first_inhibitor'){
                $first_inhibitor_detail = [
                    'position' => $position,
                    'killer' => $websocket_killer,
                    'victim' => $websocket_victim,
                    'assist' => $websocket_assist
                ];
                $match_battle_ext_lol = [
                    'first_inhibitor_p_tid' => $first_event_team_order,
                    'first_inhibitor_time' => $ingame_timestamp,
                    'first_inhibitor_detail' => @json_encode($first_inhibitor_detail,320)
                ];
                $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$match_battle_ext_lol);
            }
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => $position,
            'killer' => $killer,
            'killer_player_name' => $killer_player_name,
            'killer_player_rel_name' => $killer_player_rel_name,
            'killer_champion_id' => $killer_champion_id,
            'killer_faction' => $killer_faction,
            'killer_ingame_obj_type' => $killer_ingame_obj_type,
            'killer_sub_type' => $killer_sub_type,
            'victim' => $victim,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => $victim_faction,
            'victim_ingame_obj_type' => $victim_ingame_obj_type,
            'victim_sub_type' => $victim_sub_type,
            'assist' => $assist,
            'assist_ingame_obj_type' => $assist_ingame_obj_type,
            'assist_sub_type' => $assist_sub_type,
            'is_first_event' => $is_first_event_res,
            'first_event_type' => $first_event_type_res
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //下面是websocket 结果
        $websocket_event_data = [
            'event_type' => $event_type,
            'position' => $position,
            'killer' => $websocket_killer,
            'victim' => $websocket_victim,
            'assist' => $websocket_assist,
            'is_first_event' => $is_first_event,
            'first_event_type' => $first_event_type
        ];
        $result = [
            'type' => 'events',
            'event_data' => $websocket_event_data,
            'event_order' => $event_order
        ];
        return $result;
    }
    //BattleStart
    private function conversionBattleStart($redis,$redis_base_name,$matchInfo,$rel_matchId,$matchId,$battleId,$battle_order,$match_data,$event_id){
        $map_id = 1;//可以做查询
        $event_type = 'battle_start';
        $duration = 0;
        $battlesBase['game'] = 2;
        $battlesBase['match'] = $matchId;
        $battlesBase['status'] = 2;
        $battlesBase['is_draw'] = 2;
        $battlesBase['is_forfeit'] = 2;
        $battlesBase['is_default_advantage'] = 2;
        $battlesBase['map'] = $map_id;
        $battlesBase['duration'] = $duration;
        $battlesBase['is_battle_detailed'] = 1;
        $battlesBase['flag'] = 1;
        $battlesBase['deleted_at'] = null;
        $battlesBase['deleted'] = 2;
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battlesBase);
        //detail
        $battlesDetail = [
            'is_confirmed' => 1,
            'is_pause' => 2,
            'is_live' => 1
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$battlesDetail);
        //获取blue队伍信息
        $blue_team_id = self::getMainIdByRelIdentityId('team',$match_data['blue']['id'],'3');
        //获取$team_order
        $blue_team_order = self::getTeamOrderByTeamId($blue_team_id, $matchInfo);
        $blue_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$blue_team_id);
        $blue_team_info = @json_decode($blue_team_info_redis,true);
        $blue = [
            'team_id' => self::checkAndChangeInt(@$blue_team_info['id']),
            'name' => self::checkAndChangeString(@$blue_team_info['name']),
            'image' => self::checkAndChangeString(@$blue_team_info['image_200x200']),
            'opponent_order' => self::checkAndChangeInt($blue_team_order)
        ];
        //获取red队伍信息
        $red_team_id = self::getMainIdByRelIdentityId('team',$match_data['red']['id'],'3');
        //获取$team_order
        $red_team_order = self::getTeamOrderByTeamId($red_team_id, $matchInfo);
        $red_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$red_team_id);
        $red_team_info = @json_decode($red_team_info_redis,true);
        $red = [
            'team_id' => self::checkAndChangeInt(@$red_team_info['id']),
            'name' => self::checkAndChangeString(@$red_team_info['name']),
            'image' => self::checkAndChangeString(@$red_team_info['image_200x200']),
            'opponent_order' => self::checkAndChangeInt($red_team_order)
        ];
        //地图
        $map = $this->getMapInfoByRedisOrMemory($redis,$map_id);
        //获取事件的order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => (Int)$matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $duration,
            'position' => null,
            'killer' => (Int)$blue['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => (Int)$red['team_id'],
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => 0,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type,
            'map' => $map,
            'blue' => $blue,
            'red' => $red,
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,3,2,'events',$matchId,$rel_matchId,$battleId,$battle_order,false,$event_type,$event_data_ws_res,$event_order);
        return true;
    }
    //BattleEnd
    private function conversionBattleEnd($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$end_data,$event_id){
        $event_type = 'battle_end';
        $ingame_timestamp = (Int)$end_data['duration'];
        $battle_winner = $end_data['battle_winner'];
        $winnerId = $battle_winner['team_id'];
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => (Int)$winnerId,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type,
            'winner' => $battle_winner
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,3,2,'events',$matchId,$rel_matchId,$battleId,$battle_order,true,$event_type,$event_data_ws_res,$event_order);
        return true;
    }
    //BattlePause
    private function conversionBattlePause($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$battle_is_pause_data,$ingame_timestamp,$event_id){
        //true 就是暂停
        if ($battle_is_pause_data){
            $event_type = 'battle_pause';
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'is_pause',1);
        }else{//解除暂停
            $event_type = 'battle_unpause';
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'is_pause',2);
        }
        //获取event_order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'position' => null,
            'killer' => null,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,3,2,'events',$matchId,$rel_matchId,$battleId,$battle_order,true,$event_type,$event_data_ws_res,$event_order);
        return true;

    }
    //最终保存信息
    private function saveWsData($redis,$redis_base_name,$originId,$gameId,$socket_type,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_data_ws_res,$event_order){
        //插入到redis
        $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
        //保存 api
        $this->refreshToApi($redis,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order);
        //接下来存数据库
        $insertData = [
            'match_data' =>@json_encode($event_data_ws_res,320),
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => $socket_type,
            'battle_order' => $battle_order
        ];
        //当前数据库连接
//        $this->saveDataBase($insertData);
        $this->saveKafka($insertData);
    }
    //执行ws to rest api
    private function refreshToApi($redis,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order){
        //判断是不是修复 并且比赛是不是结束
        if ($this->operate_type == 'xiufu') {
            if ($event_type == 'battle_end'){
                $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
                if ($match_status_check == 3) {
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'  => $rel_matchId,
                            'battleId'     => $battleId,
                            'battleOrder'  => $battle_order,
                            'event_type'   => 'refresh_match_xiufu',
                            'event_order'  => 0
                        ]
                    ];
                    TaskRunner::addTask($item, 4);
                }
            }
            return true;
        }
        if (empty($this->run_type)){
            $run_type = 'socket';
        }else{
            $run_type = $this->run_type;
        }
        $nowTime = time();
        $refreshTime = $this->redis_Get_Or_Memory($redis,['current', 'battles', $battleId, 'ws_to_api']);
        //判断类型
        if ($event_type == 'frames'){
            if (!$is_battle_finished){
                $refreshTime = empty($refreshTime) ? 0 : (Int)$refreshTime;
                $diff_time = $nowTime - $refreshTime;
                if ($diff_time < 5) {
                    return true;
                }
            }
            //插入到队列
            $item = [
                "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                    $originId, "", "", 2),
                "type"     => '',
                "batch_id" => date("YmdHis"),
                "params"   => [
                    'matchId'      => $matchId,
                    'rel_matchId'      => $rel_matchId,
                    'battleId'      => $battleId,
                    'battleOrder'      => $battle_order,
                    'event_type'      => $event_type,
                    'event_order'      => 0,
                    'run_type'      => $run_type
                ],
            ];
            TaskRunner::addTask($item, 4);
            $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'ws_to_api'],$nowTime);
        }else{
            $event_type_array = ['battle_start','battle_end','battle_pause','battle_unpause','battle_reloaded','elite_announced','elite_spawned','player_spawned','player_kill','player_suicide','elite_kill','building_kill','ward_placed','ward_kill','ward_expired'];
            if (in_array($event_type,$event_type_array)){
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        $originId, "", "", 2),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => [
                        'matchId'      => $matchId,
                        'rel_matchId'      => $rel_matchId,
                        'battleId'      => $battleId,
                        'battleOrder'      => $battle_order,
                        'event_type'      => $event_type,
                        'event_order'      => $event_order,
                        'run_type'      => $run_type
                    ],
                ];
                TaskRunner::addTask($item, 4);
            }
        }
        return true;
    }
    //转化装备
    private function conversionPandascorePlayerItemsIds($redis,$battleId,$ingame_timestamp,$rel_player_id,$playerItems,$lolItemsList,$lolItemsRelationList){
        //获取未知的装备ID
        $item_unknown_id = $this->getLolItemsUnknownId($redis);
        $need_change = 0;
        //当前这个player的装备信息 然后获取上一个装备信息列表，判断有没有变化
        $playersItemsRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$rel_player_id);
        if ($playersItemsRedis){
            $playersItemsBeforeLine = @json_decode($playersItemsRedis,true);
            $playersItemsBefore = @end($playersItemsBeforeLine)['items'];
        }else{
            $playersItemsBeforeLine = $playersItemsBefore = [];
        }
        $playerOrnamentsInfo = [];
        $playerItemsNull = [];
        $playerItemsInfo = [];
        if ($playerItems){
            foreach ($playerItems as $key => $item_val){
                if ($item_val['id']){
                    $itemInfoId = $lolItemsRelationList[$item_val['id']];
                    if($itemInfoId){
                        if (!in_array($itemInfoId,$playersItemsBefore)){
                            $need_change = 1;
                        }
                        $itemInfoId_val = (Int)$itemInfoId;
                        //获取当前
                        $itemInfoResRedis = $lolItemsList[$itemInfoId_val];
                        if ($itemInfoResRedis){
                            $itemInfoRes = @json_decode($itemInfoResRedis,true);
                            if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                                $playerOrnamentsInfo[$key]['item_id'] = $itemInfoId_val;
                                $playerOrnamentsInfo[$key]['purchase_time'] = null;
                                $playerOrnamentsInfo[$key]['cooldown'] = null;
                            }else{//道具
                                $playerItemsInfo[$key]['total_cost'] = @$itemInfoRes['total_cost'];
                                $playerItemsInfo[$key]['item_id'] = $itemInfoId_val;
                                $playerItemsInfo[$key]['purchase_time'] = null;
                                $playerItemsInfo[$key]['cooldown'] = null;
                            }
                        }else{//道具
                            $playerItemsInfo[$key]['total_cost'] = 1;
                            $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                            $playerItemsInfo[$key]['purchase_time'] = null;
                            $playerItemsInfo[$key]['cooldown'] = null;
                        }
                    }else{
                        $playerItemsInfo[$key]['total_cost'] = 1;
                        $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                        $playerItemsInfo[$key]['purchase_time'] = null;
                        $playerItemsInfo[$key]['cooldown'] = null;
                    }
                }else{
                    $playerItemsNull[$key] = null;
                }
            }
            $total_cost_array = array_column($playerItemsInfo,'total_cost');
            array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
        }
        $playerItemsInfoResult = array_merge($playerItemsInfo,$playerItemsNull,$playerOrnamentsInfo);
        $playerItemsInfoResultEnd = [];
        $playerItemsInfoIdResultEnd = [];
        //循环
        foreach ($playerItemsInfoResult as $key_res => $val_res){
            $ItemsId = null;
            if ($val_res){
                $ItemsId = @$val_res['item_id'];
                unset($val_res['total_cost']);
            }
            $playerItemsInfoResultEnd[] = $val_res;
            $playerItemsInfoIdResultEnd[] = $ItemsId;
        }
        //需要变化则更新redis
        if ($need_change == 1){
            $playerItemsInfoArray = [
                'ingame_timestamp'=>$ingame_timestamp,
                'items'=>$playerItemsInfoIdResultEnd
            ];
            $playersItemsBeforeLine[] = $playerItemsInfoArray;
            $playerItemsLine =  @json_encode($playersItemsBeforeLine,320);
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$rel_player_id,$playerItemsLine);
        }
        //设置返回数据
        $result = [
            'items' => $playerItemsInfoResultEnd,
            'items_timeline' => $playersItemsBeforeLine
        ];
        return $result;
    }
    //转化召唤师技能
    private function conversionPandascorePlayerSummonerSpellsIds($redis,$player_summoner_spells,$summonerSpellNameRelationIdList){
        $summoner_spells_data = [];
        foreach ($player_summoner_spells as $item) {
            $rel_summonerSpellId = $item['id'];
            $summonerSpellId = @$summonerSpellNameRelationIdList[$rel_summonerSpellId];
            if ($summonerSpellId){
                $spell_id = (Int)$summonerSpellId;
            }else{
                $spell_id = $this->getSummonerSpellUnknownId($redis);
            }
            $spell_array = [
                'spell_id' => $spell_id,
                'cooldown' => null
            ];
            $summoner_spells_data[] = $spell_array;
        }
        return $summoner_spells_data;
    }
    //召唤师技能 webscoket 信息
    private function getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_summoner_spells){
        $player_summoner_spells_res = [];
        foreach ($player_summoner_spells as $val_s){
            $player_summoner_spell_info = [];
            $summoner_spell_id_is_unknown = $val_s['is_unknown'];
            if ($summoner_spell_id_is_unknown == 1){
                $player_summoner_spell_info['summoner_spell_id'] = (int)@$val_s['unknown_info']['id'];
                $player_summoner_spell_info['name'] = (String)@$val_s['unknown_info']['name'];
                $player_summoner_spell_info['name_cn'] = (String)@$val_s['unknown_info']['name_cn'];
                $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$val_s['unknown_info']['external_id']);
                $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$val_s['unknown_info']['external_name']);
                $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$val_s['unknown_info']['slug']);
                $summoner_spell_info_image = !empty(@$val_s['unknown_info']['image']) ? (string)@$val_s['unknown_info']['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                $player_summoner_spell_info['cooldown'] = null;
            }else{
                $summoner_spell_info_redis = @$summonerSpellInfoList[$val_s['spell_id']];
                if ($summoner_spell_info_redis){
                    $summoner_spell_info = @json_decode($summoner_spell_info_redis,320);
                    $player_summoner_spell_info['summoner_spell_id'] = (int)@$summoner_spell_info['id'];
                    $player_summoner_spell_info['name'] = (String)@$summoner_spell_info['name'];
                    $player_summoner_spell_info['name_cn'] = (String)@$summoner_spell_info['name_cn'];
                    $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$summoner_spell_info['external_id']);
                    $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$summoner_spell_info['external_name']);
                    $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$summoner_spell_info['slug']);
                    $summoner_spell_info_image = !empty(@$summoner_spell_info['image']) ? (string)@$summoner_spell_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                    $player_summoner_spell_info['cooldown'] = null;
                }
            }
            $player_summoner_spells_res[] = $player_summoner_spell_info;
        }
        return $player_summoner_spells_res;
    }
    //item websocket 信息
    private function getWebSocketPlayerItemsList($lolItemsList,$player_items_rel){
        $player_items = [];
        foreach ($player_items_rel as $key_item => $val_item){
            $player_items_val = [];
            if ($val_item){
                $item_info_data = @$lolItemsList[$val_item['item_id']];
                if ($item_info_data){
                    $item_info = @json_decode($item_info_data,true);
                    $player_items_val['item_id'] = (int)$item_info['id'];
                    $player_items_val['name'] = (string)$item_info['name'];
                    $player_items_val['name_cn'] = (string)$item_info['name_cn'];
                    $player_items_val['external_id'] = self::checkAndChangeString($item_info['external_id']);
                    $player_items_val['external_name'] = self::checkAndChangeString($item_info['external_name']);
                    $player_items_val['total_cost'] = (int)$item_info['total_cost'];
                    $player_items_val['is_trinket'] = @$item_info['is_trinket'] == 1 ? true : false;
                    $player_items_val['is_purchasable'] = @$item_info['is_purchasable'] == 1 ? true : false;
                    $player_items_val['slug'] = (string)$item_info['slug'];
                    $player_item_image = @$item_info['image'] ? @$item_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_items_val['image'] = $player_item_image;
                    $player_items_val['purchase_time'] = null;
                    $player_items_val['cooldown'] = null;
                }
            }
            $key_name_item = $key_item + 1;
            $player_items['slot_'.$key_name_item] = $player_items_val;
        }
        return $player_items;
    }
    //处理队伍的杀龙信息
    private function conversionTeamDragonKills($drakeInfoList,$team_dragon_kills_detail){
        $dragon_kills_detail_res = null;
        if ($team_dragon_kills_detail){
            foreach ($team_dragon_kills_detail as $val_d){
                $drakeInfoRedis = $drakeInfoList[$val_d['drake']];
                if ($drakeInfoRedis){
                    $drakeInfo = @json_decode($drakeInfoRedis,true);
                    $drakeInfoRes = [
                        'ingame_timestamp' => self::checkAndChangeInt($val_d['date']),
                        'dragon_name' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name']),
                        'dragon_name_cn' => self::checkAndChangeString(@$drakeInfo['ingame_obj_name_cn'])
                    ];
                    $dragon_kills_detail_res[] = $drakeInfoRes;
                }
            }
        }
        return $dragon_kills_detail_res;
    }
    //元素龙 刷新
    private function getElitesStatusNew($redis,$battleId,$team_elites_Kills,$duration){
        //计算 elites_status
        $enumIngameGoalInfoList = $this->getEnumIngameGoalInfo($redis);
        //enum_socket_game_configure
        $enum_socket_game_configure_info = $this->enum_socket_game_configure;
        //rift_herald
        $rift_herald_first_time = $enum_socket_game_configure_info['rift_herald_first_time'];
        $rift_herald_quit_time = $enum_socket_game_configure_info['rift_herald_quit_time'];
        $rift_herald_reborn_time = $enum_socket_game_configure_info['rift_herald_reborn_time'];
        $rift_herald_exited = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited');
        if ($rift_herald_exited){
            $rift_herald_status_val = 'exited';
            $rift_herald_spawntimer = null;
        }else{
            $rift_herald_status_val = 'alive';
            $rift_herald_spawntimer = null;
            if ($duration < $rift_herald_first_time){
                $rift_herald_status_val = 'not_spawned';
                $rift_herald_spawntimer = $rift_herald_first_time - $duration;
            }elseif ($duration > $rift_herald_quit_time){
                $rift_herald_status_val = 'exited';
                $rift_herald_spawntimer = null;
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'rift_herald'],'exited',1);
            }else{
                $rift_herald_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'rift_herald'],'kill_time');
                if ($rift_herald_kill_time){
                    $rift_herald_spawntimer_time = $duration - $rift_herald_kill_time;
                    if ($rift_herald_spawntimer_time < $rift_herald_reborn_time){
                        $rift_herald_status_val = 'not_spawned';
                        $rift_herald_spawntimer = $rift_herald_reborn_time - $rift_herald_spawntimer_time;
                    }
                }
            }
        }
        //rift_herald Info   1
        $rift_herald_array = @json_decode($enumIngameGoalInfoList[1],true);
        $rift_herald_name_val = @$rift_herald_array['ingame_obj_name'];
        $rift_herald_name_cn_val = @$rift_herald_array['ingame_obj_name_cn'];
        $rift_herald_status = [
            'status' => $rift_herald_status_val,
            'name' => self::checkAndChangeString($rift_herald_name_val),
            'name_cn' => self::checkAndChangeString($rift_herald_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($rift_herald_spawntimer)
        ];
        //dragon_status
        $dragon_first_time = $enum_socket_game_configure_info['dragon_first_time'];
        $dragon_reborn_time = $enum_socket_game_configure_info['dragon_reborn_time'];
        $dragon_quit_num = $enum_socket_game_configure_info['dragon_quit_num'];
        $elder_dragon_first_time = $enum_socket_game_configure_info['elder_dragon_first_time'];
        $elder_dragon_reborn_time = $enum_socket_game_configure_info['elder_dragon_reborn_time'];

        $dragon_status_status_val = 'alive';
        $dragon_status_spawntimer = null;
        $dragon_status_name = null;
        $dragon_status_name_cn= null;
        //判断大龙是不是被杀了
        $dragon_status_Elder_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'dragon_status'],'elder_dragon_kill_time');
        if ($dragon_status_Elder_kill_time) {
            $dragon_status_Elder_spawntimer_cha = $duration - $dragon_status_Elder_kill_time;
            if ($dragon_status_Elder_spawntimer_cha < $elder_dragon_reborn_time){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = $elder_dragon_reborn_time - $dragon_status_Elder_spawntimer_cha;
            }
            $dragon_status_name = 'Elder Dragon';
            $dragon_status_name_cn = '远古巨龙';
        }else{
            if ($duration < $dragon_first_time){
                $dragon_status_status_val = 'not_spawned';
                $dragon_status_spawntimer = $dragon_first_time - $duration;
            }else{
                $dragon_status_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'dragon_status'],'kill_time');
                if ($dragon_status_kill_time){
                    //首先判断两队是否累计杀了4个了  $dragon_quit_num 是4
                    $ElderDragonIsAlive = false;
                    foreach ($team_elites_Kills as $val_ek ){
                        if ($val_ek['dragon_kills'] >= $dragon_quit_num){
                            $ElderDragonIsAlive = true;
                        }
                    }
                    if ($ElderDragonIsAlive){
                        $dragon_status_Elder_spawned_cha = $duration - $dragon_status_kill_time;
                        $dragon_status_spawntimer = $elder_dragon_first_time - $dragon_status_Elder_spawned_cha;
                        if ($dragon_status_spawntimer > 0){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_status_spawntimer;
                        }else{
                            $dragon_status_status_val = 'alive';
                            $dragon_status_spawntimer = null;
                        }
                        $dragon_status_name = 'Elder Dragon';
                        $dragon_status_name_cn = '远古巨龙';
                    }else{
                        $dragon_status_spawntimer_cha = $duration - $dragon_status_kill_time;
                        if ($dragon_status_spawntimer_cha < $dragon_reborn_time){
                            $dragon_status_status_val = 'not_spawned';
                            $dragon_status_spawntimer = $dragon_reborn_time - $dragon_status_spawntimer_cha;
                        }
                    }
                }
            }
        }
        $dragon_status = [
            'status' => $dragon_status_status_val,
            'name' => $dragon_status_name,
            'name_cn' => $dragon_status_name_cn,
            'spawntimer' => self::checkAndChangeInt($dragon_status_spawntimer)
        ];
        //baron_nashor_status  7
        $baron_nashor_first_time = $enum_socket_game_configure_info['baron_nashor_first_time'];
        $baron_nashor_reborn_time = $enum_socket_game_configure_info['baron_nashor_reborn_time'];
        $baron_nashor_spawntimer = null;
        $baron_nashor_array = @json_decode($enumIngameGoalInfoList[7],true);
        $baron_nashor_name_val = @$baron_nashor_array['ingame_obj_name'];
        $baron_nashor_name_cn_val = @$baron_nashor_array['ingame_obj_name_cn'];
        $baron_nashor_kill_time = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'baron_nashor'],'kill_time');
        if ($baron_nashor_kill_time){
            $baron_nashor_spawntimer_time = $duration - $baron_nashor_kill_time;
            if ($baron_nashor_spawntimer_time > $baron_nashor_reborn_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_reborn_time - $baron_nashor_spawntimer_time;
            }
        }else{
            if ($duration > $baron_nashor_first_time){
                $baron_nashor_status_val = 'alive';
                $baron_nashor_spawntimer = null;
            }else{
                $baron_nashor_status_val = 'not_spawned';
                $baron_nashor_spawntimer = $baron_nashor_first_time - $duration;
            }
        }
        $baron_nashor_status = [
            'status' => $baron_nashor_status_val,
            'name' => self::checkAndChangeString($baron_nashor_name_val),
            'name_cn' => self::checkAndChangeString($baron_nashor_name_cn_val),
            'spawntimer' => self::checkAndChangeInt($baron_nashor_spawntimer)
        ];
        $elites_status = [
            'rift_herald_status' => $rift_herald_status,
            'dragon_status' => $dragon_status,
            'baron_nashor_status' => $baron_nashor_status
        ];
        return $elites_status;
    }
    //第一次事件
    public function getCommonFirstEventInfo($redis,$battleId,$killed_type,$faction){
        $event_first_redis_name = ['current','battles',$battleId,'event_first'];
        //记录事件
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
                //设置初始值
                $team_kills_key = 'team_kills_'.$faction;
                //查询是否是一血
                //$first_blood = $redis->hGet($event_first_redis_name,'first_blood');
                $first_blood = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_blood');
                if (!$first_blood){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_blood'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_blood',$faction);
                }
                //查询 first_to_5_kills first_to_10_kills
                //查询当前team击杀了多少人
                //查询是否五杀了
                $first_to_5_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_5_kills');
                $first_to_10_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_10_kills');
                $team_kills_res = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,$team_kills_key);//击杀队伍
                if ($team_kills_res){
                    $now_team_kills = $team_kills_res + 1;
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,$now_team_kills);
                    if ($now_team_kills == 5 && empty($first_to_5_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_5_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_5_kills',$faction);
                    }
                    if ($now_team_kills == 10 && empty($first_to_10_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_10_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_10_kills',$faction);
                    }
                }else{
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,1);
                }
                break;
            case 'rift_herald':
                $rift_herald = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_rift_herald',$faction);
                }
                break;
            case 'drake':
                $first_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_dragon');
                if (!$first_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_dragon',$faction);
                }
                break;
            case 'elder_dragon':
                $first_elder_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_elder_dragon',$faction);
                }
                break;
            case 'baron_nashor':
                $first_baron_nashor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_baron_nashor',$faction);
                }
                break;
            case 'tower':
                $first_turret = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_turret',$faction);
                }
                break;
            case 'inhibitor':
                $first_inhibitor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_inhibitor',$faction);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null
                ];
        }
        return $eventInfoRes;
    }
    //////////////////////////  获取比赛信息  /////////////////////////////////////
    //获取比赛ID
    public function getMatchIdAndRelMatchId($redis,$originId,$rel_match_id){
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        if ($this->memory_mode){
            $this->matchId = $matchId;
        }
        return $matchId;
    }
    //获取比赛信息
    public function getMatchInfo($redis,$matchId,$socket_time){
        if ($this->memory_mode && !empty($this->matchInfo)){
            return $this->matchInfo;
        }
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $matchInfo = $redis->hGetAll($matchInfoRedisName);
        if (!$matchInfo){
            //获取比赛信息
            $matchInfo = self::getMatchAllInfoByMatchId($matchId);
            if (empty($matchInfo)){
                return 'no_match_info';
            }
            $redis->hMSet($matchInfoRedisName,$matchInfo);
            //setMatchRealTimeInfo
            $this->getMatchRealTimeInfo($redis,$matchId,$socket_time);
            $this->getInitTeamInfo($redis,$matchId,$matchInfo);
            //$this->getInitPlayerInfo($redis,$matchId,$matchInfo);
        }
        //设置比赛
        if ($this->memory_mode){
            $this->matchInfo = $matchInfo;
        }
        return $matchInfo;
    }
    //设置team 各种信息
    public function getInitTeamInfo($redis,$matchId,$matchInfo){
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $teams_info = $redis->hGetAll($matchTeamInfoRedisName);
        if (!$teams_info){
            $Team1Info = Team::find()->where(['id'=>$matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])){
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team1Info['id'],@json_encode($Team1Info,320));
            $Team2Info = Team::find()->where(['id'=>$matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])){
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team2Info['id'],@json_encode($Team2Info,320));
            $teams_info = [
                $Team1Info['id'] =>@json_encode($Team1Info,320),
                $Team2Info['id'] =>@json_encode($Team2Info,320)
            ];
        }
        if ($this->memory_mode){
            $this->teams_info = $teams_info;
            $this->setDataToMemory(['teams_info'],$teams_info);
        }
        return $teams_info;
    }
    //初始化玩家
    public function getInitPlayerInfo($redis,$matchId,$matchInfo){
        //根据队伍ID 获取player 战队选手快照   选手表
        $playerInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_info";
        $player_info = $redis->hGetAll($playerInfoRedisName);
        if (!$player_info){
            $player_name_id_relation_save = false;
            $playerNameIdRelationRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":player_name_id_relation";
            $player_name_id_relation = $redis->hGetAll($playerNameIdRelationRedisName);
            if (!$player_name_id_relation){
                $player_name_id_relation_save = true;
            }
            $team_ids = [$matchInfo['team_1_id'],$matchInfo['team_2_id']];
            $playersInfo = TeamSnapshot::find()->alias('ts')->select('p.id,p.nick_name')
                ->leftJoin('team_player_relation as tpr','ts.team_id = tpr.team_id')
                ->leftJoin('player as p','p.id = tpr.player_id')
                ->where(['ts.relation_id'=>$matchInfo['tournament_id']])
                ->andWhere(['in', 'ts.team_id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val){
                if ($player_name_id_relation_save){
                    $player_name_id_relation[@$val['nick_name']] = @$val['id'];
                }
                $player_info[@$val['id']] = @json_encode($val, 320);
            }
            $redis->hMSet($playerInfoRedisName,$player_info);
            if ($player_name_id_relation_save){
                if ($this->memory_mode) {
                    $this->player_name_id_relation = $player_name_id_relation;
                }
                $redis->hMSet($playerNameIdRelationRedisName,$player_name_id_relation);
            }
        }
        if ($this->memory_mode){
            $this->player_info = $player_info;
        }
        return $player_info;
    }
    //获取比赛信息
    public function getMatchRealTimeInfo($redis,$matchId,$socket_time)
    {
        if ($this->memory_mode && !empty($this->matchRealTimeInfo)) {
            return $this->matchRealTimeInfo;
        }
        //match_real_time_info
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":".Consts::MATCH_REAL_TIME_INFO;
        $match_real_time_info = $redis->hGetAll($matchRealTimeInfoRedisName);
        if (!$match_real_time_info){
            $match_real_time_info = MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
            if ($this->operate_type == 'xiufu') {
                $match_real_time_info['team_1_score'] = 0;
                $match_real_time_info['team_2_score'] = 0;
                $match_real_time_info['winner'] = null;
                $match_real_time_info['status'] = 2;
            }
            if (empty(@$match_real_time_info['begin_at'])){
                $match_real_time_info['begin_at'] = $socket_time;
            }
            $redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
        }
        //存内存
        if ($this->memory_mode){
            $this->matchRealTimeInfo = $match_real_time_info;
        }
        return $match_real_time_info;
    }
    //设置比赛状态
    public function setMatchStatus($redis,$matchId){
        if ($this->memory_mode){
            $match_status = $this->match_status;
            if ($match_status){
                return true;
            }
            $this->match_status = 2;
        }
        $matchStatusRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_status";
        $match_status = $redis->get($matchStatusRedisName);
        if (!$match_status){
            $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
            if ($MatchRealTimeInfo){
                $MatchRealTimeInfo->setAttribute('status',2);
                $MatchRealTimeInfo->save();
                $redis->set($matchStatusRedisName,2);
            }
        }
        return true;
    }
    //获取地图信息
    private function getMapInfoByRedisOrMemory($redis,$map_id){
        $map = null;
        if ($this->memory_mode && !empty($this->map)){
            return $this->map;
        }
        $mapInfoRedis = $redis->hGet(Consts::GAME_MAPS.":lol",$map_id);
        if ($mapInfoRedis){
            $mapInfo = @json_decode($mapInfoRedis,true);
            $map_is_default = @$mapInfo['is_default'] == 1 ? true : false;
            $map_image = [
                'square_image' => null,
                'rectangle_image' => null,
                'thumbnail' => (String)@$mapInfo['image']
            ];
            $map = [
                'map_id' => (Int)@$mapInfo['id'],
                'name' => (String)@$mapInfo['name'],
                'name_cn' => self::checkAndChangeString(@$mapInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$mapInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$mapInfo['external_name']),
                'short_name' => self::checkAndChangeString(@$mapInfo['short_name']),
                'map_type' => self::checkAndChangeString(@$mapInfo['map_type']),
                'map_type_cn' => self::checkAndChangeString(@$mapInfo['map_type_cn']),
                'is_default' => $map_is_default,
                'slug' => self::checkAndChangeString(@$mapInfo['slug']),
                'image' => $map_image
            ];
        }
        if ($this->memory_mode){
            $this->map = $map;
        }
        return $map;
    }
    //获取游戏版本号
    private function getGameVersion($redis,$matchId){
        //服务器设置游戏版本
        $match_game_version = $redis->hGetAll(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version");
        if (empty(@$match_game_version['game_version'])){
            $game_version = null;
            $now_time = time();
            if ($match_game_version['get_time']){
                $get_game_version_time_cha = $now_time - $match_game_version['get_time'];
                if ($get_game_version_time_cha > 300){
                    $MatchBaseInfo = MatchBase::find()->select('game_version')->where(['match_id'=>$matchId])->asArray()->one();
                    $game_version = @$MatchBaseInfo['game_version'];
                    $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'game_version',$game_version);
                    $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'get_time',$now_time);
                }
            }else{
                $MatchBaseInfo = MatchBase::find()->select('game_version')->where(['match_id'=>$matchId])->asArray()->one();
                $game_version = @$MatchBaseInfo['game_version'];
                $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'game_version',$game_version);
                $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'get_time',$now_time);
            }
        }else{
            $game_version = @$match_game_version['game_version'];
        }
        return $game_version;
    }
    /////////////////  基础信息   /////////////////////
    //获取英雄信息
    private function getChampionInfoByRedisOrMemory($redis,$metadata_origin_id,$champions_id){
        if ($this->memory_mode && !empty($this->match_all_champion_websocket[$champions_id])){
            return $this->match_all_champion_websocket[$champions_id];
        }
        $champion = null;
        $championInfoRedis = $redis->hGet('lol:champions:list:'.$metadata_origin_id,$champions_id);
        if ($championInfoRedis){
            $championInfo = @json_decode($championInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $redis->get("lol:champions:list:unknown");
            $championInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($championInfo['image'])){
                $image_rel = null;
            }else{
                $image_rel = $championInfo['image']."?x-oss-process=image/resize,m_fixed,h_120,w_120";
            }
            if (empty($championInfo['small_image'])){
                $small_image_rel = null;
            }else{
                $small_image_rel = $championInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $champion = [
                'champion_id' => self::checkAndChangeInt(@$championInfo['id']),
                'name' => self::checkAndChangeString(@$championInfo['name']),
                'name_cn' => self::checkAndChangeString(@$championInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$championInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$championInfo['external_name']),
                'title' => self::checkAndChangeString(@$championInfo['title']),
                'title_cn' => self::checkAndChangeString(@$championInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$championInfo['slug']),
                'image' => $image
            ];
        }
        if ($this->memory_mode){
            $this->match_all_champion_websocket[$champions_id] = $champion;
        }
        return $champion;
    }
    //获取英雄 relation id 信息
    public function getChampionRelationIdList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->lol_champion_relation_id_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('lol:champions:relation:'.$originId);
        if ($this->memory_mode) {
            return $this->lol_champion_relation_id_list = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getChampionUnknownId($redis){
        $champion_unknown_redis = $redis->get('lol:champions:list:unknown');
        $champion_unknown_info = @json_decode($champion_unknown_redis,true);
        return @$champion_unknown_info['id'];
    }
    //获取Lol 英雄ID
    private function getLolChampionId($redis,$metadata_origin_id,$rel_championId){
        $championRelationIdList = $this->getChampionRelationIdList($redis,$metadata_origin_id);
        $champions_id = @$championRelationIdList[$rel_championId];
        if (!$champions_id){
            $champions_id = $this->getChampionUnknownId($redis);
        }
        return (Int)$champions_id;
    }
    //获取LOL Items Relation
    public function getLolItemsList($redis,$originId)
    {
        if ($this->memory_mode) {
            $strValue = $this->lol_items_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll("lol:items:list:".$originId);
        if ($this->memory_mode) {
            $this->lol_items_list = $returnValue;
        }
        return $returnValue;
    }
    //获取LOL Items Relation
    public function getLolItemsRelationList($redis,$originId)
    {
        if ($this->memory_mode) {
            $strValue = $this->lol_items_relation[$originId];
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll("lol:items:relation:".$originId);
        if ($this->memory_mode) {
            return $this->lol_items_relation[$originId] = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getLolItemsUnknownId($redis){
        if ($this->memory_mode) {
            $strValue = $this->lol_item_unknown_id;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $item_unknown_id = null;
        $item_unknown_redis = $redis->get('lol:items:list:unknown');
        $item_unknown_info = @json_decode($item_unknown_redis,true);
        $item_unknown_id = @$item_unknown_info['id'];
        if ($this->memory_mode) {
            $this->lol_item_unknown_id = $item_unknown_id;
        }
        return $item_unknown_id;
    }
    //召唤师技能 name relation id
    public function getSummonerSpellRelationIdList($redis,$originId){

        if ($this->memory_mode) {
            $strValue = $this->lol_summoner_spell_relation_id_list[$originId];
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('lol:summoner_spell:relation:'.$originId);
        if ($this->memory_mode) {
            return $this->lol_summoner_spell_relation_id_list[$originId] = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 列表
    public function getSummonerSpellInfoList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->lol_summoner_spell_list[$originId];
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('lol:summoner_spell:list:'.$originId);
        if ($this->memory_mode) {
            return $this->lol_summoner_spell_list[$originId] = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能
    private function getSummonerSpellUnknownId($redis){
        $redisNameList = "lol:summoner_spell:list:unknown";
        $UnknownInfoRedis = $redis->get($redisNameList);
        $UnknownInfo = @json_decode($UnknownInfoRedis,true);
        $summoner_spell_id = @$UnknownInfo['id'];
        return $summoner_spell_id;
    }
    //获取battle的队伍信息
    private function getBattleTeamsInfoFromMemory($redis,$battleId){
        $teams_Id_Info_Array = [];
        $teams_Info_Redis = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        if ($teams_Info_Redis){
            foreach ($teams_Info_Redis as $key => $val){
                $val_array = @json_decode($val,true);
                $teams_Id_Info_Array[$key] = @$val_array;
            }
        }
        return $teams_Id_Info_Array;
    }
    //获取EnumIngameGoal信息
    private function getEnumIngameGoalInfo($redis){
        if ($this->memory_mode) {
            $strValue = $this->enum_ingame_goal_info;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('enum:ingame_goal');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->enum_ingame_goal_info = $returnValue;
            }
        }
        return $returnValue;
    }
    //socket录入服务器配置
    private function setSocketGameConfigure($game_type){
        $enum_socket_lol = $this->enum_socket_game_configure;
        if (empty($enum_socket_lol)){
            $this->enum_socket_game_configure = $this->getSocketGameConfigure($game_type);
        }
        return true;
    }
    //////////////////////////    redis 和 内存      ///////////////////////////////////
    //获取数组 key
    public function redis_hKeys_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_keys($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hKeys($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组 key
    public function redis_hVals_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_values($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hVals($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetTeamAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 2){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetPlayersAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 10 ){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取hash 单条
    public function redis_hGet_Or_Memory($redis,$keys,$field)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$field];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGet($keyString,$field);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
//            if (array_key_exists($keyString, $this->current)){
//                $this->current[$keyString][$field] = $returnValue;
//            }else{
//                if (!empty($returnValue)){
//                    $this->current[$keyString][$field] = $returnValue;
//                }
//            }
            //重新赋值所有
            $allData = $redis->hGetAll($keyString);
            if ($allData){
                $this->current[$keyString] = $allData;
            }
        }
        return $returnValue;
    }
    //获取 get 单条
    public function redis_Get_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->get($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取长度
    public function redis_hLen_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return count($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hLen($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue){
                $this->setDataToMemory($keys,$returnAllValue);
            }
        }
        return $returnValue;
    }
    //设置缓存 内存
    public function redis_hMSet_And_Memory($redis,$keys,$values)
    {
        if ($values){
            $matchId = $this->matchId;
            $keyString = implode(":", $keys);
            $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
            $redis->hMSet($keyString,$values);
            if ($this->memory_mode) {
                $memory_values = $redis->hGetAll($keyString);
                $keyString = implode("_", $keys);
                $this->current[$keyString] = $memory_values;
            }
        }
        return true;
    }
    //设置缓存 内存
    public function redis_hSet_And_Memory($redis,$keys,$filedKey,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->hSet($keyString,$filedKey,$values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString][$filedKey] = $values;
        }
        return true;
    }
    //设置缓存 内存
    public function redis_Set_And_Memory($redis,$keys,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->set($keyString, $values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $values;
        }
        return true;
    }
    //设置缓存 单独设置 内存
    public function setDataToMemory($keys,$value){
        if ($this->memory_mode && $value) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $value;
        }
        return true;
    }
    //保存到DB
    public function saveDataBase($data){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $data);
        $db_websoket->execute();
        return true;
    }
    //保存到Kafka
    public function saveKafka($data){
        if ($this->operate_type == 'xiufu'){
            return true;
        }
        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_LOL'),$data);
    }
    //清空redis和内存
    public function delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id){
        //内存清空
        $this->current = [];
        //清空redis
        //删除关联关系
        $redis_match_lol_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":lol:".$origin_id;
        $redis->hdel($redis_match_lol_relation,$rel_match_id);
        //新的结构删除
        $match_all_info_list = $redis->Keys('match_all_info:'.$match_id."*");
        if ($match_all_info_list) {
            foreach ($match_all_info_list as $item_mai) {
                $redis->del($item_mai);
            }
        }
        return true;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}