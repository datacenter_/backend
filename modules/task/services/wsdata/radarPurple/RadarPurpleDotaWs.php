<?php

namespace app\modules\task\services\wsdata\radarPurple;
use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\WinnerHelper;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchBase;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\MatchSocketData;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class RadarPurpleDotaWs extends HotBase
{
    private static $instance;
    public $gameId = 3;
    public $battle_order_relation_id = [];
    public $matchId = null;
    public $originId = null;
    public $rel_matchId = null;
    public $map = null;
    public $matchInfo = null;
    public $matchRealTimeInfo = null;
    public $rel_order_id = null;
    public $now_battle_order = null;
    public $socket_id = null;
    public $socket_time = null;
    public $match_status = null;
    public $battleId = null;
    public $battle_order = null;
    public $team_name_id_relation = null;
    public $teams_info = null;
    public $player_name_id_relation = null;
    public $player_info = null;
    public $current = [];
    public $battles = null;
    public $memory_mode = false;
    public $is_reload = false;
    public $match_all_champion_websocket = [];
    //其它
    public $dota2_hero_relation_id_list = [];
    public $dota_items_relation = [];
    public $dota_items_list = [];
    public $dota_item_unknown_id = null;
    public $dota_runes_list = [];
    public $dota_runes_relation_list = [];
    public $dota_summoner_spell_relation_id_list = [];
    public $dota_summoner_spell_list = [];
    public $enum_ingame_goal_info = [];
    public $run_type = null;
    public $operate_type = null;

    //程序入口
    public static function run($tagInfo, $taskInfo)
    {
        //初始化自己
        $RadarPurpleDotaWs = self::getInstance();
        $redis = $RadarPurpleDotaWs->redis;
        //内存模式
        $RadarPurpleDotaWs->memory_mode = true;
        //解析
        $runTag = @$tagInfo['ext2_type'];
        if ($runTag == 'refresh') {
            $info = @json_decode($taskInfo['params'], true);
            $rel_match_id = @$info['rel_match_id'];
            $match_id = @$info['match_id'];
            $origin_id = @$info['origin_id'];
            $RadarPurpleDotaWs->operate_type = @$info['operate_type'];
            $RadarPurpleDotaWs->delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id);
            //查询所有
            $match_list = MatchLived::find()->where(['match_id'=>$rel_match_id])->asArray()->all();
            if ($match_list){
                foreach ($match_list as $key => $value){
                    $RadarPurpleDotaWs->runWithInfo($redis,$value,'refresh');
                }
            }
            $res = 'success';
        } else {
            $res = $RadarPurpleDotaWs->runWithInfo($redis, $taskInfo,'socket');
        }
        return $res;
    }

    public function runWithInfo($redis,$taskInfo,$run_type)
    {
        $this->run_type = $run_type;
        $conversionRes = null;
        if ($run_type == 'socket'){
            $info = @json_decode($taskInfo['params'], true);
            $match_data = $info['match_data'];
            $socket_id = (Int)$info['socket_id'];
        }else{
            $info = $taskInfo;
            $match_data = @json_decode($taskInfo['match_data'], true);
            $socket_id = (Int)$info['id'];
        }
        $originId = (Int)@$info['origin_id'];
        $this->originId = $originId;
        if (isset($info['socket_time']) && $info['socket_time']){
            $socket_time = $info['socket_time'];
        }else{
            $socket_time = date('Y-m-d H:i:s');
        }
        //判断是不是空
        if (empty($match_data)){
            return [];
        }
        //判断是不是 hello
        if (isset($match_data['type']) && $match_data['type'] == 'hello'){
            return 'hello';
        }
        $rel_matchId = $info['match_id'];
        $this->rel_matchId = $rel_matchId;
        $matchId = $this->matchId;
        if (empty($matchId)){
            $matchId = $this->getMatchIdAndRelMatchId($redis,$originId,$rel_matchId);
        }
        //判断有没有数据
        if (empty($matchId)){
            return 'no-match';
        }
        $this->socket_id = $socket_id;
        $this->socket_time = $socket_time;
        //获取比赛数据
        $matchInfo = $this->getMatchInfo($redis,$matchId,$socket_time);
        if ($matchInfo == 'no_match_info') {
            return 'no_match_info';
        }
        //更改比赛状态 为进行中
        if ($this->operate_type != 'xiufu') {
            $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
            if ($match_status_check == 3) {
                return 'match is over';
            }
            $this->redis_hSet_And_Memory($redis, ['match_real_time_info'], 'status',2);
            //$this->setMatchStatus($redis, $matchId);
        }
        //基础redisName
        $redis_base_name = Consts::MATCH_ALL_INFO.":".$matchId.":current:";
        $rel_battleId = $match_data['box_num'];
        //获取battle和order
        $battleId = $battle_order = $match_start = null;
        $battleInfoMemory = $this->battle_order_relation_id[$rel_battleId];
        if ($battleInfoMemory){
            $battle_res_explode = explode('-',$battleInfoMemory);
            $battleId = $battle_res_explode[0];
            $battle_order = $battle_res_explode[1];
        }
        if (empty($battleId) || empty($battle_order)) {
            //找到对应的平台的match_id
            $isSetBattleStartTime = true;
            $battle_Info = self::getbattleIdByMatchIdAndOrderNew($redis,$redis_base_name,$matchId,$rel_matchId,$rel_battleId,$socket_time,$match_data['length_time'],$isSetBattleStartTime);
            if (empty($battle_Info['battle_id'])){
                return 'no-battleId';
            }
            $battleId = $battle_Info['battle_id'];
            $battle_order = $battle_Info['battle_order'];
            $match_start = $battle_Info['match_start'];
            $this->battle_order_relation_id[$rel_battleId] = $battleId.'-'.$battle_order;
        }
        //比赛开始 更改比赛的实际开始时间
        if ($match_start == 1){
            if ($this->operate_type != 'xiufu') {
                $match_real_time_info['begin_at'] = $socket_time;
                $this->redis_hMSet_And_Memory($redis, ['match_real_time_info'], $match_real_time_info);
            }
        }
        $is_battle_finished = false;
        $event_type_type = null;
        //判断类型
        $type = $info['type'];//frames event
        if ($type == 'no') {//如果是frames 就判断创建battle_start事件
            $is_have_ws_data = $this->redis_Get_Or_Memory($redis,['current', 'battles', $battleId, 'is_have_ws_data']);
            if (!$is_have_ws_data) {
                $this->conversionBattleStart($redis,$redis_base_name,$matchInfo,$rel_matchId,$matchId,$battleId,$battle_order,$match_data,$socket_id);
                $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'is_have_ws_data'],1);
            }
        }
        //处理type TODO
        $conversionRes = $this->transFramesWs($redis,$originId,$battleId,$match_data,$matchInfo,$socket_time);
        if (empty($conversionRes)){
            return $battleId.'no-data';
        }
        $event_order = 0;
        //判断是不是暂停
        $battle_is_pause_data = $match_data['paused'];
        $battle_is_pause_old = false;
        $battle_is_pause = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'detail'],'is_pause');
        if ($battle_is_pause == 1){
            $battle_is_pause_old = true;
        }
        if ($battle_is_pause_old != $battle_is_pause_data){
            $this->conversionBattlePause($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$battle_is_pause_data,$match_data['current_timestamp'],$socket_id);
        }

        $is_battle_finished = $conversionRes['is_battle_end'];
        if (!$is_battle_finished){
            $now_time = time();
            $out_frames_time_before = $this->redis_Get_Or_Memory($redis,['out_frames_time']);
            if ($out_frames_time_before){
                $out_frames_time_cha = $now_time - $out_frames_time_before;
                if ($out_frames_time_cha < 1){
                    return 'no-handle';
                }
            }
            $this->redis_Set_And_Memory($redis,['out_frames_time'],$now_time);
        }else{
            $this->conversionBattleEnd($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$conversionRes['trans_data'],$socket_id);
            //给redis 设置有效期
            $redis->expire($redis_base_name."battles:".$battleId.":ws_out_put_data",86400);
            $redis->expire($redis_base_name."battles:".$battleId.":event_list",86400);
        }
        $socket_type = 'frames';
        $event_type = 'frames';
        $game_rules = @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']);
        $match_type = @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']);
        $number_of_games = $matchInfo['number_of_games'];
        //获取地图
        $map = $this->getMapInfoByRedisOrMemory($redis,1);
        $event_data_ws_common = [
            'match_id' => (Int)$matchId,
            'game_rules' => (String)$game_rules,
            'match_type' => (String)$match_type,
            'number_of_games' => (Int)$number_of_games,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'map' => $map
        ];
        $event_data_ws_res = @array_merge($event_data_ws_common,$conversionRes['trans_data']);

        //最后保存
        if ($event_data_ws_res){
            $this->saveWsData($redis,$redis_base_name,$originId,$this->gameId,$socket_type,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_data_ws_res,$event_order);
        }
        return $battleId;
    }
    public function transFramesWs($redis,$originId,$battleId,$match_data,$matchInfo,$socket_time)
    {
        $matchId = (Int)$matchInfo['id'];
        //获取游戏版本
        $server_config = [
            'version' => null
        ];
        $radiant = $dire = [];
        //定义building_status
        $outposts = [
            'top_outpost'=> null,
            'bot_outpost'=> null
        ];
        $towers = [
            'top_tier_1_tower'=> null,
            'top_tier_2_tower'=> null,
            'top_tier_3_tower'=> null,
            'mid_tier_1_tower'=> null,
            'mid_tier_2_tower'=> null,
            'mid_tier_3_tower'=> null,
            'bot_tier_1_tower'=> null,
            'bot_tier_2_tower'=> null,
            'bot_tier_3_tower'=> null,
            'top_tier_4_tower'=> null,
            'bot_tier_4_tower'=> null
        ];
        $barracks = [
            'top_ranged_barrack' => null,
            'top_melee_barrack' => null,
            'mid_ranged_barrack' => null,
            'mid_melee_barrack' => null,
            'bot_ranged_barrack' => null,
            'bot_melee_barrack' => null
        ];
        $building_status = [
            'outposts' => $outposts,
            'towers' => $towers,
            'barracks' => $barracks,
            'ancient' => null
        ];
        $duration = $match_data['length_time'];
        //准备元数据
        $dotaItemsList = $this->getdotaItemsList($redis,$originId);
        $dotaItemsRelationList = $this->getdotaItemsRelationList($redis,$originId);
        //获取未知的装备ID
        $item_unknown_id = $this->getdotaItemsUnknownId($redis);

        //定义一些数据
        $factions = $websocket_team_players = $winner_teams_info_res = [];
        $winner_teams_socket_info = $team_rel_id_and_order = $team_rel_id_and_faction = $team_rel_id_and_master_id = [];
        $team_info_new = $team_info_res = $player_array_new = [];
        //获取原来team的数据
        $battleTeamInfo_old = $this->getBattleTeamsInfoFromMemory($redis,$battleId,'all_info');
        //获取原来的player数据
        $battlePlayersInfo_old = $this->redis_hGetPlayersAll_Or_Memory($redis,['current','battles',$battleId,'players']);
        $ban_pick_array = [];
        $ban_pick_is_have = false;
        $ban_pick_info = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'ban_pick']);
        if ($ban_pick_info){
            $ban_pick_is_have = true;
        }
        //组合数据  TODO 这里做ban_pick
        $rel_teamsInfo = [$match_data['home'],$match_data['away']];
        foreach ($rel_teamsInfo as $team_val){
            $rel_team_id = $team_val['id'];
            $team_info_old = @$battleTeamInfo_old[$rel_team_id];
            if (empty($team_info_old)){
                $team_id = self::getMainIdByRelIdentityId('team',$rel_team_id,$originId);
                $team_order = self::getTeamOrderByTeamId($team_id,$matchInfo);
            }else{
                $team_id = $team_info_old['team_id'];
                $team_order = $team_info_old['order'];
            }
            $team_kills = $team_val['stats'][7];
            $team_faction_id = $team_val['side'];
            if ($team_faction_id == 1){
                $team_faction = 'radiant';
            }else{
                $team_faction = 'dire';
            }
            $team_net_worth = $team_val['stats'][8];
            $team_experience = $team_val['stats'][0];
            $team_tower_kills = $team_val['destoryed_tower'];
            $team_deaths = $team_val['stats'][12];
            $team_assists = $team_val['stats'][13];
            $team_net_worth_diff = $team_val['stats'][14];
            $team_info = [
                'battle_id' => $battleId,
                'order' => $team_order,
                'score' => 0,
                'identity_id' => $rel_team_id,
                'rel_identity_id' => $rel_team_id,
                'team_id' => $team_id,
                'faction' => $team_faction,
                'kills' => $team_kills,
                'net_worth' => $team_net_worth,
                'gold_earned' => $team_net_worth,
                'experience' => $team_experience,
                'tower_kills' => $team_tower_kills,
                'building_status' => $building_status,
                'deaths' => $team_deaths,
                'assists' => $team_assists,
                'net_worth_diff' => $team_net_worth_diff,
                'barrack_kills' => $team_val['destoryed_barracks'],
            ];
            //设置team信息
            $team_info_new[$team_faction] = $team_info;
            $team_info_res[$rel_team_id] = $team_info;
            $team_rel_id_and_order[$rel_team_id] = $team_order;
            $team_rel_id_and_faction[$rel_team_id] = $team_faction_id;
            $team_rel_id_and_master_id[$rel_team_id] = $team_id;
        }
        //循环玩家
        $rel_players_list = $match_data['players'];
        foreach ($rel_players_list as $player_key => $player_item){
            $player_order = $player_key + 1;
            $player_nick_name = $rel_player_nick_name = $player_item['name'];
            $rel_player_id = $player_item['id'];
            $player_rel_team_id = $player_item['team_id'];
            $player_old_res = @$battlePlayersInfo_old[$rel_player_id];
            $old_player = @json_decode($player_old_res,true);
            if (empty($old_player)){
                $old_player = [];
                $player_id = self::getMainIdByRelIdentityId('player',$rel_player_id,$originId);
                if (empty($player_id)){
                    $player_id = null;
                }else{
                    $player_id = (Int)$player_id;
                    $Player_Info = Player::find()->select('nick_name')->where(['id'=>$player_id])->asArray()->one();
                    $player_nick_name = @$Player_Info['nick_name'];
                }
                //基础信息
                $player_team_order = $team_rel_id_and_order[$player_rel_team_id];
                //玩家seed 天辉：0、1、2、3、4 夜魇：128,129,130,131,132
                $player_team_faction_id = $team_rel_id_and_faction[$player_rel_team_id];
                if ($player_team_faction_id == 1){
                    $player_seed = $player_item['player_slot'] + 1;
                    $player_team_faction = 'radiant';
                }else{
                    switch ($player_item['player_slot']){
                        case 128:
                            $player_seed = 1;
                            break;
                        case 129:
                            $player_seed = 2;
                            break;
                        case 130:
                            $player_seed = 3;
                            break;
                        case 131:
                            $player_seed = 4;
                            break;
                        case 132:
                            $player_seed = 5;
                            break;
                        default:
                            $player_seed = null;
                    }
                    $player_team_faction = 'dire';
                }
                //team_id
                $player_team_id = $team_rel_id_and_master_id[$player_rel_team_id];
                $player_lane = Common::getDota2LaneByPositionId('radarpurple',$player_seed);
                $player_role = $player_seed;
                $player_info = [
                    'order' => $player_order,
                    'rel_identity_id' => $rel_player_id,
                    'rel_team_id' => $player_rel_team_id,
                    'team_order' => $player_team_order,
                    'seed' => $player_seed,
                    'battle_id' => $battleId,
                    'game' => $this->gameId,
                    'match' => $matchId,
                    'team_id' => $player_team_id,
                    'faction' => $player_team_faction,
                    'role' => $player_role,
                    'lane' => $player_lane,
                    'player_id' => $player_id,
                    'nick_name' => $player_nick_name,
                    'rel_nick_name' => $rel_player_nick_name
                ];
            }
            //需要更新的数据
            $old_player_heroId = $old_player['hero'];
            if (empty($old_player_heroId)){
                if ($player_item['hero_id'] > 0){
                    $player_hero_id = $this->getdota2HeroId($redis,$originId,$player_item['hero_id']);
                }else{
                    $player_hero_id = null;
                }
                $player_info['hero'] = $player_hero_id;
            }
            $player_info['level'] = $player_item['level'];
            $player_info['is_alive'] = null;
            $player_info['ultimate_cd'] = $player_item['hero_cd'];
            $player_info['coordinate'] = null;
            $player_info['kills'] = $player_item['status'][0];
            $player_info['double_kill'] = null;
            $player_info['triple_kill'] = null;
            $player_info['ultra_kill'] = null;
            $player_info['rampage'] = null;
            $player_info['largest_multi_kill'] = null;
            $player_info['largest_killing_spree'] = null;
            $player_info['deaths'] = $player_item['status'][1];
            $player_info['assists'] = $player_item['status'][5];
            //计算选手信息
            $player_deaths = $player_info['deaths'] ? $player_info['deaths'] : 1;
            $player_info['kda'] = (String)round((($player_info['kills'] + $player_info['assists'])/$player_deaths),2);
            //participation
            $team_kills_num = empty($team_info_res[$player_rel_team_id]['kills']) ? 1 : $team_info_res[$player_rel_team_id]['kills'];
            $player_info['participation'] = (String)round((($player_info['kills'] + $player_info['assists'])/$team_kills_num),4);
            //last_hits
            $player_info['last_hits'] = $player_item['status'][2];
            $player_info['lane_creep_kills'] = null;
            $player_info['neutral_creep_kills'] = null;
            $player_info['neutral_minion_team_jungle_kills'] = null;
            $player_info['neutral_minion_enemy_jungle_kills'] = null;
            if ($duration >0){
                $player_info['lhpm'] = (string)round(($player_info['last_hits'] / $duration), 2);
            }else{
                $player_info['lhpm'] = '0';
            }
            $player_info['denies'] = $player_item['status'][3];
            $player_info['net_worth'] = $player_item['status'][4];
            $player_info['gold_earned'] = $player_item['status'][4];
            $player_info['gold_spent'] = $player_item['gold_spend'];
            $player_info['gold_remaining'] = $player_item['gold'];
            $player_info['gpm'] = (String)round(($player_info['gold_earned']/($duration/60)),2);
            //gold_earned_percent
            $team_gold_res = $team_info_res[$player_rel_team_id]['gold_earned'] ? $team_info_res[$player_rel_team_id]['gold_earned'] : 1;
            $player['gold_earned_percent'] = (String)round(($player_info['gold_earned'] / $team_gold_res),4) ;
            //experience  是否需要用 分均经验去求  $player_item['status'][7] * ($duration/60)
            //xpm
            $player_info['xpm'] = $player_item['status'][7];
            //items
            $player_info['items'] = $this->conversionPandascorePlayerItemsIds($redis,$battleId,$duration,$rel_player_id,$player_item['equipments'],$dotaItemsList,$dotaItemsRelationList,$item_unknown_id);

            //组合玩家的数据
            $player_new_array_res = array_merge($old_player,$player_info);
            $player_array_new[$rel_player_id] = $player_new_array_res;
            //计算队伍的信息
            $team_deaths += $player_item['deaths'];
            $team_assists += $player_item['assists'];
            //ban pick
            if (!$ban_pick_is_have){
                $team_ban_pick = [
                    "hero" => @$player_new_array_res['hero'],
                    "team" => (Int)$team_id,
                    "type" => 2,
                    "order" => null
                ];
                $ban_pick_array[$player_order] = @json_encode($team_ban_pick,320);
            }
            //整体的玩家order
            $player_order ++;
        }


        //保存ban_pick
        if (!empty($ban_pick_array) && !$ban_pick_is_have){
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'ban_pick'],$ban_pick_array);
        }
        //组合最新的数据
        $team_info_res[$team_info_new['radiant']['rel_identity_id']] = $team_info_new['radiant'];
        $team_info_res[$team_info_new['dire']['rel_identity_id']] = $team_info_new['dire'];
        //组合websocket数据
        $playerResVal = [];
        foreach ($player_array_new as $player_key => $player_res) {
            //组合websocket
            $websocket_player = [];
            $player_seed_id = (int)$player_res['seed'];
            $websocket_player['seed'] = $player_seed_id;
            $websocket_player['faction'] = (String)$player_res['faction'];
            $websocket_player['role'] = null;
            $websocket_player['lane'] = null;
            $websocket_player['player']['player_id'] = self::checkAndChangeInt($player_res['player_id']);
            $websocket_player['player']['nick_name'] = self::checkAndChangeString($player_res['nick_name']);
            $websocket_player['player']['steam_id'] = null;
            //获取英雄信息
            $hero = $this->getHeroInfoByRedisOrMemory($redis,$this->originId,$player_res['hero']);
            $websocket_player['hero'] = $hero;
            $websocket_player['level'] = (int)$player_res['level'];
            $websocket_player['is_alive'] = $player_info['is_alive'] == 1 ? true : false;
            $websocket_player['kills'] = (Int)$player_res['kills'];
            $websocket_player['deaths'] = (Int)$player_res['deaths'];
            $websocket_player['assists'] = (Int)$player_res['assists'];
            $websocket_player['kda'] = (String)$player_res['kda'];
            $websocket_player['participation'] = (String)$player_res['participation'];
            $websocket_player['last_hits'] = (Int)$player_res['last_hits'];
            $websocket_player['lhpm'] = (String)$player_res['lhpm'];

            $websocket_player['denies'] =  null;
            $websocket_player['items']['inventory']['slot_1'] =  null;
            $websocket_player['items']['inventory']['slot_2'] =  null;
            $websocket_player['items']['inventory']['slot_3'] =  null;
            $websocket_player['items']['inventory']['slot_4'] =  null;
            $websocket_player['items']['inventory']['slot_5'] =  null;
            $websocket_player['items']['inventory']['slot_6'] =  null;
            $websocket_player['items']['backpack']['slot_1'] =  null;
            $websocket_player['items']['backpack']['slot_2'] =  null;
            $websocket_player['items']['backpack']['slot_3'] =  null;
            $websocket_player['items']['neutral'] =  null;
            $websocket_player['items']['stash']['slot_1'] =  null;
            $websocket_player['items']['stash']['slot_2'] =  null;
            $websocket_player['items']['stash']['slot_3'] =  null;
            $websocket_player['items']['stash']['slot_4'] =  null;
            $websocket_player['items']['stash']['slot_5'] =  null;
            $websocket_player['items']['stash']['slot_6'] =  null;
            $websocket_player['items']['buffs'] =  null;
            $websocket_player['items']['mob_inventory']['slot_1'] =  null;
            $websocket_player['items']['mob_inventory']['slot_2'] =  null;
            $websocket_player['items']['mob_inventory']['slot_3'] =  null;
            $websocket_player['items']['mob_inventory']['slot_4'] =  null;
            $websocket_player['items']['mob_inventory']['slot_5'] =  null;
            $websocket_player['items']['mob_inventory']['slot_6'] =  null;
            $websocket_player['items']['mob_backpack']['slot_1'] =  null;
            $websocket_player['items']['mob_backpack']['slot_2'] =  null;
            $websocket_player['items']['mob_backpack']['slot_3'] =  null;
            $websocket_player['items']['mob_neutral'] =  null;

            //summoner_spells
            $advanced = [
                'talents' => null,
                'ultimate_cd' => null,
                'coordinate' => null,
                'is_visible' => null,
                'respawntimer' => null,
                'buyback_cooldown' => null,
                'buyback_cost' => null,
                'health' => null,
                'health_max' => null,
                'tower_kills' => null,
                'barrack_kills' => null,
                'melee_barrack_kills' => null,
                'ranged_barrack_kills' => null,
                'roshan_kills' => null,
                'double_kill' => null,
                'triple_kill' => null,
                'ultra_kill' => null,
                'rampage' => null,
                'largest_multi_kill' => null,
                'largest_killing_spree' => null,
                'lane_creep_kills' => null,
                'neutral_creep_kills' => null,
                'net_worth' => null,
                'net_worth_percent' => null,
                'gold_earned' => null,
                'gold_spent' => null,
                'gold_remaining' => null,
                'gold_reliable' => null,
                'gold_unreliable' => null,
                'gold_herokill' => null,
                'gold_creepkill' => null,
                'gpm' => null,
                'experience' => null,
                'xpm' => null,
                'damage_to_heroes' => null,
                'damage_to_heroes_hp_removal' => null,
                'damage_to_heroes_magical' => null,
                'damage_to_heroes_physical' => null,
                'damage_to_heroes_pure' => null,
                'dpm_to_heroes' => null,
                'damage_percent_to_heroes' => null,
                'damage_taken' => null,
                'damage_taken_hp_removal' => null,
                'damage_taken_magical' => null,
                'damage_taken_physical' => null,
                'damage_taken_pure' => null,
                'dtpm' => null,
                'damage_taken_percent' => null,
                'damage_conversion_rate' => null,
                'damage_to_buildings' => null,
                'damage_to_towers' => null,
                'damage_to_objectives' => null,
                'total_crowd_control_time' => null,
                'total_heal' => null,
                'total_runes_pickedup' => null,
                'bounty_runes_pickedup' => null,
                'double_damage_runes_pickedup' => null,
                'haste_runes_pickedup' => null,
                'illusion_runes_pickedup' => null,
                'invisibility_runes_pickedup' => null,
                'regeneration_runes_pickedup' => null,
                'arcane_runes_pickedup' => null,
                'smoke_purchased' => null,
                'smoke_used' => null,
                'dust_purchased' => null,
                'dust_used' => null,
                'observer_wards_purchased' => null,
                'observer_wards_placed' => null,
                'observer_wards_kills' => null,
                'sentry_wards_purchased' => null,
                'sentry_wards_placed' => null,
                'sentry_wards_kills' => null
            ];
            $websocket_player['advanced'] = $advanced;
            $websocket_team_players[$player_res['team_order']][$player_seed_id] = $websocket_player;
            //组成最终player数组
            $playerResVal[$player_key] = @json_encode($player_res,320);
        }
        //最终保存player
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'players'],$playerResVal);
        //获取一些信息  TODO
        $teamResVal = [];
        //team新信息
        foreach ($team_info_res as $team_key => $team_info_item){
            $match_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$team_info_item['team_id']);
            $team_info_base = @json_decode($match_team_info_redis,true);
            $team_opponent_order = @$team_info_item['order'];
            $teamInfo = [
                'team_id' => self::checkAndChangeInt(@$team_info_item['team_id']),
                'name' => self::checkAndChangeString(@$team_info_base['name']),
                'image' => self::checkAndChangeString(@$team_info_base['image_200x200']),
                'opponent_order' => self::checkAndChangeInt($team_opponent_order)
            ];
            //advanced
            $team_advanced = [
                'total_runes_pickedup' => null,
                'bounty_runes_pickedup' => null,
                'double_damage_runes_pickedup' => null,
                'haste_runes_pickedup' => null,
                'illusion_runes_pickedup' => null,
                'invisibility_runes_pickedup' => null,
                'regeneration_runes_pickedup' => null,
                'arcane_runes_pickedup' => null,
                'smoke_purchased' => null,
                'smoke_used' => null,
                'dust_purchased' => null,
                'dust_used' => null,
                'observer_wards_purchased' => null,
                'observer_wards_placed' => null,
                'observer_wards_kills' => null,
                'sentry_wards_purchased' => null,
                'sentry_wards_placed' => null,
                'sentry_wards_kills' => null
            ];
            $player_res_data = $websocket_team_players[$team_opponent_order];
            ksort($player_res_data);
            $player_res_data_ws = @array_values($player_res_data);
            //factions team的信息
            $faction_team = [
                'faction' => (String)$team_info_item['faction'],
                'team' => $teamInfo,
                'kills' => (int)$team_info_item['kills'],
                'deaths' => (int)@$team_info_item['deaths'],
                'assists' => (int)@$team_info_item['assists'],
                'net_worth' => null,
                'net_worth_diff' => null,
                'experience' => null,
                'experience_diff' => null,
                'tower_kills' => (int)@$team_info_item['tower_kills'],
                'barrack_kills' => (int)@$team_info_item['barrack_kills'],
                'melee_barrack_kills' => null,
                'ranged_barrack_kills' => null,
                'roshan_kills' => null,
                'building_status' => $team_info_item['building_status'],
                'players' => $player_res_data_ws,
                'advanced' => $team_advanced
            ];
            $winner_teams_socket_info[$team_opponent_order] = $teamInfo;
            $match_scores[$team_opponent_order] = $teamInfo;
            $match_scores[$team_opponent_order]['score'] = 0;
            $factions[] = $faction_team;
        }
        //battle 是否结束
        $is_battle_finished = false;
        $is_match_finished = false;
        $match_winner = null;
        $battle_winner = null;
        $match_real_time_info = $this->redis_hGetAll_Or_Memory($redis,['match_real_time_info']);
        if ($match_data['game']['finished'] && !empty($match_data['game']['winner_id'])){
            $is_battle_finished = true;
            $battle_winner_id = $match_data['game']['winner_id'];
            $winner_id = $winner_teams_info_res[$battle_winner_id];
            $battle_winner = $winner_teams_socket_info[$winner_id];
            //上面计算
            switch ($winner_id){
                case 1:
                    $match_real_time_info['team_1_score'] = (int) $match_real_time_info['team_1_score'] + 1;
                    $match_scores[1]['score'] = (int) $match_real_time_info['team_1_score'];
                    break;
                case 2:
                    $match_real_time_info['team_2_score'] = (int)  $match_real_time_info['team_2_score'] + 1;
                    $match_scores[2]['score'] = (int)  $match_real_time_info['team_2_score'];
                    break;
            }
            //battle的分数
            $team_info_res[$battle_winner_id]['score'] = 1;
            //计算结束时间
            $battle_base = [
                'duration'=>$duration,
                'end_at' => $socket_time,
                'status' => 3,
                'winner' => $winner_id
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battle_base);
            //计算比赛的胜算
            $statusInfo = WinnerHelper::lolWinnerInfo($match_real_time_info['team_1_score'],$match_real_time_info['team_2_score'],$matchInfo['number_of_games']);
            if($statusInfo['is_finish']==1){
                $is_match_finished = true;
                $match_real_time_info['status'] = 3;
                $match_real_time_info['end_at']= $socket_time;
                if ($statusInfo['is_draw'] == 1){//判断是不是平局
                    $match_real_time_info['winner'] = null;
                    $match_real_time_info['is_draw'] = 1;
                }else{
                    $match_real_time_info['winner'] = $statusInfo['winner_team'];
                    $match_winner = $winner_teams_socket_info[$statusInfo['winner_team']];
                }
            }
            $this->redis_hMSet_And_Memory($redis,['match_real_time_info'],$match_real_time_info);
        }else{
            $match_scores[1]['score'] = (int) $match_real_time_info['team_1_score'];
            $match_scores[2]['score'] = (int)  $match_real_time_info['team_2_score'];

            $base_data = [
                'duration'=>$duration
            ];
            $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$base_data);
        }
        $team_elites_Kills = [];
        //循环$team_info_res
        foreach ($team_info_res as $team_index => $team_val){
            //杀龙信息
            $team_elites_Kills[$team_index]['dragon_kills'] = (int)@$team_val['dragon_kills'];
            //组合队伍加密
            $teamResVal[$team_index] = @json_encode($team_val,320);
        }
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'teams'],$teamResVal);
        //计算 elites_status
        //精英怪状态
        $match_scores_res = array_values($match_scores);
        //组合数据
        $elites_status = ['roshan_status' => null];

        $transData = [
            'duration' => (Int)$duration,
            'is_battle_finished' => $is_battle_finished,
            'is_match_finished' => $is_match_finished,
            'match_scores' => $match_scores_res,
            'match_winner' => $match_winner,
            'battle_winner' => $battle_winner,
            'is_pause' => false,
            'is_live' => true,
            'time_of_day' => null,
            'elites_status' => $elites_status,
            'factions' =>$factions,
            'server_config' =>$server_config
        ];
        //$trans_data = json_encode($transData,320);
        $frames_data = [
            'trans_data' => $transData,
            'is_battle_end' => $is_battle_finished
        ];
        return $frames_data;
    }
    ///////////////// 处理事件 ///////////
    //BattleStart
    private function conversionBattleStart($redis,$redis_base_name,$matchInfo,$rel_matchId,$matchId,$battleId,$battle_order,$match_data,$event_id){
        $map_id = 1;//可以做查询
        $event_type = 'battle_start';
        $duration = -90;
        $battlesBase['game'] = 2;
        $battlesBase['match'] = $matchId;
        $battlesBase['status'] = 2;
        $battlesBase['is_draw'] = 2;
        $battlesBase['is_forfeit'] = 2;
        $battlesBase['is_default_advantage'] = 2;
        $battlesBase['map'] = $map_id;
        $battlesBase['duration'] = $duration;
        $battlesBase['is_battle_detailed'] = 1;
        $battlesBase['flag'] = 1;
        $battlesBase['deleted_at'] = null;
        $battlesBase['deleted'] = 2;
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'base'],$battlesBase);
        //detail
        $battlesDetail = [
            'is_confirmed' => 1,
            'is_pause' => 2,
            'is_live' => 1
        ];
        $this->redis_hMSet_And_Memory($redis,['current','battles',$battleId,'detail'],$battlesDetail);
        //获取dire队伍信息  1天辉，2夜魇
        $home_side = $match_data['home']['side'];
        if ($home_side == 1){
            //获取radiant队伍信息 天辉
            $radiant_team_id = self::getMainIdByRelIdentityId('team',$match_data['home']['id'],$this->originId);
            //获取$team_order
            $radiant_team_order = self::getTeamOrderByTeamId($radiant_team_id, $matchInfo);
            $radiant_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$radiant_team_id);
            $radiant_team_info = @json_decode($radiant_team_info_redis,true);
            //夜魇
            $dire_team_id = self::getMainIdByRelIdentityId('team',$match_data['away']['id'],$this->originId);
            //获取$team_order
            $dire_team_order = self::getTeamOrderByTeamId($dire_team_id, $matchInfo);
            $dire_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$dire_team_id);
            $dire_team_info = @json_decode($dire_team_info_redis,true);
        }else{
            //获取radiant队伍信息 天辉
            $radiant_team_id = self::getMainIdByRelIdentityId('team',$match_data['away']['id'],$this->originId);
            //获取$team_order
            $radiant_team_order = self::getTeamOrderByTeamId($radiant_team_id, $matchInfo);
            $radiant_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$radiant_team_id);
            $radiant_team_info = @json_decode($radiant_team_info_redis,true);
            //夜魇
            $dire_team_id = self::getMainIdByRelIdentityId('team',$match_data['home']['id'],$this->originId);
            //获取$team_order
            $dire_team_order = self::getTeamOrderByTeamId($dire_team_id, $matchInfo);
            $dire_team_info_redis = $this->redis_hGet_Or_Memory($redis,['teams_info'],$dire_team_id);
            $dire_team_info = @json_decode($dire_team_info_redis,true);
        }
        //组合数据
        $dire = [
            'team_id' => self::checkAndChangeInt(@$dire_team_info['id']),
            'name' => self::checkAndChangeString(@$dire_team_info['name']),
            'image' => self::checkAndChangeString(@$dire_team_info['image_200x200']),
            'opponent_order' => self::checkAndChangeInt($dire_team_order)
        ];
        //组合数据
        $radiant = [
            'team_id' => self::checkAndChangeInt(@$radiant_team_info['id']),
            'name' => self::checkAndChangeString(@$radiant_team_info['name']),
            'image' => self::checkAndChangeString(@$radiant_team_info['image_200x200']),
            'opponent_order' => self::checkAndChangeInt($radiant_team_order)
        ];
        //地图
        $map = $this->getMapInfoByRedisOrMemory($redis,$map_id);
        //获取事件的order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => (Int)$matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $duration,
            'position' => null,
            'killer' => (Int)$radiant['team_id'],
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => (Int)$dire['team_id'],
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => $duration,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type,
            'map' => $map,
            'radiant' => $radiant,
            'dire' => $dire
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,$this->originId,$this->gameId,'events',$matchId,$rel_matchId,$battleId,$battle_order,false,$event_type,$event_data_ws_res,$event_order);
        return true;
    }
    //BattleEnd
    private function conversionBattleEnd($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$end_data,$event_id){
        $event_type = 'battle_end';
        $ingame_timestamp = (Int)$end_data['duration'];
        $battle_winner = $end_data['battle_winner'];
        $winnerId = $battle_winner['team_id'];
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => $ingame_timestamp,
            'position' => null,
            'killer' => (Int)$winnerId,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type,
            'winner' => $battle_winner
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,$this->originId,$this->gameId,'events',$matchId,$rel_matchId,$battleId,$battle_order,true,$event_type,$event_data_ws_res,$event_order);
        return true;
    }
    //BattlePause
    private function conversionBattlePause($redis,$redis_base_name,$rel_matchId,$matchId,$battleId,$battle_order,$battle_is_pause_data,$ingame_timestamp,$event_id){
        //true 就是暂停
        if ($battle_is_pause_data){
            $event_type = 'battle_pause';
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'is_pause',1);
        }else{//解除暂停
            $event_type = 'battle_unpause';
            $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'detail'],'is_pause',2);
        }
        //获取event_order
        $eventNum = $this->redis_hLen_Or_Memory($redis,['current','battles',$battleId,'event_list']);
        if ($eventNum){
            $event_order = $eventNum + 1;
        }else{
            $event_order = 1;
        }
        //组合成事件
        //组合成事件
        $event_data_list = [
            'battle_id' => $battleId,
            'game' => 2,
            'match' => $matchId,
            'order' => $event_order,
            'event_id' => $event_id,
            'event_type' => $event_type,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'position' => null,
            'killer' => null,
            'killer_player_name' => null,
            'killer_player_rel_name' => null,
            'killer_champion_id' => null,
            'killer_faction' => null,
            'killer_ingame_obj_type' => null,
            'killer_sub_type' => null,
            'victim' => null,
            'victim_player_name' => null,
            'victim_player_rel_name' => null,
            'victim_champion_id' => null,
            'victim_faction' => null,
            'victim_ingame_obj_type' => null,
            'victim_sub_type' => null,
            'is_first_event' => 2,
            'first_event_type' => 0
        ];
        $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'event_list'],$event_order,@json_encode($event_data_list,320));
        //返回值
        $event_data_ws_res = [
            'match_id' => (Int)$matchId,
            'battle_id' => (Int)$battleId,
            'battle_order' => (Int)$battle_order,
            'ingame_timestamp' => (Int)$ingame_timestamp,
            'event_id' => (Int)$event_id,
            'event_type' => $event_type
        ];
        //最终保存信息
        $this->saveWsData($redis,$redis_base_name,$this->originId,$this->gameId,'events',$matchId,$rel_matchId,$battleId,$battle_order,true,$event_type,$event_data_ws_res,$event_order);
        return true;

    }
    //最终保存信息
    private function saveWsData($redis,$redis_base_name,$originId,$gameId,$socket_type,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_data_ws_res,$event_order){
        //插入到redis
        $redis->lPush($redis_base_name."battles:".$battleId.":ws_out_put_data",@json_encode($event_data_ws_res,320));
        //保存 api
        $this->refreshToApi($redis,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order);
        //接下来存数据库
        $insertData = [
            'match_data' =>@json_encode($event_data_ws_res,320),
            'match_id' => $matchId,
            'game_id' => $gameId,
            'origin_id' => $originId,
            'type' => $socket_type,
            'battle_order' => $battle_order
        ];
        //当前数据库连接
//        $this->saveDataBase($insertData);
        $this->saveKafka($insertData);
    }
    //执行ws to rest api
    private function refreshToApi($redis,$originId,$matchId,$rel_matchId,$battleId,$battle_order,$is_battle_finished,$event_type,$event_order){
        //判断是不是修复 并且比赛是不是结束
        if ($this->operate_type == 'xiufu') {
            if ($event_type == 'battle_end'){
                $match_status_check = $this->redis_hGet_Or_Memory($redis, ['match_real_time_info'], 'status');
                if ($match_status_check == 3) {
                    //插入到队列
                    $item = [
                        "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                            $originId, "", "", 2),
                        "type"     => '',
                        "batch_id" => date("YmdHis"),
                        "params"   => [
                            'matchId'      => $matchId,
                            'rel_matchId'  => $rel_matchId,
                            'battleId'     => $battleId,
                            'battleOrder'  => $battle_order,
                            'event_type'   => 'refresh_match_xiufu',
                            'event_order'  => 0
                        ]
                    ];
                    TaskRunner::addTask($item, 4);
                }
            }
            return true;
        }
        if (empty($this->run_type)){
            $run_type = 'socket';
        }else{
            $run_type = $this->run_type;
        }
        $nowTime = time();
        $refreshTime = $this->redis_Get_Or_Memory($redis,['current', 'battles', $battleId, 'ws_to_api']);
        //判断类型
        if ($event_type == 'frames'){
            if (!$is_battle_finished){
                $refreshTime = empty($refreshTime) ? 0 : (Int)$refreshTime;
                $diff_time = $nowTime - $refreshTime;
                if ($diff_time < 5) {
                    return true;
                }
            }
            //插入到队列
            $item = [
                "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                    $originId, "", "", $this->gameId),
                "type"     => '',
                "batch_id" => date("YmdHis"),
                "params"   => [
                    'matchId'      => $matchId,
                    'rel_matchId'      => $rel_matchId,
                    'battleId'      => $battleId,
                    'battleOrder'      => $battle_order,
                    'event_type'      => $event_type,
                    'event_order'      => 0,
                    'run_type'      => $run_type
                ],
            ];
            TaskRunner::addTask($item, 4);
            $this->redis_Set_And_Memory($redis,['current', 'battles', $battleId, 'ws_to_api'],$nowTime);
        }else{
            $event_type_array = ['battle_start','battle_end','battle_pause','battle_unpause','battle_reloaded','elite_announced','elite_spawned','player_spawned','player_kill','player_suicide','elite_kill','building_kill','ward_placed','ward_kill','ward_expired'];
            if (in_array($event_type,$event_type_array)){
                //插入到队列
                $item = [
                    "tag"      => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WS_TO_API,
                        $originId, "", "", $this->gameId),
                    "type"     => '',
                    "batch_id" => date("YmdHis"),
                    "params"   => [
                        'matchId'      => $matchId,
                        'rel_matchId'      => $rel_matchId,
                        'battleId'      => $battleId,
                        'battleOrder'      => $battle_order,
                        'event_type'      => $event_type,
                        'event_order'      => $event_order,
                        'run_type'      => $run_type
                    ],
                ];
                TaskRunner::addTask($item, 4);
            }
        }
        return true;
    }
    //转化装备
    private function conversionPandascorePlayerItemsIds($redis,$battleId,$ingame_timestamp,$rel_player_id,$playerRelItems,$dotaItemsList,$dotaItemsRelationList,$item_unknown_id){
        $playerItems = [];
        //1.首先解析装备的信息
        foreach ($playerRelItems as $rel_item){
            if ($rel_item){
                $rel_item_array = explode('/',$rel_item);
                $rel_item_end = end($rel_item_array);
                $rel_item_data = explode('.',$rel_item_end);
                $playerItems[] = $rel_item_data[0];
            }
        }
        //2.详细转化
        if ($playerItems){
            $need_change = 0;
            //当前这个player的装备信息 然后获取上一个装备信息列表，判断有没有变化
            $playersItemsRedis = $this->redis_hGet_Or_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$rel_player_id);
            if ($playersItemsRedis){
                $playersItemsBeforeLine = @json_decode($playersItemsRedis,true);
                $playersItemsBefore = @end($playersItemsBeforeLine)['items_modified'];
            }else{
                $playersItemsBeforeLine = $playersItemsBefore = [];
            }
            $playerOrnamentsInfo = [];
            $playerItemsNull = [];
            $playerItemsInfo = [];
            if ($playerItems){
                foreach ($playerItems as $key => $item_val){
                    if ($item_val){
                        $itemInfoId = $dotaItemsRelationList[$item_val];
                        if($itemInfoId){
                            if (!in_array($itemInfoId,$playersItemsBefore)){
                                $need_change = 1;
                            }
                            $itemInfoId_val = (Int)$itemInfoId;
                            //获取当前
                            $itemInfoResRedis = $dotaItemsList[$itemInfoId_val];
                            if ($itemInfoResRedis){
                                $itemInfoRes = @json_decode($itemInfoResRedis,true);
                                if ($itemInfoRes['is_trinket'] == 1 && isset($itemInfoRes['is_trinket'])){//饰品
                                    $playerOrnamentsInfo[$key]['item_id'] = $itemInfoId_val;
                                    $playerOrnamentsInfo[$key]['purchase_time'] = null;
                                    $playerOrnamentsInfo[$key]['cooldown'] = null;
                                }else{//道具
                                    $playerItemsInfo[$key]['total_cost'] = @$itemInfoRes['total_cost'];
                                    $playerItemsInfo[$key]['item_id'] = $itemInfoId_val;
                                    $playerItemsInfo[$key]['purchase_time'] = null;
                                    $playerItemsInfo[$key]['cooldown'] = null;
                                }
                            }else{//道具
                                $playerItemsInfo[$key]['total_cost'] = 1;
                                $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                                $playerItemsInfo[$key]['purchase_time'] = null;
                                $playerItemsInfo[$key]['cooldown'] = null;
                            }
                        }else{
                            $playerItemsInfo[$key]['total_cost'] = 1;
                            $playerItemsInfo[$key]['item_id'] = (Int)$item_unknown_id;
                            $playerItemsInfo[$key]['purchase_time'] = null;
                            $playerItemsInfo[$key]['cooldown'] = null;
                        }
                    }else{
                        $playerItemsNull[$key] = null;
                    }
                }
                $total_cost_array = array_column($playerItemsInfo,'total_cost');
                array_multisort($total_cost_array,SORT_DESC,$playerItemsInfo);
            }
            $playerItemsInfoResult = array_merge($playerItemsInfo,$playerItemsNull,$playerOrnamentsInfo);
            $playerItemsInfoResultEnd = [];
            $playerItemsInfoIdResultEnd = [];
            //循环
            foreach ($playerItemsInfoResult as $key_res => $val_res){
                $ItemsId = null;
                if ($val_res){
                    $ItemsId = @$val_res['item_id'];
                    unset($val_res['total_cost']);
                }
                $playerItemsInfoResultEnd[] = $val_res;
                $playerItemsInfoIdResultEnd[] = $ItemsId;
            }
            //需要变化则更新redis
            if ($need_change == 1){
                $playerItemsInfoArray = [
                    'ingame_timestamp'=>$ingame_timestamp,
                    'items'=>$playerItemsInfoIdResultEnd
                ];
                $playersItemsBeforeLine[] = $playerItemsInfoArray;
                $playerItemsLine =  @json_encode($playersItemsBeforeLine,320);
                $this->redis_hSet_And_Memory($redis,['current','battles',$battleId,'players_items_timeline'],$rel_player_id,$playerItemsLine);
            }
        }
        //设置返回数据
        $result = [
            'items' => $playerItemsInfoResultEnd,
            'items_timeline' => $playersItemsBeforeLine
        ];
        return $result;
    }
    //转化召唤师技能
    private function conversionPandascorePlayerSummonerSpellsIds($redis,$player_summoner_spells,$summonerSpellNameRelationIdList){
        $summoner_spells_data = [];
        foreach ($player_summoner_spells as $item) {
            $rel_summonerSpellId = $item['id'];
            $summonerSpellId = @$summonerSpellNameRelationIdList[$rel_summonerSpellId];
            if ($summonerSpellId){
                $spell_id = (Int)$summonerSpellId;
            }else{
                $spell_id = $this->getSummonerSpellUnknownId($redis);
            }
            $spell_array = [
                'spell_id' => $spell_id,
                'cooldown' => null
            ];
            $summoner_spells_data[] = $spell_array;
        }
        return $summoner_spells_data;
    }
    //召唤师技能 webscoket 信息
    private function getWebSocketPlayerSummonerSpellInfoList($summonerSpellInfoList,$player_summoner_spells){
        $player_summoner_spells_res = [];
        foreach ($player_summoner_spells as $val_s){
            $player_summoner_spell_info = [];
            $summoner_spell_id_is_unknown = $val_s['is_unknown'];
            if ($summoner_spell_id_is_unknown == 1){
                $player_summoner_spell_info['summoner_spell_id'] = (int)@$val_s['unknown_info']['id'];
                $player_summoner_spell_info['name'] = (String)@$val_s['unknown_info']['name'];
                $player_summoner_spell_info['name_cn'] = (String)@$val_s['unknown_info']['name_cn'];
                $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$val_s['unknown_info']['external_id']);
                $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$val_s['unknown_info']['external_name']);
                $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$val_s['unknown_info']['slug']);
                $summoner_spell_info_image = !empty(@$val_s['unknown_info']['image']) ? (string)@$val_s['unknown_info']['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                $player_summoner_spell_info['cooldown'] = null;
            }else{
                $summoner_spell_info_redis = @$summonerSpellInfoList[$val_s['spell_id']];
                if ($summoner_spell_info_redis){
                    $summoner_spell_info = @json_decode($summoner_spell_info_redis,320);
                    $player_summoner_spell_info['summoner_spell_id'] = (int)@$summoner_spell_info['id'];
                    $player_summoner_spell_info['name'] = (String)@$summoner_spell_info['name'];
                    $player_summoner_spell_info['name_cn'] = (String)@$summoner_spell_info['name_cn'];
                    $player_summoner_spell_info['external_id'] = self::checkAndChangeString(@$summoner_spell_info['external_id']);
                    $player_summoner_spell_info['external_name'] = self::checkAndChangeString(@$summoner_spell_info['external_name']);
                    $player_summoner_spell_info['slug'] = self::checkAndChangeString(@$summoner_spell_info['slug']);
                    $summoner_spell_info_image = !empty(@$summoner_spell_info['image']) ? (string)@$summoner_spell_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_summoner_spell_info['image'] = $summoner_spell_info_image;
                    $player_summoner_spell_info['cooldown'] = null;
                }
            }
            $player_summoner_spells_res[] = $player_summoner_spell_info;
        }
        return $player_summoner_spells_res;
    }
    //item websocket 信息
    private function getWebSocketPlayerItemsList($dotaItemsList,$player_items_rel){
        $player_items = [];
        foreach ($player_items_rel as $key_item => $val_item){
            $player_items_val = [];
            if ($val_item){
                $item_info_data = @$dotaItemsList[$val_item['item_id']];
                if ($item_info_data){
                    $item_info = @json_decode($item_info_data,true);
                    $player_items_val['item_id'] = (int)$item_info['id'];
                    $player_items_val['name'] = (string)$item_info['name'];
                    $player_items_val['name_cn'] = (string)$item_info['name_cn'];
                    $player_items_val['external_id'] = self::checkAndChangeString($item_info['external_id']);
                    $player_items_val['external_name'] = self::checkAndChangeString($item_info['external_name']);
                    $player_items_val['total_cost'] = (int)$item_info['total_cost'];
                    $player_items_val['is_trinket'] = @$item_info['is_trinket'] == 1 ? true : false;
                    $player_items_val['is_purchasable'] = @$item_info['is_purchasable'] == 1 ? true : false;
                    $player_items_val['slug'] = (string)$item_info['slug'];
                    $player_item_image = @$item_info['image'] ? @$item_info['image'].'?x-oss-process=image/resize,m_fixed,h_50,w_50' : null;
                    $player_items_val['image'] = $player_item_image;
                    $player_items_val['purchase_time'] = null;
                    $player_items_val['cooldown'] = null;
                }
            }
            $key_name_item = $key_item + 1;
            $player_items['slot_'.$key_name_item] = $player_items_val;
        }
        return $player_items;
    }

    //第一次事件
    public function getCommonFirstEventInfo($redis,$battleId,$killed_type,$faction){
        $event_first_redis_name = ['current','battles',$battleId,'event_first'];
        //记录事件
        $eventInfoRes = [
            'is_first_event' => false,
            'first_event_type' => null
        ];
        switch ($killed_type){
            case 'player':
                //记录一下击杀者的teams 个数
                //设置初始值
                $team_kills_key = 'team_kills_'.$faction;
                //查询是否是一血
                //$first_blood = $redis->hGet($event_first_redis_name,'first_blood');
                $first_blood = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_blood');
                if (!$first_blood){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_blood'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_blood',$faction);
                }
                //查询 first_to_5_kills first_to_10_kills
                //查询当前team击杀了多少人
                //查询是否五杀了
                $first_to_5_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_5_kills');
                $first_to_10_kills = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_to_10_kills');
                $team_kills_res = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,$team_kills_key);//击杀队伍
                if ($team_kills_res){
                    $now_team_kills = $team_kills_res + 1;
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,$now_team_kills);
                    if ($now_team_kills == 5 && empty($first_to_5_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_5_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_5_kills',$faction);
                    }
                    if ($now_team_kills == 10 && empty($first_to_10_kills)){
                        $eventInfoRes = [
                            'is_first_event' => true,
                            'first_event_type' => 'first_to_10_kills'
                        ];
                        $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_to_10_kills',$faction);
                    }
                }else{
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,$team_kills_key,1);
                }
                break;
            case 'rift_herald':
                $rift_herald = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_rift_herald');
                if (!$rift_herald){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_rift_herald'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_rift_herald',$faction);
                }
                break;
            case 'drake':
                $first_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_dragon');
                if (!$first_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_dragon',$faction);
                }
                break;
            case 'elder_dragon':
                $first_elder_dragon = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_elder_dragon');
                if (!$first_elder_dragon){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_elder_dragon'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_elder_dragon',$faction);
                }
                break;
            case 'baron_nashor':
                $first_baron_nashor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_baron_nashor');
                if (!$first_baron_nashor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_baron_nashor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_baron_nashor',$faction);
                }
                break;
            case 'tower':
                $first_turret = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_turret');
                if (!$first_turret){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_turret'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_turret',$faction);
                }
                break;
            case 'inhibitor':
                $first_inhibitor = $this->redis_hGet_Or_Memory($redis,$event_first_redis_name,'first_inhibitor');
                if (!$first_inhibitor){
                    $eventInfoRes = [
                        'is_first_event' => true,
                        'first_event_type' => 'first_inhibitor'
                    ];
                    $this->redis_hSet_And_Memory($redis,$event_first_redis_name,'first_inhibitor',$faction);
                }
                break;
            default:
                $eventInfoRes = [
                    'is_first_event' => false,
                    'first_event_type' => null
                ];
        }
        return $eventInfoRes;
    }
    //////////////////////////  获取比赛信息  /////////////////////////////////////
    //获取比赛ID
    public function getMatchIdAndRelMatchId($redis,$originId,$rel_match_id){
        $matchId = self::getMainIdByRelIdentityId('match', $rel_match_id, $originId);
        if ($this->memory_mode){
            $this->matchId = $matchId;
        }
        return $matchId;
    }
    //获取比赛信息
    public function getMatchInfo($redis,$matchId,$socket_time){
        if ($this->memory_mode && !empty($this->matchInfo)){
            return $this->matchInfo;
        }
        $matchInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":base_info";
        $matchInfo = $redis->hGetAll($matchInfoRedisName);
        if (!$matchInfo){
            //获取比赛信息
            $matchInfo = self::getMatchAllInfoByMatchId($matchId);
            if (empty($matchInfo)){
                return 'no_match_info';
            }
            $redis->hMSet($matchInfoRedisName,$matchInfo);
            //setMatchRealTimeInfo
            $this->getMatchRealTimeInfo($redis,$matchId,$socket_time);
            $this->getInitTeamInfo($redis,$matchId,$matchInfo);
        }
        //设置比赛
        if ($this->memory_mode){
            $this->matchInfo = $matchInfo;
        }
        return $matchInfo;
    }
    //设置team 各种信息
    public function getInitTeamInfo($redis,$matchId,$matchInfo){
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $matchTeamInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":teams_info";
        $teams_info = $redis->hGetAll($matchTeamInfoRedisName);
        if (!$teams_info){
            $Team1Info = Team::find()->where(['id'=>$matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])){
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team1Info['id'],@json_encode($Team1Info,320));
            $Team2Info = Team::find()->where(['id'=>$matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])){
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'],200,200);
            }
            $redis->hSet($matchTeamInfoRedisName,@$Team2Info['id'],@json_encode($Team2Info,320));
            $teams_info = [
                $Team1Info['id'] =>@json_encode($Team1Info,320),
                $Team2Info['id'] =>@json_encode($Team2Info,320)
            ];
        }
        if ($this->memory_mode){
            $this->teams_info = $teams_info;
            $this->setDataToMemory(['teams_info'],$teams_info);
        }
        return $teams_info;
    }
    //获取比赛信息
    public function getMatchRealTimeInfo($redis,$matchId,$socket_time)
    {
        if ($this->memory_mode && !empty($this->matchRealTimeInfo)) {
            return $this->matchRealTimeInfo;
        }
        //match_real_time_info
        $matchRealTimeInfoRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":".Consts::MATCH_REAL_TIME_INFO;
        $match_real_time_info = $redis->hGetAll($matchRealTimeInfoRedisName);
        if (!$match_real_time_info){
            $match_real_time_info = MatchRealTimeInfo::find()->where(['id'=>$matchId])->asArray()->one();
            if ($this->operate_type == 'xiufu') {
                $match_real_time_info['team_1_score'] = 0;
                $match_real_time_info['team_2_score'] = 0;
                $match_real_time_info['winner'] = null;
                $match_real_time_info['status'] = 2;
            }
            if (empty(@$match_real_time_info['begin_at'])){
                $match_real_time_info['begin_at'] = $socket_time;
            }
            $redis->hMSet($matchRealTimeInfoRedisName,$match_real_time_info);
        }
        //存内存
        if ($this->memory_mode){
            $this->matchRealTimeInfo = $match_real_time_info;
        }
        return $match_real_time_info;
    }
    //设置比赛状态
    public function setMatchStatus($redis,$matchId){
        if ($this->memory_mode){
            $match_status = $this->match_status;
            if ($match_status){
                return true;
            }
            $this->match_status = 2;
        }
        $matchStatusRedisName = Consts::MATCH_ALL_INFO.":".$matchId.":match_status";
        $match_status = $redis->get($matchStatusRedisName);
        if (!$match_status){
            $MatchRealTimeInfo = MatchRealTimeInfo::find()->where(['id'=>$matchId])->one();
            if ($MatchRealTimeInfo){
                $MatchRealTimeInfo->setAttribute('status',2);
                $MatchRealTimeInfo->save();
                $redis->set($matchStatusRedisName,2);
            }
        }
        return true;
    }
    //获取地图信息
    private function getMapInfoByRedisOrMemory($redis,$map_id){
        $map = null;
        if ($this->memory_mode && !empty($this->map)){
            return $this->map;
        }
        $mapInfoRedis = $redis->hGet(Consts::GAME_MAPS.":dota",$map_id);
        if ($mapInfoRedis){
            $mapInfo = @json_decode($mapInfoRedis,true);
            $map_is_default = @$mapInfo['is_default'] == 1 ? true : false;
            $map_image = [
                'square_image' => null,
                'rectangle_image' => null,
                'thumbnail' => (String)@$mapInfo['image']
            ];
            $map = [
                'map_id' => (Int)@$mapInfo['id'],
                'name' => (String)@$mapInfo['name'],
                'name_cn' => self::checkAndChangeString(@$mapInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$mapInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$mapInfo['external_name']),
                'short_name' => self::checkAndChangeString(@$mapInfo['short_name']),
                'map_type' => self::checkAndChangeString(@$mapInfo['map_type']),
                'map_type_cn' => self::checkAndChangeString(@$mapInfo['map_type_cn']),
                'is_default' => $map_is_default,
                'slug' => self::checkAndChangeString(@$mapInfo['slug']),
                'image' => $map_image
            ];
        }
        if ($this->memory_mode){
            $this->map = $map;
        }
        return $map;
    }
    //获取游戏版本号
    private function getGameVersion($redis,$matchId){
        //服务器设置游戏版本
        $match_game_version = $redis->hGetAll(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version");
        if (empty(@$match_game_version['game_version'])){
            $game_version = null;
            $now_time = time();
            if ($match_game_version['get_time']){
                $get_game_version_time_cha = $now_time - $match_game_version['get_time'];
                if ($get_game_version_time_cha > 300){
                    $MatchBaseInfo = MatchBase::find()->select('game_version')->where(['match_id'=>$matchId])->asArray()->one();
                    $game_version = @$MatchBaseInfo['game_version'];
                    $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'game_version',$game_version);
                    $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'get_time',$now_time);
                }
            }else{
                $MatchBaseInfo = MatchBase::find()->select('game_version')->where(['match_id'=>$matchId])->asArray()->one();
                $game_version = @$MatchBaseInfo['game_version'];
                $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'game_version',$game_version);
                $redis->hSet(Consts::MATCH_ALL_INFO.':'.$matchId.":match_game_version",'get_time',$now_time);
            }
        }else{
            $game_version = @$match_game_version['game_version'];
        }
        return $game_version;
    }
    /////////////////  基础信息   /////////////////////
    //获取英雄信息
    private function getHeroInfoByRedisOrMemory($redis,$metadata_origin_id,$hero_id){
        if ($this->memory_mode && !empty($this->match_all_champion_websocket[$hero_id])){
            return $this->match_all_champion_websocket[$hero_id];
        }
        $hero = null;
        $heroInfoRedis = $redis->hGet('dota2:hero:list:'.$metadata_origin_id,$hero_id);
        if ($heroInfoRedis){
            $heroInfo = @json_decode($heroInfoRedis,true);
            if (empty($heroInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $heroInfo['image']."?x-oss-process=image/resize,m_fixed,h_144,w_256";
                $small_image_rel = $heroInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $hero = [
                'hero_id' => self::checkAndChangeInt(@$heroInfo['id']),
                'name' => self::checkAndChangeString(@$heroInfo['name']),
                'name_cn' => self::checkAndChangeString(@$heroInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$heroInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$heroInfo['external_name']),
                'title' => self::checkAndChangeString(@$heroInfo['title']),
                'title_cn' => self::checkAndChangeString(@$heroInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$heroInfo['slug']),
                'image' => $image
            ];
        }else{
            $UnknownInfoRedis = $redis->get("dota2:hero:list:unknown");
            $heroInfo = @json_decode($UnknownInfoRedis,true);
            if (empty($heroInfo['image'])){
                $image_rel = null;
                $small_image_rel = null;
            }else{
                $image_rel = $heroInfo['image']."?x-oss-process=image/resize,m_fixed,h_144,w_256";
                $small_image_rel = $heroInfo['small_image']."?x-oss-process=image/resize,m_fixed,h_32,w_32";
            }
            $image = [
                'image' => $image_rel,
                'small_image' => $small_image_rel
            ];
            $hero = [
                'hero_id' => self::checkAndChangeInt(@$heroInfo['id']),
                'name' => self::checkAndChangeString(@$heroInfo['name']),
                'name_cn' => self::checkAndChangeString(@$heroInfo['name_cn']),
                'external_id' => self::checkAndChangeString(@$heroInfo['external_id']),
                'external_name' => self::checkAndChangeString(@$heroInfo['external_name']),
                'title' => self::checkAndChangeString(@$heroInfo['title']),
                'title_cn' => self::checkAndChangeString(@$heroInfo['title_cn']),
                'slug' => self::checkAndChangeString(@$heroInfo['slug']),
                'image' => $image
            ];
        }
        if ($this->memory_mode){
            $this->match_all_champion_websocket[$hero_id] = $hero;
        }
        return $hero;
    }
    //获取英雄 relation id 信息
    public function getChampionRelationIdList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->dota2_hero_relation_id_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('dota2:hero:relation:'.$originId);
        if ($this->memory_mode) {
            return $this->dota2_hero_relation_id_list = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的符文ID
    private function getChampionUnknownId($redis){
        $champion_unknown_redis = $redis->get('dota2:hero:list:unknown');
        $champion_unknown_info = @json_decode($champion_unknown_redis,true);
        return @$champion_unknown_info['id'];
    }
    //获取dota 英雄ID
    private function getdota2HeroId($redis,$metadata_origin_id,$rel_championId){
        $championRelationIdList = $this->getChampionRelationIdList($redis,$metadata_origin_id);
        $champions_id = @$championRelationIdList[$rel_championId];
        if (!$champions_id){
            $champions_id = $this->getChampionUnknownId($redis);
        }
        return (Int)$champions_id;
    }
    //获取dota Items Relation
    public function getdotaItemsList($redis,$originId)
    {
        if ($this->memory_mode) {
            $strValue = $this->dota_items_list;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll("dota2:items:list:".$originId);
        if ($this->memory_mode) {
            $this->dota_items_list = $returnValue;
        }
        return $returnValue;
    }
    //获取dota Items Relation
    public function getdotaItemsRelationList($redis,$originId)
    {
        if ($this->memory_mode) {
            $strValue = $this->dota_items_relation;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll("dota2:items:relation:".$originId);
        if ($this->memory_mode) {
            return $this->dota_items_relation = $returnValue;
        }
        return $returnValue;
    }
    //获取未知的装备ID
    private function getdotaItemsUnknownId($redis){
        if ($this->memory_mode) {
            $strValue = $this->dota_item_unknown_id;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $item_unknown_id = null;
        $item_unknown_redis = $redis->get('dota2:items:list:unknown');
        $item_unknown_info = @json_decode($item_unknown_redis,true);
        $item_unknown_id = @$item_unknown_info['id'];
        if ($this->memory_mode) {
            $this->dota_item_unknown_id = $item_unknown_id;
        }
        return $item_unknown_id;
    }
    //召唤师技能 name relation id
    public function getSummonerSpellRelationIdList($redis,$originId){

        if ($this->memory_mode) {
            $strValue = $this->dota_summoner_spell_relation_id_list[$originId];
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('dota2:summoner_spell:relation:'.$originId);
        if ($this->memory_mode) {
            return $this->dota_summoner_spell_relation_id_list[$originId] = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能 列表
    public function getSummonerSpellInfoList($redis,$originId){
        if ($this->memory_mode) {
            $strValue = $this->dota_summoner_spell_list[$originId];
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('dota2:summoner_spell:list:'.$originId);
        if ($this->memory_mode) {
            return $this->dota_summoner_spell_list[$originId] = $returnValue;
        }
        return $returnValue;
    }
    //召唤师技能
    private function getSummonerSpellUnknownId($redis){
        $redisNameList = "dota2:summoner_spell:list:unknown";
        $UnknownInfoRedis = $redis->get($redisNameList);
        $UnknownInfo = @json_decode($UnknownInfoRedis,true);
        $summoner_spell_id = @$UnknownInfo['id'];
        return $summoner_spell_id;
    }
    //获取battle的队伍信息
    private function getBattleTeamsInfoFromMemory($redis,$battleId){
        $teams_Id_Info_Array = [];
        $teams_Info_Redis = $this->redis_hGetTeamAll_Or_Memory($redis,['current','battles',$battleId,'teams']);
        if ($teams_Info_Redis){
            foreach ($teams_Info_Redis as $key => $val){
                $val_array = @json_decode($val,true);
                $teams_Id_Info_Array[$key] = @$val_array;
            }
        }
        return $teams_Id_Info_Array;
    }
    //获取EnumIngameGoal信息
    private function getEnumIngameGoalInfo($redis){
        if ($this->memory_mode) {
            $strValue = $this->enum_ingame_goal_info;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $returnValue = $redis->hGetAll('enum:ingame_goal');
        if ($this->memory_mode) {
            if (!empty($returnValue)){
                $this->enum_ingame_goal_info = $returnValue;
            }
        }
        return $returnValue;
    }
    //////////////////////////    redis 和 内存      ///////////////////////////////////
    //获取数组 key
    public function redis_hKeys_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_keys($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hKeys($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组 key
    public function redis_hVals_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return array_values($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hVals($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue) {
                $this->setDataToMemory($keys, $returnAllValue);
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetTeamAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 2){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取数组
    public function redis_hGetPlayersAll_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue) && count($strValue) == 10 ){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGetAll($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取hash 单条
    public function redis_hGet_Or_Memory($redis,$keys,$field)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString][$field];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hGet($keyString,$field);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
//            if (array_key_exists($keyString, $this->current)){
//                $this->current[$keyString][$field] = $returnValue;
//            }else{
//                if (!empty($returnValue)){
//                    $this->current[$keyString][$field] = $returnValue;
//                }
//            }
            //重新赋值所有
            $allData = $redis->hGetAll($keyString);
            if ($allData){
                $this->current[$keyString] = $allData;
            }
        }
        return $returnValue;
    }
    //获取 get 单条
    public function redis_Get_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return $strValue;
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->get($keyString);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $this->current[$keyString] = $returnValue;
            }else{
                if (!empty($returnValue)){
                    $this->current[$keyString] = $returnValue;
                }
            }
        }
        return $returnValue;
    }
    //获取长度
    public function redis_hLen_Or_Memory($redis,$keys)
    {
        if ($this->memory_mode) {
            $keyString  = implode("_", $keys);
            if (array_key_exists($keyString, $this->current)){
                $strValue = $this->current[$keyString];
                if (!empty($strValue)){
                    return count($strValue);
                }
            }
        }
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $returnValue = $redis->hLen($keyString);
        if ($this->memory_mode) {
            $returnAllValue = $redis->hGetAll($keyString);
            if ($returnAllValue){
                $this->setDataToMemory($keys,$returnAllValue);
            }
        }
        return $returnValue;
    }
    //设置缓存 内存
    public function redis_hMSet_And_Memory($redis,$keys,$values)
    {
        if ($values){
            $matchId = $this->matchId;
            $keyString = implode(":", $keys);
            $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
            $redis->hMSet($keyString,$values);
            if ($this->memory_mode) {
                $memory_values = $redis->hGetAll($keyString);
                $keyString = implode("_", $keys);
                $this->current[$keyString] = $memory_values;
            }
        }
        return true;
    }
    //设置缓存 内存
    public function redis_hSet_And_Memory($redis,$keys,$filedKey,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->hSet($keyString,$filedKey,$values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString][$filedKey] = $values;
        }
        return true;
    }
    //设置缓存 内存
    public function redis_Set_And_Memory($redis,$keys,$values)
    {
        $matchId = $this->matchId;
        $keyString = implode(":", $keys);
        $keyString = sprintf(Consts::MATCH_ALL_INFO.":%s:%s", $matchId, $keyString);
        $redis->set($keyString, $values);
        if ($this->memory_mode) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $values;
        }
        return true;
    }
    //设置缓存 单独设置 内存
    public function setDataToMemory($keys,$value){
        if ($this->memory_mode && $value) {
            $keyString = implode("_", $keys);
            $this->current[$keyString] = $value;
        }
        return true;
    }
    //保存到DB
    public function saveDataBase($data){
        //当前数据库连接
        $db_websoket = DbSocket::getDb()->createCommand();
        //单条插入
        $db_websoket->insert('match_data', $data);
        $db_websoket->execute();
        return true;
    }
    //保存到Kafka
    public function saveKafka($data){
        if ($this->operate_type == 'xiufu'){
            return true;
        }
        //todo 最终上线修改
//        $MatchSocketData = new MatchSocketData();
//        $MatchSocketData->setAttributes($data);
//        $MatchSocketData->save();

        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_DOTA2'),$data);
    }
    //清空redis和内存
    public function delRedisAndMemory($redis,$origin_id,$rel_match_id,$match_id){
        //内存清空
        $this->current = [];
        //清空redis
        //删除关联关系
        $redis_match_dota_relation = Consts::MATCH_RELATION_ORIGIN_MATCH.":dota2:".$origin_id;
        $redis->hdel($redis_match_dota_relation,$rel_match_id);
        //新的结构删除
        $match_all_info_list = $redis->Keys('match_all_info:'.$match_id."*");
        if ($match_all_info_list) {
            foreach ($match_all_info_list as $item_mai) {
                $redis->del($item_mai);
            }
        }
        return true;
    }
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}