<?php


namespace app\modules\task\services\wsdata\video;


class ParseText
{
    public static function parsePostion($prismWordsInfos)
    {
        $dataInfo = [];
        if(! $prismWordsInfos){
            return false;
        }

        foreach ($prismWordsInfos as $prismWordsInfo){
            //双方战队
            if($prismWordsInfo['pos']['0']['x'] >= 489 && $prismWordsInfo['pos']['0']['x'] <= 492
                && $prismWordsInfo['pos']['0']['y']>=5 && $prismWordsInfo['pos']['0']['y']<=6
                && $prismWordsInfo['pos']['2']['x'] >= 551 && $prismWordsInfo['pos']['2']['x'] <= 553
                && $prismWordsInfo['pos']['2']['y']>=28 && $prismWordsInfo['pos']['2']['y']<=31)
            {
                $dataInfo['team_info'][] = $prismWordsInfo;
            }
            if($prismWordsInfo['pos']['0']['x'] >= 1374 && $prismWordsInfo['pos']['0']['x'] <= 1378
                && $prismWordsInfo['pos']['0']['y']>=8 && $prismWordsInfo['pos']['0']['y']<=10
                && $prismWordsInfo['pos']['2']['x'] >= 1408 && $prismWordsInfo['pos']['2']['x'] <= 1413
                && $prismWordsInfo['pos']['2']['y']>=28 && $prismWordsInfo['pos']['2']['y']<=32)
            {
                $dataInfo['team_info'][] = $prismWordsInfo;
            }
            //比分
            if($prismWordsInfo['pos']['0']['x'] >= 923 && $prismWordsInfo['pos']['0']['x'] <= 928
                && $prismWordsInfo['pos']['0']['y']>=21 && $prismWordsInfo['pos']['0']['y']<=26
            && $prismWordsInfo['pos']['2']['x'] >= 1005 && $prismWordsInfo['pos']['2']['x'] <= 1011
                && $prismWordsInfo['pos']['2']['y']>=42 && $prismWordsInfo['pos']['2']['y']<=46)
            {
                $dataInfo['score_info'] = $prismWordsInfo;
            }
            //双方经济
            if($prismWordsInfo['pos']['0']['x'] >= 724 && $prismWordsInfo['pos']['0']['x'] <= 727
                && $prismWordsInfo['pos']['0']['y']>=13 && $prismWordsInfo['pos']['0']['y']<=16
                && $prismWordsInfo['pos']['2']['x'] >= 811 && $prismWordsInfo['pos']['2']['x'] <= 814
                && $prismWordsInfo['pos']['2']['y']>=33 && $prismWordsInfo['pos']['2']['y']<=35)
            {
                $dataInfo['money'][] = $prismWordsInfo;
            }
            if($prismWordsInfo['pos']['0']['x'] >= 1108 && $prismWordsInfo['pos']['0']['x'] <= 1111
                && $prismWordsInfo['pos']['0']['y']>=13 && $prismWordsInfo['pos']['0']['y']<=16
                && $prismWordsInfo['pos']['2']['x'] >= 1195 && $prismWordsInfo['pos']['2']['x'] <= 1198
                && $prismWordsInfo['pos']['2']['y']>=33 && $prismWordsInfo['pos']['2']['y']<=35)
            {
                $dataInfo['money'][] = $prismWordsInfo;
            }
//            //选手
//            if($prismWordsInfo['pos']['0']['x'] >= 0 && $prismWordsInfo['pos']['0']['x'] <= 3
//                && $prismWordsInfo['pos']['0']['y']>=128 && $prismWordsInfo['pos']['0']['y']<=134
//                && $prismWordsInfo['pos']['2']['x'] >= 59 && $prismWordsInfo['pos']['2']['x'] <= 62
//                && $prismWordsInfo['pos']['2']['y']>=147 && $prismWordsInfo['pos']['2']['y']<=150)
//            {
//                $dataInfo['players'][] = $prismWordsInfo;
//            }
//            if($prismWordsInfo['pos']['0']['x'] >= 835 && $prismWordsInfo['pos']['0']['x'] <= 1140
//                && $prismWordsInfo['pos']['0']['y']>=122 && $prismWordsInfo['pos']['0']['y']<=135
//                && $prismWordsInfo['pos']['2']['x'] >= 898 && $prismWordsInfo['pos']['2']['x'] <= 1205
//                && $prismWordsInfo['pos']['2']['y']>=145 && $prismWordsInfo['pos']['2']['y']<=150)
//            {
//                $dataInfo['players'][] = $prismWordsInfo;
//            }
//            if($prismWordsInfo['pos']['0']['x'] >= 1856 && $prismWordsInfo['pos']['0']['x'] <= 1856
//                && $prismWordsInfo['pos']['0']['y']>=132 && $prismWordsInfo['pos']['0']['y']<=135
//                && $prismWordsInfo['pos']['2']['x'] >= 898 && $prismWordsInfo['pos']['2']['x'] <= 1205
//                && $prismWordsInfo['pos']['2']['y']>=145 && $prismWordsInfo['pos']['2']['y']<=150)
//            {
//                $dataInfo['players'][] = $prismWordsInfo;
//            }

        }
        return  $dataInfo;
    }
}