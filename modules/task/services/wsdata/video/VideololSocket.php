<?php

namespace app\modules\task\services\wsdata\video;

use app\modules\match\models\MatchLived;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketBattleService;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketEventService;
use app\modules\task\services\wsdata\bayes\BayesCsgoSocketMatchService;
use app\modules\task\services\wsdata\WebsocketCommon;

class VideololSocket extends WebsocketCommon
{
    private static $instance;
    public $currentPre = "video_lol_ws";
    public $battle_order_relation_id = [];
    public $matchId = null;
    public $origin_id = 12;
    public $rel_matchId = null;
    public $map = null;
    public $matchInfo = null;
    public $matchRealTimeInfo = null;
    public $rel_order_id = null;
    public $now_battle_order = null;
    public $socket_id = null;
    public $socket_time = null;
    public $match_status = null;
    public $battleId = null;
    public $battle_order = null;
    public $team_name_id_relation = null;
    public $teams_info = null;
    public $player_name_id_relation = null;
    public $player_info = null;
    public $current = [];
    public $battles = null;
    public $memory_mode = false;
    public $is_reload = false;
    public $match_all_champion_websocket = [];
    public $match_data = '';
    //其它
    public $lol_champion_relation_id_list = [];
    public $lol_items_relation = [];
    public $lol_items_list = [];
    public $lol_item_unknown_id = null;
    public $lol_champion_list = [];
    public $lol_runes_list = [];
    public $lol_runes_relation_list = [];
    public $lol_summoner_spell_name_relation_id_list = [];
    public $lol_summoner_spell_list = [];
    public $enum_ingame_goal_info = [];
    public $enum_socket_game_configure = [];
    public $match_socket_sub_type = null;
    public $operate_type = null;
    //单例 初始化
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function run($tagInfo, $taskInfo)
    {
        $self               = self::getInstance();
        $self->refresh_task = true;
        $refreshType        = @$tagInfo['ext2_type'];
        $refreshType        = $refreshType ? $refreshType : "one";
        $info               = json_decode($taskInfo['params'], true);
        $self->info         = $info;
        $originId           = $info['origin_id'];
        //match
        $self->perId = @$info['match_id'];

        if ($refreshType == 'refresh') {
            $self->matchId       = $info['match_id'];
            $rel_match_id        = $info['rel_match_id'];
            $rel_match_id_string =$self::getMatchIdByVideo($info['rel_match_id']);
        } else {
            $match_id_list   = self::getMatchIdByPerId($self->perId, $originId);
            $self->relMatchId = @$match_id_list['rel_identity_id'];
            $self->matchId   = @$match_id_list['matchId'];
        }

        if (empty($self->matchId) || $originId != $self->origin_id) {
            return false;
        }

        if ($refreshType == 'refresh') {
//            $relMatchId = $info['rel_match_id'];
            try {
                //删除
                $redisDelKey = $self->currentPre . ':' . $self->matchId . ':';
                $self->batchDelRedisKey($redisDelKey);
                $self->clearBattleMatchId($self->matchId);
                $self->match = VideololSocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
                if (empty($self->match) || $self->matchfg == 'no_match_info') {
                    return false;
                }
            }catch (\Throwable $e) {
                $self->error_catch_log(['msg' => $e->getMessage()]);
            }

            $livedObj = MatchLived::find()->where(['and', ['game_id' => 2], ['match_id' => (string)$rel_match_id_string]]);
            $count    = $livedObj->count();
            $limit    = 2000;
            if ($count > $limit) {
                $countSlice = (int)ceil($count / $limit);
                for ($i = 0; $i < $countSlice; $i++) {
                    $offset    = 0 + $limit * $i;
                    $list      = $livedObj->orderBy('id')->offset($offset)->limit($limit)->asArray()->all();
                    $countList = count($list);
                    foreach ($list as $k => $v) {
//                        $start_time = date('Y-m-d H:i:s');//test
                        $matchDetail = json_decode($self->getValue(['current','match','details']),true);
                        if($matchDetail['end_at'] && $matchDetail['status'] == 3){
                            $redisExpireKey = $self->currentPre .':' . $self->matchId . ':';
                            $self->error_catch_log(['match_end' => true]);
                            $self->batchExpireRedisKey($redisExpireKey);
                            return false;
                        }
//                        $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
//                        if ($matchEnd) {
//                            self::changeHotData(7,1,$self->matchId);//调用hotRefresh
//                            $self->error_catch_log(['match_end' => true]);
//                            return false;
//                        }

                        if ($k + 1 == $countList) {
                            $self->refresh = true;
                            $self->error_catch_log(['end_refresh' => true]);
                        }
                        $self->info              = $v;
                        $self->info['socket_id'] = $v['id'];
                        $self->match_data        = json_decode($v['match_data'], true);
                        $self->doRun();
//                        $end_time = date('Y-m-d H:i:s');//test
//                        echo 'startTime--'.$start_time.'--endTime--'.$end_time.'--id--'.$self->info['socket_id'].PHP_EOL;//test
                    }
                }
            } else {
                $list      = $livedObj->orderBy('socket_time')->limit($limit)->asArray()->all();
                $countList = count($list);
                foreach ($list as $k => $v) {
                    $matchEnd = $self->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_END_AT]);
                    if ($matchEnd) {
//                        self::changeHotData(7,1,$self->matchId);//调用hotRefresh
                        $self->error_catch_log(['match_end1' => true]);
                        return false;
                    }
                    if ($k + 1 == $countList) {
                        $self->refresh = true;
                        $self->error_catch_log(['end_refresh' => true]);

                    }
                    $self->info              = $v;
                    $self->info['socket_id'] = $v['id'];
                    $self->match_data        = json_decode($v['match_data'], true);
                    $self->doRun();
                }
            }
        } else {

            $self->match = BayesCsgoSocketMatchService::getMatchInfoOrInitMatch($self, $self->matchId);
            if (empty($self->match) || $self->match == 'no_match_info') {
                return false;
            }
            $self->match_data = $info['match_data'];
            if (empty($info['match_data'])) {
                return false;
            }
            $self->doRun();
        }
    }

    public function doRun()
    {
        try {
            $prismWordsInfos = $this->match_data['text']['prism_wordsInfo'];
            $dataInfo = ParseText::parsePostion($prismWordsInfos);
            //结束后的操作
            $matchDetail = json_decode($this->getValue(['current','match','details']),true);
            if($matchDetail['end_at'] && $matchDetail['status'] == 3){
                $has_expire = $this->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE]);
                if (!$has_expire){
                    $redisDelKey = $this->currentPre .':' . $this->matchId . ':';
                    $this->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_MATCH, self::KEY_TAG_HAS_EXPIRE],1);
                    $this->batchExpireRedisKey($redisDelKey);
                }
                $this->error_hash_log('match end after events',['event_id'=>$this->info['socket_id']]);
                return  false;
            }

            $this->redis->incr('speed:' . $this->matchId);
            $match_data = $this->match_data;

//            $bayesRedisBattleId     = BayesCsgoSocketBattleService::getBayesBattleId($this);
//            $battleId               = BayesCsgoSocketBattleService::getBayesRealBattleId($this);
//            $battleOrder            = BayesCsgoSocketBattleService::getBayesBattleOrder($this);
//            $battleList             = BayesCsgoSocketBattleService::getBayesBattleList($this);
//
//            $roundId                = BayesCsgoSocketBattleService::getBayesBattleRoundId($this);
//            $roundOrder             = BayesCsgoSocketBattleService::getBayesBattleRoundOrder($this);
////            $roundList             = BayesCsgoSocketBattleService::getBayesBattleList($this);
//
//            $tableMapList           = BayesCsgoSocketMatchService::getTableCsgoMapList($this);
//            $tableWeaponList        = BayesCsgoSocketMatchService::getTableCsgoWeaponList($this);
            // 对局内时间戳
            $this->battle_timestamp = $this->currentBattleBeginAt();
            //是否是冻结时间
            $this->is_freeze_time   = $this->isFreezeTime();
//            //是否是暂停时间
//            $this->isPause          = $this->isPause();
//            $this->battleOrder      = $battleOrder;
//            $this->battleId         = $battleId;
//            $this->roundId          = $roundId;
//            $this->roundOrder       = $roundOrder;
//            $this->battleUrn        = $liveDataMatchUrn;
//            $this->duration         = $battleList[$liveDataMatchUrn] ? (json_decode($battleList[$liveDataMatchUrn], true) ? json_decode($battleList[$liveDataMatchUrn], true)['duration'] : 0) : 0;
//            $this->tableMapList     = $tableMapList;
//            $this->tableWeaponList  = $tableWeaponList;
            $matchScores =
            $transScores = explode('×',$dataInfo['score_info']['word']);
            $team_list_key = [self::KEY_TAG_TABLE, self::KEY_TAG_TEAM, self::KEY_TAG_LIST];
            $redis_team_list = $this->hGetAll($team_list_key);
            $team_1_info = json_decode($redis_team_list[$dataInfo['team_info']['0']['word']],true);
            $team_2_info = json_decode($redis_team_list[$dataInfo['team_info']['1']['word']],true);
            $tranMatchScoresData[] = [
                'team_id' => $team_1_info['id'],
                'name' => $team_1_info['name'],
                'image' => $team_1_info['image'],
                'opponent_order' => 1,
                'score' => $transScores['0'],
            ];
            $tranMatchScoresData[] = [
                'team_id' => $team_2_info['id'],
                'name' => $team_2_info['name'],
                'image' => $team_2_info['image'],
                'opponent_order' => 2,
                'score' => $transScores['1'],
            ];
            $framesData = [
                'match_id'=> $this->matchId,
                'game_rules' => 'normal',
                'match_type' => 'best_of',
                'number_of_games' => $this->match['number_of_games'],
                "match_scores" => $tranMatchScoresData
            ];
            $handle_data = [
                'type' => 'frame',
                'data' => $framesData
            ];
            //空内容不返回
            if (empty($handle_data)) {
                return false;
            }
            if ($this->needOutPutFrame) {
                $this->lPush([self::KEY_TAG_HISTORY, 'battle',$this->battleId,self::KEY_TAG_FRAMES], json_encode($this->needOutPutFrame, 320));
                //存入socket
                $data_ws = [
                    'match_data' => json_encode($this->needOutPutFrame, 320),
                    'type'       => 'frames',
                    'game_id'    => 1,
                    'match_id'   => $this->matchId
                ];
                $this->saveSocketToDb($data_ws);
                $this->needOutPutFrame = '';
            }

            if ($handle_data['type'] == 'frame') {
                $data = $handle_data['data'];
                $this->lPush([self::KEY_TAG_HISTORY, 'battle', $this->battleId, self::KEY_TAG_FRAMES], json_encode($data, 320));
                //存入socket
                $data_ws     = [
                    'match_data' => json_encode($data, 320),
                    'type'       => 'frame',
                    'game_id'    => 1,
                    'match_id'   => $this->matchId
                ];
                $this->event = $handle_data['data'];
                $this->saveSocketToDb($data_ws);
            } else {
                if(isset($handle_data['data']) && $handle_data['data']){
                    $data = $handle_data['data'];
                    $this->lPush(['history', 'battle', $this->battleId, 'events'], json_encode($data, 320));
                    //存入socket
                    $data_ws = [
                        'match_data' => json_encode($data, 320),
                        'type'       => 'events',
                        'game_id'    => 1,
                        'match_id'   => $this->matchId
                    ];
                    $this->saveSocketToDb($data_ws);
                }else{ //二维数组
                    foreach ($handle_data as $value){
                        $data = $value['data'];
                        $this->lPush(['history', 'battle', $this->battleId, 'events'], json_encode($data, 320));
                        //存入socket
                        $data_ws = [
                            'match_data' => json_encode($data, 320),
                            'type'       => 'events',
                            'game_id'    => 1,
                            'match_id'   => $this->matchId
                        ];
                        $this->saveSocketToDb($data_ws);
                    }
                }
            }

            $this->saveSocketToDb($data_ws);
            $this->refreshToApi();


        } catch (Throwable $e) {

            $this->error_catch_log(['msg' => $e->getMessage()]);
        }

    }
    //matchid转换方法
    public static function getMatchIdByVideo($rel_match_id)
    {
        return $rel_match_id;
    }
    
}