<?php
namespace app\modules\task\services\wsdata\video;

use app\modules\common\models\EnumIngameGoal;
use app\modules\common\services\ImageConversionHelper;
use app\modules\match\models\Match as MatchModel;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\metadata\models\MetadataCsgoWeapon;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\wsdata\WebsocketCommon;
use Throwable;
use yii\base\Exception;

class VideololSocketMatchService extends WebsocketCommon
{
    static private $instance;
    public $matchId = "";
    public $match = [];
    public $event = [];
    public $frame = [];
    public $match_data = [];
    public $info = [];
    public $currentPre = "video_csgo_ws";
    public $muti = '';
    public $battleId = '';
    public $relMatchId = '';
    const ORIGIN_ID = 9;

    /**
     * getMatchInfo
     * @param $model
     * @param $matchId
     * @return array|bool|\yii\db\ActiveRecord|null
     */
    public static function getMatchInfoOrInitMatch($model,$matchId)
    {
        if (empty($model)){
            $model = new VideololSocket();
        }
        if ($matchId == 0) {
            return false;
        }
        $redis_res = $model->hGetAll(['table','match' ,$matchId]);
        if ($redis_res) {
            return $redis_res;
        } else {
            $match = MatchModel::find()->where(['id' => $matchId])->asArray()->one();
            if (empty($match)){
                return 'no_match_info';
            }
            $model->hMSet(['table','match' ,$matchId], $match);
            self::setMatchInfo($model,$matchId, 'init',$match);
            return $match;
        }
    }

    public static function getMatchDetails($model){
        if (empty($model)){
            $model = new VideololSocket();
        }
        $key = ['current','match','details'];
        return json_decode($model->getValue($key),true);
    }

    public static function setMatchDetails($model,$updateArr){
        if (!is_array($updateArr)){
            return false;
        }
        if (empty($model)){
            $model = new VideololSocket();
        }
        $oldDetails = self::getMatchDetails($model);
        if (!empty($oldDetails)){
            $update = array_merge($oldDetails,$updateArr);
        }else{
            $initDetails = self::getMatchBase($model);
            $update = array_merge($initDetails,$updateArr);
        }
        $key = ['current','match','details'];
        if (is_array($update)){
            $update = json_encode($update,320);
        }
        $model->setValue($key,$update);
    }

    public static function getMatchBase($model){
        if (empty($model)){
            $model = new VideololSocket();
        }
        $baseArr = [
            'team_1_score'=>0,
            'team_2_score'=>0,
        ];
        return $baseArr;
    }

    public static function setMatchInfo($model,$matchId,$type,$matchInfo)
    {
        if (empty($model)){
            $model = new VideololSocket();
        }
        if ($type=='init'){
            self::initTeams($model, $matchInfo);
            self::initPlayers($model, $matchInfo);
        }

    }





    /**
     * 初始化选手
     * @param $model
     * @param $matchInfo
     * @return array
     */
    public static function initPlayers($model, $matchInfo)
    {
        if (empty($model)){
            $model = new VideololSocket();
        }
        $playerRedisKey = [self::KEY_TAG_TABLE, self::KEY_TAG_PLAYER, self::KEY_TAG_LIST];
        $player_info    = $model->hGetAll($playerRedisKey);
        if (empty($player_info)) {
            $team_ids    = [$matchInfo['team_1_id'], $matchInfo['team_2_id']];
            $playersInfo = TeamSnapshot::find()->alias('ts')->select('p.id,p.nick_name,ts.team_id')
                ->leftJoin('team_player_relation as tpr', 'ts.team_id = tpr.team_id')
                ->leftJoin('player as p', 'p.id = tpr.player_id')
                ->where(['ts.relation_id' => $matchInfo['tournament_id']])
                ->andWhere(['in', 'ts.team_id', $team_ids])
                ->asArray()->all();
            //循环玩家
            foreach ($playersInfo as $key => $val) {
                if ($val['id']){
                    $player_info[@$val['id']] = @json_encode($val, 320);
                }
            }
            $model->hMSet($playerRedisKey, $player_info);
        }
        return $player_info;
    }


    /**
     * 初始化team信息
     * @param $model
     * @param $matchInfo
     * @return array
     */
    public static function initTeams($model, $matchInfo)
    {
        //根据比赛的team_1_id team_2_id 获取队伍信息
        $teamRedisKey = [self::KEY_TAG_TABLE, self::KEY_TAG_TEAM, self::KEY_TAG_LIST];
        $teams        = $model->hGetAll($teamRedisKey);
        $teams_info = null;
        if (empty($teams)) {
            $Team1Info = Team::find()->where(['id' => $matchInfo['team_1_id']])->asArray()->one();
            if (!empty($Team1Info['image'])) {
                $Team1Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team1Info['image'], 200, 200);
            }
            $model->hSet($teamRedisKey, $Team1Info['short_name'], json_encode($Team1Info, 320));
            $Team2Info = Team::find()->where(['id' => $matchInfo['team_2_id']])->asArray()->one();
            if (!empty($Team2Info['image'])) {
                $Team2Info['image_200x200'] = ImageConversionHelper::showFixedSizeConversion($Team2Info['image'], 200, 200);
            }
            $model->hSet($teamRedisKey, @$Team2Info['short_name'], @json_encode($Team2Info, 320));
            $teams_info = [
                $Team1Info['short_name'] => @json_encode($Team1Info, 320),
                $Team2Info['short_name'] => @json_encode($Team2Info, 320)
            ];
        }
        return $teams_info;
    }

    public static function transFrames($model,$matchId,$battleId,$data_payload){

        if (!empty($data_payload)) {
            if (empty($model)) {
                $model = new BayesCsgoSocket();
            }
            // 数据表比赛信息
            $matchInfo = $model->match;
            // 对局时长
            $battleDuration = $model->getCurrentBattleTimestamp($model->battle_timestamp,$model->info['socket_time']);
            // 实时比赛（比分，开始时间，比赛状态，比赛获胜方战队）
            $matchDetails = self::getMatchDetails($model);
            //是否输出,为true时输出
            $isOutFrame = true;
            //frame的时间间隔(60秒内的不刷)
            $lastFrameTime = $model->getValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, 'frame','time']);
            if($lastFrameTime){
                $diffTime = time() - strtotime($lastFrameTime);
                if($diffTime < 60){
                    if($data_payload['gameEnded'] == true){
                        $isOutFrame = true;
                    }elseif ($matchDetails['status'] == 3){
                        $isOutFrame = true;
                    }
                    else{
                        $isOutFrame = false;
                    }
                }
            }
            $model->setValue([self::KEY_TAG_CURRENT, self::KEY_TAG_BATTLE, 'frame','time'],date("Y-m-d H:i:s",time()));
            // 战队信息
            $teamsFormat = BayesCsgoSocketTeamService::getTeamListFormat($model,'all');
            // 实时阵营（t/ct）信息
            $startingCtJson = $model->getValue([self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,'team_ct','details']);
            $startingTJson = $model->getValue([self::KEY_TAG_CURRENT,self::KEY_TAG_BATTLE,'team_t','details']);
            $startingCt = json_decode($startingCtJson,true);
            $startingT = json_decode($startingTJson,true);
            $startingCtInfo = $startingCt;
            $startingTInfo = $startingT;
            unset($startingT['teamUrn']);
            unset($startingCt['teamUrn']);
            // 炸弹是否安放 存入redis
            if(isset($data_payload['currentRoundState']['bombState']['plantedAt'])){
                // 炸弹倒计时 时间
                $time_since_plant = (int)substr($data_payload['currentRoundState']['timeRemaining'],0,-3);
                $bombInfo = [
                    'is_bomb_planted' => true,
                    'time_since_plant' => $time_since_plant,
                    'player_urn' => $data_payload['currentRoundState']['bombState']['plantingPlayerUrn'],
                    'defused' => $data_payload['currentRoundState']['bombState']['defused'],
                ];
                // 每个回合是否安放炸弹记录
                $model->setBombState($model,$model->battleId,$data_payload['currentRoundNumber'],json_encode($bombInfo));
            }
            //游戏内的时间（秒）
            if($model->is_freeze_time){ //冻结时间内
                $in_round_timestamp = 0;
                $round_time = $data_payload['roundTimeDurationSeconds'];
            }else{ //非冻结时间
                $round_time = (int)substr($data_payload['currentRoundState']['timeRemaining'],0,-3);  //剩余时间
                if($data_payload['currentRoundState']['bombState']['planted'] == false){
                    $in_round_timestamp = $data_payload['roundTimeDurationSeconds'] - (int)substr($data_payload['currentRoundState']['timeRemaining'],0,-3);
                }elseif($data_payload['currentRoundState']['bombState']['planted'] == true){
                    $in_round_timestamp = strtotime($data_payload['currentRoundState']['bombState']['plantedAt']) - strtotime($data_payload['currentRoundState']['startTime'])
                    +   (int)substr($data_payload['currentRoundState']['timeRemaining'],0,-3);
                }
            }

            // battle结果的数据
            $winnerTeam = null;
            $currentTeamsSide = BayesCsgoSocketEventService::getCurrentRoundTeamsSide($model);
            if($currentTeamsSide['current_ct']['teamUrn'] == $data_payload['currentRoundState']['winningTeamUrn']){
                unset($currentTeamsSide['current_ct']['teamUrn']);
                $winnerTeam = $currentTeamsSide['current_ct'];
            } elseif ($currentTeamsSide['current_t']['teamUrn'] == $data_payload['winningTeamUrn']){
                unset($currentTeamsSide['current_t']['teamUrn']);
                $winnerTeam = $currentTeamsSide['current_t'];
            }
            //比赛结果的数据
            $winnerMatchTeam = null;
            $teamListFormat = BayesCsgoSocketTeamService::getTeamListFormat($model,'urnField');
            if($matchDetails['status'] == 3){
                if($matchDetails['winner'] == 1){
                    $winnerTeamId = $matchInfo['team_1_id'];
                }
                if($matchDetails['winner'] == 2){
                    $winnerTeamId = $matchInfo['team_2_id'];
                }
                foreach ($teamListFormat as $v){
                    if($v['team_id'] == $winnerTeamId){
                        $winnerMatchTeam = $v;
                    }
                }
            }
            // frame结构
            $frameInfo = [
                'match_id' => (int)$model->matchId,
                'game_rules' => @Common::getGameRulesStringByNum('common',$matchInfo['game_rules']),
                'match_type' => @Common::getMatchTypeStringByNum('common',$matchInfo['match_type']),
                'number_of_games' => (int)$matchInfo['number_of_games'],
                'battle_id' => (int)$model->battleId,
                'battle_order' => (int)$model->battleOrder,
                'map' => BayesCsgoSocketBattleService::getMap($model,$data_payload['map']),
                'duration' => $battleDuration,
                'is_battle_finished' => $data_payload['gameEnded'],
                'is_match_finished' => $matchDetails['status'] == 3 ? true : false,
                'match_scores' => BayesCsgoSocketMatchService::getScoreFormat($model,$teamsFormat),
                'match_winner' => $winnerMatchTeam, // 获胜信息
                'battle_winner' => $winnerTeam, // 对局获胜战队
                'starting_ct' => $startingCt, // 开局作为反恐精英
                'starting_t' => $startingT, // 开局作为恐怖分子
                'is_pause' => $model->isPause, // 是否暂停
                'is_live' => null, // 是否比赛正式数据
                'current_round' => $data_payload['currentRoundNumber'], //当前回合
                'is_freeze_time' => $model->is_freeze_time, // 是否冻结时间
                'in_round_timestamp' => $in_round_timestamp, // 回合内时间戳
                'round_time' => $round_time, // 回合内时间
                'is_bomb_planted' => $data_payload['currentRoundState']['bombState']['planted'], // 炸弹是否已安放
                'time_since_plant' => $time_since_plant ?? null, // 安放炸弹后时间
                'side' => self::getFramesFactions($model,$data_payload,$teamsFormat,[$startingCtInfo,$startingTInfo]), // 阵营
                'server_config' => [], // 服务器设置
            ];
            // 推出结果数据
            $frameStructure = [
                'type' => 'frame',
                'data' => $frameInfo,
            ];
            //更新historyBattle的详情
            $battleUpdate['duration'] = (int)$battleDuration;
            $battleUpdate['is_pause'] = $frameInfo['is_pause'];
            $battleUpdate['is_live']  = $frameInfo['is_live'];
            BayesCsgoSocketBattleService::setBattleDetailsByUrn($model,$model->battleUrn,$battleUpdate);
            // 存入当前frame
            $model->setValue([self::KEY_TAG_CURRENT,'battle',"frame"],json_encode($frameInfo,320));
            //如果是最后END_MAP
            if($model->getValue([self::KEY_TAG_CURRENT,self::CSGO_EVENT_INFO_TYPE_EVENTS,'type']) == "END_MAP"){
                $model->battleEndClear($model);
            }
            if($isOutFrame == true){
                return $frameStructure;
            }else{
                return false;
            }
        }
    }


    public static function getScoreFormat($model, $teamFormat)
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        $data = [];
        foreach ($teamFormat as $k=>$v){
            $vNew['team_id']        = $v['team_id'];
            $vNew['name']           = $v['name'];
            $vNew['image']          = $v['image'];
            $vNew['opponent_order'] = $v['order'];
            $vNew['score']          = $v['score'];
            $data[] = $vNew;
        }
        return $data;
    }
    /**
     * frame阵营内容
     * @param $model
     * @param $data_payload
     * @param $teamFormat
     * @return array
     */
    public static function getFramesFactions($model, $data_payload, $teamFormat, $teamsInfo=[])
    {
        if (!$model) {
            $model = new BayesCsgoSocket();
        }
        // 战队比分
        $teamsScore = [
            $data_payload['teamOne']['liveDataUrn'] => $data_payload['teamOne']['score'],
            $data_payload['teamTwo']['liveDataUrn'] => $data_payload['teamTwo']['score'],
        ];
        // 数据源两个战队所有player数据 (当前round下的选手统计 - 待验证)
        $playerStatesData = $data_payload['currentRoundState']['playerStates'];
//        foreach ($playerStatesData as $oriPlayerId=>$oriPlayerInfo)
//        {}
        // 处理主队选手
        foreach ($data_payload['teamOne']['players'] as $oriTeamPlayerOne)
        {
            $oriTeamPlayerOneInfo = [
                'side' => $data_payload['currentRoundState']['teamOne']['side'],
                'rel_nick_name' => $oriTeamPlayerOne['name'],
                'steam_id' => $oriTeamPlayerOne['references']['STEAM_ID'],
                'kills' => $oriTeamPlayerOne['kills'], //击杀总数
                'team_kills' => $oriTeamPlayerOne['friendlyKills'], // 误杀队友
//                'team_kills' => $oriTeamPlayerOne['enemyKills'], // 击杀敌人
                'kd_diff' => $oriTeamPlayerOne['kills'] - $oriTeamPlayerOne['deaths'], // 击杀死亡差
                'deaths' => $oriTeamPlayerOne['deaths'], // 死亡
                'assists' => $oriTeamPlayerOne['assists'], // 助攻
                'headshot_kills' => $oriTeamPlayerOne['headshots'], // 爆头击杀
            ];
            BayesCsgoSocketPlayerService::setPlayerInfo($model,$oriTeamPlayerOne['liveDataUrn'],$oriTeamPlayerOneInfo);
        }
        // 处理客队选手
        foreach ($data_payload['teamTwo']['players'] as $oriTeamPlayerTwo)
        {
            $oriTeamPlayerTwoInfo = [
                'side' => $data_payload['currentRoundState']['teamOne']['side'],
                'rel_nick_name' => $oriTeamPlayerTwo['name'],
                'steam_id' => $oriTeamPlayerTwo['references']['STEAM_ID'],
                'kills' => $oriTeamPlayerTwo['kills'], //击杀总数
                'team_kills' => $oriTeamPlayerTwo['friendlyKills'], // 误杀队友
                'kd_diff' => $oriTeamPlayerTwo['kills'] - $oriTeamPlayerTwo['deaths'], // 击杀死亡差
                'deaths' => $oriTeamPlayerTwo['deaths'], // 死亡
                'assists' => $oriTeamPlayerTwo['assists'], // 助攻
                'headshot_kills' => $oriTeamPlayerTwo['headshots'], // 爆头击杀
            ];
            BayesCsgoSocketPlayerService::setPlayerInfo($model,$oriTeamPlayerTwo['liveDataUrn'],$oriTeamPlayerTwoInfo);
        }

        // 取出处理好的当前所有players数据
        $currentPlayers = self::getFramesPlayers($model);
        // 最后结果数据
        $currentTeamsSides = [];
        // 处理sides下结构
        foreach ($teamsInfo as $k=>$teamInfo){
            // 定义结构
            $currentTeamSide = [
                'side' => null,
                'team' => [],
                'score' => null,
                'players' => [],
                'rounds_history' => [],
            ];
            $teamUrn = $teamInfo['teamUrn'];
            unset($teamInfo['teamUrn']);
            // 战队阵营
            if($k){
                $currentTeamSide['side'] = 'terrorists';
            }else{
                $currentTeamSide['side'] = 'ct';
            }
            // 战队
            $currentTeamSide['team'] = $teamInfo;
            // 比分
            $currentTeamSide['score'] = $teamsScore[$teamUrn];
            // 阵营选手
            $currentTeamSide['players'] = $currentPlayers[$teamUrn];
            $currentTeamsSides[] = $currentTeamSide;
        }
        return $currentTeamsSides;
    }

    public static function formatFrameTeam($model,$team){
        $newTeam = [];
        $newTeam['team_id'] = $team['team_id'];
        $newTeam['name'] = $team['name'];
        $newTeam['image'] = $team['image'];
        $newTeam['opponent_order'] = $team['order'];
        return $newTeam;
    }

    public static function getBuildingStatusOnFrame($model,$data_payload){
        if (!$model) {
            $model = new Dota2Socket();
        }
        $top_outpost['is_captured']               = false;
        $top_outpost['health']                    = 100;
        $top_outpost['health_max']                = 100;
        $top_outpost['armor']                     = 100;
        $returnData['outposts']['top_outpost']    = $top_outpost;
        $returnData['outposts']['bot_outpost']    = $top_outpost;
        $top_tier_1_tower['is_alive']             = true;
        $top_tier_1_tower['health']               = 100;
        $top_tier_1_tower['health_max']           = 100;
        $top_tier_1_tower['armor']                = 100;
        $returnData['towers']['top_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_2_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_3_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_2_tower'] = $top_tier_1_tower;
        $returnData['towers']['mid_tier_3_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_1_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_2_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_3_tower'] = $top_tier_1_tower;
        $returnData['towers']['top_tier_4_tower'] = $top_tier_1_tower;
        $returnData['towers']['bot_tier_4_tower'] = $top_tier_1_tower;
        $returnData['barracks']['top_ranged_barrack'] = $top_tier_1_tower;
        $returnData['barracks']['top_melee_barrack'] = $top_tier_1_tower;
        $returnData['barracks']['mid_ranged_barrack'] = $top_tier_1_tower;
        $returnData['barracks']['mid_melee_barrack'] = $top_tier_1_tower;
        $returnData['barracks']['bot_ranged_barrack'] = $top_tier_1_tower;
        $returnData['barracks']['bot_melee_barrack'] = $top_tier_1_tower;
        $returnData['ancient'] = $top_tier_1_tower;
        return $returnData;

    }


    public static function dealFramePlayer($model, $framePlayer, $faction, $teamUrn)
    {
        if (!$model) {
            $model = new Dota2Socket();
        }
        $teamDetails = [];
        $durationNum = $model->duration / 60;
        $durationNum = $durationNum > 0 ? $durationNum : 1;
        foreach ($framePlayer as $k => $v) {
            $teamTempDetails['kills']     = +$v['kills'];
            $teamTempDetails['net_worth'] = +$v['net_worth'];
        }
        foreach ($framePlayer as $k => $v) {
            $playerDetails['faction']        = $faction;
            $playerDetails['rel_nick_name']  = $v['playerName'];
            $playerDetails['steam_id']       = $v['references']['STEAM_ID_64'];
            $playerDetails['heroId']         = $v['heroId'];
            $playerDetails['heroName']       = $v['heroName'];
            $playerDetails['itemsInventory'] = $v['itemsInventory'];//物品栏
            $playerDetails['itemsBackpack']  = $v['itemsBackpack'];//背包
            $playerDetails['itemsStash']     = $v['itemsStash'];//储藏处
            $playerDetails['itemsBear']      = $v['itemsBear'];//召唤物
            $playerDetails['itemNeutral']    = $v['itemNeutral'];//野怪掉落
            $playerDetails['abilities']      = $v['abilities'];//技能
            $playerDetails['coordinate']     = implode(',', $v['position']);//	坐标
            $playerDetails['level']          = $v['level'];//等级
            $playerDetails['is_alive']       = $v['alive'];//存活状态
            $playerDetails['kills']          = $v['kills'];//击杀
            $playerDetails['deaths']         = $v['deaths'];//死亡
            $playerDetails['assists']        = $v['assists'];//助攻
            //计算
            $death                              = $v['deaths'] > 0 ? $v['deaths'] : 1;
            $teamTempDetails['kills']           = $teamTempDetails['kills'] > 0 ? $teamTempDetails['kills'] : 1;
            $teamTempDetails['net_worth']       = $teamTempDetails['net_worth'] > 0 ? $teamTempDetails['net_worth'] : 1;
            $playerDetails['kda']               = (string)round(($v['kills'] + $v['assists']) / $death, 2);//kda
            $playerDetails['participation']     = (string)round(($v['kills'] + $v['assists']) / $teamTempDetails['kills'], 2);//participation
            $playerDetails['lhpm']              = (string)round(($v['lastHits'] / $durationNum), 2);//lhpm 选手补刀/（对局时长（秒）/60）
            $playerDetails['net_worth_percent'] = round(($v['net_worth'] / $teamTempDetails['net_worth']), 4);
            $playerDetails['gpm']               = round(($v['gold_earned'] / $durationNum), 2);//
            $playerDetails['xpm']               = round(($v['xp'] / $durationNum), 2);//

            $playerDetails['last_hits']      = $v['lastHits'];//	补刀
            $playerDetails['denies']         = $v['denies'];//	反补
            $playerDetails['gold_remaining'] = $v['goldCurrent'];//	当前金钱
            $playerDetails['gold_earned']    = $v['goldEarned'];//	金钱获取
            $playerDetails['gold_spent ']    = $v['goldEarned'] - $v['goldCurrent'];//	金钱花费

            $playerDetails['net_worth']             = $v['networth'];//	财产总和
            $playerDetails['experience']            = $v['xp'];//	经验
            $playerDetails['total_heal']            = $v['healingDone'];//	治疗量
            $playerDetails['total_runes_pickedup']  = $v['runesPickedUp'];//	拾取符文
            $playerDetails['buyback_cost']          = $v['buybackCost'];//买活花费
            $playerDetails['buyback_cooldown']      = $v['buybackCooldown'];//买活CD
            $playerDetails['roshan_kills']          = $v['roshanKills'];//击杀肉山
            $playerDetails['health']                = $v['health'];//	当前血量
            $playerDetails['health_max']            = $v['healthMax'];//总血量
            $playerDetails['is_visible']            = $v['visibleByEnemy'];//	敌人是否不可见
            $playerDetails['gold_reliable']         = $v['reliableGold'];//可靠金钱
            $playerDetails['gold_herokill']         = $v['heroKillGold'];//击杀英雄获取的金钱
            $playerDetails['gold_creepkill']        = $v['creepKillGold'];//补刀获取的金钱
            $playerDetails['gold_unreliable']       = $v['unreliableGold'];//不可靠金钱
            $playerDetails['tower_kills']           = $v['towerKills'];//摧毁防御塔
            $playerDetails['largest_killing_spree'] = $v['bestKillStreak'];//最大连杀
            //保存选手数据
            Dota2SocketPlayerService::setPlayerInfo($model, $v['liveDataUrn'], $playerDetails);
            //处理team数据
            $teamDetails['deaths']                += $v['deaths'];//死亡
            $teamDetails['assists']               += $v['assists'];//助攻
            $teamDetails['last_hits']             += $v['lastHits'];//	补刀
            $teamDetails['denies']                += $v['denies'];//	反补
            $teamDetails['gold_remaining']        += $v['goldCurrent'];//	当前金钱
            $teamDetails['gold_earned']           += $v['goldEarned'];//	金钱获取
            $teamDetails['net_worth']             += $v['networth'];//	财产总和
            $teamDetails['experience']            += $v['xp'];//	经验
            $teamDetails['total_heal']            += $v['healingDone'];//	治疗量
            $teamDetails['total_runes_pickedup']  += $v['runesPickedUp'];//	拾取符文
            $teamDetails['buyback_cost']          += $v['buybackCost'];//买活花费
            $teamDetails['buyback_cooldown']      += $v['buybackCooldown'];//买活CD
            $teamDetails['health']                += $v['health'];//	当前血量
            $teamDetails['health_max']            += $v['healthMax'];//总血量
            $teamDetails['is_visible']            += $v['visibleByEnemy'];//	敌人是否不可见
            $teamDetails['gold_reliable']         += $v['reliableGold'];//可靠金钱
            $teamDetails['gold_herokill']         += $v['heroKillGold'];//击杀英雄获取的金钱
            $teamDetails['gold_creepkill']        += $v['creepKillGold'];//补刀获取的金钱
            $teamDetails['gold_unreliable']       += $v['unreliableGold'];//不可靠金钱
            $teamDetails['tower_kills']           += $v['towerKills'];//摧毁防御塔
            $teamDetails['largest_killing_spree'] += $v['bestKillStreak'];//最大连杀
        }
        Dota2SocketTeamService::setTeamInfo($model, $teamUrn, $teamDetails);
        //返回待处理的team数据
        $teamNeedDealData['net_worth']  = $teamDetails['net_worth'];
        $teamNeedDealData['experience'] = $teamDetails['experience'];
        return $teamNeedDealData;
    }

    public static function dealFrameTeam($model,$team){

        $playerDetails['kills']  = $team['heroKills'];
        $playerDetails['tower_kills']       = $team['towerKills'];
        $playerDetails['barrack_kills']         = $team['barracksKills'];
        $playerDetails['roshan_kills']       = $team['roshanKills'];
        Dota2SocketTeamService::setTeamInfo($model, $team['liveDataUrn'], $playerDetails);

    }

    // 取出当前所有players数据
    public static function getFramesPlayers($model){
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $players = [];
        $currentRedisPlayers = BayesCsgoSocketPlayerService::getPlayerList($model);
        if (!empty($currentRedisPlayers)) {
            foreach ($currentRedisPlayers as $playerJson){
                $playerInfo = json_decode($playerJson, true);
                // 处理player结构
                $currentPlayerInfo = self::getFormatFramesFactionPlayer($model,$playerInfo);
//                $playerInfo['rel_team_id'] = $currentPlayerInfo;
                $players[$playerInfo['rel_team_id']][] = $currentPlayerInfo;
            }
        }
        return $players;
    }

    /**
     * @param $model
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getTableCsgoMapList($model)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['table','map','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $where=  ['=','state',1];
            $dbResult = MetadataCsgoMap::find()->where($where)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $v['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'], 88, 64);
                $newArr[$v['external_name']]= json_encode($v,320);
                $returnArr[$v['external_name']]= $v;
            }
            $model->hMset($key,$newArr);
            return $dbResult;
        }
    }

    public static function getTableCsgoWeaponList($model)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        $key = ['table','weapon','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $where=  ['=','state',1];
            $dbResult = MetadataCsgoWeapon::find()->where($where)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $external_name = str_replace("weapon_","",$v['external_name']);
                if($external_name == "usp_silencer"){ //特殊情况的武器名字处理
                    $external_name = "usp";
                }
                if($external_name == "item_kevlar"){
                    $external_name = "kevlar";
                }
                $v['image'] = ImageConversionHelper::showFixedSizeConversion($v['image'], 88, 64);
                $newArr[$external_name]= json_encode($v,320);
            }
            $model->hMset($key,$newArr);
            return $dbResult;
        }
    }
    public static function getFormatFramesHero($model,$heroArr)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        $returnArr['hero_id'] = (int)$heroArr['id'];
        $returnArr['name'] = $heroArr['name'];
        $returnArr['name_cn'] = $heroArr['name_cn'];
        $returnArr['external_id'] = (int)$heroArr['external_id'];
        $returnArr['external_name'] = $heroArr['external_name'];
        $returnArr['title'] = $heroArr['title'];
        $returnArr['title_cn'] = $heroArr['title_cn'];
        $returnArr['slug'] = $heroArr['slug'];
        $returnArr['image']['image'] = $heroArr['image'];
        $returnArr['image']['small_image'] = $heroArr['small_image'];
        return $returnArr;
    }

    public static function getTableIngameGoalList($model)
    {
        if (!$model){
            $model = new Dota2Socket();
        }
        $key = ['table','ingameGoal','list'];
        $redisResult = $model->hGetAll($key);
        if (!empty($redisResult)){
            $newArr = [];
            foreach ($redisResult as $k=>$v){
                $newArr[$k] = json_decode($v,true);
            }
            return $newArr;
        }else{
            $returnArr= [];
            $where=  ['>','id',0];
            $dbResult = EnumIngameGoal::find()->where($where)->asArray()->all();
            foreach ($dbResult as $k=>$v){
                $returnArr[$v['id']]= $v;
                $newArr[$v['id']]= json_encode($v,320);
            }
            $model->hMset($key,$newArr);
            return $returnArr;
        }
    }

    public static function getFromatFramesItems($model,$playerArr){
        if (!$model){
            $model = new Dota2Socket();
        }
        $inventoryArr = self::getFormatFrameItem($model,$playerArr['itemsInventory'],'itemsInventory');
        $backpackArr = self::getFormatFrameItem($model,$playerArr['itemsBackpack'],'itemsBackpack');
        $neutralArr = self::getFormatFrameItem($model,$playerArr['itemNeutral'],'itemNeutral');
        $stashArr = self::getFormatFrameItem($model,$playerArr['itemsStash'],'itemsStash');
        $mob_inventoryArr = self::getFormatFrameItem($model,$playerArr['itemsBear'],'itemsBear');
        $mob_backpackArr = self::getFormatFrameItem($model,$playerArr['mob_backpack'],'mob_backpack');

        $returnArr['inventory'] = $inventoryArr;//物品栏
        $returnArr['backpack'] = $backpackArr;//背包
        $returnArr['neutral'] = $neutralArr;//野外掉落
        $returnArr['stash'] = $stashArr;//储藏处
        $returnArr['buffs'] = null;//增益
        $returnArr['mob_inventory'] = $mob_inventoryArr;//召唤物物品栏
        $returnArr['mob_backpack'] = null;//召唤物背包
        $returnArr['mob_neutral'] = null;//召唤物野外掉落
        return $returnArr;
    }

    public static function getFormatFrameItem($model, $item, $itemKey){
        $tableItemList=$model->tableItemList;

        $inventoryArr = [];

        $mutiArr = [
            'itemsInventory',
            'itemsBackpack',
            'itemsStash',
            'itemsBear',
            'mob_backpack',
        ];
        if (in_array($itemKey,$mutiArr)){
            if (empty($item)){
                if ($itemKey=='itemsInventory'||$itemKey=='itemsStash'||$itemKey=='itemsBear'){
                    for ($i=1;$i<=6;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }else{
                    for ($i=1;$i<=3;$i++){
                        $inventoryArr['slot_'.$i]=null;
                    }
                }
            }else{
                $i = 1;
                foreach ($item as $k => $v) {
                    $i = $k + 1;
                    if (array_key_exists($v['itemId'], $tableItemList)) {
                        $dbItem                                    = $tableItemList[$v['itemId']];
                        $soltKey                                   = 'slot_' . $i;
                        $inventoryArr[$soltKey]['item_id']         = (int)$dbItem['id'];
                        $inventoryArr[$soltKey]['name']            = $dbItem['name'];
                        $inventoryArr[$soltKey]['name_cn']         = $dbItem['name_cn'];
                        $inventoryArr[$soltKey]['external_id']     = (int)$dbItem['external_id'];
                        $inventoryArr[$soltKey]['external_name']   = $dbItem['external_name'];
                        $inventoryArr[$soltKey]['total_cost']      = (int)$dbItem['total_cost'];
                        $inventoryArr[$soltKey]['is_recipe']       = $dbItem['is_recipe']==1;
                        $inventoryArr[$soltKey]['is_secret_shop']  = $dbItem['is_secret_shop']==1;
                        $inventoryArr[$soltKey]['is_home_shop']    = $dbItem['is_home_shop']==1;
                        $inventoryArr[$soltKey]['is_neutral_drop'] = $dbItem['is_neutral_drop']==1;
                        $inventoryArr[$soltKey]['slug']            = $dbItem['slug'];
                        $inventoryArr[$soltKey]['image']           = $dbItem['image'];
                        $inventoryArr[$soltKey]['purchase_time']   = null;
                        $inventoryArr[$soltKey]['cooldown']        = (int)$v['cooldownUntil'];
                    }
                }
                if ($itemKey == 'itemsInventory' || $itemKey == 'itemsStash' || $itemKey == 'itemsBear') {
                    if ($i < 6) {
                        for ($j = $i+1; $j <= 6; $j++) {
                            $inventoryArr['slot_'.$j] = null;
                        }
                    }
                    if ($i > 6) {
                        unset($inventoryArr['slot_7']);
                        unset($inventoryArr['slot_8']);
                        unset($inventoryArr['slot_9']);
                    }
                }else{
                    if ($i < 3) {
                        for ($j = $i+1; $j <= 3; $j++) {
                            $inventoryArr['slot_'.$j] = null;
                        }
                    }
                }
            }

        }else{
            if (empty($item)){
                return null;
            }
            $dbItem                          = $tableItemList[$item['itemId']];
            if (empty($dbItem)){
                return null;
            }
            $inventoryArr['item_id']         = (int)$dbItem['id'];
            $inventoryArr['name']            = $dbItem['name'];
            $inventoryArr['name_cn']         = $dbItem['name_cn'];
            $inventoryArr['external_id']     = (int)$dbItem['external_id'];
            $inventoryArr['external_name']   = $dbItem['external_name'];
            $inventoryArr['total_cost']      = (int)$dbItem['total_cost'];
            $inventoryArr['is_recipe']       = $dbItem['is_recipe']==1;
            $inventoryArr['is_secret_shop']  = $dbItem['is_secret_shop']==1;
            $inventoryArr['is_home_shop']    = $dbItem['is_home_shop']==1;
            $inventoryArr['is_neutral_drop'] = $dbItem['is_neutral_drop']==1;
            $inventoryArr['slug']            = $dbItem['slug'];
            $inventoryArr['image']           = $dbItem['image'];
            $inventoryArr['purchase_time']   = null;
            $inventoryArr['cooldown']        = (int)$item['cooldownUntil'];
        }
        return $inventoryArr;
    }

    // 处理当前players的结构
    public static function getFormatFramesFactionPlayer($model, $playerInfo)
    {
        if (!$model){
            $model = new BayesCsgoSocket();
        }
        //该选手的所有武器
        $playerInfoRedisWeapons = BayesCsgoSocketPlayerService::getPlayerWeapon($model,$playerInfo['rel_identity_id']);
        $playerInfoWeapons = ['weapon'=>[],'grenades'=>[]];
        foreach ($playerInfoRedisWeapons as $weaponItem){
            if($weaponItem['kind'] == 1 || $weaponItem['kind'] == 2 || $weaponItem['kind'] == 3){
                $playerInfoWeapons['weapon'][] = $weaponItem;
            }
            if($weaponItem['kind'] == 4){
                $playerInfoWeapons['grenades'][] = $weaponItem;
            }
        }
        $data = [
            'side'=> $playerInfo['side'],
            'player'=> [
                'player_id'=> $playerInfo['player_id'],
                'nick_name'=> $playerInfo['nick_name'],
                'steam_id'=> $playerInfo['steam_id'],
            ],
            'weapon'=>$playerInfoWeapons['weapon'],
            'has_kevlar'=> $playerInfo['has_kevlar'],
            'has_helmet'=> $playerInfo['has_helmet'],
            'has_defusekit'=> $playerInfo['has_defusekit'],
            'has_bomb'=> $playerInfo['has_bomb'],
            'grenades'=> $playerInfoWeapons['grenades'],
            'hp' => $playerInfo['hp'],
            'is_alive'=>$playerInfo['is_alive'],
            'money'=>$playerInfo['money'],
            'kills'=>$playerInfo['kills'],
            'headshot_kills'=>$playerInfo['headshot_kills'],
            'deaths'=>$playerInfo['deaths'],
            'kd_diff'=>$playerInfo['kd_diff'],
            'assists'=>$playerInfo['assists'],
            'flash_assist'=>$playerInfo['flash_assist'],
            'adr'=>$playerInfo['adr'],
            'first_kills'=>$playerInfo['first_kills'],
            'first_deaths'=>$playerInfo['first_deaths'],
            'first_kills_diff'=>$playerInfo['first_kills_diff'],
            'multi_kills'=>$playerInfo['multi_kills'],
            'one_on_x_clutches'=>$playerInfo['one_on_x_clutches'],
            'kast'=>$playerInfo['kast'],
            'advanced'=>[
                'position'=>$playerInfo['position'],
                'equipment_value'=>$playerInfo['equipment_value'],
                'blinded_time'=>$playerInfo['blinded_time'],
                'ping'=>$playerInfo['ping'],
                'rating'=>$playerInfo['rating'],
                'damage'=>$playerInfo['damage'],
                'team_damage'=>$playerInfo['team_damage'],
                'damage_taken'=>$playerInfo['damage_taken'],
                'hegrenade_damage_taken'=>$playerInfo['hegrenade_damage_taken'],
                'inferno_damage_taken'=>$playerInfo['inferno_damage_taken'],
                'planted_bomb'=>$playerInfo['planted_bomb'],
                'chicken_kills'=>$playerInfo['chicken_kills'],
                'defused_bomb'=>$playerInfo['defused_bomb'],
                'blind_enemy_time'=>$playerInfo['blind_enemy_time'],
                'blind_teammate_time'=>$playerInfo['blind_teammate_time'],
                'team_kills'=>$playerInfo['team_kills'],
                'two_kills'=>$playerInfo['two_kills'],
                'three_kills'=>$playerInfo['three_kills'],
                'four_kills'=>$playerInfo['four_kills'],
                'five_kills'=>$playerInfo['five_kills'],
                'one_on_one_clutches'=>$playerInfo['one_on_one_clutches'],
                'one_on_two_clutches'=>$playerInfo['one_on_two_clutches'],
                'one_on_four_clutches'=>$playerInfo['one_on_four_clutches'],
                'one_on_five_clutches'=>$playerInfo['one_on_five_clutches'],
                'awp_kills'=>$playerInfo['awp_kills'],
                'knife_kills'=>$playerInfo['knife_kills'],
                'taser_kills'=>$playerInfo['taser_kills'],
                'shotgun_kills'=>$playerInfo['shotgun_kills'],
            ],
        ];
        return $data;
    }


    public static function getFramesTeamAdvanced($model, $redisData)
    {
        $data['total_runes_pickedup']         = $redisData['total_runes_pickedup'];
        $data['bounty_runes_pickedup']        = $redisData['bounty_runes_pickedup'];
        $data['double_damage_runes_pickedup'] = $redisData['double_damage_runes_pickedup'];
        $data['haste_runes_pickedup']         = $redisData['haste_runes_pickedup'];
        $data['illusion_runes_pickedup']      = $redisData['illusion_runes_pickedup'];
        $data['invisibility_runes_pickedup']  = $redisData['invisibility_runes_pickedup'];
        $data['regeneration_runes_pickedup']  = $redisData['regeneration_runes_pickedup'];
        $data['arcane_runes_pickedup']        = $redisData['arcane_runes_pickedup'];
        $data['smoke_purchased']              = $redisData['smoke_purchased'];
        $data['smoke_used']                   = $redisData['smoke_used'];
        $data['dust_purchased']               = $redisData['dust_purchased'];
        $data['dust_used']                    = $redisData['dust_used'];
        $data['observer_wards_purchased']     = $redisData['observer_wards_purchased'];
        $data['observer_wards_placed']        = $redisData['observer_wards_placed'];
        $data['observer_wards_kills']         = $redisData['observer_wards_kills'];
        $data['sentry_wards_purchased']       = $redisData['sentry_wards_purchased'];
        $data['sentry_wards_placed']          = $redisData['sentry_wards_placed'];
        $data['sentry_wards_kills']           = $redisData['sentry_wards_kills'];
        return $data;
    }








}
