<?php

namespace app\modules\tournament\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\org\models\Player;
use app\modules\org\models\PlayerBase;
use app\modules\org\models\PlayerBaseSnapshot;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\Team;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamIntroductionSnapshot;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\models\TeamPlayerRelationSnasphot;
use app\modules\org\models\TeamSnapshot;
use app\modules\org\services\PlayerService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\TournamentBase;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentPriceDistribute;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\modules\tournament\models\TournamentTeamRelation;
use app\modules\tournament\services\TournamentService;
use app\modules\tournament\models\Tournament;
use app\rest\exceptions\BusinessException;

/**
 * 赛事管理
 */
class TournamentController extends WithTokenAuthController
{
    public function actionDetail()
    {
        $id = $this->pGet('id');
        return TournamentService::getDetailByTreeId($id);
    }

    public function actionTreesList()
    {
        $id = $this->pGet('id');
        return TournamentService::getTournametTreesList($id);
    }

    public function actionEdit()
    {
        $params = $this->pPost();
        $basisId=$this->pPost('todo_id');
//        $parentId=$this->pPost('parent_tree_id');
//        $type=$this->pPost('type');
//        这两个需要传
        if(isset($params['deleted']) && $params['deleted'] == 1){
            Common::deletedMaster($params['id'],Consts::RESOURCE_TYPE_TOURNAMENT,"");
        }
        if (isset($params['deleted']) && $params['deleted'] == 2){
            Common::recoverMaster($params['id'],Consts::RESOURCE_TYPE_TOURNAMENT,"");
        }

        if (!empty($params['core_data']['scheduled_begin_at']) && !empty($params['core_data']['scheduled_end_at'])) {
            if ($params['core_data']['scheduled_begin_at'] > $params['core_data']['scheduled_end_at']) {
                throw new BusinessException([], '计划开始时间大于计划结束时间,不能保存');
            }
        }

        return TournamentService::setTournament($params, Consts::USER_TYPE_ADMIN, $this->user->id,$basisId);
    }


    public function actionAdd()
    {
        $attributes = \Yii::$app->getRequest()->post();

        if ($attributes['type'] == 1) {

            return TournamentService::add($attributes, $this->user->id);
        } elseif ($attributes['type'] == 2) {
//            if (empty($attributes['core_data']['name'])) {
//                throw new BusinessException([],'子赛事名称不能为空');
//            }

            return TournamentService::add($attributes, $this->user->id);
        }
    }

    public function actionCreateCatalog()
    {
        $attributes = \Yii::$app->getRequest()->post();

        $tournament = new TournamentStage();
        $tournament->setAttributes($attributes['core_data']);
        if ($tournament->validate()) {
            return TournamentStage::add($attributes['core_data']);
        } else {
            throw new BusinessException($tournament->getErrors(), '参数信息错误');
        }
    }

    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();
        return TournamentService::getTournamentList($params);
    }

    public function actionDel()
    {
        $id = \Yii::$app->getRequest()->post('id');
        $deleted = \Yii::$app->getRequest()->post('status');
        if(isset($deleted) && $deleted == 1){
            $data = PlayerService::checkBing('tournament', $id);
            if (!$data) {
                throw new BusinessException([], '绑定有数据源的比赛不能删除！');
            }
            return Common::deletedMaster($id,Consts::RESOURCE_TYPE_TOURNAMENT,"");
        }
        if (isset($deleted) && $deleted == 2){
            return Common::recoverMaster($id,Consts::RESOURCE_TYPE_TOURNAMENT,"");
        }

//        if ($deleted == Tournament::NO) {
//
//            $tournament = Tournament::find()->where(['id' => $id])->one();
//            $logOldInfo = $tournament->toArray();
//
//            $tournament->setScenario('son_tournament');
//            $tournament->deleted = $deleted;
//            $tournament->deleted_at = null;
//
//            if ($tournament->save()) {
//                $logNewInfo = $tournament->toArray();
//                $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
//                if($diffInfo['changed']){
//                    OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                        Consts::RESOURCE_TYPE_TOURNAMENT,
//                        $id,
//                        ["diff" => $diffInfo['diff'], "new" => $logNewInfo],
//                        0,
//                        Consts::TAG_TYPE_BASE_DATA,
//                        null,
//                        0,
//                        Consts::USER_TYPE_ADMIN,
//                        $this->user->id
//                    );
//                }
//                return ['id' => $id, 'msg' => '恢复成功'];
//            }
//        }
//        //TODO 绑定有数据源的赛事不能删除
//        $data = PlayerService::checkBing('tournament', $id);
//        if (!$data) {
//            throw new BusinessException([], '绑定有数据源的赛事不能删除！');
//        }
//        $tournament = Tournament::find()->where(['id' => $id])->one();
//        $logOldInfo = $tournament->toArray();
//
//        $tournament->setScenario('son_tournament');
//        $tournament->deleted = $deleted;
//        $tournament->deleted_at = date('Y-m-d H:i:s');
//
//        if ($tournament->save()) {
//            $logNewInfo = $tournament->toArray();
//            $diffInfo = Common::getDiffInfo($logOldInfo, $logNewInfo, [], true);
//            if($diffInfo['changed']){
//                OperationLogService::addLog(QueueServer::CHANGE_TYPE_DELETE,
//                    Consts::RESOURCE_TYPE_TOURNAMENT,
//                    $id,
//                    ["diff" => $diffInfo['diff'], "new" => $logNewInfo],
//                    0,
//                    Consts::TAG_TYPE_BASE_DATA,
//                    null,
//                    0,
//                    Consts::USER_TYPE_ADMIN,
//                    $this->user->id
//                );
//            }
//            return ['id' => $id, 'msg' => '删除成功'];
//        }
//        throw new BusinessException($tournament->getErrors(), '删除失败');
    }


    public function actionInfo()
    {
        $id = \Yii::$app->getRequest()->get('id');

        $info = Tournament::find()->alias('t')
            ->select('t.*,tb.*')
            ->leftJoin('tournament_base as tb', 'tb.tournament_id = t.id')
            ->where(['t.id' => $id])->asArray()->one();
        $info['player_config'] = TournamentTeamRelation::find()
            ->select('tr.*,t.name as name,t.country')
            ->alias('tr')
            ->leftJoin('team as t', 't.id = tr.team_id')
            ->where(['tournament_id' => $id])->asArray()->all();
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');

        $sort = array_column($info['player_config'], 'team_sort');
        array_multisort($sort, SORT_ASC, $info['player_config']);
        foreach ($info['player_config'] as &$value) {
            $value['country_name'] = $country[$value['country']] ?? '';
        }
        $info['tournament_price_distribute'] = TournamentPriceDistribute::find()->where(['tournament_id' => $id])->asArray()->all();
        $info['data_config'] = UpdateConfigService::getResourceUpdateConfigDetail($id, 'tournament');
        return $info;
    }

    // 创建关系树

    public function actionCreateRelation()
    {
        $post = \Yii::$app->getRequest()->post();

        return TournamentService::createTournamentRelation($post);
    }


    public function actionGetRelationTree()
    {
        $id = \Yii::$app->getRequest()->get('id');
        $type = \Yii::$app->getRequest()->get('type','all');
        return TournamentService::getRelationTree($id,$type);
    }

    public function actionCreateStage()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $tournament = new TournamentStage();
        $tournament->setAttributes($attributes['core_data']);
        if ($tournament->validate()) {
            return TournamentStage::add($attributes['core_data']);
        } else {
            throw new BusinessException($tournament->getErrors(), '参数信息错误');
        }
    }

    /**
     * @return
     * 阶段，目录详情
     */
    public function actionGetStage()
    {
        $id = \Yii::$app->getRequest()->get('tree_id');

        $tournamentStage = TournamentStage::find()->where(['tree_id' => $id])->one();
        $result['core_data'] = $tournamentStage->toArray();
        return $result;
    }

    public function actionCreateGroup()
    {
        $attributes = \Yii::$app->getRequest()->post();

        $tournament = new TournamentGroup();
        $tournament->setAttributes($attributes['core_data']);
        if ($tournament->validate()) {
            return TournamentGroup::add($attributes);
        } else {
            throw new BusinessException($tournament->getErrors(), '参数信息错误');
        }
    }


    public function actionGetGroup()
    {
        $id = \Yii::$app->getRequest()->get('tree_id');
        $result = [];
        $groupInfo = TournamentGroup::find()->where(['tree_id' => $id])->asArray()->one();
        $result['core_data'] = $groupInfo;

        $groupInfo['group_team_relation'] = GroupTeamRelation::find()
            ->select('tr.*,t.name as name,t.country')
            ->alias('tr')
            ->leftJoin('team as t', 't.id = tr.team_id')
            ->where(['tr.group_id' => $groupInfo['id']])->asArray()->all();
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');

        foreach ($groupInfo['group_team_relation'] as &$value) {
            $value['country_name'] = $country[$value['country']] ?? '';
        }
        $result['group_team_relation'] = $groupInfo['group_team_relation'];
        return $result;
    }

    public function actionGetSonTournament()
    {
        $tree_id = \Yii::$app->getRequest()->get('tree_id');
        $Core = Tournament::find()->where(['and', ["tree_id" => $tree_id], ['type' => 2]])->asArray()->one();
        $tournamentId = $Core['id'];
        $Base = TournamentBase::find()->where(['tournament_id' => $Core['id']])->asArray()->one();
        $configList = TournamentTeamRelation::find()->alias('tr')
            ->select("tr.*,tournament.name")
            ->leftJoin('tournament', 'tournament.id=tr.tournament_id')
            ->where(["tr.tournament_id" => $tournamentId])->asArray()->all();
        return [
            'id' => $tournamentId,
            Consts::TAG_TYPE_CORE_DATA => $Core,
            Consts::TAG_TYPE_BASE_DATA => $Base,
            Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION => TournamentService::getTeamRelation($tournamentId),
            'tournament_price_distribute' => TournamentService::getTournamentPriceDistribute($tournamentId),
//            'entry_conditions' => self::getTeamRelation($tournamentId),
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($tournamentId, Consts::RESOURCE_TYPE_TOURNAMENT),
        ];

//        $id = \Yii::$app->getRequest()->get('tree_id');
//        $result = [];
//        $tournamentInfo = Tournament::find()->alias('t')
//            ->select('tb.*,t.*')
//            ->leftJoin('tournament_base as tb','t.id = tb.tournament_id')
//            ->where(['t.tree_id' => $id,'t.type' => 2])
//            ->asArray()
//            ->one();
//        $tournamentInfo['team_relation'] = TournamentTeamRelation::find()
//            ->select('tr.*,t.name as name,t.country')
//            ->alias('tr')
//            ->leftJoin('team as t','t.id = tr.team_id')
//            ->where(['tournament_id' => $tournamentInfo['id'],'type' => 2])
//            ->asArray()
//            ->all();
//        $sort = array_column($tournamentInfo['team_relation'],'team_sort');
//        array_multisort($sort,SORT_ASC,$tournamentInfo['team_relation']);
//        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
//
//        foreach ($tournamentInfo['team_relation'] as &$value)
//        {
//            $value['country_name'] = $country[$value['country']] ?? '';
//        }
//        $tournamentInfo['tournament_price_distribute'] = TournamentPriceDistribute::find()->where(['tournament_id' => $tournamentInfo['id']])->asArray()->all();
//
//        return $tournamentInfo;
    }

    public function actionDelGroup()
    {
        $id = \Yii::$app->getRequest()->post('tree_id');

        $groupInfo = TournamentGroup::find()->where(['tree_id' => $id])->one();

        // 比赛绑定分组不能删除
        $matchInfo = Match::find()->where(['group_id' => $groupInfo->id])->asArray()->one();
        if ($matchInfo && !empty($matchInfo)) {
            throw new BusinessException([], '比赛绑定分组不能删除');
        }

        $groupInfo->setAttribute('deleted', 1);
        if ($groupInfo->save() > 0) {

            TournamentRelationTree::deleteAll(['id' => $id]);
            return ['id' => $id, 'msg' => '成功'];
        }

        throw new BusinessException($groupInfo->getErrors(), '操作失败');
    }

    public function actionDelStage()
    {
        $id = \Yii::$app->getRequest()->post('tree_id');
        $info = TournamentRelationTree::find()->where(['parent_id' => $id])->asArray()->one();
        if ($info && !empty($info) && $info['type'] == 2) {
            throw new BusinessException($info, '下面有子赛事。不能删除！');
        }
        if ($info && !empty($info) && $info['type'] == 4) {
            throw new BusinessException($info, '下面有分组。不能删除！');
        }
        if ($info && !empty($info) && $info['type'] == 5) {
            throw new BusinessException($info, '下面有目录。不能删除！');
        }

        TournamentRelationTree::deleteAll(['id' => $id]);

        $stageInfo = TournamentStage::find()->where(['tree_id' => $id])->one();
        $stageInfo->setAttribute('deleted', 1);
        if ($stageInfo->save() > 0) {
            return ['id' => $id, 'msg' => '成功'];
        }

        throw new BusinessException($stageInfo->getErrors(), '操作失败');
    }

    public function actionDelSonTournament()
    {
        $id = \Yii::$app->getRequest()->post('tree_id');

        $info = TournamentRelationTree::find()->where(['parent_id' => $id])->asArray()->one();
        if ($info && !empty($info)) {
            throw new BusinessException($info, '下面有阶段。不能删除！');
        }
        TournamentRelationTree::deleteAll(['id' => $id]);

        $tournamentInfo = Tournament::find()->where(['tree_id' => $id, 'type' => 2])->one();
        $tournamentInfo->setScenario('son_tournament');
        $tournamentInfo->setAttribute('deleted', 1);
        if ($tournamentInfo->save() > 0) {
            return ['id' => $id, 'msg' => '成功'];
        }

        throw new BusinessException($tournamentInfo->getErrors(), '操作失败');
    }

    /**
     * @return array|bool
     * @throws BusinessException
     * 更新战队快照
     */
    public function actionUpdateTeamSnapshot()
    {
        $newIds = \Yii::$app->getRequest()->post('new_team_ids');
        $oldIds = \Yii::$app->getRequest()->post('old_team_ids');
        $isUpdate = \Yii::$app->getRequest()->post('is_update');
        $relationId = \Yii::$app->getRequest()->post('relation_id');
        $type = \Yii::$app->getRequest()->post('type');
        //前台 点击更新战队快照按钮
        if ($isUpdate == 1){
            $newIds = $oldIds;
        }
        TournamentService::updateTeamSnapshot($newIds,$isUpdate,$relationId,$type);
        return ['msg' => '成功', 'success' => true];
//        $newIds = explode(',', $newIds);
//        $oldIds = explode(',', $oldIds);
//        if (empty($newIds) && empty($oldIds)) {
//            throw new BusinessException([], '更新战队ids为空');
//        }
//        if (in_array($type, ['tournament', 'group', 'son-tournament']) && $isUpdate != 1) {
//            $ids = array_diff($newIds, $oldIds);
//
//            // 移出去老的
//            $remove = array_diff($oldIds, $newIds);
//            if (!empty($remove)) {
//                TournamentService::removeTeamSnapshot($remove, $type, $relationId);
//            }
//        } else {
//            $ids = $oldIds;
//        }
//        if (empty($ids)) {
//            return true;
//        }
//        //获取战队信息
//        $team = Team::find()->where(['in', 'id', $ids])->asArray()->all();
//        $newIds = [];
//        foreach ($team as $value) {
//            $value['team_id'] = $value['id'];
//            $value['type'] = $type;
//            $value['relation_id'] = $relationId;
//            $newTeam = TeamSnapshot::find()->where(['team_id' => $value['id'], 'type' => $type, 'relation_id' => $relationId])->one();
//            if (empty($newTeam) || !$newTeam) {
//                $newTeam = new TeamSnapshot();
//            }
//            $newTeam->setAttributes($value);
//            $newTeam->save();
//            $teamId = $newTeam->id;
//            $newIds[$value['id']] = $teamId;
//
//            // 获取战队基础数据
//            $ti = TeamIntroduction::find()->where(['team_id' => $value['id']])->asArray()->one();
//            if (!empty($ti) || $ti) {
//                $teamInt = TeamIntroductionSnapshot::find()->where(['team_id' => $teamId])->one();
//                if (empty($teamInt) || !$teamInt) {
//                    $teamInt = new TeamIntroductionSnapshot();
//                    $ti['team_id'] = $teamId;
//                }
//                $teamInt->setAttributes($ti);
//                $teamInt->save();
//            }
//        }
//
//        TeamPlayerRelationSnasphot::deleteAll(['IN', 'team_id', $newIds]);
//        // 战队选手关系
//        $tr = TeamPlayerRelation::find()->where(['in', 'team_id', $ids])->asArray()->all();
//
//        foreach ($tr as $v) {
//            unset($v['id']);
//            foreach ($newIds as $key => $val) {
//                if ($v['team_id'] == $key) {
//                    $obj = new TeamPlayerRelationSnasphot();
//                    $v['team_id'] = $val;
//                    $obj->setAttributes($v);
//                    $obj->save();
//                    $relationId = $obj->id;
//                    // 选手
//                    $player = Player::find()
//                        ->where(['id' => $v['player_id']])->asArray()->one();
//                    if (!empty($player) && $player) {
//                        $p = PlayerSnapshot::find()->where(['player_id' => $player['id'], 'relation_id' => $relationId])->one();
//                        if (empty($p) || !$p) {
//                            $p = new PlayerSnapshot();
//                            unset($player['id']);
//                            $player['player_id'] = $v['player_id'];
//                            $player['relation_id'] = $relationId;
//                        }
//                        $p->setAttributes($player);
//                        $p->save();
//                        $playerId = $p->id;
//
//                        // todo 这里的数据会有问题
//                        // 选手
//                        $playerBase = PlayerBase::find()
//                            ->where(['player_id' => $v['player_id']])->asArray()->one();
//                        if (!empty($playerBase) && $playerBase) {
//                            $objBase = PlayerBaseSnapshot::find()->where(['player_id' => $playerId])->one();
//                            if (empty($objBase) || !$objBase) {
//                                $objBase = new PlayerBaseSnapshot();
//                                $playerBase['player_id'] = $playerId;
//                            }
//                            $objBase->setAttributes($playerBase);
//                            $objBase->save();
//
//                        }
//                    }
//                }
//            }
//        }

//        return ['msg' => '成功', 'success' => true];
    }

    public function actionGetTeamSnapshot()
    {
        $ids = \Yii::$app->getRequest()->get('team_ids');
        $type = \Yii::$app->getRequest()->get('type');
        $resourceId = \Yii::$app->getRequest()->get('resource_id');

        return $this->getTeamSnapshot($ids, $type, $resourceId);
    }


    /**
     * 战队快照编辑
     * @return array
     * @throws BusinessException
     */
    public function actionTeamSnapshotEdit() {
        $params = $this->pPost();
        return TournamentService::teamSnapshotEdit($params);
    }

    /**
     * 战队快照选手详情
     * @return array|\yii\db\ActiveRecord|null
     */
    public function actionTeamDetailSnapshot() {
        $team_id = \Yii::$app->getRequest()->get('team_id');
        $type = \Yii::$app->getRequest()->get('type');
        $resourceId = \Yii::$app->getRequest()->get('resource_id');
        return TournamentService::teamDetailSnapshot($team_id,$type,$resourceId);

    }


    public function actionPlayerSnapshotAdd() {
        $team_id = \Yii::$app->getRequest()->post('team_id');
        $type = \Yii::$app->getRequest()->post('type');
        $resourceId = \Yii::$app->getRequest()->post('resource_id');
        $players = \Yii::$app->getRequest()->post('players');
        return TournamentService::playerSnapshotAdd($team_id,$type,$resourceId,$players);
    }


    public function actionGetTournamentTeam()
    {
        $id = \Yii::$app->getRequest()->get('id');
        $type = \Yii::$app->getRequest()->get('type');
        $key = \Yii::$app->getRequest()->get('name');
        $stage = \Yii::$app->getRequest()->get('stage');

        $data = $this->getTournamentTeamById($type, $id, $stage, $key);

        return $data;
    }

    /**
     * @param array $ids
     * @param array $type
     * @param array $resourceId
     * @return array
     * @throws BusinessException
     */
    private function getTeamSnapshot($ids, $type, $resourceId)
    {
        $team = TeamSnapshot::find()
            ->select('ts.team_id as els_team_id,
            ts.name as team_name,
            ts.id as snapshot_team_id,ts.image,ts.slug,ts.country,ts.full_name,ts.short_name
            ,tis.world_ranking
            ,enum_country.name as country_name,
            ,enum_country.e_name as country_e_name,
            ')
            ->alias('ts')
            ->leftJoin('team_introduction_snapshot as tis', 'tis.team_id = ts.id')
            ->leftJoin('enum_country', 'ts.country = enum_country.id');
            if (!empty($ids)){
                $team->where(['in', 'ts.team_id', explode(',', $ids)]);
            }
        $team = $team->andWhere(['ts.type' => $type])->andWhere(['ts.relation_id' => $resourceId])->asArray()->all();

        // 查找下面的选手
        $teamIds = array_column($team, 'snapshot_team_id');
        $player = PlayerSnapshot::find()
            ->select('
            ps.player_id as els_player_id,
            tprs.team_id as team_id,
            ps.id as snapshot_player_id,ps.slug as player_slug,ps.image as player_image,
            ps.role,ps.name as player_name,ps.name_cn as player_name_cn,ps.player_country,tprs.status_id,
            ps.nick_name,
            ps.player_country,
            enum_country.name as country_name,
            enum_country.e_name as country_e_name,
            ps.role,
            enum_position.name as role_name,
            enum_position.c_name as role_e_name
            '
            )
            ->alias('ps')
            ->leftJoin('team_player_relation_snapshot as tprs', 'ps.relation_id = tprs.id')
            ->leftJoin('enum_country', 'ps.player_country = enum_country.id')
            ->leftJoin('enum_position', 'ps.role = enum_position.id')
            ->where(['in', 'tprs.team_id', $teamIds])->asArray()->all();


//        if (!$team || empty($team)) {
//            throw new BusinessException([], '没有该战队信息');
//        }

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');

        foreach ($team as &$value) {
            $value['team_country'] = $country[$value['country']] ?? '';
            $value['players'] = [];
            if($player){
                foreach ($player as &$val) {
                    $val['player_country'] = $country[$val['player_country']] ?? '';
                    if ($value['snapshot_team_id'] == $val['team_id']) {
                        $value['players'][] = $val;
                    }
                }
            }
        }

        return $team;
    }
    /**
     * @param array $type
     * @param array $id
     * @param array $stage
     * @param array $key
     * @return array
     */
    private function getTournamentTeamById($type, $id, $stage, $key)
    {
        switch ($type) {
            case 'son_tournament' :
                $class = TournamentTeamRelation::find()
                    ->alias('tr')
                    ->select('t.image,t.name,t.id,t.country')
                    ->leftJoin('team as t', 't.id = tr.team_id')
                    ->where(['tr.tournament_id' => $id, 'type' => 1]);
                break;
            case 'group' :
//                if ($stage == 1) {
//                    $stage = TournamentStage::find()
//                        ->where(['id' => $id])->asArray()->one();
//                } else {
//                    $groupInfo = TournamentGroup::find()
//                        ->where(['id' => $id])->asArray()->one();
//                    $treeInfo = TournamentRelationTree::find()->where(['id' => $groupInfo['tree_id']])->asArray()->one();
//                    // 阶段
//                    $stage = TournamentRelationTree::find()->where(['id' => $treeInfo['parent_id']])->asArray()->one();
//                }
//
//
//                $son_tournament = TournamentRelationTree::find()->where(['id' => $stage['parent_id']])->asArray()->one();
//                if ($son_tournament['type'] != 2) {
//                    $class = TournamentTeamRelation::find()
//                        ->alias('tr')
//                        ->select('t.image,t.name,t.id,t.country')
//                        ->leftJoin('team as t', 't.id = tr.team_id')
//                        ->where(['tr.tournament_id' => $stage['tournament_id'], 'type' => 1]);
//                } else {
//                    $son = Tournament::find()->where(['tree_id' => $son_tournament['id']])->asArray()->one();
//                    $class = TournamentTeamRelation::find()
//                        ->select('t.image,t.name,t.id,t.country')
//                        ->alias('tr')
//                        ->leftJoin('team as t', 't.id = tr.team_id')
//                        ->where(['tr.tournament_id' => $son['id'], 'type' => 2]);
//                }
                if ($stage == 1) {
                    $tmType='stage';
                }else{
                    $tmType='group';
                }
                $info=TournamentService::getRelTeamInfoByStageId($id,$tmType);
                $class = TournamentTeamRelation::find()
                    ->select('t.image,t.name,t.id,t.country')
                    ->alias('tr')
                    ->leftJoin('team as t', 't.id = tr.team_id')
                    ->where(['tr.tournament_id' => $info['tournament_id'], 'type' => $info['type']]);
                break;
        }

        if (!empty($key) && $key) {
            $class->andWhere(['or',
                ['like', 't.name', $key],
                ['like', 't.full_name', $key],
                ['like', 't.short_name', $key],
                ['like', 't.alias', $key]
            ]);
        }
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $data = $class->asArray()->all();

        foreach ($data as &$value) {
            $value['country'] = $country[$value['country']] ?? '';
        }
        return $data;
    }

}
