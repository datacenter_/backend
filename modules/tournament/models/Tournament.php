<?php

namespace app\modules\tournament\models;

use app\modules\common\services\Consts;
use app\modules\common\services\OperationLogService;
use app\modules\data\services\UpdateConfigService;
use app\modules\task\services\Common;
use app\modules\task\services\QueueServer;
use app\modules\tournament\models\TournamentBase;
use app\modules\tournament\models\TournamentTeamRelation;
use app\modules\tournament\services\TournamentService;
use app\rest\exceptions\BusinessException;
use Yii;
class Tournament extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','slug','type','status'], 'required','on'=>'tournament'],
            [['name'], 'required','on'=>'son_tournament'],
            [['country','region','deleted','is_private','public_delay','public_data_devel','is_private_delay','private_delay_seconds'], 'integer'],
            [['game', 'status', 'cuser','tree_id','type','is_rescheduled','number_of_teams','tournament_id'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['name','name_cn','scheduled_begin_at','scheduled_end_at','format','format_cn',
                'introduction','introduction_cn','deleted_at','location','location_cn','match_range',
                'organizer','organizer_cn','version','prize_bonus','prize_points','prize_seed','map_pool',
                'top_reward'], 'string'],
            [['image','steam_id','series_match','tier'], 'string'],
            [['short_name','short_name_cn','slug','son_match_sort','match_condition'], 'string']
        ];
    }
    public function scenarios()
    {
        return [
            'tournament' => ['name','name_cn','game','status'
            ,'scheduled_begin_at', 'scheduled_end_at', 'slug',
                'short_name','short_name_cn'
                ,'son_match_sort','image','type','tree_id','cuser',
                'deleted_at'
            ],
            'son_tournament' => ['name','name_cn','game','status',
                'scheduled_begin_at', 'scheduled_end_at', 'slug',
                'short_name','short_name_cn',
                'son_match_sort','image','type','tree_id','cuser',
                'deleted_at'
            ],
            'bing_tournament' => ['name','name_cn','game','status',
                'scheduled_begin_at', 'scheduled_end_at', 'slug',
                'short_name','short_name_cn','deleted','deleted_at',
                'son_match_sort','image','type','tree_id','cuser','teams_condition'
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }

    public static function add($attribute, $userId)
    {
        if (isset($attribute['id']) && $attribute['id']) {
            $logOld =TournamentService::getTournament($attribute['id']);
            $tournament = Tournament::find()->where(['id' => $attribute['id']])->one();
            if ($attribute['type'] == 1) {
                $tournament->setScenario('tournament');
            }elseif($attribute['type'] == 2){
                $tournament->setScenario('son_tournament');
            }
//            $tournament->setAttributes(['id'=>,
//                ’sex’=>’女’,
//                ’age’=>20]);
            $tournament->setAttributes($attribute);
            if ($tournament->save() > 0 ) {
                $tournamentBase = TournamentBase::find()->where(['tournament_id' => $attribute['id']])->one();
                $attribute['tournament_id'] = $attribute['id'];
                if (!empty($tournamentBase)) {
                    $tournamentBase->setAttributes($attribute);
                }else{
                    $tournamentBase = new TournamentBase();
                    $tournamentBase->setAttributes($attribute);
                }
                if ($tournamentBase->save() <= 0) {
                    throw new BusinessException($tournamentBase->getErrors(),'基础信息添加失败');
                }
                // 奖池分配
                if (isset($attribute['tournament_price_distribute']) && !empty($attribute['tournament_price_distribute']))
                {
                    $priceDistributeInfo = json_decode($attribute['tournament_price_distribute'],true);
                    TournamentPriceDistribute::deleteAll(['tournament_id' => $attribute['id']]);

                    $model = new TournamentPriceDistribute();
                    foreach($priceDistributeInfo as $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('team_num',$val['team_num']);
                        $_model->setAttribute('rank',$val['rank']);
                        $_model->setAttribute('price',$val['price']);
                        $_model->setAttribute('score',$val['score']);
                        $_model->setAttribute('num',$val['num']);
                        $_model->setAttribute('tournament_id',$attribute['id']);
                        $_model->save();
                    }
                }
                // 进入选手关系
                if (isset($attribute['tournament_team_ids']) && !empty($attribute['tournament_team_ids'])) {
                    $tournamentTeamIds = json_decode($attribute['tournament_team_ids'],true);
                    // 先删后加
                    TournamentTeamRelation::deleteAll(['tournament_id' => $attribute['id']]);

                    $model = new TournamentTeamRelation();
                    foreach($tournamentTeamIds as $key => $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('tournament_id',$attribute['id']);
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('type',$attribute['type']);
                        $_model->setAttribute('match_condition',$val['match_condition']);
                        $_model->setAttribute('match_condition_cn',$val['match_condition_cn']);
                        $_model->setAttribute('team_sort',$val['team_sort']);

                        $_model->save();
                    }
                }
            }
            if ($attribute['type'] == 1) {
                $logNew =TournamentService::getTournament($attribute['id']);
                // 添加修改log
                $diffInfo = Common::getDiffInfo($logOld, $logNew,[],true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                        Consts::RESOURCE_TYPE_TOURNAMENT,
                        $attribute['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }
            }
            return ['tournament_id' => $attribute['id'], 'msg' => '成功'];
        }
        else {
            $tournament = new Tournament();
            if ($attribute['type']  == 1) {
                $tournament->setScenario('tournament');
            }elseif($attribute['type']  == 2){
                $tournament->setScenario('son_tournament');
            }
            $tournament->setAttributes($attribute);
            $tournament->setAttribute('cuser',$userId);
            if ($tournament->save() > 0 ) {
                $tournamentId = $attribute['tournament_id'];
                unset($attribute['tournament_id']);
                $tId = $tournament->id;
                $tournamentBase = new TournamentBase();
                $tournamentBase->setAttribute('tournament_id',$tId);
                $tournamentBase->setAttributes($attribute);
                if ($tournamentBase->save() <= 0) {
                    throw new BusinessException($tournamentBase->getErrors(),'基础信息添加失败');
                }
                // 奖池分配
                if (isset($attribute['tournament_price_distribute']) && !empty($attribute['tournament_price_distribute']))
                {
                    $priceDistributeInfo = json_decode($attribute['tournament_price_distribute'],true);
                    $model = new TournamentPriceDistribute();
                    foreach($priceDistributeInfo as $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('team_num',$val['team_num']);
                        $_model->setAttribute('rank',$val['rank']);
                        $_model->setAttribute('price',$val['price']);
                        $_model->setAttribute('score',$val['score']);
                        $_model->setAttribute('num',$val['num']);
                        $_model->setAttribute('tournament_id',$tId);
                        $_model->save();
                    }
                }

                    // 战队 赛事关系
                if (isset($attribute['tournament_team_ids']) && !empty($attribute['tournament_team_ids'])) {
                    $tournamentTeamIds = json_decode($attribute['tournament_team_ids'],true);

                    $model = new TournamentTeamRelation();
                    foreach($tournamentTeamIds as $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('tournament_id',$tId);

                        $_model->setAttribute('type',$attribute['type']);
                        $_model->setAttribute('match_condition',$val['match_condition']);
                        $_model->setAttribute('match_condition_cn',$val['match_condition_cn']);
                        $_model->setAttribute('team_sort',$val['team_sort']);

                        $_model->save();
                    }
                }

                // 加入赛事关系树
                if (isset($attribute['type']) && $attribute['type'] == 2) {

                    $tree = new TournamentRelationTree();
                    $tree->setAttribute('tournament_id', $tournamentId);
                    $tree->setAttribute('type',2);
                    $tree->setAttribute('parent_id',$attribute['tree_parent_id']);
                    $tree->save();

                    $t = Tournament::find()->where(['id' => $tId])->one();
                    $t->setScenario('son_tournament');
                    $t->setAttribute('tree_id',$tree->id);
                    $t->setAttribute('name',$attribute['name']);
                    $t->save();
                }else{
                    $tree = new TournamentRelationTree();
                    $tree->setAttribute('tournament_id', $tId);
                    $tree->setAttribute('type',1);
                    $tree->save();
                }
                if ($attribute['type'] == 1) {
                    $Introduction = $tournamentBase->toArray();
                    $tournamentInfo = $tournament->toArray();
                    $logNew = array_merge($Introduction,$tournamentInfo);
                    // 添加修改log
                    $diffInfo = Common::getDiffInfo([], $logNew,[],true);
                    if ($diffInfo["changed"]) {
                        OperationLogService::addLog(OperationLogService::OPERATION_TYPE_ADD,
                            Consts::RESOURCE_TYPE_TOURNAMENT,
                            $logNew['id'],
                            ["diff" => $diffInfo["diff"], "new" => $logNew],
                            0,
                            Consts::TAG_TYPE_BASE_DATA,
                            null,
                            0,
                            Consts::USER_TYPE_ADMIN,
                            $userId
                        );
                    }
                }
                if ($attribute['type'] == 1) {
                    $resourceType = $attribute['type'] == 1 ? Consts::RESOURCE_TYPE_TOURNAMENT : Consts::RESOURCE_TYPE_SON_TOURNAMENT;
                    UpdateConfigService::initResourceUpdateConfig($logNew["id"], $resourceType, $attribute["game"]);
                }
                return ['tournament_id' => $tId, 'msg' => '成功'];
            }else{
                throw new BusinessException($tournament->getErrors(),'创建失败');
            }
        }

    }
}
