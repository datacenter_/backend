<?php

namespace app\modules\tournament\models;

use Yii;

/**
 * This is the model class for table "club_logo".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部ID
 * @property string $logo 图片地址
 * @property int $img_ver 版本
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class TournamentBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id','country','region','number_of_teams'], 'integer'],
            [['steam_id', 'series_match', 'introduction', 'introduction_cn','tier','location','location_cn','organizer',
                'organizer_cn','version','prize_bonus','prize_points','map_pool','top_reward',
                'format','format_cn','prize_seed'
            ], 'string'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
