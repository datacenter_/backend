<?php

namespace app\modules\tournament\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class TournamentGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tree_id', 'tournament_id','deleted','game','order'], 'integer'],
            [['name', 'name_cn','number_of_teams',
                'advance','eliminate','win_points','draw_ponits','lose_points','scheduled_begin_at','scheduled_end_at'],
                'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }

    public static function add($attribute)
    {
        // 修改
        if (isset($attribute['core_data']['id']) && $attribute['core_data']['id']) {
            $tg = TournamentGroup::find()->where(['id' => $attribute['core_data']['id']])->one();
            $tg->setAttributes($attribute['core_data']);
            if ($tg->save() > 0 ) {

                // 进入战队关系
                if (isset($attribute['group_team_ids']) && !empty($attribute['group_team_ids'])) {
                    $groupTeamIds = json_decode($attribute['group_team_ids'],true);
                    // 先删后加
                    GroupTeamRelation::deleteAll(['group_id' => $attribute['id']]);

                    $model = new GroupTeamRelation();
                    foreach($groupTeamIds as $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('group_id',$attribute['id']);
                        $_model->setAttribute('tournament_id',$attribute['tournament_id']);
                        $_model->save();
                    }
                }

                return ['group_id' => $attribute['id'], 'msg' => '成功'];
            }else{
                throw new BusinessException($tg->getErrors(),'创建失败');
            }
        } else {
            $tg = new TournamentGroup();
            $tg->setAttributes($attribute);
            if ($tg->save() > 0 ) {
                $tgId = $tg->id;

                // 战队 位置状态关系
                if (isset($attribute['group_team_ids']) && !empty($attribute['group_team_ids'])) {
                    $groupTeamIds = json_decode($attribute['group_team_ids'],true);

                    $model = new GroupTeamRelation();
                    foreach($groupTeamIds as $val)
                    {
                        $_model = clone $model;
                        $_model->setAttribute('team_id',$val['team_id']);
                        $_model->setAttribute('group_id',$tgId);
                        $_model->setAttribute('tournament_id',$attribute['tournament_id']);
                        $_model->save();
                    }
                }
                // 加入关系树
                $tree = new TournamentRelationTree();
                $tree->setAttribute('tournament_id', $attribute['tournament_id']);
                $tree->setAttribute('type',4);
                $tree->setAttribute('parent_id',$attribute['tree_parent_id']);
                $tree->save();

                // 修改分组树id
                $tgInfo = TournamentGroup::find()->where(['id' => $tg->id])->one();
                $tgInfo->setAttribute('tree_id', $tree->id);
                $tgInfo->save();
                return ['group_id' => $tgId, 'msg' => '成功'];
            }else{
                throw new BusinessException($tg->getErrors(),'创建失败');
            }
        }
    }
}
