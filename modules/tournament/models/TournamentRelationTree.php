<?php

namespace app\modules\tournament\models;

use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class TournamentRelationTree extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_releation_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'tournament_id','parent_id'], 'integer'],
        ];
    }

}
