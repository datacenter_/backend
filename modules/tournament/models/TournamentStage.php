<?php

namespace app\modules\tournament\models;

use app\rest\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "clan_introduction".
 *
 * @property int $id 主键
 * @property int $clan_id 俱乐部表主键ID
 * @property string|null $introduction 英文介绍
 * @property string|null $introduction_cn 中文介绍
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class TournamentStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tree_id', 'tournament_id','stage_type','status','is_rescheduled','game','order'], 'integer'],
            [['name', 'name_cn','stage_sort','stage_format_extra'], 'string','max' => 50],
            [['begin_at', 'end_at'
            ,'slug','scheduled_begin_at','scheduled_end_at','original_scheduled_begin_at',
                'original_scheduled_end_at','stage_format'
            ], 'string'],
            [['begin_at', 'end_at'], 'safe'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    }

    public static function add($attribute)
    {
        // 修改
        if (isset($attribute['id']) && $attribute['id']) {
            $ts = TournamentStage::find()->where(['id' => $attribute['id']])->one();
            $ts->setAttributes($attribute);
            if ($ts->save() > 0 ) {
                return ['id' => $attribute['id'], 'msg' => '成功'];
            }
        } else {
            $type = $attribute['type'] == 1 ? 3 : 5;
            $ts = new TournamentStage();
            $ts->setAttributes($attribute);

            if ($ts->save() > 0 ) {

                $tree = new TournamentRelationTree();
                $tree->setAttribute('tournament_id', $attribute['tournament_id']);

                $tree->setAttribute('type',$type);
                $tree->setAttribute('parent_id',$attribute['tree_parent_id']);
                $tree->save();

                // 修改阶段表里树id
                $tsInfo = TournamentStage::find()->where(['id' => $ts->id])->one();
                $tsInfo->setAttribute('tree_id', $tree->id);
                $tsInfo->save();
                return ['id' => $ts->id, 'msg' => '成功'];
            }else{
                throw new BusinessException($ts->getErrors(),'创建失败');
            }
        }
    }
}
