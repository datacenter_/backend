<?php

namespace app\modules\tournament\services;


use app\modules\common\services\Consts;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\OperationLogService;
use app\modules\common\services\TaskService;
use app\modules\data\models\DataStandardTournamentGroupStage;
use app\modules\data\services\DataTodoService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\org\models\PlayerBase;
use app\modules\org\models\PlayerBaseSnapshot;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\Team;
use app\modules\org\models\TeamIntroduction;
use app\modules\org\models\TeamIntroductionSnapshot;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\org\models\TeamPlayerRelationSnasphot;
use app\modules\org\models\TeamSnapshot;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataTournament;
use app\modules\task\services\Common;
use app\modules\tournament\models\GroupTeamRelation;
use app\modules\tournament\models\Tournament;
use app\modules\tournament\models\TournamentBase;
use app\modules\tournament\models\TournamentGroup;
use app\modules\tournament\models\TournamentPriceDistribute;
use app\modules\tournament\models\TournamentRelationTree;
use app\modules\tournament\models\TournamentStage;
use app\modules\tournament\models\TournamentTeamRelation;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\db\Exception;
use function JmesPath\search;
use yii\data\Pagination;
use app\modules\org\models\Player;
use app\modules\common\models\EnumCountry;
use app\modules\common\services\EnumService;

class TournamentService
{
    public static function getTournamentList($params)
    {

        $tournament = Tournament::find()->alias('t')
            ->select('t.*,tb.country,tb.organizer,tb.location,tb.location_cn,tb.id as tournament_base_id')
            ->leftJoin('tournament_base as tb', 'tb.tournament_id = t.id');

        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $tournament->andWhere(['t.game' => $params['game_id']]);
        }
        if (isset($params['match_name']) && !empty($params['match_name'])) {
//            $tournament->andfilterwhere(['like', 't.name', $params['match_name']]);
            $tournament->andfilterwhere(['or',
                ['like','t.name', $params['match_name']],
                ['like','t.name_cn', $params['match_name']],
                ['like','t.short_name', $params['match_name']],
                ['like','t.short_name_cn', $params['match_name']],
            ]);
        }
        if (isset($params['tournament_status']) && !empty($params['tournament_status'])) {
            $tournament->andWhere(['t.status' => $params['tournament_status']]);
        }
        if (isset($params['mach_country']) && !empty($params['mach_country'])) {
            $tournament->andWhere(['tb.country' => $params['mach_country']]);
        }
        if (isset($params['id']) && !empty($params['id'])) {
            $tournament->andWhere(['t.id' => $params['id']]);
        }


        if (isset($params['scheduled_begin_at']) && !empty($params['scheduled_begin_at']) &&
            isset($params['scheduled_end_at']) && !empty($params['scheduled_end_at']) && $params['is_null'] == 'false'
            ) {
            $scheduled_end_at = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));
            $tournament->andWhere([
                'and',
                [
                    'or',
                    ['<', 't.scheduled_begin_at', $scheduled_end_at],
                    ['>', 't.scheduled_end_at', $params['scheduled_begin_at']],


                ],
                't.scheduled_end_at is not null',
                't.scheduled_begin_at is not null'

            ]);
        }

        if ($params['is_null'] == 'true') {
            $scheduled_end_at = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at'])));
            $tournament->andWhere([
                'and',
                [
                    'or',
                    ['<', 't.scheduled_begin_at', $scheduled_end_at],
                    ['>', 't.scheduled_end_at', $params['scheduled_begin_at']],

                ],
                [
                    'or',
                    't.scheduled_end_at is  null',
                    't.scheduled_begin_at is  null'
                ]
            ]);
        }

        if (isset($params['scheduled_begin_at_1']) && !empty($params['scheduled_begin_at_1']) &&
            isset($params['scheduled_begin_at_2']) && !empty($params['scheduled_begin_at_2'])) {
            $scheduled_begin_at_2_end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_begin_at_2'])));

            $tournament->andWhere(['>=', 't.scheduled_begin_at', $params['scheduled_begin_at_1']]);
            $tournament->andWhere(['<', 't.scheduled_begin_at', $scheduled_begin_at_2_end]);
        }

        if (isset($params['scheduled_end_at_1']) && !empty($params['scheduled_end_at_1']) &&
            isset($params['scheduled_end_at_2']) && !empty($params['scheduled_end_at_2'])) {
            $scheduled_end_at_2_end = date('Y-m-d', strtotime('+1 day', strtotime($params['scheduled_end_at_2'])));

            $tournament->andWhere(['>=', 't.scheduled_end_at', $params['scheduled_end_at_1']]);
            $tournament->andWhere(['<', 't.scheduled_end_at', $scheduled_end_at_2_end]);
        }

        if (isset($params['date_begin']) && !empty($params['date_begin'])) {
            $tournament->andWhere(['>=', 't.modified_at', $params['date_begin']]);
        }
        if (isset($params['date_end']) && !empty($params['date_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['date_end'])));
            $tournament->andWhere(['<', 't.modified_at', $end]);
        }
        $tournament->andWhere(['t.type' => 1]);
        $total = $tournament->count();
        $pages = new Pagination([
            'totalCount' => $tournament->count(),
            'pageSize' => 20,  // 分页默认条数是20条
        ]);
        $pages->page = \Yii::$app->request->get('page') - 1;
        $pages->pageSize = \Yii::$app->request->get('per_page');
        $model = $tournament->groupBy('t.id')->orderBy('t.scheduled_begin_at desc , t.id desc')->offset($pages->offset)->limit($pages->limit)->asArray()->all();
//        $sql = $tournament->createCommand()->getRawSql();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        $games = array_column(EnumService::getGameLogo(), 'logo', 'id');
        $tState = array_column(EnumService::getTournamentState(), 'name', 'id');

        $resourceIds = array_column($model, 'id');
        $operationInfo = OperationLogService::operationCounts(Consts::RESOURCE_TYPE_TOURNAMENT, $resourceIds);

        foreach ($model as &$val) {
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'],66,66);
            $val['country_name'] = $country[$val['country']] ?? '';
            $val['game_logo'] = $games[$val['game']] ?? '';
            $val['state_name'] = $tState[$val['status']] ?? '';
            $val['operation'] = $operationInfo[$val['id']];
            $val['image'] = ImageConversionHelper::showFixedSizeConversion($val['image'], 66, 66, 'ali');
            $val['update_config'] = UpdateConfigService::getResourceUpdateConfigDetail($val['id'], Consts::RESOURCE_TYPE_TOURNAMENT);
            $val['binding_info'] = TeamService::actionGetBingInfo($val['id'],'tournament');

        }
        return ['list' => $model, 'total' => $total];
    }

    public static function createTournamentRelation($post)
    {
        $relation = new TournamentRelationTree();
        $relation->setAttribute('tournament_id', $post['tournament_id']);
        $relation->setAttribute('type', $post['type']);
        $relation->setAttribute('parent_id', $post['parent_id']);

        if ($relation->save() > 0) {
            return $relation->id;
        }

        throw new BusinessException($relation->getErrors(), '创建关系失败');
    }

    /**
     * @param  $id ,必须是根节点id
     * @return
     * 树状结构的赛事列表
     */
    public static function getTournametTreesList($id)
    {
        $one = StandardDataTournament::find()->where(['and', ['id' => $id]])->one();
        $sons = self::getTreeTournament($one['rel_identity_id'], $one['origin_id']);
        $one = $one->toArray();
        $one['son'] = $sons;
        return $one;
    }

    /**
     * @param $rel_identity_id
     * @param $origin_id
     * @return array
     * 根据顶级赛事获取树状赛事
     */
    public static function getTreeTournament($rel_identity_id, $origin_id)
    {
        $result = [];
        $all = StandardDataTournament::find()->where(['and',
            ['parent_id' => $rel_identity_id],
            ['origin_id' => $origin_id],
        ])->all();
        if ($all) {
            foreach ($all as $one) {
                $aa = $one->toArray();
                $aa['son'] = self::getTreeTournament($aa['rel_identity_id'], $aa['origin_id']);
                $result[] = $aa;
            }
        }
        return $result;
    }

    public static function getRelationTree($id,$type='all')
    {
        // 特定条件下，需要选择带有分组的列表，我们先根据parent_id做更新
        if($type=='only_group'){
            $parentTreeList=self::getParentListsByTournamentId($id);
            // 获取所有分组的id

            $groups=TournamentGroup::find()->where(['tournament_id' => $id])->asArray()->all();
            $ids=array_column($groups,'tree_id');
            $treeIds=[];
            foreach($ids as $groupTreeId){
                $treeIds = array_merge($treeIds,$parentTreeList[$groupTreeId]);
            }
            if((!$treeIds) || (!count($treeIds))){
                return [];
            }
            $tree = TournamentRelationTree::find()->andWhere(['in','id', $treeIds])->asArray()->all();
        }else{
            $tree = TournamentRelationTree::find()->where(['tournament_id' => $id])->asArray()->all();
        }
        $tName = Tournament::find()->select('id,name,name_cn,son_match_sort as order')->where(['id' => $id])->asArray()->one();
        // 分类取名字
        $treeType2 = $treeType3 = $treeType4 = $treeType5 = [];
        foreach ($tree as $value) {
            if ($value['type'] == 2) {
                $treeType2[] = $value['id'];
            }
            if ($value['type'] == 3) {
                $treeType3[] = $value['id'];
            }
            if ($value['type'] == 4) {
                $treeType4[] = $value['id'];
            }
            if ($value['type'] == 5) {
                $treeType5[] = $value['id'];
            }
        }
        // 子赛事
        $son = Tournament::find()->select('name,name_cn,tree_id,id,son_match_sort as order')->where(['in', 'tree_id', $treeType2])->asArray()->all();
        $sonInfo = array_column($son, 'name', 'tree_id');
        $sonIds = array_column($son, 'id', 'tree_id');
        $sonOrder = array_column($son, 'order', 'tree_id');
        // 阶段名称
        $stage = TournamentStage::find()->select('tree_id,name,name_cn,id,stage_sort as order')->where(['in', 'tree_id', $treeType3])->asArray()->all();

        $stageInfo = array_column($stage, 'name', 'tree_id');
        $stageIds = array_column($stage, 'id', 'tree_id');
        $stageOrder = array_column($stage, 'order', 'tree_id');
        // 分组
        $group = TournamentGroup::find()->select('tree_id,name,name_cn,id,order')->where(['in', 'tree_id', $treeType4])->asArray()->all();
        $groupInfo = array_column($group, 'name', 'tree_id');
        $groupIds = array_column($group, 'id', 'tree_id');
        $groupOrder = array_column($group, 'order', 'tree_id');
        // 目录
        $catalog = TournamentStage::find()->select('tree_id,name,name_cn,id,stage_sort as order')->where(['in', 'tree_id', $treeType5])->asArray()->all();
        $catalogInfo = array_column($catalog, 'name', 'tree_id');
        $catalogIds = array_column($catalog, 'id', 'tree_id');
        $catalogOrder = array_column($catalog, 'order', 'tree_id');
        // 获取已绑定的
        $bingInfo = DataStandardTournamentGroupStage::find()->where(['els_tournament_id' => $id])->asArray()->all();

        // 扩展信息标示
        foreach ($tree as $key => &$value) {
            if ($value['type'] == 2) {
                $value['standard_id'] = '';
                $value['tree_name'] = $sonInfo[$value['id']] ?? "";
                $value['primary_key'] = $sonIds[$value['id']] ?? "";
                $value['order'] = $sonOrder[$value['id']] ?? "";
            } elseif ($value['type'] == 3) {
                $value['standard_id'] = '';
                $value['tree_name'] = $stageInfo[$value['id']] ?? "";
                $value['primary_key'] = $stageIds[$value['id']] ?? "";
                $value['order'] = $stageOrder[$value['id']] ?? "";

                if (!empty($bingInfo) && !empty($value['primary_key'])) {

                    if ($standard_id = self::deep_in_array($value['primary_key'], $bingInfo)) {

                        $value['standard_id'] = $standard_id;
                    }
                }
            } elseif ($value['type'] == 5) {
                $value['standard_id'] = '';
                $value['tree_name'] = $catalogInfo[$value['id']] ?? "";
                $value['primary_key'] = $catalogIds[$value['id']] ?? "";
                $value['order'] = $catalogOrder[$value['id']] ?? "";

                if (!empty($bingInfo) && !empty($value['primary_key'])) {

                    if ($standard_id = self::deep_in_array($value['primary_key'], $bingInfo)) {

                        $value['standard_id'] = $standard_id;
                    }
                }
            } elseif ($value['type'] == 4) {
                $value['standard_id'] = '';
                $value['tree_name'] = $groupInfo[$value['id']] ?? "";
                $value['primary_key'] = $groupIds[$value['id']] ?? "";
                $value['order'] = $groupOrder[$value['id']] ?? "";

                if (!empty($bingInfo) && !empty($value['primary_key'])) {
                    if ($standard_id = self::deep_in_array($value['primary_key'], $bingInfo)) {

                        $value['standard_id'] = $standard_id;
                    }
                }
            } else {
                $value['tree_name'] = $tName['name'] ?? "";
                $value['primary_key'] = $tName['id'] ?? "";
                $value['order'] = $tName['order'] ?? "";
            }
        }

        return self::generateTree($tree, 'id', 'parent_id');
    }

    public static function generateTree($rows, $id = 'id', $pid = 'pid')
    {
        $items = array();
        foreach ($rows as $row) {
            $items[$row[$id]] = $row;
        }
        foreach ($items as $item) {
            $items[$item[$pid]]['son'][$item[$id]] = &$items[$item[$id]];
            $arr = array_column($items[$item[$pid]]['son'], 'order');
            array_multisort($arr, SORT_ASC, $items[$item[$pid]]['son']);
        }
        return isset($items[0]['son']) ? $items[0]['son'] : array();
    }

    public static function setTournament($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        $detail = $attribute;
        $parentId = isset($attribute['parent_tree_id']) ? $attribute['parent_tree_id'] : 0;
//        $type = $attribute['type'];
        $type = $attribute['core_data']['type'];
        $resourceId = @$attribute['id'];

        // 检查是新增还是修改，如果新增，设置树，如果修改，不用管树，直接修改应的类,如果不传id，就是新增
        if ($resourceId) {

        } else {
            // 新增
            // 管理tee
            $treeInfo = self::addTree($parentId, $type, '');
            $detail['tree_id'] = $treeInfo['id'];
            if($type != 1){
                $detail['tournament_id'] = $treeInfo['tournament_id'];
            }
            // 修改tree_id
        }
        $detail = self::setDetail($type, $detail, $userType, $userId, $basisId, $originId);
        if(!$resourceId && $type == 1){
            $treeInfo->setAttribute('tournament_id',$detail['id']);
            $treeInfo->save();
        }
        return $detail;
    }

    public static function addTree($parentId, $type, $tournamentId = '')
    {
        $treeModel = new TournamentRelationTree();
        // 从父里面复制tournament_id
        $parentTree = TournamentRelationTree::find()->where(['id'=>$parentId])->one();
        $treeModel->setAttributes([
            'tournament_id' => $parentTree?$parentTree['tournament_id']:0,
            'type' => $type,
            'parent_id' => $parentId,
        ]);
        if (!$treeModel->save()) {
            throw new BusinessException($treeModel->getErrors(), '保存tree失败');
        }
        return $treeModel;
    }

    public static function delTree($treeId)
    {
        // 删除tree
    }

    public static function setDetail($type, $detail, $userType, $userId, $basisId = 0, $originId = 0)
    {
        switch ($type) {
            case 1:
                // 编辑主赛事
            case 2:
                // 编辑子赛事
                $info = self::setDetailTournament($detail, $userType, $userId, $basisId, $originId);
                break;
            case 3:
                // 编辑阶段
                $info = self::setDetailStage($detail, $userType, $userId, $basisId, $originId);
                break;
            case 4:
                // 编辑分组
                $info = self::setDetailGroup($detail, $userType, $userId, $basisId, $originId);
                break;
            case 5:
                // 编辑目录
                $info = self::setDetailStage($detail, $userType, $userId, $basisId, $originId);
                break;
        }
        return $info;
    }

    public static function getDetailByTreeId($treeId)
    {
        $treeInfo = TournamentRelationTree::find()->where(['id' => $treeId])->asArray()->one();
        if ($treeInfo) {
            return self::getDetailByTypeAndTreeId($treeInfo['type'], $treeInfo['id']);
        }
    }

    public static function getDetailByTypeAndTreeId($type, $treeId)
    {
        switch ($type) {
            case 1:
                // 编辑主赛事
            case 2:
                // 编辑子赛事
                $info = self::getDetailTournament("", $treeId);
                break;
            case 3:
                // 编辑阶段
                $info = self::getDetailStage("", $treeId);
                break;
            case 4:
                // 编辑分组
                $info = self::getDetailGroup("", $treeId);
                break;
            case 5:
                // 编辑目录
                $info = self::getDetailStage("", $treeId);
                break;
        }
        return $info;
    }

    public static function getDetailTournament($id = "", $treeId = "")
    {
        if ($id) {
            $Core = Tournament::find()->where(["id" => $id])->asArray()->one();
        } elseif ($treeId) {
            $Core = Tournament::find()->where(["tree_id" => $treeId])->asArray()->one();

        }
        if (!$Core) {
            throw new BusinessException([], '不存在对应的data');
        }
        $teams_condition = $Core['teams_condition'];
        unset($Core['teams_condition']);
        $tournamentId = $Core['id'];
        $Base = TournamentBase::find()->where(['tournament_id' => $tournamentId])->asArray()->one();
        $configList = TournamentTeamRelation::find()->alias('tr')
            ->select("tr.*,tournament.name")
            ->leftJoin('tournament', 'tournament.id=tr.tournament_id')
            ->where(["tr.tournament_id" => $tournamentId])->asArray()->all();
        return [
            'id' => $tournamentId,
            Consts::TAG_TYPE_CORE_DATA => $Core,
            Consts::TAG_TYPE_BASE_DATA => $Base,
            Consts::TAG_TYPE_ATTEND_TEAM => self::getTeamRelation($tournamentId),
            Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION => $teams_condition,
            'tournament_price_distribute' => self::getTournamentPriceDistribute($tournamentId),
//            'entry_conditions' => self::getTeamRelation($tournamentId),
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($tournamentId, Consts::RESOURCE_TYPE_TOURNAMENT),
        ];
    }

    public static function setDetailTournament($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        $tournamentId = isset($attribute['id']) ? $attribute['id'] : null;
        $coreData = $attribute[Consts::TAG_TYPE_CORE_DATA];
        $baseData = $attribute[Consts::TAG_TYPE_BASE_DATA];
        //新增赛事时参赛条件空时，为数组
        if(is_array( $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]) && !array_key_exists('teams_condition',$attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION])){
            $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION] = null;
        }

//        $teamsCondition['teams_condition'] = $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION] ? $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION] : null;  //判断是前端修改
        if(is_array($attribute[Consts::TAG_TYPE_ATTEND_TEAM]) && array_key_exists('team_relation',$attribute[Consts::TAG_TYPE_ATTEND_TEAM])){
            $attribute[Consts::TAG_TYPE_ATTEND_TEAM] = $attribute[Consts::TAG_TYPE_ATTEND_TEAM]['team_relation'] ? $attribute[Consts::TAG_TYPE_ATTEND_TEAM]['team_relation'] : null;
        }
        $teamPlayer =  $attribute[Consts::TAG_TYPE_ATTEND_TEAM];

        if(isset($attribute[Consts::TAG_TYPE_ATTEND_TEAM]) && $attribute[Consts::TAG_TYPE_ATTEND_TEAM]){
            $teamPlayer = $attribute[Consts::TAG_TYPE_ATTEND_TEAM];
        }

        if(is_array($attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]) && array_key_exists('teams_condition',$attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION])){
            $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION] = $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]['teams_condition'] ? $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]['teams_condition'] : null;
        }
        $attribute['teams_condition'] =  $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION];
        if(isset($attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]) && $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION]){
            $attribute['teams_condition'] = $attribute[Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION];
        }

        //stand新增赛事的奖池
        if(isset($attribute[Consts::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE]['prize_distribution']) && $attribute[Consts::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE]['prize_distribution']){
            $attribute['tournament_price_distribute'] = $attribute[Consts::TAG_TYPE_TOURNAMENT_PRICE_DISTRIBUTE]['prize_distribution'];
        }
        if ($tournamentId && !isset($coreData["id"])) {
            $coreData["id"] = $tournamentId;
        }
        if(!$tournamentId){
            // 新增，这里修改tree_id
            if(isset($attribute['type']) && $attribute['type']){
                $coreData['type']=$attribute['type'];
            }
            $coreData['tree_id']=$attribute['tree_id'];
        }
        $tournament = self::setTournamentCoreData($coreData, $userId, $userType, $basisId, $originId);
        $tournamentId = $tournament['id'];

        self::setTournamentBaseData($tournamentId, $baseData, $userType, $userId, $basisId);
        //前端修改
        if(array_key_exists(Consts::TAG_TYPE_ATTEND_TEAM,$attribute)){
            $teamPlayer = json_decode($teamPlayer,true);
            self::setTeamRelation($tournamentId, $teamPlayer, $userType, $userId, $basisId);
        }
        //参赛条件
        if(array_key_exists('teams_condition',$attribute)){
            $condition['id'] =  $tournamentId;
            if(count($attribute['teams_condition']) > 0){
                //排序
                $teamsConditionArray = Common::sort_for_arrays(json_decode($attribute['teams_condition'],true),'team_sort');
                if($teamsConditionArray){
                    $condition['teams_condition'] =  json_encode($teamsConditionArray);
                }else{
                    $condition['teams_condition'] = null;
                }
                self::setTournamentCoreData($condition, $userId, $userType, $basisId, $originId);
            }
        }
        //奖池分配（前端修改）
        if(array_key_exists('tournament_price_distribute',$attribute)){
            self::setTournamentPriceDistribute($tournamentId, $attribute['tournament_price_distribute'], $userType, $userId, $basisId);
        }
        // 设置操作状态
        if ($basisId) {
            DataTodoService::setDealStatus($basisId, Consts::TODO_DEAL_STATUS_DONE);
        }
        return self::getTournament($tournamentId);
    }

    public static function setDetailStage($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        $type = $attribute['type'];
        $treeId = $attribute['tree_id'];
        $tournamentId=$attribute['tournament_id'];
        $attribute = $attribute['core_data'];

        if (isset($attribute['id']) && $attribute['id']) {
            $ts = TournamentStage::find()->where(['id' => $attribute['id']])->one();
            $ts->setAttributes($attribute);
            if (!$ts->save()) {
                throw new BusinessException($ts->getErrors(), '保存失败');
            }
        } else {
            $ts = new TournamentStage();
            $attribute['tree_id'] = $treeId;
            $attribute['type'] = $type;
            $attribute['tournament_id']=$tournamentId;
            $ts->setAttributes($attribute);
            if (!$ts->save()) {
                throw new BusinessException($ts->getErrors(), '保存失败');
            }
        }
        // todo 添加修改记录
        return self::getDetailStage($ts->id);
    }

    public static function getDetailStage($id = "", $treeId = "")
    {
        if ($id) {
            $tournamentStage = TournamentStage::find()->where(['id' => $id])->one();
        } elseif ($treeId) {
            $tournamentStage = TournamentStage::find()->where(['tree_id' => $treeId])->one();
        }
        $result['core_data'] = $tournamentStage->toArray();
        $result['id'] = $id;
        return $result;
    }

    public static function setDetailGroup($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        // 修改
        if(isset($attribute['tree_id'])&&isset($attribute['type'])){
            $attribute['core_data']['type'] = $attribute['type'];
            $attribute['core_data']['tree_id'] = $attribute['tree_id'];
            $attribute['core_data']['tournament_id'] = $attribute['tournament_id'];
        }
        if (isset($attribute['core_data']['id']) && $attribute['core_data']['id']) {
            $tg = TournamentGroup::find()->where(['id' => $attribute['core_data']['id']])->one();
            $tg->setAttributes($attribute['core_data']);
            if ($tg->save() > 0) {

                // 进入战队关系
                if (isset($attribute['group_team_ids']) && !empty($attribute['group_team_ids'])) {
                    $groupTeamIds = json_decode($attribute['group_team_ids'], true);
                    // `先删后加
                    GroupTeamRelation::deleteAll(['group_id' => $attribute['id']]);

                    $model = new GroupTeamRelation();
                    foreach ($groupTeamIds as $val) {
                        $_model = clone $model;
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('group_id', $tg['id']);
                        $_model->setAttribute('tournament_id', $tg['tournament_id']);

                        $_model->save();
                    }
                }

                return ['group_id' => $attribute['id'], 'msg' => '成功'];
            } else {
                throw new BusinessException($tg->getErrors(), '创建失败');
            }
        } else {
            $tg = new TournamentGroup();
            $tg->setAttributes($attribute['core_data']);
            if ($tg->save() > 0) {
                $tgId = $tg->id;

                // 战队 位置状态关系
                if (isset($attribute['group_team_ids']) && !empty($attribute['group_team_ids'])) {
                    $groupTeamIds = json_decode($attribute['group_team_ids'], true);

                    $model = new GroupTeamRelation();
                    foreach ($groupTeamIds as $val) {
                        $_model = clone $model;
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('group_id', $tgId);
                        $_model->setAttribute('tournament_id', $attribute['tournament_id']);
                        $_model->save();
                    }
                }
            } else {
                throw new BusinessException($tg->getErrors(), '创建失败');
            }
        }
        return self::getDetailGroup($tg->id);
    }

    public static function getDetailGroup($id="",$treeId="")
    {
        $result = [];
        if($id){
            $groupInfo = TournamentGroup::find()->where(['id' => $id])->asArray()->one();
        }elseif($treeId){
            $groupInfo = TournamentGroup::find()->where(['tree_id' => $treeId])->asArray()->one();
        }

        $result['core_data'] = $groupInfo;

        $groupInfo['group_team_relation'] = GroupTeamRelation::find()
            ->select('tr.*,t.name as name,t.country')
            ->alias('tr')
            ->leftJoin('team as t', 't.id = tr.team_id')
            ->where(['tr.group_id' => $groupInfo['id']])->asArray()->all();
        $country = array_column(EnumCountry::getCountry(), 'name', 'id');

        foreach ($groupInfo['group_team_relation'] as &$value) {
            $value['country_name'] = $country[$value['country']] ?? '';
        }
        $result['group_team_relation'] = $groupInfo['group_team_relation'];
        $result['id'] = $id;
        return $result;
    }


    public static function getTournament($tournamentId)
    {
        $Core = Tournament::find()->where(["id" => $tournamentId])->asArray()->one();
        $Base = TournamentBase::find()->where(['tournament_id' => $tournamentId])->asArray()->one();
        $configList = TournamentTeamRelation::find()->alias('tr')
            ->select("tr.*,tournament.name")
            ->leftJoin('tournament', 'tournament.id=tr.tournament_id')
            ->where(["tr.tournament_id" => $tournamentId])->asArray()->all();
        $teamRelation = self::getTeamRelation($tournamentId);
        //战队快照
        $teamSnapShot = [];
        foreach ($teamRelation as $value){
            if($value['type'] == 1){
                $type = Consts::RESOURCE_TYPE_TOURNAMENT;
            }
            if($value['type'] == 2){
                $type = Consts::RESOURCE_TYPE_SON_TOURNAMENT;
            }
            $itemOldTeam = TeamSnapshot::find()->where(['team_id' => $value['team_id'], 'type' => $type, 'relation_id' => $tournamentId])->one();
            $teamSnapShot[] = $itemOldTeam;
        }
        return [
            'id' => $tournamentId,
            Consts::TAG_TYPE_CORE_DATA => $Core,
            Consts::TAG_TYPE_BASE_DATA => $Base,
            Consts::TAG_TYPE_TOURNAMENT_TEAM_RELATION => $teamRelation,
            'tournament_price_distribute' => self::getTournamentPriceDistribute($tournamentId),
            'team_snapshot' => $teamSnapShot,
//            'entry_conditions' => self::getTeamRelation($tournamentId),
            'data_config' => UpdateConfigService::getResourceUpdateConfigDetail($tournamentId, Consts::RESOURCE_TYPE_TOURNAMENT),
        ];
    }

    public static function getTournamentPriceDistribute($tournamentId)
    {
        return TournamentPriceDistribute::find()->where(['tournament_id' => $tournamentId])->orderBy('id asc')->asArray()->all();
    }

    public static function getTeamRelation($tournamentId)
    {
        return TournamentTeamRelation::find()
            ->select(['tr.*', 't.name as name', 't.country', 't.image', 'country.name as country_name', 'country.e_name as country_e_name'])
            ->alias('tr')
            ->leftJoin('team as t', 't.id = tr.team_id')
            ->leftJoin('enum_country as country', 't.country=country.id')
            ->where(['tournament_id' => $tournamentId])
            ->orderBy('team_sort')->asArray()->all();
    }

    public static function setTeamRelation($tournamentId, $relations, $userType, $userId, $basisId = 0, $originId = 0)
    {
        $idsOld = [];
        $teams = TournamentTeamRelation::find()->select('team_id')->where(['tournament_id' => $tournamentId])->asArray()->all();
        if ($teams) {
            $idsOld = array_column($teams, 'team_id');
        }

        sort($idsOld);
        $old = [
            'id'=>$tournamentId,
            'rel_team_info'=>json_encode($idsOld),

        ];
        if (!is_array($relations)) {
            $relations = json_decode($relations, true);
        }
//        $idsNew = array_column($relations, 'team_id');
        $idsNew =$relations;
        sort($idsNew);
        $new =[
            'id'=>$tournamentId,
            'rel_team_info'=>json_encode($idsNew),
        ];
        // 检查
        $diffInfo = Common::getDiffInfo($old,$new);

        // 检查赛事
        if ($diffInfo['changed']) {
            TournamentTeamRelation::deleteAll(['tournament_id' => $tournamentId]);
        }
        // 检查当前赛事是主赛事还是子赛事
        $tournamentCoreInfo=Tournament::find()->where(['id'=>$tournamentId])->one();
        $type=$tournamentCoreInfo['type'];
        $oldTeams = '';
        $oldTeamsResult = TournamentTeamRelation::find()->select(['team_id'])->where(['tournament_id' => $tournamentId])->asArray()->all();
        if($oldTeamsResult){
            $oldTeams =  implode(',', array_column($oldTeamsResult,'team_id'));
        }
        $newTeamsResult = [];
        $newTeams = '';
        foreach ($relations as $key => $val) {
            $model = TournamentTeamRelation::find()->where(['tournament_id' => $tournamentId, 'team_id' => $val])->one();
            if (!$model) {
                $model = new TournamentTeamRelation();
                $model->setAttribute('tournament_id', $tournamentId);
            }
            $model->setAttribute('team_id',$val);
            $model->setAttribute('tournament_id', $tournamentId);
            $model->setAttribute('type',$type);
            if (!$model->save()) {
                throw new BusinessException($model->getErrors(), '保存关联失败');
            }
//            $newTeamsResult[]=$val['team_id'];
            $newTeamsResult[]=$val;
        }
        $time = date("Y-m-d H:i:s",time());
        $upTime = self::setTournamentUpdateTime($tournamentId,$time);

        $newTeams = implode(',',$newTeamsResult);
        //更新快照
        self::updateTeamSnapshot($newTeams,2,$tournamentId,Consts::RESOURCE_TYPE_TOURNAMENT);
//        $diffInfo = Common::getDiffInfo($idsOld, $idsNew);
        if($diffInfo['changed']){
            OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                Consts::RESOURCE_TYPE_TOURNAMENT,
                $tournamentId,
                ["diff" => $diffInfo["diff"], "new" => $new],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
    }

    /**
     * @param $newIds
     * @param $oldIds
     * @param $isUpdate
     * @param $relationId
     * @param $type
     * @return bool
     * @throws BusinessException
     * 更新战队快照
     */
    public static function updateTeamSnapshot($newIds,$isUpdate,$relationId,$type)
    {
        $tournament_id = $relationId;
        $newIds = explode(',', $newIds);
        if (empty($newIds) && empty($oldIds)) {
            throw new BusinessException([], '更新战队ids为空');
        }
        if (in_array($type, ['tournament', 'group', 'son-tournament']) && $isUpdate != 1) {
            $ids = $newIds;
        } else {
            $ids = $newIds;
        }
        if (empty($ids)) {
            return true;
        }
        //获取战队信息
        $team = Team::find()->where(['in', 'id', $ids])->asArray()->all();
        $newIdsRes = [];
        $newTeamIdsRes = [];
        $teamIdsNewAdd = [];
        foreach ($team as $value) {
            $isHaveRelation = 1;
            $value['team_id'] = $value['id'];
            $value['type'] = $type;
            $value['relation_id'] = $relationId;
            $value['modified_at'] = date('Y-m-d H:i:s',time());
            $newTeam = TeamSnapshot::find()->where(['team_id' => $value['id'], 'type' => $type, 'relation_id' => $relationId])->one();
            if (empty($newTeam) || !$newTeam) {
                $newTeam = new TeamSnapshot();
                $newTeam->setAttributes($value);
                $newTeam->save();
                $isHaveRelation = 0;
            }else{
                // 1:更新快照按钮 更新所有
                if ($isUpdate == 1){
                    $newTeam->setAttributes($value);
                    $newTeam->save();
                }
            }
            $teamId = $newTeam->id;
            $newIdsRes[$value['id']] = $teamId;
//            //查询赛事和队伍关系表
//            $TournamentTeamRelationRes = TournamentTeamRelation::find()->where(['team_id' => $value['id'], 'tournament_id' => $relationId])->one();
//            if (empty($TournamentTeamRelationRes) || !$TournamentTeamRelationRes){
//                $newTeamIdsRes[] = $teamId;
//            }
            //没有战队快照才更新
            if ($isHaveRelation==0){
                $newTeamIdsRes[] = $teamId;
                // 需要刷新关联关系的player
                $teamIdsNewAdd[]=$newTeam['team_id'];
            }
            // 获取战队基础数据
            $ti = TeamIntroduction::find()->where(['team_id' => $value['id']])->asArray()->one();
            if (!empty($ti) || $ti) {
                $teamInt = TeamIntroductionSnapshot::find()->where(['team_id' => $teamId])->one();
                if (empty($teamInt) || !$teamInt) {
                    $teamInt = new TeamIntroductionSnapshot();
                    $ti['team_id'] = $teamId;
                    $teamInt->setAttributes($ti);
                    $teamInt->save();
                }else{
                    // 1:更新快照按钮 更新所有
                    if ($isUpdate == 1){
                        $ti['team_id'] = $teamId;
                        $teamInt->setAttributes($ti);
                        $teamInt->save();
                    }
                }
            }
        }
        // 1:更新快照按钮 更新所有
        if ($isUpdate == 1){
            if ($newIdsRes){
                foreach ($newIdsRes as $item){
                    //删除原有的比赛队伍信息
                    TeamPlayerRelationSnasphot::deleteAll(['team_id'=>$item]);
                }
            }
        }else{
             $ids = $teamIdsNewAdd;
        }
        if ($ids){
            // 战队选手关系
            $tr = TeamPlayerRelation::find()->where(['in', 'team_id', $ids])->asArray()->all();
            foreach ($tr as $v) {
                unset($v['id']);
                foreach ($newIdsRes as $key => $val) {
                    if ($v['team_id'] == $key) {
                        // 查看是否有关联关系 如果有就更新，没有就添加
                        $obj = TeamPlayerRelationSnasphot::find()->where(['player_id' => $v['player_id'], 'team_id' => $val])->one();
                        if(!$obj){
                            $obj = new TeamPlayerRelationSnasphot();
                        }
                        $v['team_id'] = $val;
                        $obj->setAttributes($v);
                        $obj->save();
                        $relationId = $obj->id;
                        // 选手
                        $player = Player::find()
                            ->where(['id' => $v['player_id']])->asArray()->one();
                        if (!empty($player) && $player) {
                            $p = PlayerSnapshot::find()->where(['player_id' => $player['id'], 'relation_id' => $relationId])->one();
                            if (empty($p) || !$p) {
                                $p = new PlayerSnapshot();
                                $player['created_at'] = date('Y-m-d H:i:s',time());
                            }
                            unset($player['id']);
                            $player['modified_at'] = date('Y-m-d H:i:s',time());
                            $player['player_id'] = $v['player_id'];
                            $player['relation_id'] = $relationId;
                            $p->setAttributes($player);
                            $p->save();
                            $playerId = $p->id;
                            // todo 这里的数据会有问题
                            // 选手
                            $playerBase = PlayerBase::find()
                                ->where(['player_id' => $v['player_id']])->asArray()->one();
                            if (!empty($playerBase) && $playerBase) {
                                $objBase = PlayerBaseSnapshot::find()->where(['player_id' => $playerId])->one();
                                if (empty($objBase) || !$objBase) {
                                    $objBase = new PlayerBaseSnapshot();
                                }
                                $playerBase['player_id'] = $playerId;
                                $objBase->setAttributes($playerBase);
                                $objBase->save();

                            }
                        }
                    }
                }
            }
        }

        $infoMainIncrement  = ["diff" => [], "new" => ['id'=>$tournament_id]];
        TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_TOURNAMENT,OperationLogService::OPERATION_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_ATTEND_TEAM);
        return true;
    }
    public static function setTournamentPriceDistribute($tournamentId, $relations, $userType = 0, $userId = 0, $basisId = 0, $originId = 0)
    {
        // 设置
        $oldTeamsJson = '';
        $newTeamsJson = '';
        $old =[];
        $new =[];
        $oldTeams = TournamentPriceDistribute::find()->select("rank,price,score,num,team_id")->where(['tournament_id' => $tournamentId])->orderBy('id asc')->asArray()->all();
        if($oldTeams){
            $oldTeamsJson = json_encode($oldTeams);
            $old = [
                'id'=>$tournamentId,
                'rel_price_distribute_info'=>$oldTeamsJson
            ];
        }
        if(!is_array($relations)){
            $newTeamsJson = $relations;
        }
        if($oldTeamsJson == $newTeamsJson){
            return true;
        }
        TournamentPriceDistribute::deleteAll(['tournament_id' => $tournamentId]);

        $relations = json_decode($relations, true);
        foreach ($relations as $key => $val) {
            $model = new TournamentPriceDistribute();
            $model->setAttribute('tournament_id', $tournamentId);
            $model->setAttributes($val);
            if (!$model->save()) {
                throw new BusinessException($model->getErrors(), '保存关联失败');
            }
        }
        $time = date("Y-m-d H:i:s",time());
        $upTime = self::setTournamentUpdateTime($tournamentId,$time);

        $newTeams = TournamentPriceDistribute::find()->select("rank,price,score,num,team_id")->where(['tournament_id' => $tournamentId])->orderBy('id asc')->asArray()->all();
        if($newTeams){
            $newTeamsJson = json_encode($newTeams);
            $new = [
                'id'=>$tournamentId,
                'rel_price_distribute_info'=>$newTeamsJson
            ];
        }
        $diffInfo = Common::getDiffInfo($old,$new);

        if($diffInfo['changed']){
            OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                Consts::RESOURCE_TYPE_TOURNAMENT,
                $tournamentId,
                ["diff" => $diffInfo['diff'], "new" => $new],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
    }

    public static function setTournamentCoreData($attribute, $userType, $userId, $basisId = 0, $originId = 0)
    {
        // 这里判断必填项
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldTournament = [];
        if (isset($attribute['id']) && $attribute['id']) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $tournament = Tournament::find()->where(['id' => $attribute['id']])->one();
            //
            if (!$tournament) {
                throw new BusinessException($attribute, "不存在这个tournament");
            }
            $logOldTournament = $tournament->toArray();
            $tournament->setScenario('bing_tournament');
            $tournament->setAttributes($attribute);
            if (!$tournament->save()) {
                throw new BusinessException($tournament->getErrors(), "设置core_data失败");
            }
        } else {
            $attribute['cuser'] = $userId;
            $tournament = new Tournament();
            $tournament->setScenario('bing_tournament');
            $tournament->setAttributes($attribute);

            if (!$tournament->save()) {
                throw new BusinessException($tournament->getErrors(), "设置core_data失败");
            }

            // 创建关系
            if ($basisId != 0) {
                \Yii::$app->db->createCommand()->batchInsert('data_change_config', ['basis_id', 'resource_type', 'resource_id', 'origin_id'],
                    [[$basisId, 'tournament', $tournament->id, $originId]])->execute();
            }
            if (isset($attribute["game"]) && $attribute["game"]){
                //初始化更新
                UpdateConfigService::initResourceUpdateConfig($tournament["id"], Consts::RESOURCE_TYPE_TOURNAMENT, $attribute["game"]);
            }

        }
        $logNewTournament = $tournament->toArray();
        // 记录log
        $filter = array_keys($logNewTournament);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldTournament, $logNewTournament, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_TOURNAMENT,
                $logNewTournament['id'],
                ["diff" => $diffInfo["diff"], "new" => $logNewTournament],
                0,
                Consts::TAG_TYPE_CORE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $tournament;
    }


    public static function setTournamentUpdateTime($Tournamen,$updateTime)
    {
        return Tournament::updateAll(['modified_at'=>$updateTime],['id'=>$Tournamen]);
    }

    public static function setTournamentBaseData($tournamentId, $attribute, $userType, $userId, $basisId = 0)
    {
        // 这里不用id，以免产生冲突
        $operationType = OperationLogService::OPERATION_TYPE_ADD;
        $logOldTournamentBase = [];
        $updateData = $attribute;
        unset($updateData['id']);
        $updateData['tournament_id'] = $tournamentId;
        $tournamentBase = TournamentBase::find()->where(['tournament_id' => $tournamentId])->one();
        if ($tournamentBase) {
            $operationType = OperationLogService::OPERATION_TYPE_UPDATE;
            $logOldTournamentBase = $tournamentBase->toArray();
            $tournamentBase->setAttributes($attribute);
        } else {
            $tournamentBase = new TournamentBase();
            $tournamentBase->setAttributes($updateData);
        }
        if (!$tournamentBase->save()) {
            throw new BusinessException($tournamentBase->getErrors(), "设置base_data失败");
        }
        //奖池分配(数据源stand表新增数据)
        if(isset($attribute['prize_distribution']) && $attribute['prize_distribution']){
            self::setTournamentPriceDistribute($tournamentId, $attribute['prize_distribution'], $userType, $userId, $basisId);
        }
//        //战队(数据源stand表新增)
//        if(isset($attribute['teams_condition']) && $attribute['teams_condition']){
//            self::setTeamRelation($tournamentId, $attribute['teams_condition'], $userType, $userId, $basisId);
//        }

        $logNewTournamentBase = $tournamentBase->toArray();

        $time = date("Y-m-d H:i:s",time());
        $upTime = self::setTournamentUpdateTime($tournamentId,$time);
        // 记录log
        $filter = array_keys($logNewTournamentBase);
        $filter = array_diff($filter, ["created_at", "modify_at"]);
        $diffInfo = Common::getDiffInfo($logOldTournamentBase, $logNewTournamentBase, $filter);
        if ($diffInfo['changed']) {
            OperationLogService::addLog($operationType,
                Consts::RESOURCE_TYPE_TOURNAMENT,
                $tournamentId,
                ["diff" => $diffInfo["diff"], "new" => $logNewTournamentBase],
                0,
                Consts::TAG_TYPE_BASE_DATA,
                null,
                $basisId,
                $userType,
                $userId
            );
        }
        return $tournamentBase;
    }


    public static function deep_in_array($value, $array, $key = 'standard_id')
    {
        foreach ($array as $item) {
            if (in_array($value, $item)) {
                return $item[$key];
            }
        }
        return false;

    }

    public static function removeTeamSnapshot($ids, $type, $relationId)
    {
        if (empty($ids)) {
            return false;
        }
        $teamIds = TeamSnapshot::find()->where(['IN', 'team_id', $ids])
            ->andWhere(['type' => $type])
            ->andWhere(['relation_id' => $relationId])
            ->asArray()->all();
        TeamSnapshot::deleteAll(['AND', 'type = :type', ['IN', 'team_id', $ids]], [':type' => $type]);

        $teamIds = array_column($teamIds, 'id');
        // 获取战队基础数据
        TeamIntroductionSnapshot::deleteAll(['IN', 'team_id', $teamIds]);

        // 战队选手关系
        $tr = TeamPlayerRelationSnasphot::find()->where(['in', 'team_id', $teamIds])->asArray()->all();
        TeamPlayerRelationSnasphot::deleteAll(['IN', 'team_id', $teamIds]);

        $relationIds = array_column($tr, 'id');
        $player = PlayerSnapshot::find()->where(['in', 'relation_id', $relationIds])->asArray()->all();
        PlayerSnapshot::deleteAll(['in', 'relation_id', $relationIds]);
        PlayerBaseSnapshot::deleteAll(['in', 'player_id', array_column($player, 'id')]);

        return true;
    }


    public static function add($attribute, $userId)
    {
        if (isset($attribute['id']) && $attribute['id']) {
            $logOld = TournamentService::getTournament($attribute['id']);
            $tournament = Tournament::find()->where(['id' => $attribute['id']])->one();
            if ($attribute['type'] == 1) {
                $tournament->setScenario('tournament');
            } elseif ($attribute['type'] == 2) {
                $tournament->setScenario('son_tournament');
            }

            $tournament->setAttributes($attribute['core_data']);
            if ($tournament->save() > 0) {
                $tournamentBase = TournamentBase::find()->where(['tournament_id' => $attribute['id']])->one();
                $attribute['tournament_id'] = $attribute['id'];
                if (!empty($tournamentBase)) {
                    $tournamentBase->setAttributes($attribute['base_data']);
                } else {
                    $tournamentBase = new TournamentBase();
                    $tournamentBase->setAttributes($attribute['base_data']);
                }
                if ($tournamentBase->save() <= 0) {
                    throw new BusinessException($tournamentBase->getErrors(), '基础信息添加失败');
                }
                // 奖池分配
                if (isset($attribute['tournament_price_distribute']) && !empty($attribute['tournament_price_distribute'])) {
                    $priceDistributeInfo = json_decode($attribute['tournament_price_distribute'], true);
                    TournamentPriceDistribute::deleteAll(['tournament_id' => $attribute['id']]);

                    $model = new TournamentPriceDistribute();
                    foreach ($priceDistributeInfo as $val) {
                        $_model = clone $model;
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('team_num', $val['team_num']);
                        $_model->setAttribute('rank', $val['rank']);
                        $_model->setAttribute('price', $val['price']);
                        $_model->setAttribute('score', $val['score']);
                        $_model->setAttribute('num', $val['num']);
                        $_model->setAttribute('tournament_id', $attribute['id']);
                        $_model->save();
                    }
                }
                // 进入选手关系
                if (isset($attribute['tournament_team_relation']) && !isset($attribute['tournament_team_ids'])) {
                    $attribute['tournament_team_ids'] = $attribute['tournament_team_relation'];
                }

                if (isset($attribute['tournament_team_ids']) && !empty($attribute['tournament_team_ids'])) {
                    $tournamentTeamIds = json_decode($attribute['tournament_team_ids'], true);
                    // 先删后加
                    TournamentTeamRelation::deleteAll(['tournament_id' => $attribute['id']]);

                    $model = new TournamentTeamRelation();
                    foreach ($tournamentTeamIds as $key => $val) {
                        $_model = clone $model;
                        $_model->setAttribute('tournament_id', $attribute['id']);
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('type', $attribute['type']);
                        $_model->setAttribute('match_condition', $val['match_condition']);
                        $_model->setAttribute('match_condition_cn', $val['match_condition_cn']);
                        $_model->setAttribute('team_sort', $val['team_sort']);

                        $_model->save();
                    }
                }
            }
            if ($attribute['type'] == 1) {
                $logNew = TournamentService::getTournament($attribute['id']);
                // 添加修改log
                $diffInfo = Common::getDiffInfo($logOld, $logNew, [], true);
                if ($diffInfo["changed"]) {
                    OperationLogService::addLog(OperationLogService::OPERATION_TYPE_UPDATE,
                        Consts::RESOURCE_TYPE_TOURNAMENT,
                        $attribute['id'],
                        ["diff" => $diffInfo["diff"], "new" => $logNew],
                        0,
                        Consts::TAG_TYPE_BASE_DATA,
                        null,
                        0,
                        Consts::USER_TYPE_ADMIN,
                        $userId
                    );
                }
            }
            return ['tournament_id' => $attribute['id'], 'msg' => '成功'];
        } else {
            $tournament = new Tournament();
            if ($attribute['type'] == 1) {
                $tournament->setScenario('tournament');
            } elseif ($attribute['type'] == 2) {
                $tournament->setScenario('son_tournament');
            }
            $tournament->setAttributes($attribute['core_data']);
            $tournament->setAttribute('cuser', $userId);
            if ($tournament->save() > 0) {
                $tournamentId = $attribute['tournament_id'];
                unset($attribute['tournament_id']);
                $tId = $tournament->id;
                $tournamentBase = new TournamentBase();
                $tournamentBase->setAttribute('tournament_id', $tId);
                $tournamentBase->setAttributes($attribute['base_data']);
                if ($tournamentBase->save() <= 0) {
                    throw new BusinessException($tournamentBase->getErrors(), '基础信息添加失败');
                }
                // 奖池分配
                if (isset($attribute['tournament_price_distribute']) && !empty($attribute['tournament_price_distribute'])) {
                    $priceDistributeInfo = json_decode($attribute['tournament_price_distribute'], true);
                    $model = new TournamentPriceDistribute();
                    foreach ($priceDistributeInfo as $val) {
                        $_model = clone $model;
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('team_num', $val['team_num']);
                        $_model->setAttribute('rank', $val['rank']);
                        $_model->setAttribute('price', $val['price']);
                        $_model->setAttribute('score', $val['score']);
                        $_model->setAttribute('num', $val['num']);
                        $_model->setAttribute('tournament_id', $tId);
                        $_model->save();
                    }
                }

                // 战队 赛事关系
                if (isset($attribute['tournament_team_ids']) && !empty($attribute['tournament_team_ids'])) {
                    $tournamentTeamIds = json_decode($attribute['tournament_team_ids'], true);

                    $model = new TournamentTeamRelation();
                    foreach ($tournamentTeamIds as $val) {
                        $_model = clone $model;
                        $_model->setAttribute('team_id', $val['team_id']);
                        $_model->setAttribute('tournament_id', $tId);

                        $_model->setAttribute('type', $attribute['type']);
                        $_model->setAttribute('match_condition', $val['match_condition']);
                        $_model->setAttribute('match_condition_cn', $val['match_condition_cn']);
                        $_model->setAttribute('team_sort', $val['team_sort']);

                        $_model->save();
                    }
                }

                // 加入赛事关系树
                if (isset($attribute['type']) && $attribute['type'] == 2) {

                    $tree = new TournamentRelationTree();
                    $tree->setAttribute('tournament_id', $tournamentId);
                    $tree->setAttribute('type', 2);
                    $tree->setAttribute('parent_id', $attribute['tree_parent_id']);
                    $tree->save();

                    $t = Tournament::find()->where(['id' => $tId])->one();
                    $t->setScenario('son_tournament');
                    $t->setAttribute('tree_id', $tree->id);
                    $t->setAttribute('name', $attribute['name']);
                    $t->save();
                } else {
                    $tree = new TournamentRelationTree();
                    $tree->setAttribute('tournament_id', $tId);
                    $tree->setAttribute('type', 1);
                    $tree->save();
                }
                if ($attribute['type'] == 1) {
                    $Introduction = $tournamentBase->toArray();
                    $tournamentInfo = $tournament->toArray();
                    $logNew = array_merge($Introduction, $tournamentInfo);
                    // 添加修改log
                    $diffInfo = Common::getDiffInfo([], $logNew, [], true);
                    if ($diffInfo["changed"]) {
                        OperationLogService::addLog(OperationLogService::OPERATION_TYPE_ADD,
                            Consts::RESOURCE_TYPE_TOURNAMENT,
                            $logNew['id'],
                            ["diff" => $diffInfo["diff"], "new" => $logNew],
                            0,
                            Consts::TAG_TYPE_BASE_DATA,
                            null,
                            0,
                            Consts::USER_TYPE_ADMIN,
                            $userId
                        );
                    }
                }
//                if ($attribute['type'] == 1) {
//                    $resourceType = $attribute['type'] == 1 ? Consts::RESOURCE_TYPE_TOURNAMENT : Consts::RESOURCE_TYPE_SON_TOURNAMENT;
//                    UpdateConfigService::initResourceUpdateConfig($logNew["id"], $resourceType, $attribute["game"]);
//                }
                return ['tournament_id' => $tId, 'msg' => '成功'];
            } else {
                throw new BusinessException($tournament->getErrors(), '创建失败');
            }
        }

    }

    public static function setSnapshot($tournamentId, $teamIds, $adminType = 1, $userId = 0)
    {
        // 赛事管理
    }

    private function treeArray($data, $parent_key, $child_key)
    {
        $tree = [];
        foreach ($data as $key => $val) {
            if ($val[$parent_key] == 0) {
                $tree[] = &$data[$key];
            } else {
                $data[$val[$parent_key]][$child_key][] = &$data[$key];
            }
        }
        return $tree;
    }

    public static function getRelTeamInfoByStageId($stageId,$type='stage')
    {
        $groupParentList=[];
        if($type=='stage'){
            $stageInfo=TournamentStage::find()->where(['id'=>$stageId])->asArray()->one();
            $tournamentId=$stageInfo['tournament_id'];
            $parentLists=self::getParentListsByTournamentId($tournamentId);
            $groupParentList=$parentLists[$stageInfo['tree_id']];
        }elseif($type=='group'){
            $groupInfo=TournamentGroup::find()->where(['id'=>$stageId])->asArray()->one();
            $tournamentId=$groupInfo['tournament_id'];
            $parentLists=self::getParentListsByTournamentId($tournamentId);
            $groupParentList=$parentLists[$groupInfo['tree_id']];
        }

        $trees=TournamentRelationTree::find()->andWhere(['in','id',$groupParentList])->asArray()->all();
        $teamInfoType=1;
        $teamInfoId=$tournamentId;
        foreach($trees as $key=>$val){
            if($val['type']==2){
                $teamInfoType='2';
                $subTournament=Tournament::find()->where(['tree_id'=>$val['id']])->one();
                $teamInfoId=$subTournament['id'];
                break;
            }
        }
        // 获取战队信息
        return [
            'type'=>$teamInfoType,
            'tournament_id'=>$teamInfoId,
        ];

    }

    public static function getParentListsByTournamentId($id)
    {
        $re=TournamentRelationTree::find()->where(['tournament_id'=>$id])->asArray()->all();
        $list=array_column($re,null,'id');
        $parentIdLists=[];
        foreach($list as $tId=>$tVal){
            $parentIdLists[$tId]=self::getParentList($tId,$list);
        }
        return $parentIdLists;
    }

    private static function getParentList($id,$list)
    {
        $parentId=$list[$id]['parent_id'];
        if($parentId){
            $parentInfo=$list[$parentId];
        }
        if(isset($parentInfo)&&$parentInfo){
            $parentList=self::getParentList($parentId,$list);
            return array_merge([$id],$parentList);
        }else{
            return [$id];
        }
    }

    //获取赛事下所有比赛
    public static function getMatchListByTournamentId($tournamentId){
        $matchList = Match::find()->where(['tournament_id'=>$tournamentId])->asArray()->all();
//        $sql = $matchList->createCommand()->getRawSql();
        return $matchList;
    }

    public static function teamSnapshotEdit($params) {
        if (!isset($params['team_id'])) {
            throw new BusinessException([], "请传战队快照id");
        }

        if (!isset($params['relation_id'])) {
            throw new BusinessException([], "请传战队快照赛事id");
        }

        if (!isset($params['type'])) {
            throw new BusinessException([], "请传战队快照类型");
        }
        $where = [];
        if (isset($params['name'])) {
            $where['name'] = $params['name'];
        }

        if (isset($params['country'])) {
            $where['country'] = $params['country'];
        }
        if (isset($params['full_name'])) {
            $where['full_name'] = $params['full_name'];
        }
        if (isset($params['short_name'])) {
            $where['short_name'] = $params['short_name'];
        }

        if (isset($params['slug'])) {
            $where['slug'] = $params['slug'];
        }

        if (isset($params['image'])) {
            $where['image'] = $params['image'];
        }

        $teamSnapshot = TeamSnapshot::find()->where(
            ['team_id' => $params['team_id'] ,'relation_id'=>$params['relation_id'],'type'=>$params['type']])->one();
        if (!$teamSnapshot) {
            throw new BusinessException([], "不存在这个战队快照");
        }


        if (isset($params['world_ranking'])) {
            $team_introduction_snapshot = TeamIntroductionSnapshot::find()->where(['team_id'=> $teamSnapshot['id']])->one();
            if (!$team_introduction_snapshot) {
                $world_ranking['team_id'] = $teamSnapshot['id'];
                $team_introduction_snapshot = new TeamIntroductionSnapshot;
            }
            $world_ranking['world_ranking'] = $params['world_ranking'];
            $team_introduction_snapshot->setAttributes($world_ranking);
            if (!$team_introduction_snapshot->save()) {
                throw new BusinessException($teamSnapshot->getErrors(), "编辑世界排名失败");
            }

        }

        $teamSnapshot->setAttributes($where);
        if (!$teamSnapshot->save()) {
            throw new BusinessException($teamSnapshot->getErrors(), "编辑失败");
        }
        return $teamSnapshot->toArray();
    }

    public static function teamDetailSnapshot($team_id,$type,$resourceId) {
        $team = TeamSnapshot::find()
            ->select('ts.team_id as els_team_id,
            ts.name as team_name,
            ts.game as game_id,
            ts.id as snapshot_team_id,ts.image,ts.slug,ts.country,ts.full_name,ts.short_name
            ,tis.world_ranking
            ,enum_country.name as country_name,
            ,enum_country.e_name as country_e_name,
            ')
            ->alias('ts')
            ->leftJoin('team_introduction_snapshot as tis', 'tis.team_id = ts.id')
            ->leftJoin('enum_country', 'ts.country = enum_country.id');
        if (!empty($team_id)){
            $team->where(['ts.team_id'=> $team_id]);
        }
        $team = $team->andWhere(['ts.type' => $type])->andWhere(['ts.relation_id' => $resourceId])->asArray()->one();
        if (empty($team)){
            return [];
        }
        $player = PlayerSnapshot::find()
            ->select('
            ps.player_id as els_player_id,
            tprs.team_id as team_id,
            ps.id as snapshot_player_id,ps.slug as player_slug,ps.image as player_image,
            ps.role,ps.name as player_name,ps.name_cn as player_name_cn,ps.player_country,tprs.status_id,
            ps.nick_name,
            ps.player_country,
            enum_country.name as country_name,
            enum_country.e_name as country_e_name,
            ps.role,
            enum_position.name as role_name,
            enum_position.c_name as role_e_name
            '
            )
            ->alias('ps')
            ->leftJoin('team_player_relation_snapshot as tprs', 'ps.relation_id = tprs.id')
            ->leftJoin('enum_country', 'ps.player_country = enum_country.id')
            ->leftJoin('enum_position', 'ps.role = enum_position.id')
            ->where([ 'tprs.team_id'=>$team['snapshot_team_id']])->asArray()->all();

        $country = array_column(EnumCountry::getCountry(), 'name', 'id');
        foreach ($player as &$val) {
            $val['player_country'] = $country[$val['player_country']] ?? '';
        }

        $team['team_country'] = $country[$team['country']] ?? '';
        $team['players'] = $player;
        $infoMainIncrement  = ["diff" => [], "new" => ['id'=>$resourceId]];
        TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_TOURNAMENT,OperationLogService::OPERATION_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_ATTEND_TEAM);
        return $team;
    }

    public static function playerSnapshotAdd($team_id,$type,$resourceId,$players) {
        if (empty($team_id) || empty($type) || empty($resourceId)) {
            throw new BusinessException([], '必选项不可为空');
        }


        $newTeam = TeamSnapshot::find()->where(['team_id' => $team_id, 'type' => $type, 'relation_id' => $resourceId])->one();
        if (empty($newTeam)) {
            throw new BusinessException([], '没有战队快照');
        }

        //删除原有的比赛队伍信息
        TeamPlayerRelationSnasphot::deleteAll(['team_id'=>$newTeam['id']]);

        foreach ($players as $key => $val ) {
            $obj = TeamPlayerRelationSnasphot::find()->where(['player_id' => $val, 'team_id' => $newTeam['id']])->one();
            if(!$obj){
                $obj = new TeamPlayerRelationSnasphot();
            }
            $v['team_id'] = $newTeam['id'];
            $v['player_id'] = $val;
            $obj->setAttributes($v);
            $obj->save();
            $relationId = $obj->id;

            $player = Player::find()
                ->where(['id' => $v['player_id']])->asArray()->one();

            if (!empty($player) && $player) {
                $p = PlayerSnapshot::find()->where(['player_id' => $player['id'], 'relation_id' => $relationId])->one();
                if (empty($p) || !$p) {
                    $p = new PlayerSnapshot();
                    $player['created_at'] = date('Y-m-d H:i:s',time());
                }
                unset($player['id']);
                $player['modified_at'] = date('Y-m-d H:i:s',time());
                $player['player_id'] = $v['player_id'];
                $player['relation_id'] = $relationId;
                $p->setAttributes($player);
                $p->save();
                $playerId = $p->id;
                // todo 这里的数据会有问题
                // 选手
                $playerBase = PlayerBase::find()
                    ->where(['player_id' => $v['player_id']])->asArray()->one();
                if (!empty($playerBase) && $playerBase) {
                    $objBase = PlayerBaseSnapshot::find()->where(['player_id' => $playerId])->one();
                    if (empty($objBase) || !$objBase) {
                        $objBase = new PlayerBaseSnapshot();
                    }
                    $playerBase['player_id'] = $playerId;
                    $objBase->setAttributes($playerBase);
                    $objBase->save();

                }
            }

        }
        $infoMainIncrement  = ["diff" => [], "new" => ['id'=>$resourceId]];
        TaskService::addTaskMainIncrement(Consts::RESOURCE_TYPE_TOURNAMENT,OperationLogService::OPERATION_TYPE_UPDATE,$infoMainIncrement,Consts::TAG_TYPE_ATTEND_TEAM);

        return $p;

    }
}