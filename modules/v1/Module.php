<?php

namespace app\modules\v1;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => \app\modules\admin\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'common' => [
                'class' => \app\modules\common\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'task' => [
                'class' => \app\modules\task\Module::class,
            ],
            'file' => [
                'class' => \app\modules\file\Module::class,
            ],
            'org' => [
                'class' => \app\modules\org\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'tournament' => [
                'class' => \app\modules\tournament\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'metadata' => [
                'class' => \app\modules\metadata\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'match' => [
                'class' => \app\modules\match\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'data' => [
                'class' => \app\modules\data\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'account' => [
                'class' => \app\modules\account\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'operation' => [
                'class' => \app\modules\operation\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'receive' => [
                'class' => 'app\modules\receive\Module',
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
        ];
    }
}
