<?php
namespace WBY\MQ\SDK\ProxyMQ;

/**
 * @deprecated
 * Class Message
 * @package WBY\MQ\SDK\ProxyMQ
 */
class Message
{
    /**
     * @var string 消息内容
     */
    private $messageBody;

    /**
     * @var string 区分业务
     */
    private $key;

    public function __construct($messageBody, $key)
    {
        $this->messageBody = $messageBody;
        $this->key = $key;
    }

    public function getMessageBody()
    {
        return $this->messageBody;
    }

    public function getKey()
    {
        return $this->key;
    }
}