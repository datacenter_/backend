<?php
namespace WBY\MQ\SDK\ProxyMQ;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Promise\PromiseInterface;

/**
 * @deprecated
 * Class ProxyPromise
 * @package WBY\MQ\SDK\ProxyMQ
 */
class ProxyPromise
{
    private $response;
    private $promise;

    public function __construct(PromiseInterface &$promise, Response &$response)
    {
        $this->promise = $promise;
        $this->response = $response;
    }

    public function isCompleted()
    {
        return $this->promise->getState() != 'pending';
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getState()
    {
        return $this->promise->getState();
    }

    public function wait()
    {
        try {
            $res = $this->promise->wait();
            if ($res instanceof ResponseInterface)
            {
                return $this->response->parseResponse($res->getStatusCode(), $res->getBody()->getContents());
            }
        } catch (TransferException $e) {
            $message = $e->getMessage();
            $this->response->parseErrorResponse($e->getCode(), $message);
        }

        $this->response->parseErrorResponse("5000", "Unknown");
    }
}
