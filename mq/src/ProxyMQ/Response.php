<?php
namespace WBY\MQ\SDK\ProxyMQ;

/**
 * @deprecated
 * Class Response
 * @package WBY\MQ\SDK\ProxyMQ
 */
class Response
{
    private $succeed;

    public function parseResponse($statusCode, $body)
    {
        if ($statusCode == 200 || $statusCode == 201) {
            $this->succeed = true;
        }

        $content = $this->readJSONContent($body);
        if ((int)$content['code'] != 1000 && (int)$content['code'] != 200) {
            return $this->parseErrorResponse((int)$content['code'], $body);
        }

        return [
            'code' => 1000,
            'result' => 'success',
            'content' => $content
        ];
    }

    public function parseErrorResponse($statusCode, $body)
    {
        $this->succeed = false;
        $content = $this->readJSONContent($body);

        return [
            'code' => $statusCode,
            'result' => 'failed',
            'content' => $content
        ];
    }

    /**
     * @param $body string
     * @return array|mixed
     */
    private function readJSONContent($body)
    {
        if (strlen($body) > 0) {
            return json_decode($body, true);
        }

        return [];
    }
}