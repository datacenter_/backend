<?php
namespace WBY\MQ\SDK\RocketMQ;

use MQ\Config;
use MQ\MQClient;

class Client
{
    /** @var MQClient */
    private $client;

    /**
     * ```
     * $config = [
     *      'endPoint' => '',       //required
     *      'accessId' => '',       //required
     *      'accessKey' => '',      //required
     *      'securityToken' => '',  // default null
     *      'proxy' => '',          // default null
     *      'requestTimeout' => '', // default null
     *      'connectTimeout' => '', // default null
     *      'expectContinue' => '', // default null
     * ]
     * ```
     * @param array $config
     */
    public function __construct(array $config)
    {
        $endPoint = $config['endPoint'];
        $accessId = $config['accessId'];
        $accessKey = $config['accessKey'];
        $securityToken = isset($config['securityToken']) ? $config['securityToken'] : null;

        $httpConfig = new Config();

        if (isset($httpOptions['proxy'])) {
            $httpConfig->setProxy($config['proxy']);
        }

        if (isset($httpOptions['requestTimeout'])) {
            $httpConfig->setProxy($config['requestTimeout']);
        }

        if (isset($httpOptions['connectTimeout'])) {
            $httpConfig->setProxy($config['connectTimeout']);
        }

        if (isset($httpOptions['expectContinue'])) {
            $httpConfig->setProxy($config['expectContinue']);
        }

        $this->client = new MQClient($endPoint, $accessId, $accessKey, $securityToken, $httpConfig);
    }

    /**
     * 生产者
     * @param string $instanceId
     * @param string $topicName
     * @return \MQ\MQProducer
     */
    public function getProducer($instanceId, $topicName)
    {
        return $this->client->getProducer($instanceId, $topicName);
    }

    /**
     * 消费者
     * @param string $instanceId
     * @param string $topicName
     * @param string $groupId
     * @param null|string $messageTag
     * @return \MQ\MQConsumer
     */
    public function getConsumer($instanceId, $topicName, $groupId, $messageTag = null)
    {
        return $this->client->getConsumer($instanceId, $topicName, $groupId, $messageTag);
    }


}