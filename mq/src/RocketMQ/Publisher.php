<?php
namespace WBY\MQ\SDK\RocketMQ;

use MQ\Model\TopicMessage;
use MQ\MQProducer;
use WBY\MQ\SDK\AbstractPublisher;

/**
 * Class Producer
 * RocketMQ消息发布
 * @package WBY\MQ\SDK\RocketMQ
 */
class Publisher extends AbstractPublisher
{
    /** @var MQProducer  */
    protected $producer;

    /** @var string  */
    protected $tag;

    /**
     * Producer constructor.
     * @param MQProducer $producer
     * @param string|null $tag
     */
    public function __construct(MQProducer $producer, $tag = null)
    {
        $this->producer = $producer;
        $this->tag = $tag;
    }

    /**
     * @param string $message
     * @param null|null $startDeliverTime 单位:秒 投递时间
     * @return TopicMessage
     */
    protected function publishMessage($message, $startDeliverTime = null)
    {
        $currTime = time();
        $maxDeliverTimeLater = 259200;   // 3*86400 三天内
        $topicMessage = new TopicMessage($message);
        if (!is_null($this->tag)) {
            $topicMessage->setMessageTag($this->tag);
        }

        if (!is_null($startDeliverTime) && intval($startDeliverTime)) {
            if (($startDeliverTime - $currTime) >= $maxDeliverTimeLater) {
                throw new \UnexpectedValueException('只允许发送三天内的定时消息');
            }

            $topicMessage->setStartDeliverTime($startDeliverTime * 1000);
        }

         return $this->producer->publishMessage($topicMessage);
    }

}