<?php
namespace WBY\MQ\SDK\Test;

use MQ\Model\Message;
use PHPUnit\Framework\TestCase;
use WBY\MQ\SDK\MQClientFactory;

class RocketMQConsumerTest extends TestCase
{
    public function testConsume()
    {
        $config = [
            'endPoint' => "http://1969896105636301.mqrest.cn-qingdao-public.aliyuncs.com",
            'accessKey' => 'h06LoYx9ppFyxcvwx3ivdOLVHtUZjY',
            'accessId' => 'LTAITP7lzQFRNER1',
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = 'MQ_INST_1969896105636301_BbHKV8Us';
        $topicName = 'xm_test';
        $groupId = 'GID_clock_xmTest';
        $tag = 'test';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId, $tag);

        $subscriber = new RocketMQConsumer($consumer, 1, 3);
        /** @var Message[] $message */
        $messages = $subscriber->consume();//$subscriber->daemon()
        echo "消费:";
        print_r($messages);
        $this->assertSame($messages[0]->getMessageBody(), "test1");
    }
}

