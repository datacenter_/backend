<?php
namespace WBY\MQ\SDK\Test;

use MQ\Model\TopicMessage;
use PHPUnit\Framework\TestCase;
use WBY\MQ\SDK\MQClientFactory;
use WBY\MQ\SDK\RocketMQ\Publisher;

/**
 * Class RocketMQProducerTest
 * @package WBY\MQ\SDK\Test
 */

class RocketMQProducerTest extends TestCase
{
    public function testProducer()
    {
        $config = [
            'endPoint' => "http://1969896105636301.mqrest.cn-qingdao-public.aliyuncs.com",
            'accessKey' => 'YzUoTJeageBIRXK3Acq0L3PI9piAAr',
            'accessId' => 'LTAIIx7whqOFlpmx',
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = 'MQ_INST_1969896105636301_BbHKV8Us';
        $topicName = 'xm_test';
        $tag = 'test';
        $producer = $client->getProducer($instanceId, $topicName);
        $publisher = new Publisher($producer, $tag);
        $messages = ["test1"];
        $startDeliverTime = time() + 60; //设定发送时间 三天内
        echo "生产: " . PHP_EOL;
        echo 'pushTime: ' . $startDeliverTime . PHP_EOL;
        /** @var TopicMessage[] $messages */
        $messages = $publisher->produce($messages, $startDeliverTime);
        print_r($messages);
        $this->assertNotEmpty($messages[0]->getMessageId());
    }
}



