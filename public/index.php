<?php
ini_set("display_errors", 0);
ini_set("display_startup_errors", 0);
error_reporting(0);
date_default_timezone_set('Asia/Shanghai');
//autoload
require __DIR__ . '/../vendor/autoload.php';
// 判断是否存在方法
if (class_exists('\Gclibs\Log\Log')) {
    \Gclibs\Log\Log::init("backend", dirname(__DIR__) . DIRECTORY_SEPARATOR . "runtime/logs");
//    \Gclibs\Log\Log::inputLog($_GET, $_POST, $_SERVER, $_COOKIE);
}

//functions
require __DIR__ . '/../helpers/functions.php';

// Environment
require __DIR__ . '/../env.php';

//debug
env("APP_DEBUG") && define('YII_DEBUG', true);

//framework
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/app.php';

require __DIR__ . '/../components/Application.php';

(new \app\components\Application($config))->run();
