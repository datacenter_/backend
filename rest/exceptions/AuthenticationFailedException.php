<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 身份验证失败异常
 * Class AuthenticationFailedException
 * @package app\rest\exceptions
 */
class AuthenticationFailedException extends RestException
{
    public function getErrCode(): int
    {
        return Code::AUTHENTICATION_FAILED_EXCEPTION;
    }
}