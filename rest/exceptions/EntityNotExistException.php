<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 没有找到要访问的实体数据
 * Class EntityNotExistException
 * @package app\rest\exceptions
 */
class EntityNotExistException extends RestException
{
    public function getErrCode(): int
    {
        return Code::ENTITY_NOT_EXIST_EXCEPTION;
    }
}