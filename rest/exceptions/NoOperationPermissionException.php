<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 没有操作权限
 * Class NoOperationPermissionException
 * @package app\rest\exceptions
 */
class NoOperationPermissionException extends RestException
{
    public function getErrCode(): int
    {
        return Code::NO_OPERATION_PERMISSION_EXCEPTION;
    }
}