<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 参数验证失败异常
 * Class RequestParamsUnvalidatedException
 * @package app\rest\exceptions
 */
class RequestParamsUnvalidatedException extends RestException
{
    public function getErrCode(): int
    {
        return Code::REQUEST_PARAMS_UNVALIDATED_EXCEPTION;
    }
}