<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 鉴权异常
 * Class UnAuthorizationException
 * @package app\rest\exceptions
 */
class UnAuthorizationException extends RestException
{
    public function getErrCode(): int
    {
        return Code::UNAUTHORIZATION_EXCEPTION;
    }
}