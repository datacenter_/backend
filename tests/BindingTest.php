<?php

use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingPlayerService;

/**
 *
 */

class BindingTest extends PHPUnit\Framework\TestCase
{
    public function testBind()
    {
        $standardId = 7606;
        $masterId = 76;
        $info = BindingPlayerService::setBinding($standardId, $masterId, Consts::USER_TYPE_ADMIN, $userId);
    }

    public function testGetDetail()
    {
        $id = 52;
        $info = \app\modules\data\services\binding\BindingTeamService::getDetail($id);
        print_r($info);
    }

    public function testUpdateConfig()
    {
        $json = '{"game_id":"1","config":{"team.base_data":{"id":"11","game_id":"1","resource_type":"team","tag_type":"base_data","slug":"team.base_data","origin_id":"1","update_type":"1"},"team.core_data":{"id":"10","game_id":"1","resource_type":"team","tag_type":"core_data","slug":"team.core_data","origin_id":"1","update_type":"1"},"team.team_player":{"id":"12","game_id":"1","resource_type":"team","tag_type":"team_player","slug":"team.team_player","origin_id":"1","update_type":"1"}}}';
        $info = json_decode($json, true);
        $re = \app\modules\data\services\UpdateConfigService::addBatchUpdateTask($info['game_id'], 'team', $info['config']);
        print_r($re);
//        print_r($info);
    }

    public function testUpdate()
    {
        $json = '{"resource_type":"player","resource_id":"135","config":{"config":{"player.base_data":{"id":"461","game_id":"2","resource_type":"player","resource_id":"135","tag_type":"base_data","slug":"player.base_data","origin_id":"1","update_type":"1","created_time":"2020-06-15 15:15:57","modify_at":null},"player.core_data":{"id":"462","game_id":"2","resource_type":"player","resource_id":"135","tag_type":"core_data","slug":"player.core_data","origin_id":"1","update_type":"2","created_time":"2020-06-15 15:15:57","modify_at":null}},"game":{"id":"2","name":"英雄联盟","e_name":"League of Legends","e_short":"LOL","logo":"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=511400857,3906495553&fm=26&gp=0.jpg","default_game_rule":"1","default_match_type":"1","full_name":null,"short_name":null,"slug":null,"created_at":null,"deleted_at":null,"modified_at":"2020-05-22 18:49:22","image":null,"flag":"1"}}}';
        $info = json_decode($json, true);
        $re = \app\modules\data\services\UpdateConfigService::updateResourceUpdateConfig($info['resource_id'], $info['config']['config']);
        print_r($re);
    }

    public function testGetList()
    {
        $list=BindingPlayerService::getList(['rel_identity_id'=>'25385']);
        print_r($list);
    }

    public function testGetMatchList()
    {
        $list=\app\modules\data\services\binding\BindingMatchService::getList(['origin_id'=>3]);
        print_r($list);
    }

    public function testBinding()
    {
        \app\modules\task\services\TaskRunner::run(36096);
    }

}