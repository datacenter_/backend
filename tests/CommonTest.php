<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class TaskTest extends PHPUnit\Framework\TestCase
{

    public function testResource()
    {
        //俱乐部
        //$input=[
        //    'name' => "ewq",
        //    'country' => "211",
        //    'full_name' => '而我却二无群',
        //    'short_name' => "way",
        //    'alias' => "别名",//TODO:这是个坑
        //    'slug' => "slug",
        //    'image' => "www.baidu.com.png",
        //    'introduction_id' => "英文简介ID",
        //    //简介(中文)
        //    //下属战队
        //    'deleted' => 1,//是否删除
        //];
        //选手
         $input = [
             'nick_name' => "而我却二无群",//名称
             'game_id' => '2',//游戏项目
             'nationality' => '211',//选手国家
             'name' => 'ewq',//姓名英文
             'name_cn' => 'ewq',//姓名中文
             'role' => '2',//角色位置
             //steam_id    标准表无此字段
             'slug' => 'slug',
             'image' => '123123',//头像
             'birthday' => '2010-02-13',//生日
             'introduction' => '123123',//简介
             'deleted' => '1'//是否删除
         ];
        // 战队
        //$input = [
        //    'name' => "321",//名称
        //    'game_id' => '2',//游戏项目
        //    'clan_id' => '5',//俱乐部
        //    'country' => '211',//国家地区
        //    'full_name' => 'wangaoyuan',//全称
        //    'short_name' => 'way',//简称
        //    'alias' => '王傲渊',//别名
        //    'slug' => 'slug',
        //    'logo' => 'ewq',//图标
        //    'deleted' => 1//是否删除
        //];
        //俱乐部
        //$resourceType = \app\modules\common\services\Consts::RESOURCE_TYPE_CLAN;
        //选手
        $resourceType = \app\modules\common\services\Consts::RESOURCE_TYPE_PLAYER;
        //战队
        //$resourceType = \app\modules\common\services\Consts::RESOURCE_TYPE_TEAM;
        //数据源新增
        //$tag = QueueServer::QUEUE_TYPE_ADD;
        //数据源更新
        $tag = QueueServer::QUEUE_TYPE_CHANGE;
        $vals = \app\modules\common\services\HelperResourceFormat::validateAndFormat($input,$resourceType,$tag,2);
        print_r($vals);

    }

    public function testGetCountry()
    {
        $twoKey='SO';
        $info=\app\modules\common\services\EnumService::getCountryByPandascore('Thailand');
        print_r($info);
    }

    public function testDb()
    {
        $resourceType='team';
        $gameId=1;
        $ids = DataResourceUpdateConfig::find()->select('distinct(resource_id) as resource_id')->where(['resource_type' => $resourceType, 'game_id' => $gameId])->asArray()->all();
        $resourceIds = array_column($ids, 'resource_id');
    }
    public function testCon(){
        $info='origin_type=pandascore&resource_type=player&game_type=lol&rel_id=28735';
        $info=[
            'origin_type'=>'pandascore',
            'resource_type'=>'player',
            'game_type'=>'2',
            'rel_id'=>'12509',
        ];
        $re=\app\modules\task\services\ContrastService::getDetail($info);
        print_r($re);
    }
    public function testInfo()
    {
        $url='http://image-demo.oss-cn-hangzhou.aliyuncs.com/example.jpg?x-oss-process=image/resize,m_fixed,h_10,w_10';
//        $url='http://shaoxiatec.oss-cn-beijing.aliyuncs.com/type_logo/a48ed4c09b6709dfeb1c4a6ed7f1f5d3/2KILL.svg';
        print_r($url);
        echo "\n";
        $urlCon=\app\modules\common\services\ImageConversionHelper::showFixedSizeConversion($url,10,10);
        print_r($urlCon);
    }

    public function testChange()
    {
        $url='https://cdn.pandascore.co/images/lol/champion/image/8e6827b2850371389fe0337aa33932c2.png';
        $url='https://img.abiosgaming.com/competitors/PENTA-Sports.png';
        $url='http://admin.score.playesport.cn/static/img/%E6%B7%B1%E8%89%B2%E9%80%82%E7%94%A8-%E6%A8%AA%E7%89%88.af4b4f1c.png';

        print_r($url);
        echo "\n";
        $urlCon=\app\modules\common\services\ImageConversionHelper::mvImageToAli($url);
        print_r($urlCon);
    }

}