<?php
/**
 *
 */

class CsgoFormatTest extends PHPUnit\Framework\TestCase
{
    public function testRegx()
    {
        $maps=\app\modules\task\services\logformat\CsgoLogUCC::getEventMapReg();
        foreach($maps as $key=>$val){
            $lines=$val['test_lines'];
            foreach($lines as $line){
                $eventInfo=\app\modules\task\services\logformat\CsgoLogUCC::formatEventByLineAndMapConfig($line,$maps);
                print_r($eventInfo);
                $this->assertEquals($val['event_type'],$eventInfo['event_type']);
            }
        }
    }
}