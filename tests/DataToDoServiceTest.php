<?php
/**
 *
 */

class DataToDoServiceTest extends PHPUnit\Framework\TestCase
{
    public function testAddTodo()
    {
        $resourceType = 'player';
        $majorId = '46';
        $tagType = 'core_data';
        $tagValue = ['nick_name' => ['before' => '张三', 'after' => '李四']];
        $standardId = 25;
        $taskId = 1688;
        $updateType = 3;

        $info = \app\modules\data\services\DataTodoService::addTodo($resourceType, $majorId, $tagType, $tagValue, $standardId, $taskId, $updateType);
        print_r($info);
    }
}