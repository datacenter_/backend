<?php

use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\QueueServer;
use WBY\MQ\SDK\MQClientFactory;
use WBY\MQ\SDK\Test\RocketMQConsumer;

/**
 *
 */
class MQTest extends PHPUnit\Framework\TestCase
{
    public function testPub()
    {
        $mq=\app\modules\common\services\MqService::publish('gamecenter','tag_hello',json_encode(['hello'=>'world']));
    }
    public function testCon()
    {
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = 'MQ_INST_1969896105636301_BbHKV8Us';
        $topicName = 'gamecenter';
        $groupId = 'GID_gamecenter';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new \app\modules\task\services\TaskMQConsumer($consumer, 1, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }
}