<?php
/**
 *
 */

class MappingTest extends  PHPUnit\Framework\TestCase
{
    public function testT()
    {
        $keyMap=[
            'nick_name'=>'nick_name',
            'name'=>function($params){
                return $params['first_name'].$params['last_name'];
            },
//            'birthday'=>'birthday',
            'game_id'=>function($params){
                // 根据游戏对应关系，过去对应的游戏id
                return \app\modules\common\services\EnumService::getEnum('games');
            },
            'player_id'=>'id',
            'image'=>'image_url',
            'nationality'=>'nationality',
            'role'=>function($params){

            },
            'slug'=>'slug',
        ];
        $params=[
            'nick_name'=>'nick_name',
            'first_name'=>'first_name',
            'last_name'=>'last_name',
        ];
        $info=\app\modules\task\services\originIncrement\Mapping::transformation($keyMap,$params);
        print_r($info);
    }

    public function testPlayer()
    {
        $params=[
            'params'=>json_encode(['identity_id'=>'27970'])
        ];
        $info=\app\modules\task\services\originIncrement\pandascore\Player::run([],$params);
        print_r($info);
    }
}