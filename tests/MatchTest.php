<?php

use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\LOLHeroList;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class TaskTest extends PHPUnit\Framework\TestCase
{
    public function testParentRelInfo()
    {
        $info=\app\modules\tournament\services\TournamentService::getParentListsByTournamentId(381);
        print_r($info);
    }
    public function testEditTu()
    {
        $info=json_decode('{"type":1,"id":"174","core_data":{"id":"174","name":"LOL赛事","name_cn":"","game":"2","status":"2","begin_at":null,"end_at":null,"scheduled_begin_at":null,"scheduled_end_at":null,"original_scheduled_begin_at":null,"original_scheduled_end_at":null,"slug":"lol赛事","is_rescheduled":"2","short_name":"","short_name_cn":"","son_match_sort":"","image":"http://shaoxiatec.oss-cn-beijing.aliyuncs.com/type_logo/c1b5e3f635f0130da7a23acec61d4ed4/%E8%8B%B1%E9%9B%84%E8%81%94%E7%9B%9F.png","type":"1","tree_id":null,"cuser":"21","deleted":"0","created_at":"2020-07-15 01:53:30","modified_at":"2020-07-15 01:53:30","deleted_at":null},"base_data":{"id":"145","tournament_id":"174","steam_id":"","series_match":"","number_of_teams":"","tier":"","country":"","location":"","location_cn":"","match_range":null,"organizer":"","organizer_cn":"","version":"","prize_bonus":"","prize_points":"","map_pool":"","top_reward":"","introduction":"","introduction_cn":"","format":"","format_cn":"","region":"","prize_seed":""},"tournament_team_relation":"[{\"id\":\"992\",\"team_id\":\"158\",\"tournament_id\":\"174\",\"type\":\"1\",\"match_condition\":\"\",\"match_condition_cn\":\"\",\"team_sort\":\"\",\"name\":\"eStar\",\"country\":\"210\",\"country_name\":\"津巴布韦\",\"country_e_name\":\"Zimbabwe\"},{\"id\":\"993\",\"team_id\":\"3828\",\"tournament_id\":\"174\",\"type\":\"1\",\"match_condition\":\"\",\"match_condition_cn\":\"\",\"team_sort\":\"\",\"name\":\"JD Gaming\",\"country\":\"211\",\"country_name\":\"中国\",\"country_e_name\":\"China\"},{\"team_id\":\"200\",\"name\":\"312312 (200) \",\"match_condition\":\"\",\"match_condition_cn\":\"\",\"team_sort\":\"\"},{\"team_id\":\"175\",\"name\":\"629 (175) \",\"match_condition\":\"\",\"match_condition_cn\":\"\",\"team_sort\":\"\"}]","tournament_price_distribute":"[{\"id\":\"644\",\"tournament_id\":\"174\",\"team_num\":null,\"rank\":\"\",\"price\":\"\",\"score\":\"\",\"num\":\"\",\"team_id\":null}]"}',true);
        \app\modules\tournament\services\TournamentService::add($info,1,1);
    }
    public function testAddSettle()
    {
        //初始化时，确认对阵双方
        $matchId = 1;
        $gameType = 1;
        $data = '';
        $dataType = '';
        $battle = \app\modules\match\services\BattleService::addBattleInfo($matchId, $gameType, $data, $dataType);
        print_r($battle);
    }

    public function testSetBattle()
    {
        $js = '{"battle_id":"6","game_type":"lol","data_type":"base","data":{"id":"6","game":null,"match":"21","order":null,"begin_at":"2020-06-16 12:00:00","end_at":"2020-06-22 01:06:42","status":"2","is_draw":"1","if_forfeit":"5","is_default_advantage":"1","map":"1","duation":"-1","winner":"-1","is_battle_detailed":"2","flag":"1","modified_at":null,"created_at":"2020-06-22 00:14:31","deleted_at":null,"team_1_score":"主队比分","team_2_score":"客队比分"}}';
        $info = json_decode($js, true);
        $re = \app\modules\match\services\BattleService::setBattleInfo($info['battle_id'], $info['game_type'], $info['data'], $info['data_type']);
    }

    public function testHotMatch()
    {
        $hotId = 1;
        \app\modules\task\services\operation\OperationRefreshHotInfo::refreshInfo($hotId);
    }

    public function testSetInfo()
    {
        $jsonInfo = '[{"rescheduled":false,"begin_at":"2019-10-04T16:40:43Z","end_at":"2019-10-04T17:15:58Z","modified_at":"2019-10-04T16:40:43Z","tournament":{"begin_at":"2019-10-01T22:00:00Z","end_at":"2019-10-07T22:00:00Z","id":2521,"league_id":297,"live_supported":true,"modified_at":"2019-11-19T10:46:42Z","name":"Play-in: Group A","prizepool":"27812 United States Dollar","serie_id":1839,"slug":"league-of-legends-world-championship-2019-play-in-group-a","winner_id":1536,"winner_type":"Team"},"slug":"unicorns-of-love-vs-mammoth-2019-10-04-979139e4-6680-4fb4-971f-4c318fc6c812","live_embed_url":"https://player.twitch.tv/?channel=lck_korea","match_type":"best_of","status":"finished","draw":false,"official_stream_url":"https://www.twitch.tv/lck_korea","original_scheduled_at":"2019-10-04T16:00:00Z","winner":{"acronym":"UOL","id":19,"image_url":"https://cdn.pandascore.co/images/team/image/19/unicorns-of-love.png","location":"DE","modified_at":"2020-06-17T15:52:06Z","name":"Unicorns of Love","slug":"unicorns-of-love"},"live_url":"https://www.twitch.tv/lck_korea","opponents":[{"opponent":{"acronym":"UOL","id":19,"image_url":"https://cdn.pandascore.co/images/team/image/19/unicorns-of-love.png","location":"DE","modified_at":"2020-06-17T15:52:06Z","name":"Unicorns of Love","slug":"unicorns-of-love"},"type":"Team"},{"opponent":{"acronym":"MMM","id":2796,"image_url":"https://cdn.pandascore.co/images/team/image/2796/220px_mammot_hlogo_square.png","location":"AU","modified_at":"2020-06-05T11:25:23Z","name":"MAMMOTH","slug":"mammoth"},"type":"Team"}],"winner_id":19,"name":"Tiebreaker: UOL vs MMM","scheduled_at":"2019-10-04T16:00:00Z","streams":{"english":{"embed_url":null,"raw_url":null},"russian":{"embed_url":null,"raw_url":null}},"live":{"opens_at":"2019-10-04T16:25:43Z","supported":true,"url":"wss://live.pandascore.co/matches/549354"},"videogame":{"id":1,"name":"LoL","slug":"league-of-legends"},"results":[{"score":1,"team_id":19},{"score":0,"team_id":2796}],"forfeit":false,"games":[{"begin_at":"2019-10-04T16:40:44Z","detailed_stats":true,"end_at":"2019-10-04T17:15:58Z","finished":true,"forfeit":false,"id":211092,"length":1703,"match_id":549354,"position":1,"status":"finished","video_url":null,"winner":{"id":19,"type":"Team"},"winner_type":"Team"}],"serie":{"begin_at":"2019-10-01T22:00:00Z","description":null,"end_at":"2019-11-10T15:19:00Z","full_name":"2019","id":1839,"league_id":297,"modified_at":"2019-11-11T12:47:28Z","name":null,"season":null,"slug":"league-of-legends-world-championship-2019","tier":null,"winner_id":1568,"winner_type":"Team","year":2019},"detailed_stats":true,"league":{"id":297,"image_url":"https://cdn.pandascore.co/images/league/image/297/220px-Worlds_2020.png","modified_at":"2020-02-13T15:23:37Z","name":"World Championship","slug":"league-of-legends-world-championship","url":null},"serie_id":1839,"id":549354,"videogame_version":{"current":false,"name":"9.19.1"},"league_id":297,"tournament_id":2521,"game_advantage":null,"number_of_games":1}]';
        $toInfo = json_decode($jsonInfo,true);
        $info = $toInfo[0];
        \app\modules\task\services\operation\OperationRefreshHotInfo::setPandaScoreLolMatch('21', $info);
    }

}