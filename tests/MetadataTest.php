<?php

use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\QueueServer;

/**
 *
 */
class MetadataTest extends PHPUnit\Framework\TestCase
{
    public function testDel()
    {
        \app\modules\metadata\services\MetadataService::del(\app\modules\common\services\Consts::METADATA_TYPE_LOL_ITEM,1,1,1);
    }

    public function testBinding()
    {
        \app\modules\data\services\binding\BindingMetadataService::addAndBinding(\app\modules\common\services\Consts::METADATA_TYPE_LOL_CHAMPION,1);
    }

}