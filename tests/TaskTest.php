<?php

use app\modules\common\services\EnumService;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\feijing\LOLPropList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\LOLHeroList;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

use app\modules\task\models\StandardDataPlayer;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\StandardDataTournament;

/**
 *
 */
class TaskTest extends PHPUnit\Framework\TestCase
{
    public function testMatchesa()
    {
        // 添加pandascore的抓取
        TaskRunner::run(3478501);
//        $task=[
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "", "", "", ""),
//            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
//            "batch_id" => date("YmdHis"),
//            "params" => [
//                "action" => "/matches",
//                "params" => [
//                    "page" => 1,
//                    'per_page' => 10
//                ]
//            ],
//        ];
//        TaskRunner::addTask($task,2);
    }

    public function testAddtask11()
    {
//        INSERT INTO `task_info` (`id`, `run_type`, `redis_server`, `tag`, `type`, `batch_id`, `params`, `response`, `error_info`, `status`, `substeps`, `debug_info`, `begin_time`, `end_time`, `created_time`)
//VALUES
//(608055, 1, NULL, 'main_increment..team.delete..', NULL, NULL, '{\"diff\":{\"deleted\":{\"key\":\"deleted\",\"before\":2,\"after\":1},\"deleted_at\":{\"key\":\"deleted_at\",\"before\":\"\",\"after\":\"2020-12-10 18:49:35\"}},\"new\":{\"id\":4359,\"name\":\"Sqreen\'s Squadssdss0sww\",\"organization\":null,\"game\":3,\"country\":null,\"full_name\":null,\"short_name\":\"SqrS\",\"alias\":null,\"slug\":\"sqreens_squadssdss0sww\",\"image\":\"http:\\/\\/oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com\\/transfer_station\\/7b533c495dad90bc10787c7627411714\\/fba119ef85bbd506a326864ee2e5c7be.png\",\"cuser\":1,\"deleted\":1,\"created_at\":\"2020-09-12 00:55:42\",\"modified_at\":\"2020-12-10 18:47:09\",\"deleted_at\":\"2020-12-10 18:49:35\",\"playes\":\"[\\\"562\\\",\\\"610\\\",\\\"614\\\"]\"}}', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2020-12-10 18:49:35');
        $params=json_decode('{"diff":{"deleted":{"key":"deleted","before":2,"after":1},"deleted_at":{"key":"deleted_at","before":"","after":"2020-12-10 20:57:27"}},"new":{"id":13346,"name":"test666","organization":null,"game":1,"country":211,"full_name":"acd","short_name":"dsadasd","alias":null,"slug":"test666","image":"http:\/\/oss-gamecenter-prd.oss-cn-beijing.aliyuncs.com\/game_logo_default\/csgo.png","cuser":1,"deleted":1,"created_at":"2020-12-09 12:25:18","modified_at":"2020-12-10 20:53:22","deleted_at":"2020-12-10 20:57:27"}}',true);
        $task=[
            'run_type'=>5,
            'tag'=>'main_increment..team.delete..',
            'batch_id'=>'test',
            'params'=>$params,

        ];
        TaskRunner::addTask($task,5);
    }

    public function testEcho()
    {
        $task=[
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/lol/champions",
                    "params" => [
                        "page" => 1,
                        'per_page' => 20
                    ]
                ],
            ];
        TaskRunner::addTask($task,1);
    }

    /**
     * 代替前端测试假数据Player    By  王傲渊
     */
    public function testDebugAddToDoPlayer()
    {
        // 参数模板
        $params = [
            // 'origin_id'=>3,// 数据源 1，2，3
            'origin_type' => "pandascore",
            'resource_type'=>'player',// resource_type ,参考资源类型
            'standard_id'=>35, // 对应的标准表的id
            // 变化值信息
            'change_info'=>[
                //'nick_name' => "",//名称
                //'game_id' => '2',//游戏项目
                //'nationality' => '211',//选手国家
                //'name' => 'tr',//姓名英文
                //'name_cn' => 'rew',//姓名中文
                //'role' => '1',//角色位置
                ////'steam_id' => '321',//标准表无此字段
                //'slug' => '',
                //'image' => 'https://cdn.pandascore.co/images/team/image/127396/123px_percent_teamlogo_square.png',
                'image' => "https://cdn.pandascore.co/images/team/image/127528/175px_plus_one_logo.png",
                //'birthday' => '',//生日
                //'introduction' => '',//简介
                //'deleted' => ''//是否删除
            ],
        ];
        \app\modules\data\services\DebugDataTodoService::debugAddToDo($params);
    }

    /**
     * 测试假数据team   By  王傲渊
     */
    public function testDebugAddToDoTeam()
    {
        // 参数
        $param = [
            // 'origin_id'=>3,// 数据源 1，2，3
            'origin_type' => "pandascore",
            'resource_type'=>'team',// 参考资源类型
            'standard_id'=>42, // 对应的标准表的id
            // 变化值信息
            'change_info'=>[
                //'game_id' => '2',
                //'name' => "",
                //'clan_id' => "666",
                //'country' => "0",
                //'full_name' => "aaa",
                //'short_name' => "sss",
                //'alias' => "ddd",
                //'logo' => 'https://cdn.pandascore.co/images/team/image/127396/123px_percent_teamlogo_square.png',
                'logo' => "https://cdn.pandascore.co/images/team/image/127528/175px_plus_one_logo.png",
                //'players' => "",
                //'slug' => "rrr-esports-club",
                ////'history_players' => "[7623,1303,111,222]",
                //'history_players' => "",
                //'deleted' => 1
            ],
        ];
        \app\modules\data\services\DebugDataTodoService::debugAddToDo($param);
    }

    /**
     * 测试假数据match   By  王傲渊
     */
    public function testDebugAddToDoMatch()
    {
        // 数据模板
        $map = [
            'id' => 928,
        ];
        $template = StandardDataMatch::find()
            ->where($map)
            ->one()
            ->toArray();
        // 参数
        $param = [
            // 'origin_id' => 3,// 数据源 1，2，3
            'origin_type' => "pandascore",
            'resource_type' => 'match',// 参考资源类型
            'standard_id' => 928, // 对应的标准表的id
            // 变化值信息
            'change_info' => [
                'tournament_id' => 2819,
                'match_rule_id' => 2,
                'son_tournament_id' => 5,
                'stage_id' => 5,
                'group_id' => 4233,
                'team_1_id' => 123456,
                'team_2_id' => 2333,
                'team_1_score' => 6,
                'team_2_score' => 3,
                'winner' => 123,
                'game_rules' => 2,
                'match_type' => "best_of",
                'number_of_games' => "2",
                'original_scheduled_begin_at' => "2020-07-10 07:30:00",
                'scheduled_begin_at' => "2020-07-10 07:30:00",
                'slug' => "inside-games-vs-cyber-gaming-2020-07-10",
                'is_rescheduled' => 1,
                'relation' => "呵呵",
                'name' => "WE vs OMG",
                'draw' => 1,
                'forfeit' => 1,
                'end_at' => "2020-07-10 08:10:10",
                'begin_at' => "2020-07-10 09:10:10",
                'status' => 2,
                'deleted' => 1,
                'is_battle_detailed' => 2,
                'live_url' => "www.baidu.com",
                'embed_url' => "www.baidu.com",
                'official_stream_url' =>"www.baidu.com",
                'default_advantage' => "呵呵",
                'description' => "LOD vs WE",
            ],
        ];
        \app\modules\data\services\DebugDataTodoService::debugAddToDo($param);
    }

    /**
     * 测试假数据tournament     By      王傲渊
     */
    public function testDebugAddToDoTournament()
    {
        // 参数
        $param = [
            // 'origin_id' => 3,// 数据源 1，2，3
            'origin_type' => "pandascore",
            'resource_type' => 'tournament',// 参考资源类型
            'standard_id' => 3414, // 对应的标准表的id
            // 变化值信息
            'change_info' => [
                'name' => "Regular season",//赛事英文名称
                'name_cn' => "糖糖",//赛事中文名称
                'status' => 1,//赛事状态
                'scheduled_begin_at' => "2019-05-15 19:00:00",//开始时间
                'scheduled_end_at' => "2019-05-19 02:00:00",//结束时间
                'short_name' => "RS",//赛事简称英文
                'short_name_cn' => "常规赛",//赛事简称中文
                'son_sort' => "就不告诉你",
                'address' => "北京",
                'country' => 211,
                'organizer' => "就不告诉你是谁",
                'introduction' => "想知道么",
                'format' => "你愁啥",
                //'image' => 'https://cdn.pandascore.co/images/team/image/127396/123px_percent_teamlogo_square.png',
                'image' => "https://cdn.pandascore.co/images/team/image/127528/175px_plus_one_logo.png",
                'type' => 1
            ],
        ];
        \app\modules\data\services\DebugDataTodoService::debugAddToDo($param);
    }

    public function testStart()
    {
        \app\modules\task\services\grab\pandascore\Start::run();
    }

    public function testRunTask()
    {
//        \app\modules\task\services\TaskRunner::run(31219);
//        \app\modules\task\services\TaskRunner::run(39404);
        \app\modules\task\services\TaskRunner::run(7054);
    }

    public function testRefresh()
    {
        \app\modules\task\services\operation\OperationConfigRefresh::initAll();
    }

    public function testAddTask()
    {
        $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascorePlayerList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/players",
                    "params" => [
                        "page" => 1,
                        'per_page' => 10
                    ]
                ],
            ];
            TaskRunner::addTask($item, 2);
    }

    public function testAddTaskT()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/series",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }

    public function testPandascoreTask()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/teams",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 3);
    }

    public function testMatches()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/matches",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }

    public function testRefreshA()
    {
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
    }

    public function testAt()
    {
        $teamInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => TeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/teams",
                "params" => [
                    "page" => 2,
                    'with' => ['game'],
                ]
            ],
        ];
        TaskRunner::addTask($teamInfo, 2);
    }

    public function testSp()
    {
        $info = sprintf("%%%s%%", "tes");
        print_r($info);
    }

    public function testMetadata()
    {
        $heroInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLHeroList::class,
            "batch_id" => "" . time(),
            "params" => [
                "action" => "/lol/champions",
                "params" => [
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page" => 1,
                    'per_page' => 100
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($heroInfo, 3);
    }

    public function testLOLProp()
    {
        $propInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLPropList::class,
            "batch_id" => "" . time(),
            "params" => [
                "action" => "/lol/items",
                "params" => [
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page" => 1,
                    'per_page' => 3
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($propInfo, 3);
    }

    public function testgetResourceUpdateConfig()
    {
        $info = \app\modules\data\services\UpdateConfigService::getResourceUpdateConfig(45, 'player');
        print_r($info);
    }

    // 这里测试抓取一个账号，产生的增量和后续，然后是更新一个账号
    public function testPlayerCatch()
    {
        // https://api.pandascore.co/dota2/players?token=SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM

        // pandascore抓取一个账号
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => DotaPlayer::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/dota2/players",
                'params' => [
                    "filter[id]" => 28013,
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info);
    }

    public function testRefreshMetadata()
    {
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMetadata();
    }
    //刷新一个推荐
    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_TEAM,[4032],[],'team');

    }

    public function testPandaSeries()
    {
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandaSeries::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/series",
                'params' => [
                    "filter[year]" => 2019,
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testPandaPlayers()
    {
        $info = [
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "", "", "", ""),
//            "type" => \app\modules\task\services\grab\pandascore\PandaSeries::class,
//            "batch_id" => "" . time(),
//            "params" => [
//                'action' => "/matches",
//                'params' => [
////                    "filter[year]" => 2019,
//                    "range[begin_at]"=>'2019-01-01,2020-01-01',
//                    "page"=>'203',
//                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                ]
//            ],
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLTeamList::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/lol/teams",
                'params' => [
                    "filter[name]"=>'Vici Gaming',
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testPandaT()
    {
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLSeries::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/lol/series",
                'params' => [
                    "filter[id]"=>'2738',
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testCt()
    {
        $matchInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>MatchList::class,
            "batch_id"=>"" . time(),
            "params"=>[
                "action"=>"/matches/running",
                "params"=>[
                    "token"=>'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page"=>1,
                    'per_page'=>10
                ]
            ],

        ];
        \app\modules\task\services\TaskRunner::addTask($matchInfo,1);
    }

    public function testShow()
    {
        $list=\app\modules\receive\models\ReceiveData5eplay::find()->orderBy('id')->asArray()->all();
        foreach($list as $i){
            echo $i['log'];
        }
    }

    public function testTask()
    {
        $params=[
            'origin_type'=>'pandascore',
            'resource_type'=>'player',
            'game_type'=>'lol',
            'rel_id'=>1,
        ];
        \app\modules\task\services\ContrastService::refresh($params);
    }

}