<?php

class EnumIngameGoalTest extends PHPUnit\Framework\TestCase
{
    //
    public function testInsert()
    {
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        $redisName = "enum:ingame_goal";
        //获取道具列表
        $res = \app\modules\common\models\EnumIngameGoal::find()->asArray()->all();
        foreach ($res as $key => $item){
            $master_id = $item['id'];
            //插入关系列表
            $redis->hset($redisName,$master_id,@json_encode($item,320));
        }
    }
}