<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DataMetaDataNoRelation;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\DataBindingService;
use app\modules\data\services\UpdateConfigService;
use app\modules\match\models\Match;
use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleBanpick;
use app\modules\match\models\MatchBattleEventLol;
use app\modules\match\models\MatchBattleExtCsgo;
use app\modules\match\models\MatchBattleTeam;
use app\modules\match\models\MatchBattleTeamExtLol;
use app\modules\match\models\MatchComingSocketOrder;
use app\modules\match\models\MatchLived;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\BattleService;
use app\modules\match\services\MatchService;
use app\modules\match\services\MatchsocketService;
use app\modules\match\services\SortlogService;
use app\modules\metadata\models\MetadataDota2Item;
use app\modules\metadata\models\MetadataLolChampion;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\Team;
use app\modules\metadata\models\MetadataLolItem;
use app\modules\org\models\TeamPlayerRelationSnasphot;
use app\modules\org\models\TeamSnapshot;
use app\modules\org\services\PlayerService;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\MatchBattleRelation;
use app\modules\task\models\MatchComingScoket;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\Common;
use app\modules\task\services\grab\orange\OrangeBase;
use app\modules\task\services\hot\abios\AbiosHotBase;
use app\modules\task\services\operation\OperationRefreshHotInfo;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\hot\HotBase;
use app\rest\exceptions\BusinessException;
use rdKafka;
use app\modules\match\models\MatchLivedDatasTableRelation;


define("MAX_SHOW",8192*5); //新增数据提交阈值
define("LOG_NAME", "../../runtime/logs/backend_20201029.log"); //读取的日志文件
define("LOG_SIZE", ""); //保留上次读取的位置
define("LOG_URL", ""); //日志提交地址

class LivedMatchTest extends PHPUnit\Framework\TestCase
{
    public $battle_order_id = null;
    public $memory_mode = null;
    //  php yii gii/model --ns=app\modules\match\models --tableName=match_battle_player_dota2 --modelClass=MatchBattlePlayerDota2
    //php yii gii/model --ns=app\modules\match\models --tableName=match_coming_socket_order --modelClass=MatchComingSocketOrder
    //php yii gii/model --ns=app\modules\data\models --tableName=data_bind_time --modelClass=DataBindTime
    //php yii gii/model --ns=app\modules\match\models --tableName=socket_game_configure --modelClass=SocketGameConfigure
    //php yii gii/model --ns=app\modules\match\models --tableName=match_lived_datas_table_relation --modelClass=MatchLivedDatasTableRelation
    public function testAdd()
    {//'30 10 * * 0'  riotgames-riotgames   slow-once-again-up
        \app\modules\task\services\TaskRunner::run(806204);// 17230883 15421040  16469740
        //esports:match:fd16739f-50e0-45f9-bd85-ba8779cc8f5f
        $z = 1;
        //kda、cspm、gpm：保留两位小数
        //participation、gold_earned_percent：保留4位小数
    }
    public function testAAA(){
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshBdMatch();


        $z = 1;
    }
    public function testXiufu(){
        $queryOriginIds = '12,3';
        $where = [
            'and',
            ['<>','deal_status','4'],
            ['>=','scheduled_begin_at','']
        ];
        if ($queryOriginIds){
            $where[] = ['in', 'origin_id', $queryOriginIds];
        }


        $queryTime = date("Y-m-d H:i:s",strtotime("-2 month"));
        $pageCount=200;

        $sql = StandardDataMatch::find()->select("id")->where(['and',
            ['<>','deal_status','4'],
            ['>=','modified_at',$queryTime],
        ])->limit($pageCount)->offset(0)->createCommand()->getRawSql();


        $operate_type = 'xiufu';
        $match_id = 55715;
        $origin_id = 3;
        $gameType = 'lol';
        $refresh_sub_type = '';
        MatchService::refreshMatchInfoBySocketBtn($operate_type,$match_id,$origin_id,$gameType,$refresh_sub_type);


        $z = 1;
    }
    public function testRefreshMatch(){
        $operate_type = '';
        $match_id = 55630;
        $origin_id = 3;
        $gameType = 'csgo';
        MatchService::refreshMatchInfoBySocketBtn($operate_type,$match_id,$origin_id,$gameType);
        $z = 1;
    }
    public function testBindingTeamServiceAndBindingPlayerService(){
        $params = [
            'origin_id' => 9,
            'deal_status' => 3,
            'page' => 1,
            'per_page' => 20,
            'rel_rel_identity_id' => 20,
        ];
//        $res = \app\modules\data\services\binding\BindingTeamService::getList($params);
        $res = \app\modules\data\services\binding\BindingPlayerService::getList($params);
        //ALTER TABLE match_lived_datas ADD COLUMN deleted int(2) DEFAULT 2 COMMENT '是否删除' AFTER socket_time;
        //ALTER TABLE match_lived_datas ADD COLUMN deleted_at datetime NULL COMMENT '删除时间' AFTER deleted;
        $z = 1;
    }
    public function testCustom2()
    {
        $groupId = 'test';
        $topic = 'ori_hltv_csgo_01';
        //$broken = '10.1.0.145:9092,10.1.0.144:9092,10.1.0.143:9092';
        $broken = '8.210.182.73:9093,8.210.153.140:9093,8.210.207.154:9093';
        $conf = new RdKafka\Conf();
        //$conf->set('sasl.mechanisms', 'PLAIN');
        $conf->set('api.version.request', 'true');
        //$conf->set('sasl.username', 'alikafka_pre_public_intl-sg-25u22b8wg02');
        //$conf->set('sasl.password', 'JmWBZ3IhQROtVIZdMrp5SM6krONfoXih');
//        $conf->set('enable.ssl.certificate.verification','false');
        //$conf->set('security.protocol', 'SASL_SSL');
        //$conf->set('ssl.ca.location', 'ca-cert.pem');

        $conf->set('group.id', $groupId);

        $conf->set('metadata.broker.list', $broken);

        $topicConf = new RdKafka\TopicConf();

        $conf->setDefaultTopicConf($topicConf);

        $consumer = new RdKafka\KafkaConsumer($conf);

        $consumer->subscribe([$topic]);

//        $redis       = new Redis();
//        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
//        $redis->auth(env('REDIS_PASSWORD'));
//        $redis->select(env('REDIS_DATABASE'));

//        if ($topic == 'sparkstreamingsql_test'){
//            //消费开始位置 18747252
//            $offsetListKey = 'kafka:offset_list';
//        }else{
//            $offsetListKey = 'kafka:'.$topic.':offset_list';
//        }

        while (true) {
            $message = $consumer->consume(3*1000);
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    //$dataOffsetList = $redis->lRange($offsetListKey,0,-1);
                    $z = 1;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo "Timed out\n";
                    break;
                default:
                    throw new \Exception($message->errstr(), $message->err);
                    break;
            }
            $x = $message;
        }
    }
    public function testNNNN(){
        $conn = [
            // Rabbitmq 服务地址
            'host' => 'egest.bdx.gg',
            // Rabbitmq 服务端口
            'port' => '5671',
            // Rabbitmq 帐号
            'user' => 'production-rabbitmq-egest-data-consumer-allwin-dGJy9g',
            // Rabbitmq 密码
            'password' => 'A0Ju8BkVHLQVacNcOSr5tRM0gQ8U6lGc',
            'vhost'=>'egest'
        ];
        //创建连接和channel
        $conn = new \PhpAmqpLib\Connection\AMQPSSLConnection('egest.bdx.gg','5671','production-rabbitmq-egest-data-consumer-allwin-dGJy9g','A0Ju8BkVHLQVacNcOSr5tRM0gQ8U6lGc','egest');
        if(!$conn->connectOnConstruct()) {
            die("Cannot connect to the broker!\n");
        }
        $channel = new AMQPChannel($conn);
        //$exchangeName = 'allwin.outbox';

        //创建交换机
        $ex = new AMQPExchange($channel);
        //$ex->setName($exchangeName);

        $ex->setType(AMQP_EX_TYPE_DIRECT); //direct类型
        $ex->setFlags(AMQP_DURABLE); //持久化
        $ex->declare();

//  创建队列
        $queueName = 'allwin.outbox';
        $q = new AMQPQueue($channel);
        $q->setName($queueName);
        $q->setFlags(AMQP_DURABLE);
        $q->declareQueue();

//        // 用于绑定队列和交换机，跟 send.php 中的一致。
//        $routingKey = 'key_1';
//        $q->bind($exchangeName,  $routingKey);

//接收消息
        $q->consume(function ($envelope, $queue) {
            $msg = $envelope->getBody();
            echo $msg."\n"; //处理消息
        }, AMQP_AUTOACK);

        $conn->disconnect();

        $z = 1;
    }
    public function testAbiosFramesAndEvents(){
        $params=[
            'grant_type'=>'client_credentials',
            'client_id'=>'allwinesports_bdb70',
            'client_secret'=>'a29e31ed-0848-408e-ac45-8f9b357caa33'
        ];
        $url='http://api.abiosgaming.com/v2/oauth/access_token';
        $infoJson = Common::requestPost($url,[],$params);
        $info=json_decode($infoJson,true);
        $token = $info['access_token'];
        $frames = "wss://ws.abiosgaming.com/v0?access_token={$token}&subscription_id=23157612-c36a-4f38-9e7b-fa5ec76a174a";
        $events = "wss://ws.abiosgaming.com/v0?access_token={$token}&subscription_id=dota_realtime_api_events";
        $z = 1;
        //https://ws.abiosgaming.com/v0/subscription?access_token=a9f2138fcd4171bf71f159b10296c78ce4c88913b7b6298ee1957221aae671bf
    }
    public function teste(){
        $killsres = $turret_killsres = [];
        $matchList = MatchRealTimeInfo::find()->alias('mr')->select('mr.id')
            ->leftJoin('match as m','m.id = mr.id')
            ->where(['mr.status'=>3])
            ->andWhere(['m.game'=> 3])
            ->asArray()->all();
        foreach ($matchList as $value){
            $battles = MatchBattle::find()->select('id')->where(['match'=> $value['id']])->andWhere(['deleted'=>2])->andWhere(['status'=>3])->asArray()->all();
            foreach ($battles as $item){
                $teams = MatchBattleTeam::find()->select('id')->where(['battle_id'=>$item['id']])->asArray()->all();
                $team_kills = 0;
                $team_turret_kills = 0;
                foreach ($teams as $val){
                    $extInfo = \app\modules\match\models\MatchBattleTeamExtDota2::find()->select(['kills','tower_kills'])->where(['id'=>$val['id']])->asArray()->one();
                    $kills = $extInfo['kills'];
                    $turret_kills = $extInfo['tower_kills'];
                    if ($kills){
                        $team_kills += $kills;
                    }
                    if ($turret_kills){
                        $team_turret_kills += $turret_kills;
                    }
                }

                if (empty($killsres[$team_kills])){
                    $killsres[$team_kills] = 1;
                }else{
                    $killsres[$team_kills] += 1;
                }

                if (empty($turret_killsres[$team_turret_kills])){
                    $turret_killsres[$team_turret_kills] = 1;
                }else{
                    $turret_killsres[$team_turret_kills] += 1;
                }
            }
        }
        ksort($killsres);
        ksort($turret_killsres);
        $z = 1;
    }
    public function testASDF(){
        $self = new self();
        $self->memory_mode = true;
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
        $match_id = 3074;
        $redis_name = Consts::MATCH_ALL_INFO.':'.$match_id."*";
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        $res = $redis->scan($it, $redis_name, 10);
        while ($arr_keys = $redis->scan($it, $redis_name, 5)) {
            call_user_func_array([$redis, 'del'], $arr_keys);
            echo var_export($arr_keys, true) . PHP_EOL;
        }
        /*

//        $redis->connect(env('REDIS_HOST'),env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        $originId =6 ;
        $MemoryNameRelation = "lol_items_relation_".$originId;
        if ($self->memory_mode) {
            $strValue = $self->$MemoryNameRelation;
            if (!empty($strValue)){
                return $strValue;
            }
        }
        $redisNameRelation = "lol:items:relation:".$originId;
        $returnValue = $redis->hGetAll($redisNameRelation);
        if ($self->memory_mode) {
            $self->$MemoryNameRelation = $returnValue;
        }
        */
        $z = 1;
    }
    public function testFG(){
        $b = base64_encode('codingant@163.com:AllwinBedex1507');
        $z = 1;
    }
    public function testRedis(){
        $redis = new \Redis();
        $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        //redis name
        $redis_Name_Str = 'enum:socket_';
        $socketGameConfigure = \app\modules\match\models\SocketGameConfigure::find()->asArray()->all();
        foreach ($socketGameConfigure as $key => $item){
            if ($item['game_id'] == 1){
                $csgo_socket_configure = [
                    'mp_maxrounds' => $item['mp_maxrounds'],
                    'mp_overtime_enable' => $item['mp_overtime_enable'],
                    'mp_overtime_maxrounds' => $item['mp_overtime_maxrounds'],
                    'mp_roundtime_defuse' => $item['mp_roundtime_defuse'],
                    'mp_freezetime' => $item['mp_freezetime'],
                    'mp_c4timer' => $item['mp_c4timer']
                ];
                $redis->hMSet($redis_Name_Str.'csgo',$csgo_socket_configure);
            }
            if ($item['game_id'] == 2){
                $lol_socket_configure = [
                    'inhibitor_reborn_time' => $item['inhibitor_reborn_time'],
                    'rift_herald_first_time' => $item['rift_herald_first_time'],
                    'rift_herald_reborn_time' => $item['rift_herald_reborn_time'],
                    'rift_herald_quit_time' => $item['rift_herald_quit_time'],
                    'rift_herald_max_num' => $item['rift_herald_max_num'],
                    'dragon_first_time' => $item['dragon_first_time'],
                    'dragon_reborn_time' => $item['dragon_reborn_time'],
                    'dragon_quit_num' => $item['dragon_quit_num'],
                    'baron_nashor_first_time' => $item['baron_nashor_first_time'],
                    'baron_nashor_reborn_time' => $item['baron_nashor_reborn_time'],
                    'elder_dragon_first_time' => $item['elder_dragon_first_time'],
                    'elder_dragon_reborn_time' => $item['elder_dragon_reborn_time']
                ];
                $redis->hMSet($redis_Name_Str.'lol',$lol_socket_configure);
            }
        }

        $z = 1;
    }
    public function testCheck(){
        $battleType = 'forfeit';//'advantage' 或者 forfeit
        $dealType = 'create';//create 或者 delete
        $originId = 3;//数据源ID
        $matchId = 789;//比赛ID
        $battleOrder = 3;// 具体orderID 或者 传空 是新建
        $battleId = '';//如果传值就按照battleID 不按照battle order
        $winnerType = 'order';//'order';team_id  order：获胜队伍是传主客队1、2  或者 team_id：平台的队伍ID
        $winnerId= 1;//4317  1
        $rel_matchId = 0;//数据源比赛ID
        $rel_battleId = 1;//数据源对局ID
        HotBase::dealMatchSpecialBattle($battleType,$dealType,$originId,$matchId,$battleOrder,$battleId,$winnerType,$winnerId,$rel_matchId,$rel_battleId);
    }
    public static function getAllDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=\app\modules\task\services\StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == \app\modules\data\models\StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        $sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }
    public function testAD(){
        $battleId = 738;
        $order_id = 3;
        //redis初始化
        $Redis= \Yii::$app->redis;
        $Redis->select(env('REDIS_DATABASE'));
        //api的
        $redis_match_battles = 'match_battle_info:738';
        $match_battles = $Redis->hvals($redis_match_battles);
        if ($match_battles){
            foreach ($match_battles as $key => $item){
                $battle_res_explode = explode('-',$item);
                $battle_id_val = $battle_res_explode[0];
                if ($battleId == $battle_id_val){
                    $battle_id_num = $key;
                }
            }
        }
        if ($battle_id_num >= 0){
            $battle_list_keys = $Redis->hkeys($redis_match_battles);
            $battle_num_val = $battle_list_keys[$battle_id_num];
            if ($battle_num_val){
                $Redis->hdel($redis_match_battles,$battle_num_val);
            }
            $Redis->hset($redis_match_battles,'forfeit_'.$order_id,$battleId.'-'.$order_id);
        }else{
            $Redis->hset($redis_match_battles,'forfeit_'.$order_id,$battleId.'-'.$order_id);
        }
    }
    //清空比赛的battle TODO  在执行  1
    public function testQingKong(){

        BattleService::deleteBattle('lol', 19202);

        $gameType = 'lol';
        $match_id = 55701;//3074  2474  758
        $origin_id = 3;
        $operate_type = null;
        //里面有debug 断点
        MatchService::refreshMatchInfoBySocketBtn($operate_type,$match_id,$origin_id,$gameType);
    }
    //测试添加socket数据 TODO  在执行  2
    public function testAdd3()
    {
        $match_id = 'esports:match:c25a8410-c5bd-4e29-8257-0ed26d206841';
        $map = [
//            'origin_id' => 3,
            'match_id' => $match_id, //571079,
            'game_id' => 2,
//            'type' => 'events'
//            'type' => 'no'
        ];
        $data = \app\modules\match\models\MatchLived::find()
            ->orderBy('id ASC')
            ->where($map)
//            ->andwhere(['<=','id','14013602'])
//            ->andwhere(['>=','id','1133998'])
            ->where(['=','id','72935148'])
//            ->andwhere(['=','id','1137928'])//802622
//            ->andwhere(['>','id','193385'])
//            ->andwhere(['<','id','330331'])
//                ->andWhere(['like','match_data','BUILDING_KILL'])//BUILDING_KILL  ELITE_MONSTER_KILL
            ->select('id,match_data,type,round,match_id,game_id,origin_id,socket_time,created_at')
            ->asArray()
            ->all();
        //$socket_time = date('Y-m-d H:i:s');
        foreach($data as $key => $value){
            $insertData = [
                'origin_id' => $value['origin_id'],
                'game_id' => $value['game_id'],
                'match_id' => $value['match_id'],
                'round' => $value['round'],
                'match_data' => $value['match_data'],
                'type' => $value['type'],
                'socket_time' => isset($value['socket_time']) ? $value['socket_time']: $value['created_at'],
                'socket_id' => $value['id']
            ];
            if(!isset($insertData['socket_time'])){
                $insertData['socket_time'] = date('Y-m-d H:i:s');
            }
            $newInsertData = @$insertData;
            $newInsertData['match_data']= json_decode($value['match_data'],true);

            $taskType = '9002';
            //再插入一次 用户restapi
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
//                    $value['origin_id'], \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $value['game_id']),
//                "type" => \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
//                "batch_id" => date("YmdHis"),
//                "params" => $newInsertData,
//                "redis_server"=>'order_lol_'.$match_id
//            ];
//            TaskRunner::addTask($item, $taskType);
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                    $value['origin_id'], Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $value['game_id']),
                "type" => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                "batch_id" => date("YmdHis"),
                "params" => $newInsertData,
                "redis_server"=>'order_lol_'.$match_id
            ];
            TaskRunner::addTask($item, $taskType);
//            sleep(0.5);
        }
        return "执行完毕!!!";
    }

    public function testAddTaskpast()
    {
        $transInfo = StandardDataMatch::find()->where(['and',
            ['=','is_pbpdata_supported',1],
            ['=','status',1],
        ])->asArray()->all();
        foreach ($transInfo as $value){
            $info['begin_at'] = $value['begin_at'];
            $info['end_at'] = $value['end_at'];
            $info['status'] = $value['status'];
            $standardInfo = MatchComingScoket::find()->where([
                'origin_id' => $value['origin_id'],
                'match_id' => $value['rel_identity_id'],
                'game_id' => $value['game_id'],
            ])->one();
            if (!$standardInfo) {
                $standardInfo = new MatchComingScoket();
                $standardInfo->setAttributes(
                    [
                        'origin_id' => $value['origin_id'],
                        'match_id' => $value['rel_identity_id'],
                        'game_id' => $value['game_id'],
                    ]
                );
            }
            $standardInfo->setAttributes($info);
            if (!$standardInfo->save()) {
                throw new BusinessException($standardInfo->getErrors(), '保存失败');
            }
        }
    }

    //刷新HOT表
    public function testRefresh(){
        \app\modules\task\services\operation\OperationRefreshHotInfo::refreshList();

    }

    //测试日志添加
    public function testlogAdd()
    {
        \app\modules\task\services\logformat\LogFormatDeguoDispatch::run();
    }

    //ceshi
    public function testCinfo(){
//        $data = \app\modules\match\models\MatchLived::find()->select('match_data')->where(['match_id'=>'lmcmqe71c15'])->asArray()->all();
//        $res = [];
//        foreach ($data as $val){
//            $event_type_info = json_decode($val['match_data'],true);
//            $event_type = @$event_type_info['event_type'];
//            $res[$event_type] = $event_type;
//        }
//
//        $b = implode(',',$res);
        $c = '{"BAN_PICK":"BAN_PICK","GAME_INFO":"GAME_INFO","STATS_UPDATE":"STATS_UPDATE","ITEM_PURCHASED":"ITEM_PURCHASED","SKILL_LEVEL_UP":"SKILL_LEVEL_UP","ITEM_UNDO":"ITEM_UNDO","ITEM_DESTROYED":"ITEM_DESTROYED","ELITE_MONSTER_KILL":"ELITE_MONSTER_KILL","CHAMPION_KILL":"CHAMPION_KILL","KILL_ACHIEVEMENT":"KILL_ACHIEVEMENT","ITEM_SOLD":"ITEM_SOLD","BUILDING_KILL":"BUILDING_KILL","GAME_END":"GAME_END"}';
        $d = json_decode($c,true);
        $c = implode(',',$d);

        $z = 1;

    }

    //json
    public function testJson(){

        $infoString=file_get_contents(dirname(dirname(__DIR__))."/a1.txt");
        $infoString = utf8_encode($infoString);
        $infoArray=json_decode($infoString,true);
        print_r(json_last_error());
        print_r($infoArray);

    }
    public function tests()
    {
        for ($i = 4000; $i <= 6712;$i++ ){
            $json = file_get_contents('./log/'.$i.'.json');
            file_put_contents('json.txt',$json.PHP_EOL,FILE_APPEND);
        }

    }

    //拆分文件
    public function testChaiFen(){
        $socket_time = date('Y-m-d H:i:s');
        $i= 0;//分割的块编号
        $fp= fopen("C:\\Users\\Administrator\\Desktop\\rbmq\\rbmq.log", "r+");//要分割的文件
        while (!feof($fp)) {
            $res = fgets($fp);
            $ret = substr($res,stripos($res,"{"));
            $decode_data = @json_decode($ret,true);
            //$title = @$decode_data['payload']['payload']['title'];
            $liveDataMatchUrn = @$decode_data['payload']['payload']['liveDataMatchUrn'];
            //$perId = @$decode_data['payload']['payload']['additionalProperties']['perId'];
            //if ($title == 'LOL' && $perId == 'esports:match:f9f83851-7641-4d5c-9b41-1f593c06ac66'){
            if ($liveDataMatchUrn == 'live:lol:riot:map:esportstmnt01-1683114'){
//                $handle = fopen("C:\\Users\\Administrator\\Desktop\\rbmq\\line\\{$i}.log", "w+");
//                fwrite($handle, $ret);
//                fclose($handle);
//                unset($handle);
//                $i++;
                $insertData = [
                    'origin_id' => 9,
                    'game_id' => 2,
                    'match_id' => '62045',
                    'round' => null,
                    'match_data' => @json_encode($decode_data,320),
                    'type' => 'no',
                    'socket_time' => $socket_time
                ];
                $MatchLivedModel = new MatchLived();
                $MatchLivedModel->setAttributes($insertData);
                $MatchLivedModel->save();
            }
        }
        fclose($fp);
        echo "ok";
    }

    //kafka
    public function testConsumer2(){
        //设置消费组 test-2(测试) consumer-group3(正式)
        $conf = new \RdKafka\Conf();
        $conf->set('group.id', 'test');
//        $conf->set('group.id', 'consumer-group3');
        $conf->set('enable.auto.commit', 'true');

        //设置broker BROKERS_OUT(外网ip)  BROKERS_INTERNAL(内网ip)
        $rk = new \RdKafka\Consumer($conf);
        $rk->addBrokers(\app\modules\task\services\kafka\KafkaProducer::BROKERS_OUT);

        //设置topic
        $topicConf = new \RdKafka\TopicConf();
        $topic     = $rk->newTopic("socket_scr_lol", $topicConf);

        $topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);
//        $topic->consumeStart(0, 2);

        $repeatCount = 0;
        $newCount    = 0;
        $count       = 0;

        while (true) {
            //参数1表示消费分区，这里是分区0
            //参数2表示同步阻塞多久
            $message = $topic->consume(0, 12 * 1000);
            if (is_null($message)) {
                sleep(1);
                echo 'No more messages' . PHP_EOL;
                continue;
            }
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:

                    $count++;

                    echo '总数:' . $count . PHP_EOL;

//                    var_dump($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:

                    echo 'No more messages; will wait for more' . PHP_EOL;
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo 'Timed out' . PHP_EOL;
                    break;
                default:
                    throw new BusinessException($message->errstr(), $message->err);
                    break;
            }
        }
    }
    public function maopao1($array)
    {
        $num = count($array);
        for ($i=0;$i<$num;$i++){
            for ($j = $i+1;$j<$num;$j++){
                if ($array[$i] > $array[$j]){
                    $tmp = $array[$i];
                    $array[$i] = $array[$j];
                    $array[$j] = $tmp;
                }
            }
        }
        return $array;
    }

    //快速排序
    public function testKuaiSuPaiXu(){
        $array = [6,3,5,1,8,7];
        //$res = $this->maopao1($array);
        $res = $this->kspx($array);
        $a = $res;
    }

    public function kspx($array){
        $num = count($array);
        if ($num <= 1){
            return $array;
        }
        $zj = $array[0];
        $l = $r = [];
        for ($i = 1;$i < $num; $i++){
            if ($array[$i] > $zj){
                $r[] = $array[$i];
            }else{
                $l[] = $array[$i];
            }
        }
        $r = $this->kspx($r);
        $l = $this->kspx($l);
        return array_merge($l, array($zj) , $r);
    }

    //递归排序
    public function testDiGui(){
        $array = [
            ['id' => 1, 'pid' => 0, 'name' => '湖北省'],
            ['id' => 2, 'pid' => 0, 'name' => '北京市'],
            ['id' => 3, 'pid' => 1, 'name' => '武汉市'],
            ['id' => 4, 'pid' => 2, 'name' => '朝阳区'],
            ['id' => 5, 'pid' => 2, 'name' => '通州区'],
            ['id' => 6, 'pid' => 4, 'name' => '望京'],
            ['id' => 7, 'pid' => 4, 'name' => '酒仙桥'],
            ['id' => 8, 'pid' => 3, 'name' => '武昌区'],
            ['id' => 9, 'pid' => 1, 'name' => '武安市']
        ];
        $res = $this->digui1($array);
        $result = $res;
    }
    public function digui1($array, $num=0, &$list=[])
    {
        foreach ($array as $item){
            if ($item['pid'] == $num){
                $son = $this->digui1($array,$item['id']);
                $item['son'] = $son;
                $list[] = $item;
            }
        }
        return $list;
    }

    public function testAddLog(){
        include_once '../../vendor/aliyunlog/Log_Autoload.php';
        $endpoint = 'http://cn-hongkong.log.aliyuncs.com';
        $accessKeyId = 'LTAI4G34n8nSEE3H7D7GUYWX';
        $accessKey = 'WqiMInx91XQ8ve2PIoQmZgKQ5JUGxy';
        $project = 'k8s-log-c9cd783cb0f3a420192fca95a11781c8d';
        $logstore = 'backend_api';
        $topic = '';
        $source = '';

        $date_old = null;
        $num = 0;
        while (1) {
            $date = date('Ymd');
            if ($date_old != $date){
                $num = 0;
                $date_old = $date;
            }
            $file = "../../runtime/logs/backend_".$date.".log";
            $handle = fopen($file, "r");
            if ($handle){
                fseek($handle, $num);
                $buffer = fgets($handle, 40960);
                $num =  ftell($handle);
                if (!empty($buffer)){
                    //file_put_contents('json.txt',$buffer,FILE_APPEND);
                    $client = new \Aliyun_Log_Client($endpoint,$accessKeyId,$accessKey);
                    $data = [$buffer];
                    $logItems = [];
                    $logItem = new \Aliyun_Log_Models_LogItem();
                    $logItem->setTime(time());
                    $logItem->setContents($data);
                    array_push($logItems,$logItem);
                    $almpr = new \Aliyun_Log_Models_PutLogsRequest($project,$logstore,$topic,$source,$logItems);
                    $res = $client ->putLogs($almpr);
                    $a = 1;
                }
            }
        }
    }
    public function aaaa($file, $length = 40960)
    {
        $i = 1; // 行数
        $handle = fopen($file, "r");
        if (!$handle) {
            return false;
        }
        while (1) {
            fgets($handle, $length);
            $i++;
            if ($i > 100000) {
                break;
            }
        }
        fclose($handle);
        return $i;
    }
    public function getLineContent($file, $length = 40960)
    {
        $returnTxt = null; // 初始化返回
        if (!is_file($file)) {
            return false;
        }
        $handle = fopen($file, "r");
        $data = [];
        while (1) {
            $buffer = fgets($handle, $length);
            file_put_contents('json.txt',$buffer.PHP_EOL,FILE_APPEND);
        }
        fclose($handle);
        if (empty($data)) {
            return false;
        }
        return $data;
    }

    public function testaaaa(){
        $params = [
            'origin_id' => 4
        ];
        //$res = MatchsocketService::pageApilist($params);

        $list = MatchComingSocketOrder::find()->alias('mcso')
            ->select(['mcso.*','mcs.match_id as match_match_id','mcs.begin_at as match_begin_at',
                'mcs.end_at as match_end_at', 'mcs.status as match_status',
                'mcs.websocket_time1 as match_websocket_time1',
                'mcs.websocket_time2 as match_websocket_time2'])
            ->leftJoin('match_coming_socket as mcs','mcs.match_id = mcso.match_id')
            ->where(['mcs.origin_id' => 4])->where(['mcso.connection_status' => 2])->asArray()->all();

        $z = 1;
    }

    public function testLivedData(){

        $a = date('Y-m-d 00:00:00', strtotime(date('Y-m-01') . ' -1 month'));
        $b = date('Y-m-d 23:59:59', strtotime(date('Y-m-01') . ' -1 day'));

        $sql = "select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '".$a."' and '".$b."' ";
        \Yii::$app->getDb()->createCommand($sql)->execute();
        $z = 1;
    }

    public function testLivedDataRelation(){
        //3天一执行
        $j = date("t"); //获取当前月份天数
        $start_time = strtotime(date('Y-m-01 H:i:s'));//获取本月第一天时间戳
        for($i=0;$i<$j;$i++){
            $before_time = $start_time+$i*86400; //每隔一天赋值给数组
            $now_day_str = date("Y-m-d",$before_time);
            $time_str = date('Y_m',$before_time);
            //初始化时间
            $created_at_start = $now_day_str.' 00:00:00';
            $created_at_end = $now_day_str.' 23:59:59';
            $this->saveMatchLivedDatasTableRelation($time_str,$created_at_start,$created_at_end);
        }

        $z=1;
    }

    public function saveMatchLivedDatasTableRelation($time_str,$created_at_start,$created_at_end){
        $table_name = "match_lived_datas_".$time_str;
        $sql = "SELECT origin_id,game_id,match_id from ".$table_name." where created_at BETWEEN '".$created_at_start."' and '".$created_at_end."' GROUP BY origin_id, match_id ORDER BY id asc";
        $allData = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        if ($allData){
            foreach ($allData as $val){
                $origin_id = $val['origin_id'];
                $game_id = $val['game_id'];
                $match_id = $val['match_id'];
                if (!empty($origin_id) && !empty($game_id) && !empty($match_id)){
                    $md5_str = md5($origin_id.$game_id.$match_id.$table_name);
                    $res = MatchLivedDatasTableRelation::find()->where(['md5_str' => $md5_str])->one();
                    if (!$res){
                        print_r("正在保存：origin_id：".$origin_id."，game_id：".$game_id."，match_id：".$match_id."，md5_str：".$md5_str."，table_name：".$table_name);
                        $MatchLivedDatasTableRelation = new MatchLivedDatasTableRelation();
                        $insertData = [
                            'origin_id' => $origin_id,
                            'game_id' => $game_id,
                            'rel_match_id' => $match_id,
                            'table_name' => $table_name,
                            'md5_str' => $md5_str
                        ];
                        $MatchLivedDatasTableRelation->setAttributes($insertData);
                        if (!$MatchLivedDatasTableRelation->save()) {
                            throw new BusinessException($MatchLivedDatasTableRelation->getErrors(), "保存失败：origin_id：".$origin_id."，game_id：".$game_id."，match_id：".$match_id."，md5_str：".$md5_str."，table_name：".$table_name);
                        }
                        print_r("保存成功：origin_id：".$origin_id."，game_id：".$game_id."，match_id：".$match_id."，md5_str：".$md5_str."，table_name：".$table_name);
                    }
                }
            }
        }
        return true;
    }

    public function testSearchMatchLivedTableRelation(){
        $origin_id = 9;
        $game_id = 3;
        $rel_match_id ='esports:match:34629a4d-1bea-4dc4-8aa0-553e450f092c';

        $res = HotBase::searchMatchLivedDataList($origin_id,$game_id,$rel_match_id);

        $z = 1;
    }

    //重新获取数据
    public function testWSDA(){
        $origin_id = 4;
        $match_id = 49813;
        $game_id = 2;
        \app\modules\task\services\wsdata\RegraspSocket::regraspOrangeSocket($origin_id,$match_id,$game_id);
    }


    public static $instance;

    public static function getInstance(){
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;

    }

/*
2020-08

CREATE TABLE `match_lived_datas_2020_08` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `origin_id` int(11) DEFAULT NULL COMMENT '数据源ID',
  `game_id` int(11) DEFAULT NULL COMMENT '游戏ID',
  `match_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '比赛ID',
  `round` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对局ID',
  `match_data` longtext COLLATE utf8mb4_unicode_ci COMMENT '数据源返回值',
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类型',
  `socket_time` datetime DEFAULT NULL COMMENT 'socket时间',
  `deleted` int(2) DEFAULT '2' COMMENT '是否删除',
  `deleted_at` datetime DEFAULT NULL COMMENT '删除时间',
  `lived_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '冗余字段',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `index_event` (`origin_id`,`match_id`,`type`),
  KEY `index_match_id` (`match_id`),
  KEY `index_game_id` (`game_id`),
  KEY `index_time` (`created_at`),
  KEY `index_origin` (`origin_id`),
  KEY `index_type` (`type`),
  KEY `index_deleted` (`deleted`),
  KEY `index_lived_status` (`lived_status`(191)),
  KEY `index_socket_time` (`socket_time`),
  KEY `index_cz` (`origin_id`,`game_id`,`match_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;



insert into match_lived_datas_2020_08 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.`created_at` < '2020-09-01 00:00:00' );
insert into match_lived_datas_2020_09 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2020-09-01 00:00:00' and '2020-09-30 23:59:59');
insert into match_lived_datas_2020_10 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2020-10-01 00:00:00' and '2020-10-31 23:59:59');
insert into match_lived_datas_2020_11 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2020-11-01 00:00:00' and '2020-11-30 23:59:59');
insert into match_lived_datas_2020_12 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2020-12-01 00:00:00' and '2020-12-31 23:59:59');
insert into match_lived_datas_2021_01 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2021-01-01 00:00:00' and '2021-01-31 23:59:59');
insert into match_lived_datas_2021_02 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2021-02-01 00:00:00' and '2021-02-28 23:59:59');
insert into match_lived_datas_2021_03 (id,origin_id,game_id,match_id,round,match_data,type,socket_time,created_at,updated_at) ( select mld.* from match_lived_datas as mld where mld.created_at BETWEEN '2021-03-01 00:00:00' and '2021-03-31 23:59:59');


*/

// 查看有绑定记录了，但是绑定状态不对  match
//select sdm.id,sdm.deal_status from standard_data_match as sdm LEFT JOIN data_standard_master_relation as dsmr on dsmr.standard_id = sdm.id  where sdm.deal_status < 4 and dsmr.resource_type = 'match'
//select sdm.id,sdm.deal_status from standard_data_player as sdm LEFT JOIN data_standard_master_relation as dsmr on dsmr.standard_id = sdm.id  where sdm.deal_status < 4 and dsmr.resource_type = 'player'
//select sdm.id,sdm.deal_status from standard_data_team as sdm LEFT JOIN data_standard_master_relation as dsmr on dsmr.standard_id = sdm.id  where sdm.deal_status < 4 and dsmr.resource_type = 'team'
//select sdm.id,sdm.deal_status from standard_data_tournament as sdm LEFT JOIN data_standard_master_relation as dsmr on dsmr.standard_id = sdm.id  where sdm.deal_status < 4 and dsmr.resource_type = 'tournament'









}