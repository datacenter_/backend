<?php
use app\modules\metadata\models\MetadataLolAbility;
use \app\modules\data\models\DataMetaDataNoRelation;
use app\modules\metadata\models\MetadataLolSummonerSpell;
use app\rest\exceptions\BusinessException;

class LolAbilityTest extends PHPUnit\Framework\TestCase
{
    //元数据 LOL Ability
    public function testAbility()
    {
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $abilityNameList = "lol:ability:list";
        $externalIdRedisName = 'lol:ability:external_id_relation_list';
        $championAbilityHotkeyRelation = "lol:ability:champion_ability_hotkey_relation_list";
        $externalNameRedisName = 'lol:ability:external_name_relation_list';
        $LolAbility = MetadataLolAbility::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolAbility as $key => $value){
            $ability_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            if (!empty($value['champion_id']) && !empty($value['hotkey'])){
                $redis->hset($championAbilityHotkeyRelation,$value['champion_id'].'_'.$value['hotkey'],$value['id']);
            }
            $redis->hset($abilityNameList,$value['id'],$ability_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:ability:relation:".$originId;
            $redisChampionAbilityRelation = "lol:ability:champion_ability_relation_id:".$originId;
            $redisNameList = "lol:ability:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_ability',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                //插入名称和ID的关联关系
//                if ($val == 6){
//                    $redis->hset($redisNameIdRelation,$item['external_name'],$master_id);
//                }
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolAbility::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes && $itemRes['deleted'] == 2){
                        $redis->hset($redisChampionAbilityRelation,$itemRes['champion_id'].'_'.$itemRes['hotkey'],$itemRes['id']);
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameListUnknownInfo = "lol:ability:list:unknown";
        $UnknownInfo = MetadataLolAbility::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameListUnknownInfo,@json_encode($UnknownInfo,320));
        }
        return true;
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getAllDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=\app\modules\task\services\StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == \app\modules\data\models\StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','std.name','std.external_name','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        //$sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }
}