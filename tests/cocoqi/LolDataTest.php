<?php

use app\modules\data\models\StandardDataMetadata;
use app\modules\task\services\StandardDataService;
use app\modules\metadata\models\MetadataLolItem;

class LolDataTest extends PHPUnit\Framework\TestCase
{
    //添加道具
    public function testAddItem(){
        $originId = 3; //3 PS  6 Riot Game
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        $redisNameRelation = "lol:items:relation:".$originId;
        $redisNameList = "lol:items:list:".$originId;
        //获取道具列表
        $res = self::getAllDataByOriginIdAndRelIdentityId('lol_item',$originId,$gameId);
        foreach ($res as $key => $item){
            $master_id = $item['master_id'];
            //插入关系列表
            $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
            if ($master_id){
                //查找主表数据插入到redis
                $itemRes = MetadataLolItem::find()->where(['id'=>$master_id])->asArray()->one();
                if ($itemRes){
                    $redis->hset($redisNameList,$item['master_id'],json_encode($itemRes,320));
                }
            }
        }

    }


    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getAllDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        $sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }

}