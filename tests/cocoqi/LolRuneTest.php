<?php
use app\rest\exceptions\BusinessException;
use app\modules\metadata\models\MetadataLolRune;

class LolRuneTest extends PHPUnit\Framework\TestCase
{
    //元数据 LOL 符文
    public function testRune()
    {
        $gameId = 2;
        $redis= \Yii::$app->redis;
        $redis->select(env('REDIS_DATABASE'));
        //循环所有主表的信息  官方ID和list的 关系列表
        $master_id_relation_list = [];
        $runeNameList = "lol:rune:list";
        $externalIdRedisName = 'lol:rune:external_id_relation_list';
        $externalNameRedisName = 'lol:rune:external_name_relation_list';
        $LolRune = MetadataLolRune::find()->where(['deleted'=>2])->asArray()->all();
        foreach ($LolRune as $key => $value){
            $rune_encode = @json_encode($value,320);
            if (!empty($value['external_id'])){
                $redis->hset($externalIdRedisName,$value['external_id'],$value['id']);
            }
            if (!empty($value['external_name'])){
                $redis->hset($externalNameRedisName,$value['external_name'],$value['id']);
            }
            $redis->hset($runeNameList,$value['id'],$rune_encode);
            $master_id_relation_list[$value['id']] = $value;
        }
        $origins = [3,6];
        foreach ($origins as $val){
            $originId = $val;
            $redisNameRelation = "lol:rune:relation:".$originId;
            //$redisNameIdRelation = "lol:rune:name_relation_id:".$originId;
            $redisNameList = "lol:rune:list:".$originId;
            //获取道具列表
            $res = self::getAllDataByOriginIdAndRelIdentityId('lol_rune',$originId,$gameId);
            foreach ($res as $key => $item){
                $master_id = $item['master_id'];
                //插入关系列表
                $redis->hset($redisNameRelation,$item['rel_identity_id'],$master_id);
                //插入名称和ID的关联关系
//                if ($val == 6){
//                    $redis->hset($redisNameIdRelation,$item['external_name'],$master_id);
//                }
                if ($master_id){
                    //查找主表数据插入到redis
                    //$itemRes = MetadataLolRune::find()->where(['id'=>$master_id])->asArray()->one();
                    $itemRes = $master_id_relation_list[$master_id];
                    if ($itemRes){
                        $redis->hset($redisNameList,$master_id,json_encode($itemRes,320));
                    }
                }
            }
        }
        //获取Unknown
        $redisNameList = "lol:rune:list:unknown";
        $UnknownInfo = MetadataLolRune::find()->where(['name'=>'Unknown'])->asArray()->one();
        if ($UnknownInfo){
            $redis->set($redisNameList,@json_encode($UnknownInfo,320));
        }
        return true;
    }
    /**
     * @param $resourceType
     * @param $relIdentityId
     * @param $originId
     * @return mixed
     * @throws \yii\base\Exception
     * 根据metadata_type 和 identityId和originId获取对应所有内容
     */
    public static function getAllDataByOriginIdAndRelIdentityId($resourceType,$originId,$gameId)
    {
        $standardClass=\app\modules\task\services\StandardDataService::getStandardActiveTable($resourceType);
        $where =['and',
            ['=','std.origin_id',$originId],
            ['=','std.game_id',$gameId],
        ];
        if($standardClass == \app\modules\data\models\StandardDataMetadata::class){
            $where[] =
                ['=','std.metadata_type',$resourceType]
            ;
        }
        $q=$standardClass::find()->alias('std')
            ->select(['std.id','std.name','std.external_name','rel.master_id','std.rel_identity_id'])
            ->leftJoin('data_standard_master_relation as rel','std.id=rel.standard_id and rel.resource_type="'.$resourceType.'"')
            ->where($where);
        //$sql=$q->createCommand()->getRawSql();
        $info=$q->asArray()->all();
        if($info){
            return $info;
        }else{
            return null;
        }
    }
}