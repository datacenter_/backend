<?php
/**
 *
 */
namespace tests\console;

use app\modules\task\services\Common;
use app\modules\task\services\grab\feijing\Start;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    public function testTeam(){
        Start::run();
    }

    public function testDiff(){
        $old=[
            "first"=>'hello',
            "second"=>'world',
        ];
        $new=[
            "first"=>"hell",
        ];
        $filter=["first"];
        $info= Common::diffParams($old,$new,$filter);
        print_r($info);
    }
}