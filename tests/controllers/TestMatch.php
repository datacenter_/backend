<?php
/**
 *
 */

class TaskTestMatch extends PHPUnit\Framework\TestCase
{
    public function testTeam(){
        $infoJson='{"id":"180","core_data":{"id":"180","name":"Team 7AM","organization":null,"game":"2","country":"20","full_name":"Team 7AM","short_name":"7AM","alias":null,"slug":"team_7am","image":"http://shaoxiatec.oss-cn-beijing.aliyuncs.com/transfer_station/1f48b8e9b2832e84234a0b8021c8a6d5/Team7AMteamlogo.png","cuser":"1","deleted":"0","created_at":"2020-06-30 20:08:05","modified_at":"2020-07-01 17:23:05","deleted_at":null},"base_data":{"id":"178","team_id":"180","steam_id":"","region":"258","world_ranking":"12","ago_30":"","total_earnings":"11","average_player_age":"223","history_players":"333","create_team_date":null,"close_team_date":null,"introduction":"444","introduction_cn":"5555"},"team_player":["125","216"]}';
        $info=json_decode($infoJson,true);
        \app\modules\org\services\TeamService::setTeam($info,1,0);
    }

    public function testMatch(){
        $infoJson='{"id":"40","core_data":{"id":"40","game":"2","tournament_id":"136","match_rule_id":null,"son_tournament_id":null,"stage_id":null,"team_1_id":"159","team_2_id":"158","game_rules":"1","match_type":"OW Best of_5","number_of_games":"5","original_scheduled_begin_at":"2020-06-30 06:11:00","scheduled_begin_at":"2020-07-31 00:00:00","slug":"royal_never_give_up_vs_estar_2020_06_30","is_rescheduled":"1","relation":"\"\"","deleted":"0","cuser":"21","created_at":"2020-06-30 06:11:07","modified_at":"2020-06-30 15:43:09","deleted_at":null},"base_data":{"id":"37","match_id":"40","location":"11","location_cn":"22","round_order":"33","round_name":"44","round_name_cn":"55","bracket_pos":"88","description":"666","description_cn":"77"}}';
        $info=json_decode($infoJson,true);
        \app\modules\match\services\MatchService::setMatch($info,1,0);
    }
}