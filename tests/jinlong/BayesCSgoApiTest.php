<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchLived;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
class BayesCSgoTest extends PHPUnit\Framework\TestCase
{

    public function testBayesApi(){
        $json = '{"updateType":3,"match_id":58301,"event_id":null,"battleUrn":"live:csgo:prodb:map:77efdc61-98b7-4608-b5fa-bf4b9b49ef56","currentPre":"bayes_csgo_ws","event_type":null,"origin_id":10}';
        $tagInfo = null;
        $param=json_decode($json,true);
        $taskInfo['params'] = json_encode($param,true);
        \app\modules\task\services\wsdata\bayes\BayesCsgoSocketToApi::run($tagInfo, $taskInfo);

    }


    public function testWeapon(){


        $sql  ='SELECT * FROM `dc_prd`.`match_lived_datas` WHERE `match_id` = \'esports:match:e39a5cf4-7eb3-42b6-bca9-4390e560c16e\' AND `match_data` LIKE \'%\"KILL\"%\' ORDER BY `id`  ';
        $connection  = Yii::$app->db;
        $command = $connection->createCommand($sql);
        $list = $command->queryAll();
        foreach ($list as $k=>$v){
            $matchData = json_decode($v['match_data'],true);
            $matchDataArr[] = $matchData['payload']['payload']['payload'];
            $weapon = $matchData['payload']['payload']['payload']['weapon'];
            if ($weapon){
                $arr[] = $weapon;
            }else{
                $d=1;
            }

        }
        $res = array_unique($arr);
        $c=1;
    }

    public function testHit(){


        $sql  ='SELECT * FROM `dc_prd`.`match_lived_datas` WHERE `match_id` = \'esports:match:e39a5cf4-7eb3-42b6-bca9-4390e560c16e\' AND `match_data` LIKE \'%\"hitgroup\"%\' ORDER BY `id`  ';
        $connection  = Yii::$app->db;
        $command = $connection->createCommand($sql);
        $list = $command->queryAll();
        foreach ($list as $k=>$v){
            $matchData = json_decode($v['match_data'],true);
            $matchDataArr[] = $matchData['payload']['payload']['payload'];
            $weapon = $matchData['payload']['payload']['payload']['hitgroup'];
            if ($weapon){
                $arr[] = $weapon;
            }else{
                $d=1;
            }

        }
        $res = array_unique($arr);
        $c=1;
    }

}