<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchLived;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
class BayesCSgoTest extends PHPUnit\Framework\TestCase
{

    public function testBayesTaskEventsBySingle(){
        $matchId              = 'esports:match:e39a5cf4-7eb3-42b6-bca9-4390e560c16e';
//        $where                = ['match_id' => $matchId];
        $livedId = 73216709;//第二个annnounce
        $livedId = 73216711;//
        $livedId = 73263510;//
//        $livedId = 16556481;
        $where                = ['id' => $livedId];
        $livedData            = \app\modules\match\models\MatchLived::find()->where($where)->asArray()->one();
        $tag                  = 'websocket_match_data.9.websocket_match_data.add.3.';
        $tagInfo['ext2_type'] = '';
//        $tagInfo['ext2_type'] = 'refresh';
        $params['id'] =$livedId;
        $params['origin_id'] =10;
        $params['game_id'] =1;
        $params['match_id'] =$matchId;
        $params['type'] ='frames';
        $params['socket_time'] =$livedData['socket_time'];
        $params['socket_id'] =$livedId;
        $params['match_data'] =json_decode($livedData['match_data'],true);
        $taskInfoArr['params'] = json_encode($params,true);
        $taskInfo = $taskInfoArr;
        \app\modules\task\services\wsdata\bayes\BayesCsgoSocket::run($tagInfo, $taskInfo);

    }

    public function testBayesTaskEventsByForeach(){ //重刷



        //esports:match:e39a5cf4-7eb3-42b6-bca9-4390e560c16e    58301    7705

        $matchId              = 58301;
//        $where                = ['match_id' => $matchId];
        $livedId = 16287034;
//        $livedId = 16556481;
        $where                = ['id' => $livedId];
        $livedData            = \app\modules\match\models\MatchLived::find()->where($where)->asArray()->one();
        $tag                  = 'websocket_match_data.9.websocket_match_data.add.3.';
        $tagInfo['ext2_type'] = '';
        $tagInfo['ext2_type'] = 'refresh';
        $params['id'] =$livedId;
        $params['origin_id'] =10;
        $params['game_id'] =1;
        $params['rel_match_id'] =7705;
        $params['match_id'] =$matchId;
        $params['type'] ='frames';
        $params['socket_time'] =$livedData['socket_time'];
        $params['socket_id'] =$livedId;
        $params['match_data'] =json_decode($livedData['match_data'],true);
        $taskInfoArr['params'] = json_encode($params,true);
        $taskInfo = $taskInfoArr;
        \app\modules\task\services\wsdata\bayes\BayesCsgoSocket::run($tagInfo, $taskInfo);

    }
    public function testAA()
    {
//        $livedObj = MatchLived::find()->where(['and', ['game_id' => 1], ['=','match_id','esports:match:755d6d63-e698-463b-896a-f6b7bfd4d846']])->asArray()->all();
//        $livedObj = MatchLived::find()->where(['and', ['game_id' => 1]])->orderBy('id desc')->limit(30000)->asArray()->all();
        $livedObj = MatchLived::find()->where(['and',
            ['>=','id','32108000'],
            ['<=','id','32108390'],
        ])->asArray()->all();
        foreach ($livedObj as $value){
            $match_data = json_decode($value['match_data'],true);
            if($match_data['payload']['payload']['subject'] == "PLAYER"){
                if($match_data['payload']['payload']['action'] == "DIED"){
                    if($match_data['payload']['payload']['payload']['suicide'] == true){
                        print_r($value);
                    }
                }
            }
        }
//        foreach ($livedObj as $value){
//            $data[] = json_decode($value['match_data'],true);
//        }
//        print_r($data);
    }
    public function testbb()
    {
        $time = "2019-12-03T11:05:29.838Z";
        $a = strtotime($time);
        $log_time = date('Y-m-d H:i:s',strtotime($time));
    }
    public function testjj()
    {
        $a = substr(15000,0,-3);
    }
    public function testgg()
    {
        if(isset($params['onewin']) && isset($params['twowin'])){
            $winner = null;
            if((int)$params['onewin'] + (int)$params['twowin'] == $params['number_of_games']){
                if($params['onewin'] > $params['twowin']){
                    $winner = (string)$params['oneseedid'];
                }elseif ($params['onewin'] < $params['twowin']){
                    $winner = (string)$params['twoseedid'];
                }
            }
            elseif ($params['onewin'] > $params['bonum']/2 || $params['twowin'] > $params['number_of_games']/2){
                if($params['onewin'] > $params['twowin']){
                    $winner = (string)$params['oneseedid'];
                }elseif ($params['onewin'] < $params['twowin']){
                    $winner = (string)$params['twoseedid'];
                }
            }
            return $winner;
        }
    }
    public function testj()
    {
        $url = env("AGENCY_URL");
    }
}