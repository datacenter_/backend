<?php

use app\modules\match\models\MatchBattle;
use app\modules\match\models\MatchBattleExtDota2;
use app\modules\match\models\MatchBattlePlayerCsgo;
use app\modules\match\services\MatchService;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\match\models\MatchLived;
use app\modules\task\services\hot\hltv\HltvMatchWs;
use Redis;
class HltvTest extends PHPUnit\Framework\TestCase
{

    public function testHltvMatchWs()
    {

        try {

//            $d = \app\modules\match\models\Match::find()->where(['id'=>1])->one();
           $c = \app\modules\match\models\Match::find()->where(['id1'=>1])->one();
//            $d->setAttribute('deleted','cc');
//           $res = $d->save();
//           $f= $d->getErrors();
           $x =1;
//            throw new BusinessException($d->getErrors(), "保存选手失败");
        }catch (Throwable $e) {
           $a =  ['error'=>$e->getMessage()];
            throw new BusinessException($e->getMessage(), "保存选手失败");
        }


        exit;

        //全量
        $socket_match_id = 2344574;
        $limit = 4000;
//        $type = 'events';
//        $type = 'no';
        $q = MatchLived::find()
            ->andwhere(['match_id' => $socket_match_id])
            ->andwhere(['>=' ,'id',1113652])//
            ->limit($limit)
            ;
        $list=$q
            ->select(['id','type','match_data'])
            ->asArray()
            ->all();
        foreach ($list as $k => $v) {
            if ( 'no' == 'no'){
                $match_data = json_decode($v['match_data'],true);
                $newList[$k.$match_data['mapName']] =$match_data['live'];
            }else{
                $match_data = json_decode($v['match_data'],true);
                $newList[$k]['id'] = $v['id'];
                $newList[$k]['type'] = $v['type'];
                $newList[$k]['socket_time'] = $v['socket_time'];
                $newList[$k]['match_data'] =json_decode($v['match_data'],true) ;
            }
            $cc['params']  = json_encode($v);
            $res           = HltvMatchWs::run('', $cc);;
        }

        $this->assertEquals(
            1,
            1
        );
        return 1;

    }
    public function testRedisEval(){

        $redis = new Redis();
        $redis->connect(env('REDIS_HOST'));
        $redis->auth(env('REDIS_PASSWORD'));

        $redis->select(env('REDIS_DATABASE'));
        $oldMatchId =571937;
        $newMatchId =57193;
        $pre = "hltv_ws:$oldMatchId:";
        $pre_new = "hltv_ws:$newMatchId:";
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 100)) {
            foreach ($arr_keys as $k => $v) {
                //     * - string: Redis::REDIS_STRING
                //     * - set:   Redis::REDIS_SET
                //     * - list:  Redis::REDIS_LIST
                //     * - zset:  Redis::REDIS_ZSET
                //     * - hash:  Redis::REDIS_HASH
                $type       = $redis->type($v);
                $vArr       = explode(':', $v);
                $newK       = array_slice($vArr, 2);
                $newKString = implode(':', $newK);
                $newNewK    = $pre_new . $newKString;

                if ($type == 1) {
                    $res1 = $redis->get($v);
                    $redis->set($newNewK, $res1);
                } elseif ($type == 3) {
                    $res2 = $redis->lRange($v, 0, -1);
                    foreach ($res2 as $k1 => $v1) {
                        $redis->rPush($newNewK, $v1);
                    }
                } elseif ($type == 5) {
                    $res3 = $redis->hGetAll($v);
                    $redis->hMSet($newNewK, $res3);
                }

            }

        }
        $this->assertEquals(
            1,
            1
        );

    }

    public function testRedisEvalDel(){

        $redis = new Redis();
        $redis->connect(env('REDIS_HOST'));
        $redis->auth(env('REDIS_PASSWORD'));

        $redis->select(env('REDIS_DATABASE'));
        $oldMatchId =58262;

        $pre_new = "hltv_ws:$oldMatchId:";
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre_new*", 20)) {
            foreach ($arr_keys as $k => $v) {
                if (!empty($v)){
//                    $redis->del($v);
                }

            }

        }
        $this->assertEquals(
            1,
            1
        );

    }
    public function testSingle(){


        $testEventId = 57555460;//event battle1 半场
        $testEventId = 67490221;//event battle1 半场
        $aa          = MatchLived::find()->where(['=', 'id', $testEventId])->asArray()->all();
        foreach ($aa as $k => $v) {
            $newList[$k]['id'] = $v['id'];
            $newList[$k]['type'] = $v['type'];
            $newList[$k]['socket_time'] = $v['socket_time'];
            if ($v['type'] == 'no'){
                $newList['match_data'] =json_decode($v['match_data'],true) ;
            }else{
                $newList[key(json_decode($v['match_data'],true)['log'][0])] =current(json_decode($v['match_data'],true)['log'][0]) ;
            }
            $v['match_data'] = json_decode($v['match_data']);
            $dd['params']  = json_encode($v);
            $res           = HltvMatchWs::run('', $dd);
            print_r($res);
        }
        $this->assertEquals(
            1,
            1
        );
//        exit;
    }
    public function testHltvMatchRefresh()
    {


//        $v['rel_match_id'] = 2347899;
//        $v['match_id'] = 57193;
//        $v['rel_match_id'] = 2348077;
//        $v['match_id'] = 57435;


//        $v['rel_match_id'] = 2347760;
//        $v['match_id'] = 56881;

        $v['rel_match_id'] = 2348299;
        $v['match_id'] = 58223;
        $v['origin_id'] = 7;
        $tag['ext2_type'] = 'refresh';
        $dd['params']  = json_encode($v);
        $res           = HltvMatchWs::run($tag, $dd);
        print_r($res);

        $this->assertEquals(
            1,
            1
        );

    }

    public function testTest2(){
        $battleId = 19071;
        $data  = MatchBattlePlayerCsgo::find()->where(['battle_id'=>$battleId])->limit(10)->asArray()->all();
        if (empty($battleId)){
            throw new BusinessException([], "保存选手时需要battleId");
        }
        $matchId = MatchBattle::find()->select(['match'])->where(['id' => $battleId])->one();
        $playerList = MatchBattlePlayerCsgo::find()->where(['battle_id'=>$battleId])->asArray()->all();
        $oldPlayerListIdKeys = array_column($playerList,'id');
        $newPlayerListIdKeys = [];
        $diffPlayerListIdKeys = [];
        // 根据battleId，order
        foreach ($data as $player) {

            if (isset($player['id']) && $player['id']) {
                $playerModel = MatchBattlePlayerCsgo::find()->where(['id' => $player['id']])->one();
            }
//            $player['order'] = 1;
            unset($player['id']);
            //battle_id order
            if (!isset($player['id']) && isset($player['order']) && $player['order']) {
                //事务
                $transaction = \Yii::$app->db->beginTransaction( 'REPEATABLE READ');
                try {
                    if ($player['steam_id']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'steam_id' => $player['steam_id']])->one();
                    }elseif ($player['nick_name']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'nick_name' => $player['nick_name'],
                        ])->one();
                    }elseif ($player['player_id']){
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'player_id' => $player['player_id'],
                        ])->one();
                    }else{
                        $playerModel = MatchBattlePlayerCsgo::find()->where([
                            'battle_id' => $battleId,
//                    'team_order' => $player['team_order'],
                            'order' => $player['order'],
                        ])->one();
                    }

                    $player['match'] = $matchId['match'];
                    $player['battle_id'] = $battleId;
                    if (isset($player['adr'])) $player['adr'] = strval($player['adr']);
                    if (isset($player['first_kills_diff'])) $player['first_kills_diff'] = strval($player['first_kills_diff']);
                    if (isset($player['kast'])) $player['kast'] = strval($player['kast']);
                    if (isset($player['rating'])) $player['rating'] = strval($player['rating']);
                    if (!$playerModel) {
                        $playerModel = new MatchBattlePlayerCsgo();
//                    $playerModel->setAttribute('battle_id',$battleId);
                    }
                    $playerModel->setAttributes($player);
                    $res = $playerModel->save();
                    $newPlayerListIdKeys[] = $playerModel->id;
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw new BusinessException($e->getMessage(), "csgo选手保存错误,事务回滚");
                }
                if (!$res) {
                    throw new BusinessException($playerModel->getErrors(), "保存选手失败");
                }

            }
        }
        $diffPlayerListIdKeys = array_diff($oldPlayerListIdKeys,$newPlayerListIdKeys);
        if (!empty($diffPlayerListIdKeys)&&!empty($newPlayerListIdKeys)){
            foreach ($diffPlayerListIdKeys as $k=>$v){
                $diffPlayer = MatchBattlePlayerCsgo::find()->where(['id'=>$v,'battle_id'=>$battleId])->one();
                $diffPlayer->delete();
            }
        }
    }


    public function testWstoApi(){
        $sql ='select id,deleted,team_1_id,team_2_id,scheduled_begin_at,COUNT(team_1_id) As Account,COUNT(team_2_id) As Account2 ,COUNT(scheduled_begin_at) AS Account3 ,COUNT(deleted) as Account4 from  `match` group by team_1_id,team_2_id,scheduled_begin_at,deleted HAVING Account>1 and Account2>1 AND Account3>1 and Account4>1 and deleted=2';
        $list=\Yii::$app->getDb()->createCommand($sql)->queryAll();
        foreach ($list as $k=>$v){
            $where =
                [
                    'team_1_id'=>$v['team_1_id'],
                    'team_2_id'=>$v['team_2_id'],
                    'scheduled_begin_at'=>$v['scheduled_begin_at'],
                    'deleted'=>$v['deleted'],
                ]
                ;
            $res = \app\modules\match\models\Match::find()->alias('m')
                ->select('m.id,team_1_id,team_2_id,scheduled_begin_at,mr.standard_id')
                ->leftJoin('data_standard_master_relation as mr' , 'mr.master_id= m.`id` and mr.`resource_type` = "match"')
                ->where($where)->asArray()->all();
            foreach ($res as $k1=>$v1){
                $newArr['id'] = $v1['id'];
                $newArr['team_1_id'] = $v1['team_1_id'];
                $newArr['team_2_id'] = $v1['team_2_id'];
                $newArr['scheduled_begin_at'] = $v1['scheduled_begin_at'];
                $newArr['standard_id'] = $v1['standard_id'];
                if ($v1['standard_id']){
                    $TotalArr[] = $newArr;
                    $repeatBindingArr[$v1['team_1_id'].'_'.$v1['team_1_id'].'_'.$v1['scheduled_begin_at']][] =  $newArr;
                }else{
                    $TotalNoArr[] = $newArr;
                }

            }

        }
        foreach ($repeatBindingArr as $k=>$v){
            $count = count($v);
            if ($count>1){
                $repeatBindingArrNew[] = $v;
            }
        }
        foreach ($TotalNoArr as $k=>$v){

                file_put_contents('C:\Users\jiyeon\Documents\test.txt',$v['id'].PHP_EOL,FILE_APPEND);
                \app\modules\match\controllers\MatchController::actionDel($v['id'],1);


            $delArr[] = $v['id'];
        }

        $a=1;
    }

    public function testAbc()
    {
        /**
         * @param $relIdentityId(比赛id)
         * @param $resourceType(资源类型)
         * @param $originId(来源id)
         * @param $gameId(游戏id)
         * @param $reason(刷新方式)
         */
        $relIdentityId = "2344574";
        $resourceType = "match";
        $originId = "7";
        $gameId = "1";
         \app\modules\data\services\binding\BindingMatchService::getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId);
        $this->assertEquals(
            1,
            1
        );
    }
    public function testUpcomingCount(){
        $params['name'] = 'IEM Summer 2021 Open Qualifier 2';
        $JSON           = MatchService::getMatchList($params);
//        exit;
//        $JSON = ["2347968","2348018","2348077","2347967","2348017","2347966","2347963","2347927","2347928","2347926","2347452","2347922","2347923","2347924","2347878","","","","2347925","2347899","2347896","2347897","2347898","2347840","2347891","2347893","2347892","2347894","2347509","2347510","2347507","2347508","2347860","2347396","2347395","2347398","2347397","2347502","2347501","2347499","2347500","2347760","2347506","2347714","2347659","2347665","2347617","2347723","2347724","2347713","2347712","2347718","2347708","2347709","2347711","2347710","2347633","2347661","2347660","2347634","2347630","2347592","2347588","2347626","2347541","2347619","2347616","2347417","2347492","2347589","2347540","2347590","2347593","2347591","2347615","2347618","2347607","2347606","2347551","2347524","2347613","2347504","2347603","2347602","2347601","2347600","2347525","2347527","2347466","2347462","2347529","2347528","2347526","2347505","2347463","2347460","2347465","2347464","2347459","2347461","","","","2347450","2347447","","","","","","2347449","2347445","2347446","2347448","2347451","2347444","2347072","2347079","2347418","","2346485","2346610","2347405","2347406","","","","","2347416","","","","","","","2347401","2347402","","2347404","2347403","","","2347074","","","","","","","","","","","2347080","","","","","","","","","","","","","","","","","","2347266","2347261","2347265","2347264","2347263","","","","2347262","","","","","","2347252","","2347248","","","","","2347251","2347249","2347247","2347250","","","","","","","2347280","2347281","","2347279","2347258","2347259","2347255","2347260","2347257","2347256","","","","","","","","","","","2347246","2347245","2347244","2347243","2346710","","","","","","2347242","","","","","2347268","2347267","","","","2347253","2347254","2347078","2347083","","","","","2346846","2346625","","","","","","","","2346849","2347077","","","","","","","","2347095","","","2346705","","","","","","","","","2346601","2346506","","","","","","","","","2347081","2347082","","","","","","","","","","2347092","2347094","","2347091","2347093","","","","2347101","2347102","","","","2347076","2347073","2347075","","","","","","","","","","2347087","2347086","2347089","2347090","","","","2347088","2347085","","","","2347098","","","2347099","","","","2347084","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","2346708","","2346971","2346970","2346972","2346969","","","","2346973","","","","","","","2346851","2346847","2346848","2346605","","","","","","","2346850","","","","","","2346980","2346978","","2346979","2346965","","2346968","2346964","2346966","2346967","","","","","","","","2346845","","","","","2346840","2346842","2346841","2346843","2346844","","","","","2346897","2346898","2346896","","","2346838","2346839","2346832","2346833","","","2346837","","","","2346831","2346830","2346829","2346828","","","","","","2346615","","","","","","","","","","","2346726","","","2346725","2346724","2346611","","","","","","","","","2346711","2346713","","","","","","2346709","","2346712",""];
//        $where=['status'=>'Match over'];
////        $where3 =['matche_id'=>2346904];
////        $where4 =['<','id',72832];
//        $matchList = \app\modules\task\models\MatchAll::find()
//            ->where($where)
////            ->andWhere($where3)
////            ->andWhere($where4)
//            ->asArray()->orderBy('id desc')
//            ->limit(100)
//            ->all();
        foreach ($JSON as $k=>$v){
            if (!empty($v)){
                $matchList[$k]['matche_id'] = $v;

            }
        }
        $upcomingCountArr = [];
        foreach ($matchList as $k=>$v){
//            $v['matche_id']  = $v;
            $where2['match_id'] = (string)$v['matche_id'];
            $where3=['<>','match_data','{"log":[{"Status":[]}]}'];
//            $liveDataList = MatchLived::find()->where($where2)->andWhere($where3)->asArray()->limit(5000)->all();
            $count = MatchLived::find()->where($where2)->andWhere($where3)->count();
            $lastCurrentRound=0;
//            $arr[] = $count;
//            continue;
            if ($count>0){
                if ($count>1000){
                    $limit    = 1000;

                }else{
                    $limit = (int)ceil($count / 2);
                }
                $countSlice = (int)ceil($count / $limit);

                for ($i = 0; $i < $countSlice; $i++) {
                    $offset    = 0 + $limit * $i;
                    $liveDataList      = MatchLived::find()->where($where2)->andWhere($where3)->orderBy('id asc')->offset($offset)->limit($limit)->asArray()->all();
//                    $liveDataList = MatchLived::find()->where($where2)->andWhere($where3)->asArray()->all();
                    foreach ($liveDataList as $k1=>$v1){
                        $matchDataArr= json_decode($v1['match_data'],true);
                        //battle_start
                        if (strstr($v1['match_data'],'MatchStarted')){
                            $battleStartArr[$v['matche_id']]['MatchStarted'] = $v1['socket_time'];
                            $battleStartArr[$v['matche_id']]['MatchStartedKey'] = $k1;
                        }

                        if ($lastCurrentRound==15&&!empty($battleStartArr)&&strstr($v1['match_data'],'terroristMatchHistory')&&$k1>0&&$matchDataArr['currentRound']==1){
                            $battleStartCountArr[$v['matche_id']][$v1['id']] = $v1['socket_time'];
                            $LessRoundCountArr[$v['matche_id']][$v1['id']] = $v1['socket_time'];
                            unset($battleStartArr);
                        }
                        if (!empty($battleStartArr)&&strstr($v1['match_data'],'terroristMatchHistory')&&$k1>0&&$k1 == ($battleStartArr[$v['matche_id']]['MatchStartedKey'] + 1)&&$matchDataArr['currentRound']==1){
                            $battleStartCountArr[$v['matche_id']][$v1['id']] = $v1['socket_time'];

                            unset($battleStartArr);
                        }
                        $lastCurrentRound= $matchDataArr['currentRound'];
                        //upcoming
                        if (strstr($v1['match_data'],'Restart')){
                            unset($upcomingArr);
                            $vLength = strlen($v1['match_data']);
                            $upcomingArr[$v['matche_id']]['Restart'] = $v1['socket_time'];

                        }



                        if ($upcomingArr[$v['matche_id']]['Restart']&&strstr($v1['match_data'],'MatchStarted')){
                            $matchDataArr= json_decode($v1['match_data'],true);
                            $upcomingArr[$v['matche_id']]['MatchStarted'] = $v1['socket_time'];
                            $upcomingArr[$v['matche_id']]['MatchStartedKey'] = $k1;

                            continue;
                        }
                        if ($upcomingArr[$v['matche_id']]['MatchStarted'] &&
                            $upcomingArr[$v['matche_id']]['Restart'] &&
                            $upcomingArr[$v['matche_id']]['MatchStartedKey'] &&
                            $k1 == ($upcomingArr[$v['matche_id']]['MatchStartedKey'] + 1)) {
                            if (strstr($v1['match_data'],'RoundStart')){
                                if ($v1['socket_time']!=$upcomingArr[$v['matche_id']]['MatchStarted']){
                                    $upcomingCountArr[$v['matche_id']]['RoundStartToBattleStart'][$v1['id']] =$upcomingArr[$v['matche_id']]['MatchStarted'];
                                }
                            } elseif (strstr($v1['match_data'],'terroristMatchHistory')){
                                if ($matchDataArr['currentRound']==1){
                                    $upcomingCountArr[$v['matche_id']]['FrameToBattleStart'][$v1['id']] =$upcomingArr[$v['matche_id']]['MatchStarted'];
                                }
                            }else{

                            }

                            unset($upcomingArr);
                        }

                    }

                }
                if (empty($upcomingCountArr[$v['matche_id']])){
                    $upcomingNoCountArr[$v['matche_id']] = $v['matche_id'];
                }
                if (empty($battleStartCountArr[$v['matche_id']])){
                    $battleStartNoCountArr[$v['matche_id']] = $v['matche_id'];
                }


            }else{
                $noDataMatch[] = $v['matche_id'];
            }
            $c=1;
            file_put_contents('C:\Users\jiyeon\Documents\logs\LessRoundCountArr.txt',json_encode($LessRoundCountArr,320).PHP_EOL);
            file_put_contents('C:\Users\jiyeon\Documents\logs\testNoDataMatch2.txt',json_encode($noDataMatch,320).PHP_EOL);
            file_put_contents('C:\Users\jiyeon\Documents\logs\pcomingCountArr2.txt',json_encode($upcomingCountArr,320).PHP_EOL);

            file_put_contents('C:\Users\jiyeon\Documents\logs\battleStartCountArr2.txt',json_encode($battleStartCountArr,320).PHP_EOL);
            file_put_contents('C:\Users\jiyeon\Documents\logs\battleStartNoCountArr2.txt',json_encode($battleStartNoCountArr,320).PHP_EOL);
        }
        return 1;
    }

    public function testTaskHltv(){
        $map = [
            'origin_id' => 7,
//            'match_id' => 562371,
            'match_id' => 2344574, //571079,
            'game_id' => 1,
//            'type' => 'events'
//            'type' => 'no'
        ];
        $data = \app\modules\match\models\MatchLived::find()
            ->orderBy('id ASC')
            ->where($map)
//            ->andwhere(['<','id','797792'])
//            ->andwhere(['>=','id','797766'])
//            ->where(['=','id','133957'])

//            ->andwhere(['=','id','1122300'])//798813
            ->andwhere(['=','id','1111558'])//798813
//            ->andwhere(['>','id','193385'])
//            ->andwhere(['<','id','330331'])
            ->select('id,match_data,type,round,match_id,game_id,origin_id,socket_time,created_at')
            ->asArray()
            ->all();
        //$socket_time = date('Y-m-d H:i:s');
        foreach($data as $key => $value){
            $insertData = [
                'origin_id' => $value['origin_id'],
                'game_id' => $value['game_id'],
                'match_id' => $value['match_id'],
                'round' => $value['round'],
                'match_data' => $value['match_data'],
                'type' => $value['type'],
                'socket_time' => isset($value['socket_time']) ? $value['socket_time']: $value['created_at'],
                'socket_id' => $value['id']
            ];
            if(!isset($insertData['socket_time'])){
                $insertData['socket_time'] = date('Y-m-d H:i:s');
            }
            $newInsertData = @$insertData;
            $newInsertData['match_data']= json_decode($value['match_data'],true);
//            $taskType = 4;
//            if ($value['type'] == 'events'){
//                $taskType = '9001';
//            }


            $taskType = '9001';
            //再插入一次 用户restapi
//            $item = [
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
////                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
//                    $value['origin_id'], \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $value['game_id']),
//                "type" => \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
//                "batch_id" => date("YmdHis"),
//                "params" => $newInsertData,
//                "redis_server"=>'order_hltvcsgo'
//            ];
//            TaskRunner::addTask($item, $taskType);
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                    $value['origin_id'], \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $value['game_id']),
                "type" => \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                "batch_id" => date("YmdHis"),
                "params" => $newInsertData,
                "redis_server"=>'order_hltvcsgo'
            ];
            TaskRunner::addTask($item, $taskType);
            sleep(1);
        }
        $this->assertEquals('1','1');
        return "执行完毕!!!";

    }









}