<?php

use app\modules\common\services\Consts;
use app\modules\common\services\EsService;
use app\modules\data\models\DbSocket;
use app\modules\match\models\MatchLived;
use app\modules\match\services\MatchService;
use app\modules\task\models\MatchLivedDatasOffset;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\kafka\KafkaCustomer;
use app\modules\task\services\kafka\KafkaProducer;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\task\services\api\RenovateService;


class KafkaTest extends PHPUnit\Framework\TestCase
{


    public function testCustom(){

        $groupId = 'test';
        $topic = 'ori_hltv_csgo_02';
//        $topic = 'ori_bayes_lol_01';
        //$broken = KafkaProducer::BROKERS_INTERNAL;
        $broken = env('KAFKA_BROKERS_OUT');
        KafkaCustomer::customLiveData($groupId,$topic,$broken);
    }

    public function testCustom2()
    {
        $self = (object)[];
        $message = (object)[];
        $message->payload = '{"origin_id": 7, "game_id": 1, "match_id": "2347862", "round": 0, "type": "events", "match_data": "{\"server_id\": 2, \"type\": \"events\", \"version\": \"1.0\", \"seqidx\": 3885, \"match_id\": \"2347862\", \"socket_time\": \"2021-04-11 20:57:45.122\", \"origin_id\": 7, \"game_id\": 1, \"payload\": {\"log\": [{\"RoundEnd\": {\"counterTerroristScore\": 15, \"terroristScore\": 16, \"winner\": \"TERRORIST\", \"winType\": \"Terrorists_Win\"}}]}}", "task": 1}';
//        $message->payload = '{"origin_id": 4, "game_id": 2, "match_id": "lmilebe0915", "round": "2", "type": "no", "match_data": "{\"server_id\": 1, \"type\": \"frames\", \"version\": \"1.0\", \"seqidx\": 8573, \"match_id\": \"lmilebe0915\", \"socket_time\": \"2021-04-13 15:41:09.206\", \"origin_id\": 4, \"game_id\": 2, \"payload\": \"ping\"}", "task": 1}';
//        $message->payload = '{"origin_id": 7, "game_id": 1, "match_id": "2347774", "round": 0, "type": "events", "match_data": "{\"server_id\": 2, \"type\": \"events\", \"version\": \"1.0\", \"seqidx\": \"\", \"match_id\": \"2347774\", \"socket_time\": \"2021-04-13 17:13:01.377633\", \"origin_id\": 7, \"game_id\": 1, \"payload\": {\"log\": [{\"Status\": {}}]}}", "task": 1}';
        //双服务器的排重原则: 插入自己的,判断对面的
        $revNum          = '';
        $redisOldKey     = '';
        $arrRev          = [1 => 2, 2 => 1];
        $redisKey        = 'kafka:' . $message->payload;
        $payLoadArr      = json_decode($message->payload, true);
        $match_data_json = $payLoadArr['match_data'];
        $match_data_arr  = json_decode($match_data_json, true);
        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'), env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));
        if (array_key_exists('server_id',$match_data_arr)){
            $serverNum = $match_data_arr['server_id'];
            $payLoadArr['socket_time'] = substr($match_data_arr['socket_time'],0,19);//dalei的时间带有毫秒
            if ($serverNum == 1 || $serverNum == 2) {
                //判断是否带有服务器编号
                $revNum                = $arrRev[$serverNum];
                $match_data_field      = $match_data_arr['payload'];
                if ($match_data_field=='ping'){
                    return false;
                }
                $match_data_field_json = json_encode($match_data_field, 320);
                //插入jianqi的统计信息(比赛的最后一条的比赛时间),除了bayes的比赛
                if ($payLoadArr['match_id'] != 9&&$match_data_field_json!='{"log":[{"Status":[]}]}') {
                    $nowDay = date('Y-m-d');
                    $redis->hSet('match_last_socket_time:' . $nowDay, $payLoadArr['match_id'] . '_s' . $serverNum, $payLoadArr['socket_time']);
                }
            } else {
                echo 'errorServerId ' . $message->offset . ' topic' . $self->topic . PHP_EOL;
            }

            if ($revNum) {
                $redisKey    = 'kafka:' . $revNum . ':' . $match_data_field_json;
                $redisOldKey = 'kafka:' . $serverNum . ':' . $match_data_field_json;
            }else{
                echo 'errorServerId2 ' . $message->offset . ' topic' . $self->topic . PHP_EOL;

            }

            $payLoadArr['match_data'] = $match_data_field_json;
            $matchContent = $payLoadArr;
        } else {
            $arr                = json_decode($message->payload, true);
            $arr['socket_time'] = $self->kafka_time;
            $matchContent   = $arr;
        }
        if ($revNum && $redisOldKey) {
            //原则: 插入自己的,判断对面的
            $getOther  = $redis->get($redisKey);
            $setResult = !$getOther;
            if (empty($getOther)) {
                //说明另一个服务器10秒内没有插入过,是正确的数据
                $redis->set($redisOldKey, 'yes', ['nx', 'ex' => 10]);
            }
        }else{
            $setResult          = $redis->set($redisKey, 'yes',['nx','ex'=>10]);
        }
        if ($setResult) {
            //不重复的数据
//            $self->doLivesData($matchContent);//插入到表,插入rocket队列
        } else {
            //重复的数据
        }
    }
    public function customLiveData($groupId,$topicName,$broken){
        if (empty($groupId)||empty($topicName)){
            return false;
        }

        $self = new self();
        $self->topic = $topicName;
        //设置消费组 test-2(测试) consumer-group3(正式)
        $conf = new \RdKafka\Conf();
        $conf->set('sasl.mechanisms', 'PLAIN');
        $conf->set('api.version.request', 'true');
        $conf->set('sasl.username', "alikafka_pre_public_intl-sg-25u22b8wg02");
        $conf->set('sasl.password', "JmWBZ3IhQROtVIZdMrp5SM6krONfoXih");
        $conf->set('security.protocol', 'SASL_SSL');
        $conf->set('ssl.ca.location', 'C:\Users\jiyeon\Code\ca-cert.pem');
        $conf->set('group.id', $groupId);
//        $conf->set('group.id', 'consumer-group3');
        $conf->set('enable.auto.commit', 0);
        $conf->set('metadata.broker.list', $broken);
        //设置broker BROKERS_OUT(外网ip)  BROKERS_INTERNAL(内网ip)
        $topicConf = new RdKafka\TopicConf();
        $conf->setDefaultTopicConf($topicConf);
        $consumer = new RdKafka\KafkaConsumer($conf);
        $consumer->subscribe($topicName);

        $repeatCount = 0;
        $newCount    = 0;
        $count       = 0;

        while (true) {
            //参数1表示消费分区，这里是分区0
            //参数2表示同步阻塞多久
            $message = $consumer->consume(0, 12 * 1000);
            if (is_null($message)) {
                sleep(1);
                echo 'No more messages' . PHP_EOL;
                continue;
            }
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    $message->payload='{"origin_id": 4, "game_id": 2, "match_id": "lmieddc90b5", "round": "3", "type": "no", "match_data": "{\"server_id\": 1, \"type\": \"frames\", \"version\": \"1.0\", \"seqidx\": 6952, \"match_id\": \"lmieddc90b5\", \"socket_time\": \"2021-04-06 14:32:37.441\", \"origin_id\": 4, \"game_id\": 2, \"payload\": \"\"ping\"\"}", "task": 1}';
                    $dataOffsetList = $redis->lRange($offsetListKey,0,-1);
                    if (!empty($dataOffsetList)&&is_array($dataOffsetList)&&in_array($message->offset,$dataOffsetList)){
                        $offsetRepeatKey = 'kafka:'.$topicName.':repeat_list';
                        $redis->lPush($offsetRepeatKey,$message->offset);
                        if ($redis->lLen($offsetRepeatKey) > 1000) {
                            $redis->rPop($offsetRepeatKey);//删除旧的重复offset,只保留最近1000条,防止list无限增长
                        }
                        echo 'repeatOffset ' . $message->offset .' topic' . $self->topic.PHP_EOL;
//                        break;
                    }
                    exit;
                    $self->kafka_time = date('Y-m-d H:i:s', substr($message->timestamp, 0, -3));
                    $self->offset = $message->offset;
                    $redis->lPush($offsetListKey, $message->offset);
                    if ($redis->lLen($offsetListKey) > 100) {
                        $redis->rPop($offsetListKey);//删除旧的offset,只保留最近100条,防止list无限增长
                    }

                    if ($message->payload) {
                        $redisKey           = 'kafka:' . $message->payload;
                        $payLoadArr = json_decode($message->payload,true);
                        $match_data_json = $payLoadArr['match_data'];
                        $match_data_arr  = json_decode($match_data_json,true);
                        $arrRev    = [1 => 2, 2 => 1];
                        $revNum    = '';
                        $redisOldKey    = '';
                        if (array_key_exists('server_id',$match_data_arr)){
                            $serverNum = $match_data_arr['server_id'];
//                            $serverNum=1;
                            if ($serverNum == 1 || $serverNum == 2) {
                                //判断是否带有服务器编号
                                $revNum           = $arrRev[$serverNum];
                                $match_data_field = $match_data_arr['payload'];
                                $match_data_field_json = json_encode($match_data_field,320);

                            }

//                            $redisKey = 'kafka:' . $match_data_field_json;

                            if ($revNum) {
                                $redisKey = 'kafka:' . $revNum .':'. $match_data_field_json;
                                $redisOldKey = 'kafka:' . $serverNum .':'. $match_data_field_json;
                            }
                            $payLoadArr['socket_time'] = substr($match_data_arr['socket_time'],0,19);
                            $payLoadArr['match_data'] = $match_data_field_json;
                            $message->payload = json_encode($payLoadArr,true);
                        }else{
                            $arr                = json_decode($message->payload, true);
                            $arr['socket_time'] = $self->kafka_time;

                            $message->payload   = json_encode($arr, 320);
                        }
                        if ($revNum && $redisOldKey) {
                            //原则: 插入自己的,判断对面的
                            $getOther = $redis->get($redisKey);
                            $setResult = !$getOther;
                            if (empty($getOther)){
                                $redis->set($redisOldKey, 'yes',['nx','ex'=>10]);
                            }
                        }else{
                            $setResult          = $redis->set($redisKey, 'yes',['nx','ex'=>10]);
                        }

                        if ($setResult) {
                            //不重复的数据
                            $self->doLivesData($message->payload);//插入到表,插入rocket队列
                            $redisKeyCommon = 'kafka:count:common';
//                            $redis->incr($redisKeyCommon);
                            $newCount++;
                        } else {
                            //重复的数据
                            $redisKeyRepeat = 'kafka:count:repeat';
//                            $redis->incr($redisKeyRepeat);
                            $repeatCount++;
                        }
                    }
                    $count++;
                    $redisKeyTotal = 'kafka:count:total';
//                    $redis->incr($redisKeyTotal);
                    echo 'success ' . $count .' common ' . $newCount.' repeat ' . $repeatCount . ' offset ' . $message->offset.PHP_EOL;
//                    var_dump($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:

                    echo 'No more messages; will wait for more' . PHP_EOL;
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo 'Timed out' . PHP_EOL;
                    break;
                default:
                    print_r($message->err);
                    break;
            }
        }
    }

        public function testTest2(){

        $arr['a'] =json_encode('ping',320);
        $b = json_encode($arr,320);
        $c = json_decode($b,true);
        $d = json_decode($c['a']);

        $arr2['f'] ="ping";
        $g = json_encode($arr2,320);
        $h = json_decode($g,true);
        $i = json_decode($h['f']);
        exit;
        $message=  new stdClass();
        $message->payload = '{"origin_id": 4, "game_id": 2, "match_id": "lmieddc90b5", "round": "3", "type": "no", "match_data": "{\"server_id\": 1, \"type\": \"frames\", \"version\": \"1.0\", \"seqidx\": 6952, \"match_id\": \"lmieddc90b5\", \"socket_time\": \"2021-04-06 14:32:37.441\", \"origin_id\": 4, \"game_id\": 2, \"payload\": \"ping\"}", "task": 1}';

        if ($message->payload) {
            $redisKey           = 'kafka:' . $message->payload;
            $payLoadArr = json_decode($message->payload,true);
            $match_data_json = $payLoadArr['match_data'];
            $match_data_arr  = json_decode($match_data_json,true);
            $arrRev    = [1 => 2, 2 => 1];
            $revNum    = '';
            $redisOldKey    = '';
            if (array_key_exists('server_id',$match_data_arr)){
                $serverNum = $match_data_arr['server_id'];
//                            $serverNum=1;
                if ($serverNum == 1 || $serverNum == 2) {
                    //判断是否带有服务器编号
                    $revNum           = $arrRev[$serverNum];
                    $match_data_field = $match_data_arr['payload'];
                    $match_data_field_json = json_encode($match_data_field,320);
                    $message->payload = substr($message->payload, 1);
                }

//                            $redisKey = 'kafka:' . $match_data_field_json;

                if ($revNum) {
                    $redisKey = 'kafka:' . $revNum .':'. $match_data_field_json;
                    $redisOldKey = 'kafka:' . $serverNum .':'. $match_data_field_json;
                }
                $payLoadArr['socket_time'] = substr($match_data_arr['socket_time'],0,19);
                $payLoadArr['match_data'] = $match_data_field_json;
                $message->payload = json_encode($payLoadArr,true);
            }else{
                $arr                = json_decode($message->payload, true);
                $arr['socket_time'] = $self->kafka_time;

                $message->payload   = json_encode($arr, 320);
            }
            if ($revNum && $redisOldKey) {
                //原则: 插入自己的,判断对面的
                $getOther = $redis->get($redisKey);
                $setResult = !$getOther;
                if (empty($getOther)){
                    $redis->set($redisOldKey, 'yes',['nx','ex'=>10]);
                }
            }else{
                $setResult          = $redis->set($redisKey, 'yes',['nx','ex'=>10]);
            }

            if ($setResult) {
                //不重复的数据
                $self->doLivesData($message->payload);//插入到表,插入rocket队列
                $redisKeyCommon = 'kafka:count:common';
//                            $redis->incr($redisKeyCommon);
                $newCount++;
            } else {
                //重复的数据
                $redisKeyRepeat = 'kafka:count:repeat';
//                            $redis->incr($redisKeyRepeat);
                $repeatCount++;
            }
        }
        $count++;
    }


    public function testKafka()
    {

        $redis = new Redis();
        $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
//        $redis->connect(env('REDIS_HOST'),env('REDIS_PORT'));
        $redis->auth(env('REDIS_PASSWORD'));
        $redis->select(env('REDIS_DATABASE'));

        $conf = new RdKafka\Conf();
        $conf->setDrMsgCb(function ($kafka, $message) {
            file_put_contents("./c_dr_cb.log", var_export($message, true), FILE_APPEND);
        });
        $conf->setErrorCb(function ($kafka, $err, $reason) {
            file_put_contents("./err_cb.log", sprintf("Kafka error: %s (reason: %s)", rd_kafka_err2str($err), $reason).PHP_EOL, FILE_APPEND);
        });

//设置消费组
        $conf->set('group.id', 'test');//设置

        $rk = new RdKafka\Consumer($conf);
        $rk->addBrokers("47.93.12.155:9093,101.201.233.215:9093,47.93.1.163:9093");
//        $rk->addBrokers("172.17.147.202:9092,172.17.147.203:9092,172.17.147.204:9092");

        $topicConf = new RdKafka\TopicConf();
        $topicConf->set('request.required.acks', 1);
//在interval.ms的时间内自动提交确认、建议不要启动
//$topicConf->set('auto.commit.enable', 1);
        $topicConf->set('auto.commit.enable', 0);
        $topicConf->set('auto.commit.interval.ms', 100);

// 设置offset的存储为file
//$topicConf->set('offset.store.method', 'file');
// 设置offset的存储为broker
        $topicConf->set('offset.store.method', 'broker');
//$topicConf->set('offset.store.path', __DIR__);

//smallest：简单理解为从头开始消费，其实等价于上面的 earliest
//largest：简单理解为从最新的开始消费，其实等价于上面的 latest
//$topicConf->set('auto.offset.reset', 'smallest');

//        $topic = $rk->newTopic("bcjj_football", $topicConf);
        $topic = $rk->newTopic("mysql_sync_kafka", $topicConf);

// 参数1消费分区0
// RD_KAFKA_OFFSET_BEGINNING 重头开始消费
// RD_KAFKA_OFFSET_STORED 最后一条消费的offset记录开始消费
// RD_KAFKA_OFFSET_END 最后一条消费
        $topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);
//        $topic->consumeStart(0, RD_KAFKA_OFFSET_END);
//$topic->consumeStart(0, RD_KAFKA_OFFSET_END); //
//$topic->consumeStart(0, RD_KAFKA_OFFSET_STORED);
    $count =0;
        while (true) {
            //参数1表示消费分区，这里是分区0
            //参数2表示同步阻塞多久
            $message = $topic->consume(0, 12 * 1000);
            if (is_null($message)) {
                sleep(1);
                echo "No more messages\n";
                continue;
            }
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    if ($message->payload){
                        $redisKey = 'kafka:'.$message->payload;
                        $setResult=$redis->setnx($redisKey,'yes');
                        $redis->expire($redisKey,10);
                        if ($setResult){
                            //不重复的数据插入到我们的队列
//                            $this->doLivesData($message->payload);
                        }else{
                            //重复的数据
                        }
                    }
                    $count++;
                    echo $count;
                    var_dump($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo "Timed out\n";
                    break;
                default:
                    throw new \Exception($message->errstr(), $message->err);
                    break;
            }
        }
        echo $count;
    }


    public function testKafkaProducer(){
        $sql = 'select * from match_data where id= 205630';
        $db_websoket = DbSocket::getDb()->createCommand($sql);
        $dbres = $db_websoket->queryAll();
        $db = $dbres[0]['match_data'];
        $events_data = $db;
        $event_data_ws  = [
            'match_data' => $events_data,
            'type'       => 'events',
            'game_id'    => 1,
            'match_id'   => 45435
        ];
//        KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'),$event_data_ws);

//exit;
//        $rk = new RdKafka\Producer();
//        $rk->setLogLevel(LOG_DEBUG);
//        $rk->addBrokers("47.93.12.155:9093,101.201.233.215:9093,47.93.1.163:9093");
//
//        $topic = $rk->newTopic("socket_scr_test");
//        $sql = 'select * from match_data where id= 205630';
//        $db_websoket = DbSocket::getDb()->createCommand($sql);
//        $dbres = $db_websoket->queryAll();
//        $arr['data'] = $dbres[0];
//        $data= json_encode($arr,320);
//        $db_websoket->insert('match_data', $event_data_ws);
//        $res = $db_websoket->execute();
        for ($i = 0; $i < 100; $i++) {
            KafkaProducer::KafkaMatchData(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'),$event_data_ws);
//            $res = $topic->produce(RD_KAFKA_PARTITION_UA, 0, $data);
        }
    }

    public function testP()
    {
        $sql = 'select * from match_data where id= 205630';
        $db_websoket = DbSocket::getDb()->createCommand($sql);
        $dbres = $db_websoket->queryAll();
        $db = $dbres[0]['match_data'];
        $events_data = $db;
        $event_data_ws  = [
            'match_data' => $events_data,
            'type'       => 'events',
            'game_id'    => 1,
            'match_id'   => 45435
        ];

        $rk = new RdKafka\Producer();
        $rk->setLogLevel(LOG_DEBUG);
        $rk->addBrokers(KafkaProducer::BROKERS_OUT);
        $topic       = $rk->newTopic(env('KAFKA_OUTPUT_TOPIC_SOCKET_SCR_CSGO'));

        for ($i = 0; $i < 100; $i++) {
            $event_data_ws['lg_id']=$i;
            $arr['data'] = $event_data_ws;
            $data        = json_encode($arr, 320);
            $res         = $topic->produce(RD_KAFKA_PARTITION_UA, 0, $data);
            $rk->flush(-1);
        }
    }

    public function doLivesData($data)
    {
        $data1        = json_decode($data, true);
        $origin_id    = $data1['origin_id'];
        $game_id      = $data1['game_id'];
        $rel_match_id = $data1['match_id'];
        $round        = $data1['round'];
        $match_data   = $data1['match_data'];
        $type         = $data1['type'];
        $socket_time  = $data1['socket_time'];
        $task         = $data1['task'];
        if (empty($origin_id) || empty($game_id)) {
            echo '参数为空';
            return false;
//            throw new BusinessException([], '参数不能为空');
        }
        if (empty($socket_time)) {
            $socket_time = date('Y-m-d H:i:s');
        }
        if ($origin_id == 2 && $game_id == 3) {//Abios dota 需要根据battleID 获取原始比赛ID
            //查询原始比赛ID
            $match_data_array = @json_decode($match_data, true);
            if ($match_data_array) {
                $rel_battle_id     = $match_data_array['payload']['match']['id'];
                $standardMatchInfo = StandardDataMatch::find()->where(['like', 'battlesIds', $rel_battle_id])->asArray()->one();
                $rel_match_id      = @$standardMatchInfo['rel_identity_id'];
            }
        }
        if ($origin_id == 4 && $match_data == 'ping'){
            return true;
        }
        $insertData      = [
            'origin_id'   => $origin_id,
            'game_id'     => $game_id,
            'match_id'    => (string)$rel_match_id,
            'round'       => (string)$round,
            'match_data'  => $match_data,
            'type'        => $type,
            'socket_time' => $socket_time
        ];
        $MatchLivedModel = new MatchLived();
        $MatchLivedModel->setAttributes($insertData);
        if ($MatchLivedModel->save()) {
            $redisStatus = $this->getAutoStatusByRedis($origin_id, $rel_match_id);
            if ($redisStatus){
                if ($redisStatus == 1) {
                    $auto_status = true;
                } else {
                    $auto_status = false;
                }
            }else{
                $db_status = MatchService::getMatchConfigAndHotDataByLog($origin_id, $game_id, $rel_match_id, $socket_time);
                if ($db_status == 1) {
                    $auto_status = true;
                } else {
                    $auto_status = false;
                }
                $this->redis->set('kafka:autoStatus:' . $origin_id . ':' . $rel_match_id,$db_status,60);
            }

            $socket_id   = $MatchLivedModel->id;
            $modelMatchOffset = new MatchLivedDatasOffset();
            $modelMatchOffset->setAttribute('data_id',(int)$socket_id);
            $modelMatchOffset->setAttribute('offset',(string)$this->offset);
            $modelMatchOffset->setAttribute('topic',(string)$this->topic);
            $modelMatchOffset->setAttribute('help_field',(string)$this->kafka_time);
            $modelMatchOffset->save();
            if ($task == 1 && in_array($game_id, [1, 2, 3]) && $auto_status) {
                //添加到队列
                $insertData['socket_id']     = $socket_id;
                $newInsertData               = @$insertData;
                $newInsertData['match_data'] = @json_decode($match_data, true);
                $api_server    = null;
                $socket_server = null;
                $taskType      = 4;
                if ($game_id == 2) {
                    $taskType = '9002';
                    if ($origin_id == 4||$origin_id == 9||$origin_id == 3) {//玩家的是ws to api
                        $socket_server = 'order_lol_' . $rel_match_id;
                    } else {//现在是有3 ps
                        $api_server    = 'order_lol_' . $rel_match_id;
                        //$socket_server = 'order_lol_socket_' . $rel_match_id;
                    }
                }
                if ($game_id == 1) {
                    if ($origin_id == 7||$origin_id == 9) {
                        $socket_server = 'order_hltvcsgo_' . $rel_match_id;
                    } else {
                        //origin_id 3
                        $api_server    = 'order_csgo_' . $rel_match_id;
                        $socket_server = 'order_csgo_' . $rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($game_id == 3) {
                    if ($origin_id == 9) {
                        $socket_server = 'order_dota_' . $rel_match_id;
                    }
                    $taskType = '9002';
                }
                if ($api_server) {
                    //设置队列
                    $item = [
                        "tag"          => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $game_id),
                        "type"         => Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
                        "batch_id"     => date("YmdHis"),
                        "params"       => $newInsertData,
                        "redis_server" => $api_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }
                if ($socket_server) {
                    $item = [
                        "tag"          => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                            $origin_id, Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $game_id),
                        "type"         => Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                        "batch_id"     => date("YmdHis"),
                        "params"       => $newInsertData,
                        "redis_server" => $socket_server
                    ];
                    TaskRunner::addTask($item, $taskType);
                }

            }
        } else {
            echo '插入数据库错误';
            return false;
//            throw new BusinessException($MatchLivedModel->getErrors(), '插入数据库错误');
        }
        return true;
    }






}
