<?php

use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;

class LogGeneratorTest extends PHPUnit\Framework\TestCase
{

    public function testAdd()
    {
        $a=100;
        $this->assertEquals(
            '100',
            $a
        );
        $lg=new LogGenerator();
        $lg->addInfo('funspark','2020-08-19 02:11:31','2020-08-19 02:13:31','127.0.0.7','test0826',1);

    }

    public function testFileAdd(){
        $ip="test0828";
        $file =  "D:\\test\\logs\\508\\forZe vs K23 de_nuke1.log";
        $url =   "http://123.56.138.90:8080/v1/receive/receive/debug";
        $a=100;
        $this->assertEquals(
            '100',
            $a
        );
        $lg=new LogGenerator();
        $lg->addFileInfo($ip,$file,$url,10);
    }
    public function testRuns()
    {
        $taskId=144283;
        \app\modules\task\services\TaskRunner::run(229552);
    }
    public function testFiveHotCsgoMatch()
    {
        $id = 226028;
        $id = 20288;
        $id = 191481;
        $id = 226010;

        $taskInfo = TaskInfo::find()->where(['id'=>$id])->asArray()->one();

        \app\modules\task\services\hot\five\FiveHotCsgoMatch::run('',$taskInfo);
    }

    public function testLogHello()
    {
        $id = 226028;
        $id = 20288;
        $id = 191481;
        $id = 226010;
        $obj =   \app\commands\LogController::test();

    }

    public function testCsgoLogUCC()
    {
        $id = 226010;
        $id = 422777;
//        $taskInfo = TaskInfo::find()->where(['id'=>$id])->asArray()->all();
        $taskInfo['log']='08/19/2020 - 11:32:29.000 - "mou 1xbet<37><STEAM_1:1:26339383>" switched from team <CT> to <TERRORIST>
08/19/2020 - 11:32:29.000 - "n0rb3r7 1XBET<53><STEAM_1:0:131088388>" switched from team <CT> to <TERRORIST>
08/19/2020 - 11:32:29.000 - "facecrack x LUKOIL<43><STEAM_1:1:57871072>" switched from team <TERRORIST> to <CT>
08/19/2020 - 11:32:29.000 - "Keoz 1XBET<41><STEAM_1:0:69039258>" switched from team <CT> to <TERRORIST>"
08/19/2020 - 11:32:29.000 - "FL1T x LUKOIL<42><STEAM_1:1:17775886>" switched from team <TERRORIST> to <CT>
08/19/2020 - 11:32:29.000 - "almazer x LUKOIL<46><STEAM_1:1:41891152>" switched from team <TERRORIST> to <CT>
08/19/2020 - 11:32:29.000 - "nealan 1xBET<47><STEAM_1:0:46888525>" switched from team <CT> to <TERRORIST>
08/19/2020 - 11:32:29.000 - "Jerry x VulkanBET<51><STEAM_1:0:32911214>" switched from team <TERRORIST> to <CT>
08/19/2020 - 11:32:29.000 - "kade0 1xBET<49><STEAM_1:1:171557725>" switched from team <CT> to <TERRORIST>
08/19/2020 - 11:32:29.000 - "xsepower x VulkanBET<50><STEAM_1:0:56007113>" switched from team <TERRORIST> to <CT>';

        $res = CsgoLogUCC::dealWithRecord($taskInfo);
        print_r($res);
    }

    public function testFiveHotCsgoMatchWs()
    {
        $res = FiveHotCsgoMatchWs::run('','');
        $this->assertEquals(
            1,
            1
        );

    }



}