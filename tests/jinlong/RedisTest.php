<?php

use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskMQConsumer;
use app\modules\task\services\TaskRunner;
use MQ\Model\Message;
use WBY\MQ\SDK\MQClientFactory;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
class RedisTest extends PHPUnit\Framework\TestCase
{



    public function testRedis()
    {
        $redis = new Redis();
//        $redis->connect('123.56.138.90',6379);
//        $redis->auth('BF6F0DAE9FACC174707AB44E303D0968E4E517A89BBDD');
        $redis->connect('192.168.4.21',6379);
        $redis->hGet('192.168.4.21',6379);
//        $redis->sAdd('ws:508:history:battle:0:round:0:player:STEAM_1:1:17775886:grenades','hegrenade');
//        $redis->sAdd('ws:508:history:battle:0:round:0:player:STEAM_1:1:17775886:grenades','hegrenade');
        $this->assertEquals(
            1,
            1
        );

    }
    public function testFiveHotCsgoMatchApi()
    {
        $id = 226028;
        $id = 20288;
        $id = 191481;
        $id = 327019;//tiger
//        $id = 277892;//dev
//        $id = 277892;//dev
        $id = 12351;//master

        $taskInfo = TaskInfo::find()->where(['id'=>$id])->asArray()->one();

        $res=\app\modules\task\services\hot\five\FiveHotCsgoMatchWsToApi::run('',$taskInfo);
//        print_r($res);exit;
        $this->assertEquals(
            1,
            1
        );
    }

    /**
     * csgoRefresh
     * 注意要修改ip 时间 和删除的redis_key
     * @throws \yii\db\Exception
     */
    public function testAddRefresh()
    {
        $a = \app\modules\task\services\grab\bayes2\BayesBase::getAuth2();
$c=1;

        return false;
        $info=[
            'tag'=>'event_ws.5eplay....refresh',
            'params'=>[
                'where'=>[
                    'and',
                    ['in','ip_address' , ['test092723','test092722']],
                    ['>=', 'created_at', "2020-09-28 04:43:05"]
                ],
                'del_redis_keys'=>'ws:508:',
                'match_id'=>'508',
            ],
            'redis_server'=>'order_csgolog',
        ];

        \app\modules\task\services\TaskRunner::addTask($info,9001);
        $this->assertEquals(
            1,
            1
        );

    }



    public function testTest1(){
        $mWs = new FiveHotCsgoMatchWs();
        $mWs->getSortingLog('47.89.30.28',3063);

        $this->assertEquals(
            1,
            1
        );
    }
    public function testTest(){
        $taskInfo  = TaskInfo::find()->andWhere(['tag'=>'event_ws.5eplay....refresh'])->andWhere(['<','status',3])->andWhere(['>=','created_time',date('Y-m-d 00:00:00')])
//            ->createCommand()->getRawSql();
            ->asArray()->one();
        if (empty($taskInfo)){
            $a =1;
        }else{
            $a =2;
        }
    }
    public function testRedisEval(){

        $redis = new Redis();
        $redis->connect(env('REDIS_HOST'));
        $redis->auth(env('REDIS_PASSWORD'));

        $redis->select(env('REDIS_DATABASE'));
        $oldMatchId =571936;
        $newMatchId =57193;
        $pre = "hltv_ws:$oldMatchId:";
        $pre_new = "hltv_ws:$newMatchId:";
        // Have scan retry
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        $it = NULL;
        while ($arr_keys = $redis->scan($it, "$pre*", 100)) {
            foreach ($arr_keys as $k => $v) {
                //     * - string: Redis::REDIS_STRING
                //     * - set:   Redis::REDIS_SET
                //     * - list:  Redis::REDIS_LIST
                //     * - zset:  Redis::REDIS_ZSET
                //     * - hash:  Redis::REDIS_HASH
                $type       = $redis->type($v);
                $vArr       = explode(':', $v);
                $newK       = array_slice($vArr, 2);
                $newKString = implode(':', $newK);
                $newNewK    = $pre_new . $newKString;

                if ($type == 1) {
                    $res1 = $redis->get($v);
                    $redis->del($newNewK);
                    $redis->set($newNewK, $res1);
                } elseif ($type == 3) {
                    $res2 = $redis->lRange($v, 0, -1);
                    $redis->del($newNewK);
                    foreach ($res2 as $k1 => $v1) {
                        $redis->rPush($newNewK, $v1);
                    }
                } elseif ($type == 5) {
                    $res3 = $redis->hGetAll($v);
                    $redis->del($newNewK);
                    $redis->hMSet($newNewK, $res3);
                }
            }

        }
        $this->assertEquals(
            1,
            1
        );

    }

    public function testFiveHotCsgoMatchWs()
    {
        $testEventId = 388472;//round_time_frozen_start

        $aa          = ReceiveData5eplayFormatEvents::find()->where(['in', 'id', $testEventId])->asArray()->all();
        foreach ($aa as $k => $v) {
            $v['event_id'] = $v['id'];
            $dd['params']  = json_encode($v);
            $res           = app\modules\task\services\hot\five\FiveHotCsgoMatchWs::run('', $dd);
            print_r($res);
        }

        $this->assertEquals(
            1,
            1
        );

    }





    /**
     * 从原始log重跑事件
     * @throws \app\rest\exceptions\BusinessException
     */
    public function testTranslateLogsToEvents(){
        $list = \app\modules\data\models\ReceiveDataFunspark::find()->where(['ip_address'=>'test091601'])->asArray()->all();
        foreach ($list as $k=>$v){
            CsgoLogUCC::dealWithRecord($v);
        }
        $this->assertEquals(
            1,
            1
        );
    }
    public function testRunner(){

        \app\modules\task\services\TaskRunner::run(8342988);//13728

        $this->assertEquals(
            1,
            1
        );
    }



    public function testFiveHotCsgoMatchWs2()
    {

        $where = [
            'and',
            ['in', 'ip_address', 1],
            ['>=', 'created_at', 2]
        ];
        if (1){
            $where[]= ['<=','created_at', date('Y-m-h H:i:s',strtotime('2020-10-04 19:27:25.324')+600)];
        }
        exit;

        $testEventId = 407145;
        $testEventId = 404590;
        $testEventId = 404726;
        $testEventId = 406811;
        $testEventId = 401556;
        $testEventId = 402489;
        $testEventId = 826607;
        $aa = ReceiveData5eplayFormatEvents::find()->where(['in','id',$testEventId])->asArray()->all();
        foreach ($aa as $k => $v) {
            $list['params'] = json_encode($v);
            $res = app\modules\task\services\hot\five\FiveHotCsgoMatchWs::run('',$list);
            print_r($res);
        }
        exit;


        $list = ReceiveData5eplayFormatEvents::find()->where(['ip_address'=>'test090510'])->asArray()->offset(0)->limit(5000)->all();
        foreach ($list as $k => $v) {
            $list['params'] = json_encode($v);
            $res = app\modules\task\services\hot\five\FiveHotCsgoMatchWs::run('',$list);
//            print_r($res);
        }
//        $runnerId = 230737;
//        \app\modules\task\services\TaskRunner::run(230737);//13728
        $this->assertEquals(
            1,
            1
        );

    }











    public function testReadFolderJson(){
        $date = "D:\\test\\logs\\5e\\live-csgo-prodb-map-0797bbee-6e14-4628-9c1c-0341a8a60c63";
//        $date = "D:\\test\\logs\\5e\\live_csgo_carp_match";
        //1、首先先读取文件夹
        $temp=scandir($date);
        $unsetKey1 = array_search('.',$temp);
        if (!empty($unsetKey1)||$unsetKey1===0){
            unset($temp[$unsetKey1]);
        }
        $unsetKey2 = array_search('..',$temp);
         if ($unsetKey2){
             unset($temp[$unsetKey2]);
         }
         $newTemp = [];
         foreach ($temp as $k =>$v){
             $newV = explode('.',$v);
             if (is_numeric($newV[0])){
                 $newTemp[$newV[0]] = $v;
             }
         }
         ksort($newTemp);
         if (in_array('additionalData.json',$temp)){
             array_push($newTemp,'additionalData.json');
         }
        //遍历文件夹
        foreach($newTemp as $v){
            $a=$date.'/'.$v;
            if(is_dir($a)){//如果是文件夹则执行
                if($v=='.' || $v=='..'){//判断是否为系统隐藏的文件.和..  如果是则跳过否则就继续往下走，防止无限循环再这里。
                    continue;
                }
                print_r($a).PHP_EOL;  //把文件夹红名输出

                list_file($a);//因为是文件夹所以再次调用自己这个函数，把这个文件夹下的文件遍历出来
            }else{
                $json = file($a);
                $matchModel = new \app\modules\task\models\Test1Match();
                $matchModel->setAttribute('content',$json[0]);
                $matchModel->setAttribute('type','all');
                $matchModel->setAttribute('create_at',date('Y-m-d H:i:s'));
                $matchModel->save();
                echo $a.PHP_EOL;
            }

        }
        $this->assertEquals(
            1,
            1
        );
    }





}