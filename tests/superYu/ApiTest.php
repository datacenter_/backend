<?php

use app\modules\common\services\Consts;
use app\modules\task\services\api\GetApi\MatchService;
use app\modules\task\services\api\SetApi;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\commands\ApiController;
use app\modules\common\services\EsService;
use app\modules\task\services\api\GetApi;

/**
 *
 */
class ApiTest extends PHPUnit\Framework\TestCase
{
    function unparse_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }
    public function testFiveHotCsgoMatchWs()
    {
//        $url = 'https://img.elementsdata.cn/sdfdsf/oss/els/MjEwMzI4/?sdfMTk0MDQ5540.png';
//        $arr = parse_url($url);
//        $str = $this->unparse_url($arr);


//        $a = GetApi\BattleService::getBattleDetail(2543);
        // 搜索redis - key
//        $redisKeys = \app\modules\task\services\api\ApiBase::searchKey(['2020-12-0']);
//        foreach ($redisKeys as $key){
//            \app\modules\task\services\api\ApiBase::delRedis($key);
//        }
//        $currentDate = date('Y-m-d H:i:s',time());

//        $aa = GetApi\TournamentService::getTournamentDetail(245);
//        $aaa = $aa;
//        $battleInfo = BattleService::getBattleDetail(2533);
//        $matchInfo = MatchService::getMatchDetail(null,['page' => 1,'per_page' => 20],2);
//        $a = BattleService::getBattleDetail(2533);
        RenovateService::refurbishApi('match',3063);
//        RenovateService::refurbishApi(['battle','csgo_battle','match'],551);
//        ApiController::actionMatchHotRefresh();
//        RenovateService::getOngoingMatch();
//        RenovateService::refurbishApi('tournament');
//        RenovateService::refurbishApi('team',[1,2,3,4,5,6,7,8,9,10]);
//        RenovateService::refurbishApi(['game','organization'],3063);

        // websocket_match_data_api.3.websocket_match_data_api.add.1.
//        \app\modules\task\services\TaskRunner::run(697521);
//        $matchId = 511;
//        // 循环添加任务
//        $item = [
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_ES_HOT_REFRESH,
//                "", Consts::RESOURCE_TYPE_MATCH, "", "", ""),
//            "type" => "",
//            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB, "", Consts::RESOURCE_TYPE_MATCH),
//            "params" => [
//                'match_id' => $matchId
//            ],
//        ];
//        TaskRunner::addTask($item, 3);



        // game
//        SetApi\GameSet::setGame(20);
        // team
//        RenovateService::refurbishApi('team');
//        SetApi\TeamSet::setTeam();
        // player
//        SetApi\PlayerSet::setPlayer();
        // csgo
//        SetApi\MetadataCsgoSet::setMap(1);
//        SetApi\MetadataCsgoSet::setWeapon(20);
        // dota
//        SetApi\MetadataDotaSet::setHero(1);
//        SetApi\MetadataDotaSet::setItems(1);
        // lol
//        SetApi\MetadataLolSet::setChampions(1);
//        SetApi\MetadataLolSet::setItems(20);
//        SetApi\MetadataLolSet::setSummonerspell(1);
//        SetApi\MetadataLolSet::setRunes(19);
    }

}
