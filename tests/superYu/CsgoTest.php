<?php

use app\modules\data\models\ReceiveDataFunspark;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\operation\OperationRefreshHotInfo;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

use app\modules\match\controllers\BattleController;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\models\ReceiveData5eplayFormatEvents;

/**
 *
 */
class CsgoTest extends PHPUnit\Framework\TestCase
{
    public function testFiveHotCsgoMatchWs()
    {
        $testEventId = 281741 ;//battle up coming
        $testEventId = 281810 ;//battle start
        $testEventId = 278675 ;//join
        $testEventId = 278692 ;//player_quit
        $testEventId = 278687 ;//lost goods 以前的返回要修改
        $testEventId = 278687 ;//throw goods 以前的返回要修改
        $testEventId = 278687 ;//get goods 以前的返回要修改
        $testEventId = 285561 ;//round end
        $testEventId = 281939 ;//round_win_type

        $testEventId = 289531 ;//player_joined

//        $testEventId = 269812 ;//get_goods
//        $testEventId = 278775 ;//money_change
//        $testEventId = 324777 ;//player_joined
        $testEventId = 324777 ;//battle_start
        $testEventId = 344264 ;//player_joined
//        $testEventId = 344321 ;//player_joined
        $testEventId = 345975 ;//player_joined
//        $testEventId = 351945 ;//round_start
        $testEventId = 352954 ;//round_win_type
        $testEventId = 321707 ;
        $a = ReceiveData5eplayFormatEvents::find()->where(['id'=>$testEventId])->asArray()->one();
        $a['event_id'] = $a['id'];
        $list['params'] = json_encode($a);

        $res = app\modules\task\services\hot\five\FiveHotCsgoMatchWs::run('',$list);
        print_r($res);
//        $list = ReceiveData5eplayFormatEvents::find()->where(['ip_address'=>'test090102'])->asArray()->offset(4000)->limit(10000)->all();
//        foreach ($list as $k => $v) {
//            $res = app\modules\task\services\hot\five\FiveHotCsgoMatchWs::run($v['id']);
//            print_r($res);
//        }
//        $runnerId = 230737;
//        \app\modules\task\services\TaskRunner::run(230737);//13728
        $this->assertEquals(
            1,
            1
        );

    }


    public function testRunTaskTwo(){
        $record=ReceiveDataFunspark::find()->where(['id'=>'2090'])->one();
        CsgoLogUCC::dealWithRecord($record);
    }
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(3839734);
//        \app\modules\task\services\TaskRunner::run(60454);
//        BattleController::actionDel();

        die;
        $filename = "./log.log";
        $maxProcess = 2;// 分配几个线程

        $length = $this->length($filename);
        $singleProcessLength = ceil($length / $maxProcess);
        // L 08/08/2020 - 11:36:06: "foxL<31><STEAM_1:0:969768><TERRORIST>" [809 2596 95] killed "GeT_RiGhT<16><STEAM_1:0:10885595><CT>" [1552 1646 65] with "awp"
        // L 08/08/2020 - 11:36:35: Starting Freeze period
        $ah = [];
        foreach(range(0,$maxProcess - 1) as $index)
        {
            // 多线程采用多线程的方式创建，这里采用yield回调。
            foreach ($this->processRead($filename, $index, $singleProcessLength) as $value)
            {
//                echo $index;
                // $value为每一行的内容，处理后释放
                $log = $value;
//                $reg = '/^.*"f0rest<.*><STEAM_1:0:46862><(CT|TERRORIST)>"\s\[.*\]\skilled\s\".*$/';
//                $reg = '/^.*"f0rest<.*><STEAM_1:0:46862><(CT|TERRORIST)>"\s\[.*\]\sattacked\s\".*\(hitgroup\s"(.*head.*)"\)$/';
                $reg = '/^.*"f0rest<.*><STEAM_1:0:46862><(CT|TERRORIST)>"\s\[.*\]\skilled\s".*".*\((.*headshot.*)\)$/';
                preg_match($reg, $log, $data);
                if(!empty($data)){
                    $ah[] = $data;
                }
            }
        }
        print_r($ah);

    }
    public function testRunHotrefesh()
    {
        ReceiveDataFunspark::updateAll(['is_handle'=>2],['>','id','0']);
        \app\modules\task\services\logformat\CsgoFunspark::getListAndDo();
//         /private/var/www/work/game_center_server/commands/HotController.php;
//        return OperationRefreshHotInfo::refreshList();
    }




    //获取文件大小
    //首先第一步，就是获取整个文件的体积大小，然后计算每个线程应该负责处理的一部分内容。
    public function length($filename)
    {
        $handle = fopen($filename, "rb");
        $currentPos = ftell($handle);
        fseek($handle, 0, SEEK_END);
        $length = ftell($handle);
        fseek($handle, $currentPos);
        // $length 文件总长度
        return $length;
    }
    // 线程负责读取的内容
    public function processRead($filename, $index, $singleProcessLength)
    {
        $fh = fopen($filename, 'r');

        $beginPos = $index * $singleProcessLength;
        //结束位置=线程序列*线程处理数据长度+线程处理数据 - 1 (长度转指针，实际结束指针小于结束长度)
        $endPos = $index * $singleProcessLength + $singleProcessLength - 1;

        fseek($fh, $beginPos);
        echo '线程:' . $index . '，起始位置:' . $beginPos . '，结束位置:' . $endPos . PHP_EOL;
        //移动到上个\n 以便首次顺利获取整行内容
        while (fseek($fh, -1, SEEK_CUR) === 0) {
            if (fread($fh, 1) == "\n" || ftell($fh) <= 0) {
                break;
            }
            fseek($fh, -1, SEEK_CUR);
        }
        echo '线程:' . $index . '，移动完毕!!!!!' . PHP_EOL;

        //整行读取数据
        //结束时位置超过预计结束位置是正常状况，fgets 读取一整行内容
        //预计结束位置可能在行内，所以产生不同结果。
        while (ftell($fh) <= $endPos && !feof($fh)) {
        yield $raw = fgets($fh);
        }
        echo '进程' . $index . '结束时 指针位置:' . ftell($fh) . ', 应该到:' . $endPos . PHP_EOL;
        fclose($fh);
    }




}