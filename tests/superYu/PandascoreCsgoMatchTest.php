<?php

use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\match\controllers\MatchController;;

/**
 *
 */
class PandascoreCsgoMatchTest extends PHPUnit\Framework\TestCase
{
    //测试添加socket数据
    public function testAdd3()
    {
        $map = [
            'origin_id' => 3,
            'match_id' => 572305,
//            'match_id' => 569314,
//            'match_id' => 569316,
            'game_id' => 1,
//            'type' => 'events'
//            'type' => 'no'
        ];
        $data = \app\modules\match\models\MatchLived::find()
            ->orderBy('id ASC')
            ->where($map)
//            ->andwhere(['<','id','139957'])
//            ->andwhere(['>=','id','138691'])
//            ->where(['=','id','133957'])

            ->andwhere(['=','id','1059047'])
//            ->andwhere(['=','id','777650'])
//            ->andwhere(['=','id','798814'])
//            ->andwhere(['>','id','193385'])
//            ->andwhere(['>=','id','703611'])
//            ->andwhere(['<=','id','719492'])
            ->select('id,match_data,type,round,match_id,game_id,origin_id')
            ->asArray()
            ->all();
        $socket_time = date('Y-m-d H:i:s');
        foreach($data as $key => $value){
            $insertData = [
                'origin_id' => $value['origin_id'],
                'game_id' => $value['game_id'],
                'match_id' => $value['match_id'],
                'round' => $value['round'],
                'match_data' => $value['match_data'],
                'type' => $value['type'],
                'socket_time' => isset($value['socket_time']) ? $value['socket_time']:$socket_time
            ];
            if(!isset($insertData['socket_time'])){
                $insertData['socket_time'] = date('Y-m-d H:i:s');
            }
            $newInsertData = @$insertData;
            $newInsertData['match_data']= json_decode($value['match_data'],true);
            $redis_server = 'order_csgo';
            //再插入一次 用户restapi
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA_RESTAPI,
//                "tag" => QueueServer::getTag(QueueServer::QUEUE_ORIGIN_WEBSOCKET_MATCH_DATA,
                    $value['origin_id'], \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI, "add", $value['game_id']),
//                    $value['origin_id'], \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA, "add", $value['game_id']),
                "type" => \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA_RESTAPI,
//                "type" => \app\modules\common\services\Consts::RESOURCE_WEBSOCKET_MATCH_DATA,
                "batch_id" => date("YmdHis"),
                "params" => $newInsertData,
                "redis_server"=>$redis_server
            ];
            TaskRunner::addTask($item, 9001);
            sleep(1);
        }
        return "执行完毕!!!";
    }
    public function testRunTaskCsgoApi()
    {
//        app\modules\match\services\MatchService::refreshMatchInfo(739,3,'csgo');
//        app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi::run();
        // Api - no
//        \app\modules\task\services\TaskRunner::run(1853277);
        \app\modules\task\services\TaskRunner::run(2075258);
//        \app\modules\task\services\TaskRunner::run(1851715);
//        \app\modules\task\services\TaskRunner::run(223005);
        // Ws - events
//        \app\modules\task\services\TaskRunner::run(531219);
//        \app\modules\match\controllers\LivedController::add();
    }
    public function testRunTask()
    {
//        \app\modules\task\services\TaskRunner::run(138);
        \app\modules\task\services\TaskRunner::run(18);
//        \app\modules\task\services\hot\pandascore\PandascoreCsgoMatch::run([],[]);
        // 添加pandascore的抓取
//        TaskRunner::run(97);
    }
    public function testRunTask2()
    {
        $data = array(
            0 => array(
                'order' => 1
            ),
        );
        app\modules\match\services\battle\LolBattleService::setStatics(180,$data);
    }
    public function testAb()
    {
        MatchController::actionRealtimeSet();
    }
    function testRefresh()
    {
        \app\modules\task\services\operation\OperationRefreshHotInfo::refreshList();
    }
}