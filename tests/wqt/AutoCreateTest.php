<?php

class AutoCreateTest extends PHPUnit\Framework\TestCase
{
    public function testMatchInfo()
    {

    }

    public function testTournamentListInfo()
    {
        $sql='SELECT `std_trmt`.*,
       `trmt_rel`.`master_id`         AS `main_tournament_id`,
       `config_create`.`origin_id`    AS `create_origin_id`,
       `config_create`.`update_type`  AS `create_update_type`,
       `config_binding`.`origin_id`   AS `binding_origin_id`,
       `config_binding`.`update_type` AS `binding_update_type`
FROM `standard_data_tournament` `std_trmt`
         LEFT JOIN `data_standard_master_relation` `trmt_rel`
                   ON std_trmt.id = trmt_rel.standard_id and trmt_rel.resource_type = "tournament"
         LEFT JOIN `data_update_config_default` `config_create`
                   ON config_create.game_id = std_trmt.game_id and config_create.slug = "match.create_data"
         LEFT JOIN `data_update_config_default` `config_binding`
                   ON config_binding.game_id = std_trmt.game_id and config_binding.slug = "match.binding_data"
         LEFT JOIN `tournament` `m_trmt` ON m_trmt.id = trmt_rel.master_id
WHERE (`trmt_rel`.`id` > 0)
  AND (`m_trmt`.`status` <> 3)';
        $relId='3395';
        $sql=$sql . ' and std_trmt.rel_identity_id='. $relId;
        echo $sql;

    }

    public function testDetail()
    {
        $params=[
            'rel_identity_id'=>3395,
            'origin_id'=>3
        ];
        $info=\app\modules\task\services\crontabTask\AutoCreateMatch::getStandardMatchInfosByTournament($params);
        print_r($info);

    }
}