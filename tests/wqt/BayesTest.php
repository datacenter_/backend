<?php

use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\console\ExitCode;

class BayesTest extends PHPUnit\Framework\TestCase
{
    public function testTitles()
    {
        $info=\app\modules\task\services\grab\bayes\BayesBase::getCurlInfo('/title/',[]);
        print_r($info);
    }
    public function testMatch()
    {
        $info=\app\modules\task\services\grab\bayes\BayesBase::getCurlInfo('/match/',[
            "title"=>"3",
//            "ordering"=>"",
            "state"=>"pending",
            "page"=>1
        ]);
        print_r($info);
    }

    // 24905
    public function testDetail()
    {
        $info=\app\modules\task\services\grab\bayes\BayesBase::getCurlInfo('/match/24905',[]);
        print_r($info);
    }

    public function testMatchBeta()
    {
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"",""),
            "type"=>\app\modules\task\services\grab\bayes\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,'wqt'),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "title"=>"3",
//            "ordering"=>"",
                    "state"=>"pending",
                    "page"=>1
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
        return ExitCode::OK;
    }
}