<?php

use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMatchService;
use app\modules\data\services\DebugDataTodoService;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\TaskMQConsumer;
use MQ\Model\Message;
use WBY\MQ\SDK\MQClientFactory;

/**
 *
 */
class CsgoTest extends PHPUnit\Framework\TestCase
{
    public function testAd()
    {
        $info=\app\commands\MatchStatusChangeController::actionMatchHaveDetail();
        print_r($info);
    }
    public function testGetAbiosLol()
    {
        \app\modules\task\services\realTime\abios\AbiosSocketCsgo::run('live_csgo');
    }

    public function testSpt()
    {
        \app\modules\tournament\services\TournamentService::updateTeamSnapshot(12983,2,245,'tournament');
    }

    public function testCsgoInfo()
    {
        $matchId=452285;
        $action='/matches/452285';
        $url='/v2/matches/452285';
        $params=[
            'with'=>[
                'summary'
            ],
        ];
        $info=\app\modules\task\services\hot\abios\AbiosHotBase::getRestInfo($url,$params);
        print_r($info);
    }

    public function testBindingList()
    {
        $params = [
            'deal_status' => 2
        ];
        $list = \app\modules\data\services\binding\BindingTeamService::getList($params);
        print_r($list);
    }

    public function testAddinfo()
    {
        $name = "fazeaaa";
        $q = StandardDataTeam::find()->select("id,name")->Where(
            ['and',
                ["=", "game_id", 1],
                ["<>", "deal_status", 4],
                ['or',
                    ["like", "name", $name],
                    "locate(name,'" . $name . "')"
                ]
            ]
        );
        $sql = $q->createCommand()->getRawSql();
        $temRelInfo = $q->asArray()->all();
        if ($temRelInfo) {
        }
    }

    public function testTeamList()
    {
        $list = \app\modules\org\services\TeamService::getTeamList([]);
        print_r($list);
    }

    public function testRun()
    {
        $info = \app\modules\task\services\TaskRunner::run(100271);
    }

    public function testGetDetail()
    {
        $info = \app\modules\data\services\binding\BindingTeamService::getDetail(4118);
        print_r($info);
    }

    public function testDebud()
    {
        $params = json_decode('{"origin_type":"hltv","resource_type":"team","change_info":{"name":null,"game_id":null,"clan_id":null,"country":1,"full_name":null,"short_name":null,"alias":null,"slug":null,"logo":null,"region":null,"world_ranking":null,"total_earnings":null,"average_player_age":null,"history_players":null,"introduction":null,"introduction_cn":null,"players":null,"deleted":null},"standard_id":"4696"}', true);
        DebugDataTodoService::debugAddToDo($params);
    }

    public function testAddAndBinding()
    {
        BindingMatchService::addAndBinding(35340,Consts::USER_TYPE_ADMIN,0);
    }

    public function testAddAndBindingSs()
    {
        \app\modules\data\services\binding\BindingTournamentService::addAndBinding(8394,Consts::USER_TYPE_ADMIN,0);
    }

    public function testSlug()
    {
        $a = 'NAME123ABC中文`~!@#$%^&*_-+={}[]:;"\'<,>.?/|\~·！@#￥%……&*（）——-+={}【】：；“”‘’《》，。？、|、  end';
        $preg = '/[a-zA-Z0-9\s_-]/i';
        preg_match_all($preg, $a, $matchAll);
        print_r($matchAll);
        $str = implode("", $matchAll[0]);
        $str = str_replace(' ', '_', $str);
        $str = str_replace('-', '_', $str);
        $str = strtolower($str);
        print_r($str);
    }

    public function testTeamAdd()
    {
        // team添加
        $attributes = json_decode('{"core_data":{"name":"","organization":"","game":1,"country":"","full_name":"","short_name":"","alias":"","slug":"ee","image":"http://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/type_logo/154e45a869e1ce68ea6eeffb6dc5cc71/296d3f1f21084795d6ba7c0155c96a27.png"},"base_data":{"region":"","world_ranking":"","total_earnings":"","average_player_age":"","history_players":"","introduction":"","introduction_cn":""},"players":[]}', true);
        TeamService::setTeam($attributes, Consts::USER_TYPE_ADMIN, 0);
    }

    public function testTeamEdit()
    {
        //team修改
        $attributes = json_decode('{"id":"4204","core_data":{"id":"4204","name":"","organization":"","game":"1","country":"","full_name":"","short_name":"","alias":"","slug":"ee","image":"http://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/type_logo/154e45a869e1ce68ea6eeffb6dc5cc71/296d3f1f21084795d6ba7c0155c96a27.png","cuser":"1","deleted":"2","created_at":"2020-09-03 18:23:01","modified_at":"2020-09-03 18:25:42","deleted_at":null},"base_data":{"id":"4200","team_id":"4204","steam_id":null,"region":"","world_ranking":"","ago_30":null,"total_earnings":"","average_player_age":"","history_players":"","create_team_date":null,"close_team_date":null,"introduction":"","introduction_cn":"","created_at":"2020-09-03 18:23:01","modified_at":"2020-09-03 18:23:01"},"team_player":[]}', true);
        TeamService::setTeam($attributes, Consts::USER_TYPE_ADMIN, 0);
    }

    public function testTeamSourceAdd()
    {
        // 数据源新增
        $config = [
            'endPoint' => env('ONS_endPoint'),
            'accessKey' => env('ONS_accessKey'),
            'accessId' => env('ONS_accessId'),
        ];
        $client = MQClientFactory::getRocketMQClient($config);

        $instanceId = env('ONS_instanceId');
        $topicName = 'priority_list';
        $groupId = 'GID_business';
        $consumer = $client->getConsumer($instanceId, $topicName, $groupId);

        $subscriber = new TaskMQConsumer($consumer, 3, 3);
        /** @var Message[] $message */
        $subscriber->daemon();
    }

    public function testTeamSourceEdit()
    {
        // 数据源修改
        $json = '{"origin_type":"hltv","resource_type":"team","change_info":{"name":null,"game_id":null,"clan_id":null,"country":1,"full_name":null,"short_name":null,"alias":null,"slug":null,"logo":null,"region":null,"world_ranking":null,"total_earnings":null,"average_player_age":null,"history_players":null,"introduction":null,"introduction_cn":null,"players":null,"deleted":null},"standard_id":"4188"}';
        $params = json_decode($json, true);
        DebugDataTodoService::debugAddToDo($params);
    }

}