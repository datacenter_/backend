<?php
/**
 *
 */

class PandaCsgoTest extends PHPUnit\Framework\TestCase
{
    public function testWsApi()
    {
        // 循环，生成事件
        $list = \app\modules\match\models\MatchLived::find()->where(['game_id' => 1, 'match_id' => 567926])->orderBy('socket_time')->limit(500000)->asArray()->all();
        foreach ($list as $k => $v) {
            $params = $v;
            $params['match_data'] = json_decode($params['match_data'], true);
            print_r($params);
            $taskInfo = [
                'params' => json_encode($params)
            ];
            $info = \app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi::run([], $taskInfo);
        }
    }

    public function testWsApis()
    {
        // 循环，生成事件
        // 删除关系，清空缓存 删除battle 重跑
        $relMatchId=567926;

        $v = \app\modules\match\models\MatchLived::find()->where(['game_id' => 1, 'match_id' => $relMatchId])->orderBy('socket_time')->limit(500000)->asArray()->one();
        $matchId=\app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi::getMainIdByRelIdentityId('match', $relMatchId, $v['origin_id']);
        $sqlDelRel="delete from match_battle_relation where match_id=".$matchId;
        $sqlDelBattle="delete from `match_battle` where `match`=".$matchId;
        \Yii::$app->getDb()->createCommand($sqlDelRel)->execute();
        \Yii::$app->getDb()->createCommand($sqlDelBattle)->execute();
        $params = $v;
        $params['match_data'] = json_decode($params['match_data'], true);
        print_r($params);
        $taskInfo = [
            'params' => json_encode($params)
        ];
        $info = \app\modules\task\services\wsdata\pandascore\PandascoreCsgoApi::run(['ext_type2' => 'refresh'], $taskInfo);
    }
}