<?php


use app\modules\file\services\UploadService;

class WqtCommonTest extends PHPUnit\Framework\TestCase
{
    public function testAddImg()
    {
        $imgUri=dirname(dirname(__DIR__))."/mp/"."WechatIMG8.png";
        // 文件是否存在，如果不存在，用默认的
        if(!file_exists($imgUri)){
            return 'https://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/logo_default/csgo.png';
        }
        $info=pathinfo($imgUri);
        $dst = 'static_up/' . md5(time()) . '/'  . md5($info['filename']).'.'.$info['extension'];
        $url = UploadService::upload($dst,$imgUri);
        print_r($url);
        return $url;
    }
    public function testImg()
    {
        $connection  = Yii::$app->db;
        $sql     = "select * from dota2_add where id>70665 order by id limit 100000";
        $command = $connection->createCommand($sql);
        $res     = $command->queryAll();
        foreach($res as $val){
            // 上传头像
            $url=$this->addImg($val['name']);
            // 添加账号
            $teamInfo=[
                'core_data'=>[
                    'name'=>$val['name'],
                    'game'=>'3',
                    'country'=>$this->getCountryByString($val['country']),
                    'full_name'=>$val['full_name'],
                    'short_name'=>$val['short_name'],
                    'alias'=>$val['alias'],
                    'image'=>$url,
                ],
                'base_data'=>[
                    'region'=>$this->getCountryByString($val['region']),
                    'total_earnings'=>$val['total_earnings'],
                ],
                'team_player'=>[]
            ];
            \app\modules\org\services\TeamService::setTeam($teamInfo,2,0);
        }
    }

    private function getCountryByString($string)
    {
        $country=\app\modules\common\models\EnumCountry::getNationalityByKey('e_name',$string);
        if($country){
            return $country['id'];
        }
        return null;
    }

    private function addImg($name)
    {
        $imgUri=dirname(dirname(__DIR__))."/DOTA2/战队图标新/战队图标新/".$name.".png";
        // 文件是否存在，如果不存在，用默认的
        if(!file_exists($imgUri)){
            return 'https://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/logo_default/dota2.png';
        }
        $info=pathinfo($imgUri);
        $dst = 'static_up/' . md5(time()) . '/'  . md5($info['filename']).'.'.$info['extension'];
        $url = UploadService::upload($dst,$imgUri);
        return $url;
    }

}