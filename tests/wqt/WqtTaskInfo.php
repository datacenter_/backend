<?php

use app\modules\data\models\ReceiveDataFunspark;
use app\modules\match\services\StreamService;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\services\operation\OperationRefreshHotInfo;
use app\modules\tournament\services\TournamentService;
use yii\console\ExitCode;

class WqtTaskInfo extends PHPUnit\Framework\TestCase
{
    public function testRedisQueue()
    {
        \app\commands\ConsumerController::actionRedisRunnerOrder('csgo');
    }
    public function testInfo()
    {
        $line='08/09/2020 - 11:44:08.235 - "CJ-O-OB<"BC"><80><STEAM_1:1:198950207><CT>" left buyzone with [ weapon_knife weapon_usp_silencer weapon_awp kevlar(100) ]';
        \app\modules\task\services\logformat\CsgoFunspark::leftBuyZoneWith($line);
    }

    public function testGet()
    {
        StreamService::grabOrigin();
    }

    public function testAddRefresh()
    {
        $info=[
            'tag'=>'event_ws.5eplay....refresh',
            'params'=>[
                'where'=>[
                    'and',
                    ['ip_address' => '47.89.30.28'],
                    ['<', 'id', 366536],
                    ['>', 'created_at', "2020-09-28 12:30:00"]
                ],
            ],
            'redis_server'=>'order_csgolog',
        ];
        \app\modules\task\services\TaskRunner::addTask($info,9001);
    }

    public function testR()
    {
        \app\modules\task\services\TaskRunner::run(3994929);
    }

    public function testReg()
    {
        $line='06/25/2020 - 17:00:56.000 - "splashske<5><STEAM_1:0:21800><CT>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1" (headshot)';
        $reg='/^(.*) - "(.*)<5><(.*)><(CT|TERRORIST)>" [-794 -1160 -414] killed "xiaosaGe<6><STEAM_1:1:112788938><TERRORIST>" [-1576 -1114 -354] with "m4a1" (headshot)$/i';
        $regTest=preg_match_all($reg,$line,$matchAll);
        print_r($regTest);
    }
    public function testStrToTime()
    {
        $timeStr='07/23/2020 - 16:07:37';
        $timeStr=str_replace(' - ',' ',$timeStr);
        $info=strtotime($timeStr);
        $localTime=date('Y-m-d H:i:s',$info);
        print_r($localTime);
    }

    public function testE()
    {
        ReceiveDataFunspark::updateAll(['is_handle'=>2],['>','id','0']);
        \app\modules\task\services\logformat\CsgoFunspark::getListAndDo();
        return ExitCode::OK;
    }

    public function testRefresh()
    {
        return OperationRefreshHotInfo::refreshList();
    }
    public function testRiotgames()
    {
//        \app\modules\task\services\TaskRunner::run(2262);
         \app\modules\task\services\TaskRunner::run(406260);

//        \app\modules\task\services\grab\riotgames\Start::run();
//        \app\modules\task\services\TaskRunner::run(129897);
    }

    public function testRefreshSaishi()
    {
        $tournamentList=\app\modules\tournament\models\Tournament::find()->asArray()->all();
        foreach($tournamentList as $key=>$trmt){
            $teamList=\app\modules\tournament\models\TournamentTeamRelation::find()->where(['tournament_id'=>$trmt['id']])->asArray()->all();
            if($teamList){
                $teamIds=array_column($teamList,'team_id');
                TournamentService::updateTeamSnapshot(implode(",",$teamIds),1,$trmt['id'],'tournament');
            }
        }
        // 赛事刷新快照

    }

    public function testAutoRun()
    {
        $info=\app\modules\data\models\ReceiveDataFunspark::find()->where(['id'=>'342'])->one();
        \app\modules\task\services\logformat\CsgoFunspark::dealWithLine($info);
        return [];
    }
    public function testAd()
    {
        $lg=new LogGenerator();
        $lg->addInfo('funspark','2020-06-25 16:58:49','2020-06-25 17:52:29','127.0.0.2');
    }

    public function testGetMainIdByRelIdentityId()
    {
        $relId=1618;
        $resourceType='player';
        $originId=3;
        $info=\app\modules\task\services\hot\HotBase::getMainIdByRelIdentityId($resourceType,$relId,$originId);
        print_r($info);
    }

    public function testRef()
    {
        OperationRefreshHotInfo::refreshListForLog();
    }

    public function testBatter()
    {
        \app\modules\task\services\TaskRunner::run(365664);
    }

}