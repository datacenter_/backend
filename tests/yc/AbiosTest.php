<?php


use app\modules\common\models\EnumGame;
use app\modules\common\services\Consts;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\match\models\Match;
use app\modules\match\services\StreamService;
use app\modules\org\models\TeamPlayerRelation;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\v3\AbiosBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\match\controllers\VideoUrlController;

class AbiosTest  extends PHPUnit\Framework\TestCase
{

    public function testRunTask()
    {
//        \app\modules\task\services\TaskRunner::run(7269);  //
        \app\modules\task\services\TaskRunner::run(5973020);  //
    }
    public function testaaa12(){
            $datas =  TaskInfo::find()->select(['id'])->where(['batch_id'=>'ctb_abios_match__20210107203002','status'=>1])
                ->asArray()->all();
            foreach ($datas as $k=> $v){
                TaskRunner::run($v['id']);  //

            }

    }


    public function testDotaItemList()
    {
        //刀塔道具
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosDotaItemList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_DOTA2_ITEM,''),
            "params"=>[
                "action"=>"/assets",
                "params"=>[
                    "filter"=>"game.id=1",
//                    "filter"=>"game.id=1,category=hero",
//                    "filter"=>"game.id=1,category=spell",
//                    "filter"=>"id<={2716,2717}",
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 1);
    }
    //lol道具
    public function testLolItemList()
    {

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosLOLItemList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_LOL_ITEM),
            "params"=>[
                "action"=>"/assets",
                "params"=>[
                    "filter"=>"game.id=2",
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 1);
    }
    //csgo武器

    public function testCSWeapons()
    {
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosCSWeaponsList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_CSGO_WEAPON),
            "params"=>[
                "action"=>"/assets",
                "params"=>[
                    "filter"=>"game.id=5,category=weapon",
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }

    public function testCSMap()
    {
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosCSMapList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::METADATA_TYPE_CSGO_MAP),
            "params"=>[
                "action"=>"/maps",
                "params"=>[
//                    "filter"=>"game.id=5",
                    "filter"=>"game.id=5,id<={1,2}",
//                    "skip" =>0,
//                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 1);
    }

    //比赛
    public function testMatch()
    {

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "order"=>"id-desc",
//                    "filter"=>"game.id=1,category=hro",
//                    "filter"=>"lifecycle=over",/**/
//                    "filter"=>"id<={145868,145520,144887}",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);
    }


    //比赛
    public function testovetEndTimeMatch()
    {
        $endTimeData = date('Y-m-d H:i:s', strtotime('-32 hours'));
        $endTimeEx = explode(' ',$endTimeData);
        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=over,end>=$endTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);

        $live=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=live",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($live, 5);


        $upcoming=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($upcoming, 5);

    }

    public function testSingleMatch()
    {

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatch::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series/"."10",
                "params"=>[]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }




    public function testaa() {
        $teamByPlayerInfo = TeamPlayerRelation::find()
            ->select("team_player_relation.team_id as team_id")
            ->leftJoin('team', 'team.id=team_player_relation.team_id')
            ->where(["player_id" => 662])->asArray()->all();

        foreach ($teamByPlayerInfo as $teamsIdByPlayer) {
            if ($teamsIdByPlayer['team_id']) {
                RenovateService::refurbishApi('team', $teamsIdByPlayer['team_id']);
            }
        }


    }





};
