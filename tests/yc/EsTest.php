<?php


use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\common\services\EsService;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\v3\AbiosBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\api\RenovateService;

class EsTest  extends PHPUnit\Framework\TestCase
{
    public function testESDelIndex(){
        $client = EsService::initConnect();

        $params = [
            'index' => 'articles_index'
        ];
        $res = $client->indices()->delete($params);
        print_r($res);


    }

    public function testEsDelText() {
        $client = EsService::initConnect();

        $params = [
            'index' => 'articles_index',
            'id' => '1'
        ];
        $res = $client->deleteIndex('articles_index');
        print_r($res);
    }

    public function testESRead(){
        $client = EsService::initConnect();

        $params = [
            'index' => 'articles_index',
            'id' => 2
        ];
        $res = $client->get($params);
        print_r($res);

    }

    public function testEsSerch() {
        $client = EsService::initConnect();
        $params = [
            'index' => 'articles_index',
//            'type' => 'articles_type',
        ];

        $params['body']['query']['match']['content'] = '测试';
        $res = $client->getIndexList($params);
        print_r($res);

    }

    public function testEsCreate() {
        //初始化Es链接
        $client = EsService::initConnect();
        $data = [
            ['id'=> 1 ,'title'=>'测试1','content'=>'测试文章内容1'],
            ['id'=> 2 ,'title'=>'测试2','content'=>'测试文章内容2'],
            ['id'=> 3 ,'title'=>'测试3','content'=>'测试文章内容3']
        ];
        $index_info = 'articles_index';




        foreach ($data as $key => $val) {

                $body = [
                    'id' => $val['id'],
                    'title' => $val['title'],
                    'content' => $val['content']
                ];

            $client->createIndexAdd($index_info,strval($val['id']),$body);

        }

    }

    public function testData(){
        $aa = (new RenovateService)->refurbishApi(RenovateService::INTERFACE_TYPE_TOURNAMENT);

    }

    public function testaa(){
       $params['body']['query']['match']['content'] = '测试';
        $aaa = json_encode($params);





        $playerss[1]['nationality'] = EnumCountry::find()->select(['id', 'e_name as name', 'name as name_cn', 'two as short_name', 'image'])
            ->where(["id"=>99999])->asArray()->one();
        if (empty($playerss[1]['nationality'])){
            $playerss[1]['nationality'] = null;
        }



        $aa =[];
        $aa['o'] = 'dsf';
        $aa['aa']['id'] = null;
        $aa['aa']['name'] = null;
        $aa['aa']['title'] = null;
        if (empty($aa['aa']['id'])){
            unset($aa['aa']['id']);
//            $aa['aa'] = null;
        }
        $bb =$aa;


    }





};
