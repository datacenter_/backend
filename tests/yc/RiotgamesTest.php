<?php

use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class RiotgamesTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(58);  //
    }
    public function testRunStart()
    {
        \app\modules\task\services\grab\riotgames\Start::run();//
    }
    public function testRiotSummoner()
    {
        //召唤师技能
            $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotSummoner::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "summoner",
                    "params" => [
                    ]
                ],
            ];
        TaskRunner::addTask($task, 2);
    }

    /**
     * lol英雄
     * @throws \yii\db\Exception
     */
    public function testRiotHero()
    {
        //英雄
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotChampions::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "champion",
                "params" => [
                    'id' => 1,
                ]
            ],
        ];
        TaskRunner::addTask($task, 2);
    }

    /**
     * lol技能
     * @throws \yii\db\Exception
     */
    public function testRiotAbility()
    {
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotAbility::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "ability",
                "params" => [
//                    'id' => 'AatroxE',
                ]
            ],
        ];
        TaskRunner::addTask($task, 3);
    }
    /**
     * lol道具
     * @throws \yii\db\Exception
     */
    public function testRiotItems()
    {
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotItems::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "item",
                    "params" => [
                        'id' =>1001
                    ]
                ],
        ];
        TaskRunner::addTask($task, 3);
    }

    /**
     * 召唤师技能
     * @throws \yii\db\Exception
     */
    public function testSummoner()
    {
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotSummoner::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "summoner",
                "params" => [
                        'id' =>1,
                ]
            ],
        ];
        TaskRunner::addTask($task, 3);
    }


    /**
     * 符文
     * @throws \yii\db\Exception
     */
    public function testRune()
    {
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotRunesReforged::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "",
                "params" => [
                        'id' =>8124,
                ]
            ],
        ];
        TaskRunner::addTask($task, 2);
    }

    public function test()
    {
        $a = \app\modules\task\services\grab\riotgames\RiotgamesBase::curlNewVersion();
        printf($a);
    }

    /**
     * 元数据新增
     */
    public function testAddRiotMetadata()
    {
        $modelResult = \app\modules\data\models\StandardDataMetadata::find()
            ->select('id,metadata_type')->where(['origin_id'=>6])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
                $metadataType=$item['metadata_type'];
                BindingMetadataService::addAndBinding($metadataType, $standardId,Consts::USER_TYPE_ROBOT,0);
            }catch (Exception $e){
                print_r($e);
            }

        }
    }

    /**
     * pandascore选手新增(中国选手)
     */
    public function testBindingPlayer()
    {
        $modelResult = \app\modules\task\models\StandardDataPlayer::find()
            ->select('id')->where(['origin_id'=>3,'nationality'=> 211])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
               return \app\modules\data\services\binding\BindingPlayerService::addAndBinding($standardId, Consts::USER_TYPE_ROBOT, 0);
            }catch (Exception $e){
                print_r($e);
            }

        }


    }

    public function testAbc()
    {
        /**
         * @param $relIdentityId(比赛id)
         * @param $resourceType(资源类型)
         * @param $originId(来源id)
         * @param $gameId(游戏id)
         * @param $reason(刷新方式)
         */
        $relIdentityId = "1";
        $resourceType = "lol_champion";
        $originId = "6";
        $gameId = "2";
        return \app\modules\data\services\binding\BindingMatchService::getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId);
    }
}