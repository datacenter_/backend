<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DbStream;
use app\modules\data\services\binding\BindingPlayerService;
use app\modules\data\services\binding\BindingTeamService;
use app\modules\data\services\DebugDataTodoService;
use app\modules\match\services\MatchService;
use app\modules\match\services\StreamService;
use app\modules\metadata\models\MetadataCsgoMap;
use app\modules\org\services\TeamService;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\services\ContrastService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

class TestAddTaskTest   extends PHPUnit\Framework\TestCase
{

    public function testAddPlayerTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvPlayer::class,
            "batch_id" => "" . time(),
            "params" => [
                'type' => "player",
                'offset' => "0",
                'limit' => "500",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }


    public function testAddTeamTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvTeam::class,
            "batch_id" => "" . time(),
            "params" => [
                'type' => "team",
                'offset' => "0",
                'limit' => "500",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }

    public function testAddMatchTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => "" . time(),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "500",
                'where' => "where id = 299",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,2);
    }

    public function testaaaa() {
        $a = '[{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10851","map":" Train","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10206","map":" Vertigo","types":"removed"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10851","map":" Nuke","types":"picked"},{"team_id":"10206","map":" Inferno","types":"picked"},{"team_id":"10206","map":" Inferno","types":"picked"},{"team_id":"10206","map":" Inferno","types":"picked"},{"team_id":"10206","map":" Inferno","types":"picked"},{"team_id":"10851","map":" Dust2","types":"removed"},{"team_id":"10851","map":" Dust2","types":"removed"},{"team_id":"10851","map":" Dust2","types":"removed"},{"team_id":"10851","map":" Dust2","types":"removed"},{"team_id":"10851","map":" Dust2","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Overpass","types":"removed"},{"team_id":"10206","map":" Mirage ","types":"was left over"},{"team_id":"10206","map":" Mirage ","types":"was left over"},{"team_id":"10206","map":" Mirage ","types":"was left over"}]';

       $b = json_decode($a,true);
        $ttypes = ['removed'=>'1','picked'=>'2','was left over'=>'3'];
        foreach ($b as $kkk => $vvv) {
            $mapId = MetadataCsgoMap::find()->where(['name'=>trim($vvv['map']," "),'deleted' => 2])->asArray()->one();
            $newArr[$kkk]['team'] = $vvv['team_id'];
            $newArr[$kkk]['type'] = $ttypes[$vvv['types']];
            $newArr[$kkk]['map'] = $mapId['id'];
        }
        $mapBan = json_encode($newArr);

    }


    public function testAddTournamentTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvTournament::class,
            "batch_id" => "" . time(),
            "params" => [
                'type' => "tournament",
                'offset' => "0",
                'limit' => "500",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,2);
    }



    public function testTaskRun()
    {
//        \app\modules\task\services\TaskRunner::run(376947);
        \app\modules\task\services\TaskRunner::run(3493);
    }

    public function testmmm() {
        $attribute['team_1_score'] = null;
        if (isset($attribute['team_1_score'])){
            echo '1';

        }else{
            echo 's';
        }

    }
        public function testpp(){
        $a = null;
        $c = json_encode($a);
        var_dump($c);



        $a = '[{"team":"","type":"1","map":""}]';
        $b[0] = ['team'=>'','type'=>"",'map'=>''];
        $c = json_encode($b);
        if ($a == $c){
            echo "s";
        }else{
            echo 'sss';
        }


        }
    public function testTaskAdd()
    {
        $attributes = json_decode('{"core_data":{"name":"","organization":"","game":1,"country":"","full_name":"","short_name":"","alias":"","slug":"ee","image":"http://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/type_logo/154e45a869e1ce68ea6eeffb6dc5cc71/296d3f1f21084795d6ba7c0155c96a27.png"},"base_data":{"region":"","world_ranking":"","total_earnings":"","average_player_age":"","history_players":"","introduction":"","introduction_cn":""},"players":[]}',true);

        TeamService::setTeam($attributes, Consts::USER_TYPE_ADMIN, 0);


//        \app\modules\task\services\TaskRunner::run(376947);
//        \app\modules\task\services\TaskRunner::run(194428);
    }

    public function testTaskEdit()
    {
        $attributes = json_decode('{"id":"4204","core_data":{"id":"4204","name":"","organization":"","game":"1","country":"","full_name":"","short_name":"","alias":"","slug":"ee","image":"http://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/type_logo/154e45a869e1ce68ea6eeffb6dc5cc71/296d3f1f21084795d6ba7c0155c96a27.png","cuser":"1","deleted":"2","created_at":"2020-09-03 18:23:01","modified_at":"2020-09-03 18:25:42","deleted_at":null},"base_data":{"id":"4200","team_id":"4204","steam_id":null,"region":"","world_ranking":"","ago_30":null,"total_earnings":"","average_player_age":"","history_players":"","create_team_date":null,"close_team_date":null,"introduction":"","introduction_cn":"","created_at":"2020-09-03 18:23:01","modified_at":"2020-09-03 18:23:01"},"team_player":[]}',true);

        TeamService::setTeam($attributes, Consts::USER_TYPE_ADMIN, 0);

    }

    public function testBindingAdd(){
//        $standardId=json_decode('{"standard_id":"35239"}',true);
        $standardId = 18842;
//
//
        return BindingPlayerService::addAndBinding($standardId,Consts::USER_TYPE_ROBOT,0);


////        }
///   public static function getInfoFromUrl($url)
//    {
//        $mp=[
//            [
//                'reg'=>'/^https\:\/\/www\.huya\.com\/(.*)$/',
//                'type'=>'Huya'
//            ],
//            [
//                'reg'=>'/^https\:\/\/www\.huomao\.com\/(.*)$/',
//                'type'=>'Huomao'
//            ],
//            [
//                'reg'=>'/^https\:\/\/www\.twitch\.tv\/(.*)$/',
//                'type'=>'Twitch'
//            ],
//            [
//                'reg'=>'/^https\:\/\/live\.bilibili\.\com\/(.*)$/',
//                'type'=>'BiliBili'
//            ],
//            [
//                'reg'=>'/^https\:\/\/www\.douyu\.com\/(.*)$/',
//                'type'=>'Douyu'
//            ],
//            [
//                'reg'=>'/^http\:\/\/www\.youtube\.com\/(.*)$/',
//                'type'=>'YouTube'
//            ],
//            [
//                'reg'=>'/^http\:\/\/www\.afreecatv\.com\/(.*)$/',
//                'type'=>'AfreecaTV'
//            ],
//        ];
//        $info=[
//            'room_id'=>"",
//            'platform_name'=>'',
//            'url'=>$url,
//        ];
//        foreach($mp as $r){
//            $mt=preg_match_all($r['reg'],$url,$matchAll);
//            if($mt){
//                $info['platform_name']=$r['type'];
//                $info['room_id']=@$matchAll[1][0];
//                break;
//            }
//        }
//        return $info;
//    }


            $mp=[
                [
                    'reg'=>'/^https\:\/\/www\.huya\.com\/(.*)$/',
                    'type'=>'Huya'
                ],
                [
                    'reg'=>'/^https\:\/\/www\.huomao\.com\/(.*)$/',
                    'type'=>'Huomao'
                ],
                [
                    'reg'=>'/^https\:\/\/www\.twitch\.tv\/(.*)$/',
                    'type'=>'Twitch'
                ],
                [
                    'reg'=>'/^https\:\/\/live\.bilibili\.\com\/(.*)$/',
                    'type'=>'BiliBili'
                ],
                [
                    'reg'=>'/^https\:\/\/www\.douyu\.com\/(.*)$/',
                    'type'=>'Douyu'
                ],
                [
                    'reg'=>'/^http\:\/\/www\.youtube\.com\/(.*)$/',
                    'type'=>'YouTube'
                ],
                [
                    'reg'=>'/^http\:\/\/www\.afreecatv\.com\/(.*)$/',
                    'type'=>'AfreecaTV'
                ],
            ];
            $info=[
                'room_id'=>"",
                'platform_name'=>'',
                'url'=>$url,
            ];
            foreach($mp as $r){
                $mt=preg_match_all($r['reg'],$url,$matchAll);
                if($mt){
                    $info['platform_name']=$r['type'];
                    $info['room_id']=@$matchAll[1][0];
                    break;
                }
            }

            var_dump($info);


    }

    public function testBindingedit(){

        $json = '{"origin_type":"pandascore","resource_type":"match","change_info":{"description_cn":"ppp"},"standard_id":"1368"}';
        $params = json_decode($json, true);
        $a = DebugDataTodoService::debugAddToDo($params);
       print_r($a);
    }

    public function test(){

        $json = '{"rel_id":"9896","origin_id":"7","game_id":"1","resource_type":"player"}';
        $params = json_decode($json, true);
        $a =  ContrastService::getMasterData($params);;
        print_r($a);
    }


    public function testCheck() {

        $b['ss'] ="";
        $a = 's';
        if (isset($b) || empty($a)){
            echo 's';
        }
        echo "b";

//        }
//        var_dump($front);


//                $front = strstr($val['viewers'], $back, true);


    }


    public function testaa(){
        $a = \app\modules\common\services\HelperResourceFormat::checkTime('8147454847',"Y-m-d");
        var_dump($a);

    }

    public function testbb(){

        StreamService::upSaveUrl();
//        StreamService::grabOrigin();


//        StreamService::UpSaveUrl();


//        StreamService::onceAgainUp();
//        $a = StreamService::transformationUrl("https://s1.hdslb.com/bfs/static/blive/live-assets/player/flash/pageplayer-latest.swf?room_id=21733448&cid=21733448&state=LIVE");
//        var_dump($a);
//        https://s1.hdslb.com/bfs/static/blive/live-assets/player/flash/pageplayer-latest.swf?room_id=11435516&cid=11435516&state=LIVE
    }


    public function testcc(){
        $teamCount = DbStream::getDb()->createCommand(
            "select * from match_all  limit  0,5"
        )->queryAll();
        $matcheId = array_column($teamCount,'matche_id');

        $sql = "select * from map_vetoes where `matche_id` in ('" .implode("','",$matcheId)."')" ;
        $mapVetoes = DbStream::getDb()->createCommand($sql)->queryAll();
        $newMapVetoes = [];
        foreach ($mapVetoes as $k => $v) {
            $newMapVetoes[$v['']] = $v;
        }
        $getMapByMatcheId = array_column($mapVetoes,'map_vetoes','matche_id');
        foreach ($teamCount as $key => &$val) {
            $val['map_vetoes'] = $getMapByMatcheId[$val['matche_id']];
        }


        var_dump($teamCount);
        var_dump($getMapByMatcheId);

    }

    //刷新推荐绑定关联关系
    public function testRefreshOn()
    {
//        $a = strtotime(date("Y-m-d H:i:s",time()+3600*12));
//        echo $a;
       \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_MATCH,[41250],[],'match');
    }





}