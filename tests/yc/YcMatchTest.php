<?php

use app\modules\common\models\EnumCountry;
use app\modules\common\services\ApiBase;
use app\modules\common\services\Consts;
use app\modules\common\services\EsService;
use app\modules\data\models\DataStandardMasterRelation;
use app\modules\match\models\ErrorNotice;
use app\modules\match\models\MatchStreamList;
use app\modules\org\models\PlayerSnapshot;
use app\modules\org\models\TeamSnapshot;
use app\modules\task\models\MatchBattle;
use \app\modules\task\models\StandardDataMatch;
use app\modules\match\services\StreamService;
use app\modules\metadata\services\LolRiotgamesService;
use app\modules\task\services\Common;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\originIncrement\OriginRunBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use \app\modules\task\models\TaskInfo;
use \app\modules\match\models\Match;
use app\rest\exceptions\BusinessException;

class LivedMatchTest extends PHPUnit\Framework\TestCase
{

    public function testAdd()
    {
        \app\modules\task\services\TaskRunner::run(18863281);//13728
    }

    public function testaaa12(){
//        $datas =  TaskInfo::find()->select(['id'])->where(['batch_id'=>'ctb_abios_match__20210107203002','tag'=>'grab....overMatchendTime.','status'=>1])
//            ->asArray()->all();

        $a  = '["17371229","17371251","17371269","17371325","17371368","17754493","17788881","17788882","17845052","17916099","17916133","17949264","17951138","17951239","17951240","17951241","17951466"]';
        $json = json_decode($a,true);


        foreach ($json as $k=> $v){
            TaskRunner::run($v);
        }
    }

    //测试日志添加
    public function testlogAdd()
    {
//        \app\modules\task\services\logformat\LogFormatDeguoDispatch::run();

    }

    public function testAAAA() {
        $lastDate = date("Y-m-d H:i:s", strtotime("-120 minute"));
        $taskInfo = TaskInfo::find()->where(['>=','begin_time',$lastDate])->andWhere(['status'=>4]);
        $datas = $taskInfo->asArray()->all();
//        var_dump($datas);;die;

        foreach ($datas as $key => $val) {

            $newData = ['run_type'=>$val['run_type'] ,'batch_id'=>$val['batch_id'],
                'error_info'=>$val['error_info'],'debug_info'=>$val['debug_info']];
            $info = json_encode($newData);


            $url = 'www.yinmonitor.com/v1/warning/warning/message';
            $params = [
                'type' =>'task_info',
                'tag' =>$val['tag'],
                'info' =>$info,
            ];

            $infoJson = Common::requestPost($url,[],$params);

        }

    }

    public function testaaaaab() {
      $aa =   $this->testCheckPsGreb('team');
    }


    public function testAllMatch()
    {
        $nowTime = date('Y-m-d');
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "300",
                "where" => ""
            ],
        ];
        TaskRunner::addTask($info, 1);

    }

    public function testAllTournament()
    {
        $nowTime = date('Y-m-d');
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvTournament::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params" => [
                'type' => "tournament",
                'offset' => "0",
                'limit' => "300",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);

    }




    //二维数组排序方法
    //$arrays 是数组
    //$sort_key 排序的KEY
    //$sort_order 排序方式
    //$sort_type 排序类型
    public function my_array_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
        return $arrays;
    }

    public function testbb()
    {
       $a['team_order'] = '[{"map":1,"team":"6137"}]';

       $c = json_encode($a);
//        $string = '323.24';
//        $patterns = "/[\d\.,]+/";
//        preg_match_all($patterns,$string,$arr);
//        $result = implode('',$arr[0]);
//        $res = str_replace(",","",$result);
//        $a = intval($res);
//        if($res){
//            return intval($res);
//        }
    }
    public function testaaaaa()
    {
        $talent = '[{"id":10,"left":"34","right":"16"},{"id":15,"left":"54","right":"1"},{"id":20,"left":"7","right":"32"},{"id":28,"left":"33","right":"43"}]';
        $talents = json_decode($talent,true);
        $error = json_last_error();
        if (!empty($error)) {
            return false;
        }
        $newarr = [];
        foreach ($talents as $key => $val) {
            if (!in_array($val['id'],[10,15,20,25])){
                unset($val);
                $isKey = [ 0 => 10, 1 => 15 , 2 => 20 , 3 => 25];
                if ($isKey[$key]) {
                    $newarr[$key] = ['id'=>$isKey[$key],'left' => '','right'=>''];
                }
                continue;
            };

            if (!empty($talents[$key])){
                $new = array_slice($talents[$key],-2);
            }
            $newarr[$key]['id'] = $val['id'];
            foreach ($new as $k => $v){
                $std = \app\modules\data\models\StandardDataMetadata::find()
                    ->select(["id","rel_identity_id"])
                    ->where(["metadata_type"=>"dota2_talent","deal_status"=>4,'rel_identity_id'=>$v])->asArray()->one();
                if ($std){
                    $rel = DataStandardMasterRelation::find()
                        ->select(['master_id','standard_id'])
                        ->where(['resource_type'=>'dota2_talent','standard_id'=>$std['id']])
                        ->asArray()->one();
                    if (!empty($rel)) {
                        $newarr[$key][$k] = $rel['master_id'];
                    }else{
                        $newarr[$key][$k] = '';
                    }
                }else{
                    $newarr[$key][$k] = '';

                }

            }

        }


        $newJson = json_encode(array_values($newarr));
        return  $newJson;

    }

    public function testaaa() {
//        $a = "perk-images/Styles/Domination/Predator/Predator.png";
//        $b = basename($a);
//
//        $v = pathinfo($a, PATHINFO_FILENAME) ;
        $a = 'Nuke--Mirage--Overpass';
        $b = explode('--',$a);
        foreach ($b as  $k => $val) {
            $c[]['map'] = $val;
        }

        $d = json_encode($c);

        $v = '[{"map":"3"},{"map":"8"},{"map":"9"}]';
        $vv = json_decode($v,true);
        if(!is_null($vv)){
            $ttypes = ['removed'=>'1','picked'=>'2','was left over'=>'3'];

            foreach ($vv as $kk=>$vv){
                $a = $vv['map'];

            }
        }else{
            echo "c";
        }

    }

    //钉钉群推动消息
    public function testvvv() {

//
//        $str = "POST\n/api/tenants/5950/rest/channels/20/messages\n1489490514142\n705bfbd388d2bf852813fc90e655b5ed";
//        $key = '02a0693ba5a57560df1f26a991204cb0';
//        $sig = hash_hmac("sha256", $str, $key, true);
//        echo $sig = base64_encode($sig);
        list($s1, $s2) = explode(' ', microtime());
        $timestamp = (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
        $secret = "SEC248d3a9a2060a5cb558282f3736ff2ba987d48b254f37f7ea72b129d68f3622a";
//        $secret = "SEC85a9d182138ee8df9682ce7ef9b8559ba88d9563fc6249cd7d91104a073697c2";
        $data = $timestamp . "\n" . $secret;

        $signStr = base64_encode(hash_hmac('sha256', $data, $secret,true));
        $signStr = utf8_encode(urlencode($signStr));
//        zPJaOVRgmiCKr0GcXNxmECyMulv6TNqRSCFjKFgB1rg%3D
        $webhook = "https://oapi.dingtalk.com/robot/send?access_token=3b58c13eb03a8cbd854f336dec3197531d93c8a3a781e26ece7f2335ce4cc5c8";
//        $webhook = "https://oapi.dingtalk.com/robot/send?access_token=2cebfd76fac502be65c5679190e696d64784c5872ec3ad06f199703aca5266a1";

        $webhook .= "&timestamp=$timestamp&sign=$signStr";

        $lastDate = date("Y-m-d H:i:s", strtotime("-15 minute"));
        $taskInfo = TaskInfo::find()->where(['>=','begin_time',$lastDate])->andWhere(['status'=>4]);
        $q = $taskInfo->createCommand()->getRawSql();
        $datas = $taskInfo->asArray()->all();

        $newData = ['batch_id'=>$datas[0]['batch_id'],
            'error_info'=>$datas[0]['error_info'],'debug_info'=>$datas[0]['debug_info']];
        $aa = json_encode($newData);

//        foreach ($datas as $key => $val) {
            $message="业务报警";
            $data = array ('msgtype' => 'tet','text' => array ('content' => $message),
                'at' =>array('atMobiles'=>['15710099947','15030001195'],"isAtAll"=>false)
            );
            $data_string = json_encode($data,JSON_UNESCAPED_UNICODE);
            $result = $this->request_by_curl($webhook, $data_string);
//        }


//        $result = $this->request_by_curl($webhook, $data_string);
//        echo $result;


    }


    public function request_by_curl($remote_server, $post_string)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $remote_server);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=utf-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }



    public function testmessage(){
        $lastDate = date("Y-m-d H:i:s", strtotime("-360 minute"));

        $taskInfo = TaskInfo::find()->where(['>=','begin_time',$lastDate])->andWhere(['status'=>4]);
        $q = $taskInfo->createCommand()->getRawSql();
        $datas = $taskInfo->asArray()->all();


        foreach ($datas as $key => $val) {

            $newData = ['run_type'=>$val['run_type'] ,'batch_id'=>$val['batch_id'],
                'error_info'=>$val['error_info'],'debug_info'=>$val['debug_info']];
            $info = json_encode($newData);


            $url = 'www.yinmonitor.com/v1/warning/warning/message';
            $params = [
                'type' =>'task_info',
                'tag' =>$val['tag'],
                'info' =>$info,
            ];

            $infoJson = Common::requestPost($url,[],$params);

        }



    }


    public function testRelation($metadata_type) {
      $standardDataActiveTable = OriginRunBase::getStandardActiveTable($metadata_type);


        $lolo = $standardDataActiveTable::find()->select([])->where(['deal_status'=>4])->asArray()->all();
        $lolsStandard = array_column($lolo,'id','id');
        $standardRelation = DataStandardMasterRelation::find()->where(['standard_id'=>$lolsStandard])->andWhere(
            ['and',
                ['!=','resource_type','clan'],
                ['!=','resource_type','team'],
                ['!=','resource_type','series'],
                ['!=','resource_type','player'],
                ['!=','resource_type','tournament'],
                ['!=','resource_type','match'],
            ])//->createCommand()->getRawSql();

            ->asArray()->all();
        $standardRelationId = array_column($standardRelation,'standard_id','id');

        $diff = array_diff($lolsStandard,$standardRelationId);
        $standardRelation2 = DataStandardMasterRelation::find()->where(['standard_id'=>$diff])->andWhere(
            ['and',
                ['!=','resource_type','clan'],
                ['!=','resource_type','team'],
                ['!=','resource_type','series'],
                ['!=','resource_type','player'],
                ['!=','resource_type','tournament'],
                ['!=','resource_type','match'],
            ])->createCommand()->getRawSql();

//        ->asArray()->all();

    }

    public function testaasss($metadata_type){
        //        $Relationmaster_id = array_column($standardRelation,'master_id','id');
//        $master = \app\modules\metadata\models\MetadataLolChampion::find()->where(['id'=>$Relationmaster_id])->asArray()->all();
//        $master_id =array_column($master,'id','id');
//        $diffmaster_id = array_diff($Relationmaster_id,$master_id);
//        $standardDataActiveTable = OriginRunBase::getStandardActiveTable($metadata_type);

//        $data = $standardDataActiveTable::find()->where(['metadata_type'=>$metadata_type,'deal_status'=>4])->asArray()->all();
//        foreach ($data as $key => $val){
//            HotBase::getMainIdByRelIdentityId('team', $formatDiffInfo['winner']['after'], $originId, $gameId);


    }


    public function testasssaa(){
        $matchData = MatchBattle::find()->select(['id'])->where(['deleted'=>2])->orderBy('id asc')->asArray()->all();
        $matchCount = count($matchData);
        $matchEs = $this->testgetMatchList($matchCount);
        if ($matchCount == $matchEs['count']) {
            return null;
        }
        $matchDataIds = array_column($matchData,'id','id');
        $matchEsIds = array_column($matchEs['list'],'battle_id','battle_id');

        $diffArr = array_diff($matchDataIds,$matchEsIds);
        $diffJson = json_encode(array_values($diffArr));
//        if (!$matchDat)


    }


    public function testgetMatchList($matchCount) {


//        $index_info = 'resource_battle_list';
//        $conditions = [];
//        $order = array('field' => 'match_id', 'sort' => 'asc');
//        $offset  =  0;
//        $columns = [];
//        $EsService = EsService::initConnect();
//        $res     = $EsService->getIndexList($index_info, $offset, $matchCount,$conditions,$order,$columns);
//        return $res;
    }

    public function testaaaassdf(){
        $standardDataActiveTable = OriginRunBase::getStandardActiveTable('player');
        $standardData = $standardDataActiveTable::find()->select(['id','rel_identity_id'])->where(['deal_status'=>4,'origin_id'=>7,'deleted'=>2])->asArray()->all();
        $standardId = array_column($standardData,'id');
        $standardRelation = DataStandardMasterRelation::find()->select(['standard_id'])->where(['standard_id'=>$standardId,"resource_type"=>'player'])
            ->asArray()->all();
        $relation = array_column($standardRelation,'standard_id');
        $diddId = array_diff($standardId,$relation);
      $param['deal_status'] = 1;
       \app\modules\task\models\StandardDataPlayer::updateAll($param,['id'=>$diddId]);
//        $arrid = array_column($standardData,null,'id');
//        $a = [];
//        foreach ($diddId as $key => $val) {
//            $a[] = $arrid[$val]['rel_identity_id'];
//        }
//
//        $aaa = implode(',',$a);
    }



    public function testaasaa(){

        $url = "http://ip.ws.126.net/ipquery?ip=43.224.44.10";

        $header=[
            "content-type"=>'application/json; charset=utf-8',
        ];
       $aa =  Common::requestGet($url,$header,[],false);
        $outPageTxt = mb_convert_encoding($aa, 'utf-8','gbk');
    }

    public function testcv(){
        $sal = "SELECT * FROM `gamecenter_pro`.`warning_info` WHERE `type` LIKE '%task_info_reuse%' and `created_at` >= '2021-03-06' ORDER BY `id` DESC ";
        $connection  = Yii::$app->db;
        $command = $connection->createCommand($sal);
        $data = $command->queryAll();

        foreach ($data as $key=> $val) {
            $j = json_decode($val['info'],ture);
            $i .=  implode(',',$j);

        }
        $a = implode(',',array_unique(explode(',',$i)));


    }


    public function testaaassfs(){
//       $a =  strpos("main_increment..sys_data_resource_update_config.add..", 'main_ncrement.');



        $lastDate = date("Y-m-d H:i:s", strtotime("-1 day"));
        $minuteDate = date("Y-m-d H:i:s", strtotime("-10 minute"));

        $taskInfo = TaskInfo::find()->select('id,tag')->where([
            "and",
            ['>=', 'created_time', $lastDate],
            ["<=", 'created_time', $minuteDate],
        ])->andWhere(['status' => 1]);
        $datas = $taskInfo->asArray()->all();
        if (empty($datas)) {
            echo "没事数据\n";
        }
        

        if (!empty($datas)) {
            $newData = array_column($datas,"id",'');
            $info = json_encode($newData);

            $params = [
                'type' => 'task_info_reuse',
                'tag' => "task_info没有执行",
                'info' => $info,
            ];

        }



//        \app\modules\tournament\services\TournamentService::playerSnapshotAdd('14','tournament','473',['238','274']);


    }
}