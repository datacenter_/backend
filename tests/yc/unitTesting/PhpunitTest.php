<?php


namespace yc\unitTesting;
use app\modules\common\services\Consts;
use app\modules\match\services\PbpService;
use app\modules\match\services\StreamService;
use PHPUnit\Framework\InvalidArgumentException;
use PHPUnit\Framework\TestCase;


class PhpunitTest extends TestCase
{
//    public function testPushAndPop()
//    {
        //断言两个变量是否等
//      $this->assertEquals('aa', 'aa');
        //判断是否为ture
       // $this->assertTrue(false)
        //判断 $stack == empty 是否为真
        //$this->assertEmpty($stack);
        // 判断 $stack != empty 是否为真
//        $this->assertNotEmpty($stack);


//                断言
//        布尔类型
//        assertTrue 断言为真
//        assertFalse 断言为假
//
//        NULL类型
//        assertNull 断言为NULL
//        assertNotNull 断言非NULL
//
//        数字类型
//        assertEquals 断言等于
//        assertNotEquals 断言不等于
//        assertGreaterThan 断言大于
//        assertGreaterThanOrEqual 断言大于等于
//        assertLessThan 断言小于
//        assertLessThanOrEqual 断言小于等于
//
//        字符类型
//        assertEquals 断言等于
//        assertNotEquals 断言不等于
//        assertContains 断言包含
//        assertNotContains 断言不包含
//        assertContainsOnly 断言只包含
//        assertNotContainsOnly 断言不只包含
//
//        数组类型
//        assertEquals 断言等于
//        assertNotEquals 断言不等于
//        assertArrayHasKey 断言有键
//        assertArrayNotHasKey 断言没有键
//        assertContains 断言包含
//        assertNotContains 断言不包含
//        assertContainsOnly 断言只包含
//        assertNotContainsOnly 断言不只包含
//
//        对象类型
//        assertAttributeContains 断言属性包含
//        assertAttributeContainsOnly 断言属性只包含
//        assertAttributeEquals 断言属性等于
//        assertAttributeGreaterThan 断言属性大于
//        assertAttributeGreaterThanOrEqual 断言属性大于等于
//        assertAttributeLessThan 断言属性小于
//        assertAttributeLessThanOrEqual 断言属性小于等于
//        assertAttributeNotContains 断言不包含
//        assertAttributeNotContainsOnly 断言属性不只包含
//        assertAttributeNotEquals 断言属性不等于
//        assertAttributeNotSame 断言属性不相同
//        assertAttributeSame 断言属性相同
//        assertSame 断言类型和值都相同
//        assertNotSame 断言类型或值不相同
//        assertObjectHasAttribute 断言对象有某属性
//        assertObjectNotHasAttribute 断言对象没有某属性
//
//        class类型
//        class类型包含对象类型的所有断言，还有
//        assertClassHasAttribute 断言类有某属性
//        assertClassHasStaticAttribute 断言类有某静态属性
//        assertClassNotHasAttribute 断言类没有某属性
//        assertClassNotHasStaticAttribute 断言类没有某静态属性
//
//        文件相关
//        assertFileEquals 断言文件内容等于
//        assertFileExists 断言文件存在
//        assertFileNotEquals 断言文件内容不等于
//        assertFileNotExists 断言文件不存在
//
//        XML相关
//        assertXmlFileEqualsXmlFile 断言XML文件内容相等
//        assertXmlFileNotEqualsXmlFile 断言XML文件内容不相等
//        assertXmlStringEqualsXmlFile 断言XML字符串等于XML文件内容
//        assertXmlStringEqualsXmlString 断言XML字符串相等
//        assertXmlStringNotEqualsXmlFile 断言XML字符串不等于XML文件内容
//        assertXmlStringNotEqualsXmlString 断言XML字符串不相等



//        $stack = [];
////        $this->assertEquals(0, count($stack));
//
//
//        array_push($stack, 'foo');
//        $this->assertEquals('foo', $stack[count($stack)-1]);
//        $this->assertEquals(1, count($stack));
//        $a = array_pop($stack);
//        $this->assertEquals('foo', $a);
//        $this->assertEquals(0,[]);
//    }

    public function testPushAndPop()
    {
        $stack = [];
        $this->assertEquals(0, count($stack));

        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));

        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }



    //生产者
    public function testEmpty()
    {
        $stack = [];
        $this->assertEmpty($stack);

        return $stack;
    }

    /**
     * @depends testEmpty
     * testPush对testEmpty是消费者
     */
    public function testPush(array $stack)
    {
        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertNotEmpty($stack);

        return $stack;
    }

    /**
     * @depends testPush
     * testPop 消费者    testPush生产者
     */
    public function testPop(array $stack)
    {
        $this->assertEquals('foo', array_pop($stack));
        $this->assertEmpty($stack);
    }



    //2.3
    //利用测试之间的依赖关系
    public function testOne()
    {
        $this->assertTrue(false);
    }

    /**
     * @depends testOne
     */
    public function testTwo()
    {
    }


    public function testProducerFirst()
    {
        $this->assertTrue(true);
        return 'first';
    }

    public function testProducerSecond()
    {
        $this->assertTrue(true);
        return 'second';
    }

    //2.4
    //测试可以使用多个 @depends 标注。PHPUnit 不会更改测试的运行顺序，因此你需要自行保证某个测试所依赖的所有测试均出现于这个测试之前。
    //
    //拥有多个 @depends 标注的测试，其第一个参数是第一个生产者提供的基境，第二个参数是第二个生产者提供的基境，以此类推
    //不会以@depends 不会更改测试的运行顺序 但是返回参数会以@depends顺序


//    /**
//     * @depends testProducerFirst
//     * @depends testProducerSecond
//     */
//    public function testConsumer($aa,$bb)
//    {
//        $a = func_get_args();
//        $this->assertEquals(
//            ['first', 'second'],
//            func_get_args()
//        );
//    }


   // 2.5: 使用返回数组的数组的数据供给器

//    /**
//     * @dataProvider additionProvider
//     */
//    public function testAdd($a, $b, $expected)
//    {
//
//        $this->assertEquals($expected, $a + $b);
//    }
//
//    public function additionProvider()
//    {
//        return [
//            [0, 0, 0],
//            [0, 1, 1],
//            [1, 0, 1],
//            [1, 1, 3]
//        ];
//    }



//    /**
//     * @dataProvider additionProvider
//     */
//    public function testAdd($a, $b, $expected)
//    {
//        $this->assertEquals($expected, $a + $b);
//    }
//
//    public function additionProvider()
//    {
//        return [
//            'adding zeros'  => [0, 0, 0],
//            'zero plus one' => [0, 1, 9],
//            'one plus zero' => [1, 0, 1],
//            'one plus one'  => [1, 1, 3]
//        ];
//    }


//    /**
//     * @dataProvider additionProvider
//     */
//    public function testAdd($a, $b, $expected)
//    {
//        $this->assertEquals($expected, $a + $b);
//    }
//
//    public function additionProvider()
//    {
//        return new CsvFileIterator('data.csv');
//    }


    public function provider()
    {
        return [['provider1'], ['provider2']];
    }


    /**
     * @depends testProducerFirst
     * @depends testProducerSecond
     * @dataProvider provider
     */
    public function testConsumer()
    {
        $a = func_get_args();
        $this->assertEquals(
            ['provider1', 'first', 'second'],
            func_get_args()
        );
    }



    public function testFailingInclude()
    {

      $a = StreamService::pageList($params['matchId'] = '4343',Consts::USER_TYPE_ADMIN,$this->user->id);

    }







}