<?php


namespace yc\unitTesting;


use app\modules\common\services\Consts;
use app\modules\match\services\StreamService;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\TaskRunner;
use PHPUnit\Framework\TestCase;

class TestByMatchVideo extends TestCase
{
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(5511266);  //
    }

    public function testnnnn(){
        $datas =  TaskInfo::find()->select(['id'])->where(['batch_id'=>'ctb_abios_match__20210107203002','tag'=>'grab....overMatchendTime.','status'=>1])->orderBy('id desc')
            ->asArray()->all();
        foreach ($datas as $k=> $v){
            TaskRunner::run($v['id']);

        }
    }
    public function testaa(){
        $datas = '{"id":147782,"title":"Bracket - Final","start":"2018-01-07T17:16:00Z","end":"2018-01-07T23:04:54Z","postponed_from":null,"deleted_at":null,"lifecycle":"over","tier":1,"best_of":5,"chain":[],"streamed":true,"bracket_position":{"part":"UB","col":0,"offset":1},"participants":[{"seed":1,"score":3,"forfeit":false,"roster":{"id":29283},"winner":true,"stats":null,"teamId":1462},{"seed":2,"score":2,"forfeit":false,"roster":{"id":28829},"winner":false,"stats":null,"teamId":30}],"tournament":{"id":2593},"substage":{"id":16505},"game":{"id":1},"matches":[{"id":261059,"matchesd":{"id":261059,"lifecycle":"over","order":1,"series":{"id":147782},"map":null,"deleted_at":null,"game":{"id":1},"participants":[{"seed":1,"score":22,"forfeit":false,"roster":{"id":29283},"winner":true,"stats":null,"teamId":29283},{"seed":2,"score":30,"forfeit":false,"roster":{"id":28829},"winner":false,"stats":null,"teamId":28829}]},"coverage":"{\"data\":{\"postgame\":{\"server\":{\"status\":\"available\"},\"api\":{\"status\":\"unsupported\"}},\"realtime\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"}},\"live\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"},\"cv\":{\"status\":\"unknown\"}}},\"av\":{\"status\":\"unknown\"}}"},{"id":261060,"matchesd":{"id":261060,"lifecycle":"over","order":2,"series":{"id":147782},"map":null,"deleted_at":null,"game":{"id":1},"participants":[{"seed":1,"score":23,"forfeit":false,"roster":{"id":29283},"winner":false,"stats":null,"teamId":29283},{"seed":2,"score":33,"forfeit":false,"roster":{"id":28829},"winner":true,"stats":null,"teamId":28829}]},"coverage":"{\"data\":{\"postgame\":{\"server\":{\"status\":\"available\"},\"api\":{\"status\":\"unsupported\"}},\"realtime\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"}},\"live\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"},\"cv\":{\"status\":\"unknown\"}}},\"av\":{\"status\":\"unknown\"}}"},{"id":261061,"matchesd":{"id":261061,"lifecycle":"over","order":3,"series":{"id":147782},"map":null,"deleted_at":null,"game":{"id":1},"participants":[{"seed":1,"score":11,"forfeit":false,"roster":{"id":29283},"winner":false,"stats":null,"teamId":29283},{"seed":2,"score":25,"forfeit":false,"roster":{"id":28829},"winner":true,"stats":null,"teamId":28829}]},"coverage":"{\"data\":{\"postgame\":{\"server\":{\"status\":\"available\"},\"api\":{\"status\":\"unsupported\"}},\"realtime\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"}},\"live\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"},\"cv\":{\"status\":\"unknown\"}}},\"av\":{\"status\":\"unknown\"}}"},{"id":261075,"matchesd":{"id":261075,"lifecycle":"over","order":4,"series":{"id":147782},"map":null,"deleted_at":null,"game":{"id":1},"participants":[{"seed":1,"score":37,"forfeit":false,"roster":{"id":29283},"winner":true,"stats":null,"teamId":29283},{"seed":2,"score":36,"forfeit":false,"roster":{"id":28829},"winner":false,"stats":null,"teamId":28829}]},"coverage":"{\"data\":{\"postgame\":{\"server\":{\"status\":\"available\"},\"api\":{\"status\":\"unsupported\"}},\"realtime\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"}},\"live\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"},\"cv\":{\"status\":\"unknown\"}}},\"av\":{\"status\":\"unknown\"}}"},{"id":261097,"matchesd":{"id":261097,"lifecycle":"over","order":5,"series":{"id":147782},"map":null,"deleted_at":null,"game":{"id":1},"participants":[{"seed":1,"score":23,"forfeit":false,"roster":{"id":29283},"winner":true,"stats":null,"teamId":29283},{"seed":2,"score":11,"forfeit":false,"roster":{"id":28829},"winner":false,"stats":null,"teamId":28829}]},"coverage":"{\"data\":{\"postgame\":{\"server\":{\"status\":\"available\"},\"api\":{\"status\":\"unsupported\"}},\"realtime\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"}},\"live\":{\"server\":{\"status\":\"unsupported\"},\"api\":{\"status\":\"unknown\"},\"cv\":{\"status\":\"unknown\"}}},\"av\":{\"status\":\"unknown\"}}"}],"casters":[{"primary":false,"caster":{"id":3434},"casterContent":[{"id":2175456,"username":"wxcstudio","display_name":"","status_text":"","viewer_count":0,"online":false,"last_online":"2018-01-07T23:04:07Z","images":[{"id":0,"type":"external","url":"","thumbnail":"","fallback":false},{"id":0,"type":"external","url":"","thumbnail":"","fallback":false}],"platform":{"id":1,"name":"Twitch","color":"6441A5","images":[{"id":1967,"type":"default","url":"https:\/\/img.abiosgaming.com\/other\/platform-logo-twitch.png","thumbnail":"https:\/\/img.abiosgaming.com\/other\/thumbnails\/platform-logo-twitch.png","fallback":false}]}}]},{"primary":false,"caster":{"id":2552},"casterContent":[{"id":1196496,"username":"thedreamcasters","display_name":"","status_text":"","viewer_count":0,"online":false,"last_online":"2018-01-17T16:25:08Z","images":[{"id":0,"type":"external","url":"","thumbnail":"","fallback":false},{"id":0,"type":"external","url":"","thumbnail":"","fallback":false}],"platform":{"id":1,"name":"Twitch","color":"6441A5","images":[{"id":1967,"type":"default","url":"https:\/\/img.abiosgaming.com\/other\/platform-logo-twitch.png","thumbnail":"https:\/\/img.abiosgaming.com\/other\/thumbnails\/platform-logo-twitch.png","fallback":false}]}}]},{"primary":false,"caster":{"id":2753},"casterContent":[{"id":1544124,"username":"froggedtv","display_name":"FroggedTV","status_text":"[FR] Retour à 17h avec Voja","viewer_count":30,"online":true,"last_online":"2018-01-07T20:58:08Z","images":[{"id":0,"type":"external","url":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_froggedtv-640x360.jpg","thumbnail":"","fallback":false},{"id":0,"type":"external","url":"https:\/\/static-cdn.jtvnw.net\/jtv_user_pictures\/7f207769538c49d2-profile_image-300x300.jpeg","thumbnail":"","fallback":false}],"platform":{"id":1,"name":"Twitch","color":"6441A5","images":[{"id":1967,"type":"default","url":"https:\/\/img.abiosgaming.com\/other\/platform-logo-twitch.png","thumbnail":"https:\/\/img.abiosgaming.com\/other\/thumbnails\/platform-logo-twitch.png","fallback":false}]}}]},{"primary":true,"caster":{"id":1111},"casterContent":[{"id":93506,"username":"moonducktv","display_name":"","status_text":"","viewer_count":0,"online":false,"last_online":"2018-02-07T02:58:08Z","images":[{"id":0,"type":"external","url":"","thumbnail":"","fallback":false},{"id":0,"type":"external","url":"","thumbnail":"","fallback":false}],"platform":{"id":1,"name":"Twitch","color":"6441A5","images":[{"id":1967,"type":"default","url":"https:\/\/img.abiosgaming.com\/other\/platform-logo-twitch.png","thumbnail":"https:\/\/img.abiosgaming.com\/other\/thumbnails\/platform-logo-twitch.png","fallback":false}]}}]}],"has_incident_report":false}';
        $dataArr = json_decode($datas,true);
            if (!empty($dataArr['casters'])){
                foreach ($dataArr['casters'] as $castk => $castv){
                    if (!empty($castv['casterContent'])){
                        foreach ($castv['casterContent'] as $castKK => $castVV){
                            if ($castVV['platform']['name'] == 'Twitch') {
                                $liveUrl = 'https://www.twitch.tv/'.$castVV['username'];
                                $data['streamer'] = $castVV['display_name'] ?: null;
                                $data['title'] = $castVV['status_text'] ?: null;
                                $data['viewers'] = $castVV['viewer_count'] ?: null;
                                if ($castv['primary'] === true) {
                                    Common::addStandardDataMatchVideo($liveUrl,'',true,2, 3, 147782,Consts::ORIGIN_ABIOS,$data);

                                }else{
                                    Common::addStandardDataMatchVideo($liveUrl,'',false,2, 3, 147782,Consts::ORIGIN_ABIOS,$data);
                                }


                            }

                        }

                    }

                }

            }
    }

    public function testaaaa(){

    StreamService::grabHltvOrigin();
//        StreamService::upSaveUrl();

    }


}