<?php

use app\modules\common\models\EnumCountry;
use app\modules\common\services\Consts;
use app\modules\data\models\StandardDataMetadata;
use app\modules\file\services\UploadService;
use app\modules\task\models\MatchComingScoket;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\abios\v3\AbiosBase;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;

/**
 *
 */
class AbiosTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//         \app\modules\task\services\TaskRunner::run(15);  //
         \app\modules\task\services\TaskRunner::run(1688982);  //
    }
    public function testRunStart()
    {

//        \app\modules\task\services\grab\abios\Start::run();
    }
    //刷新一条推荐关联关系
    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_METADATA,[16356],[],'lol_item');

    }

    public function testTeam()
    {
        $teamInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosTeam::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB, Consts::ORIGIN_ABIOS, Consts::RESOURCE_TYPE_TEAM),
            "params" => [
                "action" => "/teams/e",
                "params" => [
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }
    public function testTeamOne()
    {
        $taskInfo = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","","history_players"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTeamList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/teams",
                "params"=>[
                    "filter"=>"id=27701"
                ]
            ],
        ];
        TaskRunner::addTask($taskInfo, 3);
    }
    public function testPlayer()
    {
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","refresh"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/players",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);
    }
    public function testTournament()
    {
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","refresh"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournaments",
                "params"=>[
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 5);
    }
    //测试abios的赛事列表
    public function testAbiosMatchList()
    {
        $string_prize_pool =[
            "total"=>"$64,000",
            "first"=> "$800,00",
            "second"=> "See Info",
            "third"=> "See Info"
        ];
        $result = [];
        foreach ($string_prize_pool as $key=>$value){
            if($key == "first"){
                $trans['rank'] = "first";
                $trans['price'] = $value;
                $trans['num'] = null;
                $trans['team_id'] = null;
                $result[] = $trans;
            }
            if($key == "second"){
                $trans['rank'] = "second";
                $trans['price'] = $value;
                $trans['num'] = null;
                $trans['team_id'] = null;
                $result[] = $trans;
            }
            if($key == "third"){
                $trans['rank'] = "third";
                $trans['price'] = $value;
                $trans['num'] = null;
                $trans['team_id'] = null;
                $result[] = $trans;
            }
        }
        if($result){
            $a = json_encode($result);
        }
    }
    public function testTree()
    {
        $res = \app\modules\tournament\services\TournamentService::getTournametTreesList(79);
//        echo json_encode($res);
    }
    //刷新推荐绑定
    public function testabc()
    {
        $resourceIds=StandardDataMetadata::find()->select(['id','metadata_type'])->where(['and',
            ['<>','deal_status',4],
            ['=','metadata_type','dota2_item'],
        ])->asArray()->all();
        foreach($resourceIds as $val){
            StandardMasterRelationExpectService::refresh(Consts::RESOURCE_TYPE_METADATA,[$val['id']],[],$val['metadata_type']);
        }
    }
    public function testRefresh()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60*24*5);  //抓取五分钟以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
            "batch_id" => date("YmdHis"),
            "params" => [
//                "action" => "/deletions",
                "action" => "/incidents",
                "params" => [
                    "since" =>$incidentTime,
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }

    public function testFunction()
    {

        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,"incidents"),
            "params" => [
                "action" => "/tournaments",
                "params" => [
                    "filter"=>"end=null",
                    "skip" =>0,
                    "take" =>50
                ],
            ],
        ];
        TaskRunner::addTask($item, 3);

    }
    //排序方法
    function sort_for_arrays($array, $key, $sort = SORT_ASC)
    {
        $keys = array_column($array,$key);
        array_multisort($keys, $sort, $array);
        return $array;
    }
    //执行线上参赛条件迁移记录
    public function testTeamRelation()
    {
        $tournamentAll = \app\modules\tournament\models\Tournament::find()->select('id')->asArray()->all();
        foreach ($tournamentAll as $key=>$value){
            $old = \app\modules\tournament\models\TournamentTeamRelation::find()->select('team_id,type,match_condition,match_condition_cn,team_sort,tournament_id')
                ->where(['and',
                    ['<>','team_id',''],
                    ['=','tournament_id',$value['id']],
                ])->asArray()->all();
            if($old){
                foreach ($old as $ke=>$val){
                    $new['team_id'] = $val['team_id'];
                    $new['type'] = $val['type'];
                    $new['match_condition'] = $val['match_condition'];
                    $new['match_condition_cn'] = $val['match_condition_cn'];
                    $new['team_sort'] = $val['team_sort'];
                    $arrayll[] = $new;
                }
                $a = json_encode($arrayll);
                \app\modules\tournament\models\Tournament::updateAll(['teams_condition'=>$a],['id' => $value['id']]);
            }
        }
        $old = \app\modules\tournament\models\TournamentTeamRelation::find()->select('team_id,type,match_condition,match_condition_cn,team_sort,tournament_id')
            ->where(['<>','match_condition',''])->groupBy('tournament_id')->asArray()->all();

//        $oldd = \Yii::$app->db->createCommand('select team_id,type,match_condition,match_condition_cn,team_sort,tournament_id from tournament_team_relation where match_condition <> "" group by tournament_id')->execute();
    }
    public function testD()
    {
        $a = "";
        $b = json_decode($a,true);
        if(empty($b) || count($b) <= 0){
            echo "空";
        }else{
            echo "非空";
        }
        if($a){
            echo "1";
        }else{
            echo "2";
        }
    }
    public function testCound()
    {
        $countries = EnumCountry::find()->select('id,two')->asArray()->all();
        $diff = [];
        if($countries){
            foreach ($countries as $key=>$value){
                $connection  = Yii::$app->get('db');
                $sql = "select * from abios_country where short_name='".$value['two']."'";
                $command = $connection->createCommand($sql);
                $res  = $command->queryOne();
                if($res){
                    continue;
                }else{
                    $diff[] = $value;
                }
            }
            $a = json_encode($diff);
        }
        var_dump($diff);
    }

    public function testCound1()
    {
        $connection  = Yii::$app->get('db');
        $sql = "select * from abios_country";
        $command = $connection->createCommand($sql);
        $countries  = $command->queryAll();
        $diff = [];
        if($countries){
            foreach ($countries as $key=>$value){
                $one = EnumCountry::find()->select('id,two')->where(['two'=>$value['short_name']])->asArray()->one();
                if($one['two'] == $value['short_name']){
                    continue;
                } else{
                    $diff[] = $value;
                }
            }
            $a = json_encode($diff);
        }
        var_dump($diff);
    }
    public function testAbcd()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);  //抓取五分钟以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
//        $incidentTime ="2020-11-14T03:04:50Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\abios\v3\AbiosIncidents::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,"incidents"),
            "params" => [
                "action" => "/incidents",
                "params" => [
                    "order" =>"id-desc",
                    "skip" =>0,
                    "take" =>50
                ],
                "since" =>$incidentTime,
            ],
        ];
        TaskRunner::addTask($item, 5);
    }
    public function testAliyunImage()
    {
        $url = "https://img.abiosgaming.com/events/tournamentDarkHorse-large.jpg";
        $localPath = \app\modules\common\services\ImageConversionHelper::downfile($url);
        $fileName = basename($localPath);

        $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
        if($ext == "jpg"){ //如果事jpg，需要转
            $imageResource = imagecreatefromjpeg($localPath);
            $postion = strrpos($localPath,'.',0);
            $localPath = substr($localPath,0,$postion).'.png';
            $res = imagepng($imageResource,$localPath);
            $ext = "png";
            if($res){
                $dst = 'transfer_station/' . md5(time()) . '/'  . md5($fileName).'.'.$ext;
                $aliUrl = UploadService::upload($dst, $res);
            }
        }
//        $dst = 'transfer_station/' . md5(time()) . '/'  . md5($fileName).'.'.$ext;
//
//        $aliUrl = UploadService::upload($dst, $localPath);
    }
    public function testAbssb()
    {
        $url = "https://img.abiosgaming.com/events/ESEAtournamentsquare.jpg";
        $a = \app\modules\common\services\ImageConversionHelper::mvImageToAli($url);
        print_r($a);
    }
    public function testAllWhereMatch(){
//        $endTimeData = date('Y-m-d H:i:s', strtotime('-9 hours'));
//        $endTimeEx = explode(' ',$endTimeData);
//        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);
//
//        //刚结束（结束时间1小时内））
//        $grebEndTime=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
//            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
//            "params"=>[
//                "action"=>"/series",
//                "params"=>[
//                    "filter"=>"lifecycle=over,end>=$endTime",/**/
//                    "skip" =>0,
//                    "take" =>50
//                ]
//            ],
//
//        ];
//        TaskRunner::addTask($grebEndTime, 1);
//
//        $live=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
//            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
//            "params"=>[
//                "action"=>"/series",
//                "params"=>[
//                    "filter"=>"lifecycle=live",/**/
//                    "skip" =>0,
//                    "take" =>50
//                ]
//            ],
//
//        ];
//        TaskRunner::addTask($live, 1);
//
//
//        $upcoming=[
//            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "","","",""),
//            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
//            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
//            "params"=>[
//                "action"=>"/series",
//                "params"=>[
//                    "filter"=>"lifecycle=upcoming",/**/
//                    "skip" =>0,
//                    "take" =>50
//                ]
//            ],
//
//        ];
//        TaskRunner::addTask($upcoming, 1);

        $unknown=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>5
                ]
            ],

        ];
        TaskRunner::addTask($unknown, 1);

    }
    public function testAllMatchUpcoming()
    {

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 1);
    }
    //比赛
    public function testovetEndTimeMatch()
    {

        $endTimeData = date('Y-m-d H:i:s', strtotime('-32 hours'));
        $endTimeEx = explode(' ',$endTimeData);
        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=over,end>=$endTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }
    public function test12()
    {
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/player",
                "params"=>[
                    "page"=>1
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }
    public function testTeamexception()
    {
         TaskRunner::runTaskInfoExceptionData(5,"","ctb_abios_team__20201215174340");
    }
    public function testRunException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-3 hour"));
        $timeSub2 = date("Y-m-d H:i:s",strtotime("-1 hour"));
        $nowTime = date("Y-m-d H:i:s");
        $taskInfo = \app\modules\task\models\TaskInfo::find()->where(['and',
            ['=','status',5],
            ['=','run_type',5],
        ]);
        $taskInfo->andWhere(['or',
            ['and',['>=','created_time',$timeSub],
                 ['<=','created_time',$nowTime]
            ],
            ['and',['>=','begin_time',$timeSub2],
                ['<=','begin_time',$nowTime]
            ]
        ]);
        $taskInfoArray = $taskInfo->asArray()->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                TaskRunner::run($value['id']);
            }
        }
    }
    public function testAbiosMatch()
    {
        $endTimeData = date('Y-m-d H:i:s', strtotime('-32 hours'));
        $endTimeEx = explode(' ',$endTimeData);
        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);

        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=over,end>=$endTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);
    }
    public function testB()
    {
    }

}