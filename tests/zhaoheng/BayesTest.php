<?php

use app\modules\common\models\EnumCountry;
use app\modules\common\models\HelperImageToAli;
use app\modules\common\models\UploadImageLog;
use app\modules\common\services\Consts;
use app\modules\common\services\HelperResourceFormat;
use app\modules\common\services\ImageConversionHelper;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\DataTodoService;
use app\modules\file\services\UploadService;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\MatchService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\MatchComingScoket;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\abios\v3\AbiosBase;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use yii\console\ExitCode;

/**
 *
 */
class BayesTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//         \app\modules\task\services\TaskRunner::run(15);  //
         \app\modules\task\services\TaskRunner::run(10564173);  //
    }
    public function testRunStart()
    {

//        \app\modules\task\services\grab\abios\Start::run();
    }
    //刷新一条推荐关联关系
    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_TOURNAMENT,[],[71],'tournament');

    }
    public function testTeamUrl()
    {
        $teamInfo = \app\modules\task\services\grab\bayes\BayesBase::getCurlInfo("team/",[]);
        if($teamInfo){
            $teamArray = json_decode($teamInfo,true);
        }
    }
    public function testTeam()
    {

        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","","allTeam"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/title",
                "team"=>"allTeam",
                "params"=>[
                    "page"=>1
                ]
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTeama()
    {

        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","refresh","allTeam"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTeamList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/team",
                "team"=>"allTeam",
                "params"=>[
                    "title"=>10,
                    "page"=>1
                ],
                "game_id"=>10
            ],

        ];
        TaskRunner::addTask($playerInfo, 1);
    }
    public function testTeamIncidents()
    {
        $titleList = Common::getBayesTitleList();
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM);
        foreach ($titleList as $titleValue){
            $nowTime = time()-60*60*2; //取最近俩小时内更新的数据
            $item = [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","","incidents"),
                "type"=>\app\modules\task\services\grab\Bayes\BayesTeamList::class,
                "batch_id" => $batchId,
                "params"=>[
                    "action"=>"/team",
                    "params"=>[
                        'title' => $titleValue['id'],
                        "ordering"=>"-last_modified",
                        "page"=>1
                    ],
                    'game_id' =>$titleValue['id'],
                    "incidents_time"=>$nowTime,
                ],
            ];
            TaskRunner::addTask($item, 1);
        }
    }

    /**
     * 选手全量
     */
    public function testPlayer()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"",""),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/player",
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testIncidentsPlayer()
    {
        $nowTime = time()-60*30; //半天，三页
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/player",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testOnePlayer()
    {
        $taskInfo = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","refresh"),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayer::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
            'params' => [
                "action" => "/player/100000",
            ],
        ];
        TaskRunner::addTask($taskInfo, 3);
    }
    public function testoo()
    {
//        $responseInfo['detail']['Invalid page.'];
        $responseInfo['detail'] = 'Not found.';
    }
    public function testMatch()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","refresh"),
            "type"=>\app\modules\task\services\grab\bayes\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "page"=>1,
                    "title"=>7
                ]
            ],
        ];
        TaskRunner::addTask($playerInfo,1);
    }
    public function testMatchIncidents()
    {
        $nowTime = time()-60*60*24*7;
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTournament()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"","","all"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "page"=>1,
//                    "ordering"=>"-last_modified"
                ]
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTournamentIncidents()
    {
        $nowTime = time()-60*60*12;  //半天，七条
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"",""),
            "type"=>\app\modules\task\services\grab\bayes\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }

    public function testaa()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 1day'));
        $matchStreamList = MatchRealTimeInfo::find()->alias('real')->select(['real.id'])
            ->leftJoin('match','match.id = real.id')
            ->where(['or',
                ['=','status',1],
                ['=','status',2],
            ])->andWhere(['>=','match.scheduled_begin_at',$dateTime])->asArray()->all();
    }
    public function testb()
    {
//        $identifiers = [
//            ['type'=>'faceit_team_id',
//             'value'=>'6011c0e9-2074-4c95-ac54-9aeceffb1c6c',
//            ],
//            ['type'=>'faceit_team_id',
//             'value'=>'6011c0e9-2074-4c95-ac54-9aeceffb1c6',
//            ],
//        ];
        $identifiers =[];
        foreach ($identifiers as $value){
            $identifierInfo = \app\modules\task\models\BayesIdentifiers::find()->where([
                'resource_type'=>Consts::RESOURCE_TYPE_TEAM,
                'rel_identity_id'=>(string)8980,
                'type'=>$value['type'],
                'value'=> $value['value'],
            ])->one();
            if ($identifierInfo) {
                continue;
            }else{
                $identifierInfo = new \app\modules\task\models\BayesIdentifiers();
                $identifierInfo->setAttributes(
                    [
                        'resource_type'=>Consts::RESOURCE_TYPE_TEAM,
                        'rel_identity_id'=>(string)8980,
                        'type'=>$value['type'],
                        'value'=> $value['value'],
                    ]
                );
                $identifierInfo->save();
            }

            print_r($value);
        }
    }
    public function testvv()
    {
        $result = Common::getOriginGameById(Consts::ORIGIN_BAYES, 1);
    }
    public function testBayesImage()
    {

        $url = "https://admin.esportsdirectory.info//v1/assets/9";
        if(strpos($url,"https://admin.esportsdirectory.info") !== false){
//            $accessToken = \app\modules\task\services\grab\bayes\BayesBase::getAccessToken();
//            $header=[
//                "Content-Type"=>'application/json;charset=utf-8',
//                "Authorization"=>"Bearer ".$accessToken
//            ];
//            $imageStream = Common::requestGet($url,$header,"");
//            $im = imagecreatefromstring($imageStream);
//            $type = getimagesizefromstring($imageStream);
//            if($im){
//                $localPath = \app\modules\common\services\ImageConversionHelper::downBayesFile($url,$imageStream,$type['mime']);
//            }
            $localPath = \app\modules\common\services\ImageConversionHelper::downBayesFile($url);

        }
         else{
             $localPath=\app\modules\common\services\ImageConversionHelper::downfile($url);
         }
            $fileName = basename($localPath);

            $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
            if($ext == "jpg" || $ext == "jpeg"){
                $imageResource = imagecreatefromjpeg($localPath);
                $postion = strrpos($localPath,'.',0);
                $localPath = substr($localPath,0,$postion).'.png';
                $res = imagepng($imageResource,$localPath);
                if($res){
                    $ext = "png";
                }
//                else{
//                    throw new BusinessException($info->getErrors(), '转存png失败');
//                }
            }
            $dst = 'transfer_station/' . md5(time()) . '/'  . md5($fileName).'.'.$ext;
//            $dst = 'transfer_station/' . md5(time()) . '/' . $fileName;
            $aliUrl = UploadService::upload($dst, $localPath);
            // 上传到ali,
            $info = new HelperImageToAli();
            $info->setAttributes([
                'origin_url' => $url,
                'ali_url' => $aliUrl,
                'identity_key' => md5($url)
            ]);
            if (!$info->save()) {
                throw new BusinessException($info->getErrors(), '转存失败');
            }
            // 存数据库
//        }
        return $info['ali_url'];
    }
    public function testdd()
    {
        $enumService = \app\modules\common\services\EnumService::getCountryInfoByEName('Europe');
    }
    public function testHaveVideo()
    {
        $matchStreamList = MatchRealTimeInfo::find()->alias('match_real')
            ->select(["match_real.id as match_id"])
            ->innerJoin('match_stream_list as msl','match_real.id = msl.match_id')
            ->where(
                ['or',
                    ['=','match_real.status',1],
                    ['=','match_real.status',2],
                ])->groupBy('msl.match_id')->asArray()->all();
        foreach ($matchStreamList as $value){
//            $matchStreamModel = MatchRealTimeInfo::find()->where(['id'=>$value['match_id']])->asArray()->one();
            MatchService::setMatchRealData($value['match_id'], ['is_streams_supported'=> 1], Consts::USER_TYPE_ROBOT, 0, 0, 0, 0);
//            MatchService::setMatch($attribute, Consts::USER_TYPE_ROBOT, 0, $basisId = 0, $originId = 0);
        }
    }
    public function testException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-3 hour"));
        $timeSub2 = date("Y-m-d H:i:s",strtotime("-1 hour"));
        $nowTime = date("Y-m-d H:i:s");
        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',5],
//            ['=','run_type',5],
        ]);
        $taskInfo->andWhere(['or',
            ['and',['>=','created_time',$timeSub],
                ['<=','created_time',$nowTime]
            ],
            ['and',['>=','begin_time',$timeSub2],
                ['<=','begin_time',$nowTime]
            ]
        ]);
        $sqlString = $taskInfo->createCommand()->getRawSql();
        $taskInfoArray = $taskInfo->asArray()->all();
    }
    public function testRunTaskInfoException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-30 minute"));
        $nowTime = date("Y-m-d H:i:s");

        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',2],
            ['=','run_type',1],
            ["like", "tag", "bayes.team"],
            ["like", "tag", "incidents"],
        ]);
        $taskInfo->andWhere(['or',
            ['is','response',null],
            ['=','response',""],
         ]);
        $taskInfo->andWhere(['and',
            ['>=','created_time',$timeSub],
            ['<=','created_time',$nowTime]
        ]);
        $taskInfoArray = $taskInfo->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                $value->setAttribute('status',1);
                $value->setAttribute('error_info',null);
                $value->save();
                TaskRunner::run($value['id']);
            }
        }
    }
    public function testooe()
    {
        $chars =['token1','token2'];
        $key = array_rand($chars,1);
        $token = $chars[$key];
        if($token == 'token1'){
            echo 1;
        }
        if($token == 'token2'){
            echo 2;
        }
    }
    public function testMatchUpcoming()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*20);  //取未开始的比赛+已结束20分钟的比赛
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTournamentUpcoming()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8);  //取未开始的赛事
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"","","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    public function testMainSteamIdChange()
    {
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerCsgo::find()->select(['player_id','steam_id'])->where(['not',['steam_id'=>null]])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\task\models\StandardDataPlayer::find()->alias('std')->select(['std.id','std.origin_id','std.rel_identity_id','std.steam_id'])
                ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['dsmr.master_id'=>$itemPlayer['player_id']])
                ->andWhere(['std.origin_id'=>7])
                ->andWhere(['or',
                    ['std.steam_id'=>null],
                    ['std.steam_id'=>""]
                ])->one();
            if($result){
                $diffInfo = \app\modules\task\services\originIncrement\OriginRunBase::setStandardData(7, Consts::RESOURCE_TYPE_PLAYER, 1, $result['rel_identity_id'], ['steam_id'=>$itemPlayer['steam_id']]);
                $sub = \app\modules\task\services\originIncrement\OriginRunBase::getSubStep($diffInfo, Consts::ORIGIN_HLTV, Consts::RESOURCE_TYPE_PLAYER, 1);
                foreach ($sub as $key => $val) {
                    TaskRunner::addTask($val, 1);
                }
            }
        }
    }
    public function testMatchHaveVideo()
    {
        $matchStreamList = MatchRealTimeInfo::find()->alias('match_real')
            ->select(["match_real.id as match_id","count(*) as video_count",])
            ->innerJoin('match_stream_list as msl','match_real.id = msl.match_id')
            ->where(['or',
                    ['=','match_real.status',1],
                    ['=','match_real.status',2],
                ])->groupBy('msl.match_id')->asArray()->all();
        foreach ($matchStreamList as $value){
            MatchService::setMatchRealData($value['match_id'], ['is_streams_supported'=> 1,'video_count'=>(int)$value['video_count']], Consts::USER_TYPE_ROBOT, 0, 0, 0, 0);
        }
    }

    public function testAbc()
    {
        \app\modules\task\services\operation\OperationRefreshHotInfo::refreshList();
    }
    public function testc()
    {
        $a = 1613582700; //a<b
        $b = 1612427693;
        $a = date("Y-m-d H:i:s",$a);
        $b = date("Y-m-d H:i:s",$b);
    }
    public function testg()
    {
        $a = \app\modules\data\services\DataBindingService::getToDoBindingList();
    }
    public function testBindingStatisticsByOrigin()
    {
        \app\modules\data\services\DataBindingService::BindingStatisticsByOrigin();
    }
    public function testk()
    {
        $a = date("Y-m-d",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-7,date("Y")));
        $b = date("Y-m-d",mktime(23,59,59,date("m"),date("d")-date("w")+7-7,date("Y")));
        $d = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y")));
        $e =  date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y")));
        $f = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+8,date("Y")));
        $g =  date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")-date("w")+14,date("Y")));
        $h = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-1,1,date("Y")));
        $i = date("Y-m-d H:i:s",mktime(23,59,59,date("m") ,0,date("Y")));
        //本月
        $aa = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y")));
        $bb = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y")));
        //下月
        $cc = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),+1,date("Y")));
        $x = date("Y-m-d H:i:s");
        $y = Common::getNextMonthDays($x);
    }
    public function testDb()
    {
        $standardTeam = StandardDataTeam::find()->where(['=', 'id', 133])->one();
        $statusInfo = [
            'deal_status' => 2, // 有多个推荐待绑定
        ];
        $a = $standardTeam->updateAll($statusInfo,['and',['=','id',$standardTeam['id']],['<>','deal_status',3]]);
    }
    public function testRefreshPlayer()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function testhh()
    {
        $arr = [49106,55213,2014,73343,18467];
        $a = 0;
        foreach ($arr as $value){
            $a = $a+$value;
        }
    }
    public function testi()
    {
        $new = new TaskInfo();
        $new->setAttribute('created_time','2');
        if (!$new->save()) {
//            throw new BusinessException($new->getErrors());
            print_r("abc".$new->getErrors());
        }
    }
    /**
     * match_battle_player_dota2表steam_id变化触发的主表任务(赵恒)
     */
    public function testActionStandardSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerDota2::find()->select(['player_id','steam_id'])->where(['steam_id'=>'76561198197124361'])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\task\models\StandardDataPlayer::find()->alias('std')->select(['std.id','std.origin_id','std.rel_identity_id','std.steam_id'])
                ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['dsmr.master_id'=>$itemPlayer['player_id']])
                ->andWhere(['std.origin_id'=>9])->one();
//            if($result['steam_id'] != $itemPlayer['steam_id']){
            if($result){
                $diffInfo = \app\modules\task\services\originIncrement\OriginRunBase::setStandardData(9, Consts::RESOURCE_TYPE_PLAYER, 3, $result['rel_identity_id'], ['steam_id'=>$itemPlayer['steam_id']]);
                $sub = \app\modules\task\services\originIncrement\OriginRunBase::getSubStep($diffInfo, Consts::ORIGIN_BAYES, Consts::RESOURCE_TYPE_PLAYER, 3);
                foreach ($sub as $key => $val) {
                    TaskRunner::addTask($val, 3);
                }
            }
        }
    }

    public function testActionMasterSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerDota2::find()->select(['player_id','steam_id'])->distinct()->where(['not',['steam_id'=>null]])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\org\models\Player::find()->alias('p')->select(['p.id','p.steam_id','p.game_id','dsmr.standard_id as standard_id','std.origin_id'])
                ->innerJoin('data_standard_master_relation as dsmr','p.id = dsmr.master_id')
                ->innerJoin('standard_data_player as std','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['p.id'=>$itemPlayer['player_id']])->asArray()->one();
            if($result['steam_id'] !=$itemPlayer['steam_id'] ){
                $oldInfo = [
                    'id'=> $result['id'],
                    'steam_id'=> $result['steam_id']
                ];
                $newInfo = [
                    'id'=> $itemPlayer['player_id'],
                    'steam_id'=> $itemPlayer['steam_id']
                ];
                $diffInfo = Common::getDiffInfo($oldInfo, $newInfo);
                $diff=$diffInfo["diff"];
                DataTodoService::addToDo(Consts::RESOURCE_TYPE_PLAYER, $result['id'], Consts::TAG_TYPE_CORE_DATA, $diff, $result['standard_id'], null, 2);
            }
        }
    }
    //上传csgomap
    public function testMetadataCsgoMap()
    {
        $masterDatas = \app\modules\metadata\models\MetadataCsgoMap::find()->where(['<>','square_image',''])->andWhere(['not like','square_image','elmt.oss-cn-hongkong.aliyuncs.com'])->all();
        foreach ($masterDatas as $value){
            $square_image = HelperImageToAli::find()->where(['ali_url'=>$value['square_image']])->one();
            $rectangle_image = HelperImageToAli::find()->where(['ali_url'=>$value['rectangle_image']])->one();
            $thumbnail = HelperImageToAli::find()->where(['ali_url'=>$value['thumbnail']])->one();
            if($square_image['ali_url'] && !$square_image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($square_image['ali_url']);
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $square_image->setAttributes($insertData);
                $square_image->save();
            }elseif($square_image['hk_url']){
                $aliUrl = $square_image['hk_url'];
            }else{
                if($value['square_image']){
                    $aliUrl = self::mvImageToAli($value['square_image']);
                }
            }
            if($rectangle_image['ali_url'] && !$rectangle_image['hk_url']){
                $rectangleImageAliUrl = self::mvImageToAli($rectangle_image['ali_url']);
                $insertData = [
                    'hk_url' =>$rectangleImageAliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $rectangle_image->setAttributes($insertData);
                $rectangle_image->save();
            }elseif ($rectangle_image['hk_url']){
                $rectangleImageAliUrl = $rectangle_image['hk_url'];
            }else{
                if($value['rectangle_image']){
                    $rectangleImageAliUrl = self::mvImageToAli($value['rectangle_image']);
                }
            }

            if($thumbnail['ali_url'] && !$thumbnail['hk_url']){
                $thumbnailAliUrl = self::mvImageToAli($thumbnail['ali_url']);
                $insertData = [
                    'hk_url' =>$thumbnailAliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $thumbnail->setAttributes($insertData);
                $thumbnail->save();
            }elseif ($thumbnail['hk_url']){
                $thumbnailAliUrl = $thumbnail['hk_url'];
            }else{
                if($value['thumbnail']){
                    $thumbnailAliUrl = self::mvImageToAli($value['thumbnail']);
                }
            }
            $value->setAttributes([
                'square_image'=>$aliUrl,
                'rectangle_image'=>$rectangleImageAliUrl,
                'thumbnail'=>$thumbnailAliUrl,
            ]);
            $value->save();
        }
    }
    //上传csgo武器,dota2ability
    public function testMetadataCsgoWeapon()
    {
//        $masterDatas = \app\modules\metadata\models\MetadataCsgoWeapon::find()->where(['and',
//            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
//         ])->all(); //csgo武器
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Ability::find()->where(['and',
//            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
//        ])->all(); //
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Item::find()->where(['and',
//            ['<>','image',''],
//            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
//        ])->all(); //
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Map::find()->where(['and',
//            ['<>','image',''],
//            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
//        ])->all(); //
        $masterDatas = \app\modules\tournament\models\Tournament::find()->where(['and',
            ['like','image','oss-gamecenter-prd.oss-cn-beijing.aliyuncs.com']
        ])->all();
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Ability::find()->all(); //dota2ability
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Item::find()->all(); //dota2item
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Map::find()->all(); //dota2map
//        $masterDatas = \app\modules\metadata\models\MetadataLolAbility::find()->all(); //lolability
//        $masterDatas = \app\modules\metadata\models\MetadataLolItem::find()->asArray()->all(); //lolitem
//        $masterDatas = \app\modules\metadata\models\MetadataLolMap::find()->asArray()->all(); //lolmap
//        $masterDatas = \app\modules\metadata\models\MetadataLolRune::find()->asArray()->all(); //lolrune
//        $masterDatas = \app\modules\metadata\models\MetadataLolSummonerSpell::find()->asArray()->all(); //lolspell
//        $masterDatas = Team::find()->where(['and',
//            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
//        ])->all(); //team
//        $masterDatas = \app\modules\org\models\Player::find()->asArray()->all(); //player
//        $masterDatas = \app\modules\tournament\models\Tournament::find()->asArray()->all(); //tournament
        static $i = 0;
        foreach ($masterDatas as $value){
            $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
            if($image['ali_url'] && !$image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($image['ali_url']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $image->setAttributes($insertData);
                $image->save();
            }elseif($image['hk_url']){
                $aliUrl = $image['hk_url'];
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
            }else{
                $aliUrl= self::mvImageToAli($value['image']);
                if($aliUrl){
                    $i=$i+1;
                }
                echo $i.PHP_EOL;
            }
            $value->setScenario('bing_tournament');
            $value->setAttributes([
                'image'=>$aliUrl
            ]);
            $value->save();
        }
        print_r($i);
    }
    //上传csgo武器,dota2ability
    public function testMetadataDota2Hero()
    {
//        $masterDatas = \app\modules\metadata\models\MetadataDota2Hero::find()->where(['and',
//            ['<>','small_image',''],
//            ['not like','small_image','elmt.oss-cn-hongkong.aliyuncs.com']
//        ])->all(); //dota2hero
        $masterDatas = \app\modules\metadata\models\MetadataLolChampion::find()->where(['and',
            ['<>','image',''],
            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
        ])->all(); //dota2hero
//        $masterDatas = \app\modules\metadata\models\MetadataLolChampion::find()->asArray()->all(); //lolchampion
        static $i = 0;
        foreach ($masterDatas as $value){
            $image = HelperImageToAli::find()->where(['ali_url'=>$value['image']])->one();
//            $small_image = HelperImageToAli::find()->where(['ali_url'=>$value['small_image']])->one();
            if($image['ali_url'] && !$image['hk_url']){  //有转存的
                $aliUrl = self::mvImageToAli($image['ali_url']);
                if($aliUrl){
                    $i=$i+1;
                }
                $insertData = [
                    'hk_url' =>$aliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $image->setAttributes($insertData);
                $image->save();
            }elseif($image['hk_url']){
                $aliUrl = $image['hk_url'];
                if($aliUrl){
                    $i=$i+1;
                }
            }else{
                $aliUrl= self::mvImageToAli($value['image']);
                if($aliUrl){
                    $i=$i+1;
                }
            }
            if($small_image['ali_url'] && !$small_image['hk_url']){  //有转存的
                $smallAliUrl = self::mvImageToAli($small_image['ali_url']);
                $insertData = [
                    'hk_url' =>$smallAliUrl,
//                    'ali_url' =>$aliUrl
                ];
                $small_image->setAttributes($insertData);
                $small_image->save();
            }elseif ($small_image['hk_url']){
                $smallAliUrl = $small_image['hk_url'];
            }else{
                $smallAliUrl= self::mvImageToAli($value['small_image']);
            }
            $value->setAttributes([
                'image'=>$aliUrl,
                'small_image'=>$smallAliUrl,
            ]);
            $value->save();
        }
        print_r($i);
    }

    public static function mvImageToAli($url)
    {
        $localPath = \app\modules\common\services\ImageConversionHelper::downfile($url);
        $fileName = basename($localPath);

        $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
        if($ext == "jpg" || $ext == "jpeg"){
            $imageResource = imagecreatefromjpeg($localPath);

            if(!$imageResource){
                $imageResource = imagecreatefrompng($localPath);
            }
            $postion = strrpos($localPath,'.',0);
            $localPath = substr($localPath,0,$postion).'.png';
            $res = imagepng($imageResource,$localPath);
            if($res){
                $ext = "png";
            }else{
                throw new BusinessException($res->getErrors(), '转存png失败');
            }
        }
//        $dst = "elmt/".substr(time(),0,5).'.'.$ext;
        $dst = "els/".base64_encode(date("ymd")).'/'.base64_encode(date("His")).rand(100,999).'.'.$ext;
        $aliOriginUrl = TaskRunner::upload($dst, $localPath);
        $aliUrl = str_replace(env("OSS_Bucket").".".env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliOriginUrl);
        $is = strstr($aliUrl, 'http');
        if($is){
            $aliUrl = str_replace("http","https",$aliUrl);
        }
        return $aliUrl;
    }
    public function testcdd()
    {
      $aliUrl = "http://elmt.oss-cn-hongkong.aliyuncs.com/els/MjEwMzI1/MjAyMjU0234.png";
//      $a = str_replace(env("OSS_Endpoint"),env("DEFINE_ALI_URL"),$aliUrl);
    }
    public function testjj()
    {
        $aliUrl = "http://oss-gamecenter-dev.oss-cn-beijing.aliyuncs.com/els/MjEwMzI4/MjMzMTQz664.png";
        $is = strstr($aliUrl, 'http');
        if($is){
            $aliUrl = str_replace("http","https",$aliUrl);
        }
    }
    public function testpp()
    {
        $a = \app\modules\task\services\grab\bayes2\BayesBase::getAuth();
    }


}