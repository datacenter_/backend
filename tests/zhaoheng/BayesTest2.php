<?php

use app\modules\common\models\EnumCountry;
use app\modules\common\models\HelperImageToAli;
use app\modules\common\models\UploadImageLog;
use app\modules\common\services\Consts;
use app\modules\common\services\HelperResourceFormat;
use app\modules\common\services\ImageConversionHelper;
use app\modules\data\models\StandardDataMetadata;
use app\modules\data\services\DataTodoService;
use app\modules\file\services\UploadService;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\MatchService;
use app\modules\org\models\Player;
use app\modules\org\models\Team;
use app\modules\task\models\MatchComingScoket;
use app\modules\task\models\StandardDataTeam;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\Common;
use app\modules\task\services\exceptions\TaskRestException;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\abios\v3\AbiosBase;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\standardIncrement\StandardMasterRelationExpectService;
use app\modules\task\services\TaskRunner;
use app\modules\task\services\UpdateConfig;
use app\rest\exceptions\BusinessException;
use yii\console\ExitCode;

/**
 *
 */
class BayesTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//         \app\modules\task\services\TaskRunner::run(15);  //
         \app\modules\task\services\TaskRunner::run(548590);  //
    }
    public function testRunStart()
    {

//        \app\modules\task\services\grab\abios\Start::run();
    }
    //刷新一条推荐关联关系
    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_TOURNAMENT,[],[71],'tournament');

    }
    public function testTeamUrl()
    {
        $teamInfo = \app\modules\task\services\grab\bayes\BayesBase::getCurlInfo("team/",[]);
        if($teamInfo){
            $teamArray = json_decode($teamInfo,true);
        }
    }
    public function testTeam()
    {

        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM,"","refresh","allTeam"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/title",
                "team"=>"allTeam",
                "params"=>[
                    "page"=>1
                ]
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTeama()
    {

        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","refresh","allTeam"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTeamList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM),
            "params"=>[
                "action"=>"/team",
                "team"=>"allTeam",
                "params"=>[
                    "title"=>10,
                    "page"=>1
                ],
                "game_id"=>10
            ],

        ];
        TaskRunner::addTask($playerInfo, 1);
    }
    public function testTeamIncidents()
    {
        $titleList = Common::getBayesTitleList();
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM);
        foreach ($titleList as $titleValue){
            $nowTime = time()-60*60*2; //取最近俩小时内更新的数据
            $item = [
                "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TEAM,"","","incidents"),
                "type"=>\app\modules\task\services\grab\Bayes\BayesTeamList::class,
                "batch_id" => $batchId,
                "params"=>[
                    "action"=>"/team",
                    "params"=>[
                        'title' => $titleValue['id'],
                        "ordering"=>"-last_modified",
                        "page"=>1
                    ],
                    'game_id' =>$titleValue['id'],
                    "incidents_time"=>$nowTime,
                ],
            ];
            TaskRunner::addTask($item, 1);
        }
    }

    /**
     * 选手全量
     */
    public function testPlayer()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","refresh","allPlayer"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTitleList::class,
//            "type"=>\app\modules\task\services\grab\bayes2\BayesPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/title",
                "player"=>"allPlayer",
                "params"=>[
                    "page"=>1
                ]
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testIncidentsPlayer()
    {
        $nowTime = time()-60*30; //半天，三页
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayerList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
            "params"=>[
                "action"=>"/player",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],

        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testOnePlayer()
    {
        $taskInfo = [
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER,"","refresh"),
            "type"=>\app\modules\task\services\grab\bayes\BayesPlayer::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_PLAYER),
            'params' => [
                "action" => "/player/100000",
            ],
        ];
        TaskRunner::addTask($taskInfo, 3);
    }
    public function testoo()
    {
//        $responseInfo['detail']['Invalid page.'];
        $responseInfo['detail'] = 'Not found.';
    }
    //比赛
    public function testMatch()
    {
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH,"",""),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ]
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    public function testMatchIncidents()
    {
        $nowTime = time()-60*60*24*7;
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","","incidents"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    //赛事
    public function testTournament()
    {
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","","all"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "page"=>1,
                    "ordering"=>"-date_end",
//                    "ordering"=>"-last_modified"
                ]
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTournamentIncidents()
    {
        $nowTime = time()-60*60*12;  //半天，七条
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"",""),
            "type"=>\app\modules\task\services\grab\bayes\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-last_modified",
                    "page"=>1
                ],
                "incidents_time"=>$nowTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }

    public function testaa()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 1day'));
        $matchStreamList = MatchRealTimeInfo::find()->alias('real')->select(['real.id'])
            ->leftJoin('match','match.id = real.id')
            ->where(['or',
                ['=','status',1],
                ['=','status',2],
            ])->andWhere(['>=','match.scheduled_begin_at',$dateTime])->asArray()->all();
    }
    public function testdd()
    {
        $enumService = \app\modules\common\services\EnumService::getCountryInfoByEName('Europe');
    }
    public function testException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-3 hour"));
        $timeSub2 = date("Y-m-d H:i:s",strtotime("-1 hour"));
        $nowTime = date("Y-m-d H:i:s");
        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',5],
//            ['=','run_type',5],
        ]);
        $taskInfo->andWhere(['or',
            ['and',['>=','created_time',$timeSub],
                ['<=','created_time',$nowTime]
            ],
            ['and',['>=','begin_time',$timeSub2],
                ['<=','begin_time',$nowTime]
            ]
        ]);
        $sqlString = $taskInfo->createCommand()->getRawSql();
        $taskInfoArray = $taskInfo->asArray()->all();
    }
    public function testRunTaskInfoException()
    {
        $timeSub = date("Y-m-d H:i:s",strtotime("-30 minute"));
        $nowTime = date("Y-m-d H:i:s");

        $taskInfo = TaskInfo::find()->where(['and',
            ['=','status',2],
            ['=','run_type',1],
            ["like", "tag", "bayes.team"],
            ["like", "tag", "incidents"],
        ]);
        $taskInfo->andWhere(['or',
            ['is','response',null],
            ['=','response',""],
         ]);
        $taskInfo->andWhere(['and',
            ['>=','created_time',$timeSub],
            ['<=','created_time',$nowTime]
        ]);
        $taskInfoArray = $taskInfo->all();
        if($taskInfoArray){
            foreach ($taskInfoArray as $value){
                $value->setAttribute('status',1);
                $value->setAttribute('error_info',null);
                $value->save();
                TaskRunner::run($value['id']);
            }
        }
    }
    public function testooe()
    {
        $chars =['token1','token2'];
        $key = array_rand($chars,1);
        $token = $chars[$key];
        if($token == 'token1'){
            echo 1;
        }
        if($token == 'token2'){
            echo 2;
        }
    }
    public function testMatchUpcoming()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*20);  //取未开始的比赛+已结束20分钟的比赛
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $playerInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH,"","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes\BayesMatchList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_MATCH),
            "params"=>[
                "action"=>"/match",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($playerInfo, 3);
    }
    public function testTournamentUpcoming()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8);  //取未开始的赛事
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT,"","","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    public function testMatchHaveVideo()
    {
        $matchStreamList = MatchRealTimeInfo::find()->alias('match_real')
            ->select(["match_real.id as match_id","count(*) as video_count",])
            ->innerJoin('match_stream_list as msl','match_real.id = msl.match_id')
            ->where(['or',
                    ['=','match_real.status',1],
                    ['=','match_real.status',2],
                ])->groupBy('msl.match_id')->asArray()->all();
        foreach ($matchStreamList as $value){
            MatchService::setMatchRealData($value['match_id'], ['is_streams_supported'=> 1,'video_count'=>(int)$value['video_count']], Consts::USER_TYPE_ROBOT, 0, 0, 0, 0);
        }
    }
    public function testRefreshPlayer()
    {
        try {
            \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
        }catch (\Exception $e){
            $error_info = [
                "msg" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "code" => ExitCode::UNSPECIFIED_ERROR,
            ];
            return $error_info;
        }
    }
    public function testhh()
    {
        $arr = [49106,55213,2014,73343,18467];
        $a = 0;
        foreach ($arr as $value){
            $a = $a+$value;
        }
    }
    public function testi()
    {
        $new = new TaskInfo();
        $new->setAttribute('created_time','2');
        if (!$new->save()) {
//            throw new BusinessException($new->getErrors());
            print_r("abc".$new->getErrors());
        }
    }
    /**
     * match_battle_player_dota2表steam_id变化触发的主表任务(赵恒)
     */
    public function testActionStandardSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerDota2::find()->select(['player_id','steam_id'])->where(['steam_id'=>'76561198197124361'])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\task\models\StandardDataPlayer::find()->alias('std')->select(['std.id','std.origin_id','std.rel_identity_id','std.steam_id'])
                ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['dsmr.master_id'=>$itemPlayer['player_id']])
                ->andWhere(['std.origin_id'=>9])->one();
//            if($result['steam_id'] != $itemPlayer['steam_id']){
            if($result){
                $diffInfo = \app\modules\task\services\originIncrement\OriginRunBase::setStandardData(9, Consts::RESOURCE_TYPE_PLAYER, 3, $result['rel_identity_id'], ['steam_id'=>$itemPlayer['steam_id']]);
                $sub = \app\modules\task\services\originIncrement\OriginRunBase::getSubStep($diffInfo, Consts::ORIGIN_BAYES, Consts::RESOURCE_TYPE_PLAYER, 3);
                foreach ($sub as $key => $val) {
                    TaskRunner::addTask($val, 3);
                }
            }
        }
    }

    public function testActionMasterSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerDota2::find()->select(['player_id','steam_id'])->distinct()->where(['not',['steam_id'=>null]])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\org\models\Player::find()->alias('p')->select(['p.id','p.steam_id','p.game_id','dsmr.standard_id as standard_id','std.origin_id'])
                ->innerJoin('data_standard_master_relation as dsmr','p.id = dsmr.master_id')
                ->innerJoin('standard_data_player as std','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['p.id'=>$itemPlayer['player_id']])->asArray()->one();
            if($result['steam_id'] !=$itemPlayer['steam_id'] ){
                $oldInfo = [
                    'id'=> $result['id'],
                    'steam_id'=> $result['steam_id']
                ];
                $newInfo = [
                    'id'=> $itemPlayer['player_id'],
                    'steam_id'=> $itemPlayer['steam_id']
                ];
                $diffInfo = Common::getDiffInfo($oldInfo, $newInfo);
                $diff=$diffInfo["diff"];
                DataTodoService::addToDo(Consts::RESOURCE_TYPE_PLAYER, $result['id'], Consts::TAG_TYPE_CORE_DATA, $diff, $result['standard_id'], null, 2);
            }
        }
    }
    public function testTeamIncidentsbb()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60);  //取未开始的赛事+已经开始一个小时的比赛
        $upcomingTime = str_replace(" ","T",$timeNow)."Z";
        $item=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT,"","","upcoming"),
            "type"=>\app\modules\task\services\grab\bayes2\BayesTournamentList::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_BAYES2,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params"=>[
                "action"=>"/tournament",
                "params"=>[
                    "ordering"=>"-date_start",
                    "page"=>1
                ],
                "upcoming_time"=>$upcomingTime,
            ],
        ];
        TaskRunner::addTask($item, 3);
    }

}