<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DbSocket;
use app\modules\task\models\ReceiveData5eplayFormatEvents;
use app\modules\task\services\logformat\LogGenerator;
use app\modules\task\models\TaskInfo;
use app\modules\task\services\logformat\CsgoLogUCC;
use app\modules\task\services\hot\five\FiveHotCsgoMatchWs;
use \app\modules\metadata\models\MetadataCsgoMap;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;
use app\modules\task\services\api\RenovateService;
use app\modules\task\services\wsdata\bayes\Dota2Socket;
class Dota2ApiTest extends PHPUnit\Framework\TestCase
{

    public function testApi(){
        $tagInfo = null;
        $param['match_id']=45568;
        $param['currentPre']='dota_ws';
        $taskInfo['params'] = json_encode($param,true);

        \app\modules\task\services\wsdata\bayes\Dota2SocketToApi::run($tagInfo, $taskInfo);
    }




    public function testBayesTaskEventsByRefresh(){
        $matchId              = 'esports:match:4ab7748d-86b6-4309-8e42-00df5ac2e564';
//        $where                = ['match_id' => $matchId];
        $livedId = 16285097;
//        $livedId = 16556481;
        $where                = ['id' => $livedId];
        $livedData            = \app\modules\match\models\MatchLived::find()->where($where)->asArray()->one();
        $tag                  = 'websocket_match_data.9.websocket_match_data.add.3.';
        $tagInfo['ext2_type'] = 'refresh';
//        $tagInfo['ext2_type'] = 'refresh';
        $params['origin_id'] =9;
        $params['game_id'] =3;

        $params['match_id'] =45568;
        $params['rel_match_id'] =97934;
        $params['type'] ='frames';
        $params['socket_time'] =$livedData['socket_time'];
        $params['socket_id'] =$livedId;
        $params['match_data'] =json_decode($livedData['match_data'],true);
        $taskInfoArr['params'] = json_encode($params,true);
        $taskInfo = $taskInfoArr;
        Dota2Socket::run($tagInfo, $taskInfo);

    }

    public function testBayesTaskEventsBySingle(){
        $matchId              = 'esports:match:e5a0d413-c84f-4584-a371-f24cd0a3eb5c';
//        $where                = ['match_id' => $matchId];
        $livedId = 16285097;
//        $livedId = 16556481;
        $where                = ['id' => $livedId];
        $livedData            = \app\modules\match\models\MatchLived::find()->where($where)->asArray()->one();
        $tag                  = 'websocket_match_data.9.websocket_match_data.add.3.';
        $tagInfo['ext2_type'] = '';
//        $tagInfo['ext2_type'] = 'refresh';
        $params['origin_id'] =9;
        $params['game_id'] =3;
        $params['match_id'] =$matchId;
        $params['type'] ='frames';
        $params['socket_time'] =$livedData['socket_time'];
        $params['socket_id'] =$livedId;
        $params['match_data'] =json_decode($livedData['match_data'],true);
        $taskInfoArr['params'] = json_encode($params,true);
        $taskInfo = $taskInfoArr;
        Dota2Socket::run($tagInfo, $taskInfo);

    }

    public function testBayesTaskEventsByForeach(){
        $matchId              = 'esports:match:e5a0d413-c84f-4584-a371-f24cd0a3eb5c';
//        $where                = ['match_id' => $matchId];
        $where1               = ['=','match_id', $matchId];
//        $where2               = ['>=','id',16340789];
        $limit                = 100;
        $orderBy = 'id asc';
//        $livedId = 16496732;
//        $where                = ['id' => $livedId];
        $livedData            = \app\modules\match\models\MatchLived::find()->andWhere($where1)
//            ->andWhere($where2)
            ->asArray()->limit($limit)->orderBy($orderBy)->all();
        $tag                  = 'websocket_match_data.9.websocket_match_data.add.3.';
        foreach ($livedData as $k=>$v){
            $tagInfo['ext2_type'] = '';
//        $tagInfo['ext2_type'] = 'refresh';
            $params['origin_id'] =9;
            $params['game_id'] =3;
            $params['match_id'] =$matchId;
            $params['type'] ='frames';
            $params['socket_time'] =$v['socket_time'];
            $params['socket_id'] =$v['id'];
            $params['match_data'] =$matchData =json_decode($v['match_data'],true);
            $taskInfoArr['params'] = json_encode($params,true);
            $taskInfo = $taskInfoArr;
            $type = $matchData['payload']['payload']['type'];
            $subject = $matchData['payload']['payload']['subject'];
            $action = $matchData['payload']['payload']['action'];
            $seqIdx = $matchData['seqIdx'];
            if ($k==99){
                $c=1;
            }
            if ($action=='START_MAP'){
                $c=1;
            }
            if ($action=='END_MAP'){
                $c=1;
            }


            Dota2Socket::run($tagInfo, $taskInfo);

            $redis = new Redis();
            $redis->pconnect(env('REDIS_HOST'),env('REDIS_PORT'));
//        $redis->connect(env('REDIS_HOST'),env('REDIS_PORT'));
            $redis->auth(env('REDIS_PASSWORD'));
            $redis->select(env('REDIS_DATABASE'));
            $redis->incr('speed'.$matchId);


        }


    }








}