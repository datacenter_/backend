<?php

use app\modules\common\services\Consts;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use app\rest\exceptions\BusinessException;

/**
 *
 */
class OrangeTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//        \app\modules\task\services\grab\orange\Start::run();
        \app\modules\task\services\TaskRunner::run(6856527);  //
    }
    public function testRunStart()
    {
        \app\modules\task\services\grab\riotgames\Start::run();//
    }
    //赛事
    public function testList()
    {
        $url = "/lol/event/list";
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', []);
        $json = "";
        print_r($info);
    }


    //比赛列表
    public function testMatch()
    {
        $url = "/lol/event/schedule";
        $params = ['eid' => 'qcqc1e48c46'];
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', $params);
        print_r($info);
    }

    //比赛详情
    public function testSchedule()
    {
        $url = "/lol/schedule/match";
        $params = ['matchid' => 'lmcnda646a5'];
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', $params);
        print_r($info);
    }

    //战队详情
    public function testTeam()
    {
        $url = "/lol/event/team";
        $params = ['teamid' => 'qim85d8ce57'];
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', $params);
        print_r($info);
    }

    //选手详情
    public function testPlayer()
    {
        $url = "/lol/event/player";
        $params = ['playerid' => 'ddo67f7fb87'];
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', $params);
        print_r($info);
    }

    //正在进行中的比赛
    public function testTodo()
    {
        $url = "/lol/schedule/todo";
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', []);
        print_r($info);
    }
    //历史比赛时间数据流水
    public function testmatchLog()
    {
        $url = "/lol/schedule/matchlog";
        $params = ["schedule_id"=>"lm6en1409e5",
                   "match_order"=>1,
                   "start_time"=>"1"
            ];
        $info = \app\modules\task\services\grab\orange\OrangeBase::curlGet($url, 'GET', $params);
        print_r($info);
    }
    public function testRun()
    {
        \app\modules\task\services\hot\orange\OrangeHotLolMatch::run(1, 2);
    }

    public function testguanlian()
    {
        \app\modules\task\services\hot\orange\OrangeHotLolMatch::transform(3, \app\modules\common\services\Consts::RESOURCE_TYPE_TEAM, 10086);
    }
    function testOpenjson()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotRunesReforged::class,
            "batch_id" => date("YmdHis"),
        ];
        TaskRunner::addTask($item, 2);
    }
    function testABC()
    {
        $runes ='[{"runeId": 8230, "rank": 1}, {"runeId": 8275, "rank": 1}, {"runeId": 8234, "rank": 1}, {"runeId": 8232, "rank": 1}, {"runeId": 8306, "rank": 1}, {"runeId": 8316, "rank": 1}, {"runeId": 5005, "rank": 1}, {"runeId": 5008, "rank": 1}, {"runeId": 5002, "rank": 1}]';
        $json_runes = json_decode($runes,true);
        foreach($json_runes as &$val){
            $val['rune_id'] = \app\modules\task\services\hot\HotBase::getMainIdByRelIdentityId(Consts::METADATA_TYPE_LOL_RUNE,$val['runeId'],'6');
            unset($val['runeId']);
        }
        $a = json_encode([1]);
    }
    //赛事任务
    public function testOrangeList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\orange\OrangeTournament::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params" => [
                "action" => "/lol/event/list",
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    //todo任务
    public function testDotoTask()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh","todo"),
            "type" => \app\modules\task\services\grab\orange\OrangeSchedule::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ORANGE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                "action" => "/lol/schedule/todo",
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    /**
     * 重刷sulg
     */
    public function testSulg()
    {
        $resourceType = "match";
        $model = \app\modules\task\services\MasterDataService::getMasterActiveTable($resourceType);
        $modelReslut = $model::find()->select('*')->asArray()->all();
        $result=[];
        foreach ($modelReslut as $value){

            if(array_key_exists('team_1_id',$value)){
                $teamOneId = $value['team_1_id'];
            }else{
                unset($value['team_1_id']);
            }
            // slug生成 客队id
            if(array_key_exists('team_2_id',$value)){
                $teamTwoId = $value['team_2_id'];
            }else{
                unset($value['team_2_id']);
            }
            if(isset($teamOneId)&&isset($teamTwoId)){
                $slug = \app\modules\task\services\UpdateConfig::matchTeamSlugGenerate($teamOneId,$teamTwoId,$value['scheduled_begin_at'],true);
            }else{
                continue;
            }
            $oneModel = $model::find()->where(['id'=>$value['id']])->one();
            $oneModel->setAttribute('slug',$slug);
            $oneModel->save();
            $result[] = $value['slug'];
            print_r($value);
        }
        print_r($result);
    }
    public function testaaa()
    {
        $str="~$1,600";
        $result = \app\modules\task\services\Common::pregMatchAbiosPrize($str);
    }
    public function testbb()
    {

//        $a = "spot11";
//        $result = stristr($a,"points");
//        if(!stristr($a,"points") || !stristr($a,"spot") ){
//            echo "对的";
//        }else{
//            echo "匹配上了";
//        }
        $str = "abc";
        preg_match("/(points|spot)/",$str,$return);
        if($return){
            echo "匹配上了";
        }else{
           echo "匹配不上";
        }
    }
    public function testdd()
    {
        $params['starttime'] = 1608480000;
        $params['endtime'] = 1609084799;
        $starttime = date('Y-m-d H:i:s',$params['starttime']);
        $endtime = date('Y-m-d H:i:s',$params['endtime']);
        if(isset($params['starttime'])){
            if(date("Y-m-d H:i:s") < date('Y-m-d H:i:s',$params['starttime'])){
                return (int)1;
            }
            if(date("Y-m-d H:i:s") > date('Y-m-d H:i:s',$params['starttime']) && date('Y-m-d H:i:s',$params['endtime']) > date("Y-m-d H:i:s")){
                return (int)2;
            }
            if(date("Y-m-d H:i:s") >= date('Y-m-d H:i:s',$params['endtime'])){
                return (int)3;
            }
        }
    }
    public function testTournament()
    {
        $arr = array('key' => null);
        $a = null;
        $b = (int)$a;
        if(isset($arr['key'])){
            echo 'isset';
        } else {
            echo 'unset';
        }
        echo '<br/>';
        if(array_key_exists('key', $arr)){
            echo 'key exists';
        } else {
            echo 'key does not exist';
        }
    }
    public function testRefreshMatch()
    {
        $resStandardDataMatch = \app\modules\task\models\StandardDataMatch::find()->where(['=','id',23])->asArray()->all();
        static $doend = [];
        foreach ($resStandardDataMatch as $value){
            \app\modules\data\services\binding\BindingMatchService::resetStandard($value['rel_identity_id'],Consts::RESOURCE_TYPE_MATCH,$value['origin_id'],$value['game_id']);
            print_r($value['id']);
            $doend[] = $value['id'];
        }
        print_r($doend);
    }
    public function testBayesImage()
    {
        $baseUrl='https://api.pandascore.co/matches?token=SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM&range[begin_at]=2019-01-01,2020-01-01&page=27';
        $infoJson=file_get_contents("php://input");
        $info=json_decode($infoJson,true);
        $info=array(
            'url' => 'http://api.pandascore.co/dota2/players?token=SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM&page=2&per_page=10',
            'method' => 'get'
        );
        $url=$info['url'];
        $header=$info['header'];
        $method=$info['method'];
        $postInfo=$info['post'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        if(strtolower($method)=='post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postInfo);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        print_r($output);
    }

    public function testgetImage()
    {
        $url = "https://admin.esportsdirectory.info//v1/assets/9";
        $accessToken = \app\modules\task\services\grab\bayes\BayesBase::getAccessToken();
        $header=[
            "Content-Type"=>'application/json;charset=utf-8',
            "Authorization"=>"Bearer ".$accessToken
        ];
        $imageUrl = Common::requestGet($url,$header,"");
        $mime = image_type_to_mime_type(exif_imagetype($imageUrl));
        header('Content-Type:'.$mime);
        echo file_get_contents($imageUrl);
    }
    //比赛
    public function testOverEndTimeData()
    {
        $endTimeData = date('Y-m-d H:i:s', strtotime('-32 hours'));
        $endTimeEx = explode(' ',$endTimeData);
        $endTime = sprintf("%sT%sZ",$endTimeEx[0],$endTimeEx[1]);
        $batchId = QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_ABIOS,Consts::RESOURCE_TYPE_MATCH);
        $teamInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=over,end>=$endTime",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($teamInfo, 3);

        $live=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=live",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($live, 3);


        $upcoming=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","","overMatchendTime"),
            "type"=>\app\modules\task\services\grab\abios\v3\AbiosMatchList::class,
            "batch_id" => $batchId,
            "params"=>[
                "action"=>"/series",
                "params"=>[
                    "filter"=>"lifecycle=upcoming",/**/
                    "skip" =>0,
                    "take" =>50
                ]
            ],

        ];
        TaskRunner::addTask($upcoming, 3);






    }
}