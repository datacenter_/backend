<?php

use app\modules\common\services\Consts;
use app\modules\data\models\DataResourceUpdateConfig;
use app\modules\match\models\MatchRealTimeInfo;
use app\modules\match\services\StreamService;
use app\modules\org\models\Team;
use app\modules\task\models\HotDataRunningMatch;
use app\modules\task\models\StandardDataMatch;
use app\modules\task\services\Common;
use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;
use yii\data\Pagination;

/**
 *
 */
class PandascoreTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(548552);  //
//        \app\modules\task\services\TaskRunner::run(82);  //
    }
    public function testRunStart()
    {
        \app\modules\task\services\grab\pandascore\Start::run();
    }
    public function testad()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);
    }
    //重抓

    public function testAbc()
    {
        /**
         * @param $relIdentityId(比赛id)
         * @param $resourceType(资源类型)
         * @param $originId(来源id)
         * @param $gameId(游戏id)
            * @param $reason(刷新方式)
    */
        $relIdentityId = "5307";
        $resourceType = "tournament";
        $originId = "7";
        $gameId = "1";
        return \app\modules\data\services\binding\BindingMatchService::getRefreshToStand($relIdentityId,$resourceType,$originId,$gameId);
    }
    //技能天赋
    public function testAbility()
    {
        $item = [  //dota技能天赋
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
            "batch_id" => QueueServer::setBatchId("opc",Consts::ORIGIN_PANDASCORE,Consts::METADATA_TYPE_DOTA2_ABILITY.Consts::METADATA_TYPE_DOTA2_TALENT),
            "params" => [
                "action" => "/dota2/abilities",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item,1);
    }
    //刷新正在进行中的比赛
    public function testAddTaskRunning()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "",""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/matches/running",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 3);
    }
    //刷新2020年的比赛
    public function testAddTaskpast()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "",""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => QueueServer::setBatchId("opc",Consts::ORIGIN_PANDASCORE,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
//                "action" => "/matches/past",
                "action" => "/matches",
                "params" => [
                    "range[begin_at]" => "2020-01-01,2020-12-24",
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    //刷新一个赛事的比赛
    public function testAddTaskDate()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh",""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/matches",
                "params" => [
                    "filter[serie_id]" => 2364,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    //p接口cs武器地图列表
    public function testPandascoreCSList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/csgo/weapons",  //地图csgo/maps
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testIncidents()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*5);
//        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*20);
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item =[
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreIncrementDispatcher::class,
            "batch_id" => QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::RESOURCE_TYPE_MATCH,Consts::ORIGIN_PANDASCORE),
            "params" => [
                "action" => "/incidents",
                "since" => $incidentTime,
                "params" => [
                    "page" => 1,
//
                ]
            ],
        ];
        TaskRunner::addTask($item, 3); //添加任务的方法
    }
    public function testAbcd()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60*24*18);
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
    }

    //p接口dota英雄列表
    public function testPandascoredotaList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/heroes",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }

    //p接口lol召唤师技能列表
    public function testPandascoreLOLSummonerList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/spells",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口lol道具列表
    public function testPandascoreLOLItemList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/items",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota道具列表
    public function testPandascoreDotaItem()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/items",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota技能天赋列表
    public function testPdotaAbilities()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/abilities",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    //p接口lol英雄列表
    public function testPLOLHero()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/champions",
                "params" => [
                    "page" => 1,
//                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    //p接口dota英雄列表
    public function testPDotaHero()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/heroes",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testPandascorePlayer()
    {
               $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascorePlayerList::class,
               "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/players",
                    "params" => [
                        "page" => '1',
                    ]
                ],
            ];
        TaskRunner::addTask($item, 2);
    }
    //测试pandascore赛事列表
    public function testPandascoreseriesList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/series",
                "params" => [
                    "page" => '1',
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    public function testAddTask()
    {
        $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreSeriesList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/matches",
                    "params" => [
                        "page" => 1,
                        'per_page' => 10
                    ]
                ],
            ];
            TaskRunner::addTask($item, 2);
    }
    //测试pandascore平台的战队team
    public function testPandascoreTask()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/teams",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }

    //测试pandascore平台的比赛列表
    public function testPandasmatchescore()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "",""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreMatchesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/matches",
                "params" => [
                    "filter[begin_at]" => '2020-08-03',
                ]
            ],
        ];
        TaskRunner::addTask($item, 3);
    }



    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_METADATA,[2400],[],'lol_champion');

    }

    public function testPandaPlayers()
    {
        $info = [
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "", "", "", ""),
//            "type" => \app\modules\task\services\grab\pandascore\PandaSeries::class,
//            "batch_id" => "" . time(),
//            "params" => [
//                'action' => "/matches",
//                'params' => [
////                    "filter[year]" => 2019,
//                    "range[begin_at]"=>'2019-01-01,2020-01-01',
//                    "page"=>'203',
//                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                ]
//            ],
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLTeamList::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/lol/teams",
                'params' => [
                    "filter[name]"=>'Vici Gaming',
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public static function testAB()
    {
        $param['match_id'] = 493;
//        \app\modules\match\services\MatchService::getLiveSteamList($param);
        \app\modules\match\services\MatchService::addLiveSteam(26,493);
    }
    public function testAddRiotMetadata()
    {
        $modelResult = \app\modules\data\models\StandardDataMetadata::find()
            ->select('id,metadata_type')->where(['origin_id'=>3])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
                $metadataType=$item['metadata_type'];
                \app\modules\data\services\binding\BindingMetadataService::addAndBinding($metadataType, $standardId,\app\modules\common\services\Consts::USER_TYPE_ROBOT,0);
            }catch (Exception $e){
                print_r($e);
            }

        }
    }
    public function testList()
    {
//        $params['origin_id'] = 3;
//        $params['game_id'] = 2;
        $params['date_begin'] = "2020-08-24";
        $params['date_end'] = "2020-08-25";
        $params['status'] = 1;

        \app\modules\match\services\MatchsocketService::pageList($params);
    }

    public function testa()
    {
        $a2 = date('Y-m-d H:i:s',strtotime("2020-11-03T11:42:29Z"));
    }
    public function testb()
    {
        $time = "07/16/2020 - 15:47:35.000";
        $time = substr($time, 0, 21);
        $time = str_replace(' - ', ' ', $time);
        $time = strtotime($time);
        $time = date("Y-m-d H:i:s", $time);
        return $time;
    }
    public function testRefresh()
    {
        $params['origin_id'] = 3;
        $params['resource_type'] ="match";
        $params['game_id'] = 1;
        $params['rel_id'] = 100865;
        return \app\modules\task\services\ContrastService::getMasterData($params);
    }
    //刷默认配置的方法
    public function testAddConfig()
    {
//        $metadataType = 'lol_ability';
//        $metadataType = 'lol_champion';
//        $metadataType = 'lol_item';
//        $metadataType = 'lol_rune';
//        $metadataType = 'lol_summoner_spell';
//        $metadataType = 'dota2_hero';
//        $metadataType = 'dota2_item';
//        $metadataType = 'dota2_ability';
//        $metadataType = 'dota2_talent';
//        $metadataType = 'csgo_map';
        $metadataType = 'csgo_weapon';

        $masterTableName = \app\modules\task\services\MasterDataService::getMasterActiveTable($metadataType);
        $allId = $masterTableName::find()->select('id')->asArray()->all();
        $resourceIds = array_column($allId, 'id');

        foreach ($resourceIds as $key => $resourceId) {
            \app\modules\data\services\UpdateConfigService::initResourceUpdateConfig($resourceId,$metadataType,1);
        }


//        $allData = $masterTableName::find()->select('*') ->alias('m')
//            ->leftJoin('data_resource_update_config as config','config.resource_type = '.$metadataType)
//            ->where(['=', 'm.id','config.resource_id'])->all();
    }
    public function testTe()
    {
        $matchStreamList = MatchRealTimeInfo::find()->alias('match_real')
            ->select(["match_real.id as match_id"])
            ->innerJoin('match_stream_list as msl','match_real.id = msl.match_id')
            ->where(['and',
                ['=','match_real.id',3063],
            ])->groupBy('msl.match_id')->asArray()->all();
        foreach ($matchStreamList as $value){
            $matchStreamModel = MatchRealTimeInfo::find()->where(['id'=>$value['match_id']])->one();
            $matchStreamModel->setAttribute('is_streams_supported',1);
            $matchStreamModel->save();
        }
    }
    public function testArray()
    {
        $metaDataAttribute = [
            'belong'=> null,
            'name'=> null,
            'name_cn'=> null,
            'map_type_cn'=> null,
            'map_type'=> null,
            'thumbnail'=> null,
            'rectangle_image'=> null,
            'square_image'=> null,
            'is_default'=> null,
            'short_name'=> null,
            'hotkey'=> null,
            'image'=> null,
            'small_image'=> null,
            'description'=> null,
            'description_cn'=> null,
            'slug'=> null,
            'state'=> 1,
            'title'=> null,
            'title_cn'=> null,
            'primary_role'=> null,
            'primary_role_cn'=> null,
            'secondary_role'=> null,
            'secondary_role_cn'=> null,
            'external_id'=> null,
            'external_name'=> null,
            'total_cost'=> null,
            'is_trinket'=> null,
            'is_purchasable'=> null,
            'path_name'=> null,
            'path_name_cn'=> null,
            'ammunition'=> null,
            'capacity'=> null,
            'cost'=> null,
            'kill_award'=> null,
            'kind'=> null,
            'is_recipe'=> null,
            'is_secret_shop'=> null,
            'is_home_shop'=> null,
            'is_neutral_drop'=> null,
            'primary_attr'=> null,
            'primary_attr_cn'=> null,
            'roles_cn'=> null,
            'roles'=> null,
            'data_source'=> null,
            'firing_mode'=> null,
            'is_t_available'=> null,
            'is_ct_available'=> null,
            'reload_time'=> null,
            'movement_speed'=> null,
            'left_right'=> null,
            'level'=> null,
            'talent_cn'=> null,
            'talent'=> null,
            'talents'=> null,
            'champion_id'=> null,
            'hero_id'=> null,
            'deleted'=> 2,
            'deleted_at'=> null,
        ];
        print_r(sizeof($metaDataAttribute));
    }
    public function testaa()
    {
//        $a = date('Y-m-d H:i:s',strtotime("2020-12-14T15:00:00Z"));
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*60*12);
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
    }
    public function testaav()
    {
       $debugInfo = \app\modules\task\services\crontabTask\AutoCreateMatch::run(['ext_type'=>'test'],[]);
       print_r($debugInfo);
    }
    public function testRefreshTeam()
    {
        $masterDatas = Team::find()->where(['and',
            ['<>','image',''],
            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
        ])->asArray()->all(); //team
        foreach ($masterDatas as $value){
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                    "", "", "", ""),
                "batch_id" => QueueServer::setBatchId("opc","",Consts::RESOURCE_TYPE_TEAM.Consts::RESOURCE_TYPE_TEAM),
                "type" =>"team",
                "params" =>$value
            ];
            TaskRunner::addTask($item,1);
        }
    }
    public function testRefreshPlayer()
    {
        $masterDatas = \app\modules\org\models\Player::find()->where(['and',
            ['<>','image',''],
            ['not like','image','elmt.oss-cn-hongkong.aliyuncs.com']
        ])->orderBy('id desc')->asArray()->all(); //player
        foreach ($masterDatas as $value){
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                    "", "", "", ""),
                "batch_id" => QueueServer::setBatchId("opc","",Consts::RESOURCE_TYPE_PLAYER.Consts::RESOURCE_TYPE_PLAYER),
                "type" =>"player",
                "params" =>$value
            ];
            TaskRunner::addTask($item,1);
        }
    }
    public function testRefreshsn() //快照
    {
        $masterDatas = \app\modules\org\models\TeamSnapshot::find()->where(['and',
            ['<=','id',2],
            ['<>','image',''],
            ['not like','image',' img.elementsdata.cn']
        ])->asArray()->all(); //team
        foreach ($masterDatas as $value){
            $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                    "", "", "", ""),
                "batch_id" => QueueServer::setBatchId("opc","","team_snapshot","team_snapshot"),
                "type" =>"team_snapshot",
                "params" =>$value
            ];
            TaskRunner::addTask($item,1);
        }
    }
    public function testRefreshs() //快照
    {
        for ($i =1;$i<=30000;$i++){
            $q = \app\modules\org\models\TeamSnapshot::find()->where(['and',
                ['<>','image',''],
                ['not like','image',' img.elementsdata.cn']
            ]);
            $count = $q->count();
            $page = (empty($i) ? 1 : $i) - 1;
            $pageSize = 500;
            $pages = new Pagination([
                'totalCount' => $count,
                'pageSize' => $pageSize,  // 分页默认条数是20条
                'page' => $page,
            ]);
            $masterDatas = $q->offset($pages->offset)->limit($pages->limit)
                ->asArray()->all();
            if($masterDatas){
                foreach ($masterDatas as $value){
                    $item = [
                        "tag" => QueueServer::getTag(QueueServer::QUEUE_REFRESH_HK,
                            "", "", "", ""),
                        "batch_id" => QueueServer::setBatchId("opc","","team_snapshot","team_snapshot"),
                        "type" =>"team_snapshot",
                        "params" =>$value
                    ];
//                    TaskRunner::addTask($item,1);
                }
            }else{
                break;
            }
        }
    }
    public function testpp()
    {
        $result = \app\modules\task\models\StandardDataMatch::find()->alias('std')->select(["std.id","std.rel_identity_id","std.is_battle_detailed","std.is_pbpdata_supported"])
            ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
            ->where(['dsmr.resource_type'=>\app\modules\common\services\Consts::RESOURCE_TYPE_MATCH])
            ->andWhere(['dsmr.deleted'=>2])->createCommand()->getRawSql();
//            ->asArray()->one();
    }

}