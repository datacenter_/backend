<?php

use app\modules\common\services\Consts;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class RadarPurpleTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(1595999);  //
    }
    public function test1()
    {
        $prize_di = [
            ['price'=>'abc','rank'=>'1st','team_id'=>'8481','num'=>'Lan'],
            ['price'=>'def','rank'=>'1st','team_id'=>'8481','num'=>'Lan'],
            ['price'=>'ef','rank'=>'1st','team_id'=>'8481','num'=>'Lan'],
            ['price'=>'ba','rank'=>'1st','team_id'=>'8481','num'=>'Lan']
        ];
//        $reArray = ['rank'=>'','price'=>'','score'=>'','num'=>'','team_id'=>''];
//        foreach ($prize_di as $key=>$value){
//            $reArray = ['rank'=>'','price'=>'','score'=>'','num'=>'','team_id'=>''];
//            $reArray['rank'] = $value['rank'];
//            $reArray['price'] = $value['price'];
//            $reArray['score'] = $value['score'];
//            $reArray['num'] = $value['num'];
//            $reArray['team_id'] = $value['team_id'];
//            $result []= $reArray;
//        }
        $a = json_encode([]);
    }
    public function test2()
    {
        $v = "af";
        $tArray = json_decode($v,true);
        if (!is_array($tArray)) {
            return false;
        }
    }
    public static function testjson($v)
    {
        if($v === null || $v === ''){
            return false;
        }
        $tArray = json_decode($v,true);
        if (!is_array($tArray)) {
            return false;
        }
        $error = json_last_error();
        if (!empty($error)) {
            return true;
        }
    }
    /**
     * 抓取一条数据
     */
    public function testHltvrefresh()
    {
        \app\modules\task\services\RefreshResource::refreshByResourceId(38672, "match", 3, 1, $reason = "refresh");
    }
    public function testAddTeamTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvTeam::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TEAM),
            "params" => [
                'type' => "team",
                'offset' => "0",
                'limit' => "20",
                'where' => "where update_at > '2020-12-04 00:00:00'"
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }
    public function testAddPlayerTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvPlayer::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_PLAYER),
            "params" => [
                'type' => "player",
                'offset' => "0",
                'limit' => "20",
                'where' => "where modified_at > '2020-12-04 00:00:00'"
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }
    public function testactionRefresh()
    {
//        $dateSub = date("Y-m-d H:i:s",time()-5*60);
        $dateSub = date("Y-m-d H:i:s",time()-60*60*24*4);
        $dateNow = date("Y-m-d H:i:s");
        $startTask = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "","incidents"),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "20",
//                    'where' => "WHERE update_at> DATE_SUB(NOW(),INTERVAL 2 MINUTE)", //抓1分钟内的数据
//                'where' => "WHERE match_time> '".$dateSub."'", //抓两分钟内的数据
                'where' => "WHERE update_at> '".$dateSub."' AND update_at< '".$dateNow."'", //抓两分钟内的数据
            ]
        ];
        TaskRunner::addTask($startTask, 1);
    }
    public function testAddMatchTask()
    {
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "300",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }


    public function testAddTournamentTask()
    {
        $dateSub = date("Y-m-d H:i:s",time()-60*60*24*4);
        $dateNow = date("Y-m-d H:i:s");
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvTournament::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_OPT,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_TOURNAMENT),
            "params" => [
                'type' => "tournament",
                'offset' => "0",
                'limit' => "20",
                'where' => "WHERE update_at> '".$dateSub."' AND update_at< '".$dateNow."'",
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,1);
    }

    public function testHltvIncrement()
    {
        $timeNow = date("Y-m-d H:i:s",time()-60*60*8-60*30);  //抓取半个小时以内更新的
        $incidentTime = str_replace(" ","T",$timeNow)."Z";
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\hltv\HltvIncrementDispatcher::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "params" => [
                    'type' => "all",
                    'offset' => "0",
                    'limit' => "10",
                ]
            ],
        ];
        TaskRunner::addTask($item, 1);
    }
    public function testNowMatch()
    {
        $nowTime = date('Y-m-d H:i:s');
        $info = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh"),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "100",
                'where' => "where match > '$nowTime'"
            ],
        ];
        TaskRunner::addTask($info, 2);

    }

    /**
     * @throws \yii\db\Exception
     * 刷新最近开始的比赛（按照比赛时间）
     */
    public function testa()
    {
//        $dateSub = date("Y-m-d H:i:s",time()-5*60);
//        $dateSub = date("Y-m-d H:i:s",time()-60*60*24*4);
        $dateSub = strtotime('-4 days');
        $dateNow = time();
        echo $dateNow;
        $startTask = [
            "tag" => \app\modules\task\services\QueueServer::getTag(\app\modules\task\services\QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", "refresh",""),
            "type" => \app\modules\task\services\grab\hltv\HltvMatch::class,
            "batch_id" => \app\modules\task\services\QueueServer::setBatchId(Consts::EXECUTE_TYPE_CTB,Consts::ORIGIN_HLTV,Consts::RESOURCE_TYPE_MATCH),
            "params" => [
                'type' => "match",
                'offset' => "0",
                'limit' => "20",
//                    'where' => "WHERE update_at> DATE_SUB(NOW(),INTERVAL 2 MINUTE)", //抓1分钟内的数据
//                'where' => "WHERE match_time> '".$dateSub."'", //抓两分钟内的数据
                'where' => "WHERE match_time> '".$dateSub."' AND match_time< '".$dateNow."'",
            ]
        ];
        TaskRunner::addTask($startTask, 1);
    }
    public function testactionStandardSteamIdChange()
    {
        $dateTime =  date('Y-m-d H:i:s',strtotime('- 2day'));
        $battlePlayers = \app\modules\match\models\MatchBattlePlayerCsgo::find()->select(['player_id','steam_id'])->where(['not',['steam_id'=>null]])
            ->andWhere(['>=','modified_at',$dateTime])->all();
        foreach ($battlePlayers as $itemPlayer){
            $result = \app\modules\task\models\StandardDataPlayer::find()->alias('std')->select(['std.id','std.origin_id','std.rel_identity_id','std.steam_id'])
                ->innerJoin('data_standard_master_relation as dsmr','std.id = dsmr.standard_id')
                ->where(['dsmr.resource_type'=>Consts::RESOURCE_TYPE_PLAYER])
                ->andWhere(['dsmr.master_id'=>$itemPlayer['player_id']])
                ->andWhere(['std.origin_id'=>7])->one();
            if($result['steam_id'] != $itemPlayer['steam_id']){
                $diffInfo = \app\modules\task\services\originIncrement\OriginRunBase::setStandardData(7, Consts::RESOURCE_TYPE_PLAYER, 1, $result['rel_identity_id'], ['steam_id'=>$itemPlayer['steam_id']]);
                $sub = \app\modules\task\services\originIncrement\OriginRunBase::getSubStep($diffInfo, Consts::ORIGIN_HLTV, Consts::RESOURCE_TYPE_PLAYER, 1);
                foreach ($sub as $key => $val) {
                    TaskRunner::addTask($val, 1);
                }
            }
        }
    }

}