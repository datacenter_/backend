<?php

use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\file\services\UploadService;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class RiotgamesTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//        \app\modules\task\services\TaskRunner::run(70060);  //
        \app\modules\task\services\TaskRunner::run(3626776);  //
    }
    public function testRunStart()
    {
        \app\modules\task\services\grab\riotgames\Start::run();//
    }
    public function testRiotSummoner()
    {
        //召唤师技能
            $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotSummoner::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "summoner",
                    "params" => [
                    ]
                ],
            ];
        TaskRunner::addTask($task, 2);
    }
    public function testChampion()
    {
        //技能
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotAbility::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "ability",
                "params" => [
                ]
            ],
        ];
        TaskRunner::addTask($task, 2);
    }
    //符文
    public function testRunnes()
    {
        //技能
        $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\riotgames\RiotRunesReforged::class,
                "batch_id" => date("YmdHis"),
            ];
        TaskRunner::addTask($task, 2);
    }

    /**
     * lol英雄
     * @throws \yii\db\Exception
     */
    public function testRiotHero()
    {
        //英雄
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotChampions::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "champion",
                "params" => [
//                    'id' => 1,
                ]
            ],
        ];
        TaskRunner::addTask($task, 3);
    }

    /**
     * lol技能
     * @throws \yii\db\Exception
     */
    public function testRiotAbility()
    {
        $task = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\riotgames\RiotItems::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "item",
                "params" => [
                ]
            ],
        ];
        TaskRunner::addTask($task, 1);
    }


    public function test()
    {
        $a = \app\modules\task\services\grab\riotgames\RiotgamesBase::curlNewVersion();
        printf($a);
    }

    /**
     * 元数据新增
     */
    public function testAddRiotMetadata()
    {
        $modelResult = \app\modules\data\models\StandardDataMetadata::find()
            ->select('id,metadata_type')->where(['origin_id'=>6])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
                $metadataType=$item['metadata_type'];
                BindingMetadataService::addAndBinding($metadataType, $standardId,Consts::USER_TYPE_ROBOT,0);
            }catch (Exception $e){
                print_r($e);
            }

        }
    }

    /**
     * pandascore选手新增(中国选手)
     */
    public function testBindingPlayer()
    {
        $modelResult = \app\modules\task\models\StandardDataPlayer::find()
            ->select('id')->where(['origin_id'=>3,'nationality'=> 211])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
               return \app\modules\data\services\binding\BindingPlayerService::addAndBinding($standardId, Consts::USER_TYPE_ROBOT, 0);
            }catch (Exception $e){
                print_r($e);
            }

        }
    }
    public function testToAliyun()
    {
        $base = \Yii::$app->basePath;
        $riotgamesFilePath = $base . "/data/";
        $newchampionFullPath = $riotgamesFilePath . "championFull.json";
        $newchampionFullzh_CNPath = $riotgamesFilePath . "championFullzh_CN.json";
        $newRunesReforgedPath = $riotgamesFilePath . "runesReforged.json";
        $newRunesReforgedzh_CNPath = $riotgamesFilePath . "runesReforgedzh_CN.json";
        //curl文件上传到该路径
        $curlFilePath = "riotgames/curl";
        //上传文件到阿里云
        UploadService::uploadTo($newchampionFullPath, $curlFilePath."/championFull.json");
        UploadService::uploadTo($newchampionFullzh_CNPath, $curlFilePath."/championFullzh_CN.json");
        UploadService::uploadTo($newRunesReforgedPath, $curlFilePath."/runesReforged.json");
        UploadService::uploadTo($newRunesReforgedzh_CNPath, $curlFilePath."/runesReforgedzh_CN.json");
    }
    public function testceshi()
    {
        $value = "NullLancA";
        $a = substr($value, -1);
        if(ord($a) > ord('A') && ord($a) < ord('Z')) {
            $b = $a;
        }
    }
    public function testceshis()
    {
       $tags=[
         "Active",
         "Trinket",
         "Vision",
       ];
       $a = self::testTags($tags);

    }
    public static function testTags($tags)
    {
        if(in_array("Trinket",$tags)){
            return 1;
        }else{
            return 2;
        }
    }
}