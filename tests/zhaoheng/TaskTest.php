<?php

use app\modules\task\services\grab\abios\TeamList;
use app\modules\task\services\grab\pandascore\DotaPlayer;
use app\modules\task\services\grab\pandascore\MatchList;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class TaskTest1 extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
//        \app\modules\task\services\TaskRunner::run(234922);  //
        \app\modules\task\services\TaskRunner::run(10480);  //
    }
    public function testRunStart()
    {
//        \app\modules\task\services\grab\pandascore\Start::run();
        \app\modules\task\services\grab\abios\Start::run();
    }
    //a接口dota道具列表
    public function testAbiosItemList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosDotaItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/games/1/assets",  //dota道具url，1表示dota的游戏id
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //a接口LOL道具列表
    public function testAbiosLOLItemList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosLOLItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/games/2/assets",  //dota道具url，1表示dota的游戏id
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口cs武器地图列表
    public function testPandascoreCSList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreCSWeaponsList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/csgo/weapons",  //地图csgo/maps
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota英雄列表
    public function testPandascoredotaList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/heroes",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }

    //p接口lol召唤师技能列表
    public function testPandascoreLOLSummonerList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLSummonerSpellList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/spells",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口lol道具列表
    public function testPandascoreLOLItemList()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/items",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota道具列表
    public function testPandascoreDotaItem()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaItemList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/items",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota技能天赋列表
    public function testPdotaAbilities()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaAbilitiesList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/abilities",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口lol英雄列表
    public function testPLOLHero()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreLOLHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/lol/champions",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //p接口dota英雄列表
    public function testPDotaHero()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreDotaHeroList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/dota2/heroes",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testAbiosTournament()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosTournamentList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/tournaments",
                "params" => [
                    "page" => 1,
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testPandascoreTournament()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTournamentList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/tournaments",
                "params" => [
                    "page" => 1,
                    'per_page' => 50
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //测试pandascore接口
    public function testPandascore()
    {
        $params =[
            "params" =>[
                "action" => "/teams",
                "params" => [
                    "page" => 1,
                    'per_page' => 50
                ]
            ],
        ];

        \app\modules\task\services\grab\pandascore\PandascoreBase::getResponse($params);
    }
    //获取token
    public function testgetAuthToken()
    {
        $token = \app\modules\task\services\grab\abios\AbiosBase::getAuthKey();
        print_r($token);
    }
    //测试abios的比赛列表
    public function testAbiosTask()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosMatchList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/series",
                "params" => [
                    "with[]"=>"game",
                    "page" => '1',
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    //测试abios的赛事列表
    public function testAbiosMatches()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosPlayerList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/players",
                "params" => [
                    "with[]"=>"game",
                    "page" => '1',
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testAddTask()
    {
        $item = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\pandascore\PandascoreSeriesList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/matches",
                    "params" => [
                        "page" => 1,
                        'per_page' => 10
                    ]
                ],
            ];
            TaskRunner::addTask($item, 2);
    }
    //测试abios平台的team
    public function testAbiosTasklist()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\abios\AbiosTeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/teams",
                "params" => [
                    "with[]" => 'game',
                    "page" => 1
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }
    public function testPandascoreTask()
    {
        $item = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandascoreTeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/teams",
                "params" => [
                    "page" => 1,
                    'per_page' => 10
                ]
            ],
        ];
        TaskRunner::addTask($item, 2);
    }

    public function testRefresh()
    {
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshPlayer();
    }

    public function testAt()
    {
        $teamInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => TeamList::class,
            "batch_id" => date("YmdHis"),
            "params" => [
                "action" => "/v2/teams",
                "params" => [
                    "page" => 2,
                    'with' => ['game'],
                ]
            ],
        ];
        TaskRunner::addTask($teamInfo, 2);
    }

    public function testSp()
    {
        $info = sprintf("%%%s%%", "tes");
        print_r($info);
    }

    public function testMetadata()
    {
        $heroInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLHeroList::class,
            "batch_id" => "" . time(),
            "params" => [
                "action" => "/lol/champions",
                "params" => [
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page" => 1,
                    'per_page' => 100
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($heroInfo, 3);
    }

    public function testLOLProp()
    {
        $propInfo = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLPropList::class,
            "batch_id" => "" . time(),
            "params" => [
                "action" => "/lol/items",
                "params" => [
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page" => 1,
                    'per_page' => 3
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($propInfo, 3);
    }

    public function testgetResourceUpdateConfig()
    {
        $info = \app\modules\data\services\UpdateConfigService::getResourceUpdateConfig(45, 'player');
        print_r($info);
    }

    // 这里测试抓取一个账号，产生的增量和后续，然后是更新一个账号
    public function testPlayerCatch()
    {
        // https://api.pandascore.co/dota2/players?token=SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM

        // pandascore抓取一个账号
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => DotaPlayer::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/dota2/players",
                'params' => [
                    "filter[id]" => 28013,
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info);
    }

    public function testJsonInfo()
    {
        $json = '{"last_page":2,"current_page":1,"data":[{"id":1,"title":"Dota 2","default_match_type":"team","default_map":100,"default_lineup_size":5,"color":"dc6450","long_title":"Dota 2","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/dota-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-dota-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-Dota-logo.jpg"}},{"id":2,"title":"LoL","default_match_type":"team","default_map":101,"default_lineup_size":5,"color":"5d76d0","long_title":"League of Legends","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/lol-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-lol-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-lol-logo.jpg"}},{"id":3,"title":"SCII","default_match_type":"brawl","default_map":null,"default_lineup_size":1,"color":"3c5576","long_title":"StarCraft II","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/starcraft-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-sc2-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-SCII-logo.jpg"}},{"id":4,"title":"HoN","default_match_type":"team","default_map":102,"default_lineup_size":5,"color":"397c85","long_title":"Heroes of Newerth","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/hon-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-hon-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-hon-logo.jpg"}},{"id":5,"title":"CS:GO","default_match_type":"team","default_map":null,"default_lineup_size":5,"color":"fbb766","long_title":"Counter-Strike: Global Offensive","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/cs-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-cs-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-cs-go-logo.jpg"}},{"id":6,"title":"Hearthstone","default_match_type":"brawl","default_map":103,"default_lineup_size":1,"color":"7f9ac8","long_title":"Hearthstone","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/hearthstone-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-hearthstone-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-hearthstone-logo.jpg"}},{"id":7,"title":"WoW","default_match_type":"team","default_map":null,"default_lineup_size":3,"color":"5586b0","long_title":"World of Warcraft","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/wow-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-wow-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-wow-logo.jpg"}},{"id":8,"title":"SMITE","default_match_type":"team","default_map":null,"default_lineup_size":5,"color":"425677","long_title":"SMITE","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/smite-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-smite-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-smite-logo.jpg"}},{"id":9,"title":"HotS","default_match_type":"team","default_map":null,"default_lineup_size":5,"color":"6765a3","long_title":"Heroes of the Storm","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/hots-square-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-hots-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/flat-rectangular-hots-logo.jpg"}},{"id":10,"title":"Smash","default_match_type":"brawl","default_map":null,"default_lineup_size":null,"color":"dd675e","long_title":"Super Smash Bros.","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/ssb-smash-flat-icon-square.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-ssb-smash-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/thin-flat-smash-logo-v2.png"}},{"id":11,"title":"CoD","default_match_type":"team","default_map":null,"default_lineup_size":5,"color":"c1c1cd","long_title":"Call of Duty","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/square-flat-call-of-duty-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/flat-round-call-of-duty-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/thin-flat-cod-logo-v2.png"}},{"id":12,"title":"OW","default_match_type":"team","default_map":null,"default_lineup_size":6,"color":"ffb43f","long_title":"Overwatch","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/square-overwatch-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/flat-overwatch-logo-round.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/thin-flat-overwatch-logo.png"}},{"id":13,"title":"SC","default_match_type":"brawl","default_map":null,"default_lineup_size":1,"color":"3c559b","long_title":"StarCraft","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/starcraft-rectangular-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/starcraft-circular-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/starcraft-thin-logo.png"}},{"id":14,"title":"KoG","default_match_type":"team","default_map":null,"default_lineup_size":5,"color":"00023e","long_title":"King of Glory","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/king-of-glory-square-icon-v2.png","circle":"https:\/\/img.abiosgaming.com\/games\/king-of-glory-circular-icon-v2.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/king-of-glory-rectangular-icon-v2.png"}},{"id":15,"title":"PUBG","default_match_type":"battle_royale","default_map":null,"default_lineup_size":4,"color":"F5CA28","long_title":"PlayerUnknown\'s Battlegrounds","deleted_at":null,"images":{"square":"https:\/\/img.abiosgaming.com\/games\/square-flat-pubg-logo.png","circle":"https:\/\/img.abiosgaming.com\/games\/round-flat-pubg-logo.png","rectangle":"https:\/\/img.abiosgaming.com\/games\/thin-flat-pubg-logo.png"}}]}';
        $arr = json_decode($json, true);
        $data = $arr['data'];
        foreach ($data as $k => $v) {
            echo sprintf('%s=>[\'title\'=>\'%s\'],' . "\n", $v['id'], $v['title']);
        }
    }

    public function testRefreshMetadata()
    {
        \app\modules\task\services\operation\OperationExpectBindingRefresh::refreshMetadata();
    }

    public function testRefreshOne()
    {
        \app\modules\task\services\standardIncrement\StandardMasterRelationExpectService::refresh(\app\modules\common\services\Consts::RESOURCE_TYPE_METADATA,[2400],[],'lol_champion');

    }

    public function testPandaSeries()
    {
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\PandaSeries::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/series",
                'params' => [
                    "filter[year]" => 2019,
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testPandaPlayers()
    {
        $info = [
//            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
//                "", "", "", ""),
//            "type" => \app\modules\task\services\grab\pandascore\PandaSeries::class,
//            "batch_id" => "" . time(),
//            "params" => [
//                'action' => "/matches",
//                'params' => [
////                    "filter[year]" => 2019,
//                    "range[begin_at]"=>'2019-01-01,2020-01-01',
//                    "page"=>'203',
//                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
//                ]
//            ],
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLTeamList::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/lol/teams",
                'params' => [
                    "filter[name]"=>'Vici Gaming',
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testPandaT()
    {
        $info = [
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "", "", "", ""),
            "type" => \app\modules\task\services\grab\pandascore\LOLSeries::class,
            "batch_id" => "" . time(),
            "params" => [
                'action' => "/lol/series",
                'params' => [
                    "filter[id]"=>'2738',
                    "page"=>'1',
                    "token" => 'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                ]
            ],
        ];
        \app\modules\task\services\TaskRunner::addTask($info,3);
    }

    public function testCt()
    {
        $matchInfo=[
            "tag"=>QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                "","","",""),
            "type"=>MatchList::class,
            "batch_id"=>"" . time(),
            "params"=>[
                "action"=>"/matches/running",
                "params"=>[
                    "token"=>'SV5fkFqbqhutNmXrmT1NZ7RPgnB5wUzhY6uAEC7FP0kmFeUeNtM',
                    "page"=>1,
                    'per_page'=>10
                ]
            ],

        ];
        \app\modules\task\services\TaskRunner::addTask($matchInfo,1);
    }

    public function testShow()
    {
        $list=\app\modules\receive\models\ReceiveData5eplay::find()->orderBy('id')->asArray()->all();
        foreach($list as $i){
            echo $i['log'];
        }
    }

//    public function testTask()
//    {
//        $params=[
//            'origin_type'=>'pandascore',
//            'resource_type'=>'player',
//            'game_type'=>'lol',
//            'rel_id'=>1,
//        ];
//        \app\modules\task\services\ContrastService::refresh($params);
//    }

}