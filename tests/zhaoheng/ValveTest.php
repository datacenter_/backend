<?php

use app\modules\common\services\Consts;
use app\modules\data\services\binding\BindingMetadataService;
use app\modules\task\services\grab\valve\ValveDotaHeroList;
use app\modules\task\services\hot\HotBase;
use app\modules\task\services\QueueServer;
use app\modules\task\services\TaskRunner;

/**
 *
 */
class ValveTest extends PHPUnit\Framework\TestCase
{
    public function testRunTask()
    {
        \app\modules\task\services\TaskRunner::run(301336);  //
    }
    public function testDota2()
    {
        //dota2英雄
            $task = [
                "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB,
                    "", "", "", ""),
                "type" => \app\modules\task\services\grab\valve\ValveDotaHeroList::class,
                "batch_id" => date("YmdHis"),
                "params" => [
                    "action" => "/IEconDOTA2_570/GetHeroes/v1",
                    "params" => [
                        "language"=>"zh,en",
                    ]
                ],
            ];
        TaskRunner::addTask($task, 3);
    }
    public function test()
    {
        $response = null;
        $batchId = 's'.time();
        $reason = "refresh";
        $task = [
            "type" => ValveDotaHeroList::class,
            "tag" => QueueServer::getTag(QueueServer::QUEUE_MAJOR_GRAB, "", "", "", "", $reason),
            "batch_id" => $batchId,
            "params" => [
                'response'=>$response,
                'action'=>'/IEconDOTA2_570/GetHeroes/v1',
                'params'=>[
                    "language"=>"zh,en",
                    'id' => 1,
                ],

            ],
        ];
        TaskRunner::addTask($task, 3);
    }

    /**
     * 元数据新增
     */
    public function testAddRiotMetadata()
    {
        $modelResult = \app\modules\data\models\StandardDataMetadata::find()
            ->select('id,metadata_type')->where(['origin_id'=>6])
            ->asArray()->all();
        foreach ($modelResult as $item){
            try {
                $standardId=$item['id'];
                $metadataType=$item['metadata_type'];
                BindingMetadataService::addAndBinding($metadataType, $standardId,Consts::USER_TYPE_ROBOT,0);
            }catch (Exception $e){
                print_r($e);
            }

        }
    }

    /**
     * pandascore选手新增(中国选手)
     */
    public function testBindingPlayer()
    {
        $url = "https://ddragon.leagueoflegends.com/api/versions.json?";
//        $info = [
//            'url' => $url,
//            'method' => 'get',
//        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $url);
        $output = curl_exec($ch);
        curl_close($ch);
    }
    public function testAB()
    {
        $data = date("Y-m-d h:i:s","1600930002");
    }
    public function testc()
    {
        echo "更新元数据拳头";
        $data = date("Y-m-d h:i:s","1600930002");

    }
}