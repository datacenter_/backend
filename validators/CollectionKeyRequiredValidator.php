<?php
namespace app\validators;

use yii\validators\Validator;

/**
 * 验证数组中必要key
 * Class CollectionKeyRequiredValidator
 * @package app\validators
 */
class CollectionKeyRequiredValidator extends Validator
{
    /**
     * 需要验证的属性
     * @var
     */
    public $requiredKeys;

    public function init()
    {
        $this->message = null;

        if ($this->message === null) {
            $this->message = \Yii::t('app', '{attribute} is invalid');
        }

        parent::init();
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!empty($model->$attribute) && !empty($this->requiredKeys)) {
            $keys = array_keys($model->$attribute);

            foreach ($this->requiredKeys as $key) {
                if (!isset($model->$attribute[$key]) || strlen(trim(strval($model->$attribute[$key]))) <= 0) {
                    $this->addError($model, $attribute, $this->message);
                    break;
                }
            }
        }
    }
}