<?php
namespace app\validators;

use yii\validators\Validator;

/**
 * 对数据内value调用trim  只支持一维数组
 * Class AarryValueTrimFilter
 * @package app\validators
 */
class CollectionValueTrimValidator extends Validator
{
    /**
     * @param $value
     * @return array
     * ```code
     * $value = [
     *   key1 => value1,
     *   key2 => value2
     * ]
     * ```
     */
    public static function trim(array $value = []): array
    {
        if (!empty($value)) {
            $trimValue = [];
            foreach ($value as $key => $v) {
                $trimValue[$key] = trim($v);
            }

            return $trimValue;
        }

        return $value;
    }
}