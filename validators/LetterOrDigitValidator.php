<?php
namespace app\validators;


use yii\validators\Validator;

/**
 * 字母和数字验证
 * Class MobilPhoneValidator
 * @package app\validators
 */
class LetterOrDigitValidator extends Validator
{
    public $pattern = '/^[A-Za-z0-9]+$/';

    public function init()
    {
        parent::init();

        if ($this->message === null) {
            $this->message = \Yii::t('app', '{attribute} only accept word and number');
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match($this->pattern, $model->{$attribute})) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}